#!/bin/bash

echo "Content-type: text/html"
echo ""

echo '<!DOCTYPE html>'
echo '<html lang="en">'
echo '<head>'
echo '<title>Combined AMPs DB</title>'
echo '<meta name="viewport" content="width=1080">'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<link rel="shortcut icon" href="../favicon.ico" />'
echo '<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">'
echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">'
# Using local fonts is faster and more reliable
#echo '<link rel="stylesheet" media="none" href="https://fontlibrary.org/face/dejavu-sans-mono" onload="this.media='all';" type="text/css"/>'

echo '<link href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" rel="stylesheet">'

echo '<link rel="stylesheet" href="../alerts-css-master/assets/css/alerts-css.min.css">'
echo '<script src="../alerts-css-master/assets/js/alerts.min.js"></script>' 

# For autocompletion
echo '<!-- JS file -->'
echo '<script async src="../awesomplete/awesomplete.min.js"></script>'

echo '<!-- CSS file -->'
echo '<link rel="stylesheet" href="../awesomplete/awesomplete.css">'

echo '<style>'
echo '.tooltip {'
echo '    position: relative;'
echo '    display: inline-block;'
echo '    border-bottom: 0px dotted black;'
echo '}'

echo '.tooltip .tooltiptext {'
echo '   '
echo '    visibility: hidden;'
echo '    width: 320px;'
echo '    background-color: black;'
echo '    color: #fff;'
echo '    text-align: center;'
echo '    border-radius: 6px;'
echo '    padding: 5px 5px;'
echo '    position: absolute;'
echo '    z-index: 1;'
# This for right tooltips
#echo '    top: -5px;'
#echo '    left: 120%;'
# This for top tooltips
echo '    bottom: 100%;'
echo '    left: 50%; '
echo '    margin-left: -160px; /* Use half of the width (120/2 = 60), to center the tooltip */'
echo '}'

echo '.tooltip .tooltiptext::after {'
echo '    content: "";'
echo '    position: absolute;'
# This for right tooltips
#echo '    top: 50%;'
#echo '    right: 100%; /* To the left of the tooltip */'
#echo '    margin-top: -5px;'
# This for top tooltips
echo '    top: 100%; /* At the bottom of the tooltip */'
echo '    left: 50%;'
echo '    margin-left: -5px;'
echo '    border-width: 5px;'
echo '    border-style: solid;'
#echo '    border-color: transparent black transparent transparent;'
echo '    border-color: black transparent transparent transparent;'
echo '}'
echo '.tooltip:hover .tooltiptext {'
echo '    visibility: visible;'
echo '}'

# Needed to display blocks in big list of extract options properly without cutting in the middle
echo 'label {'
echo '  display: inline-block;'
echo '}'

echo 'input:hover[type="submit"] {background: green;}'
echo 'input:hover[type="reset"] {background: red;}'
echo 'button:hover {background: blue;}'

echo 'input[type="radio"]{'
echo '  -webkit-appearance: none;'
echo '  -moz-appearance: none;'
echo '  appearance: none;'
echo
echo '  border-radius: 50%;'
echo '  width: 16px;'
echo '  height: 16px;'
echo
echo '  border: 2px solid #999;'
echo '  transition: 0.2s all linear;'
echo '  outline: none;'
#echo '  margin-right: 1px;'
echo '  margin-left: 10px;'
echo
echo '  position: relative;'
echo '  top: 4px;'
echo '}'
echo
echo 'input:checked {'
echo '  border: 6px solid grey;'
echo '}'

# For the boxes... as it doesn't work for radio
#echo 'input:hover {'
#echo '  color: red;'
#echo '}'

echo '#banner {'
echo '  padding: 10px;'
echo '  color: rgb(80,80,80);'
echo '  text-align: center;'
echo '  text-decoration: underline;'
#echo '  text-decoration-color: red;'
echo '  letter-spacing: 5px;'
echo '  text-shadow: 1px 1px gray;'
echo '  font-size: 40px;'
echo '}'
echo
echo '#header {'
echo '  background-image: repeating-linear-gradient(-45deg, rgba(255,255,255,0.15), rgba(255,255,255,0.15) 15px, transparent 15px, transparent 25px), linear-gradient(to top ,'
echo '  rgba(230,230,230,0.2), rgba(120,120,120,0.5));'
echo '  height: 80px;'
echo '  border-radius: 10px 10px 10px 10px;'
echo '}'

echo 'a:hover {color: red;}'
echo 'body, h1,h2,h3,h4,h5,h6 {font-family: "Ubuntu", sans-serif}'
echo 'fieldset{border-radius: 10px; border:2px solid #bbb;margin:0 2px;padding:.35em .625em .75em}'
echo 'hr {border:0;border-top:5px solid #eee;margin:20px 0}'
echo '.w3-row-padding img {margin-bottom: 12px}'
echo '/* Set the width of the sidebar to 120px */'
echo '.w3-sidebar {width: 120px;background: #bbb;}'
echo '/* Add a left margin to the "page content" that matches the width of the sidebar (120px) */'
echo '#main {margin-left: 120px}'
echo '/* Remove margins from "page content" on small screens */'
echo '@media only screen and (max-width: 600px) {#main {margin-left: 0}}'

echo "select {"
#echo "    width: 10%;"
#echo "    padding: 10px 10px;"
echo "    border: none;"
echo "    border-radius: 4px;"
echo "    background-color: #ffffff;"
echo "}"
echo "button, input[type=button], input[type=reset] {"
echo "    background-color: #616161;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=submit] {"
echo "    background-color: DarkGreen;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=text], select {"
#echo "    width: 100%;"
echo "    padding: 5px 10px;"
#echo "    margin: 8px 0;"
echo "    display: inline-block;"
echo "    border: 1px solid #ccc;"
echo "    border-radius: 4px;"
echo "    box-sizing: border-box;"
echo "}"

# Using local fonts is faster and more reliable
#echo 'pre {font-family: 'DejaVu Sans Mono'}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Regular.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo 'pre {font-family: "Local Dejavu", "Local Liberation", monospace; font-size: 14px}'

echo '* {box-sizing: border-box;}'

echo '/* Button used to open the characters picker - fixed at the bottom of the page */'
echo '.open-button-chars {'
echo '  background-color: #555;'
echo '  color: white;'
#echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  opacity: 0.8;'
#echo '  position: fixed;'
#echo '  bottom: 23px;'
#echo '  right: 28px;'
#echo '  width: 280px;'
echo '}'

echo '/* The characters picker window - hidden by default */'
echo '.characters-popup {'
echo '  display: none;'
echo '  position: fixed;'
echo '  bottom: 23px;'
echo '  right: 28px;'
echo '  border: 3px solid #f1f1f1;'
echo '  z-index: 9;'
echo '}'

# Break names in the table, as they can be really long
echo '.characters-popup td {'
echo '  word-break: break-all;'
echo '}'

echo '/* Add styles to the form container */'
echo '.form-container-chars {'
echo '  max-width: 550px;'
echo '  padding: 10px;'
echo '  padding: 16px 20px;'
echo '  background-color: white;'
echo '}'

echo '/* Full-width textarea */'
echo '.form-container-chars textarea {'
echo '  width: 100%;'
echo '  padding: 15px;'
echo '  margin: 5px 0 22px 0;'
echo '  border: none;'
echo '  background: #f1f1f1;'
echo '  resize: none;'
echo '  min-height: 200px;'
echo '}'

echo '/* When the textarea gets focus, do something */'
echo '.form-container-chars textarea:focus {'
echo '  background-color: #ddd;'
echo '  outline: none;'
echo '}'

echo '/* Set a style for the submit/send button */'
echo '.form-container-chars .btn {'
echo '  background-color: #4CAF50;'
echo '  color: white;'
echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  width: 100%;'
echo '  margin-bottom:10px;'
echo '  opacity: 0.8;'
echo '}'

echo '/* Add a red background color to the cancel button */'
echo '.form-container-chars .cancel {'
echo '  background-color: red;'
echo '}'

echo '/* Add some hover effects to buttons */'
echo '.form-container-chars .btn:hover, .open-button-chars:hover {'
echo '  opacity: 1;'
echo '}'

echo '</style>'
echo '</head>'
echo '<body class="w3-light-grey">'

echo '<!-- Icon Bar (Sidebar) -->'
echo '<nav class="w3-sidebar w3-bar-block w3-small w3-center">'
echo '  <!-- Avatar image in top left corner -->'
echo '  <img src="../webicons/icon.svg" alt="ADAPTABLE logo" style="width:100%">'
echo '  <a href="../index.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-home w3-xxlarge"></i>'
echo '    <p>HOME</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./adaptable-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-cogs w3-xxlarge"></i>'
echo '    <p>FAMILY GENERATOR</p>'
echo '  </a>'
echo '  <a href="./extract-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-search-plus w3-xxlarge"></i>'
echo '    <p>FAMILY ANALYZER</p>'
echo '  </a>'
echo '  <a href="./index-results.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-download w3-xxlarge"></i>'
echo '    <p>DOWNLOAD RESULTS</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./index-browse.sh" class="w3-bar-item w3-button w3-padding-large w3-light-grey">'
echo '    <i class="fas fa-list-ul w3-xxlarge"></i>'
echo '    <p>BROWSE AMPs & FAMILIES</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="../faq.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-question-circle w3-xxlarge"></i>'
echo '    <p>FAQ & TUTORIAL</p>'
echo '  </a>'
echo '  <a href="../about.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-users w3-xxlarge"></i>'
echo '    <p>ABOUT</p>'
echo '  </a>'
echo '</nav>'

echo '<div class="w3-padding-large" id="main">'

echo '<div id="header"> '
echo '        <div id="banner">'
echo '        ADAPTABLE'
echo '        </div>'
echo '</div>'

echo \
	"<article>"\
	"<header>"\
	"<h1>Browse AMPs database</h1>"\
	"<h6>Find information from online databases and/or sequence related peptides (ADAPTABLE families)</h6>"\
	"</header>"\
	"</article>"

source ./tooltips.sh

if [ -z "$QUERY_STRING" ]; then
	echo "<hr>"
        echo "<form method=GET style='display: inline-block;'>"
        echo "<div class="tooltip" style='display: inline-block;'><h6 style='display: inline-block;'>Peptide sequence:</h6><span class="tooltiptext">"${tooltiptext_peptide_sequence}"</span></div><input id="myinput" type="text" name="_peptide_sequence" size=70 placeholder='Search peptide by sequence or name' pattern='[A-Za-zԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]+' required>"
        echo "<input type="submit" value='Submit'>"
        echo "<input type="reset" value="Reset">"
        echo "</form>"

# Character selector
echo '<script>'
echo 'function openForm() {'
echo '    document.getElementById("myForm").style.display = "inline-block";'
echo '}'
echo 'function closeForm() {'
echo '    document.getElementById("myForm").style.display = "none";'
echo '}'
echo '</script>'

echo '<div class="characters-popup" id="myForm">'
echo '  <form class="form-container-chars">'
echo '    <h3>Special characters for modified aminoacids</h3>'
echo "<i>You can click on the symbol to select it and use Ctrl+F to search your desired aminoacid</i>"
echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
cat ../character-selector | sed -e 's/input-text/myinput/g'
echo '</div>'
echo '    <button type="button" class="btn cancel" onclick="closeForm()"><i class="fas fa-signature"></i></button>'
echo '  </form>'
echo '</div>'
echo '<button class="open-button-chars" onclick="openForm()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'
#

                echo '</div>'
echo '<script>'
echo 'var input = document.getElementById("myinput");'

echo '// Show label but insert value into the input:'
echo 'new Awesomplete(input, {'
echo '  maxItems: 20,'
echo '  list: ['
zcat ../DATABASE.seqs.js.awesomplete.gz
echo '  ]'
echo '});'
echo '</script>'

        exit 0
fi

if [ "$QUERY_STRING" ]; then
        echo "<form name='sel' style='display: inline-block;' method=GET>"
	echo "<div class="tooltip"><h6 style='display: inline-block;'>Peptide sequence:</h6><span class="tooltiptext">"${tooltiptext_peptide_sequence}"</span></div><input id="myinput" type="text" name="_peptide_sequence" size=70 placeholder='Search peptide by sequence or name' pattern='[A-Za-zԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]+' required>"
        echo "<input type="submit" value='Submit'>"
        echo "<input type="reset" value="Reset">"
        echo "</form>"

# Shorten addressbar
echo "<script>"
echo 'function shorten_addressbar_url() {'
echo 'var url = window.location.href;'
echo "var new_url = url.split('?')[0];"
echo "history.replaceState('1', '', new_url);"
echo '}'

echo 'shorten_addressbar_url();'
echo "</script>"

echo '<script>'
echo 'var input = document.getElementById("myinput");'

echo '// Show label but insert value into the input:'
echo 'new Awesomplete(input, {'
echo '  maxItems: 20,'
echo '  list: ['
zcat ../DATABASE.seqs.js.awesomplete.gz
echo '  ]'
echo '});'
echo '</script>'

# Character selector
echo '<script>'
echo 'function openForm() {'
echo '    document.getElementById("myForm").style.display = "inline-block";'
echo '}'
echo 'function closeForm() {'
echo '    document.getElementById("myForm").style.display = "none";'
echo '}'
echo '</script>'

echo '<div class="characters-popup" id="myForm">'
echo '  <form class="form-container-chars">'
echo '    <h2>Special characters for modifed aminoacids</h2>'
echo "<i>You can click on the symbol to select it and use Ctrl+F to search your desired aminoacid</i>"
echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
cat ../character-selector | sed -e 's/input-text/myinput/g'
echo '</div>'
echo '    <button type="button" class="btn cancel" onclick="closeForm()"><i class="fas fa-signature"></i></button>'
echo '  </form>'
echo '</div>'
echo '<button class="open-button-chars" onclick="openForm()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'
#

        export __peptide_sequence=$(echo "$QUERY_STRING" | sed -n "s/^.*peptide_sequence=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
	export utf8_seq=$(echo -e $(echo "${__peptide_sequence}" | sed -e 's/%/.0x/g'| sed -e 's/0x\(..\)\.\?/\\x\1/g')|sed -e 's/\.//g')
	export __peptide_sequence="${utf8_seq}"
	
        # Check for number of occurrences
	search_result_items=$(grep -c "${__peptide_sequence}" ADAPTABLE/DATABASE.seqs)
	if [ "${search_result_items}" == 0 ]; then
	        echo '<div class="container-alert"><div class="alert alert_danger"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
	        echo "<h4>No peptide with that pattern sequence was found, please try a different one</h4>"
        	echo '</div></div></div>'

		echo "</div>"
		exit 0
#	fi
	else
#	if [[ "${search_result_items}" < 10 && "${search_result_items}" > 1 ]]; then
#		echo '<div class="container-alert"><div class="alert alert_warning"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
#        	echo "<h4>Please specify one of the following peptides mathing your search pattern</h4>"
#	        echo '</div></div></div>'

#		echo "<pre>"
#                echo "$(grep -i -a "${__peptide_sequence}" ADAPTABLE/DATABASE.seqs)"
#                echo "</pre>"
#		echo "</div>"
#                exit 0
#        fi

#if [ "${search_result_items}" == 1 ] || [ $(grep -i -x -c "${__peptide_sequence}" ADAPTABLE/DATABASE.seqs) == 1 ]; then

	# Update title of the window/tab
        echo "<script>"
        echo "document.title = 'AMP: ${__peptide_sequence}';"
        echo "</script>"

	echo '<div class="w3-container">'
	echo '  <h2>AMP properties</h2>'

	# Run some checks before starting to use the needed variables
	if ! grep -q -a "${__peptide_sequence}" ADAPTABLE/DATABASE.seqs; then
		echo "<h6><b>${__peptide_sequence}</b> not found in our database: showing alternative AMP containing its pattern</h6>"
		export __peptide_sequence=$(grep -a "${__peptide_sequence}" ADAPTABLE/DATABASE.seqs)
	fi

        my_pep_num="1"
        peptide_info=$(grep -a " ${__peptide_sequence} " ADAPTABLE/DATABASE|iconv -f utf-8 -t utf-8 -c)
        for i in ${peptide_info// / }; do
                export "var$my_pep_num"="$i"
                my_pep_num=$((my_pep_num+1))
        done

	label_num=1
	# Need to be listed in the order of analyse_AMPs_database_head.awk
	for x in {ID,sequence,name,source,Family,gene,stereo,N_terminus,C_terminus,PTM,cyclic,target,synthetic,antimicrobial,antibacterial,\
antigram_pos,antigram_neg,antifungal,antiyeast,antiviral,antiprotozoal,antiparasitic,antiplasmodial,antitrypanosomic,antileishmania,\
insecticidal,anticancer,antitumor,cell_line,tissue,cancer_type,anticancer_activity,anticancer_activity_test,antiangiogenic,toxic,\
cytotoxic,hemolytic,hemolytic_activity,hemolytic_activity_test,RBC_source,cell_cell,hormone,quorum_sensing,immunomodulant,antihypertensive,\
drug_delivery,cell_penetrating,tumor_homing,blood_brain,antioxidant,antiproliferative,DSSP,PDB,experim_structure,PMID,\
taxonomy,all_organisms,activity_viral,activity_viral_test,\
ribosomal,experimental,biofilm,virus_name,\
solubility,activity,activity_test,sequence_length_range,n_regions,align_method,\
level_agreement,families_to_analyse,min_seq_length,work_in_simplified_aa_space,verbose,plot_graphs,design_family_representative_peptide,analyse_only_region_of_the_fam_father,calculation_label,simplify_aminoacids};do
		export "label$label_num"="$x"
		label_num=$((label_num+1))
	done
	
	echo "	  <h5>Properties of AMP <b>${__peptide_sequence}</b></h5>"

	echo '  <table style="table-layout: fixed;" class="w3-table-all w3-bordered w3-hoverable w3-card-4">'
	echo '    <tr>'
	echo '      <th>Property</th>'
	echo '     <th style="width: 70%;">Value</th>'
	echo '    </tr>'

	# Add a special field to link peptides with our 'all_families' built-in DB
	# We need pipefail inside the subshell to properly die when grep doesn't find anything
	# We need to run with -w to match exact peptide 'word'

	# Run commands together and retrieve their outputs to allow the table to be shown apparently 'faster' without waiting for each 'grep' command to end
	in_all_families=$(set -o pipefail && grep -w -a ${__peptide_sequence} ADAPTABLE/Results/all_families/.family_fathers|cut -d: -f1)
	member_all_families=$(set -o pipefail && grep -w -a ${__peptide_sequence} ADAPTABLE/Results/all_families/.outputs/f*.seqs|sort -V|cut -d/ -f5|cut -d: -f1|sed -e 's/\.seqs//')

	#if in_all_families=$(set -o pipefail && grep -w -a ${__peptide_sequence} ADAPTABLE/Results/all_families/.family_fathers|cut -d: -f1); then
	if [[ ${in_all_families} ]]; then
		echo '    <tr>'
		echo "          <td style='background-color: LightGrey;'>Best representing member (father) of the following ADAPTABLE family:</td>"
		echo "<td style='background-color: LightGrey;'>"
		echo "<a href='./all_familiesDB.sh?_fnum=${in_all_families/f/}'>${in_all_families}</a>"
		echo "</td>"
		echo "</tr>"
	fi
	
	# We need another field for listing in what family numbers this peptide is contained
	#if member_all_families=$(set -o pipefail && grep -w -a ${__peptide_sequence} ADAPTABLE/Results/all_families/.outputs/f*.seqs|cut -d/ -f5|cut -d: -f1|sed -e 's/\.seqs//'); then
	if [[ ${member_all_families} ]]; then
		echo '    <tr>'
		echo "          <td style='background-color: LightGrey;'>Member of the following ADAPTABLE families (all_families DB):</td>"
		echo "<td style='background-color: LightGrey;'>"
		for i in ${member_all_families}; do
			echo "<a href='./all_familiesDB.sh?_fnum=${i/f/}'>${i}</a>"
		done
		echo "</td>"
		echo "</tr>"
	fi
	
	# Special handling for ID (var/label1)
	echo '    <tr>'
	echo "		<td>External database "${label1}"</td>"

	id=${var1/>/}
	echo "<td>"
	for k in ${id//;/ }; do
		[[ ${k} = *"satpdb"* ]] && echo "<a href='http://crdd.osdd.net/raghava/satpdb/display_seq.php?details=${k}' target="_blank">${k}</a>"
		[[ ${k} = *"hemo"* ]] && echo "<a href='http://crdd.osdd.net/raghava/hemolytik/display.php?details=${k/hemo/}' target="_blank">${k}</a>"
		[[ ${k} = *"DRAMP"* ]] && echo "<a href='http://dramp.cpu-bioinfor.org/browse/All_Information.php?id=${k}' target="_blank">${k}</a>"
		[[ ${k} = *"canPPD"* ]] && echo "<a href='http://crdd.osdd.net/raghava/cancerppd/display_sub.php?details=${k/canPPD}' target="_blank">${k}</a>"
		[[ ${k} = *"PHYT"* ]] && echo "<a href='http://phytamp.hammamilab.org/${k}' target="_blank">${k}</a>"
		[[ ${k} = *"CAMPSQ"* ]] && echo "<a href='http://www.camp.bicnirrh.res.in/seqDisp.php?id=${k}' target="_blank">${k}</a>"
		[[ ${k} = *"HIP"* ]] && echo "<a href='http://crdd.osdd.net/servers/hipdb/record.php?details=${k}' target="_blank">${k}</a>"
		[[ ${k} = *"DBAASP"* ]] && echo "<a href='https://dbaasp.org/peptide-card?id=${k/DBAASP}' target="_blank">${k}</a>"
                [[ ${k} = *"APD_"* ]] && echo "<a href='http://aps.unmc.edu/AP/database/query_output.php?ID=${k/APD_AP}' target="_blank">${k}</a>"
                [[ ${k} = *"InverPep"* ]] && echo "<a href='http://ciencias.medellin.unal.edu.co/gruposdeinvestigacion/prospeccionydisenobiomoleculas/InverPep/public/Peptido/see/${k/InverPep}' target="_blank">${k}</a>"
                [[ ${k} = *"ParaPep"* ]] && echo "<a href='http://crdd.osdd.net/raghava/parapep/display_sub.php?details=${k/ParaPep}' target="_blank">${k}</a>"
                [[ ${k} = *"ANTISTAPHY"* ]] && echo "${k}" # Upstream dead
                [[ ${k} = *"DADP_"* ]] && echo "<a href='http://split4.pmfst.hr/dadp/?a=kartica&id=SP_${k/DADP_}' target="_blank">${k}</a>"
                [[ ${k} = *"MilkAMP"* ]] && echo "<a href='http://milkampdb.org/${k/MilkAMP}' target="_blank">${k}</a>"
                [[ ${k} = *"BACTIBASE_"* ]] && echo "<a href='http://bactibase.hammamilab.org/${k/BACTIBASE_}' target="_blank">${k}</a>"
                [[ ${k} = *"BAAMPS"* ]] && echo "<a href='http://www.baamps.it/peptidelist?task=peptide.display&ID=${k/BAAMPS}' target="_blank">${k}</a>"
                [[ ${k} = *"Defe"* ]] && echo "<a href='http://defensins.bii.a-star.edu.sg/pops/pop_proteinDetails.php?id=${k/Defe}' target="_blank">${k}</a>"
                [[ ${k} = *"Conoserver"* ]] && echo "<a href='http://www.conoserver.org/index.php?page=card&table=protein&id=${k/ConoserverP0}' target="_blank">${k}</a>"
                [[ ${k} = "AVP"* ]] && echo "<a href='http://crdd.osdd.net/servers/avpdb/record.php?details=${k}' target="_blank">${k}</a>" # AVP 
		[[ ${k} = *"MAVP"* ]] && echo "<a href='http://crdd.osdd.net/servers/avpdb/record2.php?details=${k}' target="_blank">${k}</a>" #  MAVP
                [[ ${k} = *"LAMP"* ]] && echo "<a href='http://biotechlab.fudan.edu.cn/database/lamp/detail.php?id=${k/LAMP}' target="_blank">${k}</a>"
		# Use faster IP for now
#                [[ ${k} = "ADAM"* ]] && echo "<a href='http://bioinformatics.cs.ntou.edu.tw/ADAM/adam_info.php?f=${k}' target="_blank">${k}</a>" # don't conflict with YADAMP
		[[ ${k} = "ADAM"* ]] && echo "<a href='http://140.121.197.185/ADAM/adam_info.php?f=${k}' target="_blank">${k}</a>" # don't conflict with YADAMP
                [[ ${k} = *"YADAMP"* ]] && echo "<a href='http://yadamp.unisa.it/showItem.aspx?yadampid=1${k/YADAMP}' target="_blank">${k}</a>"
                [[ ${k} = *"Peptaibol"* ]] && echo "<a href='http://peptaibol.cryst.bbk.ac.uk/singlerecords/${k/Peptaibol}.html' target="_blank">${k}</a>"
                [[ ${k} = *"CPP"* ]] && echo "<a href='http://crdd.osdd.net/raghava/cppsite/display.php?details=${k/CPP}' target="_blank">${k}</a>"
                [[ ${k} = *"uniprot"* ]] && echo "<a href='https://www.uniprot.org/uniprot/${k/uniprot}' target="_blank">${k}</a>"
	done
	echo "</td>"
	echo '	  </tr>'
	
	# Handling of remaining fields
	count=2
	while [ -v var$count ]; do
		if [ $(eval "echo \$var"${count}"") != "_" ]; then
			echo '    <tr>'
			# Simply print "Yes" when label and value are the same
			if [ $(eval "echo \$label"${count}"") == $(eval "echo \$var"${count}"") ]; then
				export var${count}="Yes"
			fi

			if [ $(eval "echo \$label"${count}"") == sequence ]; then
				export sequence=$(eval "echo \$var"${count}"")
				# FIXME: I cannot use builtin bash ${sequence//[!ARNDCQEGHILKMFPSTWYVOUarndcqeghilkmfpstwyvou]/X} because it adds more X due to some strange chars being around (works out of web page...)
				# We need to replace not standard aa with X for Protparam
				export seq_for_protparam=$(echo ${sequence} | sed -r 's/[^ARNDCQEGHILKMFPSTWYVOUarndcqeghilkmfpstwyvou]+/X/g')
				export links="${sequence}&nbsp;&nbsp;[<a href='https://web.expasy.org/cgi-bin/protparam/protparam/?sequence=${seq_for_protparam}' target="_blank">Go to ProtParam</a>]"
				export var${count}="${links}"
				
				# The same for running pepwheel and generate helical wheel diagram
				# SVG is better, but pepwheel in Ubuntu is buggy (works fine for me on Gentoo)
				# https://bugs.launchpad.net/ubuntu/+source/emboss/+bug/1862960
				# Switch to pdf for now
				mkdir -p ADAPTABLE/tmp_files3
				rm -f ADAPTABLE/tmp_files3/pepwheel*
				echo -e ">\n${seq_for_protparam}" > ADAPTABLE/tmp_files3/pepwheel.seq
				cd ADAPTABLE/tmp_files3/
				#pepwheel -seq pepwheel.seq -graph svg
				#mv pepwheel pepwheel.svg
				# FIXME: Send ugly command output to /dev/null
				pepwheel -seq pepwheel.seq -graph pdf 1>/dev/null
				mv pepwheel pepwheel.pdf
				cd ../..

				#export links="&nbsp;&nbsp;[<a href='../tmp/pepwheel.svg' target="_blank">Helical wheel diagram</a>]"
				export links="&nbsp;&nbsp;[<a href='../tmp/pepwheel.pdf' target="_blank">Helical wheel diagram</a>]"
				export var${count}+=" ${links}"

				# We also append here the I-Tasser information, if available
				export itasserdir=$(grep -w -a ${sequence} ADAPTABLE/itasserDB-sequences.index|cut -d: -f1)
				if [ -n "${itasserdir}" ]; then
					export links="&nbsp;&nbsp;[<a href='../itasserDB/${itasserdir}/index.html' target="_blank"><img src="../webicons/ITASSER.png" height="24">Review I-Tasser protein structure and function prediction info</a>]"
					export var${count}+=" ${links}"
				fi
			fi

			if [[ $(eval "echo \$label"${count}"") == PDB || $(eval "echo \$label"${count}"") == experim_structure ]]; then
				pdb=$(eval "echo \$var"${count}"")
				export links=""
				for j in ${pdb//;/ }; do
					if [[ ${j} = *"satpdb"* ]]; then
						export links="${links} <a href='http://crdd.osdd.net/raghava/satpdb/structures/allpdbs/${j}' target="_blank">${j}</a>"
					else
						if [[ ${j} = "AF-"* ]]; then
							export links="${links} <a href='https://alphafold.ebi.ac.uk/entry/${j}' target="_blank">${j} (AlphaFold model)</a>"
						else
							export links="${links} <a href='https://www.rcsb.org/structure/$(echo ${j}|sed -e 's/\.pdb//')' target="_blank">${j}</a>"
						fi
					fi
				done
				export var${count}="${links}"
			fi
			
			if [ $(eval "echo \$label"${count}"") == PMID ]; then
				pmid=$(eval "echo \$var"${count}"")
				export links=""
				for l in ${pmid//;/ }; do
					# If it's a number it is a PMID, if not, go to Google
					case ${l} in
					    ''|*[!0-9]*) 
							if [[ ${l} == "http"* ]]; then
								export links="${links} <a href='${l}' target="_blank">${l/http:\/\/dx.doi.org\//DOI:}</a>"
					    		elif [[ ${l} == "US"* ]]; then
					    			export links="${links} <a href='https://www.lens.org/lens/search?q=${l}' target="_blank">${l}</a>"
					    		elif [[ ${l} == "EP"* ]]; then
								export links="${links} <a href='https://www.lens.org/lens/search?q=${l}' target="_blank">${l}</a>"
					    		else
					    			export links="${links} <a href='https://www.google.com/search?q=${l}' target="_blank">${l}</a>"
					    		fi ;;
					    *) export links="${links} <a href='https://www.ncbi.nlm.nih.gov/pubmed/?term=${l}' target="_blank">${l}</a>" ;;
					esac
				done
				export var${count}="${links}"
			fi
			
			# Remove "_", set first letter to uppercase, remove last ')' and start '(', remove last ';' and append a space after other ';'
			# Also add icons to some fields
			echo "      <td>"
			echo "$(eval "echo \$label"${count}""|sed \
				-e 's/_/ /g' -e 's/\b\(.\)/\u\1/g' \
				-e 's/All Organisms/Targeted Organisms/' \
				-e 's/Antigram Pos/Antigram Positive/' \
				-e 's/Antigram Neg/Antigram Negative/' \
				-e 's/Biofilm/Antibiofilm/' \
				-e 's:Experimental:<i class="fas fa-flask"></i> Experimentally validated:' \
				)"
			echo "</td>"

			if [ $(eval "echo \$label"${count}"") != "biofilm" ]; then
				echo "      <td>$(eval "echo \$var"${count}"" | sed -e 's/;/; /g' -e 's/_/ /g' -e 's/(/ (/g' -e 's/\(.*\);/\1 /')</td>"
			else
				echo "      <td>$(eval "echo \$var"${count}"" | sed -e 's/;/; /g' -e 's/_/ /g' -e 's/(/ (/g' -e 's/\(.*\);/\1 /' -e 's/biofilm;/Yes, against: /')</td>"
			fi
			echo '    </tr>'
		fi
		count=$((count+1))
	done
	echo '  </table>'
	echo '</div>'
#else
#	echo '<div class="container-alert"><div class="alert alert_danger"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
#        echo "<h4>Too many peptides were selected by your search pattern, please specify a more concrete one</h4>"
#        echo '</div></div></div>'
fi
fi

echo '</div>'
echo '</body>'
echo '</html>'

exit 0
