#!/bin/bash

echo "Content-type: text/html"
echo ""

echo '<!DOCTYPE html>'
echo '<html lang="en">'
echo '<head>'
echo '<title>Family analyzer</title>'
echo '<meta name="viewport" content="width=1080">'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<link rel="shortcut icon" href="../favicon.ico" />'
echo '<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">'
echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">'

# Needed for collapsable fieldsets
echo '<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>'

# Using local fonts is faster and more reliable
#echo '<link rel="stylesheet" media="none" href="https://fontlibrary.org/face/dejavu-sans-mono" onload="this.media='all';" type="text/css"/>'

echo '<link href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" rel="stylesheet">'

echo '<link rel="stylesheet" href="../alerts-css-master/assets/css/alerts-css.min.css">'
echo '<script src="../alerts-css-master/assets/js/alerts.min.js"></script>' 

echo '<style>'
echo '.tooltip {'
echo '    position: relative;'
echo '    display: inline-block;'
echo '    border-bottom: 0px dotted black;'
echo '}'

echo '.tooltip .tooltiptext {'
echo '   '
echo '    visibility: hidden;'
echo '    width: 320px;'
echo '    background-color: black;'
echo '    color: #fff;'
echo '    text-align: center;'
echo '    border-radius: 6px;'
echo '    padding: 5px 5px;'
echo '    position: absolute;'
echo '    z-index: 1;'
# This for right tooltips
#echo '    top: -5px;'
#echo '    left: 120%;'
# This for top tooltips
echo '    bottom: 100%;'
echo '    left: 50%; '
echo '    margin-left: -160px; /* Use half of the width (120/2 = 60), to center the tooltip */'
echo '}'

echo '.tooltip .tooltiptext::after {'
echo '    content: "";'
echo '    position: absolute;'
# This for right tooltips
#echo '    top: 50%;'
#echo '    right: 100%; /* To the left of the tooltip */'
#echo '    margin-top: -5px;'
# This for top tooltips
echo '    top: 100%; /* At the bottom of the tooltip */'
echo '    left: 50%;'
echo '    margin-left: -5px;'
echo '    border-width: 5px;'
echo '    border-style: solid;'
#echo '    border-color: transparent black transparent transparent;'
echo '    border-color: black transparent transparent transparent;'
echo '}'
echo '.tooltip:hover .tooltiptext {'
echo '    visibility: visible;'
echo '}'

# Needed to display blocks in big list of extract options properly without cutting in the middle
echo 'label {'
echo '  display: inline-block;'
echo '}'

echo 'input:hover[type="submit"] {background: green;}'
echo 'input:hover[type="reset"] {background: red;}'
echo 'button:hover {background: blue;}'

echo 'input[type="radio"]{'
echo '  -webkit-appearance: none;'
echo '  -moz-appearance: none;'
echo '  appearance: none;'
echo
echo '  border-radius: 50%;'
echo '  width: 16px;'
echo '  height: 16px;'
echo
echo '  border: 2px solid #999;'
echo '  transition: 0.2s all linear;'
echo '  outline: none;'
#echo '  margin-right: 1px;'
echo '  margin-left: 10px;'
echo
echo '  position: relative;'
echo '  top: 4px;'
echo '}'
echo
echo 'input:checked {'
echo '  border: 6px solid grey;'
echo '}'

# For the boxes... as it doesn't work for radio
#echo 'input:hover {'
#echo '  color: red;'
#echo '}'

echo '#banner {'
echo '  padding: 10px;'
echo '  color: rgb(80,80,80);'
echo '  text-align: center;'
echo '  text-decoration: underline;'
#echo '  text-decoration-color: red;'
echo '  letter-spacing: 5px;'
echo '  text-shadow: 1px 1px gray;'
echo '  font-size: 40px;'
echo '}'
echo
echo '#header {'
echo '  background-image: repeating-linear-gradient(-45deg, rgba(255,255,255,0.15), rgba(255,255,255,0.15) 15px, transparent 15px, transparent 25px), linear-gradient(to top ,'
echo '  rgba(230,230,230,0.2), rgba(120,120,120,0.5));'
echo '  height: 80px;'
echo '  border-radius: 10px 10px 10px 10px;'
echo '}'

echo 'a:hover {color: red;}'
echo 'body, h1,h2,h3,h4,h5,h6 {font-family: "Ubuntu", sans-serif}'
echo 'fieldset{border-radius: 10px; border:2px solid #bbb;margin:0 2px;padding:.35em .625em .75em}'
echo 'hr {border:0;border-top:5px solid #eee;margin:20px 0}'
echo '.w3-row-padding img {margin-bottom: 12px}'
echo '/* Set the width of the sidebar to 120px */'
echo '.w3-sidebar {width: 120px;background: #bbb;}'
echo '/* Add a left margin to the "page content" that matches the width of the sidebar (120px) */'
echo '#main {margin-left: 120px}'
echo '/* Remove margins from "page content" on small screens */'
echo '@media only screen and (max-width: 600px) {#main {margin-left: 0}}'

echo "select {"
#echo "    width: 10%;"
#echo "    padding: 10px 10px;"
echo "    border: none;"
echo "    border-radius: 4px;"
echo "    background-color: #ffffff;"
echo "}"
echo "button, input[type=button], input[type=reset] {"
echo "    background-color: #616161;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=submit] {"
echo "    background-color: DarkGreen;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=text], select {"
#echo "    width: 100%;"
echo "    padding: 5px 10px;"
#echo "    margin: 8px 0;"
echo "    display: inline-block;"
echo "    border: 1px solid #ccc;"
echo "    border-radius: 4px;"
echo "    box-sizing: border-box;"
echo "}"

# Using local fonts is faster and more reliable
#echo 'pre {font-family: 'DejaVu Sans Mono'}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Regular.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo 'pre {font-family: "Local Dejavu", "Local Liberation", monospace; font-size: 14px}'

echo '* {box-sizing: border-box;}'

echo '/* Button used to open the chat form - fixed at the bottom of the page */'
echo '.open-button {'
echo '  background-color: #555;'
echo '  color: white;'
echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  opacity: 0.8;'
echo '  position: fixed;'
echo '  bottom: 23px;'
echo '  right: 28px;'
echo '  width: 280px;'
echo '}'

echo '/* The popup sequences window - hidden by default */'
echo '.sequences-popup {'
echo '  display: none;'
echo '  position: fixed;'
echo '  bottom: 23px;'
echo '  right: 28px;'
echo '  border: 3px solid #f1f1f1;'
echo '  z-index: 9;'
echo '}'

echo '/* Add styles to the form container */'
echo '.form-container {'
#echo '  max-width: 550px;'
#echo '  padding: 10px;'
echo '  padding: 16px 20px;'
echo '  background-color: white;'
echo '}'

echo '/* Full-width textarea */'
echo '.form-container textarea {'
echo '  width: 100%;'
echo '  padding: 15px;'
echo '  margin: 5px 0 22px 0;'
echo '  border: none;'
echo '  background: #f1f1f1;'
echo '  resize: none;'
echo '  min-height: 200px;'
echo '}'

echo '/* When the textarea gets focus, do something */'
echo '.form-container textarea:focus {'
echo '  background-color: #ddd;'
echo '  outline: none;'
echo '}'

echo '/* Set a style for the submit/send button */'
echo '.form-container .btn {'
echo '  background-color: #4CAF50;'
echo '  color: white;'
echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  width: 100%;'
echo '  margin-bottom:10px;'
echo '  opacity: 0.8;'
echo '}'

echo '/* Add a red background color to the cancel button */'
echo '.form-container .cancel {'
echo '  background-color: red;'
echo '}'

echo '/* Add some hover effects to buttons */'
echo '.form-container .btn:hover, .open-button:hover {'
echo '  opacity: 1;'
echo '}'

echo '/* Button used to open the characters picker - fixed at the bottom of the page */'
echo '.open-button-chars {'
echo '  background-color: #555;'
echo '  color: white;'
#echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  opacity: 0.8;'
#echo '  position: fixed;'
#echo '  bottom: 23px;'
#echo '  right: 28px;'
#echo '  width: 280px;'
echo '}'

echo '/* The characters picker window - hidden by default */'
echo '.characters-popup {'
echo '  display: none;'
echo '  position: fixed;'
echo '  bottom: 23px;'
echo '  right: 28px;'
echo '  border: 3px solid #f1f1f1;'
echo '  z-index: 9;'
echo '}'

# Break names in the table, as they can be really long
echo '.characters-popup td {'
echo '  word-break: break-all;'
echo '}'

echo '/* Add styles to the form container */'
echo '.form-container-chars {'
echo '  max-width: 550px;'
echo '  padding: 10px;'
echo '  padding: 16px 20px;'
echo '  background-color: white;'
echo '}'

echo '/* Full-width textarea */'
echo '.form-container-chars textarea {'
echo '  width: 100%;'
echo '  padding: 15px;'
echo '  margin: 5px 0 22px 0;'
echo '  border: none;'
echo '  background: #f1f1f1;'
echo '  resize: none;'
echo '  min-height: 200px;'
echo '}'

echo '/* When the textarea gets focus, do something */'
echo '.form-container-chars textarea:focus {'
echo '  background-color: #ddd;'
echo '  outline: none;'
echo '}'

echo '/* Set a style for the submit/send button */'
echo '.form-container-chars .btn {'
echo '  background-color: #4CAF50;'
echo '  color: white;'
echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  width: 100%;'
echo '  margin-bottom:10px;'
echo '  opacity: 0.8;'
echo '}'

echo '/* Add a red background color to the cancel button */'
echo '.form-container-chars .cancel {'
echo '  background-color: red;'
echo '}'

echo '/* Add some hover effects to buttons */'
echo '.form-container-chars .btn:hover, .open-button-chars:hover {'
echo '  opacity: 1;'
echo '}'

echo '</style>'
echo '</head>'
echo '<body class="w3-light-grey">'

echo '<!-- Icon Bar (Sidebar) -->'
echo '<nav class="w3-sidebar w3-bar-block w3-small w3-center">'
echo '  <!-- Avatar image in top left corner -->'
echo '  <img src="../webicons/icon.svg" alt="ADAPTABLE logo" style="width:100%">'
echo '  <a href="../index.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-home w3-xxlarge"></i>'
echo '    <p>HOME</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./adaptable-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-cogs w3-xxlarge"></i>'
echo '    <p>FAMILY GENERATOR</p>'
echo '  </a>'
echo '  <a href="./extract-form.sh" class="w3-bar-item w3-button w3-padding-large w3-light-grey">'
echo '    <i class="fas fa-search-plus w3-xxlarge"></i>'
echo '    <p>FAMILY ANALYZER</p>'
echo '  </a>'
echo '  <a href="./index-results.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-download w3-xxlarge"></i>'
echo '    <p>DOWNLOAD RESULTS</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./index-browse.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-list-ul w3-xxlarge"></i>'
echo '    <p>BROWSE AMPs & FAMILIES</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="../faq.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-question-circle w3-xxlarge"></i>'
echo '    <p>FAQ & TUTORIAL</p>'
echo '  </a>'
echo '  <a href="../about.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-users w3-xxlarge"></i>'
echo '    <p>ABOUT</p>'
echo '  </a>'
echo '</nav>'

echo '<div class="w3-padding-large" id="main">'

echo '<div id="header"> '
echo '        <div id="banner">'
echo '        ADAPTABLE'
echo '        </div>'
echo '</div>'

echo \
	"<article>"\
	"<header>"\
	"<h1>Family analyzer</h1>"\
	"</header>"\
	"</article>"

source ./tooltips.sh

# Here is the black magic for collapsing fields
echo '<script>'
echo '        $(function(){'
echo '               $("legend.collapsibleLegend").click(function(){'
echo '                        $(this).parent().find(".collapsibleFieldset").slideToggle("slow");'
echo '                });'
echo '        });'
echo '</script>'
echo '<script>'
echo '  $(document).ready(function() {'
# Only get the first part of the lagend and change the rest
echo '  var prev_all = $("legend.collapsibleLegend").text();'
echo '  var ret = prev_all.split(" ");'
echo '  var prev = ret[0];'
echo '    $("legend.collapsibleLegend").click(function() {'
echo '    $("#collapsibleDiv").toggleClass("active");'
echo '    if ($(".collapsibleFieldset").hasClass("active")) {'
echo '      $("legend.collapsibleLegend").html("Please visit the following links for more information on each family:<sup> (+) Expand Me to show families with 1 member</sup>");' # prev + "<sub..." Dropped to not mess with phrases with multiple spaces
echo '    } else {'
echo '      $("legend.collapsibleLegend").html("Please visit the following links for more information on each family:<sup> (-) Collapse Me to hide families with 1 member</sup>");' # prev + "<sub..." Dropped to not mess with phrases with multiple spaces
echo '    }'
echo '  });'
echo '});'
echo '</script>'
#

if [ -z "$QUERY_STRING" ]; then
	echo "<form method=GET>"
	echo "<div class="tooltip"><i class='fas fa-user'></i>&nbsp;&nbsp;Username&nbsp;<span class="tooltiptext">"${tooltiptext_user_key}"</span></div><input type="text" name="_user_key" size=20 placeholder='Name of your run experiment...' pattern='[A-Za-z0-9-_.]+' required>"
	echo "<input type="submit" value='Submit'>"
	echo "<input type="reset" value="Reset">"
	echo "</form>"
	echo "<p>You can also review the built-in database <a href='./extract-form.sh?_calculation_label=all_families&_user_key=all_families'>all_families</a>, where all peptides of the database are used as reference <i>(father)</i> to form its own sequence-related family. "\
	"</p>"
	echo '</div>'
	exit 0
else

# Shorten addressbar
echo "<script>"
echo 'function shorten_addressbar_url() {'
echo 'var url = window.location.href;'
echo "var new_url = url.split('?')[0];"
echo "history.replaceState('1', '', new_url);"
echo '}'

echo 'shorten_addressbar_url();'
echo "</script>"

# Character selector
echo '<script>'
echo 'function openForm2() {'
echo '    document.getElementById("myForm2").style.display = "inline-block";'
echo '}'
echo 'function closeForm2() {'
echo '    document.getElementById("myForm2").style.display = "none";'
echo '}'
echo '</script>'

echo '<div class="characters-popup" id="myForm2">'
echo '  <form class="form-container-chars">'
echo '    <h3>Special characters for modified aminoacids</h3>'
echo "<i>You can click on the symbol to select it and use Ctrl+F to search your desired aminoacid</i>"
echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
cat ../character-selector | sed -e 's/input-text/nothing/g'
echo '</div>'
echo '    <button type="button" class="btn cancel" onclick="closeForm2()"><i class="fas fa-signature"></i></button>'
echo '  </form>'
echo '</div>'
##

	export __calculation_label=$(echo "$QUERY_STRING" | sed -n "s/^.*calculation_label=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
	if [ -z "${__user_key}" ]; then
		export __user_key=$(echo "$QUERY_STRING" | sed -n "s/^.*user_key=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
	fi
	export __first_fam=$(echo "$QUERY_STRING" | sed -n "s/^.*first_fam=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
	export __end_fam=$(echo "$QUERY_STRING" | sed -n "s/^.*end_fam=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
	export __code=$(echo "$QUERY_STRING" | tr '&' '\n'|grep -E '_code' | sed "s/%20/ /g" | cut -d '=' -f2)
	export __list_fam=$(echo "$QUERY_STRING" | tr '&' '\n'|grep -E '_list_fam' | sed "s/%20/ /g" | cut -d '=' -f2 | sed -e 's/%2C/ /g')

	if [ "${__user_key}" ]; then
		if ! [ -e "ADAPTABLE/Results/${__user_key}/" ]; then
#                        echo '<h2>Wrong "Username" set... please <a style="color:red;" href="./extract-form.sh">RELOAD this page</a> and chose a proper value</h2>'
        echo '<div class="container-alert"><div class="alert alert_danger"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
        echo '<h2>Wrong "Username" set... please <a style="color:red;" href="./extract-form.sh">RELOAD this page</a> and chose a proper value</h2>'
        echo '</div></div></div>'
#                        echo '<meta http-equiv="refresh" content="2;url=./extract-form.sh" />'
			echo "<script>window.setTimeout(function(){window.location.replace('./extract-form.sh')},2000);</script>"
                        exit 0
		fi
	fi
	
	if [ -z "${__calculation_label}" ]; then
		# It is not possible to have people using user_key like .databases because forms will rename them as they will exist in Results dir
                if grep -Fxq "${__user_key}" ADAPTABLE/Results/.databases; then
			export __calculation_label="${__user_key}"
#			echo "<meta http-equiv='Refresh' content='0;url=./extract-form.sh?_calculation_label=${__calculation_label}&_user_key=${__user_key}'>"
			echo "<script>window.setTimeout(function(){window.location.replace('./extract-form.sh?_calculation_label=${__calculation_label}&_user_key=${__user_key}')},0);</script>"
		else
	        echo \
#	                "<form method=GET>"
	        echo        "<table>"
	        echo '<tr><td>'
	        echo '<div style="margin-left: 30px">'
	        echo "<h4>Available projects (named by 'Calculation label'):</h4>"
	        echo '</div>'
#	        echo '<div style="text-align:left;border-radius: 5px;border: 2px solid Darkgrey;margin-left: 50px;margin-bottom: 20px;height:350px;width: 800px;padding:10px;overflow:auto;resize: vertical">'
		echo '<div style="text-align:left;border-left: 5px solid Darkgrey;margin-left:40px;padding:10px;overflow:auto;max-height:70vh">'
	        echo '<h4><u>Built-in databases:</u></h4>'
	        echo "<code style='white-space: pre;'>$(for j in $(cat ADAPTABLE/Results/.databases | tr '\r\n' ' '); do echo "<a href='./extract-form.sh?_calculation_label=${j}&_user_key=.'>${j}</a>"; done)</code>"
	        echo '<h4><u>User experiments:</u></h4>'
	        echo "<code style='white-space: pre;'>$(for x in $(ls -t --color=never -C ADAPTABLE/Results/${__user_key} $(for i in $(cat ADAPTABLE/Results/.databases); do echo "-I $i";done)); do echo "<a href='./extract-form.sh?_calculation_label=${x}&_user_key=${__user_key}'>${x}</a>"; done)</code>"
	        echo "</div>"
#	        echo "Calculation label&nbsp;<input type="text" name="_calculation_label" size=20 placeholder='Name of your run experiment...' pattern='[A-Za-z0-9-_.]+' required>"
#		echo "Username&nbsp;<input type="text" name="_user_key" value="${__user_key}" size=10 placeholder='Name of your run experiment...' pattern='[A-Za-z0-9-_.]+' readonly>"
	        echo "</table>"
#	        echo "<input type="submit" value='Submit'>"
#	        echo "<input type="reset" value="Reset">"
#	        echo "</form>"
	        fi
	else
                if grep -Fxq "${__calculation_label}" ADAPTABLE/Results/.databases; then
                        export __user_key="."
                fi

		if ! [ -e "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.fam_num" ]; then
		        echo '<div class="container-alert"><div class="alert alert_danger"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
		        echo '<h2>Wrong calculation_label set, no project run has its name... please <a style="color:red;" href="./extract-form.sh">RELOAD this page</a> and chose a proper calculation_name</h2>'
		        echo '</div></div></div>'
#			echo '<meta http-equiv="refresh" content="2;url=./extract-form.sh" />'
			echo "<script>window.setTimeout(function(){window.location.replace('./extract-form.sh')},2000);</script>"
			exit 0
		else
		
		fam_num=$(tail -n1 "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.fam_num")

		if [ -e "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.prediction-prefs" ] ; then
			echo "<h5>Your run with calculation label <i><b style='user-select: all;'>"${__calculation_label}"</b></i> generated a family similar to the peptide <b>$(tail -n1 ADAPTABLE/Results/${__user_key}/${__calculation_label}/.prediction-prefs)</b>:</h5>"
		else
			if [ ${fam_num} == "1" ] && ! [ -e "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.prediction-prefs" ] ; then
				echo "<h5>Your run with calculation label <i><b style='user-select: all;'>"${__calculation_label}"</b></i> generated <b>${fam_num}</b> family <sub>(i.e. f1)</sub></h5>"
			else
				echo "<h5>Your run with calculation label <i><b style='user-select: all;'>"${__calculation_label}"</b></i> generated <b>${fam_num}</b> families <sub>(i.e f1, f2, ..., fn)</sub> from $(tail -n1 "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.selection"|cut -d " " -f1) peptides</h5>"
			fi
		fi
		[[ ${__calculation_label} != all_families ]] && echo "<p><i>Note: You can download <a href="../Results/${__user_key}/${__calculation_label}/${__calculation_label}-full.tar.xz" target="_blank">this compressed package <i class='far fa-file-archive fa-lg'></i></a> with all the results.</i></p>"
		echo "<h4><b><u>Global properties:</u></b></h4>"
		if ! [ -e "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.prediction-prefs" ]; then
			if [[ ${__calculation_label} != all_families ]]; then
				fam_multi="$(grep elements "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.family_elements" |awk '{print $4}')"
				fam_uni="$(grep "1 element" "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.family_elements" |awk '{print $4}')"

				if [ "$fam_multi" ]; then
		                        [ -z "$fam_uni" ] ||  echo '<fieldset style="border:0px;padding-left:0em;"><legend style="display:inline;" class="collapsibleLegend">Please visit the following links for more information on each family:<sup>(+) Expand Me to show families with 1 member</sup></legend>'
	                        fi
				echo '<div style="text-align:left;border-left: 2px solid Darkgrey;margin-left:30px;padding:5px;overflow:auto">'

				echo "<i class='fas fa-list-ul fa-lg'></i>&nbsp;&nbsp;Summaries:<br>"
				echo '<div style="max-height:100px;overflow:auto">'
				for z in $fam_multi; do
					echo "<a href='./all_familiesDB.sh?_fnum=${z}&_user_key=${__user_key}&_calculation_label=${__calculation_label}' target="_blank">f${z}</a>"
				done
				echo "<br>"
	
				if [ "$fam_multi" ]; then
					[ -z "$fam_uni" ] || echo '<div id="collapsibleDiv" class="collapsibleFieldset active" style="display: none;">'
				fi
       					for zz in $fam_uni; do
		                                echo "<a href='./all_familiesDB.sh?_fnum=${zz}&_user_key=${__user_key}&_calculation_label=${__calculation_label}' target="_blank">f${zz}</a>"
		                        done
		                if [ "$fam_multi" ]; then
					[ -z "$fam_uni" ] || echo '</div>'
				fi
				echo '</div>'
				echo "<hr>"				
				echo "<i class='fas fa-chart-pie fa-lg'></i>&nbsp;&nbsp;Full output:<br>"
				echo '<div style="max-height:100px;padding:5px;overflow:auto">'
				for zzz in $fam_multi; do
        	                        echo "<a href='../Results/${__user_key}/${__calculation_label}/Families/f${zzz}.html' target="_blank">f${zzz}</a>"
	                        done
	                        echo "<br>"
				# This div sets class to collapsibleFieldset but not to active or legend switch won't work
				if [ "$fam_multi" ]; then
					[ -z "$fam_uni" ] || echo '<div class="collapsibleFieldset" style="display: none;">'
				fi
	                        for zzzz in $fam_uni; do
	                                echo "<a href='../Results/${__user_key}/${__calculation_label}/Families/f${zzzz}.html' target="_blank">f${zzzz}</a>"
	                        done
	                        if [ "$fam_multi" ]; then
		                        [ -z "$fam_uni" ] || echo '</div>'
	                        fi
				echo '</div>'
				
	                        echo "</div>"

				if [ "$fam_multi" ]; then			
					[ -z "$fam_uni" ] || echo "</fieldset>"
				fi
			else
				echo '<div style="text-align:left;margin-left:30px;width: 1000px;padding:5px;overflow:auto">'
				echo "<h6>Please visit <a href='./all_familiesDB.sh' target="_blank">BROWSE FAMILIES DATABASE</a> section to see an overview of each family</h6>"
				echo "</div>"
			fi
		else
                        echo "<h6>You can review:</h6>"
                        echo "<ul class='fa-ul'><li><span class='fa-li' ><a href='./all_familiesDB.sh?_fnum=1&_user_key=${__user_key}&_calculation_label=${__calculation_label}' target="_blank"><i class='fas fa-list-ul fa-lg'></i></span>The summary</a> for the generated family of peptides similar to the one provided by you</li></ul>"
                        echo "<ul class='fa-ul'><li><span class='fa-li' ><a href='../Results/${__user_key}/${__calculation_label}/${__calculation_label}-output.html' target="_blank"><i class='fas fa-chart-pie fa-lg'></i></span>The full output</a> for the experiment.</li></ul>"
		fi
		[ -d "../Results/${__user_key}/${__calculation_label}/Logos" ] && echo "<ul class='fa-ul'><li><h6><span class='fa-li' ><a href="../Results/${__user_key}/${__calculation_label}/Logos" target="_blank"><i class='fas fa-draw-polygon fa-lg'></i></a></span>Logos were generated for this experiment, click <a href="../Results/${__user_key}/${__calculation_label}/Logos" target="_blank">here</a> to review them.</h6></li></ul>"
 		[ -d "../Results/${__user_key}/${__calculation_label}/Graphics" ] && echo "<ul class='fa-ul'><li><h6><span class='fa-li' ><a href="../Results/${__user_key}/${__calculation_label}/Graphics" target="_blank"><i class='fas fa-chart-area fa-lg'></i></a></span>Graphical analysis was generated for this experiment, click <a href="../Results/${__user_key}/${__calculation_label}/Graphics" target="_blank">here</a> to review.</h6></li></ul>"
		
		if [ -e "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.prediction-prefs" ]; then
			export prediction_type="$(head -n1 "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.prediction-prefs")"
			if [ "${prediction_type}" = "fathers" ]; then
				echo "Your peptide <b>$(tail -n1 "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.prediction-prefs")</b> was included in the following <b>${__calculation_label}</b> families:"
				echo "<b>$(cat ADAPTABLE/Results/${__user_key}/${__calculation_label}/.my_peptide-families)</b>"
				echo "<br>"
				echo "And it is contained in the following <b>all_families</b> experiment families:"
				echo "<br>"
				echo "<br>"
				echo "<code style='white-space: pre-wrap;'>$(cat ADAPTABLE/Results/${__user_key}/${__calculation_label}/.all_families-fam-fathers)</code>"
				echo "Feel free to use this extract tool with <i>all_families</i> and relevant families to know more about the features of peptides included in them"
				echo "<br>"
			else
				# If file has some content, otherwise no peptide is a father of all_families
				if [ -s "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.fathers_in_all_families" ]; then
					echo "<div class="w3-container">"
					echo "<p>"
					echo "Your peptide is similar to some fathers of the ADAPTABLE built-in database <i>all_families</i>."
					echo "Therefore its properties and activities might be similar. You can use this tool to extract relevant information on the similar families."
					echo "Simply set the calculation label field to <i>all_families</i> and choose the family number"
					echo "(e.g. if your peptide is similar to f14 of <i>all_families</i>, choose <i>14</i> to display the properties of family 14).</p>"
					echo "Families of <i>all_families</i> similar to your peptide and corresponding father (in order of similarity)"
					echo '<div style="text-align:left;border-left: 2px solid Darkgrey;margin-left:30px;max-height:250px;width: 99%;padding:5px;overflow:auto">'
					echo "<code style='white-space: pre-wrap;'>$(cat ADAPTABLE/Results/${__user_key}/${__calculation_label}/.fathers_in_all_families|uniq)</code>"
					echo '</div>'
					echo "</div>"
					echo "<br>"
				fi
			fi
		else
			group_fam_num=$(tail -n1 "ADAPTABLE/Results/${__user_key}/${__calculation_label}/.group_fam_num")
			if [ "${group_fam_num}" != "0" ] && [ "${group_fam_num}" != "${fam_num}" ]; then
                	echo "Some families are similar among them:"
                	echo '<div style="text-align:left;border-left: 2px solid Darkgrey;margin-left:30px;max-height:205px;padding:5px;overflow:auto">'
                	echo "<code style='white-space: pre;'>$(cat ADAPTABLE/Results/${__user_key}/${__calculation_label}/.different-fathers)</code>"
                	echo '</div>'
			fi
		fi
		echo "<hr>"
		echo "<h4 style='display:inline-block;'><b><u>Detailed analysis of family members:</u></b></h4>"
		echo "<p style='display:inline-block;'>Select the families to analyze (a <b>range</b> is noted with numbers separated by hyphens and a list is specified by separated by commas):</p>"

		echo "<form method=GET>"

		if [ -n "${prediction_type}" ]; then
			echo "Calculation label: <input type="text" name="_calculation_label" value="${__calculation_label}" size="10">"
			echo "<div style='display:none;'>"
				echo "Username&nbsp;<input type="text" name="_user_key" value="${__user_key}" size=5 placeholder='Name of your run experiment...' pattern='[A-Za-z0-9-_.]+' readonly>"
			echo "</div>"
		else
			echo "<div style='display:none;'>"
			if [[ ${__calculation_label} != all_families ]]; then
				echo "Calculation label: <input type="text" name="_calculation_label" value="${__calculation_label}" size="5" readonly>"
				echo "Username&nbsp;<input type="text" name="_user_key" value="${__user_key}" size=5 placeholder='Name of your run experiment...' pattern='[A-Za-z0-9-_.]+' readonly>"
			else
				echo "Calculation label: <input type="text" name="_calculation_label" value="${__calculation_label}" size="5" readonly>"
			fi
			echo "</div>"
		fi

                # type number does not follow size parameter, but we can workaround it with something like
                # <input name=quantity type=text pattern='[0-9]{1,3}'>  The first to allow only numbers, the second for the case we want to impose limits
                #"<tr><td>First family to analyze: </td><td><input type="number" min="1" name="_first_fam" size="5"></td></tr>" \
                #"<tr><td> Last family to analyze: </td><td><input type="number" min="1" name="_end_fam" size="5"></td></tr>" \
#		echo "First family to analyze: <input type="text" name="_first_fam" pattern='[0-9]{1,}' title='Must contain a single number' size="1" value="1" required>"
#		echo "Last family to analyze: <input type="text" name="_end_fam" pattern='[0-9]{1,}' title='Must contain a single number' size="1" value="1" required>"
#		echo "Concrete list of families to analyze: <input type="text" name="_list_fam" pattern='[0-9,]{1,}' title='Must contain a list of numbers separated by commas and without spaces between' size="5">"
		if [ -n "${prediction_type}" ]; then
			echo "List of families to analyze: <input type="text" name="_list_fam" pattern='[0-9,]{1,-}' title='Must contain a list of numbers separated by commas and without spaces between' size="5" placeholder='e.g. 1,2,7-9' value=1 required>"
		else
			echo "List of families to analyze (up to ${fam_num}): <input type="text" name="_list_fam" pattern='[0-9,]{1,-}' title='Must contain a list of numbers separated by commas and without spaces between' size="5" placeholder='e.g. 1,2,7-9' required>"
		fi
		echo "<br><br>"
# Needs jquery
echo "<script>"
echo '$(function(){'
echo '    var requiredCheckboxes = $(".options :checkbox[required]");'
echo '    requiredCheckboxes.change(function(){'
echo '        if(requiredCheckboxes.is(":checked")) {'
echo '            requiredCheckboxes.removeAttr("required");'
echo '        } else {'
echo '            requiredCheckboxes.attr("required", "required");'
echo '        }'
echo '    });'
echo '})'
echo "</script>"

		echo "<div class="options" style='margin-left: 10px;margin-right: 10px;'>"
		echo	'<input type="checkbox" name="_code" value="f" oninvalid="this.setCustomValidity('"'"'Check at least one option'"'"')" onchange="this.setCustomValidity('"'"''"'"')" required> Aminoacid frequencies<br>' \
			"<input type="checkbox" name="_code" value="p" required> Polarity<br>" \
			"<input type="checkbox" name="_code" value="s" required> Structure<br>" \
			"<input type="checkbox" name="_code" value="r" required> Residue type<br>" \
			"<input type="checkbox" name="_code" value="u" required> Best representative peptide<br>" \
			"<input type="checkbox" name="_code" value="d" required> Available data per sequence<br>" \
			"<input type="checkbox" name="_code" value="t" required> Available data per sequence (as text)<br>"
# For all data is better point people to "Raw data" links, they are better from a performance point of view
#			"<input type="checkbox" name="_code" value="a" required> All data<br>"

		echo "<br>"

# Here is the black magic for collapsing fields
echo '<script>'
echo '        $(function(){'
echo '               $("legend.collapsibleLegend2").click(function(){'
echo '                        $(this).parent().find(".collapsibleFieldset2").slideToggle("slow");'
echo '                });'
echo '        });'
echo '</script>'
echo '<script>'
echo '  $(document).ready(function() {'
# Only get the first part of the lagend and change the rest
echo '  var prev_all = $("legend.collapsibleLegend2").text();'
echo '  var ret = prev_all.split(" ");'
echo '  var prev = ret[0];'
echo '    $("legend.collapsibleLegend2").click(function() {'
echo '    $("#collapsibleDiv2").toggleClass("active");'
echo '    if ($(".collapsibleFieldset2").hasClass("active")) {'
echo '      $("legend.collapsibleLegend2").html("Specific parameters:<sup> (+) Expand Me</sup>");' # prev + "<sub..." Dropped to not mess with phrases with multiple spaces
echo '    } else {'
echo '      $("legend.collapsibleLegend2").html("Specific parameters:<sup> (-) Collapse Me</sup>");' # prev + "<sub..." Dropped to not mess with phrases with multiple spaces
echo '    }'
echo '  });'
echo '});'
echo '</script>'
#

		echo "<fieldset>"
		echo "<legend class='collapsibleLegend2'>Specific parameters:<sup> (+) Expand Me</sup></legend>"
		echo '<div id="collapsibleDiv2" class="collapsibleFieldset2 active" style="display: none;">'
		for x in {ID,sequence,name,source,taxonomy,Family,gene,stereo,N_terminus,C_terminus,PTM,cyclic,ribosomal,target,all_organisms,activity,activity_test,\
experimental,antimicrobial,antiprotozoal,insecticidal,anticancer,toxic,cell_cell,drug_delivery,antioxidant,antiproliferative,\
antibacterial,antigram_pos,antigram_neg,antifungal,antiyeast,antiviral,virus_name,activity_viral,activity_viral_test,\
antiparasitic,antiplasmodial,antitrypanosomic,antileishmania,\
antitumor,cell_line,tissue,cancer_type,anticancer_activity,anticancer_activity_test,antiangiogenic,\
cytotoxic,hemolytic,hemolytic_activity,hemolytic_activity_test,RBC_source,\
hormone,quorum_sensing,immunomodulant,antihypertensive,\
cell_penetrating,tumor_homing,blood_brain,\
DSSP,pdb,experim_structure,PMID,synthetic,antibiofilm,solubility};do
			echo "<label for="${x/antibiofilm/biofilm}"><input type="checkbox" name="_code" value="x_${x/antibiofilm/biofilm}" id="${x}" required>&nbsp;${x}&nbsp;&nbsp;</label>"
		done
		echo '</div>'
		echo "</fieldset>"
		echo "</div>"

		echo "<br>"
		echo "<input type="submit" value='Submit'>" \
			"<input type="reset" value="Reset">"

		echo "</form>"

	fi

	fi
	 
	if [ -n "${__code}" ]; then

# Check if at least one of the family numbers is valid and continue only in that case
function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}

A=($(seq 1 ${fam_num}))
#if [ $(contains "${A[@]}" "3") == "y" ]; then
#    echo "contained in array"
#else
#    echo "not contained in array"
#fi

# Split ranges (e.g 1-2) for this check
for x in ${__list_fam/-/ }; do
	if [ $(contains "${A[@]}" "${x}") == "y" ]; then
		export fam_in_array="y"
	fi
done

if [ "${fam_in_array}" ]; then
echo '<div class="sequences-popup" id="myForm">'
echo '  <form class="form-container">'
echo '    <h1>Sequences</h1>'

echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
                        #if [ "${__list_fam}" != "" ]; then
				# Show only first 150 sequences or generated html is too big and web will be too heavy to load
                                for x in ${__list_fam}; do
                                	if [[ $x == *-* ]]; then
						# We cannot rely on 'ranges' internal handling as cutting later the output will lead to only the first family output being shown, set the limit a bit lower for ranges
	                                        #./ADAPTABLE/extract_family_splitted.sh "${x}"-f "ADAPTABLE/Results/${__user_key}/${__calculation_label}/" | head -n 157 | cl-ansi2html -w -n | sed -e 's/white-space:pre-wrap/white-space:pre/'
	                                        range="$(seq ${x/-/ })"
	                                        for xx in ${range}; do
							./ADAPTABLE/extract_family_splitted.sh "${xx}"-"${xx}"-f "ADAPTABLE/Results/${__user_key}/${__calculation_label}/" | head -n 107 | cl-ansi2html -w -n | sed -e 's/white-space:pre-wrap/white-space:pre/'
	                                        done
					else
						./ADAPTABLE/extract_family_splitted.sh "${x}"-"${x}"-f "ADAPTABLE/Results/${__user_key}/${__calculation_label}/" | head -n 107 | cl-ansi2html -w -n | sed -e 's/white-space:pre-wrap/white-space:pre/'
					fi
                                done
                        #else
                        #        ./ADAPTABLE/extract_family_splitted.sh "${__first_fam}"-"${__end_fam}"-f "ADAPTABLE/Results/${__user_key}/${__calculation_label}/" | cl-ansi2html -w -n | sed -e 's/white-space:pre-wrap/white-space:pre/'
                        #fi
echo '</div>'

echo '    <button type="button" class="btn cancel" onclick="closeForm()">Close</button>'
echo '  </form>'
echo '</div>'
echo '<button class="open-button" onclick="openForm()">Sequences</button>'

echo '<script>'
echo 'function openForm() {'
echo '    document.getElementById("myForm").style.display = "block";'
echo '}'

echo 'function closeForm() {'
echo '    document.getElementById("myForm").style.display = "none";'
echo '}'
echo '</script>'

		echo '<script>'
		echo '// When the user clicks on the button, scroll to the top of the document'
		echo 'function topFunction() {'
		echo '    document.body.scrollTop = 0; // For Safari'
		echo '    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera'
		echo '}'
		echo '</script>'
		echo '<button onclick="topFunction()" id="topbtn" title="Go to top"><i class="fas fa-arrow-up"></i></button>'
		echo '<button style="float:right;" class="open-button-chars" onclick="openForm2()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'
		echo '<div style="border-radius: 5px;border: 2px solid Darkgrey;padding:10px;overflow:auto;max-height: 95vh">'
		for i in ${__code};do
			#if [ "${__list_fam}" != "" ]; then
				for x in ${__list_fam}; do
					if [[ $x == *-* ]]; then
						#./ADAPTABLE/extract_family_splitted.sh "${x}"-${i/_/ } "ADAPTABLE/Results/${__user_key}/${__calculation_label}/" | cl-ansi2html -w -n | sed -e 's/white-space:pre-wrap/white-space:pre/'
						range="$(seq ${x/-/ })"
						for xx in ${range}; do
							./ADAPTABLE/extract_family_splitted.sh "${xx}"-"${xx}"-${i/_/ } "ADAPTABLE/Results/${__user_key}/${__calculation_label}/" | head -n 157 | cl-ansi2html -w -n | sed -e 's/white-space:pre-wrap/white-space:pre/'
						done
					else
						./ADAPTABLE/extract_family_splitted.sh "${x}"-"${x}"-${i/_/ } "ADAPTABLE/Results/${__user_key}/${__calculation_label}/" | head -n 157 | cl-ansi2html -w -n | sed -e 's/white-space:pre-wrap/white-space:pre/'
					fi
				done
			#else
			#	./ADAPTABLE/extract_family_splitted.sh "${__first_fam}"-"${__end_fam}"-${i/_/ } "ADAPTABLE/Results/${__user_key}/${__calculation_label}/" | cl-ansi2html -w -n | sed -e 's/white-space:pre-wrap/white-space:pre/'
			#fi
		done
		echo "</div>"

                # Scroll down
                echo '<script>window.scrollTo(0, document.body.scrollHeight);</script>'

		if [[ ${__calculation_label} == all_families ]]; then
	                # Generate full output for all_families as we need to crop part of the extract output
	                echo '<p><i>Note: Too big output needs to be cropped due to performance reasons. To access full output for each family, click the links below:</i><p>'

			echo '<div style="text-align:left;border-left: 2px solid Darkgrey;margin-left:30px;max-height:205px;padding:5px;overflow:auto">'
			rm -rf ADAPTABLE/tmp_files3/*
			mkdir -p ADAPTABLE/tmp_files3/.fonts
			cp ../dejavu/ttf/*.ttf "ADAPTABLE/tmp_files3/.fonts/."
			cp ../liberation/*.ttf "ADAPTABLE/tmp_files3/.fonts/."
			for x in ${__list_fam}; do
				if [[ $x == *-* ]]; then
					range="$(seq ${x/-/ })"
					for xx in ${range}; do
						cat ADAPTABLE/fix-monospace-font.html > ADAPTABLE/tmp_files3/f${xx}.html && cl-ansi2html -w -n < "ADAPTABLE/Results/all_families/.outputs/f${xx}" >> ADAPTABLE/tmp_files3/f${xx}.html | sed -e 's/white-space:pre-wrap/white-space:pre/'
						echo "<a href='../tmp/f${xx}.html' target='_blank'>f${xx}</a>"
					done
				else
					cat ADAPTABLE/fix-monospace-font.html > ADAPTABLE/tmp_files3/f${x}.html && cl-ansi2html -w -n < "ADAPTABLE/Results/all_families/.outputs/f${x}" >> ADAPTABLE/tmp_files3/f${x}.html | sed -e 's/white-space:pre-wrap/white-space:pre/'
					echo "<a href='../tmp/f${x}.html' target='_blank'>f${x}</a>"
				fi
			done
		fi
		echo '</div>'
else
	echo '<div class="container-alert"><div class="alert alert_danger"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
	echo '<h4>List of families to analyze does not correspond with the amount of created families for this experiment</h4>'
	echo '</div></div></div>'
	# Scroll down
	echo '<script>window.scrollTo(0, document.body.scrollHeight);</script>'
fi
fi
fi

echo '</div>'
echo '</body>'
echo '</html>'

exit 0
