#!/bin/bash

echo "Content-type: text/html"
echo ""

echo '<!DOCTYPE html>'
echo '<html lang="en">'
echo '<head>'
echo '<title>Family Generator</title>'
echo '<meta name="viewport" content="width=1440">'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<link rel="shortcut icon" href="../favicon.ico" />'
echo '<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">'
echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">'

# Needed for collapsable fieldsets and hide divs
echo '<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>'

# Using local fonts is faster and more reliable
#echo '<link rel="stylesheet" media="none" href="https://fontlibrary.org/face/dejavu-sans-mono" onload="this.media='all';" type="text/css"/>'

echo '<link href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" rel="stylesheet">'

echo '<link rel="stylesheet" href="../alerts-css-master/assets/css/alerts-css.min.css">'
echo '<script src="../alerts-css-master/assets/js/alerts.min.js"></script>' 

# For autocompletion
echo '<!-- JS file -->'
echo '<script src="../awesomplete/awesomplete.min.js" async></script>'

echo '<!-- CSS file -->'
echo '<link rel="stylesheet" href="../awesomplete/awesomplete.css">'

# For copy to clipboard
echo '<script src="../clipboard.js/dist/clipboard.min.js"></script>'
 
echo '<style>'
# This for the popup explaining the risks about dropping Aurein from the default field
echo '/* Popup container - can be anything you want */'
echo '.popup {'
echo '    position: relative;'
echo '    display: inline-block;'
echo '    cursor: pointer;'
echo '    -webkit-user-select: none;'
echo '    -moz-user-select: none;'
echo '    -ms-user-select: none;'
echo '    user-select: none;'
echo '}'

echo '/* The actual popup */'
echo '.popup .popuptext {'
echo '    visibility: hidden;'
echo '    position: absolute;'
echo '    width: 480px;'
echo '    background-color: #FF9933;'
echo '    color: #fff;'
echo '    text-align: center;'
echo '    padding: 5px 0;'
echo '    border-radius: 6px;'
echo '    z-index: 1;'
echo '    opacity: 0;'
echo '    transition: opacity 0.3s;'
echo '  top: 0px;'
echo '  left: 105%;'
echo '}'

echo '/* Popup arrow */'
echo '.popup .popuptext::after {'
echo '    content: "";'
echo '    position: absolute;'
echo '    top: 50%;'
echo '    right: 100%;'
echo '    margin-top: -5px;'
echo '    border-width: 5px;'
echo '    border-style: solid;'
echo '    border-color: transparent #FF9933 transparent transparent;'
echo '}'

echo '/* Toggle this class - hide and show the popup */'
echo '.popup .show {'
echo '    visibility: visible;'
echo '    -webkit-animation: fadeIn 7s;'
echo '    animation: fadeIn 7s;'
echo '}'
echo '/* Add animation (fade in the popup) */'
echo '@-webkit-keyframes fadeIn {'
echo '    from {opacity: 1;} '
echo '    to {opacity: 0;}'
echo '}'

echo '@keyframes fadeIn {'
echo '    from {opacity: 1;}'
echo '    to {opacity:0 ;}'
echo '}'

echo '.tooltip {'
echo '    position: relative;'
echo '    display: inline-block;'
echo '    border-bottom: 0px dotted black;'
echo '}'

echo '.tooltip .tooltiptext {'
echo '	 '
echo '    visibility: hidden;'
echo '    width: 320px;'
echo '    background-color: black;'
echo '    color: #fff;'
echo '    text-align: center;'
echo '    border-radius: 6px;'
echo '    padding: 5px 5px;'
echo '    position: absolute;'
echo '    z-index: 1;'
# This for right tooltips
#echo '    top: -5px;'
#echo '    left: 120%;'
# This for top tooltips
echo '    bottom: 100%;'
echo '    left: 50%; '
echo '    margin-left: -160px; /* Use half of the width (120/2 = 60), to center the tooltip */'
echo '}'

echo '.tooltip .tooltiptext::after {'
echo '    content: "";'
echo '    position: absolute;'
# This for right tooltips
#echo '    top: 50%;'
#echo '    right: 100%; /* To the left of the tooltip */'
#echo '    margin-top: -5px;'
# This for top tooltips
echo '    top: 100%; /* At the bottom of the tooltip */'
echo '    left: 50%;'
echo '    margin-left: -5px;'
echo '    border-width: 5px;'
echo '    border-style: solid;'
#echo '    border-color: transparent black transparent transparent;'
echo '    border-color: black transparent transparent transparent;'
echo '}'
echo '.tooltip:hover .tooltiptext {'
echo '    visibility: visible;'
echo '}'

echo 'input:hover[type="submit"] {background: green;}'
echo 'input:hover[type="reset"] {background: red;}'
echo 'input:hover[type="button"] {background: orange;}'
echo 'input:hover.kill[type="submit"] {background-color: OrangeRed;}'

echo 'input[type="radio"]{'
echo '  -webkit-appearance: none;'
echo '  -moz-appearance: none;'
echo '  appearance: none;'
echo
echo '  border-radius: 50%;'
echo '  width: 16px;'
echo '  height: 16px;'
echo
echo '  border: 2px solid #999;'
echo '  transition: 0.2s all linear;'
echo '  outline: none;'
#echo '  margin-right: 1px;'
echo '  margin-left: 10px;'
echo
echo '  position: relative;'
echo '  top: 4px;'
echo '}'
echo
echo 'input:checked {'
echo '  border: 6px solid grey;'
echo '}'

echo '#banner {'
echo '  padding: 10px;'
echo '  color: rgb(80,80,80);'
echo '  text-align: center;'
echo '  text-decoration: underline;'
#echo '  text-decoration-color: red;'
echo '  letter-spacing: 5px;'
echo '  text-shadow: 1px 1px gray;'
echo '  font-size: 40px;'
echo '}'
echo
echo '#header {'
echo '  background-image: repeating-linear-gradient(-45deg, rgba(255,255,255,0.15), rgba(255,255,255,0.15) 15px, transparent 15px, transparent 25px), linear-gradient(to top ,'
echo '  rgba(230,230,230,0.2), rgba(120,120,120,0.5));'
echo '  height: 80px;'
echo '  border-radius: 10px 10px 10px 10px;'
echo '}'

echo 'a:hover {color: red;}'
echo 'body, h1,h2,h3,h4,h5,h6 {font-family: "Ubuntu", sans-serif}'
echo 'fieldset{border-radius: 10px; border:2px solid #bbb;margin:0 2px;padding:.35em .625em .75em}'
echo 'hr {border:0;border-top:5px solid #eee;margin:20px 0}'
echo '.w3-row-padding img {margin-bottom: 12px}'
echo '/* Set the width of the sidebar to 120px */'
echo '.w3-sidebar {width: 120px;background: #bbb;}'
echo '/* Add a left margin to the "page content" that matches the width of the sidebar (120px) */'
echo '#main {margin-left: 120px}'
echo '/* Remove margins from "page content" on small screens */'
echo '@media only screen and (max-width: 600px) {#main {margin-left: 0}}'

echo "select {"
#echo "    width: 10%;"
#echo "    padding: 10px 10px;"
echo "    border: none;"
echo "    border-radius: 4px;"
echo "    background-color: #ffffff;"
echo "}"
# If we add button here we get the buttons like all the others instead of the special style we want for them only for adaptable-form.sh
#echo "button, input[type=button], input[type=reset] {"
echo "input[type=button], input[type=reset] {"
echo "    background-color: #616161;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=submit] {"
echo "    background-color: DarkGreen;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input.kill[type=submit], .btn-red {"
echo "    background-color: #616161;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo '  -webkit-animation: glowing 1500ms infinite;'
echo '  -moz-animation: glowing 1500ms infinite;'
echo '  -o-animation: glowing 1500ms infinite;'
echo '  animation: glowing 1500ms infinite;'
echo "}"
echo '@-webkit-keyframes glowing {'
echo '  0% { background-color: #B20000; -webkit-box-shadow: 0 0 0px #B20000; }' #3px if you want to see the button like "growing"
echo '  50% { background-color: #FF0000; -webkit-box-shadow: 0 0 0px #FF0000; }' #40px
echo '  100% { background-color: #B20000; -webkit-box-shadow: 0 0 0px #B20000; }' #3px
echo '}'

echo '@-moz-keyframes glowing {'
echo '  0% { background-color: #B20000; -moz-box-shadow: 0 0 0px #B20000; }'
echo '  50% { background-color: #FF0000; -moz-box-shadow: 0 0 0px #FF0000; }'
echo '  100% { background-color: #B20000; -moz-box-shadow: 0 0 0px #B20000; }'
echo '}'

echo '@-o-keyframes glowing {'
echo '  0% { background-color: #B20000; box-shadow: 0 0 0px #B20000; }'
echo '  50% { background-color: #FF0000; box-shadow: 0 0 0px #FF0000; }'
echo '  100% { background-color: #B20000; box-shadow: 0 0 0px #B20000; }'
echo '}'

echo '@keyframes glowing {'
echo '  0% { background-color: #B20000; box-shadow: 0 0 0px #B20000; }'
echo '  50% { background-color: #FF0000; box-shadow: 0 0 0px #FF0000; }'
echo '  100% { background-color: #B20000; box-shadow: 0 0 0px #B20000; }'
echo '}'

echo "input[type=text], input[type=email], select {"
#echo "    width: 100%;"
echo "    padding: 5px 10px;"
#echo "    margin: 8px 0;"
echo "    display: inline-block;"
echo "    border: 1px solid #ccc;"
echo "    border-radius: 4px;"
echo "    box-sizing: border-box;"
echo "}"
echo "pre {"
echo "    white-space: pre-wrap;       /* Since CSS 2.1 */"
echo "    white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */"
echo "    white-space: -pre-wrap;      /* Opera 4-6 */"
echo "    white-space: -o-pre-wrap;    /* Opera 7 */"
echo "}"

# Using local fonts is faster and more reliable
#echo 'pre {font-family: 'DejaVu Sans Mono'}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Regular.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo 'pre {font-family: "Local Dejavu", "Local Liberation", monospace; font-size: 14px}'

echo '* {box-sizing: border-box;}'

echo '/* Button used to open the characters picker - fixed at the bottom of the page */'
echo '.open-button-chars {'
echo '  background-color: #555;'
echo '  color: white;'
#echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  opacity: 0.8;'
#echo '  position: fixed;'
#echo '  bottom: 23px;'
#echo '  right: 28px;'
#echo '  width: 280px;'
echo '}'

echo '/* The characters picker window - hidden by default */'
echo '.characters-popup {'
echo '  display: none;'
echo '  position: fixed;'
echo '  bottom: 23px;'
echo '  right: 28px;'
echo '  border: 3px solid #f1f1f1;'
echo '  z-index: 9;'
echo '}'

# Break names in the table, as they can be really long
echo '.characters-popup td {'
echo '  word-break: break-all;'
echo '}'

echo '/* Add styles to the form container */'
echo '.form-container-chars {'
echo '  max-width: 550px;'
echo '  padding: 10px;'
echo '  padding: 16px 20px;'
echo '  background-color: white;'
echo '}'

echo '/* Full-width textarea */'
echo '.form-container-chars textarea {'
echo '  width: 100%;'
echo '  padding: 15px;'
echo '  margin: 5px 0 22px 0;'
echo '  border: none;'
echo '  background: #f1f1f1;'
echo '  resize: none;'
echo '  min-height: 200px;'
echo '}'

echo '/* When the textarea gets focus, do something */'
echo '.form-container-chars textarea:focus {'
echo '  background-color: #ddd;'
echo '  outline: none;'
echo '}'

echo '/* Set a style for the submit/send button */'
echo '.form-container-chars .btn {'
echo '  background-color: #4CAF50;'
echo '  color: white;'
echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  width: 100%;'
echo '  margin-bottom:10px;'
echo '  opacity: 0.8;'
echo '}'

echo '/* Add a red background color to the cancel button */'
echo '.form-container-chars .cancel {'
echo '  background-color: red;'
echo '}'

echo '/* Add some hover effects to buttons */'
echo '.form-container-chars .btn:hover, .open-button-chars:hover {'
echo '  opacity: 1;'
echo '}'

echo '.dropbtn, .btn {'
echo '    text-decoration: none;'
echo '    background-color: DarkBlue;'
echo '    color: white;'
echo "    padding: 10px 30px;"
echo "    margin: 4px 2px;"
echo '    border: none;'
#echo "    border-radius: 10px;"
echo '    cursor: pointer;'
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo '}'

echo '.dropbtn:hover, .dropbtn:focus {'
echo '    background-color: Blue;'
echo '}'

echo '.dropdown {'
echo '    float: right;'
echo '    position: relative;'
echo '    display: inline-block;'
echo '}'

echo '.dropdown-content {'
echo '    display: none;'
echo '    position: absolute;'
echo '    background-color: #f1f1f1;'
echo '    min-width: 300px;'
echo '    overflow: auto;'
echo '    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);'
echo '    right: 0;'
echo '    z-index: 1;'
echo '}'

echo '.dropdown-content a {'
echo '    color: black;'
echo '    padding: 12px 16px;'
echo '    text-decoration: none;'
echo '    display: block;'
echo '}'

echo '.dropdown a:hover {background-color: #ddd;}'

echo '/* Show the dropdown menu on hover */'
echo '.dropdown:hover .dropdown-content {display: block;}'

echo '.show {display: block;}'

echo '.glyph{'
echo '    color: #fefefe;'
echo '    position: relative;'
echo '    left: 30px;'
echo '    pointer-events: none; /* this makes the glyph clickable as part of the input */'
echo '}'

#echo 'form { display: inline; }'

echo '/* The Modal (background) */'
echo '.modal {'
echo '    display: none; /* Hidden by default */'
echo '    position: fixed; /* Stay in place */'
echo '    z-index: 1; /* Sit on top */'
echo '    left: 0;'
echo '    top: 0;'
echo '    width: 100%; /* Full width */'
echo '    height: 100%; /* Full height */'
echo '    overflow: auto; /* Enable scroll if needed */'
echo '    background-color: rgb(0,0,0); /* Fallback color */'
echo '    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */'
echo '    -webkit-animation-name: fadeIn; /* Fade in the background */'
echo '    -webkit-animation-duration: 0.4s;'
echo '    animation-name: fadeIn;'
echo '    animation-duration: 0.4s'
echo '}'

echo '/* Modal Content */'
echo '.modal-content {'
echo '    position: fixed;'
echo '    bottom: 0;'
#echo '    background-color: #fefefe;'
echo '    width: 100%;'
echo '    -webkit-animation-name: slideIn;'
echo '    -webkit-animation-duration: 0.4s;'
echo '    animation-name: slideIn;'
echo '    animation-duration: 0.4s'
echo '}'

#echo '/* The Close Button */'
#echo '.close {'
#echo '    color: white;'
#echo '    float: right;'
#echo '    font-size: 28px;'
#echo '    font-weight: bold;'
#echo '}'

#echo '.close:hover,'
#echo '.close:focus {'
#echo '    color: #000;'
#echo '    text-decoration: none;'
#echo '    cursor: pointer;'
#echo '}'

#echo '.modal-header {'
#echo '    padding: 2px 16px;'
#echo '    background-color: #5cb85c;'
#echo '    color: white;'
#echo '}'

#echo '.modal-body {padding: 2px 16px;}'

#echo '.modal-footer {'
#echo '    padding: 2px 16px;'
#echo '    background-color: #5cb85c;'
#echo '    color: white;'
#echo '}'

echo '/* Add Animation */'
echo '@-webkit-keyframes slideIn {'
echo '    from {bottom: -300px; opacity: 0} '
echo '    to {bottom: 0; opacity: 1}'
echo '}'

echo '@keyframes slideIn {'
echo '    from {bottom: -300px; opacity: 0}'
echo '    to {bottom: 0; opacity: 1}'
echo '}'

echo '@-webkit-keyframes fadeIn {'
echo '    from {opacity: 0} '
echo '    to {opacity: 1}'
echo '}'

echo '@keyframes fadeIn {'
echo '    from {opacity: 0} '
echo '    to {opacity: 1}'
echo '}'

echo ".eraser {"
echo "        float: right;"
echo "        right: 10px;"
echo "        top: 10px;"
echo "        position: absolute;"
echo "        z-index: 1;"
echo "    }"
    
echo '</style>'
echo '</head>'

echo '<body class="w3-light-grey">'

echo '<!-- Icon Bar (Sidebar) -->'
echo '<nav class="w3-sidebar w3-bar-block w3-small w3-center">'
echo '  <!-- Avatar image in top left corner -->'
echo '  <img src="../webicons/icon.svg" alt="ADAPTABLE logo" style="width:100%">'
echo '  <a href="../index.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-home w3-xxlarge"></i>'
echo '    <p>HOME</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./adaptable-form.sh" class="w3-bar-item w3-button w3-padding-large w3-light-grey">'
echo '    <i class="fas fa-cogs w3-xxlarge"></i>'
echo '    <p>FAMILY GENERATOR</p>'
echo '  </a>'
echo '  <a href="./extract-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-search-plus w3-xxlarge"></i>'
echo '    <p>FAMILY ANALYZER</p>'
echo '  </a>'
echo '  <a href="./index-results.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-download w3-xxlarge"></i>'
echo '    <p>DOWNLOAD RESULTS</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./index-browse.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-list-ul w3-xxlarge"></i>'
echo '    <p>BROWSE AMPs & FAMILIES</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="../faq.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-question-circle w3-xxlarge"></i>'
echo '    <p>FAQ & TUTORIAL</p>'
echo '  </a>'
echo '  <a href="../about.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-users w3-xxlarge"></i>'
echo '    <p>ABOUT</p>'
echo '  </a>'
echo '</nav>'

echo '<div class="w3-padding-large" id="main">'

echo '<div id="header"> '
echo '        <div id="banner">'
echo '        ADAPTABLE'
echo '        </div>'
echo '</div>'

if [ "$(pgrep -f -u apache run-all.sh)" ] || [ "$(pgrep -f -u apache run-all-predictions.sh)" ]; then
	# Refresh from time to time
	echo "<script>window.setTimeout(function(){window.location.href=window.location.href},30000);</script>"

	# Previous instance is running
	export calculation_label="$(head -n1 ADAPTABLE/OUTPUTS/.out_parallel)"
	echo \
		"<article>"\
  		"<header>"\
                "<h1 style='display:inline;'>ADAPTABLE is running: </h1>"\
                "<h2 style='color:Red;display:inline;'>Please return later to start your job after current one is finished</h2>"
	echo '<h5>Results will be viewable in the <a href="./index-results.sh">"DOWNLOAD RESULTS"</a> section. You can extract meaningful information from the experiment using <a href="./extract-form.sh">"FAMILY ANALYZER"</a> tool.</h5>'
	cd ADAPTABLE
	echo    "<h3><i class='far fa-clock'></i>&nbsp;&nbsp;Approximate remaining time: <b>$(./ETA-date.sh "$(./ETA.sh)")</b></h3>"
	cd ..

	if ! [ "$(pgrep -f -u apache run-all-predictions.sh)" ]; then	
	        # Warn if families are > 1000 (that means 1000*4 lines in selection file)
        	if (( $(wc -l <ADAPTABLE/OUTPUTS/${calculation_label}/selection) > 4000 )); then
		        echo '<div class="container-alert"><div class="alert alert_warning"><div class="alert--icon"><i class="fas fa-exclamation-circle"></i></div><div class="alert--content">'
       		 	echo "<strong>Many peptides were selected, a run with unspecified criteria might require long calculation times.</strong>"
        		echo "<div class="alert--paragraph">If this is not intended, you can <a href="./kill.sh">kill the running process</a> and start a new one choosing more specific optional selection criteria fitting to your needs.</div>"
	       		echo '</div></div></div>'
        	fi
        fi


        if [ "$(pgrep -f -u apache awks/analyse_AMPs_database_body.awk)" ]; then
		# Needs to print and invisible &zwnj; character to make validator W3 to pass
#                echo '<h4 style="display:inline;"><i class="fas fa-spinner fa-spin"></i>&zwnj;</h4>'
#                echo "<h5 style='display:inline;color:Green;'>&nbsp;&nbsp;ADAPTABLE is generating families...</h5>"
		echo '<div class="container-alert"><div class="alert alert_success"><div class="alert--icon"><i class="fas fa-spinner fa-spin"></i></div><div class="alert--content">'
		echo "ADAPTABLE is generating families..."
		echo '</div></div></div>'
        fi

        if [ "$(pgrep -f -u apache awks/analyse_AMPs_database_tail_sort.awk)" ]; then
#                echo '<h4 style="display:inline;"><i class="fas fa-spinner fa-spin"></i>&zwnj;</h4>'
#                echo "<h5 style='display:inline;color:Green;'>&nbsp;&nbsp;Sorting of families running, after that, Analysis will run and provide meaningful ETA time measurements again</h5>"
                echo '<div class="container-alert"><div class="alert alert_success"><div class="alert--icon"><i class="fas fa-spinner fa-spin"></i></div><div class="alert--content">'
                echo "Sorting of families running, after that, Analysis will run and provide meaningful ETA time measurements again"
                echo '</div></div></div>'
        fi

        if [ "$(pgrep -f -u apache awks/analyse_AMPs_database_tail_parallel.awk)" ]; then
#                echo '<h4 style="display:inline;"><i class="fas fa-spinner fa-spin"></i>&zwnj;</h4>'
#                echo "<h4 style='display:inline;'>&nbsp;&nbsp;ADAPTABLE Analysis is running: </h4><h5 style='display:inline;color:Green;'>$(tail -n 1000000 "ADAPTABLE/OUTPUTS/${calculation_label}/${calculation_label}-output"|grep "Calculating for Family"|tail -n1)</h5>"
                echo '<div class="container-alert"><div class="alert alert_success"><div class="alert--icon"><i class="fas fa-spinner fa-spin"></i></div><div class="alert--content">'
                echo "ADAPTABLE Analysis is running: $(tail -n 1000000 "ADAPTABLE/OUTPUTS/${calculation_label}/${calculation_label}-output"|grep "Calculating for Family"|tail -n1)"
                echo '</div></div></div>'
        fi

        if [ "$(pgrep -f -u apache plot_graph.py)" ]; then
                echo '<div class="container-alert"><div class="alert alert_success"><div class="alert--icon"><i class="fas fa-spinner fa-spin"></i></div><div class="alert--content">'
                echo "Generating graphics..."
                echo '</div></div></div>'
        fi

	echo	'<div id="out" style="border: 2px solid Darkgrey;padding: 15px;margin: 25px;height:60vh;overflow:auto;resize: both">'
	# Not too buffering or people could see used calculation_label at the top of the output while calculating families and kill other people jobs
	# We don't need to put it into <pre> tags because ansi2html makes that for us
    	echo	"$(tail -n 1000 "ADAPTABLE/OUTPUTS/${calculation_label}/${calculation_label}-output" | cl-ansi2html -n -w)"
    	echo	'</div>'

        # It's is better to not use window.location.reload because form will be resent if people click on "Refresh" once run finished
	#echo '<input type="button" value="Refresh Page" onClick="window.location.reload()">'
        echo \
	        "<form style='float:left;' action="adaptable-form.sh" method="get">"\
		"<input class="Reload" type="submit" value='Refresh Page'>"\
        	"</form>"

	echo \
		"</header>"\
		"</article>"

	echo \
        "<form style='float:left;' action="kill.sh" method="get">"\
        "<input class="kill" type="submit" value='Kill processes'>"\
        "</form>"

	# Scroll down the div
	echo '<script>var objDiv = document.getElementById("out");'
	echo 'objDiv.scrollTop = objDiv.scrollHeight;</script>'
	# Scroll down the whole page
        echo '<script>window.scrollTo(0, document.body.scrollHeight);</script>'

	echo "</div>"
	echo '</body>'
	echo '</html>'

	exit 0
else

# Here we list all the options that can have infinite values
# We start a form here
	echo "<article>"
	echo "<header>"
	echo "<h1>Family generator</h1>"
	echo "</header>"	
	echo "</article>"

	echo '<div class="w3-main">'

	echo '  <div class="w3-padding-8">'
	echo '    <div class="w3-container">'
	echo '      <h5>In this page you can create a family of antimicrobial peptides featuring user-selected properties.</h5>'
	echo '	    <h6>Please take a look at the <a href="../faq.html">Frequently Asked Questions (FAQ) and the tutorial</a>.</h6>'
	echo '	    <p><i>Note: To know more about each field simply hover your mouse over it. in some fields the following options are available: y="yes; n="no"; or i=ignored.</i><p>'
	echo '    </div>'
	echo '  </div>'

	echo '</div>'

	source ./tooltips.sh

# Here is the black magic for collapsing fields
echo '<script>'
echo '        $(function(){'
echo '               $("legend.collapsibleLegend").click(function(){'
echo '                        $(this).parent().find(".collapsibleFieldset").slideToggle("slow");'
echo '                });'
echo '        });'
echo '</script>'
echo '<script>'
echo '  $(document).ready(function() {'
# Only get the first part of the lagend and change the rest
echo '  var prev_all = $("legend.collapsibleLegend").text();'
echo '  var ret = prev_all.split(" ");'
echo '  var prev = ret[0];'
echo '    $("legend.collapsibleLegend").click(function() {'
echo '    $("#collapsibleDiv").toggleClass("active");'
echo '    if ($(".collapsibleFieldset").hasClass("active")) {'
echo '      $("legend.collapsibleLegend").html(prev + "<sub> (+) Expand Me</sub>");'
echo '    } else {'
echo '      $("legend.collapsibleLegend").html(prev + "<sub> (-) Collapse Me</sub>");'
echo '    }'
echo '  });'
echo '});'
echo '</script>'
#

# Show some fields only if user needs them
echo "<script>"
echo 'function mailfunction() {'
echo '  // Get the checkbox'
echo '  var checkBox = document.getElementById("mailcheck");'
echo '  // Get the output text'
echo '  var text = document.getElementById("showmail");'
echo '  // If the checkbox is checked, display the output text'
echo '  if (checkBox.checked == true){'
echo '    text.style.display = "inline";'
echo '  } else {'
echo '    text.style.display = "none";'
echo '  }'
echo '}'
echo "</script>"

	echo "<hr>"

echo '<div class="dropdown">'
echo '<button onclick="mydropdownfunc()" class="dropbtn">Samples</button>'
echo '  <div id="myDropdown" class="dropdown-content">'
echo '    <a href="#" onclick="loadSample(3)">Append User Peptides</a>'
echo '    <a href="#" onclick="loadSample(0)">Peptides targeting Acinetobacter</a>'
echo '    <a href="#" onclick="loadSample(2)">Peptides targeting Shigella</a>'
echo '    <a href="#" onclick="loadSample(1)">Aurein</a>'
echo '    <a href="#" onclick="loadSample(4)">Select by pattern</a>'
echo '  </div>'
echo '</div>'

echo '<script>'
echo '/* When the user clicks on the button, '
echo 'toggle between hiding and showing the dropdown content */'
echo 'function mydropdownfunc() {'
echo '    document.getElementById("myDropdown").classList.toggle("show");'
echo '}'

echo '// Close the dropdown if the user clicks outside of it'
echo 'window.onclick = function(event) {'
echo "  if (!event.target.matches('.dropbtn')) {"

echo '    var dropdowns = document.getElementsByClassName("dropdown-content");'
echo '    var i;'
echo '    for (i = 0; i < dropdowns.length; i++) {'
echo '      var openDropdown = dropdowns[i];'
echo "      if (openDropdown.classList.contains('show')) {"
echo "        openDropdown.classList.remove('show');"
echo '      }'
echo '    }'
echo '  }'
echo '}'
echo '</script>'

echo "<script>"
echo 'function loadSample(sample_id)'
echo '{'
echo ' $( "#reset" ).trigger( "click" );'
echo ' if(sample_id==0)'
echo '      loadAcinetobacter();'
echo '  if(sample_id==1)'
echo '      loadAurein();'
echo '  if(sample_id==2)'
echo '      loadShigella();'
echo '  if(sample_id==3)'
echo '      loadMyPeptide();'
echo '  if(sample_id==4)'
echo '      loadMypattern();'
echo '}'


echo 'function loadAcinetobacter(){'
echo 'var samplevar = "acinetobacter";'
echo '$("#sample").val(samplevar);'
# Empty value from other fields
echo "document.getElementById('sample2').value = '';"
echo "document.getElementById('sample3').value = '';"
echo "document.getElementById('sample4').value = '';"
echo '}'

echo 'function loadAurein(){'
echo 'var samplevar = "Aurein";'
echo '$("#sample2").val(samplevar);'
# Empty value from other fields
echo "document.getElementById('sample').value = '';"
echo "document.getElementById('sample3').value = '';"
echo "document.getElementById('sample4').value = '';"
echo '}'

echo 'function loadShigella(){'
echo 'var samplevar = "shigella";'
echo '$("#sample").val(samplevar);'
# Empty value from other fields
echo "document.getElementById('sample2').value = '';"
echo "document.getElementById('sample3').value = '';"
echo "document.getElementById('sample4').value = '';"
echo '}'

echo 'function loadMyPeptide(){'
echo 'var samplevar = "FDIVKKIAGHIAS,KKVVGTLAGL";'
echo '$("#sample3").val(samplevar);'
# Empty value from other fields
echo "document.getElementById('sample2').value = '';"
echo "document.getElementById('sample').value = '';"
echo "document.getElementById('sample4').value = '';"
echo '}'

echo 'function loadMypattern(){'
echo 'var samplevar = "EEEAA";'
echo '$("#sample4").val(samplevar);'
# Empty value from other fields
echo "document.getElementById('sample2').value = '';"
echo "document.getElementById('sample').value = '';"
echo "document.getElementById('sample3').value = '';"
echo '}'

echo "</script>"

	echo "<form name='sel' method=GET action=adaptable-form.sh>"
	echo "<table style='table-layout: fixed;'>"
	echo "<tr><td><div class="tooltip"><i class='fas fa-address-book'></i>&nbsp;&nbsp;Calculation label <sup>(mandatory)</sup><span class="tooltiptext">"${tooltiptext_calculation_label}"</span></div></td><td><input type="text" name="_calculation_label" pattern='[A-Z,a-z,0-9,_,-]{1,100}' title='Only letters, numbers and hyphens are allowed' placeholder='Choose a name for your run...' size=30 required></td></tr>"
	echo "<tr><td><div class="tooltip"><i class='fas fa-user'></i>&nbsp;&nbsp;Username <sup>(mandatory)</sup><span class="tooltiptext">"${tooltiptext_user_key}"</span></div></td><td><input type="text" name="_user_key" pattern='[A-Z,a-z,0-9,_,-]{1,100}' title='Only letters, numbers and hyphens are allowed' placeholder='Choose a username to store your experiments...' size=30 required>"
# For now we make this mandatory as it helps people to know the calculation_label, for cases of long runs with a renamed label
#	echo "&nbsp;&nbsp;<input type='checkbox' id='mailcheck' onclick='mailfunction()'>&nbsp;&nbsp;<i class='fas fa-envelope'></i>&nbsp;&nbsp;Email notification&nbsp;&nbsp;"
#	echo "<div id='showmail' style='display:none;'><input type='email' name='_sendmail' placeholder='example@example.org'></div>"
       echo "<div class="tooltip">&nbsp;&nbsp;<i class='fas fa-envelope'></i>&nbsp;&nbsp;Email notification <sup>(mandatory)</sup>&nbsp;&nbsp;<span class="tooltiptext">"${tooltiptext_sendmail}"</span></div>"
       echo "<div style='display:inline;'><input type='email' name='_sendmail' placeholder='example@example.org' required></div>"
	echo "</td></tr>"
	echo "<tr><td>"
	echo "<div class="tooltip"><i class='fas fa-plus'></i>&nbsp;&nbsp;Append User peptides<span class="tooltiptext">"${tooltiptext_my_peptide}"</span></div></td><td><input id="sample3" type="text" name="_my_peptide" pattern='[A-ZԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ,,]{0,100}' title='In capital letters, different peptides separated by commas' placeholder='1-letter code aminoacid sequence' size=30>"

# FIXME: We need to close here to allow the styles to be passed properly for newer forms... probably there is a non closed tag elsewhere
# Not needed anymore since nested forms were moved out of the main form
#echo '</form>'

# Character selector
echo '<script>'
echo 'function openForm() {'
echo '    document.getElementById("myForm").style.display = "inline-block";'
echo '}'
echo 'function closeForm() {'
echo '    document.getElementById("myForm").style.display = "none";'
echo '}'
echo '</script>'

echo '<button type="button" class="open-button-chars" onclick="openForm()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'
#
# type="button" mandatory to not submit the form from it

        echo "&nbsp;&nbsp;<input type='checkbox' id='predict' onclick='predictfunction()'>&nbsp;&nbsp;<i class='fas fa-sitemap'></i>&nbsp;&nbsp;Create the family of a specific peptide&nbsp;&nbsp;"
        echo "<div id='showpredict' style='display:none;'><div class='tooltip'></div><input id='prediction' type='text' name='_prediction' pattern='[A-ZԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]{0,100}' title='In capital letters, different peptides separated by commas' placeholder='1-letter code aminoacid sequence' size=20>"
# Character selector
echo '<script>'
echo 'function openForm2() {'
echo '    document.getElementById("myForm2").style.display = "inline-block";'
echo '}'
echo 'function closeForm2() {'
echo '    document.getElementById("myForm2").style.display = "none";'
echo '}'
echo '</script>'

echo '<button type="button" class="open-button-chars" onclick="openForm2()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'
#
        echo "</div>"

echo '<script>'
echo '// When the user clicks on div, open the popup'
echo 'function mypopupfunc() {'
echo '    var popup = document.getElementById("myPopup");'
echo '    popup.classList.toggle("show");'
echo '}'
echo '</script>'
	echo "</td></tr>"
	echo "</table>"
	echo "<hr>"
	echo '      <h6><i>Optional selection criteria (a run with unspecified criteria might require long calculation times):</i></h6>'

	echo "<table style='table-layout: fixed;'>"
	echo "<tr><td>"
	echo "<div class="tooltip">Peptide name<sup id='typeeffect'></sup><span class="tooltiptext">"${tooltiptext_name}"</span></div></td><td>"

echo '<script>'
echo 'var i = 0;'
echo "var txt = '(empty if indiferent)';"
echo 'var speed = 50;'

echo 'function typeWriter() {'
echo '  if (i < txt.length) {'
echo '    document.getElementById("typeeffect").innerHTML += txt.charAt(i);'
echo '    i++;'
echo '    setTimeout(typeWriter, speed);'
echo '  }'
echo '}'
echo 'window.onload = typeWriter;'
echo '</script>'

	echo '<div class="popup"><span class="popuptext" id="myPopup">A run with unspecified criteria might require long calculation times</span>'
	echo "<input id="sample2" style='border: 2px solid grey;border-radius: 4px;' type="text" placeholder='e.g. Aurein'"
	# Split this because of the quotes craziness
	echo 'onfocus="this.value='"''"'"'
	echo 'onblur="mypopupfunc()"'
	echo " value="Aurein" name="_name" size=30 pattern='[A-Z,a-z,0-9,-,_,.]{1,100}' title='Only letters, numbers and hyphens are allowed'>"
	echo '	<span class="fas fa-eraser eraser" onclick="dropaureinfunction()"></span>'
	echo "</div></td></tr>"

	# Add this &nbsp;&nbsp; to workaround the movement of the table after typewritter script does its job
	echo "<tr><td><div class="tooltip">Sequence pattern&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="tooltiptext">"${tooltiptext_sequence}"</span></div></td><td><input id='sample4' type="text" name="_sequence" pattern='[A-ZԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]{0,100}' title='In capital letters' placeholder='e.g. K-KK{polar}KK{K,P,L} or GL-DIVKKVVGA-GSL' size=30>"

# Character selector
echo '<script>'
echo 'function openForm3() {'
echo '    document.getElementById("myForm3").style.display = "inline-block";'
echo '}'
echo 'function closeForm3() {'
echo '    document.getElementById("myForm3").style.display = "none";'
echo '}'
echo '</script>'

echo '<button type="button" class="open-button-chars" onclick="openForm3()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'
#

#        echo \
#                "<div class="tooltip">&nbsp;&nbsp;Work in simplified aa space<span class="tooltiptext">"${tooltiptext_work_in_simplified_aa_space}"</span></div>"\
#                '<select onchange="showDiv(this)" name="_work_in_simplified_aa_space">'\
#                        '<option value="n">n</option>'\
#                        '<option value="y">y</option>'\
#                        '<option value="DSSP">DSSP</option>'\
#                "</select>"\
#                "<div id='hidden_div' style='display: none;' class="tooltip">DSSP sequence &nbsp;<span class="tooltiptext">"${tooltiptext_DSSP}"</span><input type="text" name="_DSSP" size=30 pattern='[A-Z]{0,100}' title='In capital letters'></div></td></tr>"

# Show div only when user needs it
#echo "<script>"
#echo 'function showDiv(elem){'
#echo '   if(elem.value == "DSSP")'
#echo '      document.getElementById("hidden_div").style.display = "inline";'
#echo '}'
#echo "</script>"

# Show some fields only if user needs them
echo "<script>"
echo 'function predictfunction() {'
# Drop default 'Aurein' value to not interfere
echo "document.getElementById('sample2').value = '';"
echo "document.getElementById('similarity').value = '51';"
echo '  // Get the checkbox'
echo '  var checkBox = document.getElementById("predict");'
echo '  // Get the output text'
echo '  var text = document.getElementById("showpredict");'
echo '  // If the checkbox is checked, display the output text'
echo '  if (checkBox.checked == true){'
echo '    text.style.display = "inline";'
echo '  } else {'
echo '    text.style.display = "none";'
echo '  }'
echo '}'
echo "</script>"

echo "<script>"
echo 'function dropaureinfunction() {'
echo "document.getElementById('sample2').value = '';"
echo '}'
echo "</script>"

	echo \
		"<tr><td><div class="tooltip">Target Organism<span class="tooltiptext">"${tooltiptext_all_organisms}"</span></div></td><td><input type="text" id="sample" name="_all_organisms" size=30 pattern='[A-Z,a-z,0-9,-,_, ,.,]{1,100}' title='Only letters, numbers, hyphens, dots and spaces are allowed. Type X for any kind of organism' placeholder='e.g. baumannii'>" \
		"<div class="tooltip">&nbsp;&nbsp;Activity (µM) &nbsp;<span class="tooltiptext">"${tooltiptext_activity}"</span></div><input type="text" name="_activity" size=1 pattern='[X,0-9,,,.,i,<,>,=]{1,100}' title='Must be numbers, X or i'>" \
		"<div class="tooltip"> Activity test &nbsp;<span class="tooltiptext">"${tooltiptext_activity_test}"</span></div><select name="_activity_test">" \
					"<option value="i">i</option>"\
					"<option value="MIC">MIC</option>"\
					"<option value="MIC50">MIC50</option>"\
					"<option value="MIC90">MIC90</option>"\
					"<option value="MAC">MAC</option>"\
					"<option value="MBC">MBC</option>"\
					"<option value="MBC50">MBC50</option>"\
					"<option value="MBC90">MBC90</option>"\
					"<option value="GI50">GI50</option>"\
					"<option value="IC50">IC50</option>"\
					"<option value="IC100">IC100</option>"\
					"<option value="HC10">HC10</option>"\
					"<option value="EC50">EC50</option>"\
					"<option value="EC100">EC100</option>"\
					"<option value="LC50">LC50</option>"\
					"<option value="HD50">HD50</option>"\
					"<option value="HC50">HC50</option>"\
					"<option value="LD50">LD50</option>"\
					"<option value="MHC">MHC</option>"\
				"</select>"\
		"</td></tr>"
	echo "</table>"
	
	echo '<fieldset>'
	echo '    <legend class="collapsibleLegend">Advanced <sub>(+) Expand Me</sub> &nbsp;</legend>'
	echo '    <div id="collapsibleDiv" class="collapsibleFieldset active" style="display: none;">'
	echo "<table style='table-layout: fixed;'>"



	echo "<tr><td><div class="tooltip">Sequence length range<span class="tooltiptext">"${tooltiptext_sequence_length_range}"</span></div></td><td><input type="text" name="_sequence_length_range" value="5-50" pattern='[0-9]+[-][0-9]+' title='Must fill two numbers separated by a hyphen' size=3 required></td></tr>"

	echo \
		"<tr><td><div class="tooltip">Stereoisomerism<span class="tooltiptext">"${tooltiptext_stereo}"</span></div></td><td>"\
		"<select name="_stereo">"\
			"<option value="i">i</option>"\
			"<option value="L">L</option>"\
			"<option value="D">D</option>"\
			"<option value="mix">mix</option>"\
		"</select>"\
		"</td></tr>"
				
	for i in {ID,source,taxonomy,Family,gene,N_terminus,C_terminus,PTM,target,pdb,DSSP,PMID};do
		# We need that stupid &nbsp; to force spacing of the tooltips to not break with short names getting the arrow too close to the name
		# Not needed as I opted for top tooltips instead or right ones
		echo \
			"<tr><td><div class="tooltip">"${i/_/ }"&nbsp;&nbsp;<span class="tooltiptext">$(eval "echo \$tooltiptext_"${i}"")</span></div></td><td><input placeholder='$(eval "echo \$tooltiptext_"${i}""|awk -F"[()]" '{print $2}')' type="text" name="_${i}" size=30 pattern='[A-Z,a-z,0-9,-,_]{1,100}' title='Only letters, numbers and hyphens are allowed'></td></tr>"
	done
	echo	"</table>"
	# This is for running really long experiments
	echo "<hr>"
	echo "<input type="checkbox" name="_long_run" value="yes"> I want to proceed with a run including many peptides, even if that will require more time to finish.<br>"
	echo '</div>'
	echo '</fieldset>'

#echo '  <div class="w3-panel w3-border w3-round">'  
echo "<br>"

# Here is the black magic for collapsing fields
echo '<script>'
echo '        $(function(){'
echo '               $("legend.collapsibleLegend2").click(function(){'
echo '                        $(this).parent().find(".collapsibleFieldset2").slideToggle("slow");'
echo '                });'
echo '        });'
echo '</script>'
echo '<script>'
echo '  $(document).ready(function() {'
# Only get the first part of the legend and change the rest
echo '  var prev_all = $("legend.collapsibleLegend2").text();'
echo '  var ret = prev_all.split(" ");'
echo '  var prev = ret[0];'
echo '    $("legend.collapsibleLegend2").click(function() {'
echo '    $("#collapsibleDiv2").toggleClass("active");'
echo '    if ($(".collapsibleFieldset2").hasClass("active")) {'
echo '      $("legend.collapsibleLegend2").html("Peptide properties <sub> (+) Expand Me</sub>");' # prev + "<sub..." Dropped to not mess with phrases with multiple spaces
echo '    } else {'
echo '      $("legend.collapsibleLegend2").html("Peptide properties <sub> (-) Collapse Me</sub>");' # prev + "<sub..." Dropped to not mess with phrases with multiple spaces
echo '    }'
echo '  });'
echo '});'
echo '</script>'
#

echo "<fieldset>"
#echo "<legend>Peptide properties:</legend>"
echo '<legend style="display:inline;" class="collapsibleLegend2">Peptide properties <sub> (+) Expand Me</sub></legend>'
echo '<div id="collapsibleDiv2" class="collapsibleFieldset2 active" style="display: none;">'
# FIXME: antiproliferative skipped for now because it only has one occurrence in the whole DATABASE and looks redundant with anticancer... this will force 'i' for that field as a consequence
for j in {antimicrobial,antibiofilm,antiparasitic,insecticidal,anticancer,toxic,cell_cell,drug_delivery,antioxidant,cyclic,ribosomal,synthetic};do
  [ ${j} != "antimicrobial" ] && echo "<hr>"
  echo \
  	"<div class="tooltip"><b>"${j/_/ }":</b><span class="tooltiptext">$(eval "echo \$tooltiptext_"${j}"")</span></div>"\
  	"<input type="radio" name="_${j}" value="i" checked> i"\
	"<input type="radio" name="_${j}" value="y"> y"\
	"<input type="radio" name="_${j}" value="n"> n"\
	"<br>"
	case "${j}" in
		antimicrobial)
			echo \
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">antibacterial:<span class="tooltiptext">"${tooltiptext_antibacterial}"</span></div>"\
				"<input type="radio" name="_antibacterial" value="i" checked> i"\
                                "<input type="radio" name="_antibacterial" value="y"> y"\
                                "<input type="radio" name="_antibacterial" value="n"> n"
                        echo "<br>"
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			for jj in {antigram_pos,antigram_neg};do
				echo \
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip"><i>"${jj/_/ }"</i>:<span class="tooltiptext">$(eval "echo \$tooltiptext_"${jj}"")</span></div>"\
					"<input type="radio" name="_${jj}" value="i" checked> i"\
					"<input type="radio" name="_${jj}" value="y"> y"\
					"<input type="radio" name="_${jj}" value="n"> n"
			done
			echo "<br>"
			for jjj in {antifungal,antiyeast};do
				echo \
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">"${jjj}":<span class="tooltiptext">$(eval "echo \$tooltiptext_"${jjj}"")</span></div>"\
					"<input type="radio" name="_${jjj}" value="i" checked> i"\
					"<input type="radio" name="_${jjj}" value="y"> y"\
					"<input type="radio" name="_${jjj}" value="n"> n"
			done
			echo "<br>"
			echo \
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">antiviral:<span class="tooltiptext">"${tooltiptext_antiviral}"</span></div>"\
				"<input type="radio" name="_antiviral" value="i" checked> i"\
				"<input type="radio" name="_antiviral" value="y"> y"\
				"<input type="radio" name="_antiviral" value="n"> n"
			echo \
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">virus_name:&nbsp;<span class="tooltiptext">$(eval "echo \$tooltiptext_virus_name")</span></div><input type="text" name="_virus_name" size=10 pattern='[A-Z,a-z,0-9,-,_]{1,100}' title='Must be letters, numbers or hyphens' placeholder='$(eval "echo \$tooltiptext_virus_name"|awk -F"[()]" '{print $2}')'>"
			echo \
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">activity viral (µM):<span class="tooltiptext">"${tooltiptext_activity_viral}"</span></div> <input type="text" name="_activity_viral" size=1 pattern='[0-9,,,.,i]{1,100}' title='Must be numbers or i'>" \
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">activity viral test:<span class="tooltiptext">"${tooltiptext_activity_viral_test}"</span></div> <select name="_activity_viral_test">" \
						"<option value="i">i</option>"\
                                        	"<option value="MIC">MIC</option>"\
                                        	"<option value="MIC50">MIC50</option>"\
                                        	"<option value="MIC90">MIC90</option>"\
                                        	"<option value="MAC">MAC</option>"\
                                        	"<option value="MBC">MBC</option>"\
                                        	"<option value="MBC50">MBC50</option>"\
                                        	"<option value="MBC90">MBC90</option>"\
                                        	"<option value="GI50">GI50</option>"\
                                        	"<option value="IC50">IC50</option>"\
                                        	"<option value="IC100">IC100</option>"\
                                        	"<option value="HC10">HC10</option>"\
                                        	"<option value="EC50">EC50</option>"\
                                        	"<option value="EC100">EC100</option>"\
                                        	"<option value="LC50">LC50</option>"\
                                        	"<option value="HD50">HD50</option>"\
                                        	"<option value="HC50">HC50</option>"\
                                        	"<option value="LD50">LD50</option>"\
                                        	"<option value="MHC">MHC</option>"\
                                	"</select>"
			echo "<br>"
			;;
		antiparasitic)
			for jk in {antiprotozoal,antiplasmodial,antitrypanosomic,antileishmania};do
				echo \
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">"${jk}":<span class="tooltiptext">$(eval "echo \$tooltiptext_"${jk}"")</span></div>"\
					"<input type="radio" name="_${jk}" value="i" checked> i"\
					"<input type="radio" name="_${jk}" value="y"> y"\
					"<input type="radio" name="_${jk}" value="n"> n"
			done
			echo "<br>"
			;;
		anticancer)
			echo \
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">antitumor:<span class="tooltiptext">"${tooltiptext_antitumor}"</span></div>"\
				"<input type="radio" name="_antitumor" value="i" checked> i"\
				"<input type="radio" name="_antitumor" value="y"> y"\
				"<input type="radio" name="_antitumor" value="n"> n"
			for jl in {cell_line,tissue,cancer_type};do
				echo \
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">"${jl/_/ }":&nbsp;<span class="tooltiptext">$(eval "echo \$tooltiptext_"${jl}"")</span></div><input type="text" name="_${jl}" size=10 pattern='[A-Z,a-z,0-9,-,_]{1,100}' title='Must be letters, numbers or hyphens' placeholder='$(eval "echo \$tooltiptext_"${jl}""|awk -F"[()]" '{print $2}')'>"
			done
                        echo \
                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">anticancer activity (µM):<span class="tooltiptext">"${tooltiptext_anticancer_activity}"</span></div> <input type="text" name="_anticancer_activity" size=1 pattern='[0-9,,,.,i]{1,100}' title='Must be numbers or i'>" \
                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">anticancer activity test:<span class="tooltiptext">"${tooltiptext_anticancer_activity_test}"</span></div> <select name="_anticancer_activity_test">" \
                                                "<option value="i">i</option>"\
                                                "<option value="MIC">MIC</option>"\
                                                "<option value="MIC50">MIC50</option>"\
                                                "<option value="MIC90">MIC90</option>"\
                                                "<option value="MAC">MAC</option>"\
                                                "<option value="MBC">MBC</option>"\
                                                "<option value="MBC50">MBC50</option>"\
                                                "<option value="MBC90">MBC90</option>"\
                                                "<option value="GI50">GI50</option>"\
                                                "<option value="IC50">IC50</option>"\
                                                "<option value="IC100">IC100</option>"\
                                                "<option value="HC10">HC10</option>"\
                                                "<option value="EC50">EC50</option>"\
                                                "<option value="EC100">EC100</option>"\
                                                "<option value="LC50">LC50</option>"\
                                                "<option value="HD50">HD50</option>"\
                                                "<option value="HC50">HC50</option>"\
                                                "<option value="LD50">LD50</option>"\
                                                "<option value="MHC">MHC</option>"\
                                        "</select>"

			echo "<br>"
			echo \
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">antiangiogenic:<span class="tooltiptext">"${tooltiptext_antiangiogenic}"</span></div>"\
				"<input type="radio" name="_antiangiogenic" value="i" checked> i"\
				"<input type="radio" name="_antiangiogenic" value="y"> y"\
				"<input type="radio" name="_antiangiogenic" value="n"> n"
			echo "<br>"
			;;
		toxic)
			echo \
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">cytotoxic:<span class="tooltiptext">"${tooltiptext_cytotoxic}"</span></div>"\
				"<input type="radio" name="_cytotoxic" value="i" checked> i"\
				"<input type="radio" name="_cytotoxic" value="y"> y"\
				"<input type="radio" name="_cytotoxic" value="n"> n"
			echo "<br>"
			echo \
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">hemolytic:<span class="tooltiptext">"${tooltiptext_hemolytic}"</span></div>"\
				"<input type="radio" name="_hemolytic" value="i" checked> i"\
				"<input type="radio" name="_hemolytic" value="y"> y"\
				"<input type="radio" name="_hemolytic" value="n"> n"
                        echo \
                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">hemolytic activity (µM)<span class="tooltiptext">"${tooltiptext_hemolytic_activity}"</span></div> <input type="text" name="_hemolytic_activity" size=1 pattern='[0-9,,,.,i]{1,100}' title='Must be numbers or i'>" \
                                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">hemolytic activity test<span class="tooltiptext">"${tooltiptext_hemolytic_activity_test}"</span></div> <select name="_hemolytic_activity_test">" \
                                                "<option value="i">i</option>"\
                                                "<option value="MIC">MIC</option>"\
                                                "<option value="MIC50">MIC50</option>"\
                                                "<option value="MIC90">MIC90</option>"\
                                                "<option value="MAC">MAC</option>"\
                                                "<option value="MBC">MBC</option>"\
                                                "<option value="MBC50">MBC50</option>"\
                                                "<option value="MBC90">MBC90</option>"\
                                                "<option value="GI50">GI50</option>"\
                                                "<option value="IC50">IC50</option>"\
                                                "<option value="IC100">IC100</option>"\
                                                "<option value="HC10">HC10</option>"\
                                                "<option value="EC50">EC50</option>"\
                                                "<option value="EC100">EC100</option>"\
                                                "<option value="LC50">LC50</option>"\
                                                "<option value="HD50">HD50</option>"\
                                                "<option value="HC50">HC50</option>"\
                                                "<option value="LD50">LD50</option>"\
                                                "<option value="MHC">MHC</option>"\
                                        "</select>"
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">RBC source<span class="tooltiptext">"${tooltiptext_RBC_source}"</span></div> <input type="text" name="_RBC_source" size=10 pattern='[A-Z,a-z,0-9,-,_]{1,100}' title='Must be letters, numbers or hyphens' placeholder='$(echo "${tooltiptext_RBC_source}"|awk -F"[()]" '{print $2}')'>"
			echo "<br>"
			;;
		cell_cell)
			for jn in {hormone,quorum_sensing,immunomodulant,antihypertensive};do
				echo \
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">"${jn/_/ }":<span class="tooltiptext">$(eval "echo \$tooltiptext_"${jn}"")</span></div>"\
					"<input type="radio" name="_${jn}" value="i" checked> i"\
					"<input type="radio" name="_${jn}" value="y"> y"\
					"<input type="radio" name="_${jn}" value="n"> n"
			done
			echo "<br>"
			;;
		drug_delivery)
			for jo in cell_penetrating;do # FIXME: no peptide under {tumor_homing,blood_brain} categories
				echo \
					"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <div class="tooltip">"${jo/_/ }":<span class="tooltiptext">$(eval "echo \$tooltiptext_"${jo}"")</span></div>"\
					"<input type="radio" name="_${jo}" value="i" checked> i"\
					"<input type="radio" name="_${jo}" value="y"> y"\
					"<input type="radio" name="_${jo}" value="n"> n"
			done
			echo "<br>"
			;;				
		*)
			continue
	esac
done

# We need to handle this in a special way because it needs 'pdb' as 'y' and it only handles that and 'y' as valid values
#  echo \
#        "<hr>"\
#        "<div class="tooltip"><b>antibiofilm:</b><span class="tooltiptext">"${tooltiptext_antibiofilm}"</span></div>"\
#        "<input type="radio" name="_antibiofilm" value="i" checked> i"\
#        "<input type="radio" name="_antibiofilm" value="biofilm"> y"
#        "<br>"

  echo \
        "<hr>"\
        "<div class="tooltip"><b>experimental structure:</b><span class="tooltiptext">"${tooltiptext_experim_structure}"</span></div>"\
        "<input type="radio" name="_experim_structure" value="i" checked> i"\
        "<input type="radio" name="_experim_structure" value="pdb"> y"\
        "<br>"

  echo \
	"<hr>"\
        "<div class="tooltip"><b>Solubility:</b><span class="tooltiptext">"${tooltiptext_solubility}"</span></div>"\
        "<input type="radio" name="_solubility" value="i" checked> i"\
        "<input type="radio" name="_solubility" value="soluble"> soluble"\
        "<input type="radio" name="_solubility" value="poorly_soluble"> poorly soluble"\
        "<input type="radio" name="_solubility" value="almost_insoluble"> almost insoluble"\
        "<input type="radio" name="_solubility" value="insoluble"> insoluble"\
	"<br>"
echo "<br>"
echo "</div>"
echo "</fieldset>"

echo "<br><fieldset>"

  echo \
        "<div class="tooltip"><i class='fas fa-align-center'></i>&nbsp;&nbsp;<b>Align method:</b><span class="tooltiptext">"${tooltiptext_align_method}"</span></div>"\
        "<input type="radio" name="_align_method" value="simple"> Simple"\
        "<input type="radio" name="_align_method" value="DSSP"> DSSP"\
        "<input type="radio" name="_align_method" value="blosum" checked> Substitution matrix"
  echo \
                "<div class="tooltip"><span class="tooltiptext">"${tooltiptext_align_matrix}"</span></div>"\
                "<select name="_align_matrix">"\
                        "<option value="blosum45">Blosum45</option>"\
                        "<option value="blosum62">Blosum62</option>"\
                        "<option value="blosum30">Blosum30</option>"\
                        "<option value="blosum35">Blosum35</option>"\
			"<option value="blosum40">Blosum40</option>"\
			"<option value="blosum50">Blosum50</option>"\
			"<option value="blosum55">Blosum55</option>"\
			"<option value="blosum60">Blosum60</option>"\
			"<option value="blosum65">Blosum65</option>"\
			"<option value="blosum70">Blosum70</option>"\
			"<option value="blosum75">Blosum75</option>"\
			"<option value="blosum80">Blosum80</option>"\
			"<option value="blosum85">Blosum85</option>"\
			"<option value="blosum90">Blosum90</option>"\
			"<option value="blosum100">Blosum100</option>"\
			"<option value="PAM250">PAM250</option>"\
			"<option value="PAM160">PAM160</option>"\
			"<option value="PAM100">PAM100</option>"\
			"<option value="PAM120">PAM120</option>"\
			"<option value="PAM200">PAM200</option>"\
			"<option value="PAM350">PAM350</option>"\
			"<option value="UNITARY">Unitary scoring</option>"\
                "</select>"
 echo "<div class="tooltip">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Minimum % of similarity </b><span class="tooltiptext">"${tooltiptext_level_agreement}"</span></div> <input id="similarity" type="text" name="_level_agreement" value="40" size=1 pattern='[0-9]{1,100}' title='Must be numbers' required>"
 echo "<div class="tooltip">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fas fa-layer-group'></i> <b>Threshold percentage to group families </b><span class="tooltiptext">"${tooltiptext_group_fam_level}"</span></div> <input type="text" name="_group_fam_level" value="75" size=1 pattern='[0-9]{1,100}' title='Must be numbers' required>"

 echo "<hr>"
 echo \
        "<div class="tooltip"><i class='fas fa-signature'></i>&nbsp;&nbsp;<b>Simplify aminoacids:</b><span class="tooltiptext">"${tooltiptext_simplify_aminoacids}"</span></div>"\
        '<input type="radio" name="_simplify_aminoacids" value="y"> y'\
        '<input type="radio" name="_simplify_aminoacids" value="n" checked> n'
  echo \
        "<div class="tooltip">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fas fa-flask'></i>&nbsp;&nbsp;<b>Experimentally validated:</b><span class="tooltiptext">"${tooltiptext_experimental}"</span></div>"\
        "<input type="radio" name="_experimental" value="i" checked> i"\
        "<input type="radio" name="_experimental" value="y"> y"\
        "<input type="radio" name="_experimental" value="n"> n"
  echo \
        "<div class="tooltip">&nbsp;&nbsp;<i>Include only peptides with data about</i>:</b><span class="tooltiptext">"${tooltiptext_tune_experimental}"</span></div>"\
        "<select name="_tune_experimental">"\
	        "<option value="i">i</option>"\
                "<option value="target">Target Organism</option>"\
                "<option value="activity">Activity</option>"\
        "</select>"
  echo \
        "<hr>"\
        "<div class="tooltip"><i class='fas fa-draw-polygon fa-lg'></i>&nbsp;&nbsp;<b>Run <a href="https://github.com/WebLogo/weblogo" target="blank"/>SeqLogo</a>:</b><span class="tooltiptext">"${tooltiptext_seqlogo}"</span></div>"\
        "<input type="radio" name="_seqlogo" value="y"> y"\
        "<input type="radio" name="_seqlogo" value="n" checked> n"
 echo \
        "<div class="tooltip">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class='fas fa-chart-area fa-lg'></i>&nbsp;&nbsp;<b>Generate additional graphical analysis:</b><span class="tooltiptext">"${tooltiptext_plot_graphs}"</span></div>"\
        '<input type="radio" name="_plot_graphs" value="y" id="myCheck" onclick="mydisplaycheck()"> y'\
        "<input type="radio" name="_plot_graphs" value="n" checked> n"
 echo "<div style='display:none' id='show'>"
 echo "<div class="tooltip">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>n regions </b><span class="tooltiptext">"${tooltiptext_n_regions}"</span></div> <input type="text" name="_n_regions" size=10 pattern='[0-9,i]{1,100}' title='Must be numbers or i'>"
 echo "</div>"

# Show some fields only if user needs them
echo "<script>"
echo 'function mydisplaycheck() {'
echo '  // Get the checkbox'
echo '  var checkBox = document.getElementById("myCheck");'
echo '  // Get the output text'
echo '  var text = document.getElementById("show");'
echo '  // If the checkbox is checked, display the output text'
echo '  if (checkBox.checked == true){'
echo '    text.style.display = "inline";'
echo '  } else {'
echo '    text.style.display = "none";'
echo '  }'
echo '}'
echo "</script>"

echo "</fieldset>"

echo "<h6><i>Results will be viewable in the <a href="./index-results.sh">"DOWNLOAD RESULTS"</a> section. You can extract meaningful information from the experiment using <a href="./extract-form.sh">"FAMILY ANALYZER"</a> tool too.</i></h6>"

echo '<br><input type="submit" value="Submit">'\
      '<input type="reset" value="Reset"></form>'

  # If form wasn't filled, exit gracefully now.
  if [ -z "$QUERY_STRING" ]; then
        echo "</div>"

# Character selector (part 2): we need to put forms here as they are not allowed instead another form per specification and causes other issues depending on the browser
echo '<div class="characters-popup" id="myForm2">'
echo '  <form class="form-container-chars">'
echo '    <h3>Special characters for modified aminoacids</h3>'
echo "<i>You can click on the symbol to select it and use Ctrl+F to search your desired aminoacid</i>"
echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
if [ -z "$QUERY_STRING" ]; then
	cat ../character-selector | sed -e 's/input-text/prediction/g' | sed -e 's/iAC/iAC2/g'
else
	echo ""
fi
echo '</div>'
echo '    <button type="button" class="btn cancel" onclick="closeForm2()"><i class="fas fa-signature"></i></button>'
echo '  </form>'
echo '</div>'

echo '<div class="characters-popup" id="myForm3">'
echo '  <form class="form-container-chars">'
echo '    <h3>Special characters for modified aminoacids</h3>'
echo "<i>You can click on the symbol to select it and use Ctrl+F to search your desired aminoacid</i>"
echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
if [ -z "$QUERY_STRING" ]; then
	cat ../character-selector | sed -e 's/input-text/sample4/g' | sed -e 's/iAC/iAC3/g'
else
	echo ""
fi
echo '</div>'
echo '    <button type="button" class="btn cancel" onclick="closeForm3()"><i class="fas fa-signature"></i></button>'
echo '  </form>'
echo '</div>'

echo '<div class="characters-popup" id="myForm">'
echo '  <form class="form-container-chars">'
echo '    <h3>Special characters for modified aminoacids</h3>'
echo "<i>You can click on the symbol to select it and use Ctrl+F to search your desired aminoacid</i>"
echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
if [ -z "$QUERY_STRING" ]; then
	cat ../character-selector | sed -e 's/input-text/sample3/g'
else
	echo ""
fi
echo '</div>'
echo '    <button type="button" class="btn cancel" onclick="closeForm()"><i class="fas fa-signature"></i></button>'
echo '  </form>'
echo '</div>'

# End of the form

# New form
  # If form wasn't filled, exit gracefully now.
#  if [ -z "$QUERY_STRING" ]; then
#        echo "</div>"

echo '<script>'
echo 'var input = document.getElementById("sample3");'
echo '// Show label but insert value into the input:'
echo 'new Awesomplete(input, {'
echo '  maxItems: 20,'
echo '  list: ['
# Compress the file with gzip -9 to load it faster later
zcat ../DATABASE.seqs.js.awesomplete.gz
echo '  ]'
echo '});'
echo '</script>'
echo '<script>'
echo 'var input = document.getElementById("prediction");'
echo '// Show label but insert value into the input:'
echo 'new Awesomplete(input, {'
echo '  maxItems: 20,'
echo '  list: ['
zcat ../DATABASE.seqs.js.awesomplete.gz
echo '  ]'
echo '});'
echo '</script>'
        exit 0
  else

echo '<!-- The Modal -->'
echo '<div id="myModal" class="modal">'

echo '  <!-- Modal content -->'
echo '  <div class="modal-content">'
#echo '    <div class="modal-header">'
#echo '      <span class="close">&times;</span>'
#echo '      <h2>Modal Header</h2>'
#echo '    </div>'
#echo '    <div class="modal-body">'
#echo '      <p>Some text in the Modal Body</p>'
#echo '      <p>Some other text...</p>'

# Sometimes it can be a small delay with subsequent operations, try to inform the user meantime
# FIXME: not enough for solving it
#echo '<div class="container-alert"><div class="alert"><div class="alert--icon"><i class="fas fa-check-circle"></i></div><div class="alert--content">'
#echo "Launching experiment"
#echo '</div></div></div>'

# Not useful anymore as we create now a model for the progress
#echo "<form style='float:right;' action="kill.sh" method="get">"
#echo '<span><i class="fas fa-skull glyph"></i></span>'
#echo "<input class="kill" type="submit" value='Kill processes'>"
#echo "</form>"

	# Shorten addressbar
	echo "<script>"
	echo 'function shorten_addressbar_url() {'
	echo 'var url = window.location.href;'
	echo "var new_url = url.split('?')[0];"
	echo "history.replaceState('1', '', new_url);"
	echo '}'

	echo 'shorten_addressbar_url();'
	echo "</script>"

	# Now we set all the splitted vars from QUERY_STRING
	for k in {calculation_label,user_key,sendmail,source,ID,sequence,name,taxonomy,Family,gene,work_in_simplified_aa_space,stereo,N_terminus,C_terminus,PTM,cyclic,target,all_organisms,activity,activity_test,\
antimicrobial,antiprotozoal,insecticidal,anticancer,toxic,cell_cell,drug_delivery,antioxidant,antiproliferative,DSSP,pdb,experim_structure,PMID,synthetic,sequence_length_range,solubility,align_method,align_matrix,level_agreement,group_fam_level,\
plot_graphs,simplify_aminoacids,n_regions,seqlogo,\
experimental,tune_experimental,\
antibacterial,antigram_pos,antigram_neg,antifungal,antiyeast,antiviral,virus_name,activity_viral,activity_viral_test,\
antiparasitic,antiplasmodial,antitrypanosomic,antileishmania,\
antitumor,cell_line,tissue,cancer_type,anticancer_activity,anticancer_activity_test,antiangiogenic,\
cytotoxic,hemolytic,hemolytic_activity,hemolytic_activity_test,RBC_source,\
hormone,quorum_sensing,immunomodulant,antihypertensive,\
cell_penetrating,tumor_homing,blood_brain,\
ribosomal,antibiofilm,\
my_peptide,prediction,long_run};do
#		export __${k}="`echo "$QUERY_STRING" | sed -n "s/^.*$k=\([^&]*\).*$/\1/p" | sed "s/%20/ /g"`"
		# Last is for converting spaces (+ in QUERY_STRING to _) and get proper commas and characters
		export __${k}="$(echo "$QUERY_STRING" | tr '&' '\n'|grep -E "\<_$k\>" | sed "s/%20/ /g" | cut -d '=' -f2 | sed "s/+/_/g" | sed "s/%2C/,/g" | sed "s/%3C/</g" | sed "s/%3E/>/g" | sed "s/%3D/=/g")"
		# Fix UTF8 chars handling
	        export utf8_seq=$(echo -e $(echo "${__sequence}" | sed -e 's/%/.0x/g'| sed -e 's/0x\(..\)\.\?/\\x\1/g')|sed -e 's/\.//g')
	        export __sequence="${utf8_seq}"
                export utf8_seq=$(echo -e $(echo "${__prediction}" | sed -e 's/%/.0x/g'| sed -e 's/0x\(..\)\.\?/\\x\1/g')|sed -e 's/\.//g')
                export __prediction="${utf8_seq}"
                export utf8_seq=$(echo -e $(echo "${__my_peptide}" | sed -e 's/%/.0x/g'| sed -e 's/0x\(..\)\.\?/\\x\1/g')|sed -e 's/\.//g')
                export __my_peptide="${utf8_seq}"


		# FIXME: antiproliferative force to 'i' due it tagging only one anticancer peptide
		# FIXME: no peptide under tumor_homing,blood_brain categories
		export __antiproliferative="i"
		export __tumor_homing="i"
		export __blood_brain="i"

		# If DSSP or simple align methods are chosen, work_in_simplified_aa_space needs to be in sync
		export __work_in_simplified_aa_space="n"
		[ "${__align_method}" == "DSSP" ] && export __work_in_simplified_aa_space="DSSP"
		[ "${__align_method}" == "simple" ] && export __work_in_simplified_aa_space="y"
		
		# When people chooses to include only peptides with any target organism or activity, we need to set them properly to not collide
		if [ "${__tune_experimental}" == "target" ]; then
			[ "${__all_organisms}" == "" ] && export __all_organisms="X"
		fi
		if [ "${__tune_experimental}" == "activity" ]; then
			[ "${__activity}" == "" ] && export __activity="X"
		fi
	done
	echo 
  fi

# Handle people not setting calculation label
if [ -z "${__calculation_label}" ]; then
#	echo '<li><i class="fa-li fas fa-spinner fa-spin"></i><b>calculation label set as 'empty'</b></li>'
	echo '<div class="container-alert"><div class="alert alert_info"><div class="alert--icon"><i class="fas fa-check-circle"></i></div><div class="alert--content">'
	echo "<b>Calculation label set as "empty"</b>"
	echo '</div></div></div>'

	export __calculation_label="empty"
fi

# Don't overwrite existing files
if grep -Fxq "${__user_key}" ADAPTABLE/Results/.databases; then
#        echo "<li><i class='fa-li fas fa-spinner fa-spin'></i><div style='color:red;'>Username coincides with built-in DBs, renamed to <b>user-"${__user_key}"</b> (please remember this new user for accessing your data later)</div></li>"
        echo '<div class="container-alert"><div class="alert alert_warning"><div class="alert--icon"><i class="fas fa-exclamation-circle"></i></div><div class="alert--content">'
        echo "Username coincides with built-in DBs, renamed to <b>user-"${__user_key}"</b> (please remember this new user for accessing your data later)"
        echo '</div></div></div>'

        export __user_key="user-${__user_key}"
	# Scroll down and give some seconds to read the messages
	echo '<script>window.scrollTo(0, document.body.scrollHeight);</script>'
	sleep 5
fi
if [ -d "ADAPTABLE/Results/${__user_key}/${__calculation_label}" ] || grep -Fxq "${__calculation_label}" ADAPTABLE/Results/.databases; then
	export timestamp="${__calculation_label}-$(date -u +%c | sed -e 's/ /_/g'| sed -e 's/:/./g')" 
	export __calculation_label="${timestamp}"	
	#echo "<li><i class='fa-li fas fa-spinner fa-spin'></i>calculation label already used, renamed to <b>"${timestamp}"</b> (please remember this for the case you want to kill the processes later)</li>"
        echo '<div class="container-alert"><div class="alert alert_warning"><div class="alert--icon"><i class="fas fa-exclamation-circle"></i></div><div class="alert--content">'
        echo "<p style='float:left;'>Calculation label already used, <b><u>RENAMED</u></b> to <b style='user-select: all;'>"${timestamp}"</b> (please remember this for the case you want to kill the processes later)</p>"
        echo "<button style='float:right;' class='btn-red' data-clipboard-text="${__calculation_label}">"
	echo '<i class="fas fa-copy"></i>   Copy Calculation Label to clipboard'
	echo '</button>'
        echo '</div></div></div>'
fi

# Needed to workaround ugly interaction between CGI that makes UTF8 encoding to be lost at some point
export LANG="en_US.UTF-8"

#echo "<li><i class='fa-li fas fa-spinner fa-spin'></i>Running <b>"${__calculation_label}"</b>... you can check the progress <b><a style='color:red;' href='./adaptable-form.sh'>here</a></b></li>"
echo '<div class="container-alert"><div class="alert alert_info"><div class="alert--icon"><i class="fas fa-spinner fa-spin"></i></div><div class="alert--content">'
echo "<strong>Running <b style='user-select: all;'>"${__calculation_label}"</b>... you can check the progress <b><a href='./adaptable-form.sh'>here</a></b></strong>"
echo "<div class="alert--paragraph">Results will be viewable in the <a href="./index-results.sh">"DOWNLOAD RESULTS"</a> section. You can extract meaningful information from the experiment using <a href="./extract-form.sh">"FAMILY ANALYZER"</a> tool too."

echo "<form style='float:right;' action="kill.sh" method="get">"
echo '<span><i class="fas fa-skull glyph"></i></span>'
echo "<input class="kill" type="submit" value='Kill processes'>"
echo "</form>"

# Don't show another button when it has been renamed as that warning also has a button
if [ -z "${timestamp}" ]; then
	echo "<button style='float:right;' class='btn' data-clipboard-text="${__calculation_label}">"
	echo '    Copy Calculation Label to clipboard'
	echo '</button>'
fi

echo "</div>"
echo '</div></div></div>'

echo '<script>'
echo "var clipboard = new ClipboardJS('.btn');"

echo "clipboard.on('success', function(e) {"
echo "    console.info('Action:', e.action);"
echo "    console.info('Text:', e.text);"
echo "    console.info('Trigger:', e.trigger);"

echo "    e.clearSelection();"
echo "});"

echo "clipboard.on('error', function(e) {"
echo "    console.error('Action:', e.action);"
echo "    console.error('Trigger:', e.trigger);"
echo "});"
echo "</script>"

echo '<script>'
echo "var clipboard = new ClipboardJS('.btn-red');"

echo "clipboard.on('success', function(e) {"
echo "    console.info('Action:', e.action);"
echo "    console.info('Text:', e.text);"
echo "    console.info('Trigger:', e.trigger);"

echo "    e.clearSelection();"
echo "});"

echo "clipboard.on('error', function(e) {"
echo "    console.error('Action:', e.action);"
echo "    console.error('Trigger:', e.trigger);"
echo "});"
echo "</script>"

#echo '    </div>'
#echo '    <div class="modal-footer">'
#echo '      <h3>Modal Footer</h3>'
#echo '    </div>'
echo '  </div>'

echo '</div>'

#echo '<!-- Trigger/Open The Modal -->'
#echo '<button id="myBtn">Open Modal</button>'

echo '<script>'
echo '// Get the modal'
echo "var modal = document.getElementById('myModal');"

#echo '// Get the button that opens the modal'
#echo 'var btn = document.getElementById("myBtn");'

#echo '// Get the <span> element that closes the modal'
#echo 'var span = document.getElementsByClassName("close")[0];'

#echo '// When the user clicks the button, open the modal '
#echo 'btn.onclick = function() {'
# Open automatically
echo '    modal.style.display = "block";'
#echo '}'

#echo '// When the user clicks on <span> (x), close the modal'
#echo 'span.onclick = function() {'
#echo '    modal.style.display = "none";'
#echo '}'

#echo '// When the user clicks anywhere outside of the modal, close it'
#echo 'window.onclick = function(event) {'
#echo '    if (event.target == modal) {'
#echo '        modal.style.display = "none";'
#echo '    }'
#echo '}'
echo '</script>'

# Scroll down
echo '<script>window.scrollTo(0, document.body.scrollHeight);</script>'

# Javascript method to reload (the only that works even when the page is still loading (in ms)
echo "<script>window.setTimeout(function(){window.location.href=window.location.href},45000);</script>"

mkdir -p ./ADAPTABLE/OUTPUTS/"${__calculation_label}"
cd ./ADAPTABLE/

# Run all commands in one big line to get all completed even when apache timeouts act
if [ ${__prediction} ]; then
	export __comparison_option="sons"
	./run-all-predictions.sh
else
	./run-all.sh
fi

echo '<!-- The Modal -->'
echo '<div id="myModal2" class="modal">'

echo '  <!-- Modal content -->'
echo '  <div class="modal-content">'

if [ -e "Results/${__user_key}/${__calculation_label}/.empty" ] || [ -e "OUTPUTS/${__calculation_label}/.empty" ] ; then
	echo '<div class="container-alert"><div class="alert alert_danger"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
	echo "<h2>*** No peptides were selected, you need to choose different selection criteria ***</h2>"
	echo '</div></div></div>'
else
	if [ -e "Results/${__user_key}/${__calculation_label}/.long_run" ] || [ -e "OUTPUTS/${__calculation_label}/.long_run" ] ; then
		echo '<div class="container-alert"><div class="alert alert_danger"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
		echo "<h2>*** More than 1000 peptides were selected, this could take a long time to finish ***</h2>"
		echo "<h4>You can define more strictive filters to reduce the amount of selectioned peptides.</h4>"
		echo "<h4>If you still want to run as-is, please enable <i>I want to proceed with a run...</i> option under <i>Advanced</i> section</h4>"
		echo '</div></div></div>'
	else
		echo '<div class="container-alert"><div class="alert alert_success"><div class="alert--icon"><i class="fas fa-check-circle"></i></div><div class="alert--content">'
		echo "<h2>Run complete</h2>"
		echo '</div></div></div>'
	fi
fi

echo '  </div>'

echo '</div>'

echo '<script>'
echo '// Get the modal'
echo "var modal = document.getElementById('myModal2');"
# Open automatically
echo '    modal.style.display = "block";'
echo '</script>'

# Scroll down
echo '<script>window.scrollTo(0, document.body.scrollHeight);</script>'
echo '</div>'
if [ -e "Results/${__user_key}/${__calculation_label}/.empty" ] || [ -e "OUTPUTS/${__calculation_label}/.empty" ] ; then
	echo "<script>window.setTimeout(function(){window.location.replace('./adaptable-form.sh')},3000);</script>"
else
	if [ -e "Results/${__user_key}/${__calculation_label}/.long_run" ] || [ -e "OUTPUTS/${__calculation_label}/.long_run" ] ; then
		echo "<script>window.setTimeout(function(){window.location.replace('./adaptable-form.sh')},15000);</script>"
	else
		echo "<script>window.setTimeout(function(){window.location.replace('./extract-form.sh?_calculation_label=${__calculation_label}&_user_key=${__user_key}')},1000);</script>"
	fi
fi
fi

echo '</body>'
echo '</html>'

exit 0
