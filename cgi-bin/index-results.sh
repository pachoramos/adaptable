#!/bin/bash

echo "Content-type: text/html"
echo ""

echo '<!DOCTYPE html>'
echo '<html lang="en">'
echo '<head>'
echo '<title>Results</title>'
echo '<meta name="viewport" content="width=1080">'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<link rel="shortcut icon" href="../favicon.ico" />'
echo '<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">'
echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">'
# Using local fonts is faster and more reliable
#echo '<link rel="stylesheet" media="none" href="https://fontlibrary.org/face/dejavu-sans-mono" onload="this.media='all';" type="text/css"/>'

echo '<link href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" rel="stylesheet">'

echo '<link rel="stylesheet" href="../alerts-css-master/assets/css/alerts-css.min.css">'
echo '<script src="../alerts-css-master/assets/js/alerts.min.js"></script>' 

echo '<style>'
echo '.tooltip {'
echo '    position: relative;'
echo '    display: inline-block;'
echo '    border-bottom: 0px dotted black;'
echo '}'

echo '.tooltip .tooltiptext {'
echo '   '
echo '    visibility: hidden;'
echo '    width: 320px;'
echo '    background-color: black;'
echo '    color: #fff;'
echo '    text-align: center;'
echo '    border-radius: 6px;'
echo '    padding: 5px 5px;'
echo '    position: absolute;'
echo '    z-index: 1;'
# This for right tooltips
#echo '    top: -5px;'
#echo '    left: 120%;'
# This for top tooltips
echo '    bottom: 100%;'
echo '    left: 50%; '
echo '    margin-left: -160px; /* Use half of the width (120/2 = 60), to center the tooltip */'
echo '}'

echo '.tooltip .tooltiptext::after {'
echo '    content: "";'
echo '    position: absolute;'
# This for right tooltips
#echo '    top: 50%;'
#echo '    right: 100%; /* To the left of the tooltip */'
#echo '    margin-top: -5px;'
# This for top tooltips
echo '    top: 100%; /* At the bottom of the tooltip */'
echo '    left: 50%;'
echo '    margin-left: -5px;'
echo '    border-width: 5px;'
echo '    border-style: solid;'
#echo '    border-color: transparent black transparent transparent;'
echo '    border-color: black transparent transparent transparent;'
echo '}'
echo '.tooltip:hover .tooltiptext {'
echo '    visibility: visible;'
echo '}'

# Needed to display blocks in big list of extract options properly without cutting in the middle
echo 'label {'
echo '  display: inline-block;'
echo '}'

echo 'input:hover[type="submit"] {background: green;}'
echo 'input:hover[type="reset"] {background: red;}'
echo 'button:hover {background: blue;}'

echo 'input[type="radio"]{'
echo '  -webkit-appearance: none;'
echo '  -moz-appearance: none;'
echo '  appearance: none;'
echo
echo '  border-radius: 50%;'
echo '  width: 16px;'
echo '  height: 16px;'
echo
echo '  border: 2px solid #999;'
echo '  transition: 0.2s all linear;'
echo '  outline: none;'
#echo '  margin-right: 1px;'
echo '  margin-left: 10px;'
echo
echo '  position: relative;'
echo '  top: 4px;'
echo '}'
echo
echo 'input:checked {'
echo '  border: 6px solid grey;'
echo '}'

# For the boxes... as it doesn't work for radio
#echo 'input:hover {'
#echo '  color: red;'
#echo '}'

echo '#banner {'
echo '  padding: 10px;'
echo '  color: rgb(80,80,80);'
echo '  text-align: center;'
echo '  text-decoration: underline;'
#echo '  text-decoration-color: red;'
echo '  letter-spacing: 5px;'
echo '  text-shadow: 1px 1px gray;'
echo '  font-size: 40px;'
echo '}'
echo
echo '#header {'
echo '  background-image: repeating-linear-gradient(-45deg, rgba(255,255,255,0.15), rgba(255,255,255,0.15) 15px, transparent 15px, transparent 25px), linear-gradient(to top ,'
echo '  rgba(230,230,230,0.2), rgba(120,120,120,0.5));'
echo '  height: 80px;'
echo '  border-radius: 10px 10px 10px 10px;'
echo '}'

echo 'a:hover {color: red;}'
echo 'body, h1,h2,h3,h4,h5,h6 {font-family: "Ubuntu", sans-serif}'
echo 'fieldset{border-radius: 10px; border:2px solid #bbb;margin:0 2px;padding:.35em .625em .75em}'
echo 'hr {border:0;border-top:5px solid #eee;margin:20px 0}'
echo '.w3-row-padding img {margin-bottom: 12px}'
echo '/* Set the width of the sidebar to 120px */'
echo '.w3-sidebar {width: 120px;background: #bbb;}'
echo '/* Add a left margin to the "page content" that matches the width of the sidebar (120px) */'
echo '#main {margin-left: 120px}'
echo '/* Remove margins from "page content" on small screens */'
echo '@media only screen and (max-width: 600px) {#main {margin-left: 0}}'

echo "select {"
#echo "    width: 10%;"
#echo "    padding: 10px 10px;"
echo "    border: none;"
echo "    border-radius: 4px;"
echo "    background-color: #ffffff;"
echo "}"
echo "button, input[type=button], input[type=reset] {"
echo "    background-color: #616161;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=submit] {"
echo "    background-color: DarkGreen;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=text], select {"
#echo "    width: 100%;"
echo "    padding: 5px 10px;"
#echo "    margin: 8px 0;"
echo "    display: inline-block;"
echo "    border: 1px solid #ccc;"
echo "    border-radius: 4px;"
echo "    box-sizing: border-box;"
echo "}"

# Using local fonts is faster and more reliable
#echo 'pre {font-family: 'DejaVu Sans Mono'}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Regular.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo 'pre {font-family: "Local Dejavu", "Local Liberation", monospace; font-size: 14px}'

echo '</style>'
echo '</head>'
echo '<body class="w3-light-grey">'

echo '<!-- Icon Bar (Sidebar) -->'
echo '<nav class="w3-sidebar w3-bar-block w3-small w3-center">'
echo '  <!-- Avatar image in top left corner -->'
echo '  <img src="../webicons/icon.svg" alt="ADAPTABLE logo" style="width:100%">'
echo '  <a href="../index.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-home w3-xxlarge"></i>'
echo '    <p>HOME</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./adaptable-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-cogs w3-xxlarge"></i>'
echo '    <p>FAMILY GENERATOR</p>'
echo '  </a>'
echo '  <a href="./extract-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-search-plus w3-xxlarge"></i>'
echo '    <p>FAMILY ANALYZER</p>'
echo '  </a>'
echo '  <a href="./index-results.sh" class="w3-bar-item w3-button w3-padding-large w3-light-grey">'
echo '    <i class="fas fa-download w3-xxlarge"></i>'
echo '    <p>DOWNLOAD RESULTS</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./index-browse.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-list-ul w3-xxlarge"></i>'
echo '    <p>BROWSE AMPs & FAMILIES</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="../faq.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-question-circle w3-xxlarge"></i>'
echo '    <p>FAQ & TUTORIAL</p>'
echo '  </a>'
echo '  <a href="../about.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-users w3-xxlarge"></i>'
echo '    <p>ABOUT</p>'
echo '  </a>'
echo '</nav>'

echo '<div class="w3-padding-large" id="main">'

echo '<div id="header"> '
echo '        <div id="banner">'
echo '        ADAPTABLE'
echo '        </div>'
echo '</div>'

echo \
	"<article>"\
	"<header>"\
	"<h1>Results</h1>"\
	"  <h5>Download your calculation. Please note that experiments will be removed after 6 months.</h5>"\
	"<p><i>Note: <a href='../faq.html#extract'>Here</a> you can find more information on how to extract generated files, in case they don't unpack automatically.</i></p>"\
	"</header>"\
	"</article>"

source ./tooltips.sh

if [ -z "$QUERY_STRING" ]; then
	echo "<hr>"
	echo "<form method=GET>"
	echo "<div class="tooltip"><i class='fas fa-user'></i>&nbsp;&nbsp;Username&nbsp;<span class="tooltiptext">"${tooltiptext_user_key}"</span></div><input type="text" name="_user_key" size=20 placeholder='Name of your run experiment...' pattern='[A-Za-z0-9-_.]+' required>"
	echo "<input type="submit" value='Submit'>"
	echo "<input type="reset" value="Reset">"
	echo "</form>"
	echo "</div>"

	exit 0
else

	# Shorten addressbar
	echo "<script>"
	echo 'function shorten_addressbar_url() {'
	echo 'var url = window.location.href;'
	echo "var new_url = url.split('?')[0];"
	echo "history.replaceState('1', '', new_url);"
	echo '}'

	echo 'shorten_addressbar_url();'
	echo "</script>"

	export __user_key=$(echo "$QUERY_STRING" | sed -n "s/^.*user_key=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
	if ! [ -e "ADAPTABLE/Results/${__user_key}/" ]; then
		echo "<hr>"
                        echo '<div class="container-alert"><div class="alert alert_danger"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
                        echo '<h2>Wrong "Username" set... please <a style="color:red;" href="./index-results.sh">RELOAD this page</a> and chose a proper value</h2>'
                        echo '</div></div></div>'

		echo "<hr>"
#		echo '<meta http-equiv="refresh" content="2;url=./index-results.sh" />'
		echo "<script>window.setTimeout(function(){window.location.replace('./index-results.sh')},2000);</script>"
		exit 0
	else
		# This is to try to get proper height for iframe, we need to substract banner, header... and many other pixels
		echo "<div style='height: calc(100vh - 228px);'><iframe src="../Results/${__user_key}/" style='border:none;width:100%;height:100%;'></iframe></div>"
	fi
fi

echo '</div>'
echo '</body>'
echo '</html>'

exit 0
