#!/bin/bash

echo "Content-type: text/html"
echo ""

echo '<!DOCTYPE html>'
echo '<html lang="en">'
echo '<head>'
echo '<title>AMP Families DB</title>'
echo '<meta name="viewport" content="width=1080">'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<link rel="shortcut icon" href="../favicon.ico" />'
echo '<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">'
echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">'
# Using local fonts is faster and more reliable
#echo '<link rel="stylesheet" media="none" href="https://fontlibrary.org/face/dejavu-sans-mono" onload="this.media='all';" type="text/css"/>'

echo '<link href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" rel="stylesheet">'

echo '<link rel="stylesheet" href="../alerts-css-master/assets/css/alerts-css.min.css">'
echo '<script src="../alerts-css-master/assets/js/alerts.min.js"></script>' 

# Top+bottom bars for tables
echo '<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>'
echo '<script src="../jqDoubleScroll-master/jquery.doubleScroll.js"></script>'

echo '<script>'
echo '  $(document).ready(function() {'
echo '     $("#table2").doubleScroll({resetOnWindowResize: true});'
echo '  });'
echo '</script>'

# For autocompletion
echo '<!-- JS file -->'
echo '<script async src="../awesomplete/awesomplete.min.js"></script>'

echo '<!-- CSS file -->'
echo '<link rel="stylesheet" href="../awesomplete/awesomplete.css">'

echo '<style>'
echo '.tooltip {'
echo '    position: relative;'
echo '    display: inline-block;'
echo '    border-bottom: 0px dotted black;'
echo '}'

echo '.tooltip .tooltiptext {'
echo '   '
echo '    visibility: hidden;'
echo '    width: 320px;'
echo '    background-color: black;'
echo '    color: #fff;'
echo '    text-align: center;'
echo '    border-radius: 6px;'
echo '    padding: 5px 5px;'
echo '    position: absolute;'
echo '    z-index: 1;'
# This for right tooltips
#echo '    top: -5px;'
#echo '    left: 120%;'
# This for top tooltips
echo '    bottom: 100%;'
echo '    left: 50%; '
echo '    margin-left: -160px; /* Use half of the width (120/2 = 60), to center the tooltip */'
echo '}'

echo '.tooltip .tooltiptext::after {'
echo '    content: "";'
echo '    position: absolute;'
# This for right tooltips
#echo '    top: 50%;'
#echo '    right: 100%; /* To the left of the tooltip */'
#echo '    margin-top: -5px;'
# This for top tooltips
echo '    top: 100%; /* At the bottom of the tooltip */'
echo '    left: 50%;'
echo '    margin-left: -5px;'
echo '    border-width: 5px;'
echo '    border-style: solid;'
#echo '    border-color: transparent black transparent transparent;'
echo '    border-color: black transparent transparent transparent;'
echo '}'
echo '.tooltip:hover .tooltiptext {'
echo '    visibility: visible;'
echo '}'

# Needed to display blocks in big list of extract options properly without cutting in the middle
echo 'label {'
echo '  display: inline-block;'
echo '}'

echo 'input:hover[type="submit"] {background: green;}'
echo 'input:hover[type="reset"] {background: red;}'
echo 'button:hover {background: blue;}'

echo 'input[type="radio"]{'
echo '  -webkit-appearance: none;'
echo '  -moz-appearance: none;'
echo '  appearance: none;'
echo
echo '  border-radius: 50%;'
echo '  width: 16px;'
echo '  height: 16px;'
echo
echo '  border: 2px solid #999;'
echo '  transition: 0.2s all linear;'
echo '  outline: none;'
#echo '  margin-right: 1px;'
echo '  margin-left: 10px;'
echo
echo '  position: relative;'
echo '  top: 4px;'
echo '}'
echo
echo 'input:checked {'
echo '  border: 6px solid grey;'
echo '}'

# For the boxes... as it doesn't work for radio
#echo 'input:hover {'
#echo '  color: red;'
#echo '}'

echo '#banner {'
echo '  padding: 10px;'
echo '  color: rgb(80,80,80);'
echo '  text-align: center;'
echo '  text-decoration: underline;'
#echo '  text-decoration-color: red;'
echo '  letter-spacing: 5px;'
echo '  text-shadow: 1px 1px gray;'
echo '  font-size: 40px;'
echo '}'
echo
echo '#header {'
echo '  background-image: repeating-linear-gradient(-45deg, rgba(255,255,255,0.15), rgba(255,255,255,0.15) 15px, transparent 15px, transparent 25px), linear-gradient(to top ,'
echo '  rgba(230,230,230,0.2), rgba(120,120,120,0.5));'
echo '  height: 80px;'
echo '  border-radius: 10px 10px 10px 10px;'
echo '}'

echo 'a:hover {color: red;}'
echo 'body, h1,h2,h3,h4,h5,h6 {font-family: "Ubuntu", sans-serif}'
echo 'fieldset{border-radius: 10px; border:2px solid #bbb;margin:0 2px;padding:.35em .625em .75em}'
echo 'hr {border:0;border-top:5px solid #eee;margin:20px 0}'
echo '.w3-row-padding img {margin-bottom: 12px}'
echo '/* Set the width of the sidebar to 120px */'
echo '.w3-sidebar {width: 120px;background: #bbb;}'
echo '/* Add a left margin to the "page content" that matches the width of the sidebar (120px) */'
echo '#main {margin-left: 120px}'
echo '/* Remove margins from "page content" on small screens */'
echo '@media only screen and (max-width: 600px) {#main {margin-left: 0}}'

echo "select {"
#echo "    width: 10%;"
#echo "    padding: 10px 10px;"
echo "    border: none;"
echo "    border-radius: 4px;"
echo "    background-color: #ffffff;"
echo "}"
echo "button, input[type=button], input[type=reset] {"
echo "    background-color: #616161;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=submit] {"
echo "    background-color: DarkGreen;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=text], input[type=number],select {"
#echo "    width: 100%;"
echo "    padding: 5px 10px;"
#echo "    margin: 8px 0;"
echo "    display: inline-block;"
echo "    border: 1px solid #ccc;"
echo "    border-radius: 4px;"
echo "    box-sizing: border-box;"
echo "}"

# Using local fonts is faster and more reliable
#echo 'pre {font-family: 'DejaVu Sans Mono'}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Regular.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo 'pre {font-family: "Local Dejavu", "Local Liberation", monospace; font-size: 14px}'

echo '* {box-sizing: border-box;}'

echo '/* Button used to open the chat form - fixed at the bottom of the page */'
echo '.open-button {'
echo '  background-color: #555;'
echo '  color: white;'
echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  opacity: 0.8;'
echo '  position: fixed;'
echo '  bottom: 23px;'
echo '  right: 28px;'
echo '  width: 280px;'
echo '}'

echo '/* The popup sequences window - hidden by default */'
echo '.sequences-popup {'
echo '  display: none;'
echo '  position: fixed;'
echo '  bottom: 23px;'
echo '  right: 28px;'
echo '  border: 3px solid #f1f1f1;'
echo '  z-index: 9;'
echo '}'

echo '/* Add styles to the form container */'
echo '.form-container {'
#echo '  max-width: 550px;'
#echo '  padding: 10px;'
echo '  padding: 16px 20px;'
echo '  background-color: white;'
echo '}'

echo '/* Full-width textarea */'
echo '.form-container textarea {'
echo '  width: 100%;'
echo '  padding: 15px;'
echo '  margin: 5px 0 22px 0;'
echo '  border: none;'
echo '  background: #f1f1f1;'
echo '  resize: none;'
echo '  min-height: 200px;'
echo '}'

echo '/* When the textarea gets focus, do something */'
echo '.form-container textarea:focus {'
echo '  background-color: #ddd;'
echo '  outline: none;'
echo '}'

echo '/* Set a style for the submit/send button */'
echo '.form-container .btn {'
echo '  background-color: #4CAF50;'
echo '  color: white;'
echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  width: 100%;'
echo '  margin-bottom:10px;'
echo '  opacity: 0.8;'
echo '}'

echo '/* Add a red background color to the cancel button */'
echo '.form-container .cancel {'
echo '  background-color: red;'
echo '}'

echo '/* Add some hover effects to buttons */'
echo '.form-container .btn:hover, .open-button:hover {'
echo '  opacity: 1;'
echo '}'

echo '/* Button used to open the characters picker - fixed at the bottom of the page */'
echo '.open-button-chars {'
echo '  background-color: #555;'
echo '  color: white;'
#echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  opacity: 0.8;'
#echo '  position: fixed;'
#echo '  bottom: 23px;'
#echo '  right: 28px;'
#echo '  width: 280px;'
echo '}'

echo '/* The characters picker window - hidden by default */'
echo '.characters-popup {'
echo '  display: none;'
echo '  position: fixed;'
echo '  bottom: 23px;'
echo '  right: 28px;'
echo '  border: 3px solid #f1f1f1;'
echo '  z-index: 9;'
echo '}'

# Break names in the table, as they can be really long
echo '.characters-popup td {'
echo '  word-break: break-all;'
echo '}'

echo '/* Add styles to the form container */'
echo '.form-container-chars {'
echo '  max-width: 550px;'
echo '  padding: 10px;'
echo '  padding: 16px 20px;'
echo '  background-color: white;'
echo '}'

echo '/* Full-width textarea */'
echo '.form-container-chars textarea {'
echo '  width: 100%;'
echo '  padding: 15px;'
echo '  margin: 5px 0 22px 0;'
echo '  border: none;'
echo '  background: #f1f1f1;'
echo '  resize: none;'
echo '  min-height: 200px;'
echo '}'

echo '/* When the textarea gets focus, do something */'
echo '.form-container-chars textarea:focus {'
echo '  background-color: #ddd;'
echo '  outline: none;'
echo '}'

echo '/* Set a style for the submit/send button */'
echo '.form-container-chars .btn {'
echo '  background-color: #4CAF50;'
echo '  color: white;'
echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  width: 100%;'
echo '  margin-bottom:10px;'
echo '  opacity: 0.8;'
echo '}'

echo '/* Add a red background color to the cancel button */'
echo '.form-container-chars .cancel {'
echo '  background-color: red;'
echo '}'

echo '/* Add some hover effects to buttons */'
echo '.form-container-chars .btn:hover, .open-button-chars:hover {'
echo '  opacity: 1;'
echo '}'

echo '</style>'
echo '</head>'
echo '<body class="w3-light-grey">'

echo '<!-- Icon Bar (Sidebar) -->'
echo '<nav class="w3-sidebar w3-bar-block w3-small w3-center">'
echo '  <!-- Avatar image in top left corner -->'
echo '  <img src="../webicons/icon.svg" alt="ADAPTABLE logo" style="width:100%">'
echo '  <a href="../index.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-home w3-xxlarge"></i>'
echo '    <p>HOME</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./adaptable-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-cogs w3-xxlarge"></i>'
echo '    <p>FAMILY GENERATOR</p>'
echo '  </a>'
echo '  <a href="./extract-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-search-plus w3-xxlarge"></i>'
echo '    <p>FAMILY ANALYZER</p>'
echo '  </a>'
echo '  <a href="./index-results.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-download w3-xxlarge"></i>'
echo '    <p>DOWNLOAD RESULTS</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./index-browse.sh" class="w3-bar-item w3-button w3-padding-large w3-light-grey">'
echo '    <i class="fas fa-list-ul w3-xxlarge"></i>'
echo '    <p>BROWSE AMPs & FAMILIES</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="../faq.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-question-circle w3-xxlarge"></i>'
echo '    <p>FAQ & TUTORIAL</p>'
echo '  </a>'
echo '  <a href="../about.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-users w3-xxlarge"></i>'
echo '    <p>ABOUT</p>'
echo '  </a>'
echo '</nav>'

echo '<div class="w3-padding-large" id="main">'

echo '<div id="header"> '
echo '        <div id="banner">'
echo '        ADAPTABLE'
echo '        </div>'
echo '</div>'

echo \
	"<article>"\
	"<header>"\
	"<h1>Family overview</h1>"\
	"<h6>"\
	"Each family contains sequence-related peptides which tend to have similar activity, structure, and mechanism of action. "\
	"</h6>"\
	"</header>"\
	"</article>"
echo "<hr>"

source ./tooltips.sh

fmax=$(cat ADAPTABLE/Results/all_families/.fam_num)

if [ -z "$QUERY_STRING" ]; then
#        echo "<form style='display: inline-block;' method=GET>"
#        echo "<div class="tooltip">Family number&nbsp;</div><input type="number" name="_fnum" min="1" max="${fmax}" required>"
#        echo "<input type="submit" value='Submit'>"
#        echo "<input type="reset" value="Reset">"
#        echo "</form>"

        echo "<form style='display: inline-block;' method=GET>"
        echo "<div class="tooltip"><h6 style='display: inline-block;'>Peptide sequence / Name of the father:</h6><span class="tooltiptext">"${tooltiptext_peptide_sequence}"</span></div><input id="myinput" type="text" name="_peptide_sequence" size=70 placeholder='Search peptide by sequence or name' pattern='[A-Za-zԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]+' required>"
        echo "<input type="submit" value='Submit'>"
        echo "<input type="reset" value="Reset">"
        echo "</form>"
        echo '</div>'
        echo '<script>'
        echo 'var input = document.getElementById("myinput");'

        echo '// Show label but insert value into the input:'
        echo 'new Awesomplete(input, {'
        echo '  maxItems: 20,'
        echo '  list: ['
        zcat ../DATABASE.seqs-all_families.js.awesomplete.gz
        echo '  ]'
        echo '});'
        echo '</script>'

        exit 0
fi

if [ "$QUERY_STRING" ]; then

# Shorten addressbar
echo "<script>"
echo 'function shorten_addressbar_url() {'
echo 'var url = window.location.href;'
echo "var new_url = url.split('?')[0];"
echo "history.replaceState('1', '', new_url);"
echo '}'

echo 'shorten_addressbar_url();'
echo "</script>"

	export __fnum=$(echo "$QUERY_STRING" | sed -n "s/^.*fnum=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
	export __user_key=$(echo "$QUERY_STRING" | sed -n "s/^.*user_key=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
	export __calculation_label=$(echo "$QUERY_STRING" | sed -n "s/^.*calculation_label=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
	
	export __peptide_sequence=$(echo "$QUERY_STRING" | sed -n "s/^.*peptide_sequence=\([^&]*\).*$/\1/p" | sed "s/%20/ /g")
        export utf8_seq=$(echo -e $(echo "${__peptide_sequence}" | sed -e 's/%/.0x/g'| sed -e 's/0x\(..\)\.\?/\\x\1/g')|sed -e 's/\.//g')
        export __peptide_sequence="${utf8_seq}"

	if [ "$__peptide_sequence" ]; then
		in_all_families=$(set -o pipefail && grep -w -a ${__peptide_sequence} ADAPTABLE/Results/all_families/.family_fathers|cut -d: -f1)

	        if [[ ${in_all_families} ]]; then
#			echo "<meta http-equiv='refresh' content='0;url=./all_familiesDB.sh?_fnum=${in_all_families/f/}'/>"
			echo "<script>window.setTimeout(function(){window.location.replace('./all_familiesDB.sh?_fnum=${in_all_families/f/}')},0);</script>"
		else
	                echo '<div class="container-alert"><div class="alert alert_danger"><div class="alert--icon"><i class="fas fa-times-circle"></i></div><div class="alert--content">'
	                echo "<h4>Supplied peptide does not generate any family, please try a different one</h4>"
        	        echo '</div></div></div>'
			echo "<script>window.setTimeout(function(){window.location.replace('./index-browse.sh')},2000);</script>"
		fi

	else

	# Update title of the window/tab
	echo "<script>"
	echo "document.title = 'Family ${__fnum}';"
	echo "</script>"

	if [[ ${__user_key} && ${__calculation_label} ]]; then
		export __fam_path="${__user_key}/${__calculation_label}"
	else
		export __fam_path="all_families"
	
#	        echo "<form style='display: inline-block;' method=GET>"
#		echo "<div class="tooltip">Family number&nbsp;</div><input type="number" value="${__fnum}" name="_fnum" size=20 min="1" max="${fmax}" required>"
#	        echo "<input type="submit" value='Submit'>"
	        #echo "<input type="reset" value="Reset">"
#	        echo "</form>"
	        
        echo "<form style='display: inline-block;' method=GET>"
        echo "<div style='display: inline-block;' class="tooltip"><h6 style='display: inline-block;'>Peptide sequence / Name of the father:</h6><span class="tooltiptext">"${tooltiptext_peptide_sequence}"</span></div><input id="myinput" type="text" name="_peptide_sequence" size=70 placeholder='Search peptide by sequence or name' pattern='[A-Za-zԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]+' required>"
        echo "<input type="submit" value='Submit'>"
        echo "<input type="reset" value="Reset">"
        echo "</form>"
        echo '<script>'
        echo 'var input = document.getElementById("myinput");'

        echo '// Show label but insert value into the input:'
        echo 'new Awesomplete(input, {'
        echo '  maxItems: 20,'
        echo '  list: ['
        zcat ../DATABASE.seqs-all_families.js.awesomplete.gz
        echo '  ]'
        echo '});'
        echo '</script>'

	echo '<button class="open-button-chars" onclick="openForm2()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'
        
fi
        
# Character selector
echo '<script>'
echo 'function openForm2() {'
echo '    document.getElementById("myForm2").style.display = "inline-block";'
echo '}'
echo 'function closeForm2() {'
echo '    document.getElementById("myForm2").style.display = "none";'
echo '}'
echo '</script>'

echo '<div class="characters-popup" id="myForm2">'
echo '  <form class="form-container-chars">'
echo '    <h3>Special characters for modified aminoacids</h3>'
echo "<i>You can click on the symbol to select it and use Ctrl+F to search your desired aminoacid</i>"
echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
cat ../character-selector | sed -e 's/input-text/myinput/g'
echo '</div>'
echo '    <button type="button" class="btn cancel" onclick="closeForm2()"><i class="fas fa-signature"></i></button>'
echo '  </form>'
echo '</div>'
##
	source ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.bash
	
        echo '<div class="w3-container">'

        [[ ${__user_key} && ${__calculation_label} ]] && echo '<button style="float:right;" class="open-button-chars" onclick="openForm2()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'

        echo \
        "<ul>"\
        "<li><a href="#properties">Properties of the family</a></li>"\
        "<li><a href="#members">Members of the family</a></li>"\
        "</ul>"

	echo "<hr>"

        if [[ -f ADAPTABLE/Results/${__fam_path}/.prediction-prefs ]]; then
        	echo "  <h2 id="properties">Properties of the family of peptides similar to $(tail -n1 ADAPTABLE/Results/${__fam_path}/.prediction-prefs)</h2>"
        	echo "<h6>You can visit <a href='./index-results.sh?_user_key=${__user_key}' target="_blank">'DOWNLOAD RESULTS'</a> section to download all the data.</h6>"
        else

		echo "  <h2 id="properties">Family ${__fnum} properties</h2>"
		echo "  <h3>Family of peptides similar to $(head -n2 ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.seqs|sed -e 's/-//g')</h3>"
        fi
        
	echo "<ul><li><a href="#members">Members of the family</a></li></ul>"
	echo "<br>"

        echo '  <table style="table-layout: fixed;" class="w3-table-all w3-hoverable w3-card-4">'
        echo '    <tr>'
        echo '      <th>Property</th>'
        echo '     <th style="width: 70%;">Value (% of members that could have it depending on available data per member)</th>'
        echo '    </tr>'

	[[ "${!anti*}" || "${!biofilm*}" ]] && export activity_label="<b><u><i>Activity:</i></u></b>"
	[[ "${!hemo*}" || "${!toxic*}" || "${!citotoxic*}" ]] && export toxicity_label="<b><u><i>Toxicity:</i></u></b>"
	[[ "${!PTM*}" || "${!N_terminus*}" || "${!C_terminus*}" || "${!cyclic*}" || "${!stereo*}" ]] && export chemical_characteristics_label="<b><u><i>Chemical characteristics:</i></u></b>"
	[[ "${!RBC_source*}" || "${!source*}" || "${!name*}" || "${!Family*}" || "${!taxonomy*}" ]] && export source_label="<b><u><i>Source:</i></u></b>"
	[[ "${!gene*}" || "${!target*}" || "${!synthetic*}" || "${!cell_line*}" || "${!tissue*}" || "${!cancer_type*}" || "${!virus_name*}" || "${!drug_delivery*}" || "${!cell_penetrating*}" || "${!PMID*}" || "${!all_organisms*}" || "${!solubility*}" || "${!experimental*}" || "${!ribosomal*}" ]] && export others_label="<b><u><i>Others:</i></u></b>"

	# List in the order desired for the table
	for x in {\
activity_label,\
antimicrobial,antibacterial,antigram_pos,antigram_neg,biofilm,antifungal,antiyeast,antiviral,virus_name,antiprotozoal,antiparasitic,anticancer,anticancer_activity,anticancer_activity_test,cell_line,tissue,cancer_type,\
toxicity_label,\
toxic,cytotoxic,hemolytic,hemolytic_activity,hemolytic_activity_test,\
source_label,\
name,source,RBC_source,taxonomy,Family,\
chemical_characteristics_label,\
stereo,N_terminus,C_terminus,PTM,cyclic,\
others_label,\
gene,target,experimental,ribosomal,synthetic,drug_delivery,cell_penetrating,PMID,all_organisms,solubility};do
		# Black magic to get value of the var ${!x}, like eval echo \$${i}
		if [ "${!x}" ]; then
			echo '<tr>'
			if [[ ${x} =~ ^(activity_label|toxicity_label|source_label|chemical_characteristics_label|others_label)$ ]]; then
				echo "<td style='background-color: LightGrey;'>"
			else
				echo "<td>"
			fi
			# Remove extra decimals with sed -r, rename 'all_organisms' to something more understandable. Make the first letter of a phase capital.
			echo "$(echo "${!x}"|sed -re 's/([0-9]+\.[0-9]{2})[0-9]+/\1/g' -e 's/all_organisms/organisms_keywords/' -e 's/cell_line/ - cancer_cell_line/' -e 's/tissue/ - cancer_tissue/' -e 's/cancer_type/ - cancer_type/' -e 's/virus_name/ - virus_name/' -e 's/activity:/activity (µM):/' | sed 's/\w/\u&/' )</td>"

			if [[ ${x} =~ ^(activity_label|toxicity_label|source_label|chemical_characteristics_label|others_label)$ ]]; then
				echo "<td style='background-color: LightGrey;'>"
			else
				echo "<td>"
			fi
			# Remove ending ; and extra decimals with sed -r, use proper chars for micromolar, use capital for Stereo.
			# Drop extra 'biofilm' value as it's not needed here
			if [[ ${x} =~ ^biofilm$ ]]; then
				echo "$(eval echo \$${x}_value|sed -e 's/[;]$//' -re 's/([0-9]+\.[0-9]{2})[0-9]+/\1/g' -e 's/_um/_μm/' -e 's/\<l\>/L/g' -e 's/\<r\>/R/g' -e 's/biofilm//g')</td>"
			else
				echo "$(eval echo \$${x}_value|sed -e 's/[;]$//' -re 's/([0-9]+\.[0-9]{2})[0-9]+/\1/g' -e 's/_um/_μm/' -e 's/\<l\>/L/g' -e 's/\<r\>/R/g')</td>"
			fi
			echo '</tr>'
		fi
	done

	echo "</table>"

        if [[ -f ADAPTABLE/Results/${__fam_path}/.prediction-prefs ]]; then
                echo "  <h2 id="members">Members of the family of peptides similar to $(tail -n1 ADAPTABLE/Results/${__fam_path}/.prediction-prefs)</h2>"
        else
                echo "  <h2 id="members">Members of family ${__fnum}</h2>"
        fi

	echo "<ul><li><a href="#properties">Properties of the family</a></li></ul>"
	echo "<br>"

#	echo '<div style="overflow-x:auto;">'
	echo '<div id="table2">'
        echo '  <table class="w3-table-all w3-hoverable w3-card-4">'
        echo '    <tr>'
	echo '	   <th>Num</th>'
        echo '     <th>Sequence <sub>(Click each for more information)</sub></th>'
        echo '     <th>ID</th>'
        echo '    </tr>'
	
# This is faster than loading the file later, but we need to do in the other way to not fill the execve limit will all the variables exported before using them
# I tried to use the while read -r line; do below (at the step of generating the page) + done <ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.seqs but it is too slow
# hence, I try to split the while close to the limit
#	seq_num=0 # Start by 0 as we have an empty line at the start
#	while read -r line; do
#		export "seq$seq_num"="$line"
#		seq_num=$((seq_num+1))
#	done <ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.seqs

        seq_num=0 # Start by 0 as we have an empty line at the start
	while read -r line; do
		# More than 6500 hits the execve limit
		if [ $seq_num -lt "6500" ]; then
	                export "seq$seq_num"="$line"
        	        seq_num=$((seq_num+1))
                fi
        done <ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.seqs

        seq_num=0 # Start by 0 as we have an empty line at the start
        while read -r line; do
        	if [ $seq_num -lt "6500" ]; then
	                export "ID$seq_num"="$line"
        	        seq_num=$((seq_num+1))
		fi
        done <ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.IDs
			
	count=0 # Start by 0 as we have an empty line at the start
#       while [ $count -lt ${fmax} ]; do
	while [ -v seq$count ]; do
#	while read -r line; do
		if [ ${count} != 0 ]; then
#			export seq$count=$line
			echo '    <tr>'
			echo "		<td>${count}</td>"
			if [[ ${count} == 1 && ${ID1} = *"My_Peptide"* ]]; then
				echo "<td><pre style='font-size:0.9em;'><b>$(eval "echo \$seq"${count}""|cut -d';' -f2)</b></pre></td>"
			else
				echo "<td><pre style='font-size:0.9em;'><a style='text-decoration: none' href='./peptideDB.sh?_peptide_sequence=$(eval "echo \$seq"${count}""|sed 's/\-//g'|cut -d';' -f2)' target="_blank">$(eval "echo \$seq"${count}""|cut -d';' -f2)</a></pre></td>"
			fi
#			unset seq$count

			id=$(eval "echo \$ID"${count}"")
			echo "<td>"
			for k in ${id//;/ }; do
				[[ ${k} = *"My_Peptide"* ]] && echo "<b>My Peptide</b>"
				[[ ${k} = *"satpdb"* ]] && echo "<a href='http://crdd.osdd.net/raghava/satpdb/display_seq.php?details=${k}' target="_blank">${k}</a>"
				[[ ${k} = *"hemo"* ]] && echo "<a href='http://crdd.osdd.net/raghava/hemolytik/display.php?details=${k/hemo/}' target="_blank">${k}</a>"
				[[ ${k} = *"DRAMP"* ]] && echo "<a href='http://dramp.cpu-bioinfor.org/browse/All_Information.php?id=${k}' target="_blank">${k}</a>"
				[[ ${k} = *"canPPD"* ]] && echo "<a href='http://crdd.osdd.net/raghava/cancerppd/display_sub.php?details=${k/canPPD}' target="_blank">${k}</a>"
				[[ ${k} = *"PHYT"* ]] && echo "<a href='http://phytamp.hammamilab.org/${k}' target="_blank">${k}</a>"
				[[ ${k} = *"CAMPSQ"* ]] && echo "<a href='http://www.camp.bicnirrh.res.in/seqDisp.php?id=${k}' target="_blank">${k}</a>"
				[[ ${k} = *"HIP"* ]] && echo "<a href='http://crdd.osdd.net/servers/hipdb/record.php?details=${k}' target="_blank">${k}</a>"
				[[ ${k} = *"DBAASP"* ]] && echo "<a href='https://dbaasp.org/peptide-card?id=${k/DBAASP}' target="_blank">${k}</a>"
				[[ ${k} = *"APD_"* ]] && echo "<a href='http://aps.unmc.edu/AP/database/query_output.php?ID=${k/APD_AP}' target="_blank">${k}</a>"
				[[ ${k} = *"InverPep"* ]] && echo "<a href='http://ciencias.medellin.unal.edu.co/gruposdeinvestigacion/prospeccionydisenobiomoleculas/InverPep/public/Peptido/see/${k/InverPep}' target="_blank">${k}</a>"
				[[ ${k} = *"ParaPep"* ]] && echo "<a href='http://crdd.osdd.net/raghava/parapep/display_sub.php?details=${k/ParaPep}' target="_blank">${k}</a>"
				[[ ${k} = *"ANTISTAPHY"* ]] && echo "${k}" # Upstream dead
				[[ ${k} = *"DADP_"* ]] && echo "<a href='http://split4.pmfst.hr/dadp/?a=kartica&id=SP_${k/DADP_}' target="_blank">${k}</a>"
				[[ ${k} = *"MilkAMP"* ]] && echo "<a href='http://milkampdb.org/${k/MilkAMP}' target="_blank">${k}</a>"
				[[ ${k} = *"BACTIBASE_"* ]] && echo "<a href='http://bactibase.hammamilab.org/${k/BACTIBASE_}' target="_blank">${k}</a>"				
				[[ ${k} = *"BAAMPS"* ]] && echo "<a href='http://www.baamps.it/peptidelist?task=peptide.display&ID=${k/BAAMPS}' target="_blank">${k}</a>"
				[[ ${k} = *"Defe"* ]] && echo "<a href='http://defensins.bii.a-star.edu.sg/pops/pop_proteinDetails.php?id=${k/Defe}' target="_blank">${k}</a>"
				[[ ${k} = *"Conoserver"* ]] && echo "<a href='http://www.conoserver.org/index.php?page=card&table=protein&id=${k/ConoserverP0}' target="_blank">${k}</a>"
				[[ ${k} = "AVP"* ]] && echo "<a href='http://crdd.osdd.net/servers/avpdb/record.php?details=${k}' target="_blank">${k}</a>" # AVP 
				[[ ${k} = *"MAVP"* ]] && echo "<a href='http://crdd.osdd.net/servers/avpdb/record2.php?details=${k}' target="_blank">${k}</a>" # MAVP
				[[ ${k} = *"LAMP"* ]] && echo "<a href='http://biotechlab.fudan.edu.cn/database/lamp/detail.php?id=${k/LAMP}' target="_blank">${k}</a>"
				[[ ${k} = "ADAM"* ]] && echo "<a href='http://bioinformatics.cs.ntou.edu.tw/ADAM/adam_info.php?f=${k}' target="_blank">${k}</a>" # don't conflict with YADAMP
				[[ ${k} = *"YADAMP"* ]] && echo "<a href='http://yadamp.unisa.it/showItem.aspx?yadampid=1${k/YADAMP}' target="_blank">${k}</a>"
				[[ ${k} = *"Peptaibol"* ]] && echo "<a href='http://peptaibol.cryst.bbk.ac.uk/singlerecords/${k/Peptaibol}.html' target="_blank">${k}</a>"
				[[ ${k} = *"CPP"* ]] && echo "<a href='http://crdd.osdd.net/raghava/cppsite/display.php?details=${k/CPP}' target="_blank">${k}</a>"
				[[ ${k} = *"uniprot"* ]] && echo "<a href='https://www.uniprot.org/uniprot/${k/uniprot}' target="_blank">${k}</a>"
			done
			echo "</td>"
			echo '	  </tr>'
		fi
			
		count=$((count+1))
	done

	# For more than 6500 sequences

	# Clean vars
        seq_num=0 # Start by 0 as we have an empty line at the start
	while [ -v seq$seq_num ]; do
                unset "seq$seq_num"
                seq_num=$((seq_num+1))
        done

        seq_num=0 # Start by 0 as we have an empty line at the start
	while [ -v ID$seq_num ]; do
                unset "ID$seq_num"
                seq_num=$((seq_num+1))
        done
	# Cleaning done
	
        seq_num=0 # Start by 0 as we have an empty line at the start
        while read -r line; do
		# More than 6500 hits the execve limit
                if [ $seq_num -ge "6500" ]; then
                        export "seq$seq_num"="$line"
                fi
                seq_num=$((seq_num+1))
        done <ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.seqs

        seq_num=0 # Start by 0 as we have an empty line at the start
        while read -r line; do
                if [ $seq_num -ge "6500" ]; then
                        export "ID$seq_num"="$line"
                fi
		seq_num=$((seq_num+1))
        done <ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.IDs
                        
        count=6500 # Start by 0 as we have an empty line at the start
        while [ -v seq$count ]; do
                if [ ${count} != 0 ]; then
                        echo '    <tr>'
                        echo "          <td>${count}</td>"
                        if [[ ${count} == 1 && ${ID1} = *"My_Peptide"* ]]; then
                                echo "<td><pre style='font-size:0.9em;'><b>$(eval "echo \$seq"${count}""|cut -d';' -f2)</b></pre></td>"
                        else
                                echo "<td><pre style='font-size:0.9em;'><a style='text-decoration: none' href='./peptideDB.sh?_peptide_sequence=$(eval "echo \$seq"${count}""|sed 's/\-//g'|cut -d';' -f2)' target="_blank">$(eval "echo \$seq"${count}""|cut -d';' -f2)</a></pre></td>"
                        fi

                        id=$(eval "echo \$ID"${count}"")
                        echo "<td>"
                        for k in ${id//;/ }; do
                                [[ ${k} = *"My_Peptide"* ]] && echo "<b>My Peptide</b>"
                                [[ ${k} = *"satpdb"* ]] && echo "<a href='http://crdd.osdd.net/raghava/satpdb/display_seq.php?details=${k}' target="_blank">${k}</a>"
                                [[ ${k} = *"hemo"* ]] && echo "<a href='http://crdd.osdd.net/raghava/hemolytik/display.php?details=${k/hemo/}' target="_blank">${k}</a>"
                                [[ ${k} = *"DRAMP"* ]] && echo "<a href='http://dramp.cpu-bioinfor.org/browse/All_Information.php?id=${k}' target="_blank">${k}</a>"
                                [[ ${k} = *"canPPD"* ]] && echo "<a href='http://crdd.osdd.net/raghava/cancerppd/display_sub.php?details=${k/canPPD}' target="_blank">${k}</a>"
                                [[ ${k} = *"PHYT"* ]] && echo "<a href='http://phytamp.hammamilab.org/${k}' target="_blank">${k}</a>"
                                [[ ${k} = *"CAMPSQ"* ]] && echo "<a href='http://www.camp.bicnirrh.res.in/seqDisp.php?id=${k}' target="_blank">${k}</a>"
                                [[ ${k} = *"HIP"* ]] && echo "<a href='http://crdd.osdd.net/servers/hipdb/record.php?details=${k}' target="_blank">${k}</a>"
				[[ ${k} = *"DBAASP"* ]] && echo "<a href='https://dbaasp.org/peptide-card?id=${k/DBAASP}' target="_blank">${k}</a>"
                                [[ ${k} = *"APD_"* ]] && echo "<a href='http://aps.unmc.edu/AP/database/query_output.php?ID=${k/APD_AP}' target="_blank">${k}</a>"
                                [[ ${k} = *"InverPep"* ]] && echo "<a href='http://ciencias.medellin.unal.edu.co/gruposdeinvestigacion/prospeccionydisenobiomoleculas/InverPep/public/Peptido/see/${k/InverPep}' target="_blank">${k}</a>"
                                [[ ${k} = *"ParaPep"* ]] && echo "<a href='http://crdd.osdd.net/raghava/parapep/display_sub.php?details=${k/ParaPep}' target="_blank">${k}</a>"
                                [[ ${k} = *"ANTISTAPHY"* ]] && echo "${k}" # Upstream dead
                                [[ ${k} = *"DADP_"* ]] && echo "<a href='http://split4.pmfst.hr/dadp/?a=kartica&id=SP_${k/DADP_}' target="_blank">${k}</a>"
                                [[ ${k} = *"MilkAMP"* ]] && echo "<a href='http://milkampdb.org/${k/MilkAMP}' target="_blank">${k}</a>"
                                [[ ${k} = *"BACTIBASE_"* ]] && echo "<a href='http://bactibase.hammamilab.org/${k/BACTIBASE_}' target="_blank">${k}</a>"
                                [[ ${k} = *"BAAMPS"* ]] && echo "<a href='http://www.baamps.it/peptidelist?task=peptide.display&ID=${k/BAAMPS}' target="_blank">${k}</a>"
                                [[ ${k} = *"Defe"* ]] && echo "<a href='http://defensins.bii.a-star.edu.sg/pops/pop_proteinDetails.php?id=${k/Defe}' target="_blank">${k}</a>"
                                [[ ${k} = *"Conoserver"* ]] && echo "<a href='http://www.conoserver.org/index.php?page=card&table=protein&id=${k/ConoserverP0}' target="_blank">${k}</a>"
                                [[ ${k} = "AVP"* ]] && echo "<a href='http://crdd.osdd.net/servers/avpdb/record.php?details=${k}' target="_blank">${k}</a>" # AVP 
				[[ ${k} = *"MAVP"* ]] && echo "<a href='http://crdd.osdd.net/servers/avpdb/record2.php?details=${k}' target="_blank">${k}</a>" # MAVP
                                [[ ${k} = *"LAMP"* ]] && echo "<a href='http://biotechlab.fudan.edu.cn/database/lamp/detail.php?id=${k/LAMP}' target="_blank">${k}</a>"
                                # Use faster IP
#                                [[ ${k} = "ADAM"* ]] && echo "<a href='http://bioinformatics.cs.ntou.edu.tw/ADAM/adam_info.php?f=${k}' target="_blank">${k}</a>" # don't conflict with YADAMP
				[[ ${k} = "ADAM"* ]] && echo "<a href='http://140.121.197.185/ADAM/adam_info.php?f=${k}' target="_blank">${k}</a>" # don't conflict with YADAMP
                                [[ ${k} = *"YADAMP"* ]] && echo "<a href='http://yadamp.unisa.it/showItem.aspx?yadampid=1${k/YADAMP}' target="_blank">${k}</a>"
                                [[ ${k} = *"Peptaibol"* ]] && echo "<a href='http://peptaibol.cryst.bbk.ac.uk/singlerecords/${k/Peptaibol}.html' target="_blank">${k}</a>"
                                [[ ${k} = *"CPP"* ]] && echo "<a href='http://crdd.osdd.net/raghava/cppsite/display.php?details=${k/CPP}' target="_blank">${k}</a>"
                                [[ ${k} = *"uniprot"* ]] && echo "<a href='https://www.uniprot.org/uniprot/${k/uniprot}' target="_blank">${k}</a>"
                        done
                        echo "</td>"
                        echo '    </tr>'
                fi

                count=$((count+1))
        done

	echo '  </table>'
	echo '</div>'
	echo '</div>'

# Clean vars
#        seq_num=0 # Start by 0 as we have an empty line at the start
#        while read -r line; do
#                unset "seq$seq_num"
#                seq_num=$((seq_num+1))
#        done <ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.seqs

#        seq_num=0 # Start by 0 as we have an empty line at the start
#        while read -r line; do
#                unset "ID$seq_num"
#                seq_num=$((seq_num+1))
#        done <ADAPTABLE/Results/${__fam_path}/.outputs/f${__fnum}.IDs

echo '<script>'
echo 'function openForm() {'
echo '    document.getElementById("myForm").style.display = "block";'
echo '}'

echo 'function closeForm() {'
echo '    document.getElementById("myForm").style.display = "none";'
echo '}'
echo '</script>'

echo '<div class="sequences-popup" id="myForm">'
echo '  <form class="form-container">'
echo '    <h1>Members</h1>'

echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
# Show only first 150 sequences or generated html is too big and web will be too heavy to load
./ADAPTABLE/extract_family_splitted.sh "${__fnum}"-"${__fnum}"-f "ADAPTABLE/Results/${__fam_path}/" | head -n 157 | cl-ansi2html -w -n | sed -e 's/white-space:pre-wrap/white-space:pre/'
echo '</div>'

echo '    <button type="button" class="btn cancel" onclick="closeForm()">Close</button>'
echo '  </form>'
echo '</div>'
echo '<button class="open-button" onclick="openForm()">Show frequency of aminoacids</button>'

fi

fi

echo '</div>'
echo '</body>'
echo '</html>'

exit 0
