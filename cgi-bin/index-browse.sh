#!/bin/bash

echo "Content-type: text/html"
echo ""

echo '<!DOCTYPE html>'
echo '<html lang="en">'
echo '<head>'
echo '<title>Browse...</title>'
echo '<meta name="viewport" content="width=1080">'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<link rel="shortcut icon" href="../favicon.ico" />'
echo '<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">'
echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu">'

echo '<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>'

# Using local fonts is faster and more reliable
#echo '<link rel="stylesheet" media="none" href="https://fontlibrary.org/face/dejavu-sans-mono" onload="this.media='all';" type="text/css"/>'

echo '<link href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" rel="stylesheet">'

# For autocompletion
echo '<!-- JS file -->'
echo '<script async src="../awesomplete/awesomplete.min.js"></script>'

echo '<!-- CSS file -->'
echo '<link rel="stylesheet" href="../awesomplete/awesomplete.css">'

echo '<style>'
echo '.tooltip {'
echo '    position: relative;'
echo '    display: inline-block;'
echo '    border-bottom: 0px dotted black;'
echo '}'

echo '.tooltip .tooltiptext {'
echo '   '
echo '    visibility: hidden;'
echo '    width: 320px;'
echo '    background-color: black;'
echo '    color: #fff;'
echo '    text-align: center;'
echo '    border-radius: 6px;'
echo '    padding: 5px 5px;'
echo '    position: absolute;'
echo '    z-index: 1;'
# This for right tooltips
#echo '    top: -5px;'
#echo '    left: 120%;'
# This for top tooltips
echo '    bottom: 100%;'
echo '    left: 50%; '
echo '    margin-left: -160px; /* Use half of the width (120/2 = 60), to center the tooltip */'
echo '}'

echo '.tooltip .tooltiptext::after {'
echo '    content: "";'
echo '    position: absolute;'
# This for right tooltips
#echo '    top: 50%;'
#echo '    right: 100%; /* To the left of the tooltip */'
#echo '    margin-top: -5px;'
# This for top tooltips
echo '    top: 100%; /* At the bottom of the tooltip */'
echo '    left: 50%;'
echo '    margin-left: -5px;'
echo '    border-width: 5px;'
echo '    border-style: solid;'
#echo '    border-color: transparent black transparent transparent;'
echo '    border-color: black transparent transparent transparent;'
echo '}'
echo '.tooltip:hover .tooltiptext {'
echo '    visibility: visible;'
echo '}'

# Needed to display blocks in big list of extract options properly without cutting in the middle
echo 'label {'
echo '  display: inline-block;'
echo '}'

echo 'input:hover[type="submit"] {background: green;}'
echo 'input:hover[type="reset"] {background: red;}'
echo 'button:hover {background: blue;}'

echo 'input[type="radio"]{'
echo '  -webkit-appearance: none;'
echo '  -moz-appearance: none;'
echo '  appearance: none;'
echo
echo '  border-radius: 50%;'
echo '  width: 16px;'
echo '  height: 16px;'
echo
echo '  border: 2px solid #999;'
echo '  transition: 0.2s all linear;'
echo '  outline: none;'
#echo '  margin-right: 1px;'
echo '  margin-left: 10px;'
echo
echo '  position: relative;'
echo '  top: 4px;'
echo '}'
echo
echo 'input:checked {'
echo '  border: 6px solid grey;'
echo '}'

# For the boxes... as it doesn't work for radio
#echo 'input:hover {'
#echo '  color: red;'
#echo '}'

echo '#banner {'
echo '  padding: 10px;'
echo '  color: rgb(80,80,80);'
echo '  text-align: center;'
echo '  text-decoration: underline;'
#echo '  text-decoration-color: red;'
echo '  letter-spacing: 5px;'
echo '  text-shadow: 1px 1px gray;'
echo '  font-size: 40px;'
echo '}'
echo
echo '#header {'
echo '  background-image: repeating-linear-gradient(-45deg, rgba(255,255,255,0.15), rgba(255,255,255,0.15) 15px, transparent 15px, transparent 25px), linear-gradient(to top ,'
echo '  rgba(230,230,230,0.2), rgba(120,120,120,0.5));'
echo '  height: 80px;'
echo '  border-radius: 10px 10px 10px 10px;'
echo '}'

echo 'a:hover {color: red;}'
echo 'body, h1,h2,h3,h4,h5,h6 {font-family: "Ubuntu", sans-serif}'
echo 'fieldset{border-radius: 10px; border:2px solid #bbb;margin:0 2px;padding:.35em .625em .75em}'
echo 'hr {border:0;border-top:5px solid #eee;margin:20px 0}'
echo '.w3-row-padding img {margin-bottom: 12px}'
echo '/* Set the width of the sidebar to 120px */'
echo '.w3-sidebar {width: 120px;background: #bbb;}'
echo '/* Add a left margin to the "page content" that matches the width of the sidebar (120px) */'
echo '#main {margin-left: 120px}'
echo '/* Remove margins from "page content" on small screens */'
echo '@media only screen and (max-width: 600px) {#main {margin-left: 0}}'

echo "select {"
#echo "    width: 10%;"
#echo "    padding: 10px 10px;"
echo "    border: none;"
echo "    border-radius: 4px;"
echo "    background-color: #ffffff;"
echo "}"
echo "button, input[type=button], input[type=reset] {"
echo "    background-color: #616161;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=submit] {"
echo "    background-color: DarkGreen;"
echo "    border: none;"
echo "    border-radius: 10px;"
echo "    color: white;"
echo "    padding: 10px 30px;"
echo "    text-decoration: none;"
echo "    margin: 4px 2px;"
echo "    cursor: pointer;"
echo "    -webkit-transition-duration: 0.4s; /* Safari */"
echo "    transition-duration: 0.4s;"
echo "}"
echo "input[type=text], input[type=number],select {"
#echo "    width: 100%;"
echo "    padding: 5px 10px;"
#echo "    margin: 8px 0;"
echo "    display: inline-block;"
echo "    border: 1px solid #ccc;"
echo "    border-radius: 4px;"
echo "    box-sizing: border-box;"
echo "}"

# Using local fonts is faster and more reliable
#echo 'pre {font-family: 'DejaVu Sans Mono'}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Dejavu";'
echo "    src:url('../dejavu/ttf/DejaVuSansMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Regular.ttf') format('truetype');"
echo '    font-weight: normal;'
echo '    font-style: normal;'
echo '}'
echo '@font-face {'
echo '    font-family: "Local Liberation";'
echo "    src:url('../liberation/LiberationMono-Bold.ttf') format('truetype');"
echo '    font-weight: bold;'
echo '    font-style: normal;'
echo '}'
echo 'pre {font-family: "Local Dejavu", "Local Liberation", monospace; font-size: 14px}'

echo '* {box-sizing: border-box;}'

echo '/* Button used to open the characters picker - fixed at the bottom of the page */'
echo '.open-button-chars {'
echo '  background-color: #555;'
echo '  color: white;'
#echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  opacity: 0.8;'
#echo '  position: fixed;'
#echo '  bottom: 23px;'
#echo '  right: 28px;'
#echo '  width: 280px;'
echo '}'

echo '/* The characters picker window - hidden by default */'
echo '.characters-popup {'
echo '  display: none;'
echo '  position: fixed;'
echo '  bottom: 23px;'
echo '  right: 28px;'
echo '  border: 3px solid #f1f1f1;'
echo '  z-index: 9;'
echo '}'

# Break names in the table, as they can be really long
echo '.characters-popup td {'
echo '  word-break: break-all;'
echo '}'

echo '/* Add styles to the form container */'
echo '.form-container-chars {'
echo '  max-width: 550px;'
echo '  padding: 10px;'
echo '  padding: 16px 20px;'
echo '  background-color: white;'
echo '}'

echo '/* Full-width textarea */'
echo '.form-container-chars textarea {'
echo '  width: 100%;'
echo '  padding: 15px;'
echo '  margin: 5px 0 22px 0;'
echo '  border: none;'
echo '  background: #f1f1f1;'
echo '  resize: none;'
echo '  min-height: 200px;'
echo '}'

echo '/* When the textarea gets focus, do something */'
echo '.form-container-chars textarea:focus {'
echo '  background-color: #ddd;'
echo '  outline: none;'
echo '}'

echo '/* Set a style for the submit/send button */'
echo '.form-container-chars .btn {'
echo '  background-color: #4CAF50;'
echo '  color: white;'
echo '  padding: 16px 20px;'
echo '  border: none;'
echo '  cursor: pointer;'
echo '  width: 100%;'
echo '  margin-bottom:10px;'
echo '  opacity: 0.8;'
echo '}'

echo '/* Add a red background color to the cancel button */'
echo '.form-container-chars .cancel {'
echo '  background-color: red;'
echo '}'

echo '/* Add some hover effects to buttons */'
echo '.form-container-chars .btn:hover, .open-button-chars:hover {'
echo '  opacity: 1;'
echo '}'

echo '.link-btn {'
echo '  background-color: red;'
echo '  box-shadow: 0 5px 0 darkred;'
echo '  color: white;'
echo '  padding: 1em 1.5em;'
echo '  position: relative;'
echo '  text-decoration: none;'
#echo '  text-transform: uppercase;'
echo '}'

echo '.link-btn:hover {'
echo '  background-color: #ce0606;'
echo '  cursor: pointer;'
echo '}'

echo '.link-btn:active {'
echo '  box-shadow: none;'
echo '  top: 5px;'
echo '}'

echo '</style>'
echo '</head>'
echo '<body class="w3-light-grey">'

echo '<!-- Icon Bar (Sidebar) -->'
echo '<nav class="w3-sidebar w3-bar-block w3-small w3-center">'
echo '  <!-- Avatar image in top left corner -->'
echo '  <img src="../webicons/icon.svg" alt="ADAPTABLE logo" style="width:100%">'
echo '  <a href="../index.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-home w3-xxlarge"></i>'
echo '    <p>HOME</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./adaptable-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-cogs w3-xxlarge"></i>'
echo '    <p>FAMILY GENERATOR</p>'
echo '  </a>'
echo '  <a href="./extract-form.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-search-plus w3-xxlarge"></i>'
echo '    <p>FAMILY ANALYZER</p>'
echo '  </a>'
echo '  <a href="./index-results.sh" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-download w3-xxlarge"></i>'
echo '    <p>DOWNLOAD RESULTS</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="./index-browse.sh" class="w3-bar-item w3-button w3-padding-large w3-light-grey">'
echo '    <i class="fas fa-list-ul w3-xxlarge"></i>'
echo '    <p>BROWSE AMPs & FAMILIES</p>'
echo '  </a>'
echo '<hr style="border:0;border-top:1px solid DarkGray;margin:0 0">'
echo '  <a href="../faq.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-question-circle w3-xxlarge"></i>'
echo '    <p>FAQ & TUTORIAL</p>'
echo '  </a>'
echo '  <a href="../about.html" class="w3-bar-item w3-button w3-padding-large w3-hover-light-grey">'
echo '    <i class="fas fa-users w3-xxlarge"></i>'
echo '    <p>ABOUT</p>'
echo '  </a>'
echo '</nav>'

echo '<div class="w3-padding-large" id="main">'

echo '<div id="header"> '
echo '        <div id="banner">'
echo '        ADAPTABLE'
echo '        </div>'
echo '</div>'

echo \
	"<article>"\
	"<header>"\
	"<h1>Browse ADAPTABLE AMPs and families</h1>"\
	"</header>"\
	"</article>"
echo "<hr>"

source ./tooltips.sh

fmax=$(cat ADAPTABLE/Results/all_families/.fam_num)

echo "<h3>Browse AMPs database</h3>"\
"<h6>Find information on single entries by peptide sequence in one-letter-code or name.</h6>"

echo "<form action='./peptideDB.sh' style='display: inline-block;' method=GET>"
echo "<div class="tooltip"><h6 style='display: inline-block;'>Peptide sequence&nbsp;</h6><span class="tooltiptext">"${tooltiptext_peptide_sequence}"</span></div><input id='myinput2' type='text' name="_peptide_sequence" size=70 placeholder='Search peptide by sequence or name' pattern='[A-Za-zԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]+' required>"
echo "<input type="submit" value='Submit'>"
echo "<input type="reset" value='Reset'>"
echo "</form>"

# Character selector
echo '<script>'
echo 'function openForm2() {'
echo '    document.getElementById("myForm2").style.display = "inline-block";'
echo '}'
echo 'function closeForm2() {'
echo '    document.getElementById("myForm2").style.display = "none";'
echo '}'
echo '</script>'

echo '<div class="characters-popup" id="myForm2">'
echo '  <form class="form-container-chars">'
echo '    <h3>Special characters for modified aminoacids</h3>'
echo "<i>You can click on the symbol to select it and use Ctrl+F to search your desired aminoacid</i>"
echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
cat ../character-selector | sed -e 's/input-text/myinput2/g'
echo '</div>'
echo '    <button type="button" class="btn cancel" onclick="closeForm2()"><i class="fas fa-signature"></i></button>'
echo '  </form>'
echo '</div>'
echo '<button class="open-button-chars" onclick="openForm2()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'
#

echo "<hr>"

echo "<h3>Family overview</h3>"\
"<h6>"\
"Find information on the families of the built-in <i>all_families</i> calculation, where all peptides of the database are used as reference <i>(father)</i> to form its own sequence-related family. "\
"In case your peptide is part of one of these families, you can retrieve missing information from other family members, due to the fact that they tend to have similar activity, structure, and mechanism of action."\
"</h6>"
#echo "<p><i>Note: More detailed information can be retrieved using <a href='./extract-form.sh?_calculation_label=all_families&_user_key=all_families'>'FAMILY ANALYZER'</a> tool.</i></p>"

#echo "<form action='./all_familiesDB.sh' style='display: inline-block;' method=GET>"
#echo "<div class="tooltip"><h6 style='display: inline-block;'>Family number&nbsp;</h6></div><input type="number" name="_fnum" min="1" max="${fmax}" required>"
#echo "<input type="submit" value='Submit'>"
#echo "<input type="reset" value="Reset">"
#echo "</form>"

echo "<form action='./all_familiesDB.sh' style='display: inline-block;' method=GET>"
echo "<div class="tooltip"><h6 style='display: inline-block;'>Peptide sequence/Name of the father&nbsp;</h6><span class="tooltiptext">"${tooltiptext_peptide_sequence}"</span></div><input id='myinput' type="text" name="_peptide_sequence" size=70 placeholder='Search peptide by sequence or name' pattern='[A-Za-zԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]+' required>"
echo "<input type="submit" value='Submit'>"
echo "<input type="reset" value="Reset">"
echo "</form>"

# Character selector
echo '<script>'
echo 'function openForm() {'
echo '    document.getElementById("myForm").style.display = "inline-block";'
echo '}'
echo 'function closeForm() {'
echo '    document.getElementById("myForm").style.display = "none";'
echo '}'
echo '</script>'

echo '<div class="characters-popup" id="myForm">'
echo '  <form class="form-container-chars">'
echo '    <h3>Special characters for modified aminoacids</h3>'
echo "<i>You can click on the symbol to select it and use Ctrl+F to search your desired aminoacid</i>"
echo '<div style="max-width:70vw;max-height:70vh;overflow:auto;">'
cat ../character-selector | sed -e 's/input-text/myinput/g' | sed -e 's/iAC/iAC2/g'
echo '</div>'
echo '    <button type="button" class="btn cancel" onclick="closeForm()"><i class="fas fa-signature"></i></button>'
echo '  </form>'
echo '</div>'
echo '<button class="open-button-chars" onclick="openForm()" title="Click here to pick special characters"><i class="fas fa-signature"></i></button>'
#
# Load (heavy) awesomeplete at the end
echo '<script>'
echo 'var input = document.getElementById("myinput2");'

echo '// Show label but insert value into the input:'
echo 'new Awesomplete(input, {'
echo '  maxItems: 20,'
echo '  list: ['
zcat ../DATABASE.seqs.js.awesomplete.gz
echo '  ]'
echo '});'
echo '</script>'
echo '<script>'
echo 'var input = document.getElementById("myinput");'

echo '// Show label but insert value into the input:'
echo 'new Awesomplete(input, {'
echo '	maxItems: 20,'
echo '  list: ['
zcat ../DATABASE.seqs-all_families.js.awesomplete.gz
echo '  ]'
echo '});'
echo '</script>'
echo "<hr>"

echo "<h3>FASTA generator</h3>"\
"<h6>"\
"It is possible to download all the sequences from database as plain text from <a target='_blank' href='https://gitlab.com/pachoramos/adaptable/-/raw/master/cgi-bin/ADAPTABLE/DATABASE.seqs' download>here</a>. "\
"To download a FASTA file with all the peptides having this properties you can use this form:"\
"</h6>"

# Needs jquery
# FIXME: Needs to know how to drop required when someone writtes in the input text field too
echo "<script>"
echo '$(function(){'
echo '    var requiredCheckboxes = $(".options :checkbox[required]");'
echo '    requiredCheckboxes.change(function(){'
echo '		if(requiredCheckboxes.is(":checked")) {'
echo '            requiredCheckboxes.removeAttr("required");'
echo '        } else {'
echo '            requiredCheckboxes.attr("required", "required");'
echo '        }'
echo '    });'
echo '})'
echo "</script>"

echo "<form style='display: inline-block;' method=GET>"

                echo "<div class="options" style='margin-left: 10px;margin-right: 10px;'>"
                for x in {\
antimicrobial,antibacterial,antigram_pos,antigram_neg,antibiofilm,\
antifungal,antiyeast,antiviral,\
antiparasitic,antiplasmodial,antitrypanosomic,antileishmania,\
antiprotozoal,insecticidal,anticancer,\
antiproliferative,antitumor,antiangiogenic,\
toxic,cell_cell,drug_delivery,antioxidant,\
cytotoxic,hemolytic,\
hormone,quorum_sensing,immunomodulant,antihypertensive,\
cell_penetrating,tumor_homing,blood_brain,\
cyclic,ribosomal,experimental,\
synthetic};do
                        echo "<label for="${x/antibiofilm/biofilm}"><input type="checkbox" name="_code" value="${x/antibiofilm/biofilm}" id="${x}">&nbsp;${x}&nbsp;&nbsp;</label>" # FIXME: Append 'required' when fixed
                done
		echo "<br><br>"
		echo "<div class="tooltip">Peptide name<span class="tooltiptext">"${tooltiptext_name}"</span></div></td><td><input type="text" id="sample" name="_name" size=30 pattern='[A-Z,a-z,0-9,-,_, ,.,]{1,100}' title='Only letters, numbers, hyphens, dots and spaces are allowed' placeholder='e.g. aurein'>"
		echo "<div class="tooltip">Target Organism<span class="tooltiptext">"${tooltiptext_all_organisms}"</span></div></td><td><input type="text" id="sample" name="_all_organisms" size=30 pattern='[A-Z,a-z,0-9,-,_, ,.,]{1,100}' title='Only letters, numbers, hyphens, dots and spaces are allowed' placeholder='e.g. baumannii'>"
		echo "</div>"
		echo "<br>"
                echo "<input type="submit" value='Submit'>" \
                        "<input type="reset" value="Reset">"

echo "</form>"

if [ "$QUERY_STRING" ]; then
        # Shorten addressbar
        echo "<script>"
        echo 'function shorten_addressbar_url() {'
        echo 'var url = window.location.href;'
        echo "var new_url = url.split('?')[0];"
        echo "history.replaceState('1', '', new_url);"
        echo '}'

        echo 'shorten_addressbar_url();'
        echo "</script>"

	export __code=$(echo "$QUERY_STRING" | tr '&' '\n'|grep -E '_code' | sed "s/%20/ /g" | cut -d '=' -f2)
	export __name=$(echo "$QUERY_STRING" | tr '&' '\n'|grep -E '_name' | sed "s/%20/ /g" | cut -d '=' -f2)
	export __all_organisms=$(echo "$QUERY_STRING" | tr '&' '\n'|grep -E '_all_organisms' | sed "s/%20/ /g" | cut -d '=' -f2)
	
	mkdir -p ADAPTABLE/tmp_files3/
	./ADAPTABLE/FASTA_generator.sh
	echo "<center>"
	echo '<a style="display: inline-block;" onclick="'
	echo "window.setTimeout(function(){window.location.replace('./index-browse.sh')},500);"
	echo '" href="../tmp/ADAPTABLE.fasta" class="link-btn" download>Download FASTA file <i class="fas fa-download"></i></a>'
	echo "</center>"
fi

echo '</div>'
echo '</body>'
echo '</html>'

exit 0
