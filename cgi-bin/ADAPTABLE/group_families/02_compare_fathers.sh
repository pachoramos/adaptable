#!/bin/bash
export group_fam_level="$(sed -n '4{p;q}' ../.out_parallel)"
for i in $(awk '{print $2}' .outputs/f${1}.in_families | sed -e 's/f//g'); do echo -n "f$i:" && echo "$(LC_ALL=C grep -m 1 -w f$i .outputs/f${1}.in_families | awk '{print $1}') / $(cat .family_elements|grep "sequences ${1} "|awk '{print $6}') * 100"|bc -l; done | awk -v percent="${group_fam_level}" -F: '$2>percent' | cut -d: -f1 > .outputs/f${1}.percentages
