#!/bin/bash
while [ -s .fam_num_seq ]; do
#    echo "file is not empty - keep checking it "
	g=$(head -n1 .fam_num_seq)
	echo "" >> .different-fathers
	echo -n "Group similar to family ${g} $(head -n1 .outputs/f${g}.seqs.clean): " >> .different-fathers
	sed -i -e "/^${g}$/d" .fam_num_seq
	while IFS= read -r line; do grep -vwFf .outputs/f${g}.percentages .outputs/${line}.percentages | grep -w -q ${line} || echo -n "${line/f/} " >> .different-fathers && sed -i -e "/^${line/f/}$/d" .fam_num_seq && sed -i -e "/^${line}$/d" .outputs/f*.percentages ; done < .outputs/f${g}.percentages
	sed -i -e "/^f${g}$/d" .outputs/f*.percentages
#    sleep 10 # throttle the check
done
#echo "file is empty "
