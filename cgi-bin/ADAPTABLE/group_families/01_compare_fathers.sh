#/bin/bash
seq 1 $(cat .fam_num) > .fam_num_seq

# We create a list for the used sequences during the experiment:
LC_ALL=C grep -w "seq" .selection| cut -d " " -f3 > .all_seqs

# At first we need to create clean sequence files without - and blank lines, use only transformed seq (for simple and DSSP)
parallel --jobs +1 "sed -e 's/-//g' .outputs/f'{}'.seqs | sed -e '/^$/d' | cut -d';' -f1 > .outputs/f'{}'.seqs.clean" ::: $(cat .fam_num_seq)

# Now we locate each sequence, no quotes at the end to not break some strange seqs with $ character
parallel --jobs +1 "grep -l -w -a  '{}' .outputs/*.seqs.clean|sed -e 's/.seqs.clean//' -e 's:.outputs/::g' > {}.in_families" ::: $(cat .all_seqs)

# We then replace the sequences by the families they are members of
parallel --jobs +1  "while IFS= read -r line; do cat "'${line}'".in_families ; done < .outputs/f'{}'.seqs.clean | sort -V | uniq -c > .outputs/f'{}'.in_families" ::: $(cat .fam_num_seq)

# We count the lines calculating percentages, for example, f1 contains 100% of members of the same f1, 80% of f2...
# After, we rely on awk to handle the cutoff percentage of similarity between families (ex. 75%), once processed, we do not need the values of the percentages, only the families that passed the limit.
parallel --jobs +1  "bash 02_compare_fathers.sh '{}'" ::: $(cat .fam_num_seq)

# Now we need to check for each family the contents, if the family is NOT in the output of the used grep, it is a member of the SAME superfamily, if not I need to continue with the others
bash 03_compare_fathers.sh

# We do some cleaning after
rm -f .fam_num_seq .all_seqs
find *.in_families -delete
find .outputs/*.in_families -delete
find .outputs/*.clean -delete
find .outputs/*.percentages -delete
sed -i -e '/^$/d' .different-fathers

# Create .group_fam_num, needed for extract form
cat .different-fathers | wc -l > .group_fam_num
