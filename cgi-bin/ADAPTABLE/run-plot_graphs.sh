#!/bin/bash
export plot_graphs="$(sed -n '2{p;q}' OUTPUTS/.out_parallel)"
if [ "${plot_graphs}" = "y" ]; then
	export calculation_label="$(head -n1 OUTPUTS/.out_parallel)"
	export fam_num="$(cat "OUTPUTS/${calculation_label}/.fam_num")"
	export multiplier="120"
	echo -e "\nGenerating graphics files..." >> "OUTPUTS/${calculation_label}/${calculation_label}-output"
	./countdown.sh "$(echo "${multiplier} * ${fam_num} / $(nproc) " | bc | cut -d "." -f1)" > OUTPUTS/.remaining_time &
#	for i in $(seq 1 "${fam_num}"); do
	processes=$(echo "$(nproc) / 6" | bc)
	if [[ ${processes} < 1 ]]; then
		parallel_procs="1"
	else
		parallel_procs=${processes}
	fi
	# Skip for families of 1 member as they are not useful for them
	#parallel --jobs "${parallel_procs}" "fam_count='{}' python3 python/plot_graph.py "${calculation_label}" '{}'" ::: $(seq 1 "${fam_num}")
	if [ "${fam_num}" = "1" ]; then
		fam_count='1' python3 python/plot_graph.py "${calculation_label}" '1'
		mv OUTPUTS/"${calculation_label}"/Graphics/f1/* OUTPUTS/"${calculation_label}"/Graphics/. && rm -rf OUTPUTS/"${calculation_label}"/Graphics/f1/
	else
		fam_multi="$(grep elements "OUTPUTS/${__calculation_label}/.family_elements" |awk '{print $4}')"
		parallel --jobs "${parallel_procs}" "fam_count='{}' python3 python/plot_graph.py "${calculation_label}" '{}'" ::: ${fam_multi}
#		fam_count="${i}" python3 python/plot_graph.py "${calculation_label}" "${i}"
#	done
	fi
	echo "IndexOrderDefault Ascending Name" > OUTPUTS/${calculation_label}/Graphics/.htaccess
fi
