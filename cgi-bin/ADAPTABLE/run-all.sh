#!/bin/bash
[ ${__sendmail} ] && echo -e "Subject: Job ${__calculation_label} started\n\nHello\nYour submitted ${__calculation_label} job started, feel free to visit http://gec.u-picardie.fr/adaptable/cgi-bin/adaptable-form.sh to review the progress or kill it if wanted\nBest regards" | msmtp -a default "${__sendmail/'%40'/@}" "pachoramos@gmail.com" "nicola.damelio@u-picardie.fr"
echo "${__calculation_label}" > OUTPUTS/.out_parallel && echo "${__plot_graphs}" >> OUTPUTS/.out_parallel && echo "${__seqlogo}" >> OUTPUTS/.out_parallel && echo "${__group_fam_level}" >> OUTPUTS/.out_parallel && \
cp -f awks/"${__align_matrix}" tmp_files/align_matrix 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
echo "user=${__user_key}" > "OUTPUTS/${__calculation_label}/${__calculation_label}-output" && \
echo "mail=${__sendmail/'%40'/@}" >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" && \
echo "long_run=${__long_run}" >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" && \
awk -f awks/analyse_AMPs_database_head.awk DATABASE >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
./append-seq.sh >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
if (( $(wc -l <OUTPUTS/${__calculation_label}/selection) > 4000 )) && [[ ${__long_run} != "yes" ]]; then
	touch OUTPUTS/${__calculation_label}/.long_run
	exit 0
fi
parallel --jobs +1 --bar "family_index='{}' awk -f awks/analyse_AMPs_database_body.awk DATABASE 2>>OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" ::: $(cat "OUTPUTS/${__calculation_label}/.fam_num") 1>>"OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>OUTPUTS/.out_parallel && \
echo "Exit status for parallel analyse_AMPs_database_body.awk: $?" >>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
zsh -c "cat tmp_files/score*_${__calculation_label}(n) > tmp_files/all_scores_${__calculation_label}" && \
awk -f awks/analyse_AMPs_database_tail_sort.awk DATABASE >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
echo "Exit status for analyse_AMPs_database_tail_sort.awk: $?" >>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
parallel --keep-order --jobs +1 --bar "family_index='{}' awk -f awks/analyse_AMPs_database_tail_parallel.awk DATABASE 2>>OUTPUTS/${__calculation_label}/.${__calculation_label}-errors " ::: $(seq 1 $(cat "OUTPUTS/${__calculation_label}/.fam_num")) 1>>"OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>OUTPUTS/.out_parallel && \
echo "Exit status for parallel analyse_AMPs_tail_parallel.awk: $?" >>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
# This 'Calculating for' helps csplit to know that it must stop last family output file here
echo "Calculating for all:" >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" && \
# awk -f awks/analyse_AMPs_database_body_fathers.awk DATABASE needs ./build-selection-fathers-sorted.sh to be run before
./build-selection-fathers-sorted.sh 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
# We show "parent" instead of "father", even if internally it keeps using father notation
echo "...The following families are notably different (parent sequence, family number):" >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" && \
# We now have a different way to group families
#awk -f awks/analyse_AMPs_database_body_fathers.awk DATABASE >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
./run-plot_graphs.sh 1>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
./seqlogo.sh && \
./build-selection-fathers.sh 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
mkdir -p "OUTPUTS/${__calculation_label}/.FASTA_files" && find "OUTPUTS/${__calculation_label}/" -name '*_FASTA*' -not -path "*FASTA_files/*" -exec mv --target-directory="OUTPUTS/${__calculation_label}/.FASTA_files" '{}' + 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
mkdir -p "OUTPUTS/${__calculation_label}/.outputs" && \
# Ensure it has a proper utf-8 encoding
iconv -c -f utf-8 -t utf-8 "OUTPUTS/${__calculation_label}/${__calculation_label}-output" > "OUTPUTS/${__calculation_label}/tmp" && mv -f "OUTPUTS/${__calculation_label}/tmp" "OUTPUTS/${__calculation_label}/${__calculation_label}-output" && \
csplit -s -b %01d -f "OUTPUTS/${__calculation_label}/.outputs/f" "OUTPUTS/${__calculation_label}/${__calculation_label}-output" '/Calculating for/' {*} 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
./generate-user_famDB.sh 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" && \
mv "OUTPUTS/${__calculation_label}/selection" "OUTPUTS/${__calculation_label}/.selection" && \
cp -r group_families/*.sh OUTPUTS/${__calculation_label}/ && cd OUTPUTS/${__calculation_label}/ && bash 01_compare_fathers.sh 2>>".${__calculation_label}-errors" && rm -f *.sh && cd ../.. && \
if [[ ${__calculation_label} = "all_families" ]]; then
	# Assign fathers of all_families to family number
	# FIXME: Don't do this as this will show all fathers... even those that are not the best father, hence, each peptide will be shown as father of more than one family even not being the best
#	grep . tmp_files/.all_fathers_* | sed -e 's:tmp_files/.all_fathers_:f:g' | sort -V > OUTPUTS/all_families/.family_fathers 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
	grep -r "The best alignment is found with" OUTPUTS/"${__calculation_label}"/.outputs/ | awk '{print "f"$2":"$12}' > OUTPUTS/all_families/.family_fathers 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
	cp -f Results/.gitignore OUTPUTS/all_families/.
	rm -f "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
else
	mkdir -p "OUTPUTS/${__calculation_label}/Families/.fonts"
	cp outputpimper/.styles.css "OUTPUTS/${__calculation_label}/Families/."
	cp dejavu/ttf/*.ttf "OUTPUTS/${__calculation_label}/Families/.fonts/."
	cp liberation/*.ttf "OUTPUTS/${__calculation_label}/Families/.fonts/."
	echo "IndexOrderDefault Ascending Name" > "OUTPUTS/${__calculation_label}/Families/.htaccess"
	echo "Generating HTML Output file..." >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
	# We use new output-pimper for this and the other one if it fails
	parallel --jobs +1 "cd outputpimper/ && python3 ./output2html.py ../OUTPUTS/${__calculation_label}/.outputs/f'{}' 1>/dev/null 2>>../OUTPUTS/${__calculation_label}/.${__calculation_label}-errors && mv ../OUTPUTS/${__calculation_label}/.outputs/f'{}'.html ../OUTPUTS/${__calculation_label}/Families/f'{}'.html" ::: $(seq 1 $(cat "OUTPUTS/${__calculation_label}/.fam_num")) 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
	if [ "$?" -eq "0" ]; then
		true
	else
		parallel --jobs +1 "cat fix-monospace-font.html > OUTPUTS/${__calculation_label}/Families/f'{}'.html && cat OUTPUTS/${__calculation_label}/.outputs/f'{}' | cl-ansi2html -w -n >> OUTPUTS/${__calculation_label}/Families/f'{}'.html" ::: $(seq 1 $(cat "OUTPUTS/${__calculation_label}/.fam_num")) 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
		# sed needs to be run after all html are generated or it will mix them
		parallel --jobs +1 "sed -i -e '29 s/white-space:pre-wrap/white-space:pre/' OUTPUTS/${__calculation_label}/Families/f'{}'.html" ::: $(seq 1 $(cat "OUTPUTS/${__calculation_label}/.fam_num")) 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
	fi
	rm -f "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
	tar -cf "OUTPUTS/${__calculation_label}-full.tar" --exclude ".*" -C "OUTPUTS/" "${__calculation_label}/" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
	tar --append --exclude "*.IDs" --exclude "*.bash" --exclude "*.seqs" --file="OUTPUTS/${__calculation_label}-full.tar" -C "OUTPUTS/" "${__calculation_label}/.outputs/" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
	tar --append --file="OUTPUTS/${__calculation_label}-full.tar" -C "OUTPUTS/" "${__calculation_label}/Families/.fonts/" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
	tar --append --file="OUTPUTS/${__calculation_label}-full.tar" -C "OUTPUTS/" "${__calculation_label}/Families/.styles.css" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
	xz -T0 "OUTPUTS/${__calculation_label}-full.tar"
	mv "OUTPUTS/${__calculation_label}-full.tar.xz" "OUTPUTS/${__calculation_label}/"
fi
mkdir -p Results/"${__user_key}"/ && \
mv "OUTPUTS/${__calculation_label}/"  Results/"${__user_key}"/. && \
echo "IndexOrderDefault Descending Date" > Results/"${__user_key}"/.htaccess && \
rm -f OUTPUTS/.out_parallel OUTPUTS/.remaining_time && \
cd OUTPUTS/ && find . ! -name ".gitignore" -delete && cd ../tmp_files/ && find . ! -name ".gitignore" -delete && cd ../tmp_files2 && find . ! -name ".gitignore" -delete && cd .. && \
pgrep -f -u apache countdown | xargs --no-run-if-empty -t kill -9 ; \
[ ${__sendmail} ] && echo -e "Subject: Job ${__calculation_label} ended\n\nHello\nYour submitted ${__calculation_label} job ended, feel free to visit http://gec.u-picardie.fr/adaptable/cgi-bin/extract-form.sh to review the results\nBest regards" | msmtp -a default "${__sendmail/'%40'/@}" "pachoramos@gmail.com" "nicola.damelio@u-picardie.fr"
