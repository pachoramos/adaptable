#!/bin/bash
# usage ./extract_family.exe $fam_numb $filename or ./extract_family.exe $fam_numb1-$fam_numb2 $filename or ./extract_family.exe $fam_numb1-$fam_numb2-$code $filename
# $code=p(polarity),s(structure),r(residue type),f(family or frequency),d(data),t(data as text),x(specific parameters which must be specified as a third field)
# example ./extract_family.exe 1-1-s cancerfile (see the structures of family 1)
# example2 ./extract_family.exe 1-1-x file name-" ID" (it select the parameter name and ID (ID is written in quotes with a spece in front to avoid including other parameters containibg ID as PMID)
if [[ $1 == *"-"* ]]; then
	let k=0
	IFS='-' read -a myarray <<< "$1"
        if [[ ${myarray[0]} == *","* ]]; then
                IFS=',' read -a values <<< "${myarray[0]}"
                datatype=${myarray[1]}
        else
                for(( i=${myarray[0]}; i<${myarray[1]}+1 ; i++ ))
                        do
                                values[${k}]=$i
				let "k=k+1"
                        done
                datatype=${myarray[2]}
        fi
        
	export SCRIPT_EXTRACT="./ADAPTABLE/tmp_files/deletable.exe-$(date -u +%c | sed -e 's/ /_/g'| sed -e 's/:/./g')"

	echo "echo """ > "${SCRIPT_EXTRACT}"
	
        for(( i=0; i<${#values[@]} ; i++ ))
	do
	if [ -n "$3" ]  && [[ $datatype == *"x"* ]]; then
		fmax=$(cat "$3/.fam_num")
	else
		fmax=$(cat "$2/.fam_num")
	fi
	if [[ "${values[$i]}" -le "${fmax}" ]]; then
		if [[ $datatype == *"a"* ]] ||  [[ $datatype == "" ]]; then
			echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
			echo "cat $2/.outputs/f${values[$i]}" >> "${SCRIPT_EXTRACT}"
			echo "echo """ >> "${SCRIPT_EXTRACT}"
		fi
		if [[ $datatype == *"f"* ]] ||  [[ $datatype == "" ]]; then
			echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
			echo "sed -n '/Family ${values[$i]} /,/Colouring by residue type/{/Colouring by residue type/!p}' $2/.outputs/f${values[$i]}" >> "${SCRIPT_EXTRACT}"
			# We don't print for now this tip for seeing the length of the sequences
			#echo "echo "......____5___10___15___20___25___30___35___40___45___50"" >> "${SCRIPT_EXTRACT}"
			echo "echo """ >> "${SCRIPT_EXTRACT}"
		fi
		if [[ $datatype == *"d"* ]] ||  [[  $datatype == "" ]]; then
			echo "echo """ >> "${SCRIPT_EXTRACT}"
			echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
			echo "sed '0,/This family has the following properties:/d;/ ID/Q' $2/.outputs/f${values[$i]}" >> "${SCRIPT_EXTRACT}"
			echo "echo """ >> "${SCRIPT_EXTRACT}"
		fi
		if [[ $datatype == *"s"* ]] ||  [[  $datatype == "" ]]; then
			echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
			# Quit after the first occurence is found to not print data from Best representative peptide
			echo "sed -n '/Secondary structure prediction/,/This family has the following properties/{/This family has the following properties/!p};/This family has the following properties/q' $2/.outputs/f${values[$i]}" >> "${SCRIPT_EXTRACT}"
			echo "echo """ >> "${SCRIPT_EXTRACT}"
		fi
                if [[ $datatype == *"r"* ]] ; then
	                echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
			echo "sed -n '/Colouring by residue type/,/Colouring by polarity/{/Colouring by polarity/!p}' $2/.outputs/f${values[$i]}" >> "${SCRIPT_EXTRACT}"
			echo "echo """ >> "${SCRIPT_EXTRACT}"
		fi
		if [[ $datatype == *"p"* ]] ||  [[  $datatype == "" ]]; then
			echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
			echo "sed -n '/Colouring by polarity/,/Secondary structure prediction/{/Secondary structure prediction/!p}' $2/.outputs/f${values[$i]}" >> "${SCRIPT_EXTRACT}"
                	echo "echo """ >> "${SCRIPT_EXTRACT}"
		fi
		# Easier for webpage form to keep filename as the last parameter and pass the names for code just after code x
                if [ -n "$3" ]  && [[ $datatype == *"x"* ]]; then
			#IFS='-' read -a myarray2 <<< "$3"
			IFS='-' read -a myarray2 <<< "$2"
			for(( j=0 ; j<${#myarray2[@]} ; j++ ))
				do
                			#echo "awk '/Family ${values[$i]} /,/best alignment with sequence number/' $2 | awk '/${myarray2[$j]}/,/___/' "   >> "${SCRIPT_EXTRACT}"
                			echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
                			echo "awk '/Family ${values[$i]} /,/best alignment with sequence number/' $3/.outputs/f${values[$i]} | awk '/ ${myarray2[$j]}:/,/___/' "   >> "${SCRIPT_EXTRACT}"
				done
                	echo "echo """ >> "${SCRIPT_EXTRACT}"
                fi
                if [[ $datatype == *"t"* ]] ||  [[  $datatype == "" ]]; then
	                echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
       		        echo "awk '/Family ${values[$i]} /,/best alignment with sequence number/' $2/.outputs/f${values[$i]} | awk '/ ID:/,/End of analysis/'"   >> "${SCRIPT_EXTRACT}"
                	echo "echo """ >> "${SCRIPT_EXTRACT}"
                fi
		echo "echo """ >> "${SCRIPT_EXTRACT}"
		if  [[  $datatype == "u" ]]; then
			echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
			echo "sed -n '/Best representing sequence is sequence/,/End of analysis for/{/End of analysis for/!p}' $2/.outputs/f${values[$i]}" >> "${SCRIPT_EXTRACT}"
		fi
	fi
	done
else
		echo "echo -e '\033[1mFamily ${values[$i]}:\033[0m'" >> "${SCRIPT_EXTRACT}"
		echo "cat $2/.outputs/f${values[$i]}" >> "${SCRIPT_EXTRACT}"
                echo "echo -e '________________________________________________________________________________________________________________________________________________'" >> "${SCRIPT_EXTRACT}"
fi
/bin/bash "${SCRIPT_EXTRACT}"
rm -f "${SCRIPT_EXTRACT}"
