#!/bin/bash
[ ${__sendmail} ] && echo -e "Subject: Job ${__calculation_label} started\n\nHello\nYour submitted ${__calculation_label} job started, feel free to visit http://gec.u-picardie.fr/adaptable/cgi-bin/adaptable-form.sh to review the progress or kill it if wanted\nBest regards" | msmtp -a default "${__sendmail/'%40'/@}" "pachoramos@gmail.com" "nicola.damelio@u-picardie.fr"
echo "${__calculation_label}" > OUTPUTS/.out_parallel && echo "${__plot_graphs}" >> OUTPUTS/.out_parallel && echo "${__seqlogo}" >> OUTPUTS/.out_parallel && echo "${__comparison_option}" >> OUTPUTS/.out_parallel && \
# Prepare the output to be detected at extract time
echo "${__comparison_option}" > "OUTPUTS/${__calculation_label}/.prediction-prefs"
echo "user=${__user_key}" > "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
echo "mail=${__sendmail/'%40'/@}" >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
# Tell user all the information about the peptide if it is already present in the DATABASE
if output=$(grep -i -w -a " ${__my_peptide} " DATABASE); then
	echo -e "\033[1mYour peptide has already an entry in our DATABASE:" >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
	echo -e " ${output}\033[0m" >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
	echo >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
fi
if output=$(grep -a " ${__prediction} " DATABASE); then
	echo -e "\033[1mYour peptide has already an entry in our DATABASE:" >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
	echo -e " ${output}\033[0m" >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
	echo >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
fi
[[ ${__simplify_aminoacids} = "y" ]] && simplified_peptide=$(echo "${__prediction}" | awk -f awks/simplify_aminoacids.awk) && export __prediction="${simplified_peptide}"
echo "${__prediction}" >> "OUTPUTS/${__calculation_label}/.prediction-prefs"
# We launch a countdown with a hardcoded duration as we don't have a better way to calculate the remaining time
./countdown.sh "100" > OUTPUTS/.remaining_time &
echo "Starting comparison of user supplied peptide with all sequences of all_families..." >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
# We need to use the selection file containing the fathers, we need to also fix encoding
#	iconv -f utf-8 -t utf-8 -c Results/all_families/.selection > "OUTPUTS/${__calculation_label}/selection" 2>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
# We start
cp -f awks/"${__align_matrix}" tmp_files/align_matrix 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
awk -f awks/analyse_AMPs_database_head.awk DATABASE > /dev/null 2>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
./append-seq.sh >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
# Now add the peptide used as reference
unset __my_peptide && export __my_peptide=${__prediction} && ./append-seq.sh >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
awk 'END{print$1}' "OUTPUTS/${__calculation_label}/selection" > "OUTPUTS/${__calculation_label}/.fam_num" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
# The user peptide is compared with the all sequences as it was the father (family_index=dmax because the user peptide is added in the end of the selection list)
family_index="$(tail -n1 "OUTPUTS/${__calculation_label}/.fam_num")" comparison_option='sons' awk -f awks/analyse_AMPs_database_body.awk DATABASE 1>>"OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
echo "Exit status for analyse_AMPs_database_body.awk: $?" >>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
zsh -c "cat tmp_files/score*_${__calculation_label}(n) > tmp_files/all_scores_${__calculation_label}"
comparison_option='sons' awk -f awks/analyse_AMPs_database_tail_sort.awk DATABASE >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
echo "Exit status for analyse_AMPs_database_tail_sort.awk: $?" >>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
echo "Performing analysis..." >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
# FIXME: for now we put errors of internal awk command also in .parallel_output because otherwise we cannot parse the file for getting time left properly
comparison_option='sons' parallel --keep-order --jobs +1 --bar "family_index='{}' awk -f awks/analyse_AMPs_database_tail_parallel.awk DATABASE 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" " ::: $(seq 1 $(cat "OUTPUTS/${__calculation_label}/.fam_num")) 1>>"OUTPUTS/${__calculation_label}/${__calculation_label}-output" 2>>OUTPUTS/.out_parallel
echo "Exit status for parallel analyse_AMPs_tail_parallel.awk: $?" >>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
./run-plot_graphs.sh 1>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
./seqlogo.sh 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
mkdir -p "OUTPUTS/${__calculation_label}/.FASTA_files" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
find "OUTPUTS/${__calculation_label}/" -name '*_FASTA*' -not -path "*FASTA_files/*" -exec mv --target-directory="OUTPUTS/${__calculation_label}/.FASTA_files" '{}' + 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
# Create a file showing if the components of the family are fathers of all_families experiment
for x in $(cat "OUTPUTS/${__calculation_label}/.seq_ordered_1"|sed -e 's/;//g');do grep ":${x}$" Results/all_families/.family_fathers; done > "OUTPUTS/${__calculation_label}/.fathers_in_all_families"
# Ensure it has a proper utf-8 encoding
iconv -c -f utf-8 -t utf-8 "OUTPUTS/${__calculation_label}/${__calculation_label}-output" > "OUTPUTS/${__calculation_label}/tmp" && mv -f "OUTPUTS/${__calculation_label}/tmp" "OUTPUTS/${__calculation_label}/${__calculation_label}-output" && \
echo "Generating HTML Output file..." >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
mkdir -p "OUTPUTS/${__calculation_label}/.fonts"
cp dejavu/ttf/*.ttf "OUTPUTS/${__calculation_label}/.fonts/."
cp liberation/*.ttf "OUTPUTS/${__calculation_label}/.fonts/."
# We use now outputpimper for this and the old method if it fails
cp outputpimper/.styles.css "OUTPUTS/${__calculation_label}/."
cd outputpimper/ && \
python3 ./output2html.py ../OUTPUTS/${__calculation_label}/${__calculation_label}-output 1>/dev/null 2>>../OUTPUTS/${__calculation_label}/.${__calculation_label}-errors
if [ "$?" -eq "0" ]; then
	cd ..
else
	cd ..
	cat fix-monospace-font.html > "OUTPUTS/${__calculation_label}/${__calculation_label}-output.html"
	cat "OUTPUTS/${__calculation_label}/${__calculation_label}-output" | cl-ansi2html -w -n >> "OUTPUTS/${__calculation_label}/${__calculation_label}-output.html" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
	# Remember to adapt below line if header of html changes as the number of line could change (we set specific number for better performance)
	sed -i -e '29 s/white-space:pre-wrap/white-space:pre/' "OUTPUTS/${__calculation_label}/${__calculation_label}-output.html" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
fi
mkdir -p "OUTPUTS/${__calculation_label}/.outputs" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
csplit -s -b %01d -f "OUTPUTS/${__calculation_label}/.outputs/f" "OUTPUTS/${__calculation_label}/${__calculation_label}-output" '/Calculating for Family/' {*} 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
./generate-user_famDB.sh 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
mv "OUTPUTS/${__calculation_label}/selection" "OUTPUTS/${__calculation_label}/.selection"
tar -cf "OUTPUTS/${__calculation_label}-full.tar" --exclude ".*" -C "OUTPUTS/" "${__calculation_label}/" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
tar --append --file="OUTPUTS/${__calculation_label}-full.tar" -C "OUTPUTS/" "${__calculation_label}/.fonts/" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
tar --append --file="OUTPUTS/${__calculation_label}-full.tar" -C "OUTPUTS/" "${__calculation_label}/.styles.css" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
xz -T0 "OUTPUTS/${__calculation_label}-full.tar"
rm -f "OUTPUTS/${__calculation_label}/${__calculation_label}-output"
mv "OUTPUTS/${__calculation_label}-full.tar.xz" "OUTPUTS/${__calculation_label}/" 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
mkdir -p Results/"${__user_key}"/
echo "IndexOrderDefault Descending Date" > Results/"${__user_key}"/.htaccess
mv "OUTPUTS/${__calculation_label}/"  Results/"${__user_key}"/. 2>>"OUTPUTS/${__calculation_label}/.${__calculation_label}-errors"
rm -f OUTPUTS/.out_parallel OUTPUTS/.remaining_time
cd OUTPUTS/ && find . ! -name ".gitignore" -delete && cd ../tmp_files/ && find . ! -name ".gitignore" -delete && cd ../tmp_files2 && find . ! -name ".gitignore" -delete && cd ..
# Ensure we don't leave a defunct countdown or it can cause ugly delays
pgrep -f -u apache countdown | xargs --no-run-if-empty -t kill -9;
[ ${__sendmail} ] && echo -e "Subject: Job ${__calculation_label} ended\n\nHello\nYour submitted ${__calculation_label} job ended, feel free to visit http://gec.u-picardie.fr/adaptable/cgi-bin/extract-form.sh to review the results\nBest regards" | msmtp -a default "${__sendmail/'%40'/@}" "pachoramos@gmail.com" "nicola.damelio@u-picardie.fr"
