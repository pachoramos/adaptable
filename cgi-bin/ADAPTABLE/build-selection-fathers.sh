#!/bin/bash
export calculation_label="$(head -n1 OUTPUTS/.out_parallel)"
if [ "${calculation_label}" = "all_families" ]; then
	cat tmp_files/.all_fathers* > OUTPUTS/"${calculation_label}"/.all_fathers
	# Get list of fathers
	# $ cat "OUTPUTS/"${calculation_label}"/.all_fathers"
	# Now we pass that to get all the information from "selection" -a needed to make grep not getting confused thinking it is a binary file due to some strange characters
	# $ for i in $(cat "OUTPUTS/"${calculation_label}"/.all_fathers"); do grep -a -E " seq ${i}$" -A3 selection; done
	# All in one line after renumbering the fathers:
	# $ for i in $(cat "OUTPUTS/"${calculation_label}"/.all_fathers"); do grep -a -E " seq ${i}$" -A3 selection; done| cut -d" " -f2- |awk -v x=4 '{print (NR%x?c+1:++c),$0}' > .selection-fathers
	for i in $(cat "OUTPUTS/"${calculation_label}"/.all_fathers"); do grep -a -E " seq ${i}$" -A3 OUTPUTS/"${calculation_label}"/selection; done| cut -d" " -f2- |awk -v x=4 '{print (NR%x?c+1:++c),$0}' > OUTPUTS/"${calculation_label}"/.selection-fathers
fi
