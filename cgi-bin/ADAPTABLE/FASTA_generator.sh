#!/bin/bash
rm -f ./ADAPTABLE/tmp_files3/ADAPTABLE.fasta.prev
rm -f ./ADAPTABLE/tmp_files3/ADAPTABLE.fasta

for i in ${__code}; do
	# At first we put in the same line all to sort and drop duplicates, at the end we jump sequences to newlines
	grep -a -w ${i} ./ADAPTABLE/DATABASE| awk '{name=$3; seq=$2 ; print ">"name"@@@@"seq}' >> ./ADAPTABLE/tmp_files3/ADAPTABLE.fasta.prev
done
# Use 'fasta_name' as variable name to not collide with already defined 'name'
[ ${__name} ] && awk -v name="${__name}" 'BEGIN{IGNORECASE=1}$3~name{fasta_name=$3; seq=$2 ; print ">"fasta_name"@@@@"seq}' ADAPTABLE/DATABASE >> ./ADAPTABLE/tmp_files3/ADAPTABLE.fasta.prev
[ ${__all_organisms} ] && awk -v all_organisms="${__all_organisms}" 'BEGIN{IGNORECASE=1}$57~all_organisms{name=$3; seq=$2 ; print ">"name"@@@@"seq}' ADAPTABLE/DATABASE >> ./ADAPTABLE/tmp_files3/ADAPTABLE.fasta.prev

sort -V ./ADAPTABLE/tmp_files3/ADAPTABLE.fasta.prev | uniq > ./ADAPTABLE/tmp_files3/ADAPTABLE.fasta

# Move seqs to new lines
sed -i -e 's/@@@@/\n/g' ./ADAPTABLE/tmp_files3/ADAPTABLE.fasta

# FASTA format doesn't allow ;
sed -i -e 's/;/,/g' ./ADAPTABLE/tmp_files3/ADAPTABLE.fasta
