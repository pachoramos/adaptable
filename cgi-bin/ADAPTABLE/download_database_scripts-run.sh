#!/bin/bash
# You need to append the output of this script to parallel as
# ./download_database_scripts-run.sh | parallel --jobs 0
for i in awks/download_database_scripts/*.awk; do
	echo "awk -f $i"
done
echo "bash awks/download_database_scripts/download_dbaasp.sh"
