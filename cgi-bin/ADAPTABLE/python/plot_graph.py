#!/usr/bin/env python3
import os
import tempfile
os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()
import matplotlib as mpl
# Needed to run on a headless system, for example a web server
# Needs to be called before trying to use pyplot
mpl.use('Agg')
from matplotlib.pyplot import cm 
import itertools
import os
import os.path
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import sys
from joblib import Parallel, delayed
# Needed to run on a headless system, for example a web server
mpl.pyplot.switch_backend('agg')
#mpl.style.use('fivethirtyeight')
#mpl.rcParams['patch.force_edgecolor'] = True
#mpl.rcParams['patch.facecolor'] = 'b'
#mpl.rcParams['patch.antialiased'] = True
mpl.rcParams['patch.linewidth'] = '0.1'

filename = os.environ.get('PYTHONSTARTUP')
if filename and os.path.isfile(filename):
    execfile(filename)

from numpy import loadtxt
from pylab import plot, show, xlabel, ylabel, subplot, tight_layout, savefig, title, xticks, yticks

# This for comes from the non parallel version
#for p in range(1,7):
def analysis(p):

	x=[0] * 400
	y=[0] * 400
	label=[0] * 400
	lines=[0] * 80
	color_graph=[0] * 80
	color_graph[1]='b'
	color_graph[2]='g'
	color_graph[3]='r'
	color_graph[4]='c'
	color_graph[5]='m'
	color_graph[6]='y'
	color_graph[7]='k'
	color_graph[8]='w'
	color_graph[9]='#FFEBCD'
	color_graph[10]='#8A2BE2'
	color_graph[11]='#A52A2A'
	color_graph[12]='#DEB887'
	color_graph[13]='#5F9EA0'
	color_graph[14]='#7FFF00'
	color_graph[15]='#D2691E'
	color_graph[16]='#FF7F50'
	color_graph[17]='#008B8B'
	color_graph[18]='#B8860B'
	color_graph[19]='#8B008B'
	color_graph[20]='#FF6347'
	color_first_graph=[0] * 80
	color_first_graph[1]='#B08687'
	color_first_graph[2]='#C5B44B'
	color_first_graph[3]='#C59D4B'
	color_first_graph[4]='#C5834B'
	color_first_graph[5]='#C5644B'
	color_first_graph[6]='#C54B52'
	color_first_graph[7]='#4BBEC5'
	color_first_graph[8]='#4B97C5'
	color_first_graph[9]='#4B72C5'
	color_first_graph[10]='#4B57C5'
	color_first_graph[11]='#554BC5'

	aa_name=[0] * 21
	aa_name[1]="A"; aa_name[2]="R";aa_name[3]="N";aa_name[4]="D";aa_name[5]="C";aa_name[6]="Q";aa_name[7]="E";aa_name[8]="G";aa_name[9]="H";aa_name[10]="I";
	aa_name[11]="L";aa_name[12]="K";aa_name[13]="M";aa_name[14]="F";aa_name[15]="P";aa_name[16]="S";aa_name[17]="T";aa_name[18]="W";aa_name[19]="Y";aa_name[20]="V";
	aa_label=[0] * 21
	aa_label[1]="Alanine"; aa_label[2]="Arginine";aa_label[3]="Asparagine";aa_label[4]="Aspartate";aa_label[5]="Cysteine";aa_label[6]="Glutamine";aa_label[7]="Glutamate";aa_label[8]="Glycine";aa_label[9]="Histidine";aa_label[10]="Isoleucine";
	aa_label[11]="Leucine";aa_label[12]="Lysine";aa_label[13]="Methionine";aa_label[14]="Phenylalanine";aa_label[15]="Proline";aa_label[16]="Serine";aa_label[17]="Threonine";aa_label[18]="Tryptofan";aa_label[19]="Tyrosine";aa_label[20]="Valine";

	title_all=""
	calculation_label=sys.argv[1]

	tag=os.environ['fam_count']
	fo=open('tmp_files/tmp0_'+str(calculation_label)+'_'+str(tag),'r')
	for m in fo.readlines():
		title_all=title_all+m+"\n"
	fo.close()
	title_all="In the following graphs, we analise the population of peptides for each length within family "+str(tag)+" (brown). We also report the essentiality of each aminoacid\nor class ('Average presence of AA type per peptide', red shades) and the average number of aminoacid types per peptide within the family ('Average\nnumber of AA type per peptide', blue shades).\nThe analysis is repeated by classifying aminoacids in groups according to their chemical characteristics, steric hyndrance of the side chains (volume) or\nchemical functional groups.\nDifferent classifications are meant to highlight possible characteristic features of the family. The essentiality parameter counts each aminoacid type (or\nclass of aminoacids) only once per peptide.\nSuppose the user has created a family using only peptide active against a specific organism, if lysine is an essential \"ingredient\" for the activity, the\nessentiality parameter will have a value of 100 %.\n\n"+title_all
	pp=0
	ppp=0
	ff=1
	title_sequences=[0] * 80
	sizev=[0] * 80
	max_number_of_plots_per_page=16
	fo=open('tmp_files/tmp00_'+str(tag)+'_'+str(calculation_label),'r')
	for m in fo.readlines():
		if ppp==int(max_number_of_plots_per_page):
			sizev[ff]=int(ppp)*6
			ppp=0
			ff=ff+1
		pp=pp+1
		ppp=ppp+1
		if ppp==1:
			title_sequences[ff]=str(m)+"\n"
		else:
			title_sequences[ff]=str(title_sequences[ff])+str(m)+"\n"
	fo.close()
	ppmax=pp
	sizeh=ppmax
	if sizeh<40:
		sizeh=40
	if int(sizev[1])<60:
		sizev[1]=60
	lines[1]=10
	lines[2]=1
	lines[3]=12
	lines[4]=12
	lines[5]=int(max_number_of_plots_per_page/2+0.5)+4
	lines[6]=int(max_number_of_plots_per_page/2+0.5)+4
	jj=0
	jjj=0
	colors=[]
	markers=[',', '+', '.', 'o', '*']
	mm=0
	rec=[]
	x3d=[]
	y3d=[]
	z3d=[]
	dx=[]
	dy=[]
	dz=[]
	labx3d=[]
	laby3d=[]
	mode=''
	
	cl=0
	if p>1:
		jj=0
		jjj=0
	if p==6:
		jjj=2
	if p==4 or p==5:
		jmax=21 
	else:
		jmax=2

	title_main=""
	if p==1:
		title_main="\n\n            Peptide length population and amino acid composition of Family "+str(tag)+"            \n\n"
		fig=plt.figure(figsize=(30,50))
	if p==2:
		title_main="Family amino acid composition per position (3D version)\n\n"
		title_fig2="\n\nPercental ocurrence of aminoacid types along aminoacid-position along\nthe sequence of the peptide representing the family. In case the\nparameter n_region has been set, the percentage refers to different\n regions of the sequence (r:xy, where x and y are the starting\n and the ending aminoacid position in the peptide sequence)\n\n"   
		fig=plt.figure(figsize=(int(sizeh*1.8),int(sizeh*1.8)))
	if p==3:
		title_main="\n\n            Family amino acid composition per position (2D version)            \n\n"
		title_fig3="Percental ocurrence of aminoacid types along aminoacid-position along the sequence of\nthe peptide representing the family. In case the parameter n_region has been set, the\npercentage refers to different regions of the sequence (r:xy, where x and y are the starting\nand the ending amino-acid position in the sequence)\n\n"
		fig=plt.figure(figsize=(int(sizeh+10),65))
	if p==4:
		title_main="\n\nMotifs: \n  Position-independent probability to find a residue type at a certain  \n distance from a reference residue type\n" 
		title_fig4="e.g. if an Alanine (A) is present one peptide of the family, the graph relative to A\nreports on the probability to find each residue type n positions (or regions) apart.\n"
		fig=plt.figure(figsize=(int(sizeh+10),65))
	if p==5:
		title_main="\n\nMotifs:\n  Position-dependent probability to find a residue type at a certain  \n distance from a residue type in position n\n"
		title_fig5="e.g. if the position 6 of the family is more often occupied by an A, the graph relative to position 6 reports\non the probability to find each residue type n positions (or regions) apart.\n\nAt the bottom of this page is the list of best representative peptides (scaffolds) obtained starting from the\nmost abundant aminoacid at each positions.\
All other positions are filled with the most abundant neighbour\nin the subgroup of peptides having the selected aminoacid in the specified position\n\ne.g. if Alanine is the most abundant aminoacid in position 6, the sixth peptide of the list is obtained by\nplacing the most abundant aminoacids in each position within the family subgroup of peptides having A in\nposition 6.\n"
		fig=plt.figure(figsize=(int(sizeh+10),sizev[1]))
		ff=1
		count_plots=0
	if p==6:
		title_main="\n\n   Optimal peptide scaffold:\n\n  Each peptide scaffold of Fig 5 is tested to represent the full family.   \n"
		title_fig6="At the bottom of this page is the list of best representative peptides (scaffolds) obtained starting from the\nmost abundant aminoacid at each positions.\
All other positions are filled with the most\nabundant neighbour in the subgroup of peptides having the selected aminoacid in the specified position\ne.g. if Alanine is the most abundant aminoacid in position 6, the sixth peptide of the list is obtained by\nplacing the most abundant aminoacids in each position withing the family subgroup of peptides having A\nin position 6.\n\nIn this figure we report the position-dependent probability to find each residue type of each scaffold\nsequence within the peptides of the family.\ne.g. The scaffold peptide creating the largest ocurrence integral is the one that better represents the family.\n\nThe largest graph in the figure shows the superimposition of all curves as a function of sequence\nposition.\n"
		fig=plt.figure(figsize=(int(sizeh+10),sizev[1]))
		ff=1
		count_plots=0
	if p==5 or p==6:
		if p==5:	
			fig.suptitle(title_main, fontsize=36, fontweight='bold',color='blue')
			plt.figtext(0.5, 0.82,title_fig5, wrap=True,horizontalalignment='center', fontsize=26,color='#4B82C5')
		if p==6:
			fig.suptitle(title_main, fontsize=36, fontweight='bold',color='blue')
			plt.figtext(0.5, 0.82,title_fig6, wrap=True,horizontalalignment='center', fontsize=26,color='#4B82C5')
	else:
		if p==1:
			fig.suptitle(title_main, fontsize=36, fontweight='bold', color='#4B82C5')
			plt.figtext(0.5, 0.78,title_all, wrap=True,horizontalalignment='center', fontsize=16,color='#4B82C5')
		else:
			fig.suptitle(title_main, fontsize=36, fontweight='bold', color='blue')
			if p==2:
				plt.figtext(0.1, 0.82,title_fig2, wrap=True,horizontalalignment='left', fontsize=26,color='#4B82C5')
			else:
				if p==3 or p==4:
					if p==3:
						plt.figtext(0.5, 0.89,title_fig3, wrap=True,horizontalalignment='center', fontsize=26,color='#4B82C5')
					if p==4:
						plt.figtext(0.5, 0.90,title_fig4, wrap=True,horizontalalignment='center', fontsize=26,color='#4B82C5')
					color_aa=iter(cm.rainbow(np.linspace(0,1,20)))
					color_aa_legend=iter(cm.rainbow(np.linspace(0,1,20)))
					cc=0
					for cc in range(1,21):
						string_label=' ('+str(aa_name[cc])+')'+str(aa_label[cc])
						plt.figtext(0.45, 0.884-cc*0.005,string_label, wrap=True,horizontalalignment='left', fontsize=26,color=next(color_aa_legend))
                # 'red', 'orange', 'brown', 'green', 'blue', 'purple', 'black'
	ii=0
	for i in range(1,ppmax+4):
		ii=ii+1
		color=iter(cm.rainbow(np.linspace(0,1,20)))
		marker = itertools.cycle((',', '+', '.', 'o', '*'))
		for j in range(1,jmax):
			l=0
			if p==1 or p==2 or p==3 :
				temp='tmp_files/tmp_'+str(p)+'_'+str(i)+'_'+str(tag)+'_'+str(calculation_label)
			if p==4 or p==5 :
				temp='tmp_files/tmp_'+str(p)+'_'+str(i)+'_'+str(j)+'_'+str(tag)+'_'+str(calculation_label)
			if p==5 :
				temp='tmp_files2/tmp_'+str(p)+'_'+str(i)+'_'+str(j)+'_'+str(tag)+'_'+str(calculation_label)
			if p==6 :
				temp='tmp_files2/tmp_'+str(p)+'_'+str(i)+'_'+str(tag)+'_'+str(calculation_label)
			if os.path.exists(temp):
				fo=open(temp,'r')
				for m in fo.readlines():
					l=l+1
					dlr=m.split()
					if l==1:
						labx=str(dlr[0])
						laby=str(dlr[1])
						tit=str(dlr[2])
						if '3d' in m:
							mode='3d'
						else:
							mode=''
					else:
						if l==2:
							first=int(dlr[0])
						if mode=='3d':
							cl=cl+1
							if cl>20:
								cl=1
							colors.append(color_graph[cl])
							rec.append(plt.Rectangle((0, 0), 1, 1, fc=color_graph[cl]))
							x3d.append(int(dlr[0])-0.5)
							y3d.append(int(dlr[1])-0.5)
							z3d.append(float(0))
							dx.append(int(1))
							dy.append(int(1))
							dz.append(float(dlr[2]))
							if int(dlr[0])==1:
								laby3d.append(dlr[4])
							if int(dlr[1])==1:
								labx3d.append(dlr[3])
						else:
							x[l-1]=int(dlr[0])
							y[l-1]=float(dlr[1])
							label[l-2]=str(dlr[2])
						last=int(dlr[0])
						#if last>50:
						#	last=50
				if mode=='3d':
					# FIXME: 3dbar representations are overlapped from time to time. Is a known bug:
					# https://matplotlib.org/mpl_toolkits/mplot3d/faq.html
					ax = subplot(lines[p],1,1, projection='3d')
					ax.bar3d(x3d,y3d,z3d,dx,dy,dz,color=colors) # bar3d doesn't honor alpha setting: ,alpha=0.8)
					#ax.set_xlabel(labx3d,fontsize=6)
					#ax.set_ylabel(laby3d,fontsize=6)
					ax.set_xticks(np.arange(first, last+1, 1))
					ax.set_xticklabels(labx3d,rotation=90,fontsize=26)
					ax.set_yticks(np.arange(1.5, 21.5, 1))
					ax.set_yticklabels(laby3d,rotation=0,fontsize=26)
					labz3d=np.arange(0,100, 20)
					ax.set_zticklabels(labz3d,rotation=0,fontsize=26)
					ax.set_xlabel('\n\n\n\nSequence position/region     ',fontsize=36)
					ax.set_ylabel('\n\nAmino-acid type',fontsize=36)
					ax.set_zlabel('\nPercental ocurrence',fontsize=36)
					# FIXME: ortho axes will be available from 2.1.0 matplotlib version
					# https://matplotlib.org/users/whats_new.html#orthographic-projection-for-mplot3d
					ax.set_proj_type('ortho')
					ax.view_init(azim=-45, elev=35)
					#savefig('graphs3d.png',dpi=450)
					ax.legend(rec,laby3d,fontsize=20)
				else:
					ax1=subplot(lines[p],2,ii+4+jjj);
					x1 = np.fromiter( (x[j] for j in range(first,last+1)), dtype="float" )
					y1=np.fromiter( (y[j] for j in range(first,last+1)), dtype="float" )
					plt.xlim(first-0.5,last+0.5)
					plt.ylim(0,y1.max()*1.05)
					if '%' in laby:
						plt.ylim(0,110)
					if p==4 or p==5:
						plt.plot(x1,y1,c=next(color),label=aa_name[j], marker = next(marker), linestyle='-')
						if i==1:
							plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
					else: 
						if p==6:
							#cmap = mpl.cm.autumn
							cmap=mpl.cm.rainbow
							if ff==1:
								mm=mm+1
								if mm>4:
									mm=1
								ax2 = subplot(10,1,2)
								plt.plot(x1,y1,c=cmap(i/float(ppmax+4)), marker = markers[mm], label=str(i),linestyle='-')
								plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

							ax1 = subplot(lines[p],2,ii+4+jjj)
							plt.plot(x1,y1,c=cmap(i/float(ppmax+4)), marker = markers[mm], label=str(i),linestyle='-')
						else:
                                                        #data = [go.Histogram(x=x)]
                                                        #py.iplot(data, filename='basic histogram')
							if p==1:
								jj=jj+1
								plt.bar(x1,y1,1,color=color_first_graph[jj],align='center')
							else:
								plt.bar(x1,y1,1,color=next(color_aa),align='center')
					xlabel(labx,fontsize=16, color='black');ylabel(laby,fontsize=16, color='black');
					#title(tit,fontsize=12, color='black')
					plt.title(tit,fontsize=12)
					if i==7 and p==1:
						jjj=1
					if i==1 and p==1:
						yticks(fontsize=16)	
					else:
						xticks(x1,label,rotation=90,fontsize=16);yticks(fontsize=16)
				fo.close()
		if p==5 or p==6:
			count_plots=int(count_plots)+1
			if int(count_plots)==int(max_number_of_plots_per_page):
				count_plots=0
				plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=1.2)
				fig.suptitle(title_main+'(Part '+str(ff)+')', fontsize=36, fontweight='bold',color='blue')
				#if p==5:
				#	plt.figtext(0.1, 0.82,title_fig5, wrap=True,horizontalalignment='left', fontsize=26,color='#4B82C5')
				#if p==6:
				#	plt.figtext(0.1, 0.82,title_fig6, wrap=True,horizontalalignment='left', fontsize=26,color='#4B82C5')
				plt.figtext(0.5, 0.030,title_sequences[ff], wrap=True,horizontalalignment='center', fontsize=26,color='#4B82C5')
				savedir='OUTPUTS/'+str(calculation_label)+'/Graphics/f'+str(tag)+'/'
				if not os.path.isdir(savedir):
					os.makedirs(savedir)
				savefig(savedir+'Graph'+str(p)+'_'+str(ff)+'_fam'+str(tag)+'.svg',bbox_inches='tight',pad_inches = 1.5);#,bbox_inches='tight');
				plt.close(fig)
				ff=ff+1
				if sizev[ff]<60:
					sizev[ff]=60
				fig=plt.figure(figsize=(sizeh,sizev[ff]))
				fig.suptitle(title_main+'(Part '+str(ff)+')', fontsize=36, fontweight='bold',color='blue')
				#if p==5:
				#	plt.figtext(0.1, 0.82,title_fig5, wrap=True,horizontalalignment='left', fontsize=26,color='#4B82C5')
				#if p==6:
				#	plt.figtext(0.1, 0.82,title_fig6, wrap=True,horizontalalignment='left', fontsize=26,color='#4B82C5')
				ii=0
				jjj=0
	#tight_layout();
	#plt.title(title_all,fontsize=8)
	plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=1.2)
	# Breaks 6th graph
	# https://stackoverflow.com/questions/22734068/matplotlib-tight-layout-causing-runtimeerror
#	plt.tight_layout()
#	plt.tight_layout(pad=0.4, h_pad=1.0, w_pad=0.5)
	# Available formats: eps, jpeg, jpg, pdf, pgf, png, ps, raw, rgba, svg, svgz, tif, tiff
	# eps looks to be the fastest with any dpi; bbox_inches='tight' makes all look nicer but a bit slower
	#savefig('OUTPUTS/'+str(calculation_label)+'_graphs'+str(p)+'fam'+str(tag)+'.png',dpi=450);
#	savefig('OUTPUTS/'+str(calculation_label)+'_graphs'+str(p)+'fam'+str(tag)+'.eps',dpi=450);#,bbox_inches='tight');
	if p==5 or p==6:
		plt.figtext(0.5, 0.1,title_sequences[ff], wrap=True,horizontalalignment='center', fontsize=26,color='#4B82C5')
	else:
		ff=1
	savedir='OUTPUTS/'+str(calculation_label)+'/Graphics/f'+str(tag)+'/'
	if not os.path.isdir(savedir):
		os.makedirs(savedir)
	savefig(savedir+'Graph'+str(p)+'_'+str(ff)+'_fam'+str(tag)+'.svg',bbox_inches='tight',pad_inches = 1.5);
	plt.close(fig)

# Run all the analysis in parallel
# n_jobs: If -1 all CPUs are used. If 1 is given, no parallel computing code is used at all, which is useful for debugging. For n_jobs below -1, (n_cpus + 1 + n_jobs) are used. Thus for n_jobs = -2, all CPUs but one are used.
# FIXME: it seems we could be affected by https://bugs.python.org/issue6721 :
# https://stackoverflow.com/questions/39884898/large-amount-of-multiprocessing-process-causing-deadlock ->Under heavy load (like compiling lots of things) it tends to get randomly stalled after "savefig" run... the only workaround is to force n_jobs=1 to disable parallel
Parallel(n_jobs=-1,verbose=0)(delayed(analysis)(p) for p in range(1, 7))
