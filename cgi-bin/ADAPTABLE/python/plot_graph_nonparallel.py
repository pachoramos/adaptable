#!/usr/bin/env python3
import os
import tempfile
os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()
from matplotlib.pyplot import cm 
import itertools
import os
import os.path
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import sys
#from joblib import Parallel, delayed

filename = os.environ.get('PYTHONSTARTUP')
if filename and os.path.isfile(filename):
    execfile(filename)

from numpy import loadtxt
from pylab import plot, show, xlabel, ylabel, subplot, tight_layout, savefig, title, xticks, yticks
x=[0] * 400
y=[0] * 400
label= [0] * 400
lines=[0] * 80
color_graph= [0] * 80
color_graph[1]='b'
color_graph[2]='g'
color_graph[3]='r'
color_graph[4]='c'
color_graph[5]='m'
color_graph[6]='y'
color_graph[7]='k'
color_graph[8]='w'
color_graph[9]='b'
color_graph[10]='g'
color_graph[11]='r'
color_graph[12]='c'
color_graph[13]='m'
color_graph[14]='y'
color_graph[15]='k'
color_graph[16]='w'
color_graph[17]='b'
color_graph[18]='g'
color_graph[19]='b'
color_graph[20]='g'
aa_name=[0] * 21
aa_name[1]="A"; aa_name[2]="R";aa_name[3]="N";aa_name[4]="D";aa_name[5]="C";aa_name[6]="Q";aa_name[7]="E";aa_name[8]="G";aa_name[9]="H";aa_name[10]="I";
aa_name[11]="L";aa_name[12]="K";aa_name[13]="M";aa_name[14]="F";aa_name[15]="P";aa_name[16]="S";aa_name[17]="T";aa_name[18]="W";aa_name[19]="Y";aa_name[20]="V";
lines[1]=10
lines[2]=4
lines[3]=12
lines[4]=12
lines[5]=17
lines[6]=18
title_all=""
title_sequences=""
calculation_label=sys.argv[1]

fo=open('tmp_files/tmp0_'+str(calculation_label),'r')
for m in fo.readlines():
	title_all=title_all+m+"\n"
	if 'Family' in m:
		dlr=m.split()
		tag=dlr[1]
fo.close() 
fo=open('tmp_files/tmp00_'+str(calculation_label),'r')
for m in fo.readlines():
        title_sequences=title_sequences+m+"\n"
fo.close()

jjj=0
colors=[]
markers=[',', '+', '.', 'o', '*']
mm=0
rec=[]
x3d=[]
y3d=[]
z3d=[]
dx=[]
dy=[]
dz=[]
labx3d=[]
laby3d=[]
mode=''

for p in range(1,7):
	cl=0
	if p>1:
		 jjj=0
	if p==6:
		jjj=2
	if p==4 or p==5:
		jmax=21 
	else:
		jmax=2
	fig=plt.figure(figsize=(16,35))
	if p==5:
		fig.suptitle(title_sequences, fontsize=6, fontweight='bold')
	else:
		fig.suptitle(title_all, fontsize=10, fontweight='bold')
	for i in range(1,31):
		color=iter(cm.rainbow(np.linspace(0,1,20)))
		marker = itertools.cycle((',', '+', '.', 'o', '*'))
		for j in range(1,jmax):
			l=0
			if p==1 or p==2 or p==3 :
				temp='tmp_files/tmp_'+str(p)+'_'+str(i)+'_'+str(tag)+'_'+str(calculation_label)
			if p==4 or p==5:
				temp='tmp_files/tmp_'+str(p)+'_'+str(i)+'_'+str(j)+'_'+str(tag)+'_'+str(calculation_label)
			if p==5 :
				temp='tmp_files2/tmp_'+str(p)+'_'+str(i)+'_'+str(j)+'_'+str(tag)+'_'+str(calculation_label)
			if p==6 :
				temp='tmp_files2/tmp_'+str(p)+'_'+str(i)+'_'+str(tag)+'_'+str(calculation_label)
			if os.path.exists(temp):
				fo=open(temp,'r')
				for m in fo.readlines():
					l=l+1
					dlr=m.split()
					if l==1:
						labx=str(dlr[0])
						laby=str(dlr[1])
						tit=str(dlr[2])
						if '3d' in m:
							mode='3d'
						else:
							mode=''
					else:
						if l==2:
							first=int(dlr[0])
						if mode=='3d':
							cl=cl+1
							if cl>20:
								cl=1
							colors.append(color_graph[cl])
							rec.append(plt.Rectangle((0, 0), 1, 1, fc=color_graph[cl]))
							x3d.append(int(dlr[0])-0.5)
							y3d.append(int(dlr[1])-0.5)
							z3d.append(float(0))
							dx.append(int(1))
							dy.append(int(1))
							dz.append(float(dlr[2]))
							if int(dlr[0])==1:
								laby3d.append(dlr[4])
							if int(dlr[1])==1:
								labx3d.append(dlr[3])
						else:
							x[l-1]=int(dlr[0])
							y[l-1]=float(dlr[1])
							label[l-2]=str(dlr[2])
						last=int(dlr[0])
						#if last>50:
						#	last=50
				if mode=='3d':
					ax = subplot(lines[p],1,2, projection='3d')
					ax.bar3d(x3d,y3d,z3d,dx,dy,dz,color=colors,alpha=0.8)
					#ax.set_xlabel(labx3d,fontsize=6)
					#ax.set_ylabel(laby3d,fontsize=6)
					ax.set_xticks(np.arange(first, last+1, 1))
					ax.set_xticklabels(labx3d,rotation=90,fontsize=10)
					ax.set_yticks(np.arange(1.5, 21.5, 1))
					ax.set_yticklabels(laby3d,rotation=0,fontsize=10)
					ax.view_init(azim=-45, elev=65)
					#savefig('graphs3d.png',dpi=450)
					ax.legend(rec,laby3d)
				else:
					ax1=subplot(lines[p],2,i+4+jjj);
					x1 = np.fromiter( (x[j] for j in range(first,last+1)), dtype="float" )
					y1=np.fromiter( (y[j] for j in range(first,last+1)), dtype="float" )
					plt.xlim(first-0.5,last+0.5)
					plt.ylim(0,y1.max()*1.05)
					if '%' in laby:
						plt.ylim(0,100)
					if p==4 or p==5:
						plt.plot(x1,y1,c=next(color),label=aa_name[j], marker = next(marker), linestyle='-')
						if i==1:
							plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
					else: 
						if p==6:
							#cmap = mpl.cm.autumn
							cmap=mpl.cm.rainbow
							mm=mm+1
							if mm>4:
								mm=1
							ax2 = subplot(10,1,2)
							plt.plot(x1,y1,c=cmap(i/float(31)), marker = markers[mm], label=str(i),linestyle='-')
							plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
							ax1=subplot(lines[p],2,i+4+jjj)
							plt.plot(x1,y1,c=cmap(i/float(31)), marker = markers[mm], label=str(i),linestyle='-')
						else:
							plt.bar(x1,y1,1,color=color_graph[j],align='center')
					xlabel(labx,fontsize=8, color='black');ylabel(laby,fontsize=8, color='black');
					#title(tit,fontsize=12, color='black')
					plt.title(tit,fontsize=12)
					if i==7 and p==1:
						jjj=1
					if i==1 and p==1:
						yticks(fontsize=8)	
					else:
						xticks(x1,label,rotation=90,fontsize=8);yticks(fontsize=8)
			fo.close()
	#tight_layout();
	#plt.title(title_all,fontsize=8)
	plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=0.5)
	# Available formats: eps, jpeg, jpg, pdf, pgf, png, ps, raw, rgba, svg, svgz, tif, tiff
	# eps looks to be the fastest with any dpi; bbox_inches='tight' makes all look nicer but a bit slower
	#savefig('OUTPUTS/'+str(calculation_label)+'_graphs'+str(p)+'fam'+str(tag)+'.png',dpi=450);
	savefig('OUTPUTS/'+str(calculation_label)+'_graphs'+str(p)+'fam'+str(tag)+'.eps',dpi=450);#,bbox_inches='tight');
	plt.close(fig)
