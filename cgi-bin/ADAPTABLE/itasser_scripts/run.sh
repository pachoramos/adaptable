#!/bin/bash
# Prepare sequences, we need to create dirs with numbers instead of reusing sequence because there is a limit in the length of the directory name
bash ./prepare_seqs.sh

# Go to ITasser directory (libs in the same dir)
#cd /run/media/pacho/fbd481e5-9238-4e43-8482-cabb31b6cdfc/I-TASSER5.1/I-TASSERmod/sequences
# FIXME: Better on a SSD device as it relies on fast I/O
cd /home/pacho/I-TASSER5.1/I-TASSERmod/sequences

# Run in parallel (for pkgdir and libdir it wants absolute PATHs
for i in $(cat seq_num); do
	../runI-TASSER.pl -runstyle gnuparallel -pkgdir /home/pacho/I-TASSER5.1/ -libdir /home/pacho/I-TASSER5.1/ -LBS true -EC true -GO true -seqname $(tail -n1 ${i}/seq.fasta) -datadir ./${i} -light true -outdir ./${i}
done

# FIXME: doesn't parallelize well... try to drain more our CPUs, runstyle serial looks to perform well with this, but you can try gnuparallel too if you have enough memory
parallel --compress --jobs +1 "cd '{}' ; ../../runI-TASSER.pl -runstyle serial -pkgdir /home/pacho/I-TASSER5.1/ -libdir /home/pacho/I-TASSER5.1/ -LBS true -EC true -GO true -seqname '{}' -datadir ./ -light true -outdir ./" ::: $(cat ./seq_num)