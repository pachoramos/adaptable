#!/usr/bin/env python2
docstring='''file2html.py datadir
    convert I-TASSER suite output result to user friendly html results
    "datadir" is input and output directory of an I-TASSER job

output files:
    model[1-5].png (image of I-TASSER models)
    index.html     (html webpage output)
    result.tar.bz2 (result tarball)
'''
import os,sys
import subprocess
import shutil
import re
import textwrap
import tarfile
import bz2

from configure import initdat2txt,lscore2png,pymol_exe,ITLIB
from html_template import rcsb_link,zhanglab_link,ec_link,go_link
from html_template import zscore0_dict,coach_dict
from html_template import html_template,model_png_template,lomets_template
from html_template import tmalign_template,tmalign_cell_template
from html_template import function_template
from html_template import lbs_template,lbs_cell_template
from html_template import ec_template,ec_cell_template
from html_template import go_template,aspect_template,cell_template,go_term_template,gosearch_template

def make_html(jobID,target_name,sequence,
    seq_dat_ss="seq.ss",sol_txt="exp.dat",initdat="init.dat",
    model_pdb_list=["model1.pdb"],model_png_list=["model1.png"],
    cscore_txt='cscore',
    similarpdb_model1="similarpdb_model1.lst",
    Bsites_inf="Bsites.inf",EC_dat="EC.dat",
    GO_dat="GOsearchresult_model1.dat",
    MF_dat="GO_MF.dat",BP_dat="GO_BP.dat",CC_dat="GO_CC.dat"):
    '''make html text for job'''

    ## parse I-TASSER output files ##
    scale=make_scale(len(sequence),interval=20,use_html=True)
    ss_pred,ss_cscore=psipred2seq(seq_dat_ss,use_html=True)
    sa_pred=solvent_accessbility2seq(sol_txt)
    L=len(sequence)
    
    ## parse LOMETS output ##
    lomets_table,ss=make_lomets_table(initdat)
    
    ## parse final models ##
    model_num=len(model_pdb_list)
    fp=open(cscore_txt)
    txt=fp.read()
    fp.close()
    model_cell=''
    for i in range(2,model_num+1):
        model_line=[line for line in txt.splitlines() if \
            line.lower().startswith("model%u"%i)][0].split()
        model_cell+=model_png_template.substitute(dict(
            I=str(i), CSCORE=model_line[1]))
    model_line=[line for line in txt.splitlines() if \
            line.lower().startswith("model1")][0].split()

    tmalign_table=''
    if os.path.isfile(similarpdb_model1):
        tmalign_table=make_tmalign_table(similarpdb_model1)

    function_table=make_function_table(Bsites_inf,EC_dat,
        GO_dat,MF_dat,BP_dat,CC_dat)

    ## make html text ##
    html_txt=html_template.substitute(dict(
        ZHANGLAB_LINK=zhanglab_link,
        JOBID=jobID, TARGET=target_name, LEN=L,
        WRAP_SEQUENCE="<br>\n".join(textwrap.wrap(sequence,60)),
        
        SCALE=scale,SEQUENCE=sequence,
        SS_PRED=ss_pred,SS_CSCORE=ss_cscore,

        SA_PRED=sa_pred,

        SS=ss, LOMETS_TABLE=lomets_table,

        MODEL_NUM=model_num,MODEL_CELL=model_cell,
        CSCORE=model_line[1],
        TMSCORE=model_line[2].split('+-')[0],
        dTMSCORE=model_line[2].split('+-')[1],
        RMSD=model_line[3].split('+-')[0],
        dRMSD=model_line[3].split('+-')[1],

        TMALIGN_TABLE=tmalign_table,
        FUNCTION_TABLE=function_table,
    ))
    return html_txt

def make_function_table(Bsites_inf="Bsites.inf",EC_dat="EC.dat",
    GO_dat="GOsearchresult_model1.dat",
    MF_dat="GO_MF.dat",BP_dat="GO_BP.dat",CC_dat="GO_CC.dat"):
    '''convert COACH and COFACTOR output to HTML table'''
    if not os.path.isfile(Bsites_inf) and not os.path.isfile(EC_dat) and \
       not os.path.isfile(GO_dat) and not os.path.isfile(MF_dat) and \
       not os.path.isfile(BP_dat) and not os.path.isfile(CC_dat):
        return ''

    lbs_table=''
    if os.path.isfile(Bsites_inf):
        lbs_cell=''
        fp=open(Bsites_inf,'rU')
        txt=fp.read()
        fp.close()
        read_site=False
        for line in txt.splitlines():
            if line.startswith("SITE"):
                line=line.split()
                read_site=True

                i=line[1]
                cscore=line[2]
                cluster_size=line[3]
            elif read_site==True:
                line=line.split(':')
                resi=line[-1]

                line=line[0].split()
                method=coach_dict[line[0]]
                chain=line[1]
                bs=line[2]
                
                mult_file="model1/coach/CH_complex%s.pdb"%i

                rep_file=mult_file
                if method in ['CF','TM']:
                    rep_file="model1/coach/CH_%s_%s_%s.pdb"%(method,chain,bs)
                elif method in ['FN']:
                    rep_file="model1/coach/CH_%s_%s.pdb"%(method,bs)
                    mult_file="model1/coach/CH_complex%s.pdb"%i

                lbs_cell+=lbs_cell_template.substitute(dict(
                    ZHANGLAB_LINK=zhanglab_link,I=i,
                    CSCORE=cscore,CLUSTER_SIZE=cluster_size,
                    PDB=chain[:4],LIG=bs.split('_')[-1],CHAIN=chain,
                    REP_FILE=rep_file,MULT_FILE=mult_file,RESI=resi,
                ))

                read_site=False


        lbs_table=lbs_template.substitute(dict(
            ZHANGLAB_LINK=zhanglab_link,
            LBS_CELL=lbs_cell,
        ))

    ec_table=''
    if os.path.isfile(EC_dat):
        ec_cell=''
        fp=open(EC_dat,'rU')
        txt=fp.read()
        fp.close()

        for idx,line in enumerate(txt.strip().splitlines()):
            line=line.split()
            ec_cell+=ec_cell_template.substitute(
                I=str(idx+1),
                CSCORE="%.3f"%float(line[5]),
                RCSB_LINK=rcsb_link,
                PDB=line[0][:4],
                CHAIN=line[0],
                TMSCORE="%.3f"%float(line[1]),
                RMSD="%.2f"%float(line[2]),
                ID="%.3f"%float(line[3]),
                COV="%.3f"%float(line[4]),
                BS=line[7],
                EC_LINK=ec_link,
                EC=line[6],
            )
        ec_table=ec_template.substitute(dict(EC_CELL=ec_cell))

    go_table=''
    if os.path.isfile(GO_dat) or os.path.isfile(MF_dat) or \
       os.path.isfile(BP_dat) or os.path.isfile(CC_dat):
        go_name_dict=dict() # mapping between go id and name

        mf_table=''
        if os.path.isfile(MF_dat):
            fp=open(MF_dat,'rU')
            txt=fp.read()
            fp.close()
            goterm_cell=''
            cscore_cell=''
            for line in txt.strip().splitlines():
                line=line.split()
                goterm_cell+=cell_template.substitute(dict(
                    CONTENT=go_term_template.substitute(dict(GO=line[0],
                        NAME=' '.join(line[2:]), GO_LINK=go_link))))
                cscore_cell+=cell_template.substitute(dict(
                    CONTENT=line[1]))
                go_name_dict[line[0]]=' '.join(line[2:])
            mf_table=aspect_template.substitute(dict(
                ASPECT="Molecular Function",
                GOTERM_CELL=goterm_cell, CSCORE_CELL=cscore_cell))

        bp_table=''
        if os.path.isfile(BP_dat):
            fp=open(BP_dat,'rU')
            txt=fp.read()
            fp.close()
            goterm_cell=''
            cscore_cell=''
            for line in txt.strip().splitlines():
                line=line.split()
                goterm_cell+=cell_template.substitute(dict(
                    CONTENT=go_term_template.substitute(dict(GO=line[0],
                        NAME=' '.join(line[2:]), GO_LINK=go_link))))
                cscore_cell+=cell_template.substitute(dict(
                    CONTENT=line[1]))
                go_name_dict[line[0]]=' '.join(line[2:])
            bp_table=aspect_template.substitute(dict(
                ASPECT="Biological Processes",
                GOTERM_CELL=goterm_cell, CSCORE_CELL=cscore_cell ))

        cc_table=''
        if os.path.isfile(CC_dat):
            fp=open(CC_dat,'rU')
            txt=fp.read()
            fp.close()
            goterm_cell=''
            cscore_cell=''
            for line in txt.strip().splitlines():
                line=line.split()
                goterm_cell+=cell_template.substitute(dict(
                    CONTENT=go_term_template.substitute(dict(GO=line[0],
                        NAME=' '.join(line[2:]), GO_LINK=go_link))))
                cscore_cell+=cell_template.substitute(dict(
                    CONTENT=line[1]))
                go_name_dict[line[0]]=' '.join(line[2:])
            cc_table=aspect_template.substitute(dict(
                ASPECT="Cellular Component",
                GOTERM_CELL=goterm_cell, CSCORE_CELL=cscore_cell,
            ))

        gosearch_table=''
        if os.path.isfile(GO_dat):
            fp=open(GO_dat)
            txt=fp.read()
            fp.close()
            for i,line in enumerate(txt.strip().splitlines()):
                line=line.split(':')
                go_list=[]
                for Term in ':'.join(line[1:]).strip().split(','):
                    name=''
                    if Term in go_name_dict:
                        name=go_name_dict[Term]
                    go_list.append(go_term_template.substitute(dict(
                        GO=Term,NAME=name,GO_LINK=go_link)))
                line=line[0].split()
                gosearch_table+=gosearch_template.substitute(dict(
                    I=i,
                    CSCORE="%.2f"%float(line[5]),
                    TMSCORE="%.3f"%float(line[1]),
                    RMSD="%.2f"%float(line[2]),
                    ID="%.2f"%float(line[3]),
                    COV="%.2f"%float(line[4]),
                    RCSB_LINK=rcsb_link,
                    PDB=line[0][:4],
                    CHAIN=line[0],
                    GO_LIST=' '.join(go_list),
                ))

        go_table=go_template.substitute(dict(
            MF_TABLE=mf_table,
            BP_TABLE=bp_table,
            CC_TABLE=cc_table,
            GOSEARCH_TABLE=gosearch_table,
        ))

    html_txt=function_template.substitute(dict(
        ZHANGLAB_LINK=zhanglab_link,
        LBS_TABLE=lbs_table,
        EC_TABLE=ec_table,
        GO_TABLE=go_table,
        ))
    return html_txt

def make_tmalign_table(similarpdb_model1="similarpdb_model1.lst"):
    '''convert Gsearch tabular output to HTML table'''
    fp=open(similarpdb_model1,'rU')
    txt=fp.read()
    fp.close()
    tmalign_cell=''
    for idx,line in enumerate(txt.strip().splitlines()):
        line=line.split()
        tmalign_cell+=tmalign_cell_template.substitute(dict(
            I=str(idx+1),
            RCSB_LINK=rcsb_link,
            PDB=line[0][:4],
            CHAIN=line[0],
            TMSCORE="%.3f"%float(line[1]),
            RMSD="%.2f"%float(line[2]),
            ID="%.3f"%float(line[3]),
            COV="%.3f"%float(line[4]),
        ))

    html_txt=tmalign_template.substitute(dict(
        ZHANGLAB_LINK=zhanglab_link,
        TMALIGN_CELL=tmalign_cell,
    ))
    return html_txt

def make_lomets_table(initdat="init.dat"):
    '''convert LOMETS tabular output to HTML table'''
    html_txt=''

    txt=id_pl(initdat)
    lines=txt.splitlines()
    sequence=lines[3].split()[1]
    ss=lines[4].split()[1 # secondary structure
        ].replace('-','C'
        ).replace('CH','C<font color="red">H'
        ).replace('EH','E</font><font color="red">H'
        ).replace('HC','H</font>C'
        ).replace('CE','C<font color="blue">E'
        ).replace('HE','H</font><font color="blue">E'
        ).replace('EC','H</font>C') 
    scale=make_scale(len(sequence),interval=20,use_html=True)

    L=len(sequence)
    template_pattern=re.compile("^\s*(\d+)\s+(\d+)\s+([-\w]+)\s+(\w+)\s+([-.\d]+)/\s*([-.\d]+)\*{0,1}\s+\(\s*(\d+)/\s*\d+\)=[.\d]+\s+\(\s*\d+/\s*\d+\)=[.\d]+\s+([-*A-Z]+)$")
    threader_list=[]
    for line in lines:
        line_list=template_pattern.findall(line)
        if not line_list:
            continue
        i,L_ali,threader,template,zscore,zscore0,n_id,template_seq=line_list[0]
        norm_zscore=1
        if float(zscore0)>0:
            norm_zscore=float(zscore)/float(zscore0)
        for T in zscore0_dict:
            if T.startswith(threader) and zscore0_dict[T]==float(zscore0):
                threader=T
        threader_list.append(threader)

        html_txt+=lomets_template.safe_substitute(dict(
            I=i,
            RCSB_LINK=rcsb_link,
            PDB=template[:4],
            CHAIN=template,
            ID1="%.2f"%(float(n_id)/float(L_ali)),
            ID2="%.2f"%(float(n_id)/float(L)),
            COV="%.2f"%(float(len(template_seq.replace('-','')))/L),
            ZSCORE="%.2f"%norm_zscore,
            THREADER=threader,
            SEQ=template_seq,
        ))

    datadir=os.path.dirname(initdat)
    for idx,threader in enumerate(threader_list):
        threading_pdb=os.path.join(datadir,"threading%d.pdb"%(idx+1))
        threader_pdb=os.path.join(datadir,"threading_%s_%d.pdb"%(threader,
            sum([T==threader for T in threader_list[:idx]])+1))
        if os.path.isfile(threader_pdb):
            shutil.copy(threader_pdb,threading_pdb)
    return html_txt,ss

def solvent_accessbility2seq(sol_file="exp.dat"):
    '''convert QUARK format solvent accessbility prediction file "sol_file"
    to string of scores, where 0 is buried and 9 is exposed.
    '''
    sa_pred=''
    if not os.path.isfile(sol_file):
        return ''
    fp=open(sol_file)
    txt=fp.read()
    fp.close()
    for line in txt.splitlines()[1:]:
        sa_pred+="%1d"%(sum(map(int,line.strip().split()[1:]))*0.5)
    return sa_pred

def psipred2seq(psipred_file,use_html=False):
    '''convert PSIPRED format three state secondary structure prediction
    to string of prediction. return two strings, the first string is the
    predicted secondary structure (Helix, Strand, Coil) for each position.
    the second string is the confidence score (0 - 9) for each positions.
    to'''
    if not os.path.isfile(psipred_file):
        return '',''
    ss_pred=''
    ss_cscore=''
    psipred_pattern=re.compile(
        "^\s*\d+\s+[A-Z]\s+([HCE])\s+([.\d]+)\s+([.\d]+)\s+([.\d]+)\s*$")
    
    fp=open(psipred_file,'rU')
    txt=fp.read()
    fp.close()
    
    ss,c,h,e=zip(*[psipred_pattern.findall(line)[0] for line in \
        txt.splitlines() if psipred_pattern.match(line)])

    ss_pred=''.join(ss).replace('E','S')
    ss_cscore=[int(max(map(float,[c1,c2,c3]))*10) for c1,c2,c3 in zip(c,h,e)]
    ss_cscore=''.join([str(min([s,9])) for s in ss_cscore])

    if use_html:
        ss_pred=ss_pred.replace('CH','C<font color="red">H'
                      ).replace('SH','S</font><font color="red">H'
                      ).replace('HC','H</font>C'
                      ).replace('CS','C<font color="blue">S'
                      ).replace('HS','H</font><font color="blue">S'
                      ).replace('SC','S</font>C')
        if ss_pred.startswith('H'):
            ss_pred='<font color="red">'+ss_pred
        if ss_pred.startswith('S'):
            ss_pred='<font color="blue">'+ss_pred
        if not ss_pred.endswith('C'):
            ss_pred+='</font>'
    return ss_pred,ss_cscore

def make_scale(L=0,interval=20,use_html=False):
    '''make horizontal residue index scale to mark every "interval" positions
    for a sequence with "L" postions. 
    use_html - whether to format text to html format'''
    txt=''
    marker_num=int(L/interval)
    for i in range(marker_num):
        txt+=' '*(interval-len(str((i+1)*interval)))+str((i+1)*interval)
    txt+='\n'+(' '*(interval-1)+'|')*marker_num
    if use_html:
        txt=txt.replace('\n','<br>\n').replace(' ','&nbsp;')
    return txt

def pdb2png(pdb_in="model1.pdb",png_out="model1.png"):
    '''convert pdb structure file 'pdb_in' to png image file 'png_out' '''
    cmd=pymol_exe+' -c -d "load %s;hide all;show cartoon;spectrum count,rainbow;save %s"'%(pdb_in,png_out)
    os.system(cmd)
    return os.path.isfile(png_out)

def id_pl(initdat="init.dat"):
    '''convert LOMETS threading output 'init.dat' to text'''
    cmd='cd '+os.path.dirname(initdat)+';'+ \
        initdat2txt+' '+initdat+' 0 10' # only the top 10 templates
    p=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
    stdout,stderr=p.communicate()
    return stdout

def read_single_sequence_from_fasta(seq_txt="seq.txt"):
    '''read single sequence file "seq_txt". return header and sequence'''
    target_name=''
    sequence=''
    fp=open(seq_txt)
    for line in fp.read().splitlines():
        if line.startswith('>'):
            target_name+=line.lstrip('>').strip().split()[0]
        else:
            sequence+=line.strip()
    fp.close()
    return target_name,sequence

def file2html(datadir='.'):
    '''display I-TASSER output folder to user friendly HTML results'''
    ## parse directory structure ##
    datadir=os.path.abspath(datadir)
    jobID=os.path.basename(datadir)
    coachdir=os.path.join(datadir,"model1/coach")
    cofactordir=os.path.join(datadir,"model1/cofactor")

    ## parse user input files ##
    seq_txt=os.path.join(datadir,"seq.txt") # FASTA sequence
    if not os.path.isfile(seq_txt):
       seq_txt=os.path.join(datadir,"seq.fasta") # FASTA sequence

    ## parse I-TASSER simulation input feature files ##
    target_name,sequence=read_single_sequence_from_fasta(seq_txt)
    sol_txt=os.path.join(datadir,"exp.dat") # solvant accessibility
    seq_dat_ss=os.path.join(datadir,"seq.ss") # PSSpred prediction
    initdat=os.path.join(datadir,"init.dat") # lomets threading

    ## parse final structure model ##
    model_pdb_list=[]
    model_png_list=[]
    for i in range(1,11):
        model_pdb=os.path.join(datadir,"model%d.pdb"%i)
        if not os.path.isfile(model_pdb):
            break
        model_png=os.path.join(datadir,"model%d.png"%i)
        model_pdb_list.append(model_pdb)
        model_png_list.append(model_png)
        pdb2png(pdb_in=model_pdb,png_out=model_png)

    ## parse clustering and refinement files ##
    cscore_txt=os.path.join(datadir,"cscore") # confidence score of fold
    lscore_txt=os.path.join(datadir,"lscore.txt") # ResQ
    BFP_png=os.path.join(datadir,"BFP.png") # image of ResQ predicted Bfactor
    os.system(lscore2png+' '+lscore_txt+' '+BFP_png)

    ## parse COFACTOR function prediction files ##
    similarpdb_model1=os.path.join(cofactordir,"similarpdb_model1.lst")
    EC_dat=os.path.join(cofactordir,"ECsearchresult_model1.dat")
    GO_dat=os.path.join(cofactordir,"GOsearchresult_model1.dat")
    MF_dat=os.path.join(cofactordir,"GOsearchresult_model1_MF.dat")
    BP_dat=os.path.join(cofactordir,"GOsearchresult_model1_BP.dat")
    CC_dat=os.path.join(cofactordir,"GOsearchresult_model1_CC.dat")

    ## parse COACH binding site prediction files ##
    Bsites_inf=os.path.join(coachdir,"Bsites.inf")
    Bsites_clr=os.path.join(coachdir,"Bsites.clr")
    coach_pdb_list=[]
    coach_pdb_pattern=re.compile("CH_\w+_\w+_\w+.pdb")
    coach_complex_pattern=re.compile("CH_complex\d+.pdb")
    if os.path.isdir(coachdir):
        for coach_pdb in os.listdir(coachdir):
            if coach_pdb_pattern.match(coach_pdb) or \
               coach_complex_pattern.match(coach_pdb):
                coach_pdb_list.append(os.path.join(coachdir,coach_pdb))

    ## make result tarball ##
    index_html=os.path.join(datadir,"index.html")
    html_txt=make_html(jobID,target_name,sequence,
        seq_dat_ss,sol_txt,initdat,
        model_pdb_list,model_png_list,cscore_txt,
        similarpdb_model1,
        Bsites_inf,EC_dat,GO_dat,MF_dat,BP_dat,CC_dat,
        )
    fp=open(index_html,'w')
    fp.write(html_txt)
    fp.close()
    result_tarball=os.path.join(datadir,"result.tar.bz2")
    fp=tarfile.open(result_tarball,'w:bz2')
    fp.add(index_html,jobID+"_results/index.html")
    fp.add(seq_txt, jobID+"_results/seq.fasta")
    if os.path.isfile(cscore_txt):
        fp.add(cscore_txt, jobID+"_results/cscore")
    if os.path.isfile(lscore_txt):
        fp.add(lscore_txt, jobID+"_results/lscore.txt")
        if os.path.isfile(BFP_png):
            fp.add(BFP_png,jobID+"_results/BFP.png")
    for idx,model_pdb in enumerate(model_pdb_list):
        fp.add(model_pdb,jobID+"_results/model%d.pdb"%(1+idx))
    for idx,model_png in enumerate(model_png_list):
        fp.add(model_png,jobID+"_results/model%s.png"%(1+idx))
    if os.path.isfile(Bsites_inf):
        fp.add(Bsites_inf,jobID+"_results/model1/coach/Bsites.inf")
    if os.path.isfile(Bsites_clr):
        fp.add(Bsites_clr,jobID+"_results/model1/coach/Bsites.clr")
    for coach_pdb in coach_pdb_list:
        fp.add(coach_pdb,jobID+"_results/model1/coach/"+os.path.basename(coach_pdb))
    for i in range(1,11):
        threading_pdb=os.path.join(datadir,"threading%d.pdb"%i)
        if os.path.isfile(threading_pdb):
            fp.add(threading_pdb,jobID+"_results/"+os.path.basename(threading_pdb))
    if os.path.isfile(EC_dat):
        fp.add(EC_dat,jobID+"_results/model1/coach/EC.dat")
    if os.path.isfile(MF_dat):
        fp.add(MF_dat,jobID+"_results/model1/coach/GO_MF.dat")
    if os.path.isfile(BP_dat):
        fp.add(BP_dat,jobID+"_results/model1/coach/GO_BP.dat")
    if os.path.isfile(CC_dat):
        fp.add(CC_dat,jobID+"_results/model1/coach/GO_CC.dat")
    if os.path.isfile(similarpdb_model1):
        fp.add(similarpdb_model1,jobID+"_results/model1/cofactor/similarpdb_model1.lst")
    fp.close()
    return index_html,result_tarball

def check_pymol_exe(pymol_exe):
    '''check if pymol exists'''
    p=subprocess.Popen("which "+pymol_exe,shell=True,stdout=subprocess.PIPE)
    stdout,stderr=p.communicate()
    if not stdout:
        sys.stderr.write('''ERROR! Cannot locate PyMOL executable %s
Change 'pymol_exe' variable at 'configure.py' to the path of PyMOL'''
        %pymol_exe)
        return ''
    else:
        return stdout.split()[-1]

def check_libdir(ITLIB):
    '''check if I-TASSER library is correct'''
    if os.getenv("ITLIB"):
        ITLIB=os.getenv("ITLIB")
    if not os.path.isfile(ITLIB+"/PDB/list"):
        sys.stderr.write('''ERROR! Cannot locate %s/PDB/list
Please change 'ITLIB' variable at 'configure.py' to the path of I-TASSER library'''
        %ITLIB)
        return ''
    else:
        return os.path.abspath(ITLIB)

if __name__=="__main__":
    if len(sys.argv)!=2:
        sys.stderr.write(docstring)
        exit()
    check_pymol_exe(pymol_exe)
    check_libdir(ITLIB)
    
    file_list=file2html(sys.argv[1])
    for f in file_list:
        sys.stdout.write(f+'\n')
