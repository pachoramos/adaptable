#!/usr/bin/env python
'''html templates'''
from string import Template

## pdb entry website
rcsb_link="http://www.rcsb.org/pdb/explore/explore.do?structureId="

## zhanglab website
zhanglab_link="http://zhanglab.ccmb.med.umich.edu"

## enzyme comission website
ec_link="http://enzyme.expasy.org/EC"

## GO term entry website
go_link="https://www.ebi.ac.uk/QuickGO/GTerm?id="

## lomets zscore cutoff
zscore0_dict={
    'MUSTER':5.8,
    'dPPAS':9.3,
    'wdPPAS':9.0,
    'wPPAS':7.5,
    'wMUSTER':6.0,
    'dPPAS2':10.5,
    'PPAS':7.0,
    'Env-PPAS':8.0,
}

## coach subprograms
coach_dict={
    'COF':'CF', # cofactor
    'SST':'',   # S-site
    'TMS':'TM', # TM-site
    'FIN':'FN',   # findsite
}

## template for index.html
html_template=Template('''<html>
<head><meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>I-TASSER results</title></head>
<body>
<font face="Times New Roman">
[<a href="$ZHANGLAB_LINK">Home</a>]
[<a href="$ZHANGLAB_LINK/I-TASSER">Server</a>]
[<a href="$ZHANGLAB_LINK/I-TASSER/about.html">About</a>]
[<a href="$ZHANGLAB_LINK/I-TASSER/statistics.html">Statistics</a>]
[<a href="$ZHANGLAB_LINK/I-TASSER/annotation/">Annotation</a>]
<br>
<h1 align="center">I-TASSER results for job id $JOBID</h1>
<h4 align="center">[Click on <a href="result.tar.bz2" download>
    result.tar.bz2</a> to download the tarball file including all modelling
    results listed on this page]</h4>
<font size="4" face="Arial">

<div style="background:#6599FF;width:350px;"><b><font face="Arial" 
color="#FFFFFF" size=4>&nbsp;Input Sequence in FASTA format</font></b></div>
<ul>
    <table style="font-family:Monospace;font-size:14px;background:#F2F2F2;">
    <tr><td colspan="2">&gt;$TARGET ($LEN residues)<br>
    $WRAP_SEQUENCE<br>
    </td></tr>
    </table><br>
</ul>

<div style="background:#6599FF;width:300px;"><b><font face="Arial" 
color="#FFFFFF" size=4>&nbsp;Predicted Secondary Structure</font></b></div>
<ul>
    <table border="0" style="font-family:Monospace;font-size:14px;background:#F2F2F2;">
    <tr><td valign="bottom"><font="Arial"><b>Sequence</b></font></td><td>
    $SCALE<br>
    $SEQUENCE</td></tr>
    <tr><td><font="Arial"><span title="Predicted Secondary Structure">
    <b>Prediction</b></span></font></td><td>$SS_PRED</td></tr>
    <tr><td><font="Arial"><span title="Confidence Score of Prediction 
    (0 being least confident, 9 being most confident)">
    <b>Conf.Score</b></span></font></td><td>$SS_CSCORE</td></tr>
    <tr><td><font="Arial"></td><td><font color=red>H</font>:Helix; 
    <font color=blue>S</font>:Strand; C:Coil</td><tr>
    </table>
</ul>

<div style="background:#6599FF;width:300px;"><b><font face="Arial" 
color="#FFFFFF" size=4>&nbsp;Predicted Solvent Accessibility</font></b></div>
<ul>
    <table border="0" style="font-family:Monospace;font-size:14px;background:#F2F2F2;">
    <tr><td valign="bottom"><font="Arial"><b>Sequence</b></font></td><td>
    $SCALE<br>
    $SEQUENCE</td></tr>
    <tr><td><font="Arial"><span title="Solvent Accessibility Prediction">
    <b>Prediction</b></span></font></td><td>$SA_PRED</td></tr>
    <tr><td><font="Arial"></td><td>Values range from <b>0</b> (buried residue) 
    to <b>8</b> (highly exposed residue)</td><tr>
    </table>
</ul>

<div style="background:#6599FF;width:300px;"><b><font face="Arial" 
color="#FFFFFF" size=4>&nbsp;Predicted Normalized B-facotr</font></b></div>
<ul>
<font size=2><p>
(B-factor is a value to indicate the extent of the inherent thermal 
mobility of residues/atoms in proteins. In I-TASSER, this value is 
deduced from threading template proteins from the PDB in combination with the 
sequence profiles derived from sequence databases. The reported B-factor 
profile in the figure below corresponds to the normalized B-factor of the 
target protein, defined by B=(B'-u)/s, where B' is the raw B-factor 
value, u and s are respectively the mean and standard deviation of the raw 
B-factors along the sequence. 
(<a href="lscore.txt" target="_blank">Click here to read more about predicted 
normalized B-factor</a>)</p><br>
</font>
    <table border="0" style="font-family:Monospace;font-size:14px;background:#F2F2F2;">
    <tr><td align=center><img src=BFP.png height=200></td></tr>
    </table>
</ul>


<div style="background:#6599FF;width:400px;"><b><font face="Arial" 
color="#FFFFFF" size=4>&nbsp;Top 10 threading templates used by I-TASSER</font></b></div>
<ul>
    <table border="0" style="font-family:Monospace;font-size:14px;background:#F2F2F2;">
    <tr><th align="left" valign="top"><font face="Arial">Rank</font></th> <th align="center" valign="top"><font face="Arial">PDB<br/>Hit</font></th><th align="center" valign="top"><font face="Arial">Iden1</font></th><th align="center" valign="top"><font face="Arial">Iden2</font></th><th valign="top"><font face="Arial">Cov</font></th><th valign="top"><font face="Arial">Norm.<br/>Zscore</font></th><th valign="top"><font face="Arial">Download</br>Align.</font></th><td>&nbsp;</td><td BGCOLOR="#DEDEDE" style="font-family:Monospace;font-size:13px;">$SCALE</td></tr>
    <tr><td colspan="7"></td><th align="right" valign ="bottom" ><font face="Arial">Sec.Str<br/>Seq</font></th><td BGCOLOR="#D7D7D7" style="font-family:Monospace;font-size:13px;">$SS<br/>$SEQUENCE</td></tr>

$LOMETS_TABLE

    <tr><td colspan="9" align="left">
        <table cellspacing="2" border="0" width="1000px" style="font-family:Arial;font-size:12px;">
        <tr><td valign="top">(a)</td><td align="justify"><i>Iden1</i> is the number of template residues identical to query divided by number of aligned residues.</td></tr>
        <tr><td valign="top">(b)</td><td align="justify"><i>Iden2</i> is the number of template residues identical to query divided by query sequence length.</td></tr>
        <tr><td valign="top">(c)</td><td align="justify"><i>Cov</i> is equal the number of aligned template residues divided by query sequence length.</td></tr>
        <tr><td valign="top">(d)</td><td align="justify"><i>Norm. Zscore</i> is the normalized Z-score of the threading alignments. A Normalized Z-score >1 means a good alignment.</td></tr>
        <tr><td valign="top">(e)</td><td align="justify"><i>Download Align.</i> list the threading program</a> used to identify the template provide the 3D structure of aligned regions of threading templates.</td></tr>
        </table>
    </table>
</ul>


<div style="background:#6599FF;width:380px;"><b><font face="Arial" 
color="#FFFFFF" size=4>&nbsp;Top $MODEL_NUM final models predicted by I-TASSER</font></b></div>
<font size=2 face="Times New Roman"><br>
(For each target, I-TASSER simulations generate a large ensemble of
structural conformations, called decoys. To select the final models, I-TASSER uses the SPICKER program to cluster 
all the decoys based on the pair-wise structure similarity, and reports up to five models which 
corresponds to the five largest structure clusters. The confidence of each model is quantitatively 
measured by C-score that is calculated based on the significance of threading template alignments 
and the convergence parameters of the structure assembly simulations. 
C-score is typically in the range of [-5, 2], where a C-score of higher value signifies a model with a high 
confidence and vice-versa. TM-score and RMSD are estimated based on C-score and protein length following 
the correlation observed between these qualities. Since the top 5 models are ranked by the cluster size, 
it is possible that the lower-rank models have a higher C-score in
rare cases. Although the first model has a better 
quality in most cases, it is also possible that the lower-rank models have a better quality than the 
higher-rank models as seen in our benchmark tests. If the I-TASSER simulations converge, it 
is possible to have less than 5 clusters generated. This is usually an indication that the 
models have a good quality because of the converged simulations.)
</font>
<ul>
    <table border="0" style="font-size:14px;background:#F2F2F2;">

    <td align ="left">
    <li><a href="cscore">More about C-score</a>
    <li><a href="lscore.txt">Local structure accuracy of first model</a>
    <img src="model1.png" style="display: block;" width="350"></br>
    <a href="model1.pdb" download>Download Model 1</a>
    <li>C-score=$CSCORE
    <li>Estimated TM-score = $TMSCORE&plusmn;$dTMSCORE
    <li>Estimated RMSD = $RMSD&plusmn;$dRMSD&Aring;
    </td>
    $MODEL_CELL
    </table>
</ul>

$TMALIGN_TABLE

$FUNCTION_TABLE

</font>
<h4 align="center">[Click on <a href="result.tar.bz2" download>
    result.tar.bz2</a> to download the tarball file including all modelling
    results listed on this page]</h4>
<hr><br/>
<div style="position:relative;left:5px;">
    <table style="font-family:Arial;font-size:14px;">
    <tr><td colspan=2>Please cite the following articles when you use the I-TASSER server:</td><tr>
    <tr><td valign="top">1.</td><td> J Yang, R Yan, A Roy, D Xu, J Poisson, Y Zhang. The I-TASSER Suite: Protein structure and function prediction. Nature Methods, 12: 7-8, 2015.</td></tr>
    <tr><td valign="top">2.</td><td> J Yang, Y Zhang. I-TASSER server: new development for protein structure and function predictions, Nucleic Acids Research, 43: W174-W181, 2015.</td></tr>
    <tr><td valign="top">3.</td><td>A Roy, A Kucukural, Y Zhang. I-TASSER: a unified platform for automated protein structure and function prediction. Nature Protocols, 5: 725-738, 2010.</td></tr>
    <tr><td valign="top">4.</td><td>Y Zhang. I-TASSER server for protein 3D structure prediction.  BMC Bioinformatics, 9: 40, 2008.</td></tr>
    </table>
</div><br>
</body></html>
''') # ZHANGLAB_LINK - zhanglab website
     # JOBID     - jobID
     # TARGET    - target name
     # LEN       - sequence length
     # WRAP_SEQUENCE - wrapping sequence into 60 residues per line

     # SCALE     - scale to mark residue index every 20 position
     # SEQUENCE  - HTML format sequence
     # SS_PRED   - string of predicted secondary structure
     # SS_CSCORE - string of secondary structure prediction [1-10]

     # LOMETS_TABLE - HTML format table showing LOMETS threading result

     # MODEL_NUM - final model number
     # MODEL_CELL- HTML text for cells to display model structures

     # CSCORE   - cscore of model1
     # TMSCORE  - estimated TM-score of model1
     # dTMSCORE - estimated TM-score deviation of model1
     # RMSD     - estimated RMSD of model1
     # dRMSD    - estimated RMSD deviation of model1

     # TMALIGN_TABLE - HTML format table showing Gsearch result
     # FUNCTION_TABLE - HTML format table showing COACH & COFACTOR result

## for model 2-5
model_png_template=Template('''
    <td align ="left">
    <img src="model$I.png" style="display: block;" width="350"></br>
    <a href="model$I.pdb" download>Download Model $I</a>
    <li>C-score=$CSCORE
    </td>
''') # I      - model index [2-5]
     # CSCORE - cscore

## template for individual lomets template
lomets_template=Template('''    <tr><td align="center"><font face="Arial">$I</font></td><td><a href="$RCSB_LINK$PDB">$CHAIN</a></td><td align="center"><font face="Arial"> $ID1</font></td><td><font face="Arial"> $ID2</font></td><td><font face="Arial"> $COV</font></td><td align="center"><font face="Arial"> $ZSCORE</font></td><td align="center"><font face="Monospace"><a href="threading$I.pdb">$THREADER</a></font></td><td>&nbsp;</td><td BGCOLOR="#ECECEG" style="font-family:Monospace;font-size:13px;">$SEQ</td></tr>
''') # I         - rank
     # RCSB_LINK - http link to pdb entry
     # PDB       - PDB id
     # CHAIN     - PDB chain ID
     # ID1       - identity of aligned region
     # ID2       - indeitiy over the full query length
     # COV       - sequence coverage
     # THREADER  - threading program (short name)
     # SEQ       - sequence

## template for structure analogs idenitified by TMalign
tmalign_template=Template('''
<div style="background:#6599FF;width:650px;"><b><font face="Arial" 
color="#FFFFFF" size=4>&nbsp;Proteins structureally close to the target in PDB (as identified by <a href="$ZHANGLAB_LINK/TM-align" target="_blank">TM-align</a></font></b></div>
<font size=2 face="Times New Roman"><p>
(After the structure assembly simulation, I-TASSER uses the TM-align
structural alignment program to match the first I-TASSER model to all
structures in the PDB library. This section reports the top 10 proteins
from the PDB that have the closest structural similarity, i.e. the highest
<a href=$ZHANGLAB_LINK/TM-score>TM-score</a>, to the predicted I-TASSER 
model. Due to the structural similarity, these proteins often have similar
function to the target. However, users are encouraged to use the data in the
next section 'Predicted function using COACH' to infer the function of the 
target protein, since COACH has been extensively trained to derive biological
functions from multi-source of sequence and structure features which has on
average a higher accuracy than the function annotations derived only from
the global structure comparison.)</p><br>
</font>
<ul>
<table border="0" style="font-family:Monospace;font-size:14px;background:#F2F2F2;">
<tr>
    <td align="center">
    <!-- place holder for TMalign image-->
    </td>
    
    <td valign="top"><b>Top 10 Identified stuctural analogs in PDB</b><br/><br/>
    <table border="0" style="font-family:Arial;font-size:13px;">
    <tr bgcolor="#DEDEDE"><th >Rank</th><th >PDB Hit</th><th >TM-score</th><th >RMSD<sup>a</sup></th><th >IDEN<sup>a</sup></th><th>Cov</th></tr>

$TMALIGN_CELL

    </table><br/>
    <table border="0" cellspacing="2" style="font-family:Arial;font-size:12px;background:#F2F2F2;">
        <tr><td valign="top">(a)</td><td align="justify">Query structure is shown in cartoon, while the structural analog is displayed using backbone trace.</td></tr>
        <tr><td valign="top">(b)</td><td align="justify">Ranking of proteins is based on TM-score of the structural alignment between the query structure and known structures in the PDB library.</td></tr>
        <tr><td valign="top">(c)</td><td align="justify">RMSD<sup>a</sup> is the RMSD between residues that are structurally aligned by TM-align.</td></tr>
        <tr><td valign="top">(d)</td><td align="justify">IDEN<sup>a</sup> is the percentage sequence identity in the structurally aligned region.</td></tr>
        <tr><td valign="top">(e)</td><td align="justify">Cov represents the coverage of the alignment by TM-align and is equal to the number of structurally aligned residues divided by length of the query protein.</td></tr>
    </table>
    </td>
</tr>
</table>
</ul>
''') # ZHANGLAB_LINK - zhanglab website
     # TMALIGN_CELL  - HTML text fir cells ti display TMalign statistics

## template for individual TMalign identified structure analog
tmalign_cell_template=Template('''    <td valign="middle" align="center">$I</td><td valign="middle"><a href="$RCSB_LINK$PDB" target="_blank">$CHAIN</a></td><td align="center">$TMSCORE</td><td valign="middle" align="center">$RMSD</td><td valign="middle" align="center">$ID</td><td valign="middle">$COV</td><td align="center"></td></tr>
''') # I         - rank
     # RCSB_LINK - http link to pdb entry
     # PDB       - PDB id
     # CHAIN     - PDB chain ID
     # TMSCORE   - TM-score
     # RMSD      - aligned region RMSD
     # ID        - aligned region seqID
     # COV       - sequence coverage

## template for predicted function
function_template=Template('''
<div style="background:#6599FF;width:300px;"><b><font face="Arial" 
color="#FFFFFF" size=4>&nbsp;Predicted function using <a href="$ZHANGLAB_LINK/COACH" target="_blank">COACH</a></font></b></div>
<font size=2 face="Times New Roman"><p>
(This section reports biological annotations of the target 
protein by COACH based on the I-TASSER structure prediction. 
COACH is a meta-server approach that combines multiple function
annotation results from the COFACTOR, TM-SITE and S-SITE programs.)</p><br>
</font>
$LBS_TABLE
$EC_TABLE
$GO_TABLE
</div>
''') # ZHANGLAB_LINK - zhanglab website
     # LBS_TABLE     - ligand binding site prediction table
     # EC_TABLE      - EC prediction table
     # GO_TABLE      - GO prediction table

## template for ligand binding site prediction
lbs_template=Template('''
<div style="position:relative;left:30px;background:#FF9900; width:280px;">
<table><tr><td  valign="middle;">
<font color="#FFFFFF" size="4" face="Arial">&nbsp;&nbsp;Ligand binding sites</font>
</td></tr></table>
</div><p></p>

<div style="position:relative;left:30px;background:#F2F2F2;width:1200px">
<table  style="width:1200px;background:#F2F2F2;" border="0" cellspacing="5" cellpadding="0" align="left">
<tr>
    <table border="0"  style="font-family:Arial;font-size:13px;">

    <tr bgcolor="#DEDEDE">
        <th>Rank</th>
        <th>C-score</th>
        <th>Cluster<br>size</th>
        <th>PDB<br>Hit</th>
        <th>Lig<br>Name</th>
        <th>Download<br>Complex</th>
        <th align=left>Ligand Binding Site Residues</th>
    </tr>

$LBS_CELL
    </table></br>

    <table  cellspacing="2" border="0" style="font-family:Arial;font-size:12px;">
        <tr><td valign="top"></td><td align="justify"><a href=model1/coach/Bsites.inf target="_blank">Download</a> the all possible binding ligands and detailed prediction summary. </td></tr> 
        <tr><td valign="top"></td><td align="justify"><a href=model1/coach/Bsites.clr target="_blank">Download</a> the templates clustering results. </td></tr> 
        <tr><td valign="top">(a)</td><td align="justify"><b>C-score</b> is the confidence score of the prediction. C-score ranges [0-1], where a higher score indicates a more reliable prediction. </td></tr>
        <tr><td valign="top">(b)</td><td align="justify"><b>Cluster size</b> is the total number of templates in a cluster. </td></tr>
        <tr><td valign="top">(c)</td><td align="justify"><b>Lig Name</b> is name of possible binding ligand. Click the name to view its information in <a href=$ZHANGLAB_LINK/BioLiP/>the BioLiP database</a>.</td></tr>
        <tr><td valign="top">(d)</td><td align="justify"><b>Rep</b> is a single complex structure with the most representative ligand in the cluster, i.e., the one listed in the <b>Lig Name</b> column. <br>
        <b>Mult</b> is the complex structures with all potential binding ligands in the cluster.</td></tr>
    </table>

</tr>
</table>
</div>
<p></p>
''') # ZHANGLAB_LINK - zhanglab website
     # LBS_CELL      - ligand binding site cells

## template for individual ligand binding site
lbs_cell_template=Template('''    <tr><font face="Monospace"><td valign="middle" align="center">$I</td><td valign="middle" align="center">$CSCORE</td> <td valign="middle" align="center">$CLUSTER_SIZE</td> <td valign="middle" align="left"><a href=$ZHANGLAB_LINK/BioLiP/qsearch.cgi?pdbid=$PDB target="_blank">$CHAIN</a></td> <td valign="middle" align="left"><a href=$ZHANGLAB_LINK/BioLiP/sym.cgi?code=$LIG target="_blank">$LIG</a> </td>  <td valign="middle" align=center><a href="$REP_FILE" target="_blank">Rep</a>, <a href="model1/coach/CH_complex$I.pdb" target="_blank">Mult</a></td>  <td valign="middle" align="left">$RESI</td></tr>
''') # I             - rank
     # ZHANGLAB_LINK - zhanglab website
     # CSCORE        - confidence score
     # CLUSTER_SIZE  - cluster size
     # PDB           - lower case PDB id
     # CHAIN         - PDB chain id
     # LIG           - ligand
     # RESI          - comma seperated list of residue index
     # REP_FILE      - single complex representative ligand
     # MULT_FILE     - complex with all ligand in cluster

## template for EC prediction
ec_template=Template('''
<div style="position:relative;left:30px;background:#FF9900; width:450px;">
<table><tr><td  valign="middle;">
<font color="#FFFFFF" size="4" face="Arial">&nbsp;&nbsp;Enzyme Commission (EC) numbers and active sites</font>
</td></tr></table>
</div><p></p>

<div style="position:relative;left:30px;background:#F2F2F2;width:1200">
<table  style="width:1200px;background:#F2F2F2;" border="0" cellspacing="5" cellpadding="0" align="left">
<tr>
    <table  border="0" style="font-family:Arial;font-size:13px;"><tr bgcolor="#DEDEDE"> <th >Rank</th><th >Cscore<sup>EC</sup></th><th>PDB<br/>Hit</th><th >TM-score</th><th >RMSD<sup>a</sup></th><th >IDEN<sup>a</sup></th><th >Cov</th><th >EC Number</th><th align=left>Active Site Residues</th></tr>
$EC_CELL
    </table><br/>

    <table border="0"  cellspacing="2" border="0" style="font-family:Arial;font-size:12px;">
        <tr><td valign="top">(a)</td><td align="justify">Cscore<sup>EC</sup> is the confidence score for the EC number prediction. Cscore<sup>EC</sup> values range in between [0-1]; <br>where a higher score indicates a more reliable EC number prediction.</td></tr>
        <tr><td valign="top">(b)</td><td align="justify">TM-score is a measure of  global structural similarity between query and template protein.</td></tr>
        <tr><td valign="bottom">(c)</td><td align="justify">RMSD<sup>a</sup> is the RMSD between residues that are structurally aligned by TM-align.</td></tr>
        <tr><td valign="bottom">(d)</td><td align="justify">IDEN<sup>a</sup> is the percentage sequence identity in the structurally aligned region.</td></tr>
        <tr><td valign="top">(e)</td><td align="justify">Cov represents the coverage of global structural  alignment and is equal to the number of structurally aligned residues divided <br>by length of the query protein.</td></tr>
    </table>
</tr>
</table>
</div>
<p></p>
''') # EC_CELL - cell for individual EC prediction

## template for individual EC prediction
ec_cell_template=Template('''
       <tr><td align="left"><font face="Arial">$I</font></td><td align="center"><font face="Arial">$CSCORE</font></td><td><a href="$RCSB_LINK$PDB" target="_blank">$CHAIN</a></td><td align="center"><font face="Arial">$TMSCORE</font></td><td align="center"><font face="Arial">$RMSD</font></td><td align="center"><font face="Arial">$ID</font></td><td align="right"><font face="Arial">$COV</font></td><td align="left"><a href="$EC_LINK/$EC" target="_blank"><font face="Arial">$EC</font></a></td><td align="left"><font face="Arial">$BS</font></td></tr>
''') 
     # I         - rank
     # CSCORE    - CscoreEC
     # RCSB_LINK - http link to pdb entry
     # PDB       - PDB id
     # CHAIN     - PDB chain id
     # TMSCORE   - TM-score
     # RMSD      - RMSD of aligned region
     # ID        - aligned region seqID
     # COV       - coverage
     # BS        - active site
     # EC_LINK   - http link to Enzyme Commission number 
     # EC        - Enzyme Commission number

## template for Gene Ontology prediction
go_template=Template('''
<div style="position:relative;left:30px;background:#FF9900; width:280px;">
<table><tr><td  valign="middle;">
<font color="#FFFFFF" size="4" face="Arial">&nbsp;&nbsp;Gene Ontology (GO) terms</font>
</td></tr></table>
</div><p></p>


<div style="position:relative;left:30px;background:#F2F2F2;width:1200">
<table  style="width:1200px;background:#F2F2F2;" border="0" cellspacing="5" cellpadding="0" align="left">
<tr>
    <table border="0"  cellspacing="2" border="0" style="font-family:Arial;font-size:12px;">
    <tr background:#FF9900;><th colspan=8 align="left"><font face="Arial" size="3">Homologous GO templates in PDB</font><th><td>&nbsp;</td></tr>
    <tr bgcolor="#DEDEDE"><th ><font face="Arial">Rank</font></th><th><font face="Arial">Cscore<sup>GO</sup></font></th><th><font face="Arial">TM-score</font></th><th><font face="Arial">RMSD<sup>a</sup></font></th><th><font face="Arial">IDEN<sup>a</sup></font></th><th><font face="Arial">Cov</font></th><th align="left"><font face="Arial">PDB Hit</font></th><th align="left"><font face="Arial">Associated GO Terms</font></th></tr>
$GOSEARCH_TABLE
    </table> <p></p>

    <table border="0"  cellspacing="2" border="0" style="font-family:Arial;font-size:12px;">
    <tr ><th colspan=6 align="left"><font face="Arial" size="3"><br>Consensus prediction of GO terms</font><th><td>&nbsp;</td></tr>
    <tr><td>
    <table cellpadding="2" cellspacing="3" style="font-family:Monospace;font-size:13px;background:#F2F2F2">
$MF_TABLE
$BP_TABLE
$CC_TABLE
    </table>
    </td></tr>
    </table> <p></p>

    <table border="0"  cellspacing="2" border="0" style="font-family:Arial;font-size:12px;">
        <tr><td valign="top">(a)</td><td align="justify">Cscore<sup>GO</sup> is a combined measure for evaluating global and local similarity between query and template protein. It's range is [0-1] and higher values indicate more confident predictions.</td></tr>
        <tr><td valign="top">(b)</td><td align="justify">TM-score is a measure of  global structural similarity between query and template protein.</td></tr>
        <tr><td valign="top">(c)</td><td align="justify">RMSD<sup>a</sup> is the RMSD between residues that are structurally aligned by TM-align.</td></tr>
        <tr><td valign="top">(d)</td><td align="justify">IDEN<sup>a</sup> is the percentage sequence identity in the structurally aligned region.</td></tr>
        <tr><td valign="top">(e)</td><td align="justify">Cov represents the coverage of global structural alignment and is equal to the number of structurally aligned residues divided by length of the query protein.</td></tr>
        <tr><td valign="top">(f)</td><td align="justify">The second table shows a consensus GO terms amongst the top scoring templates. The GO-Score associated with each prediction is defined as the average weight of the GO term, where the weights are assigned based on Cscore<sup>GO</sup> of the template.</td></tr>
    </table>
</tr>
</div>
''') # MF_TABLE - concensus prediction for molecular function
     # BP_TABLE - consensus prediction for biological process
     # CC_TABLE - consensus prediction for cellular component
     # GOSEARCH_TABLE - GO search templates

## template for GOsearchresult_model1.dat
gosearch_template=Template('''
    <tr><td align="center" valign="middle"><font face="Arial">$I</font></td><td align="center" valign="middle"><font face="Arial">$CSCORE</font></td><td valign="middle" align="center"><font face="Arial">$TMSCORE</font></td><td align="center" valign="middle"><font face="Arial">$RMSD</font></td><td align="center" valign="middle"><font face="Arial">$ID</font></td><td valign="middle" align="center"><font face="Arial">$COV</font></td><td align="left" valign="middle"><a href="$RCSB_LINK$PDB" target="_blank">$CHAIN</a></td><td valign="middle"><font face="Arial">
    $GO_LIST
    </font></td></tr>
''') # I         - rank
     # CSCORE    - CscoreGO
     # TMSCORE   - TM-score
     # RMSD      - RMSD of aligned region
     # ID        - seqID of aligned region
     # COV       - sequence coverage
     # RCSB_LINK - website link to 
     # PDB       - PDB id
     # CHAIN     - PDB chain id
     # GO_LIST   - space seperated list of go terms


## template for GOsearchresult_model1_{MF,BP,CC}.dat
aspect_template=Template('''
        <tr bgcolor="#DEDEDE"><td><font face="Arial"><b>$ASPECT</b></font></td>
$GOTERM_CELL
        </tr>
        <tr><td ><font face="Arial"><b>GO-Score</b></font></td>
$CSCORE_CELL
        </tr>
''') # ASEPCT - GO namespace {MF,BP,CC}
     # GOTERM_CELL - cells for GO term
     # CSCORE_CELL - cells for cscore

## template for each cell in the table
cell_template=Template('''
            <td align="center"><font face="Arial">$CONTENT</font></td>
''') # CONTENT - text to be put into the cell

## template for individual GO term
go_term_template=Template(
    '''<span title="$NAME"><a href=$GO_LINK$GO target="_blank">$GO</a></span>'''
)   # NAME    - name
    # GO_LINK - http link to individual entry of QuickGO
    # GO      - GO id
