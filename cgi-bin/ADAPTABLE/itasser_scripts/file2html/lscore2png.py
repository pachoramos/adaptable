#!/usr/bin/env python
docstring='''lscore2png.py lscore.txt BFP.png
    plot ResQ format predicted normalized Bfactor to PNG format image
'''
import sys,os
import re

import matplotlib
matplotlib.use("agg")
from matplotlib import pylab as plt

lscore_pattern=re.compile(
    "^(\d+)\s+(\w)\s+\w\s+[-.\d]+\s+([-.\d]+)\s+[-.\d]+\s+[-.\d]+\s+[-.\d]+\s*")

def lscore2png(lscore_txt="lscore.txt",BFP_png="BFP.png"):
    '''plot ResQ format predicted normalized Bfactor "lscore_txt" to PNG 
    format image "BFP_png" '''
    fp=open(lscore_txt,'rU')
    lscore_lines=[lscore_pattern.findall(line)[0] for line in \
        fp.read().splitlines() if lscore_pattern.match(line)]
    fp.close()
    resi,ss,bfactor=zip(*lscore_lines)
    resi=list(map(int,resi))
    ss=list(ss)
    bfactor=list(map(float,bfactor))
    helix=[i for i,s in zip(resi,ss) if s=="H"]
    strand=[i for i,s in zip(resi,ss) if s in "SE"]
    coil=[i for i,s in zip(resi,ss) if s=="C"]

    max_bfactor=max(bfactor)
    min_bfactor=min(bfactor)

    fig= plt.figure(figsize=(30, 3))
    bfactor_plot,=plt.plot(resi,bfactor,'b')
    helix_plot,=plt.plot(helix,[min_bfactor-0.5]*len(helix),'r8')
    strand_plot,=plt.plot(strand,[min_bfactor-0.5]*len(strand),'gs')
    coil_plot,=plt.plot(coil,[min_bfactor-0.5]*len(coil),'k.')
    plt.legend([bfactor_plot,helix_plot,strand_plot,coil_plot],
        ["Normalized B-factor","Helix","Strand","Coil"],
        loc=9,ncol=4)
    plt.plot([0,max(resi)+1],[0,0],'k-')
    plt.axis([0,max(resi)+1,min_bfactor-1,max_bfactor+1])
    plt.xlabel("Residue Number\n")
    plt.ylabel("Normalzied B-factor")
    plt.savefig(BFP_png,bbox_inches="tight",dpi=300)
    plt.close()
    return os.path.abspath(BFP_png)

if __name__=="__main__":
    if len(sys.argv)<2:
        sys.stderr.write(docstring)
        exit()

    lscore_txt=sys.argv[1]
    BFP_png="BFP.png"
    if len(sys.argv)>2:
        BFP_png=sys.argv[2]

    lscore2png(lscore_txt,BFP_png)
