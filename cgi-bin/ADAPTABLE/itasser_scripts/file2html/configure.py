#!/usr/bin/env python
'''configuration file for file2html.py'''
import os

# change this to the location of PyMOL executable, e.g. /usr/bin/pymol
pymol_exe="pymol"

# change this to the location of I-TASSER library
ITLIB="/home/pacho/I-TASSER5.1"

# location of id.pl
initdat2txt=os.path.join(os.path.dirname(__file__),"id.pl")

# location of lscore2png.py
lscore2png=os.path.join(os.path.dirname(__file__),"lscore2png.py")
