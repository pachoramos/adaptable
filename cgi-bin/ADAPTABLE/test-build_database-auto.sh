#!/bin/bash
# At first you need to comment the last line in awks/build_database_auto.awk to prevent compressing of ss DB while running this.
#
# You need to append the output of this script to parallel as
# ./test-build_database-auto.sh | parallel --jobs +1 
for i in satpdb cancerPPD Hemolytik DRAMP HIPdb PhytoAMP Camp DBAASP APD InverPep ParaPep ANTISTAPHY DADP MilkAMPs BACTIBASE BAAMPS defensins conoserver AVP MAVP LAMP ADAM YADAMP Peptaibol CPP UniProt ; do
	echo "awk -f awks/build_database_auto.awk $i > test-build_database_$i"
done
