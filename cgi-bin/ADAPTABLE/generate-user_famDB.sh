#!/bin/bash
export calculation_label="$(head -n1 OUTPUTS/.out_parallel)"
cd "OUTPUTS/${calculation_label}"

shopt -s extglob
for i in .outputs/!(f0); do
	cat ${i}|ansi2txt|output_files="${i}" awk -f ../../awks/extract_family_info.awk -
done
