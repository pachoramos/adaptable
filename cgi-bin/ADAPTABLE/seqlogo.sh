#!/bin/bash
# Run seqlogo if requested
# Update to weblogo-3.5
export calculation_label="$(head -n1 OUTPUTS/.out_parallel)"
export seqlogo="$(sed -n '3{p;q}' OUTPUTS/.out_parallel)"

if [ "${seqlogo}" = "y" ]; then
	cd "OUTPUTS/${calculation_label}"
	mkdir -p Logos
	echo "Generating SeqLogo files..." >> "${__calculation_label}-output"
	# FIXME: For some reason weblogo is not able to use all CPU, I am not sure if the bottleneck is gs maybe...
	parallel --compress --jobs +5 "../../weblogo-3.6.0/weblogo -t 'Family {}' -s large -P '' --errorbars NO -F svg <"${calculation_label}_FASTA_OUTPUT_cut_f'{}'"> Logos/"${calculation_label}_seqlogo_f'{}'.svg"" ::: $(seq 1 $(cat .fam_num)) 1>>"${__calculation_label}-output" 2>>".${__calculation_label}-errors"
#	parallel --compress --jobs +1 "inkscape -z "logos/${calculation_label}_seqlogo_f'{}'.eps" --export-plain-svg="logos/${calculation_label}_seqlogo_f'{}'.svg"" ::: $(seq 1 $(cat .fam_num)) 1>>"${__calculation_label}-output" 2>>".${__calculation_label}-errors"
#	rm -f "${calculation_label}"_seqlogo_f*.eps
	echo "IndexOrderDefault Ascending Name" > Logos/.htaccess
else
	exit 0
fi
