#!/bin/bash
# Prepare sequences, we need to create dirs with numbers instead of reusing sequence because there is a limit in the length of the directory name
bash ./prepare_seqs.sh

# Go to ITasser directory (libs in the same dir)
cd /run/media/pacho/fbd481e5-9238-4e43-8482-cabb31b6cdfc/I-TASSER5.1/PSSpred/sequences

# Run in parallel
parallel --compress --jobs +1 "cd '{}' ; ../../mPSSpred.pl seq.txt /run/media/pacho/fbd481e5-9238-4e43-8482-cabb31b6cdfc/I-TASSER5.1/ /run/media/pacho/fbd481e5-9238-4e43-8482-cabb31b6cdfc/I-TASSER5.1/" ::: $(cat seq_num)

# We need to drop the first line of the seq.dat.ss files
sed -i -e '1d' */seq.dat.ss

#rm -f /home/pacho/public_html/cgi-bin/ADAPTABLE/DATABASES/PSSpred_DB/ss.txt
for x in */seq.dat.ss; do
	seq=$(awk '{printf $2}' ${x})
	dssp=$(awk '{printf $3}' ${x})
	echo "${seq}:${dssp}" >> /home/pacho/public_html/cgi-bin/ADAPTABLE/DATABASES/PSSpred_DB/ss.txt
done
