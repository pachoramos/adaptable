#!/bin/bash
if [ -z "${__my_peptide}" ]; then
	echo "No custom peptide set, skipping append..."
	exit 0
else
        if [[ ${__simplify_aminoacids} = "y" ]]; then
                simplified_peptide=$(echo "${__my_peptide}" | awk -f awks/simplify_aminoacids.awk)
                __my_peptide="${simplified_peptide}"
        fi

	# This could break with spaces as they would also be splitted, but web form doesn't allow it and we could even ask people to also use spaces
	# to concatenate peptides
#	for i in ${__my_peptide//,/ }; do
	# At this step, comma gets transformed to %2C character
	my_pep_num="1"
	for i in ${__my_peptide//%2C/ }; do
		prev_num=$(tail -n1 "OUTPUTS/${__calculation_label}/.fam_num")
		new_num=$((prev_num+1))
		echo "Appending user supplied peptide sequence: "${i}""
		echo "${new_num}" >> "OUTPUTS/${__calculation_label}/.fam_num"
		echo "${new_num} seq ${i}" >> "OUTPUTS/${__calculation_label}/selection"
		echo "${new_num} oseq ${i}" >> "OUTPUTS/${__calculation_label}/selection"
		echo "${new_num} prop_separator >My_Peptide_${my_pep_num}; ${i} >My_Peptide_${my_pep_num}; _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ " >> "OUTPUTS/${__calculation_label}/selection"
		echo "${new_num} names >My_Peptide_${my_pep_num}" >> "OUTPUTS/${__calculation_label}/selection"
		my_pep_num=$((my_pep_num+1))
	done
	# Allow to continue in the case user only wants to proceed with his appended peptides
	rm -f "OUTPUTS/${__calculation_label}/.empty"
fi

