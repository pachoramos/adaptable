# -*- coding: utf-8 -*-

# dictionnaires des degrÃ©s d'affinitÃ© de chaque acide aminÃ© avec les hÃ©lices alpha, feuillets bÃ©tas et coudes
Pa={'E':1.51, 'M':1.45, 'A':1.42, 'L':1.21, 'K':1.16, 'F':1.13, 'Q':1.11, 'W':1.08, 'I':1.08, 'V':1.06, 'D':1.01, 'H':1, 'R':0.98, 'T':0.83, 'S':0.77, 'C':0.7, 'Y':0.69, 'N':0.67, 'P':0.57, 'G':0.57}
Pb={'V':1.7, 'I':1.6, 'Y':1.47, 'F':1.38, 'W':1.37, 'L':1.3, 'C':1.19, 'T':1.19, 'Q':1.1, 'M':1.05, 'R':0.93, 'N':0.89, 'H':0.87, 'A':0.83, 'S':0.75, 'G':0.75, 'K':0.74, 'P':0.55, 'D':0.54, 'E':0.37}
Pc={'N':1.56, 'G':1.56, 'P':1.52, 'D':1.46, 'S':1.43, 'C':1.19, 'Y':1.14, 'K':1.01, 'Q':0.98, 'T':0.96, 'W':0.96, 'R':0.95, 'H':0.95, 'E':0.74, 'A':0.66, 'M':0.6, 'F':0.6, 'L':0.59, 'V':0.5, 'I':0.47}

# fonction qui copie la suite de caractÃ¨res de la sÃ©quence dans une seule string en enlevant les \n; on obtient ainsi une string avec la suite d'acides aminÃ©s.
def copyseq():
    nblignes = len(sequence) # compte le nombre de caractÃ¨res dans la sÃ©quence d'origine
    sequence2 = "" # crÃ©e une string vide afin de pouvoir rajouter les caractÃ¨res voulus un par un
    for lignes in range(nblignes):
        for char in sequence[lignes]: # controle tous les caractÃ¨res dans la sÃ©quence ...
            if char != "\n": # ... pour vÃ©rifier si ce n'est pas un retour Ã  la ligne ...
                sequence2 += char # ... et rajoute tous ceux qui ne sont pas "\n" dans une nouvelle string
    return sequence2

# Fonction qui calcule les scores (alpha, bÃ©ta et coude) de chaque acide aminÃ© comme expliquÃ© dans la mÃ©thode Chou-Fassman, en tenant compte des deux acides aminÃ©s qui suivent.
# !!! NE PREND PAS EN COMPTE LES 2 DERNIERS ACIDES AMINES car ils n'ont pas deux autres a.a. qui les suivent. Ce script ne tiendra donc pas compte de la structure des deux derniers a.a. Ceci dit, les derniers a.a. sont rarement dans une structure dÃ©finie !!!
def scores():
    global SCa # scores alpha pour chaque acide aminÃ© de la sÃ©quence
    global SCb # scores beta pour chaque acide aminÃ© de la sÃ©quence
    global SCc # scores coude pour chaque acide aminÃ© de la sÃ©quence
    SCa, SCb, SCc = [], [], []
    for n in range(len(sequence2)-3):# le -3 Ã©vite de tenir compte des 2 derniers acides aminÃ© qui n'ont pas de scores, afin de ne pas sortir du cadre de lecture
        SCa.append(Pa[sequence2[n]]+Pa[sequence2[n+1]]+Pa[sequence2[n+2]]+Pa[sequence2[n+3]])
        SCb.append(Pb[sequence2[n]]+Pb[sequence2[n+1]]+Pb[sequence2[n+2]]+Pb[sequence2[n+3]])
        SCc.append(Pc[sequence2[n]]+Pc[sequence2[n+1]]+Pc[sequence2[n+2]]+Pc[sequence2[n+3]])

#-------- ALPHA -------- ALPHA -------- ALPHA -------- ALPHA -------- ALPHA -------- ALPHA -------- ALPHA -------- ALPHA -------- ALPHA -------- ALPHA --------
# Fonction qui dÃ©tecte les initiateurs d'hÃ©lices alpha, selon la mÃ©thode Chou-Fassman
def initiateurs_alpha():
    global initiators # tous les a.a. initiateurs
    global initiators_debut # uniquement les premiers a.a. de chaque groupe d'initiateurs
    global initiators_fin # uniquement les derniers a.a. de chaque groupe d'initiateurs
    global initiators_seul # uniquement les a.a. des initiateurs seuls qui ne sont pas dans des groupes
    scores() # appelle la fonction scores dÃ©finie plus haut
    initiators=[]
    for n in range(len(sequence2)-8): # Le -8 Ã©vite que les sequences se trouvent en dehors du carde de lecture, compte tenu du fait que les 2 derniers a.a. n'aient pas de scores.
        helice = 0 # crÃ©e un compteur initialisÃ© Ã  zÃ©ro
        for x in range(n,n+6): # scanne tous les a.a. par fanÃªtre de 6 a.a.
            if SCa[x]>SCb[x] and SCa[x]>SCc[x]: # Teste les a.a. de la fenÃªtre et si un a.a. Ã  un score alpha plus grand que son score beta...
                helice += 1 # ... on rajoute 1 au compteur
        if helice >= 4: # Si 4 a.a. ou plus sur les 6 suivent cette rÃ¨gle ...
            initiators.append(n) # ... on les ajoute en tant qu'initeurs alpha
            
    initiators_debut, initiators_fin, initiators_seul = [], [], []
    
    if initiators[0]==initiators[1]-1: # pour le premier acide aminÃ© des initiateurs, si il fait partie d'une suite d'initiateurs ... 
        initiators_debut.append(initiators[0]) # ... le programme va garder uniquement le premier de la liste et le mettra dans la string initiateur debut ...
    else:
        initiators_seul.append(initiators[0]) # ... sinon il le mettra dans la string initiateur seul
        
    for q in range(1,len(initiators)-1): # pour les initiateurs sauf le premier et le dernier ...
        if initiators[q]==initiators[q+1]-1 and initiators[q]!=initiators[q-1]+1: # ... regarde si un initiateur a un intiateur qui le suit a sa droite et pas d initiateur a sa gauche ...
            initiators_debut.append(initiators[q]) # ... et le rajoute dans la string initiateur dÃ©but
        elif initiators[q]!=initiators[q+1]-1 and initiators[q]==initiators[q-1]+1: # regarde si un initiateur a un initiateur a sa gauche et pas d initiateur a sa droite ...
            initiators_fin.append(initiators[q]) # ... si tel est le cas, il le rajoute dans la string initiateur fin
        elif initiators[q]!=initiators[q+1]-1 and initiators[q]!=initiators[q-1]+1: # si l'initaiteur n'a pas de voisin du tout ...
            initiators_seul.append(initiators[q]) # ... il le rajoute dans initiateur seul
            
    if initiators[len(initiators)-1]==initiators[len(initiators)-2]+1: # pour le premier acide aminÃ© des initiateurs, si il fait partie d'une suite d'initiateurs ...
        initiators_fin.append(initiators[len(initiators)-1])  # ... le programme va garder uniquement le dernier de la liste et le mettra dans la string initiateur fin ...
    else:
        initiators_seul.append(initiators[len(initiators)-1]) # ... sinon il le mettra dans la string initiateur seul

    return initiators, initiators_debut, initiators_fin, initiators_seul

# Fonction qui rallonge les bouts des initiateurs alpha
def rallonge_alpha():
    global rallonge_debut # rallonge des initiateurs Ã  gauche d'un groupe d'initiateurs
    global rallonge_fin # rallonge des initiateurs Ã  droite d'un groupe d'initiateurs
    global rallonge_seul # rallonge des initiateurs des deux cÃ´tÃ©s d'un initiateur seul, ne faisant pas partie d'un groupe
    rallonge_debut=[]
    for x in range(len(initiators_debut)): # parmis les premiers a.a. de chaque groupe d'initiateurs, on teste les a.a. du cÃ´tÃ© gauche jusqu'Ã  ce que Pa ne soit plus > 1 ...
        y=1
        while Pa[sequence2[initiators_debut[x]-y]] >1.00: # !!!! puisque tous les scores SCa sont > 1, on a pris les Pa !!!! Si le Pa de l'a.a. est > 1 ...
            if initiators_debut[x]-y==-1 or initiators_debut[x]-y in initiators: # ... on teste les conditions de fin de boucle ...
                break # ... et on arrÃªte la boucle si fin de boucle
            rallonge_debut.append(initiators_debut[x]-y)  # ... si on n'est pas en fin de boucle, on rajoute l'a.a. dans la rallonge
            y+=1 # on incrÃ©mente y de 1 pour que la boucle reteste avec l'a.a. Ã  cÃ´tÃ©
    rallonge_fin=[]
    for x in range(len(initiators_fin)): # parmis les derniers a.a. de chaque groupe d'initiateurs, on teste les a.a. du cÃ´tÃ© droite jusqu'Ã  ce que Pa ne soit plus > 1 ...
        y=1
        while Pa[sequence2[initiators_fin[x]+y]] >1.00: # !!!! puisque tous les scores SCa sont > 1, on a pris les Pa !!!!
            if initiators_fin[x]+y==len(sequence2) or initiators_fin[x]+y in initiators or initiators_fin[x]+y in rallonge_debut:
                break
            rallonge_fin.append(initiators_fin[x]+y)
            y+=1
    rallonge_seul=[]
    for x in range(len(initiators_seul)): # parmis les a.a. des initiateurs seuls, on teste les a.a. des deux cÃ´tÃ©s jusqu'Ã  ce que Pa ne soit plus > 1 ...
        y=1
        while Pa[sequence2[initiators_seul[x]-y]] >1.00:# !!!! puisque tous les scores SCa sont > 1, on a pris les Pa !!!!
            if initiators_seul[x]-y==-1 or initiators_seul[x]-y in initiators or initiators_seul[x]-y in rallonge_debut or initiators_seul[x]-y in rallonge_fin:
                break
            rallonge_seul.append(initiators_seul[x]-y)
            y+=1
        y=1
        while Pa[sequence2[initiators_seul[x]+y]] >1.00: # !!!! puisque tous les scores SCa sont > 1, on a pris les Pa !!!!
            if initiators_seul[x]+y==len(sequence2) or initiators_seul[x]+y in initiators or initiators_seul[x]+y in rallonge_debut or initiators_seul[x]+y in rallonge_fin or initiators_seul[x]+y in rallonge_seul:
                break
            rallonge_seul.append(initiators_seul[x]+y)
            y+=1
    return rallonge_debut, rallonge_fin, rallonge_seul

# Fonction qui teste les conditions des hÃ©lices alpha, et retourne les vraies hÃ©lices
def alpha():
    global alpha3 # indexes des acides aminÃ©s faisant partie d'une hÃ©lice alpha calssÃ©s par listes
    global alpha4 # indexes des acides aminÃ©s faisant partie d'une hÃ©lice alpha calssÃ©s par listes aprÃ¨s avoir enlevÃ© les hÃ©lices dont le segment Ã©tendu est infÃ©rieur Ã  4
    global alpha5 # indexes des acides aminÃ©s faisant partie d'une hÃ©lice alpha calssÃ©s par listes aprÃ¨s avoir Ã©liminÃ© toutes les autres hÃ©lices selon les critÃ¨res de Chou Fassman
    alpha=initiators+rallonge_debut+rallonge_fin+rallonge_seul # crÃ©e une liste avec tous les a.a. faisant partie d'une hÃ©lice alpha (initiateurs + rallonges)
    alpha.sort() # classe les initiateurs dans l'ordre des indexes
    for k in range(len(alpha)): # boucle qui supprime les prolines des chaines alpha
        if sequence2[alpha[k]]=="P":
            alpha[k]="P"
    for p in range(alpha.count("P")):
        alpha.remove("P")
    alpha2=[]
    alpha3=[]
    for q in range(len(alpha)): # boucle qui met des listes diffÃ©rentes pour chaque hÃ©lices dans une grande liste nommÃ©e alpha3
        if q==len(alpha)-1:
            alpha2.append(alpha[q])
            alpha3.append(alpha2)
            break
        if alpha[q+1]==alpha[q]+1:
            alpha2.append(alpha[q])
        else:
            alpha2.append(alpha[q])
            alpha3.append(alpha2)
            alpha2=[]
    alpha4=[]
    for x in range(len(alpha3)): # boucle qui enlÃ¨ve les hÃ©lices alpha dont le segment Ã©tendu est infÃ©rieur Ã  4 (et pas 6 comme mentionnÃ© dans la mÃ©thode) et crÃ©e une nouvelle liste sans ces segments nommÃ©e alpha4
        if len(alpha3[x])>4:
            alpha4.append(alpha3[x])
    Pa_helice=[]
    Pa_helice2=[]
    Pb_helice=[]
    Pb_helice2=[]
    SCa_helice=[]
    SCa_helice2=[]
    SCb_helice=[]
    SCb_helice2=[]
    for x in range(len(alpha4)): # boucle qui crÃ©e des listes des Pa (Pa_helice2), Pb (Pb_helice2), SCa (SCa_helice2) et SCb (SCb_helice2) de chaque acide aminÃ© pour chaque hÃ©lice
        for y in range(len(alpha4[x])):
            if y==len(alpha4[x])-1:
                Pa_helice.append(Pa[sequence2[alpha4[x][y]]])
                Pa_helice2.append(Pa_helice)
                Pa_helice=[]
                Pb_helice.append(Pb[sequence2[alpha4[x][y]]])
                Pb_helice2.append(Pb_helice)
                Pb_helice=[]
                SCa_helice.append(SCa[alpha4[x][y]])
                SCa_helice2.append(SCa_helice)
                SCa_helice=[]
                SCb_helice.append(SCb[alpha4[x][y]])
                SCb_helice2.append(SCb_helice)
                SCb_helice=[]
                break
            Pa_helice.append(Pa[sequence2[alpha4[x][y]]])
            Pb_helice.append(Pb[sequence2[alpha4[x][y]]])
            SCa_helice.append(SCa[alpha4[x][y]])
            SCb_helice.append(SCb[alpha4[x][y]])
    alpha5=[]
    for x in range(len(alpha4)): # boucle qui vÃ©rifie la moyenne des Pa (!! et non les SCa comme mentionnÃ© dans la mÃ©thode !!) pour chaque hÃ©lice et qui vÃ©rifie que la moyenne des SCa soit plus grande que la moyenne des SCb pour chaque hÃ©lice
        if (sum(Pa_helice2[x])/len(Pa_helice2[x]))>1.03 and (sum(SCa_helice2[x])/len(SCa_helice2[x]))>(sum(SCb_helice2[x])/len(SCb_helice2[x])):
            alpha5.append(alpha4[x]) # la liste alpha5 est donc la liste finale contenant tous les a.a. aprÃ¨s toutes les vÃ©rifications
    return alpha, alpha2, alpha3, alpha4, alpha5

#-------- BETA -------- BETA -------- BETA -------- BETA -------- BETA -------- BETA -------- BETA -------- BETA -------- BETA -------- BETA -------- BETA --------
# Toutes les fonctions pour les feuillets beta sont comparables Ã  celles pour les hÃ©lices alpha Ã  quelques exceptions prÃ¨s (en rapport Ã  la mÃ©thode). Puisque les principes sont les mÃªmes, nous n'allons pas les dÃ©tailler Ã  nouveau
# Fonction qui dÃ©tecte les initieurs de feuillet beta
def initiateurs_beta():
    global initiators2 # tous les a.a. initiateurs
    global initiators2_debut # uniquement les premiers a.a. de chaque groupe d'initiateurs
    global initiators2_fin # uniquement les derniers a.a. de chaque groupe d'initiateurs
    global initiators2_seul # uniquement les a.a. des initiateurs seuls qui ne sont pas dans des groupes
    scores()
    initiators2=[]
    for n in range(len(sequence2)-8):
        beta=0
        for y in range(n,n+5): # pour les feuillets bÃ©ta, il faut 3 a.a. favorisant les feuillets sur une fenÃªtre de 5 a.a. pour dÃ©finir les initiateurs
            if SCb[y]>SCa[y] and SCb[y]>SCc[y]:
                beta+=1
        if beta>=3:
            initiators2.append(n)

    initiators2_debut,initiators2_fin,initiators2_seul = [],[],[]

    if initiators2[0]==initiators2[1]-1:
        initiators2_debut.append(initiators2[0])
    else:
        initiators2_seul.append(initiators2[0])
        
    for q in range(1,len(initiators2)-1):
        if initiators2[q]==initiators2[q+1]-1 and initiators2[q]!=initiators2[q-1]+1:
            initiators2_debut.append(initiators2[q])
        elif initiators2[q]!=initiators2[q+1]-1 and initiators2[q]==initiators2[q-1]+1:
            initiators2_fin.append(initiators2[q])
        elif initiators2[q]!=initiators2[q+1]-1 and initiators2[q]!=initiators2[q-1]+1:
            initiators2_seul.append(initiators2[q])
            
    if initiators2[len(initiators2)-1]==initiators2[len(initiators2)-2]+1:
        initiators2_fin.append(initiators2[len(initiators2)-1])
    else:
        initiators2_seul.append(initiators2[len(initiators2)-1])

    return initiators2, initiators2_debut, initiators2_fin, initiators2_seul

# Fonction qui rallonge les bouts des initiateurs bÃ©ta
def rallonge_beta():
    global rallonge2_debut # rallonge des initiateurs Ã  gauche d'un groupe d'initiateurs
    global rallonge2_fin # rallonge des initiateurs Ã  droite d'un groupe d'initiateurs
    global rallonge2_seul # rallonge des initiateurs des deux cÃ´tÃ©s d'un initiateur seul, ne faisant pas partie d'un groupe   
    initiateurs_beta()
    rallonge2_debut=[]
    for y in range(len(initiators2_debut)):
        z=1
        while Pb[sequence2[initiators2_debut[y]-z]] >1.00: # !!!! tous les scores SCb sont > 1 (on a pris les Pb) !!!!
            if initiators2_debut[y]-z==-1 or initiators2_debut[y]-z in initiators2:
                break
            rallonge2_debut.append(initiators2_debut[y]-z)
            z+=1
    rallonge2_fin=[]
    for y in range(len(initiators2_fin)):
        z=1
        while Pb[sequence2[initiators2_fin[y]+z]] >1.00: # !!!! tous les scores SCb sont > 1 (on a pris les Pb) !!!!
            if initiators2_fin[y]+z==len(sequence2) or initiators2_fin[y]+z in initiators2 or initiators2_fin[y]+z in rallonge2_debut:
                break
            rallonge2_fin.append(initiators2_fin[y]+z)
            z+=1
    rallonge2_seul=[]
    for y in range(len(initiators2_seul)):
        z=1
        while Pb[sequence2[initiators2_seul[y]-z]] >1.00: # !!!! tous les scores SCb sont > 1 (on a pris les Pb) !!!!
            if initiators2_seul[y]-z==-1 or initiators2_seul[y]-z in initiators2 or initiators2_seul[y]-z in rallonge2_debut or initiators2_seul[y]-z in rallonge2_fin:
                break
            rallonge2_seul.append(initiators2_seul[y]-z)
            z+=1
        z=1
        while Pb[sequence2[initiators2_seul[y]+z]] >1.00: # !!!! tous les scores SCb sont > 1 (on a pris les Pb) !!!!
            if initiators2_seul[y]+z==len(sequence2) or initiators2_seul[y]+z in initiators2 or initiators2_seul[y]+z in rallonge2_debut or initiators2_seul[y]+z in rallonge2_fin or initiators2_seul[y]+z in rallonge2_seul:
                break
            rallonge2_seul.append(initiators2_seul[y]+z)
            z+=1
    return rallonge2_debut, rallonge2_fin, rallonge2_seul

# Fonction qui teste les conditions des feuillets beta, et retourne les vrais feuillets
def beta():
    global beta3 # indexes des acides aminÃ©s faisant partie d'un feuillet beta calssÃ©s par listes
    global beta4 # indexes des acides aminÃ©s faisant partie d'un feuillet beta calssÃ©s par listes aprÃ¨s avoir Ã©liminÃ© tous les autres feuillets beta selon les critÃ¨res de Chou Fassman    
    beta=initiators2+rallonge2_debut+rallonge2_fin+rallonge2_seul
    beta.sort()
    beta2=[]
    beta3=[]
    for q in range(len(beta)): # boucle qui met des listes diffÃ©rentes pour chaque feuillet dans une grande liste nommÃ©e beta3
        if q==len(beta)-1:
            beta2.append(beta[q])
            beta3.append(beta2)
            break
        if beta[q+1]==beta[q]+1:
            beta2.append(beta[q])
        else:
            beta2.append(beta[q])
            beta3.append(beta2)
            beta2=[]
    Pa_beta=[]
    Pa_beta2=[]
    Pb_beta=[]
    Pb_beta2=[]
    SCa_beta=[]
    SCa_beta2=[]
    SCb_beta=[]
    SCb_beta2=[]
    for x in range(len(beta3)): # boucle qui crÃ©e des listes des Pa, Pb, SCa et SCb de chaque acide aminÃ© pour chaque feuillet beta
        for y in range(len(beta3[x])):
            if y==len(beta3[x])-1:
                Pa_beta.append(Pa[sequence2[beta3[x][y]]])
                Pa_beta2.append(Pa_beta)
                Pa_beta=[]
                Pb_beta.append(Pb[sequence2[beta3[x][y]]])
                Pb_beta2.append(Pb_beta)
                Pb_beta=[]
                SCa_beta.append(SCa[beta3[x][y]])
                SCa_beta2.append(SCa_beta)
                SCa_beta=[]
                SCb_beta.append(SCb[beta3[x][y]])
                SCb_beta2.append(SCb_beta)
                SCb_beta=[]
                break
            Pa_beta.append(Pa[sequence2[beta3[x][y]]])
            Pb_beta.append(Pb[sequence2[beta3[x][y]]])
            SCa_beta.append(SCa[beta3[x][y]])
            SCb_beta.append(SCb[beta3[x][y]])
    beta4=[]
    for x in range(len(beta3)): # boucle qui vÃ©rifie la moyenne des Pb (!! et non les SCb comme mentionnÃ© dans la mÃ©thode !!) pour chaque feuillet et qui vÃ©rifie que la moyenne des SCb soit plus grande que la moyenne des SCa pour chaque feuillet
        if (sum(Pb_beta2[x])/len(Pb_beta2[x]))>1.05 and (sum(SCb_beta2[x])/len(SCb_beta2[x]))>(sum(SCa_beta2[x])/len(SCa_beta2[x])):
            beta4.append(beta3[x]) # la liste beta4 est donc la liste finale contenant tous les a.a. aprÃ¨s toutes les vÃ©rifications
    return beta, beta2, beta3, beta4

#-------- RECOUVREMENT -------- RECOUVREMENT -------- RECOUVREMENT -------- RECOUVREMENT -------- RECOUVREMENT -------- RECOUVREMENT -------- RECOUVREMENT --------
# Fonction qui teste les recouvrements alpha-bÃ©ta et les attribue Ã  la bonne structure
def recouvrement():
    global recouvrement3 # grande liste composÃ©e des diffÃ©rentes listes contenant chaque groupe de recouvrement
    global SCa_recouvrement2 # scores alpha des acides aminÃ©s faisant partie des recouvrements
    global SCb_recouvrement2 # scores beta des acides aminÃ©s faisant partie des recouvrements
    recouvrement=[]
    for x in range(len(sequence2)): # boucle qui cherche les recouvrements et qui les met dans une liste nommÃ©e recouvrement
        for y in range(len(alpha5)):
            for z in range(len(beta4)):
                if x in alpha5[y] and x in beta4[z]: # si l'acide aminÃ© est prÃ©sent dans les hÃ©lices alpha et les fueuillets beta ...
                    recouvrement.append(x) # ... il le rajoute dans une nouvelle liste nommÃ©e recouvrement
    recouvrement2=[]
    recouvrement3=[]
    for q in range(len(recouvrement)): # boucle qui met des listes diffÃ©rentes pour chaque groupe de recouvrement dans une grande liste nommÃ©e recouvrement3
        if q==len(recouvrement)-1:
            recouvrement2.append(recouvrement[q])
            recouvrement3.append(recouvrement2)
            break
        if recouvrement[q+1]==recouvrement[q]+1:
            recouvrement2.append(recouvrement[q])
        else:
            recouvrement2.append(recouvrement[q])
            recouvrement3.append(recouvrement2)
            recouvrement2=[]
    SCa_recouvrement=[]
    SCa_recouvrement2=[]
    SCb_recouvrement=[]
    SCb_recouvrement2=[]
    for x in range(len(recouvrement3)): # boucle qui crÃ©e des listes des SCa et SCb de chaque acide aminÃ© de recouvrement
        for y in range(len(recouvrement3[x])):
            if y==len(recouvrement3[x])-1:
                SCa_recouvrement.append(SCa[recouvrement3[x][y]])
                SCa_recouvrement2.append(SCa_recouvrement)
                SCa_recouvrement=[]
                SCb_recouvrement.append(SCb[recouvrement3[x][y]])
                SCb_recouvrement2.append(SCb_recouvrement)
                SCb_recouvrement=[]
                break
            SCa_recouvrement.append(SCa[recouvrement3[x][y]])
            SCb_recouvrement.append(SCb[recouvrement3[x][y]])
    for x in range(len(recouvrement3)): # boucle qui attribue les recouvrements Ã  la bonne structure
        if (sum(SCa_recouvrement2[x])/len(SCa_recouvrement2[x]))>(sum(SCb_recouvrement2[x])/len(SCb_recouvrement2[x])): # si la moyenne des SCa est plus grande que celle des SCb ...
            for y in range(len(recouvrement3[x])):
                for z in range(len(beta4)):
                    if recouvrement3[x][y] in beta4[z]:
                        beta4[z].remove(recouvrement3[x][y]) # ... on enlÃ¨ve ces acides aminÃ©s de la liste des feuillets beta
        else: # si la moyenne des SCa est plus petite que celle des SCb ...
            for y in range(len(recouvrement3[x])):
                for z in range(len(alpha5)):
                    if recouvrement3[x][y] in alpha5[z]:
                        alpha5[z].remove(recouvrement3[x][y]) # ... on enlÃ¨ve ces acides aminÃ©s de la liste des hÃ©lices alpha
    return recouvrement3

#-------- prÃ©sentation des rÃ©sultats -------- prÃ©sentation des rÃ©sultats -------- prÃ©sentation des rÃ©sultats -------- prÃ©sentation des rÃ©sultats --------
# Fonction qui affiche La sÃ©quence entiÃ¨re avec les positions des structures
def affiche_seq():
    debut = 0 # initiation du dÃ©but de la premiÃ¨re ligne d'affichage
    fin = 60 # initiation de la fin de la premiÃ¨re ligne d'affichage
    while (debut < len(sequence2)): # tant que l'indexe du dÃ©but (initialement Ã  0, puis additionnÃ© de 60 Ã  chaque itÃ©ration) est plus petit que le longueur de la sÃ©quence ...
        print str(debut + 10).rjust(10) + \
              str(debut + 20).rjust(10) + \
              str(debut + 30).rjust(10) + \
              str(debut + 40).rjust(10) + \
              str(debut + 50).rjust(10) + \
              str(debut + 60).rjust(10) # ... on crÃ©e une ligne de 60 caractÃ¨res vides avec toutes les dixaines Ã©crites Ã  partir de dÃ©but (initialement Ã  0, puis additionnÃ© de 60 Ã  chaque itÃ©ration)
        print sequence2[debut:fin] # Ã  chaque itÃ©ration, on affiche Ã©galement la sÃ©quence correspondante allant de "dÃ©but" Ã  "fin" (dÃ©but et fin ltant additionnÃ©s de 60 Ã  chaque itÃ©ration
        alpha6=[] # liste unique contenant tous les indexes des acides aminÃ©s des hÃ©lices alpha (on a crÃ©Ã© une liste nommÃ©e alpha6 composÃ©e de toutes les sous-listes de alpha5)
        beta5=[] # liste unique contenant tous les indexes des acides aminÃ©s des feuillets beta (on a crÃ©Ã© une liste nommÃ©e beta5 composÃ©e de toutes les sous-listes de beta4)
        structure="" # string vide qui va contenir la structure Ã  laquelle appartient chaque acide aminÃ©
        for x in range(len(alpha5)): # crÃ©ation de la liste alpha6
            for y in range(len(alpha5[x])):
                alpha6.append(alpha5[x][y])
        for x in range(len(beta4)): # crÃ©ation de la liste beta5
            for y in range(len(beta4[x])):
                beta5.append(beta4[x][y])
        for n in range(len(sequence2)): # pour tous les acides aminÃ©s de la sÃ©quence ...
            if n in alpha6: # ... si l'acide aminÃ© fait partie d'une hÃ©lice alpha ...
                structure += "H" # ... on rajoute la lettre "H" Ã  la string structure ...
            elif n in beta5: # ... si l'acide aminÃ© fait partie d'un feuillet beta ...
                structure += "F" # ... on rajoute la lettre "F" Ã  la string structure ...
            else: # ... si la structure de l'acide aminÃ© n'a pas Ã©tÃ© prÃ©dite comme alpha ou beta ...
                structure += "-" # ... on rajoute le signe "-" Ã  la string structure
        print structure[debut:fin] # Ã  chaque itÃ©ration, on affiche aussi la structure prÃ©dite de la sÃ©quence correspondante allant de "dÃ©but" Ã  "fin" (dÃ©but et fin ltant additionnÃ©s de 60 Ã  chaque itÃ©ration
        print "" # on affiche un caractÃ¨re vide afin de crÃ©er un espace entre chaque ligne d'affichage
        debut = debut + 60 # on incrÃ©mente de 60 Ã  chaque itÃ©ration
        fin = fin + 60 # on incrÃ©mente de 60 Ã  chaque itÃ©ration
    return "\n"

# Fonction qui affiche les hÃ©lices alpha
def affiche_alpha():
    for x in range(len(alpha5)):
        print "helice", x+1, ": de", alpha5[x][0], "a", alpha5[x][len(alpha5[x])-1]
    return "\n"

# Fonction qui affiche les feuillets beta
def affiche_beta():
    for x in range(len(beta4)):
        print "feuillet", x+1, ": de", beta4[x][0], "a", beta4[x][len(beta4[x])-1]
    return "\n"

#-------- corps principal -------- corps principal -------- corps principal -------- corps principal -------- corps principal -------- corps principal --------

for x in range(3): # le script donne 3 chances pour donner un nom de fichier existant
    fichier = raw_input("entrez le nom du fichier fasta Ã  explorer avec son extention : ")
    try:
        fasta = open(fichier, "r")
        header = fasta.readline()
        sequence = fasta.readlines()
        sequence2 = copyseq()

        initiateurs_alpha()
        rallonge_alpha()
        alpha()

        initiateurs_beta()
        rallonge_beta()
        beta()

        recouvrement()

        print "Analyse de la structure secondaire selon Chou-Fasman: efficacitÃ© entre 50% et 60%\n"
        print "protÃ©ine = ", header, "\n"
        print "Cette protÃ©ine contient", len(alpha5), "hÃ©lices alpha et", len(beta4), "feuillets bÃ©ta\n\n"
        print affiche_seq()
        print "hÃ©lices alpha :\n", affiche_alpha()
        print "feuillets bÃ©ta :\n", affiche_beta()
        break
    except:
        if x==2: # A la troisiÃ¨me fois, il retourne un message d'erreur
            print "Game Over, insert coin!"
        else: # les deux premiÃ¨res fois, il donne la possibilitÃ© de rÃ©essayer
            print "LeÂ fichier", fichier, "estÂ introuvable"

