#!/bin/bash
# Outputs remaining time and also starts countdown.sh when needed
# countdown complexity dropped since we run most stuff with parallel
if [ -f "OUTPUTS/.out_parallel" ]; then
	if [ -e "OUTPUTS/${calculation_label}/.prediction-prefs" ]; then
		export prediction_type="$(head -n1 "OUTPUTS/${calculation_label}/.prediction-prefs")"
	fi
	export calculation_label="$(head -n1 OUTPUTS/.out_parallel)"
	export plot_graphs="$(sed -n '2{p;q}' OUTPUTS/.out_parallel)"

        if [ "${prediction_type}" = "sons" ]; then
                # We read the time directly from the stored countdown output
                awk 'END{print$1}' "OUTPUTS/.remaining_time"
        else
		# Sadly we need to add multiple diff because there are many steps that need to be handled differently and different tips need to be printed to the user
		# This will also cover awks/analyse_AMPs_database_tail_sort.awk, it will simply show estimation with body_time=0
		if [ "$(pgrep -f -u apache awks/analyse_AMPs_database_body.awk)" ] || [ "$(pgrep -f -u apache awks/analyse_AMPs_database_tail_sort.awk)" ] ; then
			export body_time="$(sed -e 's/\r/\n/g' OUTPUTS/.out_parallel|grep '#'|awk 'END{print$2}')"
			export fam_num="$(tail -n1 "OUTPUTS/${calculation_label}/.fam_num")"
			if [ "${plot_graphs}" = "y" ]; then
				# Change in run-plot-graphs.sh too
				export multiplier="120"
			else
				export multiplier="100"
			fi

			# All times are divided by number of processors
			echo "${body_time} + ${multiplier} * ${fam_num} / $(nproc)" | bc | cut -d "." -f1 # We cut to not deal with ugly decimals in seconds
		else
			# Now that we run tail in parallel, we can rely on direct estimates instead of guessing remaining time
			if [ "$(pgrep -f -u apache awks/analyse_AMPs_database_tail_parallel.awk)" ]; then
				tail_time="$(sed -e 's/\r/\n/g' OUTPUTS/.out_parallel|grep '#'|awk 'END{print$2}')"
				if [ "${plot_graphs}" = "y" ]; then
					export multiplier="120"
					export fam_num="$(tail -n1 "OUTPUTS/${calculation_label}/.fam_num")" # fam_num is updated after sort is run
					echo "${tail_time} + ${multiplier} * ${fam_num} / $(nproc)" | bc | cut -d "." -f1
				else
					echo "${tail_time}" | cut -d "." -f1
				fi
			else
				# For the generation of plot graphs
				if [ "$(pgrep -u apache python)" ]; then
					awk 'END{print$1}' "OUTPUTS/.remaining_time"
				else
					# If we reach this point we are finishing the experiment, running html generation, tarball ...
					echo "Running final phases of the experiment, refresh in few minutes"
				fi
			fi
		fi
	fi
else
	# This will be shown in the few seconds before parallel *body starts, this covers *head then
	echo 'Calculating times, refresh in a few minutes.....'
fi

#0.65 * numero de familias -> el tiempo posterior cuando plot_graphs es n
#0.65 + 45s * numero de familias cuando hay plot graphs
