{"id":12332,"dbaaspId":"DBAASPR_12332","name":"Cathelicidin-5, myeloid antimicrobial peptide BMAP-28","majorName":"","sequence":"GGLRSLGRKILRAWKKYGPIIVPIIRI","sequenceLength":27,"nTerminus":null,"cTerminus":null,"synthesisType":{"name":"Ribosomal"},"complexity":{"name":"Monomer"},"pdbs":[],"pubChemCid":{"cid":"16131512","cidUrl":"https://pubchem.ncbi.nlm.nih.gov/compound/16131512"},"noteReference":"<p><br></p>","structureModel":{"representativeStructure":"representative_structure.png","selfConsistency":"self_consistency.png","secondaryStructure":"secondary_structure.png","trajectoryDCD":"trajectory_dcd.dcd","trajectoryPSF":"trajectory_psf.psf","trajectoryWithWaterDCD":"trajectory_with_water_dcd.dcd","trajectoryWithWaterPSF":"trajectory_with_water_psf.psf","protocolId":10,"protocolDescription":"Peptide modeled in extended conformation and then minimized with chiral restraints in NAMD ahead of unrestrained minimization and dynamics in AceMD. TIP3P water/250 mM NaCl/310K/CHARMM36m. Note that interactions with periodic self are possible given box dimensions derived from starting conformation. For protocol details see https://doi.org/10.1093/nar/gkaa991"},"targetGroups":[{"name":"Gram+"},{"name":"Gram-"}],"targetObjects":[{"name":"Lipid Bilayer"}],"physicoChemicalProperties":[{"name":"ID","value":"GGLRSLGRKILRAWKKYGPI"},{"name":"Normalized Hydrophobic Moment","value":"1.53"},{"name":"Normalized Hydrophobicity","value":"-0.26"},{"name":"Net Charge","value":"7.00"},{"name":"Isoelectric Point","value":"12.15"},{"name":"Penetration Depth","value":"14"},{"name":"Tilt Angle","value":"97"},{"name":"Disordered Conformation Propensity","value":"0.06"},{"name":"Linear Moment","value":"0.29"},{"name":"Propensity to in vitro Aggregation","value":"0.00"},{"name":"Angle Subtended by the Hydrophobic Residues","value":"40.00"},{"name":"Amphiphilicity Index","value":"1.21"},{"name":"Propensity to PPII coil","value":"0.98"}],"monomers":[],"sourceGenes":[{"id":3655,"kingdom":{"name":"Animalia"},"source":"Bos taurus","subkingdom":"Eumetazoa","plasmid":"","gene":"CATHL5","chromLoc":null,"orientation":null,"geneInSequence":null,"locus":"","note":"","dblink":{"name":"NCBI","url":"https://www.ncbi.nlm.nih.gov/gene/282167"}},{"id":3656,"kingdom":{"name":"Animalia"},"source":"Bos indicus","subkingdom":"Eumetazoa","plasmid":"","gene":"CATHL5","chromLoc":null,"orientation":null,"geneInSequence":null,"locus":"","note":"","dblink":{"name":"NCBI","url":"https://www.ncbi.nlm.nih.gov/protein/ACI31917.1"}}],"intrachainBonds":[],"interchainBonds":[],"coordinationBonds":[],"unusualAminoAcids":[],"targetActivities":[{"id":94411,"targetSpecies":{"name":"Acinetobacter baumannii ATCC 19606"},"activityMeasureGroup":{"name":"MIC"},"activityMeasureValue":"MIC","concentration":"5","unit":{"name":"µg/ml","description":""},"ph":"","ionicStrength":"","saltType":"","medium":{"name":"BHIB","description":"Brain-Heart Infusion Broth"},"cfu":"1E3-1E4","cfuGroup":{"name":"1E3 - 1E4"},"note":"","reference":"1","activity":5.0},{"id":94412,"targetSpecies":{"name":"Acinetobacter baumannii"},"activityMeasureGroup":{"name":"MIC"},"activityMeasureValue":"MIC","concentration":"5-10","unit":{"name":"µg/ml","description":""},"ph":"","ionicStrength":"","saltType":"","medium":{"name":"BHIB","description":"Brain-Heart Infusion Broth"},"cfu":"1E3-1E4","cfuGroup":{"name":"1E3 - 1E4"},"note":"10 clinical isolates tested","reference":"1","activity":10.0},{"id":94421,"targetSpecies":{"name":"Staphylococcus aureus MS","description":""},"activityMeasureGroup":{"name":"MIC50"},"activityMeasureValue":"MIC50","concentration":"2.5","unit":{"name":"µg/ml","description":""},"ph":"","ionicStrength":"","saltType":"","medium":{"name":"BHIB","description":"Brain-Heart Infusion Broth"},"cfu":"1E3-5E3","cfuGroup":{"name":"1E3 - 1E4"},"note":"Clinical isolate","reference":"2","activity":2.5},{"id":94422,"targetSpecies":{"name":"Staphylococcus aureus MS","description":""},"activityMeasureGroup":{"name":"MIC90"},"activityMeasureValue":"MIC90","concentration":"10","unit":{"name":"µg/ml","description":""},"ph":"","ionicStrength":"","saltType":"","medium":{"name":"BHIB","description":"Brain-Heart Infusion Broth"},"cfu":"1E3-5E3","cfuGroup":{"name":"1E3 - 1E4"},"note":"Clinical isolate","reference":"2","activity":10.0},{"id":94423,"targetSpecies":{"name":"Staphylococcus aureus MR","description":""},"activityMeasureGroup":{"name":"MIC50"},"activityMeasureValue":"MIC50","concentration":"10","unit":{"name":"µg/ml","description":""},"ph":"","ionicStrength":"","saltType":"","medium":{"name":"BHIB","description":"Brain-Heart Infusion Broth"},"cfu":"1E3-5E3","cfuGroup":{"name":"1E3 - 1E4"},"note":"MRSA clinical isolate","reference":"2","activity":10.0},{"id":94424,"targetSpecies":{"name":"Staphylococcus aureus MR","description":""},"activityMeasureGroup":{"name":"MIC90"},"activityMeasureValue":"MIC90","concentration":"20","unit":{"name":"µg/ml","description":""},"ph":"","ionicStrength":"","saltType":"","medium":{"name":"BHIB","description":"Brain-Heart Infusion Broth"},"cfu":"1E3-5E3","cfuGroup":{"name":"1E3 - 1E4"},"note":"MRSA clinical isolate","reference":"2","activity":20.0}],"antibiofilmActivities":[],"uniprots":[{"id":10378,"uniprotId":"P54229","description":"","proPeptide":{"name":"Precursor","description":"The amino acid sequence of the current peptide coincides with the part of the UniProt entry that represents a peptide precursor."},"uniprotUrl":"http://www.uniprot.org/uniprot/P54229"},{"id":10379,"uniprotId":"A0A0A7NSJ6","description":"","proPeptide":{"name":"Probable Precursor","description":"The amino acid sequence of the current peptide coincides with the part of the UniProt entry that may represent a peptide precursor."},"uniprotUrl":"http://www.uniprot.org/uniprot/A0A0A7NSJ6"},{"id":10380,"uniprotId":"B9UKL9","description":"","proPeptide":{"name":"Probable Precursor","description":"The amino acid sequence of the current peptide coincides with the part of the UniProt entry that may represent a peptide precursor."},"uniprotUrl":"http://www.uniprot.org/uniprot/B9UKL9"},{"id":10381,"uniprotId":"B9UKL5","description":"","proPeptide":{"name":"Probable Precursor","description":"The amino acid sequence of the current peptide coincides with the part of the UniProt entry that may represent a peptide precursor."},"uniprotUrl":"http://www.uniprot.org/uniprot/B9UKL5"},{"id":10382,"uniprotId":"B9UKL8","description":"","proPeptide":{"name":"Probable Precursor","description":"The amino acid sequence of the current peptide coincides with the part of the UniProt entry that may represent a peptide precursor."},"uniprotUrl":"http://www.uniprot.org/uniprot/B9UKL8"},{"id":10383,"uniprotId":"B9UKL4","description":"","proPeptide":{"name":"Probable Precursor","description":"The amino acid sequence of the current peptide coincides with the part of the UniProt entry that may represent a peptide precursor."},"uniprotUrl":"http://www.uniprot.org/uniprot/B9UKL4"}],"hemoliticCytotoxicActivities":[],"articles":[{"id":14447,"journal":{"name":"Medicine (Baltimore)","description":"Medicine (Baltimore)"},"year":2018,"volume":"97","pages":"e12832","title":"A bovine myeloid antimicrobial peptide (BMAP-28) and its analogs kill pan-drug-resistant Acinetobacter baumannii by interacting with outer membrane protein A (OmpA).","additional":"","pubmed":{"pubmedId":"30334982","pubmedUrl":"http://www.ncbi.nlm.nih.gov/pubmed/30334982"},"authors":[{"name":"Guo Y","description":"Guo Y"},{"name":"Xun M","description":"Xun M"},{"name":"Han J","description":""}]},{"id":14451,"journal":{"name":"Anim Sci J","description":"Animal Science Journal"},"year":2012,"volume":"83","pages":"482-486","title":"Antimicrobial activity of a bovine myeloid antimicrobial peptide (BMAP-28) against methicillin-susceptible and methicillin-resistant Staphylococcus aureus.","additional":"","pubmed":{"pubmedId":"22694332","pubmedUrl":"http://www.ncbi.nlm.nih.gov/pubmed/22694332"},"authors":[{"name":"Takagi S","description":"Takagi S"},{"name":"Hayashi S","description":"Hayashi S"},{"name":"Takahashi K","description":""},{"name":"Isogai H","description":"Isogai H"},{"name":"Bai L","description":"Bai L"},{"name":"Yoneyama H","description":"Yoneyama H"},{"name":"Ando T","description":"Ando T"},{"name":"Ito K","description":"Ito K"},{"name":"Isogai E","description":""}]}],"synergies":[],"url":"https://dbaasp.org/peptide-card?id=DBAASPR_12332","interproJobId":null,"interproJobIdGeneratedAt":null,"smiles":null,"smilesImageUrl":null,"opms":[]}