
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    
<title>Defensins Knowledgebase</title>
<link rel="StyleSheet" href="/common/css/stylesheet.css" type="text/css">
<script language="JavaScript" src="/common/js/showHideTable.js"></script>
<link rel="StyleSheet" href="/common/css/showHideTable.css" type="text/css">

</head>

<body>
<center>
<TABLE width="770" cellpadding="0" cellspacing="0">
<tr>
	<td colspan="3" height="35">&nbsp;</td>
</tr>
<tr>
	<td colspan="3" bgcolor="#033566" style="text-align:center; padding-left:15px; padding-right:15px;"><img src="/images/common/title_long.jpg" align="absmiddle"></td>
</tr>
<tr>
	<td colspan="3" height="30">&nbsp;</td>
</tr>

<tr>
	<td colspan="3" height="30" style="text-align:center;"><script language="javaScript" src="/common/js/jmoljs.php"></script>
<!-- Start Content -->

<center>
<TABLE width="90%" cellpadding="5" cellspacing="0">
<thead>
<tr valign="top">
	<td class="contentNorm" width="160"><b>Protein</b></td>
	<td class="contentNorm" width="6">:</td>
	<td class="contentNorm" width="">Phormicin</td>
</tr>
<tr valign="top">
	<td class="contentNorm" width="160"><b>Description</b></td>
	<td class="contentNorm">:</td>
		<td class="contentNorm" width="">Insect defensins A and B. The Phormia terranovae defensins were some of the first insect defensins to be discovered.<br /> Note: The sequence, mass spectrometry profiles, and disulfide bonding of the recombinant Defensin A expressed in Saccharomyces cerevisiae yeast [PMID:2013293] are identical to natural Defensin A.  The recombinant Defensin A retains anti-Micrococcus luteus activity. [PMID:2013293]<br /> Note: The source of defensin A (natural or recombinant) assayed in [PMID:10092609, 11053427] was not explicitly stated.<br /> Note: Defensin A forms voltage-dependent potassium ion channels in the cytoplasmic membrane of Microccocus luteus bacteria. Intracellular ATP levels of M. luteus cells dramatically decreased within 30 seconds to 40% of the original level, when treated with Defensin A (data not shown). ATP was not found in the external medium. [PMID:7690029]<br /> Note: The Defensin A mature peptide differs from the sapecin mature peptide of Sarcophaga peregrina by only 1 amino acid at position 34, K in Defensin A, A in sapecin. [PMID:7663941]</td>
</tr>
<tr valign="top">
	<td class="contentNorm" width="160"><b>Family</b></td>
	<td class="contentNorm">:</td>
	<td class="contentNorm">Invertebrate Insect defensin</td>
</tr>
<tr valign="top">
	<td class="contentNorm"><b>Organism</b></td>
	<td class="contentNorm">:</td>
	<td class="contentNorm">Protophormia terraenovae (Black blowfly, Nestling-sucking blowfly, Phormia terranovae)</td>
</tr>	
</thead>
</TABLE>
</center>

				
<dl class="expandMenu">
<!-- Alias -->

<!-- All other databases -->
	<dt id="dt_otherDBs" onClick="showHideSwitch('dd_otherDBs')" style="text-align:left;"><img src="/images/common/expandbutton-open.gif" alt="" name="dd_otherDBs_btn" width="9" height="9" id="dd_otherDBs_btn" /> Other Databases</dt>
  	<dd class="hideSwitch" id="dd_otherDBs">
		<table>
				<tbody>
		<tr>
			<td><a href="http://www.uniprot.org/uniprot/P10891" target="_blank">SwissProt: P10891</a><br></td>
		</tr>
		</tbody>
		</table>
  	</dd>
	
							

<!-- Expression -->
	
	<dt id="dt_expression" onClick="showHideSwitch('dd_expression')" style="text-align:left;"><img src="/images/common/expandbutton-open.gif" alt="" name="dd_expression_btn" width="9" height="9" id="dd_expression_btn" /> Expression</dt>
  	<dd class="hideSwitch" id="dd_expression">
		<table>
		<tbody>
		<tr valign="top">
			<td width="160">Sites of Expression</td>
			<td width="6">:</td>
						<td>Hemolymph [PMID:2911573], fat body, thrombocytoid hemocyte (both mRNA transcripts only) [PMID:2369900]<br />Not expressed in unchallenged third-instar larvae (mRNA transcript only). [PMID:2369900]<br />Not expressed in plasmatocyte hemocytes or oenocytoid hemocytes (both mRNA transcripts only). [PMID:2369900]<br />mRNA expression levels of Defensin A in the fat body were higher than that of unchallenged third-instar larvae, at 8 hours after the larvae were pricked with a sterile needle. At 24 hours post-pricking, mRNA expression levels had decreased to near that of unchallenged levels. At 48 hours post-pricking, mRNA expression levels increased to levels greater than that at the 8-hour time point. At 72 hours post-pricking, mRNA expression levels had returned to that of unchallenged levels. The authors acknowledge this to be an unusual transcriptional profile. [PMID:2369900]<br />mRNA expression levels of Defensin A in the fat body were dramatically higher than that of unchallenged third-instar larvae, at 8 hours after the larvae were challenged with heat-killed E. coli bacteria. At 24 hours post-challenge, mRNA expression levels had returned to that of unchallenged levels. When this experiment was repeated with 10X higher concentration of heat-killed E. coli bacteria, mRNA expression levels are higher at all time points, and returned to that of unchallenged levels only at 72 hours post-challenge. Similar results were obtained with heat-killed M. luteus bacteria (results not shown). [PMID:2369900]<br />mRNA expression levels of Defensin A in the fat body were dramatically higher than that of unchallenged third-instar larvae, at 8 hours after the larvae were challenged with lipopolysaccharide purified from Serratia marcescens bacteria. At 24 hours post-challenge, mRNA expression levels had returned to that of unchallenged levels. When this experiment was repeated with 10X higher concentration of lipopolysaccharide, mRNA expression levels are higher at all time points, and returned to that of unchallenged levels only at 48 hours post-challenge. [PMID:2369900]<br />The mRNA transcriptional profiles of Defensin A in the fat body of third-instar larvae of the same age challenged with lipopolysaccharide purified from Serratia marcescens bacteria, the larvae being either challenged 48 hours prior with the same amount of lipopolysaccharide or unchallenged 48 hours prior, were identical. This suggests that the larvae have no immune memory after 48 hours, with regard to Defensin A induction. [PMID:2369900]<br />Defensin A assayed in [PMID:2911573, 9603495, 9887520] was harvested from third-instar larvae challenged with live Enterobacter cloacae bacteria, unless otherwise indicated.<br />Defensin A mRNA assayed in [PMID:2369900] was harvested from third-instar larvae challenged with heat-killed Escherichia coli bacteria, unless otherwise indicated.<br />The recombinant Defensin A assayed in [PMID:1425705] was indicated to be prepared identically to [PMID:2369900], which in turn did not indicate a protein expression system or conditions.</td>
		</tr>
		</table>
  	</dd>


<!-- Sequence Information -->
  	<dt id="dt_seqInfo" onClick="showHideSwitch('dd_seqInfo')" style="text-align:left;"><img src="/images/common/expandbutton-open.gif" alt="" name="dd_seqInfo_btn" width="9" height="9" id="dd_seqInfo_btn" /> Sequence Information</dt>
  	<dd class="hideSwitch" id="dd_seqInfo">
		<table>
		<tbody>
		<tr valign="top"><td width="160">Description</td><td width="6">:</td><td>Defensin A (G at position 86)</td></tr><tr valign="top"><td width="160">Sequence Length</td><td width="6">:</td><td>94 aa</td></tr><tr valign="top"><td width="160">Protein Sequence</td><td width="6">:</td><td>MKFFMVFVVT FCLAVCFVSQ SLAIPADAAN DAHFVDGVQA LKEIEPELHG RYKRATCDLL SGTGINHSAC AAHCLLRGNR GGYCNGKGVC VCRN</td></tr><tr valign="top"><td width="160">Molecular Weight</td><td width="6">:</td><td>10110 Da</td></tr><tr valign="top"><td width="160">Sequence Organization</td><td width="6">:</td><td>Signal peptide: 1-23<br />Proprotein: 24-54<br />Mature peptide: 55-94</td></tr>		</tbody>
		</table>
	</dd>
	
	
							
<!-- Organisation of Gene -->
	
<!-- Structure Information -->
	
  	<dt id="dt_structureInfo" onClick="showHideSwitch('dd_structureInfo')" style="text-align:left;"><img src="/images/common/expandbutton-open.gif" alt="" name="dd_structureInfo_btn" width="9" height="9" id="dd_structureInfo_btn" /> Structure Information</dt>
  	<dd class="hideSwitch" id="dd_structureInfo">
		<table>
		<tbody>
		<tr valign="top"><td width="160">Mature peptide Sequence</td><td width="6">:</td><td>ATCDLL SGTGINHSAC AAHCLLRGNR GGYCNGKGVC VCRN</td></tr><tr valign="top"><td width="160">Mature Sequence Length</td><td width="6">:</td><td>40 aa</td></tr><tr valign="top"><td width="160">No. of disulfide bonds</td><td width="6">:</td><td>3</td></tr><tr valign="top"><td width="160">Disulfide Connectivities</td><td width="6">:</td><td>57-84, 70-90, 74-92 (3-30, 16-36, 20-38)</td></tr><tr valign="top"><td width="160">Structure</td><td width="6">:</td><td>Both alpha-helix and beta-structure</td></tr><tr valign="top"><td width="160">Structure Description</td><td width="6">:</td><td>The Defensin A mature peptide comprises an alpha-helix and a 2-stranded antiparallel beta-sheet. Cystine stabilized alpha beta motif. [PMID:1392568, 1467342, 7663941]<br />The helical content of defensin A is maximum at the same pH value range (7.5-8) for which the optimum antibacterial activity was observed in [PMID:7690029]. [PMID:8589051]<br />Defensin A interacts with phospholipid by forming complexes at 1:4 defensin:phospholipid stoichiometry. [PMID:9370446]</td></tr>	
		</tbody>
		</table>
  	</dd>

<!-- Experimental Evidence -->
  	<dt id="dt_experimentalEvidence" onClick="showHideSwitch('dd_experimentalEvidence')" style="text-align:left;"><img src="/images/common/expandbutton-open.gif" alt="" name="dd_experimentalEvidence_btn" width="9" height="9" id="dd_experimentalEvidence_btn" /> Experimental Evidence</dt>
  	<dd class="hideSwitch" id="dd_experimentalEvidence">
		<table>
		<tbody>
	<tr valign="top"><td width="160">Experimental System Used</td><td width="6">:</td><td>NMR</td></tr><tr valign="top"><td width="160">Experimental Description</td><td width="6">:</td><td>Defensin A mature peptide (G at position 32)</td></tr><tr valign="top"><td width="160">Protein</td><td width="6">:</td><td>1ICA_A Protein chain A<br> Sequence:atcdllsgtg inhsacaahc llrgnrggyc ngkgvcvcrn (40 aa)</td></tr><tr valign="top"><td width="160">No. of Chains</td><td width="6">:</td><td>1</td></tr><tr valign="top"><td width="160">All Other Databases</td><td width="6">:</td><td><a href="http://www.rcsb.org/pdb/explore/explore.do?structureId=1ICA" target="_blank">PDB: 1ICA</a> [ <a href="pop_proteinViewer.php?type=Jmol&pdb=1ICA" target="_blank" style="text-decoration:none;">JMol</a> | <a href="pop_proteinViewer.php?type=Astex&pdb=1ICA" target="_blank" style="text-decoration:none;">Astex Viewer</a> ]<br><a href="http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?form=6&db=t&Dopt=s&uid=56408" target="_blank">MMDB: 56408</a><br></td></tr><tr><td colspan="3" bgcolor="#FFFFFF">&nbsp;</td></tr><tr valign="top"><td width="160">Experimental System Used</td><td width="6">:</td><td>Circular dichroism</td></tr><tr valign="top"><td width="160">Experimental Description</td><td width="6">:</td><td>Defensin A mature peptide (G at position 32), studied in solvents acetonitrile, water, hexafluoroisopropanol, and methanol, pH 3.3 to 10.6, peptide concentration 10 microM to 1 mM, and various salt concentrations of KCl and CaCl2, all at room temperature. [PMID:8589051]</td></tr><tr valign="top"><td width="160">Protein</td><td width="6">:</td><td>Sequence:atcdllsgtg inhsacaahc llrgnrggyc ngkgvcvcrn (40 aa)</td></tr><tr valign="top"><td width="160">No. of Chains</td><td width="6">:</td><td>1</td></tr><tr valign="top"><td width="160">All Other Databases</td><td width="6">:</td><td><a href="" target="_blank">: </a><br></td></tr>		</tbody>
		</table>
  	</dd>
 
<!-- Sequence Variation -->
  	<dt id="dt_sequenceVariation" onClick="showHideSwitch('dd_sequenceVariation')" style="text-align:left;"><img src="/images/common/expandbutton-open.gif" alt="" name="dd_sequenceVariation_btn" width="9" height="9" id="dd_sequenceVariation_btn" /> Sequence Variation</dt>
  	<dd class="hideSwitch" id="dd_sequenceVariation">
		<table>
		<tbody>
	<tr valign="top"><td width="160">Natural variant</td><td width="6">:</td><td>ATCDLL SGTGINHSAC AAHCLLRGNR GGYCNRKGVC VCRN</td></tr><tr valign="top"><td width="160">Description</td><td width="6">:</td><td>Defensin B (R at position 32) of Protophormia terraenovae / Phormia terranovae (Black blowfly).</td></tr><tr valign="top"><td width="160">Length</td><td width="6">:</td><td>40 aa</td></tr><tr valign="top"><td width="160">Disulfide connectivities</td><td width="6">:</td><td>57-84, 70-90, 74-92 (3-30, 16-36, 20-38)</td></tr><tr valign="top"><td width="160">Antimicrobial Activity</td><td width="6">:</td><td>Escherichia coli (No activity [PMID:2911573])<br />Micrococcus luteus (Some activity shown during peptide purification. [PMID:2911573])</td></tr><tr valign="top"><td width="160">Literature</td><td width="6">:</td><td>Insect immunity: isolation from immune blood of the dipteran Phormia terranovae of two insect antibacterial peptides with sequence homology to rabbit lung macrophage bactericidal peptides. &nbsp; <br />(PMID: 2911573)</td></tr>		</tbody>
		</table>
  	</dd>

<!-- Antimicrobial Activity -->
	<dt id="dt_antimicrobialActivity" onClick="showHideSwitch('dd_antimicrobialActivity')" style="text-align:left;"><img src="/images/common/expandbutton-open.gif" alt="" name="dd_antimicrobialActivity_btn" width="9" height="9" id="dd_antimicrobialActivity_btn" /> Antimicrobial Activity</dt>
  	<dd class="hideSwitch" id="dd_antimicrobialActivity">
		<table>
		<thead>
		<tr>
			<td>Microbe Type</td>
			<td>Microbe Name</td>
			<td>Antimicrobial Activity</td>
		</tr>
		</thead>
		<tbody>
	<tr><td width="160">Fungus</td><td width="160">Neurospora crassa</td><td>MIC = 6 microM [PMID:10092609]<br> MIC = 3-6 microM [PMID:11053427, 11167093]</td></tr><tr><td width="160">Fungus</td><td width="160">Fusarium culmorum</td><td>MIC = 3 microM [PMID:10092609]<br> MIC = 1.5-3 microM [PMID:11053427, 11167093]</td></tr><tr><td width="160">Fungus</td><td width="160">Fusarium oxysporum</td><td>MIC = 6 microM [PMID:10092609]<br> MIC = 3-6 microM [PMID:11053427, 11167093]</td></tr><tr><td width="160">Fungus</td><td width="160">Nectria haematococca</td><td>MIC = 0.8-1.5 microM [PMID:11053427]<br> No activity up to 50 microM [PMID:10092609]</td></tr><tr><td width="160">Fungus</td><td width="160">Aspergillus fumigatus</td><td>MIC > 100 microM [PMID:11053427]<br> No activity up to 50 microM [PMID:10092609]<br> No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Fungus</td><td width="160">Trichoderma viride</td><td>MIC = 6-12 microM [PMID:11053427]<br> No activity up to 50 microM [PMID:10092609]</td></tr><tr><td width="160">Fungus</td><td width="160">Candida albicans</td><td>MIC > 100 microM [PMID:11053427]<br> No activity up to 50 microM. [PMID:10092609]<br> No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Fungus</td><td width="160">Candida glabrata</td><td>MIC > 100 microM [PMID:11053427]<br> No activity up to 50 microM [PMID:10092609]<br> No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Fungus</td><td width="160">Cryptococcus neoformans</td><td>MIC > 100 microM [PMID:11053427]<br> No activity up to 50 microM [PMID:10092609]<br> No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Fungus</td><td width="160">Saccharomyces cerevisiae</td><td>No activity up to 50 microM [PMID:10092609]</td></tr><tr><td width="160">Fungus</td><td width="160">Beauveria bassiana</td><td>MIC > 100 microM [PMID:11053427]<br> No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Fungus</td><td width="160">Botrytis cinerea</td><td>MIC = 25-50 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-negative bacteria</td><td width="160">Alcaligenes faecalis</td><td>No activity. [PMID:1425705]<br> No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-negative bacteria</td><td width="160">Salmonella typhimurium</td><td>No activity up to 100 microM [PMID:11167093]<br> No activity [PMID:1425705]</td></tr><tr><td width="160">Gram-negative bacteria</td><td width="160">Pseudomonas aeruginosa</td><td>MIC > 100 microM [PMID:11053427]<br> No activity up to 100 microM [PMID:11167093]<br> No activity [PMID:1425705]</td></tr><tr><td width="160">Gram-negative bacteria</td><td width="160">Pseudomonas cepacia</td><td>No activity. [PMID:1425705]</td></tr><tr><td width="160">Gram-negative bacteria</td><td width="160">Escherichia coli</td><td>MIC = 1.5-3 microM [PMID:11053427, 11167093]<br> MIC = 6-12.5 microM [PMID:11167093]<br> MIC = 25-50 microM [PMID:11053427, 11167093]<br> MIC = 25 microM [PMID:10092609]<br> No activity. [PMID:1425705, 2911573, 11167093]</td></tr><tr><td width="160">Gram-negative bacteria</td><td width="160">Erwinia carotovora carotovora</td><td>No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-negative bacteria</td><td width="160">Enterobacter cloacae</td><td>No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-negative bacteria</td><td width="160">Serratia marcescens</td><td>No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-negative bacteria</td><td width="160">Xanthomonas campestris oryzae</td><td>No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Micrococcus luteus</td><td>MIC = 0.75 microM [PMID:10092609]<br> MIC = 0.4-0.8 microM [PMID:11053427]<br> MIC = 0.4-0.75 microM [PMID:11167093]<br> The values are expressed as peptide amount, diameter of clear zone in the plate growth-inhibition assay.<br> 10 picomol, 8.5 mm<br> 5 picomol, 7 mm<br> 2 picomol, 4.5 mm<br> 1 picomol, 2.5 mm.<br> [PMID:1425705]<br> At concentration 10 microM, 7.15 X 10^6 CFU of bacteria were killed within 1 minute and no growth was observed within 120 minutes [PMID:9887520].<br> Cell-free hemolymph harvested from unchallenged third-instar larvae showed no activity. [PMID:2911573]<br> Strong activity (results not shown). [PMID:2911573]<br> At concentration 0.5 microM, all bacteria were killed within 1 hour and no growth was observed within 5 hours. [PMID:2911573]<br> At concentration 0.5 microM, rapid lysis (15 mins) occurred only with protoplasts and not with intact cells, indicating that the target of defensin A is probably the cytoplasmic membrane. [PMID:2911573]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Aerococcus viridans</td><td>MIC = 0.05-0.1 microM [PMID:11053427, 11167093]<br> The values are expressed as peptide amount, diameter of clear zone in the plate growth-inhibition assay.<br> 10 picomol, 11 mm<br> 5 picomol, 9 mm<br> 2 picomol, 6 mm<br> 1 picomol, 0 mm.<br> [PMID:1425705]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Pediococcus acidilactici</td><td>MIC = 1.5-3 microM [PMID:11167093]<br> The values are expressed as peptide amount, diameter of clear zone in the plate growth-inhibition assay.<br> 10 picomol, 3 mm<br> 5 picomol, 0 mm<br> 2 picomol, 0 mm<br> 1 picomol, 0 mm.<br> [PMID:1425705]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Bacillus megaterium</td><td>MIC = 0.05-0.1 microM [PMID:11053427, 11167093]<br> The values are expressed as peptide amount, diameter of clear zone in the plate growth-inhibition assay.<br> 10 picomol, 5 mm<br> 5 picomol, 5 mm<br> 2 picomol, 4 mm<br> 1 picomol, 3 mm.<br> [PMID:1425705]<br> Strong activity (results not shown). [PMID:2911573]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Streptococcus pyogenes</td><td>MIC = 1.5-3 microM [PMID:11053427, 11167093]<br> The values are expressed as peptide amount, diameter of clear zone in the plate growth-inhibition assay.<br> 10 picomol, 3 mm<br> 5 picomol, 0 mm<br> 2 picomol, 0 mm<br> 1 picomol, 0 mm.<br> [PMID:1425705]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Staphylococcus aureus</td><td>MIC = 1.5-3 microM [PMID:11053427, 11167093]<br> No activity. [PMID:1425705]<br> Poorly sensitive (results not shown). [PMID:2911573]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Listeria monocytogenes</td><td>MIC = 3-6 microM [PMID:11167093]<br> No activity. Erroneously listed in [PMID:1425705] as a Gram-negative bacterium.</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Bacillus subtilis</td><td>MIC = 0.4-0.75 microM [PMID:11167093]<br> Moderate activity (results not shown). [PMID:2911573]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Bacillus thuringiensis</td><td>MIC = 25-50 microM [PMID:11167093]<br> Poorly sensitive (results not shown). [PMID:2911573]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Bacillus cereus</td><td>No activity up to 100 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Enterococcus faecalis</td><td>MIC = 12.5-25 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Staphylococcus epidermidis</td><td>MIC = 1.5-3 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Staphylococcus haemolyticus</td><td>MIC = 1.5-3 microM [PMID:11167093]</td></tr><tr><td width="160">Gram-positive bacteria</td><td width="160">Staphylococcus saprophyticus</td><td>MIC = 3-6 microM [PMID:11167093]</td></tr><tr><td width="160">Protist</td><td width="160">Plasmodium gallinaceum</td><td>At concentration 250 microM, no toxic effect on the in vitro transformation from zygotes to ookinetes and the in vitro viability of mature ookinetes. [PMID:9603495]<br> At concentration 240 microM, a majority of oocysts were abnormal after the host Aedes aegypti mosquitoes had been injected with the peptide, as compared to saline injection. The peptide or saline injection was done on day 5 after the mosquitoes' infectious blood meal, and the mosquitoes were dissected on day 8. [PMID:9603495]<br> At concentration 250 microM, significant toxic effect on in vitro viability of sporozoites. At concentration 1 microM, both the Aeschna cyanea defensin and the Phormia terranovae defensin began to kill sporozoites, with an increasing effect at higher concentrations up to 25 microM. At concentration 25 microM, large clumps of dead sporozoites were observed, preventing further accurate quantitation. No significant difference in the effect of Aeschna and Phormia defensin was noted. [PMID:9603495]</td></tr>	
		</tbody>
		</table>
  	</dd>
	
<!-- Cytotoxicity -->

<!-- Commercial Entities -->

<!-- Literature -->
	
	<dt id="dt_literature" onClick="showHideSwitch('dd_literature')" style="text-align:left;"><img src="/images/common/expandbutton-open.gif" alt="" name="dd_literature_btn" width="9" height="9" id="dd_literature_btn" /> Literature</dt>
  	<dd class="hideSwitch" id="dd_literature">
		<table>
		<tbody>
		<tr><td> (1) Lambert J, Keppi E, Dimarcq JL, Wicker C, Reichhart JM, Dunbar B, Lepage P, Van Dorsselaer A, Hoffmann J, Fothergill J. Insect immunity: isolation from immune blood of the dipteran Phormia terranovae of two insect antibacterial peptides with sequence homology to rabbit lung macrophage bactericidal peptides. Proc. Natl. Acad. Sci. U.S.A. 1989 Jan;86 (1):262-6 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids=2911573" target="_blank" class="listlink">PMID: 2911573</a>]</td></tr><tr><td> (2) Dimarcq JL, Zachary D, Hoffmann JA, Hoffmann D, Reichhart JM. Insect immunity: expression of the two major inducible antibacterial peptides, defensin and diptericin, in Phormia terranovae. EMBO J. 1990 Aug;9 (8):2507-15 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 2369900" target="_blank" class="listlink">PMID:  2369900</a>]</td></tr><tr><td> (3) Lepage P, Bitsch F, Roecklin D, Keppi E, Dimarcq JL, Reichhart JM, Hoffmann JA, Roitsch C, Van Dorseelaer A. Determination of disulfide bridges in natural and recombinant insect defensin A. Eur. J. Biochem. 1991 Mar;196 (3):735-42 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 2013293" target="_blank" class="listlink">PMID:  2013293</a>]</td></tr><tr><td> (4) Bonmatin JM, Bonnat JL, Gallet X, Vovelle F, Ptak M, Reichhart JM, Hoffmann JA, Keppi E, Legrain M, Achstetter T. Two-dimensional 1H NMR study of recombinant insect defensin A in water: resonance assignments, secondary structure and global folding. J. Biomol. NMR 1992 May;2 (3):235-56 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 1392568" target="_blank" class="listlink">PMID:  1392568</a>]</td></tr><tr><td> (5) Bonmatin JM, Genest M, Petit MC, Gincel E, Simorre JP, Cornet B, Gallet X, Caille A, Labb� H, Vovelle F, Ptak M. Progress in multidimensional NMR investigations of peptide and protein 3-D structures in solution. From structure to functional aspects. Biochimie. 1992 Sep-Oct;74(9-10):825-36. [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 1467342" target="_blank" class="listlink">PMID:  1467342</a>]</td></tr><tr><td> (6) Bulet P, Cociancich S, Reuland M, Sauber F, Bischoff R, Hegy G, Van Dorsselaer A, Hetru C, Hoffmann JA. A novel insect defensin mediates the inducible antibacterial activity in larvae of the dragonfly Aeschna cyanea (Paleoptera, Odonata). Eur. J. Biochem. 1992 Nov;209 (3):977-84 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 1425705" target="_blank" class="listlink">PMID:  1425705</a>]</td></tr><tr><td> (7) Cociancich S, Ghazi A, Hetru C, Hoffmann JA, Letellier L. Insect defensin, an inducible antibacterial peptide, forms voltage-dependent channels in Micrococcus luteus. J. Biol. Chem. 1993 Sep;268 (26):19239-45 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 7690029" target="_blank" class="listlink">PMID:  7690029</a>]</td></tr><tr><td> (8) Cornet B, Bonmatin JM, Hetru C, Hoffmann JA, Ptak M, Vovelle F. Refined three-dimensional solution structure of insect defensin A. Structure 1995 May;3 (5):435-48 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 7663941" target="_blank" class="listlink">PMID:  7663941</a>]</td></tr><tr><td> (9) Maget-Dana R, Bonmatin JM, Hetru C, Ptak M, Maurizot JC. The secondary structure of the insect defensin A depends on its environment. A circular dichroism study. Biochimie 1995 ;77 (4):240-4 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 8589051" target="_blank" class="listlink">PMID:  8589051</a>]</td></tr><tr><td> (10) Maget-Dana R, Ptak M. Penetration of the insect defensin A into phospholipid monolayers and formation of defensin A-lipid complexes. Biophys. J. 1997 Nov;73 (5):2527-33 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 9370446" target="_blank" class="listlink">PMID:  9370446</a>]</td></tr><tr><td> (11) Shahabuddin M, Fields I, Bulet P, Hoffmann JA, Miller LH. Plasmodium gallinaceum: differential killing of some mosquito stages of the parasite by insect defensin. Exp. Parasitol. 1998 May;89 (1):103-12 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 9603495" target="_blank" class="listlink">PMID:  9603495</a>]</td></tr><tr><td> (12) Lauth X, Nesin A, Briand JP, Roussel JP, Hetru C. Isolation, characterization and chemical synthesis of a new insect defensin from Chironomus plumosus (Diptera). Insect Biochem. Mol. Biol. 1998 Dec;28 (12):1059-66 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 9887520" target="_blank" class="listlink">PMID:  9887520</a>]</td></tr><tr><td> (13) Lamberty M, Ades S, Uttenweiler-Joseph S, Brookhart G, Bushey D, Hoffmann JA, Bulet P. Insect immunity. Isolation from the lepidopteran Heliothis virescens of a novel insect defensin with potent antifungal activity. J. Biol. Chem. 1999 Apr;274 (14):9320-6 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 10092609" target="_blank" class="listlink">PMID:  10092609</a>]</td></tr><tr><td> (14) Lamberty M, Zachary D, Lanot R, Bordereau C, Robert A, Hoffmann JA, Bulet P. Insect immunity. Constitutive expression of a cysteine-rich antifungal and a linear antibacterial peptide in a termite insect. J. Biol. Chem. 2001 Feb;276 (6):4085-92 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 11053427" target="_blank" class="listlink">PMID:  11053427</a>]</td></tr><tr><td> (15) Vizioli J, Richman AM, Uttenweiler-Joseph S, Blass C, Bulet P. The defensin peptide of the malaria vector mosquito Anopheles gambiae: antimicrobial activities and expression in adult mosquitoes. Insect Biochem. Mol. Biol. 2001 Mar;31 (3):241-8 [<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=pubmed&dopt=Abstract&list_uids= 11167093" target="_blank" class="listlink">PMID:  11167093</a>]</td></tr>		</tbody>
		</table>
  	</dd>
<!-- End Content -->
	</td>
</tr>	
<tr>
	<td colspan="3" height="10">&nbsp;</td>
</tr>	
</TABLE>

<a href="/privacy.php" target="_self" class="copyright">
| Privacy Policy 
</a>

<a href="/termsOfUse.php" target="_self" class="copyright">| Terms of Use </a>

<font style="font-size:10px;">| Copyright 2006 Defensins Knowledgebase |</font>

</center>
</body>
</html>