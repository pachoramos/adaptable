#!/bin/bash
export LC_ALL=C
pdb_full_id=$(grep -w "${1}" DATABASES/PDB_DSSP/ss.txt |sort -V| head -n1|cut -d: -f1,2)
[[ ${pdb_full_id} ]] && grep -w "${pdb_full_id}:dssp" DATABASES/PDB_DSSP/ss.txt| sed -e 's/>//' | cut -d: -f1,4
