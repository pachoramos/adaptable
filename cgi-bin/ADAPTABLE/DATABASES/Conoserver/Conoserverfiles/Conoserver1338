<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="description" content="ConoServer is a database of toxins isolated from Cone snails. The database provide annotated information on nucleic acid sequences, protein sequences and 3D structures. It tracks known cysteins frameworks, genetic superfamily and pharmacological families."/>
	<meta http-equiv="keywords" content="ConoServer, cone snail, cone snail toxin, conopeptide, conotoxin, conophan, contryphan, conantokin, conolysin, conopressin, conomarphin, conorfamide, contulakin, David Craik, Quentin Kaas"/>
	<title>ConoServer</title>
	<link rel="StyleSheet" href="index.css?b" type="text/css">

	<link rel="stylesheet" href="lightbox.css" type="text/css" media="screen" />
	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
	<script src="js/lightbox.js" type="text/javascript"></script>
	<script src="js/jquery-3.6.0.min.js" type="text/javascript"></script>
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
<script>
function SelectValue(selectid,value) {
	var i;
	var select = document.getElementById(selectid);
	for(i = 0; i < select.options.length; i++) {
		if (select.options[i].value == value) {
			select.options[i].selected= true;
		} else {
			select.options[i].selected= false;
		}
	}
}
</script>


<div id='globalcontainer'>

<div id='pagehead'>
	<div>
	<a href="index.php">
		<img src="images/conoserver_small.png" alt="ConoServer: A database for conotoxins" class='expl'/>
	</a>
	</div>
	<div><a class='primarycitation' href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>Kaas Q et al. (2012) <i>Nucleic Acids Res.</i></a>
<form>
<div class='firstline'>
Search a
<select name="table">
  <option value='protein'>Protein</option>
  <option value='nucleicacid'>Nucleic acids</option>
  <option value='structure'>3D structure</option>
</select>
name
</div>
<div class='secondline'>
<input type='text'   name='quicktext' size='30' id='textsearch'\>
<input type='hidden' name='textfield' value='All fields'\>
<input type='hidden' name='page' value='list'\>
<input type='checkbox' name='exactsearch' value='1' style='vertical-align:middle'\>
<div class='exactmatch'>exact match</div>
<input type='submit' value='Search' class='button'\>
<input type='reset' value='Clear' class='button'\>
</div>
</form>
</div>
	
	<div id='mainmenu'>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='./'>Home</a>
		</span>
		<span class='menuright'></span>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>About</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=about_conoserver'>About ConoServer</a></li>
			<li><a href='?page=about_conotoxins'>About conopeptides</a></li>
			<li><a href='?page=about_us'>About us</a></li>
			<li><a href='?page=links'>Related websites</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Search</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=search&table=protein'>Search a peptide</a></li>
			<li><a href='?page=search&table=nucleicacid'>Search a nucleic acid</a></li>
			<li><a href='?page=search&table=structure'>Search a 3D structure</a></li>
			<li><a href='?page=list&table=reference&listonly=1'>Search by references</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Tools</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=conoprec'>ConoPrec: precursor analysis</a></li>
			<li><a href='?page=ptmdiffmass'>ConoMass 1: differential PTM masses</a></li>
			<li><a href='?page=identifymasslist'>ConoMass 2: identify peptide mass</a></li>
			<li><a href='?page=comparemasses'>Compare mass lists</a></li>
			<li><a href='?page=uniquemasses'>Remove duplicated masses</a></li>
			<li><a href='?page=download'>Download ConoServer's data</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=stats'>Statistics</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=stats&tab=classification'>On classification schemes</a></li>
			<li><a href='?page=stats&tab=superfamilies'>On superfamily signal sequences</a></li>
			<li><a href='?page=stats&tab=organisms'>On cone snail species</a></li>
			<li><a href='?page=stats&tab=3dstructures'>On 3D structures</a></li>
			<li><a href='?page=stats&tab=3doverlays'>On structural scaffold conservation</a></li>
			<li><a href='?page=stats&tab=references'>On journals in ConoServer</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=classification'>Classification</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=classification&type=genesuperfamilies'>Gene superfamilies</a></li>
			<li><a href='?page=classification&type=cysteineframeworks'>Cysteine frameworks</a></li>
			<li><a href='?page=classification&type=pharmacologicalfamilies'>Pharmacological families</a></li>
			</ul>
		</div>
	</li>
	</div>
</div>


<div id='main'>

<h1><a href='?page=card&table=protein&id=1338'>Conantokin-G</a> (P01338) Protein Card</h1>



<fieldset id='general'>
<legend><b>General Information</b></legend>
<a href='images/organism/Conus_geographus.jpg' id='card_image'  rel="lightbox" title='Credits : Kerry Matz, 
National Institute of General Medical Services'><img src='images/organism/Conus_geographus.jpg'/></a>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Name</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=1338'>Conantokin-G</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Alternative name(s)</b></td>
	<td class='text'>
		CGX-1007,Con-G,sleeper	</td>
</tr>
<tr>
	<td width='160px'><b>Organism</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Organism_search%5B%5D=Conus geographus'><i>Conus geographus</i></a> (Geography cone)	</td>
</tr>
<tr>
	<td width='160px'><b>Organism region</b></td>
	<td class='text'>
		Indo-Pacific	</td>
</tr>
<tr>
	<td width='160px'><b>Organism diet</b></td>
	<td class='text'>
		piscivorous	</td>
</tr>
<tr>
	<td width='160px'><b>Protein Type</b></td>
	<td class='text'>
		Wild type	</td>
</tr>
<tr>
	<td width='160px'><b>Protein precursor</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=1353'>Conantokin-G precursor (variant) (1353)</a><br><a href='?page=card&table=protein&id=1373'>Conantokin-G precursor (1373)</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Notes</b></td>
	<td class='text'>
		<P>Conantokin-G is active against the NMDA glutamate receptors, and more precisely a NR2B-selective competitive antagonist (Donevan and McCabe 2000).</P>
<P>Dutt et al., 2019 reported that conantokin-G (1 &mu;M) showed no effect on the zebrafish larvae.</P>	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Classification</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Conopeptide class</b></td>
	<td class='text'>
		conantokin	</td>
</tr>
<tr>
	<td width='160px'><b>Gene superfamily</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Superfam%5B%5D=B1 superfamily&fields[]=ID&fields[]=Name&fields[]=Class&fields[]=Superfam&fields[]=Organism'>B1 superfamily</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Pharmacological family</b></td>
	<td class='text'>
			</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Sequence</b></legend>
<table class='cardtable'>
<tr><td class='cardsequence' colspan='2'><div class='seq'>GE(Gla)(Gla)LQ(Gla)NQ(Gla)LIR(Gla)KSN(nh2)</div></td></tr>
</tr>
<tr>
	<td width='160px'><b>Modified residues</b></td>
	<td class='text'>
		<table class='postable'><tr><th>position</th><th>symbol</th><th>name</th></tr>
<tr><td>3</td><td>Gla</td><td>Gamma carboxylic glutamic acid</td></tr>
<tr><td>4</td><td>Gla</td><td>Gamma carboxylic glutamic acid</td></tr>
<tr><td>7</td><td>Gla</td><td>Gamma carboxylic glutamic acid</td></tr>
<tr><td>10</td><td>Gla</td><td>Gamma carboxylic glutamic acid</td></tr>
<tr><td>14</td><td>Gla</td><td>Gamma carboxylic glutamic acid</td></tr>
<tr><td>18</td><td>nh2</td><td>C-term amidation</td></tr></table>	</td>
</tr>
<tr>
	<td width='160px'><b>Sequence evidence</b></td>
	<td class='text'>
		protein level	</td>
</tr>
<tr>
	<td width='160px'><b>Average Mass</b></td>
	<td class='num'>
		2264.21	</td>
</tr>
<tr>
	<td width='160px'><b>Monoisotopic Mass</b></td>
	<td class='num'>
		2262.94	</td>
</tr>
<tr>
	<td width='160px'><b>Isoelectric Point</b></td>
	<td class='num'>
		2.02	</td>
</tr>
<tr>
	<td width='160px'><b>Extinction Coefficient [280nm]</b></td>
	<td class='num'>
		NA	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Activity</b></legend>
<h2>IC50: Ionotropic glutamate receptors</h2><table class="activitytable"><tr><th>Target</th><th>Organism</th><th colspan="2">IC50</th><th>Agonist</th><th>Ref</th></tr><tr><td>GluN2A</td><td><i>R. norvegicus</i></td><td style="text-align:right">&gt;10uM</td><td></td><td>200uM glutamate</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=17962189&query_hl=1&itool=pubmed_docsum'>Teichert,R.W. <i>et al.</i></a> (2007) </td></tr><tr><td>GluN2B</td><td><i>R. norvegicus</i></td><td style="text-align:right">0.1uM</td><td></td><td>	200uM glutamate</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=17962189&query_hl=1&itool=pubmed_docsum'>Teichert,R.W. <i>et al.</i></a> (2007) </td></tr><tr><td>GluN2C</td><td><i>R. norvegicus</i></td><td style="text-align:right">1uM</td><td></td><td>200uM glutamate</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=17962189&query_hl=1&itool=pubmed_docsum'>Teichert,R.W. <i>et al.</i></a> (2007) </td></tr><tr><td>GluN2D</td><td><i>R. norvegicus</i></td><td style="text-align:right">1uM</td><td></td><td>	200uM glutamate</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=17962189&query_hl=1&itool=pubmed_docsum'>Teichert,R.W. <i>et al.</i></a> (2007) </td></tr></table></fieldset>
<br>

<fieldset>
<legend><b>Synthetic variants</b></legend>
<table><tr><td><a href="?page=card&table=protein&id=5699">Conantokin-G[+10Gla,Gla10Buc,Gla14Buc]</a></td><td style="font-family:monospace">GE(Gla)(Gla)LQ(Gla)NQ(Gla)(Buc)LIR(Buc)KSN(nh2)</td></tr><tr><td><a href="?page=card&table=protein&id=5698">Conantokin-G[Gla10Bsc,Gla14Bsc]</a></td><td style="font-family:monospace">GE(Gla)(Gla)LQ(Gla)NQ(Bsc)LIR(Bsc)KSN(nh2)</td></tr><tr><td><a href="?page=card&table=protein&id=5697">Conantokin-G[Gla10Buc,Gla14Buc]</a></td><td style="font-family:monospace">GE(Gla)(Gla)LQ(Gla)NQ(Buc)LIR(Buc)KSN(nh2)</td></tr><tr><td><a href="?page=card&table=protein&id=5700">Conantokin-G[Gla7Buc,Gla14Buc]</a></td><td style="font-family:monospace">GE(Gla)(Gla)LQ(Huc)NQ(Gla)LIR(Buc)KSN(nh2)</td></tr><tr><td><a href="?page=card&table=protein&id=7643">conantokin-G[+10O]</a></td><td style="font-family:monospace">GE(Gla)(Gla)LQ(Gla)NQO(Gla)LIR(Gla)KSN(nh2)</td></tr></table></pre>
</fieldset>
<br>


<fieldset>
<legend><b>References</b></legend>
<table class='cardtable'>
<tr>
	<td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=12507705&query_hl=1&itool=pubmed_docsum'>Malmberg,A.B., Gilbert,H., McCabe,R.T. and Basbaum,A.I.</a> (2003) Powerful antinociceptive effects of the cone snail venom-derived subtype-selective NMDA receptor antagonists conantokins G and T <i>Pain</i> <b>101</b>:109-116</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=8999936&query_hl=1&itool=pubmed_docsum'>Skjaerbaek,N., Nielsen,K.J., Lewis,R.J., Alewood,P. and Craik,D.J.</a> (1997) Determination of the solution structures of conantokin-G and conantokin-T by CD and NMR spectroscopy <i>J. Biol. Chem.</i> <b>272</b>:2291-2299</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=9188685&query_hl=1&itool=pubmed_docsum'>Rigby,A.C., Baleja,J.D., Furie,B.C. and Furie,B.</a> (1997) Three-dimensional structure of a gamma-carboxyglutamic acid-containing conotoxin, conantokin G, from the marine snail Conus geographus: the metal-free conformer <i>Biochemistry</i> <b>36</b>:6906-6914</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=9398296&query_hl=1&itool=pubmed_docsum'>Rigby,A.C., Baleja,J.D., Li,L., Pedersen,L.G., Furie,B.C. and Furie,B.</a> (1997) Role of gamma-carboxyglutamic acid in the calcium-induced structural transition of conantokin G, a conotoxin from the marine snail Conus geographus <i>Biochemistry</i> <b>36</b>:15677-15684</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=6501296&query_hl=1&itool=pubmed_docsum'>McIntosh,J.M., Olivera,B.M., Cruz,L.J. and Gray,W.R.</a> (1984) Gamma-carboxyglutamate in a neuroactive toxin <i>J. Biol. Chem.</i> <b>259</b>:14343-14346</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10871336&query_hl=1&itool=pubmed_docsum'>Williams,A.J., Dave,J.R., Phillips,J.B., Lin,Y. and Tortella,F.C.</a> (2000) Neuroprotective efficacy and therapeutic window of the high-affinity N-methyl-D-aspartate antagonist conantokin-G: in vitro (primary cerebellar neurons) and in vivo (rat model of transient focal brain ischemia) studies. <i>J. Pharmacol. Exp. Ther.</i> <b>294</b>:378-386</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=11997694&query_hl=1&itool=pubmed_docsum'>Williams,A.J., Ling,G. and Tortella,F.C.</a> (2002) Intrathecal CGX-1007 is neuroprotective in a rat model of focal cerebral ischemia. <i>Neuroreport</i> <b>13</b>:821-824</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=12955387&query_hl=1&itool=pubmed_docsum'>Williams,A.J., Ling,G., Berti,R., Moffett,J.R., Yao,C., Lu,X.M. and Tortella,F.C.</a> (2003) Treatment with the snail peptide CGX-1007 reduces DNA damage and alters gene expression of c-fos and bcl-2 following focal ischemic brain injury in rats. <i>Exp Brain Res</i> <b>153</b>:16-26</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10953056&query_hl=1&itool=pubmed_docsum'>Donevan,S.D. and McCabe,R.T.</a> (2000) Conantokin G is an NR2B-selective competitive antagonist of N-methyl-D-aspartate receptors. <i>Mol. Pharmacol.</i> <b>58</b>:614-623</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=4024137&query_hl=1&itool=pubmed_docsum'>Olivera BM, McIntosh JM, Clark C, Middlemas D, Gray WR, Cruz LJ.</a> (1985) A sleep-inducing peptide from Conus geographus venom. <i>Toxicon</i> <b>23</b>:277-282</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=22306487&query_hl=1&itool=pubmed_docsum'>Balsara,R., Li,N., Weber-Adrian,D., Huang,L. and Castellino,F.J.</a> (2012) Opposing action of conantokin-G on synaptically and extrasynaptically-activated NMDA receptors. <i>Neuropharmacology</i> <b>62</b>:2227-2238</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31780714&query_hl=1&itool=pubmed_docsum'>Dutt,M., Giacomotto,J., Ragnarsson,L., Andersson,Å., Brust,A., Dekan,Z., Alewood,P.F., and Lewis,R.J.</a> (2019) The &alpha;<sub>1</sub>-adrenoceptor inhibitor &rho;-TIA facilitates net hunting in piscivorous Conus tulipa <i>Scientific reports</i> <b>9</b>:17841</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Internal links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Protein Precursor</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=1353'>Conantokin-G precursor (variant) (1353)</a><br><a href='?page=card&table=protein&id=1373'>Conantokin-G precursor (1373)</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Nucleic acids</b></td>
	<td class='text'>
			</td>
</tr>
<tr>
	<td width='160px'><b>Structure</b></td>
	<td class='text'>
		<a href='?page=card&table=structure&id=40'>NMR STRUCTURE OF METAL-FREE CONANTOKIN G, 1 STRUCTURE</a><br><a href='?page=card&table=structure&id=41'>NMDA RECEPTOR ANTAGONIST, CONANTOKIN-G, NMR, 17 STRUCTURES</a><br><a href='?page=card&table=structure&id=44'>NMR STRUCTURE OF CALCIUM BOUND CONFORMER OF CONANTOKIN G, MINIMIZED AVERAGE STRUCTURE</a><br><a href='?page=card&table=structure&id=216'>The Solution Structure of the Magnesium-bound Conantokin-G</a>	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>External links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Ncbi</b></td>
	<td><a href='http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?db=protein&val=1AD7_A'>1AD7_A</a>, <a href='http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?db=protein&val=1AWY_A'>1AWY_A</a>, <a href='http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?db=protein&val=1ONU_A'>1ONU_A</a></td>
</tr>
</table>
</fieldset>
<br>






<fieldset>
<legend><b>Tools</b></legend>
<form action='index.php?page=digest' method='POST'>
<input type='hidden' name='sequence' value='GEEELQENQELIREKSNX'>
<input type='hidden' name='cyclic' value='0'>
<input type='submit' class='button' value='Digest Peptide'>
</form>
</fieldset>

	
</div>

<div id='foot'>
<u>Please cite</u>:<br>
Kaas Q, Yu R, Jin AH, Dutertre S and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>ConoServer: updated content, knowledge, and discovery tools in the conopeptide database.</a> <i>Nucleic Acids Research</i> (2012) <b>40</b>(Database issue):D325-30<br>
Kaas Q, Westermann JC, Halai R, Wang CK and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/18065428'>ConoServer, a database for conopeptide sequences and structures.</a> <i>Bioinformatics</i> (2008) <b>24</b>(3):445-6
<p>
ConoServer is managed at the Institute of Molecular Bioscience <a href='http://imb.uq.edu.au/'>IMB</a>, Brisbane, Australia.
</p>
<p>The database and computational tools found on this website may be used for academic research only, provided that it is referred to ConoServer, the database of conotoxins (http://www.conoserver.org) and the above reference is cited. For any other use please contact David Craik (d.craik@imb.uq.edu.au).
</p>
<p>
<a href='http://www.imb.uq.edu.au/'>
<img src='images/IMB_UQ_small.png'/>
</a>
Contacts:<br/>
<a href="mailto:d.craik@imb.uq.edu.au?subject=ConoServer">David Craik</a><br/>
<a href="mailto:q.kaas@imb.uq.edu.au?subject=ConoServer">Quentin Kaas</a><br/>
</p>
<p>
Last updated: Wednesday 15 January 2025</p>
</div>

</div>
</div>

</div>

</body>
</html>
