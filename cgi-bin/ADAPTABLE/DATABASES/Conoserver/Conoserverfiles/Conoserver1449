<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="description" content="ConoServer is a database of toxins isolated from Cone snails. The database provide annotated information on nucleic acid sequences, protein sequences and 3D structures. It tracks known cysteins frameworks, genetic superfamily and pharmacological families."/>
	<meta http-equiv="keywords" content="ConoServer, cone snail, cone snail toxin, conopeptide, conotoxin, conophan, contryphan, conantokin, conolysin, conopressin, conomarphin, conorfamide, contulakin, David Craik, Quentin Kaas"/>
	<title>ConoServer</title>
	<link rel="StyleSheet" href="index.css?b" type="text/css">

	<link rel="stylesheet" href="lightbox.css" type="text/css" media="screen" />
	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
	<script src="js/lightbox.js" type="text/javascript"></script>
	<script src="js/jquery-3.6.0.min.js" type="text/javascript"></script>
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
<script>
function SelectValue(selectid,value) {
	var i;
	var select = document.getElementById(selectid);
	for(i = 0; i < select.options.length; i++) {
		if (select.options[i].value == value) {
			select.options[i].selected= true;
		} else {
			select.options[i].selected= false;
		}
	}
}
</script>


<div id='globalcontainer'>

<div id='pagehead'>
	<div>
	<a href="index.php">
		<img src="images/conoserver_small.png" alt="ConoServer: A database for conotoxins" class='expl'/>
	</a>
	</div>
	<div><a class='primarycitation' href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>Kaas Q et al. (2012) <i>Nucleic Acids Res.</i></a>
<form>
<div class='firstline'>
Search a
<select name="table">
  <option value='protein'>Protein</option>
  <option value='nucleicacid'>Nucleic acids</option>
  <option value='structure'>3D structure</option>
</select>
name
</div>
<div class='secondline'>
<input type='text'   name='quicktext' size='30' id='textsearch'\>
<input type='hidden' name='textfield' value='All fields'\>
<input type='hidden' name='page' value='list'\>
<input type='checkbox' name='exactsearch' value='1' style='vertical-align:middle'\>
<div class='exactmatch'>exact match</div>
<input type='submit' value='Search' class='button'\>
<input type='reset' value='Clear' class='button'\>
</div>
</form>
</div>
	
	<div id='mainmenu'>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='./'>Home</a>
		</span>
		<span class='menuright'></span>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>About</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=about_conoserver'>About ConoServer</a></li>
			<li><a href='?page=about_conotoxins'>About conopeptides</a></li>
			<li><a href='?page=about_us'>About us</a></li>
			<li><a href='?page=links'>Related websites</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Search</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=search&table=protein'>Search a peptide</a></li>
			<li><a href='?page=search&table=nucleicacid'>Search a nucleic acid</a></li>
			<li><a href='?page=search&table=structure'>Search a 3D structure</a></li>
			<li><a href='?page=list&table=reference&listonly=1'>Search by references</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Tools</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=conoprec'>ConoPrec: precursor analysis</a></li>
			<li><a href='?page=ptmdiffmass'>ConoMass 1: differential PTM masses</a></li>
			<li><a href='?page=identifymasslist'>ConoMass 2: identify peptide mass</a></li>
			<li><a href='?page=comparemasses'>Compare mass lists</a></li>
			<li><a href='?page=uniquemasses'>Remove duplicated masses</a></li>
			<li><a href='?page=download'>Download ConoServer's data</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=stats'>Statistics</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=stats&tab=classification'>On classification schemes</a></li>
			<li><a href='?page=stats&tab=superfamilies'>On superfamily signal sequences</a></li>
			<li><a href='?page=stats&tab=organisms'>On cone snail species</a></li>
			<li><a href='?page=stats&tab=3dstructures'>On 3D structures</a></li>
			<li><a href='?page=stats&tab=3doverlays'>On structural scaffold conservation</a></li>
			<li><a href='?page=stats&tab=references'>On journals in ConoServer</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=classification'>Classification</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=classification&type=genesuperfamilies'>Gene superfamilies</a></li>
			<li><a href='?page=classification&type=cysteineframeworks'>Cysteine frameworks</a></li>
			<li><a href='?page=classification&type=pharmacologicalfamilies'>Pharmacological families</a></li>
			</ul>
		</div>
	</li>
	</div>
</div>


<div id='main'>

<h1><a href='?page=card&table=protein&id=1449'>PIVA</a> (P01449) Protein Card</h1>



<fieldset id='general'>
<legend><b>General Information</b></legend>
<a href='images/organism/Conus_purpurascens.jpg' id='card_image'  rel="lightbox" title='&copy; Jeffrey Milisen and Jon-Paul Bingham'><img src='images/organism/Conus_purpurascens.jpg'/></a>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Name</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=1449'>PIVA</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Alternative name(s)</b></td>
	<td class='text'>
		P4a	</td>
</tr>
<tr>
	<td width='160px'><b>Organism</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Organism_search%5B%5D=Conus purpurascens'><i>Conus purpurascens</i></a>	</td>
</tr>
<tr>
	<td width='160px'><b>Organism region</b></td>
	<td class='text'>
		Eastern Pacific	</td>
</tr>
<tr>
	<td width='160px'><b>Organism diet</b></td>
	<td class='text'>
		piscivorous	</td>
</tr>
<tr>
	<td width='160px'><b>Protein Type</b></td>
	<td class='text'>
		Wild type	</td>
</tr>
<tr>
	<td width='160px'><b>Protein precursor</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=4222'>PIVA precursor (4222)</a>	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Classification</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Conopeptide class</b></td>
	<td class='text'>
		conotoxin	</td>
</tr>
<tr>
	<td width='160px'><b>Gene superfamily</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Superfam%5B%5D=A superfamily&fields[]=ID&fields[]=Name&fields[]=Class&fields[]=Superfam&fields[]=Organism'>A superfamily</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Cysteine framework</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Framework%5B%5D=IV&fields[]=Class&fields[]=Framework&fields[]=Organism'>IV</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Pharmacological family</b></td>
	<td class='text'>
		alpha conotoxin	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Sequence</b></legend>
<table class='cardtable'>
<tr><td class='cardsequence' colspan='2'><div class='seq'>G<b>C</b><b>C</b>GSYONAA<b>C</b>HO<b>C</b>S<b>C</b>KDROSY<b>C</b>GQ(nh2)</div></td></tr>
</tr>
<tr>
	<td width='160px'><b>Modified residues</b></td>
	<td class='text'>
		<table class='postable'><tr><th>position</th><th>symbol</th><th>name</th></tr>
<tr><td>7</td><td>O</td><td>4-Hydroxyproline</td></tr>
<tr><td>13</td><td>O</td><td>4-Hydroxyproline</td></tr>
<tr><td>20</td><td>O</td><td>4-Hydroxyproline</td></tr>
<tr><td>26</td><td>nh2</td><td>C-term amidation</td></tr></table>	</td>
</tr>
<tr>
	<td width='160px'><b>Sequence evidence</b></td>
	<td class='text'>
		protein level	</td>
</tr>
<tr>
	<td width='160px'><b>Average Mass</b></td>
	<td class='num'>
		2648.88	</td>
</tr>
<tr>
	<td width='160px'><b>Monoisotopic Mass</b></td>
	<td class='num'>
		2646.92	</td>
</tr>
<tr>
	<td width='160px'><b>Isoelectric Point</b></td>
	<td class='num'>
		10.45	</td>
</tr>
<tr>
	<td width='160px'><b>Extinction Coefficient [280nm]</b></td>
	<td class='num'>
		2980.00	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Activity</b></legend>
<h2>IC50: Nicotinic acetylcholine receptors</h2><table class="activitytable"><tr><th>Target</th><th>Organism</th><th colspan="2">IC50</th><th>Agonist</th><th>Ref</th></tr><tr><td>&alpha;1&beta;1&delta;&epsilon;</td><td><i>M. musculus</i></td><td style="text-align:right">2.3nM</td><td>+/-0.7</td><td>1-10uM Ach</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=16430227&query_hl=1&itool=pubmed_docsum'>Teichert,R.W. <i>et al.</i></a> (2006) </td></tr><tr><td>&alpha;1&beta;1&gamma;&delta;</td><td><i>M. musculus</i></td><td style="text-align:right">22nM</td><td>+/-4</td><td>1-10uM Ach</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=16430227&query_hl=1&itool=pubmed_docsum'>Teichert,R.W. <i>et al.</i></a> (2006) </td></tr></table></fieldset>
<br>



<fieldset>
<legend><b>References</b></legend>
<table class='cardtable'>
<tr>
	<td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=16430227&query_hl=1&itool=pubmed_docsum'>Teichert,R.W., Lopez-Vera,E., Gulyas,J., Watkins,M., Rivier,J. and Olivera,B.M.</a> (2006) Definition and characterization of the short alphaA-conotoxins: a single residue determines dissociation kinetics from the fetal muscle nicotinic acetylcholine receptor <i>Biochemistry</i> <b>45</b>:1304-1312</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=7673220&query_hl=1&itool=pubmed_docsum'>Hopkins,C., Grilley,M., Miller,C., Shon,K.J., Cruz,L.J., Gray,W.R., Dykert,J., Rivier,J., Yoshikami,D. and Olivera,B.M.</a> (1995) A new family of Conus peptides targeted to the nicotinic acetylcholine receptor <i>J. Biol. Chem.</i> <b>270</b>:22361-22367</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=9048550&query_hl=1&itool=pubmed_docsum'>Han,K.H., Hwang,K.J., Kim,S.M., Kim,S.K., Gray,W.R., Olivera,B.M., Rivier,J. and Shon,K.J.</a> (1997) NMR structure determination of a novel conotoxin, [Pro 7,13] alpha A-conotoxin PIVA <i>Biochemistry</i> <b>36</b>:1669-1677</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=29321522&query_hl=1&itool=pubmed_docsum'>Himaya, S.W.A., Mari, F. and Lewis, R.J.</a> (2018) Accelerated proteomic visualization of individual predatory venoms of Conus purpurascens reveals separately evolved predation-evoked venom cabals <i>Scientific reports</i> <b>8</b>:1-8</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Internal links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Protein Precursor</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=4222'>PIVA precursor (4222)</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Nucleic acids</b></td>
	<td class='text'>
			</td>
</tr>
<tr>
	<td width='160px'><b>Structure</b></td>
	<td class='text'>
			</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>External links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>UniProtKB/Swiss-Prot</b></td>
	<td><a href='http://www.uniprot.org/uniprot/P55963'>P55963</a></td>
</tr>
<tr>
	<td width='160px'><b>Ncbi</b></td>
	<td><a href='http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?db=protein&val=P55963'>P55963</a></td>
</tr>
</table>
</fieldset>
<br>






<fieldset>
<legend><b>Tools</b></legend>
<form action='index.php?page=digest' method='POST'>
<input type='hidden' name='sequence' value='GCCGSYPNAACHPCSCKDRPSYCGQX'>
<input type='hidden' name='cyclic' value='0'>
<input type='submit' class='button' value='Digest Peptide'>
</form>
</fieldset>

	
</div>

<div id='foot'>
<u>Please cite</u>:<br>
Kaas Q, Yu R, Jin AH, Dutertre S and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>ConoServer: updated content, knowledge, and discovery tools in the conopeptide database.</a> <i>Nucleic Acids Research</i> (2012) <b>40</b>(Database issue):D325-30<br>
Kaas Q, Westermann JC, Halai R, Wang CK and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/18065428'>ConoServer, a database for conopeptide sequences and structures.</a> <i>Bioinformatics</i> (2008) <b>24</b>(3):445-6
<p>
ConoServer is managed at the Institute of Molecular Bioscience <a href='http://imb.uq.edu.au/'>IMB</a>, Brisbane, Australia.
</p>
<p>The database and computational tools found on this website may be used for academic research only, provided that it is referred to ConoServer, the database of conotoxins (http://www.conoserver.org) and the above reference is cited. For any other use please contact David Craik (d.craik@imb.uq.edu.au).
</p>
<p>
<a href='http://www.imb.uq.edu.au/'>
<img src='images/IMB_UQ_small.png'/>
</a>
Contacts:<br/>
<a href="mailto:d.craik@imb.uq.edu.au?subject=ConoServer">David Craik</a><br/>
<a href="mailto:q.kaas@imb.uq.edu.au?subject=ConoServer">Quentin Kaas</a><br/>
</p>
<p>
Last updated: Wednesday 15 January 2025</p>
</div>

</div>
</div>

</div>

</body>
</html>
