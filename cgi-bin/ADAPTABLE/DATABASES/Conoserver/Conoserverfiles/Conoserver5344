<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="description" content="ConoServer is a database of toxins isolated from Cone snails. The database provide annotated information on nucleic acid sequences, protein sequences and 3D structures. It tracks known cysteins frameworks, genetic superfamily and pharmacological families."/>
	<meta http-equiv="keywords" content="ConoServer, cone snail, cone snail toxin, conopeptide, conotoxin, conophan, contryphan, conantokin, conolysin, conopressin, conomarphin, conorfamide, contulakin, David Craik, Quentin Kaas"/>
	<title>ConoServer</title>
	<link rel="StyleSheet" href="index.css?b" type="text/css">

	<link rel="stylesheet" href="lightbox.css" type="text/css" media="screen" />
	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
	<script src="js/lightbox.js" type="text/javascript"></script>
	<script src="js/jquery-3.6.0.min.js" type="text/javascript"></script>
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
<script>
function SelectValue(selectid,value) {
	var i;
	var select = document.getElementById(selectid);
	for(i = 0; i < select.options.length; i++) {
		if (select.options[i].value == value) {
			select.options[i].selected= true;
		} else {
			select.options[i].selected= false;
		}
	}
}
</script>


<div id='globalcontainer'>

<div id='pagehead'>
	<div>
	<a href="index.php">
		<img src="images/conoserver_small.png" alt="ConoServer: A database for conotoxins" class='expl'/>
	</a>
	</div>
	<div><a class='primarycitation' href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>Kaas Q et al. (2012) <i>Nucleic Acids Res.</i></a>
<form>
<div class='firstline'>
Search a
<select name="table">
  <option value='protein'>Protein</option>
  <option value='nucleicacid'>Nucleic acids</option>
  <option value='structure'>3D structure</option>
</select>
name
</div>
<div class='secondline'>
<input type='text'   name='quicktext' size='30' id='textsearch'\>
<input type='hidden' name='textfield' value='All fields'\>
<input type='hidden' name='page' value='list'\>
<input type='checkbox' name='exactsearch' value='1' style='vertical-align:middle'\>
<div class='exactmatch'>exact match</div>
<input type='submit' value='Search' class='button'\>
<input type='reset' value='Clear' class='button'\>
</div>
</form>
</div>
	
	<div id='mainmenu'>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='./'>Home</a>
		</span>
		<span class='menuright'></span>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>About</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=about_conoserver'>About ConoServer</a></li>
			<li><a href='?page=about_conotoxins'>About conopeptides</a></li>
			<li><a href='?page=about_us'>About us</a></li>
			<li><a href='?page=links'>Related websites</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Search</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=search&table=protein'>Search a peptide</a></li>
			<li><a href='?page=search&table=nucleicacid'>Search a nucleic acid</a></li>
			<li><a href='?page=search&table=structure'>Search a 3D structure</a></li>
			<li><a href='?page=list&table=reference&listonly=1'>Search by references</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Tools</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=conoprec'>ConoPrec: precursor analysis</a></li>
			<li><a href='?page=ptmdiffmass'>ConoMass 1: differential PTM masses</a></li>
			<li><a href='?page=identifymasslist'>ConoMass 2: identify peptide mass</a></li>
			<li><a href='?page=comparemasses'>Compare mass lists</a></li>
			<li><a href='?page=uniquemasses'>Remove duplicated masses</a></li>
			<li><a href='?page=download'>Download ConoServer's data</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=stats'>Statistics</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=stats&tab=classification'>On classification schemes</a></li>
			<li><a href='?page=stats&tab=superfamilies'>On superfamily signal sequences</a></li>
			<li><a href='?page=stats&tab=organisms'>On cone snail species</a></li>
			<li><a href='?page=stats&tab=3dstructures'>On 3D structures</a></li>
			<li><a href='?page=stats&tab=3doverlays'>On structural scaffold conservation</a></li>
			<li><a href='?page=stats&tab=references'>On journals in ConoServer</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=classification'>Classification</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=classification&type=genesuperfamilies'>Gene superfamilies</a></li>
			<li><a href='?page=classification&type=cysteineframeworks'>Cysteine frameworks</a></li>
			<li><a href='?page=classification&type=pharmacologicalfamilies'>Pharmacological families</a></li>
			</ul>
		</div>
	</li>
	</div>
</div>


<div id='main'>

<h1><a href='?page=card&table=protein&id=5344'>GeXIVA[ribbon]</a> (P05344) Protein Card</h1>



<fieldset id='general'>
<legend><b>General Information</b></legend>
<a href='images/organism/Conus_generalis.jpg' id='card_image'  rel="lightbox" title='&copy; David Touitou<br><a href="http://www.seashell-collector.com">http://www.seashell-collector.com</a>'><img src='images/organism/Conus_generalis.jpg'/></a>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Name</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=5344'>GeXIVA[ribbon]</a> (named by ConoServer)	</td>
</tr>
<tr>
	<td width='160px'><b>Alternative name(s)</b></td>
	<td class='text'>
		Ge14a,GeXIVA[1-4]	</td>
</tr>
<tr>
	<td width='160px'><b>Organism</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Organism_search%5B%5D=Conus generalis'><i>Conus generalis</i></a>	</td>
</tr>
<tr>
	<td width='160px'><b>Organism region</b></td>
	<td class='text'>
		Indo-Pacific	</td>
</tr>
<tr>
	<td width='160px'><b>Organism diet</b></td>
	<td class='text'>
		vermivorous	</td>
</tr>
<tr>
	<td width='160px'><b>Protein Type</b></td>
	<td class='text'>
		Wild type	</td>
</tr>
<tr>
	<td width='160px'><b>Protein precursor</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=5224'>GeXIVA precursor (5224)</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Notes</b></td>
	<td class='text'>
		<p>GeXIVA inhibits Sf9 cell growth (Gao et al., 2013). There is a discrepancy in the sequence initally published in GenBank in 2012 and the sequence published in peer-reviewed journals (involving the same authors).</p>

<p>Yousuf et al. (2021) discovered that GeXIVA[ribbon] does not affect the membrane excitability of mouse DRG neurons, and inhibits Cav2.2 through the GABA<sub>B</sub> receptor pathway.</p>




	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Classification</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Conopeptide class</b></td>
	<td class='text'>
		conotoxin	</td>
</tr>
<tr>
	<td width='160px'><b>Gene superfamily</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Superfam%5B%5D=O1 superfamily&fields[]=ID&fields[]=Name&fields[]=Class&fields[]=Superfam&fields[]=Organism'>O1 superfamily</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Cysteine framework</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Framework%5B%5D=XIV&fields[]=Class&fields[]=Framework&fields[]=Organism'>XIV</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Pharmacological family</b></td>
	<td class='text'>
		alpha conotoxin	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Sequence</b></legend>
<table class='cardtable'>
<tr><td class='cardsequence' colspan='2'><div class='seq'>T<b>C</b>RSSGRY<b>C</b>RSPYDRRRRY<b>C</b>RRITDA<b>C</b>V</div></td></tr>
</tr>
<tr>
	<td width='160px'><b>Sequence evidence</b></td>
	<td class='text'>
		protein level	</td>
</tr>
<tr>
	<td width='160px'><b>Average Mass</b></td>
	<td class='num'>
		3452.92	</td>
</tr>
<tr>
	<td width='160px'><b>Monoisotopic Mass</b></td>
	<td class='num'>
		3450.62	</td>
</tr>
<tr>
	<td width='160px'><b>Isoelectric Point</b></td>
	<td class='num'>
		12.80	</td>
</tr>
<tr>
	<td width='160px'><b>Extinction Coefficient [280nm]</b></td>
	<td class='num'>
		4470.00	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Activity</b></legend>
<h2>IC50: Nicotinic acetylcholine receptors</h2><table class="activitytable"><tr><th>Target</th><th>Organism</th><th colspan="2">IC50</th><th>n<sub>hill</sub></th><th>Agonist</th><th>Ref</th></tr><tr><td>&alpha;1&beta;1&delta;&epsilon;</td><td><i>M. musculus</i></td><td style="text-align:right">840 nM</td><td>[583-1206]</td><td style="text-align:right"></td><td>Ach (10 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31986036&query_hl=1&itool=pubmed_docsum'>Xu <i>et al.</i></a> (2020) </td></tr><tr><td>&alpha;2&beta;2</td><td><i>R. norvegicus</i></td><td style="text-align:right">440 nM</td><td>[335-575]</td><td style="text-align:right"></td><td>Ach (100 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31986036&query_hl=1&itool=pubmed_docsum'>Xu <i>et al.</i></a> (2020) </td></tr><tr><td>&alpha;3&beta;2</td><td><i>R. norvegicus</i></td><td style="text-align:right">1250 nM</td><td>[782-1991]</td><td style="text-align:right"></td><td>Ach (100 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31986036&query_hl=1&itool=pubmed_docsum'>Xu <i>et al.</i></a> (2020) </td></tr><tr><td>&alpha;3&beta;4</td><td><i>R. norvegicus</i></td><td style="text-align:right">3400 nM</td><td>[1279-9048]</td><td style="text-align:right"></td><td>Ach (100 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31986036&query_hl=1&itool=pubmed_docsum'>Xu <i>et al.</i></a> (2020) </td></tr><tr><td>&alpha;6/&alpha;3&beta;4</td><td><i>R. norvegicus</i></td><td style="text-align:right">1110 nM</td><td>[30-1949]</td><td style="text-align:right"></td><td>Ach (100 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31986036&query_hl=1&itool=pubmed_docsum'>Xu <i>et al.</i></a> (2020) </td></tr><tr><td>&alpha;7</td><td><i>R. norvegicus</i></td><td style="text-align:right">2830 nM</td><td>[1910-4188]</td><td style="text-align:right"></td><td>Ach (200 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31986036&query_hl=1&itool=pubmed_docsum'>Xu <i>et al.</i></a> (2020) </td></tr><tr><td>&alpha;9&alpha;10</td><td><i>H. sapiens</i></td><td style="text-align:right">35.1 nM</td><td>+/-2.7</td><td style="text-align:right"></td><td>Ach (6 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=28851841&query_hl=1&itool=pubmed_docsum'>Wu X <i>et al.</i></a> (2017) </td></tr><tr><td></td><td><i>R. norvegicus</i></td><td style="text-align:right">7 nM</td><td>[3.6-13.4]</td><td style="text-align:right"></td><td>Ach (10 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=26170295&query_hl=1&itool=pubmed_docsum'>Luo,S. <i>et al.</i></a> (2015) </td></tr><tr><td></td><td></td><td style="text-align:right">7.0 nM</td><td>[3.6-13.4]</td><td style="text-align:right">0.79</td><td>Ach (10 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=26170295&query_hl=1&itool=pubmed_docsum'>Luo,S. <i>et al.</i></a> (2015) </td></tr><tr><td></td><td></td><td style="text-align:right">16 nM</td><td>[13-20]</td><td style="text-align:right"></td><td>Ach (10 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31986036&query_hl=1&itool=pubmed_docsum'>Xu <i>et al.</i></a> (2020) </td></tr></table><h2>Percentage inhibition: Nicotinic acetylcholine receptors</h2><table class="activitytable"><tr><th>Target</th><th>Organism</th><th colspan="2">% inhibition</th><th>Concentration</th><th>Agonist</th><th>Ref</th></tr><tr><td>&alpha;9&alpha;10</td><td><i>H. sapiens</i></td><td style="text-align:right">76</td><td>+/-2</td><td>100 nM</td><td>Ach (6 uM)</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=28851841&query_hl=1&itool=pubmed_docsum'>Wu X <i>et al.</i></a> (2017) </td></tr></table></fieldset>
<br>

<fieldset>
<legend><b>Synthetic variants</b></legend>
<table><tr><td><a href="?page=card&table=protein&id=8691">GeXIVA[C2A,C27A]</a></td><td style="font-family:monospace">TARSSGRY<b>C</b>RSPYDRRRRY<b>C</b>RRITDAAV</td></tr><tr><td><a href="?page=card&table=protein&id=8692">GeXIVA[C2S,C27S]</a></td><td style="font-family:monospace">TSRSSGRY<b>C</b>RSPYDRRRRY<b>C</b>RRITDASV</td></tr><tr><td><a href="?page=card&table=protein&id=8693">GeXIVA[C9A,C20A]</a></td><td style="font-family:monospace">T<b>C</b>RSSGRYARSPYDRRRRYARRITDA<b>C</b>V</td></tr><tr><td><a href="?page=card&table=protein&id=8694">GeXIVA[C9S,C20S]</a></td><td style="font-family:monospace">T<b>C</b>RSSGRYSRSPYDRRRRYSRRITDA<b>C</b>V</td></tr><tr><td><a href="?page=card&table=protein&id=10450">GeXIVA[insC_G, ribbon] cyclic</a></td><td style="font-family:monospace">T<b>C</b>RSSGRY<b>C</b>RSPYDRRRRY<b>C</b>RRITDA<b>C</b>VG</td></tr><tr><td><a href="?page=card&table=protein&id=10461">GeXIVA[insC_GAAGG, ribbon] cyclic</a></td><td style="font-family:monospace">T<b>C</b>RSSGRY<b>C</b>RSPYDRRRRY<b>C</b>RRITDA<b>C</b>VGAAGG</td></tr><tr><td><a href="?page=card&table=protein&id=10452">GeXIVA[insC_GAG, ribbon] cyclic</a></td><td style="font-family:monospace">T<b>C</b>RSSGRY<b>C</b>RSPYDRRRRY<b>C</b>RRITDA<b>C</b>VGAG</td></tr><tr><td><a href="?page=card&table=protein&id=10460">GeXIVA[insC_GAGG, ribbon] cyclic</a></td><td style="font-family:monospace">T<b>C</b>RSSGRY<b>C</b>RSPYDRRRRY<b>C</b>RRITDA<b>C</b>VGAGG</td></tr><tr><td><a href="?page=card&table=protein&id=10451">GeXIVA[insC_GG, ribbon] cyclic</a></td><td style="font-family:monospace">T<b>C</b>RSSGRY<b>C</b>RSPYDRRRRY<b>C</b>RRITDA<b>C</b>VGG</td></tr></table></pre>
</fieldset>
<br>


<fieldset>
<legend><b>References</b></legend>
<table class='cardtable'>
<tr>
	<td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=22825834&query_hl=1&itool=pubmed_docsum'>Gao,B., Zhangsun,D., Wu,Y., Lin,B., Zhu,X. and Luo,S.</a> (2013) Expression, renaturation and biological activity of recombinant conotoxin GeXIVAWT. <i>Appl. Microbiol. Biotechnol.</i> <b>97</b>:1223-1230</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=26170295&query_hl=1&itool=pubmed_docsum'>Luo,S., Zhangsun,D., Harvey,P.J., Kaas,Q., Wu,Y., Zhu,X., Hu,Y., Li,X., Tsetlin,V.I., Christensen,S., Romero,H.K., McIntyre,M., Dowell,C., Baxter,J.C., Elmslie,K.S., Craik,D.J. and McIntosh,J.M.</a> (2015) Cloning, synthesis, and characterization of &alpha;O-conotoxin GeXIVA, a potent &alpha;9&alpha;10 nicotinic acetylcholine receptor antagonist. <i>Proc. Natl. Acad. Sci. U.S.A.</i> <b>112</b>-35</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31060282&query_hl=1&itool=pubmed_docsum'>Wang, H., Li, X., Zhangsun, D., Yu, G., Su, R. and Luo, S.</a> (2019) The &alpha;9&alpha;10 Nicotinic Acetylcholine Receptor Antagonist &alpha;O-Conotoxin GeXIVA[1,2] Alleviates and Reverses Chemotherapy-Induced Neuropathic Pain <i>Marine drugs</i> <b>17</b>:265</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=31986036&query_hl=1&itool=pubmed_docsum'>Xu, P., Kaas, Q., Wu, Y., Zhu, X., Li, X., Harvey, P.J., Zhangsun, D., Craik, D.J. and Luo, S</a> (2020) Structure and Activity Studies of Disulfide-Deficient Analogues of &alpha;O-Conotoxin GeXIVA <i>Journal of Medicinal Chemistry</i> </td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=34738241&query_hl=1&itool=pubmed_docsum'>Yousuf,A., Wu,X., Bony,A.R., Sadeghi,M., Huang,Y.H., Craik,D.J., Adams,D.J.</a> (2021) &alpha;O-Conotoxin GeXIVA isomers modulate N-type calcium (Ca<sub>V</sub> 2.2) channels and inwardly-rectifying potassium (GIRK) channels via GABAB receptor activation <i>J Neurochem</i> </td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=28851841&query_hl=1&itool=pubmed_docsum'>Wu X, Huang YH, Kaas Q, Harvey PJ, Wang CK, Tae HS, Adams DJ, Craik DJ.</a> (2017) Backbone cyclization of analgesic conotoxin GeXIVA facilitates direct folding of the ribbon isomer <i>J Biol Chem</i> <b>292</b>:17101-17112</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Internal links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Protein Precursor</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=5224'>GeXIVA precursor (5224)</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Nucleic acids</b></td>
	<td class='text'>
			</td>
</tr>
<tr>
	<td width='160px'><b>Structure</b></td>
	<td class='text'>
			</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>External links</b></legend>
<table class='cardtable'>
</table>
</fieldset>
<br>






<fieldset>
<legend><b>Tools</b></legend>
<form action='index.php?page=digest' method='POST'>
<input type='hidden' name='sequence' value='TCRSSGRYCRSPYDRRRRYCRRITDACV'>
<input type='hidden' name='cyclic' value='0'>
<input type='submit' class='button' value='Digest Peptide'>
</form>
</fieldset>

	
</div>

<div id='foot'>
<u>Please cite</u>:<br>
Kaas Q, Yu R, Jin AH, Dutertre S and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>ConoServer: updated content, knowledge, and discovery tools in the conopeptide database.</a> <i>Nucleic Acids Research</i> (2012) <b>40</b>(Database issue):D325-30<br>
Kaas Q, Westermann JC, Halai R, Wang CK and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/18065428'>ConoServer, a database for conopeptide sequences and structures.</a> <i>Bioinformatics</i> (2008) <b>24</b>(3):445-6
<p>
ConoServer is managed at the Institute of Molecular Bioscience <a href='http://imb.uq.edu.au/'>IMB</a>, Brisbane, Australia.
</p>
<p>The database and computational tools found on this website may be used for academic research only, provided that it is referred to ConoServer, the database of conotoxins (http://www.conoserver.org) and the above reference is cited. For any other use please contact David Craik (d.craik@imb.uq.edu.au).
</p>
<p>
<a href='http://www.imb.uq.edu.au/'>
<img src='images/IMB_UQ_small.png'/>
</a>
Contacts:<br/>
<a href="mailto:d.craik@imb.uq.edu.au?subject=ConoServer">David Craik</a><br/>
<a href="mailto:q.kaas@imb.uq.edu.au?subject=ConoServer">Quentin Kaas</a><br/>
</p>
<p>
Last updated: Wednesday 15 January 2025</p>
</div>

</div>
</div>

</div>

</body>
</html>
