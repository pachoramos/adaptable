<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="description" content="ConoServer is a database of toxins isolated from Cone snails. The database provide annotated information on nucleic acid sequences, protein sequences and 3D structures. It tracks known cysteins frameworks, genetic superfamily and pharmacological families."/>
	<meta http-equiv="keywords" content="ConoServer, cone snail, cone snail toxin, conopeptide, conotoxin, conophan, contryphan, conantokin, conolysin, conopressin, conomarphin, conorfamide, contulakin, David Craik, Quentin Kaas"/>
	<title>ConoServer</title>
	<link rel="StyleSheet" href="index.css?b" type="text/css">

	<link rel="stylesheet" href="lightbox.css" type="text/css" media="screen" />
	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
	<script src="js/lightbox.js" type="text/javascript"></script>
	<script src="js/jquery-3.6.0.min.js" type="text/javascript"></script>
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
<script>
function SelectValue(selectid,value) {
	var i;
	var select = document.getElementById(selectid);
	for(i = 0; i < select.options.length; i++) {
		if (select.options[i].value == value) {
			select.options[i].selected= true;
		} else {
			select.options[i].selected= false;
		}
	}
}
</script>


<div id='globalcontainer'>

<div id='pagehead'>
	<div>
	<a href="index.php">
		<img src="images/conoserver_small.png" alt="ConoServer: A database for conotoxins" class='expl'/>
	</a>
	</div>
	<div><a class='primarycitation' href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>Kaas Q et al. (2012) <i>Nucleic Acids Res.</i></a>
<form>
<div class='firstline'>
Search a
<select name="table">
  <option value='protein'>Protein</option>
  <option value='nucleicacid'>Nucleic acids</option>
  <option value='structure'>3D structure</option>
</select>
name
</div>
<div class='secondline'>
<input type='text'   name='quicktext' size='30' id='textsearch'\>
<input type='hidden' name='textfield' value='All fields'\>
<input type='hidden' name='page' value='list'\>
<input type='checkbox' name='exactsearch' value='1' style='vertical-align:middle'\>
<div class='exactmatch'>exact match</div>
<input type='submit' value='Search' class='button'\>
<input type='reset' value='Clear' class='button'\>
</div>
</form>
</div>
	
	<div id='mainmenu'>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='./'>Home</a>
		</span>
		<span class='menuright'></span>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>About</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=about_conoserver'>About ConoServer</a></li>
			<li><a href='?page=about_conotoxins'>About conopeptides</a></li>
			<li><a href='?page=about_us'>About us</a></li>
			<li><a href='?page=links'>Related websites</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Search</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=search&table=protein'>Search a peptide</a></li>
			<li><a href='?page=search&table=nucleicacid'>Search a nucleic acid</a></li>
			<li><a href='?page=search&table=structure'>Search a 3D structure</a></li>
			<li><a href='?page=list&table=reference&listonly=1'>Search by references</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Tools</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=conoprec'>ConoPrec: precursor analysis</a></li>
			<li><a href='?page=ptmdiffmass'>ConoMass 1: differential PTM masses</a></li>
			<li><a href='?page=identifymasslist'>ConoMass 2: identify peptide mass</a></li>
			<li><a href='?page=comparemasses'>Compare mass lists</a></li>
			<li><a href='?page=uniquemasses'>Remove duplicated masses</a></li>
			<li><a href='?page=download'>Download ConoServer's data</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=stats'>Statistics</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=stats&tab=classification'>On classification schemes</a></li>
			<li><a href='?page=stats&tab=superfamilies'>On superfamily signal sequences</a></li>
			<li><a href='?page=stats&tab=organisms'>On cone snail species</a></li>
			<li><a href='?page=stats&tab=3dstructures'>On 3D structures</a></li>
			<li><a href='?page=stats&tab=3doverlays'>On structural scaffold conservation</a></li>
			<li><a href='?page=stats&tab=references'>On journals in ConoServer</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=classification'>Classification</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=classification&type=genesuperfamilies'>Gene superfamilies</a></li>
			<li><a href='?page=classification&type=cysteineframeworks'>Cysteine frameworks</a></li>
			<li><a href='?page=classification&type=pharmacologicalfamilies'>Pharmacological families</a></li>
			</ul>
		</div>
	</li>
	</div>
</div>


<div id='main'>

<h1><a href='?page=card&table=protein&id=4377'>PnIA [sTy15Y]</a> (P04377) Protein Card</h1>



<fieldset id='general'>
<legend><b>General Information</b></legend>
<a href='images/organism/synthetic_construct.jpg' id='card_image'  rel="lightbox" title='Credit: Jan-Christoph Westerman'><img src='images/organism/synthetic_construct.jpg'/></a>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Name</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=4377'>PnIA [sTy15Y]</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Alternative name(s)</b></td>
	<td class='text'>
		PnIA,PnIA[Y(SO<sub>3</sub>)15Y]-NH<sub>2</sub>	</td>
</tr>
<tr>
	<td width='160px'><b>Organism</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Organism_search%5B%5D=synthetic construct'><i>synthetic construct</i></a>	</td>
</tr>
<tr>
	<td width='160px'><b>Protein Type</b></td>
	<td class='text'>
		Synthetic	</td>
</tr>
<tr>
	<td width='160px'><b>Parent</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=51'>PnIA</a>	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Classification</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Conopeptide class</b></td>
	<td class='text'>
		conotoxin	</td>
</tr>
<tr>
	<td width='160px'><b>Gene superfamily</b></td>
	<td class='text'>
			</td>
</tr>
<tr>
	<td width='160px'><b>Cysteine framework</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Framework%5B%5D=I&fields[]=Class&fields[]=Framework&fields[]=Organism'>I</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Pharmacological family</b></td>
	<td class='text'>
		alpha conotoxin	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Sequence</b></legend>
<table class='cardtable'>
<tr><td class='cardsequence' colspan='2'><div class='seq'>G<b>C</b><b>C</b>SLPP<b>C</b>AANNPDY<b>C</b>(nh2)</div></td></tr>
</tr>
<tr>
	<td width='160px'><b>Modified residues</b></td>
	<td class='text'>
		<table class='postable'><tr><th>position</th><th>symbol</th><th>name</th></tr>
<tr><td>17</td><td>nh2</td><td>C-term amidation</td></tr></table>	</td>
</tr>
<tr>
	<td width='160px'><b>Average Mass</b></td>
	<td class='num'>
		1622.81	</td>
</tr>
<tr>
	<td width='160px'><b>Monoisotopic Mass</b></td>
	<td class='num'>
		1621.58	</td>
</tr>
<tr>
	<td width='160px'><b>Isoelectric Point</b></td>
	<td class='num'>
		5.47	</td>
</tr>
<tr>
	<td width='160px'><b>Extinction Coefficient [280nm]</b></td>
	<td class='num'>
		1490.00	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Activity</b></legend>
<h2>IC50: Nicotinic acetylcholine receptors</h2><table class="activitytable"><tr><th>Target</th><th>Organism</th><th colspan="2">IC50</th><th>n<sub>hill</sub></th><th>Agonist</th><th>Ref</th></tr><tr><td>&alpha;3&beta;2</td><td><i>R. norvegicus</i></td><td style="text-align:right">9.56nM</td><td></td><td style="text-align:right">1</td><td>300uM Ach</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10545176&query_hl=1&itool=pubmed_docsum'>Luo,S. <i>et al.</i></a> (1999) </td></tr><tr><td></td><td><i>Unknown</i></td><td style="text-align:right">7.74nM</td><td>[6.79-8.81]</td><td style="text-align:right">0.85</td><td>100uM Ach</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=15929983&query_hl=1&itool=pubmed_docsum'>Dutertre,S. <i>et al.</i></a> (2005) </td></tr><tr><td>&alpha;4&beta;2</td><td><i>Unknown</i></td><td style="text-align:right">&gt;10uM</td><td></td><td style="text-align:right"></td><td>100uM Ach</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=15929983&query_hl=1&itool=pubmed_docsum'>Dutertre,S. <i>et al.</i></a> (2005) </td></tr><tr><td>&alpha;6/&alpha;3&beta;2&beta;3</td><td><i>H. sapiens</i></td><td style="text-align:right">2.8 nM</td><td>[2-3.8]</td><td style="text-align:right"></td><td>ACh(100 uM)	</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=30249616&query_hl=1&itool=pubmed_docsum'>Hone <i>et al.</i></a> (2018) </td></tr><tr><td></td><td><i>R. norvegicus</i></td><td style="text-align:right">3.3 nM</td><td>[3-3.6]</td><td style="text-align:right"></td><td>ACh(100 uM)	</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=30249616&query_hl=1&itool=pubmed_docsum'>Hone <i>et al.</i></a> (2018) </td></tr><tr><td>&alpha;6/&alpha;3&beta;4</td><td><i>H. sapiens</i></td><td style="text-align:right">149 nM</td><td>[135-164]</td><td style="text-align:right"></td><td>ACh(300 uM)	</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=30249616&query_hl=1&itool=pubmed_docsum'>Hone <i>et al.</i></a> (2018) </td></tr><tr><td></td><td><i>R. norvegicus</i></td><td style="text-align:right">8456 nM</td><td>[5419-13290]</td><td style="text-align:right"></td><td>300uM Ach</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=30249616&query_hl=1&itool=pubmed_docsum'>Hone <i>et al.</i></a> (2018) </td></tr><tr><td>&alpha;7</td><td><i>G. gallus</i></td><td style="text-align:right">349nM</td><td></td><td style="text-align:right"></td><td>200uM Ach</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=12746432&query_hl=1&itool=pubmed_docsum'>Hogg,R.C. <i>et al.</i></a> (2003) </td></tr><tr><td></td><td><i>R. norvegicus</i></td><td style="text-align:right">252nM</td><td></td><td style="text-align:right">1.2</td><td>1mM Ach</td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10545176&query_hl=1&itool=pubmed_docsum'>Luo,S. <i>et al.</i></a> (1999) </td></tr></table><h2>Kd: Nicotinic acetylcholine receptors</h2><table class="activitytable"><tr><th>Target</th><th>Organism</th><th colspan="2">Kd</th><th>Ref</th></tr><tr><td>&alpha;7</td><td><i>R. norvegicus</i></td><td style="text-align:right">176nM</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10545176&query_hl=1&itool=pubmed_docsum'>Luo,S. <i>et al.</i></a> (1999) </td></tr></table><h2>Ki: Nicotinic acetylcholine receptors</h2><table class="activitytable"><tr><th>Target</th><th>Organism</th><th colspan="2">Ki</th><th>Competitor</th><th>Agonist</th><th>Ref</th></tr><tr><td>AChBP</td><td><i>A. californica</i></td><td style="text-align:right">1.41 nM</td><td>[1.1-1.7]</td><td>1 nM [<sup>3</sup>H]-epibatidine</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=34671739&query_hl=1&itool=pubmed_docsum'>Ho,T.N.T. <i>et al.</i></a> (2021) </td></tr><tr><td></td><td><i>L. stagnalis</i></td><td style="text-align:right">9.12 uM</td><td>[3.24-26.92]</td><td>1 nM [<sup>3</sup>H]-epibatidine</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=34671739&query_hl=1&itool=pubmed_docsum'>Ho,T.N.T. <i>et al.</i></a> (2021) </td></tr><tr><td></td><td></td><td style="text-align:right">1000 nM</td><td>+/-300</td><td>I-Bgt(10 uM)</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=17660751&query_hl=1&itool=pubmed_docsum'>Dutertre,S. <i>et al.</i></a> (2007) </td></tr></table></fieldset>
<br>



<fieldset>
<legend><b>References</b></legend>
<table class='cardtable'>
<tr>
	<td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10545176&query_hl=1&itool=pubmed_docsum'>Luo,S., Nguyen,T.A., Cartier,G.E., Olivera,B.M., Yoshikami,D. and McIntosh,J.M.</a> (1999) Single-residue alteration in alpha-conotoxin PnIA switches its nAChR subtype selectivity <i>Biochemistry</i> <b>38</b>:14542-14548</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=17660751&query_hl=1&itool=pubmed_docsum'>Dutertre,S., Ulens,C., B&uuml;ttner,R., Fish,A., van Elk,R., Kendel,Y., Hopping,G., Alewood,P.F., Schroeder,C., Nicke,A., Smit,A.B., Sixma,T.K. and Lewis,R.J.</a> (2007) AChBP-targeted alpha-conotoxin correlates distinct binding orientations with nAChR subtype selectivity. <i>EMBO J.</i> <b>26</b>:3858-3867</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=12746432&query_hl=1&itool=pubmed_docsum'>Hogg,R.C., Hopping,G., Alewood,P.F., Adams,D.J. and Bertrand,D.</a> (2003) Alpha-conotoxins PnIA and [A10L]PnIA stabilize different states of the alpha7-L247T nicotinic acetylcholine receptor. <i>J. Biol. Chem.</i> <b>278</b>:26908-26914</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=15929983&query_hl=1&itool=pubmed_docsum'>Dutertre,S., Nicke,A., and Lewis,R.J.</a> (2005) Beta2 subunit contribution to 4/7 alpha-conotoxin binding to the nicotinic acetylcholine receptor. <i>J. Biol. Chem.</i> <b>280</b>:30460-30468</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=30249616&query_hl=1&itool=pubmed_docsum'>Hone, A.J., Talley, T.T., Bobango, J., Melo, C.H., Hararah, F., Gajewiak, J., Christensen, S., Harvey, P.J., Craik, D.J. and McIntosh, J.M.</a> (2018) Molecular determinants of &alpha;-conotoxin potency for inhibition of human and rat &alpha;6&beta;4 nicotinic acetylcholine receptors <i>Journal of Biological Chemistry</i> <b>293</b>:17838-17852</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=34671739&query_hl=1&itool=pubmed_docsum'>Ho,T.N.T., Lee,H.S., Swaminathan,S., Goodwin,L., Rai,N., Ushay,B., Lewis,R.J. and Conibear,A.C.</a> (2021) Posttranslational modifications of &alpha;-conotoxins: sulfotyrosine and C-terminal amidation stabilise structures and increase acetylcholine receptor binding. <i>RSC Med Chem</i> <b>12</b>:1574-1584</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Internal links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Nucleic acids</b></td>
	<td class='text'>
			</td>
</tr>
<tr>
	<td width='160px'><b>Structure</b></td>
	<td class='text'>
			</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>External links</b></legend>
<table class='cardtable'>
</table>
</fieldset>
<br>






<fieldset>
<legend><b>Tools</b></legend>
<form action='index.php?page=digest' method='POST'>
<input type='hidden' name='sequence' value='GCCSLPPCAANNPDYCX'>
<input type='hidden' name='cyclic' value='0'>
<input type='submit' class='button' value='Digest Peptide'>
</form>
</fieldset>

	
</div>

<div id='foot'>
<u>Please cite</u>:<br>
Kaas Q, Yu R, Jin AH, Dutertre S and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>ConoServer: updated content, knowledge, and discovery tools in the conopeptide database.</a> <i>Nucleic Acids Research</i> (2012) <b>40</b>(Database issue):D325-30<br>
Kaas Q, Westermann JC, Halai R, Wang CK and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/18065428'>ConoServer, a database for conopeptide sequences and structures.</a> <i>Bioinformatics</i> (2008) <b>24</b>(3):445-6
<p>
ConoServer is managed at the Institute of Molecular Bioscience <a href='http://imb.uq.edu.au/'>IMB</a>, Brisbane, Australia.
</p>
<p>The database and computational tools found on this website may be used for academic research only, provided that it is referred to ConoServer, the database of conotoxins (http://www.conoserver.org) and the above reference is cited. For any other use please contact David Craik (d.craik@imb.uq.edu.au).
</p>
<p>
<a href='http://www.imb.uq.edu.au/'>
<img src='images/IMB_UQ_small.png'/>
</a>
Contacts:<br/>
<a href="mailto:d.craik@imb.uq.edu.au?subject=ConoServer">David Craik</a><br/>
<a href="mailto:q.kaas@imb.uq.edu.au?subject=ConoServer">Quentin Kaas</a><br/>
</p>
<p>
Last updated: Wednesday 15 January 2025</p>
</div>

</div>
</div>

</div>

</body>
</html>
