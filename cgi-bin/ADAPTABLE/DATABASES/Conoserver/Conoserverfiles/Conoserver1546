<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="description" content="ConoServer is a database of toxins isolated from Cone snails. The database provide annotated information on nucleic acid sequences, protein sequences and 3D structures. It tracks known cysteins frameworks, genetic superfamily and pharmacological families."/>
	<meta http-equiv="keywords" content="ConoServer, cone snail, cone snail toxin, conopeptide, conotoxin, conophan, contryphan, conantokin, conolysin, conopressin, conomarphin, conorfamide, contulakin, David Craik, Quentin Kaas"/>
	<title>ConoServer</title>
	<link rel="StyleSheet" href="index.css?b" type="text/css">

	<link rel="stylesheet" href="lightbox.css" type="text/css" media="screen" />
	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
	<script src="js/lightbox.js" type="text/javascript"></script>
	<script src="js/jquery-3.6.0.min.js" type="text/javascript"></script>
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
<script>
function SelectValue(selectid,value) {
	var i;
	var select = document.getElementById(selectid);
	for(i = 0; i < select.options.length; i++) {
		if (select.options[i].value == value) {
			select.options[i].selected= true;
		} else {
			select.options[i].selected= false;
		}
	}
}
</script>


<div id='globalcontainer'>

<div id='pagehead'>
	<div>
	<a href="index.php">
		<img src="images/conoserver_small.png" alt="ConoServer: A database for conotoxins" class='expl'/>
	</a>
	</div>
	<div><a class='primarycitation' href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>Kaas Q et al. (2012) <i>Nucleic Acids Res.</i></a>
<form>
<div class='firstline'>
Search a
<select name="table">
  <option value='protein'>Protein</option>
  <option value='nucleicacid'>Nucleic acids</option>
  <option value='structure'>3D structure</option>
</select>
name
</div>
<div class='secondline'>
<input type='text'   name='quicktext' size='30' id='textsearch'\>
<input type='hidden' name='textfield' value='All fields'\>
<input type='hidden' name='page' value='list'\>
<input type='checkbox' name='exactsearch' value='1' style='vertical-align:middle'\>
<div class='exactmatch'>exact match</div>
<input type='submit' value='Search' class='button'\>
<input type='reset' value='Clear' class='button'\>
</div>
</form>
</div>
	
	<div id='mainmenu'>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='./'>Home</a>
		</span>
		<span class='menuright'></span>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>About</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=about_conoserver'>About ConoServer</a></li>
			<li><a href='?page=about_conotoxins'>About conopeptides</a></li>
			<li><a href='?page=about_us'>About us</a></li>
			<li><a href='?page=links'>Related websites</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Search</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=search&table=protein'>Search a peptide</a></li>
			<li><a href='?page=search&table=nucleicacid'>Search a nucleic acid</a></li>
			<li><a href='?page=search&table=structure'>Search a 3D structure</a></li>
			<li><a href='?page=list&table=reference&listonly=1'>Search by references</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Tools</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=conoprec'>ConoPrec: precursor analysis</a></li>
			<li><a href='?page=ptmdiffmass'>ConoMass 1: differential PTM masses</a></li>
			<li><a href='?page=identifymasslist'>ConoMass 2: identify peptide mass</a></li>
			<li><a href='?page=comparemasses'>Compare mass lists</a></li>
			<li><a href='?page=uniquemasses'>Remove duplicated masses</a></li>
			<li><a href='?page=download'>Download ConoServer's data</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=stats'>Statistics</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=stats&tab=classification'>On classification schemes</a></li>
			<li><a href='?page=stats&tab=superfamilies'>On superfamily signal sequences</a></li>
			<li><a href='?page=stats&tab=organisms'>On cone snail species</a></li>
			<li><a href='?page=stats&tab=3dstructures'>On 3D structures</a></li>
			<li><a href='?page=stats&tab=3doverlays'>On structural scaffold conservation</a></li>
			<li><a href='?page=stats&tab=references'>On journals in ConoServer</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=classification'>Classification</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=classification&type=genesuperfamilies'>Gene superfamilies</a></li>
			<li><a href='?page=classification&type=cysteineframeworks'>Cysteine frameworks</a></li>
			<li><a href='?page=classification&type=pharmacologicalfamilies'>Pharmacological families</a></li>
			</ul>
		</div>
	</li>
	</div>
</div>


<div id='main'>

<h1><a href='?page=card&table=protein&id=1546'>GVIA</a> (P01546) Protein Card</h1>



<fieldset id='general'>
<legend><b>General Information</b></legend>
<a href='images/organism/Conus_geographus.jpg' id='card_image'  rel="lightbox" title='Credits : Kerry Matz, 
National Institute of General Medical Services'><img src='images/organism/Conus_geographus.jpg'/></a>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Name</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=1546'>GVIA</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Alternative name(s)</b></td>
	<td class='text'>
		G6a,SNX-124,Shaker peptide	</td>
</tr>
<tr>
	<td width='160px'><b>Organism</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Organism_search%5B%5D=Conus geographus'><i>Conus geographus</i></a> (Geography cone)	</td>
</tr>
<tr>
	<td width='160px'><b>Organism region</b></td>
	<td class='text'>
		Indo-Pacific	</td>
</tr>
<tr>
	<td width='160px'><b>Organism diet</b></td>
	<td class='text'>
		piscivorous	</td>
</tr>
<tr>
	<td width='160px'><b>Protein Type</b></td>
	<td class='text'>
		Wild type	</td>
</tr>
<tr>
	<td width='160px'><b>Protein precursor</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=820'>GVIA precursor (820)</a><br><a href='?page=card&table=protein&id=5749'>GVIA precursor (5749)</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Notes</b></td>
	<td class='text'>
		Calcium Channel Blocker; Paralysis in lower vertebrates; shaking symptoms upon ic injection in mice. 
Kim et al. have performed an ala scan of GVIA (see references).
GVIA was also shown by Horne et al. 1991,to bind to the &alpha;1 subunit of Torpedo nAChR.
	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Classification</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Conopeptide class</b></td>
	<td class='text'>
		conotoxin	</td>
</tr>
<tr>
	<td width='160px'><b>Gene superfamily</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Superfam%5B%5D=O1 superfamily&fields[]=ID&fields[]=Name&fields[]=Class&fields[]=Superfam&fields[]=Organism'>O1 superfamily</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Cysteine framework</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Framework%5B%5D=VI/VII&fields[]=Class&fields[]=Framework&fields[]=Organism'>VI/VII</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Pharmacological family</b></td>
	<td class='text'>
		omega conotoxin	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Sequence</b></legend>
<table class='cardtable'>
<tr><td class='cardsequence' colspan='2'><div class='seq'><b>C</b>KSOGSS<b>C</b>SOTSYN<b>C</b><b>C</b>RS<b>C</b>NOYTKR<b>C</b>Y(nh2)</div></td></tr>
</tr>
<tr>
	<td width='160px'><b>Modified residues</b></td>
	<td class='text'>
		<table class='postable'><tr><th>position</th><th>symbol</th><th>name</th></tr>
<tr><td>4</td><td>O</td><td>4-Hydroxyproline</td></tr>
<tr><td>10</td><td>O</td><td>4-Hydroxyproline</td></tr>
<tr><td>21</td><td>O</td><td>4-Hydroxyproline</td></tr>
<tr><td>28</td><td>nh2</td><td>C-term amidation</td></tr></table>	</td>
</tr>
<tr>
	<td width='160px'><b>Sequence evidence</b></td>
	<td class='text'>
		protein level	</td>
</tr>
<tr>
	<td width='160px'><b>Average Mass</b></td>
	<td class='num'>
		3037.34	</td>
</tr>
<tr>
	<td width='160px'><b>Monoisotopic Mass</b></td>
	<td class='num'>
		3035.15	</td>
</tr>
<tr>
	<td width='160px'><b>Isoelectric Point</b></td>
	<td class='num'>
		12.58	</td>
</tr>
<tr>
	<td width='160px'><b>Extinction Coefficient [280nm]</b></td>
	<td class='num'>
		4470.00	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Activity</b></legend>
<h2>IC50: Voltage-Gated Calcium Channels</h2><table class="activitytable"><tr><th>Target</th><th>Organism</th><th></th><th colspan="2">IC50</th><th>Competitor</th><th>Ref</th></tr><tr><td>Ca<sub>v</sub>2.2</td><td><i>H. sapiens</i></td><td>((Ca<sub>v</sub>2.2 channels expressed in the SH-SY5Y cells))</td><td style="text-align:right">11.2 nM</td><td>+/-3.3</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=33670311&query_hl=1&itool=pubmed_docsum'>Hasan,M.M. <i>et al.</i></a> (2021) </td></tr><tr><td></td><td><i>R. norvegicus</i></td><td></td><td style="text-align:right">134pM</td><td></td><td>MVIIA</td><td>Miljanich,G.P. <i>et al.</i> (1993) </td></tr></table><h2>Ki: Voltage-Gated Calcium Channels</h2><table class="activitytable"><tr><th>Target</th><th>Organism</th><th colspan="2">Ki</th><th>Competitor</th><th>Agonist</th><th>Ref</th></tr><tr><td>Ca<sub>v</sub>2.1</td><td><i>R. norvegicus</i></td><td style="text-align:right">1050 (830-1320) nM</td><td></td><td><sup>125</sup>I-MVIIC</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10938268&query_hl=1&itool=pubmed_docsum'>Lewis,R.J. <i>et al.</i></a> (2000) </td></tr><tr><td>Ca<sub>v</sub>2.2</td><td><i>R. norvegicus</i></td><td style="text-align:right">2.02 pM</td><td></td><td><sup>125</sup>I-CNVIIA</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=11724570&query_hl=1&itool=pubmed_docsum'>Favreau,P. <i>et al.</i></a> (2001) </td></tr><tr><td></td><td></td><td style="text-align:right">0.038 (0.030-0.047) nM</td><td></td><td><sup>125</sup>I-GVIA</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10938268&query_hl=1&itool=pubmed_docsum'>Lewis,R.J. <i>et al.</i></a> (2000) </td></tr><tr><td></td><td></td><td style="text-align:right">3.7-4.3 pM</td><td></td><td><sup>125</sup>I-GVIA</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=11724570&query_hl=1&itool=pubmed_docsum'>Favreau,P. <i>et al.</i></a> (2001) </td></tr><tr><td></td><td></td><td style="text-align:right">0.03 nM</td><td></td><td><sup>125</sup>I-GVIA</td><td></td><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=15166237&query_hl=1&itool=pubmed_docsum'>Mould,J. <i>et al.</i></a> (2004) </td></tr></table></fieldset>
<br>

<fieldset>
<legend><b>Synthetic variants</b></legend>
<table><tr><td><a href="?page=card&table=protein&id=1780">GVIA [O10>K]</a></td><td style="font-family:monospace"><b>C</b>KSOGSS<b>C</b>SKTSYN<b>C</b><b>C</b>RS<b>C</b>NOYTKR<b>C</b>Y(nh2)</td></tr><tr><td><a href="?page=card&table=protein&id=5701">GVIA[C8U,C19U]</a></td><td style="font-family:monospace"><b>C</b>KSOGSS<b>U</b>SOTSYN<b>C</b><b>C</b>RS<b>U</b>NOYTKR<b>C</b>Y(nh2)</td></tr></table></pre>
</fieldset>
<br>


<fieldset>
<legend><b>References</b></legend>
<table class='cardtable'>
<tr>
	<td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=6509012&query_hl=1&itool=pubmed_docsum'>Olivera,B.M., McIntosh,J.M., Cruz,L.J., Luque,F.A. and Gray,W.R.</a> (1984) Purification and sequence of a presynaptic peptide toxin from Conus geographus venom <i>Biochemistry</i> <b>23</b>:5087-5090</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=8394704&query_hl=1&itool=pubmed_docsum'>Sato,K., Park,N.G., Kohno,T., Maeda,T., Kim,J.I., Kato,R. and Takahashi,M.</a> (1993) Role of basic residues for the binding of omega-conotoxin GVIA to N-type calcium channels <i>Biochem. Biophys. Res. Commun.</i> <b>194</b>:1292-1296</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=7929033&query_hl=1&itool=pubmed_docsum'>Kim,J.I., Takahashi,M., Ogura,A., Kohno,T., Kudo,Y. and Sato,K.</a> (1994) Hydroxyl group of Tyr13 is essential for the activity of omega-conotoxin GVIA, a peptide toxin for N-type calcium channel <i>J. Biol. Chem.</i> <b>269</b>:23876-23878</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=9115267&query_hl=1&itool=pubmed_docsum'>Lew,M.J., Flinn,J.P., Pallaghy,P.K., Murphy,R., Whorlow,S.L., Wright,C.E., Norton,R.S. and Angus,J.A.</a> (1997) Structure-function relationships of omega-conotoxin GVIA. Synthesis, structure, calcium channel binding, and functional assay of alanine-substituted analogues <i>J. Biol. Chem.</i> <b>272</b>:12014-12023</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=8338837&query_hl=1&itool=pubmed_docsum'>Davis,J.H., Bradley,E.K., Miljanich,G.P., Nadasdi,L., Ramachandran,J. and Basus,V.J.</a> (1993) Solution structure of omega-conotoxin GVIA using 2-D NMR spectroscopy and relaxation matrix analysis <i>Biochemistry</i> <b>32</b>:7396-7405</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10231724&query_hl=1&itool=pubmed_docsum'>Pallaghy,P.K. and Norton,R.S.</a> (1999) Refined solution structure of omega-conotoxin GVIA: implications for calcium channel binding <i>J. Pept. Res.</i> <b>53</b>:343-351</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10938268&query_hl=1&itool=pubmed_docsum'>Lewis,R.J., Nielsen,K.J., Craik,D.J., Loughnan,M.L., Adams,D.A., Sharpe,I.A., Luchian,T., Adams,D.J., Bond,T., Thomas,L., Jones,A., Matheson,J.L., Drinkwater,R., Andrews,P.R. and Alewood,P.F.</a> (2000) Novel omega-conotoxins from Conus catus discriminate among neuronal calcium channel subtypes <i>J. Biol. Chem.</i> <b>275</b>:35335-35344</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=2433275&query_hl=1&itool=pubmed_docsum'>Rivier,J., Galyean,R., Gray,W.R., Azimi-Zonooz,A., McIntosh,J.M., Cruz,L.J. and Olivera,B.M.</a> (1987) Neuronal calcium channel inhibitors. Synthesis of omega-conotoxin GVIA and effects on 45Ca uptake by synaptosomes <i>J. Biol. Chem.</i> <b>262</b>:1194-1198</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=11724570&query_hl=1&itool=pubmed_docsum'>Favreau,P., Gilles,N., Lamthanh,H., Bournaud,R., Shimahara,T., Bouet,F., Laboute,P., Letourneux,Y., Menez,A., Molgo,J. and Le Gall,F.</a> (2001) A new omega-conotoxin that targets N-type voltage-sensitive calcium channels with unusual specificity <i>Biochemistry</i> <b>40</b>:14567-14575</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=15166237&query_hl=1&itool=pubmed_docsum'>Mould,J., Yasuda,T., Schroeder,C.I., Beedle,A.M., Doering,C.J., Zamponi,G.W., Adams,D.J. and Lewis,R.J.</a> (2004) The alpha2delta auxiliary subunit reduces affinity of omega-conotoxins for recombinant N-type (Cav2.2) calcium channels. <i>J. Biol. Chem.</i> <b>279</b>:34705-34714</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=6608056&query_hl=1&itool=pubmed_docsum'>Kerr,L.M. and Yoshikami,D.</a> (1984) A venom peptide with a novel presynaptic blocking action. <i>Nature</i> <b>308</b>:282-284</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=2545135&query_hl=1&itool=pubmed_docsum'>Yoshikami,D. and Olivera,B.M.</a> (1989) The inhibitory effects of omega-conotoxins on Ca channels and synapses. <i>Ann. N. Y. Acad. Sci.</i> <b>560</b>:230-248</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=8461292&query_hl=1&itool=pubmed_docsum'>Lampe,R.A., Lo,M.M., Keith,R.A., Horn,M.B., McLane,M.W. and Spreen,R.C.</a> (1993) Effects of site-specific acetylation on omega-conotoxin GVIA binding and function. <i>Biochemistry</i> <b>32</b>:3255-3260</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=10556572&query_hl=1&itool=pubmed_docsum'>Flinn,J.P., Pallaghy,P.K., Lew,M.J., Murphy,R. and Norton,R.S.</a> (1999) Role of disulfide bridges in the folding, structure and biological activity of omega-conotoxin GVIA. <i>Biochim. Biophys. Acta</i> <b>1434</b>:177-190</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=1649828&query_hl=1&itool=pubmed_docsum'>Horne,W.A., Hawrot,E. and Tsien,R.W.</a> (1991) omega-Conotoxin GVIA receptors of Discopyge electric organ. Characterization of omega-conotoxin binding to the nicotinic acetylcholine receptor. <i>J. Biol. Chem.</i> <b>266</b>:13719-13725</td></tr><tr><td><a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=pubmed&cmd=Retrieve&dopt=AbstractPlus&list_uids=33670311&query_hl=1&itool=pubmed_docsum'>Hasan,M.M., Starobova,H., Mueller,A., Vetter,I. and Lewis,R.J.</a> (2021) Subcutaneous &omega;-Conotoxins Alleviate Mechanical Pain in Rodent Models of Acute Peripheral Neuropathy. <i>Mar Drugs</i> <b>19</b></td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Internal links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Protein Precursor</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=820'>GVIA precursor (820)</a><br><a href='?page=card&table=protein&id=5749'>GVIA precursor (5749)</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Nucleic acids</b></td>
	<td class='text'>
			</td>
</tr>
<tr>
	<td width='160px'><b>Structure</b></td>
	<td class='text'>
		<a href='?page=card&table=structure&id=72'>SOLUTION STRUCTURE OF OMEGA-CONOTOXIN GVIA USING 2-D NMR SPECTROSCOPY AND RELAXATION MATRIX ANALYSIS</a><br><a href='?page=card&table=structure&id=89'>OMEGA-CONOTOXIN GVIA, A N-TYPE CALCIUM CHANNEL BLOCKER</a><br><a href='?page=card&table=structure&id=100'>STRUCTURE OF THE CALCIUM CHANNEL BLOCKER OMEGA CONOTOXIN GVIA, NMR, 20 STRUCTURES</a>	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>External links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Ncbi</b></td>
	<td><a href='http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?db=protein&val=1OMC_A'>1OMC_A</a>, <a href='http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?db=protein&val=1TTL_A'>1TTL_A</a>, <a href='http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?db=protein&val=2CCO_A'>2CCO_A</a></td>
</tr>
</table>
</fieldset>
<br>






<fieldset>
<legend><b>Tools</b></legend>
<form action='index.php?page=digest' method='POST'>
<input type='hidden' name='sequence' value='CKSPGSSCSPTSYNCCRSCNPYTKRCYX'>
<input type='hidden' name='cyclic' value='0'>
<input type='submit' class='button' value='Digest Peptide'>
</form>
</fieldset>

	
</div>

<div id='foot'>
<u>Please cite</u>:<br>
Kaas Q, Yu R, Jin AH, Dutertre S and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>ConoServer: updated content, knowledge, and discovery tools in the conopeptide database.</a> <i>Nucleic Acids Research</i> (2012) <b>40</b>(Database issue):D325-30<br>
Kaas Q, Westermann JC, Halai R, Wang CK and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/18065428'>ConoServer, a database for conopeptide sequences and structures.</a> <i>Bioinformatics</i> (2008) <b>24</b>(3):445-6
<p>
ConoServer is managed at the Institute of Molecular Bioscience <a href='http://imb.uq.edu.au/'>IMB</a>, Brisbane, Australia.
</p>
<p>The database and computational tools found on this website may be used for academic research only, provided that it is referred to ConoServer, the database of conotoxins (http://www.conoserver.org) and the above reference is cited. For any other use please contact David Craik (d.craik@imb.uq.edu.au).
</p>
<p>
<a href='http://www.imb.uq.edu.au/'>
<img src='images/IMB_UQ_small.png'/>
</a>
Contacts:<br/>
<a href="mailto:d.craik@imb.uq.edu.au?subject=ConoServer">David Craik</a><br/>
<a href="mailto:q.kaas@imb.uq.edu.au?subject=ConoServer">Quentin Kaas</a><br/>
</p>
<p>
Last updated: Wednesday 15 January 2025</p>
</div>

</div>
</div>

</div>

</body>
</html>
