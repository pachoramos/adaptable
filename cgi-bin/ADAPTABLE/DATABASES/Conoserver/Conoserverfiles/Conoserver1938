<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="description" content="ConoServer is a database of toxins isolated from Cone snails. The database provide annotated information on nucleic acid sequences, protein sequences and 3D structures. It tracks known cysteins frameworks, genetic superfamily and pharmacological families."/>
	<meta http-equiv="keywords" content="ConoServer, cone snail, cone snail toxin, conopeptide, conotoxin, conophan, contryphan, conantokin, conolysin, conopressin, conomarphin, conorfamide, contulakin, David Craik, Quentin Kaas"/>
	<title>ConoServer</title>
	<link rel="StyleSheet" href="index.css?b" type="text/css">

	<link rel="stylesheet" href="lightbox.css" type="text/css" media="screen" />
	<script src="js/prototype.js" type="text/javascript"></script>
	<script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
	<script src="js/lightbox.js" type="text/javascript"></script>
	<script src="js/jquery-3.6.0.min.js" type="text/javascript"></script>
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
<script>
function SelectValue(selectid,value) {
	var i;
	var select = document.getElementById(selectid);
	for(i = 0; i < select.options.length; i++) {
		if (select.options[i].value == value) {
			select.options[i].selected= true;
		} else {
			select.options[i].selected= false;
		}
	}
}
</script>


<div id='globalcontainer'>

<div id='pagehead'>
	<div>
	<a href="index.php">
		<img src="images/conoserver_small.png" alt="ConoServer: A database for conotoxins" class='expl'/>
	</a>
	</div>
	<div><a class='primarycitation' href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>Kaas Q et al. (2012) <i>Nucleic Acids Res.</i></a>
<form>
<div class='firstline'>
Search a
<select name="table">
  <option value='protein'>Protein</option>
  <option value='nucleicacid'>Nucleic acids</option>
  <option value='structure'>3D structure</option>
</select>
name
</div>
<div class='secondline'>
<input type='text'   name='quicktext' size='30' id='textsearch'\>
<input type='hidden' name='textfield' value='All fields'\>
<input type='hidden' name='page' value='list'\>
<input type='checkbox' name='exactsearch' value='1' style='vertical-align:middle'\>
<div class='exactmatch'>exact match</div>
<input type='submit' value='Search' class='button'\>
<input type='reset' value='Clear' class='button'\>
</div>
</form>
</div>
	
	<div id='mainmenu'>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='./'>Home</a>
		</span>
		<span class='menuright'></span>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>About</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=about_conoserver'>About ConoServer</a></li>
			<li><a href='?page=about_conotoxins'>About conopeptides</a></li>
			<li><a href='?page=about_us'>About us</a></li>
			<li><a href='?page=links'>Related websites</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Search</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=search&table=protein'>Search a peptide</a></li>
			<li><a href='?page=search&table=nucleicacid'>Search a nucleic acid</a></li>
			<li><a href='?page=search&table=structure'>Search a 3D structure</a></li>
			<li><a href='?page=list&table=reference&listonly=1'>Search by references</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a>Tools</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=conoprec'>ConoPrec: precursor analysis</a></li>
			<li><a href='?page=ptmdiffmass'>ConoMass 1: differential PTM masses</a></li>
			<li><a href='?page=identifymasslist'>ConoMass 2: identify peptide mass</a></li>
			<li><a href='?page=comparemasses'>Compare mass lists</a></li>
			<li><a href='?page=uniquemasses'>Remove duplicated masses</a></li>
			<li><a href='?page=download'>Download ConoServer's data</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=stats'>Statistics</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=stats&tab=classification'>On classification schemes</a></li>
			<li><a href='?page=stats&tab=superfamilies'>On superfamily signal sequences</a></li>
			<li><a href='?page=stats&tab=organisms'>On cone snail species</a></li>
			<li><a href='?page=stats&tab=3dstructures'>On 3D structures</a></li>
			<li><a href='?page=stats&tab=3doverlays'>On structural scaffold conservation</a></li>
			<li><a href='?page=stats&tab=references'>On journals in ConoServer</a></li>
			</ul>
		</div>
	</li>
	<li>
		<span class='menuleft'></span>
		<span class='menumid'>
		<a href='?page=classification'>Classification</a>
		</span>
		<span class='menuright'></span>
		<div class='sub'>
			<ul>
			<li><a href='?page=classification&type=genesuperfamilies'>Gene superfamilies</a></li>
			<li><a href='?page=classification&type=cysteineframeworks'>Cysteine frameworks</a></li>
			<li><a href='?page=classification&type=pharmacologicalfamilies'>Pharmacological families</a></li>
			</ul>
		</div>
	</li>
	</div>
</div>


<div id='main'>

<h1><a href='?page=card&table=protein&id=1938'>Sequence 327 from patent US 6762165</a> (P01938) Protein Card</h1>



<fieldset id='general'>
<legend><b>General Information</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Name</b></td>
	<td class='text'>
		<a href='?page=card&table=protein&id=1938'>Sequence 327 from patent US 6762165</a>	</td>
</tr>
<tr>
	<td width='160px'><b>Organism</b></td>
	<td class='text'>
		<a href='?page=list&table=protein&Organism_search%5B%5D=Unknown'><i>Unknown</i></a>	</td>
</tr>
<tr>
	<td width='160px'><b>Protein Type</b></td>
	<td class='text'>
		Patent	</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Classification</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Conopeptide class</b></td>
	<td class='text'>
		conotoxin	</td>
</tr>
<tr>
	<td width='160px'><b>Gene superfamily</b></td>
	<td class='text'>
			</td>
</tr>
<tr>
	<td width='160px'><b>Pharmacological family</b></td>
	<td class='text'>
			</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Sequence</b></legend>
<table class='cardtable'>
<tr><td class='cardsequence' colspan='2'><div class='seq'>MKLTCVVIVAVLFLTAWTFVTADDSINGLENLFPKARHEMR<br>KPEASRSRGRCRPRGMFCGFPKPGPYCCNGWCFFVCI</div></td></tr>
</tr>
<tr>
	<td width='160px'><b>Average Mass</b></td>
	<td class='num'>
		8848.52	</td>
</tr>
<tr>
	<td width='160px'><b>Monoisotopic Mass</b></td>
	<td class='num'>
		8842.35	</td>
</tr>
<tr>
	<td width='160px'><b>Isoelectric Point</b></td>
	<td class='num'>
		NA	</td>
</tr>
<tr>
	<td width='160px'><b>Extinction Coefficient [280nm]</b></td>
	<td class='num'>
		13365.00	</td>
</tr>
</table>
</fieldset>
<br>




<fieldset>
<legend><b>References</b></legend>
<table class='cardtable'>
<tr>
	<td>Olivera,B.M., Cartier,G.E., Watkins,M., Hillyard,D.R., McIntosh,J.M., Layer,R.T. and Jones,R.M. (2004) O-superfamily conotoxin peptides <i>Patent: US 6762165-A 282 13-JUL-2004; University of Utah Research Foundation and Cognetix, Inc.; Salt Lake City, UT</i> </td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>Internal links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Nucleic acids</b></td>
	<td class='text'>
			</td>
</tr>
<tr>
	<td width='160px'><b>Structure</b></td>
	<td class='text'>
			</td>
</tr>
</table>
</fieldset>
<br>

<fieldset>
<legend><b>External links</b></legend>
<table class='cardtable'>
<tr>
	<td width='160px'><b>Patent Lens</b></td>
	<td><a href='http://www.lens.org/lens/search?q=US6762165'>US6762165</a></td>
</tr>
<tr>
	<td width='160px'><b>Ncbi</b></td>
	<td><a href='http://www.ncbi.nlm.nih.gov/entrez/viewer.fcgi?db=protein&val=AAV99667'>AAV99667</a></td>
</tr>
</table>
</fieldset>
<br>






<fieldset>
<legend><b>Tools</b></legend>
<form action='index.php?page=digest' method='POST'>
<input type='hidden' name='sequence' value='MKLTCVVIVAVLFLTAWTFVTADDSINGLENLFPKARHEMRKPEASRSRGRCRPRGMFCGFPKPGPYCCNGWCFFVCI'>
<input type='hidden' name='cyclic' value='0'>
<input type='submit' class='button' value='Digest Peptide'>
</form>
</fieldset>

	
</div>

<div id='foot'>
<u>Please cite</u>:<br>
Kaas Q, Yu R, Jin AH, Dutertre S and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/22058133'>ConoServer: updated content, knowledge, and discovery tools in the conopeptide database.</a> <i>Nucleic Acids Research</i> (2012) <b>40</b>(Database issue):D325-30<br>
Kaas Q, Westermann JC, Halai R, Wang CK and Craik DJ.
<a href='http://www.ncbi.nlm.nih.gov/pubmed/18065428'>ConoServer, a database for conopeptide sequences and structures.</a> <i>Bioinformatics</i> (2008) <b>24</b>(3):445-6
<p>
ConoServer is managed at the Institute of Molecular Bioscience <a href='http://imb.uq.edu.au/'>IMB</a>, Brisbane, Australia.
</p>
<p>The database and computational tools found on this website may be used for academic research only, provided that it is referred to ConoServer, the database of conotoxins (http://www.conoserver.org) and the above reference is cited. For any other use please contact David Craik (d.craik@imb.uq.edu.au).
</p>
<p>
<a href='http://www.imb.uq.edu.au/'>
<img src='images/IMB_UQ_small.png'/>
</a>
Contacts:<br/>
<a href="mailto:d.craik@imb.uq.edu.au?subject=ConoServer">David Craik</a><br/>
<a href="mailto:q.kaas@imb.uq.edu.au?subject=ConoServer">Quentin Kaas</a><br/>
</p>
<p>
Last updated: Wednesday 15 January 2025</p>
</div>

</div>
</div>

</div>

</body>
</html>
