<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-12-15" modified="2024-11-27" version="88" xmlns="http://uniprot.org/uniprot">
  <accession>O77537</accession>
  <accession>O46571</accession>
  <name>B2MG_AOTAZ</name>
  <protein>
    <recommendedName>
      <fullName>Beta-2-microglobulin</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">B2M</name>
  </gene>
  <organism>
    <name type="scientific">Aotus azarae</name>
    <name type="common">Azara's night monkey</name>
    <name type="synonym">Simia azarae</name>
    <dbReference type="NCBI Taxonomy" id="30591"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Platyrrhini</taxon>
      <taxon>Aotidae</taxon>
      <taxon>Aotus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Immunogenetics" volume="48" first="133" last="140">
      <title>Beta-2-microglobulin in neotropical primates (Platyrrhini).</title>
      <authorList>
        <person name="Canavez F.C."/>
        <person name="Ladasky J.J."/>
        <person name="Muniz J.A.P.C."/>
        <person name="Seuanez H.N."/>
        <person name="Parham P."/>
      </authorList>
      <dbReference type="PubMed" id="9634477"/>
      <dbReference type="DOI" id="10.1007/s002510050413"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>Isolate AoAz1</strain>
      <strain>Isolate AoAz2</strain>
      <tissue>Blood</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="submission" date="1998-01" db="EMBL/GenBank/DDBJ databases">
      <title>Polymorphism at the third residue of owl monkey beta-2 microglobulin affects binding by the W6/32 antibody.</title>
      <authorList>
        <person name="Ladasky J.J."/>
        <person name="Shum B.P."/>
        <person name="Parham P."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Component of the class I major histocompatibility complex (MHC). Involved in the presentation of peptide antigens to the immune system (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Heterodimer of an alpha chain and a beta chain. Beta-2-microglobulin is the beta-chain of major histocompatibility complex class I molecules (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-2-microglobulin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF032093">
    <property type="protein sequence ID" value="AAC52107.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF032092">
    <property type="protein sequence ID" value="AAC52107.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF042146">
    <property type="protein sequence ID" value="AAB97465.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O77537"/>
  <dbReference type="SMR" id="O77537"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042612">
    <property type="term" value="C:MHC class I protein complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002474">
    <property type="term" value="P:antigen processing and presentation of peptide antigen via MHC class I"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006955">
    <property type="term" value="P:immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd05770">
    <property type="entry name" value="IgC1_beta2m"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.40.10:FF:001005">
    <property type="entry name" value="Beta-2-microglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015707">
    <property type="entry name" value="B2Microglobulin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003006">
    <property type="entry name" value="Ig/MHC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003597">
    <property type="entry name" value="Ig_C1-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050160">
    <property type="entry name" value="MHC/Immunoglobulin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR19944:SF62">
    <property type="entry name" value="BETA-2-MICROGLOBULIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR19944">
    <property type="entry name" value="MHC CLASS II-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07654">
    <property type="entry name" value="C1-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00407">
    <property type="entry name" value="IGc1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00290">
    <property type="entry name" value="IG_MHC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0393">Immunoglobulin domain</keyword>
  <keyword id="KW-0490">MHC I</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000018752" description="Beta-2-microglobulin">
    <location>
      <begin position="21"/>
      <end position="119"/>
    </location>
  </feature>
  <feature type="domain" description="Ig-like C1-type">
    <location>
      <begin position="25"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="45"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In strain: Isolate AoAz2.">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="24"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00114"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="119" mass="13712" checksum="093EA17AD52C5ABD" modified="1999-07-15" version="2" precursor="true">MARFVVVALLVLLSLSGLEAIQRAPKIQVYSRHPAENGKPNFLNCYVSGFHPSDIEVDLLKNGKKIEKVEHSDLSFSKDWSFYLLYYTEFTPNEKDEYACRVSHVTLSTPKTVKWDRNM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>