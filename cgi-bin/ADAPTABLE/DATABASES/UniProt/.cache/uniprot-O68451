<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-06-20" modified="2024-05-29" version="111" xmlns="http://uniprot.org/uniprot">
  <accession>O68451</accession>
  <accession>A8GSD2</accession>
  <name>DBHL_RICRS</name>
  <protein>
    <recommendedName>
      <fullName>DNA-binding protein HU-like</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">A1G_03965</name>
  </gene>
  <organism>
    <name type="scientific">Rickettsia rickettsii (strain Sheila Smith)</name>
    <dbReference type="NCBI Taxonomy" id="392021"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Alphaproteobacteria</taxon>
      <taxon>Rickettsiales</taxon>
      <taxon>Rickettsiaceae</taxon>
      <taxon>Rickettsieae</taxon>
      <taxon>Rickettsia</taxon>
      <taxon>spotted fever group</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1998-01" db="EMBL/GenBank/DDBJ databases">
      <title>Rickettsia rickettsii fragment which imparts hemolysis upon E. coli.</title>
      <authorList>
        <person name="Temenak J.J."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="2007-09" db="EMBL/GenBank/DDBJ databases">
      <title>Complete genome sequence of Rickettsia rickettsii.</title>
      <authorList>
        <person name="Madan A."/>
        <person name="Fahey J."/>
        <person name="Helton E."/>
        <person name="Ketteman M."/>
        <person name="Madan A."/>
        <person name="Rodrigues S."/>
        <person name="Sanchez A."/>
        <person name="Dasch G."/>
        <person name="Eremeeva M."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Sheila Smith</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Histone-like DNA-binding protein which is capable of wrapping DNA to stabilize it, and thus to prevent its denaturation under extreme environmental conditions.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the bacterial histone-like protein family.</text>
  </comment>
  <comment type="sequence caution" evidence="2">
    <conflict type="frameshift">
      <sequence resource="EMBL-CDS" id="AAC05204" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="AF042063">
    <property type="protein sequence ID" value="AAC05204.1"/>
    <property type="status" value="ALT_FRAME"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP000848">
    <property type="protein sequence ID" value="ABV76307.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_012150884.1">
    <property type="nucleotide sequence ID" value="NZ_CP121767.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O68451"/>
  <dbReference type="SMR" id="O68451"/>
  <dbReference type="GeneID" id="79937429"/>
  <dbReference type="KEGG" id="rri:A1G_03965"/>
  <dbReference type="HOGENOM" id="CLU_2467017_0_0_5"/>
  <dbReference type="Proteomes" id="UP000006832">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030527">
    <property type="term" value="F:structural constituent of chromatin"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030261">
    <property type="term" value="P:chromosome condensation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.520.10">
    <property type="entry name" value="IHF-like DNA-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000119">
    <property type="entry name" value="Hist_DNA-bd"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010992">
    <property type="entry name" value="IHF-like_DNA-bd_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33175">
    <property type="entry name" value="DNA-BINDING PROTEIN HU"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33175:SF3">
    <property type="entry name" value="DNA-BINDING PROTEIN HU-BETA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00216">
    <property type="entry name" value="Bac_DNA_binding"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00411">
    <property type="entry name" value="BHL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47729">
    <property type="entry name" value="IHF-like DNA-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0226">DNA condensation</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <feature type="chain" id="PRO_0000104967" description="DNA-binding protein HU-like">
    <location>
      <begin position="1"/>
      <end position="80"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="80" mass="9855" checksum="339ABC0419C3DC36" modified="2008-01-15" version="2">MITKNYLIDKIHDKLNYLSKEDVKDSVDLILDYLNESLKQQKRIEIRNFGNFSIRKRKFPESEKFYNTVYYRMPKNLFKE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>