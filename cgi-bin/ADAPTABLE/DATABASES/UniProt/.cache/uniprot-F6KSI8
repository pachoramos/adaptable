<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-05-01" modified="2022-05-25" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>F6KSI8</accession>
  <accession>A0A0S2XGP2</accession>
  <name>PANSN_PANAR</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Panusin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Defensin-like peptide 7</fullName>
      <shortName evidence="4">PaD7</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Panulirus argus</name>
    <name type="common">Caribbean spiny lobster</name>
    <name type="synonym">Palinurus argus</name>
    <dbReference type="NCBI Taxonomy" id="6737"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Eucarida</taxon>
      <taxon>Decapoda</taxon>
      <taxon>Pleocyemata</taxon>
      <taxon>Achelata</taxon>
      <taxon>Palinuroidea</taxon>
      <taxon>Palinuridae</taxon>
      <taxon>Panulirus</taxon>
    </lineage>
  </organism>
  <reference evidence="8" key="1">
    <citation type="journal article" date="2012" name="Fish Shellfish Immunol." volume="33" first="872" last="879">
      <title>Defensin like peptide from Panulirus argus relates structurally with beta defensin from vertebrates.</title>
      <authorList>
        <person name="Montero-Alejo V."/>
        <person name="Acosta-Alba J."/>
        <person name="Perdomo-Morales R."/>
        <person name="Perera E."/>
        <person name="Hernandez-Rodriguez E.W."/>
        <person name="Estrada M.P."/>
        <person name="Porto-Verdecia M."/>
      </authorList>
      <dbReference type="PubMed" id="22885029"/>
      <dbReference type="DOI" id="10.1016/j.fsi.2012.07.013"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue evidence="2">Hemolymph</tissue>
    </source>
  </reference>
  <reference evidence="6" key="2">
    <citation type="journal article" date="2017" name="Dev. Comp. Immunol." volume="67" first="310" last="321">
      <title>Panusin represents a new family of beta-defensin-like peptides in invertebrates.</title>
      <authorList>
        <person name="Montero-Alejo V."/>
        <person name="Corzo G."/>
        <person name="Porro-Suardiaz J."/>
        <person name="Pardo-Ruiz Z."/>
        <person name="Perera E."/>
        <person name="Rodriguez-Viera L."/>
        <person name="Sanchez-Diaz G."/>
        <person name="Hernandez-Rodriguez E.W."/>
        <person name="Alvarez C."/>
        <person name="Peigneur S."/>
        <person name="Tytgat J."/>
        <person name="Perdomo-Morales R."/>
      </authorList>
      <dbReference type="PubMed" id="27616720"/>
      <dbReference type="DOI" id="10.1016/j.dci.2016.09.002"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 27-48</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>PRESENCE OF DISULFIDE BONDS</scope>
    <scope>AMIDATION AT TYR-65</scope>
    <source>
      <tissue evidence="3">Hemocyte</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Antimicrobial peptide. Has antibacterial activity against Gram-positive bacteria S.aureus ATCC 29737 and B.subtilis ATCC 6633 as well as against Gram-negative bacteria E.coli ATCC 10536 and K.pneumoniae ATCC 10031.</text>
  </comment>
  <comment type="subunit">
    <text evidence="7">Forms dimers and higher-order oligomers.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">Contains 3 disulfide bonds.</text>
  </comment>
  <comment type="mass spectrometry" mass="4260.3" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="HQ599307">
    <property type="protein sequence ID" value="AEE69605.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KT253226">
    <property type="protein sequence ID" value="ALQ10737.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="F6KSI8"/>
  <dbReference type="SMR" id="F6KSI8"/>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000422178" evidence="7">
    <location>
      <begin position="23"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000445467" description="Panusin" evidence="3">
    <location>
      <begin position="27"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="3">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="32"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="39"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="44"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AEE69605." evidence="6" ref="1">
    <original>VL</original>
    <variation>IV</variation>
    <location>
      <begin position="6"/>
      <end position="7"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AEE69605." evidence="6" ref="1">
    <original>RSYG</original>
    <variation>HAYV</variation>
    <location>
      <begin position="63"/>
      <end position="66"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="22885029"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="27616720"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="22885029"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="27616720"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="27616720"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="AEE69605.1"/>
    </source>
  </evidence>
  <sequence length="66" mass="7102" checksum="919077AD8441E1DF" modified="2018-10-10" version="2" precursor="true">MKTKAVLMLMLLVLVAATLVQGEPEPSYVGDCGSNGGSCVSSYCPYGNRLNYFCPLGRTCCRRSYG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>