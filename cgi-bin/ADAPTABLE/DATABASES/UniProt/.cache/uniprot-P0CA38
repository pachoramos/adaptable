<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-05-05" modified="2024-07-24" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>P0CA38</accession>
  <name>VF129_ASFP4</name>
  <protein>
    <recommendedName>
      <fullName>Uncharacterized protein C129R</fullName>
      <shortName>pC129R</shortName>
      <ecNumber evidence="1">3.1.4.-</ecNumber>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">Pret-074</name>
  </gene>
  <organism>
    <name type="scientific">African swine fever virus (isolate Tick/South Africa/Pretoriuskop Pr4/1996)</name>
    <name type="common">ASFV</name>
    <dbReference type="NCBI Taxonomy" id="561443"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Varidnaviria</taxon>
      <taxon>Bamfordvirae</taxon>
      <taxon>Nucleocytoviricota</taxon>
      <taxon>Pokkesviricetes</taxon>
      <taxon>Asfuvirales</taxon>
      <taxon>Asfarviridae</taxon>
      <taxon>Asfivirus</taxon>
      <taxon>African swine fever virus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Ornithodoros</name>
    <name type="common">relapsing fever ticks</name>
    <dbReference type="NCBI Taxonomy" id="6937"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Phacochoerus aethiopicus</name>
    <name type="common">Warthog</name>
    <dbReference type="NCBI Taxonomy" id="85517"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Phacochoerus africanus</name>
    <name type="common">Warthog</name>
    <dbReference type="NCBI Taxonomy" id="41426"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Potamochoerus larvatus</name>
    <name type="common">Bushpig</name>
    <dbReference type="NCBI Taxonomy" id="273792"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Sus scrofa</name>
    <name type="common">Pig</name>
    <dbReference type="NCBI Taxonomy" id="9823"/>
  </organismHost>
  <reference key="1">
    <citation type="submission" date="2003-03" db="EMBL/GenBank/DDBJ databases">
      <title>African swine fever virus genomes.</title>
      <authorList>
        <person name="Kutish G.F."/>
        <person name="Rock D.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays a role in the inhibition of type I interferon signaling pathway. Mechanistically, specifically interacts with 2',3'-cGAMP and cleaves it via its phosphodiesterase activity. In turn, prevents 2',3'-cGAMP interaction with host ER-resident STING1 leading to inhibition of downstream signaling pathway and type I interferon production.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Virion</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the asfivirus C129R family.</text>
  </comment>
  <dbReference type="EC" id="3.1.4.-" evidence="1"/>
  <dbReference type="EMBL" id="AY261363">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000000859">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044423">
    <property type="term" value="C:virion component"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016787">
    <property type="term" value="F:hydrolase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039502">
    <property type="term" value="P:symbiont-mediated suppression of host type I interferon-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1114">Inhibition of host interferon signaling pathway by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <keyword id="KW-0946">Virion</keyword>
  <feature type="chain" id="PRO_0000373508" description="Uncharacterized protein C129R">
    <location>
      <begin position="1"/>
      <end position="129"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q65154"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="129" mass="14992" checksum="A58EB11C6F5A45B3" modified="2009-05-05" version="1">MEHPSTNYTLEQQHEKLKNYVLIPKHLWSYIKYGTHVRYYTKQNVFRVGGFVLQNPYEAVVKNEVKTAIRLQNSFNTKAKGHVTWAVSYDDISKLYAKPDAIMLTIQENVEKALHALNQNVLTLASKIR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>