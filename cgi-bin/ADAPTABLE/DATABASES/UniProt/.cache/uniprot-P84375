<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-15" modified="2022-12-14" version="29" xmlns="http://uniprot.org/uniprot">
  <accession>P84375</accession>
  <accession>P84372</accession>
  <name>PVK2_EURFL</name>
  <protein>
    <recommendedName>
      <fullName>Periviscerokinin-2.1</fullName>
      <shortName>PVK-2.1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Periviscerokinin-3</fullName>
      <shortName>EurFl-PVK-3</shortName>
    </alternativeName>
    <component>
      <recommendedName>
        <fullName>Periviscerokinin-2.2</fullName>
        <shortName>PVK-2.2</shortName>
      </recommendedName>
      <alternativeName>
        <fullName>Periviscerokinin-2</fullName>
        <shortName>EurFl-PVK-2</shortName>
      </alternativeName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Eurycotis floridana</name>
    <name type="common">Florida woods cockroach</name>
    <name type="synonym">Skunk roach</name>
    <dbReference type="NCBI Taxonomy" id="303877"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Dictyoptera</taxon>
      <taxon>Blattodea</taxon>
      <taxon>Blattoidea</taxon>
      <taxon>Blattidae</taxon>
      <taxon>Eurycotiinae</taxon>
      <taxon>Eurycotis</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2005" name="Peptides" volume="26" first="3" last="9">
      <title>Peptidomics of neurohemal organs from species of the cockroach family Blattidae: how do neuropeptides of closely related species differ?</title>
      <authorList>
        <person name="Predel R."/>
        <person name="Gaede G."/>
      </authorList>
      <dbReference type="PubMed" id="15626499"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2004.10.010"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT VAL-12</scope>
    <source>
      <tissue evidence="2">Abdominal perisympathetic organs</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2009" name="BMC Evol. Biol." volume="9" first="50" last="50">
      <title>A proteomic approach for studying insect phylogeny: CAPA peptides of ancient insect taxa (Dictyoptera, Blattoptera) as a test case.</title>
      <authorList>
        <person name="Roth S."/>
        <person name="Fromm B."/>
        <person name="Gaede G."/>
        <person name="Predel R."/>
      </authorList>
      <dbReference type="PubMed" id="19257902"/>
      <dbReference type="DOI" id="10.1186/1471-2148-9-50"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT VAL-12</scope>
    <source>
      <tissue>Abdominal perisympathetic organs</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="4">Mediates visceral muscle contractile activity (myotropic activity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Abdominal perisympathetic organs.</text>
  </comment>
  <comment type="mass spectrometry" mass="1157.7" method="MALDI" evidence="2">
    <molecule>Periviscerokinin-2.1</molecule>
  </comment>
  <comment type="mass spectrometry" mass="1070.6" method="MALDI" evidence="2">
    <molecule>Periviscerokinin-2.2</molecule>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the periviscerokinin family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013231">
    <property type="entry name" value="Periviscerokinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08259">
    <property type="entry name" value="Periviscerokin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000023625" description="Periviscerokinin-2.1">
    <location>
      <begin position="1"/>
      <end position="12"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000023626" description="Periviscerokinin-2.2">
    <location>
      <begin position="2"/>
      <end position="12"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="2 3">
    <location>
      <position position="12"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15626499"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="19257902"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="12" mass="1128" checksum="2F4D9FBE4EB05728" modified="2005-02-15" version="1">GGSSGLISVPRV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>