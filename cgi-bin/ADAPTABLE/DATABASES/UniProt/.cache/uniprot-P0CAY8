<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-07-28" modified="2023-11-08" version="45" xmlns="http://uniprot.org/uniprot">
  <accession>P0CAY8</accession>
  <name>DF292_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 292</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">At2g04033</name>
    <name type="ORF">F3C11</name>
    <name type="ORF">F3L12</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Nature" volume="402" first="761" last="768">
      <title>Sequence and analysis of chromosome 2 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Lin X."/>
        <person name="Kaul S."/>
        <person name="Rounsley S.D."/>
        <person name="Shea T.P."/>
        <person name="Benito M.-I."/>
        <person name="Town C.D."/>
        <person name="Fujii C.Y."/>
        <person name="Mason T.M."/>
        <person name="Bowman C.L."/>
        <person name="Barnstead M.E."/>
        <person name="Feldblyum T.V."/>
        <person name="Buell C.R."/>
        <person name="Ketchum K.A."/>
        <person name="Lee J.J."/>
        <person name="Ronning C.M."/>
        <person name="Koo H.L."/>
        <person name="Moffat K.S."/>
        <person name="Cronin L.A."/>
        <person name="Shen M."/>
        <person name="Pai G."/>
        <person name="Van Aken S."/>
        <person name="Umayam L."/>
        <person name="Tallon L.J."/>
        <person name="Gill J.E."/>
        <person name="Adams M.D."/>
        <person name="Carrera A.J."/>
        <person name="Creasy T.H."/>
        <person name="Goodman H.M."/>
        <person name="Somerville C.R."/>
        <person name="Copenhaver G.P."/>
        <person name="Preuss D."/>
        <person name="Nierman W.C."/>
        <person name="White O."/>
        <person name="Eisen J.A."/>
        <person name="Salzberg S.L."/>
        <person name="Fraser C.M."/>
        <person name="Venter J.C."/>
      </authorList>
      <dbReference type="PubMed" id="10617197"/>
      <dbReference type="DOI" id="10.1038/45471"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2007" name="Plant J." volume="51" first="262" last="280">
      <title>Small cysteine-rich peptides resembling antimicrobial peptides have been under-predicted in plants.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Moskal W.A. Jr."/>
        <person name="Wu H.C."/>
        <person name="Underwood B.A."/>
        <person name="Graham M.A."/>
        <person name="Town C.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="17565583"/>
      <dbReference type="DOI" id="10.1111/j.1365-313x.2007.03136.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="similarity">
    <text evidence="2">Belongs to the DEFL family.</text>
  </comment>
  <comment type="caution">
    <text evidence="2">Could be the product of a pseudogene. Lacks the signal peptide and 1 of the 4 disulfide bonds, which are conserved features of the family.</text>
  </comment>
  <dbReference type="EMBL" id="AC007167">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC007178">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002685">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF182803">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0CAY8"/>
  <dbReference type="Araport" id="AT2G04033"/>
  <dbReference type="TAIR" id="AT2G04033"/>
  <dbReference type="InParanoid" id="P0CAY8"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P0CAY8">
    <property type="expression patterns" value="baseline"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="uncertain"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000379752" description="Defensin-like protein 292">
    <location>
      <begin position="1"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="50"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="56"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; EF182803." evidence="2" ref="3">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="58"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="71" mass="7752" checksum="B8DF12EA85075823" modified="2009-07-28" version="1">MLLETDASRNKPSSYIPLCGSDNSCGGLWCPRKGGKYSCISMTCDIQEDCPKLVRCKDSPGPYCMEGFCTC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>