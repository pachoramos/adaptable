<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-12-20" modified="2024-11-27" version="59" xmlns="http://uniprot.org/uniprot">
  <accession>P0AGD0</accession>
  <accession>Q57225</accession>
  <name>QACE_KLEAE</name>
  <protein>
    <recommendedName>
      <fullName>Quaternary ammonium compound-resistance protein QacE</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Quaternary ammonium determinant E</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">qacE</name>
  </gene>
  <organism>
    <name type="scientific">Klebsiella aerogenes</name>
    <name type="common">Enterobacter aerogenes</name>
    <dbReference type="NCBI Taxonomy" id="548"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Klebsiella/Raoultella group</taxon>
      <taxon>Klebsiella</taxon>
    </lineage>
  </organism>
  <geneLocation type="plasmid">
    <name>IncP-beta R751</name>
  </geneLocation>
  <reference key="1">
    <citation type="submission" date="1993-03" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Radstroem P."/>
        <person name="Sundstroem L."/>
        <person name="Swedberg G."/>
        <person name="Flensburg J."/>
        <person name="Skoeld O."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="1996-08" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Thomas C.M."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Multidrug exporter. Is implicated for the resistance to bacteriocidal quaternary ammonium compounds (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Cell membrane</location>
      <topology evidence="3">Multi-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the drug/metabolite transporter (DMT) superfamily. Small multidrug resistance (SMR) (TC 2.A.7.1) family.</text>
  </comment>
  <dbReference type="EMBL" id="X68232">
    <property type="protein sequence ID" value="CAA48311.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X72585">
    <property type="protein sequence ID" value="CAA51179.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U67194">
    <property type="protein sequence ID" value="AAC64463.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_044260.1">
    <property type="nucleotide sequence ID" value="NC_001735.4"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_010890145.1">
    <property type="nucleotide sequence ID" value="NG_048041.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0AGD0"/>
  <dbReference type="SMR" id="P0AGD0"/>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0022857">
    <property type="term" value="F:transmembrane transporter activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.3730.20:FF:000001">
    <property type="entry name" value="Quaternary ammonium compound resistance transporter SugE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.3730.20">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000390">
    <property type="entry name" value="Small_drug/metabolite_transptr"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR045324">
    <property type="entry name" value="Small_multidrug_res"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR30561:SF1">
    <property type="entry name" value="MULTIDRUG TRANSPORTER EMRE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR30561">
    <property type="entry name" value="SMR FAMILY PROTON-DEPENDENT DRUG EFFLUX TRANSPORTER SUGE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00893">
    <property type="entry name" value="Multi_Drug_Res"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF103481">
    <property type="entry name" value="Multidrug resistance efflux transporter EmrE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0614">Plasmid</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000108086" description="Quaternary ammonium compound-resistance protein QacE">
    <location>
      <begin position="1"/>
      <end position="110"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="30"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="58"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="85"/>
      <end position="105"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="110" mass="11472" checksum="94C3CBAFF4C71AC6" modified="2005-12-20" version="1">MKGWLFLVIAIVGEVIATSALKSSEGFTKLAPSAVVIIGYGIAFYFLSLVLKSIPVGVAYAVWSGLGVVIITAIAWLLHGQKLDAWGFVGMGLIVSGVVVLNLLSKASAH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>