<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-04-12" modified="2024-11-27" version="93" xmlns="http://uniprot.org/uniprot">
  <accession>D4ABW7</accession>
  <name>LYZL4_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Lysozyme-like protein 4</fullName>
      <shortName>Lysozyme-4</shortName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">Lyzl4</name>
    <name evidence="5" type="synonym">Lyza</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2011" name="PLoS ONE" volume="6" first="E27659" last="E27659">
      <title>Characterization of a novel lysozyme-like 4 gene in the rat.</title>
      <authorList>
        <person name="Narmadha G."/>
        <person name="Muneswararao K."/>
        <person name="Rajesh A."/>
        <person name="Yenugu S."/>
      </authorList>
      <dbReference type="PubMed" id="22110709"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0027659"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>ABSENCE OF BACTERIOLYTIC AND LYSOZYME ACTIVITIES</scope>
    <source>
      <strain>Wistar</strain>
      <tissue>Epididymis</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Nature" volume="428" first="493" last="521">
      <title>Genome sequence of the Brown Norway rat yields insights into mammalian evolution.</title>
      <authorList>
        <person name="Gibbs R.A."/>
        <person name="Weinstock G.M."/>
        <person name="Metzker M.L."/>
        <person name="Muzny D.M."/>
        <person name="Sodergren E.J."/>
        <person name="Scherer S."/>
        <person name="Scott G."/>
        <person name="Steffen D."/>
        <person name="Worley K.C."/>
        <person name="Burch P.E."/>
        <person name="Okwuonu G."/>
        <person name="Hines S."/>
        <person name="Lewis L."/>
        <person name="Deramo C."/>
        <person name="Delgado O."/>
        <person name="Dugan-Rocha S."/>
        <person name="Miner G."/>
        <person name="Morgan M."/>
        <person name="Hawes A."/>
        <person name="Gill R."/>
        <person name="Holt R.A."/>
        <person name="Adams M.D."/>
        <person name="Amanatides P.G."/>
        <person name="Baden-Tillson H."/>
        <person name="Barnstead M."/>
        <person name="Chin S."/>
        <person name="Evans C.A."/>
        <person name="Ferriera S."/>
        <person name="Fosler C."/>
        <person name="Glodek A."/>
        <person name="Gu Z."/>
        <person name="Jennings D."/>
        <person name="Kraft C.L."/>
        <person name="Nguyen T."/>
        <person name="Pfannkoch C.M."/>
        <person name="Sitter C."/>
        <person name="Sutton G.G."/>
        <person name="Venter J.C."/>
        <person name="Woodage T."/>
        <person name="Smith D."/>
        <person name="Lee H.-M."/>
        <person name="Gustafson E."/>
        <person name="Cahill P."/>
        <person name="Kana A."/>
        <person name="Doucette-Stamm L."/>
        <person name="Weinstock K."/>
        <person name="Fechtel K."/>
        <person name="Weiss R.B."/>
        <person name="Dunn D.M."/>
        <person name="Green E.D."/>
        <person name="Blakesley R.W."/>
        <person name="Bouffard G.G."/>
        <person name="De Jong P.J."/>
        <person name="Osoegawa K."/>
        <person name="Zhu B."/>
        <person name="Marra M."/>
        <person name="Schein J."/>
        <person name="Bosdet I."/>
        <person name="Fjell C."/>
        <person name="Jones S."/>
        <person name="Krzywinski M."/>
        <person name="Mathewson C."/>
        <person name="Siddiqui A."/>
        <person name="Wye N."/>
        <person name="McPherson J."/>
        <person name="Zhao S."/>
        <person name="Fraser C.M."/>
        <person name="Shetty J."/>
        <person name="Shatsman S."/>
        <person name="Geer K."/>
        <person name="Chen Y."/>
        <person name="Abramzon S."/>
        <person name="Nierman W.C."/>
        <person name="Havlak P.H."/>
        <person name="Chen R."/>
        <person name="Durbin K.J."/>
        <person name="Egan A."/>
        <person name="Ren Y."/>
        <person name="Song X.-Z."/>
        <person name="Li B."/>
        <person name="Liu Y."/>
        <person name="Qin X."/>
        <person name="Cawley S."/>
        <person name="Cooney A.J."/>
        <person name="D'Souza L.M."/>
        <person name="Martin K."/>
        <person name="Wu J.Q."/>
        <person name="Gonzalez-Garay M.L."/>
        <person name="Jackson A.R."/>
        <person name="Kalafus K.J."/>
        <person name="McLeod M.P."/>
        <person name="Milosavljevic A."/>
        <person name="Virk D."/>
        <person name="Volkov A."/>
        <person name="Wheeler D.A."/>
        <person name="Zhang Z."/>
        <person name="Bailey J.A."/>
        <person name="Eichler E.E."/>
        <person name="Tuzun E."/>
        <person name="Birney E."/>
        <person name="Mongin E."/>
        <person name="Ureta-Vidal A."/>
        <person name="Woodwark C."/>
        <person name="Zdobnov E."/>
        <person name="Bork P."/>
        <person name="Suyama M."/>
        <person name="Torrents D."/>
        <person name="Alexandersson M."/>
        <person name="Trask B.J."/>
        <person name="Young J.M."/>
        <person name="Huang H."/>
        <person name="Wang H."/>
        <person name="Xing H."/>
        <person name="Daniels S."/>
        <person name="Gietzen D."/>
        <person name="Schmidt J."/>
        <person name="Stevens K."/>
        <person name="Vitt U."/>
        <person name="Wingrove J."/>
        <person name="Camara F."/>
        <person name="Mar Alba M."/>
        <person name="Abril J.F."/>
        <person name="Guigo R."/>
        <person name="Smit A."/>
        <person name="Dubchak I."/>
        <person name="Rubin E.M."/>
        <person name="Couronne O."/>
        <person name="Poliakov A."/>
        <person name="Huebner N."/>
        <person name="Ganten D."/>
        <person name="Goesele C."/>
        <person name="Hummel O."/>
        <person name="Kreitler T."/>
        <person name="Lee Y.-A."/>
        <person name="Monti J."/>
        <person name="Schulz H."/>
        <person name="Zimdahl H."/>
        <person name="Himmelbauer H."/>
        <person name="Lehrach H."/>
        <person name="Jacob H.J."/>
        <person name="Bromberg S."/>
        <person name="Gullings-Handley J."/>
        <person name="Jensen-Seaman M.I."/>
        <person name="Kwitek A.E."/>
        <person name="Lazar J."/>
        <person name="Pasko D."/>
        <person name="Tonellato P.J."/>
        <person name="Twigger S."/>
        <person name="Ponting C.P."/>
        <person name="Duarte J.M."/>
        <person name="Rice S."/>
        <person name="Goodstadt L."/>
        <person name="Beatson S.A."/>
        <person name="Emes R.D."/>
        <person name="Winter E.E."/>
        <person name="Webber C."/>
        <person name="Brandt P."/>
        <person name="Nyakatura G."/>
        <person name="Adetobi M."/>
        <person name="Chiaromonte F."/>
        <person name="Elnitski L."/>
        <person name="Eswara P."/>
        <person name="Hardison R.C."/>
        <person name="Hou M."/>
        <person name="Kolbe D."/>
        <person name="Makova K."/>
        <person name="Miller W."/>
        <person name="Nekrutenko A."/>
        <person name="Riemer C."/>
        <person name="Schwartz S."/>
        <person name="Taylor J."/>
        <person name="Yang S."/>
        <person name="Zhang Y."/>
        <person name="Lindpaintner K."/>
        <person name="Andrews T.D."/>
        <person name="Caccamo M."/>
        <person name="Clamp M."/>
        <person name="Clarke L."/>
        <person name="Curwen V."/>
        <person name="Durbin R.M."/>
        <person name="Eyras E."/>
        <person name="Searle S.M."/>
        <person name="Cooper G.M."/>
        <person name="Batzoglou S."/>
        <person name="Brudno M."/>
        <person name="Sidow A."/>
        <person name="Stone E.A."/>
        <person name="Payseur B.A."/>
        <person name="Bourque G."/>
        <person name="Lopez-Otin C."/>
        <person name="Puente X.S."/>
        <person name="Chakrabarti K."/>
        <person name="Chatterji S."/>
        <person name="Dewey C."/>
        <person name="Pachter L."/>
        <person name="Bray N."/>
        <person name="Yap V.B."/>
        <person name="Caspi A."/>
        <person name="Tesler G."/>
        <person name="Pevzner P.A."/>
        <person name="Haussler D."/>
        <person name="Roskin K.M."/>
        <person name="Baertsch R."/>
        <person name="Clawson H."/>
        <person name="Furey T.S."/>
        <person name="Hinrichs A.S."/>
        <person name="Karolchik D."/>
        <person name="Kent W.J."/>
        <person name="Rosenbloom K.R."/>
        <person name="Trumbower H."/>
        <person name="Weirauch M."/>
        <person name="Cooper D.N."/>
        <person name="Stenson P.D."/>
        <person name="Ma B."/>
        <person name="Brent M."/>
        <person name="Arumugam M."/>
        <person name="Shteynberg D."/>
        <person name="Copley R.R."/>
        <person name="Taylor M.S."/>
        <person name="Riethman H."/>
        <person name="Mudunuri U."/>
        <person name="Peterson J."/>
        <person name="Guyer M."/>
        <person name="Felsenfeld A."/>
        <person name="Old S."/>
        <person name="Mockrin S."/>
        <person name="Collins F.S."/>
      </authorList>
      <dbReference type="PubMed" id="15057822"/>
      <dbReference type="DOI" id="10.1038/nature02426"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Brown Norway</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="submission" date="2005-09" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Adams M.D."/>
        <person name="Myers E.W."/>
        <person name="Smith H.O."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2016" name="Data Brief" volume="6" first="208" last="213">
      <title>Curated eutherian third party data gene data sets.</title>
      <authorList>
        <person name="Premzl M."/>
      </authorList>
      <dbReference type="PubMed" id="26862561"/>
      <dbReference type="DOI" id="10.1016/j.dib.2015.11.056"/>
    </citation>
    <scope>IDENTIFICATION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 4">May be involved in fertilization (By similarity). Has no detectable bacteriolytic and lysozyme activities in vitro (PubMed:22110709).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasmic vesicle</location>
      <location evidence="1">Secretory vesicle</location>
      <location evidence="1">Acrosome</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="4">Cell projection</location>
      <location evidence="4">Cilium</location>
      <location evidence="4">Flagellum</location>
    </subcellularLocation>
    <text evidence="1 4">Found in the principal piece of sperm tail.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed in the brain, lung, ovary, uterus and testis (PubMed:22110709). In testis expressed in the germinal epithelium and on the maturing spermatozoa (at protein level) (PubMed:22110709).</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="4">Present during stages of postnatal development from 10 to 60 days old (PubMed:22110709).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the glycosyl hydrolase 22 family.</text>
  </comment>
  <comment type="caution">
    <text evidence="6">Although it belongs to the glycosyl hydrolase 22 family, Gly-72 is present instead of the conserved Asp which is an active site residue. It is therefore expected that this protein lacks hydrolase activity.</text>
  </comment>
  <dbReference type="EMBL" id="HM125534">
    <property type="protein sequence ID" value="ADK94117.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AABR07071764">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH473954">
    <property type="protein sequence ID" value="EDL76834.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HG931781">
    <property type="protein sequence ID" value="CDM98782.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001233112.1">
    <property type="nucleotide sequence ID" value="NM_001246183.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_006244187.1">
    <property type="nucleotide sequence ID" value="XM_006244125.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="D4ABW7"/>
  <dbReference type="SMR" id="D4ABW7"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000026173"/>
  <dbReference type="PhosphoSitePlus" id="D4ABW7"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000026173"/>
  <dbReference type="Ensembl" id="ENSRNOT00000026173.7">
    <property type="protein sequence ID" value="ENSRNOP00000026173.4"/>
    <property type="gene ID" value="ENSRNOG00000019350.7"/>
  </dbReference>
  <dbReference type="GeneID" id="363168"/>
  <dbReference type="KEGG" id="rno:363168"/>
  <dbReference type="UCSC" id="RGD:1308401">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:1308401"/>
  <dbReference type="CTD" id="131375"/>
  <dbReference type="RGD" id="1308401">
    <property type="gene designation" value="Lyzl4"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502SSER">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000162293"/>
  <dbReference type="HOGENOM" id="CLU_111620_1_1_1"/>
  <dbReference type="InParanoid" id="D4ABW7"/>
  <dbReference type="OrthoDB" id="5344399at2759"/>
  <dbReference type="PhylomeDB" id="D4ABW7"/>
  <dbReference type="TreeFam" id="TF324882"/>
  <dbReference type="PRO" id="PR:D4ABW7"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 8"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000234681">
    <property type="component" value="Chromosome 8"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000019350">
    <property type="expression patterns" value="Expressed in testis and 3 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001669">
    <property type="term" value="C:acrosomal vesicle"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0036126">
    <property type="term" value="C:sperm flagellum"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009566">
    <property type="term" value="P:fertilization"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007342">
    <property type="term" value="P:fusion of sperm to egg plasma membrane involved in single fertilization"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd16897">
    <property type="entry name" value="LYZ_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.530.10:FF:000001">
    <property type="entry name" value="Lysozyme C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.530.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001916">
    <property type="entry name" value="Glyco_hydro_22"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019799">
    <property type="entry name" value="Glyco_hydro_22_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000974">
    <property type="entry name" value="Glyco_hydro_22_lys"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023346">
    <property type="entry name" value="Lysozyme-like_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407">
    <property type="entry name" value="LYSOZYME C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407:SF21">
    <property type="entry name" value="LYSOZYME-LIKE PROTEIN 4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00062">
    <property type="entry name" value="Lys"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00137">
    <property type="entry name" value="LYSOZYME"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00135">
    <property type="entry name" value="LYZLACT"/>
  </dbReference>
  <dbReference type="SMART" id="SM00263">
    <property type="entry name" value="LYZ1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF53955">
    <property type="entry name" value="Lysozyme-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00128">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51348">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0966">Cell projection</keyword>
  <keyword id="KW-0969">Cilium</keyword>
  <keyword id="KW-0968">Cytoplasmic vesicle</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0278">Fertilization</keyword>
  <keyword id="KW-0282">Flagellum</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5008161378" description="Lysozyme-like protein 4" evidence="2">
    <location>
      <begin position="20"/>
      <end position="145"/>
    </location>
  </feature>
  <feature type="domain" description="C-type lysozyme" evidence="3">
    <location>
      <begin position="20"/>
      <end position="145"/>
    </location>
  </feature>
  <feature type="active site" evidence="3">
    <location>
      <position position="54"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="25"/>
      <end position="143"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="49"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="84"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="91"/>
      <end position="109"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q9D925"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00680"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="22110709"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="26862561"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="145" mass="16302" checksum="130FF4FCACC68E6A" modified="2010-04-20" version="1" precursor="true">MQLYLVLLLISYLLTPIGASILGRCVVAKKLYDGGLNYFEGYSLENWVCLAYFESKFNPSAVYENSRDGSTGFGLFQIRDNEWCDHGKNLCSVSCTALLNPNLKDTIECAKKIVKGKQGMGAWPVWSRNCQLSDILDRWLDGCEL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>