<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1999-07-15" modified="2024-11-27" version="74" xmlns="http://uniprot.org/uniprot">
  <accession>P81702</accession>
  <accession>Q8NJ53</accession>
  <name>CEPL_CERFI</name>
  <protein>
    <recommendedName>
      <fullName evidence="10 11 15 21">Cerato-platanin</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="11 12 20" type="primary">cp</name>
    <name evidence="22" type="ORF">CFO_g4460</name>
  </gene>
  <organism>
    <name type="scientific">Ceratocystis fimbriata f. sp. platani</name>
    <dbReference type="NCBI Taxonomy" id="88771"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Ascomycota</taxon>
      <taxon>Pezizomycotina</taxon>
      <taxon>Sordariomycetes</taxon>
      <taxon>Hypocreomycetidae</taxon>
      <taxon>Microascales</taxon>
      <taxon>Ceratocystidaceae</taxon>
      <taxon>Ceratocystis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="FEMS Microbiol. Lett." volume="233" first="341" last="346">
      <title>Cerato-platanin protein is located in the cell walls of ascospores, conidia and hyphae of Ceratocystis fimbriata f. sp. platani.</title>
      <authorList>
        <person name="Boddi S."/>
        <person name="Comparini C."/>
        <person name="Calamassi R."/>
        <person name="Pazzagli L."/>
        <person name="Cappugi G."/>
        <person name="Scala A."/>
      </authorList>
      <dbReference type="PubMed" id="15063505"/>
      <dbReference type="DOI" id="10.1016/j.femsle.2004.03.001"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <strain evidence="11">Cf AF 100</strain>
    </source>
  </reference>
  <reference evidence="21" key="2">
    <citation type="journal article" date="2006" name="Cell Biochem. Biophys." volume="44" first="512" last="521">
      <title>Cerato-platanin, the first member of a new fungal protein family: cloning, expression, and characterization.</title>
      <authorList>
        <person name="Pazzagli L."/>
        <person name="Pantera B."/>
        <person name="Carresi L."/>
        <person name="Zoppi C."/>
        <person name="Pertinhez T.A."/>
        <person name="Spisni A."/>
        <person name="Tegli S."/>
        <person name="Scala A."/>
        <person name="Cappugi G."/>
      </authorList>
      <dbReference type="PubMed" id="16679539"/>
      <dbReference type="DOI" id="10.1385/cbb:44:3:512"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>CIRCULAR DICHROISM ANALYSIS</scope>
    <source>
      <strain evidence="12 21">Cf AF 100</strain>
      <tissue evidence="12">Mycelium</tissue>
    </source>
  </reference>
  <reference evidence="18" key="3">
    <citation type="journal article" date="2009" name="Appl. Microbiol. Biotechnol." volume="84" first="309" last="322">
      <title>New proteins orthologous to cerato-platanin in various Ceratocystis species and the purification and characterization of cerato-populin from Ceratocystis populicola.</title>
      <authorList>
        <person name="Comparini C."/>
        <person name="Carresi L."/>
        <person name="Pagni E."/>
        <person name="Sbrana F."/>
        <person name="Sebastiani F."/>
        <person name="Luchi N."/>
        <person name="Santini A."/>
        <person name="Capretti P."/>
        <person name="Tiribilli B."/>
        <person name="Pazzagli L."/>
        <person name="Cappugi G."/>
        <person name="Scala A."/>
      </authorList>
      <dbReference type="PubMed" id="19387635"/>
      <dbReference type="DOI" id="10.1007/s00253-009-1998-4"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <strain evidence="13 18">Cf AF 100</strain>
      <strain evidence="13 19">Cf15</strain>
      <tissue evidence="13">Mycelium</tissue>
    </source>
  </reference>
  <reference evidence="20" key="4">
    <citation type="journal article" date="2013" name="Appl. Environ. Microbiol." volume="79" first="5394" last="5404">
      <title>Rapid Detection of Ceratocystis platani Inoculum by Quantitative Real-Time PCR Assay.</title>
      <authorList>
        <person name="Luchi N."/>
        <person name="Ghelardini L."/>
        <person name="Belbahri L."/>
        <person name="Quartier M."/>
        <person name="Santini A."/>
      </authorList>
      <dbReference type="PubMed" id="23811499"/>
      <dbReference type="DOI" id="10.1128/aem.01484-13"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue evidence="14">Mycelium</tissue>
    </source>
  </reference>
  <reference evidence="22 23" key="5">
    <citation type="submission" date="2015-04" db="EMBL/GenBank/DDBJ databases">
      <title>Genome sequence of Ceratocystis platani, a major pathogen of plane trees.</title>
      <authorList>
        <person name="Belbahri L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain evidence="22 23">CFO</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1999" name="J. Biol. Chem." volume="274" first="24959" last="24964">
      <title>Purification, characterization, and amino acid sequence of cerato-platanin, a new phytotoxic protein from Ceratocystis fimbriata f.sp. platani.</title>
      <authorList>
        <person name="Pazzagli L."/>
        <person name="Cappugi G."/>
        <person name="Manao G."/>
        <person name="Camici G."/>
        <person name="Santini A."/>
        <person name="Scala A."/>
      </authorList>
      <dbReference type="PubMed" id="10455173"/>
      <dbReference type="DOI" id="10.1074/jbc.274.35.24959"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 15-134</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BONDS</scope>
    <source>
      <strain evidence="10">Cf AF 100</strain>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2014" name="Appl. Microbiol. Biotechnol." volume="98" first="175" last="184">
      <title>Cerato-platanin shows expansin-like activity on cellulosic materials.</title>
      <authorList>
        <person name="Baccelli I."/>
        <person name="Luti S."/>
        <person name="Bernardi R."/>
        <person name="Scala A."/>
        <person name="Pazzagli L."/>
      </authorList>
      <dbReference type="PubMed" id="23512479"/>
      <dbReference type="DOI" id="10.1007/s00253-013-4822-0"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2014" name="PLoS ONE" volume="9" first="e100959" last="e100959">
      <title>Cerato-platanin induces resistance in Arabidopsis leaves through stomatal perception, overexpression of salicylic acid- and ethylene-signaling genes and camalexin biosynthesis.</title>
      <authorList>
        <person name="Baccelli I."/>
        <person name="Lombardi L."/>
        <person name="Luti S."/>
        <person name="Bernardi R."/>
        <person name="Picciarelli P."/>
        <person name="Scala A."/>
        <person name="Pazzagli L."/>
      </authorList>
      <dbReference type="PubMed" id="24968226"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0100959"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2020" name="Int. J. Biol. Macromol." volume="165" first="2845" last="2854">
      <title>Partitioning the structural features that underlie expansin-like and elicitor activities of cerato-platanin.</title>
      <authorList>
        <person name="Luti S."/>
        <person name="Bemporad F."/>
        <person name="Vivoli Vega M."/>
        <person name="Leri M."/>
        <person name="Musiani F."/>
        <person name="Baccelli I."/>
        <person name="Pazzagli L."/>
      </authorList>
      <dbReference type="PubMed" id="33736287"/>
      <dbReference type="DOI" id="10.1016/j.ijbiomac.2020.10.122"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>BIOTECHNOLOGY</scope>
    <scope>MUTAGENESIS OF VAL-77; ASP-91 AND ASN-98</scope>
    <scope>IN SILICO MOLECULAR DOCKING OF CELLOHEXOSE</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2011" name="J. Biol. Chem." volume="286" first="17560" last="17568">
      <title>The structure of the elicitor Cerato-platanin (CP), the first member of the CP fungal protein family, reveals a double psibeta-barrel fold and carbohydrate binding.</title>
      <authorList>
        <person name="de Oliveira A.L."/>
        <person name="Gallo M."/>
        <person name="Pazzagli L."/>
        <person name="Benedetti C.E."/>
        <person name="Cappugi G."/>
        <person name="Scala A."/>
        <person name="Pantera B."/>
        <person name="Spisni A."/>
        <person name="Pertinhez T.A."/>
        <person name="Cicero D.O."/>
      </authorList>
      <dbReference type="PubMed" id="21454637"/>
      <dbReference type="DOI" id="10.1074/jbc.m111.223644"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>REGIONS</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="2 4 6 7 8 9">Phytotoxin which causes production of phytoalexin in platanus acerifolia, platanus occidentalis and platanus orientalis (PubMed:10455173, PubMed:16679539, PubMed:21454637, PubMed:24968226, PubMed:33736287). Induces cell necrosis in tobacco leaves, and in callus cells and leaves of P.acerifolia (PubMed:10455173). Induces reactive oxygen species (ROS) synthesis, nitric oxide (NO) production and mitogen-activated protein kinases (MAPKs) phosphorylation in the leaves of A.thaliana and P.acerifolia (PubMed:33736287). Results in H(2)O(2) production on the epidermis around the stomata, rapid closure of the stomata, reduced photosynthetic and CO(2) assimilation rate, and an increase in a number of volatile organic compound (VOC) emission in A.thaliana leaves (PubMed:24968226). Induces overexpression of genes related to salicylic acid- and ethylene-signaling, camalexin synthesis, ROS production and oxidative stress, and genes of various receptor kinases, and down-regulation of a number of jasmonic acid (JA)-signaling genes in A.thaliana leaves (PubMed:24968226). Renders resistance against C.platani in A.thaliana and P.acerifolia (PubMed:24968226). Renders localised resistance of A.thaliana against infection by virulent foliar pathogens B.cinerea and P.syringae pv. tomato (PubMed:24968226). Binds cellulose analog carboxymethyl cellulose (CMC) (PubMed:33736287). Expansin-like protein with probable fungal and plant cell wall loosening activity based on its non-enzymatic cellulose weakening activity in vitro (PubMed:23512479, PubMed:33736287). Increases glucose production by cellulase after pre-incubation of cellulose with this protein (PubMed:33736287). In contrast, according to PubMed:23512479, no synergistic effect with cellulases (PubMed:23512479). May have a structural role in the fungal cell wall based on its ability to bind chitin, but does not bind beta-1,3-glucan (PubMed:23512479).</text>
  </comment>
  <comment type="biophysicochemical properties">
    <phDependence>
      <text evidence="7">Optimum pH is 5.0 for the non-enzymatic cellulose weakening activity.</text>
    </phDependence>
    <temperatureDependence>
      <text evidence="7">Optimum temperature is 38 degrees Celsius for the non-enzymatic cellulose weakening activity.</text>
    </temperatureDependence>
  </comment>
  <comment type="subunit">
    <text evidence="4 6 9">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2 3 4 5">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="3 5">Secreted</location>
      <location evidence="3 5">Cell wall</location>
    </subcellularLocation>
    <text evidence="3 5">Localizes on the cell walls of hyphae and conidia (PubMed:15063505, PubMed:19387635). Localizes on the cell walls of ascospores (PubMed:15063505). More abundant on the conidial and ascospore walls than on the hyphal wall (PubMed:15063505). Distributes across the entire thickness of the conidial wall (PubMed:15063505). More densely localized on the layer near the plasma membrane than on the more outward layer of the ascospore wall (PubMed:15063505). Localizes also on the fibrillar material at the surface of ascospores (PubMed:15063505).</text>
  </comment>
  <comment type="mass spectrometry" mass="12383.6" method="MALDI" evidence="2">
    <text>The measured mass is that of the mature protein.</text>
  </comment>
  <comment type="mass spectrometry" mass="12395.0" method="MALDI" evidence="4">
    <text>The measured mass is that of the mature protein.</text>
  </comment>
  <comment type="biotechnology">
    <text evidence="17">Generated Asn-98 mutant protein displays improved cellulose weakening activity, thus making it a potential candidate in the production of biofuel as an enhancer of enzymatic hydrolysis of cellulosic materials.</text>
  </comment>
  <comment type="similarity">
    <text evidence="16">Belongs to the cerato-platanin family.</text>
  </comment>
  <dbReference type="EMBL" id="AJ311644">
    <property type="protein sequence ID" value="CAC84090.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF017218">
    <property type="protein sequence ID" value="ABM63505.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF017226">
    <property type="protein sequence ID" value="ABM63513.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KF302680">
    <property type="protein sequence ID" value="AGQ22226.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="LBBL01000268">
    <property type="protein sequence ID" value="KKF93197.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PDB" id="2KQA">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=15-134"/>
  </dbReference>
  <dbReference type="PDBsum" id="2KQA"/>
  <dbReference type="AlphaFoldDB" id="P81702"/>
  <dbReference type="SMR" id="P81702"/>
  <dbReference type="EvolutionaryTrace" id="P81702"/>
  <dbReference type="PHI-base" id="PHI:3167"/>
  <dbReference type="Proteomes" id="UP000034841">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005619">
    <property type="term" value="C:ascospore wall"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043245">
    <property type="term" value="C:extraorganismal space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009277">
    <property type="term" value="C:fungal-type cell wall"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030446">
    <property type="term" value="C:hyphal cell wall"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008061">
    <property type="term" value="F:chitin binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044359">
    <property type="term" value="P:modulation of molecular function in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035821">
    <property type="term" value="P:modulation of process of another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd22778">
    <property type="entry name" value="DPBB_CEPL-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.40.10">
    <property type="entry name" value="RlpA-like domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010829">
    <property type="entry name" value="Cerato-platanin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036908">
    <property type="entry name" value="RlpA-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07249">
    <property type="entry name" value="Cerato-platanin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50685">
    <property type="entry name" value="Barwin-like endoglucanases"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0134">Cell wall</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="1 2">
    <location>
      <begin position="1"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000156113" description="Cerato-platanin" evidence="1 2">
    <location>
      <begin position="15"/>
      <end position="134"/>
    </location>
  </feature>
  <feature type="region of interest" description="Binding of oligomers of N-acetylglucosamine (GlcNAc)" evidence="6">
    <location>
      <begin position="18"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="region of interest" description="Binding of N-acetylglucosamine tetramer (GlcNAc-4)" evidence="6">
    <location>
      <begin position="31"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="region of interest" description="Binding of N-acetylglucosamine tetramer (GlcNAc-4)" evidence="6">
    <location>
      <begin position="65"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="region of interest" description="Binding of N-acetylglucosamine tetramer (GlcNAc-4)" evidence="6">
    <location>
      <begin position="91"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="region of interest" description="Binding of oligomers of N-acetylglucosamine (GlcNAc)" evidence="6">
    <location>
      <begin position="113"/>
      <end position="116"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 4 6 24">
    <location>
      <begin position="34"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 4 6 24">
    <location>
      <begin position="74"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Altered protein stability due to dramatic change in structure without altering the net charge of the protein. Dramatically altered thermal resistance and thermodynamic stability. No change in cellulose weakening activity in vitro. Pre-incubation of cellulose with the mutant increases glucose production by cellulase. Weak induction of phytoalexins production and reactive oxygen species (ROS) synthesis in the leaves of the non-host plant A.thaliana. No change in binding affinity to cellulose analog carboxymethyl cellulose (CMC)." evidence="9">
    <original>V</original>
    <variation>A</variation>
    <location>
      <position position="77"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No change in structure, but a dramatic change in thermal resistance and thermodynamic stability likely due to loss of a negative charge. Weak induction of phytoalexins production, but loss of induction of reactive oxygen species (ROS) synthesis in the leaves of the non-host plant A.thaliana. Lower binding affinity to cellulose analog carboxymethyl cellulose (CMC)." evidence="9">
    <original>D</original>
    <variation>A</variation>
    <location>
      <position position="91"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No change in structure, thermal resistance nor thermodynamic stability. Increased negative net charge in the region involved in carbohydrate interaction. Slightly increased cellulose weakening activity in vitro. Pre-incubation of cellulose with the mutant increases glucose production by cellulase. Weak induction of phytoalexins production, but loss of induction of reactive oxygen species (ROS) synthesis in the leaves of the non-host plant A.thaliana. Higher binding affinity to cellulose analog carboxymethyl cellulose (CMC)." evidence="9">
    <original>N</original>
    <variation>D</variation>
    <location>
      <position position="98"/>
    </location>
  </feature>
  <feature type="strand" evidence="25">
    <location>
      <begin position="3"/>
      <end position="5"/>
    </location>
  </feature>
  <feature type="helix" evidence="25">
    <location>
      <begin position="15"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="turn" evidence="25">
    <location>
      <begin position="23"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="turn" evidence="25">
    <location>
      <begin position="28"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="strand" evidence="25">
    <location>
      <begin position="32"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="strand" evidence="25">
    <location>
      <begin position="40"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="strand" evidence="25">
    <location>
      <begin position="60"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="turn" evidence="25">
    <location>
      <begin position="66"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="strand" evidence="25">
    <location>
      <begin position="69"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="strand" evidence="25">
    <location>
      <begin position="80"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="helix" evidence="25">
    <location>
      <begin position="87"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="strand" evidence="25">
    <location>
      <begin position="95"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="strand" evidence="25">
    <location>
      <begin position="106"/>
      <end position="109"/>
    </location>
  </feature>
  <feature type="helix" evidence="25">
    <location>
      <begin position="112"/>
      <end position="119"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10455173"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15063505"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="16679539"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="19387635"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="21454637"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="23512479"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="24968226"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="33736287"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="10">
    <source>
      <dbReference type="PubMed" id="10455173"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="11">
    <source>
      <dbReference type="PubMed" id="15063505"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="16679539"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="13">
    <source>
      <dbReference type="PubMed" id="19387635"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="14">
    <source>
      <dbReference type="PubMed" id="23811499"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="15">
    <source>
      <dbReference type="PubMed" id="33736287"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="16"/>
  <evidence type="ECO:0000305" key="17">
    <source>
      <dbReference type="PubMed" id="33736287"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="18">
    <source>
      <dbReference type="EMBL" id="ABM63505.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="19">
    <source>
      <dbReference type="EMBL" id="ABM63513.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="20">
    <source>
      <dbReference type="EMBL" id="AGQ22226.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="21">
    <source>
      <dbReference type="EMBL" id="CAC84090.2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="22">
    <source>
      <dbReference type="EMBL" id="KKF93197.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="23">
    <source>
      <dbReference type="Proteomes" id="UP000034841"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="24">
    <source>
      <dbReference type="PDB" id="2KQA"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="25">
    <source>
      <dbReference type="PDB" id="2KQA"/>
    </source>
  </evidence>
  <sequence length="134" mass="13893" checksum="FF489D98D0581D8D" modified="2024-10-02" version="2" precursor="true">MKFSILPMIASAMAVSISYDPIYAADLSMGSVACSNGDHGLMAQYPTLGEVPGFPNVGGIPDIAGWDSPSCGTCWKVTIPNGNSIFIRGVDSGRGGFNVNPTAFTKLVGSTEAGRVDNVNYVQVDLSNCINGAN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>