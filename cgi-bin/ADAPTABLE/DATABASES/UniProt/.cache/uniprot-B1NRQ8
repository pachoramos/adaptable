<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-06-10" modified="2023-02-22" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>B1NRQ8</accession>
  <accession>P85243</accession>
  <name>CYVE_VIOBI</name>
  <protein>
    <recommendedName>
      <fullName>Cyclotide vibi-E</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Vbc1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Viola biflora</name>
    <name type="common">Yellow wood violet</name>
    <dbReference type="NCBI Taxonomy" id="214529"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Malpighiales</taxon>
      <taxon>Violaceae</taxon>
      <taxon>Viola</taxon>
      <taxon>Viola subgen. Viola</taxon>
      <taxon>Viola sect. Chamaemelanium</taxon>
    </lineage>
  </organism>
  <reference evidence="5 6" key="1">
    <citation type="journal article" date="2008" name="Phytochemistry" volume="69" first="939" last="952">
      <title>The alpine violet, Viola biflora, is a rich source of cyclotides with potent cytotoxicity.</title>
      <authorList>
        <person name="Herrmann A."/>
        <person name="Burman R."/>
        <person name="Mylne J.S."/>
        <person name="Karlsson G."/>
        <person name="Gullbo J."/>
        <person name="Craik D.J."/>
        <person name="Clark R.J."/>
        <person name="Goeransson U."/>
      </authorList>
      <dbReference type="PubMed" id="18191970"/>
      <dbReference type="DOI" id="10.1016/j.phytochem.2007.10.023"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 70-99</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="4">Leaf</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3 4 5">Probably participates in a plant defense mechanism. Has cytotoxic activity, active against a human lymphoma cell line with an IC(50) of 3.2 uM.</text>
  </comment>
  <comment type="domain">
    <text evidence="2">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3 4">This is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="3081.0" method="Electrospray" evidence="4"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the cyclotide family. Bracelet subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="EU046618">
    <property type="protein sequence ID" value="ABW08090.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B1NRQ8"/>
  <dbReference type="SMR" id="B1NRQ8"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012323">
    <property type="entry name" value="Cyclotide_bracelet_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60008">
    <property type="entry name" value="CYCLOTIDE_BRACELET"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1" status="less than"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000341425" evidence="4">
    <location>
      <begin position="10"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000341426" description="Cyclotide vibi-E">
    <location>
      <begin position="70"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000341427" evidence="4">
    <location>
      <begin position="100"/>
      <end position="105"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="73"/>
      <end position="90"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="77"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="82"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Gly-Asn)" evidence="4">
    <location>
      <begin position="70"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="6">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P58443"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P82230"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="18191970"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="ABW08090.1"/>
    </source>
  </evidence>
  <sequence length="105" mass="11068" checksum="3A680C6294765BE5" modified="2008-04-29" version="1" precursor="true" fragment="single">AAFALPALASSFEKDVISFRAIQAVLEKRGLSKLEDDPVLSALAHTKTIISNPVIEEALLNGANLKAGNGIPCAESCVWIPCTVTALIGCGCSNKVCYNSLQTKY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>