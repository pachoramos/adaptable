<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-07-19" modified="2024-10-02" version="50" xmlns="http://uniprot.org/uniprot">
  <accession>Q5IAA6</accession>
  <name>D108B_PANTR</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 108B</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Beta-defensin 8</fullName>
      <shortName>BD-8</shortName>
      <shortName>DEFB-8</shortName>
      <shortName>cBD-8</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin, beta 108</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin, beta 108B</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB108B</name>
    <name type="synonym">DEFB108</name>
    <name type="synonym">DEFB8</name>
  </gene>
  <organism>
    <name type="scientific">Pan troglodytes</name>
    <name type="common">Chimpanzee</name>
    <dbReference type="NCBI Taxonomy" id="9598"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Pan</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="BMC Evol. Biol." volume="5" first="32" last="32">
      <title>The complexity of selection at the major primate beta-defensin locus.</title>
      <authorList>
        <person name="Semple C.A.M."/>
        <person name="Maxwell A."/>
        <person name="Gautier P."/>
        <person name="Kilanowski F.M."/>
        <person name="Eastwood H."/>
        <person name="Barran P.E."/>
        <person name="Dorin J.R."/>
      </authorList>
      <dbReference type="PubMed" id="15904491"/>
      <dbReference type="DOI" id="10.1186/1471-2148-5-32"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY831745">
    <property type="protein sequence ID" value="AAW32926.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5IAA6"/>
  <dbReference type="SMR" id="Q5IAA6"/>
  <dbReference type="STRING" id="9598.ENSPTRP00000006911"/>
  <dbReference type="PaxDb" id="9598-ENSPTRP00000053708"/>
  <dbReference type="eggNOG" id="ENOG502T9UC">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="Q5IAA6"/>
  <dbReference type="Proteomes" id="UP000002277">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025933">
    <property type="entry name" value="Beta_defensin_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13841">
    <property type="entry name" value="Defensin_beta_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1" status="less than"/>
      <end position="2"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006982" description="Beta-defensin 108B">
    <location>
      <begin position="3"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="8"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="15"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="19"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="53" mass="6058" checksum="11C7DFE35605D9D4" modified="2005-02-15" version="1" precursor="true" fragment="single">RGKFKEICERPDGSCRDFCLETEIHVGRCLNSRPCCLPLGHQPRIESTTPKKD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>