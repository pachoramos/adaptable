<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-03-28" modified="2023-09-13" version="14" xmlns="http://uniprot.org/uniprot">
  <accession>A0A023IWM6</accession>
  <name>MSD1_AMARI</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">MSDIN-like toxin proprotein 1</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="4">Toxin MSD1</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Amanita rimosa</name>
    <dbReference type="NCBI Taxonomy" id="580330"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Basidiomycota</taxon>
      <taxon>Agaricomycotina</taxon>
      <taxon>Agaricomycetes</taxon>
      <taxon>Agaricomycetidae</taxon>
      <taxon>Agaricales</taxon>
      <taxon>Pluteineae</taxon>
      <taxon>Amanitaceae</taxon>
      <taxon>Amanita</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2014" name="Toxicon" volume="83" first="59" last="68">
      <title>The molecular diversity of toxin gene families in lethal Amanita mushrooms.</title>
      <authorList>
        <person name="Li P."/>
        <person name="Deng W."/>
        <person name="Li T."/>
      </authorList>
      <dbReference type="PubMed" id="24613547"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2014.02.020"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="4">Probable toxin that belongs to the MSDIN-like toxin family responsible for a large number of food poisoning cases and deaths (PubMed:24613547).</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Processed by the macrocyclase-peptidase enzyme POPB to yield a toxic cyclic decapeptide (By similarity). POPB first removes 10 residues from the N-terminus (By similarity). Conformational trapping of the remaining peptide forces the enzyme to release this intermediate rather than proceed to macrocyclization (By similarity). The enzyme rebinds the remaining peptide in a different conformation and catalyzes macrocyclization of the N-terminal 10 residues (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the MSDIN fungal toxin family.</text>
  </comment>
  <dbReference type="EMBL" id="KF552087">
    <property type="protein sequence ID" value="AHB18715.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A023IWM6"/>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027582">
    <property type="entry name" value="Amanitin/phalloidin"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR04309">
    <property type="entry name" value="amanitin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="propeptide" id="PRO_0000443707" evidence="4">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000443708" description="Toxin MSD1" evidence="4">
    <location>
      <begin position="11"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000443709" evidence="4">
    <location>
      <begin position="21"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Ile-Pro)" evidence="4">
    <location>
      <begin position="11"/>
      <end position="20"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A067SLB9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="24613547"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="24613547"/>
    </source>
  </evidence>
  <sequence length="35" mass="3880" checksum="E5A6BA2D4EAC3212" modified="2014-07-09" version="1" precursor="true">MSDINATRLPIIIVLGLIIPLCVSDIEMILTRGER</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>