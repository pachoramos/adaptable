<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-07-24" version="34" xmlns="http://uniprot.org/uniprot">
  <accession>P69206</accession>
  <accession>P01163</accession>
  <name>MORN_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>Morphogenetic neuropeptide</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Head activator</fullName>
      <shortName>HA</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1981" name="Nature" volume="293" first="579" last="580">
      <title>Conserved amino acid sequence of a neuropeptide, the head activator, from coelenterates to humans.</title>
      <authorList>
        <person name="Bodenmuller H."/>
        <person name="Schaller H.C."/>
      </authorList>
      <dbReference type="PubMed" id="7290191"/>
      <dbReference type="DOI" id="10.1038/293579a0"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1981" name="FEBS Lett." volume="131" first="317" last="321">
      <title>Synthesis of a new neuropeptide, the head activator from hydra.</title>
      <authorList>
        <person name="Birr C."/>
        <person name="Zachmann B."/>
        <person name="Bodenmuller H."/>
        <person name="Schaller H.C."/>
      </authorList>
      <dbReference type="PubMed" id="7297679"/>
      <dbReference type="DOI" id="10.1016/0014-5793(81)80394-8"/>
    </citation>
    <scope>SYNTHESIS</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Stimulates the proliferation of neural cells.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">May interact with SORL1 (via N-terminal ectodomain); this interaction is impaired in the presence of SORL1 propeptide.</text>
  </comment>
  <comment type="caution">
    <text evidence="2">This peptide was first isolated from nerve cells of hydra and was called head activator by the authors, because it induced head-specific growth and differentiation in this animal. It has been found in mammalian intestine and hypothalamus.</text>
  </comment>
  <dbReference type="PIR" id="C01427">
    <property type="entry name" value="YHBO"/>
  </dbReference>
  <dbReference type="InParanoid" id="P69206"/>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008083">
    <property type="term" value="F:growth factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051301">
    <property type="term" value="P:cell division"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0131">Cell cycle</keyword>
  <keyword id="KW-0132">Cell division</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0339">Growth factor</keyword>
  <keyword id="KW-0498">Mitosis</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="peptide" id="PRO_0000044166" description="Morphogenetic neuropeptide">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P69208"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="11" mass="1142" checksum="37927417C325B878" modified="1986-07-21" version="1">QPPGGSKVILF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>