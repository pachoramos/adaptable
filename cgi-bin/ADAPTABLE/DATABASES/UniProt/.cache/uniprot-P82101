<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-01-23" modified="2020-06-17" version="35" xmlns="http://uniprot.org/uniprot">
  <accession>P82101</accession>
  <name>EI05_LITRU</name>
  <protein>
    <recommendedName>
      <fullName>Electrin-5</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Litoria rubella</name>
    <name type="common">Desert tree frog</name>
    <name type="synonym">Hyla rubella</name>
    <dbReference type="NCBI Taxonomy" id="104895"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Litoria</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Aust. J. Chem." volume="52" first="639" last="645">
      <title>Peptides from the skin glands of the Australian buzzing tree frog Litori electrica. Comparison with the skin peptides from Litoria rubella.</title>
      <authorList>
        <person name="Wabnitz P.A."/>
        <person name="Bowie J.H."/>
        <person name="Tyler M.J."/>
        <person name="Wallace J.C."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT ALA-7</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043796" description="Electrin-5">
    <location>
      <begin position="1"/>
      <end position="7"/>
    </location>
  </feature>
  <feature type="modified residue" description="Alanine amide" evidence="1">
    <location>
      <position position="7"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source ref="1"/>
  </evidence>
  <sequence length="7" mass="834" checksum="6DD05B076B0B5030" modified="2000-05-01" version="1">IYEPEIA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>