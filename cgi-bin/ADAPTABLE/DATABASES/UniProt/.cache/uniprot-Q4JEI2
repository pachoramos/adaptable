<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-11-22" modified="2024-07-24" version="122" xmlns="http://uniprot.org/uniprot">
  <accession>Q4JEI2</accession>
  <name>DFAL1_RAT</name>
  <protein>
    <recommendedName>
      <fullName evidence="14">Defensin alpha-like protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6">Defensin alpha-related sequence 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="7">Rattusin</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="14" type="primary">Defal1</name>
    <name evidence="6" type="synonym">Defa-rs1</name>
  </gene>
  <organism evidence="13">
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference evidence="11" key="1">
    <citation type="journal article" date="2004" name="Physiol. Genomics" volume="20" first="1" last="11">
      <title>Rapid evolution and diversification of mammalian alpha-defensins as revealed by comparative analysis of rodent and primate genes.</title>
      <authorList>
        <person name="Patil A."/>
        <person name="Hughes A.L."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="15494476"/>
      <dbReference type="DOI" id="10.1152/physiolgenomics.00150.2004"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference evidence="13" key="2">
    <citation type="journal article" date="2004" name="Nature" volume="428" first="493" last="521">
      <title>Genome sequence of the Brown Norway rat yields insights into mammalian evolution.</title>
      <authorList>
        <person name="Gibbs R.A."/>
        <person name="Weinstock G.M."/>
        <person name="Metzker M.L."/>
        <person name="Muzny D.M."/>
        <person name="Sodergren E.J."/>
        <person name="Scherer S."/>
        <person name="Scott G."/>
        <person name="Steffen D."/>
        <person name="Worley K.C."/>
        <person name="Burch P.E."/>
        <person name="Okwuonu G."/>
        <person name="Hines S."/>
        <person name="Lewis L."/>
        <person name="Deramo C."/>
        <person name="Delgado O."/>
        <person name="Dugan-Rocha S."/>
        <person name="Miner G."/>
        <person name="Morgan M."/>
        <person name="Hawes A."/>
        <person name="Gill R."/>
        <person name="Holt R.A."/>
        <person name="Adams M.D."/>
        <person name="Amanatides P.G."/>
        <person name="Baden-Tillson H."/>
        <person name="Barnstead M."/>
        <person name="Chin S."/>
        <person name="Evans C.A."/>
        <person name="Ferriera S."/>
        <person name="Fosler C."/>
        <person name="Glodek A."/>
        <person name="Gu Z."/>
        <person name="Jennings D."/>
        <person name="Kraft C.L."/>
        <person name="Nguyen T."/>
        <person name="Pfannkoch C.M."/>
        <person name="Sitter C."/>
        <person name="Sutton G.G."/>
        <person name="Venter J.C."/>
        <person name="Woodage T."/>
        <person name="Smith D."/>
        <person name="Lee H.-M."/>
        <person name="Gustafson E."/>
        <person name="Cahill P."/>
        <person name="Kana A."/>
        <person name="Doucette-Stamm L."/>
        <person name="Weinstock K."/>
        <person name="Fechtel K."/>
        <person name="Weiss R.B."/>
        <person name="Dunn D.M."/>
        <person name="Green E.D."/>
        <person name="Blakesley R.W."/>
        <person name="Bouffard G.G."/>
        <person name="De Jong P.J."/>
        <person name="Osoegawa K."/>
        <person name="Zhu B."/>
        <person name="Marra M."/>
        <person name="Schein J."/>
        <person name="Bosdet I."/>
        <person name="Fjell C."/>
        <person name="Jones S."/>
        <person name="Krzywinski M."/>
        <person name="Mathewson C."/>
        <person name="Siddiqui A."/>
        <person name="Wye N."/>
        <person name="McPherson J."/>
        <person name="Zhao S."/>
        <person name="Fraser C.M."/>
        <person name="Shetty J."/>
        <person name="Shatsman S."/>
        <person name="Geer K."/>
        <person name="Chen Y."/>
        <person name="Abramzon S."/>
        <person name="Nierman W.C."/>
        <person name="Havlak P.H."/>
        <person name="Chen R."/>
        <person name="Durbin K.J."/>
        <person name="Egan A."/>
        <person name="Ren Y."/>
        <person name="Song X.-Z."/>
        <person name="Li B."/>
        <person name="Liu Y."/>
        <person name="Qin X."/>
        <person name="Cawley S."/>
        <person name="Cooney A.J."/>
        <person name="D'Souza L.M."/>
        <person name="Martin K."/>
        <person name="Wu J.Q."/>
        <person name="Gonzalez-Garay M.L."/>
        <person name="Jackson A.R."/>
        <person name="Kalafus K.J."/>
        <person name="McLeod M.P."/>
        <person name="Milosavljevic A."/>
        <person name="Virk D."/>
        <person name="Volkov A."/>
        <person name="Wheeler D.A."/>
        <person name="Zhang Z."/>
        <person name="Bailey J.A."/>
        <person name="Eichler E.E."/>
        <person name="Tuzun E."/>
        <person name="Birney E."/>
        <person name="Mongin E."/>
        <person name="Ureta-Vidal A."/>
        <person name="Woodwark C."/>
        <person name="Zdobnov E."/>
        <person name="Bork P."/>
        <person name="Suyama M."/>
        <person name="Torrents D."/>
        <person name="Alexandersson M."/>
        <person name="Trask B.J."/>
        <person name="Young J.M."/>
        <person name="Huang H."/>
        <person name="Wang H."/>
        <person name="Xing H."/>
        <person name="Daniels S."/>
        <person name="Gietzen D."/>
        <person name="Schmidt J."/>
        <person name="Stevens K."/>
        <person name="Vitt U."/>
        <person name="Wingrove J."/>
        <person name="Camara F."/>
        <person name="Mar Alba M."/>
        <person name="Abril J.F."/>
        <person name="Guigo R."/>
        <person name="Smit A."/>
        <person name="Dubchak I."/>
        <person name="Rubin E.M."/>
        <person name="Couronne O."/>
        <person name="Poliakov A."/>
        <person name="Huebner N."/>
        <person name="Ganten D."/>
        <person name="Goesele C."/>
        <person name="Hummel O."/>
        <person name="Kreitler T."/>
        <person name="Lee Y.-A."/>
        <person name="Monti J."/>
        <person name="Schulz H."/>
        <person name="Zimdahl H."/>
        <person name="Himmelbauer H."/>
        <person name="Lehrach H."/>
        <person name="Jacob H.J."/>
        <person name="Bromberg S."/>
        <person name="Gullings-Handley J."/>
        <person name="Jensen-Seaman M.I."/>
        <person name="Kwitek A.E."/>
        <person name="Lazar J."/>
        <person name="Pasko D."/>
        <person name="Tonellato P.J."/>
        <person name="Twigger S."/>
        <person name="Ponting C.P."/>
        <person name="Duarte J.M."/>
        <person name="Rice S."/>
        <person name="Goodstadt L."/>
        <person name="Beatson S.A."/>
        <person name="Emes R.D."/>
        <person name="Winter E.E."/>
        <person name="Webber C."/>
        <person name="Brandt P."/>
        <person name="Nyakatura G."/>
        <person name="Adetobi M."/>
        <person name="Chiaromonte F."/>
        <person name="Elnitski L."/>
        <person name="Eswara P."/>
        <person name="Hardison R.C."/>
        <person name="Hou M."/>
        <person name="Kolbe D."/>
        <person name="Makova K."/>
        <person name="Miller W."/>
        <person name="Nekrutenko A."/>
        <person name="Riemer C."/>
        <person name="Schwartz S."/>
        <person name="Taylor J."/>
        <person name="Yang S."/>
        <person name="Zhang Y."/>
        <person name="Lindpaintner K."/>
        <person name="Andrews T.D."/>
        <person name="Caccamo M."/>
        <person name="Clamp M."/>
        <person name="Clarke L."/>
        <person name="Curwen V."/>
        <person name="Durbin R.M."/>
        <person name="Eyras E."/>
        <person name="Searle S.M."/>
        <person name="Cooper G.M."/>
        <person name="Batzoglou S."/>
        <person name="Brudno M."/>
        <person name="Sidow A."/>
        <person name="Stone E.A."/>
        <person name="Payseur B.A."/>
        <person name="Bourque G."/>
        <person name="Lopez-Otin C."/>
        <person name="Puente X.S."/>
        <person name="Chakrabarti K."/>
        <person name="Chatterji S."/>
        <person name="Dewey C."/>
        <person name="Pachter L."/>
        <person name="Bray N."/>
        <person name="Yap V.B."/>
        <person name="Caspi A."/>
        <person name="Tesler G."/>
        <person name="Pevzner P.A."/>
        <person name="Haussler D."/>
        <person name="Roskin K.M."/>
        <person name="Baertsch R."/>
        <person name="Clawson H."/>
        <person name="Furey T.S."/>
        <person name="Hinrichs A.S."/>
        <person name="Karolchik D."/>
        <person name="Kent W.J."/>
        <person name="Rosenbloom K.R."/>
        <person name="Trumbower H."/>
        <person name="Weirauch M."/>
        <person name="Cooper D.N."/>
        <person name="Stenson P.D."/>
        <person name="Ma B."/>
        <person name="Brent M."/>
        <person name="Arumugam M."/>
        <person name="Shteynberg D."/>
        <person name="Copley R.R."/>
        <person name="Taylor M.S."/>
        <person name="Riethman H."/>
        <person name="Mudunuri U."/>
        <person name="Peterson J."/>
        <person name="Guyer M."/>
        <person name="Felsenfeld A."/>
        <person name="Old S."/>
        <person name="Mockrin S."/>
        <person name="Collins F.S."/>
      </authorList>
      <dbReference type="PubMed" id="15057822"/>
      <dbReference type="DOI" id="10.1038/nature02426"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Brown Norway</strain>
    </source>
  </reference>
  <reference evidence="12" key="3">
    <citation type="submission" date="2005-09" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Adams M.D."/>
        <person name="Myers E.W."/>
        <person name="Smith H.O."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference evidence="8" key="4">
    <citation type="journal article" date="2013" name="Antimicrob. Agents Chemother." volume="57" first="1823" last="1831">
      <title>Rattusin, an intestinal alpha-defensin-related peptide in rats with a unique cysteine spacing pattern and salt-insensitive antibacterial activities.</title>
      <authorList>
        <person name="Patil A.A."/>
        <person name="Ouellette A.J."/>
        <person name="Lu W."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="23380721"/>
      <dbReference type="DOI" id="10.1128/aac.02237-12"/>
    </citation>
    <scope>SYNTHESIS OF 57-87</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference evidence="15" key="5">
    <citation type="journal article" date="2017" name="Sci. Rep." volume="7" first="45282" last="45282">
      <title>Rattusin structure reveals a novel defensin scaffold formed by intermolecular disulfide exchanges.</title>
      <authorList>
        <person name="Min H.J."/>
        <person name="Yun H."/>
        <person name="Ji S."/>
        <person name="Rajasekaran G."/>
        <person name="Kim J.I."/>
        <person name="Kim J.S."/>
        <person name="Shin S.Y."/>
        <person name="Lee C.W."/>
      </authorList>
      <dbReference type="PubMed" id="28345637"/>
      <dbReference type="DOI" id="10.1038/srep45282"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 57-87</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>MUTAGENESIS OF CYS-71</scope>
  </reference>
  <comment type="function">
    <text evidence="4 5">Intestinal defense peptide (PubMed:23380721, PubMed:28345637). Has potent antibacterial activity against Gram-negative bacteria E.coli O157:H7, S.typhimurium DT104, and K.pneumoniae; and against Gram-positive bacteria S.aureus, methicillin-resistant S.aureus and L.monocytogenes (PubMed:23380721, PubMed:28345637). Remains active in the presence of NaCl and Mg(2+) (PubMed:23380721). Probably functions by disrupting bacterial membrane integrity (PubMed:28345637). However, does not show cytotoxic activity towards human intestinal cells (PubMed:23380721).</text>
  </comment>
  <comment type="subunit">
    <text evidence="4 5">Antiparallel homodimer; disulfide-linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="9 10">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3 4">Specifically expressed in small intestine (jejunum and ileum) (PubMed:15494476, PubMed:23380721). Probably expressed by Paneth cells at the base of intestinal crypts (PubMed:23380721). Coexpressed with MMP7 in small intestine (PubMed:23380721).</text>
  </comment>
  <comment type="similarity">
    <text evidence="8">Belongs to the alpha-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY623756">
    <property type="protein sequence ID" value="AAT68755.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY623767">
    <property type="protein sequence ID" value="AAT68763.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC128185">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH473970">
    <property type="protein sequence ID" value="EDM08974.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001028245.1">
    <property type="nucleotide sequence ID" value="NM_001033073.1"/>
  </dbReference>
  <dbReference type="PDB" id="5GWG">
    <property type="method" value="NMR"/>
    <property type="chains" value="A/B=57-87"/>
  </dbReference>
  <dbReference type="PDBsum" id="5GWG"/>
  <dbReference type="AlphaFoldDB" id="Q4JEI2"/>
  <dbReference type="BMRB" id="Q4JEI2"/>
  <dbReference type="SMR" id="Q4JEI2"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000047361"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000047361"/>
  <dbReference type="Ensembl" id="ENSRNOT00000049394.4">
    <property type="protein sequence ID" value="ENSRNOP00000047361.2"/>
    <property type="gene ID" value="ENSRNOG00000029462.4"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060043604">
    <property type="protein sequence ID" value="ENSRNOP00060036151"/>
    <property type="gene ID" value="ENSRNOG00060025193"/>
  </dbReference>
  <dbReference type="GeneID" id="613220"/>
  <dbReference type="KEGG" id="rno:613220"/>
  <dbReference type="UCSC" id="RGD:1561398">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:1561398"/>
  <dbReference type="CTD" id="613220"/>
  <dbReference type="RGD" id="1561398">
    <property type="gene designation" value="Defal1"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502TF4X">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000153268"/>
  <dbReference type="HOGENOM" id="CLU_160803_1_0_1"/>
  <dbReference type="InParanoid" id="Q4JEI2"/>
  <dbReference type="OMA" id="RRVCRNT"/>
  <dbReference type="PhylomeDB" id="Q4JEI2"/>
  <dbReference type="TreeFam" id="TF338414"/>
  <dbReference type="Reactome" id="R-RNO-1461973">
    <property type="pathway name" value="Defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-1462054">
    <property type="pathway name" value="Alpha-defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-6798695">
    <property type="pathway name" value="Neutrophil degranulation"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q4JEI2"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 16"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000234681">
    <property type="component" value="Chromosome 16"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000029462">
    <property type="expression patterns" value="Expressed in jejunum and 12 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051673">
    <property type="term" value="P:disruption of plasma membrane integrity in another organism"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016327">
    <property type="entry name" value="Alpha-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002366">
    <property type="entry name" value="Alpha-defensin_N"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876">
    <property type="entry name" value="ALPHA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876:SF2">
    <property type="entry name" value="ALPHA-DEFENSIN 1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00879">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001875">
    <property type="entry name" value="Alpha-defensin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01418">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000442242" evidence="9">
    <location>
      <begin position="20"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000442243" description="Defensin alpha-like protein 1" evidence="9">
    <location>
      <begin position="57"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="23"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (with C-77)" evidence="5 15">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (with C-75)" evidence="5 15">
    <location>
      <position position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain" evidence="5 15">
    <location>
      <position position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (with C-67)" evidence="5 15">
    <location>
      <position position="75"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (with C-65)" evidence="5 15">
    <location>
      <position position="77"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Aberrant protein folding and disordered structure." evidence="5">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="71"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="61"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="65"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="70"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="74"/>
      <end position="77"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15494476"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="23380721"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="28345637"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="15494476"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="23380721"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <evidence type="ECO:0000305" key="9">
    <source>
      <dbReference type="PubMed" id="23380721"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10">
    <source>
      <dbReference type="PubMed" id="28345637"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="11">
    <source>
      <dbReference type="EMBL" id="AAT68755.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="12">
    <source>
      <dbReference type="EMBL" id="EDM08974.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="13">
    <source>
      <dbReference type="Proteomes" id="UP000002494"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="14">
    <source>
      <dbReference type="RGD" id="1561398"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="15">
    <source>
      <dbReference type="PDB" id="5GWG"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="16">
    <source>
      <dbReference type="PDB" id="5GWG"/>
    </source>
  </evidence>
  <sequence length="87" mass="9702" checksum="60A7A9A61C1233FA" modified="2005-08-02" version="1" precursor="true">MKTLILLSALVLLALQVQADPIQEAEEETKTEEQPADEDQDVSVSFEGPEASAVQDLRVRRTLQCSCRRVCRNTCSCIRLSRSTYAS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>