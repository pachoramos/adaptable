<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-06-28" modified="2023-02-22" version="22" xmlns="http://uniprot.org/uniprot">
  <accession>C0KJQ4</accession>
  <accession>C6K7B2</accession>
  <accession>E7CRH9</accession>
  <accession>E7CRI0</accession>
  <accession>E7CRI7</accession>
  <accession>E7CRJ1</accession>
  <accession>E7CRJ8</accession>
  <accession>E7CRK1</accession>
  <accession>E7CRK2</accession>
  <accession>E7CRK4</accession>
  <accession>E7CRK5</accession>
  <accession>E7CRK6</accession>
  <accession>E7CUG9</accession>
  <accession>E7CUH0</accession>
  <name>ALPS_PORTR</name>
  <protein>
    <recommendedName>
      <fullName evidence="6 7 9">Anti-lipopolysaccharide factor</fullName>
      <shortName evidence="6">PtALF</shortName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="10" type="primary">ALF</name>
  </gene>
  <organism>
    <name type="scientific">Portunus trituberculatus</name>
    <name type="common">Swimming crab</name>
    <name type="synonym">Neptunus trituberculatus</name>
    <dbReference type="NCBI Taxonomy" id="210409"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Eucarida</taxon>
      <taxon>Decapoda</taxon>
      <taxon>Pleocyemata</taxon>
      <taxon>Brachyura</taxon>
      <taxon>Eubrachyura</taxon>
      <taxon>Portunoidea</taxon>
      <taxon>Portunidae</taxon>
      <taxon>Portuninae</taxon>
      <taxon>Portunus</taxon>
    </lineage>
  </organism>
  <reference evidence="8 9" key="1">
    <citation type="journal article" date="2010" name="Comp. Biochem. Physiol." volume="156" first="77" last="85">
      <title>Molecular cloning, characterization and mRNA expression of two antibacterial peptides: crustin and anti-lipopolysaccharide factor in swimming crab Portunus trituberculatus.</title>
      <authorList>
        <person name="Yue F."/>
        <person name="Pan L."/>
        <person name="Miao J."/>
        <person name="Zhang L."/>
        <person name="Li J."/>
      </authorList>
      <dbReference type="PubMed" id="20167286"/>
      <dbReference type="DOI" id="10.1016/j.cbpb.2010.02.003"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 1)</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION</scope>
    <source>
      <tissue evidence="4">Hemocyte</tissue>
    </source>
  </reference>
  <reference evidence="8 10" key="2">
    <citation type="journal article" date="2011" name="Fish Shellfish Immunol." volume="30" first="583" last="591">
      <title>Three isoforms of anti-lipopolysaccharide factor identified from eyestalk cDNA library of swimming crab Portunus trituberculatus.</title>
      <authorList>
        <person name="Liu Y."/>
        <person name="Cui Z."/>
        <person name="Luan W."/>
        <person name="Song C."/>
        <person name="Nie Q."/>
        <person name="Wang S."/>
        <person name="Li Q."/>
      </authorList>
      <dbReference type="PubMed" id="21168510"/>
      <dbReference type="DOI" id="10.1016/j.fsi.2010.12.005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA] (ISOFORMS 1 AND 2)</scope>
    <scope>ALTERNATIVE SPLICING</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION</scope>
    <source>
      <tissue evidence="5">Eyestalk</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">May bind to bacterial LPS and thus specifically inhibit the LPS-mediated activation of the hemolymph coagulation. It has a strong antibacterial effect especially on the growth of Gram-negative bacteria (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>C0KJQ4-1</id>
      <name evidence="5">1</name>
      <name evidence="7">PtesALF3</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>C0KJQ4-2</id>
      <name evidence="5">2</name>
      <name evidence="7">PtesALF1</name>
      <sequence type="described" ref="VSP_041458"/>
    </isoform>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4 5">Isoform 1 is highly expressed in muscle and stomach, moderately in heart and gill and at lower levels in hemocytes and hepatopancreas. Isoform 2 is mainly expressed in gill, hepatopancreas, muscle and eyestalk.</text>
  </comment>
  <comment type="induction">
    <text evidence="4 5">By injection with live V.alginolyticus. Isoform 2 expression is highest 24 hours post-injection. Expression of isoform 1 and isoform 2 is reduced at 3 hours post-injection, increases to a peak at 12 hours post-injection, and returns to control levels at 32 hours post-injection (PubMed:21168510). Isoform 1 expression is up-regulated at 3 and 24 hours post-injection and reaches a peak at 48 hours post-injection (PubMed:20167286).</text>
  </comment>
  <comment type="miscellaneous">
    <molecule>Isoform 1</molecule>
    <text evidence="5">Major isoform.</text>
  </comment>
  <comment type="miscellaneous">
    <molecule>Isoform 2</molecule>
    <text evidence="5">Minor isoform detected in approximately 30% of the cDNA clones. May be produced at very low levels due to a premature stop codon in the mRNA, leading to nonsense-mediated mRNA decay.</text>
  </comment>
  <comment type="sequence caution" evidence="8">
    <conflict type="frameshift">
      <sequence resource="EMBL-CDS" id="ADU25043" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="FJ612108">
    <property type="protein sequence ID" value="ACM89169.2"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="GQ165621">
    <property type="protein sequence ID" value="ACS45385.2"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536671">
    <property type="protein sequence ID" value="ADU25060.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536672">
    <property type="protein sequence ID" value="ADU25061.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536673">
    <property type="protein sequence ID" value="ADU25062.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536674">
    <property type="protein sequence ID" value="ADU25063.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536675">
    <property type="protein sequence ID" value="ADU25064.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536676">
    <property type="protein sequence ID" value="ADU25065.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536677">
    <property type="protein sequence ID" value="ADU25066.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536678">
    <property type="protein sequence ID" value="ADU25067.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536679">
    <property type="protein sequence ID" value="ADU25068.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536680">
    <property type="protein sequence ID" value="ADU25069.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536681">
    <property type="protein sequence ID" value="ADU25070.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536682">
    <property type="protein sequence ID" value="ADU25071.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536683">
    <property type="protein sequence ID" value="ADU25072.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536684">
    <property type="protein sequence ID" value="ADU25073.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536685">
    <property type="protein sequence ID" value="ADU25074.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536686">
    <property type="protein sequence ID" value="ADU25075.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536687">
    <property type="protein sequence ID" value="ADU25076.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536688">
    <property type="protein sequence ID" value="ADU25077.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536689">
    <property type="protein sequence ID" value="ADU25078.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536690">
    <property type="protein sequence ID" value="ADU25079.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536691">
    <property type="protein sequence ID" value="ADU25080.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536692">
    <property type="protein sequence ID" value="ADU25081.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536693">
    <property type="protein sequence ID" value="ADU25082.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536694">
    <property type="protein sequence ID" value="ADU25083.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536695">
    <property type="protein sequence ID" value="ADU25084.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536696">
    <property type="protein sequence ID" value="ADU25085.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536697">
    <property type="protein sequence ID" value="ADU25086.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536698">
    <property type="protein sequence ID" value="ADU25087.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536699">
    <property type="protein sequence ID" value="ADU25088.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM536700">
    <property type="protein sequence ID" value="ADU25089.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM627757">
    <property type="protein sequence ID" value="ADU25042.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="HM627758">
    <property type="protein sequence ID" value="ADU25043.1"/>
    <property type="status" value="ALT_FRAME"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="C0KJQ4"/>
  <dbReference type="SMR" id="C0KJQ4"/>
  <dbReference type="GlyCosmos" id="C0KJQ4">
    <property type="glycosylation" value="1 site, No reported glycans"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_045276085.1">
    <molecule id="C0KJQ4-1"/>
    <property type="protein sequence ID" value="XP_045132020.1"/>
    <property type="gene ID" value="LOC123516576"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_045276086.1">
    <molecule id="C0KJQ4-1"/>
    <property type="protein sequence ID" value="XP_045132021.1"/>
    <property type="gene ID" value="LOC123516576"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_045276087.1">
    <molecule id="C0KJQ4-1"/>
    <property type="protein sequence ID" value="XP_045132022.1"/>
    <property type="gene ID" value="LOC123516576"/>
  </dbReference>
  <dbReference type="OrthoDB" id="3030096at2759"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.160.320">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR024509">
    <property type="entry name" value="Anti-LPS_factor/Scygonadin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038539">
    <property type="entry name" value="Anti-LPS_factor/Scygonadin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11630">
    <property type="entry name" value="Anti-LPS-SCYG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000410488" description="Anti-lipopolysaccharide factor" evidence="3">
    <location>
      <begin position="27"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="3">
    <location>
      <position position="45"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="55"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_041458" description="In isoform 2." evidence="7">
    <original>SRTKSRSGSSREATKDFVRKALQNGLVTQQDASLWLNN</original>
    <variation>CKYCVVF</variation>
    <location>
      <begin position="86"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25082." evidence="5" ref="2">
    <original>HN</original>
    <variation>RS</variation>
    <location>
      <begin position="44"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25086." evidence="5" ref="2">
    <original>S</original>
    <variation>P</variation>
    <location>
      <position position="47"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25082." evidence="5" ref="2">
    <original>F</original>
    <variation>L</variation>
    <location>
      <position position="50"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25085." evidence="5" ref="2">
    <original>C</original>
    <variation>Y</variation>
    <location>
      <position position="55"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25086." evidence="5" ref="2">
    <original>F</original>
    <variation>S</variation>
    <location>
      <position position="66"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25083." evidence="5" ref="2">
    <original>K</original>
    <variation>R</variation>
    <location>
      <position position="73"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25086." evidence="5" ref="2">
    <original>F</original>
    <variation>L</variation>
    <location>
      <position position="74"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25079." evidence="5" ref="2">
    <original>C</original>
    <variation>R</variation>
    <location>
      <position position="76"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25079." evidence="5" ref="2">
    <original>W</original>
    <variation>R</variation>
    <location>
      <position position="79"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25087." evidence="5" ref="2">
    <original>K</original>
    <variation>E</variation>
    <location>
      <position position="100"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25072." evidence="5" ref="2">
    <original>V</original>
    <variation>A</variation>
    <location>
      <position position="103"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25068." evidence="5" ref="2">
    <original>Q</original>
    <variation>L</variation>
    <location>
      <position position="115"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25061." evidence="5" ref="2">
    <location>
      <begin position="120"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ADU25087." evidence="5" ref="2">
    <original>L</original>
    <variation>R</variation>
    <location>
      <position position="121"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="B5TTX7"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P07086"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="20167286"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="21168510"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="20167286"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="21168510"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <evidence type="ECO:0000312" key="9">
    <source>
      <dbReference type="EMBL" id="ACM89169.2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="10">
    <source>
      <dbReference type="EMBL" id="ACS45385.2"/>
    </source>
  </evidence>
  <sequence length="123" mass="14109" checksum="B8389D0A9CC4BBFC" modified="2009-09-22" version="2" precursor="true">MRKGVVAGLCLALVVMCLYLPQPCEAQYEALVTSILGKLTGLWHNDSVDFMGHICYFRRRPKIRRFKLYHEGKFWCPGWAPFEGRSRTKSRSGSSREATKDFVRKALQNGLVTQQDASLWLNN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>