<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-04-14" modified="2024-11-27" version="81" xmlns="http://uniprot.org/uniprot">
  <accession>B2IQW9</accession>
  <name>ATPE_STRPS</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">ATP synthase epsilon chain</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">ATP synthase F1 sector epsilon subunit</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">F-ATPase epsilon subunit</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">atpC</name>
    <name type="ordered locus">SPCG_1491</name>
  </gene>
  <organism>
    <name type="scientific">Streptococcus pneumoniae (strain CGSP14)</name>
    <dbReference type="NCBI Taxonomy" id="516950"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Streptococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="BMC Genomics" volume="10" first="158" last="158">
      <title>Genome evolution driven by host adaptations results in a more virulent and antimicrobial-resistant Streptococcus pneumoniae serotype 14.</title>
      <authorList>
        <person name="Ding F."/>
        <person name="Tang P."/>
        <person name="Hsu M.-H."/>
        <person name="Cui P."/>
        <person name="Hu S."/>
        <person name="Yu J."/>
        <person name="Chiu C.-H."/>
      </authorList>
      <dbReference type="PubMed" id="19361343"/>
      <dbReference type="DOI" id="10.1186/1471-2164-10-158"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CGSP14</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Produces ATP from ADP in the presence of a proton gradient across the membrane.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">F-type ATPases have 2 components, CF(1) - the catalytic core - and CF(0) - the membrane proton channel. CF(1) has five subunits: alpha(3), beta(3), gamma(1), delta(1), epsilon(1). CF(0) has three main subunits: a, b and c.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cell membrane</location>
      <topology evidence="1">Peripheral membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the ATPase epsilon chain family.</text>
  </comment>
  <dbReference type="EMBL" id="CP001033">
    <property type="protein sequence ID" value="ACB90743.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000068050.1">
    <property type="nucleotide sequence ID" value="NC_010582.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B2IQW9"/>
  <dbReference type="SMR" id="B2IQW9"/>
  <dbReference type="GeneID" id="66806597"/>
  <dbReference type="KEGG" id="spw:SPCG_1491"/>
  <dbReference type="HOGENOM" id="CLU_084338_1_0_9"/>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045261">
    <property type="term" value="C:proton-transporting ATP synthase complex, catalytic core F(1)"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005524">
    <property type="term" value="F:ATP binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046933">
    <property type="term" value="F:proton-transporting ATP synthase activity, rotational mechanism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="CDD" id="cd12152">
    <property type="entry name" value="F1-ATPase_delta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.5.440:FF:000001">
    <property type="entry name" value="ATP synthase epsilon chain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.5.440">
    <property type="entry name" value="ATP synthase delta/epsilon subunit, C-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.15.10">
    <property type="entry name" value="F0F1 ATP synthase delta/epsilon subunit, N-terminal"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00530">
    <property type="entry name" value="ATP_synth_epsil_bac"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001469">
    <property type="entry name" value="ATP_synth_F1_dsu/esu"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020546">
    <property type="entry name" value="ATP_synth_F1_dsu/esu_N"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020547">
    <property type="entry name" value="ATP_synth_F1_esu_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036771">
    <property type="entry name" value="ATPsynth_dsu/esu_N"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01216">
    <property type="entry name" value="ATP_synt_epsi"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13822">
    <property type="entry name" value="ATP SYNTHASE DELTA/EPSILON CHAIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13822:SF10">
    <property type="entry name" value="ATP SYNTHASE EPSILON CHAIN, CHLOROPLASTIC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00401">
    <property type="entry name" value="ATP-synt_DE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02823">
    <property type="entry name" value="ATP-synt_DE_N"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF51344">
    <property type="entry name" value="Epsilon subunit of F1F0-ATP synthase N-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0066">ATP synthesis</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0139">CF(1)</keyword>
  <keyword id="KW-0375">Hydrogen ion transport</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_1000127899" description="ATP synthase epsilon chain">
    <location>
      <begin position="1"/>
      <end position="139"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00530"/>
    </source>
  </evidence>
  <sequence length="139" mass="15638" checksum="972D40440D879172" modified="2008-06-10" version="1">MAQLTVQIVTPDGLVYDHHASYVSVRTLDGEMGILPRHENMIAVLAVDEVKVKRIDDKDHVNWIAVNGGVIEIANDMITIVADSAERARDIDISRAERAKLRAERAIEEAQDKHLIDQERRAKIALQRAINRINVGNRL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>