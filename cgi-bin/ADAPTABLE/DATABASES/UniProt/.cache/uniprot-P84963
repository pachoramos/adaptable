<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-15" modified="2024-03-27" version="32" xmlns="http://uniprot.org/uniprot">
  <accession>P84963</accession>
  <name>DEF1_TRIKH</name>
  <protein>
    <recommendedName>
      <fullName>Defensin Tk-AMP-D1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Triticum kiharae</name>
    <name type="common">Wheat</name>
    <dbReference type="NCBI Taxonomy" id="376535"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>Liliopsida</taxon>
      <taxon>Poales</taxon>
      <taxon>Poaceae</taxon>
      <taxon>BOP clade</taxon>
      <taxon>Pooideae</taxon>
      <taxon>Triticodae</taxon>
      <taxon>Triticeae</taxon>
      <taxon>Triticinae</taxon>
      <taxon>Triticum</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2007" name="Biochimie" volume="89" first="605" last="612">
      <title>Seed defensins from T. kiharae and related species: Genome localization of defensin-encoding genes.</title>
      <authorList>
        <person name="Odintsova T.I."/>
        <person name="Egorov T.A."/>
        <person name="Musolyamov A.K."/>
        <person name="Odintsova M.S."/>
        <person name="Pukhalsky V.A."/>
        <person name="Grishin E.V."/>
      </authorList>
      <dbReference type="PubMed" id="17321030"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2006.09.009"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="2">Seed</tissue>
    </source>
  </reference>
  <reference evidence="4" key="2">
    <citation type="journal article" date="2008" name="Biochimie" volume="90" first="1667" last="1673">
      <title>Seed defensins of barnyard grass Echinochloa crusgalli (L.) Beauv.</title>
      <authorList>
        <person name="Odintsova T.I."/>
        <person name="Rogozhin E.A."/>
        <person name="Baranov Y."/>
        <person name="Musolyamov A.K."/>
        <person name="Yalpani N."/>
        <person name="Egorov T.A."/>
        <person name="Grishin E.V."/>
      </authorList>
      <dbReference type="PubMed" id="18625284"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2008.06.007"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Has weak antifungal activity against F.graminearum and F.verticillioides below 30 ug/ml, but not against A.consortiale B.cinerea, H.sativum, F.culmorum, C.graminicola and D.maydis.</text>
  </comment>
  <comment type="mass spectrometry" mass="5735.0" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84963"/>
  <dbReference type="SMR" id="P84963"/>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008176">
    <property type="entry name" value="Defensin_plant"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33147">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33147:SF31">
    <property type="entry name" value="KNOTTIN SCORPION TOXIN-LIKE DOMAIN-CONTAINING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00304">
    <property type="entry name" value="Gamma-thionin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00288">
    <property type="entry name" value="PUROTHIONIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00940">
    <property type="entry name" value="GAMMA_THIONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="chain" id="PRO_0000287889" description="Defensin Tk-AMP-D1">
    <location>
      <begin position="1"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="3"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="14"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="20"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="45"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q8GTM0"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="17321030"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18625284"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="49" mass="5744" checksum="0EF1F14B6134A8BA" modified="2007-05-15" version="1">RTCQSQSHKFKGACFSDTNCDSVCRTENFPRGQCNQHHVERKCYCERDC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>