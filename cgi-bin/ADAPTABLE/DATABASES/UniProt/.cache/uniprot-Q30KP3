<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-10-14" modified="2024-10-02" version="100" xmlns="http://uniprot.org/uniprot">
  <accession>Q30KP3</accession>
  <accession>Q8C5A7</accession>
  <name>DFB20_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 20</fullName>
      <shortName>BD-20</shortName>
      <shortName>mBD-20</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 20</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Defb20</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Physiol. Genomics" volume="23" first="5" last="17">
      <title>Cross-species analysis of the mammalian beta-defensin gene family: presence of syntenic gene clusters and preferential expression in the male reproductive tract.</title>
      <authorList>
        <person name="Patil A.A."/>
        <person name="Cai Y."/>
        <person name="Sang Y."/>
        <person name="Blecha F."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="16033865"/>
      <dbReference type="DOI" id="10.1152/physiolgenomics.00104.2005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Science" volume="309" first="1559" last="1563">
      <title>The transcriptional landscape of the mammalian genome.</title>
      <authorList>
        <person name="Carninci P."/>
        <person name="Kasukawa T."/>
        <person name="Katayama S."/>
        <person name="Gough J."/>
        <person name="Frith M.C."/>
        <person name="Maeda N."/>
        <person name="Oyama R."/>
        <person name="Ravasi T."/>
        <person name="Lenhard B."/>
        <person name="Wells C."/>
        <person name="Kodzius R."/>
        <person name="Shimokawa K."/>
        <person name="Bajic V.B."/>
        <person name="Brenner S.E."/>
        <person name="Batalov S."/>
        <person name="Forrest A.R."/>
        <person name="Zavolan M."/>
        <person name="Davis M.J."/>
        <person name="Wilming L.G."/>
        <person name="Aidinis V."/>
        <person name="Allen J.E."/>
        <person name="Ambesi-Impiombato A."/>
        <person name="Apweiler R."/>
        <person name="Aturaliya R.N."/>
        <person name="Bailey T.L."/>
        <person name="Bansal M."/>
        <person name="Baxter L."/>
        <person name="Beisel K.W."/>
        <person name="Bersano T."/>
        <person name="Bono H."/>
        <person name="Chalk A.M."/>
        <person name="Chiu K.P."/>
        <person name="Choudhary V."/>
        <person name="Christoffels A."/>
        <person name="Clutterbuck D.R."/>
        <person name="Crowe M.L."/>
        <person name="Dalla E."/>
        <person name="Dalrymple B.P."/>
        <person name="de Bono B."/>
        <person name="Della Gatta G."/>
        <person name="di Bernardo D."/>
        <person name="Down T."/>
        <person name="Engstrom P."/>
        <person name="Fagiolini M."/>
        <person name="Faulkner G."/>
        <person name="Fletcher C.F."/>
        <person name="Fukushima T."/>
        <person name="Furuno M."/>
        <person name="Futaki S."/>
        <person name="Gariboldi M."/>
        <person name="Georgii-Hemming P."/>
        <person name="Gingeras T.R."/>
        <person name="Gojobori T."/>
        <person name="Green R.E."/>
        <person name="Gustincich S."/>
        <person name="Harbers M."/>
        <person name="Hayashi Y."/>
        <person name="Hensch T.K."/>
        <person name="Hirokawa N."/>
        <person name="Hill D."/>
        <person name="Huminiecki L."/>
        <person name="Iacono M."/>
        <person name="Ikeo K."/>
        <person name="Iwama A."/>
        <person name="Ishikawa T."/>
        <person name="Jakt M."/>
        <person name="Kanapin A."/>
        <person name="Katoh M."/>
        <person name="Kawasawa Y."/>
        <person name="Kelso J."/>
        <person name="Kitamura H."/>
        <person name="Kitano H."/>
        <person name="Kollias G."/>
        <person name="Krishnan S.P."/>
        <person name="Kruger A."/>
        <person name="Kummerfeld S.K."/>
        <person name="Kurochkin I.V."/>
        <person name="Lareau L.F."/>
        <person name="Lazarevic D."/>
        <person name="Lipovich L."/>
        <person name="Liu J."/>
        <person name="Liuni S."/>
        <person name="McWilliam S."/>
        <person name="Madan Babu M."/>
        <person name="Madera M."/>
        <person name="Marchionni L."/>
        <person name="Matsuda H."/>
        <person name="Matsuzawa S."/>
        <person name="Miki H."/>
        <person name="Mignone F."/>
        <person name="Miyake S."/>
        <person name="Morris K."/>
        <person name="Mottagui-Tabar S."/>
        <person name="Mulder N."/>
        <person name="Nakano N."/>
        <person name="Nakauchi H."/>
        <person name="Ng P."/>
        <person name="Nilsson R."/>
        <person name="Nishiguchi S."/>
        <person name="Nishikawa S."/>
        <person name="Nori F."/>
        <person name="Ohara O."/>
        <person name="Okazaki Y."/>
        <person name="Orlando V."/>
        <person name="Pang K.C."/>
        <person name="Pavan W.J."/>
        <person name="Pavesi G."/>
        <person name="Pesole G."/>
        <person name="Petrovsky N."/>
        <person name="Piazza S."/>
        <person name="Reed J."/>
        <person name="Reid J.F."/>
        <person name="Ring B.Z."/>
        <person name="Ringwald M."/>
        <person name="Rost B."/>
        <person name="Ruan Y."/>
        <person name="Salzberg S.L."/>
        <person name="Sandelin A."/>
        <person name="Schneider C."/>
        <person name="Schoenbach C."/>
        <person name="Sekiguchi K."/>
        <person name="Semple C.A."/>
        <person name="Seno S."/>
        <person name="Sessa L."/>
        <person name="Sheng Y."/>
        <person name="Shibata Y."/>
        <person name="Shimada H."/>
        <person name="Shimada K."/>
        <person name="Silva D."/>
        <person name="Sinclair B."/>
        <person name="Sperling S."/>
        <person name="Stupka E."/>
        <person name="Sugiura K."/>
        <person name="Sultana R."/>
        <person name="Takenaka Y."/>
        <person name="Taki K."/>
        <person name="Tammoja K."/>
        <person name="Tan S.L."/>
        <person name="Tang S."/>
        <person name="Taylor M.S."/>
        <person name="Tegner J."/>
        <person name="Teichmann S.A."/>
        <person name="Ueda H.R."/>
        <person name="van Nimwegen E."/>
        <person name="Verardo R."/>
        <person name="Wei C.L."/>
        <person name="Yagi K."/>
        <person name="Yamanishi H."/>
        <person name="Zabarovsky E."/>
        <person name="Zhu S."/>
        <person name="Zimmer A."/>
        <person name="Hide W."/>
        <person name="Bult C."/>
        <person name="Grimmond S.M."/>
        <person name="Teasdale R.D."/>
        <person name="Liu E.T."/>
        <person name="Brusic V."/>
        <person name="Quackenbush J."/>
        <person name="Wahlestedt C."/>
        <person name="Mattick J.S."/>
        <person name="Hume D.A."/>
        <person name="Kai C."/>
        <person name="Sasaki D."/>
        <person name="Tomaru Y."/>
        <person name="Fukuda S."/>
        <person name="Kanamori-Katayama M."/>
        <person name="Suzuki M."/>
        <person name="Aoki J."/>
        <person name="Arakawa T."/>
        <person name="Iida J."/>
        <person name="Imamura K."/>
        <person name="Itoh M."/>
        <person name="Kato T."/>
        <person name="Kawaji H."/>
        <person name="Kawagashira N."/>
        <person name="Kawashima T."/>
        <person name="Kojima M."/>
        <person name="Kondo S."/>
        <person name="Konno H."/>
        <person name="Nakano K."/>
        <person name="Ninomiya N."/>
        <person name="Nishio T."/>
        <person name="Okada M."/>
        <person name="Plessy C."/>
        <person name="Shibata K."/>
        <person name="Shiraki T."/>
        <person name="Suzuki S."/>
        <person name="Tagami M."/>
        <person name="Waki K."/>
        <person name="Watahiki A."/>
        <person name="Okamura-Oho Y."/>
        <person name="Suzuki H."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
      </authorList>
      <dbReference type="PubMed" id="16141072"/>
      <dbReference type="DOI" id="10.1126/science.1112014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
      <tissue>Epididymis</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2009" name="PLoS Biol." volume="7" first="E1000112" last="E1000112">
      <title>Lineage-specific biology revealed by a finished genome assembly of the mouse.</title>
      <authorList>
        <person name="Church D.M."/>
        <person name="Goodstadt L."/>
        <person name="Hillier L.W."/>
        <person name="Zody M.C."/>
        <person name="Goldstein S."/>
        <person name="She X."/>
        <person name="Bult C.J."/>
        <person name="Agarwala R."/>
        <person name="Cherry J.L."/>
        <person name="DiCuccio M."/>
        <person name="Hlavina W."/>
        <person name="Kapustin Y."/>
        <person name="Meric P."/>
        <person name="Maglott D."/>
        <person name="Birtle Z."/>
        <person name="Marques A.C."/>
        <person name="Graves T."/>
        <person name="Zhou S."/>
        <person name="Teague B."/>
        <person name="Potamousis K."/>
        <person name="Churas C."/>
        <person name="Place M."/>
        <person name="Herschleb J."/>
        <person name="Runnheim R."/>
        <person name="Forrest D."/>
        <person name="Amos-Landgraf J."/>
        <person name="Schwartz D.C."/>
        <person name="Cheng Z."/>
        <person name="Lindblad-Toh K."/>
        <person name="Eichler E.E."/>
        <person name="Ponting C.P."/>
      </authorList>
      <dbReference type="PubMed" id="19468303"/>
      <dbReference type="DOI" id="10.1371/journal.pbio.1000112"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <comment type="sequence caution" evidence="3">
    <conflict type="erroneous initiation">
      <sequence resource="EMBL-CDS" id="BAC37500" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="DQ012031">
    <property type="protein sequence ID" value="AAY59767.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK079000">
    <property type="protein sequence ID" value="BAC37500.1"/>
    <property type="status" value="ALT_INIT"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL844517">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS38275.1"/>
  <dbReference type="RefSeq" id="NP_795924.2">
    <property type="nucleotide sequence ID" value="NM_176950.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q30KP3"/>
  <dbReference type="SMR" id="Q30KP3"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000105462"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000105462"/>
  <dbReference type="ProteomicsDB" id="279520"/>
  <dbReference type="Antibodypedia" id="82036">
    <property type="antibodies" value="1 antibodies from 1 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="319579"/>
  <dbReference type="Ensembl" id="ENSMUST00000109836.3">
    <property type="protein sequence ID" value="ENSMUSP00000105462.3"/>
    <property type="gene ID" value="ENSMUSG00000049560.5"/>
  </dbReference>
  <dbReference type="GeneID" id="319579"/>
  <dbReference type="KEGG" id="mmu:319579"/>
  <dbReference type="UCSC" id="uc008nfm.1">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:2442320"/>
  <dbReference type="CTD" id="319579"/>
  <dbReference type="MGI" id="MGI:2442320">
    <property type="gene designation" value="Defb20"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000049560"/>
  <dbReference type="eggNOG" id="ENOG502TM18">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00530000064329"/>
  <dbReference type="HOGENOM" id="CLU_2399087_0_0_1"/>
  <dbReference type="InParanoid" id="Q30KP3"/>
  <dbReference type="OMA" id="ISEMGCL"/>
  <dbReference type="OrthoDB" id="4735829at2759"/>
  <dbReference type="PhylomeDB" id="Q30KP3"/>
  <dbReference type="BioGRID-ORCS" id="319579">
    <property type="hits" value="2 hits in 76 CRISPR screens"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q30KP3"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="RNAct" id="Q30KP3">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000049560">
    <property type="expression patterns" value="Expressed in epiblast cell in embryo and 3 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050544">
    <property type="entry name" value="Beta-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025933">
    <property type="entry name" value="Beta_defensin_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001:SF5">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001">
    <property type="entry name" value="BETA-DEFENSIN 123-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13841">
    <property type="entry name" value="Defensin_beta_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000352705" description="Beta-defensin 20">
    <location>
      <begin position="22"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="36"/>
      <end position="53"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="96" mass="11039" checksum="7D2EC83872FB0FAA" modified="2005-12-06" version="1" precursor="true">MKLLQVLLVLLFVALADGAQPKRCFSNVEGYCRKKCRLVEISEMGCLHGKYCCVNELENKKHKKHSVVEETVKLQDKSKVQDYMILPTVTYYTISI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>