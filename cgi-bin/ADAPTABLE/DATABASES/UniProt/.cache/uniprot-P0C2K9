<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-03-06" modified="2023-05-03" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>P0C2K9</accession>
  <name>BX8_LOXGA</name>
  <protein>
    <recommendedName>
      <fullName>Dermonecrotic toxin LgSicTox-beta-LOXN8</fullName>
      <ecNumber evidence="5">4.6.1.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Phospholipase D</fullName>
      <shortName>PLD</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Sphingomyelin phosphodiesterase D</fullName>
      <shortName>SMD</shortName>
      <shortName>SMase D</shortName>
      <shortName>Sphingomyelinase D</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Loxosceles gaucho</name>
    <name type="common">Spider</name>
    <dbReference type="NCBI Taxonomy" id="58216"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Haplogynae</taxon>
      <taxon>Scytodoidea</taxon>
      <taxon>Sicariidae</taxon>
      <taxon>Loxosceles</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Proteomics" volume="5" first="2167" last="2176">
      <title>Proteome analysis of brown spider venom: identification of loxnecrogin isoforms in Loxosceles gaucho venom.</title>
      <authorList>
        <person name="Machado L.F."/>
        <person name="Laugesen S."/>
        <person name="Botelho E.D."/>
        <person name="Ricart C.A.O."/>
        <person name="Fontes W."/>
        <person name="Barbaro K.C."/>
        <person name="Roepstorff P."/>
        <person name="Sousa M.V."/>
      </authorList>
      <dbReference type="PubMed" id="15852345"/>
      <dbReference type="DOI" id="10.1002/pmic.200401096"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 4">Dermonecrotic toxins cleave the phosphodiester linkage between the phosphate and headgroup of certain phospholipids (sphingolipid and lysolipid substrates), forming an alcohol (often choline) and a cyclic phosphate (By similarity). This toxin acts on sphingomyelin (SM) (By similarity). It may also act on ceramide phosphoethanolamine (CPE), lysophosphatidylcholine (LPC) and lysophosphatidylethanolamine (LPE), but not on lysophosphatidylserine (LPS), and lysophosphatidylglycerol (LPG) (By similarity). It acts by transphosphatidylation, releasing exclusively cyclic phosphate products as second products (By similarity). Induces dermonecrosis, hemolysis, increased vascular permeability, edema, inflammatory response, and platelet aggregation (By similarity).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>an N-(acyl)-sphingosylphosphocholine = an N-(acyl)-sphingosyl-1,3-cyclic phosphate + choline</text>
      <dbReference type="Rhea" id="RHEA:60652"/>
      <dbReference type="ChEBI" id="CHEBI:15354"/>
      <dbReference type="ChEBI" id="CHEBI:64583"/>
      <dbReference type="ChEBI" id="CHEBI:143892"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>an N-(acyl)-sphingosylphosphoethanolamine = an N-(acyl)-sphingosyl-1,3-cyclic phosphate + ethanolamine</text>
      <dbReference type="Rhea" id="RHEA:60648"/>
      <dbReference type="ChEBI" id="CHEBI:57603"/>
      <dbReference type="ChEBI" id="CHEBI:143891"/>
      <dbReference type="ChEBI" id="CHEBI:143892"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>a 1-acyl-sn-glycero-3-phosphocholine = a 1-acyl-sn-glycero-2,3-cyclic phosphate + choline</text>
      <dbReference type="Rhea" id="RHEA:60700"/>
      <dbReference type="ChEBI" id="CHEBI:15354"/>
      <dbReference type="ChEBI" id="CHEBI:58168"/>
      <dbReference type="ChEBI" id="CHEBI:143947"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>a 1-acyl-sn-glycero-3-phosphoethanolamine = a 1-acyl-sn-glycero-2,3-cyclic phosphate + ethanolamine</text>
      <dbReference type="Rhea" id="RHEA:60704"/>
      <dbReference type="ChEBI" id="CHEBI:57603"/>
      <dbReference type="ChEBI" id="CHEBI:64381"/>
      <dbReference type="ChEBI" id="CHEBI:143947"/>
    </reaction>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="6">
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </cofactor>
    <text evidence="6">Binds 1 Mg(2+) ion per subunit.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="7">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="9">Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Contains 2 disulfide bonds.</text>
  </comment>
  <comment type="similarity">
    <text evidence="8">Belongs to the arthropod phospholipase D family. Class II subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="2 3 5">The most common activity assay for dermonecrotic toxins detects enzymatic activity by monitoring choline release from substrate. Liberation of choline from sphingomyelin (SM) or lysophosphatidylcholine (LPC) is commonly assumed to result from substrate hydrolysis, giving either ceramide-1-phosphate (C1P) or lysophosphatidic acid (LPA), respectively, as a second product. However, two studies from Lajoie and colleagues (2013 and 2015) report the observation of exclusive formation of cyclic phosphate products as second products, resulting from intramolecular transphosphatidylation. Cyclic phosphates have vastly different biological properties from their monoester counterparts, and they may be relevant to the pathology of brown spider envenomation.</text>
  </comment>
  <dbReference type="EC" id="4.6.1.-" evidence="5"/>
  <dbReference type="ArachnoServer" id="AS000156">
    <property type="toxin name" value="Sphingomyelinase D (LOXN8) (N-terminal fragment)"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016829">
    <property type="term" value="F:lyase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016042">
    <property type="term" value="P:lipid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-1061">Dermonecrotic toxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0442">Lipid degradation</keyword>
  <keyword id="KW-0443">Lipid metabolism</keyword>
  <keyword id="KW-0456">Lyase</keyword>
  <keyword id="KW-0460">Magnesium</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000279566" description="Dermonecrotic toxin LgSicTox-beta-LOXN8">
    <location>
      <begin position="1"/>
      <end position="9" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="A0A0D4WTV1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="A0A0D4WV12"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="4">
    <source>
      <dbReference type="UniProtKB" id="P0CE80"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="5">
    <source>
      <dbReference type="UniProtKB" id="Q4ZFU2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="6">
    <source>
      <dbReference type="UniProtKB" id="Q8I914"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="15852345"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <evidence type="ECO:0000305" key="9">
    <source>
      <dbReference type="PubMed" id="15852345"/>
    </source>
  </evidence>
  <sequence length="9" mass="1085" checksum="84467047632415BA" modified="2007-03-06" version="1" fragment="single">ADSRKPIWI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>