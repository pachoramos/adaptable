<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="156" xmlns="http://uniprot.org/uniprot">
  <accession>P01844</accession>
  <name>LAC2_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Ig lambda-2 chain C region</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">Iglc2</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1982" name="Proc. Natl. Acad. Sci. U.S.A." volume="79" first="4681" last="4685">
      <title>Evolution of mouse immunoglobulin lambda genes.</title>
      <authorList>
        <person name="Selsing E."/>
        <person name="Miller J."/>
        <person name="Wilson R."/>
        <person name="Storb U."/>
      </authorList>
      <dbReference type="PubMed" id="6812053"/>
      <dbReference type="DOI" id="10.1073/pnas.79.15.4681"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1982" name="Nucleic Acids Res." volume="10" first="3831" last="3843">
      <title>Nucleotide sequence of a chromosomal rearranged lambda 2 immunoglobulin gene of mouse.</title>
      <authorList>
        <person name="Wu G.E."/>
        <person name="Govindji N."/>
        <person name="Hozumi N."/>
        <person name="Murialdo H."/>
      </authorList>
      <dbReference type="PubMed" id="6287422"/>
      <dbReference type="DOI" id="10.1093/nar/10.13.3831"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1982" name="Nature" volume="298" first="380" last="382">
      <title>Somatic variants of murine immunoglobulin lambda light chains.</title>
      <authorList>
        <person name="Bothwell A.L.M."/>
        <person name="Paskind M."/>
        <person name="Reth M."/>
        <person name="Imanishi-Kari T."/>
        <person name="Rajewsky K."/>
        <person name="Baltimore D."/>
      </authorList>
      <dbReference type="PubMed" id="6283385"/>
      <dbReference type="DOI" id="10.1038/298380a0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] (MOPC 315)</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1973" name="Biochemistry" volume="12" first="5400" last="5416">
      <title>Amino acid sequence of the light chain of a mouse myeloma protein (MOPC-315).</title>
      <authorList>
        <person name="Dugan E.S."/>
        <person name="Bradshaw R.A."/>
        <person name="Simms E.S."/>
        <person name="Eisen H.N."/>
      </authorList>
      <dbReference type="PubMed" id="4760498"/>
    </citation>
    <scope>PROTEIN SEQUENCE (MOPC 315)</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1981" name="Proc. Natl. Acad. Sci. U.S.A." volume="78" first="569" last="573">
      <title>Identification of a third type of lambda light chain in mouse immunoglobulins.</title>
      <authorList>
        <person name="Azuma T."/>
        <person name="Steiner L.A."/>
        <person name="Eisen H.N."/>
      </authorList>
      <dbReference type="PubMed" id="6165998"/>
      <dbReference type="DOI" id="10.1073/pnas.78.1.569"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 66-104 (MOPC 315)</scope>
    <scope>SEQUENCE REVISION</scope>
  </reference>
  <dbReference type="EMBL" id="J00595">
    <property type="protein sequence ID" value="AAA39151.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="C93922">
    <property type="entry name" value="L2MS"/>
  </dbReference>
  <dbReference type="PDB" id="2QHR">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="L=1-102"/>
  </dbReference>
  <dbReference type="PDBsum" id="2QHR"/>
  <dbReference type="AlphaFoldDB" id="P01844"/>
  <dbReference type="SMR" id="P01844"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000100464"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000100464"/>
  <dbReference type="PeptideAtlas" id="P01844"/>
  <dbReference type="ProteomicsDB" id="264905"/>
  <dbReference type="Ensembl" id="ENSMUST00000103749.3">
    <property type="protein sequence ID" value="ENSMUSP00000100464.3"/>
    <property type="gene ID" value="ENSMUSG00000076937.4"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:99547"/>
  <dbReference type="MGI" id="MGI:99547">
    <property type="gene designation" value="Iglc2"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000076937"/>
  <dbReference type="eggNOG" id="ENOG502TFGA">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000153307"/>
  <dbReference type="InParanoid" id="P01844"/>
  <dbReference type="OMA" id="WKGNDVT"/>
  <dbReference type="PhylomeDB" id="P01844"/>
  <dbReference type="Reactome" id="R-MMU-166663">
    <property type="pathway name" value="Initial triggering of complement"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-173623">
    <property type="pathway name" value="Classical antibody-mediated complement activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-198933">
    <property type="pathway name" value="Immunoregulatory interactions between a Lymphoid and a non-Lymphoid cell"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-202733">
    <property type="pathway name" value="Cell surface interactions at the vascular wall"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2029481">
    <property type="pathway name" value="FCGR activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2029482">
    <property type="pathway name" value="Regulation of actin dynamics for phagocytic cup formation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2029485">
    <property type="pathway name" value="Role of phospholipids in phagocytosis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2168880">
    <property type="pathway name" value="Scavenging of heme from plasma"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2454202">
    <property type="pathway name" value="Fc epsilon receptor (FCERI) signaling"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2730905">
    <property type="pathway name" value="Role of LAT2/NTAL/LAB on calcium mobilization"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2871796">
    <property type="pathway name" value="FCERI mediated MAPK activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2871809">
    <property type="pathway name" value="FCERI mediated Ca+2 mobilization"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2871837">
    <property type="pathway name" value="FCERI mediated NF-kB activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-5690714">
    <property type="pathway name" value="CD22 mediated BCR regulation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-977606">
    <property type="pathway name" value="Regulation of Complement cascade"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-983695">
    <property type="pathway name" value="Antigen activates B Cell Receptor (BCR) leading to generation of second messengers"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P01844"/>
  <dbReference type="PRO" id="PR:P01844"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 16"/>
  </dbReference>
  <dbReference type="RNAct" id="P01844">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000076937">
    <property type="expression patterns" value="Expressed in spleen and 60 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P01844">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="CDD" id="cd07699">
    <property type="entry name" value="IgC1_L"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.40.10:FF:000283">
    <property type="entry name" value="Immunoglobulin kappa constant"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003006">
    <property type="entry name" value="Ig/MHC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003597">
    <property type="entry name" value="Ig_C1-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050160">
    <property type="entry name" value="MHC/Immunoglobulin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR19944:SF98">
    <property type="entry name" value="IG-LIKE DOMAIN-CONTAINING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR19944">
    <property type="entry name" value="MHC CLASS II-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07654">
    <property type="entry name" value="C1-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00407">
    <property type="entry name" value="IGc1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00290">
    <property type="entry name" value="IG_MHC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0393">Immunoglobulin domain</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000153609" description="Ig lambda-2 chain C region">
    <location>
      <begin position="1" status="less than"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="domain" description="Ig-like">
    <location>
      <begin position="6"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="27"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (with heavy chain)">
    <location>
      <position position="103"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="strand" evidence="1">
    <location>
      <begin position="7"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="helix" evidence="1">
    <location>
      <begin position="15"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="strand" evidence="1">
    <location>
      <begin position="22"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="strand" evidence="1">
    <location>
      <begin position="38"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="strand" evidence="1">
    <location>
      <begin position="46"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="strand" evidence="1">
    <location>
      <begin position="52"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="strand" evidence="1">
    <location>
      <begin position="58"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="strand" evidence="1">
    <location>
      <begin position="63"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="helix" evidence="1">
    <location>
      <begin position="74"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="strand" evidence="1">
    <location>
      <begin position="83"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="strand" evidence="1">
    <location>
      <begin position="92"/>
      <end position="98"/>
    </location>
  </feature>
  <evidence type="ECO:0007829" key="1">
    <source>
      <dbReference type="PDB" id="2QHR"/>
    </source>
  </evidence>
  <sequence length="104" mass="11255" checksum="CE4B67B8688862D3" modified="1986-07-21" version="1">QPKSTPTLTVFPPSSEELKENKATLVCLISNFSPSGVTVAWKANGTPITQGVDTSNPTKEGNKFMASSFLHLTSDQWRSHNSFTCQVTHEGDTVEKSLSPAECL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>