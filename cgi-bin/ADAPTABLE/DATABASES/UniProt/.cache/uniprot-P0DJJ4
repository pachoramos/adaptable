<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-07-11" modified="2023-02-22" version="20" xmlns="http://uniprot.org/uniprot">
  <accession>P0DJJ4</accession>
  <name>VM3_NAJOX</name>
  <protein>
    <recommendedName>
      <fullName>Zinc metalloproteinase oxiagin</fullName>
      <ecNumber>3.4.24.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Snake venom metalloprotease</fullName>
      <shortName>SVMP</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Naja oxiana</name>
    <name type="common">Central Asian cobra</name>
    <name type="synonym">Oxus cobra</name>
    <dbReference type="NCBI Taxonomy" id="8657"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Elapidae</taxon>
      <taxon>Elapinae</taxon>
      <taxon>Naja</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Mol. Immunol." volume="42" first="1141" last="1153">
      <title>Oxiagin from the Naja oxiana cobra venom is the first reprolysin inhibiting the classical pathway of complement.</title>
      <authorList>
        <person name="Shoibonov B.B."/>
        <person name="Osipov A.V."/>
        <person name="Kryukova E.V."/>
        <person name="Zinchenko A.A."/>
        <person name="Lakhtin V.M."/>
        <person name="Tsetlin V.I."/>
        <person name="Utkin Y.N."/>
      </authorList>
      <dbReference type="PubMed" id="15829304"/>
      <dbReference type="DOI" id="10.1016/j.molimm.2004.11.009"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Snake venom metalloproteinase that inhibits the classical complement pathway dose-dependently. It acts by binding to carbohydrates of IgG within the antibody-sensitized sheep erythrocytes (EA) complex, and thus prevents interaction of component C2 with immobilized C4b. Also induces cation-independent hemagglutination that can be prevented by D-galactose pretreatment.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="1">
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </cofactor>
    <text evidence="1">Binds 1 zinc ion per subunit.</text>
  </comment>
  <comment type="subunit">
    <text evidence="5">Heterotrimer; disulfide-linked. The heterotrimer consists of 1 metalloproteinase chain and 2 lectin chains (Probable).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text>N-glycosylated.</text>
  </comment>
  <comment type="mass spectrometry" mass="49800.0" method="MALDI" evidence="3"/>
  <comment type="miscellaneous">
    <text evidence="5">Negative results: does not show proteolytic activity on rabbit IgG, human C3 and C4 complement components, fibrinogen, beta-casein, hemoglobin, ferritin, myoglobin, chymotrypsinogen, and melittin.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the venom metalloproteinase (M12B) family. P-III subfamily. P-IIId sub-subfamily.</text>
  </comment>
  <dbReference type="EC" id="3.4.24.-"/>
  <dbReference type="AlphaFoldDB" id="P0DJJ4"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008237">
    <property type="term" value="F:metallopeptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006508">
    <property type="term" value="P:proteolysis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1217">Cell adhesion impairing toxin</keyword>
  <keyword id="KW-1216">Complement system impairing toxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0482">Metalloprotease</keyword>
  <keyword id="KW-0645">Protease</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <feature type="chain" id="PRO_0000418194" description="Zinc metalloproteinase oxiagin">
    <location>
      <begin position="1"/>
      <end position="22" status="greater than"/>
    </location>
  </feature>
  <feature type="domain" description="Peptidase M12B" evidence="2">
    <location>
      <begin position="14"/>
      <end position="22" status="greater than"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="8"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="14"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="22"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00276"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15829304"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="15829304"/>
    </source>
  </evidence>
  <sequence length="22" mass="2640" checksum="17B5542D28899940" modified="2012-07-11" version="1" fragment="single">TNTPEQQCYLQAKCYIEFYVVV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>