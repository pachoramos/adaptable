<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-13" modified="2024-11-27" version="122" xmlns="http://uniprot.org/uniprot">
  <accession>Q8MKD0</accession>
  <name>CCL5_HORSE</name>
  <protein>
    <recommendedName>
      <fullName>C-C motif chemokine 5</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Small-inducible cytokine A5</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>T-cell-specific protein RANTES</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CCL5</name>
    <name type="synonym">SCYA5</name>
  </gene>
  <organism>
    <name type="scientific">Equus caballus</name>
    <name type="common">Horse</name>
    <dbReference type="NCBI Taxonomy" id="9796"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Perissodactyla</taxon>
      <taxon>Equidae</taxon>
      <taxon>Equus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2002-04" db="EMBL/GenBank/DDBJ databases">
      <title>Equus caballus RANTES mRNA.</title>
      <authorList>
        <person name="Takafuji V.A."/>
        <person name="Sharova L.V."/>
        <person name="Crisman M.V."/>
        <person name="Howard R.D."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Chemoattractant for blood monocytes, memory T-helper cells and eosinophils. Causes the release of histamine from basophils and activates eosinophils. May activate several chemokine receptors including CCR1, CCR3, CCR4 and CCR5. May also be an agonist of the G protein-coupled receptor GPR75. Together with GPR75, may play a role in neuron survival through activation of a downstream signaling pathway involving the PI3, Akt and MAP kinases. By activating GPR75 may also play a role in insulin secretion by islet cells.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the intercrine beta (chemokine CC) family.</text>
  </comment>
  <dbReference type="EMBL" id="AF506970">
    <property type="protein sequence ID" value="AAM34212.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001075332.1">
    <property type="nucleotide sequence ID" value="NM_001081863.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8MKD0"/>
  <dbReference type="SMR" id="Q8MKD0"/>
  <dbReference type="STRING" id="9796.ENSECAP00000022430"/>
  <dbReference type="PaxDb" id="9796-ENSECAP00000022430"/>
  <dbReference type="GeneID" id="100033925"/>
  <dbReference type="KEGG" id="ecb:100033925"/>
  <dbReference type="CTD" id="6352"/>
  <dbReference type="HOGENOM" id="CLU_141716_4_2_1"/>
  <dbReference type="InParanoid" id="Q8MKD0"/>
  <dbReference type="OMA" id="HIQEYFY"/>
  <dbReference type="OrthoDB" id="4265193at2759"/>
  <dbReference type="TreeFam" id="TF334888"/>
  <dbReference type="Proteomes" id="UP000002281">
    <property type="component" value="Chromosome 11"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSECAG00000024888">
    <property type="expression patterns" value="Expressed in leukocyte and 20 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q8MKD0">
    <property type="expression patterns" value="baseline"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048020">
    <property type="term" value="F:CCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060326">
    <property type="term" value="P:cell chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007186">
    <property type="term" value="P:G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050796">
    <property type="term" value="P:regulation of insulin secretion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00272">
    <property type="entry name" value="Chemokine_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000002">
    <property type="entry name" value="C-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000827">
    <property type="entry name" value="Chemokine_CC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF170">
    <property type="entry name" value="C-C MOTIF CHEMOKINE 5"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00472">
    <property type="entry name" value="SMALL_CYTOKINES_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005174" description="C-C motif chemokine 5">
    <location>
      <begin position="24"/>
      <end position="91"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="33"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="34"/>
      <end position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P13501"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="91" mass="10159" checksum="AEB253E8CD4ED7FD" modified="2002-10-01" version="1" precursor="true">MKVFAAALAVILATATFCTPASASPYASDTTPCCFAYISRPLPRAHIQEYFYTSSKCSIPAVVFVTRKKRQVCANPEKKWVREYINTLEMS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>