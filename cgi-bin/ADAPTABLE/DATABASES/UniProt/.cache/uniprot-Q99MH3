<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-06-20" modified="2024-07-24" version="114" xmlns="http://uniprot.org/uniprot">
  <accession>Q99MH3</accession>
  <name>HEPC_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Hepcidin</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">Hamp</name>
    <name type="synonym">Hepc</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="J. Biol. Chem." volume="276" first="7811" last="7819">
      <title>A new mouse liver-specific gene, encoding a protein homologous to human antimicrobial peptide hepcidin, is overexpressed during iron overload.</title>
      <authorList>
        <person name="Pigeon C."/>
        <person name="Ilyin G."/>
        <person name="Courselaud B."/>
        <person name="Leroyer P."/>
        <person name="Turlin B."/>
        <person name="Brissot P."/>
        <person name="Loreal O."/>
      </authorList>
      <dbReference type="PubMed" id="11113132"/>
      <dbReference type="DOI" id="10.1074/jbc.m008923200"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>Sprague-Dawley</strain>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Liver-produced hormone that constitutes the main circulating regulator of iron absorption and distribution across tissues. Acts by promoting endocytosis and degradation of ferroportin/SLC40A1, leading to the retention of iron in iron-exporting cells and decreased flow of iron into plasma. Controls the major flows of iron into plasma: absorption of dietary iron in the intestine, recycling of iron by macrophages, which phagocytose old erythrocytes and other cells, and mobilization of stored iron from hepatocytes.</text>
  </comment>
  <comment type="function">
    <text evidence="2">Has strong antimicrobial activity against E.coli ML35P N.cinerea and weaker against S.epidermidis, S.aureus and group b streptococcus bacteria. Active against the fungus C.albicans. No activity against P.aeruginosa.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Interacts with SLC40A1; this interaction promotes SLC40A1 rapid ubiquitination.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the hepcidin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF344185">
    <property type="protein sequence ID" value="AAK12966.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_445921.1">
    <property type="nucleotide sequence ID" value="NM_053469.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q99MH3"/>
  <dbReference type="SMR" id="Q99MH3"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000028545"/>
  <dbReference type="PhosphoSitePlus" id="Q99MH3"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000028545"/>
  <dbReference type="GeneID" id="84604"/>
  <dbReference type="KEGG" id="rno:84604"/>
  <dbReference type="UCSC" id="RGD:70971">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:70971"/>
  <dbReference type="CTD" id="57817"/>
  <dbReference type="RGD" id="70971">
    <property type="gene designation" value="Hamp"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSRNOG00000021029"/>
  <dbReference type="eggNOG" id="ENOG502T0FU">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_2557737_0_0_1"/>
  <dbReference type="InParanoid" id="Q99MH3"/>
  <dbReference type="OMA" id="PICLFCC"/>
  <dbReference type="OrthoDB" id="4846842at2759"/>
  <dbReference type="PhylomeDB" id="Q99MH3"/>
  <dbReference type="TreeFam" id="TF330932"/>
  <dbReference type="PRO" id="PR:Q99MH3"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000021029">
    <property type="expression patterns" value="Expressed in liver and 19 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045179">
    <property type="term" value="C:apical cortex"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0014704">
    <property type="term" value="C:intercalated disc"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005507">
    <property type="term" value="F:copper ion binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006953">
    <property type="term" value="P:acute-phase response"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007259">
    <property type="term" value="P:cell surface receptor signaling pathway via JAK-STAT"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1903413">
    <property type="term" value="P:cellular response to bile acid"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071354">
    <property type="term" value="P:cellular response to interleukin-6"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071356">
    <property type="term" value="P:cellular response to tumor necrosis factor"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071481">
    <property type="term" value="P:cellular response to X-ray"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051649">
    <property type="term" value="P:establishment of localization in cell"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006879">
    <property type="term" value="P:intracellular iron ion homeostasis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034755">
    <property type="term" value="P:iron ion transmembrane transport"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097421">
    <property type="term" value="P:liver regeneration"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042116">
    <property type="term" value="P:macrophage activation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060586">
    <property type="term" value="P:multicellular organismal-level iron ion homeostasis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002262">
    <property type="term" value="P:myeloid cell homeostasis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045779">
    <property type="term" value="P:negative regulation of bone resorption"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050728">
    <property type="term" value="P:negative regulation of inflammatory response"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1904479">
    <property type="term" value="P:negative regulation of intestinal absorption"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034760">
    <property type="term" value="P:negative regulation of iron ion transmembrane transport"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000122">
    <property type="term" value="P:negative regulation of transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061051">
    <property type="term" value="P:positive regulation of cell growth involved in cardiac muscle cell development"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043032">
    <property type="term" value="P:positive regulation of macrophage activation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045732">
    <property type="term" value="P:positive regulation of protein catabolic process"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045944">
    <property type="term" value="P:positive regulation of transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030163">
    <property type="term" value="P:protein catabolic process"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0036017">
    <property type="term" value="P:response to erythropoietin"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045471">
    <property type="term" value="P:response to ethanol"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010039">
    <property type="term" value="P:response to iron ion"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990641">
    <property type="term" value="P:response to iron ion starvation"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033189">
    <property type="term" value="P:response to vitamin A"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010043">
    <property type="term" value="P:response to zinc ion"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006366">
    <property type="term" value="P:transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010500">
    <property type="entry name" value="Hepcidin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16877">
    <property type="entry name" value="HEPCIDIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16877:SF0">
    <property type="entry name" value="HEPCIDIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06446">
    <property type="entry name" value="Hepcidin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000013385" evidence="3">
    <location>
      <begin position="24"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000013386" description="Hepcidin">
    <location>
      <begin position="60"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="66"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="69"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="70"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="73"/>
      <end position="81"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P81172"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="84" mass="9286" checksum="65E0D5FE4F44A0B0" modified="2001-06-01" version="1" precursor="true">MALSTRIQAACLLLLLLASLSSGAYLRQQTRQTTALQPWHGAESKTDDSALLMLKRRKRDTNFPICLFCCKCCKNSSCGLCCIT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>