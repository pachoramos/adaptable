<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-05-08" modified="2022-05-25" version="6" xmlns="http://uniprot.org/uniprot">
  <accession>P0DSJ7</accession>
  <name>TX14B_MYRGU</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">U-myrmeciitoxin(01)-Mg4b</fullName>
      <shortName evidence="2">MIITX(01)-Mg4b</shortName>
      <shortName evidence="3">U-MIITX(01)-Mg4b</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Myrmecia gulosa</name>
    <name type="common">Red bulldog ant</name>
    <dbReference type="NCBI Taxonomy" id="36170"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Myrmeciinae</taxon>
      <taxon>Myrmeciini</taxon>
      <taxon>Myrmecia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2018" name="Sci. Adv." volume="4" first="EAAU4640" last="EAAU4640">
      <title>A comprehensive portrait of the venom of the giant red bull ant, Myrmecia gulosa, reveals a hyperdiverse hymenopteran toxin gene family.</title>
      <authorList>
        <person name="Robinson S.D."/>
        <person name="Mueller A."/>
        <person name="Clayton D."/>
        <person name="Starobova H."/>
        <person name="Hamilton B.R."/>
        <person name="Payne R.J."/>
        <person name="Vetter I."/>
        <person name="King G.F."/>
        <person name="Undheim E.A.B."/>
      </authorList>
      <dbReference type="PubMed" id="30214940"/>
      <dbReference type="DOI" id="10.1126/sciadv.aau4640"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">May have antimicrobial properties, like most ant linear peptides.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer; disulfide-linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">Contains 2 intrachain disulfide bonds (per chain) and 1 interchain disulfide bond.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="4">Not detected in the venom.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the ant myrmeciitoxin-01 family.</text>
  </comment>
  <comment type="online information" name="National Center for Biotechnology Information (NCBI)">
    <link uri="https://www.ncbi.nlm.nih.gov/nuccore/GGFG01000005"/>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DSJ7"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000447074" description="U-myrmeciitoxin(01)-Mg4b" evidence="1">
    <location>
      <begin position="26"/>
      <end position="64"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P0DPU9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="30214940"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="30214940"/>
    </source>
  </evidence>
  <sequence length="64" mass="7190" checksum="C075D0BF40337CA8" modified="2019-05-08" version="1" precursor="true">MGKIFFFVLMIAIIGSTFLIEEALGSLVGCPRPNFLPSWNRCKCICKNNKPMCRKLPNLLKTTA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>