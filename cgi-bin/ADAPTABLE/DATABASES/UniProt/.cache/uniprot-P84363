<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-15" modified="2019-12-11" version="31" xmlns="http://uniprot.org/uniprot">
  <accession>P84363</accession>
  <name>PPK6_DERER</name>
  <protein>
    <recommendedName>
      <fullName>Pyrokinin-6</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>FXPRL-amide</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Deropeltis erythrocephala</name>
    <name type="common">Black velvet roach</name>
    <dbReference type="NCBI Taxonomy" id="303918"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Dictyoptera</taxon>
      <taxon>Blattodea</taxon>
      <taxon>Blattoidea</taxon>
      <taxon>Blattidae</taxon>
      <taxon>Blattinae</taxon>
      <taxon>Deropeltis</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2005" name="Peptides" volume="26" first="3" last="9">
      <title>Peptidomics of neurohemal organs from species of the cockroach family Blattidae: how do neuropeptides of closely related species differ?</title>
      <authorList>
        <person name="Predel R."/>
        <person name="Gaede G."/>
      </authorList>
      <dbReference type="PubMed" id="15626499"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2004.10.010"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-14</scope>
    <source>
      <tissue evidence="3">Corpora allata</tissue>
    </source>
  </reference>
  <reference evidence="5" key="2">
    <citation type="submission" date="2004-11" db="UniProtKB">
      <authorList>
        <person name="Predel R."/>
        <person name="Gaede G."/>
      </authorList>
    </citation>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Myoactive.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed in the brain, subesophageal ganglion and in the retrocerebral complex (mainly corpora cardiaca).</text>
  </comment>
  <comment type="mass spectrometry" mass="1586.8" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the pyrokinin family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001484">
    <property type="entry name" value="Pyrokinin_CS"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00539">
    <property type="entry name" value="PYROKININ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044355" description="Pyrokinin-6">
    <location>
      <begin position="1"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="3">
    <location>
      <position position="14"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P82693"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15626499"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="14" mass="1588" checksum="3966CC3C3DC1A998" modified="2005-02-15" version="1">SDPEVPGMWFGPRL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>