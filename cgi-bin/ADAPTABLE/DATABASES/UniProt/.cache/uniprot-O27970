<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-05-10" modified="2024-11-27" version="115" xmlns="http://uniprot.org/uniprot">
  <accession>O27970</accession>
  <name>OGT_ARCFU</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Methylated-DNA--protein-cysteine methyltransferase</fullName>
      <ecNumber evidence="1">2.1.1.63</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">6-O-methylguanine-DNA methyltransferase</fullName>
      <shortName evidence="1">MGMT</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">O-6-methylguanine-DNA-alkyltransferase</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">ogt</name>
    <name type="ordered locus">AF_2314</name>
  </gene>
  <organism>
    <name type="scientific">Archaeoglobus fulgidus (strain ATCC 49558 / DSM 4304 / JCM 9628 / NBRC 100126 / VC-16)</name>
    <dbReference type="NCBI Taxonomy" id="224325"/>
    <lineage>
      <taxon>Archaea</taxon>
      <taxon>Euryarchaeota</taxon>
      <taxon>Archaeoglobi</taxon>
      <taxon>Archaeoglobales</taxon>
      <taxon>Archaeoglobaceae</taxon>
      <taxon>Archaeoglobus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Nature" volume="390" first="364" last="370">
      <title>The complete genome sequence of the hyperthermophilic, sulphate-reducing archaeon Archaeoglobus fulgidus.</title>
      <authorList>
        <person name="Klenk H.-P."/>
        <person name="Clayton R.A."/>
        <person name="Tomb J.-F."/>
        <person name="White O."/>
        <person name="Nelson K.E."/>
        <person name="Ketchum K.A."/>
        <person name="Dodson R.J."/>
        <person name="Gwinn M.L."/>
        <person name="Hickey E.K."/>
        <person name="Peterson J.D."/>
        <person name="Richardson D.L."/>
        <person name="Kerlavage A.R."/>
        <person name="Graham D.E."/>
        <person name="Kyrpides N.C."/>
        <person name="Fleischmann R.D."/>
        <person name="Quackenbush J."/>
        <person name="Lee N.H."/>
        <person name="Sutton G.G."/>
        <person name="Gill S.R."/>
        <person name="Kirkness E.F."/>
        <person name="Dougherty B.A."/>
        <person name="McKenney K."/>
        <person name="Adams M.D."/>
        <person name="Loftus B.J."/>
        <person name="Peterson S.N."/>
        <person name="Reich C.I."/>
        <person name="McNeil L.K."/>
        <person name="Badger J.H."/>
        <person name="Glodek A."/>
        <person name="Zhou L."/>
        <person name="Overbeek R."/>
        <person name="Gocayne J.D."/>
        <person name="Weidman J.F."/>
        <person name="McDonald L.A."/>
        <person name="Utterback T.R."/>
        <person name="Cotton M.D."/>
        <person name="Spriggs T."/>
        <person name="Artiach P."/>
        <person name="Kaine B.P."/>
        <person name="Sykes S.M."/>
        <person name="Sadow P.W."/>
        <person name="D'Andrea K.P."/>
        <person name="Bowman C."/>
        <person name="Fujii C."/>
        <person name="Garland S.A."/>
        <person name="Mason T.M."/>
        <person name="Olsen G.J."/>
        <person name="Fraser C.M."/>
        <person name="Smith H.O."/>
        <person name="Woese C.R."/>
        <person name="Venter J.C."/>
      </authorList>
      <dbReference type="PubMed" id="9389475"/>
      <dbReference type="DOI" id="10.1038/37052"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 49558 / DSM 4304 / JCM 9628 / NBRC 100126 / VC-16</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Involved in the cellular defense against the biological effects of O6-methylguanine (O6-MeG) and O4-methylthymine (O4-MeT) in DNA. Repairs the methylated nucleobase in DNA by stoichiometrically transferring the methyl group to a cysteine residue in the enzyme. This is a suicide reaction: the enzyme is irreversibly inactivated.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>a 6-O-methyl-2'-deoxyguanosine in DNA + L-cysteinyl-[protein] = S-methyl-L-cysteinyl-[protein] + a 2'-deoxyguanosine in DNA</text>
      <dbReference type="Rhea" id="RHEA:24000"/>
      <dbReference type="Rhea" id="RHEA-COMP:10131"/>
      <dbReference type="Rhea" id="RHEA-COMP:10132"/>
      <dbReference type="Rhea" id="RHEA-COMP:11367"/>
      <dbReference type="Rhea" id="RHEA-COMP:11368"/>
      <dbReference type="ChEBI" id="CHEBI:29950"/>
      <dbReference type="ChEBI" id="CHEBI:82612"/>
      <dbReference type="ChEBI" id="CHEBI:85445"/>
      <dbReference type="ChEBI" id="CHEBI:85448"/>
      <dbReference type="EC" id="2.1.1.63"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>a 4-O-methyl-thymidine in DNA + L-cysteinyl-[protein] = a thymidine in DNA + S-methyl-L-cysteinyl-[protein]</text>
      <dbReference type="Rhea" id="RHEA:53428"/>
      <dbReference type="Rhea" id="RHEA-COMP:10131"/>
      <dbReference type="Rhea" id="RHEA-COMP:10132"/>
      <dbReference type="Rhea" id="RHEA-COMP:13555"/>
      <dbReference type="Rhea" id="RHEA-COMP:13556"/>
      <dbReference type="ChEBI" id="CHEBI:29950"/>
      <dbReference type="ChEBI" id="CHEBI:82612"/>
      <dbReference type="ChEBI" id="CHEBI:137386"/>
      <dbReference type="ChEBI" id="CHEBI:137387"/>
      <dbReference type="EC" id="2.1.1.63"/>
    </reaction>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text>This enzyme catalyzes only one turnover and therefore is not strictly catalytic. According to one definition, an enzyme is a biocatalyst that acts repeatedly and over many reaction cycles.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the MGMT family.</text>
  </comment>
  <dbReference type="EC" id="2.1.1.63" evidence="1"/>
  <dbReference type="EMBL" id="AE000782">
    <property type="protein sequence ID" value="AAB88942.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="B69539">
    <property type="entry name" value="B69539"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O27970"/>
  <dbReference type="SMR" id="O27970"/>
  <dbReference type="STRING" id="224325.AF_2314"/>
  <dbReference type="PaxDb" id="224325-AF_2314"/>
  <dbReference type="EnsemblBacteria" id="AAB88942">
    <property type="protein sequence ID" value="AAB88942"/>
    <property type="gene ID" value="AF_2314"/>
  </dbReference>
  <dbReference type="KEGG" id="afu:AF_2314"/>
  <dbReference type="eggNOG" id="arCOG02724">
    <property type="taxonomic scope" value="Archaea"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_000445_52_2_2"/>
  <dbReference type="OrthoDB" id="372118at2157"/>
  <dbReference type="PhylomeDB" id="O27970"/>
  <dbReference type="BRENDA" id="2.1.1.63">
    <property type="organism ID" value="414"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000002199">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003908">
    <property type="term" value="F:methylated-DNA-[protein]-cysteine S-methyltransferase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006307">
    <property type="term" value="P:DNA alkylation repair"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032259">
    <property type="term" value="P:methylation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd06445">
    <property type="entry name" value="ATase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.10.10">
    <property type="entry name" value="Winged helix-like DNA-binding domain superfamily/Winged helix DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00772">
    <property type="entry name" value="OGT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001497">
    <property type="entry name" value="MethylDNA_cys_MeTrfase_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR014048">
    <property type="entry name" value="MethylDNA_cys_MeTrfase_DNA-bd"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036217">
    <property type="entry name" value="MethylDNA_cys_MeTrfase_DNAb"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023546">
    <property type="entry name" value="MGMT"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036388">
    <property type="entry name" value="WH-like_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00589">
    <property type="entry name" value="ogt"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10815">
    <property type="entry name" value="METHYLATED-DNA--PROTEIN-CYSTEINE METHYLTRANSFERASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10815:SF13">
    <property type="entry name" value="METHYLATED-DNA--PROTEIN-CYSTEINE METHYLTRANSFERASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01035">
    <property type="entry name" value="DNA_binding_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46767">
    <property type="entry name" value="Methylated DNA-protein cysteine methyltransferase, C-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00374">
    <property type="entry name" value="MGMT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0227">DNA damage</keyword>
  <keyword id="KW-0234">DNA repair</keyword>
  <keyword id="KW-0489">Methyltransferase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0808">Transferase</keyword>
  <feature type="chain" id="PRO_0000139375" description="Methylated-DNA--protein-cysteine methyltransferase">
    <location>
      <begin position="1"/>
      <end position="147"/>
    </location>
  </feature>
  <feature type="active site" description="Nucleophile; methyl group acceptor" evidence="1">
    <location>
      <position position="112"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00772"/>
    </source>
  </evidence>
  <sequence length="147" mass="16717" checksum="F223DA324732268C" modified="1998-01-01" version="1">MFSVKWGELYFNVVMEGGKAVKSYFSTYPSFSSSDSEYARQLERYFSGERVEVRIPYRLKASSFTRRVLEEVSRIPYGMVRMYSDIAKALNTSPRAVGQAVKRNPLPVIIPCHRVVGKKEIGGYTVSCSDIDGKSLKKRLLRLEGVF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>