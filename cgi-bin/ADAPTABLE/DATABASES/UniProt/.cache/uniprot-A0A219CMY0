<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-03-28" modified="2023-11-08" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>A0A219CMY0</accession>
  <name>BMNL5_BOMOR</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Bombinin-like peptides</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="3">Bombinin-BO1</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="3">Bombinin H-BO1</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name evidence="6" type="primary">BLP8-BH9</name>
  </gene>
  <organism evidence="6">
    <name type="scientific">Bombina orientalis</name>
    <name type="common">Oriental fire-bellied toad</name>
    <dbReference type="NCBI Taxonomy" id="8346"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Bombinatoridae</taxon>
      <taxon>Bombina</taxon>
    </lineage>
  </organism>
  <reference evidence="6" key="1">
    <citation type="journal article" date="2018" name="Amino Acids" volume="50" first="241" last="253">
      <title>Molecular characterization and bioactivity evaluation of two novel bombinin peptides from the skin secretion of Oriental fire-bellied toad, Bombina orientalis.</title>
      <authorList>
        <person name="Peng X."/>
        <person name="Zhou C."/>
        <person name="Hou X."/>
        <person name="Liu Y."/>
        <person name="Wang Z."/>
        <person name="Peng X."/>
        <person name="Zhang Z."/>
        <person name="Wang R."/>
        <person name="Kong D."/>
      </authorList>
      <dbReference type="PubMed" id="29098406"/>
      <dbReference type="DOI" id="10.1007/s00726-017-2509-z"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 44-68 AND 115-131</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT PHE-68 AND LEU-131</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <molecule>Bombinin-BO1</molecule>
    <text evidence="2">Has antimicrobial activity against Gram-negative bacterium E.coli (MIC=26.3 uM), Gram-positive bacterium S.aureus (MIC=26.3 uM) and yeast C.albicans (MIC=52.5 uM). Has moderate hemolytic activity towards human erythrocytes at a concentration of 52.2 uM.</text>
  </comment>
  <comment type="function">
    <molecule>Bombinin H-BO1</molecule>
    <text evidence="2">Has no antimicrobial activity at concentrations up to 161 uM. Has moderate hemolytic activity towards human erythrocytes at a concentration of 40.3 uM.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1 2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2437.89" method="Electrospray" evidence="2">
    <molecule>Bombinin-BO1</molecule>
    <text>Bombinin-BO1.</text>
  </comment>
  <comment type="mass spectrometry" mass="1589.02" method="Electrospray" evidence="2">
    <molecule>Bombinin H-BO1</molecule>
    <text>Bombinin H-BO1.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the bombinin family.</text>
  </comment>
  <dbReference type="EMBL" id="LT732575">
    <property type="protein sequence ID" value="SJN60137.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A219CMY0"/>
  <dbReference type="SMR" id="A0A219CMY0"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051715">
    <property type="term" value="P:cytolysis in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007962">
    <property type="entry name" value="Bombinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05298">
    <property type="entry name" value="Bombinin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000443728" evidence="4">
    <location>
      <begin position="19"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000443729" description="Bombinin-BO1" evidence="2">
    <location>
      <begin position="44"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000443730" evidence="4">
    <location>
      <begin position="72"/>
      <end position="112"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000443731" description="Bombinin H-BO1" evidence="2">
    <location>
      <begin position="115"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="2">
    <location>
      <position position="68"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="2">
    <location>
      <position position="131"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="29098406"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="29098406"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="29098406"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="SJN60137.1"/>
    </source>
  </evidence>
  <sequence length="132" mass="14598" checksum="01326222B8CB78FB" modified="2017-09-27" version="1" precursor="true">MNFKYIIAVSFLIASAYARSEEYDIQSLSQRDVLEEESLRKIRGIGSAILSAGKSIIKGLAKGLAEHFGKRTAEDHEVMKRLEAAMRDLDSLDYPEEASERETRGFNQEEKEKRIIGPVLGLVGKALGGLLG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>