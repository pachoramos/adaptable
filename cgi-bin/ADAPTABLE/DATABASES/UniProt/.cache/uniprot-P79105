<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-11-27" version="157" xmlns="http://uniprot.org/uniprot">
  <accession>P79105</accession>
  <accession>Q3T070</accession>
  <accession>Q9TR16</accession>
  <name>S10AC_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>Protein S100-A12</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Calcium-binding protein in amniotic fluid 1</fullName>
      <shortName>CAAF1</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Calgranulin-C</fullName>
      <shortName>CAGC</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Cornea-associated antigen</fullName>
      <shortName>CO-AG</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Extracellular newly identified RAGE-binding protein</fullName>
      <shortName>EN-RAGE</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>RAGE-binding protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>S100 calcium-binding protein A12</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">S100A12</name>
    <name type="synonym">CAAF1</name>
  </gene>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="J. Cell Sci." volume="109" first="805" last="815">
      <title>A novel calcium-binding protein in amniotic fluid, CAAF1: its molecular cloning and tissue distribution.</title>
      <authorList>
        <person name="Hitomi J."/>
        <person name="Yamaguchi K."/>
        <person name="Kikuchi Y."/>
        <person name="Kimura T."/>
        <person name="Maruyama K."/>
        <person name="Nagasaki K."/>
      </authorList>
      <dbReference type="PubMed" id="8718672"/>
      <dbReference type="DOI" id="10.1242/jcs.109.4.805"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Esophagus</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1999" name="Cell" volume="97" first="889" last="901">
      <title>RAGE mediates a novel proinflammatory axis: a central cell surface receptor for S100/calgranulin polypeptides.</title>
      <authorList>
        <person name="Hofmann M.A."/>
        <person name="Drury S."/>
        <person name="Fu C."/>
        <person name="Qu W."/>
        <person name="Taguchi A."/>
        <person name="Lu Y."/>
        <person name="Avila C."/>
        <person name="Kambham N."/>
        <person name="Bierhaus A."/>
        <person name="Nawroth P."/>
        <person name="Neurath M.F."/>
        <person name="Slattery T."/>
        <person name="Beach D."/>
        <person name="McClary J."/>
        <person name="Nagashima M."/>
        <person name="Morser J."/>
        <person name="Stern D."/>
        <person name="Schmidt A.M."/>
      </authorList>
      <dbReference type="PubMed" id="10399917"/>
      <dbReference type="DOI" id="10.1016/s0092-8674(00)80801-6"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 1-51 AND 66-83</scope>
    <scope>INTERACTION WITH AGER</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Lung</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="submission" date="2005-08" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="NIH - Mammalian Gene Collection (MGC) project"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Crossbred X Angus</strain>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1996" name="Invest. Ophthalmol. Vis. Sci." volume="37" first="944" last="948">
      <title>Amino acid sequence of an immunogenic corneal stromal protein.</title>
      <authorList>
        <person name="Liu S.H."/>
        <person name="Gottsch J.D."/>
      </authorList>
      <dbReference type="PubMed" id="8603881"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 2-71</scope>
    <source>
      <tissue>Corneal stroma</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">S100A12 is a calcium-, zinc- and copper-binding protein which plays a prominent role in the regulation of inflammatory processes and immune response. Its pro-inflammatory activity involves recruitment of leukocytes, promotion of cytokine and chemokine production, and regulation of leukocyte adhesion and migration. Acts as an alarmin or a danger associated molecular pattern (DAMP) molecule and stimulates innate immune cells via binding to receptor for advanced glycation endproducts (AGER). Binding to AGER activates the MAP-kinase and NF-kappa-B signaling pathways leading to production of pro-inflammatory cytokines and up-regulation of cell adhesion molecules ICAM1 and VCAM1. Acts as a monocyte and mast cell chemoattractant. Can stimulate mast cell degranulation and activation which generates chemokines, histamine and cytokines inducing further leukocyte recruitment to the sites of inflammation. Can inhibit the activity of matrix metalloproteinases; MMP2, MMP3 and MMP9 by chelating Zn(2+) from their active sites (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer. Homooligomer (tetramer or hexamer) in the presence of calcium, zinc and copper ions. Interacts with AGER and both calcium and zinc are essential for the interaction (By similarity). Interacts with CACYBP in a calcium-dependent manner (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
      <location evidence="1">Cytoskeleton</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cell membrane</location>
      <topology evidence="1">Peripheral membrane protein</topology>
    </subcellularLocation>
    <text evidence="1">Predominantly localized in the cytoplasm. Upon elevation of the intracellular calcium level, translocated from the cytoplasm to the cytoskeleton and the cell membrane. Upon neutrophil activation is secreted via a microtubule-mediated, alternative pathway (By similarity).</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Up-regulated in stimulated inflammatory effector cells.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The hinge domain contributes significantly to its chemotactic properties.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the S-100 family.</text>
  </comment>
  <dbReference type="EMBL" id="D49548">
    <property type="protein sequence ID" value="BAA08496.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF011757">
    <property type="protein sequence ID" value="AAB65423.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC102542">
    <property type="protein sequence ID" value="AAI02543.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_777076.1">
    <property type="nucleotide sequence ID" value="NM_174651.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_005203669.1">
    <property type="nucleotide sequence ID" value="XM_005203612.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_005203670.1">
    <property type="nucleotide sequence ID" value="XM_005203613.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P79105"/>
  <dbReference type="SMR" id="P79105"/>
  <dbReference type="BioGRID" id="159720">
    <property type="interactions" value="6"/>
  </dbReference>
  <dbReference type="STRING" id="9913.ENSBTAP00000034009"/>
  <dbReference type="PaxDb" id="9913-ENSBTAP00000056088"/>
  <dbReference type="PeptideAtlas" id="P79105"/>
  <dbReference type="GeneID" id="282467"/>
  <dbReference type="KEGG" id="bta:282467"/>
  <dbReference type="CTD" id="6283"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSBTAG00000012638"/>
  <dbReference type="eggNOG" id="ENOG502SA01">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_138624_6_2_1"/>
  <dbReference type="InParanoid" id="P79105"/>
  <dbReference type="OMA" id="HEHLHEV"/>
  <dbReference type="OrthoDB" id="4607525at2759"/>
  <dbReference type="Reactome" id="R-BTA-445989">
    <property type="pathway name" value="TAK1-dependent IKK and NF-kappa-B activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-6798695">
    <property type="pathway name" value="Neutrophil degranulation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-879415">
    <property type="pathway name" value="Advanced glycosylation endproduct receptor signaling"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-933542">
    <property type="pathway name" value="TRAF6 mediated NF-kB activation"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSBTAG00000012638">
    <property type="expression patterns" value="Expressed in anterior segment of eyeball and 102 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005856">
    <property type="term" value="C:cytoskeleton"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048306">
    <property type="term" value="F:calcium-dependent protein binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050786">
    <property type="term" value="F:RAGE receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043542">
    <property type="term" value="P:endothelial cell migration"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043303">
    <property type="term" value="P:mast cell degranulation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043123">
    <property type="term" value="P:positive regulation of canonical NF-kappaB signal transduction"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd05030">
    <property type="entry name" value="calgranulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.238.10:FF:000044">
    <property type="entry name" value="Protein S100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.238.10">
    <property type="entry name" value="EF-hand"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011992">
    <property type="entry name" value="EF-hand-dom_pair"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018247">
    <property type="entry name" value="EF_Hand_1_Ca_BS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002048">
    <property type="entry name" value="EF_hand_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001751">
    <property type="entry name" value="S100/CaBP7/8-like_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013787">
    <property type="entry name" value="S100_Ca-bd_sub"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11639:SF77">
    <property type="entry name" value="PROTEIN S100-A12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11639">
    <property type="entry name" value="S100 CALCIUM-BINDING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00036">
    <property type="entry name" value="EF-hand_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01023">
    <property type="entry name" value="S_100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00054">
    <property type="entry name" value="EFh"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01394">
    <property type="entry name" value="S_100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47473">
    <property type="entry name" value="EF-hand"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00018">
    <property type="entry name" value="EF_HAND_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50222">
    <property type="entry name" value="EF_HAND_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00303">
    <property type="entry name" value="S100_CABP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0186">Copper</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0206">Cytoskeleton</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <feature type="initiator methionine" description="Removed" evidence="5">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000144015" description="Protein S100-A12">
    <location>
      <begin position="2"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 1" evidence="6">
    <location>
      <begin position="13"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 2" evidence="3">
    <location>
      <begin position="49"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="region of interest" description="Hinge domain" evidence="1">
    <location>
      <begin position="38"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="16"/>
    </location>
    <ligand>
      <name>Cu cation</name>
      <dbReference type="ChEBI" id="CHEBI:23378"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="16"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="19"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>1</label>
      <note>low affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="24"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>1</label>
      <note>low affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="26"/>
    </location>
    <ligand>
      <name>Cu cation</name>
      <dbReference type="ChEBI" id="CHEBI:23378"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="26"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="27"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>1</label>
      <note>low affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="32"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>1</label>
      <note>low affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="62"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>2</label>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="64"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>2</label>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="66"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>2</label>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="73"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>2</label>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="86"/>
    </location>
    <ligand>
      <name>Cu cation</name>
      <dbReference type="ChEBI" id="CHEBI:23378"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="86"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="90"/>
    </location>
    <ligand>
      <name>Cu cation</name>
      <dbReference type="ChEBI" id="CHEBI:23378"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="90"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="6" ref="2">
    <original>R</original>
    <variation>Y</variation>
    <location>
      <position position="31"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="6" ref="2">
    <original>I</original>
    <variation>G</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 4; AA sequence." evidence="6" ref="4">
    <original>D</original>
    <variation>K</variation>
    <location>
      <position position="66"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 4; AA sequence." evidence="6" ref="4">
    <original>S</original>
    <variation>V</variation>
    <location>
      <position position="70"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P80511"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00448"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="10399917"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="8603881"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="92" mass="10685" checksum="060233882D7C901F" modified="2007-01-23" version="3">MTKLEDHLEGIINIFHQYSVRVGHFDTLNKRELKQLITKELPKTLQNTKDQPTIDKIFQDLDADKDGAVSFEEFVVLVSRVLKTAHIDIHKE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>