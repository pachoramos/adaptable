<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-09-13" modified="2024-11-27" version="73" xmlns="http://uniprot.org/uniprot">
  <accession>Q6EGX7</accession>
  <name>UBL5_MESAU</name>
  <protein>
    <recommendedName>
      <fullName>Ubiquitin-like protein 5</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Beacon protein</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">UBL5</name>
  </gene>
  <organism>
    <name type="scientific">Mesocricetus auratus</name>
    <name type="common">Golden hamster</name>
    <dbReference type="NCBI Taxonomy" id="10036"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Cricetidae</taxon>
      <taxon>Cricetinae</taxon>
      <taxon>Mesocricetus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2003-06" db="EMBL/GenBank/DDBJ databases">
      <title>Beacon in the golden hamster.</title>
      <authorList>
        <person name="Revel F."/>
        <person name="Simonneaux V."/>
        <person name="Sorensen B.H."/>
        <person name="Mikkelsen J.D."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Ubiquitin-like protein that plays a role in cell proliferation and sister chromatid cohesion by associating with spliceosomal proteins. Participates thereby in pre-mRNA splicing by maintaining spliceosome integrity. Promotes the functional integrity of the Fanconi anemia DNA repair pathway by interacting with FANCI component and subsequently mediating the formation of FANCI homodimers. Plays also a protective role against ER stress-induced apoptosis.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with CLK1, CLK3 and CLK4. Interacts with coilin/COIL. Interacts with spliceosome components SART1 and EFTUD2. Interacts with FANCI; this interaction promotes FANCI dimerization.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Nucleus</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EMBL" id="AY329082">
    <property type="protein sequence ID" value="AAQ99044.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001268879.1">
    <property type="nucleotide sequence ID" value="NM_001281950.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q6EGX7"/>
  <dbReference type="BMRB" id="Q6EGX7"/>
  <dbReference type="SMR" id="Q6EGX7"/>
  <dbReference type="STRING" id="10036.ENSMAUP00000001338"/>
  <dbReference type="GeneID" id="101836731"/>
  <dbReference type="KEGG" id="maua:101836731"/>
  <dbReference type="CTD" id="59286"/>
  <dbReference type="eggNOG" id="KOG3493">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="OrthoDB" id="4364at2759"/>
  <dbReference type="Proteomes" id="UP000189706">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031386">
    <property type="term" value="F:protein tag activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000398">
    <property type="term" value="P:mRNA splicing, via spliceosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0036211">
    <property type="term" value="P:protein modification process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd01791">
    <property type="entry name" value="Ubl_UBL5"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.20.90:FF:000052">
    <property type="entry name" value="Ubiquitin-like protein 5"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039732">
    <property type="entry name" value="Hub1/Ubl5"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000626">
    <property type="entry name" value="Ubiquitin-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029071">
    <property type="entry name" value="Ubiquitin-like_domsf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13042">
    <property type="entry name" value="UBIQUITIN-LIKE PROTEIN 5"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13042:SF3">
    <property type="entry name" value="UBIQUITIN-LIKE PROTEIN 5"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00240">
    <property type="entry name" value="ubiquitin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54236">
    <property type="entry name" value="Ubiquitin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0833">Ubl conjugation pathway</keyword>
  <feature type="chain" id="PRO_0000114867" description="Ubiquitin-like protein 5">
    <location>
      <begin position="1"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="domain" description="Ubiquitin-like">
    <location>
      <begin position="1"/>
      <end position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q9BZL1"/>
    </source>
  </evidence>
  <sequence length="73" mass="8547" checksum="D68BC941536AA39F" modified="2004-08-16" version="1">MIEVVCNDRLGKKVRVKCNTDDTIGDLKKLIAAQTGTRWNKIVLKKWYTIFKDHVSLGDYEIHDGMNLELYYQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>