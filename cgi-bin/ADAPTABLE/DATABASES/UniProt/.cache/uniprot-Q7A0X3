<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-11-28" modified="2024-11-27" version="106" xmlns="http://uniprot.org/uniprot">
  <accession>Q7A0X3</accession>
  <name>CSPA_STAAW</name>
  <protein>
    <recommendedName>
      <fullName>Cold shock protein CspA</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">cspA</name>
    <name type="ordered locus">MW1290</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Involved in cold stress response.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB95155.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000809131.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q7A0X3"/>
  <dbReference type="SMR" id="Q7A0X3"/>
  <dbReference type="GeneID" id="66839594"/>
  <dbReference type="KEGG" id="sam:MW1290"/>
  <dbReference type="HOGENOM" id="CLU_117621_6_1_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003676">
    <property type="term" value="F:nucleic acid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd04458">
    <property type="entry name" value="CSP_CDS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.140:FF:000006">
    <property type="entry name" value="Cold shock protein CspC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.20.370.130">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.140">
    <property type="entry name" value="Nucleic acid-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012156">
    <property type="entry name" value="Cold_shock_CspA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050181">
    <property type="entry name" value="Cold_shock_domain"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011129">
    <property type="entry name" value="CSD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019844">
    <property type="entry name" value="CSD_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002059">
    <property type="entry name" value="CSP_DNA-bd"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012340">
    <property type="entry name" value="NA-bd_OB-fold"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11544">
    <property type="entry name" value="COLD SHOCK DOMAIN CONTAINING PROTEINS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11544:SF142">
    <property type="entry name" value="COLD SHOCK PROTEIN CSPA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00313">
    <property type="entry name" value="CSD"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF002599">
    <property type="entry name" value="Cold_shock_A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00050">
    <property type="entry name" value="COLDSHOCK"/>
  </dbReference>
  <dbReference type="SMART" id="SM00357">
    <property type="entry name" value="CSP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50249">
    <property type="entry name" value="Nucleic acid-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00352">
    <property type="entry name" value="CSD_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51857">
    <property type="entry name" value="CSD_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <feature type="chain" id="PRO_0000262546" description="Cold shock protein CspA">
    <location>
      <begin position="1"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="domain" description="CSD">
    <location>
      <begin position="1"/>
      <end position="66"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <sequence length="66" mass="7321" checksum="E80AFCA652622943" modified="2004-07-05" version="1">MKQGTVKWFNAEKGFGFIEVEGENDVFVHFSAINQDGYKSLEEGQAVEFEVVEGDRGPQAANVVKL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>