<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-11-16" modified="2024-11-27" version="83" xmlns="http://uniprot.org/uniprot">
  <accession>Q9DEN9</accession>
  <name>MYG_GOBGI</name>
  <protein>
    <recommendedName>
      <fullName>Myoglobin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">Nitrite reductase MB</fullName>
      <ecNumber evidence="2">1.7.-.-</ecNumber>
    </alternativeName>
    <alternativeName>
      <fullName evidence="2">Pseudoperoxidase MB</fullName>
      <ecNumber evidence="2">1.11.1.-</ecNumber>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">mb</name>
  </gene>
  <organism>
    <name type="scientific">Gobionotothen gibberifrons</name>
    <name type="common">Humped rockcod</name>
    <name type="synonym">Notothenia gibberifrons</name>
    <dbReference type="NCBI Taxonomy" id="36202"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Actinopterygii</taxon>
      <taxon>Neopterygii</taxon>
      <taxon>Teleostei</taxon>
      <taxon>Neoteleostei</taxon>
      <taxon>Acanthomorphata</taxon>
      <taxon>Eupercaria</taxon>
      <taxon>Perciformes</taxon>
      <taxon>Notothenioidei</taxon>
      <taxon>Nototheniidae</taxon>
      <taxon>Gobionotothen</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Mol. Mar. Biol. Biotechnol." volume="6" first="207" last="216">
      <title>Conservation of the myoglobin gene among Antarctic notothenioid fishes.</title>
      <authorList>
        <person name="Vayda M.E."/>
        <person name="Small D.J."/>
        <person name="Yuan M.-L."/>
        <person name="Costello L."/>
        <person name="Sidell B.D."/>
      </authorList>
      <dbReference type="PubMed" id="9284559"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Heart ventricle</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Monomeric heme protein which primary function is to store oxygen and facilitate its diffusion within muscle tissues. Reversibly binds oxygen through a pentacoordinated heme iron and enables its timely and efficient release as needed during periods of heightened demand. Depending on the oxidative conditions of tissues and cells, and in addition to its ability to bind oxygen, it also has a nitrite reductase activity whereby it regulates the production of bioactive nitric oxide. Under stress conditions, like hypoxia and anoxia, it also protects cells against reactive oxygen species thanks to its pseudoperoxidase activity.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>Fe(III)-heme b-[protein] + nitric oxide + H2O = Fe(II)-heme b-[protein] + nitrite + 2 H(+)</text>
      <dbReference type="Rhea" id="RHEA:77711"/>
      <dbReference type="Rhea" id="RHEA-COMP:18975"/>
      <dbReference type="Rhea" id="RHEA-COMP:18976"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:16301"/>
      <dbReference type="ChEBI" id="CHEBI:16480"/>
      <dbReference type="ChEBI" id="CHEBI:55376"/>
      <dbReference type="ChEBI" id="CHEBI:60344"/>
    </reaction>
    <physiologicalReaction direction="right-to-left" evidence="2">
      <dbReference type="Rhea" id="RHEA:77713"/>
    </physiologicalReaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>AH2 + H2O2 = A + 2 H2O</text>
      <dbReference type="Rhea" id="RHEA:30275"/>
      <dbReference type="ChEBI" id="CHEBI:13193"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:16240"/>
      <dbReference type="ChEBI" id="CHEBI:17499"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="3">Monomeric.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Cytoplasm</location>
      <location evidence="2">Sarcoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the globin family.</text>
  </comment>
  <dbReference type="EC" id="1.7.-.-" evidence="2"/>
  <dbReference type="EC" id="1.11.1.-" evidence="2"/>
  <dbReference type="EMBL" id="U71057">
    <property type="protein sequence ID" value="AAG16645.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9DEN9"/>
  <dbReference type="SMR" id="Q9DEN9"/>
  <dbReference type="GO" id="GO:0070062">
    <property type="term" value="C:extracellular exosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016528">
    <property type="term" value="C:sarcoplasm"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0020037">
    <property type="term" value="F:heme binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0098809">
    <property type="term" value="F:nitrite reductase activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019825">
    <property type="term" value="F:oxygen binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005344">
    <property type="term" value="F:oxygen carrier activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004601">
    <property type="term" value="F:peroxidase activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019430">
    <property type="term" value="P:removal of superoxide radicals"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.140.2100">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.140.2110">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000971">
    <property type="entry name" value="Globin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009050">
    <property type="entry name" value="Globin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002335">
    <property type="entry name" value="Myoglobin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47132">
    <property type="entry name" value="MYOGLOBIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47132:SF1">
    <property type="entry name" value="MYOGLOBIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00042">
    <property type="entry name" value="Globin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00613">
    <property type="entry name" value="MYOGLOBIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46458">
    <property type="entry name" value="Globin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01033">
    <property type="entry name" value="GLOBIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0349">Heme</keyword>
  <keyword id="KW-0408">Iron</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0514">Muscle protein</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-0561">Oxygen transport</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="initiator methionine" description="Removed" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000053365" description="Myoglobin">
    <location>
      <begin position="2"/>
      <end position="147"/>
    </location>
  </feature>
  <feature type="domain" description="Globin" evidence="6">
    <location>
      <begin position="2"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="binding site" evidence="5">
    <location>
      <position position="60"/>
    </location>
    <ligand>
      <name>nitrite</name>
      <dbReference type="ChEBI" id="CHEBI:16301"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="4 6">
    <location>
      <position position="60"/>
    </location>
    <ligand>
      <name>O2</name>
      <dbReference type="ChEBI" id="CHEBI:15379"/>
    </ligand>
  </feature>
  <feature type="binding site" description="proximal binding residue" evidence="2">
    <location>
      <position position="89"/>
    </location>
    <ligand>
      <name>heme b</name>
      <dbReference type="ChEBI" id="CHEBI:60344"/>
    </ligand>
    <ligandPart>
      <name>Fe</name>
      <dbReference type="ChEBI" id="CHEBI:18248"/>
    </ligandPart>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P02144"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P02185"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="4">
    <source>
      <dbReference type="UniProtKB" id="P02189"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="5">
    <source>
      <dbReference type="UniProtKB" id="P68082"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="6">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00238"/>
    </source>
  </evidence>
  <sequence length="147" mass="15707" checksum="247E4558388DD109" modified="2007-01-23" version="3">MADFDMVLKCWGPVEADYTTHGSLVLTRLFTEHPETLKLFPKFAGIAHGDLAGDAGVSAHGATVLNKLGDLLKARGAHAALLKPLSSSHATKHKIPIINFKLIAEVIGKVMEEKAGLDAAGQTALRNVMAVIIADMEADYKELGFTE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>