<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-11-09" modified="2024-11-27" version="46" xmlns="http://uniprot.org/uniprot">
  <accession>P84223</accession>
  <accession>Q8WP47</accession>
  <name>CECC_DROOR</name>
  <protein>
    <recommendedName>
      <fullName>Cecropin-C</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">CecC</name>
  </gene>
  <organism>
    <name type="scientific">Drosophila orena</name>
    <name type="common">Fruit fly</name>
    <dbReference type="NCBI Taxonomy" id="7233"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Ephydroidea</taxon>
      <taxon>Drosophilidae</taxon>
      <taxon>Drosophila</taxon>
      <taxon>Sophophora</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="J. Mol. Evol." volume="54" first="665" last="670">
      <title>Rapid evolution of the male-specific antibacterial protein andropin gene in Drosophila.</title>
      <authorList>
        <person name="Date-Ito A."/>
        <person name="Kasahara K."/>
        <person name="Sawai H."/>
        <person name="Chigusa S.I."/>
      </authorList>
      <dbReference type="PubMed" id="11965438"/>
      <dbReference type="DOI" id="10.1007/s00239-001-0062-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text>Cecropins have lytic and antibacterial activity against several Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="developmental stage">
    <text>Expressed during metamorphosis in pupae.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the cecropin family.</text>
  </comment>
  <dbReference type="EMBL" id="AB047061">
    <property type="protein sequence ID" value="BAB78566.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P84223"/>
  <dbReference type="SMR" id="P84223"/>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000875">
    <property type="entry name" value="Cecropin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020400">
    <property type="entry name" value="Cecropin_insect"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR38329">
    <property type="entry name" value="CECROPIN-A1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR38329:SF1">
    <property type="entry name" value="CECROPIN-A1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00272">
    <property type="entry name" value="Cecropin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00268">
    <property type="entry name" value="CECROPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004850" description="Cecropin-C">
    <location>
      <begin position="24"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="modified residue" description="Arginine amide" evidence="1">
    <location>
      <position position="62"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="63" mass="6764" checksum="F8D86507CD727D9C" modified="2004-11-09" version="1" precursor="true">MNFNKIFVFVALILAISLGQSEAGWLKKLGKRIERIGQHTRDATIQGLGIAQQAANVAATARG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>