<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2024-11-27" version="93" xmlns="http://uniprot.org/uniprot">
  <accession>O19038</accession>
  <name>DEFB1_SHEEP</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 1</fullName>
      <shortName>BD-1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>sBD1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB1</name>
  </gene>
  <organism>
    <name type="scientific">Ovis aries</name>
    <name type="common">Sheep</name>
    <dbReference type="NCBI Taxonomy" id="9940"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Caprinae</taxon>
      <taxon>Ovis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="J. Nutr." volume="128" first="297S" last="299S">
      <title>Antimicrobial peptide expression is developmentally regulated in the ovine gastrointestinal tract.</title>
      <authorList>
        <person name="Huttner K.M."/>
        <person name="Brezinski-Caliguri D.J."/>
        <person name="Mahoney M.M."/>
        <person name="Diamond G."/>
      </authorList>
      <dbReference type="PubMed" id="9478010"/>
      <dbReference type="DOI" id="10.1093/jn/128.2.297s"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="Gene" volume="206" first="85" last="91">
      <title>Localization and genomic organization of sheep antimicrobial peptides genes.</title>
      <authorList>
        <person name="Huttner K.M."/>
        <person name="Lambeth M.R."/>
        <person name="Burkin H.R."/>
        <person name="Broad T.E."/>
      </authorList>
      <dbReference type="PubMed" id="9461419"/>
      <dbReference type="DOI" id="10.1016/s0378-1119(97)00569-6"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Trachea</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Has bactericidal activity. May act as a ligand for C-C chemokine receptor CCR6. Positively regulates the sperm motility and bactericidal activity in a CCR6-dependent manner. Binds to CCR6 and triggers Ca2+ mobilization in the sperm which is important for its motility.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Monomer. Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Membrane</location>
    </subcellularLocation>
    <text evidence="2">Associates with tumor cell membrane-derived microvesicles.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="U75250">
    <property type="protein sequence ID" value="AAB61995.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O19038"/>
  <dbReference type="SMR" id="O19038"/>
  <dbReference type="STRING" id="9940.ENSOARP00000006815"/>
  <dbReference type="PaxDb" id="9940-ENSOARP00000006815"/>
  <dbReference type="Ensembl" id="ENSOART00000006919.1">
    <property type="protein sequence ID" value="ENSOARP00000006815.1"/>
    <property type="gene ID" value="ENSOARG00000006364.1"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502SYUI">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_189296_4_1_1"/>
  <dbReference type="OMA" id="PGTKCCR"/>
  <dbReference type="OrthoDB" id="5105768at2759"/>
  <dbReference type="Proteomes" id="UP000002356">
    <property type="component" value="Chromosome 26"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSOARG00000006364">
    <property type="expression patterns" value="Expressed in rectum and 48 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="O19038">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990742">
    <property type="term" value="C:microvesicle"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097225">
    <property type="term" value="C:sperm midpiece"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042056">
    <property type="term" value="F:chemoattractant activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019722">
    <property type="term" value="P:calcium-mediated signaling"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060326">
    <property type="term" value="P:cell chemotaxis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060474">
    <property type="term" value="P:positive regulation of flagellated sperm motility involved in capacitation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.360.10:FF:000001">
    <property type="entry name" value="Beta-defensin 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006080">
    <property type="entry name" value="Beta/alpha-defensin_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515:SF2">
    <property type="entry name" value="BETA-DEFENSIN 4A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00048">
    <property type="entry name" value="DEFSN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006963" description="Beta-defensin 1">
    <location>
      <begin position="23"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="38"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="61"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P60022"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="64" mass="7244" checksum="3529A9B76ABD023A" modified="1998-01-01" version="1" precursor="true">MRLHHLLLVLFFVVLSAGSGFTQGVRNRLSCHRNKGVCVPSRCPRHMRQIGTCRGPPVKCCRKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>