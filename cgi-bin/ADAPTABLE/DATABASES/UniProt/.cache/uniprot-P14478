<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-01-01" modified="2023-11-08" version="93" xmlns="http://uniprot.org/uniprot">
  <accession>P14478</accession>
  <name>FIBB_RABIT</name>
  <protein>
    <recommendedName>
      <fullName>Fibrinogen beta chain</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Fibrinopeptide B</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Fibrinogen beta chain</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">FGB</name>
  </gene>
  <organism>
    <name type="scientific">Oryctolagus cuniculus</name>
    <name type="common">Rabbit</name>
    <dbReference type="NCBI Taxonomy" id="9986"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Lagomorpha</taxon>
      <taxon>Leporidae</taxon>
      <taxon>Oryctolagus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Toxicon" volume="35" first="597" last="605">
      <title>Capillary permeability-increasing enzyme-2 from the venom of Agkistrodon caliginosus (Kankoku-mamushi): activity resulting from the release of peptides from fibrinogen.</title>
      <authorList>
        <person name="Shimokawa K."/>
        <person name="Takahashi H."/>
      </authorList>
      <dbReference type="PubMed" id="9133714"/>
      <dbReference type="DOI" id="10.1016/s0041-0101(96)00158-4"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Plasma</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1965" name="Acta Chem. Scand." volume="19" first="1789" last="1791">
      <title>Studies on fibrinopeptides from mammals.</title>
      <authorList>
        <person name="Blombaeck B."/>
        <person name="Blombaeck M."/>
        <person name="Grondahl N.J."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE OF 1-13</scope>
    <scope>SULFATION AT TYR-4</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Cleaved by the protease thrombin to yield monomers which, together with fibrinogen alpha (FGA) and fibrinogen gamma (FGG), polymerize to form an insoluble fibrin matrix. Fibrin has a major function in hemostasis as one of the primary components of blood clots. In addition, functions during the early stages of wound repair to stabilize the lesion and guide cell migration during re-epithelialization. Was originally thought to be essential for platelet aggregation, based on in vitro studies using anticoagulated blood. However subsequent studies have shown that it is not absolutely required for thrombus formation in vivo. Enhances expression of SELP in activated platelets. Maternal fibrinogen is essential for successful pregnancy. Fibrin deposition is also associated with infection, where it protects against IFNG-mediated hemorrhage. May also facilitate the antibacterial immune response via both innate and T-cell mediated pathways.</text>
  </comment>
  <comment type="subunit">
    <text evidence="3">Heterohexamer; disulfide linked. Contains 2 sets of 3 non-identical chains (alpha, beta and gamma). The 2 heterotrimers are in head to head conformation with the N-termini in a small central domain (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="3">A long coiled coil structure formed by 3 polypeptide chains connects the central nodule to the C-terminal domains (distal nodules). The long C-terminal ends of the alpha chains fold back, contributing a fourth strand to the coiled coil structure.</text>
  </comment>
  <comment type="PTM">
    <text>Conversion of fibrinogen to fibrin is triggered by thrombin, which cleaves fibrinopeptides A and B from alpha and beta chains, and thus exposes the N-terminal polymerization sites responsible for the formation of the soft clot.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P14478"/>
  <dbReference type="STRING" id="9986.ENSOCUP00000009668"/>
  <dbReference type="PaxDb" id="9986-ENSOCUP00000009668"/>
  <dbReference type="eggNOG" id="KOG2579">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="P14478"/>
  <dbReference type="Proteomes" id="UP000001811">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009986">
    <property type="term" value="C:cell surface"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009897">
    <property type="term" value="C:external side of plasma membrane"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031091">
    <property type="term" value="C:platelet alpha granule"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051087">
    <property type="term" value="F:protein-folding chaperone binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002250">
    <property type="term" value="P:adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007596">
    <property type="term" value="P:blood coagulation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051592">
    <property type="term" value="P:response to calcium ion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-0094">Blood coagulation</keyword>
  <keyword id="KW-0175">Coiled coil</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0356">Hemostasis</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0765">Sulfation</keyword>
  <feature type="peptide" id="PRO_0000009084" description="Fibrinopeptide B">
    <location>
      <begin position="1"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000376836" description="Fibrinogen beta chain">
    <location>
      <begin position="14"/>
      <end position="41" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="4">
    <location>
      <begin position="1"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="region of interest" description="Beta-chain polymerization, binding distal domain of another fibrin" evidence="1">
    <location>
      <begin position="14"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="4">
    <location>
      <begin position="8"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="site" description="Cleavage; by thrombin; to release fibrinopeptide B" evidence="1">
    <location>
      <begin position="13"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="modified residue" description="Sulfotyrosine" evidence="6">
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="41"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="E9PV24"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P02675"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="4">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="9133714"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source ref="2"/>
  </evidence>
  <sequence length="41" mass="4585" checksum="ED1A9CDFA9B4FEBB" modified="2009-06-16" version="2" precursor="true" fragment="single">ADDYDDEVLPDARGHRPIDRKREELPSLRPAPPPISGGGYR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>