<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2022-05-25" modified="2024-11-27" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>P0DUM6</accession>
  <name>PDUA_CITFR</name>
  <protein>
    <recommendedName>
      <fullName evidence="10">Bacterial microcompartment shell protein PduA</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="11">Bacterial microcompartment protein homohexamer</fullName>
      <shortName evidence="11">BMC-H</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Propanediol utilization protein PduA</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="10" type="primary">pduA</name>
  </gene>
  <organism>
    <name type="scientific">Citrobacter freundii</name>
    <dbReference type="NCBI Taxonomy" id="546"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Citrobacter</taxon>
      <taxon>Citrobacter freundii complex</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="J. Biol. Chem." volume="283" first="14366" last="14375">
      <title>Biochemical and Structural Insights into Bacterial Organelle Form and Biogenesis.</title>
      <authorList>
        <person name="Parsons J.B."/>
        <person name="Dinesh S.D."/>
        <person name="Deery E."/>
        <person name="Leech H.K."/>
        <person name="Brindley A.A."/>
        <person name="Heldt D."/>
        <person name="Frank S."/>
        <person name="Smales C.M."/>
        <person name="Lunsdorf H."/>
        <person name="Rambach A."/>
        <person name="Gass M.H."/>
        <person name="Bleloch A."/>
        <person name="McClean K.J."/>
        <person name="Munro A.W."/>
        <person name="Rigby S.E.J."/>
        <person name="Warren M.J."/>
        <person name="Prentice M.B."/>
      </authorList>
      <dbReference type="PubMed" id="18332146"/>
      <dbReference type="DOI" id="10.1074/jbc.m709214200"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>PATHWAY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2010" name="Mol. Cell" volume="38" first="305" last="315">
      <title>Synthesis of empty bacterial microcompartments, directed organelle protein incorporation, and evidence of filament-associated organelle movement.</title>
      <authorList>
        <person name="Parsons J.B."/>
        <person name="Frank S."/>
        <person name="Bhella D."/>
        <person name="Liang M."/>
        <person name="Prentice M.B."/>
        <person name="Mulvihill D.P."/>
        <person name="Warren M.J."/>
      </authorList>
      <dbReference type="PubMed" id="20417607"/>
      <dbReference type="DOI" id="10.1016/j.molcel.2010.04.008"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH PDUB; PDUB'; PDUJ; PDUK; PDUN AND PDUU</scope>
    <scope>SUBUNIT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>BIOTECHNOLOGY (ARTIFICIAL BMCS)</scope>
    <scope>MUTAGENESIS OF SER-93</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2014" name="ACS Synth. Biol." volume="3" first="454" last="465">
      <title>Solution structure of a bacterial microcompartment targeting peptide and its application in the construction of an ethanol bioreactor.</title>
      <authorList>
        <person name="Lawrence A.D."/>
        <person name="Frank S."/>
        <person name="Newnham S."/>
        <person name="Lee M.J."/>
        <person name="Brown I.R."/>
        <person name="Xue W.F."/>
        <person name="Rowe M.L."/>
        <person name="Mulvihill D.P."/>
        <person name="Prentice M.B."/>
        <person name="Howard M.J."/>
        <person name="Warren M.J."/>
      </authorList>
      <dbReference type="PubMed" id="24933391"/>
      <dbReference type="DOI" id="10.1021/sb4001118"/>
    </citation>
    <scope>BIOTECHNOLOGY (ARTIFICIAL BMCS)</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2015" name="PLoS Comput. Biol." volume="11" first="e1004067" last="e1004067">
      <title>Exploring bacterial organelle interactomes: a model of the protein-protein interaction network in the Pdu microcompartment.</title>
      <authorList>
        <person name="Jorda J."/>
        <person name="Liu Y."/>
        <person name="Bobik T.A."/>
        <person name="Yeates T.O."/>
      </authorList>
      <dbReference type="PubMed" id="25646976"/>
      <dbReference type="DOI" id="10.1371/journal.pcbi.1004067"/>
    </citation>
    <scope>MODELING OF BMCS</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2018" name="Nat. Chem. Biol." volume="14" first="142" last="147">
      <title>Engineered synthetic scaffolds for organizing proteins within the bacterial cytoplasm.</title>
      <authorList>
        <person name="Lee M.J."/>
        <person name="Mantell J."/>
        <person name="Hodgson L."/>
        <person name="Alibhai D."/>
        <person name="Fletcher J.M."/>
        <person name="Brown I.R."/>
        <person name="Frank S."/>
        <person name="Xue W.F."/>
        <person name="Verkade P."/>
        <person name="Woolfson D.N."/>
        <person name="Warren M.J."/>
      </authorList>
      <dbReference type="PubMed" id="29227472"/>
      <dbReference type="DOI" id="10.1038/nchembio.2535"/>
    </citation>
    <scope>BIOTECHNOLOGY (INTRACELLULAR SCAFFOLDS)</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2018" name="Small" volume="14" first="e1704020" last="e1704020">
      <title>A Generic Self-Assembly Process in Microcompartments and Synthetic Protein Nanotubes.</title>
      <authorList>
        <person name="Uddin I."/>
        <person name="Frank S."/>
        <person name="Warren M.J."/>
        <person name="Pickersgill R.W."/>
      </authorList>
      <dbReference type="PubMed" id="29573556"/>
      <dbReference type="DOI" id="10.1002/smll.201704020"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <scope>BIOTECHNOLOGY (PERMUTATED PROTEIN)</scope>
  </reference>
  <reference evidence="17 18" key="7">
    <citation type="journal article" date="2014" name="J. Biol. Chem." volume="289" first="22377" last="22384">
      <title>Structural insights into higher order assembly and function of the bacterial microcompartment protein PduA.</title>
      <authorList>
        <person name="Pang A."/>
        <person name="Frank S."/>
        <person name="Brown I."/>
        <person name="Warren M.J."/>
        <person name="Pickersgill R.W."/>
      </authorList>
      <dbReference type="PubMed" id="24873823"/>
      <dbReference type="DOI" id="10.1074/jbc.m114.569285"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.72 ANGSTROMS) OF MUTATED 1-92</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>MUTAGENESIS OF LYS-26; VAL-51; ARG-79 AND SER-93</scope>
  </reference>
  <comment type="function">
    <text evidence="2 5 6 8 12 13 14 16">One of the major shell proteins of the bacterial microcompartment (BMC) dedicated to 1,2-propanediol (1,2-PD) degradation, probably important for metabolite diffusion into and out of the BMC (Probable). Overexpression of a C-terminally mutated form (PduA*) makes thin parallel filaments with a honeycomb-like assembly in cross-section that probably form nanotubes. The filaments interfere with septation (PubMed:20417607, PubMed:24873823, PubMed:29227472). PduA is probably the hub for binding multiple enzymes to the interior of the BMC (Probable). At least one of PduA or PduJ is required for BMC assembly; it must be encoded as the first gene in the pdu operon (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="4">Expression of a cosmid containing the full 21-gene pdu operon in E.coli allows E.coli to grow on 1,2-PD with the appearance of BMCs in its cytoplasm. Overexpression of this protein leads to aberrant intracellular filaments.</text>
  </comment>
  <comment type="function">
    <text evidence="12">The 1,2-PD-specific bacterial microcompartment (BMC) concentrates low levels of 1,2-PD catabolic enzymes, concentrates volatile reaction intermediates thus enhancing pathway flux and keeps the level of toxic, mutagenic propionaldehyde low.</text>
  </comment>
  <comment type="pathway">
    <text evidence="4">Polyol metabolism; 1,2-propanediol degradation.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 5 6 14">Homohexamer with a central pore; Lys-26 and Arg-79 interactions are very important for hexamer symmetry (PubMed:24873823). The hexamers pack against each other in arrays (By similarity). Interacts individually with shell proteins PduB, PduB', PduJ, PduK, PduN and PduU (PubMed:20417607). Modeling suggests PduC, PduD, PduE, PduL and PduP interact with a cleft formed by the C-terminal segments of 2 adjacent PduA subunits (on the BMC luminal side) in the hexamer (Probable).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4 5">Bacterial microcompartment</location>
    </subcellularLocation>
    <text evidence="14 15">In BMCs the hexamer concave side probably faces outward, with the N- and C-termini exposed to the cytoplasm (Probable). Modeling suggests the concave face (with both termini) is in the interior of the BMC (Probable).</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="4 9">When the whole pdu operon except this gene is expressed in E.coli no BMCs are made; with the whole operon many BMCs are produced in E.coli (PubMed:18332146). This gene is not essential for BMC formation; a pduB/B'-pduJ-pduK-pduN-pduU-pduT strain produces BMCs in E.coli (PubMed:29573556).</text>
  </comment>
  <comment type="biotechnology">
    <text evidence="5 7 9">Artificial BMCs can be made in E.coli by expressing pduA-pduB/B'-pduJ-pduK-pduN-pduU-pduT (in this order); pduT and pduU are optional, while pduA, pduB/B', pduJ, pduK and pduN are essential. A construct with the reversed gene order does not make BMCs (PubMed:20417607). Ethanogenic BMCs can be made in E.coli by targeting pyruvate decarboxylase (pdc) and alcohol dehydrogenase (adh) to them. PduP(1-18)-Pdc and PduD(1-18)-Adh strains targeted to the BMC (PduA, PduB, PduJ, PduK, PduN, PduU) make significantly more ethanol than strains where Pdc and Adh are not targeted to the BMC (PubMed:24933391). A circularly permutated form can be made in which residues 69-89 are displaced to the beginning of the protein with an extra linker. Both termini of this construct are probably in the BMC lumen and it can be used to target proteins to the BMC interior (PubMed:29573556).</text>
  </comment>
  <comment type="biotechnology">
    <text evidence="8 9">Can be used to make a synthetic intracellular scaffold that fills the E.coli cytoplasm by adding a heterodimeric coiled-coil system (CC-Di-AB). PduA* is N-terminally tagged with CC-Di-B which makes the scaffold, while other proteins can be targeted to the scaffold using a CC-Di-A tag. A strain that produces increased amounts of ethanol (using pdc and adh) has been made as proof of concept. The scaffolds can also be targeted to the cell inner membrane (PubMed:29227472). The same CC-Di-B construct also targets proteins to the BMC, probably on the exterior face (PubMed:29573556).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the bacterial microcompartments protein family.</text>
  </comment>
  <dbReference type="EMBL" id="AM498294">
    <property type="protein sequence ID" value="CAM57283.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003030255.1">
    <property type="nucleotide sequence ID" value="NZ_VWTQ01000002.1"/>
  </dbReference>
  <dbReference type="PDB" id="4P7T">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.72 A"/>
    <property type="chains" value="A/B/C/D/E/F=1-92"/>
  </dbReference>
  <dbReference type="PDB" id="4P7V">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.93 A"/>
    <property type="chains" value="A/B/C/D/E/F=1-92"/>
  </dbReference>
  <dbReference type="PDBsum" id="4P7T"/>
  <dbReference type="PDBsum" id="4P7V"/>
  <dbReference type="SMR" id="P0DUM6"/>
  <dbReference type="GeneID" id="89548324"/>
  <dbReference type="UniPathway" id="UPA00621"/>
  <dbReference type="GO" id="GO:0031469">
    <property type="term" value="C:bacterial microcompartment"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051144">
    <property type="term" value="P:propanediol catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniPathway"/>
  </dbReference>
  <dbReference type="CDD" id="cd07045">
    <property type="entry name" value="BMC_CcmK_like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.70.1710">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020808">
    <property type="entry name" value="Bact_microcomp_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000249">
    <property type="entry name" value="BMC_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050575">
    <property type="entry name" value="BMC_shell"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR037233">
    <property type="entry name" value="CcmK-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044872">
    <property type="entry name" value="CcmK/CsoS1_BMC"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33941:SF11">
    <property type="entry name" value="BACTERIAL MICROCOMPARTMENT SHELL PROTEIN PDUJ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33941">
    <property type="entry name" value="PROPANEDIOL UTILIZATION PROTEIN PDUA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00936">
    <property type="entry name" value="BMC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00877">
    <property type="entry name" value="BMC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF143414">
    <property type="entry name" value="CcmK-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01139">
    <property type="entry name" value="BMC_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51930">
    <property type="entry name" value="BMC_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-1283">Bacterial microcompartment</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000454246" description="Bacterial microcompartment shell protein PduA">
    <location>
      <begin position="1"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="domain" description="BMC" evidence="3">
    <location>
      <begin position="5"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Forms sheets but not nanotubes upon overexpression of PduA*." evidence="6">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No intracellular structures formed upon overexpression of PduA*, decreases stability of hexamers which are no longer symmetric and do not form arrays correctly." evidence="6">
    <original>K</original>
    <variation>D</variation>
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Forms sheets and altered nanotubes upon overexpression of PduA*." evidence="6">
    <original>V</original>
    <variation>A</variation>
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No structures formed upon overexpression of PduA*." evidence="6">
    <original>V</original>
    <variation>D</variation>
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Forms sheets but not nanotubes upon overexpression of PduA*." evidence="6">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="79"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="PduA*, a soluble form used for crystal structure determination." evidence="5 6 17 18">
    <original>S</original>
    <variation>RLVKDPAANKARKEAELAAATAEQ</variation>
    <location>
      <position position="93"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P0A1C7"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P37448"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01278"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="18332146"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="20417607"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="24873823"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="24933391"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="29227472"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="29573556"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="10">
    <source>
      <dbReference type="PubMed" id="18332146"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="11"/>
  <evidence type="ECO:0000305" key="12">
    <source>
      <dbReference type="PubMed" id="20417607"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13">
    <source>
      <dbReference type="PubMed" id="24873823"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="14">
    <source>
      <dbReference type="PubMed" id="25646976"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="15">
    <source>
      <dbReference type="PubMed" id="29227472"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="16">
    <source>
      <dbReference type="PubMed" id="29573556"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="17">
    <source>
      <dbReference type="PDB" id="4P7T"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="18">
    <source>
      <dbReference type="PDB" id="4P7V"/>
    </source>
  </evidence>
  <sequence length="93" mass="9464" checksum="CC7C793F19DE1E36" modified="2022-05-25" version="1">MQQEALGMVETKGLTAAIEAADAMVKSANVMLVGYEKIGSGLVTVIVRGDVGAVKAATDAGAAAARNVGEVKAVHVIPRPHTDVEKILPKGIS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>