<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-09-05" modified="2024-11-27" version="23" xmlns="http://uniprot.org/uniprot">
  <accession>P84911</accession>
  <name>CYT1_CAPHI</name>
  <protein>
    <recommendedName>
      <fullName>Cystatin-1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cystatin-I</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Capra hircus</name>
    <name type="common">Goat</name>
    <dbReference type="NCBI Taxonomy" id="9925"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Caprinae</taxon>
      <taxon>Capra</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2005" name="Comp. Biochem. Physiol." volume="142B" first="361" last="368">
      <title>Isolation, characterization and kinetics of goat cystatins.</title>
      <authorList>
        <person name="Sadaf Z."/>
        <person name="Shahid P.B."/>
        <person name="Bilqees B."/>
      </authorList>
      <dbReference type="PubMed" id="16257555"/>
      <dbReference type="DOI" id="10.1016/j.cbpb.2005.08.007"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>GLYCOSYLATION</scope>
    <source>
      <tissue evidence="1">Kidney</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Inhibits papain, ficin and bromelain with an IC(50) of 0.09 uM, 0.115 uM and 0.113 uM respectively. Has antibacterial activity against Gram-positive bacteria S.aureus and S.hemolyticus, and against the Gram-negative bacterium E.coli. Reduced antibacterial activity against Gram-positive bacterium B.subtilis. No antibacterial activity against the Gram-negative bacterium P.fluorescens.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Glycosylated.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">Stable in the pH range 5.0-9.0 at 37 degrees Celsius. Stable when incubated in the temperature range 25-75 degrees Celsius and pH 7.5.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the cystatin family.</text>
  </comment>
  <dbReference type="Proteomes" id="UP000291000">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694566">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694900">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <feature type="chain" id="PRO_0000248504" description="Cystatin-1">
    <location>
      <begin position="1"/>
      <end position="15" status="greater than"/>
    </location>
  </feature>
  <feature type="unsure residue" description="K or Y">
    <location>
      <position position="7"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="2">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="16257555"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="16257555"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="15" mass="1673" checksum="1D820D213892F004" modified="2006-09-05" version="1" fragment="single">DTHISEKIIDCNDIG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>