<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-04-12" modified="2023-02-22" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>P0DOZ9</accession>
  <name>CUEA_CONAX</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Conotoxin Asi14a</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Conus asiaticus</name>
    <name type="common">Cone snail</name>
    <dbReference type="NCBI Taxonomy" id="1945508"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Caenogastropoda</taxon>
      <taxon>Neogastropoda</taxon>
      <taxon>Conoidea</taxon>
      <taxon>Conidae</taxon>
      <taxon>Conus</taxon>
      <taxon>Phasmoconus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2016" name="Mar. Drugs" volume="14" first="199" last="216">
      <title>Novel conopeptides of largely unexplored indo Pacific Conus sp.</title>
      <authorList>
        <person name="Lebbe E.K."/>
        <person name="Ghequire M.G."/>
        <person name="Peigneur S."/>
        <person name="Mille B.G."/>
        <person name="Devi P."/>
        <person name="Ravichandran S."/>
        <person name="Waelkens E."/>
        <person name="D'Souza L."/>
        <person name="De Mot R."/>
        <person name="Tytgat J."/>
      </authorList>
      <dbReference type="PubMed" id="27801785"/>
      <dbReference type="DOI" id="10.3390/md14110199"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT GLY-17</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SYNTHESIS</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 3">May have neurotoxic activities (Probable). Does not show any effect on voltage-gated potassium channels (10 uM of toxin tested), and on nicotinic acetylcholine receptors (5 uM of toxin tested) (PubMed:27801785). Does not show antibacterial activity on both Gram-negative and Gram-positive bacteria (PubMed:27801785).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the venom duct.</text>
  </comment>
  <comment type="domain">
    <text evidence="3">The cysteine framework is XIV (C-C-C-C).</text>
  </comment>
  <comment type="PTM">
    <text evidence="4">Contains 2 disulfide bonds.</text>
  </comment>
  <comment type="mass spectrometry" mass="1697.6" method="MALDI" evidence="1"/>
  <comment type="miscellaneous">
    <text evidence="1">Negative results: does not show effect on Kv1.1, Kv1.2, Kv1.3, Kv1.4, Kv1.5, and Kv1.6 (PubMed:27801785). Does not show effect on muscular (alpha-1-beta-1-delta-epsilon) and neuronal alpha-7, alpha-4-beta-4, and alpha-3-beta-4 nAChRs (PubMed:27801785).</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DOZ9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000439518" description="Conotoxin Asi14a" evidence="1">
    <location>
      <begin position="1"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glycine amide" evidence="1">
    <location>
      <position position="17"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="27801785"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="27801785"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="27801785"/>
    </source>
  </evidence>
  <sequence length="17" mass="1701" checksum="9925C5D372394A91" modified="2017-04-12" version="1">SCGYPCSHCGIPGCYPG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>