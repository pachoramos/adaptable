<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-10-14" modified="2024-10-02" version="85" xmlns="http://uniprot.org/uniprot">
  <accession>Q32ZF3</accession>
  <name>DFB43_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 43</fullName>
      <shortName>BD-43</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 43</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Defb43</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Physiol. Genomics" volume="23" first="5" last="17">
      <title>Cross-species analysis of the mammalian beta-defensin gene family: presence of syntenic gene clusters and preferential expression in the male reproductive tract.</title>
      <authorList>
        <person name="Patil A.A."/>
        <person name="Cai Y."/>
        <person name="Sang Y."/>
        <person name="Blecha F."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="16033865"/>
      <dbReference type="DOI" id="10.1152/physiolgenomics.00104.2005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has bactericidal activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY621370">
    <property type="protein sequence ID" value="AAT51909.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001032617.1">
    <property type="nucleotide sequence ID" value="NM_001037528.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q32ZF3"/>
  <dbReference type="SMR" id="Q32ZF3"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000055982"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000055982"/>
  <dbReference type="Ensembl" id="ENSRNOT00000059212.2">
    <property type="protein sequence ID" value="ENSRNOP00000055982.1"/>
    <property type="gene ID" value="ENSRNOG00000038759.2"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055022199">
    <property type="protein sequence ID" value="ENSRNOP00055018039"/>
    <property type="gene ID" value="ENSRNOG00055012963"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060031283">
    <property type="protein sequence ID" value="ENSRNOP00060025379"/>
    <property type="gene ID" value="ENSRNOG00060018236"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065051355">
    <property type="protein sequence ID" value="ENSRNOP00065042291"/>
    <property type="gene ID" value="ENSRNOG00065029733"/>
  </dbReference>
  <dbReference type="GeneID" id="641651"/>
  <dbReference type="KEGG" id="rno:641651"/>
  <dbReference type="UCSC" id="RGD:1566384">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:1566384"/>
  <dbReference type="CTD" id="654458"/>
  <dbReference type="RGD" id="1566384">
    <property type="gene designation" value="Defb43"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502TEDN">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000001538"/>
  <dbReference type="HOGENOM" id="CLU_203372_0_0_1"/>
  <dbReference type="InParanoid" id="Q32ZF3"/>
  <dbReference type="OMA" id="ICCKLKY"/>
  <dbReference type="OrthoDB" id="4822456at2759"/>
  <dbReference type="PhylomeDB" id="Q32ZF3"/>
  <dbReference type="TreeFam" id="TF341399"/>
  <dbReference type="Reactome" id="R-RNO-1461957">
    <property type="pathway name" value="Beta defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-1461973">
    <property type="pathway name" value="Defensins"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q32ZF3"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 15"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000038759">
    <property type="expression patterns" value="Expressed in gonad and 1 other cell type or tissue"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025933">
    <property type="entry name" value="Beta_defensin_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47900">
    <property type="entry name" value="BETA-DEFENSIN 131A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47900:SF1">
    <property type="entry name" value="BETA-DEFENSIN 131A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13841">
    <property type="entry name" value="Defensin_beta_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000352717" description="Beta-defensin 43">
    <location>
      <begin position="23"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="29"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="36"/>
      <end position="50"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="70" mass="8329" checksum="8A1EC9264B93F4B9" modified="2005-12-06" version="1" precursor="true">MRLLLSILGVLTLLSILPLARSFLANQECFSEYRHCRMKCKANEYAIRYCADWTICCRVKKREAKKKIMW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>