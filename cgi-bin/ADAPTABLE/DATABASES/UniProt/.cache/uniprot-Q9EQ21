<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-06-20" modified="2024-11-27" version="149" xmlns="http://uniprot.org/uniprot">
  <accession>Q9EQ21</accession>
  <name>HEPC_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Hepcidin</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">Hamp</name>
    <name type="synonym">Hamp1</name>
    <name type="synonym">Hepc</name>
    <name type="synonym">Hepc1</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="J. Biol. Chem." volume="276" first="7811" last="7819">
      <title>A new mouse liver-specific gene, encoding a protein homologous to human antimicrobial peptide hepcidin, is overexpressed during iron overload.</title>
      <authorList>
        <person name="Pigeon C."/>
        <person name="Ilyin G."/>
        <person name="Courselaud B."/>
        <person name="Leroyer P."/>
        <person name="Turlin B."/>
        <person name="Brissot P."/>
        <person name="Loreal O."/>
      </authorList>
      <dbReference type="PubMed" id="11113132"/>
      <dbReference type="DOI" id="10.1074/jbc.m008923200"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2001" name="Proc. Natl. Acad. Sci. U.S.A." volume="98" first="8780" last="8785">
      <title>Lack of hepcidin gene expression and severe tissue iron overload in upstream stimulatory factor 2 (USF2) knockout mice.</title>
      <authorList>
        <person name="Nicolas G."/>
        <person name="Bennoun M."/>
        <person name="Devaux I."/>
        <person name="Beaumont C."/>
        <person name="Grandchamp B."/>
        <person name="Kahn A."/>
        <person name="Vaulont S."/>
      </authorList>
      <dbReference type="PubMed" id="11447267"/>
      <dbReference type="DOI" id="10.1073/pnas.151179498"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <source>
      <strain>C57BL/6 X 129/Sv</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2002" name="J. Clin. Invest." volume="110" first="1037" last="1044">
      <title>The gene encoding the iron regulatory peptide hepcidin is regulated by anemia, hypoxia, and inflammation.</title>
      <authorList>
        <person name="Nicolas G."/>
        <person name="Chauvet C."/>
        <person name="Viatte L."/>
        <person name="Danan J.L."/>
        <person name="Bigard X."/>
        <person name="Devaux I."/>
        <person name="Beaumont C."/>
        <person name="Kahn A."/>
        <person name="Vaulont S."/>
      </authorList>
      <dbReference type="PubMed" id="12370282"/>
      <dbReference type="DOI" id="10.1172/jci15686"/>
    </citation>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2003" name="FEBS Lett." volume="542" first="22" last="26">
      <title>Comparative analysis of mouse hepcidin 1 and 2 genes: evidence for different patterns of expression and co-inducibility during iron overload.</title>
      <authorList>
        <person name="Ilyin G."/>
        <person name="Courselaud B."/>
        <person name="Troadec M.-B."/>
        <person name="Pigeon C."/>
        <person name="Alizadeh M."/>
        <person name="Leroyer P."/>
        <person name="Brissot P."/>
        <person name="Loreal O."/>
      </authorList>
      <dbReference type="PubMed" id="12729891"/>
      <dbReference type="DOI" id="10.1016/s0014-5793(03)00329-6"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2004" name="J. Clin. Invest." volume="113" first="1271" last="1276">
      <title>IL-6 mediates hypoferremia of inflammation by inducing the synthesis of the iron regulatory hormone hepcidin.</title>
      <authorList>
        <person name="Nemeth E."/>
        <person name="Rivera S."/>
        <person name="Gabayan V."/>
        <person name="Keller C."/>
        <person name="Taudorf S."/>
        <person name="Pedersen B.K."/>
        <person name="Ganz T."/>
      </authorList>
      <dbReference type="PubMed" id="15124018"/>
      <dbReference type="DOI" id="10.1172/jci20945"/>
    </citation>
    <scope>INDUCTION BY INFLAMMATION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2014" name="Nat. Genet." volume="46" first="678" last="684">
      <title>Identification of erythroferrone as an erythroid regulator of iron metabolism.</title>
      <authorList>
        <person name="Kautz L."/>
        <person name="Jung G."/>
        <person name="Valore E.V."/>
        <person name="Rivella S."/>
        <person name="Nemeth E."/>
        <person name="Ganz T."/>
      </authorList>
      <dbReference type="PubMed" id="24880340"/>
      <dbReference type="DOI" id="10.1038/ng.2996"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INDUCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="5 9">Liver-produced hormone that constitutes the main circulating regulator of iron absorption and distribution across tissues. Acts by promoting endocytosis and degradation of SLC40A1, leading to the retention of iron in iron-exporting cells and decreased flow of iron into plasma. Controls the major flows of iron into plasma: absorption of dietary iron in the intestine, recycling of iron by macrophages, which phagocytose old erythrocytes and other cells, and mobilization of stored iron from hepatocytes.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Interacts with SLC40A1; this interaction promotes SLC40A1 rapid ubiquitination.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4 8">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4 7 8">Highly expressed in the liver and to a much lesser extent in the heart. Secreted in blood (PubMed:15124018).</text>
  </comment>
  <comment type="induction">
    <text evidence="6 8 9">Regulated in response to changes in circulating iron concentrations, iron stores or the development of inflammation and iron-restricted erythropoiesis. Down-regulated following anemia induced by hemorrhage or hemolysis: down-regulation is mediated by ERFE (PubMed:12370282, PubMed:15124018, PubMed:24880340). The induction of upon inflammation is mediated by IL6 (PubMed:15124018).</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the hepcidin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF297664">
    <property type="protein sequence ID" value="AAG49293.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF503444">
    <property type="protein sequence ID" value="AAM27440.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC021587">
    <property type="protein sequence ID" value="AAH21587.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS39895.1"/>
  <dbReference type="RefSeq" id="NP_115930.1">
    <property type="nucleotide sequence ID" value="NM_032541.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9EQ21"/>
  <dbReference type="SMR" id="Q9EQ21"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000055404"/>
  <dbReference type="TCDB" id="8.A.37.1.1">
    <property type="family name" value="the hepcidin (hepcidin) family"/>
  </dbReference>
  <dbReference type="PhosphoSitePlus" id="Q9EQ21"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000055404"/>
  <dbReference type="PeptideAtlas" id="Q9EQ21"/>
  <dbReference type="ProteomicsDB" id="269735"/>
  <dbReference type="Antibodypedia" id="29325">
    <property type="antibodies" value="438 antibodies from 31 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="84506"/>
  <dbReference type="Ensembl" id="ENSMUST00000062620.9">
    <property type="protein sequence ID" value="ENSMUSP00000055404.8"/>
    <property type="gene ID" value="ENSMUSG00000050440.9"/>
  </dbReference>
  <dbReference type="GeneID" id="84506"/>
  <dbReference type="KEGG" id="mmu:84506"/>
  <dbReference type="UCSC" id="uc009ghf.1">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:1933533"/>
  <dbReference type="CTD" id="57817"/>
  <dbReference type="MGI" id="MGI:1933533">
    <property type="gene designation" value="Hamp"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000050440"/>
  <dbReference type="eggNOG" id="ENOG502T0FU">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000003154"/>
  <dbReference type="HOGENOM" id="CLU_2557737_0_0_1"/>
  <dbReference type="InParanoid" id="Q9EQ21"/>
  <dbReference type="OMA" id="PICLFCC"/>
  <dbReference type="OrthoDB" id="4846842at2759"/>
  <dbReference type="PhylomeDB" id="Q9EQ21"/>
  <dbReference type="TreeFam" id="TF330932"/>
  <dbReference type="BioGRID-ORCS" id="84506">
    <property type="hits" value="0 hits in 78 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="Hamp">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q9EQ21"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 7"/>
  </dbReference>
  <dbReference type="RNAct" id="Q9EQ21">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000050440">
    <property type="expression patterns" value="Expressed in left lobe of liver and 88 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005507">
    <property type="term" value="F:copper ion binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0141108">
    <property type="term" value="F:transporter regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007259">
    <property type="term" value="P:cell surface receptor signaling pathway via JAK-STAT"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051649">
    <property type="term" value="P:establishment of localization in cell"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006879">
    <property type="term" value="P:intracellular iron ion homeostasis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034755">
    <property type="term" value="P:iron ion transmembrane transport"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042116">
    <property type="term" value="P:macrophage activation"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060586">
    <property type="term" value="P:multicellular organismal-level iron ion homeostasis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002262">
    <property type="term" value="P:myeloid cell homeostasis"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045779">
    <property type="term" value="P:negative regulation of bone resorption"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050728">
    <property type="term" value="P:negative regulation of inflammatory response"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:1904479">
    <property type="term" value="P:negative regulation of intestinal absorption"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:1904039">
    <property type="term" value="P:negative regulation of iron export across plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034760">
    <property type="term" value="P:negative regulation of iron ion transmembrane transport"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000122">
    <property type="term" value="P:negative regulation of transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="BHF-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043032">
    <property type="term" value="P:positive regulation of macrophage activation"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032436">
    <property type="term" value="P:positive regulation of proteasomal ubiquitin-dependent protein catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045732">
    <property type="term" value="P:positive regulation of protein catabolic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045944">
    <property type="term" value="P:positive regulation of transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030163">
    <property type="term" value="P:protein catabolic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010039">
    <property type="term" value="P:response to iron ion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006366">
    <property type="term" value="P:transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010500">
    <property type="entry name" value="Hepcidin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16877">
    <property type="entry name" value="HEPCIDIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16877:SF0">
    <property type="entry name" value="HEPCIDIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06446">
    <property type="entry name" value="Hepcidin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000013381" evidence="3">
    <location>
      <begin position="24"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000013382" description="Hepcidin">
    <location>
      <begin position="59"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="65"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="68"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="69"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="72"/>
      <end position="80"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P81172"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="11113132"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="11447267"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="12370282"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="12729891"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="15124018"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="24880340"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <sequence length="83" mass="9352" checksum="C76423EA46260B18" modified="2001-03-01" version="1" precursor="true">MALSTRTQAACLLLLLLASLSSTTYLHQQMRQTTELQPLHGEESRADIAIPMQKRRKRDTNFPICIFCCKCCNNSQCGICCKT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>