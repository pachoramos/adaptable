<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-10-10" modified="2022-05-25" version="6" xmlns="http://uniprot.org/uniprot">
  <accession>C0HLC5</accession>
  <name>DRS31_PHYTB</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Dermaseptin-3.1TR</fullName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Phyllomedusa trinitatis</name>
    <name type="common">Trinidad leaf frog</name>
    <dbReference type="NCBI Taxonomy" id="332092"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phyllomedusa</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2018" name="Comp. Biochem. Physiol." volume="28" first="72" last="79">
      <title>Peptidomic analysis of the host-defense peptides in skin secretions of the Trinidadian leaf frog Phyllomedusa trinitatis (Phyllomedusidae).</title>
      <authorList>
        <person name="Mechkarska M."/>
        <person name="Coquet L."/>
        <person name="Leprince J."/>
        <person name="Auguste R.J."/>
        <person name="Jouenne T."/>
        <person name="Mangoni M.L."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="29980138"/>
      <dbReference type="DOI" id="10.1016/j.cbd.2018.06.006"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antimicrobial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3127.6" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HLC5"/>
  <dbReference type="SMR" id="C0HLC5"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000445213" description="Dermaseptin-3.1TR" evidence="2">
    <location>
      <begin position="1"/>
      <end position="30"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P84923"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="29980138"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="29980138"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="29980138"/>
    </source>
  </evidence>
  <sequence length="30" mass="3129" checksum="749990B043A43EF3" modified="2018-10-10" version="1">GLFKTLIKGAGKMLGHVAKQFLGSQGQPES</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>