<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-07-28" modified="2024-10-02" version="79" xmlns="http://uniprot.org/uniprot">
  <accession>Q2V364</accession>
  <name>DEF23_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Putative defensin-like protein 23</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">At5g19315</name>
    <name type="ORF">F7K24</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Nature" volume="408" first="823" last="826">
      <title>Sequence and analysis of chromosome 5 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Tabata S."/>
        <person name="Kaneko T."/>
        <person name="Nakamura Y."/>
        <person name="Kotani H."/>
        <person name="Kato T."/>
        <person name="Asamizu E."/>
        <person name="Miyajima N."/>
        <person name="Sasamoto S."/>
        <person name="Kimura T."/>
        <person name="Hosouchi T."/>
        <person name="Kawashima K."/>
        <person name="Kohara M."/>
        <person name="Matsumoto M."/>
        <person name="Matsuno A."/>
        <person name="Muraki A."/>
        <person name="Nakayama S."/>
        <person name="Nakazaki N."/>
        <person name="Naruo K."/>
        <person name="Okumura S."/>
        <person name="Shinpo S."/>
        <person name="Takeuchi C."/>
        <person name="Wada T."/>
        <person name="Watanabe A."/>
        <person name="Yamada M."/>
        <person name="Yasuda M."/>
        <person name="Sato S."/>
        <person name="de la Bastide M."/>
        <person name="Huang E."/>
        <person name="Spiegel L."/>
        <person name="Gnoj L."/>
        <person name="O'Shaughnessy A."/>
        <person name="Preston R."/>
        <person name="Habermann K."/>
        <person name="Murray J."/>
        <person name="Johnson D."/>
        <person name="Rohlfing T."/>
        <person name="Nelson J."/>
        <person name="Stoneking T."/>
        <person name="Pepin K."/>
        <person name="Spieth J."/>
        <person name="Sekhon M."/>
        <person name="Armstrong J."/>
        <person name="Becker M."/>
        <person name="Belter E."/>
        <person name="Cordum H."/>
        <person name="Cordes M."/>
        <person name="Courtney L."/>
        <person name="Courtney W."/>
        <person name="Dante M."/>
        <person name="Du H."/>
        <person name="Edwards J."/>
        <person name="Fryman J."/>
        <person name="Haakensen B."/>
        <person name="Lamar E."/>
        <person name="Latreille P."/>
        <person name="Leonard S."/>
        <person name="Meyer R."/>
        <person name="Mulvaney E."/>
        <person name="Ozersky P."/>
        <person name="Riley A."/>
        <person name="Strowmatt C."/>
        <person name="Wagner-McPherson C."/>
        <person name="Wollam A."/>
        <person name="Yoakum M."/>
        <person name="Bell M."/>
        <person name="Dedhia N."/>
        <person name="Parnell L."/>
        <person name="Shah R."/>
        <person name="Rodriguez M."/>
        <person name="Hoon See L."/>
        <person name="Vil D."/>
        <person name="Baker J."/>
        <person name="Kirchoff K."/>
        <person name="Toth K."/>
        <person name="King L."/>
        <person name="Bahret A."/>
        <person name="Miller B."/>
        <person name="Marra M.A."/>
        <person name="Martienssen R."/>
        <person name="McCombie W.R."/>
        <person name="Wilson R.K."/>
        <person name="Murphy G."/>
        <person name="Bancroft I."/>
        <person name="Volckaert G."/>
        <person name="Wambutt R."/>
        <person name="Duesterhoeft A."/>
        <person name="Stiekema W."/>
        <person name="Pohl T."/>
        <person name="Entian K.-D."/>
        <person name="Terryn N."/>
        <person name="Hartley N."/>
        <person name="Bent E."/>
        <person name="Johnson S."/>
        <person name="Langham S.-A."/>
        <person name="McCullagh B."/>
        <person name="Robben J."/>
        <person name="Grymonprez B."/>
        <person name="Zimmermann W."/>
        <person name="Ramsperger U."/>
        <person name="Wedler H."/>
        <person name="Balke K."/>
        <person name="Wedler E."/>
        <person name="Peters S."/>
        <person name="van Staveren M."/>
        <person name="Dirkse W."/>
        <person name="Mooijman P."/>
        <person name="Klein Lankhorst R."/>
        <person name="Weitzenegger T."/>
        <person name="Bothe G."/>
        <person name="Rose M."/>
        <person name="Hauf J."/>
        <person name="Berneiser S."/>
        <person name="Hempel S."/>
        <person name="Feldpausch M."/>
        <person name="Lamberth S."/>
        <person name="Villarroel R."/>
        <person name="Gielen J."/>
        <person name="Ardiles W."/>
        <person name="Bents O."/>
        <person name="Lemcke K."/>
        <person name="Kolesov G."/>
        <person name="Mayer K.F.X."/>
        <person name="Rudd S."/>
        <person name="Schoof H."/>
        <person name="Schueller C."/>
        <person name="Zaccaria P."/>
        <person name="Mewes H.-W."/>
        <person name="Bevan M."/>
        <person name="Fransz P.F."/>
      </authorList>
      <dbReference type="PubMed" id="11130714"/>
      <dbReference type="DOI" id="10.1038/35048507"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="EMBL" id="AF296837">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002688">
    <property type="protein sequence ID" value="AED92684.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001031903.1">
    <property type="nucleotide sequence ID" value="NM_001036826.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q2V364"/>
  <dbReference type="SMR" id="Q2V364"/>
  <dbReference type="PaxDb" id="3702-AT5G19315.1"/>
  <dbReference type="EnsemblPlants" id="AT5G19315.1">
    <property type="protein sequence ID" value="AT5G19315.1"/>
    <property type="gene ID" value="AT5G19315"/>
  </dbReference>
  <dbReference type="GeneID" id="3770682"/>
  <dbReference type="Gramene" id="AT5G19315.1">
    <property type="protein sequence ID" value="AT5G19315.1"/>
    <property type="gene ID" value="AT5G19315"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT5G19315"/>
  <dbReference type="Araport" id="AT5G19315"/>
  <dbReference type="TAIR" id="AT5G19315"/>
  <dbReference type="HOGENOM" id="CLU_185732_0_0_1"/>
  <dbReference type="InParanoid" id="Q2V364"/>
  <dbReference type="OMA" id="KRCNKWC"/>
  <dbReference type="OrthoDB" id="689130at2759"/>
  <dbReference type="PhylomeDB" id="Q2V364"/>
  <dbReference type="PRO" id="PR:Q2V364"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 5"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q2V364">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022618">
    <property type="entry name" value="Defensin-like_20-28"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34453">
    <property type="entry name" value="DEFENSIN-LIKE (DEFL) FAMILY PROTEIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34453:SF6">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 23-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF10868">
    <property type="entry name" value="Defensin_like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000379605" description="Putative defensin-like protein 23">
    <location>
      <begin position="26"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="41"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="50"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="54"/>
      <end position="78"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="80" mass="8848" checksum="DD56D117300DFA4F" modified="2006-01-10" version="1" precursor="true">MTTTMKIMSFAMLLVLLFSIDVVEGSGSSLCCNTHAKFGACNTYQDRKRCNKWCLDGCDNKKGGFCKRFAGGAKKCHCYC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>