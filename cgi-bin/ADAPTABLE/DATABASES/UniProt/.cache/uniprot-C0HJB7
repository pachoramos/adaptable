<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-06-26" modified="2022-12-14" version="14" xmlns="http://uniprot.org/uniprot">
  <accession>C0HJB7</accession>
  <name>H2A_EUPCP</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Buforin-EC</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Euphlyctis cyanophlyctis</name>
    <name type="common">Skittering frog</name>
    <dbReference type="NCBI Taxonomy" id="58519"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Dicroglossidae</taxon>
      <taxon>Dicroglossinae</taxon>
      <taxon>Euphlyctis</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2012" name="Int. J. Pept. Res. Ther." volume="18" first="107" last="115">
      <title>Identification and characterization of novel antibacterial peptides from skin secretions of Euphlyctis cyanophlyctis.</title>
      <authorList>
        <person name="Asoodeh A."/>
        <person name="Ghorani-Azam A."/>
        <person name="Chamani J.K."/>
      </authorList>
      <dbReference type="DOI" id="10.1007/s10989-011-9284-6"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="2">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Has antibacterial activity against E.coli HP101BA (MIC=9.6 uM), K.pneumoniae PTCC1388 (MIC=10.6 uM), M.luteus PTCC1625 (MIC=6.0 uM) and S.aureus PTCC1431 (MIC=8.1 uM). Has no or very limited (&lt;3%) hemolytic activity at concentrations of 15 ug/ml and 60 ug/ml, respectively.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1875.05" error="0.5" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the histone H2A family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HJB7"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046982">
    <property type="term" value="F:protein heterodimerization activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009072">
    <property type="entry name" value="Histone-fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR032458">
    <property type="entry name" value="Histone_H2A_CS"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47113">
    <property type="entry name" value="Histone-fold"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00046">
    <property type="entry name" value="HISTONE_H2A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000422916" description="Buforin-EC" evidence="3">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source ref="1"/>
  </evidence>
  <sequence length="16" mass="1875" checksum="E02C61B2F8D91F4A" modified="2013-06-26" version="1">RAGLKFPVGRVHRLLR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>