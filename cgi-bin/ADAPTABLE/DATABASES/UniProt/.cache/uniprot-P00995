<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="222" xmlns="http://uniprot.org/uniprot">
  <accession>P00995</accession>
  <name>ISK1_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="14">Serine protease inhibitor Kazal-type 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="12">Pancreatic secretory trypsin inhibitor</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Tumor-associated trypsin inhibitor</fullName>
      <shortName>TATI</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">SPINK1</name>
    <name type="synonym">PSTI</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1987" name="Biochem. Biophys. Res. Commun." volume="149" first="635" last="641">
      <title>Primary structure of human pancreatic secretory trypsin inhibitor (PSTI) gene.</title>
      <authorList>
        <person name="Horii A."/>
        <person name="Kobayashi T."/>
        <person name="Tomita N."/>
        <person name="Yamamoto T."/>
        <person name="Fukushige S."/>
        <person name="Murotsu T."/>
        <person name="Ogawa M."/>
        <person name="Mori T."/>
        <person name="Matsubara K."/>
      </authorList>
      <dbReference type="PubMed" id="3501289"/>
      <dbReference type="DOI" id="10.1016/0006-291x(87)90415-3"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1985" name="Biochem. Biophys. Res. Commun." volume="132" first="605" last="612">
      <title>Molecular cloning and nucleotide sequence of human pancreatic secretory trypsin inhibitor (PSTI) cDNA.</title>
      <authorList>
        <person name="Yamamoto T."/>
        <person name="Nakamura Y."/>
        <person name="Nishide T."/>
        <person name="Emi M."/>
        <person name="Ogawa M."/>
        <person name="Mori T."/>
        <person name="Matsubara K."/>
      </authorList>
      <dbReference type="PubMed" id="3877508"/>
      <dbReference type="DOI" id="10.1016/0006-291x(85)91176-3"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1987" name="FEBS Lett." volume="225" first="113" last="119">
      <title>Expression of pancreatic secretory trypsin inhibitor gene in neoplastic tissues.</title>
      <authorList>
        <person name="Tomita N."/>
        <person name="Horii A."/>
        <person name="Yamamoto T."/>
        <person name="Ogawa M."/>
        <person name="Mori T."/>
        <person name="Matsubara K."/>
      </authorList>
      <dbReference type="PubMed" id="2961612"/>
      <dbReference type="DOI" id="10.1016/0014-5793(87)81141-9"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2000" name="Nat. Genet." volume="25" first="213" last="216">
      <title>Mutations in the gene encoding the serine protease inhibitor, Kazal type 1 are associated with chronic pancreatitis.</title>
      <authorList>
        <person name="Witt H."/>
        <person name="Luck W."/>
        <person name="Hennies H.C."/>
        <person name="Classen M."/>
        <person name="Kage A."/>
        <person name="Lass U."/>
        <person name="Landt O."/>
        <person name="Becker M."/>
      </authorList>
      <dbReference type="PubMed" id="10835640"/>
      <dbReference type="DOI" id="10.1038/76088"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>VARIANTS PCTT PRO-14 AND SER-34</scope>
    <scope>VARIANT SER-55</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Pancreas</tissue>
      <tissue>Spleen</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1977" name="Arch. Biochem. Biophys." volume="179" first="189" last="199">
      <title>The primary structure of the human pancreatic secretory trypsin inhibitor. Amino acid sequence of the reduced S-aminoethylated protein.</title>
      <authorList>
        <person name="Bartelt D.C."/>
        <person name="Shapanka R."/>
        <person name="Greene L.J."/>
      </authorList>
      <dbReference type="PubMed" id="843082"/>
      <dbReference type="DOI" id="10.1016/0003-9861(77)90103-5"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 24-79</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1982" name="J. Biol. Chem." volume="257" first="13713" last="13716">
      <title>Purification and characterization of a tumor-associated trypsin inhibitor from the urine of a patient with ovarian cancer.</title>
      <authorList>
        <person name="Huhtala M.-L."/>
        <person name="Pesonen K."/>
        <person name="Kalkkinen N."/>
        <person name="Stenman U.-H."/>
      </authorList>
      <dbReference type="PubMed" id="7142173"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)33505-1"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 24-46</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="1992" name="J. Mol. Biol." volume="225" first="1095" last="1103">
      <title>Three-dimensional structure of a recombinant variant of human pancreatic secretory trypsin inhibitor (Kazal type).</title>
      <authorList>
        <person name="Hecht H.-J."/>
        <person name="Szardenings M."/>
        <person name="Collins J."/>
        <person name="Schomburg D."/>
      </authorList>
      <dbReference type="PubMed" id="1613792"/>
      <dbReference type="DOI" id="10.1016/0022-2836(92)90107-u"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.3 ANGSTROMS)</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="1993" name="J. Mol. Biol." volume="229" first="695" last="706">
      <title>Solution structure of a variant of human pancreatic secretory trypsin inhibitor determined by nuclear magnetic resonance spectroscopy.</title>
      <authorList>
        <person name="Klaus W."/>
        <person name="Schomburg D."/>
      </authorList>
      <dbReference type="PubMed" id="8433367"/>
      <dbReference type="DOI" id="10.1006/jmbi.1993.1073"/>
    </citation>
    <scope>STRUCTURE BY NMR OF MUTANT LEU-41/ARG-44</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2000" name="J. Med. Genet." volume="37" first="67" last="69">
      <title>Mutational analysis of the human pancreatic secretory trypsin inhibitor (PSTI) gene in hereditary and sporadic chronic pancreatitis.</title>
      <authorList>
        <person name="Chen J.-M."/>
        <person name="Mercier B."/>
        <person name="Audrezet M.-P."/>
        <person name="Ferec C."/>
      </authorList>
      <dbReference type="PubMed" id="10691414"/>
      <dbReference type="DOI" id="10.1136/jmg.37.1.67"/>
    </citation>
    <scope>VARIANT PCTT SER-34</scope>
    <scope>VARIANT SER-55</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2002" name="Am. J. Hum. Genet." volume="71" first="964" last="968">
      <title>SPINK1 is a susceptibility gene for fibrocalculous pancreatic diabetes in subjects from the Indian subcontinent.</title>
      <authorList>
        <person name="Hassan Z."/>
        <person name="Mohan V."/>
        <person name="Ali L."/>
        <person name="Allotey R."/>
        <person name="Barakat K."/>
        <person name="Faruque M.O."/>
        <person name="Deepa R."/>
        <person name="McDermott M.F."/>
        <person name="Jackson A.E."/>
        <person name="Cassell P."/>
        <person name="Curtis D."/>
        <person name="Gelding S.V."/>
        <person name="Vijayaravaghan S."/>
        <person name="Gyr N."/>
        <person name="Whitcomb D.C."/>
        <person name="Azad Khan A.K."/>
        <person name="Hitman G.A."/>
      </authorList>
      <dbReference type="PubMed" id="12187509"/>
      <dbReference type="DOI" id="10.1086/342731"/>
    </citation>
    <scope>VARIANT TCP SER-34</scope>
    <scope>INVOLVEMENT IN FIBROCALCULOUS PANCREATIC DIABETES</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2002" name="J. Med. Genet." volume="39" first="347" last="351">
      <title>Mutations in the pancreatic secretory trypsin inhibitor gene (PSTI/SPINK1) rather than the cationic trypsinogen gene (PRSS1) are significantly associated with tropical calcific pancreatitis.</title>
      <authorList>
        <person name="Chandak G.R."/>
        <person name="Idris M.M."/>
        <person name="Reddy D.N."/>
        <person name="Bhaskar S."/>
        <person name="Sriram P.V.J."/>
        <person name="Singh L."/>
      </authorList>
      <dbReference type="PubMed" id="12011155"/>
      <dbReference type="DOI" id="10.1136/jmg.39.5.347"/>
    </citation>
    <scope>VARIANT TCP SER-34</scope>
    <scope>VARIANT SER-55</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2003" name="Hum. Genet." volume="113" first="369" last="369">
      <title>Gene symbol: Spink1-Omim 167790. Disease: hereditary pancreatitis.</title>
      <authorList>
        <person name="Deybach J.-C.D."/>
        <person name="Phung L."/>
        <person name="Lamoril J."/>
        <person name="Bouizegarene P."/>
        <person name="Levy P."/>
        <person name="Deybach J.-C."/>
        <person name="Ruszniewski P."/>
      </authorList>
      <dbReference type="PubMed" id="12974284"/>
      <dbReference type="DOI" id="10.1007/s00439-003-0996-3"/>
    </citation>
    <scope>VARIANT PCTT PHE-12</scope>
  </reference>
  <reference key="14">
    <citation type="journal article" date="2008" name="Eur. J. Gastroenterol. Hepatol." volume="20" first="726" last="731">
      <title>The SPINK1 N34S variant is associated with acute pancreatitis.</title>
      <authorList>
        <person name="O'Reilly D.A."/>
        <person name="Witt H."/>
        <person name="Rahman S.H."/>
        <person name="Schulz H.U."/>
        <person name="Sargen K."/>
        <person name="Kage A."/>
        <person name="Cartmell M.T."/>
        <person name="Landt O."/>
        <person name="Larvin M."/>
        <person name="Demaine A.G."/>
        <person name="McMahon M.J."/>
        <person name="Becker M."/>
        <person name="Kingsnorth A.N."/>
      </authorList>
      <dbReference type="PubMed" id="18617776"/>
      <dbReference type="DOI" id="10.1097/meg.0b013e3282f5728c"/>
    </citation>
    <scope>VARIANT PCTT SER-34</scope>
    <scope>VARIANT SER-55</scope>
  </reference>
  <reference key="15">
    <citation type="journal article" date="2010" name="Am. J. Gastroenterol." volume="105" first="446" last="451">
      <title>SPINK1 N34S is strongly associated with recurrent acute pancreatitis but is not a risk factor for the first or sentinel acute pancreatitis event.</title>
      <authorList>
        <person name="Aoun E."/>
        <person name="Muddana V."/>
        <person name="Papachristou G.I."/>
        <person name="Whitcomb D.C."/>
      </authorList>
      <dbReference type="PubMed" id="19888199"/>
      <dbReference type="DOI" id="10.1038/ajg.2009.630"/>
    </citation>
    <scope>VARIANT PCTT SER-34</scope>
  </reference>
  <comment type="function">
    <text evidence="1 10">Serine protease inhibitor which exhibits anti-trypsin activity (PubMed:7142173). In the pancreas, protects against trypsin-catalyzed premature activation of zymogens (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="1">In the male reproductive tract, binds to sperm heads where it modulates sperm capacitance by inhibiting calcium uptake and nitrogen oxide (NO) production.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-8054204">
      <id>P00995</id>
    </interactant>
    <interactant intactId="EBI-12092171">
      <id>Q12797-6</id>
      <label>ASPH</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="10">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="disease" evidence="3 4 7 8 9">
    <disease id="DI-01731">
      <name>Pancreatitis, hereditary</name>
      <acronym>PCTT</acronym>
      <description>A disease characterized by pancreas inflammation, permanent destruction of the pancreatic parenchyma, maldigestion, and severe abdominal pain attacks.</description>
      <dbReference type="MIM" id="167800"/>
    </disease>
    <text>Disease susceptibility is associated with variants affecting the gene represented in this entry.</text>
  </comment>
  <comment type="disease" evidence="5 6">
    <disease id="DI-02394">
      <name>Tropical calcific pancreatitis</name>
      <acronym>TCP</acronym>
      <description>Idiopathic, juvenile, nonalcoholic form of chronic pancreatitis widely prevalent in several tropical countries. It can be associated with fibrocalculous pancreatic diabetes (FCPD) depending on both environmental and genetic factors. TCP differs from alcoholic pancreatitis by a much younger age of onset, pancreatic calcification, a high incidence of insulin dependent but ketosis resistant diabetes mellitus, and an exceptionally high incidence of pancreatic cancer.</description>
      <dbReference type="MIM" id="608189"/>
    </disease>
    <text>Disease susceptibility is associated with variants affecting the gene represented in this entry.</text>
  </comment>
  <dbReference type="EMBL" id="M20530">
    <property type="protein sequence ID" value="AAA36522.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M22971">
    <property type="protein sequence ID" value="AAA36522.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M20528">
    <property type="protein sequence ID" value="AAA36522.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M20529">
    <property type="protein sequence ID" value="AAA36522.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Y00705">
    <property type="protein sequence ID" value="CAA68697.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M11949">
    <property type="protein sequence ID" value="AAA36521.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF286028">
    <property type="protein sequence ID" value="AAG00531.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC025790">
    <property type="protein sequence ID" value="AAH25790.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS4286.1"/>
  <dbReference type="PIR" id="A27484">
    <property type="entry name" value="TIHUA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_003113.2">
    <property type="nucleotide sequence ID" value="NM_003122.4"/>
  </dbReference>
  <dbReference type="PDB" id="1CGI">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="I=24-79"/>
  </dbReference>
  <dbReference type="PDB" id="1CGJ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="I=24-79"/>
  </dbReference>
  <dbReference type="PDB" id="1HPT">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="A=24-79"/>
  </dbReference>
  <dbReference type="PDB" id="7QE8">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.90 A"/>
    <property type="chains" value="C/D=24-79"/>
  </dbReference>
  <dbReference type="PDB" id="7QE9">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.10 A"/>
    <property type="chains" value="C/D=24-79"/>
  </dbReference>
  <dbReference type="PDBsum" id="1CGI"/>
  <dbReference type="PDBsum" id="1CGJ"/>
  <dbReference type="PDBsum" id="1HPT"/>
  <dbReference type="PDBsum" id="7QE8"/>
  <dbReference type="PDBsum" id="7QE9"/>
  <dbReference type="AlphaFoldDB" id="P00995"/>
  <dbReference type="SMR" id="P00995"/>
  <dbReference type="BioGRID" id="112568">
    <property type="interactions" value="8"/>
  </dbReference>
  <dbReference type="IntAct" id="P00995">
    <property type="interactions" value="5"/>
  </dbReference>
  <dbReference type="MINT" id="P00995"/>
  <dbReference type="STRING" id="9606.ENSP00000296695"/>
  <dbReference type="MEROPS" id="I01.011"/>
  <dbReference type="iPTMnet" id="P00995"/>
  <dbReference type="PhosphoSitePlus" id="P00995"/>
  <dbReference type="BioMuta" id="SPINK1"/>
  <dbReference type="DMDM" id="124856"/>
  <dbReference type="jPOST" id="P00995"/>
  <dbReference type="MassIVE" id="P00995"/>
  <dbReference type="PaxDb" id="9606-ENSP00000296695"/>
  <dbReference type="PeptideAtlas" id="P00995"/>
  <dbReference type="ProteomicsDB" id="51298"/>
  <dbReference type="Antibodypedia" id="27654">
    <property type="antibodies" value="269 antibodies from 31 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="6690"/>
  <dbReference type="Ensembl" id="ENST00000296695.10">
    <property type="protein sequence ID" value="ENSP00000296695.5"/>
    <property type="gene ID" value="ENSG00000164266.11"/>
  </dbReference>
  <dbReference type="GeneID" id="6690"/>
  <dbReference type="KEGG" id="hsa:6690"/>
  <dbReference type="MANE-Select" id="ENST00000296695.10">
    <property type="protein sequence ID" value="ENSP00000296695.5"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_001379610.1"/>
    <property type="RefSeq protein sequence ID" value="NP_001366539.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc003los.3">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:11244"/>
  <dbReference type="CTD" id="6690"/>
  <dbReference type="DisGeNET" id="6690"/>
  <dbReference type="GeneCards" id="SPINK1"/>
  <dbReference type="GeneReviews" id="SPINK1"/>
  <dbReference type="HGNC" id="HGNC:11244">
    <property type="gene designation" value="SPINK1"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000164266">
    <property type="expression patterns" value="Tissue enriched (pancreas)"/>
  </dbReference>
  <dbReference type="MalaCards" id="SPINK1"/>
  <dbReference type="MIM" id="167790">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="MIM" id="167800">
    <property type="type" value="phenotype"/>
  </dbReference>
  <dbReference type="MIM" id="608189">
    <property type="type" value="phenotype"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P00995"/>
  <dbReference type="OpenTargets" id="ENSG00000164266"/>
  <dbReference type="Orphanet" id="676">
    <property type="disease" value="Hereditary chronic pancreatitis"/>
  </dbReference>
  <dbReference type="Orphanet" id="103918">
    <property type="disease" value="Tropical pancreatitis"/>
  </dbReference>
  <dbReference type="PharmGKB" id="PA36074"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000164266"/>
  <dbReference type="eggNOG" id="KOG3649">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00530000064228"/>
  <dbReference type="InParanoid" id="P00995"/>
  <dbReference type="OMA" id="REAKCNN"/>
  <dbReference type="OrthoDB" id="2920086at2759"/>
  <dbReference type="PhylomeDB" id="P00995"/>
  <dbReference type="PathwayCommons" id="P00995"/>
  <dbReference type="SignaLink" id="P00995"/>
  <dbReference type="BioGRID-ORCS" id="6690">
    <property type="hits" value="11 hits in 1143 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="SPINK1">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P00995"/>
  <dbReference type="GeneWiki" id="SPINK1"/>
  <dbReference type="GenomeRNAi" id="6690"/>
  <dbReference type="Pharos" id="P00995">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P00995"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 5"/>
  </dbReference>
  <dbReference type="RNAct" id="P00995">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000164266">
    <property type="expression patterns" value="Expressed in body of pancreas and 124 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P00995">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070062">
    <property type="term" value="C:extracellular exosome"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004866">
    <property type="term" value="F:endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071375">
    <property type="term" value="P:cellular response to peptide hormone stimulus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090281">
    <property type="term" value="P:negative regulation of calcium ion import"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010751">
    <property type="term" value="P:negative regulation of nitric oxide mediated signal transduction"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007263">
    <property type="term" value="P:nitric oxide mediated signal transduction"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007204">
    <property type="term" value="P:positive regulation of cytosolic calcium ion concentration"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050679">
    <property type="term" value="P:positive regulation of epithelial cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090187">
    <property type="term" value="P:positive regulation of pancreatic juice secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090277">
    <property type="term" value="P:positive regulation of peptide hormone secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060046">
    <property type="term" value="P:regulation of acrosome reaction"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:2001256">
    <property type="term" value="P:regulation of store-operated calcium entry"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045471">
    <property type="term" value="P:response to ethanol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031667">
    <property type="term" value="P:response to nutrient levels"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048240">
    <property type="term" value="P:sperm capacitation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="CDD" id="cd01327">
    <property type="entry name" value="KAZAL_PSTI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.60.30:FF:000031">
    <property type="entry name" value="Serine protease inhibitor Kazal-type 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.60.30">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002350">
    <property type="entry name" value="Kazal_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036058">
    <property type="entry name" value="Kazal_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001239">
    <property type="entry name" value="Prot_inh_Kazal-m"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21312">
    <property type="entry name" value="SERINE PROTEASE INHIBITOR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21312:SF27">
    <property type="entry name" value="SERINE PROTEASE INHIBITOR KAZAL-TYPE 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00050">
    <property type="entry name" value="Kazal_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00290">
    <property type="entry name" value="KAZALINHBTR"/>
  </dbReference>
  <dbReference type="SMART" id="SM00280">
    <property type="entry name" value="KAZAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF100895">
    <property type="entry name" value="Kazal-type serine protease inhibitors"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00282">
    <property type="entry name" value="KAZAL_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51465">
    <property type="entry name" value="KAZAL_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0225">Disease variant</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="10 11">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000016557" description="Serine protease inhibitor Kazal-type 1">
    <location>
      <begin position="24"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="domain" description="Kazal-like" evidence="2">
    <location>
      <begin position="26"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="site" description="Reactive bond for trypsin" evidence="1 2">
    <location>
      <begin position="41"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="site" description="Necessary for sperm binding" evidence="1">
    <location>
      <begin position="43"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="32"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="39"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="47"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_032011" description="In PCTT; benign; dbSNP:rs35877720." evidence="7">
    <original>L</original>
    <variation>F</variation>
    <location>
      <position position="12"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_011688" description="In PCTT; dbSNP:rs104893939." evidence="4">
    <original>L</original>
    <variation>P</variation>
    <location>
      <position position="14"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_011689" description="In PCTT and TCP; associated with disease susceptibility; risk factor also for acute pancreatitis; may confer susceptibility to fibrocalculous pancreatic diabetes; dbSNP:rs17107315." evidence="3 4 5 6">
    <original>N</original>
    <variation>S</variation>
    <location>
      <position position="34"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_011690" description="In dbSNP:rs111966833." evidence="3 4 5 8">
    <original>P</original>
    <variation>S</variation>
    <location>
      <position position="55"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_032012" description="In dbSNP:rs35523678.">
    <original>R</original>
    <variation>H</variation>
    <location>
      <position position="67"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 6; AA sequence and 7; AA sequence." evidence="13" ref="6 7">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="44"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 6; AA sequence." evidence="13" ref="6">
    <original>N</original>
    <variation>D</variation>
    <location>
      <position position="52"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; CAA68697." evidence="13" ref="3">
    <original>N</original>
    <variation>G</variation>
    <location>
      <position position="64"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="26"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="36"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="46"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="53"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="helix" evidence="16">
    <location>
      <begin position="57"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="73"/>
      <end position="77"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P09036"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00798"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="10691414"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="10835640"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="12011155"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="12187509"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="12974284"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="18617776"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="19888199"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="7142173"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="11">
    <source>
      <dbReference type="PubMed" id="843082"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="3501289"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13"/>
  <evidence type="ECO:0000312" key="14">
    <source>
      <dbReference type="HGNC" id="HGNC:11244"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="15">
    <source>
      <dbReference type="PDB" id="1HPT"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="16">
    <source>
      <dbReference type="PDB" id="7QE9"/>
    </source>
  </evidence>
  <sequence length="79" mass="8507" checksum="3583C8196952EB3A" modified="1989-07-01" version="2" precursor="true">MKVTGIFLLSALALLSLSGNTGADSLGREAKCYNELNGCTKIYDPVCGTDGNTYPNECVLCFENRKRQTSILIQKSGPC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>