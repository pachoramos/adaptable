<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-12-05" modified="2023-02-22" version="53" xmlns="http://uniprot.org/uniprot">
  <accession>P58446</accession>
  <name>VARA_VIOAR</name>
  <protein>
    <recommendedName>
      <fullName>Varv peptide A</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Viola arvensis</name>
    <name type="common">European field pansy</name>
    <name type="synonym">Field violet</name>
    <dbReference type="NCBI Taxonomy" id="97415"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Malpighiales</taxon>
      <taxon>Violaceae</taxon>
      <taxon>Viola</taxon>
      <taxon>Viola subgen. Viola</taxon>
      <taxon>Viola sect. Melanium</taxon>
      <taxon>Viola subsect. Bracteolatae</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="J. Nat. Prod." volume="61" first="77" last="81">
      <title>Fractionation protocol for the isolation of polypeptides from plant biomass.</title>
      <authorList>
        <person name="Claeson P."/>
        <person name="Goeransson U."/>
        <person name="Johansson S."/>
        <person name="Luijendijk T."/>
        <person name="Bohlin L."/>
      </authorList>
      <dbReference type="PubMed" id="9548831"/>
      <dbReference type="DOI" id="10.1021/np970342r"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="J. Nat. Prod." volume="67" first="144" last="147">
      <title>Cytotoxic cyclotides from Viola tricolor.</title>
      <authorList>
        <person name="Svangard E."/>
        <person name="Goransson U."/>
        <person name="Hocaoglu Z."/>
        <person name="Gullbo J."/>
        <person name="Larsson R."/>
        <person name="Claeson P."/>
        <person name="Bohlin L."/>
      </authorList>
      <dbReference type="PubMed" id="14987049"/>
      <dbReference type="DOI" id="10.1021/np030101l"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Mol. Cancer Ther." volume="1" first="365" last="369">
      <title>Cyclotides: a novel type of cytotoxic agents.</title>
      <authorList>
        <person name="Lindholm P."/>
        <person name="Goransson U."/>
        <person name="Johansson S."/>
        <person name="Claeson P."/>
        <person name="Gullbo J."/>
        <person name="Larsson R."/>
        <person name="Bohlin L."/>
        <person name="Backlund A."/>
      </authorList>
      <dbReference type="PubMed" id="12477048"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3">Probably participates in a plant defense mechanism. Has cytotoxic activity against a variety of drug-resistant and drug-sensitive human tumor cell lines, and against primary chronic lymphocytic leukemia cells. Has weak cytotoxic activity against primary ovarian carcinoma cells or normal lymphocytes.</text>
  </comment>
  <comment type="domain">
    <text>The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text>This is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="2878.0" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the cyclotide family. Moebius subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="4">This peptide is cyclic. The start position was chosen by similarity to OAK1 (kalata-B1) for which the DNA sequence is known.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P58446"/>
  <dbReference type="SMR" id="P58446"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012324">
    <property type="entry name" value="Cyclotide_moebius_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF037891">
    <property type="entry name" value="Cycloviolacin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60009">
    <property type="entry name" value="CYCLOTIDE_MOEBIUS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000043632" description="Varv peptide A">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="5"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="9"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="14"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Gly-Asn)">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12477048"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="14987049"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="29" mass="2902" checksum="3228B0EE3F82FA18" modified="2001-12-05" version="1">GLPVCGETCVGGTCNTPGCSCSWPVCTRN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>