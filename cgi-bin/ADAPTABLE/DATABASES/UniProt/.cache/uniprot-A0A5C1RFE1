<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-02-26" modified="2024-10-02" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>A0A5C1RFE1</accession>
  <name>ASC10_DIDFA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Glyoxylase-like domain-containing protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Ascochitine biosynthesis cluster protein 10</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="3" type="ORF">orf10</name>
  </gene>
  <organism>
    <name type="scientific">Didymella fabae</name>
    <name type="common">Leaf and pod spot disease fungus</name>
    <name type="synonym">Ascochyta fabae</name>
    <dbReference type="NCBI Taxonomy" id="372025"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Ascomycota</taxon>
      <taxon>Pezizomycotina</taxon>
      <taxon>Dothideomycetes</taxon>
      <taxon>Pleosporomycetidae</taxon>
      <taxon>Pleosporales</taxon>
      <taxon>Pleosporineae</taxon>
      <taxon>Didymellaceae</taxon>
      <taxon>Ascochyta</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2019" name="MSphere" volume="4">
      <title>Identification of a polyketide synthase gene responsible for ascochitine biosynthesis in Ascochyta fabae and its abrogation in sister taxa.</title>
      <authorList>
        <person name="Kim W."/>
        <person name="Lichtenzveig J."/>
        <person name="Syme R.A."/>
        <person name="Williams A.H."/>
        <person name="Peever T.L."/>
        <person name="Chen W."/>
      </authorList>
      <dbReference type="PubMed" id="31554725"/>
      <dbReference type="DOI" id="10.1128/msphere.00622-19"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
    <source>
      <strain>AF247/15</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 4">Glyoxylase-like domain-containing protein; part of the gene cluster that mediates the biosynthesis of the selective antifungal agent ascochitine, an o-quinone methide that plays a possible protective role against other microbial competitors in nature and is considered to be important for pathogenicity of legume-associated Didymella species (PubMed:31554725). The pathway probably begins with the synthesis of a keto-aldehyde intermediate by the ascochitine non-reducing polyketide synthase pksAC from successive condensations of 4 malonyl-CoA units, presumably with a simple acetyl-CoA starter unit (Probable). Release of the keto-aldehyde intermediate is consistent with the presence of the C-terminal reductive release domain (Probable). The HR-PKS (orf7) probably makes a diketide starter unit which is passed to the non-reducing polyketide synthase pksAC for further extension, producing ascochital and ascochitine (Probable). The aldehyde dehydrogenase (orf1), the 2-oxoglutarate-dependent dioxygenase (orf3) and the dehydrogenase (orf9) are probably involved in subsequent oxidations of methyl groups to the carboxylic acid of the heterocyclic ring (Probable). The ascochitine gene cluster also includes a gene encoding a short peptide with a cupin domain (orf2) that is often found in secondary metabolite gene clusters and which function has still to be determined (Probable).</text>
  </comment>
  <comment type="pathway">
    <text evidence="4">Mycotoxin biosynthesis.</text>
  </comment>
  <dbReference type="EMBL" id="MN052631">
    <property type="protein sequence ID" value="QEN17978.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A5C1RFE1"/>
  <dbReference type="SMR" id="A0A5C1RFE1"/>
  <dbReference type="Gene3D" id="3.10.180.10">
    <property type="entry name" value="2,3-Dihydroxybiphenyl 1,2-Dioxygenase, domain 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR052164">
    <property type="entry name" value="Anthracycline_SecMetBiosynth"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029068">
    <property type="entry name" value="Glyas_Bleomycin-R_OHBP_Dase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR053863">
    <property type="entry name" value="Glyoxy/Ble-like_N"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR037523">
    <property type="entry name" value="VOC"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33993">
    <property type="entry name" value="GLYOXALASE-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33993:SF2">
    <property type="entry name" value="VOC DOMAIN-CONTAINING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF22677">
    <property type="entry name" value="Ble-like_N"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54593">
    <property type="entry name" value="Glyoxalase/Bleomycin resistance protein/Dihydroxybiphenyl dioxygenase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51819">
    <property type="entry name" value="VOC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="predicted"/>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="chain" id="PRO_0000448995" description="Glyoxylase-like domain-containing protein">
    <location>
      <begin position="1"/>
      <end position="128"/>
    </location>
  </feature>
  <feature type="domain" description="VOC" evidence="1">
    <location>
      <begin position="6"/>
      <end position="125"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01163"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="31554725"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="31554725"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="31554725"/>
    </source>
  </evidence>
  <sequence length="128" mass="13820" checksum="FECA752283F0BD8B" modified="2019-11-13" version="1">MVLHGQISGIEIPATDVERAAKFYSDTFGWKFQAPANGTIPASKMQTFTAPGDIFPDEGVVSKVEEIPKSGAKFYINVDDLKATIEAVTKNGGKQLSDVISLGPHVPPFQFFHDTEGNTHAICTRPGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>