<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2022-05-25" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>P82039</accession>
  <name>UPE27_UPEMJ</name>
  <protein>
    <recommendedName>
      <fullName>Uperin-2.7</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Uperoleia mjobergii</name>
    <name type="common">Mjoberg's toadlet</name>
    <name type="synonym">Pseudophryne mjobergii</name>
    <dbReference type="NCBI Taxonomy" id="104954"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Myobatrachoidea</taxon>
      <taxon>Myobatrachidae</taxon>
      <taxon>Myobatrachinae</taxon>
      <taxon>Uperoleia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Aust. J. Chem." volume="49" first="1325" last="1331">
      <title>New antibiotic uperin peptides from the dorsal glands of the australian toadlet Uperoleia mjobergii.</title>
      <authorList>
        <person name="Bradford A.M."/>
        <person name="Bowie J.H."/>
        <person name="Tyler M.J."/>
        <person name="Wallace J.C."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin dorsal glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1948.0" method="FAB" evidence="1"/>
  <dbReference type="AlphaFoldDB" id="P82039"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013157">
    <property type="entry name" value="Aurein_antimicrobial_peptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08256">
    <property type="entry name" value="Antimicrobial20"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043852" description="Uperin-2.7">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source ref="1"/>
  </evidence>
  <sequence length="19" mass="1949" checksum="24E4F83A6BA35F21" modified="2000-05-30" version="1">GIIDIAKKLVGGIRNVLGI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>