<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-06-01" modified="2023-09-13" version="54" xmlns="http://uniprot.org/uniprot">
  <accession>P36962</accession>
  <name>LCGB_LACLL</name>
  <protein>
    <recommendedName>
      <fullName>Bacteriocin lactococcin-G subunit beta</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lactococcus lactis subsp. lactis</name>
    <name type="common">Streptococcus lactis</name>
    <dbReference type="NCBI Taxonomy" id="1360"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Lactococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="J. Bacteriol." volume="174" first="5686" last="5692">
      <title>A novel lactococcal bacteriocin whose activity depends on the complementary action of two peptides.</title>
      <authorList>
        <person name="Nissen-Meyer J."/>
        <person name="Holo H."/>
        <person name="Havarstein L.S."/>
        <person name="Sletten K."/>
        <person name="Nes I.F."/>
      </authorList>
      <dbReference type="PubMed" id="1512201"/>
      <dbReference type="DOI" id="10.1128/jb.174.17.5686-5692.1992"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <strain>LMG 2081</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Kills Lactococci.</text>
  </comment>
  <comment type="subunit">
    <text>Bacteriocin activity requires interaction of alpha and beta peptides in a molar ratio of 7:1 or 8:1 respectively.</text>
  </comment>
  <dbReference type="PIR" id="C44918">
    <property type="entry name" value="C44918"/>
  </dbReference>
  <dbReference type="PDB" id="2JPK">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-35"/>
  </dbReference>
  <dbReference type="PDB" id="2JPM">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-35"/>
  </dbReference>
  <dbReference type="PDBsum" id="2JPK"/>
  <dbReference type="PDBsum" id="2JPM"/>
  <dbReference type="AlphaFoldDB" id="P36962"/>
  <dbReference type="BMRB" id="P36962"/>
  <dbReference type="SMR" id="P36962"/>
  <dbReference type="TCDB" id="1.C.25.1.1">
    <property type="family name" value="the lactococcin g (lactococcin g) family"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P36962"/>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR021089">
    <property type="entry name" value="Bacteriocin_lactococcin-G"/>
  </dbReference>
  <dbReference type="NCBIfam" id="NF038034">
    <property type="entry name" value="lactGbeta_entB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11632">
    <property type="entry name" value="LcnG-beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0078">Bacteriocin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <feature type="peptide" id="PRO_0000044641" description="Bacteriocin lactococcin-G subunit beta">
    <location>
      <begin position="1"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="helix" evidence="1">
    <location>
      <begin position="5"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="turn" evidence="1">
    <location>
      <begin position="20"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="turn" evidence="1">
    <location>
      <begin position="24"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="helix" evidence="1">
    <location>
      <begin position="32"/>
      <end position="34"/>
    </location>
  </feature>
  <evidence type="ECO:0007829" key="1">
    <source>
      <dbReference type="PDB" id="2JPK"/>
    </source>
  </evidence>
  <sequence length="35" mass="4110" checksum="76109F8BB0C489D2" modified="1994-06-01" version="1">KKWGWLAWVDPAYEFIKGFGKGAIKEGNKDKWKNI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>