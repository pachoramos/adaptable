<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2016-10-05" modified="2024-11-27" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>C0HJV6</accession>
  <accession>A0A1B3Z579</accession>
  <name>CTX4_LACTA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Cytoinsectotoxin-4</fullName>
      <shortName evidence="3">CIT-4</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lachesana tarabaevi</name>
    <name type="common">Spider</name>
    <dbReference type="NCBI Taxonomy" id="379576"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Entelegynae incertae sedis</taxon>
      <taxon>Zodariidae</taxon>
      <taxon>Lachesana</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2016" name="Biochem. J." volume="473" first="2495" last="2506">
      <title>Lachesana tarabaevi, an expert in membrane-active toxins.</title>
      <authorList>
        <person name="Kuzmenkov A.I."/>
        <person name="Sachkova M.Y."/>
        <person name="Kovalchuk S.I."/>
        <person name="Grishin E.V."/>
        <person name="Vassilevski A.A."/>
      </authorList>
      <dbReference type="PubMed" id="27287558"/>
      <dbReference type="DOI" id="10.1042/bcj20160436"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 63-123</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>TOXIC DOSE</scope>
    <scope>AMIDATION AT PHE-123</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Insecticidal and antimicrobial peptide. Has insecticidal activity against larvae of flesh fly S.carnaria. Has antibacterial activity against Gram-positive bacterium B.subtilis B-501 (MIC=2.5 uM) and Gram-negative bacterium E.coli DH5alpha (MIC=10 uM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="5">The mature peptide may form alpha-helices which disrupt target cell membranes.</text>
  </comment>
  <comment type="mass spectrometry" mass="7128.9" method="MALDI" evidence="2"/>
  <comment type="toxic dose">
    <text evidence="2">LD(50) is 75 ug/g in larvae of flesh fly S.carnaria.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the cationic peptide 06 (cytoinsectotoxin) family.</text>
  </comment>
  <dbReference type="EMBL" id="KT591340">
    <property type="protein sequence ID" value="AOH73462.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="C0HJV6"/>
  <dbReference type="SMR" id="C0HJV6"/>
  <dbReference type="TCDB" id="1.C.138.1.2">
    <property type="family name" value="the m-zodatoxin-lt8a spider toxin (zst) family"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018802">
    <property type="entry name" value="Latarcin_precursor"/>
  </dbReference>
  <dbReference type="Pfam" id="PF10279">
    <property type="entry name" value="Latarcin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444423" evidence="2">
    <location>
      <begin position="20"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000437252" description="Cytoinsectotoxin-4" evidence="2">
    <location>
      <begin position="63"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="2">
    <location>
      <position position="123"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27287558"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="27287558"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="27287558"/>
    </source>
  </evidence>
  <sequence length="124" mass="14211" checksum="8FDA7569071139C7" modified="2018-06-20" version="2" precursor="true">MKCFILAAALVLAFACIAASEPAETENEDLDDLSDLEDEEWLDELEEAAEYLESLREFEESRGYKDYMSKAKDLYKDIKKDKRVKAVMKSSYMKEAKKLYKDNPVRDAYQVYKGVKAGGKLLFG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>