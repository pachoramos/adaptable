<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-03-08" modified="2022-05-25" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>P0CJ31</accession>
  <name>ASCA7_ASCTR</name>
  <protein>
    <recommendedName>
      <fullName>Ascaphin-7</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ascaphus truei</name>
    <name type="common">Coastal tailed frog</name>
    <dbReference type="NCBI Taxonomy" id="8439"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Ascaphidae</taxon>
      <taxon>Ascaphus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Biochem. Biophys. Res. Commun." volume="320" first="170" last="175">
      <title>The ascaphins: a family of antimicrobial peptides from the skin secretions of the most primitive extant frog, Ascaphus truei.</title>
      <authorList>
        <person name="Conlon J.M."/>
        <person name="Sonnevend A."/>
        <person name="Davidson C."/>
        <person name="Smith D.D."/>
        <person name="Nielsen P.F."/>
      </authorList>
      <dbReference type="PubMed" id="15207717"/>
      <dbReference type="DOI" id="10.1016/j.bbrc.2004.05.141"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide that shows higher potency against Gram-negative bacteria than against Gram-positive bacteria. Has a very week hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2573.5" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the ascaphin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0CJ31"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000406134" description="Ascaphin-7">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="15207717"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="24" mass="2559" checksum="3E69F30193A192F3" modified="2011-03-08" version="1">GFKDWIKGAAKKLIKTVASAIANQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>