<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-05-05" modified="2021-06-02" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>P85453</accession>
  <name>FAR6_LUCCU</name>
  <protein>
    <recommendedName>
      <fullName>FMRFamide-6</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">LucFMRFamide-6</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Lucilia cuprina</name>
    <name type="common">Green bottle fly</name>
    <name type="synonym">Australian sheep blowfly</name>
    <dbReference type="NCBI Taxonomy" id="7375"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Oestroidea</taxon>
      <taxon>Calliphoridae</taxon>
      <taxon>Luciliinae</taxon>
      <taxon>Lucilia</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2009" name="Gen. Comp. Endocrinol." volume="162" first="52" last="58">
      <title>Extended FMRFamides in dipteran insects: conservative expression in the neuroendocrine system is accompanied by rapid sequence evolution.</title>
      <authorList>
        <person name="Rahman M.M."/>
        <person name="Fromm B."/>
        <person name="Neupert S."/>
        <person name="Kreusch S."/>
        <person name="Predel R."/>
      </authorList>
      <dbReference type="PubMed" id="18789334"/>
      <dbReference type="DOI" id="10.1016/j.ygcen.2008.08.006"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT PHE-10</scope>
    <source>
      <strain evidence="2">Bangladesh</strain>
      <strain evidence="2">Goondiwindi</strain>
      <tissue evidence="2">Dorsal ganglionic sheath</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Detected in the thoracic perisympathetic organs in larvae, and the dorsal ganglionic sheath in adults (at protein level).</text>
  </comment>
  <comment type="mass spectrometry" mass="1128.53" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the FARP (FMRFamide related peptide) family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000371751" description="FMRFamide-6">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="2">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="18789334"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="18789334"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="10" mass="1129" checksum="CC810399C44AB5BD" modified="2009-05-05" version="1">AAASDNFMRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>