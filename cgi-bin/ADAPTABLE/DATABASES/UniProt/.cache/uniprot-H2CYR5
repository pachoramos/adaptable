<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-09-05" modified="2022-12-14" version="17" xmlns="http://uniprot.org/uniprot">
  <accession>H2CYR5</accession>
  <name>NDB4_PANCV</name>
  <protein>
    <recommendedName>
      <fullName>Cytotoxic linear peptide</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pandinus cavimanus</name>
    <name type="common">Tanzanian red clawed scorpion</name>
    <dbReference type="NCBI Taxonomy" id="217261"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Iurida</taxon>
      <taxon>Scorpionoidea</taxon>
      <taxon>Scorpionidae</taxon>
      <taxon>Pandininae</taxon>
      <taxon>Pandinus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2012" name="Proteomics" volume="12" first="313" last="328">
      <title>Molecular diversity of the telson and venom components from Pandinus cavimanus (Scorpionidae Latreille 1802): transcriptome, venomics and function.</title>
      <authorList>
        <person name="Diego-Garcia E."/>
        <person name="Peigneur S."/>
        <person name="Clynen E."/>
        <person name="Marien T."/>
        <person name="Czech L."/>
        <person name="Schoofs L."/>
        <person name="Tytgat J."/>
      </authorList>
      <dbReference type="PubMed" id="22121013"/>
      <dbReference type="DOI" id="10.1002/pmic.201100409"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Amphipathic peptide that has antibacterial activities.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="1">Forms an alpha-helical membrane channel in the prey.</text>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the non-disulfide-bridged peptide (NDBP) superfamily. Short antimicrobial peptide (group 4) family.</text>
  </comment>
  <dbReference type="EMBL" id="JN315745">
    <property type="protein sequence ID" value="AEX09223.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="H2CYR5"/>
  <dbReference type="SMR" id="H2CYR5"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000418797" description="Cytotoxic linear peptide">
    <location>
      <begin position="24"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000418798" evidence="1">
    <location>
      <begin position="40"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="site" description="Important for activity" evidence="1">
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="36"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="68" mass="7671" checksum="7C4D25C9748CC555" modified="2012-03-21" version="1" precursor="true">MKTQFAILLIALVLFQMFSQSEAFLGGLWKAMSNLLGKRGLNELDDLDELFDGEISQADIDFLKELMS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>