<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-03-29" modified="2024-07-24" version="91" xmlns="http://uniprot.org/uniprot">
  <accession>Q5G863</accession>
  <name>DEF1_PANTR</name>
  <protein>
    <recommendedName>
      <fullName>Neutrophil defensin 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, alpha 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFA1</name>
  </gene>
  <organism>
    <name type="scientific">Pan troglodytes</name>
    <name type="common">Chimpanzee</name>
    <dbReference type="NCBI Taxonomy" id="9598"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Pan</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Physiol. Genomics" volume="20" first="1" last="11">
      <title>Rapid evolution and diversification of mammalian alpha-defensins as revealed by comparative analysis of rodent and primate genes.</title>
      <authorList>
        <person name="Patil A."/>
        <person name="Hughes A.L."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="15494476"/>
      <dbReference type="DOI" id="10.1152/physiolgenomics.00150.2004"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Effector molecule of the innate immune system that acts via antibiotic-like properties against a broad array of infectious agents including bacteria, fungi, and viruses or by promoting the activation and maturation of some APCs. Interacts with the essential precursor of cell wall synthesis lipid II to inhibit bacterial cell wall synthesis. Inhibits adenovirus infection via inhibition of viral disassembly at the vertex region, thereby restricting the release of internal capsid protein pVI, which is required for endosomal membrane penetration during cell entry. In addition, interaction with adenovirus capsid leads to the redirection of viral particles to TLR4 thereby promoting a NLRP3-mediated inflammasome response and interleukin 1-beta (IL-1beta) release. Induces the production of proinflammatory cytokines including type I interferon (IFN) in plasmacytoid dendritic cells (pDCs) by triggering the degradation of NFKBIA and nuclear translocation of IRF1, both of which are required for activation of pDCs.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Tetramer. Dimer. Interacts with RETN.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="PTM">
    <text evidence="2">ADP-ribosylation drastically reduces cytotoxic and antibacterial activities, and enhances IL8 production.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the alpha-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY746437">
    <property type="protein sequence ID" value="AAW78340.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5G863"/>
  <dbReference type="SMR" id="Q5G863"/>
  <dbReference type="STRING" id="9598.ENSPTRP00000079419"/>
  <dbReference type="Ensembl" id="ENSPTRT00000084879.1">
    <property type="protein sequence ID" value="ENSPTRP00000079419.1"/>
    <property type="gene ID" value="ENSPTRG00000050573.1"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000153268"/>
  <dbReference type="InParanoid" id="Q5G863"/>
  <dbReference type="OMA" id="LRKNMAC"/>
  <dbReference type="Proteomes" id="UP000002277">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSPTRG00000050573">
    <property type="expression patterns" value="Expressed in bone marrow and 14 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005796">
    <property type="term" value="C:Golgi lumen"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051607">
    <property type="term" value="P:defense response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051673">
    <property type="term" value="P:disruption of plasma membrane integrity in another organism"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016327">
    <property type="entry name" value="Alpha-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006081">
    <property type="entry name" value="Alpha-defensin_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002366">
    <property type="entry name" value="Alpha-defensin_N"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006080">
    <property type="entry name" value="Beta/alpha-defensin_C"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876">
    <property type="entry name" value="ALPHA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876:SF19">
    <property type="entry name" value="NEUTROPHIL DEFENSIN 1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00323">
    <property type="entry name" value="Defensin_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00879">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001875">
    <property type="entry name" value="Alpha-defensin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01418">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00048">
    <property type="entry name" value="DEFSN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00269">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0013">ADP-ribosylation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0051">Antiviral defense</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000006775" evidence="1">
    <location>
      <begin position="20"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006776" description="Neutrophil defensin 1">
    <location>
      <begin position="65"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="modified residue" description="ADP-ribosylarginine; by ART1" evidence="1">
    <location>
      <position position="78"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphotyrosine" evidence="2">
    <location>
      <position position="85"/>
    </location>
  </feature>
  <feature type="modified residue" description="ADP-ribosylarginine; by ART1" evidence="1">
    <location>
      <position position="88"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="66"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="68"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="73"/>
      <end position="93"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P59665"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="94" mass="10196" checksum="EF49989564D4391B" modified="2005-03-01" version="1" precursor="true">MRTLAILAAILLVALQAQAEPLQARADEVAAAPEQIPADNPEVVVSLAWDESLAPKHPGSRKNVACYCRIPACLAGERRYGTCIYQGRLWAFCC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>