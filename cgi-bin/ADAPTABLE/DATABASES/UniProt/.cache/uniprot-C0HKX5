<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-08-30" modified="2023-05-03" version="6" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKX5</accession>
  <name>APD_DOLMA</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Apidaecin +</fullName>
    </recommendedName>
  </protein>
  <organism evidence="2">
    <name type="scientific">Dolichovespula maculata</name>
    <name type="common">Bald-faced hornet</name>
    <name type="synonym">Vespula maculata</name>
    <dbReference type="NCBI Taxonomy" id="7441"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Vespinae</taxon>
      <taxon>Dolichovespula</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="1994" name="J. Biol. Chem." volume="269" first="26107" last="26115">
      <title>Biodiversity of apidaecin-type peptide antibiotics. Prospects of manipulating the antibacterial spectrum and combating acquired resistance.</title>
      <authorList>
        <person name="Casteels P."/>
        <person name="Romagnolo J."/>
        <person name="Castle M."/>
        <person name="Casteels-Josson K."/>
        <person name="Erdjument-Bromage H."/>
        <person name="Tempst P."/>
      </authorList>
      <dbReference type="PubMed" id="7929322"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)47165-7"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="2">Hemolymph</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide active against many Gram-negative enterobacterial and plant-associated bacterial species. Not active against other bacterial species like H.pylori, P.mirabilis, B.pertussis or N.gonorrhoeae.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text evidence="1">By bacterial infection.</text>
  </comment>
  <comment type="mass spectrometry" mass="1960.0" method="MALDI" evidence="1">
    <text>Apidaecin Ho +.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the apidaecin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HKX5"/>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000441345" description="Apidaecin +" evidence="1">
    <location>
      <begin position="1"/>
      <end position="17"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="7929322"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="7929322"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="17" mass="1957" checksum="BBAA8D70E720AB5F" modified="2017-08-30" version="1">GKPRPQQVPPRPPHPRL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>