<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-08-30" modified="2022-12-14" version="8" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKJ1</accession>
  <name>CYMEI_MELDN</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Cyclotide mden-I</fullName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Melicytus dentatus</name>
    <name type="common">Tree violet</name>
    <dbReference type="NCBI Taxonomy" id="491106"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Malpighiales</taxon>
      <taxon>Violaceae</taxon>
      <taxon>Melicytus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2017" name="J. Nat. Prod." volume="80" first="1522" last="1530">
      <title>Understanding the Diversity and Distribution of Cyclotides from Plants of Varied Genetic Origin.</title>
      <authorList>
        <person name="Ravipati A.S."/>
        <person name="Poth A.G."/>
        <person name="Troeira Henriques S."/>
        <person name="Bhandari M."/>
        <person name="Huang Y.H."/>
        <person name="Nino J."/>
        <person name="Colgrave M.L."/>
        <person name="Craik D.J."/>
      </authorList>
      <dbReference type="PubMed" id="28471681"/>
      <dbReference type="DOI" id="10.1021/acs.jnatprod.7b00061"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Probably participates in a plant defense mechanism.</text>
  </comment>
  <comment type="domain">
    <text evidence="4">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">This is a cyclic peptide.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the cyclotide family. Bracelet subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="1">This peptide is cyclic. The start position was chosen by similarity to Oak1 (kalata B1) for which the DNA sequence is known.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HKJ1"/>
  <dbReference type="SMR" id="C0HKJ1"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012323">
    <property type="entry name" value="Cyclotide_bracelet_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF037891">
    <property type="entry name" value="Cycloviolacin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60008">
    <property type="entry name" value="CYCLOTIDE_BRACELET"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000441357" description="Cyclotide mden-I" evidence="2">
    <location>
      <begin position="1"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="8"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="13"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Gly-Asn)" evidence="3">
    <location>
      <begin position="1"/>
      <end position="30"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="28471681"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="28471681"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="30" mass="3197" checksum="B9819A52E0CDE8C8" modified="2017-08-30" version="1">GIPCGESCVYIPCITTAIGCSCKNKVCYRN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>