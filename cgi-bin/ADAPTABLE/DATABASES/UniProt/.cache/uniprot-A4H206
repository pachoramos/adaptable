<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-29" modified="2024-10-02" version="51" xmlns="http://uniprot.org/uniprot">
  <accession>A4H206</accession>
  <name>D105A_GORGO</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 105A</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 105</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin, beta 105A</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB105A</name>
    <name type="synonym">DEFB105</name>
  </gene>
  <organism>
    <name type="scientific">Gorilla gorilla gorilla</name>
    <name type="common">Western lowland gorilla</name>
    <dbReference type="NCBI Taxonomy" id="9595"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Gorilla</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2006-11" db="EMBL/GenBank/DDBJ databases">
      <title>Evolution and sequence variation of human beta-defensin genes.</title>
      <authorList>
        <person name="Hollox E.J."/>
        <person name="Armour J.A.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has antimicrobial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AM410111">
    <property type="protein sequence ID" value="CAL68926.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_004046656.1">
    <property type="nucleotide sequence ID" value="XM_004046608.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A4H206"/>
  <dbReference type="SMR" id="A4H206"/>
  <dbReference type="STRING" id="9593.ENSGGOP00000006867"/>
  <dbReference type="Ensembl" id="ENSGGOT00000007048.3">
    <property type="protein sequence ID" value="ENSGGOP00000006867.2"/>
    <property type="gene ID" value="ENSGGOG00000007021.3"/>
  </dbReference>
  <dbReference type="GeneID" id="101127763"/>
  <dbReference type="KEGG" id="ggo:101127763"/>
  <dbReference type="eggNOG" id="ENOG502TE24">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000002317"/>
  <dbReference type="HOGENOM" id="CLU_197691_0_0_1"/>
  <dbReference type="InParanoid" id="A4H206"/>
  <dbReference type="OMA" id="RKMFYFV"/>
  <dbReference type="OrthoDB" id="4838921at2759"/>
  <dbReference type="Proteomes" id="UP000001519">
    <property type="component" value="Chromosome 8"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025933">
    <property type="entry name" value="Beta_defensin_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13841">
    <property type="entry name" value="Defensin_beta_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000289811" description="Beta-defensin 105A">
    <location>
      <begin position="28"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="53"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="57"/>
      <end position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="78" mass="8890" checksum="B4F42B15E3A41A05" modified="2007-05-01" version="1" precursor="true">MALIRKTFYFLFAVFFILVQLPSGCQAGLDFSQPFPSGEFAVCESCKLGRGKCRKECLENEKPDGNCRLNFLCCRQRI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>