<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-01-25" modified="2022-05-25" version="14" xmlns="http://uniprot.org/uniprot">
  <accession>G3ETQ2</accession>
  <name>JNG1A_AMOJI</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Jindongenin-1a</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="7">Jindongenin-1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Amolops jingdongensis</name>
    <name type="common">Chinese torrent frog</name>
    <dbReference type="NCBI Taxonomy" id="1077530"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Amolops</taxon>
    </lineage>
  </organism>
  <reference evidence="5 7" key="1">
    <citation type="journal article" date="2012" name="Biochimie" volume="94" first="328" last="334">
      <title>Two novel families of antimicrobial peptides from skin secretions of the Chinese torrent frog, Amolops jingdongensis.</title>
      <authorList>
        <person name="Chen Z."/>
        <person name="Yang X."/>
        <person name="Liu Z."/>
        <person name="Zeng L."/>
        <person name="Lee W."/>
        <person name="Zhang Y."/>
      </authorList>
      <dbReference type="PubMed" id="21816202"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2011.07.021"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 43-66</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>SYNTHESIS</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="7">Skin</tissue>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Displays broad-spectrum antibacterial activity against a range of Gram-positive and Gram-negative bacteria. Also displays antifungal activity against C.albicans ATCC 2002. Has low hemolytic activity, low cytotoxicity and low antioxidant activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2594.8" method="FAB" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="JF412290">
    <property type="protein sequence ID" value="AEN14487.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="G3ETQ2"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016209">
    <property type="term" value="F:antioxidant activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044179">
    <property type="term" value="P:hemolysis in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0049">Antioxidant</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000415404" evidence="2 4">
    <location>
      <begin position="23"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000415405" description="Jindongenin-1a" evidence="3">
    <location>
      <begin position="43"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="60"/>
      <end position="66"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A7WNV6"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="21816202"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="21816202"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="21816202"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="EMBL" id="AEN14487.1"/>
    </source>
  </evidence>
  <sequence length="66" mass="7392" checksum="D227FB20F62AE4FF" modified="2011-11-16" version="1" precursor="true">MFTLKKPLLLLFFLGTVSLSLCEQERAADDDEGEVIEEEVKRDSMGAVKLAKLLIDKMKCEVTKAC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>