<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-04-01" modified="2024-11-27" version="106" xmlns="http://uniprot.org/uniprot">
  <accession>P15223</accession>
  <accession>Q26460</accession>
  <accession>Q6V4Z2</accession>
  <name>SCX1_CENNO</name>
  <protein>
    <recommendedName>
      <fullName>Toxin Cn1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Toxin II.14</fullName>
      <shortName>Toxin 1</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Centruroides noxius</name>
    <name type="common">Mexican scorpion</name>
    <dbReference type="NCBI Taxonomy" id="6878"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Centruroides</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Toxicon" volume="33" first="1161" last="1170">
      <title>Cloning and characterization of the cDNAs encoding Na+ channel-specific toxins 1 and 2 of the scorpion Centruroides noxius Hoffmann.</title>
      <authorList>
        <person name="Vazquez A."/>
        <person name="Tapia J.V."/>
        <person name="Eliason W.K."/>
        <person name="Martin B.M."/>
        <person name="Lebreton F."/>
        <person name="Delepierre M."/>
        <person name="Possani L.D."/>
        <person name="Becerril B."/>
      </authorList>
      <dbReference type="PubMed" id="8585086"/>
      <dbReference type="DOI" id="10.1016/0041-0101(95)00058-t"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>AMIDATION AT SER-84</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="submission" date="2003-07" db="EMBL/GenBank/DDBJ databases">
      <title>Alignment of beta-toxin nucleotide sequences.</title>
      <authorList>
        <person name="Zhu S."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] OF 20-83</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1985" name="Biochem. J." volume="229" first="739" last="750">
      <title>Scorpion toxins from Centruroides noxius and Tityus serrulatus. Primary structures and sequence comparison by metric analysis.</title>
      <authorList>
        <person name="Possani L.D."/>
        <person name="Martin B.M."/>
        <person name="Svendsen I."/>
        <person name="Rode G.S."/>
        <person name="Erickson B.W."/>
      </authorList>
      <dbReference type="PubMed" id="4052021"/>
      <dbReference type="DOI" id="10.1042/bj2290739"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 20-84</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Beta toxins bind voltage-independently at site-4 of sodium channels (Nav) and shift the voltage of activation toward more negative potentials thereby affecting sodium channel activation and promoting spontaneous and repetitive firing.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="6">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="S81093">
    <property type="protein sequence ID" value="AAB36085.2"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY351298">
    <property type="protein sequence ID" value="AAR08033.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="S32789">
    <property type="entry name" value="S32789"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P15223"/>
  <dbReference type="SMR" id="P15223"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00284">
    <property type="entry name" value="TOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000035279" description="Toxin Cn1" evidence="3">
    <location>
      <begin position="20"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="2">
    <location>
      <begin position="20"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="modified residue" description="Serine amide" evidence="4">
    <location>
      <position position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="30"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="34"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="43"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="47"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="6" ref="3">
    <original>P</original>
    <variation>T</variation>
    <location>
      <position position="79"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="4052021"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="8585086"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="4052021"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="4052021"/>
    </source>
  </evidence>
  <sequence length="86" mass="9586" checksum="AB8C1EA742F17222" modified="1998-12-15" version="3" precursor="true">MNSLLMITACFVLIGTVWAKDGYLVDAKGCKKNCYKLGKNDYCNRECRMKHRGGSYGYCYGFGCYCEGLSDSTPTWPLPNKTCSGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>