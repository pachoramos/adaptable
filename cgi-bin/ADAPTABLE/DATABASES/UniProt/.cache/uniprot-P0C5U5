<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-11-13" modified="2024-07-24" version="64" xmlns="http://uniprot.org/uniprot">
  <accession>P0C5U5</accession>
  <name>PB1F2_I00A0</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein PB1-F2</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">PB1</name>
  </gene>
  <organism>
    <name type="scientific">Influenza A virus (strain A/Duck/Hong Kong/2986.1/2000 H5N1 genotype C)</name>
    <dbReference type="NCBI Taxonomy" id="176674"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Orthornavirae</taxon>
      <taxon>Negarnaviricota</taxon>
      <taxon>Polyploviricotina</taxon>
      <taxon>Insthoviricetes</taxon>
      <taxon>Articulavirales</taxon>
      <taxon>Orthomyxoviridae</taxon>
      <taxon>Alphainfluenzavirus</taxon>
      <taxon>Alphainfluenzavirus influenzae</taxon>
      <taxon>Influenza A virus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Aves</name>
    <dbReference type="NCBI Taxonomy" id="8782"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Felis catus</name>
    <name type="common">Cat</name>
    <name type="synonym">Felis silvestris catus</name>
    <dbReference type="NCBI Taxonomy" id="9685"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Panthera pardus</name>
    <name type="common">Leopard</name>
    <name type="synonym">Felis pardus</name>
    <dbReference type="NCBI Taxonomy" id="9691"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Panthera tigris</name>
    <name type="common">Tiger</name>
    <dbReference type="NCBI Taxonomy" id="9694"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Sus scrofa</name>
    <name type="common">Pig</name>
    <dbReference type="NCBI Taxonomy" id="9823"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="2002" name="Virology" volume="292" first="16" last="23">
      <title>H5N1 influenza viruses isolated from geese in Southeastern China: evidence for genetic reassortment and interspecies transmission to ducks.</title>
      <authorList>
        <person name="Guan Y."/>
        <person name="Peiris M."/>
        <person name="Kong K.F."/>
        <person name="Dyrting K.C."/>
        <person name="Ellis T.M."/>
        <person name="Sit T."/>
        <person name="Zhang L.J."/>
        <person name="Shortridge K.F."/>
      </authorList>
      <dbReference type="PubMed" id="11878904"/>
      <dbReference type="DOI" id="10.1006/viro.2001.1207"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays an important role in promoting lung pathology in both primary viral infection and secondary bacterial infection. Promotes alteration of mitochondrial morphology, dissipation of mitochondrial membrane potential, and cell death. Alternatively, inhibits the production of interferon in the infected cell at the level of host mitochondrial antiviral signaling MAVS. Its level of expression differs greatly depending on which cell type is infected, in a manner that is independent of the levels of expression of other viral proteins. Monocytic cells are more affected than epithelial cells. Seems to disable virus-infected monocytes or other host innate immune cells. During early stage of infection, predisposes the mitochondria to permeability transition through interaction with host SLC25A6/ANT3 and VDAC1. These proteins participate in the formation of the permeability transition pore complex (PTPC) responsible of the release of mitochondrial products that triggers apoptosis.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Oligomer. Interacts with human SLC25A6/ANT3 and VDAC1. Interacts with host MAVS.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-7673978">
      <id>P0C5U5</id>
    </interactant>
    <interactant intactId="EBI-7674009">
      <id>Q8QPG7</id>
      <label>PB2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>4</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host mitochondrion inner membrane</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
      <location evidence="1">Host cytosol</location>
    </subcellularLocation>
    <text evidence="1">Inner mitochondrial membrane in most cells types. Otherwise is detected in the nucleus and cytosol.</text>
  </comment>
  <comment type="miscellaneous">
    <text>Is not encoded in all strains, and seems to be dispensable for replication.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the influenza viruses PB1-F2 family.</text>
  </comment>
  <dbReference type="EMBL" id="AY059518">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="SMR" id="P0C5U5"/>
  <dbReference type="IntAct" id="P0C5U5">
    <property type="interactions" value="2"/>
  </dbReference>
  <dbReference type="MINT" id="P0C5U5"/>
  <dbReference type="Proteomes" id="UP000008285">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044164">
    <property type="term" value="C:host cell cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044192">
    <property type="term" value="C:host cell mitochondrial inner membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042025">
    <property type="term" value="C:host cell nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052150">
    <property type="term" value="P:symbiont-mediated perturbation of host apoptosis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039545">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of MAVS activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_04064">
    <property type="entry name" value="INFV_PB1F2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR021045">
    <property type="entry name" value="Flu_proapoptotic_PB1-F2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11986">
    <property type="entry name" value="PB1-F2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1043">Host membrane</keyword>
  <keyword id="KW-1045">Host mitochondrion</keyword>
  <keyword id="KW-1046">Host mitochondrion inner membrane</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1097">Inhibition of host MAVS by virus</keyword>
  <keyword id="KW-1113">Inhibition of host RLR pathway by virus</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1119">Modulation of host cell apoptosis by virus</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <feature type="chain" id="PRO_0000311636" description="Protein PB1-F2">
    <location>
      <begin position="1"/>
      <end position="90"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="1"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="region of interest" description="Mitochondrial targeting sequence" evidence="1">
    <location>
      <begin position="65"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="site" description="High pathogenicity" evidence="1">
    <location>
      <position position="66"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_04064"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <sequence length="90" mass="10956" checksum="27A2230304F67760" modified="2007-11-13" version="1">MEQEQDTPWTQSTEHINIQNRGNGQRTQRLEHPNSIRLMDHCLRIMSRVGMHRQIVYWKQWLSLKSPTQGSLKTRVLKRWKLFSKQEWIN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>