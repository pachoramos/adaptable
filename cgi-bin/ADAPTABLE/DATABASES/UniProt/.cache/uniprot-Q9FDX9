<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-10-31" modified="2024-10-02" version="127" xmlns="http://uniprot.org/uniprot">
  <accession>Q9FDX9</accession>
  <accession>Q9LY73</accession>
  <name>GG1_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Guanine nucleotide-binding protein subunit gamma 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Ggamma-subunit 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Heterotrimeric G protein gamma-subunit 1</fullName>
      <shortName>AtAGG1</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">GG1</name>
    <name type="synonym">AGG1</name>
    <name type="ordered locus">At3g63420</name>
    <name type="ORF">MAA21.50</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Proc. Natl. Acad. Sci. U.S.A." volume="97" first="14784" last="14788">
      <title>Completing the heterotrimer: isolation and characterization of an Arabidopsis thaliana G protein gamma-subunit cDNA.</title>
      <authorList>
        <person name="Mason M.G."/>
        <person name="Botella J.R."/>
      </authorList>
      <dbReference type="PubMed" id="11121078"/>
      <dbReference type="DOI" id="10.1073/pnas.97.26.14784"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA] (ISOFORM 1)</scope>
    <scope>INTERACTION WITH GB1</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2000" name="Nature" volume="408" first="820" last="822">
      <title>Sequence and analysis of chromosome 3 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Salanoubat M."/>
        <person name="Lemcke K."/>
        <person name="Rieger M."/>
        <person name="Ansorge W."/>
        <person name="Unseld M."/>
        <person name="Fartmann B."/>
        <person name="Valle G."/>
        <person name="Bloecker H."/>
        <person name="Perez-Alonso M."/>
        <person name="Obermaier B."/>
        <person name="Delseny M."/>
        <person name="Boutry M."/>
        <person name="Grivell L.A."/>
        <person name="Mache R."/>
        <person name="Puigdomenech P."/>
        <person name="De Simone V."/>
        <person name="Choisne N."/>
        <person name="Artiguenave F."/>
        <person name="Robert C."/>
        <person name="Brottier P."/>
        <person name="Wincker P."/>
        <person name="Cattolico L."/>
        <person name="Weissenbach J."/>
        <person name="Saurin W."/>
        <person name="Quetier F."/>
        <person name="Schaefer M."/>
        <person name="Mueller-Auer S."/>
        <person name="Gabel C."/>
        <person name="Fuchs M."/>
        <person name="Benes V."/>
        <person name="Wurmbach E."/>
        <person name="Drzonek H."/>
        <person name="Erfle H."/>
        <person name="Jordan N."/>
        <person name="Bangert S."/>
        <person name="Wiedelmann R."/>
        <person name="Kranz H."/>
        <person name="Voss H."/>
        <person name="Holland R."/>
        <person name="Brandt P."/>
        <person name="Nyakatura G."/>
        <person name="Vezzi A."/>
        <person name="D'Angelo M."/>
        <person name="Pallavicini A."/>
        <person name="Toppo S."/>
        <person name="Simionati B."/>
        <person name="Conrad A."/>
        <person name="Hornischer K."/>
        <person name="Kauer G."/>
        <person name="Loehnert T.-H."/>
        <person name="Nordsiek G."/>
        <person name="Reichelt J."/>
        <person name="Scharfe M."/>
        <person name="Schoen O."/>
        <person name="Bargues M."/>
        <person name="Terol J."/>
        <person name="Climent J."/>
        <person name="Navarro P."/>
        <person name="Collado C."/>
        <person name="Perez-Perez A."/>
        <person name="Ottenwaelder B."/>
        <person name="Duchemin D."/>
        <person name="Cooke R."/>
        <person name="Laudie M."/>
        <person name="Berger-Llauro C."/>
        <person name="Purnelle B."/>
        <person name="Masuy D."/>
        <person name="de Haan M."/>
        <person name="Maarse A.C."/>
        <person name="Alcaraz J.-P."/>
        <person name="Cottet A."/>
        <person name="Casacuberta E."/>
        <person name="Monfort A."/>
        <person name="Argiriou A."/>
        <person name="Flores M."/>
        <person name="Liguori R."/>
        <person name="Vitale D."/>
        <person name="Mannhaupt G."/>
        <person name="Haase D."/>
        <person name="Schoof H."/>
        <person name="Rudd S."/>
        <person name="Zaccaria P."/>
        <person name="Mewes H.-W."/>
        <person name="Mayer K.F.X."/>
        <person name="Kaul S."/>
        <person name="Town C.D."/>
        <person name="Koo H.L."/>
        <person name="Tallon L.J."/>
        <person name="Jenkins J."/>
        <person name="Rooney T."/>
        <person name="Rizzo M."/>
        <person name="Walts A."/>
        <person name="Utterback T."/>
        <person name="Fujii C.Y."/>
        <person name="Shea T.P."/>
        <person name="Creasy T.H."/>
        <person name="Haas B."/>
        <person name="Maiti R."/>
        <person name="Wu D."/>
        <person name="Peterson J."/>
        <person name="Van Aken S."/>
        <person name="Pai G."/>
        <person name="Militscher J."/>
        <person name="Sellers P."/>
        <person name="Gill J.E."/>
        <person name="Feldblyum T.V."/>
        <person name="Preuss D."/>
        <person name="Lin X."/>
        <person name="Nierman W.C."/>
        <person name="Salzberg S.L."/>
        <person name="White O."/>
        <person name="Venter J.C."/>
        <person name="Fraser C.M."/>
        <person name="Kaneko T."/>
        <person name="Nakamura Y."/>
        <person name="Sato S."/>
        <person name="Kato T."/>
        <person name="Asamizu E."/>
        <person name="Sasamoto S."/>
        <person name="Kimura T."/>
        <person name="Idesawa K."/>
        <person name="Kawashima K."/>
        <person name="Kishida Y."/>
        <person name="Kiyokawa C."/>
        <person name="Kohara M."/>
        <person name="Matsumoto M."/>
        <person name="Matsuno A."/>
        <person name="Muraki A."/>
        <person name="Nakayama S."/>
        <person name="Nakazaki N."/>
        <person name="Shinpo S."/>
        <person name="Takeuchi C."/>
        <person name="Wada T."/>
        <person name="Watanabe A."/>
        <person name="Yamada M."/>
        <person name="Yasuda M."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="11130713"/>
      <dbReference type="DOI" id="10.1038/35048706"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="submission" date="2004-09" db="EMBL/GenBank/DDBJ databases">
      <title>Large-scale analysis of RIKEN Arabidopsis full-length (RAFL) cDNAs.</title>
      <authorList>
        <person name="Totoki Y."/>
        <person name="Seki M."/>
        <person name="Ishida J."/>
        <person name="Nakajima M."/>
        <person name="Enju A."/>
        <person name="Kamiya A."/>
        <person name="Narusaka M."/>
        <person name="Shin-i T."/>
        <person name="Nakagawa M."/>
        <person name="Sakamoto N."/>
        <person name="Oishi K."/>
        <person name="Kohara Y."/>
        <person name="Kobayashi M."/>
        <person name="Toyoda A."/>
        <person name="Sakaki Y."/>
        <person name="Sakurai T."/>
        <person name="Iida K."/>
        <person name="Akiyama K."/>
        <person name="Satou M."/>
        <person name="Toyoda T."/>
        <person name="Konagaya A."/>
        <person name="Carninci P."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
        <person name="Shinozaki K."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM 1)</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="submission" date="2006-02" db="EMBL/GenBank/DDBJ databases">
      <title>Arabidopsis ORF clones.</title>
      <authorList>
        <person name="Shinn P."/>
        <person name="Chen H."/>
        <person name="Kim C.J."/>
        <person name="Ecker J.R."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM 1)</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2006" name="J. Cell Sci." volume="119" first="5087" last="5097">
      <title>Plant G protein heterotrimers require dual lipidation motifs of Galpha and Ggamma and do not dissociate upon activation.</title>
      <authorList>
        <person name="Adjobo-Hermans M.J.W."/>
        <person name="Goedhart J."/>
        <person name="Gadella T.W.J. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="17158913"/>
      <dbReference type="DOI" id="10.1242/jcs.03284"/>
    </citation>
    <scope>SUBUNIT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INTERACTION WITH GB1</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2007" name="Gene" volume="393" first="163" last="170">
      <title>Over-expression of a truncated Arabidopsis thaliana heterotrimeric G protein gamma subunit results in a phenotype similar to alpha and beta subunit knockouts.</title>
      <authorList>
        <person name="Chakravorty D."/>
        <person name="Botella J.R."/>
      </authorList>
      <dbReference type="PubMed" id="17383830"/>
      <dbReference type="DOI" id="10.1016/j.gene.2007.02.008"/>
    </citation>
    <scope>FUNCTION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2007" name="Mol. Cell. Proteomics" volume="6" first="1980" last="1996">
      <title>A high content in lipid-modified peripheral proteins and integral receptor kinases features in the arabidopsis plasma membrane proteome.</title>
      <authorList>
        <person name="Marmagne A."/>
        <person name="Ferro M."/>
        <person name="Meinnel T."/>
        <person name="Bruley C."/>
        <person name="Kuhn L."/>
        <person name="Garin J."/>
        <person name="Barbier-Brygoo H."/>
        <person name="Ephritikhine G."/>
      </authorList>
      <dbReference type="PubMed" id="17644812"/>
      <dbReference type="DOI" id="10.1074/mcp.m700099-mcp200"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2007" name="Plant Cell" volume="19" first="1235" last="1250">
      <title>Heterotrimeric G protein gamma subunits provide functional selectivity in Gbetagamma dimer signaling in Arabidopsis.</title>
      <authorList>
        <person name="Trusov Y."/>
        <person name="Rookes J.E."/>
        <person name="Tilbrook K."/>
        <person name="Chakravorty D."/>
        <person name="Mason M.G."/>
        <person name="Anderson D."/>
        <person name="Chen J.-G."/>
        <person name="Jones A.M."/>
        <person name="Botella J.R."/>
      </authorList>
      <dbReference type="PubMed" id="17468261"/>
      <dbReference type="DOI" id="10.1105/tpc.107.050096"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <scope>SUBUNIT</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>INDUCTION BY ALTERNARIA BRASSICICOLA AND FUSARIUM OXYSPORUM</scope>
    <source>
      <strain>cv. Columbia</strain>
      <strain>cv. Wassilewskija</strain>
    </source>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2007" name="Plant Physiol." volume="143" first="1119" last="1131">
      <title>Dual lipid modification of Arabidopsis Ggamma-subunits is required for efficient plasma membrane targeting.</title>
      <authorList>
        <person name="Zeng Q."/>
        <person name="Wang X."/>
        <person name="Running M.P."/>
      </authorList>
      <dbReference type="PubMed" id="17220359"/>
      <dbReference type="DOI" id="10.1104/pp.106.093583"/>
    </citation>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>PALMITOYLATION AT CYS-93</scope>
    <scope>ISOPRENYLATION AT CYS-95</scope>
    <scope>MUTAGENESIS OF 88-ASN--ARG-94; CYS-93 AND CYS-95</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2008" name="Plant Physiol." volume="147" first="636" last="649">
      <title>Ggamma1 + Ggamma2 not equal to Gbeta: heterotrimeric G protein Ggamma-deficient mutants do not recapitulate all phenotypes of Gbeta-deficient mutants.</title>
      <authorList>
        <person name="Trusov Y."/>
        <person name="Zhang W."/>
        <person name="Assmann S.M."/>
        <person name="Botella J.R."/>
      </authorList>
      <dbReference type="PubMed" id="18441222"/>
      <dbReference type="DOI" id="10.1104/pp.108.117655"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2009" name="Plant Cell" volume="21" first="3591" last="3609">
      <title>Arabidopsis N-MYC DOWNREGULATED-LIKE1, a positive regulator of auxin transport in a G protein-mediated pathway.</title>
      <authorList>
        <person name="Mudgil Y."/>
        <person name="Uhrig J.F."/>
        <person name="Zhou J."/>
        <person name="Temple B."/>
        <person name="Jiang K."/>
        <person name="Jones A.M."/>
      </authorList>
      <dbReference type="PubMed" id="19948787"/>
      <dbReference type="DOI" id="10.1105/tpc.109.065557"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <scope>INTERACTION WITH NDL1; NDL2 AND NDL3</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2010" name="PLoS ONE" volume="5" first="E12833" last="E12833">
      <title>Glucose attenuation of auxin-mediated bimodality in lateral root formation is partly coupled by the heterotrimeric G protein complex.</title>
      <authorList>
        <person name="Booker K.S."/>
        <person name="Schwarz J."/>
        <person name="Garrett M.B."/>
        <person name="Jones A.M."/>
      </authorList>
      <dbReference type="PubMed" id="20862254"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0012833"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="14">
    <citation type="journal article" date="2011" name="Acta Biochim. Pol." volume="58" first="609" last="616">
      <title>Arabidopsis thaliana Nudix hydrolase AtNUDT7 forms complexes with the regulatory RACK1A protein and Ggamma subunits of the signal transducing heterotrimeric G protein.</title>
      <authorList>
        <person name="Olejnik K."/>
        <person name="Bucholc M."/>
        <person name="Anielska-Mazur A."/>
        <person name="Lipko A."/>
        <person name="Kujawa M."/>
        <person name="Modzelan M."/>
        <person name="Augustyn A."/>
        <person name="Kraszewska E."/>
      </authorList>
      <dbReference type="PubMed" id="22068106"/>
    </citation>
    <scope>INTERACTION WITH NUDT7</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="15">
    <citation type="journal article" date="2012" name="J. Plant Physiol." volume="169" first="542" last="545">
      <title>Ggamma1+Ggamma2+Ggamma3=Gbeta: the search for heterotrimeric G-protein gamma subunits in Arabidopsis is over.</title>
      <authorList>
        <person name="Thung L."/>
        <person name="Trusov Y."/>
        <person name="Chakravorty D."/>
        <person name="Botella J.R."/>
      </authorList>
      <dbReference type="PubMed" id="22209167"/>
      <dbReference type="DOI" id="10.1016/j.jplph.2011.11.010"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="16">
    <citation type="journal article" date="2012" name="Mol. Plant" volume="5" first="98" last="114">
      <title>Arabidopsis heterotrimeric G-protein regulates cell wall defense and resistance to necrotrophic fungi.</title>
      <authorList>
        <person name="Delgado-Cerezo M."/>
        <person name="Sanchez-Rodriguez C."/>
        <person name="Escudero V."/>
        <person name="Miedes E."/>
        <person name="Fernandez P.V."/>
        <person name="Jorda L."/>
        <person name="Hernandez-Blanco C."/>
        <person name="Sanchez-Vallet A."/>
        <person name="Bednarek P."/>
        <person name="Schulze-Lefert P."/>
        <person name="Somerville S."/>
        <person name="Estevez J.M."/>
        <person name="Persson S."/>
        <person name="Molina A."/>
      </authorList>
      <dbReference type="PubMed" id="21980142"/>
      <dbReference type="DOI" id="10.1093/mp/ssr082"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="6 7 10 11 12 14">Guanine nucleotide-binding proteins (G proteins) are involved as a modulator or transducer in various transmembrane signaling systems. The beta and gamma chains are required for the GTPase activity, for replacement of GDP by GTP, and for G protein-effector interaction. Involved in the abscisic acid (ABA) and ethylene signaling pathways. Regulates acropetal transport of auxin (IAA) in roots and hypocotyls, and thus modulates root architecture (e.g. lateral root formation). The heterotrimeric G-protein controls defense responses to necrotrophic and vascular fungi probably by modulating cell wall-related genes expression; involved in resistance to fungal pathogens such as Alternaria brassicicola, Plectosphaerella cucumerina and Fusarium oxysporum.</text>
  </comment>
  <comment type="subunit">
    <text evidence="3 4 7 10 13">G proteins are composed of 3 units, alpha, beta and gamma. Interacts with the beta subunit GB1. The dimer GB1-GG1 interacts with NDL1, NDL2 and NDL3. Binds to NUDT7.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-1750878">
      <id>Q9FDX9</id>
    </interactant>
    <interactant intactId="EBI-1632851">
      <id>P49177</id>
      <label>GB1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>8</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-1750878">
      <id>Q9FDX9</id>
    </interactant>
    <interactant intactId="EBI-1750986">
      <id>P93397</id>
    </interactant>
    <organismsDiffer>true</organismsDiffer>
    <experiments>4</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4 5 8">Cell membrane</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="4 5">Golgi apparatus membrane</location>
    </subcellularLocation>
    <subcellularLocation>
      <location>Golgi apparatus</location>
      <location>trans-Golgi network membrane</location>
    </subcellularLocation>
    <subcellularLocation>
      <location>Cytoplasm</location>
    </subcellularLocation>
    <text evidence="4 13 16">Localized to the cell membrane when attached to beta subunit GB1 (Probable) (PubMed:17158913). Present in the cytoplasm when associated with NUDT7 (PubMed:22068106).</text>
  </comment>
  <comment type="alternative products">
    <event type="alternative initiation"/>
    <isoform>
      <id>Q9FDX9-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>Q9FDX9-2</id>
      <name>2</name>
      <sequence type="described" ref="VSP_044366"/>
    </isoform>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3 7 9">Mostly expressed in seedlings (especially at the hypocotyl/root junction), young cauline leaves, open flowers, and floral stems, and, to a lower extent, in roots (restricted to the stele), rosette leaves (restricted to veins), siliques, and unopened floral buds. Also present in hydathods.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="7 9">In seedlings, first observed at the hypocotyl/root junction but later confined to the hypocotyl. In flowers, restricted to the stigma of mature flowers. In siliques, confined to the abscission zone.</text>
  </comment>
  <comment type="induction">
    <text evidence="7">Induced locally by Alternaria brassicicola but systemically by Fusarium oxysporum.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="7 10 11 12 14">Enhanced susceptibility to Alternaria brassicicola, Plectosphaerella cucumerina and Fusarium oxysporum associated with a disturbed expression of genes involved in cell wall metabolism (e.g. lower xylose content in cell walls). Reduced induction of the plant defensin gene PDF1.2, and decreased sensitivity to methyl jasmonate (MeJA). Hypersensitive to auxin-mediated induction of lateral roots, within the central cylinder, attenuating acropetally transported auxin signaling. Enhanced sensitivity to glucose and mannitol during seed germination. Abnormal roots architecture.</text>
  </comment>
  <dbReference type="EMBL" id="AF283673">
    <property type="protein sequence ID" value="AAG45959.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF283674">
    <property type="protein sequence ID" value="AAG45960.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL163818">
    <property type="protein sequence ID" value="CAB87795.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002686">
    <property type="protein sequence ID" value="AEE80479.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002686">
    <property type="protein sequence ID" value="AEE80480.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002686">
    <property type="protein sequence ID" value="ANM63533.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK175815">
    <property type="protein sequence ID" value="BAD43578.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT024540">
    <property type="protein sequence ID" value="ABD38879.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="T49183">
    <property type="entry name" value="T49183"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001325615.1">
    <molecule id="Q9FDX9-2"/>
    <property type="nucleotide sequence ID" value="NM_001340213.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_567147.1">
    <molecule id="Q9FDX9-1"/>
    <property type="nucleotide sequence ID" value="NM_116207.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_850741.1">
    <molecule id="Q9FDX9-1"/>
    <property type="nucleotide sequence ID" value="NM_180410.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9FDX9"/>
  <dbReference type="SMR" id="Q9FDX9"/>
  <dbReference type="BioGRID" id="10831">
    <property type="interactions" value="12"/>
  </dbReference>
  <dbReference type="IntAct" id="Q9FDX9">
    <property type="interactions" value="5"/>
  </dbReference>
  <dbReference type="STRING" id="3702.Q9FDX9"/>
  <dbReference type="TCDB" id="8.A.92.1.2">
    <property type="family name" value="the g-protein AlphaBetaGama complex (gpc) family"/>
  </dbReference>
  <dbReference type="SwissPalm" id="Q9FDX9"/>
  <dbReference type="PaxDb" id="3702-AT3G63420.1"/>
  <dbReference type="ProteomicsDB" id="221831">
    <molecule id="Q9FDX9-1"/>
  </dbReference>
  <dbReference type="EnsemblPlants" id="AT3G63420.1">
    <molecule id="Q9FDX9-1"/>
    <property type="protein sequence ID" value="AT3G63420.1"/>
    <property type="gene ID" value="AT3G63420"/>
  </dbReference>
  <dbReference type="EnsemblPlants" id="AT3G63420.2">
    <molecule id="Q9FDX9-1"/>
    <property type="protein sequence ID" value="AT3G63420.2"/>
    <property type="gene ID" value="AT3G63420"/>
  </dbReference>
  <dbReference type="EnsemblPlants" id="AT3G63420.3">
    <molecule id="Q9FDX9-2"/>
    <property type="protein sequence ID" value="AT3G63420.3"/>
    <property type="gene ID" value="AT3G63420"/>
  </dbReference>
  <dbReference type="GeneID" id="825517"/>
  <dbReference type="Gramene" id="AT3G63420.1">
    <molecule id="Q9FDX9-1"/>
    <property type="protein sequence ID" value="AT3G63420.1"/>
    <property type="gene ID" value="AT3G63420"/>
  </dbReference>
  <dbReference type="Gramene" id="AT3G63420.2">
    <molecule id="Q9FDX9-1"/>
    <property type="protein sequence ID" value="AT3G63420.2"/>
    <property type="gene ID" value="AT3G63420"/>
  </dbReference>
  <dbReference type="Gramene" id="AT3G63420.3">
    <molecule id="Q9FDX9-2"/>
    <property type="protein sequence ID" value="AT3G63420.3"/>
    <property type="gene ID" value="AT3G63420"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT3G63420"/>
  <dbReference type="Araport" id="AT3G63420"/>
  <dbReference type="TAIR" id="AT3G63420">
    <property type="gene designation" value="GG1"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S439">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_105699_2_0_1"/>
  <dbReference type="InParanoid" id="Q9FDX9"/>
  <dbReference type="OrthoDB" id="460144at2759"/>
  <dbReference type="PRO" id="PR:Q9FDX9"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q9FDX9">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005794">
    <property type="term" value="C:Golgi apparatus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000139">
    <property type="term" value="C:Golgi membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005834">
    <property type="term" value="C:heterotrimeric G-protein complex"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005525">
    <property type="term" value="F:GTP binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010541">
    <property type="term" value="P:acropetal auxin transport"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007186">
    <property type="term" value="P:G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048527">
    <property type="term" value="P:lateral root development"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0018345">
    <property type="term" value="P:protein palmitoylation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0018342">
    <property type="term" value="P:protein prenylation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009845">
    <property type="term" value="P:seed germination"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015898">
    <property type="entry name" value="G-protein_gamma-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR045878">
    <property type="entry name" value="GG1/2"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35129">
    <property type="entry name" value="GUANINE NUCLEOTIDE-BINDING PROTEIN SUBUNIT GAMMA 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35129:SF1">
    <property type="entry name" value="GUANINE NUCLEOTIDE-BINDING PROTEIN SUBUNIT GAMMA 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00631">
    <property type="entry name" value="G-gamma"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01224">
    <property type="entry name" value="G_gamma"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0024">Alternative initiation</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0175">Coiled coil</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0333">Golgi apparatus</keyword>
  <keyword id="KW-0449">Lipoprotein</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0488">Methylation</keyword>
  <keyword id="KW-0564">Palmitate</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0636">Prenylation</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0807">Transducer</keyword>
  <feature type="chain" id="PRO_0000419813" description="Guanine nucleotide-binding protein subunit gamma 1">
    <location>
      <begin position="1"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000419814" description="Removed in mature form" evidence="1">
    <location>
      <begin position="96"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="domain" description="G protein gamma">
    <location>
      <begin position="19"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="region of interest" description="Regulates lipidation and cell membrane subcellular localization">
    <location>
      <begin position="88"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="coiled-coil region" evidence="2">
    <location>
      <begin position="20"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="modified residue" description="Cysteine methyl ester" evidence="15">
    <location>
      <position position="95"/>
    </location>
  </feature>
  <feature type="lipid moiety-binding region" description="S-palmitoyl cysteine" evidence="5">
    <location>
      <position position="93"/>
    </location>
  </feature>
  <feature type="lipid moiety-binding region" description="S-farnesyl cysteine" evidence="5">
    <location>
      <position position="95"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_044366" description="In isoform 2." evidence="15">
    <original>M</original>
    <variation>MEGKSRFESERERLRRQFKLVRTKTRLSLTLSDLRPASERM</variation>
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Plasma membrane only localization, probably by increased lipidation." evidence="5">
    <original>NGGEGCR</original>
    <variation>KEAKRCG</variation>
    <location>
      <begin position="88"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Normal subcellular localization." evidence="5">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="93"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No prenylation and loss of cell membrane and Golgi apparatus attachment leading to cytoplasmic subcellular localization." evidence="5">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="95"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="11121078"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="17158913"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="17220359"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="17383830"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="17468261"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="17644812"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="18441222"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="19948787"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="11">
    <source>
      <dbReference type="PubMed" id="20862254"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="12">
    <source>
      <dbReference type="PubMed" id="21980142"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="13">
    <source>
      <dbReference type="PubMed" id="22068106"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="14">
    <source>
      <dbReference type="PubMed" id="22209167"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="15"/>
  <evidence type="ECO:0000305" key="16">
    <source>
      <dbReference type="PubMed" id="22068106"/>
    </source>
  </evidence>
  <sequence length="98" mass="10947" checksum="F032D1883E59538B" modified="2001-03-01" version="1" precursor="true">MREETVVYEQEESVSHGGGKHRILAELARVEQEVAFLEKELKEVENTDIVSTVCEELLSVIEKGPDPLLPLTNGPLNLGWDRWFEGPNGGEGCRCLIL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>