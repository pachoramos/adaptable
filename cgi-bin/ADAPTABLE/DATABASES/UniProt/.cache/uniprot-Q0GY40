<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-02-06" modified="2024-05-29" version="48" xmlns="http://uniprot.org/uniprot">
  <accession>Q0GY40</accession>
  <accession>A8SDT6</accession>
  <name>KBX3_HOFGE</name>
  <protein>
    <recommendedName>
      <fullName>Hge-scorpine</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="8 9">Hge-scorpine-like 1</fullName>
      <shortName evidence="5 6">HgeScplp1</shortName>
      <shortName>Hgscplike1</shortName>
    </alternativeName>
    <component>
      <recommendedName>
        <fullName evidence="6">Hge36</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Hoffmannihadrurus gertschi</name>
    <name type="common">Scorpion</name>
    <name type="synonym">Hadrurus gertschi</name>
    <dbReference type="NCBI Taxonomy" id="380989"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Iurida</taxon>
      <taxon>Iuroidea</taxon>
      <taxon>Hadrurus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2007" name="BMC Genomics" volume="8" first="119" last="119">
      <title>Transcriptome analysis of the venom gland of the Mexican scorpion Hadrurus gertschi (Arachnida: Scorpiones).</title>
      <authorList>
        <person name="Schwartz E.F."/>
        <person name="Diego-Garcia E."/>
        <person name="Rodriguez de la Vega R.C."/>
        <person name="Possani L.D."/>
      </authorList>
      <dbReference type="PubMed" id="17506894"/>
      <dbReference type="DOI" id="10.1186/1471-2164-8-119"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2007" name="Peptides" volume="28" first="31" last="37">
      <title>Wide phylogenetic distribution of scorpine and long-chain beta-KTx-like peptides in scorpion venoms: identification of 'orphan' components.</title>
      <authorList>
        <person name="Diego-Garcia E."/>
        <person name="Schwartz E.F."/>
        <person name="D'Suze G."/>
        <person name="Gonzalez S.A."/>
        <person name="Batista C.V."/>
        <person name="Garcia B.I."/>
        <person name="Rodriguez de la Vega R.C."/>
        <person name="Possani L.D."/>
      </authorList>
      <dbReference type="PubMed" id="17141373"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2006.06.012"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 20-64</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2008" name="Cell. Mol. Life Sci." volume="65" first="187" last="200">
      <title>Cytolytic and K+ channel blocking activities of beta-KTx and scorpine-like peptides purified from scorpion venoms.</title>
      <authorList>
        <person name="Diego-Garcia E."/>
        <person name="Abdel-Mottaleb Y."/>
        <person name="Schwartz E.F."/>
        <person name="Rodriguez de la Vega R.C."/>
        <person name="Tytgat J."/>
        <person name="Possani L.D."/>
      </authorList>
      <dbReference type="PubMed" id="18030427"/>
      <dbReference type="DOI" id="10.1007/s00018-007-7370-x"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 20-64</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference evidence="10 11" key="4">
    <citation type="journal article" date="2016" name="FEBS Lett." volume="590" first="2286" last="2296">
      <title>Solution structure and antiparasitic activity of scorpine-like peptides from Hoffmannihadrurus gertschi.</title>
      <authorList>
        <person name="Flores-Solis D."/>
        <person name="Toledano Y."/>
        <person name="Rodriguez-Lima O."/>
        <person name="Cano-Sanchez P."/>
        <person name="Ramirez-Cordero B.E."/>
        <person name="Landa A."/>
        <person name="Rodriguez de la Vega R.C."/>
        <person name="Del Rio-Portilla F."/>
      </authorList>
      <dbReference type="PubMed" id="27314815"/>
      <dbReference type="DOI" id="10.1002/1873-3468.12255"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 48-95</scope>
    <scope>FUNCTION</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>MUTAGENESIS OF 1-MET--MET-51</scope>
  </reference>
  <comment type="function">
    <molecule>Hge-scorpine</molecule>
    <text evidence="3">Has antibacterial activity against B.subtilis, but not against S.aureus. Also has hemolytic and cytolytic activities. Since cell lysis occurs at the tested concentrations, observation of activity on potassium channels is impossible.</text>
  </comment>
  <comment type="function">
    <molecule>Hge36</molecule>
    <text evidence="3 4">Blocks Kv1.1/KCNA1 (IC(50)=185 nM) potassium channels. Shows a weak hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2 3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2 3">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="8370.0" method="Electrospray" evidence="2">
    <molecule>Hge-scorpine</molecule>
  </comment>
  <comment type="mass spectrometry" mass="8370.0" method="MALDI" evidence="3">
    <molecule>Hge-scorpine</molecule>
  </comment>
  <comment type="mass spectrometry" mass="5294.95" method="MALDI" evidence="3">
    <molecule>Hge36</molecule>
  </comment>
  <comment type="miscellaneous">
    <molecule>Hge-scorpine</molecule>
    <text>The C-terminus (AA 52-95) blocks Kv1.1/KCNA1, Kv1.2/KCNA2, and Kv1.3/KCNA2 potassium channels, showing a potential important role of AA 48-51.</text>
  </comment>
  <comment type="miscellaneous">
    <molecule>Hge36</molecule>
    <text evidence="9">Negative results: does not block Kv1.2/KCNA2 and Kv1.3/KCNA3.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the long chain scorpion toxin family. Class 3 subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="DQ465351">
    <property type="protein sequence ID" value="ABE98267.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF613116">
    <property type="protein sequence ID" value="ABU94956.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EL698908">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="5IPO">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=48-95"/>
  </dbReference>
  <dbReference type="PDB" id="5JYH">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=52-95"/>
  </dbReference>
  <dbReference type="PDBsum" id="5IPO"/>
  <dbReference type="PDBsum" id="5JYH"/>
  <dbReference type="AlphaFoldDB" id="Q0GY40"/>
  <dbReference type="SMR" id="Q0GY40"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029237">
    <property type="entry name" value="Long_scorpion_toxin_alpha/beta"/>
  </dbReference>
  <dbReference type="Pfam" id="PF14866">
    <property type="entry name" value="Toxin_38"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51862">
    <property type="entry name" value="BSPN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2 3">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000274682" description="Hge-scorpine" evidence="2 3">
    <location>
      <begin position="20"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000356887" description="Hge36" evidence="2 3">
    <location>
      <begin position="48"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="domain" description="BetaSPN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="55"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 10 11">
    <location>
      <begin position="58"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 10 11">
    <location>
      <begin position="68"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 10 11">
    <location>
      <begin position="72"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="In HgeD; enhanced potassium channel-blocking and antiparasitic activities." evidence="4">
    <location>
      <begin position="1"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="turn" evidence="12">
    <location>
      <begin position="50"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="strand" evidence="12">
    <location>
      <begin position="53"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="helix" evidence="12">
    <location>
      <begin position="68"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="strand" evidence="12">
    <location>
      <begin position="77"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="strand" evidence="12">
    <location>
      <begin position="86"/>
      <end position="92"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01209"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="17141373"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18030427"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="27314815"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="17141373"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="18030427"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="17141373"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="9">
    <source>
      <dbReference type="PubMed" id="18030427"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="10">
    <source>
      <dbReference type="PDB" id="5IPO"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="11">
    <source>
      <dbReference type="PDB" id="5JYH"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="12">
    <source>
      <dbReference type="PDB" id="5IPO"/>
    </source>
  </evidence>
  <sequence length="95" mass="10412" checksum="EA797C1F58CF6C3C" modified="2006-10-03" version="1" precursor="true">MNTKLTVLCFLGIVTIVSCGWMSEKKVQGILDKKLPEGIIRNAAKAIVHKMAKNQFGCFANVDVKGDCKRHCKAEDKEGICHGTKCKCGVPISYL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>