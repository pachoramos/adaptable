<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-09-05" modified="2023-02-22" version="15" xmlns="http://uniprot.org/uniprot">
  <accession>P86794</accession>
  <name>ITR2C_FAGES</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Trypsin inhibitor 2c</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">BWI-2c</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Fagopyrum esculentum</name>
    <name type="common">Common buckwheat</name>
    <name type="synonym">Polygonum fagopyrum</name>
    <dbReference type="NCBI Taxonomy" id="3617"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>Caryophyllales</taxon>
      <taxon>Polygonaceae</taxon>
      <taxon>Polygonoideae</taxon>
      <taxon>Fagopyreae</taxon>
      <taxon>Fagopyrum</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2012" name="Biochem. J." volume="446" first="69" last="77">
      <title>Buckwheat trypsin inhibitor with helical hairpin structure belongs to a new family of plant defense peptides.</title>
      <authorList>
        <person name="Oparin P.B."/>
        <person name="Mineev K.S."/>
        <person name="Dunaevsky Y.E."/>
        <person name="Arseniev A.S."/>
        <person name="Belozersky M.A."/>
        <person name="Grishin E.V."/>
        <person name="Egorov T.A."/>
        <person name="Vassilevski A.A."/>
      </authorList>
      <dbReference type="PubMed" id="22612157"/>
      <dbReference type="DOI" id="10.1042/bj20120548"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>STRUCTURE BY NMR</scope>
    <source>
      <strain evidence="1">cv. Shatilovskaya 5</strain>
      <tissue evidence="1">Seed</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Inhibits bovine trypsin with a Ki of 0.174 nM and trypsin-like proteases from G.mellonella larvae. Has no activity against serine proteases chymotrypsin, subtilisin and elastase. Has no activity against cysteine proteases from beetle gut.</text>
  </comment>
  <comment type="mass spectrometry" mass="5182.0" error="0.5" method="MALDI" evidence="1"/>
  <dbReference type="PDB" id="2LQX">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-41"/>
  </dbReference>
  <dbReference type="PDBsum" id="2LQX"/>
  <dbReference type="AlphaFoldDB" id="P86794"/>
  <dbReference type="BMRB" id="P86794"/>
  <dbReference type="SMR" id="P86794"/>
  <dbReference type="MEROPS" id="I73.002"/>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.1700">
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <feature type="chain" id="PRO_0000419016" description="Trypsin inhibitor 2c">
    <location>
      <begin position="1"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="11"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="15"/>
      <end position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="22612157"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="22612157"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="41" mass="5186" checksum="1DBA26E44BB5DB80" modified="2012-09-05" version="1">SEKPQQELEECQNVCRMKRWSTEMVHRCEKKCEEKFERQQR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>