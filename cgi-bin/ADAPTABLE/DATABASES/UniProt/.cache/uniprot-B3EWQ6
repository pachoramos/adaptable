<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-10-03" modified="2023-05-03" version="9" xmlns="http://uniprot.org/uniprot">
  <accession>B3EWQ6</accession>
  <name>AMP1_PAEAL</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Antimicrobial protein AN5-1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Paenibacillus alvei</name>
    <name type="common">Bacillus alvei</name>
    <dbReference type="NCBI Taxonomy" id="44250"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Paenibacillaceae</taxon>
      <taxon>Paenibacillus</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2013" name="J. Ind. Microbiol. Biotechnol." volume="40" first="571" last="579">
      <title>Detection of secreted antimicrobial peptides isolated from cell-free culture supernatant of Paenibacillus alvei AN5.</title>
      <authorList>
        <person name="Alkotaini B."/>
        <person name="Anuar N."/>
        <person name="Kadhum A.A."/>
        <person name="Sani A.A."/>
      </authorList>
      <dbReference type="PubMed" id="23508455"/>
      <dbReference type="DOI" id="10.1007/s10295-013-1259-5"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <strain evidence="1">AN5</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has bactericidal activity against E.coli ATCC 29522 and S.aureus. Inhibits growth of S.marcescens and B.cereus ATCC 14579. A synthetic peptide has antibacterial activity against E.coli ATCC 29522 (MIC=8 ug/ml), S.aureus (MIC=64 ug/ml), S.marcescens (MIC=32 ug/ml), B.cereus ATCC 14579 (MIC=64 ug/ml), B.subtilis (MIC=32 ug/ml), L.plantarum ATCC 8014 (MIC=32 ug/ml), B.flexus (MIC=32 ug/ml), S.enteritidis ATCC 13076 (MIC=4 ug/ml), Enterobacter spp (MIC=32 ug/ml), B.anthracis (MIC=128 ug/ml), B.licheniformis (MIC&gt;128 ug/ml) and L.lactis ATCC 11454 (MIC&gt;128 ug/ml).</text>
  </comment>
  <comment type="biophysicochemical properties">
    <phDependence>
      <text evidence="1">Active between pH 2 and 12.</text>
    </phDependence>
    <temperatureDependence>
      <text evidence="1">Active between 37 and 90 degrees Celsius. Activity is reduced at 100 degrees Celsius and absent at 110 degrees Celsius. Activity is unaffected by storage at -20 degrees Celsius.</text>
    </temperatureDependence>
  </comment>
  <comment type="mass spectrometry" mass="1316.734" method="Electrospray" evidence="1"/>
  <comment type="miscellaneous">
    <text evidence="1">A synthetic peptide has no antibacterial activity against S.agalactiae ATCC 12386, S.epidermidis, P.aeruginosa and methicillin-resistant S.aureus (MRSA).</text>
  </comment>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <feature type="peptide" id="PRO_0000419697" description="Antimicrobial protein AN5-1" evidence="1">
    <location>
      <begin position="1"/>
      <end position="12"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="23508455"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="23508455"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="12" mass="1318" checksum="5DE3A6A3A8B73767" modified="2012-10-03" version="1">YSKSLPLSVLNP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>