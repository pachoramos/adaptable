<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-02-08" modified="2023-02-22" version="29" xmlns="http://uniprot.org/uniprot">
  <accession>D2KX91</accession>
  <name>NA2X_HETHE</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Delta-thalatoxin-Hhe1a</fullName>
      <shortName evidence="6">Delta-TATX-Hhe1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Delta-thalatoxin-Hh1x</fullName>
      <shortName evidence="5">Delta-TLTX-Hh1x</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="8">Hh X</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Heterodactyla hemprichii</name>
    <name type="common">Hemprich's sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="659514"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Nynantheae</taxon>
      <taxon>Thalassianthidae</taxon>
      <taxon>Heterodactyla</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2010" name="Comp. Biochem. Physiol." volume="157B" first="389" last="393">
      <title>Isolation and cDNA cloning of type 2 sodium channel peptide toxins from three species of sea anemones (Cryptodendrum adhaesivum, Heterodactyla hemprichii and Thalassianthus aster) belonging to the family Thalassianthidae.</title>
      <authorList>
        <person name="Maeda M."/>
        <person name="Honma T."/>
        <person name="Shiomi K."/>
      </authorList>
      <dbReference type="PubMed" id="20817118"/>
      <dbReference type="DOI" id="10.1016/j.cbpb.2010.08.008"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 36-67</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Binds specifically to the voltage-gated sodium channel (Nav) and delays its inactivation.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="4">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the sea anemone sodium channel inhibitory toxin family. Type II subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AB512762">
    <property type="protein sequence ID" value="BAI66463.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="D2KX91"/>
  <dbReference type="SMR" id="D2KX91"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00706">
    <property type="entry name" value="Toxin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000404207" evidence="4">
    <location>
      <begin position="20"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000404208" description="Delta-thalatoxin-Hhe1a">
    <location>
      <begin position="36"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="38"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="40"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="61"/>
      <end position="79"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P19651"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="20817118"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="20817118"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="BAI66463.1"/>
    </source>
  </evidence>
  <sequence length="84" mass="9221" checksum="CF24AAF9E088F93A" modified="2010-03-02" version="1" precursor="true">MAYQKIVFVALMLVLAVSAMRLPDQQDQDISVAKRVACKCDDDGPDIRSATLTGTVDLGSCNEGWEKCASYYTVVADCCRRPRS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>