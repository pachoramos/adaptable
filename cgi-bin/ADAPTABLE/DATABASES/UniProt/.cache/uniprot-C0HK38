<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2016-10-05" modified="2022-12-14" version="9" xmlns="http://uniprot.org/uniprot">
  <accession>C0HK38</accession>
  <name>CYMC4_MELCT</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Cyclotide mech-4</fullName>
    </recommendedName>
  </protein>
  <organism evidence="4">
    <name type="scientific">Melicytus chathamicus</name>
    <name type="common">Chatham Island mahoe</name>
    <name type="synonym">Hymenanthera latifolia var. chathamica</name>
    <dbReference type="NCBI Taxonomy" id="453349"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Malpighiales</taxon>
      <taxon>Violaceae</taxon>
      <taxon>Melicytus</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2015" name="ACS Chem. Biol." volume="10" first="2491" last="2500">
      <title>Lysine-rich cyclotides: a new subclass of circular knotted proteins from Violaceae.</title>
      <authorList>
        <person name="Ravipati A.S."/>
        <person name="Henriques S.T."/>
        <person name="Poth A.G."/>
        <person name="Kaas Q."/>
        <person name="Wang C.K."/>
        <person name="Colgrave M.L."/>
        <person name="Craik D.J."/>
      </authorList>
      <dbReference type="PubMed" id="26322745"/>
      <dbReference type="DOI" id="10.1021/acschembio.5b00454"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>PRESENCE OF DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2">Probably participates in a plant defense mechanism (Potential). Binds to and induces leakage in phospholipd membranes, particularly ones containing 1-palmitoyl-2-oleophosphatidylethanolamine (POPE) (By similarity).</text>
  </comment>
  <comment type="domain">
    <text evidence="5">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">This is a cyclic peptide.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">Contains 3 disulfide bonds.</text>
  </comment>
  <comment type="mass spectrometry" mass="3217.41" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the cyclotide family. Bracelet subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="2">This peptide is cyclic. The start position was chosen by similarity to Oak1 (kalata B1) for which the DNA sequence is known.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HK38"/>
  <dbReference type="SMR" id="C0HK38"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012323">
    <property type="entry name" value="Cyclotide_bracelet_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF037891">
    <property type="entry name" value="Cycloviolacin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60008">
    <property type="entry name" value="CYCLOTIDE_BRACELET"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000437517" description="Cyclotide mech-4" evidence="2 3">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="5"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="9"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="14"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Gly-Asp)" evidence="6">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="unsure residue" description="I or L" evidence="3">
    <location>
      <position position="3"/>
    </location>
  </feature>
  <feature type="unsure residue" description="I or L" evidence="3">
    <location>
      <position position="12"/>
    </location>
  </feature>
  <feature type="unsure residue" description="I or L" evidence="3">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="3">
    <location>
      <position position="18"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="3">
    <location>
      <position position="19"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HK36"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="26322745"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="26322745"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="26322745"/>
    </source>
  </evidence>
  <sequence length="31" mass="3244" checksum="3E36AA52E48784C5" modified="2016-10-05" version="1">GSIPCGESCVYIPCISSLLGCSCKSKVCYKD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>