<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-11-27" version="71" xmlns="http://uniprot.org/uniprot">
  <accession>P80407</accession>
  <name>DEFI_PALPR</name>
  <protein>
    <recommendedName>
      <fullName>Defensin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Palomena prasina</name>
    <name type="common">Green shield bug</name>
    <name type="synonym">Cimex prasinus</name>
    <dbReference type="NCBI Taxonomy" id="55431"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Paraneoptera</taxon>
      <taxon>Hemiptera</taxon>
      <taxon>Heteroptera</taxon>
      <taxon>Panheteroptera</taxon>
      <taxon>Pentatomomorpha</taxon>
      <taxon>Pentatomoidea</taxon>
      <taxon>Pentatomidae</taxon>
      <taxon>Pentatominae</taxon>
      <taxon>Palomena</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="J. Insect Physiol." volume="42" first="81" last="89">
      <title>The inducible antibacterial peptides of the hemipteran insect Palomena prasina: identification of a unique family of proline-rich peptides and of a novel insect defensin.</title>
      <authorList>
        <person name="Chernysh S."/>
        <person name="Cociancich S."/>
        <person name="Briand J.-P."/>
        <person name="Hetru C."/>
        <person name="Bulet P."/>
      </authorList>
      <dbReference type="DOI" id="10.1016/0022-1910(95)00085-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Hemolymph</tissue>
      <tissue>Larval hemolymph</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Antibacterial peptide active against Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text>By bacterial infection.</text>
  </comment>
  <comment type="mass spectrometry" mass="4614.2" error="0.3" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the invertebrate defensin family. Type 1 subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P80407"/>
  <dbReference type="SMR" id="P80407"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000005">
    <property type="entry name" value="Defensin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001542">
    <property type="entry name" value="Defensin_invertebrate/fungal"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01097">
    <property type="entry name" value="Defensin_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51378">
    <property type="entry name" value="INVERT_DEFENSINS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000074473" description="Defensin">
    <location>
      <begin position="1"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="3"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="20"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="41"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00710"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source ref="1"/>
  </evidence>
  <sequence length="43" mass="4648" checksum="391EF2E8869A694D" modified="1995-11-01" version="1">ATCDALSFSSKWLTVNHSACAIHCLTKGYKGGRCVNTICNCRN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>