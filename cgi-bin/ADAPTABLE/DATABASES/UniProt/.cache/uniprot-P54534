<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-10-01" modified="2024-11-27" version="116" xmlns="http://uniprot.org/uniprot">
  <accession>P54534</accession>
  <name>BRXB_BACSU</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Bacilliredoxin BrxB</fullName>
      <shortName evidence="4">Brx-B</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6">Bacilliredoxin YqiW</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="4">Dithiol bacilliredoxin</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="4" type="primary">brxB</name>
    <name evidence="4" type="synonym">yqiW</name>
    <name type="ordered locus">BSU23990</name>
  </gene>
  <organism>
    <name type="scientific">Bacillus subtilis (strain 168)</name>
    <dbReference type="NCBI Taxonomy" id="224308"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Bacillaceae</taxon>
      <taxon>Bacillus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Microbiology" volume="142" first="3103" last="3111">
      <title>Systematic sequencing of the 283 kb 210 degrees-232 degrees region of the Bacillus subtilis genome containing the skin element and many sporulation genes.</title>
      <authorList>
        <person name="Mizuno M."/>
        <person name="Masuda S."/>
        <person name="Takemaru K."/>
        <person name="Hosono S."/>
        <person name="Sato T."/>
        <person name="Takeuchi M."/>
        <person name="Kobayashi Y."/>
      </authorList>
      <dbReference type="PubMed" id="8969508"/>
      <dbReference type="DOI" id="10.1099/13500872-142-11-3103"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>168 / JH642</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="Nature" volume="390" first="249" last="256">
      <title>The complete genome sequence of the Gram-positive bacterium Bacillus subtilis.</title>
      <authorList>
        <person name="Kunst F."/>
        <person name="Ogasawara N."/>
        <person name="Moszer I."/>
        <person name="Albertini A.M."/>
        <person name="Alloni G."/>
        <person name="Azevedo V."/>
        <person name="Bertero M.G."/>
        <person name="Bessieres P."/>
        <person name="Bolotin A."/>
        <person name="Borchert S."/>
        <person name="Borriss R."/>
        <person name="Boursier L."/>
        <person name="Brans A."/>
        <person name="Braun M."/>
        <person name="Brignell S.C."/>
        <person name="Bron S."/>
        <person name="Brouillet S."/>
        <person name="Bruschi C.V."/>
        <person name="Caldwell B."/>
        <person name="Capuano V."/>
        <person name="Carter N.M."/>
        <person name="Choi S.-K."/>
        <person name="Codani J.-J."/>
        <person name="Connerton I.F."/>
        <person name="Cummings N.J."/>
        <person name="Daniel R.A."/>
        <person name="Denizot F."/>
        <person name="Devine K.M."/>
        <person name="Duesterhoeft A."/>
        <person name="Ehrlich S.D."/>
        <person name="Emmerson P.T."/>
        <person name="Entian K.-D."/>
        <person name="Errington J."/>
        <person name="Fabret C."/>
        <person name="Ferrari E."/>
        <person name="Foulger D."/>
        <person name="Fritz C."/>
        <person name="Fujita M."/>
        <person name="Fujita Y."/>
        <person name="Fuma S."/>
        <person name="Galizzi A."/>
        <person name="Galleron N."/>
        <person name="Ghim S.-Y."/>
        <person name="Glaser P."/>
        <person name="Goffeau A."/>
        <person name="Golightly E.J."/>
        <person name="Grandi G."/>
        <person name="Guiseppi G."/>
        <person name="Guy B.J."/>
        <person name="Haga K."/>
        <person name="Haiech J."/>
        <person name="Harwood C.R."/>
        <person name="Henaut A."/>
        <person name="Hilbert H."/>
        <person name="Holsappel S."/>
        <person name="Hosono S."/>
        <person name="Hullo M.-F."/>
        <person name="Itaya M."/>
        <person name="Jones L.-M."/>
        <person name="Joris B."/>
        <person name="Karamata D."/>
        <person name="Kasahara Y."/>
        <person name="Klaerr-Blanchard M."/>
        <person name="Klein C."/>
        <person name="Kobayashi Y."/>
        <person name="Koetter P."/>
        <person name="Koningstein G."/>
        <person name="Krogh S."/>
        <person name="Kumano M."/>
        <person name="Kurita K."/>
        <person name="Lapidus A."/>
        <person name="Lardinois S."/>
        <person name="Lauber J."/>
        <person name="Lazarevic V."/>
        <person name="Lee S.-M."/>
        <person name="Levine A."/>
        <person name="Liu H."/>
        <person name="Masuda S."/>
        <person name="Mauel C."/>
        <person name="Medigue C."/>
        <person name="Medina N."/>
        <person name="Mellado R.P."/>
        <person name="Mizuno M."/>
        <person name="Moestl D."/>
        <person name="Nakai S."/>
        <person name="Noback M."/>
        <person name="Noone D."/>
        <person name="O'Reilly M."/>
        <person name="Ogawa K."/>
        <person name="Ogiwara A."/>
        <person name="Oudega B."/>
        <person name="Park S.-H."/>
        <person name="Parro V."/>
        <person name="Pohl T.M."/>
        <person name="Portetelle D."/>
        <person name="Porwollik S."/>
        <person name="Prescott A.M."/>
        <person name="Presecan E."/>
        <person name="Pujic P."/>
        <person name="Purnelle B."/>
        <person name="Rapoport G."/>
        <person name="Rey M."/>
        <person name="Reynolds S."/>
        <person name="Rieger M."/>
        <person name="Rivolta C."/>
        <person name="Rocha E."/>
        <person name="Roche B."/>
        <person name="Rose M."/>
        <person name="Sadaie Y."/>
        <person name="Sato T."/>
        <person name="Scanlan E."/>
        <person name="Schleich S."/>
        <person name="Schroeter R."/>
        <person name="Scoffone F."/>
        <person name="Sekiguchi J."/>
        <person name="Sekowska A."/>
        <person name="Seror S.J."/>
        <person name="Serror P."/>
        <person name="Shin B.-S."/>
        <person name="Soldo B."/>
        <person name="Sorokin A."/>
        <person name="Tacconi E."/>
        <person name="Takagi T."/>
        <person name="Takahashi H."/>
        <person name="Takemaru K."/>
        <person name="Takeuchi M."/>
        <person name="Tamakoshi A."/>
        <person name="Tanaka T."/>
        <person name="Terpstra P."/>
        <person name="Tognoni A."/>
        <person name="Tosato V."/>
        <person name="Uchiyama S."/>
        <person name="Vandenbol M."/>
        <person name="Vannier F."/>
        <person name="Vassarotti A."/>
        <person name="Viari A."/>
        <person name="Wambutt R."/>
        <person name="Wedler E."/>
        <person name="Wedler H."/>
        <person name="Weitzenegger T."/>
        <person name="Winters P."/>
        <person name="Wipat A."/>
        <person name="Yamamoto H."/>
        <person name="Yamane K."/>
        <person name="Yasumoto K."/>
        <person name="Yata K."/>
        <person name="Yoshida K."/>
        <person name="Yoshikawa H.-F."/>
        <person name="Zumstein E."/>
        <person name="Yoshikawa H."/>
        <person name="Danchin A."/>
      </authorList>
      <dbReference type="PubMed" id="9384377"/>
      <dbReference type="DOI" id="10.1038/36786"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>168</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1994" name="J. Biol. Chem." volume="269" first="28506" last="28513">
      <title>A protein that activates expression of a multidrug efflux transporter upon binding the transporter substrates.</title>
      <authorList>
        <person name="Ahmed M."/>
        <person name="Borsch C.M."/>
        <person name="Taylor S.S."/>
        <person name="Vazquez-Laslop N."/>
        <person name="Neyfakh A.A."/>
      </authorList>
      <dbReference type="PubMed" id="7961792"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)46956-6"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] OF 1-21</scope>
    <source>
      <strain>168 / Marburg / ATCC 6051 / DSM 10 / JCM 1465 / NBRC 13719 / NCIMB 3610 / NRRL NRS-744 / VKM B-501</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2014" name="Antioxid. Redox Signal." volume="21" first="357" last="367">
      <title>Redox regulation in Bacillus subtilis: The bacilliredoxins BrxA(YphP) and BrxB(YqiW) function in de-bacillithiolation of S-bacillithiolated OhrR and MetE.</title>
      <authorList>
        <person name="Gaballa A."/>
        <person name="Chi B.K."/>
        <person name="Roberts A.A."/>
        <person name="Becher D."/>
        <person name="Hamilton C.J."/>
        <person name="Antelmann H."/>
        <person name="Helmann J.D."/>
      </authorList>
      <dbReference type="PubMed" id="24313874"/>
      <dbReference type="DOI" id="10.1089/ars.2013.5327"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 42-60</scope>
    <scope>FUNCTION</scope>
    <scope>INDUCTION</scope>
    <scope>PTM</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <scope>MOTIF</scope>
    <scope>ACTIVE SITE</scope>
    <scope>POST-TRANSLATIONAL MODIFICATION AT CYS-52</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>MUTAGENESIS OF CYS-52 AND CYS-54</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2021" name="Redox Biol." volume="42" first="101935" last="101935">
      <title>The Bacillus subtilis monothiol bacilliredoxin BrxC (YtxJ) and the Bdr (YpdA) disulfide reductase reduce S-bacillithiolated proteins.</title>
      <authorList>
        <person name="Gaballa A."/>
        <person name="Su T.T."/>
        <person name="Helmann J.D."/>
      </authorList>
      <dbReference type="PubMed" id="33722570"/>
      <dbReference type="DOI" id="10.1016/j.redox.2021.101935"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH BRXC</scope>
    <scope>PTM</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <strain evidence="5">168 / CU1065</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">S-bacillithiolation is the formation of mixed disulfide bonds between protein thiols and the general thiol reductant bacillithiol (BSH) under oxidative stress. BSH is an equivalent of glutathione (GSH) in Firmicutes. This protein is a dithiol bacilliredoxin, which debacillithiolates (removes BSH) the S-bacillithiolated OhrR (OhrR-SSB) in vitro and in vivo NaOCl-generated S-bacillithiolated MetE (MetE-SSB). Involved in maintaining redox homeostasis in response to disulfide stress conditions.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Interacts with BrxC.</text>
  </comment>
  <comment type="induction">
    <text evidence="1">Expression is up-regulated by cumene hydroperoxide (CHP).</text>
  </comment>
  <comment type="PTM">
    <text evidence="1 2">N-terminal Cys of the CXC active site motif can react with bacillithiol (BSH) to form mixed disulfides. S-bacillithiolation protects Cys residues against overoxidation by acting as a redox switch in response to oxidative stress.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="3">Sensitive to cumene hydroperoxide (CHP) as indicated by growth inhibition assay. No effect on growth after NaOCl treatment.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the bacilliredoxin family.</text>
  </comment>
  <dbReference type="EMBL" id="D84432">
    <property type="protein sequence ID" value="BAA12603.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL009126">
    <property type="protein sequence ID" value="CAB14330.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="L25604">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="E69962">
    <property type="entry name" value="E69962"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_390279.1">
    <property type="nucleotide sequence ID" value="NC_000964.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003226477.1">
    <property type="nucleotide sequence ID" value="NZ_OZ025638.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P54534"/>
  <dbReference type="SMR" id="P54534"/>
  <dbReference type="STRING" id="224308.BSU23990"/>
  <dbReference type="jPOST" id="P54534"/>
  <dbReference type="PaxDb" id="224308-BSU23990"/>
  <dbReference type="EnsemblBacteria" id="CAB14330">
    <property type="protein sequence ID" value="CAB14330"/>
    <property type="gene ID" value="BSU_23990"/>
  </dbReference>
  <dbReference type="GeneID" id="76978805"/>
  <dbReference type="GeneID" id="938683"/>
  <dbReference type="KEGG" id="bsu:BSU23990"/>
  <dbReference type="PATRIC" id="fig|224308.179.peg.2613"/>
  <dbReference type="eggNOG" id="ENOG502ZBVN">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="InParanoid" id="P54534"/>
  <dbReference type="OrthoDB" id="9793981at2"/>
  <dbReference type="PhylomeDB" id="P54534"/>
  <dbReference type="BioCyc" id="BSUB:BSU23990-MONOMER"/>
  <dbReference type="PRO" id="PR:P54534"/>
  <dbReference type="Proteomes" id="UP000001570">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016491">
    <property type="term" value="F:oxidoreductase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045454">
    <property type="term" value="P:cell redox homeostasis"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033194">
    <property type="term" value="P:response to hydroperoxide"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006979">
    <property type="term" value="P:response to oxidative stress"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.30.10">
    <property type="entry name" value="Glutaredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009474">
    <property type="entry name" value="BrxB/BrxA"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR04191">
    <property type="entry name" value="YphP_YqiW"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR40052:SF1">
    <property type="entry name" value="UPF0403 PROTEIN SAOUHSC_01610"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR40052">
    <property type="entry name" value="UPF0403 PROTEIN YQIW-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06491">
    <property type="entry name" value="Disulph_isomer"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-0676">Redox-active center</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000049827" description="Bacilliredoxin BrxB">
    <location>
      <begin position="1"/>
      <end position="145"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="CXC active site motif" evidence="1">
    <location>
      <begin position="52"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="active site" description="Nucleophile" evidence="1">
    <location>
      <position position="52"/>
    </location>
  </feature>
  <feature type="active site" description="Nucleophile" evidence="1">
    <location>
      <position position="54"/>
    </location>
  </feature>
  <feature type="modified residue" description="S-bacillithiol cysteine disulfide" evidence="1">
    <location>
      <position position="52"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Redox-active" evidence="1">
    <location>
      <begin position="52"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of debacillithiolation of the S-bacillithiolated OhrR (OhrR-SSB) by the NaBH(4)-reduced form of this protein in vitro. Loss of regeneration of active OhrR with DNA-binding activity in vitro. Loss of debacillithiolation of in vivo NaOCl-generated S-bacillithiolated MetE (MetE-SSB)." evidence="1">
    <original>C</original>
    <variation>A</variation>
    <location>
      <position position="52"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Accumulation of S-bacillithiolated form due to lack of this resolving cysteine residue. Debacillithiolation of the S-bacillithiolated OhrR (OhrR-SSB) without the prior NaBH(4) reduction of this protein in vitro. Regenerates active OhrR with DNA-binding activity, but unable to activate the S-cysteinyl cysteine modified form of OhrR (OhrR-SSCys) in vitro. Debacillithiolation of in vivo NaOCl-generated S-bacillithiolated MetE (MetE-SSB)." evidence="1">
    <original>C</original>
    <variation>A</variation>
    <location>
      <position position="54"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="24313874"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="33722570"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7961792"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="24313874"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="33722570"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="24313874"/>
    </source>
  </evidence>
  <sequence length="145" mass="16196" checksum="612994D34B9237A9" modified="1996-10-01" version="1">MNMDFNLFMNDIVRQARQEITAAGYTELKTAEEVDEALTKKGTTLVMVNSVCGCAGGIARPAAYHSVHYDKRPDQLVTVFAGQDKEATARARDYFEGYPPSSPSFAILKDGKIMKMVERHEIEGHEPMAVVAKLQEAFEEYCEEV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>