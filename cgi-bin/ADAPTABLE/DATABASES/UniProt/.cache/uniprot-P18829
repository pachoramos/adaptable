<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2022-05-25" version="76" xmlns="http://uniprot.org/uniprot">
  <accession>P18829</accession>
  <name>AKH1_SCHGR</name>
  <protein>
    <recommendedName>
      <fullName>Adipokinetic prohormone type 1</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Adipokinetic hormone 1</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Adipokinetic hormone I</fullName>
        <shortName>AKH-I</shortName>
      </alternativeName>
    </component>
    <component>
      <recommendedName>
        <fullName>Adipokinetic hormone I4-10</fullName>
        <shortName>AKH-I4-10</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Adipokinetic hormone precursor-related peptide alpha chain</fullName>
        <shortName>APRP-alpha</shortName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Schistocerca gregaria</name>
    <name type="common">Desert locust</name>
    <name type="synonym">Gryllus gregarius</name>
    <dbReference type="NCBI Taxonomy" id="7010"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Orthoptera</taxon>
      <taxon>Caelifera</taxon>
      <taxon>Acrididea</taxon>
      <taxon>Acridomorpha</taxon>
      <taxon>Acridoidea</taxon>
      <taxon>Acrididae</taxon>
      <taxon>Cyrtacanthacridinae</taxon>
      <taxon>Schistocerca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1989" name="Neuron" volume="2" first="1369" last="1373">
      <title>Synthesis of a homodimer neurohormone precursor of locust adipokinetic hormone studied by in vitro translation and cDNA cloning.</title>
      <authorList>
        <person name="Schulz-Aellen M.F."/>
        <person name="Roulet E."/>
        <person name="Fischer-Lougheed J."/>
        <person name="O'Shea M."/>
      </authorList>
      <dbReference type="PubMed" id="2576372"/>
      <dbReference type="DOI" id="10.1016/0896-6273(89)90075-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1989" name="Neuron" volume="2" first="1363" last="1368">
      <title>Dimer structure of a neuropeptide precursor established: consequences for processing.</title>
      <authorList>
        <person name="Hekimi S."/>
        <person name="Burkhart W."/>
        <person name="Moyer M."/>
        <person name="Fowler E."/>
        <person name="O'Shea M."/>
      </authorList>
      <dbReference type="PubMed" id="2627375"/>
      <dbReference type="DOI" id="10.1016/0896-6273(89)90074-3"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 23-63</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1976" name="Nature" volume="263" first="207" last="211">
      <title>Structure of locust adipokinetic hormone, a neurohormone that regulates lipid utilisation during flight.</title>
      <authorList>
        <person name="Stone J.V."/>
        <person name="Mordue W."/>
        <person name="Batley K.E."/>
        <person name="Morris H.R."/>
      </authorList>
      <dbReference type="PubMed" id="958472"/>
      <dbReference type="DOI" id="10.1038/263207a0"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 23-32</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-23</scope>
    <scope>AMIDATION AT THR-32</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1991" name="J. Neurosci." volume="11" first="3246" last="3256">
      <title>Regulation of neuropeptide stoichiometry in neurosecretory cells.</title>
      <authorList>
        <person name="Hekimi S."/>
        <person name="Fischer-Lougheed J."/>
        <person name="O'Shea M."/>
      </authorList>
      <dbReference type="PubMed" id="1941082"/>
      <dbReference type="DOI" id="10.1523/jneurosci.11-10-03246.1991"/>
    </citation>
    <scope>PROTEOLYTIC PROCESSING</scope>
  </reference>
  <comment type="function">
    <text>This hormone, released from cells in the corpora cardiaca, causes release of diglycerides from the fat body and stimulation of muscles to use these diglycerides as an energy source during energy-demanding processes.</text>
  </comment>
  <comment type="subunit">
    <text>Adipokinetic hormone precursor-related peptide (APRP) can form three type of disulfide-bond dimers: p1 (alpha-alpha), p2 (alpha-beta), and p3 (beta-beta).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the AKH/HRTH/RPCH family.</text>
  </comment>
  <dbReference type="PIR" id="A93412">
    <property type="entry name" value="AKLQS"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P18829"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007629">
    <property type="term" value="P:flight behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002047">
    <property type="entry name" value="Adipokinetic_hormone_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010475">
    <property type="entry name" value="AKH/RPCH_hormone"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06377">
    <property type="entry name" value="Adipokin_hormo"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00256">
    <property type="entry name" value="AKH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0286">Flight</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1 2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000000916" description="Adipokinetic prohormone type 1">
    <location>
      <begin position="23"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000000917" description="Adipokinetic hormone 1">
    <location>
      <begin position="23"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000000918" description="Adipokinetic hormone I4-10">
    <location>
      <begin position="26"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000000919" description="Adipokinetic hormone precursor-related peptide alpha chain">
    <location>
      <begin position="36"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="2">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="modified residue" description="Threonine amide" evidence="2">
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain">
    <location>
      <position position="61"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2627375"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="958472"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="63" mass="6838" checksum="0BCDFAADF073914B" modified="1990-11-01" version="1" precursor="true">MVQRCLVVALLVVVVAAALCSAQLNFTPNWGTGKRDAADFGDPYSFLYRLIQAEARKMSGCSN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>