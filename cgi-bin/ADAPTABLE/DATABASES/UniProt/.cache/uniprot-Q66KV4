<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-02-21" modified="2024-11-27" version="71" xmlns="http://uniprot.org/uniprot">
  <accession>Q66KV4</accession>
  <name>BAFB_XENLA</name>
  <protein>
    <recommendedName>
      <fullName>Barrier-to-autointegration factor B</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">banf1-b</name>
    <name type="synonym">baf-b</name>
  </gene>
  <organism>
    <name type="scientific">Xenopus laevis</name>
    <name type="common">African clawed frog</name>
    <dbReference type="NCBI Taxonomy" id="8355"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Pipoidea</taxon>
      <taxon>Pipidae</taxon>
      <taxon>Xenopodinae</taxon>
      <taxon>Xenopus</taxon>
      <taxon>Xenopus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2004-07" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="NIH - Xenopus Gene Collection (XGC) project"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2009" name="Dev. Biol." volume="327" first="497" last="507">
      <title>Involvement of an inner nuclear membrane protein, Nemp1, in Xenopus neural development through an interaction with the chromatin protein BAF.</title>
      <authorList>
        <person name="Mamada H."/>
        <person name="Takahashi N."/>
        <person name="Taira M."/>
      </authorList>
      <dbReference type="PubMed" id="19167377"/>
      <dbReference type="DOI" id="10.1016/j.ydbio.2008.12.038"/>
    </citation>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INTERACTION WITH NEMP1A AND NEMP1B</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Non-specific DNA-binding protein that plays key roles in mitotic nuclear reassembly, chromatin organization, DNA damage response, gene expression and intrinsic immunity against foreign DNA. Contains two non-specific double-stranded DNA (dsDNA)-binding sites which promote DNA cross-bridging. Plays a key role in nuclear membrane reformation at the end of mitosis by driving formation of a single nucleus in a spindle-independent manner. Transiently cross-bridges anaphase chromosomes via its ability to bridge distant DNA sites, leading to the formation of a dense chromatin network at the chromosome ensemble surface that limits membranes to the surface. Also acts as a negative regulator of innate immune activation by restricting CGAS activity toward self-DNA upon acute loss of nuclear membrane integrity. Outcompetes CGAS for DNA-binding, thereby preventing CGAS activation and subsequent damaging autoinflammatory responses. Also involved in DNA damage response; acts by inhibiting the ADP-ribosyltransferase activity of PARP1. Involved in the recognition of exogenous dsDNA in the cytosol: associates with exogenous dsDNA immediately after its appearance in the cytosol at endosome breakdown and is required to avoid autophagy.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 2">Homodimer (By similarity). Interacts with nemp1a and nemp1b (PubMed:19167377).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Chromosome</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Nucleus envelope</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
    <text evidence="1">Significantly enriched at the nuclear inner membrane, diffusely throughout the nucleus during interphase and concentrated at the chromosomes during the M-phase.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="2">At the early gastrula stage, expressed mainly in the entire animal hemisphere. During neurulation, its expression becomes restricted to the anterior neuroectoderm. At the tailbud stage, expressed in various anterior regions including the anterior central nervous system (CNS), otic vesicles, and branchial arches.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">Has a helix-hairpin-helix (HhH) structural motif conserved among proteins that bind non-specifically to DNA.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the BAF family.</text>
  </comment>
  <dbReference type="EMBL" id="BC078546">
    <property type="protein sequence ID" value="AAH78546.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001087314.1">
    <property type="nucleotide sequence ID" value="NM_001093845.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q66KV4"/>
  <dbReference type="SMR" id="Q66KV4"/>
  <dbReference type="BioGRID" id="103997">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="IntAct" id="Q66KV4">
    <property type="interactions" value="2"/>
  </dbReference>
  <dbReference type="DNASU" id="447137"/>
  <dbReference type="GeneID" id="447137"/>
  <dbReference type="KEGG" id="xla:447137"/>
  <dbReference type="AGR" id="Xenbase:XB-GENE-6254337"/>
  <dbReference type="CTD" id="447137"/>
  <dbReference type="Xenbase" id="XB-GENE-6254337">
    <property type="gene designation" value="banf1.S"/>
  </dbReference>
  <dbReference type="OMA" id="SKQQGDC"/>
  <dbReference type="OrthoDB" id="5393090at2759"/>
  <dbReference type="Proteomes" id="UP000186698">
    <property type="component" value="Chromosome 4S"/>
  </dbReference>
  <dbReference type="Bgee" id="447137">
    <property type="expression patterns" value="Expressed in oocyte and 19 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000785">
    <property type="term" value="C:chromatin"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005635">
    <property type="term" value="C:nuclear envelope"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010836">
    <property type="term" value="P:negative regulation of protein ADP-ribosylation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006979">
    <property type="term" value="P:response to oxidative stress"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.150.40:FF:000001">
    <property type="entry name" value="Barrier-to-autointegration factor B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.150.40">
    <property type="entry name" value="Barrier-to-autointegration factor, BAF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR051387">
    <property type="entry name" value="BAF"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004122">
    <property type="entry name" value="BAF_prot"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036617">
    <property type="entry name" value="BAF_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47507">
    <property type="entry name" value="BARRIER TO AUTOINTEGRATION FACTOR 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47507:SF6">
    <property type="entry name" value="BARRIER-TO-AUTOINTEGRATION FACTOR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02961">
    <property type="entry name" value="BAF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01023">
    <property type="entry name" value="BAF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47798">
    <property type="entry name" value="Barrier-to-autointegration factor, BAF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0158">Chromosome</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000223615" description="Barrier-to-autointegration factor B">
    <location>
      <begin position="1"/>
      <end position="90"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="O75531"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19167377"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="90" mass="10199" checksum="7F630D18484A0710" modified="2004-10-11" version="1">MSSTSQKHRDFVAEPMGEKSVQCLAGIGDTLGRRLEEKGFDKAYVVLGQFLVLKKDEELFKEWLKDACSANAKQSRDCYGCLKEWCDAFL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>