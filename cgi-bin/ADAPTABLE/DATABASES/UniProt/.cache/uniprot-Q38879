<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-12-01" modified="2024-11-27" version="178" xmlns="http://uniprot.org/uniprot">
  <accession>Q38879</accession>
  <accession>Q39240</accession>
  <name>TRXH2_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Thioredoxin H2</fullName>
      <shortName>AtTrxh2</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Thioredoxin 2</fullName>
      <shortName>AtTRX2</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">TRX2</name>
    <name type="ordered locus">At5g39950</name>
    <name type="ORF">MYH19.14</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Proc. Natl. Acad. Sci. U.S.A." volume="92" first="5620" last="5624">
      <title>Evidence for five divergent thioredoxin h sequences in Arabidopsis thaliana.</title>
      <authorList>
        <person name="Rivera-Madrid R."/>
        <person name="Mestres D."/>
        <person name="Marinho P."/>
        <person name="Jacquot J.-P."/>
        <person name="Decottignies P."/>
        <person name="Miginiac-Maslow M."/>
        <person name="Meyer Y."/>
      </authorList>
      <dbReference type="PubMed" id="7777559"/>
      <dbReference type="DOI" id="10.1073/pnas.92.12.5620"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
      <tissue>Callus</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1996" name="J. Mol. Evol." volume="42" first="422" last="431">
      <title>Intron position as an evolutionary marker of thioredoxins and thioredoxin domains.</title>
      <authorList>
        <person name="Sahrawy M."/>
        <person name="Hecht V."/>
        <person name="Lopez Jaramillo J."/>
        <person name="Chueca A."/>
        <person name="Chartier Y."/>
        <person name="Meyer Y."/>
      </authorList>
      <dbReference type="PubMed" id="8642611"/>
      <dbReference type="DOI" id="10.1007/bf02498636"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>cv. Landsberg erecta</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1998" name="DNA Res." volume="5" first="41" last="54">
      <title>Structural analysis of Arabidopsis thaliana chromosome 5. IV. Sequence features of the regions of 1,456,315 bp covered by nineteen physically assigned P1 and TAC clones.</title>
      <authorList>
        <person name="Sato S."/>
        <person name="Kaneko T."/>
        <person name="Kotani H."/>
        <person name="Nakamura Y."/>
        <person name="Asamizu E."/>
        <person name="Miyajima N."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="9628582"/>
      <dbReference type="DOI" id="10.1093/dnares/5.1.41"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2003" name="Science" volume="302" first="842" last="846">
      <title>Empirical analysis of transcriptional activity in the Arabidopsis genome.</title>
      <authorList>
        <person name="Yamada K."/>
        <person name="Lim J."/>
        <person name="Dale J.M."/>
        <person name="Chen H."/>
        <person name="Shinn P."/>
        <person name="Palm C.J."/>
        <person name="Southwick A.M."/>
        <person name="Wu H.C."/>
        <person name="Kim C.J."/>
        <person name="Nguyen M."/>
        <person name="Pham P.K."/>
        <person name="Cheuk R.F."/>
        <person name="Karlin-Newmann G."/>
        <person name="Liu S.X."/>
        <person name="Lam B."/>
        <person name="Sakano H."/>
        <person name="Wu T."/>
        <person name="Yu G."/>
        <person name="Miranda M."/>
        <person name="Quach H.L."/>
        <person name="Tripp M."/>
        <person name="Chang C.H."/>
        <person name="Lee J.M."/>
        <person name="Toriumi M.J."/>
        <person name="Chan M.M."/>
        <person name="Tang C.C."/>
        <person name="Onodera C.S."/>
        <person name="Deng J.M."/>
        <person name="Akiyama K."/>
        <person name="Ansari Y."/>
        <person name="Arakawa T."/>
        <person name="Banh J."/>
        <person name="Banno F."/>
        <person name="Bowser L."/>
        <person name="Brooks S.Y."/>
        <person name="Carninci P."/>
        <person name="Chao Q."/>
        <person name="Choy N."/>
        <person name="Enju A."/>
        <person name="Goldsmith A.D."/>
        <person name="Gurjal M."/>
        <person name="Hansen N.F."/>
        <person name="Hayashizaki Y."/>
        <person name="Johnson-Hopson C."/>
        <person name="Hsuan V.W."/>
        <person name="Iida K."/>
        <person name="Karnes M."/>
        <person name="Khan S."/>
        <person name="Koesema E."/>
        <person name="Ishida J."/>
        <person name="Jiang P.X."/>
        <person name="Jones T."/>
        <person name="Kawai J."/>
        <person name="Kamiya A."/>
        <person name="Meyers C."/>
        <person name="Nakajima M."/>
        <person name="Narusaka M."/>
        <person name="Seki M."/>
        <person name="Sakurai T."/>
        <person name="Satou M."/>
        <person name="Tamse R."/>
        <person name="Vaysberg M."/>
        <person name="Wallender E.K."/>
        <person name="Wong C."/>
        <person name="Yamamura Y."/>
        <person name="Yuan S."/>
        <person name="Shinozaki K."/>
        <person name="Davis R.W."/>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
      </authorList>
      <dbReference type="PubMed" id="14593172"/>
      <dbReference type="DOI" id="10.1126/science.1088305"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2004" name="Plant Cell Physiol." volume="45" first="18" last="27">
      <title>Target proteins of the cytosolic thioredoxins in Arabidopsis thaliana.</title>
      <authorList>
        <person name="Yamazaki D."/>
        <person name="Motohashi K."/>
        <person name="Kasama T."/>
        <person name="Hara Y."/>
        <person name="Hisabori T."/>
      </authorList>
      <dbReference type="PubMed" id="14749482"/>
      <dbReference type="DOI" id="10.1093/pcp/pch019"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2009" name="Mol. Plant" volume="2" first="308" last="322">
      <title>Comparative genomic study of the thioredoxin family in photosynthetic organisms with emphasis on Populus trichocarpa.</title>
      <authorList>
        <person name="Chibani K."/>
        <person name="Wingsle G."/>
        <person name="Jacquot J.P."/>
        <person name="Gelhaye E."/>
        <person name="Rouhier N."/>
      </authorList>
      <dbReference type="PubMed" id="19825616"/>
      <dbReference type="DOI" id="10.1093/mp/ssn076"/>
    </citation>
    <scope>GENE FAMILY</scope>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2010" name="Proc. Natl. Acad. Sci. U.S.A." volume="107" first="3900" last="3905">
      <title>A membrane-associated thioredoxin required for plant growth moves from cell to cell, suggestive of a role in intercellular communication.</title>
      <authorList>
        <person name="Meng L."/>
        <person name="Wong J.H."/>
        <person name="Feldman L.J."/>
        <person name="Lemaux P.G."/>
        <person name="Buchanan B.B."/>
      </authorList>
      <dbReference type="PubMed" id="20133584"/>
      <dbReference type="DOI" id="10.1073/pnas.0913759107"/>
    </citation>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2018" name="J. Exp. Bot." volume="69" first="3491" last="3505">
      <title>Self-protection of cytosolic malate dehydrogenase against oxidative stress in Arabidopsis.</title>
      <authorList>
        <person name="Huang J."/>
        <person name="Niazi A.K."/>
        <person name="Young D."/>
        <person name="Rosado L.A."/>
        <person name="Vertommen D."/>
        <person name="Bodra N."/>
        <person name="Abdelgawwad M.R."/>
        <person name="Vignols F."/>
        <person name="Wei B."/>
        <person name="Wahni K."/>
        <person name="Bashandy T."/>
        <person name="Bariat L."/>
        <person name="Van Breusegem F."/>
        <person name="Messens J."/>
        <person name="Reichheld J.P."/>
      </authorList>
      <dbReference type="PubMed" id="29194485"/>
      <dbReference type="DOI" id="10.1093/jxb/erx396"/>
    </citation>
    <scope>INTERACTION WITH MDH1</scope>
  </reference>
  <comment type="function">
    <text evidence="4">Thiol-disulfide oxidoreductase probably involved in the redox regulation of a number of cytosolic enzymes. Possesses insulin disulfide bonds reducing activity.</text>
  </comment>
  <comment type="subunit">
    <text evidence="6">Interacts with MDH1.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-727983">
      <id>Q38879</id>
    </interactant>
    <interactant intactId="EBI-25512418">
      <id>Q3E9D5</id>
      <label>SAMDC4</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="5">Mitochondrion</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the thioredoxin family. Plant H-type subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="Z35475">
    <property type="protein sequence ID" value="CAA84612.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U35826">
    <property type="protein sequence ID" value="AAC49353.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB010077">
    <property type="protein sequence ID" value="BAB10219.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002688">
    <property type="protein sequence ID" value="AED94495.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY048235">
    <property type="protein sequence ID" value="AAK82498.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY113052">
    <property type="protein sequence ID" value="AAM47360.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S58123">
    <property type="entry name" value="S58123"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_198811.1">
    <property type="nucleotide sequence ID" value="NM_123358.4"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q38879"/>
  <dbReference type="SMR" id="Q38879"/>
  <dbReference type="BioGRID" id="19243">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="IntAct" id="Q38879">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="3702.Q38879"/>
  <dbReference type="PaxDb" id="3702-AT5G39950.1"/>
  <dbReference type="ProteomicsDB" id="232421"/>
  <dbReference type="EnsemblPlants" id="AT5G39950.1">
    <property type="protein sequence ID" value="AT5G39950.1"/>
    <property type="gene ID" value="AT5G39950"/>
  </dbReference>
  <dbReference type="GeneID" id="833992"/>
  <dbReference type="Gramene" id="AT5G39950.1">
    <property type="protein sequence ID" value="AT5G39950.1"/>
    <property type="gene ID" value="AT5G39950"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT5G39950"/>
  <dbReference type="Araport" id="AT5G39950"/>
  <dbReference type="TAIR" id="AT5G39950">
    <property type="gene designation" value="TRX2"/>
  </dbReference>
  <dbReference type="eggNOG" id="KOG0907">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_090389_14_1_1"/>
  <dbReference type="InParanoid" id="Q38879"/>
  <dbReference type="OMA" id="CAPCKDI"/>
  <dbReference type="OrthoDB" id="1215770at2759"/>
  <dbReference type="PhylomeDB" id="Q38879"/>
  <dbReference type="BioCyc" id="ARA:AT5G39950-MONOMER"/>
  <dbReference type="PRO" id="PR:Q38879"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 5"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q38879">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016671">
    <property type="term" value="F:oxidoreductase activity, acting on a sulfur group of donors, disulfide as acceptor"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016712">
    <property type="term" value="F:oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen, reduced flavin or flavoprotein as one donor, and incorporation of one atom of oxygen"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015035">
    <property type="term" value="F:protein-disulfide reductase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd02947">
    <property type="entry name" value="TRX_family"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.40.30.10:FF:000104">
    <property type="entry name" value="Thioredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.30.10">
    <property type="entry name" value="Glutaredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005746">
    <property type="entry name" value="Thioredoxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050620">
    <property type="entry name" value="Thioredoxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036249">
    <property type="entry name" value="Thioredoxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR017937">
    <property type="entry name" value="Thioredoxin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013766">
    <property type="entry name" value="Thioredoxin_domain"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01068">
    <property type="entry name" value="thioredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10438">
    <property type="entry name" value="THIOREDOXIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10438:SF413">
    <property type="entry name" value="THIOREDOXIN H2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00085">
    <property type="entry name" value="Thioredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00421">
    <property type="entry name" value="THIOREDOXIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF52833">
    <property type="entry name" value="Thioredoxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00194">
    <property type="entry name" value="THIOREDOXIN_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51352">
    <property type="entry name" value="THIOREDOXIN_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0249">Electron transport</keyword>
  <keyword id="KW-0496">Mitochondrion</keyword>
  <keyword id="KW-0676">Redox-active center</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000120047" description="Thioredoxin H2">
    <location>
      <begin position="1"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="domain" description="Thioredoxin" evidence="2">
    <location>
      <begin position="6"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="active site" description="Nucleophile" evidence="1">
    <location>
      <position position="59"/>
    </location>
  </feature>
  <feature type="active site" description="Nucleophile" evidence="1">
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="site" description="Deprotonates C-terminal active site Cys" evidence="1">
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="site" description="Contributes to redox potential value" evidence="1">
    <location>
      <position position="60"/>
    </location>
  </feature>
  <feature type="site" description="Contributes to redox potential value" evidence="1">
    <location>
      <position position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Redox-active" evidence="2">
    <location>
      <begin position="59"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AAC49353." evidence="7" ref="2">
    <original>T</original>
    <variation>I</variation>
    <location>
      <position position="7"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AAC49353." evidence="7" ref="2">
    <original>S</original>
    <variation>SS</variation>
    <location>
      <position position="22"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AAC49353." evidence="7" ref="2">
    <original>K</original>
    <variation>Q</variation>
    <location>
      <position position="127"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00691"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="14749482"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="20133584"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="29194485"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="133" mass="14676" checksum="BC034E2BCD4D3CA3" modified="2001-11-02" version="2">MGGALSTVFGSGEDATAAGTESEPSRVLKFSSSARWQLHFNEIKESNKLLVVDFSASWCGPCRMIEPAIHAMADKFNDVDFVKLDVDELPDVAKEFNVTAMPTFVLVKRGKEIERIIGAKKDELEKKVSKLRA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>