<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-15" modified="2024-11-27" version="29" xmlns="http://uniprot.org/uniprot">
  <accession>P84355</accession>
  <name>PVK2_MUSDO</name>
  <protein>
    <recommendedName>
      <fullName>Periviscerokinin-2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Musdo-PVK-2</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Musca domestica</name>
    <name type="common">House fly</name>
    <dbReference type="NCBI Taxonomy" id="7370"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Muscoidea</taxon>
      <taxon>Muscidae</taxon>
      <taxon>Musca</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2003" name="Peptides" volume="24" first="1487" last="1491">
      <title>Mass spectrometric analysis of putative capa-gene products in Musca domestica and Neobellieria bullata.</title>
      <authorList>
        <person name="Predel R."/>
        <person name="Russell W.K."/>
        <person name="Tichy S.E."/>
        <person name="Russell D.H."/>
        <person name="Nachman R.J."/>
      </authorList>
      <dbReference type="PubMed" id="14706527"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2003.06.009"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT VAL-9</scope>
    <source>
      <tissue evidence="1">Ganglion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Mediates visceral muscle contractile activity (myotropic activity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Dorsal ganglionic sheath of fused ventral nerve cord.</text>
  </comment>
  <comment type="mass spectrometry" mass="973.4" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the periviscerokinin family.</text>
  </comment>
  <dbReference type="Proteomes" id="UP000095301">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694905">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044264" description="Periviscerokinin-2">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="1">
    <location>
      <position position="3"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="14706527"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="14706527"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="9" mass="974" checksum="8DD1176DD449C735" modified="2005-02-15" version="1">ASLFNAPRV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>