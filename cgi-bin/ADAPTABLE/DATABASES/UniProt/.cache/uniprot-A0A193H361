<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-04-12" modified="2024-01-24" version="14" xmlns="http://uniprot.org/uniprot">
  <accession>A0A193H361</accession>
  <accession>C0HK12</accession>
  <name>CZS11_CRUCA</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Cruzioseptin-11</fullName>
      <shortName evidence="5">CZS-11</shortName>
    </recommendedName>
  </protein>
  <organism evidence="8">
    <name type="scientific">Cruziohyla calcarifer</name>
    <name type="common">Splendid leaf frog</name>
    <name type="synonym">Agalychnis calcarifer</name>
    <dbReference type="NCBI Taxonomy" id="318249"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Cruziohyla</taxon>
    </lineage>
  </organism>
  <reference evidence="8" key="1">
    <citation type="journal article" date="2016" name="J. Proteomics" volume="146" first="1" last="13">
      <title>Peptidomic approach identifies cruzioseptins, a new family of potent antimicrobial peptides in the splendid leaf frog, Cruziohyla calcarifer.</title>
      <authorList>
        <person name="Proano-Bolanos C."/>
        <person name="Zhou M."/>
        <person name="Wang L."/>
        <person name="Coloma L.A."/>
        <person name="Chen T."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="27321580"/>
      <dbReference type="DOI" id="10.1016/j.jprot.2016.06.017"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 46-72</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>AMIDATION AT GLN-72</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="8">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antimicrobial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2780.5" method="Electrospray" evidence="4"/>
  <comment type="similarity">
    <text evidence="6">Belongs to the frog skin active peptide (FSAP) family. Cruzioseptin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="KX065087">
    <property type="protein sequence ID" value="ANN87767.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A193H361"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022731">
    <property type="entry name" value="Dermaseptin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016322">
    <property type="entry name" value="FSAP"/>
  </dbReference>
  <dbReference type="Pfam" id="PF12121">
    <property type="entry name" value="DD_K"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001822">
    <property type="entry name" value="Dermaseptin_precursor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439480" evidence="5">
    <location>
      <begin position="23"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000439481" description="Cruzioseptin-11" evidence="4">
    <location>
      <begin position="46"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439482" evidence="5">
    <location>
      <begin position="74"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="25"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glutamine amide" evidence="7">
    <location>
      <position position="72"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A193H362"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="27321580"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="27321580"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="27321580"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="ANN87767.1"/>
    </source>
  </evidence>
  <sequence length="75" mass="8422" checksum="E70DA2C3A8329070" modified="2016-10-05" version="1" precursor="true">MVKLKKSLFLVLFLGLVSLSICEEEKREEENEEVQEDDDQSEEKRGFLDIVKHVGKAAGKAALNAVTEMVNQGEQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>