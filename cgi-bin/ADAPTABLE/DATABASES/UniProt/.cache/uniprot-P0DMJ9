<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2014-07-09" modified="2024-10-02" version="23" xmlns="http://uniprot.org/uniprot">
  <accession>P0DMJ9</accession>
  <name>PA2H1_BOTMT</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Basic phospholipase A2 homolog BmatTX-I</fullName>
      <shortName>svPLA2 homolog</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Lys49 PLA2-like</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bothrops mattogrossensis</name>
    <name type="common">Pitviper</name>
    <name type="synonym">Bothrops neuwiedi mattogrossensis</name>
    <dbReference type="NCBI Taxonomy" id="1171125"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Viperidae</taxon>
      <taxon>Crotalinae</taxon>
      <taxon>Bothrops</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2014" name="Biomed. Res. Int." volume="2014" first="1" last="13">
      <title>Purification and biochemical characterization of three myotoxins from Bothrops mattogrossensis snake venom with toxicity against Leishmania and tumor cells.</title>
      <authorList>
        <person name="de Moura A.A."/>
        <person name="Kayano A.M."/>
        <person name="Oliveira G.A."/>
        <person name="Setubal S.S."/>
        <person name="Ribeiro J.G."/>
        <person name="Barros N.B."/>
        <person name="Nicolete R."/>
        <person name="Moura L.A."/>
        <person name="Fuly A.L."/>
        <person name="Nomizo A."/>
        <person name="da Silva S.L."/>
        <person name="Fernandes C.F."/>
        <person name="Zuliani J.P."/>
        <person name="Stabeli R.G."/>
        <person name="Soares A.M."/>
        <person name="Calderon L.A."/>
      </authorList>
      <dbReference type="PubMed" id="24724078"/>
      <dbReference type="DOI" id="10.1155/2014/195356"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 3">Snake venom phospholipase A2 homolog that lacks enzymatic activity (PubMed:24724078). Shows high myotoxic activity, neutrophile activation (demonstrated by activation induction of IL-1beta production), slight cytotoxicity against Jurkat (leukemia T) and SK-BR-3 (breast adenocarcinoma) tumor cell lines, and slight antiparasitic activity against promastigote forms of Leishmania amazonensis (PubMed:24724078). A model of myotoxic mechanism has been proposed: an apo Lys49-PLA2 is activated by the entrance of a hydrophobic molecule (e.g. fatty acid) at the hydrophobic channel of the protein leading to a reorientation of a monomer (By similarity). This reorientation causes a transition between 'inactive' to 'active' states, causing alignment of C-terminal and membrane-docking sites (MDoS) side-by-side and putting the membrane-disruption sites (MDiS) in the same plane, exposed to solvent and in a symmetric position for both monomers (By similarity). The MDoS region stabilizes the toxin on membrane by the interaction of charged residues with phospholipid head groups (By similarity). Subsequently, the MDiS region destabilizes the membrane with penetration of hydrophobic residues (By similarity). This insertion causes a disorganization of the membrane, allowing an uncontrolled influx of ions (i.e. calcium and sodium), and eventually triggering irreversible intracellular alterations and cell death (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="3">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="13304.0" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="5">Belongs to the phospholipase A2 family. Group II subfamily. K49 sub-subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="5">Does not bind calcium as one of the calcium-binding sites is lost (Asp-&gt;Lys in position 48, which corresponds to 'Lys-49' in the current nomenclature).</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DMJ9"/>
  <dbReference type="SMR" id="P0DMJ9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0047498">
    <property type="term" value="F:calcium-dependent phospholipase A2 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005543">
    <property type="term" value="F:phospholipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050482">
    <property type="term" value="P:arachidonic acid secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016042">
    <property type="term" value="P:lipid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006644">
    <property type="term" value="P:phospholipid metabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.90.10">
    <property type="entry name" value="Phospholipase A2 domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001211">
    <property type="entry name" value="PLipase_A2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016090">
    <property type="entry name" value="PLipase_A2_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036444">
    <property type="entry name" value="PLipase_A2_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033113">
    <property type="entry name" value="PLipase_A2_His_AS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716:SF94">
    <property type="entry name" value="PHOSPHOLIPASE A2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716">
    <property type="entry name" value="PHOSPHOLIPASE A2 FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00068">
    <property type="entry name" value="Phospholip_A2_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00389">
    <property type="entry name" value="PHPHLIPASEA2"/>
  </dbReference>
  <dbReference type="SMART" id="SM00085">
    <property type="entry name" value="PA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48619">
    <property type="entry name" value="Phospholipase A2, PLA2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00118">
    <property type="entry name" value="PA2_HIS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0959">Myotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000429740" description="Basic phospholipase A2 homolog BmatTX-I">
    <location>
      <begin position="1"/>
      <end position="51" status="greater than"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="26"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="28"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="43"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="49"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="50"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="51"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="I6L8L6"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q90249"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="24724078"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="24724078"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="24724078"/>
    </source>
  </evidence>
  <sequence length="51" mass="5512" checksum="02300F06EFD79D48" modified="2014-07-09" version="1" fragment="single">SLVELGKMILQETGKNPVTSYGAYGCNCGVLGHGKPKDATDRCCYVHKCCY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>