<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2024-01-24" modified="2024-07-24" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>C0HM61</accession>
  <name>NLTP1_CARCB</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Non-specific lipid-transfer protein 1</fullName>
      <shortName evidence="5">LTP1</shortName>
      <shortName evidence="4">nsLTP1</shortName>
    </recommendedName>
  </protein>
  <organism evidence="4">
    <name type="scientific">Carum carvi</name>
    <name type="common">Caraway</name>
    <dbReference type="NCBI Taxonomy" id="48032"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>asterids</taxon>
      <taxon>campanulids</taxon>
      <taxon>Apiales</taxon>
      <taxon>Apiaceae</taxon>
      <taxon>Apioideae</taxon>
      <taxon>apioid superclade</taxon>
      <taxon>Careae</taxon>
      <taxon>Carum</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2023" name="BMC Complement. Med. Ther." volume="23" first="254" last="254">
      <title>The structural characterization and bioactivity assessment of nonspecific lipid transfer protein 1 (nsLTP1) from caraway (Carum carvi) seeds.</title>
      <authorList>
        <person name="Aldakhil T."/>
        <person name="Alshammari S.O."/>
        <person name="Siraj B."/>
        <person name="El-Aarag B."/>
        <person name="Zarina S."/>
        <person name="Salehi D."/>
        <person name="Ahmed A."/>
      </authorList>
      <dbReference type="PubMed" id="37474939"/>
      <dbReference type="DOI" id="10.1186/s12906-023-04083-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue evidence="4">Seed</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Plant non-specific lipid-transfer proteins transfer phospholipids as well as galactolipids across membranes (By similarity). May play a role in wax or cutin deposition in the cell walls of expanding epidermal cells and certain secretory tissues (By similarity).</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Detected in seeds (at protein level).</text>
  </comment>
  <comment type="mass spectrometry" mass="9652.0" method="MALDI" evidence="3"/>
  <comment type="miscellaneous">
    <text evidence="3">Displays antioxidant and antiproliferative activity (PubMed:37474939). Exhibits antiproliferative activity towards the breast cancer cell lines MDA-MB-231 (IC(50)= 52.93 uM) and MCF-7 (IC(50)= 44.76 uM) (PubMed:37474939).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the plant LTP family.</text>
  </comment>
  <dbReference type="SMR" id="C0HM61"/>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008289">
    <property type="term" value="F:lipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006869">
    <property type="term" value="P:lipid transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd01960">
    <property type="entry name" value="nsLTP1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.110.10">
    <property type="entry name" value="Plant lipid-transfer and hydrophobic proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036312">
    <property type="entry name" value="Bifun_inhib/LTP/seed_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016140">
    <property type="entry name" value="Bifunc_inhib/LTP/seed_store"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000528">
    <property type="entry name" value="Plant_nsLTP"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33076:SF53">
    <property type="entry name" value="NODULE CYSTEINE-RICH (NCR) SECRETED PEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33076">
    <property type="entry name" value="NON-SPECIFIC LIPID-TRANSFER PROTEIN 2-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00234">
    <property type="entry name" value="Tryp_alpha_amyl"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00382">
    <property type="entry name" value="LIPIDTRNSFER"/>
  </dbReference>
  <dbReference type="SMART" id="SM00499">
    <property type="entry name" value="AAI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47699">
    <property type="entry name" value="Bifunctional inhibitor/lipid-transfer protein/seed storage 2S albumin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00597">
    <property type="entry name" value="PLANT_LTP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0446">Lipid-binding</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000459113" description="Non-specific lipid-transfer protein 1">
    <location>
      <begin position="1"/>
      <end position="91"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="4"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="14"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="29"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="49"/>
      <end position="88"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HLG2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q42952"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="37474939"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="37474939"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="91" mass="9652" checksum="BBFF982558949160" modified="2024-01-24" version="1">DIDCKTVDSALSPCIPYLLGGGTPTTDCCKGVSAIKDMSTTTDNKRNACKCVKTAAARYPSLKDEVAQALPDKCQVKLDIPISRNTNCDAI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>