<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-15" modified="2024-11-27" version="39" xmlns="http://uniprot.org/uniprot">
  <accession>P84387</accession>
  <name>XT1_XENTR</name>
  <protein>
    <recommendedName>
      <fullName>Antimicrobial peptide 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>XT-1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Xenopus tropicalis</name>
    <name type="common">Western clawed frog</name>
    <name type="synonym">Silurana tropicalis</name>
    <dbReference type="NCBI Taxonomy" id="8364"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Pipoidea</taxon>
      <taxon>Pipidae</taxon>
      <taxon>Xenopodinae</taxon>
      <taxon>Xenopus</taxon>
      <taxon>Silurana</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2001" name="Biochim. Biophys. Acta" volume="1550" first="81" last="89">
      <title>Antimicrobial peptides isolated from skin secretions of the diploid frog, Xenopus tropicalis (Pipidae).</title>
      <authorList>
        <person name="Ali M.F."/>
        <person name="Soto A."/>
        <person name="Knoop F.C."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="11738090"/>
      <dbReference type="DOI" id="10.1016/s0167-4838(01)00272-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="1">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has very weak antimicrobial activity against Gram-positive bacterium S.aureus and Gram-negative bacterium E.coli and stronger activity against yeast C.albicans. Enhances the antibacterial activity of XT3. Has hemolytic activity against human red blood cells.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2852.1" error="0.6" method="Electrospray" evidence="1"/>
  <dbReference type="AlphaFoldDB" id="P84387"/>
  <dbReference type="SMR" id="P84387"/>
  <dbReference type="InParanoid" id="P84387"/>
  <dbReference type="Proteomes" id="UP000008143">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044179">
    <property type="term" value="P:hemolysis in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043865" description="Antimicrobial peptide 1">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11738090"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="27" mass="2853" checksum="9A5D08F13222CFBA" modified="2005-02-15" version="1">GFLGPLLKLAAKGVAKVIPHLIPSRQQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>