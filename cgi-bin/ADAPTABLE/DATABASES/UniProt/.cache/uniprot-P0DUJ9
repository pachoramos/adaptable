<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2021-06-02" modified="2022-05-25" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>P0DUJ9</accession>
  <name>HBB_POTCF</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Antimicrobial protein PcfHb</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Potamotrygon cf. henlei</name>
    <name type="common">Freshwater stingray</name>
    <dbReference type="NCBI Taxonomy" id="2805830"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Chondrichthyes</taxon>
      <taxon>Elasmobranchii</taxon>
      <taxon>Batoidea</taxon>
      <taxon>Myliobatiformes</taxon>
      <taxon>Potamotrygonidae</taxon>
      <taxon>Potamotrygon</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="821" last="829">
      <title>Potamotrygon cf. henlei stingray mucus: biochemical features of a novel antimicrobial protein.</title>
      <authorList>
        <person name="Conceicao K."/>
        <person name="Monteiro-dos-Santos J."/>
        <person name="Seibert C.S."/>
        <person name="Silva P.I. Jr."/>
        <person name="Marques E.E."/>
        <person name="Richardson M."/>
        <person name="Lopes-Ferreira M."/>
      </authorList>
      <dbReference type="PubMed" id="22683678"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.025"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Shows antimicrobial activity against M.luteus (MIC=4 uM) and E.coli (MIC=12 uM), as well as against the yeast C.tropicalis (MIC=4 uM) (PubMed:22683678). Shows a pro-inflammatory effect, since the topical application of the protein induces an increase of cellular recruitment characterized by an increase in the number of leukocyte rolling (PubMed:22683678). Does not show hemolytic activity on human erythrocytes (at doses up to 100 uM) (PubMed:22683678).</text>
  </comment>
  <comment type="subunit">
    <text evidence="4">Possible monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <text evidence="1">Secreted in cutaneous mucus.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed in mucus-secreting tissues.</text>
  </comment>
  <comment type="mass spectrometry" mass="16072.8" method="Electrospray" evidence="1"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the globin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DUJ9"/>
  <dbReference type="SMR" id="P0DUJ9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000452542" description="Antimicrobial protein PcfHb" evidence="4">
    <location>
      <begin position="1"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="non-consecutive residues" evidence="4">
    <location>
      <begin position="32"/>
      <end position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="22683678"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="22683678"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="22683678"/>
    </source>
  </evidence>
  <sequence length="43" mass="4970" checksum="B268CE3A03A687B7" modified="2021-06-02" version="1" fragment="multiple">GKLTDSQEDYIRHVWDDVNRKLITAKALERVNLVAEALSSNYH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>