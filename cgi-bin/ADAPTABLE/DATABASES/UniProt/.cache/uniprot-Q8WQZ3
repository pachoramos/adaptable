<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-10-10" modified="2023-02-22" version="40" xmlns="http://uniprot.org/uniprot">
  <accession>Q8WQZ3</accession>
  <name>DEF_MAMBR</name>
  <protein>
    <recommendedName>
      <fullName evidence="4 5">Defensin</fullName>
    </recommendedName>
  </protein>
  <organism evidence="5">
    <name type="scientific">Mamestra brassicae</name>
    <name type="common">Cabbage moth</name>
    <dbReference type="NCBI Taxonomy" id="55057"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Noctuoidea</taxon>
      <taxon>Noctuidae</taxon>
      <taxon>Hadeninae</taxon>
      <taxon>Mamestra</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2003" name="Biol. Cell" volume="95" first="53" last="57">
      <title>Molecular characterization of a defensin in the IZD-MB-0503 cell line derived from immunocytes of the insect Mamestra brassicae (Lepidoptera).</title>
      <authorList>
        <person name="Mandrioli M."/>
        <person name="Bugli S."/>
        <person name="Saltini S."/>
        <person name="Genedani S."/>
        <person name="Ottaviani E."/>
      </authorList>
      <dbReference type="PubMed" id="12753953"/>
      <dbReference type="DOI" id="10.1016/s0248-4900(02)01219-4"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>INDUCTION</scope>
  </reference>
  <comment type="induction">
    <text evidence="3">By bacterial infection.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the invertebrate defensin family. Type 1 subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AF465486">
    <property type="protein sequence ID" value="AAL69980.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8WQZ3"/>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009617">
    <property type="term" value="P:response to bacterium"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001542">
    <property type="entry name" value="Defensin_invertebrate/fungal"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01097">
    <property type="entry name" value="Defensin_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000445348" description="Defensin" evidence="1">
    <location>
      <begin status="unknown"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="61"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="74"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="78"/>
      <end position="96"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00710"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12753953"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="12753953"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="5">
    <source>
      <dbReference type="EMBL" id="AAL69980.2"/>
    </source>
  </evidence>
  <sequence length="98" mass="10809" checksum="196C459995FD722C" modified="2009-10-13" version="2" precursor="true">MLCLADIRIVASCSAAIKSGYGQQPWLAHVAGPYANSLFDDVPADSYHAAVEYLRLIPASCYLLDGYAAGRDDCRAHCIAPRNRRLYCASYQVCVCRY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>