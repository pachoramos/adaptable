<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2016-05-11" modified="2022-05-25" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>C0HK00</accession>
  <name>OCE5_LEPPU</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Ocellatin-PT5</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Leptodactylus pustulatus</name>
    <name type="common">Ceara white-lipped frog</name>
    <dbReference type="NCBI Taxonomy" id="1349691"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Leptodactylidae</taxon>
      <taxon>Leptodactylinae</taxon>
      <taxon>Leptodactylus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2015" name="J. Nat. Prod." volume="78" first="1495" last="1504">
      <title>Characterization and biological activities of ocellatin peptides from the skin secretion of the frog Leptodactylus pustulatus.</title>
      <authorList>
        <person name="Marani M.M."/>
        <person name="Dourado F.S."/>
        <person name="Quelemes P.V."/>
        <person name="de Araujo A.R."/>
        <person name="Perfeito M.L."/>
        <person name="Barbosa E.A."/>
        <person name="Veras L.M."/>
        <person name="Coelho A.L."/>
        <person name="Andrade E.B."/>
        <person name="Eaton P."/>
        <person name="Longo J.P."/>
        <person name="Azevedo R.B."/>
        <person name="Delerue-Matos C."/>
        <person name="Leite J.R."/>
      </authorList>
      <dbReference type="PubMed" id="26107622"/>
      <dbReference type="DOI" id="10.1021/np500907t"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 42-66</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT VAL-66</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Has antibacterial activity against Gram-negative bacterium E.coli ATCC 25922 (MIC=300 uM) but not against S.pneumoniae ATCC 700603, S.choleraesuis ATCC 14028 or Gram-positive bacterium S.aureus ATCC 29313. Shows very little hemolytic activity and no cytotoxicity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2666.45" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Ocellatin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HK00"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016322">
    <property type="entry name" value="FSAP"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001822">
    <property type="entry name" value="Dermaseptin_precursor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000436216" evidence="4">
    <location>
      <begin position="23"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000436217" description="Ocellatin-PT5" evidence="2">
    <location>
      <begin position="42"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="2">
    <location>
      <position position="66"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="26107622"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="26107622"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="26107622"/>
    </source>
  </evidence>
  <sequence length="66" mass="7471" checksum="8C592BBED4C6DDCE" modified="2016-05-11" version="1" precursor="true">MAFLKKSLFLVLFLGLVSLSICDEEKRQDEDDDDDDDEEKRGVFDIIKDAGRQLVAHAMGKIAEKV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>