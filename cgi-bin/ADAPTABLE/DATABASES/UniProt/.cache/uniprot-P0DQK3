<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-04-22" modified="2022-12-14" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>P0DQK3</accession>
  <name>RN2A_LITST</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Ranatuerin-2SPa</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lithobates septentrionalis</name>
    <name type="common">Mink frog</name>
    <name type="synonym">Rana septentrionalis</name>
    <dbReference type="NCBI Taxonomy" id="190274"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Lithobates</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Comp. Biochem. Physiol." volume="139" first="31" last="38">
      <title>Purification and characterization of antimicrobial peptides from the skin secretions of the mink frog (Rana septentrionalis).</title>
      <authorList>
        <person name="Bevier C.R."/>
        <person name="Sonnevend A."/>
        <person name="Kolodziejek J."/>
        <person name="Nowotny N."/>
        <person name="Nielsen P.F."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="15556063"/>
      <dbReference type="DOI" id="10.1016/j.cca.2004.08.019"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>DISULFIDE BOND</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antibacterial activity against Gram-positive bacterium S.aureus. Shows no detectable hemolytic activity towards human erythrocytes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="5">Is equally expressed in juvenile and adult (male and female) frogs.</text>
  </comment>
  <comment type="mass spectrometry" mass="3689.0" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Ranatuerin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=01442"/>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DQK3"/>
  <dbReference type="SMR" id="P0DQK3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000449480" description="Ranatuerin-2SPa" evidence="2">
    <location>
      <begin position="1"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="28"/>
      <end position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P82742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15556063"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="15556063"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="15556063"/>
    </source>
  </evidence>
  <sequence length="35" mass="3693" checksum="A7A02EC0B925FF9D" modified="2020-04-22" version="1">GLFLNTVKDVAKDVAKDVAGKLLESLKCKITGCKS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>