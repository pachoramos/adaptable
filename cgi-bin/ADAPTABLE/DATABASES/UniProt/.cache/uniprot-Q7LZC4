<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-05-10" modified="2019-12-11" version="34" xmlns="http://uniprot.org/uniprot">
  <accession>Q7LZC4</accession>
  <name>CAER_PHYSA</name>
  <protein>
    <recommendedName>
      <fullName>Phyllocaerulein</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Phyllomedusa sauvagei</name>
    <name type="common">Sauvage's leaf frog</name>
    <dbReference type="NCBI Taxonomy" id="8395"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phyllomedusa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1969" name="Br. J. Pharmacol." volume="37" first="198" last="206">
      <title>Structure and pharmacological actions of phyllocaerulein, a caerulein-like nonapeptide: its occurrence in extracts of the skin of Phyllomedusa sauvagei and related Phyllomedusa species.</title>
      <authorList>
        <person name="Anastasi A."/>
        <person name="Bertaccini G."/>
        <person name="Cei J.M."/>
        <person name="De Caro G."/>
        <person name="Erspamer V."/>
        <person name="Impicciatore M."/>
      </authorList>
      <dbReference type="PubMed" id="5824931"/>
      <dbReference type="DOI" id="10.1111/j.1476-5381.1969.tb09538.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>SULFATION AT TYR-3</scope>
    <scope>AMIDATION AT PHE-9</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Hypotensive neuropeptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin dorsal glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the gastrin/cholecystokinin family.</text>
  </comment>
  <dbReference type="PIR" id="A61357">
    <property type="entry name" value="A61357"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008217">
    <property type="term" value="P:regulation of blood pressure"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013152">
    <property type="entry name" value="Gastrin/cholecystokinin_CS"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00259">
    <property type="entry name" value="GASTRIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0382">Hypotensive agent</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0765">Sulfation</keyword>
  <feature type="peptide" id="PRO_0000043887" description="Phyllocaerulein">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Sulfotyrosine" evidence="1">
    <location>
      <position position="3"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="5824931"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="9" mass="1176" checksum="6C9FA6837861BB5B" modified="2003-12-15" version="1">QEYTGWMDF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>