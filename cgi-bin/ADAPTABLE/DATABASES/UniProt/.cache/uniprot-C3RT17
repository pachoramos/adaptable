<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-10-10" modified="2023-09-13" version="29" xmlns="http://uniprot.org/uniprot">
  <accession>C3RT17</accession>
  <name>GGN1_LIMKU</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Gaegurin-LK1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Limnonectes kuhlii</name>
    <name type="common">Kuhl's Creek frog</name>
    <name type="synonym">Rana kuhlii</name>
    <dbReference type="NCBI Taxonomy" id="110107"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Dicroglossidae</taxon>
      <taxon>Dicroglossinae</taxon>
      <taxon>Limnonectes</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2013" name="Mol. Biol. Rep." volume="40" first="1097" last="1102">
      <title>Five novel antimicrobial peptides from the Kuhl's wart frog skin secretions, Limnonectes kuhlii.</title>
      <authorList>
        <person name="Wang G."/>
        <person name="Wang Y."/>
        <person name="Ma D."/>
        <person name="Liu H."/>
        <person name="Li J."/>
        <person name="Zhang K."/>
        <person name="Yang X."/>
        <person name="Lai R."/>
        <person name="Liu J."/>
      </authorList>
      <dbReference type="PubMed" id="23054029"/>
      <dbReference type="DOI" id="10.1007/s11033-012-2152-4"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 47-70</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="3">Skin</tissue>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Has antimicrobial activity against Gram-positive bacteria S.aureus ATCC 2592 (MIC=5.0 uM), S.aureus ATCC 43300 (MIC=10.0 uM) and B.subtilis (MIC=20.0 uM), against Gram-negative bacteria E.coli ML-35P (MIC=50.0 uM), P.aeruginosa PA01 (MIC=10.0 uM) and P.aeruginosa ATCC 27853 (MIC=5.0 uM) and against fungus C.albicans ATCC 2002 (MIC=10.0 uM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1 2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2565.1" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="EU346900">
    <property type="protein sequence ID" value="ACB12316.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="C3RT17"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000445399" evidence="5">
    <location>
      <begin position="23"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000445400" description="Gaegurin-LK1" evidence="2">
    <location>
      <begin position="47"/>
      <end position="70"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="23054029"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="23054029"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="23054029"/>
    </source>
  </evidence>
  <sequence length="70" mass="8035" checksum="DCE5E48C857A9912" modified="2009-06-16" version="1" precursor="true">MFTMKKSLLFLFFLGTINLSLCEEERNAEEEKRDGDDEMDVEVQKRFIGPVLKMATSILPTAICKGFKKC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>