<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-10-14" modified="2024-05-29" version="82" xmlns="http://uniprot.org/uniprot">
  <accession>Q32ZF8</accession>
  <name>DFB38_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 38</fullName>
      <shortName>BD-38</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 38</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Defb38</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Physiol. Genomics" volume="23" first="5" last="17">
      <title>Cross-species analysis of the mammalian beta-defensin gene family: presence of syntenic gene clusters and preferential expression in the male reproductive tract.</title>
      <authorList>
        <person name="Patil A.A."/>
        <person name="Cai Y."/>
        <person name="Sang Y."/>
        <person name="Blecha F."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="16033865"/>
      <dbReference type="DOI" id="10.1152/physiolgenomics.00104.2005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY621365">
    <property type="protein sequence ID" value="AAT51904.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001032641.1">
    <property type="nucleotide sequence ID" value="NM_001037552.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q32ZF8"/>
  <dbReference type="SMR" id="Q32ZF8"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000051167"/>
  <dbReference type="Ensembl" id="ENSRNOT00000050296.3">
    <property type="protein sequence ID" value="ENSRNOP00000051167.2"/>
    <property type="gene ID" value="ENSRNOG00000032089.3"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055014247">
    <property type="protein sequence ID" value="ENSRNOP00055011438"/>
    <property type="gene ID" value="ENSRNOG00055008419"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060031043">
    <property type="protein sequence ID" value="ENSRNOP00060025146"/>
    <property type="gene ID" value="ENSRNOG00060018110"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065014083">
    <property type="protein sequence ID" value="ENSRNOP00065010520"/>
    <property type="gene ID" value="ENSRNOG00065008829"/>
  </dbReference>
  <dbReference type="GeneID" id="652922"/>
  <dbReference type="KEGG" id="rno:652922"/>
  <dbReference type="UCSC" id="RGD:1561801">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:1561801"/>
  <dbReference type="CTD" id="360212"/>
  <dbReference type="RGD" id="1561801">
    <property type="gene designation" value="Defb38"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT01110000267485"/>
  <dbReference type="HOGENOM" id="CLU_189296_5_1_1"/>
  <dbReference type="InParanoid" id="Q32ZF8"/>
  <dbReference type="OMA" id="YFECPWL"/>
  <dbReference type="OrthoDB" id="4335232at2759"/>
  <dbReference type="PhylomeDB" id="Q32ZF8"/>
  <dbReference type="PRO" id="PR:Q32ZF8"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 16"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388:SF5">
    <property type="entry name" value="BETA-DEFENSIN 38"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388">
    <property type="entry name" value="BETA-DEFENSIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000352714" description="Beta-defensin 38">
    <location>
      <begin position="22"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="29"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="36"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="41"/>
      <end position="59"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="63" mass="7345" checksum="BB675B28E84507F5" modified="2005-12-06" version="1" precursor="true">MKISCFLLLVLSLYLFQVNQATDQDTAKCVQKKNVCYYFECPWLSISVSTCYKGKAKCCQKRY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>