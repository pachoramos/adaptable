<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1989-10-01" modified="2023-05-03" version="69" xmlns="http://uniprot.org/uniprot">
  <accession>P11495</accession>
  <name>K1B_METSE</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">U-metritoxin-Msn1a</fullName>
      <shortName evidence="5">U-MTTX-Msn1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6">Metridin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Metridium senile</name>
    <name type="common">Brown sea anemone</name>
    <name type="synonym">Frilled sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="6116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Nynantheae</taxon>
      <taxon>Metridiidae</taxon>
      <taxon>Metridium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1987" name="Naturwissenschaften" volume="74" first="395" last="396">
      <title>Isolation and structural determination of a hemolytic active peptide from the sea anemone Metridium senile.</title>
      <authorList>
        <person name="Krebs H.C."/>
        <person name="Habermehl G.G."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Nematoblast</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2 4">Has hemolytic activity (Ref.1). Inhibits voltage-gated potassium channels (Kv1/KCNA) (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the sea anemone type 1 potassium channel toxin family. Type 1b subfamily.</text>
  </comment>
  <dbReference type="PIR" id="A27222">
    <property type="entry name" value="A27222"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P11495"/>
  <dbReference type="SMR" id="P11495"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003582">
    <property type="entry name" value="ShKT_dom"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51670">
    <property type="entry name" value="SHKT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="peptide" id="PRO_0000044862" description="U-metritoxin-Msn1a" evidence="4">
    <location>
      <begin position="1"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="domain" description="ShKT" evidence="3">
    <location>
      <begin position="4"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="4"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="11"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="19"/>
      <end position="32"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HJC2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P29186"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01005"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="36" mass="3974" checksum="5ED9CC73509E007F" modified="1989-10-01" version="1">DSDCKDKLPACGEYRGSFCKLEKVKSNCEKTCGVKC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>