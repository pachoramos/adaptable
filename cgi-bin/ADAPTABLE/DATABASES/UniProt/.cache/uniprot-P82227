<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2023-02-22" version="61" xmlns="http://uniprot.org/uniprot">
  <accession>P82227</accession>
  <name>TK1A_HADVE</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Lambda-hexatoxin-Hv1a</fullName>
      <shortName evidence="3">Lambda-HXTX-Hv1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Janus-atracotoxin-Hv1a</fullName>
      <shortName>Janus-AcTx-Hv1a</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Kappa-atracotoxin-Hv1a</fullName>
      <shortName>Kappa-AcTx-Hv1a</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="3">Kappa-hexatoxin-Hv1a</fullName>
      <shortName evidence="3">Kappa-HXTX-Hv1a</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Hadronyche versuta</name>
    <name type="common">Blue mountains funnel-web spider</name>
    <name type="synonym">Atrax versutus</name>
    <dbReference type="NCBI Taxonomy" id="6904"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Mygalomorphae</taxon>
      <taxon>Hexathelidae</taxon>
      <taxon>Hadronyche</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Nat. Struct. Biol." volume="7" first="505" last="513">
      <title>Discovery and characterization of a family of insecticidal neurotoxins with a rare vicinal disulfide bridge.</title>
      <authorList>
        <person name="Wang X.-H."/>
        <person name="Connor M."/>
        <person name="Smith R."/>
        <person name="Maciejewski M.W."/>
        <person name="Howden M.E.H."/>
        <person name="Nicholson G.M."/>
        <person name="Christie M.J."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="10881200"/>
      <dbReference type="DOI" id="10.1038/75921"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">This excitatory toxin inhibits insect calcium-activated potassium (KCa) channels (Slo-type).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the neurotoxin 11 (kappa toxin) family.</text>
  </comment>
  <comment type="caution">
    <text evidence="3">This toxin has the prefix lambda in its name (instead of kappa), since lambda is the Greek letter attributed to calcium-activated potassium (KCa) channel impairing toxins (according to the nomenclature of King et al., 2008).</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P82227"/>
  <dbReference type="SMR" id="P82227"/>
  <dbReference type="ArachnoServer" id="AS000172">
    <property type="toxin name" value="kappa-hexatoxin-Hv1a"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012499">
    <property type="entry name" value="Toxin_16"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07945">
    <property type="entry name" value="Toxin_16"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57059">
    <property type="entry name" value="omega toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60020">
    <property type="entry name" value="J_ACTX"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1221">Calcium-activated potassium channel impairing toxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000044996" description="Lambda-hexatoxin-Hv1a">
    <location>
      <begin position="1"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="site" description="Important for the neurotoxic activity" evidence="1">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <feature type="site" description="Critical for the neurotoxic activity" evidence="1">
    <location>
      <position position="8"/>
    </location>
  </feature>
  <feature type="site" description="Critical for the neurotoxic activity" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="site" description="Critical for the neurotoxic activity" evidence="1">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <feature type="site" description="Critical for the neurotoxic activity" evidence="1">
    <location>
      <position position="14"/>
    </location>
  </feature>
  <feature type="site" description="Important for the neurotoxic activity" evidence="1">
    <location>
      <position position="30"/>
    </location>
  </feature>
  <feature type="site" description="Critical for the neurotoxic activity" evidence="1">
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="3"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="10"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="13"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="16"/>
      <end position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P82228"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="36" mass="3685" checksum="D1598B2560BFE997" modified="2000-05-30" version="1">TICTGADRPCAACCPCCPGTSCQGPESNGVVYCRNF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>