<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-11-30" modified="2023-02-22" version="43" xmlns="http://uniprot.org/uniprot">
  <accession>B6DD45</accession>
  <name>TXF12_LYCSI</name>
  <protein>
    <recommendedName>
      <fullName>U15-lycotoxin-Ls1c</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Toxin-like structure LSTX-N12</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Lycosa singoriensis</name>
    <name type="common">Wolf spider</name>
    <name type="synonym">Aranea singoriensis</name>
    <dbReference type="NCBI Taxonomy" id="434756"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Lycosoidea</taxon>
      <taxon>Lycosidae</taxon>
      <taxon>Lycosa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2010" name="Zoology" volume="113" first="10" last="18">
      <title>Transcriptome analysis of the venom glands of the Chinese wolf spider Lycosa singoriensis.</title>
      <authorList>
        <person name="Zhang Y."/>
        <person name="Chen J."/>
        <person name="Tang X."/>
        <person name="Wang F."/>
        <person name="Jiang L."/>
        <person name="Xiong X."/>
        <person name="Wang M."/>
        <person name="Rong M."/>
        <person name="Liu Z."/>
        <person name="Liang S."/>
      </authorList>
      <dbReference type="PubMed" id="19875276"/>
      <dbReference type="DOI" id="10.1016/j.zool.2009.04.001"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">Contains 5 disulfide bonds.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the venom protein 11 family. 01 (wap-1) subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="EU926129">
    <property type="protein sequence ID" value="ACI41461.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="FM864133">
    <property type="protein sequence ID" value="CAS03730.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B6DD45"/>
  <dbReference type="SMR" id="B6DD45"/>
  <dbReference type="ArachnoServer" id="AS001068">
    <property type="toxin name" value="U15-lycotoxin-Ls1c"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.75.10">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036645">
    <property type="entry name" value="Elafin-like_sf"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57256">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000401888" description="U15-lycotoxin-Ls1c">
    <location>
      <begin position="21"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="domain" description="WAP">
    <location>
      <begin position="21"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="41"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="42"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="47"/>
      <end position="62"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="86" mass="9261" checksum="1F394C4FAAE9FC30" modified="2008-11-25" version="1" precursor="true">MNSKIFAVLLLLAFLSCVLSDQYCPKSSITACKKMNIRNDCCKDDDCTGGSWCCATPCGNICKYPTDRPGGKRAAGGKSCKTGYVY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>