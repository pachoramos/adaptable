<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-04-12" modified="2022-05-25" version="5" xmlns="http://uniprot.org/uniprot">
  <accession>C0HK08</accession>
  <name>CZS10_CRUCA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Cruzioseptin-10</fullName>
      <shortName evidence="3">CZS-10</shortName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Cruziohyla calcarifer</name>
    <name type="common">Splendid leaf frog</name>
    <name type="synonym">Agalychnis calcarifer</name>
    <dbReference type="NCBI Taxonomy" id="318249"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Cruziohyla</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2016" name="J. Proteomics" volume="146" first="1" last="13">
      <title>Peptidomic approach identifies cruzioseptins, a new family of potent antimicrobial peptides in the splendid leaf frog, Cruziohyla calcarifer.</title>
      <authorList>
        <person name="Proano-Bolanos C."/>
        <person name="Zhou M."/>
        <person name="Wang L."/>
        <person name="Coloma L.A."/>
        <person name="Chen T."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="27321580"/>
      <dbReference type="DOI" id="10.1016/j.jprot.2016.06.017"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antimicrobial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2912.6" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Cruzioseptin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HK08"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000439479" description="Cruzioseptin-10" evidence="2">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A193H362"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27321580"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="27321580"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="27321580"/>
    </source>
  </evidence>
  <sequence length="28" mass="2914" checksum="A9C2171CD8C7C479" modified="2017-04-12" version="1">GFLDVLKGVGKAALGAVTHHINNLVNQQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>