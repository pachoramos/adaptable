<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-05-10" modified="2024-11-27" version="74" xmlns="http://uniprot.org/uniprot">
  <accession>P61262</accession>
  <name>DEFB1_PAPAN</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 1</fullName>
      <shortName>BD-1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB1</name>
  </gene>
  <organism>
    <name type="scientific">Papio anubis</name>
    <name type="common">Olive baboon</name>
    <dbReference type="NCBI Taxonomy" id="9555"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Papio</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Immunogenetics" volume="53" first="907" last="913">
      <title>Beta-defensin 1 gene variability among non-human primates.</title>
      <authorList>
        <person name="Del Pero M."/>
        <person name="Boniotto M."/>
        <person name="Zuccon D."/>
        <person name="Cervella P."/>
        <person name="Spano A."/>
        <person name="Amoroso A."/>
        <person name="Crovella S."/>
      </authorList>
      <dbReference type="PubMed" id="11862391"/>
      <dbReference type="DOI" id="10.1007/s00251-001-0412-x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Has bactericidal activity. May act as a ligand for C-C chemokine receptor CCR6. Positively regulates the sperm motility and bactericidal activity in a CCR6-dependent manner. Binds to CCR6 and triggers Ca2+ mobilization in the sperm which is important for its motility.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Monomer. Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Membrane</location>
    </subcellularLocation>
    <text evidence="2">Associates with tumor cell membrane-derived microvesicles.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY033761">
    <property type="protein sequence ID" value="AAK61474.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY033747">
    <property type="protein sequence ID" value="AAK61474.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_003902440.1">
    <property type="nucleotide sequence ID" value="XM_003902391.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P61262"/>
  <dbReference type="SMR" id="P61262"/>
  <dbReference type="STRING" id="9555.ENSPANP00000004965"/>
  <dbReference type="Ensembl" id="ENSPANT00000021919.3">
    <property type="protein sequence ID" value="ENSPANP00000004965.1"/>
    <property type="gene ID" value="ENSPANG00000000904.3"/>
  </dbReference>
  <dbReference type="GeneID" id="100999102"/>
  <dbReference type="KEGG" id="panu:100999102"/>
  <dbReference type="CTD" id="1672"/>
  <dbReference type="eggNOG" id="ENOG502TDMV">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000017014"/>
  <dbReference type="HOGENOM" id="CLU_189296_1_0_1"/>
  <dbReference type="OMA" id="CPLFTRI"/>
  <dbReference type="OrthoDB" id="4335232at2759"/>
  <dbReference type="Proteomes" id="UP000028761">
    <property type="component" value="Chromosome 8"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSPANG00000000904">
    <property type="expression patterns" value="Expressed in renal medulla and 49 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990742">
    <property type="term" value="C:microvesicle"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097225">
    <property type="term" value="C:sperm midpiece"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019722">
    <property type="term" value="P:calcium-mediated signaling"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060474">
    <property type="term" value="P:positive regulation of flagellated sperm motility involved in capacitation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.360.10:FF:000001">
    <property type="entry name" value="Beta-defensin 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388:SF9">
    <property type="entry name" value="BETA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388">
    <property type="entry name" value="BETA-DEFENSIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000006913" evidence="1">
    <location>
      <begin position="22"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006914" description="Beta-defensin 1">
    <location>
      <begin position="33"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="49"/>
      <end position="67"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P60022"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="68" mass="7572" checksum="997336DEC3B0435E" modified="2004-05-10" version="1" precursor="true">MRTSYLLLFTLCLLLSEMASGDNFLTGLGHRSDHYNCVRSGGQCLYSACPIYTRIQGTCYHGKAKCCK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>