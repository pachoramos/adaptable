<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-03-23" modified="2024-03-27" version="24" xmlns="http://uniprot.org/uniprot">
  <accession>P86354</accession>
  <name>VIRE2_HELVI</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Viresin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Heliothis virescens</name>
    <name type="common">Tobacco budworm moth</name>
    <dbReference type="NCBI Taxonomy" id="7102"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Noctuoidea</taxon>
      <taxon>Noctuidae</taxon>
      <taxon>Heliothinae</taxon>
      <taxon>Heliothis</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2000" name="Eur. J. Biochem." volume="267" first="677" last="683">
      <title>Viresin. A novel antibacterial protein from immune hemolymph of Heliothis virescens pupae.</title>
      <authorList>
        <person name="Chung K.T."/>
        <person name="Ourth D.D."/>
      </authorList>
      <dbReference type="PubMed" id="10651803"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.2000.01034.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue evidence="1">Hemolymph</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antibacterial activity against the Gram-negative bacteria E.coli and E.cloacae, but not against the Gram-negative bacteria P.aeruginosa, P.vulgaris, K.pneumoniae and S.enteritidis or the Gram-positive bacteria S.aureus, S.epidermidis and S.salivarius.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the insect A10/OS-D protein family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.2080.10">
    <property type="entry name" value="Insect odorant-binding protein A10/Ejaculatory bulb-specific protein 3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005055">
    <property type="entry name" value="A10/PebIII"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036682">
    <property type="entry name" value="OS_D_A10/PebIII_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03392">
    <property type="entry name" value="OS-D"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF100910">
    <property type="entry name" value="Chemosensory protein Csp2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000392511" description="Viresin">
    <location>
      <begin position="1"/>
      <end position="44" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="2">
    <location>
      <position position="44"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10651803"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="10651803"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="44" mass="5062" checksum="F923BDD6F0CCA0CA" modified="2010-03-23" version="1" fragment="single">YDNVNLDEILANDRLLVPYIKCLLDEGKKAPDAKELKEHIRXAL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>