<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-05-16" modified="2023-02-22" version="51" xmlns="http://uniprot.org/uniprot">
  <accession>P0C1F5</accession>
  <name>NA13_BUNGR</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Delta-actitoxin-Bgr2b</fullName>
      <shortName evidence="6">Delta-AITX-Bgr2b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="7">Bg III</fullName>
      <shortName>BgIII</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Neurotoxin Bg-3</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bunodosoma granuliferum</name>
    <name type="common">Red warty sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="31164"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Bunodosoma</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="J. Biol. Chem." volume="269" first="16785" last="16788">
      <title>Positively charged amino acid residues located similarly in sea anemone and scorpion toxins.</title>
      <authorList>
        <person name="Loret E.P."/>
        <person name="del Valle R.M."/>
        <person name="Mansuelle P."/>
        <person name="Sampieri F."/>
        <person name="Rochat H."/>
      </authorList>
      <dbReference type="PubMed" id="7911468"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(19)89460-7"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>TOXIC DOSE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2001" name="Br. J. Pharmacol." volume="134" first="1195" last="1206">
      <title>Characterization of two Bunodosoma granulifera toxins active on cardiac sodium channels.</title>
      <authorList>
        <person name="Goudet C."/>
        <person name="Ferrer T."/>
        <person name="Galan L."/>
        <person name="Artiles A."/>
        <person name="Batista C.V.F."/>
        <person name="Possani L.D."/>
        <person name="Alvarez J."/>
        <person name="Aneiros A."/>
        <person name="Tytgat J."/>
      </authorList>
      <dbReference type="PubMed" id="11704639"/>
      <dbReference type="DOI" id="10.1038/sj.bjp.0704361"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="FEBS Lett." volume="532" first="131" last="134">
      <title>The sea anemone Bunodosoma granulifera contains surprisingly efficacious and potent insect-selective toxins.</title>
      <authorList>
        <person name="Bosmans F."/>
        <person name="Aneiros A."/>
        <person name="Tytgat J."/>
      </authorList>
      <dbReference type="PubMed" id="12459477"/>
      <dbReference type="DOI" id="10.1016/s0014-5793(02)03653-0"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2012" name="Peptides" volume="34" first="26" last="38">
      <title>Peptide fingerprinting of the neurotoxic fractions isolated from the secretions of sea anemones Stichodactyla helianthus and Bunodosoma granulifera. New members of the APETx-like family identified by a 454 pyrosequencing approach.</title>
      <authorList>
        <person name="Rodriguez A.A."/>
        <person name="Cassoli J.S."/>
        <person name="Sa F."/>
        <person name="Dong Z.Q."/>
        <person name="de Freitas J.C."/>
        <person name="Pimenta A.M."/>
        <person name="de Lima M.E."/>
        <person name="Konno K."/>
        <person name="Lee S.M."/>
        <person name="Garateix A."/>
        <person name="Zaharenko A.J."/>
      </authorList>
      <dbReference type="PubMed" id="22015268"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2011.10.011"/>
    </citation>
    <scope>MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 5">Binds voltage-dependently at site 3 of sodium channels (Nav) and inhibits the inactivation of the activated channels, thereby blocking neuronal transmission. Has effect on SCN4A/SCN1B, and SCN5A/SCN1B, has no effect on SCN2A/SCN1B, and SCN10A/SCN1B. Possesses the highest efficacy for the insect sodium channel para/tipE. Also interacts with sodium channels in cardiac cells. Shows lethality to crabs (PubMed:22015268).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="8">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="5073.1" method="Electrospray" evidence="2"/>
  <comment type="mass spectrometry" mass="5072.9" method="MALDI" evidence="4"/>
  <comment type="toxic dose">
    <text evidence="5">LD(50) is 21 ug/kg by intracerebroventricular injection into mice.</text>
  </comment>
  <comment type="similarity">
    <text evidence="8">Belongs to the sea anemone sodium channel inhibitory toxin family. Type I subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0C1F5"/>
  <dbReference type="SMR" id="P0C1F5"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009966">
    <property type="term" value="P:regulation of signal transduction"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000693">
    <property type="entry name" value="Anenome_toxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00706">
    <property type="entry name" value="Toxin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001905">
    <property type="entry name" value="Anenome_toxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0123">Cardiotoxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000236031" description="Delta-actitoxin-Bgr2b" evidence="2 5">
    <location>
      <begin position="1"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="6"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="28"/>
      <end position="46"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01530"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11704639"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12459477"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="22015268"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="7911468"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="7911468"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <sequence length="48" mass="5079" checksum="8ACCB00C829A50FA" modified="2006-05-16" version="1">GASCRCDSDGPTSRGDTLTGTLWLIGRCPSGWHNCRGSGPFIGYCCKQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>