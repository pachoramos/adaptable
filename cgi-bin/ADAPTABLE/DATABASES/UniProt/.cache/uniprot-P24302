<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1992-03-01" modified="2024-05-29" version="62" xmlns="http://uniprot.org/uniprot">
  <accession>P24302</accession>
  <accession>P80277</accession>
  <accession>Q7T3K6</accession>
  <name>DRS1_PHYSA</name>
  <protein>
    <recommendedName>
      <fullName evidence="12">Dermaseptin-S1</fullName>
      <shortName evidence="12 16">DRS-S1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="10 13 14 16">Dermaseptin</fullName>
      <shortName evidence="14">DS</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="15">Dermaseptin I</fullName>
      <shortName evidence="15">DS I</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="11">Dermaseptin-1</fullName>
      <shortName evidence="11">DS1</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Phyllomedusa sauvagei</name>
    <name type="common">Sauvage's leaf frog</name>
    <dbReference type="NCBI Taxonomy" id="8395"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phyllomedusa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Regul. Pept." volume="116" first="139" last="146">
      <title>Identification of three novel Phyllomedusa sauvagei dermaseptins (sVI-sVIII) by cloning from a skin secretion-derived cDNA library.</title>
      <authorList>
        <person name="Chen T."/>
        <person name="Tang L."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="14599725"/>
      <dbReference type="DOI" id="10.1016/j.regpep.2003.08.001"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1991" name="Biochemistry" volume="30" first="8824" last="8830">
      <title>Isolation, amino acid sequence, and synthesis of dermaseptin, a novel antimicrobial peptide of amphibian skin.</title>
      <authorList>
        <person name="Mor A."/>
        <person name="Nguyen V.H."/>
        <person name="Delfour A."/>
        <person name="Migliore-Samour D."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="1909573"/>
      <dbReference type="DOI" id="10.1021/bi00100a014"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 46-79</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1994" name="Eur. J. Biochem." volume="219" first="145" last="154">
      <title>Isolation and structure of novel defensive peptides from frog skin.</title>
      <authorList>
        <person name="Mor A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="8306981"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1994.tb19924.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 46-79</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1994" name="J. Biol. Chem." volume="269" first="1934" last="1939">
      <title>The NH2-terminal alpha-helical domain 1-18 of dermaseptin is responsible for antimicrobial activity.</title>
      <authorList>
        <person name="Mor A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="8294443"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)42116-8"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>MUTAGENESIS OF 46-ALA--ALA-71 AND 64-LYS--GLN-79</scope>
    <scope>SYNTHESIS OF 46-79</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1998" name="Biochem. Biophys. Res. Commun." volume="247" first="870" last="875">
      <title>Dermaseptin, a peptide antibiotic, stimulates microbicidal activities of polymorphonuclear leukocytes.</title>
      <authorList>
        <person name="Ammar B."/>
        <person name="Perianin A."/>
        <person name="Mor A."/>
        <person name="Sarfati G."/>
        <person name="Tissot M."/>
        <person name="Nicolas P."/>
        <person name="Giroud J.P."/>
        <person name="Roch-Arveiller M."/>
      </authorList>
      <dbReference type="PubMed" id="9647785"/>
      <dbReference type="DOI" id="10.1006/bbrc.1998.8879"/>
    </citation>
    <scope>FUNCTION ON POLYMORPHONUCLEAR LEUKOCYTES</scope>
    <scope>MUTAGENESIS OF 46-ALA--HIS-61 AND 56-GLY--GLN-79</scope>
    <scope>SYNTHESIS OF 46-79</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2004" name="Virology" volume="323" first="268" last="275">
      <title>Inactivation of viruses infecting ectothermic animals by amphibian and piscine antimicrobial peptides.</title>
      <authorList>
        <person name="Chinchar V.G."/>
        <person name="Bryan L."/>
        <person name="Silphadaung U."/>
        <person name="Noga E."/>
        <person name="Wade D."/>
        <person name="Rollins-Smith L."/>
      </authorList>
      <dbReference type="PubMed" id="15193922"/>
      <dbReference type="DOI" id="10.1016/j.virol.2004.02.029"/>
    </citation>
    <scope>FUNCTION AS ANTIVIRAL PEPTIDE</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2005" name="Contraception" volume="72" first="447" last="453">
      <title>Spermicidal activity of dermaseptins.</title>
      <authorList>
        <person name="Zairi A."/>
        <person name="Belaid A."/>
        <person name="Gahbiche A."/>
        <person name="Hani K."/>
      </authorList>
      <dbReference type="PubMed" id="16307969"/>
      <dbReference type="DOI" id="10.1016/j.contraception.2005.06.055"/>
    </citation>
    <scope>FUNCTION AS SPERMICIDE</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2005" name="J. Virol." volume="79" first="11598" last="11606">
      <title>Antimicrobial peptides from amphibian skin potently inhibit human immunodeficiency virus infection and transfer of virus from dendritic cells to T cells.</title>
      <authorList>
        <person name="VanCompernolle S.E."/>
        <person name="Taylor R.J."/>
        <person name="Oswald-Richter K."/>
        <person name="Jiang J."/>
        <person name="Youree B.E."/>
        <person name="Bowie J.H."/>
        <person name="Tyler M.J."/>
        <person name="Conlon J.M."/>
        <person name="Wade D."/>
        <person name="Aiken C."/>
        <person name="Dermody T.S."/>
        <person name="KewalRamani V.N."/>
        <person name="Rollins-Smith L.A."/>
        <person name="Unutmaz D."/>
      </authorList>
      <dbReference type="PubMed" id="16140737"/>
      <dbReference type="DOI" id="10.1128/jvi.79.18.11598-11606.2005"/>
    </citation>
    <scope>FUNCTION ON HIV</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2011" name="Peptides" volume="32" first="683" last="690">
      <title>Leishmanicidal activity of synthetic antimicrobial peptides in an infection model with human dendritic cells.</title>
      <authorList>
        <person name="Perez-Cordero J.J."/>
        <person name="Lozano J.M."/>
        <person name="Cortes J."/>
        <person name="Delgado G."/>
      </authorList>
      <dbReference type="PubMed" id="21262294"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2011.01.011"/>
    </citation>
    <scope>FUNCTION ON LEISHMANIA</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2008" name="Peptides" volume="29" first="2074" last="2082">
      <title>A consistent nomenclature of antimicrobial peptides isolated from frogs of the subfamily Phyllomedusinae.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="18644413"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.06.017"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 4 5 6 7 8 9">Antimicrobial activity with potent activity against Gram-positive and Gram-negative bacteria, fungi and protozoa (PubMed:1909573, PubMed:8294443, PubMed:8306981). Also stimulates the microbicidal activity of polymorphonuclear leukocytes (PubMed:9647785). Probably acts by disturbing membrane functions with its amphipathic structure (PubMed:8306981). Does not show cytotoxicity on lymphocytes or KB cells (PubMed:8294443). Does not show hemolytic activity (PubMed:1909573, PubMed:21262294, PubMed:8294443). In vitro, shows spermicidal, antiviral, and anti-leishmanial activities (PubMed:16140737, PubMed:16307969, PubMed:21262294).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5 8">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="18 19">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3455.4" method="FAB" evidence="5"/>
  <comment type="similarity">
    <text evidence="17">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=0157"/>
  </comment>
  <dbReference type="EMBL" id="AJ564794">
    <property type="protein sequence ID" value="CAD92232.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A40298">
    <property type="entry name" value="A40298"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P24302"/>
  <dbReference type="TCDB" id="1.C.52.1.37">
    <property type="family name" value="the dermaseptin (dermaseptin) family"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050688">
    <property type="term" value="P:regulation of defense response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022731">
    <property type="entry name" value="Dermaseptin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016322">
    <property type="entry name" value="FSAP"/>
  </dbReference>
  <dbReference type="Pfam" id="PF12121">
    <property type="entry name" value="DD_K"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001822">
    <property type="entry name" value="Dermaseptin_precursor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0930">Antiviral protein</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000449895" evidence="5 8">
    <location>
      <begin position="23"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000044732" description="Dermaseptin-S1" evidence="5 8">
    <location>
      <begin position="46"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="DS(20-34); Loss of antimicrial activity (only tested against fungi)." evidence="7">
    <location>
      <begin position="46"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of stimulation of polymorphonuclear leukocytes." evidence="9">
    <location>
      <begin position="46"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of stimulation of polymorphonuclear leukocytes." evidence="9">
    <location>
      <begin position="56"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="DS(1-18); Increase in antimicrial activity (tested against bacteria, and fungi). No change in activity against erythrocytes." evidence="7">
    <location>
      <begin position="64"/>
      <end position="79"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15193922"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="16140737"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="16307969"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="1909573"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="21262294"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="8294443"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="8306981"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="9647785"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="10">
    <source>
      <dbReference type="PubMed" id="16140737"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="11">
    <source>
      <dbReference type="PubMed" id="16307969"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="13">
    <source>
      <dbReference type="PubMed" id="21262294"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="14">
    <source>
      <dbReference type="PubMed" id="8294443"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="15">
    <source>
      <dbReference type="PubMed" id="8306981"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="16">
    <source>
      <dbReference type="PubMed" id="9647785"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="17"/>
  <evidence type="ECO:0000305" key="18">
    <source>
      <dbReference type="PubMed" id="1909573"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="19">
    <source>
      <dbReference type="PubMed" id="8306981"/>
    </source>
  </evidence>
  <sequence length="79" mass="8812" checksum="F8B1966A145E8114" modified="2020-06-17" version="3" precursor="true">MDILKKSLFLVLFLGLVSLSICEEEKRENEDEEKQEDDEQSEMKRALWKTMLKKLGTMALHAGKAALGAAADTISQGTQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>