<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-01" modified="2022-05-25" version="23" xmlns="http://uniprot.org/uniprot">
  <accession>P69028</accession>
  <accession>P82401</accession>
  <name>AUR51_RANRN</name>
  <protein>
    <recommendedName>
      <fullName>Aurein-5.1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ranoidea raniformis</name>
    <name type="common">Southern bell frog</name>
    <name type="synonym">Litoria raniformis</name>
    <dbReference type="NCBI Taxonomy" id="116057"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Eur. J. Biochem." volume="267" first="5330" last="5341">
      <title>The antibiotic and anticancer active aurein peptides from the australian bell frogs Litoria aurea and Litoria raniformis the solution structure of aurein 1.2.</title>
      <authorList>
        <person name="Rozek T."/>
        <person name="Wegener K.L."/>
        <person name="Bowie J.H."/>
        <person name="Olver I.N."/>
        <person name="Carver J.A."/>
        <person name="Wallace J.C."/>
        <person name="Tyler M.J."/>
      </authorList>
      <dbReference type="PubMed" id="10951191"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.2000.01536.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has no antimicrobial or anticancer activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin dorsal glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Aurein subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P69028"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR032021">
    <property type="entry name" value="Frog_Litoria"/>
  </dbReference>
  <dbReference type="Pfam" id="PF16049">
    <property type="entry name" value="Antimicrobial24"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043732" description="Aurein-5.1">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10951191"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="25" mass="2547" checksum="15C6169CD98AFC27" modified="2005-02-01" version="1">GLLDIVTGLLGNLIVDVLKPKTPAS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>