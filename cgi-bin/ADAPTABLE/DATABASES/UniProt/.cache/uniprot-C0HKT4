<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-06-20" modified="2022-05-25" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKT4</accession>
  <name>DIH41_AGRIP</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Diuretic hormone 41</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="5">Diuretic hormone 41 precursor-related peptide</fullName>
        <shortName evidence="5">DH(41)-PP</shortName>
        <shortName evidence="5">DH-41-PP</shortName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Agrotis ipsilon</name>
    <name type="common">Black cutworm moth</name>
    <dbReference type="NCBI Taxonomy" id="56364"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Noctuoidea</taxon>
      <taxon>Noctuidae</taxon>
      <taxon>Noctuinae</taxon>
      <taxon>Noctuini</taxon>
      <taxon>Agrotis</taxon>
    </lineage>
  </organism>
  <reference evidence="6" key="1">
    <citation type="journal article" date="2018" name="J. Proteome Res." volume="17" first="1397" last="1414">
      <title>Mating-induced differential peptidomics of neuropeptides and protein hormones in Agrotis ipsilon moths.</title>
      <authorList>
        <person name="Diesner M."/>
        <person name="Gallot A."/>
        <person name="Binz H."/>
        <person name="Gaertner C."/>
        <person name="Vitecek S."/>
        <person name="Kahnt J."/>
        <person name="Schachtner J."/>
        <person name="Jacquin-Joly E."/>
        <person name="Gadenne C."/>
      </authorList>
      <dbReference type="PubMed" id="29466015"/>
      <dbReference type="DOI" id="10.1021/acs.jproteome.7b00779"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 126-139</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <molecule>Diuretic hormone 41</molecule>
    <text evidence="2">Regulation of fluid secretion.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <molecule>Diuretic hormone 41 precursor-related peptide</molecule>
    <text evidence="4">Expressed in corpora cardiaca (CC), corpora allata (CA), antennal lobe (AL) and gnathal ganglion (GNG) (at protein level). Expression in CC and CA detected in all animals, in GNG in most animals, expression in AL detected in few animals (at protein level).</text>
  </comment>
  <comment type="mass spectrometry" mass="1651.88" method="MALDI" evidence="4">
    <molecule>Diuretic hormone 41 precursor-related peptide</molecule>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the sauvagine/corticotropin-releasing factor/urotensin I family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HKT4"/>
  <dbReference type="SMR" id="C0HKT4"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018446">
    <property type="entry name" value="Corticotropin-releasing_fac_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000187">
    <property type="entry name" value="CRF"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00473">
    <property type="entry name" value="CRF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00039">
    <property type="entry name" value="CRF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00511">
    <property type="entry name" value="CRF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444526" evidence="6">
    <location>
      <begin position="21"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000444527" description="Diuretic hormone 41" evidence="2">
    <location>
      <begin position="82"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000444528" description="Diuretic hormone 41 precursor-related peptide" evidence="4">
    <location>
      <begin position="126"/>
      <end position="139"/>
    </location>
  </feature>
  <feature type="modified residue" description="Isoleucine amide" evidence="1">
    <location>
      <position position="122"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P21819"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P82014"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="139" mass="15427" checksum="C0FA0F83A974D151" modified="2018-06-20" version="1" precursor="true">MMWWALWCAVVVAAGSGVAAAPAPDSLSPLDMVQMDSSAPDDETLYAMSPMAARYSAGAPWLYLLADMPRDSQTGSGRVKRRMPSLSIDQPMSVLRQKLSQEMERKQQAFRAAVNRNFLNDIGKRGFQWTPSVQAVRYI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>