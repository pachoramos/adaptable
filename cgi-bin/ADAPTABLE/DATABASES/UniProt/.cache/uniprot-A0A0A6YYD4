<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-05-23" modified="2024-10-02" version="52" xmlns="http://uniprot.org/uniprot">
  <accession>A0A0A6YYD4</accession>
  <name>TVB13_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="8">T cell receptor beta variable 13</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="8" type="primary">TRBV13</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Nature" volume="424" first="157" last="164">
      <title>The DNA sequence of human chromosome 7.</title>
      <authorList>
        <person name="Hillier L.W."/>
        <person name="Fulton R.S."/>
        <person name="Fulton L.A."/>
        <person name="Graves T.A."/>
        <person name="Pepin K.H."/>
        <person name="Wagner-McPherson C."/>
        <person name="Layman D."/>
        <person name="Maas J."/>
        <person name="Jaeger S."/>
        <person name="Walker R."/>
        <person name="Wylie K."/>
        <person name="Sekhon M."/>
        <person name="Becker M.C."/>
        <person name="O'Laughlin M.D."/>
        <person name="Schaller M.E."/>
        <person name="Fewell G.A."/>
        <person name="Delehaunty K.D."/>
        <person name="Miner T.L."/>
        <person name="Nash W.E."/>
        <person name="Cordes M."/>
        <person name="Du H."/>
        <person name="Sun H."/>
        <person name="Edwards J."/>
        <person name="Bradshaw-Cordum H."/>
        <person name="Ali J."/>
        <person name="Andrews S."/>
        <person name="Isak A."/>
        <person name="Vanbrunt A."/>
        <person name="Nguyen C."/>
        <person name="Du F."/>
        <person name="Lamar B."/>
        <person name="Courtney L."/>
        <person name="Kalicki J."/>
        <person name="Ozersky P."/>
        <person name="Bielicki L."/>
        <person name="Scott K."/>
        <person name="Holmes A."/>
        <person name="Harkins R."/>
        <person name="Harris A."/>
        <person name="Strong C.M."/>
        <person name="Hou S."/>
        <person name="Tomlinson C."/>
        <person name="Dauphin-Kohlberg S."/>
        <person name="Kozlowicz-Reilly A."/>
        <person name="Leonard S."/>
        <person name="Rohlfing T."/>
        <person name="Rock S.M."/>
        <person name="Tin-Wollam A.-M."/>
        <person name="Abbott A."/>
        <person name="Minx P."/>
        <person name="Maupin R."/>
        <person name="Strowmatt C."/>
        <person name="Latreille P."/>
        <person name="Miller N."/>
        <person name="Johnson D."/>
        <person name="Murray J."/>
        <person name="Woessner J.P."/>
        <person name="Wendl M.C."/>
        <person name="Yang S.-P."/>
        <person name="Schultz B.R."/>
        <person name="Wallis J.W."/>
        <person name="Spieth J."/>
        <person name="Bieri T.A."/>
        <person name="Nelson J.O."/>
        <person name="Berkowicz N."/>
        <person name="Wohldmann P.E."/>
        <person name="Cook L.L."/>
        <person name="Hickenbotham M.T."/>
        <person name="Eldred J."/>
        <person name="Williams D."/>
        <person name="Bedell J.A."/>
        <person name="Mardis E.R."/>
        <person name="Clifton S.W."/>
        <person name="Chissoe S.L."/>
        <person name="Marra M.A."/>
        <person name="Raymond C."/>
        <person name="Haugen E."/>
        <person name="Gillett W."/>
        <person name="Zhou Y."/>
        <person name="James R."/>
        <person name="Phelps K."/>
        <person name="Iadanoto S."/>
        <person name="Bubb K."/>
        <person name="Simms E."/>
        <person name="Levy R."/>
        <person name="Clendenning J."/>
        <person name="Kaul R."/>
        <person name="Kent W.J."/>
        <person name="Furey T.S."/>
        <person name="Baertsch R.A."/>
        <person name="Brent M.R."/>
        <person name="Keibler E."/>
        <person name="Flicek P."/>
        <person name="Bork P."/>
        <person name="Suyama M."/>
        <person name="Bailey J.A."/>
        <person name="Portnoy M.E."/>
        <person name="Torrents D."/>
        <person name="Chinwalla A.T."/>
        <person name="Gish W.R."/>
        <person name="Eddy S.R."/>
        <person name="McPherson J.D."/>
        <person name="Olson M.V."/>
        <person name="Eichler E.E."/>
        <person name="Green E.D."/>
        <person name="Waterston R.H."/>
        <person name="Wilson R.K."/>
      </authorList>
      <dbReference type="PubMed" id="12853948"/>
      <dbReference type="DOI" id="10.1038/nature01782"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA] (IMGT ALLELE TRBV13*01)</scope>
  </reference>
  <reference key="2">
    <citation type="book" date="2001" name="The T Cell Receptor FactsBook." first="1" last="397" publisher="Academic Press" city="London.">
      <title>The T Cell Receptor FactsBook.</title>
      <editorList>
        <person name="Lefranc M.P."/>
        <person name="Lefranc G."/>
      </editorList>
      <authorList>
        <person name="Lefranc M.P."/>
        <person name="Lefranc G."/>
      </authorList>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Nat. Rev. Immunol." volume="4" first="123" last="132">
      <title>The many important facets of T-cell repertoire diversity.</title>
      <authorList>
        <person name="Nikolich-Zugich J."/>
        <person name="Slifka M.K."/>
        <person name="Messaoudi I."/>
      </authorList>
      <dbReference type="PubMed" id="15040585"/>
      <dbReference type="DOI" id="10.1038/nri1292"/>
    </citation>
    <scope>REVIEW ON T CELL REPERTOIRE DIVERSITY</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2010" name="Cold Spring Harb. Perspect. Biol." volume="2" first="A005140" last="A005140">
      <title>Structural biology of the T-cell receptor: insights into receptor assembly, ligand recognition, and initiation of signaling.</title>
      <authorList>
        <person name="Wucherpfennig K.W."/>
        <person name="Gagnon E."/>
        <person name="Call M.J."/>
        <person name="Huseby E.S."/>
        <person name="Call M.E."/>
      </authorList>
      <dbReference type="PubMed" id="20452950"/>
      <dbReference type="DOI" id="10.1101/cshperspect.a005140"/>
    </citation>
    <scope>REVIEW ON T CELL RECEPTOR-CD3 COMPLEX ASSEMBLY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2013" name="Nat. Rev. Immunol." volume="13" first="257" last="269">
      <title>T cell receptor signalling networks: branched, diversified and bounded.</title>
      <authorList>
        <person name="Brownlie R.J."/>
        <person name="Zamoyska R."/>
      </authorList>
      <dbReference type="PubMed" id="23524462"/>
      <dbReference type="DOI" id="10.1038/nri3403"/>
    </citation>
    <scope>REVIEW ON T CELL RECEPTOR SIGNALING</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2014" name="Front. Immunol." volume="5" first="22" last="22">
      <title>Immunoglobulin and T Cell Receptor Genes: IMGT((R)) and the Birth and Rise of Immunoinformatics.</title>
      <authorList>
        <person name="Lefranc M.P."/>
      </authorList>
      <dbReference type="PubMed" id="24600447"/>
      <dbReference type="DOI" id="10.3389/fimmu.2014.00022"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2015" name="Annu. Rev. Immunol." volume="33" first="169" last="200">
      <title>T cell antigen receptor recognition of antigen-presenting molecules.</title>
      <authorList>
        <person name="Rossjohn J."/>
        <person name="Gras S."/>
        <person name="Miles J.J."/>
        <person name="Turner S.J."/>
        <person name="Godfrey D.I."/>
        <person name="McCluskey J."/>
      </authorList>
      <dbReference type="PubMed" id="25493333"/>
      <dbReference type="DOI" id="10.1146/annurev-immunol-032414-112334"/>
    </citation>
    <scope>REVIEW ON FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="3 5 6 7">V region of the variable domain of T cell receptor (TR) beta chain that participates in the antigen recognition (PubMed:24600447). Alpha-beta T cell receptors are antigen specific receptors which are essential to the immune response and are present on the cell surface of T lymphocytes. Recognize peptide-major histocompatibility (MH) (pMH) complexes that are displayed by antigen presenting cells (APC), a prerequisite for efficient T cell adaptive immunity against pathogens (PubMed:25493333). Binding of alpha-beta TR to pMH complex initiates TR-CD3 clustering on the cell surface and intracellular activation of LCK that phosphorylates the ITAM motifs of CD3G, CD3D, CD3E and CD247 enabling the recruitment of ZAP70. In turn ZAP70 phosphorylates LAT, which recruits numerous signaling molecules to form the LAT signalosome. The LAT signalosome propagates signal branching to three major signaling pathways, the calcium, the mitogen-activated protein kinase (MAPK) kinase and the nuclear factor NF-kappa-B (NF-kB) pathways, leading to the mobilization of transcription factors that are critical for gene expression and essential for T cell growth and differentiation (PubMed:23524462). The T cell repertoire is generated in the thymus, by V-(D)-J rearrangement. This repertoire is then shaped by intrathymic selection events to generate a peripheral T cell pool of self-MH restricted, non-autoaggressive T cells. Post-thymic interaction of alpha-beta TR with the pMH complexes shapes TR structural and functional avidity (PubMed:15040585).</text>
  </comment>
  <comment type="subunit">
    <text evidence="4">Alpha-beta TR is a heterodimer composed of an alpha and beta chain; disulfide-linked. The alpha-beta TR is associated with the transmembrane signaling CD3 coreceptor proteins to form the TR-CD3 (TcR or TCR). The assembly of alpha-beta TR heterodimers with CD3 occurs in the endoplasmic reticulum where a single alpha-beta TR heterodimer associates with one CD3D-CD3E heterodimer, one CD3G-CD3E heterodimer and one CD247 homodimer forming a stable octameric structure. CD3D-CD3E and CD3G-CD3E heterodimers preferentially associate with TR alpha and TR beta chains, respectively. The association of the CD247 homodimer is the last step of TcR assembly in the endoplasmic reticulum and is required for transport to the cell surface.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="polymorphism">
    <text evidence="9">There are several alleles. The sequence shown is that of IMGT allele TRBV13*01.</text>
  </comment>
  <dbReference type="EMBL" id="AC244196">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A0A6YYD4"/>
  <dbReference type="SMR" id="A0A0A6YYD4"/>
  <dbReference type="IMGT_GENE-DB" id="TRBV13"/>
  <dbReference type="GlyCosmos" id="A0A0A6YYD4">
    <property type="glycosylation" value="1 site, No reported glycans"/>
  </dbReference>
  <dbReference type="GlyGen" id="A0A0A6YYD4">
    <property type="glycosylation" value="1 site"/>
  </dbReference>
  <dbReference type="BioMuta" id="ENSG00000276405"/>
  <dbReference type="Ensembl" id="ENST00000614171.1">
    <property type="protein sequence ID" value="ENSP00000477580.1"/>
    <property type="gene ID" value="ENSG00000276405.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000633796.1">
    <property type="protein sequence ID" value="ENSP00000488778.1"/>
    <property type="gene ID" value="ENSG00000282407.1"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:12188"/>
  <dbReference type="GeneCards" id="TRBV13"/>
  <dbReference type="HGNC" id="HGNC:12188">
    <property type="gene designation" value="TRBV13"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000276405">
    <property type="expression patterns" value="Tissue enriched (lymphoid)"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_A0A0A6YYD4"/>
  <dbReference type="OpenTargets" id="ENSG00000276405"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000276405"/>
  <dbReference type="GeneTree" id="ENSGT00940000154270"/>
  <dbReference type="HOGENOM" id="CLU_077975_9_4_1"/>
  <dbReference type="InParanoid" id="A0A0A6YYD4"/>
  <dbReference type="OMA" id="WNTRLLC"/>
  <dbReference type="ChiTaRS" id="TRBV13">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="Pharos" id="A0A0A6YYD4">
    <property type="development level" value="Tdark"/>
  </dbReference>
  <dbReference type="PRO" id="PR:A0A0A6YYD4"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 7"/>
  </dbReference>
  <dbReference type="RNAct" id="A0A0A6YYD4">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000276405">
    <property type="expression patterns" value="Expressed in lymph node and 75 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042101">
    <property type="term" value="C:T cell receptor complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002250">
    <property type="term" value="P:adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007166">
    <property type="term" value="P:cell surface receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013106">
    <property type="entry name" value="Ig_V-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050413">
    <property type="entry name" value="TCR_beta_variable"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23268:SF75">
    <property type="entry name" value="T CELL RECEPTOR BETA VARIABLE 13"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23268">
    <property type="entry name" value="T-CELL RECEPTOR BETA CHAIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07686">
    <property type="entry name" value="V-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00406">
    <property type="entry name" value="IGv"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0393">Immunoglobulin domain</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0675">Receptor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1279">T cell receptor</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5014017130" description="T cell receptor beta variable 13" evidence="1">
    <location>
      <begin position="32"/>
      <end position="124"/>
    </location>
  </feature>
  <feature type="domain" description="Ig-like" evidence="2">
    <location>
      <begin position="32"/>
      <end position="124" status="greater than"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="1">
    <location>
      <position position="106"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="52"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="124"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00114"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="15040585"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="20452950"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="23524462"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="24600447"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="25493333"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000305" key="9"/>
  <sequence length="124" mass="13967" checksum="4BC64C51E96B2040" modified="2016-10-05" version="5" precursor="true">MLSPDLPDSAWNTRLLCRVMLCLLGAGSVAAGVIQSPRHLIKEKRETATLKCYPIPRHDTVYWYQQGPGQDPQFLISFYEKMQSDKGSIPDRFSAQQFSDYHSELNMSSLELGDSALYFCASSL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>