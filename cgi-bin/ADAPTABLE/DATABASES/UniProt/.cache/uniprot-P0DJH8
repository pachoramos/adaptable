<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-05-16" modified="2024-11-27" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>P0DJH8</accession>
  <name>SCX1_BUTMA</name>
  <protein>
    <recommendedName>
      <fullName>Alpha-toxin Bu1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Buthacus macrocentrus</name>
    <name type="common">Turkish scorpion</name>
    <dbReference type="NCBI Taxonomy" id="1143368"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Buthacus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2012" name="Toxicon" volume="59" first="408" last="415">
      <title>Turkish scorpion Buthacus macrocentrus: general characterization of the venom and description of Bu1, a potent mammalian Na-channel alpha-toxin.</title>
      <authorList>
        <person name="Caliskan F."/>
        <person name="Quintero-Hernandez V."/>
        <person name="Restano-Cassulini R."/>
        <person name="Batista C.V."/>
        <person name="Zamudio F.Z."/>
        <person name="Coronas F.I."/>
        <person name="Possani L.D."/>
      </authorList>
      <dbReference type="PubMed" id="22245624"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2011.12.013"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 1-65</scope>
    <scope>FUNCTION</scope>
    <scope>TOXIC DOSE</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT ARG-65</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Alpha toxins bind voltage-independently at site-3 of sodium channels (Nav) and inhibit the inactivation of the activated channels, thereby blocking neuronal transmission. Since the experiments have been done on F11 cells (immortalized cell line derived from rat DRG neurons mainly expressing Nav1.3/SCN3A, but also Nav1.7/SCN9A and Nav1.2/SCN2A), it is supposed to act on these channels. The slow of inactivation process is partially reversible. Is lethal to mice.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">10 ug of this toxin kills a mouse of 20g body weight within 20 minutes after intraperitoneal injection.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Alpha subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="JQ038136">
    <property type="protein sequence ID" value="AFG28506.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0DJH8"/>
  <dbReference type="SMR" id="P0DJH8"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00284">
    <property type="entry name" value="TOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000417436" description="Alpha-toxin Bu1">
    <location>
      <begin position="1"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="2">
    <location>
      <begin position="3"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="modified residue" description="Arginine amide" evidence="3">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="13"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="17"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="23"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="27"/>
      <end position="49"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P0DQN8"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22245624"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="22245624"/>
    </source>
  </evidence>
  <sequence length="67" mass="7485" checksum="B2FBC35EFCF8C70B" modified="2012-05-16" version="1">GVRDAYIADDKNCVYTCAKNSYCNTECTKNGAESGYCQWLGKYGNGCWCIKLPDKVPIRIPGRCRGR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>