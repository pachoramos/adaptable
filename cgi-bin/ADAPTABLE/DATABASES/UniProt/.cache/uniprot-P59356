<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-02-28" modified="2024-10-02" version="71" xmlns="http://uniprot.org/uniprot">
  <accession>P59356</accession>
  <name>SCL6_LEIHE</name>
  <protein>
    <recommendedName>
      <fullName>Alpha-like toxin Lqh6</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Lqh VI</fullName>
      <shortName>LqhVI</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Leiurus hebraeus</name>
    <name type="common">Hebrew deathstalker scorpion</name>
    <name type="synonym">Leiurus quinquestriatus hebraeus</name>
    <dbReference type="NCBI Taxonomy" id="2899558"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Leiurus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Eur. J. Biochem." volume="269" first="3920" last="3933">
      <title>Characterization of scorpion alpha-like toxin group using two new toxins from the scorpion Leiurus quinquestriatus hebraeus.</title>
      <authorList>
        <person name="Hamon A."/>
        <person name="Gilles N."/>
        <person name="Sautiere P."/>
        <person name="Martinage A."/>
        <person name="Kopeyan C."/>
        <person name="Ulens C."/>
        <person name="Tytgat J."/>
        <person name="Lancelin J.-M."/>
        <person name="Gordon D."/>
      </authorList>
      <dbReference type="PubMed" id="12180969"/>
      <dbReference type="DOI" id="10.1046/j.1432-1033.2002.03065.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PHARMACOLOGICAL CHARACTERIZATION</scope>
    <scope>AMIDATION AT LYS-64</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Alpha toxins bind voltage-independently at site-3 of sodium channels (Nav) and inhibit the inactivation of the activated channels, thereby blocking neuronal transmission. This toxin is highly toxic to insects and mice, and inhibits the binding of alpha-toxin to cockroach neuronal membranes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="3">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="2">LD(50) is 34.3 nmol/kg to cockroaches (Blattella germanica).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Alpha subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P59356"/>
  <dbReference type="SMR" id="P59356"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000066786" description="Alpha-like toxin Lqh6">
    <location>
      <begin position="1"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="2"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="modified residue" description="Lysine amide" evidence="2">
    <location>
      <position position="64"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="12"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="16"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="20"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="46"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12180969"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="64" mass="6803" checksum="49A54B0B6C75392E" modified="2003-02-28" version="1">VRDGYIAQPENCVYHCIPDCDTLCKDNGGTGGHCGFKLGHGIACWCNALPDNVGIIVDGVKCHK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>