<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-02-11" modified="2024-11-27" version="129" xmlns="http://uniprot.org/uniprot">
  <accession>Q9CR53</accession>
  <name>NMB_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Neuromedin-B</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Neuromedin-B-32</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Neuromedin-B</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">Nmb</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Science" volume="309" first="1559" last="1563">
      <title>The transcriptional landscape of the mammalian genome.</title>
      <authorList>
        <person name="Carninci P."/>
        <person name="Kasukawa T."/>
        <person name="Katayama S."/>
        <person name="Gough J."/>
        <person name="Frith M.C."/>
        <person name="Maeda N."/>
        <person name="Oyama R."/>
        <person name="Ravasi T."/>
        <person name="Lenhard B."/>
        <person name="Wells C."/>
        <person name="Kodzius R."/>
        <person name="Shimokawa K."/>
        <person name="Bajic V.B."/>
        <person name="Brenner S.E."/>
        <person name="Batalov S."/>
        <person name="Forrest A.R."/>
        <person name="Zavolan M."/>
        <person name="Davis M.J."/>
        <person name="Wilming L.G."/>
        <person name="Aidinis V."/>
        <person name="Allen J.E."/>
        <person name="Ambesi-Impiombato A."/>
        <person name="Apweiler R."/>
        <person name="Aturaliya R.N."/>
        <person name="Bailey T.L."/>
        <person name="Bansal M."/>
        <person name="Baxter L."/>
        <person name="Beisel K.W."/>
        <person name="Bersano T."/>
        <person name="Bono H."/>
        <person name="Chalk A.M."/>
        <person name="Chiu K.P."/>
        <person name="Choudhary V."/>
        <person name="Christoffels A."/>
        <person name="Clutterbuck D.R."/>
        <person name="Crowe M.L."/>
        <person name="Dalla E."/>
        <person name="Dalrymple B.P."/>
        <person name="de Bono B."/>
        <person name="Della Gatta G."/>
        <person name="di Bernardo D."/>
        <person name="Down T."/>
        <person name="Engstrom P."/>
        <person name="Fagiolini M."/>
        <person name="Faulkner G."/>
        <person name="Fletcher C.F."/>
        <person name="Fukushima T."/>
        <person name="Furuno M."/>
        <person name="Futaki S."/>
        <person name="Gariboldi M."/>
        <person name="Georgii-Hemming P."/>
        <person name="Gingeras T.R."/>
        <person name="Gojobori T."/>
        <person name="Green R.E."/>
        <person name="Gustincich S."/>
        <person name="Harbers M."/>
        <person name="Hayashi Y."/>
        <person name="Hensch T.K."/>
        <person name="Hirokawa N."/>
        <person name="Hill D."/>
        <person name="Huminiecki L."/>
        <person name="Iacono M."/>
        <person name="Ikeo K."/>
        <person name="Iwama A."/>
        <person name="Ishikawa T."/>
        <person name="Jakt M."/>
        <person name="Kanapin A."/>
        <person name="Katoh M."/>
        <person name="Kawasawa Y."/>
        <person name="Kelso J."/>
        <person name="Kitamura H."/>
        <person name="Kitano H."/>
        <person name="Kollias G."/>
        <person name="Krishnan S.P."/>
        <person name="Kruger A."/>
        <person name="Kummerfeld S.K."/>
        <person name="Kurochkin I.V."/>
        <person name="Lareau L.F."/>
        <person name="Lazarevic D."/>
        <person name="Lipovich L."/>
        <person name="Liu J."/>
        <person name="Liuni S."/>
        <person name="McWilliam S."/>
        <person name="Madan Babu M."/>
        <person name="Madera M."/>
        <person name="Marchionni L."/>
        <person name="Matsuda H."/>
        <person name="Matsuzawa S."/>
        <person name="Miki H."/>
        <person name="Mignone F."/>
        <person name="Miyake S."/>
        <person name="Morris K."/>
        <person name="Mottagui-Tabar S."/>
        <person name="Mulder N."/>
        <person name="Nakano N."/>
        <person name="Nakauchi H."/>
        <person name="Ng P."/>
        <person name="Nilsson R."/>
        <person name="Nishiguchi S."/>
        <person name="Nishikawa S."/>
        <person name="Nori F."/>
        <person name="Ohara O."/>
        <person name="Okazaki Y."/>
        <person name="Orlando V."/>
        <person name="Pang K.C."/>
        <person name="Pavan W.J."/>
        <person name="Pavesi G."/>
        <person name="Pesole G."/>
        <person name="Petrovsky N."/>
        <person name="Piazza S."/>
        <person name="Reed J."/>
        <person name="Reid J.F."/>
        <person name="Ring B.Z."/>
        <person name="Ringwald M."/>
        <person name="Rost B."/>
        <person name="Ruan Y."/>
        <person name="Salzberg S.L."/>
        <person name="Sandelin A."/>
        <person name="Schneider C."/>
        <person name="Schoenbach C."/>
        <person name="Sekiguchi K."/>
        <person name="Semple C.A."/>
        <person name="Seno S."/>
        <person name="Sessa L."/>
        <person name="Sheng Y."/>
        <person name="Shibata Y."/>
        <person name="Shimada H."/>
        <person name="Shimada K."/>
        <person name="Silva D."/>
        <person name="Sinclair B."/>
        <person name="Sperling S."/>
        <person name="Stupka E."/>
        <person name="Sugiura K."/>
        <person name="Sultana R."/>
        <person name="Takenaka Y."/>
        <person name="Taki K."/>
        <person name="Tammoja K."/>
        <person name="Tan S.L."/>
        <person name="Tang S."/>
        <person name="Taylor M.S."/>
        <person name="Tegner J."/>
        <person name="Teichmann S.A."/>
        <person name="Ueda H.R."/>
        <person name="van Nimwegen E."/>
        <person name="Verardo R."/>
        <person name="Wei C.L."/>
        <person name="Yagi K."/>
        <person name="Yamanishi H."/>
        <person name="Zabarovsky E."/>
        <person name="Zhu S."/>
        <person name="Zimmer A."/>
        <person name="Hide W."/>
        <person name="Bult C."/>
        <person name="Grimmond S.M."/>
        <person name="Teasdale R.D."/>
        <person name="Liu E.T."/>
        <person name="Brusic V."/>
        <person name="Quackenbush J."/>
        <person name="Wahlestedt C."/>
        <person name="Mattick J.S."/>
        <person name="Hume D.A."/>
        <person name="Kai C."/>
        <person name="Sasaki D."/>
        <person name="Tomaru Y."/>
        <person name="Fukuda S."/>
        <person name="Kanamori-Katayama M."/>
        <person name="Suzuki M."/>
        <person name="Aoki J."/>
        <person name="Arakawa T."/>
        <person name="Iida J."/>
        <person name="Imamura K."/>
        <person name="Itoh M."/>
        <person name="Kato T."/>
        <person name="Kawaji H."/>
        <person name="Kawagashira N."/>
        <person name="Kawashima T."/>
        <person name="Kojima M."/>
        <person name="Kondo S."/>
        <person name="Konno H."/>
        <person name="Nakano K."/>
        <person name="Ninomiya N."/>
        <person name="Nishio T."/>
        <person name="Okada M."/>
        <person name="Plessy C."/>
        <person name="Shibata K."/>
        <person name="Shiraki T."/>
        <person name="Suzuki S."/>
        <person name="Tagami M."/>
        <person name="Waki K."/>
        <person name="Watahiki A."/>
        <person name="Okamura-Oho Y."/>
        <person name="Suzuki H."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
      </authorList>
      <dbReference type="PubMed" id="16141072"/>
      <dbReference type="DOI" id="10.1126/science.1112014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
      <tissue>Embryo</tissue>
      <tissue>Embryonic head</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
      <tissue>Mammary gland</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2016" name="Nature" volume="530" first="293" last="297">
      <title>The peptidergic control circuit for sighing.</title>
      <authorList>
        <person name="Li P."/>
        <person name="Janczewski W.A."/>
        <person name="Yackle K."/>
        <person name="Kam K."/>
        <person name="Pagliardini S."/>
        <person name="Krasnow M.A."/>
        <person name="Feldman J.L."/>
      </authorList>
      <dbReference type="PubMed" id="26855425"/>
      <dbReference type="DOI" id="10.1038/nature16964"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2017" name="Exp. Cell Res." volume="359" first="112" last="119">
      <title>Neuromedin B and its receptor silencing suppresses osteoclast generation by modulating precursor proliferation via M-CSF/c-Fms/D-type cyclins.</title>
      <authorList>
        <person name="Yeo C.E."/>
        <person name="Kang W.Y."/>
        <person name="Seong S.J."/>
        <person name="Cho S."/>
        <person name="Lee H.W."/>
        <person name="Yoon Y.R."/>
        <person name="Kim H.J."/>
      </authorList>
      <dbReference type="PubMed" id="28780306"/>
      <dbReference type="DOI" id="10.1016/j.yexcr.2017.08.003"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2019" name="Acta Derm. Venereol." volume="99" first="587" last="593">
      <title>Neuromedin B Induces Acute Itch in Mice via the Activation of Peripheral Sensory Neurons.</title>
      <authorList>
        <person name="Ehling S."/>
        <person name="Fukuyama T."/>
        <person name="Ko M.C."/>
        <person name="Olivry T."/>
        <person name="Baeumer W."/>
      </authorList>
      <dbReference type="PubMed" id="30734045"/>
      <dbReference type="DOI" id="10.2340/00015555-3143"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2019" name="Vet. Res." volume="50" first="80" last="80">
      <title>Role of neuromedin B and its receptor in the innate immune responses against influenza A virus infection in vitro and in vivo.</title>
      <authorList>
        <person name="Yang G."/>
        <person name="Huang H."/>
        <person name="Tang M."/>
        <person name="Cai Z."/>
        <person name="Huang C."/>
        <person name="Qi B."/>
        <person name="Chen J.L."/>
      </authorList>
      <dbReference type="PubMed" id="31601264"/>
      <dbReference type="DOI" id="10.1186/s13567-019-0695-2"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2021" name="Cell" volume="184" first="3762" last="3773">
      <title>Sneezing reflex is mediated by a peptidergic pathway from nose to brainstem.</title>
      <authorList>
        <person name="Li F."/>
        <person name="Jiang H."/>
        <person name="Shen X."/>
        <person name="Yang W."/>
        <person name="Guo C."/>
        <person name="Wang Z."/>
        <person name="Xiao M."/>
        <person name="Cui L."/>
        <person name="Luo W."/>
        <person name="Kim B.S."/>
        <person name="Chen Z."/>
        <person name="Huang A.J.W."/>
        <person name="Liu Q."/>
      </authorList>
      <dbReference type="PubMed" id="34133943"/>
      <dbReference type="DOI" id="10.1016/j.cell.2021.05.017"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3 4 5 6">Stimulates smooth muscle contraction (By similarity). Induces sighing by acting directly on the pre-Botzinger complex, a cluster of several thousand neurons in the ventrolateral medulla responsible for inspiration during respiratory activity (PubMed:26855425). Contributes to the induction of sneezing following exposure to chemical irritants or allergens which causes release of NMB by nasal sensory neurons and activation of NMBR-expressing neurons in the sneeze-evoking region of the brainstem (PubMed:34133943). These in turn activate neurons of the caudal ventral respiratory group, giving rise to the sneeze reflex (PubMed:34133943). Contributes to induction of acute itch, possibly through activation of the NMBR receptor on dorsal root ganglion neurons (PubMed:30734045). Increases expression of NMBR and steroidogenic mediators STAR, CYP11A1 and HSD3B1 in Leydig cells, induces secretion of testosterone by Leydig cells and also promotes Leydig cell proliferation (By similarity). Plays a role in the innate immune response to influenza A virus infection by enhancing interferon alpha expression and reducing expression of IL6 (PubMed:31601264). Plays a role in CSF1-induced proliferation of osteoclast precursors by contributing to the positive regulation of the expression of the CSF1 receptor CSF1R (PubMed:28780306).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Cell projection</location>
      <location evidence="2">Neuron projection</location>
    </subcellularLocation>
    <text evidence="2">In neurons of the retrotrapezoid nucleus//parafacial respiratory group, expressed on neuron projections which project into the pre-Botzinger complex.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2 5">In the hindbrain, expressed in the medulla surrounding the lateral half of the facial nucleus (PubMed:26855425). Also expressed in the olfactory bulb and hippocampus (PubMed:26855425). Detected in a subset of neurons distributed throughout the retrotrapezoid nucleus/parafacial respiratory group (RTN/pFRG) (PubMed:26855425). Within the RTN/pFRG, expressed in neuronal subpopulations distinct from those expressing Grp (PubMed:26855425). Expressed in lung (PubMed:31601264).</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="3">During osteoclast development, expression increases as the cells differentiate with high expression levels in mature osteoclasts (at protein level).</text>
  </comment>
  <comment type="induction">
    <text evidence="5">Up-regulated in lung tissue in response to infection with influenza A virus.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="6">Significant reduction in the sneezing response to capsaicin at both low and high doses but does not affect pain-related nose wiping behavior or apnea induced by capsaicin (PubMed:34133943). Abolishes the sneezing response to histamine, seratonin and the neuropeptide Npff (PubMed:34133943). Abolishes sneezing response to mast cell-dependent allergy (PubMed:34133943).</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the bombesin/neuromedin-B/ranatensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AK014077">
    <property type="protein sequence ID" value="BAB29144.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK011929">
    <property type="protein sequence ID" value="BAB27922.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC028490">
    <property type="protein sequence ID" value="AAH28490.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS21400.1"/>
  <dbReference type="RefSeq" id="NP_080799.1">
    <property type="nucleotide sequence ID" value="NM_026523.4"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9CR53"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000113407"/>
  <dbReference type="PhosphoSitePlus" id="Q9CR53"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000026817"/>
  <dbReference type="ProteomicsDB" id="293688"/>
  <dbReference type="Antibodypedia" id="43613">
    <property type="antibodies" value="159 antibodies from 26 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="68039"/>
  <dbReference type="Ensembl" id="ENSMUST00000026817.11">
    <property type="protein sequence ID" value="ENSMUSP00000026817.5"/>
    <property type="gene ID" value="ENSMUSG00000025723.13"/>
  </dbReference>
  <dbReference type="GeneID" id="68039"/>
  <dbReference type="KEGG" id="mmu:68039"/>
  <dbReference type="UCSC" id="uc009ibm.3">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:1915289"/>
  <dbReference type="CTD" id="4828"/>
  <dbReference type="MGI" id="MGI:1915289">
    <property type="gene designation" value="Nmb"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000025723"/>
  <dbReference type="eggNOG" id="ENOG502S66V">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000154470"/>
  <dbReference type="HOGENOM" id="CLU_136527_0_0_1"/>
  <dbReference type="InParanoid" id="Q9CR53"/>
  <dbReference type="OrthoDB" id="4741465at2759"/>
  <dbReference type="PhylomeDB" id="Q9CR53"/>
  <dbReference type="TreeFam" id="TF336860"/>
  <dbReference type="Reactome" id="R-MMU-375276">
    <property type="pathway name" value="Peptide ligand-binding receptors"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-416476">
    <property type="pathway name" value="G alpha (q) signalling events"/>
  </dbReference>
  <dbReference type="BioGRID-ORCS" id="68039">
    <property type="hits" value="1 hit in 79 CRISPR screens"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q9CR53"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 7"/>
  </dbReference>
  <dbReference type="RNAct" id="Q9CR53">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000025723">
    <property type="expression patterns" value="Expressed in lumbar dorsal root ganglion and 137 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q9CR53">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043005">
    <property type="term" value="C:neuron projection"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0140374">
    <property type="term" value="P:antiviral innate immune response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0160024">
    <property type="term" value="P:Leydig cell proliferation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032715">
    <property type="term" value="P:negative regulation of interleukin-6 production"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032727">
    <property type="term" value="P:positive regulation of interferon-alpha production"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090290">
    <property type="term" value="P:positive regulation of osteoclast proliferation"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1903942">
    <property type="term" value="P:positive regulation of respiratory gaseous exchange"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000845">
    <property type="term" value="P:positive regulation of testosterone secretion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0160025">
    <property type="term" value="P:sensory perception of itch"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0160023">
    <property type="term" value="P:sneeze reflex"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000874">
    <property type="entry name" value="Bombesin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16866">
    <property type="entry name" value="GASTRIN-RELEASING PEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16866:SF3">
    <property type="entry name" value="NEUROMEDIN-B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02044">
    <property type="entry name" value="Bombesin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00257">
    <property type="entry name" value="BOMBESIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0966">Cell projection</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003020" description="Neuromedin-B-32" evidence="1">
    <location>
      <begin position="25"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003021" description="Neuromedin-B" evidence="1">
    <location>
      <begin position="47"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003022" evidence="1">
    <location>
      <begin position="60"/>
      <end position="121"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="1">
    <location>
      <position position="56"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01297"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="26855425"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="28780306"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="30734045"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="31601264"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="34133943"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="121" mass="13571" checksum="1EE94C963D3D9D31" modified="2001-06-01" version="1" precursor="true">MTRQAGSSWLLRGLLLFALFASGVAPFNWDLPEPRSRASKIRVHPRGNLWATGHFMGKKSLEPPSLSLVGTAPPNTPRDQRLQLSHDLLRILLRKKALGMNFSGPAPPIQYRRLLEPLLQK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>