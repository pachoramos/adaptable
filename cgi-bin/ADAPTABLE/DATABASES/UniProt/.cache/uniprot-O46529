<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-12-15" modified="2024-11-27" version="91" xmlns="http://uniprot.org/uniprot">
  <accession>O46529</accession>
  <name>RNAS6_SAISC</name>
  <protein>
    <recommendedName>
      <fullName>Ribonuclease K6</fullName>
      <shortName>RNase K6</shortName>
      <ecNumber>3.1.27.-</ecNumber>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">RNASE6</name>
  </gene>
  <organism>
    <name type="scientific">Saimiri sciureus</name>
    <name type="common">Common squirrel monkey</name>
    <dbReference type="NCBI Taxonomy" id="9521"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Platyrrhini</taxon>
      <taxon>Cebidae</taxon>
      <taxon>Saimiriinae</taxon>
      <taxon>Saimiri</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Genome Res." volume="8" first="599" last="607">
      <title>Ribonuclease k6: chromosomal mapping and divergent rates of evolution within the RNase A gene superfamily.</title>
      <authorList>
        <person name="Deming M.S."/>
        <person name="Dyer K.D."/>
        <person name="Bankier A.T."/>
        <person name="Piper M.B."/>
        <person name="Dear P.H."/>
        <person name="Rosenberg H.F."/>
      </authorList>
      <dbReference type="PubMed" id="9647635"/>
      <dbReference type="DOI" id="10.1101/gr.8.6.599"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Lung</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Ribonuclease which shows a preference for the pyrimidines uridine and cytosine. Has potent antibacterial activity against a range of Gram-positive and Gram-negative bacteria, including P.aeruginosa, A.baumanii, M.luteus, S.aureus, E.faecalis, E.faecium, S.saprophyticus and E.coli. Causes loss of bacterial membrane integrity, and also promotes agglutination of Gram-negative bacteria. Probably contributes to urinary tract sterility. Bactericidal activity is independent of RNase activity.</text>
  </comment>
  <comment type="subunit">
    <text evidence="3">Interacts (via N-terminus) with bacterial lipopolysaccharide (LPS).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="3">Lysosome</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="3">Cytoplasmic granule</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the pancreatic ribonuclease family.</text>
  </comment>
  <dbReference type="EC" id="3.1.27.-"/>
  <dbReference type="EMBL" id="AF037085">
    <property type="protein sequence ID" value="AAB94747.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O46529"/>
  <dbReference type="SMR" id="O46529"/>
  <dbReference type="GlyCosmos" id="O46529">
    <property type="glycosylation" value="2 sites, No reported glycans"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005764">
    <property type="term" value="C:lysosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004519">
    <property type="term" value="F:endonuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003676">
    <property type="term" value="F:nucleic acid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004540">
    <property type="term" value="F:RNA nuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="CDD" id="cd06265">
    <property type="entry name" value="RNase_A_canonical"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.130.10:FF:000001">
    <property type="entry name" value="Ribonuclease pancreatic"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.130.10">
    <property type="entry name" value="Ribonuclease A-like domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001427">
    <property type="entry name" value="RNaseA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036816">
    <property type="entry name" value="RNaseA-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023411">
    <property type="entry name" value="RNaseA_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023412">
    <property type="entry name" value="RNaseA_domain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437">
    <property type="entry name" value="RIBONUCLEASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437:SF4">
    <property type="entry name" value="RIBONUCLEASE K6"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00074">
    <property type="entry name" value="RnaseA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00794">
    <property type="entry name" value="RIBONUCLEASE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00092">
    <property type="entry name" value="RNAse_Pc"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54076">
    <property type="entry name" value="RNase A-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00127">
    <property type="entry name" value="RNASE_PANCREATIC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0255">Endonuclease</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0458">Lysosome</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000030899" description="Ribonuclease K6">
    <location>
      <begin position="24"/>
      <end position="150"/>
    </location>
  </feature>
  <feature type="active site" description="Proton acceptor" evidence="2">
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="active site" description="Proton donor" evidence="2">
    <location>
      <position position="145"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <begin position="61"/>
      <end position="65"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="86"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="105"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="site" description="Facilitates cleavage of polynucleotide substrates" evidence="3">
    <location>
      <position position="59"/>
    </location>
  </feature>
  <feature type="site" description="Critical for catalytic activity" evidence="4">
    <location>
      <position position="61"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="5">
    <location>
      <position position="55"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="5">
    <location>
      <position position="100"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="46"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="60"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="78"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="85"/>
      <end position="92"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q64438"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="Q93091"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="4">
    <source>
      <dbReference type="UniProtKB" id="Q9H1E1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="5"/>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="150" mass="17074" checksum="1163D363BE88CD4A" modified="1998-06-01" version="1" precursor="true">MVLCFPLLLLLLVLWGQVCPLHAIPKHLTKARWFEIQHIRPSPLQCNRAMNGINNYTQHCKPQNTFLHDSFQNVAAVCDLLSITCKNGYHNCHQSLKPVNMTDCRLTSGSYPQCRYSTAAQYKLFIIACEPPQKSDPPYNLVPVHLDSIL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>