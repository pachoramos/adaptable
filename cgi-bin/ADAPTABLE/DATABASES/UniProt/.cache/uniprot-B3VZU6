<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-02-09" modified="2022-05-25" version="24" xmlns="http://uniprot.org/uniprot">
  <accession>B3VZU6</accession>
  <name>TPCE_RANDY</name>
  <protein>
    <recommendedName>
      <fullName evidence="4 6">Temporin-CDYe</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Rana dybowskii</name>
    <name type="common">Dybovsky's frog</name>
    <name type="synonym">Korean brown frog</name>
    <dbReference type="NCBI Taxonomy" id="71582"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Rana</taxon>
      <taxon>Rana</taxon>
    </lineage>
  </organism>
  <reference evidence="5 6" key="1">
    <citation type="journal article" date="2009" name="Comp. Biochem. Physiol." volume="154B" first="174" last="178">
      <title>Characterization of antimicrobial peptides isolated from the skin of the Chinese frog, Rana dybowskii.</title>
      <authorList>
        <person name="Jin L.-L."/>
        <person name="Li Q."/>
        <person name="Song S.-S."/>
        <person name="Feng K."/>
        <person name="Zhang D.-B."/>
        <person name="Wang Q.-Y."/>
        <person name="Chen Y.-H."/>
      </authorList>
      <dbReference type="PubMed" id="19539775"/>
      <dbReference type="DOI" id="10.1016/j.cbpb.2009.05.015"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue evidence="6">Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Temporin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="EU827806">
    <property type="protein sequence ID" value="ACF08006.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B3VZU6"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051001">
    <property type="term" value="P:negative regulation of nitric-oxide synthase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2 6">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000391435" evidence="1">
    <location>
      <begin position="23"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5000381487" description="Temporin-CDYe" evidence="3">
    <location>
      <begin position="45"/>
      <end position="59"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P79874"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="19539775"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="19539775"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="ACF08006.1"/>
    </source>
  </evidence>
  <sequence length="59" mass="6519" checksum="F94814028EC38FB0" modified="2008-09-02" version="1" precursor="true">MFTLKKSMLLLLFLGTISLTLCEEERDANEEEENGGEVKVEEKRFIGPIISALASLFGG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>