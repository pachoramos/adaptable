<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-03-18" modified="2024-11-27" version="92" xmlns="http://uniprot.org/uniprot">
  <accession>A9QWQ1</accession>
  <name>CXL11_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>C-X-C motif chemokine 11</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Small-inducible cytokine B11</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CXCL11</name>
  </gene>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2007-11" db="EMBL/GenBank/DDBJ databases">
      <title>U.S. veterinary immune reagent network: expressed bovine gene sequences.</title>
      <authorList>
        <consortium name="U.S. Veterinary Immune Reagent Network"/>
        <person name="Hudgens T."/>
        <person name="Tompkins D."/>
        <person name="Baldwin C.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Belted Galloway</strain>
      <tissue>Peripheral blood</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Chemotactic for interleukin-activated T-cells but not unstimulated T-cells, neutrophils or monocytes. Induces calcium release in activated T-cells. Binds to CXCR3. May play an important role in CNS diseases which involve T-cell recruitment. May play a role in skin immune responses (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Interacts with TNFAIP6 (via Link domain).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the intercrine alpha (chemokine CxC) family.</text>
  </comment>
  <dbReference type="EMBL" id="EU276063">
    <property type="protein sequence ID" value="ABX72061.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001106644.1">
    <property type="nucleotide sequence ID" value="NM_001113173.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A9QWQ1"/>
  <dbReference type="SMR" id="A9QWQ1"/>
  <dbReference type="STRING" id="9913.ENSBTAP00000034148"/>
  <dbReference type="PaxDb" id="9913-ENSBTAP00000034148"/>
  <dbReference type="Ensembl" id="ENSBTAT00000125932.1">
    <property type="protein sequence ID" value="ENSBTAP00000079907.1"/>
    <property type="gene ID" value="ENSBTAG00000063224.1"/>
  </dbReference>
  <dbReference type="GeneID" id="516104"/>
  <dbReference type="KEGG" id="bta:516104"/>
  <dbReference type="CTD" id="6373"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSBTAG00000005603"/>
  <dbReference type="eggNOG" id="ENOG502SGFE">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00530000064263"/>
  <dbReference type="HOGENOM" id="CLU_143902_2_3_1"/>
  <dbReference type="InParanoid" id="A9QWQ1"/>
  <dbReference type="OMA" id="ANKGQRC"/>
  <dbReference type="OrthoDB" id="2897283at2759"/>
  <dbReference type="TreeFam" id="TF333433"/>
  <dbReference type="Reactome" id="R-BTA-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Chromosome 6"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSBTAG00000005603">
    <property type="expression patterns" value="Expressed in abdominal lymph node and 85 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045236">
    <property type="term" value="F:CXCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048248">
    <property type="term" value="F:CXCR3 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007189">
    <property type="term" value="P:adenylate cyclase-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006935">
    <property type="term" value="P:chemotaxis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030593">
    <property type="term" value="P:neutrophil chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051281">
    <property type="term" value="P:positive regulation of release of sequestered calcium ion into cytosol"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042127">
    <property type="term" value="P:regulation of cell population proliferation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00273">
    <property type="entry name" value="Chemokine_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000004">
    <property type="entry name" value="C-X-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001089">
    <property type="entry name" value="Chemokine_CXC"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018048">
    <property type="entry name" value="Chemokine_CXC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033899">
    <property type="entry name" value="CXC_Chemokine_domain"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF209">
    <property type="entry name" value="C-X-C MOTIF CHEMOKINE 11"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00437">
    <property type="entry name" value="SMALLCYTKCXC"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00471">
    <property type="entry name" value="SMALL_CYTOKINES_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000326170" description="C-X-C motif chemokine 11">
    <location>
      <begin position="22"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="30"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="O14625"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="100" mass="10943" checksum="7FF6C292B391D5FB" modified="2008-02-05" version="1" precursor="true">MSVKGMAIVLTVILCAAIVQGFPMFKGGRCLCIGPGVKAVKVADIEKVSIIYPTNNCDKTEVIITLKTHKGQRCLNPKAKQAKAIIKKVQRKNSEKYKNI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>