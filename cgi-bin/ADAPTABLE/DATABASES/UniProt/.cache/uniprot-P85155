<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-06-26" modified="2024-10-02" version="21" xmlns="http://uniprot.org/uniprot">
  <accession>P85155</accession>
  <name>ENPL4_LYSSX</name>
  <protein>
    <recommendedName>
      <fullName>Endopeptidase L4</fullName>
      <ecNumber>3.4.21.-</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lysobacter sp. (strain XL1)</name>
    <dbReference type="NCBI Taxonomy" id="186334"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Lysobacterales</taxon>
      <taxon>Lysobacteraceae</taxon>
      <taxon>Lysobacter</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="submission" date="2007-05" db="UniProtKB">
      <title>Identification of extracellular bacteriolytic enzymes from Lysobacter sp. XL1.</title>
      <authorList>
        <person name="Muranova T.A."/>
        <person name="Stepnaya O.A."/>
        <person name="Tsfasman I.M."/>
        <person name="Kulaev I.S."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBUNIT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference evidence="4" key="2">
    <citation type="journal article" date="2005" name="Biochemistry (Mosc.)" volume="70" first="1031" last="1037">
      <title>Isolation and characterization of a new extracellular bacteriolytic endopeptidase of Lysobacter sp. XL1.</title>
      <authorList>
        <person name="Stepnaya O.A."/>
        <person name="Tsfasman I.M."/>
        <person name="Logvina I.A."/>
        <person name="Ryazanova L.P."/>
        <person name="Muranova T.A."/>
        <person name="Kulaev I.S."/>
      </authorList>
      <dbReference type="PubMed" id="16266276"/>
      <dbReference type="DOI" id="10.1007/s10541-005-0221-1"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 1-14</scope>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Diaminopimelinoyl-alanine endopeptidase. Has antibacterial activity.</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="1">Inhibited by PMSF and p-chloromercuribenzoate. Unaffected by EDTA.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <phDependence>
      <text evidence="1">Optimum pH is 8.0.</text>
    </phDependence>
    <temperatureDependence>
      <text evidence="1">Optimum temperature is 50-55 degrees Celsius. Retains 50% of its maximal activity after incubation at 52 degrees Celsius for 15 minutes.</text>
    </temperatureDependence>
  </comment>
  <comment type="subunit">
    <text evidence="2">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EC" id="3.4.21.-"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008236">
    <property type="term" value="F:serine-type peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006508">
    <property type="term" value="P:proteolysis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0645">Protease</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0720">Serine protease</keyword>
  <feature type="chain" id="PRO_0000292608" description="Endopeptidase L4">
    <location>
      <begin position="1"/>
      <end position="15" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="3">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="16266276"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="15" mass="1465" checksum="1C6918761FD4BCDE" modified="2007-06-26" version="1" fragment="single">AVVNGVNYVGETTAA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>