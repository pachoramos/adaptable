<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-04-01" modified="2024-10-02" version="94" xmlns="http://uniprot.org/uniprot">
  <accession>P14947</accession>
  <name>MPAL2_LOLPR</name>
  <protein>
    <recommendedName>
      <fullName>Pollen allergen Lol p 2-A</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Allergen Lol p II-A</fullName>
    </alternativeName>
    <allergenName>Lol p 2-A</allergenName>
  </protein>
  <organism>
    <name type="scientific">Lolium perenne</name>
    <name type="common">Perennial ryegrass</name>
    <dbReference type="NCBI Taxonomy" id="4522"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>Liliopsida</taxon>
      <taxon>Poales</taxon>
      <taxon>Poaceae</taxon>
      <taxon>BOP clade</taxon>
      <taxon>Pooideae</taxon>
      <taxon>Poodae</taxon>
      <taxon>Poeae</taxon>
      <taxon>Poeae Chloroplast Group 2 (Poeae type)</taxon>
      <taxon>Loliodinae</taxon>
      <taxon>Loliinae</taxon>
      <taxon>Lolium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1989" name="J. Biol. Chem." volume="264" first="11181" last="11185">
      <title>Complete amino acid sequence of a Lolium perenne (perennial rye grass) pollen allergen, Lol p II.</title>
      <authorList>
        <person name="Ansari A.A."/>
        <person name="Shenbagamurthi P."/>
        <person name="Marsh D.G."/>
      </authorList>
      <dbReference type="PubMed" id="2472390"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)60446-6"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>ALLERGEN</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="allergen">
    <text evidence="2">Causes an allergic reaction in human. Causes grass pollen allergy. Binds to IgE.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the expansin family. Expansin B subfamily.</text>
  </comment>
  <dbReference type="PIR" id="A34291">
    <property type="entry name" value="A34291"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P14947"/>
  <dbReference type="SMR" id="P14947"/>
  <dbReference type="Allergome" id="3355">
    <property type="allergen name" value="Lol p 2.0101"/>
  </dbReference>
  <dbReference type="Allergome" id="457">
    <property type="allergen name" value="Lol p 2"/>
  </dbReference>
  <dbReference type="ABCD" id="P14947">
    <property type="antibodies" value="1 sequenced antibody"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009828">
    <property type="term" value="P:plant-type cell wall loosening"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.760">
    <property type="entry name" value="Expansin, cellulose-binding-like domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005453">
    <property type="entry name" value="Allergen_Lolp2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007117">
    <property type="entry name" value="Expansin_CBD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036749">
    <property type="entry name" value="Expansin_CBD_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR31692">
    <property type="entry name" value="EXPANSIN-B3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR31692:SF13">
    <property type="entry name" value="OS06G0662300 PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01357">
    <property type="entry name" value="Expansin_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01637">
    <property type="entry name" value="LOLP2ALLERGN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF49590">
    <property type="entry name" value="PHL pollen allergen"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50843">
    <property type="entry name" value="EXPANSIN_CBD"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0020">Allergen</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000154572" description="Pollen allergen Lol p 2-A">
    <location>
      <begin position="1"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="domain" description="Expansin-like CBD" evidence="1">
    <location>
      <begin position="15"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>S</original>
    <variation>A</variation>
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>N</original>
    <variation>D</variation>
    <location>
      <position position="49"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>D</original>
    <variation>E</variation>
    <location>
      <position position="59"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>M</original>
    <variation>E</variation>
    <location>
      <position position="76"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00078"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="2472390"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="97" mass="10880" checksum="91A7DDB5DEA5BDD7" modified="1990-04-01" version="1">AAPVEFTVEKGSDEKNLALSIKYNKEGDSMAEVELKEHGSNEWLALKKNGDGVWEIKSDKPLKGPFNFRFVSEKGMRNVFDDVVPADFKVGTTYKPE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>