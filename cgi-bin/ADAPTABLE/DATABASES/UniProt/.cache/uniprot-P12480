<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1989-10-01" modified="2024-11-27" version="113" xmlns="http://uniprot.org/uniprot">
  <accession>P12480</accession>
  <name>NEF_HV1J3</name>
  <protein>
    <recommendedName>
      <fullName>Protein Nef</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>3'ORF</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Negative factor</fullName>
      <shortName>F-protein</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">nef</name>
  </gene>
  <organism>
    <name type="scientific">Human immunodeficiency virus type 1 group M subtype B (isolate JH32)</name>
    <name type="common">HIV-1</name>
    <dbReference type="NCBI Taxonomy" id="11694"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Pararnavirae</taxon>
      <taxon>Artverviricota</taxon>
      <taxon>Revtraviricetes</taxon>
      <taxon>Ortervirales</taxon>
      <taxon>Retroviridae</taxon>
      <taxon>Orthoretrovirinae</taxon>
      <taxon>Lentivirus</taxon>
      <taxon>Human immunodeficiency virus type 1</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1989" name="AIDS Res. Hum. Retroviruses" volume="5" first="411" last="419">
      <title>Nucleotide sequences of gag and env genes of a Japanese isolate of HIV-1 and their expression in bacteria.</title>
      <authorList>
        <person name="Komiyama N."/>
        <person name="Hattori N."/>
        <person name="Inoue J."/>
        <person name="Sakuma S."/>
        <person name="Kurimura T."/>
        <person name="Yoshida M."/>
      </authorList>
      <dbReference type="PubMed" id="2669897"/>
      <dbReference type="DOI" id="10.1089/aid.1989.5.411"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Factor of infectivity and pathogenicity, required for optimal virus replication. Alters numerous pathways of T-lymphocyte function and down-regulates immunity surface molecules in order to evade host defense and increase viral infectivity. Alters the functionality of other immunity cells, like dendritic cells, monocytes/macrophages and NK cells. One of the earliest and most abundantly expressed viral proteins (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="1">In infected CD4(+) T-lymphocytes, down-regulates the surface MHC-I, mature MHC-II, CD4, CD28, CCR5 and CXCR4 molecules. Mediates internalization and degradation of host CD4 through the interaction of with the cytoplasmic tail of CD4, the recruitment of AP-2 (clathrin adapter protein complex 2), internalization through clathrin coated pits, and subsequent transport to endosomes and lysosomes for degradation. Diverts host MHC-I molecules to the trans-Golgi network-associated endosomal compartments by an endocytic pathway to finally target them for degradation. MHC-I down-regulation may involve AP-1 (clathrin adapter protein complex 1) or possibly Src family kinase-ZAP70/Syk-PI3K cascade recruited by PACS2. In consequence infected cells are masked for immune recognition by cytotoxic T-lymphocytes. Decreasing the number of immune receptors also prevents reinfection by more HIV particles (superinfection) (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="1">Bypasses host T-cell signaling by inducing a transcriptional program nearly identical to that of anti-CD3 cell activation. Interaction with TCR-zeta chain up-regulates the Fas ligand (FasL). Increasing surface FasL molecules and decreasing surface MHC-I molecules on infected CD4(+) cells send attacking cytotoxic CD8+ T-lymphocytes into apoptosis (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="1">Plays a role in optimizing the host cell environment for viral replication without causing cell death by apoptosis. Protects the infected cells from apoptosis in order to keep them alive until the next virus generation is ready to strike. Inhibits the Fas and TNFR-mediated death signals by blocking MAP3K5. Interacts and decreases the half-life of p53, protecting the infected cell against p53-mediated apoptosis. Inhibits the apoptotic signals regulated by the Bcl-2 family proteins through the formation of a Nef/PI3-kinase/PAK2 complex that leads to activation of PAK2 and induces phosphorylation of Bad (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="1">Extracellular Nef protein targets CD4(+) T-lymphocytes for apoptosis by interacting with CXCR4 surface receptors.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer (By similarity). Interacts with Nef associated p21-activated kinase (PAK2); this interaction activates PAK2. Associates with the Nef-MHC-I-AP1 complex; this complex is required for MHC-I internalization. Interacts (via C-terminus) with host PI3-kinase (via C-terminus). Interacts with host PACS1; this interaction seems to be weak. Interacts with host PACS2. Interacts with host LCK and MAPK3; these interactions inhibit the kinase activity of the latter. Interacts with host ATP6V1H; this interaction may play a role in CD4 endocytosis. Associates with the CD4-Nef-AP2 complex; this complex is required for CD4 internalization. Interacts with TCR-zeta chain; this interaction up-regulates the Fas ligand (FasL) surface expression. Interacts with various cellular proteins including MAP3K5, beta-COP, HCK, and PTE1. Interacts with human RACK1; this increases Nef phosphorylation by PKC (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host cell membrane</location>
      <topology evidence="1">Lipid-anchor</topology>
      <orientation evidence="1">Cytoplasmic side</orientation>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
      <location evidence="1">Host perinuclear region</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Virion</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <text evidence="1">Predominantly found in the paranuclear area, probably in the TGN. Correct localization requires PACS1. Also associates with the inner plasma membrane through its N-terminal domain. Nef stimulates its own export via the release of exosomes. Also incorporated in virions at a rate of about 10 molecules per virion, where it is cleaved (By similarity).</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The N-terminal domain is composed of the N-myristoyl glycine and of a cluster of positively charged amino acids. It is required for inner plasma membrane targeting of Nef and virion incorporation, and thereby for infectivity. This domain is also involved in binding to p53 (By similarity).</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The SH3-binding domain constituted of PxxP motifs mediates binding to several Src family proteins thereby regulating their tyrosine kinase activity. The same motifs also mediates the association with MAPK3, PI3-kinase and TCR-zeta (By similarity).</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The di-leucine internalization motif and a diacidic motif seem to be required for binding to AP-2.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The acidic region may play a stabilizing role in the formation of a ternary complex between Nef, the MHC-I cytoplasmic domain, and AP1M1.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">The virion-associated Nef proteins are cleaved by the viral protease to release the soluble C-terminal core protein. Nef is probably cleaved concomitantly with viral structural proteins on maturation of virus particles (By similarity).</text>
  </comment>
  <comment type="PTM">
    <text>Phosphorylated on serine residues, probably by host PKC.</text>
  </comment>
  <comment type="miscellaneous">
    <text>HIV-1 lineages are divided in three main groups, M (for Major), O (for Outlier), and N (for New, or Non-M, Non-O). The vast majority of strains found worldwide belong to the group M. Group O seems to be endemic to and largely confined to Cameroon and neighboring countries in West Central Africa, where these viruses represent a small minority of HIV-1 strains. The group N is represented by a limited number of isolates from Cameroonian persons. The group M is further subdivided in 9 clades or subtypes (A to D, F to H, J and K).</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the lentivirus primate group Nef protein family.</text>
  </comment>
  <dbReference type="EMBL" id="AH003604">
    <property type="protein sequence ID" value="AAB03527.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044220">
    <property type="term" value="C:host cell perinuclear region of cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0020002">
    <property type="term" value="C:host cell plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044423">
    <property type="term" value="C:virion component"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.890.10">
    <property type="entry name" value="HIV 1 nef anchor domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027480">
    <property type="entry name" value="HIV-1_Nef_anchor_sf"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0014">AIDS</keyword>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-0244">Early protein</keyword>
  <keyword id="KW-1032">Host cell membrane</keyword>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1043">Host membrane</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-0449">Lipoprotein</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0519">Myristate</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <keyword id="KW-0946">Virion</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="initiator methionine" description="Removed; by host" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000085228" description="Protein Nef">
    <location>
      <begin position="2"/>
      <end position="17" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="N-terminal; associates with the host plasma membrane" evidence="1">
    <location>
      <begin position="2"/>
      <end position="17" status="greater than"/>
    </location>
  </feature>
  <feature type="lipid moiety-binding region" description="N-myristoyl glycine; by host" evidence="1">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="17"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="17" mass="1901" checksum="6E6B3F26EFEB921E" modified="2007-01-23" version="3" fragment="single">MGGKWSKRSVVGWPAVR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>