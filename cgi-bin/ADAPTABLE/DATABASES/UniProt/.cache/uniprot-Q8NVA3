<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-11-28" modified="2024-11-27" version="103" xmlns="http://uniprot.org/uniprot">
  <accession>Q8NVA3</accession>
  <name>MOAE_STAAW</name>
  <protein>
    <recommendedName>
      <fullName>Molybdopterin synthase catalytic subunit</fullName>
      <ecNumber>2.8.1.12</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>MPT synthase subunit 2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Molybdenum cofactor biosynthesis protein E</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Molybdopterin-converting factor large subunit</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Molybdopterin-converting factor subunit 2</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">moaE</name>
    <name type="ordered locus">MW2189</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Converts molybdopterin precursor Z into molybdopterin. This requires the incorporation of two sulfur atoms into precursor Z to generate a dithiolene group. The sulfur is provided by MoaD (By similarity).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>2 [molybdopterin-synthase sulfur-carrier protein]-C-terminal Gly-NH-CH2-C(O)SH + cyclic pyranopterin phosphate + H2O = molybdopterin + 2 [molybdopterin-synthase sulfur-carrier protein]-C-terminal Gly-Gly + 4 H(+)</text>
      <dbReference type="Rhea" id="RHEA:26333"/>
      <dbReference type="Rhea" id="RHEA-COMP:12160"/>
      <dbReference type="Rhea" id="RHEA-COMP:12202"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:58698"/>
      <dbReference type="ChEBI" id="CHEBI:59648"/>
      <dbReference type="ChEBI" id="CHEBI:90619"/>
      <dbReference type="ChEBI" id="CHEBI:90778"/>
      <dbReference type="EC" id="2.8.1.12"/>
    </reaction>
  </comment>
  <comment type="pathway">
    <text>Cofactor biosynthesis; molybdopterin biosynthesis.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Heterotetramer of 2 MoaD subunits and 2 MoaE subunits. Also stable as homodimer. The enzyme changes between these two forms during catalysis (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the MoaE family.</text>
  </comment>
  <dbReference type="EC" id="2.8.1.12"/>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB96054.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000808500.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8NVA3"/>
  <dbReference type="SMR" id="Q8NVA3"/>
  <dbReference type="KEGG" id="sam:MW2189"/>
  <dbReference type="HOGENOM" id="CLU_089568_1_2_9"/>
  <dbReference type="UniPathway" id="UPA00344"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030366">
    <property type="term" value="F:molybdopterin synthase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006777">
    <property type="term" value="P:Mo-molybdopterin cofactor biosynthetic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00756">
    <property type="entry name" value="MoaE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.90.1170.40:FF:000003">
    <property type="entry name" value="Molybdopterin converting factor subunit 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.90.1170.40">
    <property type="entry name" value="Molybdopterin biosynthesis MoaE subunit"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036563">
    <property type="entry name" value="MoaE_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003448">
    <property type="entry name" value="Mopterin_biosynth_MoaE"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23404:SF2">
    <property type="entry name" value="MOLYBDOPTERIN SYNTHASE CATALYTIC SUBUNIT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23404">
    <property type="entry name" value="MOLYBDOPTERIN SYNTHASE RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02391">
    <property type="entry name" value="MoaE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54690">
    <property type="entry name" value="Molybdopterin synthase subunit MoaE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0501">Molybdenum cofactor biosynthesis</keyword>
  <keyword id="KW-0808">Transferase</keyword>
  <feature type="chain" id="PRO_0000163102" description="Molybdopterin synthase catalytic subunit">
    <location>
      <begin position="1"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <begin position="34"/>
      <end position="36"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="44"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <begin position="100"/>
      <end position="101"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="116"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <begin position="123"/>
      <end position="125"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="148" mass="17339" checksum="B4292F340523EA19" modified="2002-10-01" version="1">MKQFEIVTEPIQTEQYREFTINEYQGAVVVFTGHVREWTKGVKTEYLEYEAYIPMAEKKLAQIGDEINEKWPGTITSIVHRIGPLQISDIAVLIAVSSPHRKDAYRANEYAIERIKEIVPIWKKEIWEDGSKWQGHQKGNYEEAKREE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>