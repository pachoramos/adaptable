<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-05-05" modified="2024-05-29" version="42" xmlns="http://uniprot.org/uniprot">
  <accession>P0C9E5</accession>
  <name>VHLP_ASFK5</name>
  <protein>
    <recommendedName>
      <fullName>Viral histone-like protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">DNA-binding protein pA104R</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>pA104R</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="ordered locus">Ken-047</name>
  </gene>
  <organism>
    <name type="scientific">African swine fever virus (isolate Pig/Kenya/KEN-50/1950)</name>
    <name type="common">ASFV</name>
    <dbReference type="NCBI Taxonomy" id="561445"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Varidnaviria</taxon>
      <taxon>Bamfordvirae</taxon>
      <taxon>Nucleocytoviricota</taxon>
      <taxon>Pokkesviricetes</taxon>
      <taxon>Asfuvirales</taxon>
      <taxon>Asfarviridae</taxon>
      <taxon>Asfivirus</taxon>
      <taxon>African swine fever virus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Ornithodoros</name>
    <name type="common">relapsing fever ticks</name>
    <dbReference type="NCBI Taxonomy" id="6937"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Phacochoerus aethiopicus</name>
    <name type="common">Warthog</name>
    <dbReference type="NCBI Taxonomy" id="85517"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Phacochoerus africanus</name>
    <name type="common">Warthog</name>
    <dbReference type="NCBI Taxonomy" id="41426"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Potamochoerus larvatus</name>
    <name type="common">Bushpig</name>
    <dbReference type="NCBI Taxonomy" id="273792"/>
  </organismHost>
  <organismHost>
    <name type="scientific">Sus scrofa</name>
    <name type="common">Pig</name>
    <dbReference type="NCBI Taxonomy" id="9823"/>
  </organismHost>
  <reference key="1">
    <citation type="submission" date="2003-03" db="EMBL/GenBank/DDBJ databases">
      <title>African swine fever virus genomes.</title>
      <authorList>
        <person name="Kutish G.F."/>
        <person name="Rock D.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">DNA-binding protein that plays a critical role in nucleoid compaction, genome replication and DNA replication and transcription (By similarity). Binds to both ssDNA and dsDNA with a binding site covering about 15 nucleotides (By similarity). Displays DNA-supercoiling activity only when associated with the viral DNA topoisomerase 2 (By similarity).</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="1">Stilbene derivatives SD1 and SD4 disrupt the binding between pA104R and DNA and inhibit the viral replication in primary alveolar macrophages.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Virion</location>
    </subcellularLocation>
    <text evidence="1">Found in association with viral nucleoid.</text>
  </comment>
  <comment type="induction">
    <text evidence="1">Expressed in the late phase of the viral replicative cycle.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">Host antibody response against A104R protein is higher in asymptomatic than in chronically infected hosts.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the bacterial histone-like protein family.</text>
  </comment>
  <dbReference type="EMBL" id="AY261360">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="SMR" id="P0C9E5"/>
  <dbReference type="Proteomes" id="UP000000861">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044423">
    <property type="term" value="C:virion component"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030527">
    <property type="term" value="F:structural constituent of chromatin"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006260">
    <property type="term" value="P:DNA replication"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.520.10">
    <property type="entry name" value="IHF-like DNA-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000119">
    <property type="entry name" value="Hist_DNA-bd"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020816">
    <property type="entry name" value="Histone-like_DNA-bd_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010992">
    <property type="entry name" value="IHF-like_DNA-bd_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33175">
    <property type="entry name" value="DNA-BINDING PROTEIN HU"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33175:SF14">
    <property type="entry name" value="HISTONE-LIKE PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00216">
    <property type="entry name" value="Bac_DNA_binding"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00411">
    <property type="entry name" value="BHL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47729">
    <property type="entry name" value="IHF-like DNA-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00045">
    <property type="entry name" value="HISTONE_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0235">DNA replication</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0426">Late protein</keyword>
  <keyword id="KW-0946">Virion</keyword>
  <feature type="chain" id="PRO_0000373163" description="Viral histone-like protein">
    <location>
      <begin position="1"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="57"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="69"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="72"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="74"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="83"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="85"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="92"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="94"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="96"/>
    </location>
  </feature>
  <feature type="site" description="DNA-binding" evidence="1">
    <location>
      <position position="97"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P68742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="104" mass="11609" checksum="939515B48C793AF8" modified="2009-05-05" version="1">MSTKKKPTITKQELYSLVAADTQLNKALIERIFTSQQKIIQNALKHNQEVIIPPGIKFTVVTVKAKPARQGHNPATGEPIQIKAKPEHKAVKIRALKPVHDMLN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>