<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-08-16" modified="2024-11-27" version="33" xmlns="http://uniprot.org/uniprot">
  <accession>P83289</accession>
  <name>SODM_ARTDA</name>
  <protein>
    <recommendedName>
      <fullName>Superoxide dismutase [Mn/Fe]</fullName>
      <ecNumber evidence="2">1.15.1.1</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Arthrobotrys dactyloides</name>
    <name type="common">Nematode-trapping fungus</name>
    <dbReference type="NCBI Taxonomy" id="74499"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Ascomycota</taxon>
      <taxon>Pezizomycotina</taxon>
      <taxon>Orbiliomycetes</taxon>
      <taxon>Orbiliales</taxon>
      <taxon>Orbiliaceae</taxon>
      <taxon>Drechslerella</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="submission" date="2002-02" db="UniProtKB">
      <authorList>
        <person name="Zhao M."/>
        <person name="Zhang K."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <strain>072</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Destroys superoxide anion radicals which are normally produced within the cells and which are toxic to biological systems.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>2 superoxide + 2 H(+) = H2O2 + O2</text>
      <dbReference type="Rhea" id="RHEA:20696"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:15379"/>
      <dbReference type="ChEBI" id="CHEBI:16240"/>
      <dbReference type="ChEBI" id="CHEBI:18421"/>
      <dbReference type="EC" id="1.15.1.1"/>
    </reaction>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="3">
      <name>Mn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29035"/>
    </cofactor>
    <cofactor evidence="3">
      <name>Fe(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29033"/>
    </cofactor>
    <text evidence="3">Binds 1 Mn(2+) or Fe(2+) ion per subunit.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the iron/manganese superoxide dismutase family.</text>
  </comment>
  <dbReference type="EC" id="1.15.1.1" evidence="2"/>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004784">
    <property type="term" value="F:superoxide dismutase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0049">Antioxidant</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0408">Iron</keyword>
  <keyword id="KW-0464">Manganese</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <feature type="chain" id="PRO_0000159967" description="Superoxide dismutase [Mn/Fe]">
    <location>
      <begin position="1"/>
      <end position="13" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P04179"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P0A0J3"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P80293"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="4"/>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="13" mass="1515" checksum="69949202E642672B" modified="2004-08-16" version="1" fragment="single">AYELPELPYAYDA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>