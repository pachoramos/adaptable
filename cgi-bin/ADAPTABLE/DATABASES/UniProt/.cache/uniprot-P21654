<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1991-05-01" modified="2024-07-24" version="52" xmlns="http://uniprot.org/uniprot">
  <accession>P21654</accession>
  <accession>Q0R4F6</accession>
  <name>MAST_VESBA</name>
  <protein>
    <recommendedName>
      <fullName evidence="7 8 9">Mastoparan-B</fullName>
      <shortName evidence="9">MP-B</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Vespa basalis</name>
    <name type="common">Hornet</name>
    <dbReference type="NCBI Taxonomy" id="7444"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Vespinae</taxon>
      <taxon>Vespa</taxon>
    </lineage>
  </organism>
  <reference evidence="14" key="1">
    <citation type="journal article" date="2007" name="Insect Mol. Biol." volume="16" first="231" last="237">
      <title>Molecular cloning of the precursor polypeptide of mastoparan B and its putative processing enzyme, dipeptidyl peptidase IV, from the black-bellied hornet, Vespa basalis.</title>
      <authorList>
        <person name="Lee V.S."/>
        <person name="Tu W.C."/>
        <person name="Jinn T.R."/>
        <person name="Peng C.C."/>
        <person name="Lin L.J."/>
        <person name="Tzen J.T."/>
      </authorList>
      <dbReference type="PubMed" id="17298553"/>
      <dbReference type="DOI" id="10.1111/j.1365-2583.2006.00718.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2011" name="Peptides" volume="32" first="2027" last="2036">
      <title>Structural and biological characterization of mastoparans in the venom of Vespa species in Taiwan.</title>
      <authorList>
        <person name="Lin C.H."/>
        <person name="Tzen J.T."/>
        <person name="Shyu C.L."/>
        <person name="Yang M.J."/>
        <person name="Tu W.C."/>
      </authorList>
      <dbReference type="PubMed" id="21884742"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2011.08.015"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>PROBABLE AMIDATION AT LEU-59</scope>
    <scope>SYNTHESIS OF 46-59</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1991" name="Biochem. J." volume="274" first="453" last="456">
      <title>Structure and biological activities of a new mastoparan isolated from the venom of the hornet Vespa basalis.</title>
      <authorList>
        <person name="Ho C.-L."/>
        <person name="Hwang L.-L."/>
      </authorList>
      <dbReference type="PubMed" id="2006909"/>
      <dbReference type="DOI" id="10.1042/bj2740453"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 46-59</scope>
    <scope>FUNCTION</scope>
    <scope>AMIDATION AT LEU-59</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2016" name="Molecules" volume="21" first="512" last="512">
      <title>MP-V1 from the venom of social wasp vespula vulgaris is a de novo type of mastoparan that displays superior antimicrobial activities.</title>
      <authorList>
        <person name="Kim Y."/>
        <person name="Son M."/>
        <person name="Noh E.Y."/>
        <person name="Kim S."/>
        <person name="Kim C."/>
        <person name="Yeo J.H."/>
        <person name="Park C."/>
        <person name="Lee K.W."/>
        <person name="Bang W.Y."/>
      </authorList>
      <dbReference type="PubMed" id="27104500"/>
      <dbReference type="DOI" id="10.3390/molecules21040512"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 4 5 6">Antimicrobial and mast cell degranulating peptide. Has broad spectrum antibacterial activity against both Gram-positive (S.aureus MIC=96-128 ug/ml, S.xylosus MIC=2 ug/ml, S.alactolyticus MIC=32 ug/ml, and S.choleraesuis MIC=32 ug/ml) and Gram-negative bacteria (C.koseri MIC=6 ug/ml, E.coli MIC=3-16 ug/ml, K.pneumoniae MIC=128 ug/ml, P.aerugiosa MIC=128 ug/ml, S.typhimurium MIC=64 ug/ml, V.parahamelytics MIC=32 ug/ml, and S.enterica), as well as on fungi (C.albicans, C.glabrata, and C.neoformans) (PubMed:21884742, PubMed:27104500). Does not show antimicrobial activity against S.mutans (PubMed:27104500). Affects membrane permeability of E.coli (PubMed:21884742). Also acts as a mast cell degranulating peptide, that causes liberation of histamine from rat peritoneal mast cells (PubMed:2006909). Its mast cell degranulation activity may be related to the activation of G-protein coupled receptors in mast cells as well as interaction with other proteins located in cell endosomal membranes in the mast cells (By similarity). Whether this peptide shows hemolytic activities is contreversial, as Lin et al., 2011 and Ho et al., 1991 found a hemolytic activity on sheep, chicken and human erythrocytes, whereas Kim et al., 2016 found no hemolytic activity on human erythrocytes (PubMed:2006909, PubMed:21884742, PubMed:27104500). In vivo, induces edema in the rat paw (PubMed:2006909).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4 12">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="10">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="12 13">Assumes an amphipathic alpha-helical conformation in a membrane-like environment.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="11 12">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the MCD family. Mastoparan subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="DQ119291">
    <property type="protein sequence ID" value="AAZ57338.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S14336">
    <property type="entry name" value="S14336"/>
  </dbReference>
  <dbReference type="TCDB" id="1.C.32.1.7">
    <property type="family name" value="the amphipathic peptide mastoparan (mastoparan) family"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043303">
    <property type="term" value="P:mast cell degranulation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0467">Mast cell degranulation</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000458819" evidence="11">
    <location>
      <begin position="28"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000044058" description="Mastoparan-B" evidence="4">
    <location>
      <begin position="46"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 1" evidence="10">
    <location>
      <begin position="27"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 2" evidence="10">
    <location>
      <begin position="31"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 3" evidence="10">
    <location>
      <begin position="35"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 4" evidence="10">
    <location>
      <begin position="41"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="4">
    <location>
      <position position="59"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAZ57338." evidence="10" ref="1">
    <original>N</original>
    <variation>S</variation>
    <location>
      <position position="3"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01514"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P84914"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="2006909"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="21884742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="27104500"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="17298553"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="2006909"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="21884742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="2006909"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="12">
    <source>
      <dbReference type="PubMed" id="21884742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13">
    <source>
      <dbReference type="PubMed" id="27104500"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="14">
    <source>
      <dbReference type="EMBL" id="AAZ57338.1"/>
    </source>
  </evidence>
  <sequence length="60" mass="6374" checksum="A7C6CA5210F6B265" modified="2023-09-13" version="2" precursor="true">MKNTILILFTAFIALLGFFGMSAEALADPLAEPLADPNAEADPEALKLKSIVSWAKKVLG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>