<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-06-27" modified="2024-10-02" version="125" xmlns="http://uniprot.org/uniprot">
  <accession>Q9P2X7</accession>
  <name>DEC1_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Deleted in esophageal cancer 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Candidate tumor suppressor CTS9</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="3" type="primary">DELEC1</name>
    <name type="synonym">CTS9</name>
    <name type="synonym">DEC1</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Genes Chromosomes Cancer" volume="27" first="169" last="176">
      <title>Isolation and mutational analysis of a novel human cDNA, DEC1 (deleted in esophageal cancer 1), derived from the tumor-suppressor locus on chromosome 9q32.</title>
      <authorList>
        <person name="Nishiwaki T."/>
        <person name="Daigo Y."/>
        <person name="Kawasoe T."/>
        <person name="Nakamura Y."/>
      </authorList>
      <dbReference type="PubMed" id="10612805"/>
      <dbReference type="DOI" id="10.1002/(sici)1098-2264(200002)27:2&lt;169::aid-gcc8&gt;3.3.co;2-d"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Nature" volume="429" first="369" last="374">
      <title>DNA sequence and analysis of human chromosome 9.</title>
      <authorList>
        <person name="Humphray S.J."/>
        <person name="Oliver K."/>
        <person name="Hunt A.R."/>
        <person name="Plumb R.W."/>
        <person name="Loveland J.E."/>
        <person name="Howe K.L."/>
        <person name="Andrews T.D."/>
        <person name="Searle S."/>
        <person name="Hunt S.E."/>
        <person name="Scott C.E."/>
        <person name="Jones M.C."/>
        <person name="Ainscough R."/>
        <person name="Almeida J.P."/>
        <person name="Ambrose K.D."/>
        <person name="Ashwell R.I.S."/>
        <person name="Babbage A.K."/>
        <person name="Babbage S."/>
        <person name="Bagguley C.L."/>
        <person name="Bailey J."/>
        <person name="Banerjee R."/>
        <person name="Barker D.J."/>
        <person name="Barlow K.F."/>
        <person name="Bates K."/>
        <person name="Beasley H."/>
        <person name="Beasley O."/>
        <person name="Bird C.P."/>
        <person name="Bray-Allen S."/>
        <person name="Brown A.J."/>
        <person name="Brown J.Y."/>
        <person name="Burford D."/>
        <person name="Burrill W."/>
        <person name="Burton J."/>
        <person name="Carder C."/>
        <person name="Carter N.P."/>
        <person name="Chapman J.C."/>
        <person name="Chen Y."/>
        <person name="Clarke G."/>
        <person name="Clark S.Y."/>
        <person name="Clee C.M."/>
        <person name="Clegg S."/>
        <person name="Collier R.E."/>
        <person name="Corby N."/>
        <person name="Crosier M."/>
        <person name="Cummings A.T."/>
        <person name="Davies J."/>
        <person name="Dhami P."/>
        <person name="Dunn M."/>
        <person name="Dutta I."/>
        <person name="Dyer L.W."/>
        <person name="Earthrowl M.E."/>
        <person name="Faulkner L."/>
        <person name="Fleming C.J."/>
        <person name="Frankish A."/>
        <person name="Frankland J.A."/>
        <person name="French L."/>
        <person name="Fricker D.G."/>
        <person name="Garner P."/>
        <person name="Garnett J."/>
        <person name="Ghori J."/>
        <person name="Gilbert J.G.R."/>
        <person name="Glison C."/>
        <person name="Grafham D.V."/>
        <person name="Gribble S."/>
        <person name="Griffiths C."/>
        <person name="Griffiths-Jones S."/>
        <person name="Grocock R."/>
        <person name="Guy J."/>
        <person name="Hall R.E."/>
        <person name="Hammond S."/>
        <person name="Harley J.L."/>
        <person name="Harrison E.S.I."/>
        <person name="Hart E.A."/>
        <person name="Heath P.D."/>
        <person name="Henderson C.D."/>
        <person name="Hopkins B.L."/>
        <person name="Howard P.J."/>
        <person name="Howden P.J."/>
        <person name="Huckle E."/>
        <person name="Johnson C."/>
        <person name="Johnson D."/>
        <person name="Joy A.A."/>
        <person name="Kay M."/>
        <person name="Keenan S."/>
        <person name="Kershaw J.K."/>
        <person name="Kimberley A.M."/>
        <person name="King A."/>
        <person name="Knights A."/>
        <person name="Laird G.K."/>
        <person name="Langford C."/>
        <person name="Lawlor S."/>
        <person name="Leongamornlert D.A."/>
        <person name="Leversha M."/>
        <person name="Lloyd C."/>
        <person name="Lloyd D.M."/>
        <person name="Lovell J."/>
        <person name="Martin S."/>
        <person name="Mashreghi-Mohammadi M."/>
        <person name="Matthews L."/>
        <person name="McLaren S."/>
        <person name="McLay K.E."/>
        <person name="McMurray A."/>
        <person name="Milne S."/>
        <person name="Nickerson T."/>
        <person name="Nisbett J."/>
        <person name="Nordsiek G."/>
        <person name="Pearce A.V."/>
        <person name="Peck A.I."/>
        <person name="Porter K.M."/>
        <person name="Pandian R."/>
        <person name="Pelan S."/>
        <person name="Phillimore B."/>
        <person name="Povey S."/>
        <person name="Ramsey Y."/>
        <person name="Rand V."/>
        <person name="Scharfe M."/>
        <person name="Sehra H.K."/>
        <person name="Shownkeen R."/>
        <person name="Sims S.K."/>
        <person name="Skuce C.D."/>
        <person name="Smith M."/>
        <person name="Steward C.A."/>
        <person name="Swarbreck D."/>
        <person name="Sycamore N."/>
        <person name="Tester J."/>
        <person name="Thorpe A."/>
        <person name="Tracey A."/>
        <person name="Tromans A."/>
        <person name="Thomas D.W."/>
        <person name="Wall M."/>
        <person name="Wallis J.M."/>
        <person name="West A.P."/>
        <person name="Whitehead S.L."/>
        <person name="Willey D.L."/>
        <person name="Williams S.A."/>
        <person name="Wilming L."/>
        <person name="Wray P.W."/>
        <person name="Young L."/>
        <person name="Ashurst J.L."/>
        <person name="Coulson A."/>
        <person name="Blocker H."/>
        <person name="Durbin R.M."/>
        <person name="Sulston J.E."/>
        <person name="Hubbard T."/>
        <person name="Jackson M.J."/>
        <person name="Bentley D.R."/>
        <person name="Beck S."/>
        <person name="Rogers J."/>
        <person name="Dunham I."/>
      </authorList>
      <dbReference type="PubMed" id="15164053"/>
      <dbReference type="DOI" id="10.1038/nature02465"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Candidate tumor suppressor.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Expressed in many tissues, with highest expression in prostate and testis. Reduced expression in esophageal carcinomas.</text>
  </comment>
  <dbReference type="EMBL" id="AB022761">
    <property type="protein sequence ID" value="BAA96242.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL714001">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_059114.1">
    <property type="nucleotide sequence ID" value="NM_017418.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9P2X7"/>
  <dbReference type="BioGRID" id="119083">
    <property type="interactions" value="36"/>
  </dbReference>
  <dbReference type="IntAct" id="Q9P2X7">
    <property type="interactions" value="2"/>
  </dbReference>
  <dbReference type="BioMuta" id="DEC1"/>
  <dbReference type="PaxDb" id="9606-ENSP00000363128"/>
  <dbReference type="DNASU" id="50514"/>
  <dbReference type="UCSC" id="uc004bjk.1">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:23658"/>
  <dbReference type="GeneCards" id="DELEC1"/>
  <dbReference type="HGNC" id="HGNC:23658">
    <property type="gene designation" value="DELEC1"/>
  </dbReference>
  <dbReference type="MIM" id="604767">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_Q9P2X7"/>
  <dbReference type="eggNOG" id="ENOG502TM3R">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_2757121_0_0_1"/>
  <dbReference type="InParanoid" id="Q9P2X7"/>
  <dbReference type="PhylomeDB" id="Q9P2X7"/>
  <dbReference type="TreeFam" id="TF340655"/>
  <dbReference type="PathwayCommons" id="Q9P2X7"/>
  <dbReference type="SignaLink" id="Q9P2X7"/>
  <dbReference type="BioGRID-ORCS" id="50514">
    <property type="hits" value="13 hits in 1077 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="DEC1">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="GeneWiki" id="DEC1"/>
  <dbReference type="GenomeRNAi" id="50514"/>
  <dbReference type="Pharos" id="Q9P2X7">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q9P2X7"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="RNAct" id="Q9P2X7">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008285">
    <property type="term" value="P:negative regulation of cell population proliferation"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031718">
    <property type="entry name" value="DEC1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF15859">
    <property type="entry name" value="DEC1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0043">Tumor suppressor</keyword>
  <feature type="chain" id="PRO_0000240631" description="Deleted in esophageal cancer 1">
    <location>
      <begin position="1"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_050948" description="In dbSNP:rs2269700.">
    <original>A</original>
    <variation>V</variation>
    <location>
      <position position="60"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10612805"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0000312" key="3">
    <source>
      <dbReference type="HGNC" id="HGNC:23658"/>
    </source>
  </evidence>
  <sequence length="70" mass="7542" checksum="91FA5AD3C402EF43" modified="2000-10-01" version="1">MTMNVLEAGKWKSIVPAPGEGLLAVLHMMVFTDALHRERSVKWQAGVCYNGGKDFAVSLARPKAAEGIAD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>