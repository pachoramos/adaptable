<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-03-15" modified="2024-11-27" version="128" xmlns="http://uniprot.org/uniprot">
  <accession>Q5J5C9</accession>
  <accession>A1L4N1</accession>
  <name>DB121_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 121</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Beta-defensin 21</fullName>
      <shortName>DEFB-21</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin, beta 121</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB121</name>
    <name type="synonym">DEFB21</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Genes Immun." volume="6" first="203" last="210">
      <title>Identification, characterization, and evolution of a primate beta-defensin gene cluster.</title>
      <authorList>
        <person name="Radhakrishnan Y."/>
        <person name="Hamil K.G."/>
        <person name="Yenugu S."/>
        <person name="Young S.L."/>
        <person name="French F.S."/>
        <person name="Hall S.H."/>
      </authorList>
      <dbReference type="PubMed" id="15772680"/>
      <dbReference type="DOI" id="10.1038/sj.gene.6364184"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Testis</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2001" name="Nature" volume="414" first="865" last="871">
      <title>The DNA sequence and comparative analysis of human chromosome 20.</title>
      <authorList>
        <person name="Deloukas P."/>
        <person name="Matthews L.H."/>
        <person name="Ashurst J.L."/>
        <person name="Burton J."/>
        <person name="Gilbert J.G.R."/>
        <person name="Jones M."/>
        <person name="Stavrides G."/>
        <person name="Almeida J.P."/>
        <person name="Babbage A.K."/>
        <person name="Bagguley C.L."/>
        <person name="Bailey J."/>
        <person name="Barlow K.F."/>
        <person name="Bates K.N."/>
        <person name="Beard L.M."/>
        <person name="Beare D.M."/>
        <person name="Beasley O.P."/>
        <person name="Bird C.P."/>
        <person name="Blakey S.E."/>
        <person name="Bridgeman A.M."/>
        <person name="Brown A.J."/>
        <person name="Buck D."/>
        <person name="Burrill W.D."/>
        <person name="Butler A.P."/>
        <person name="Carder C."/>
        <person name="Carter N.P."/>
        <person name="Chapman J.C."/>
        <person name="Clamp M."/>
        <person name="Clark G."/>
        <person name="Clark L.N."/>
        <person name="Clark S.Y."/>
        <person name="Clee C.M."/>
        <person name="Clegg S."/>
        <person name="Cobley V.E."/>
        <person name="Collier R.E."/>
        <person name="Connor R.E."/>
        <person name="Corby N.R."/>
        <person name="Coulson A."/>
        <person name="Coville G.J."/>
        <person name="Deadman R."/>
        <person name="Dhami P.D."/>
        <person name="Dunn M."/>
        <person name="Ellington A.G."/>
        <person name="Frankland J.A."/>
        <person name="Fraser A."/>
        <person name="French L."/>
        <person name="Garner P."/>
        <person name="Grafham D.V."/>
        <person name="Griffiths C."/>
        <person name="Griffiths M.N.D."/>
        <person name="Gwilliam R."/>
        <person name="Hall R.E."/>
        <person name="Hammond S."/>
        <person name="Harley J.L."/>
        <person name="Heath P.D."/>
        <person name="Ho S."/>
        <person name="Holden J.L."/>
        <person name="Howden P.J."/>
        <person name="Huckle E."/>
        <person name="Hunt A.R."/>
        <person name="Hunt S.E."/>
        <person name="Jekosch K."/>
        <person name="Johnson C.M."/>
        <person name="Johnson D."/>
        <person name="Kay M.P."/>
        <person name="Kimberley A.M."/>
        <person name="King A."/>
        <person name="Knights A."/>
        <person name="Laird G.K."/>
        <person name="Lawlor S."/>
        <person name="Lehvaeslaiho M.H."/>
        <person name="Leversha M.A."/>
        <person name="Lloyd C."/>
        <person name="Lloyd D.M."/>
        <person name="Lovell J.D."/>
        <person name="Marsh V.L."/>
        <person name="Martin S.L."/>
        <person name="McConnachie L.J."/>
        <person name="McLay K."/>
        <person name="McMurray A.A."/>
        <person name="Milne S.A."/>
        <person name="Mistry D."/>
        <person name="Moore M.J.F."/>
        <person name="Mullikin J.C."/>
        <person name="Nickerson T."/>
        <person name="Oliver K."/>
        <person name="Parker A."/>
        <person name="Patel R."/>
        <person name="Pearce T.A.V."/>
        <person name="Peck A.I."/>
        <person name="Phillimore B.J.C.T."/>
        <person name="Prathalingam S.R."/>
        <person name="Plumb R.W."/>
        <person name="Ramsay H."/>
        <person name="Rice C.M."/>
        <person name="Ross M.T."/>
        <person name="Scott C.E."/>
        <person name="Sehra H.K."/>
        <person name="Shownkeen R."/>
        <person name="Sims S."/>
        <person name="Skuce C.D."/>
        <person name="Smith M.L."/>
        <person name="Soderlund C."/>
        <person name="Steward C.A."/>
        <person name="Sulston J.E."/>
        <person name="Swann R.M."/>
        <person name="Sycamore N."/>
        <person name="Taylor R."/>
        <person name="Tee L."/>
        <person name="Thomas D.W."/>
        <person name="Thorpe A."/>
        <person name="Tracey A."/>
        <person name="Tromans A.C."/>
        <person name="Vaudin M."/>
        <person name="Wall M."/>
        <person name="Wallis J.M."/>
        <person name="Whitehead S.L."/>
        <person name="Whittaker P."/>
        <person name="Willey D.L."/>
        <person name="Williams L."/>
        <person name="Williams S.A."/>
        <person name="Wilming L."/>
        <person name="Wray P.W."/>
        <person name="Hubbard T."/>
        <person name="Durbin R.M."/>
        <person name="Bentley D.R."/>
        <person name="Beck S."/>
        <person name="Rogers J."/>
      </authorList>
      <dbReference type="PubMed" id="11780052"/>
      <dbReference type="DOI" id="10.1038/414865a"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="4">Has antibacterial activity.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-525714">
      <id>P25942</id>
      <label>CD40</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-11749983">
      <id>Q9UHP7-3</id>
      <label>CLEC2D</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-18304435">
      <id>Q5JX71</id>
      <label>FAM209A</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-17458373">
      <id>P48165</id>
      <label>GJA8</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-19045531">
      <id>Q6UWB1</id>
      <label>IL27RA</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-19944128">
      <id>Q6UX15-2</id>
      <label>LAYN</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-11304917">
      <id>Q8N386</id>
      <label>LRRC25</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-4314784">
      <id>Q96NY8</id>
      <label>NECTIN4</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-17280858">
      <id>Q8WWF3</id>
      <label>SSMEM1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10244198">
      <id>Q5J5C9</id>
    </interactant>
    <interactant intactId="EBI-7131783">
      <id>Q8N205</id>
      <label>SYNE4</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Abundant expression in the male reproductive tract only.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY501000">
    <property type="protein sequence ID" value="AAS87294.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL121751">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC130607">
    <property type="protein sequence ID" value="AAI30608.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC130609">
    <property type="protein sequence ID" value="AAI30610.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS33456.1"/>
  <dbReference type="RefSeq" id="NP_001011878.1">
    <property type="nucleotide sequence ID" value="NM_001011878.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5J5C9"/>
  <dbReference type="SMR" id="Q5J5C9"/>
  <dbReference type="BioGRID" id="128847">
    <property type="interactions" value="32"/>
  </dbReference>
  <dbReference type="IntAct" id="Q5J5C9">
    <property type="interactions" value="19"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000417128"/>
  <dbReference type="iPTMnet" id="Q5J5C9"/>
  <dbReference type="PhosphoSitePlus" id="Q5J5C9"/>
  <dbReference type="BioMuta" id="DEFB121"/>
  <dbReference type="DMDM" id="61212937"/>
  <dbReference type="MassIVE" id="Q5J5C9"/>
  <dbReference type="PaxDb" id="9606-ENSP00000417128"/>
  <dbReference type="PeptideAtlas" id="Q5J5C9"/>
  <dbReference type="ProteomicsDB" id="62979"/>
  <dbReference type="Antibodypedia" id="49719">
    <property type="antibodies" value="66 antibodies from 15 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="245934"/>
  <dbReference type="Ensembl" id="ENST00000376314.3">
    <property type="protein sequence ID" value="ENSP00000417128.1"/>
    <property type="gene ID" value="ENSG00000204548.4"/>
  </dbReference>
  <dbReference type="GeneID" id="245934"/>
  <dbReference type="KEGG" id="hsa:245934"/>
  <dbReference type="MANE-Select" id="ENST00000376314.3">
    <property type="protein sequence ID" value="ENSP00000417128.1"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_001011878.3"/>
    <property type="RefSeq protein sequence ID" value="NP_001011878.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc002wvv.3">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:18101"/>
  <dbReference type="CTD" id="245934"/>
  <dbReference type="DisGeNET" id="245934"/>
  <dbReference type="GeneCards" id="DEFB121"/>
  <dbReference type="HGNC" id="HGNC:18101">
    <property type="gene designation" value="DEFB121"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000204548">
    <property type="expression patterns" value="Tissue enriched (epididymis)"/>
  </dbReference>
  <dbReference type="MIM" id="616075">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_Q5J5C9"/>
  <dbReference type="OpenTargets" id="ENSG00000204548"/>
  <dbReference type="PharmGKB" id="PA38497"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000204548"/>
  <dbReference type="eggNOG" id="ENOG502TDXP">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00730000111628"/>
  <dbReference type="HOGENOM" id="CLU_181906_2_1_1"/>
  <dbReference type="InParanoid" id="Q5J5C9"/>
  <dbReference type="OMA" id="TPAMKCW"/>
  <dbReference type="OrthoDB" id="4736907at2759"/>
  <dbReference type="PhylomeDB" id="Q5J5C9"/>
  <dbReference type="PathwayCommons" id="Q5J5C9"/>
  <dbReference type="Reactome" id="R-HSA-1461957">
    <property type="pathway name" value="Beta defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-1461973">
    <property type="pathway name" value="Defensins"/>
  </dbReference>
  <dbReference type="SignaLink" id="Q5J5C9"/>
  <dbReference type="BioGRID-ORCS" id="245934">
    <property type="hits" value="19 hits in 1110 CRISPR screens"/>
  </dbReference>
  <dbReference type="GenomeRNAi" id="245934"/>
  <dbReference type="Pharos" id="Q5J5C9">
    <property type="development level" value="Tdark"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q5J5C9"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 20"/>
  </dbReference>
  <dbReference type="RNAct" id="Q5J5C9">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000204548">
    <property type="expression patterns" value="Expressed in right testis and 12 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q5J5C9">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050544">
    <property type="entry name" value="Beta-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025933">
    <property type="entry name" value="Beta_defensin_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001:SF8">
    <property type="entry name" value="BETA-DEFENSIN 121"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001">
    <property type="entry name" value="BETA-DEFENSIN 123-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13841">
    <property type="entry name" value="Defensin_beta_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000006991" description="Beta-defensin 121">
    <location>
      <begin position="16"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="23"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="30"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="34"/>
      <end position="51"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15772680"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="76" mass="8456" checksum="1B80ECF2491B2E18" modified="2005-02-15" version="1" precursor="true">MKLLLLLLTVTLLLAQVTPVMKCWGKSGRCRTTCKESEVYYILCKTEAKCCVDPKYVPVKPKLTDTNTSLESTSAV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>