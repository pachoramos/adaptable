<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-12-20" modified="2024-11-27" version="149" xmlns="http://uniprot.org/uniprot">
  <accession>Q9WV95</accession>
  <name>PHLA3_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Pleckstrin homology-like domain family A member 3</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>TDAG51/Ipl homolog 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Phlda3</name>
    <name type="synonym">Tih1</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Mamm. Genome" volume="10" first="1150" last="1159">
      <title>A novel pleckstrin homology-related gene family defined by Ipl/Tssc3, TDAG51, and Tih1: tissue-specific expression, chromosomal location, and parental imprinting.</title>
      <authorList>
        <person name="Frank D."/>
        <person name="Mendelsohn C.L."/>
        <person name="Ciccone E."/>
        <person name="Svensson K."/>
        <person name="Ohlsson R."/>
        <person name="Tycko B."/>
      </authorList>
      <dbReference type="PubMed" id="10594239"/>
      <dbReference type="DOI" id="10.1007/s003359901182"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Science" volume="309" first="1559" last="1563">
      <title>The transcriptional landscape of the mammalian genome.</title>
      <authorList>
        <person name="Carninci P."/>
        <person name="Kasukawa T."/>
        <person name="Katayama S."/>
        <person name="Gough J."/>
        <person name="Frith M.C."/>
        <person name="Maeda N."/>
        <person name="Oyama R."/>
        <person name="Ravasi T."/>
        <person name="Lenhard B."/>
        <person name="Wells C."/>
        <person name="Kodzius R."/>
        <person name="Shimokawa K."/>
        <person name="Bajic V.B."/>
        <person name="Brenner S.E."/>
        <person name="Batalov S."/>
        <person name="Forrest A.R."/>
        <person name="Zavolan M."/>
        <person name="Davis M.J."/>
        <person name="Wilming L.G."/>
        <person name="Aidinis V."/>
        <person name="Allen J.E."/>
        <person name="Ambesi-Impiombato A."/>
        <person name="Apweiler R."/>
        <person name="Aturaliya R.N."/>
        <person name="Bailey T.L."/>
        <person name="Bansal M."/>
        <person name="Baxter L."/>
        <person name="Beisel K.W."/>
        <person name="Bersano T."/>
        <person name="Bono H."/>
        <person name="Chalk A.M."/>
        <person name="Chiu K.P."/>
        <person name="Choudhary V."/>
        <person name="Christoffels A."/>
        <person name="Clutterbuck D.R."/>
        <person name="Crowe M.L."/>
        <person name="Dalla E."/>
        <person name="Dalrymple B.P."/>
        <person name="de Bono B."/>
        <person name="Della Gatta G."/>
        <person name="di Bernardo D."/>
        <person name="Down T."/>
        <person name="Engstrom P."/>
        <person name="Fagiolini M."/>
        <person name="Faulkner G."/>
        <person name="Fletcher C.F."/>
        <person name="Fukushima T."/>
        <person name="Furuno M."/>
        <person name="Futaki S."/>
        <person name="Gariboldi M."/>
        <person name="Georgii-Hemming P."/>
        <person name="Gingeras T.R."/>
        <person name="Gojobori T."/>
        <person name="Green R.E."/>
        <person name="Gustincich S."/>
        <person name="Harbers M."/>
        <person name="Hayashi Y."/>
        <person name="Hensch T.K."/>
        <person name="Hirokawa N."/>
        <person name="Hill D."/>
        <person name="Huminiecki L."/>
        <person name="Iacono M."/>
        <person name="Ikeo K."/>
        <person name="Iwama A."/>
        <person name="Ishikawa T."/>
        <person name="Jakt M."/>
        <person name="Kanapin A."/>
        <person name="Katoh M."/>
        <person name="Kawasawa Y."/>
        <person name="Kelso J."/>
        <person name="Kitamura H."/>
        <person name="Kitano H."/>
        <person name="Kollias G."/>
        <person name="Krishnan S.P."/>
        <person name="Kruger A."/>
        <person name="Kummerfeld S.K."/>
        <person name="Kurochkin I.V."/>
        <person name="Lareau L.F."/>
        <person name="Lazarevic D."/>
        <person name="Lipovich L."/>
        <person name="Liu J."/>
        <person name="Liuni S."/>
        <person name="McWilliam S."/>
        <person name="Madan Babu M."/>
        <person name="Madera M."/>
        <person name="Marchionni L."/>
        <person name="Matsuda H."/>
        <person name="Matsuzawa S."/>
        <person name="Miki H."/>
        <person name="Mignone F."/>
        <person name="Miyake S."/>
        <person name="Morris K."/>
        <person name="Mottagui-Tabar S."/>
        <person name="Mulder N."/>
        <person name="Nakano N."/>
        <person name="Nakauchi H."/>
        <person name="Ng P."/>
        <person name="Nilsson R."/>
        <person name="Nishiguchi S."/>
        <person name="Nishikawa S."/>
        <person name="Nori F."/>
        <person name="Ohara O."/>
        <person name="Okazaki Y."/>
        <person name="Orlando V."/>
        <person name="Pang K.C."/>
        <person name="Pavan W.J."/>
        <person name="Pavesi G."/>
        <person name="Pesole G."/>
        <person name="Petrovsky N."/>
        <person name="Piazza S."/>
        <person name="Reed J."/>
        <person name="Reid J.F."/>
        <person name="Ring B.Z."/>
        <person name="Ringwald M."/>
        <person name="Rost B."/>
        <person name="Ruan Y."/>
        <person name="Salzberg S.L."/>
        <person name="Sandelin A."/>
        <person name="Schneider C."/>
        <person name="Schoenbach C."/>
        <person name="Sekiguchi K."/>
        <person name="Semple C.A."/>
        <person name="Seno S."/>
        <person name="Sessa L."/>
        <person name="Sheng Y."/>
        <person name="Shibata Y."/>
        <person name="Shimada H."/>
        <person name="Shimada K."/>
        <person name="Silva D."/>
        <person name="Sinclair B."/>
        <person name="Sperling S."/>
        <person name="Stupka E."/>
        <person name="Sugiura K."/>
        <person name="Sultana R."/>
        <person name="Takenaka Y."/>
        <person name="Taki K."/>
        <person name="Tammoja K."/>
        <person name="Tan S.L."/>
        <person name="Tang S."/>
        <person name="Taylor M.S."/>
        <person name="Tegner J."/>
        <person name="Teichmann S.A."/>
        <person name="Ueda H.R."/>
        <person name="van Nimwegen E."/>
        <person name="Verardo R."/>
        <person name="Wei C.L."/>
        <person name="Yagi K."/>
        <person name="Yamanishi H."/>
        <person name="Zabarovsky E."/>
        <person name="Zhu S."/>
        <person name="Zimmer A."/>
        <person name="Hide W."/>
        <person name="Bult C."/>
        <person name="Grimmond S.M."/>
        <person name="Teasdale R.D."/>
        <person name="Liu E.T."/>
        <person name="Brusic V."/>
        <person name="Quackenbush J."/>
        <person name="Wahlestedt C."/>
        <person name="Mattick J.S."/>
        <person name="Hume D.A."/>
        <person name="Kai C."/>
        <person name="Sasaki D."/>
        <person name="Tomaru Y."/>
        <person name="Fukuda S."/>
        <person name="Kanamori-Katayama M."/>
        <person name="Suzuki M."/>
        <person name="Aoki J."/>
        <person name="Arakawa T."/>
        <person name="Iida J."/>
        <person name="Imamura K."/>
        <person name="Itoh M."/>
        <person name="Kato T."/>
        <person name="Kawaji H."/>
        <person name="Kawagashira N."/>
        <person name="Kawashima T."/>
        <person name="Kojima M."/>
        <person name="Kondo S."/>
        <person name="Konno H."/>
        <person name="Nakano K."/>
        <person name="Ninomiya N."/>
        <person name="Nishio T."/>
        <person name="Okada M."/>
        <person name="Plessy C."/>
        <person name="Shibata K."/>
        <person name="Shiraki T."/>
        <person name="Suzuki S."/>
        <person name="Tagami M."/>
        <person name="Waki K."/>
        <person name="Watahiki A."/>
        <person name="Okamura-Oho Y."/>
        <person name="Suzuki H."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
      </authorList>
      <dbReference type="PubMed" id="16141072"/>
      <dbReference type="DOI" id="10.1126/science.1112014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>FVB/N</strain>
      <tissue>Mammary tumor</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2002" name="J. Biol. Chem." volume="277" first="49935" last="49944">
      <title>Phosphoinositide binding by the pleckstrin homology domains of Ipl and Tih1.</title>
      <authorList>
        <person name="Saxena A."/>
        <person name="Morozov P."/>
        <person name="Frank D."/>
        <person name="Musalo R."/>
        <person name="Lemmon M.A."/>
        <person name="Skolnik E.Y."/>
        <person name="Tycko B."/>
      </authorList>
      <dbReference type="PubMed" id="12374806"/>
      <dbReference type="DOI" id="10.1074/jbc.m206497200"/>
    </citation>
    <scope>PHOSPHOINOSITIDE-BINDING</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MUTAGENESIS OF LYS-27 AND ARG-28</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2002" name="Proc. Natl. Acad. Sci. U.S.A." volume="99" first="7490" last="7495">
      <title>Placental overgrowth in mice lacking the imprinted gene Ipl.</title>
      <authorList>
        <person name="Frank D."/>
        <person name="Fortino W."/>
        <person name="Clark L."/>
        <person name="Musalo R."/>
        <person name="Wang W."/>
        <person name="Saxena A."/>
        <person name="Li C.M."/>
        <person name="Reik W."/>
        <person name="Ludwig T."/>
        <person name="Tycko B."/>
      </authorList>
      <dbReference type="PubMed" id="12032310"/>
      <dbReference type="DOI" id="10.1073/pnas.122039999"/>
    </citation>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2010" name="Cell" volume="143" first="1174" last="1189">
      <title>A tissue-specific atlas of mouse protein phosphorylation and expression.</title>
      <authorList>
        <person name="Huttlin E.L."/>
        <person name="Jedrychowski M.P."/>
        <person name="Elias J.E."/>
        <person name="Goswami T."/>
        <person name="Rad R."/>
        <person name="Beausoleil S.A."/>
        <person name="Villen J."/>
        <person name="Haas W."/>
        <person name="Sowa M.E."/>
        <person name="Gygi S.P."/>
      </authorList>
      <dbReference type="PubMed" id="21183079"/>
      <dbReference type="DOI" id="10.1016/j.cell.2010.12.001"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Brain</tissue>
      <tissue>Lung</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">p53/TP53-regulated repressor of Akt/AKT1 signaling. Represses AKT1 by preventing AKT1-binding to membrane lipids, thereby inhibiting AKT1 translocation to the cellular membrane and activation. Contributes to p53/TP53-dependent apoptosis by repressing AKT1 activity. Its direct transcription regulation by p53/TP53 may explain how p53/TP53 can negatively regulate AKT1. May act as a tumor suppressor (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="4">Membrane</location>
      <topology evidence="4">Peripheral membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Widely expressed in fetal tissues, with the exception of liver. Strongly expressed in adult skeletal muscle and lung. Widely expressed at lower levels in other adult tissues, with weakest expression in liver and spleen.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="2">Expressed in extraembryonic tissues and placenta at 11.5 and 18.5 dpc (at protein level). Expression continues throughout gestation and is strong in adult lung (at protein level).</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The PH domain binds phosphoinositides with a broad specificity. It competes with the PH domain of AKT1 and directly interferes with AKT1 binding to phosphatidylinositol 4,5-bisphosphate (PIP2) and phosphatidylinositol 3,4,5-trisphosphate (PIP3), preventing AKT1 association to membrane lipids and subsequent activation of AKT1 signaling (By similarity).</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="3">Mice are viable and fertile, and show normal placenta and embryonic weights.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the PHLDA3 family.</text>
  </comment>
  <dbReference type="EMBL" id="AF151099">
    <property type="protein sequence ID" value="AAD42080.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK003767">
    <property type="protein sequence ID" value="BAB22985.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC023408">
    <property type="protein sequence ID" value="AAH23408.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS35722.1"/>
  <dbReference type="RefSeq" id="NP_038778.1">
    <property type="nucleotide sequence ID" value="NM_013750.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9WV95"/>
  <dbReference type="SMR" id="Q9WV95"/>
  <dbReference type="IntAct" id="Q9WV95">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="MINT" id="Q9WV95"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000040614"/>
  <dbReference type="iPTMnet" id="Q9WV95"/>
  <dbReference type="PhosphoSitePlus" id="Q9WV95"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000040614"/>
  <dbReference type="PeptideAtlas" id="Q9WV95"/>
  <dbReference type="ProteomicsDB" id="287933"/>
  <dbReference type="Pumba" id="Q9WV95"/>
  <dbReference type="Antibodypedia" id="34503">
    <property type="antibodies" value="157 antibodies from 30 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="27280"/>
  <dbReference type="Ensembl" id="ENSMUST00000038945.6">
    <property type="protein sequence ID" value="ENSMUSP00000040614.6"/>
    <property type="gene ID" value="ENSMUSG00000041801.6"/>
  </dbReference>
  <dbReference type="GeneID" id="27280"/>
  <dbReference type="KEGG" id="mmu:27280"/>
  <dbReference type="UCSC" id="uc007ctq.2">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:1351485"/>
  <dbReference type="CTD" id="23612"/>
  <dbReference type="MGI" id="MGI:1351485">
    <property type="gene designation" value="Phlda3"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000041801"/>
  <dbReference type="eggNOG" id="ENOG502S2UN">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00440000039564"/>
  <dbReference type="HOGENOM" id="CLU_062639_1_0_1"/>
  <dbReference type="InParanoid" id="Q9WV95"/>
  <dbReference type="OMA" id="FKNQQAM"/>
  <dbReference type="OrthoDB" id="3911764at2759"/>
  <dbReference type="PhylomeDB" id="Q9WV95"/>
  <dbReference type="TreeFam" id="TF332320"/>
  <dbReference type="BioGRID-ORCS" id="27280">
    <property type="hits" value="0 hits in 77 CRISPR screens"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q9WV95"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="RNAct" id="Q9WV95">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000041801">
    <property type="expression patterns" value="Expressed in temporalis muscle and 233 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005547">
    <property type="term" value="F:phosphatidylinositol-3,4,5-trisphosphate binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043325">
    <property type="term" value="F:phosphatidylinositol-3,4-bisphosphate binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0080025">
    <property type="term" value="F:phosphatidylinositol-3,5-bisphosphate binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032266">
    <property type="term" value="F:phosphatidylinositol-3-phosphate binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005546">
    <property type="term" value="F:phosphatidylinositol-4,5-bisphosphate binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010314">
    <property type="term" value="F:phosphatidylinositol-5-phosphate binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042771">
    <property type="term" value="P:intrinsic apoptotic signaling pathway in response to DNA damage by p53 class mediator"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051898">
    <property type="term" value="P:negative regulation of phosphatidylinositol 3-kinase/protein kinase B signal transduction"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043065">
    <property type="term" value="P:positive regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00821">
    <property type="entry name" value="PH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.30.29.30:FF:000270">
    <property type="entry name" value="Pleckstrin homology-like domain family A member 3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.30.29.30">
    <property type="entry name" value="Pleckstrin-homology domain (PH domain)/Phosphotyrosine-binding domain (PTB)"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011993">
    <property type="entry name" value="PH-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001849">
    <property type="entry name" value="PH_domain"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR042832">
    <property type="entry name" value="PHLA1/2/3"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15478:SF5">
    <property type="entry name" value="PLECKSTRIN HOMOLOGY-LIKE DOMAIN FAMILY A MEMBER 3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15478">
    <property type="entry name" value="PLECKSTRIN HOMOLOGY-LIKE DOMAIN, PQ-RICH PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00169">
    <property type="entry name" value="PH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00233">
    <property type="entry name" value="PH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50729">
    <property type="entry name" value="PH domain-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0043">Tumor suppressor</keyword>
  <feature type="chain" id="PRO_0000053903" description="Pleckstrin homology-like domain family A member 3">
    <location>
      <begin position="1"/>
      <end position="125"/>
    </location>
  </feature>
  <feature type="domain" description="PH">
    <location>
      <begin position="6"/>
      <end position="125"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Impairs the ability to resucue growth in cdc25ts mutant yeasts." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="27"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Impairs the ability to resucue growth in cdc25ts mutant yeasts." evidence="4">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10594239"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12032310"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="12374806"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="125" mass="13719" checksum="53E44E6A29C036A4" modified="1999-11-01" version="1">MTAAATVLKEGVLEKRSGGLLQLWKRKRCVLTERGLQLFEAKGTGGRPKELSFARIKAVECVESTGRHIYFTLVTEGGGEIDFRCPLEDPGWNAQITLGLVKFKNQQAIQTVRARQSLGTGTLVS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>