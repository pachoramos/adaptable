<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-05-10" modified="2022-05-25" version="46" xmlns="http://uniprot.org/uniprot">
  <accession>P61233</accession>
  <name>TXR1_MACRV</name>
  <protein>
    <recommendedName>
      <fullName>U5-hexatoxin-Mr1a</fullName>
      <shortName>U5-HXTX-Mr1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Raventoxin I</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Raventoxin-1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Macrothele raveni</name>
    <name type="common">Funnel-web spider</name>
    <dbReference type="NCBI Taxonomy" id="269627"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Mygalomorphae</taxon>
      <taxon>Macrothelidae</taxon>
      <taxon>Macrothele</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Toxicon" volume="41" first="651" last="656">
      <title>Purification and characterization of raventoxin-I and raventoxin-III, two neurotoxic peptides from the venom of the spider Macrothele raveni.</title>
      <authorList>
        <person name="Zeng X.-Z."/>
        <person name="Xiao Q.-B."/>
        <person name="Liang S.-P."/>
      </authorList>
      <dbReference type="PubMed" id="12727269"/>
      <dbReference type="DOI" id="10.1016/s0041-0101(02)00361-6"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>CHARACTERIZATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">This toxin blocks the neuromuscular transmission, and also acts on muscle. It exerts an effect of first exciting and then inhibiting the contraction of muscle. This toxin is active only against mammals.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="3">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text>Contains 4 disulfide bonds.</text>
  </comment>
  <comment type="mass spectrometry" mass="4841.11" method="MALDI" evidence="2"/>
  <comment type="toxic dose">
    <text evidence="2">LD(50) is 0.772 mg/kg by intraabdominal injection into mice.</text>
  </comment>
  <comment type="similarity">
    <text>Belongs to the neurotoxin 35 family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P61233"/>
  <dbReference type="SMR" id="P61233"/>
  <dbReference type="ArachnoServer" id="AS000416">
    <property type="toxin name" value="U5-hexatoxin-Mr1a"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000087680" description="U5-hexatoxin-Mr1a">
    <location>
      <begin position="1"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="8"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="15"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="17"/>
      <end position="43"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12727269"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="43" mass="4849" checksum="99DCF87DDFB88441" modified="2004-05-10" version="1">CGTNRAWCRNAKDHCCCGYSCVKPIWASKPEDDGYCWKKFGGC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>