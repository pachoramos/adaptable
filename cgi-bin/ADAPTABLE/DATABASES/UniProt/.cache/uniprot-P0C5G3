<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-10-02" modified="2023-02-22" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>P0C5G3</accession>
  <name>NA16_ANTXA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Delta-actitoxin-Axm1h</fullName>
      <shortName evidence="3">Delta-AITX-Axm1h</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">PCR3-7</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="5">Toxin PCR6</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Anthopleura xanthogrammica</name>
    <name type="common">Giant green sea anemone</name>
    <name type="synonym">Actinia xanthogrammica</name>
    <dbReference type="NCBI Taxonomy" id="6112"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Anthopleura</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Toxicon" volume="36" first="41" last="51">
      <title>Identification and characterization of novel sodium channel toxins from the sea anemone Anthopleura xanthogrammica.</title>
      <authorList>
        <person name="Kelso G.J."/>
        <person name="Blumenthal K.M."/>
      </authorList>
      <dbReference type="PubMed" id="9604281"/>
      <dbReference type="DOI" id="10.1016/s0041-0101(97)00064-0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Tentacle</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Binds specifically to voltage-gated sodium channels (Nav) (site 3), thereby delaying their inactivation during signal transduction. Thus it may strongly stimulate mammalian cardiac muscle contraction.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="5">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the sea anemone sodium channel inhibitory toxin family. Type I subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0C5G3"/>
  <dbReference type="SMR" id="P0C5G3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009966">
    <property type="term" value="P:regulation of signal transduction"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000693">
    <property type="entry name" value="Anenome_toxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00706">
    <property type="entry name" value="Toxin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001905">
    <property type="entry name" value="Anenome_toxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0123">Cardiotoxin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000305117" description="Delta-actitoxin-Axm1h">
    <location>
      <begin position="1"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="6"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="29"/>
      <end position="47"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01530"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="9604281"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="9604281"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="49" mass="5184" checksum="A486A717959E59CC" modified="2007-10-02" version="1">GVSCLCDSDGPSVRGNTLSGILWFYPSGCPSGWHNCKAHGPTIGWCCKQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>