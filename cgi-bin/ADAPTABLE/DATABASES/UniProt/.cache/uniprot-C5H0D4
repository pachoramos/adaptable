<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-06-17" modified="2022-12-14" version="34" xmlns="http://uniprot.org/uniprot">
  <accession>C5H0D4</accession>
  <name>E2ALA_AMOLO</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Esculentin-2-ALa</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6">Amolopin-9a</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Amolops loloensis</name>
    <name type="common">Lolokou Sucker Frog</name>
    <name type="synonym">Staurois loloensis</name>
    <dbReference type="NCBI Taxonomy" id="318551"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Amolops</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2010" name="Comp. Biochem. Physiol." volume="155" first="72" last="76">
      <title>Five novel antimicrobial peptides from skin secretions of the frog, Amolops loloensis.</title>
      <authorList>
        <person name="Wang M."/>
        <person name="Wang Y."/>
        <person name="Wang A."/>
        <person name="Song Y."/>
        <person name="Ma D."/>
        <person name="Yang H."/>
        <person name="Ma Y."/>
        <person name="Lai R."/>
      </authorList>
      <dbReference type="PubMed" id="19843479"/>
      <dbReference type="DOI" id="10.1016/j.cbpb.2009.10.003"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 40-76</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin</tissue>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Antimicrobial peptide with activity against Gram-positive and Gram-negative bacteria and against fungi (PubMed:19843479). Has been tested against S.aureus (MIC=2.5 ug/mL), B.pumilus (MIC=2.5 ug/mL), B.cereus (MIC=7.5 ug/mL), E.coli (MIC=12.5 ug/mL), B.dysenteriae (MIC=7.5 ug/mL), A.cacoaceticus (MIC=25.0 ug/mL), P.aeruginosa (MIC=50.0 ug/mL) and C.albicans (MIC=2.5 ug/mL) (PubMed:19843479). Also shows a weak hemolytic activity (PubMed:19843479).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3866.42" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Esculentin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=01929"/>
  </comment>
  <dbReference type="EMBL" id="EU311547">
    <property type="protein sequence ID" value="ACA09637.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="C5H0D4"/>
  <dbReference type="SMR" id="C5H0D4"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000450001" evidence="5">
    <location>
      <begin position="23"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5002950813" description="Esculentin-2-ALa" evidence="2">
    <location>
      <begin position="40"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="70"/>
      <end position="76"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19843479"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="19843479"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="19843479"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="ACA09637.1"/>
    </source>
  </evidence>
  <sequence length="76" mass="8390" checksum="9AFAB0DB756DDDCB" modified="2009-07-28" version="1" precursor="true">MFTLKKSMLLLFFLGTISLSLCEEERNADEDDGEKEVKRGIFALIKTAAKFVGKNLLKQAGKAGLEHLACKANNQC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>