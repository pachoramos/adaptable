<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-09-11" modified="2024-11-27" version="122" xmlns="http://uniprot.org/uniprot">
  <accession>Q0D840</accession>
  <accession>A0A0P0X3K2</accession>
  <accession>B7E3H0</accession>
  <accession>O04743</accession>
  <accession>Q42443</accession>
  <accession>Q7EZX7</accession>
  <name>TRXH1_ORYSJ</name>
  <protein>
    <recommendedName>
      <fullName>Thioredoxin H1</fullName>
      <shortName>OsTrxh1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Phloem sap 13 kDa protein 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">TRXH</name>
    <name type="synonym">RPP13-1</name>
    <name type="ordered locus">Os07g0186000</name>
    <name type="ordered locus">LOC_Os07g08840</name>
    <name type="ORF">OJ1339_B08.1</name>
    <name type="ORF">OsJ_022432</name>
    <name type="ORF">P0506C07.2</name>
  </gene>
  <organism>
    <name type="scientific">Oryza sativa subsp. japonica</name>
    <name type="common">Rice</name>
    <dbReference type="NCBI Taxonomy" id="39947"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>Liliopsida</taxon>
      <taxon>Poales</taxon>
      <taxon>Poaceae</taxon>
      <taxon>BOP clade</taxon>
      <taxon>Oryzoideae</taxon>
      <taxon>Oryzeae</taxon>
      <taxon>Oryzinae</taxon>
      <taxon>Oryza</taxon>
      <taxon>Oryza sativa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Planta" volume="195" first="456" last="463">
      <title>Thioredoxin h is one of the major proteins in rice phloem sap.</title>
      <authorList>
        <person name="Ishiwatari Y."/>
        <person name="Honda C."/>
        <person name="Kawashima I."/>
        <person name="Nakamura S."/>
        <person name="Hirano H."/>
        <person name="Mori S."/>
        <person name="Fujiwara T."/>
        <person name="Hayashi H."/>
        <person name="Chino M."/>
      </authorList>
      <dbReference type="PubMed" id="7766047"/>
      <dbReference type="DOI" id="10.1007/bf00202605"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <strain>cv. Nipponbare</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Nature" volume="436" first="793" last="800">
      <title>The map-based sequence of the rice genome.</title>
      <authorList>
        <consortium name="International rice genome sequencing project (IRGSP)"/>
      </authorList>
      <dbReference type="PubMed" id="16100779"/>
      <dbReference type="DOI" id="10.1038/nature03895"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Nipponbare</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2008" name="Nucleic Acids Res." volume="36" first="D1028" last="D1033">
      <title>The rice annotation project database (RAP-DB): 2008 update.</title>
      <authorList>
        <consortium name="The rice annotation project (RAP)"/>
      </authorList>
      <dbReference type="PubMed" id="18089549"/>
      <dbReference type="DOI" id="10.1093/nar/gkm978"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Nipponbare</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2013" name="Rice" volume="6" first="4" last="4">
      <title>Improvement of the Oryza sativa Nipponbare reference genome using next generation sequence and optical map data.</title>
      <authorList>
        <person name="Kawahara Y."/>
        <person name="de la Bastide M."/>
        <person name="Hamilton J.P."/>
        <person name="Kanamori H."/>
        <person name="McCombie W.R."/>
        <person name="Ouyang S."/>
        <person name="Schwartz D.C."/>
        <person name="Tanaka T."/>
        <person name="Wu J."/>
        <person name="Zhou S."/>
        <person name="Childs K.L."/>
        <person name="Davidson R.M."/>
        <person name="Lin H."/>
        <person name="Quesada-Ocampo L."/>
        <person name="Vaillancourt B."/>
        <person name="Sakai H."/>
        <person name="Lee S.S."/>
        <person name="Kim J."/>
        <person name="Numa H."/>
        <person name="Itoh T."/>
        <person name="Buell C.R."/>
        <person name="Matsumoto T."/>
      </authorList>
      <dbReference type="PubMed" id="24280374"/>
      <dbReference type="DOI" id="10.1186/1939-8433-6-4"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Nipponbare</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2005" name="PLoS Biol." volume="3" first="266" last="281">
      <title>The genomes of Oryza sativa: a history of duplications.</title>
      <authorList>
        <person name="Yu J."/>
        <person name="Wang J."/>
        <person name="Lin W."/>
        <person name="Li S."/>
        <person name="Li H."/>
        <person name="Zhou J."/>
        <person name="Ni P."/>
        <person name="Dong W."/>
        <person name="Hu S."/>
        <person name="Zeng C."/>
        <person name="Zhang J."/>
        <person name="Zhang Y."/>
        <person name="Li R."/>
        <person name="Xu Z."/>
        <person name="Li S."/>
        <person name="Li X."/>
        <person name="Zheng H."/>
        <person name="Cong L."/>
        <person name="Lin L."/>
        <person name="Yin J."/>
        <person name="Geng J."/>
        <person name="Li G."/>
        <person name="Shi J."/>
        <person name="Liu J."/>
        <person name="Lv H."/>
        <person name="Li J."/>
        <person name="Wang J."/>
        <person name="Deng Y."/>
        <person name="Ran L."/>
        <person name="Shi X."/>
        <person name="Wang X."/>
        <person name="Wu Q."/>
        <person name="Li C."/>
        <person name="Ren X."/>
        <person name="Wang J."/>
        <person name="Wang X."/>
        <person name="Li D."/>
        <person name="Liu D."/>
        <person name="Zhang X."/>
        <person name="Ji Z."/>
        <person name="Zhao W."/>
        <person name="Sun Y."/>
        <person name="Zhang Z."/>
        <person name="Bao J."/>
        <person name="Han Y."/>
        <person name="Dong L."/>
        <person name="Ji J."/>
        <person name="Chen P."/>
        <person name="Wu S."/>
        <person name="Liu J."/>
        <person name="Xiao Y."/>
        <person name="Bu D."/>
        <person name="Tan J."/>
        <person name="Yang L."/>
        <person name="Ye C."/>
        <person name="Zhang J."/>
        <person name="Xu J."/>
        <person name="Zhou Y."/>
        <person name="Yu Y."/>
        <person name="Zhang B."/>
        <person name="Zhuang S."/>
        <person name="Wei H."/>
        <person name="Liu B."/>
        <person name="Lei M."/>
        <person name="Yu H."/>
        <person name="Li Y."/>
        <person name="Xu H."/>
        <person name="Wei S."/>
        <person name="He X."/>
        <person name="Fang L."/>
        <person name="Zhang Z."/>
        <person name="Zhang Y."/>
        <person name="Huang X."/>
        <person name="Su Z."/>
        <person name="Tong W."/>
        <person name="Li J."/>
        <person name="Tong Z."/>
        <person name="Li S."/>
        <person name="Ye J."/>
        <person name="Wang L."/>
        <person name="Fang L."/>
        <person name="Lei T."/>
        <person name="Chen C.-S."/>
        <person name="Chen H.-C."/>
        <person name="Xu Z."/>
        <person name="Li H."/>
        <person name="Huang H."/>
        <person name="Zhang F."/>
        <person name="Xu H."/>
        <person name="Li N."/>
        <person name="Zhao C."/>
        <person name="Li S."/>
        <person name="Dong L."/>
        <person name="Huang Y."/>
        <person name="Li L."/>
        <person name="Xi Y."/>
        <person name="Qi Q."/>
        <person name="Li W."/>
        <person name="Zhang B."/>
        <person name="Hu W."/>
        <person name="Zhang Y."/>
        <person name="Tian X."/>
        <person name="Jiao Y."/>
        <person name="Liang X."/>
        <person name="Jin J."/>
        <person name="Gao L."/>
        <person name="Zheng W."/>
        <person name="Hao B."/>
        <person name="Liu S.-M."/>
        <person name="Wang W."/>
        <person name="Yuan L."/>
        <person name="Cao M."/>
        <person name="McDermott J."/>
        <person name="Samudrala R."/>
        <person name="Wang J."/>
        <person name="Wong G.K.-S."/>
        <person name="Yang H."/>
      </authorList>
      <dbReference type="PubMed" id="15685292"/>
      <dbReference type="DOI" id="10.1371/journal.pbio.0030038"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Nipponbare</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2003" name="Science" volume="301" first="376" last="379">
      <title>Collection, mapping, and annotation of over 28,000 cDNA clones from japonica rice.</title>
      <authorList>
        <consortium name="The rice full-length cDNA consortium"/>
      </authorList>
      <dbReference type="PubMed" id="12869764"/>
      <dbReference type="DOI" id="10.1126/science.1081288"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Nipponbare</strain>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1998" name="Planta" volume="205" first="12" last="22">
      <title>Rice phloem thioredoxin h has the capacity to mediate its own cell-to-cell transport through plasmodesmata.</title>
      <authorList>
        <person name="Ishiwatari Y."/>
        <person name="Fujiwara T."/>
        <person name="McFarland K.C."/>
        <person name="Nemoto K."/>
        <person name="Hayashi H."/>
        <person name="Chino M."/>
        <person name="Lucas W.J."/>
      </authorList>
      <dbReference type="PubMed" id="9599802"/>
      <dbReference type="DOI" id="10.1007/s004250050291"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MUTAGENESIS OF 1-MET--GLU-5; 4-GLU-GLU-5; 14-LYS--GLU-16; 25-LYS-GLU-26; 55-LYS-LYS-56; 71-LYS-GLU-72; 95-ASP-LYS-96; 101-ARG--ASP-104 AND 116-THR--ALA-122</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="137" first="317" last="327">
      <title>A novel cis-element that is responsive to oxidative stress regulates three antioxidant defense genes in rice.</title>
      <authorList>
        <person name="Tsukamoto S."/>
        <person name="Morita S."/>
        <person name="Hirano E."/>
        <person name="Yokoi H."/>
        <person name="Masumura T."/>
        <person name="Tanaka K."/>
      </authorList>
      <dbReference type="PubMed" id="15618434"/>
      <dbReference type="DOI" id="10.1104/pp.104.045658"/>
    </citation>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2009" name="FEBS Lett." volume="583" first="2734" last="2738">
      <title>A cold-induced thioredoxin h of rice, OsTrx23, negatively regulates kinase activities of OsMPK3 and OsMPK6 in vitro.</title>
      <authorList>
        <person name="Xie G."/>
        <person name="Kato H."/>
        <person name="Sasaki K."/>
        <person name="Imai R."/>
      </authorList>
      <dbReference type="PubMed" id="19665023"/>
      <dbReference type="DOI" id="10.1016/j.febslet.2009.07.057"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2009" name="Mol. Plant" volume="2" first="308" last="322">
      <title>Comparative genomic study of the thioredoxin family in photosynthetic organisms with emphasis on Populus trichocarpa.</title>
      <authorList>
        <person name="Chibani K."/>
        <person name="Wingsle G."/>
        <person name="Jacquot J.P."/>
        <person name="Gelhaye E."/>
        <person name="Rouhier N."/>
      </authorList>
      <dbReference type="PubMed" id="19825616"/>
      <dbReference type="DOI" id="10.1093/mp/ssn076"/>
    </citation>
    <scope>GENE FAMILY</scope>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="11">
    <citation type="submission" date="2004-07" db="PDB data bank">
      <title>Solution structure of thioredoxin type h from Oryza sativa.</title>
      <authorList>
        <person name="Kumeta H."/>
        <person name="Ogura H."/>
        <person name="Akagi K."/>
        <person name="Katoh E."/>
        <person name="Inagaki F."/>
      </authorList>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <comment type="function">
    <text evidence="4 5 6">Thiol-disulfide oxidoreductase involved in the redox regulation of MAP kinases. Under reducing conditions, inhibits MPK1 and MPK5 kinase activities. Mediates its own transport from cell-to-cell through plasmodesmata. Possesses insulin disulfide bonds reducing activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5 6">Expressed in the phloem companion cells of leaf sheaths and stems.</text>
  </comment>
  <comment type="induction">
    <text evidence="3 4">By methyl viologen and chilling in roots and shoots.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the thioredoxin family. Plant H-type subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="D26547">
    <property type="protein sequence ID" value="BAA05546.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D21836">
    <property type="protein sequence ID" value="BAA04864.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AP003753">
    <property type="protein sequence ID" value="BAD30186.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AP004384">
    <property type="protein sequence ID" value="BAC79928.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AP008213">
    <property type="protein sequence ID" value="BAF20983.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AP014963">
    <property type="protein sequence ID" value="BAT00373.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CM000144">
    <property type="protein sequence ID" value="EAZ38949.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK059196">
    <property type="protein sequence ID" value="BAG86917.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK121423">
    <property type="protein sequence ID" value="BAH00481.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="T04090">
    <property type="entry name" value="T04090"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_015647572.1">
    <property type="nucleotide sequence ID" value="XM_015792086.1"/>
  </dbReference>
  <dbReference type="PDB" id="1WMJ">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-122"/>
  </dbReference>
  <dbReference type="PDBsum" id="1WMJ"/>
  <dbReference type="AlphaFoldDB" id="Q0D840"/>
  <dbReference type="SMR" id="Q0D840"/>
  <dbReference type="STRING" id="39947.Q0D840"/>
  <dbReference type="CarbonylDB" id="Q0D840"/>
  <dbReference type="PaxDb" id="39947-Q0D840"/>
  <dbReference type="EnsemblPlants" id="Os07t0186000-02">
    <property type="protein sequence ID" value="Os07t0186000-02"/>
    <property type="gene ID" value="Os07g0186000"/>
  </dbReference>
  <dbReference type="Gramene" id="Os07t0186000-02">
    <property type="protein sequence ID" value="Os07t0186000-02"/>
    <property type="gene ID" value="Os07g0186000"/>
  </dbReference>
  <dbReference type="eggNOG" id="KOG0907">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_090389_14_1_1"/>
  <dbReference type="InParanoid" id="Q0D840"/>
  <dbReference type="OMA" id="QVGVAPK"/>
  <dbReference type="OrthoDB" id="1215770at2759"/>
  <dbReference type="EvolutionaryTrace" id="Q0D840"/>
  <dbReference type="Proteomes" id="UP000000763">
    <property type="component" value="Chromosome 7"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000007752">
    <property type="component" value="Chromosome 7"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000059680">
    <property type="component" value="Chromosome 7"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004857">
    <property type="term" value="F:enzyme inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016671">
    <property type="term" value="F:oxidoreductase activity, acting on a sulfur group of donors, disulfide as acceptor"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015035">
    <property type="term" value="F:protein-disulfide reductase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043086">
    <property type="term" value="P:negative regulation of catalytic activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010497">
    <property type="term" value="P:plasmodesmata-mediated intercellular transport"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009409">
    <property type="term" value="P:response to cold"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006979">
    <property type="term" value="P:response to oxidative stress"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd02947">
    <property type="entry name" value="TRX_family"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.40.30.10:FF:000104">
    <property type="entry name" value="Thioredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.30.10">
    <property type="entry name" value="Glutaredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005746">
    <property type="entry name" value="Thioredoxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050620">
    <property type="entry name" value="Thioredoxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036249">
    <property type="entry name" value="Thioredoxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR017937">
    <property type="entry name" value="Thioredoxin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013766">
    <property type="entry name" value="Thioredoxin_domain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10438">
    <property type="entry name" value="THIOREDOXIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10438:SF425">
    <property type="entry name" value="THIOREDOXIN H1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00085">
    <property type="entry name" value="Thioredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF000077">
    <property type="entry name" value="Thioredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00421">
    <property type="entry name" value="THIOREDOXIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF52833">
    <property type="entry name" value="Thioredoxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00194">
    <property type="entry name" value="THIOREDOXIN_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51352">
    <property type="entry name" value="THIOREDOXIN_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0249">Electron transport</keyword>
  <keyword id="KW-0676">Redox-active center</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000120057" description="Thioredoxin H1">
    <location>
      <begin position="1"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="domain" description="Thioredoxin" evidence="2">
    <location>
      <begin position="2"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="active site" description="Nucleophile" evidence="1">
    <location>
      <position position="40"/>
    </location>
  </feature>
  <feature type="active site" description="Nucleophile" evidence="1">
    <location>
      <position position="43"/>
    </location>
  </feature>
  <feature type="site" description="Deprotonates C-terminal active site Cys" evidence="1">
    <location>
      <position position="34"/>
    </location>
  </feature>
  <feature type="site" description="Contributes to redox potential value" evidence="1">
    <location>
      <position position="41"/>
    </location>
  </feature>
  <feature type="site" description="Contributes to redox potential value" evidence="1">
    <location>
      <position position="42"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Redox-active" evidence="2">
    <location>
      <begin position="40"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of cell-to-cell movement." evidence="6">
    <location>
      <begin position="1"/>
      <end position="5"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Reduces the efficiency of cell-to-cell movement." evidence="6">
    <original>EE</original>
    <variation>AA</variation>
    <location>
      <begin position="4"/>
      <end position="5"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Reduces the efficiency of cell-to-cell movement." evidence="6">
    <original>KDE</original>
    <variation>AAA</variation>
    <location>
      <begin position="14"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Reduces the efficiency of cell-to-cell movement." evidence="6">
    <original>KE</original>
    <variation>AA</variation>
    <location>
      <begin position="25"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Reduces the efficiency of cell-to-cell movement." evidence="6">
    <original>KK</original>
    <variation>AA</variation>
    <location>
      <begin position="55"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Reduces the efficiency of cell-to-cell movement." evidence="6">
    <original>KE</original>
    <variation>AA</variation>
    <location>
      <begin position="71"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Reduces the efficiency of cell-to-cell movement." evidence="6">
    <original>DK</original>
    <variation>AA</variation>
    <location>
      <begin position="95"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Almost loss of cell-to-cell movement." evidence="6">
    <original>RKDD</original>
    <variation>AAAA</variation>
    <location>
      <begin position="101"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Reduces the efficiency of cell-to-cell movement." evidence="6">
    <location>
      <begin position="116"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="6"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="12"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="helix" evidence="8">
    <location>
      <begin position="15"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="turn" evidence="8">
    <location>
      <begin position="26"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="32"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="37"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="43"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="helix" evidence="8">
    <location>
      <begin position="47"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="turn" evidence="8">
    <location>
      <begin position="67"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="helix" evidence="8">
    <location>
      <begin position="71"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="helix" evidence="8">
    <location>
      <begin position="74"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="turn" evidence="8">
    <location>
      <begin position="89"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="turn" evidence="8">
    <location>
      <begin position="102"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="helix" evidence="8">
    <location>
      <begin position="105"/>
      <end position="112"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="114"/>
      <end position="116"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00691"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15618434"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="19665023"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="7766047"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="9599802"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0007829" key="8">
    <source>
      <dbReference type="PDB" id="1WMJ"/>
    </source>
  </evidence>
  <sequence length="122" mass="13156" checksum="E595C3FDC0EE229E" modified="2006-10-17" version="1">MAAEEGVVIACHNKDEFDAQMTKAKEAGKVVIIDFTASWCGPCRFIAPVFAEYAKKFPGAVFLKVDVDELKEVAEKYNVEAMPTFLFIKDGAEADKVVGARKDDLQNTIVKHVGATAASASA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>