<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-11-27" version="87" xmlns="http://uniprot.org/uniprot">
  <accession>Q50373</accession>
  <name>CH60_MYCRH</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Chaperonin GroEL</fullName>
      <ecNumber evidence="1">5.6.1.7</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">60 kDa chaperonin</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>65 kDa heat shock protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Chaperonin-60</fullName>
      <shortName evidence="1">Cpn60</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">groEL</name>
    <name evidence="1" type="synonym">groL</name>
    <name type="synonym">mopA</name>
  </gene>
  <organism>
    <name type="scientific">Mycolicibacterium rhodesiae</name>
    <name type="common">Mycobacterium rhodesiae</name>
    <dbReference type="NCBI Taxonomy" id="36814"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Mycobacteriales</taxon>
      <taxon>Mycobacteriaceae</taxon>
      <taxon>Mycolicibacterium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Arch. Pathol. Lab. Med." volume="119" first="131" last="138">
      <title>Rapid Mycobacterium species assignment and unambiguous identification of mutations associated with antimicrobial resistance in Mycobacterium tuberculosis by automated DNA sequencing.</title>
      <authorList>
        <person name="Kapur V."/>
        <person name="Li L.L."/>
        <person name="Hamrick M.R."/>
        <person name="Plikaytis B.B."/>
        <person name="Shinnick T.M."/>
        <person name="Telenti A."/>
        <person name="Jacobs W.R. Jr."/>
        <person name="Banerjee A."/>
        <person name="Cole S."/>
        <person name="Yuen K.Y."/>
        <person name="Clarridge J.E."/>
        <person name="Kreiswirth B.N."/>
        <person name="Musser J.M."/>
      </authorList>
      <dbReference type="PubMed" id="7848059"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>555</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Together with its co-chaperonin GroES, plays an essential role in assisting protein folding. The GroEL-GroES system forms a nano-cage that allows encapsulation of the non-native substrate proteins and provides a physical environment optimized to promote and accelerate protein folding.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>ATP + H2O + a folded polypeptide = ADP + phosphate + an unfolded polypeptide.</text>
      <dbReference type="EC" id="5.6.1.7"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Forms a cylinder of 14 subunits composed of two heptameric rings stacked back-to-back. Interacts with the co-chaperonin GroES.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="1 2">Belongs to the chaperonin (HSP60) family.</text>
  </comment>
  <dbReference type="EC" id="5.6.1.7" evidence="1"/>
  <dbReference type="EMBL" id="U17954">
    <property type="protein sequence ID" value="AAB39073.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q50373"/>
  <dbReference type="SMR" id="Q50373"/>
  <dbReference type="GO" id="GO:1990220">
    <property type="term" value="C:GroEL-GroES complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005524">
    <property type="term" value="F:ATP binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0140662">
    <property type="term" value="F:ATP-dependent protein folding chaperone"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016853">
    <property type="term" value="F:isomerase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051082">
    <property type="term" value="F:unfolded protein binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051085">
    <property type="term" value="P:chaperone cofactor-dependent protein refolding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042026">
    <property type="term" value="P:protein refolding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009408">
    <property type="term" value="P:response to heat"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.560.10">
    <property type="entry name" value="GroEL-like equatorial domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.260.10">
    <property type="entry name" value="TCP-1-like chaperonin intermediate domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001844">
    <property type="entry name" value="Cpn60/GroEL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002423">
    <property type="entry name" value="Cpn60/GroEL/TCP-1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027413">
    <property type="entry name" value="GROEL-like_equatorial_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027410">
    <property type="entry name" value="TCP-1-like_intermed_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR45633">
    <property type="entry name" value="60 KDA HEAT SHOCK PROTEIN, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR45633:SF3">
    <property type="entry name" value="60 KDA HEAT SHOCK PROTEIN, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00118">
    <property type="entry name" value="Cpn60_TCP1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48592">
    <property type="entry name" value="GroEL equatorial domain-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0067">ATP-binding</keyword>
  <keyword id="KW-0143">Chaperone</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0413">Isomerase</keyword>
  <keyword id="KW-0547">Nucleotide-binding</keyword>
  <keyword id="KW-0346">Stress response</keyword>
  <feature type="chain" id="PRO_0000063447" description="Chaperonin GroEL">
    <location>
      <begin position="1" status="less than"/>
      <end position="120" status="greater than"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <begin position="23"/>
      <end position="27"/>
    </location>
    <ligand>
      <name>ATP</name>
      <dbReference type="ChEBI" id="CHEBI:30616"/>
    </ligand>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="120"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00600"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="120" mass="12409" checksum="30B81072F68812D9" modified="1997-11-01" version="1" fragment="single">PYEKIGAELVKEVAKKTDDVAGDGTTTATVLAQALVREGLRNVAAGANPLGLKRGIEKAVEKITETLLKSAKEVETKDQIAATAGISAGDQTIGDLIAEAMDKVGNEGVITVEESNTFGL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>