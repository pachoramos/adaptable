<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-09-13" modified="2022-12-14" version="41" xmlns="http://uniprot.org/uniprot">
  <accession>P84644</accession>
  <name>CIRF_CHAPA</name>
  <protein>
    <recommendedName>
      <fullName>Circulin-F</fullName>
      <shortName>CIRF</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Chassalia parviflora</name>
    <dbReference type="NCBI Taxonomy" id="58431"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>asterids</taxon>
      <taxon>lamiids</taxon>
      <taxon>Gentianales</taxon>
      <taxon>Rubiaceae</taxon>
      <taxon>Rubioideae</taxon>
      <taxon>Palicoureeae</taxon>
      <taxon>Chassalia</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2000" name="J. Nat. Prod." volume="63" first="176" last="178">
      <title>New circulin macrocyclic polypeptides from Chassalia parvifolia.</title>
      <authorList>
        <person name="Gustafson K.R."/>
        <person name="Walton L.K."/>
        <person name="Sowder R.C. Jr."/>
        <person name="Johnson D.G."/>
        <person name="Pannell L.K."/>
        <person name="Cardellina J.H. Jr."/>
        <person name="Boyd M.R."/>
      </authorList>
      <dbReference type="PubMed" id="10691702"/>
      <dbReference type="DOI" id="10.1021/np990432r"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 4">Probably participates in a plant defense mechanism. Inhibits the cytopathic effects of the human immunodeficiency virus.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2 3">This is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="3053.2" method="FAB" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the cyclotide family. Bracelet subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="4">This peptide is cyclic. The start position was chosen by similarity to OAK1 (kalata-B1) for which the DNA sequence is known.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84644"/>
  <dbReference type="SMR" id="P84644"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050688">
    <property type="term" value="P:regulation of defense response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012323">
    <property type="entry name" value="Cyclotide_bracelet_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF037891">
    <property type="entry name" value="Cycloviolacin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60008">
    <property type="entry name" value="CYCLOTIDE_BRACELET"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0930">Antiviral protein</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000043603" description="Circulin-F" evidence="2 3">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 2">
    <location>
      <begin position="4"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 2">
    <location>
      <begin position="8"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 2">
    <location>
      <begin position="13"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Ala-Arg)" evidence="3">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P56871"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="10691702"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="29" mass="3076" checksum="019A52EACB38A95D" modified="2005-09-13" version="1">AIPCGESCVWIPCISAAIGCSCKNKVCYR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>