<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2024-11-27" version="91" xmlns="http://uniprot.org/uniprot">
  <accession>P81180</accession>
  <name>CVN_NOSEL</name>
  <protein>
    <recommendedName>
      <fullName>Cyanovirin-N</fullName>
      <shortName>CV-N</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Nostoc ellipsosporum</name>
    <dbReference type="NCBI Taxonomy" id="45916"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Cyanobacteriota</taxon>
      <taxon>Cyanophyceae</taxon>
      <taxon>Nostocales</taxon>
      <taxon>Nostocaceae</taxon>
      <taxon>Nostoc</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Antimicrob. Agents Chemother." volume="41" first="1521" last="1530">
      <title>Discovery of cyanovirin-N, a novel human immunodeficiency virus-inactivating protein that binds viral surface envelope glycoprotein gp120: potential applications to microbicide development.</title>
      <authorList>
        <person name="Boyd M.R."/>
        <person name="Gustafson K.R."/>
        <person name="McMahon J.B."/>
        <person name="Shoemaker R.H."/>
        <person name="O'Keefe B.R."/>
        <person name="Mori T."/>
        <person name="Gulakowski R.J."/>
        <person name="Wu L."/>
        <person name="Rivera M.I."/>
        <person name="Laurencot C.M."/>
        <person name="Currens M.J."/>
        <person name="Cardellina J.H. II"/>
        <person name="Buckheit R.W. Jr."/>
        <person name="Nara P.L."/>
        <person name="Pannell L.K."/>
        <person name="Sowder R.C. II"/>
        <person name="Henderson L.E."/>
      </authorList>
      <dbReference type="PubMed" id="9210678"/>
      <dbReference type="DOI" id="10.1128/aac.41.7.1521"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>STABILITY</scope>
    <scope>BIOTECHNOLOGY (EXPRESSION IN E.COLI)</scope>
    <scope>INTERACTION WITH HIV-1 GP120</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="Biochem. Biophys. Res. Commun." volume="238" first="223" last="228">
      <title>Isolation, primary sequence determination, and disulfide bond structure of cyanovirin-N, an anti-HIV (human immunodeficiency virus) protein from the cyanobacterium Nostoc ellipsosporum.</title>
      <authorList>
        <person name="Gustafson K.R."/>
        <person name="Sowder R.C. II"/>
        <person name="Henderson L.E."/>
        <person name="Cardellina J.H. II"/>
        <person name="McMahon J.B."/>
        <person name="Rajamani U."/>
        <person name="Pannell L.K."/>
        <person name="Boyd M.R."/>
      </authorList>
      <dbReference type="PubMed" id="9299483"/>
      <dbReference type="DOI" id="10.1006/bbrc.1997.7203"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>DISULFIDE BOND</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2000" name="J. Virol." volume="74" first="4562" last="4569">
      <title>Multiple antiviral activities of cyanovirin-N: blocking of human immunodeficiency virus type 1 gp120 interaction with CD4 and coreceptor and inhibition of diverse enveloped viruses.</title>
      <authorList>
        <person name="Dey B."/>
        <person name="Lerner D.L."/>
        <person name="Lusso P."/>
        <person name="Boyd M.R."/>
        <person name="Elder J.H."/>
        <person name="Berger E.A."/>
      </authorList>
      <dbReference type="PubMed" id="10775592"/>
      <dbReference type="DOI" id="10.1128/jvi.74.10.4562-4569.2000"/>
    </citation>
    <scope>VIRAL SUBSTRATES</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2002" name="Protein Expr. Purif." volume="26" first="42" last="49">
      <title>Functional homologs of cyanovirin-N amenable to mass production in prokaryotic and eukaryotic hosts.</title>
      <authorList>
        <person name="Mori T."/>
        <person name="Barrientos L.G."/>
        <person name="Han Z."/>
        <person name="Gronenborn A.M."/>
        <person name="Turpin J.A."/>
        <person name="Boyd M.R."/>
      </authorList>
      <dbReference type="PubMed" id="12356469"/>
      <dbReference type="DOI" id="10.1016/s1046-5928(02)00513-2"/>
    </citation>
    <scope>BIOTECHNOLOGY (EXPRESSION IN PICHIA PASTORIS)</scope>
    <scope>MUTAGENESIS OF ASN-30</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2003" name="Cell. Mol. Life Sci." volume="60" first="277" last="287">
      <title>Cyanovirin-N: a sugar-binding antiviral protein with a new twist.</title>
      <authorList>
        <person name="Botos I."/>
        <person name="Wlodawer A."/>
      </authorList>
      <dbReference type="PubMed" id="12678493"/>
      <dbReference type="DOI" id="10.1007/s000180300023"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2006" name="Antimicrob. Agents Chemother." volume="50" first="3250" last="3259">
      <title>Engineered vaginal lactobacillus strain for mucosal delivery of the human immunodeficiency virus inhibitor cyanovirin-N.</title>
      <authorList>
        <person name="Liu X."/>
        <person name="Lagenaur L.A."/>
        <person name="Simpson D.A."/>
        <person name="Essenmacher K.P."/>
        <person name="Frazier-Parker C.L."/>
        <person name="Liu Y."/>
        <person name="Tsai D."/>
        <person name="Rao S.S."/>
        <person name="Hamer D.H."/>
        <person name="Parks T.P."/>
        <person name="Lee P.P."/>
        <person name="Xu Q."/>
      </authorList>
      <dbReference type="PubMed" id="17005802"/>
      <dbReference type="DOI" id="10.1128/aac.00493-06"/>
    </citation>
    <scope>BIOTECHNOLOGY (EXPRESSION IN LACTOBACILLUS JENSENII)</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2006" name="FASEB J." volume="20" first="356" last="358">
      <title>Transgenic plant production of Cyanovirin-N, an HIV microbicide.</title>
      <authorList>
        <person name="Sexton A."/>
        <person name="Drake P.M."/>
        <person name="Mahmood N."/>
        <person name="Harman S.J."/>
        <person name="Shattock R.J."/>
        <person name="Ma J.K."/>
      </authorList>
      <dbReference type="PubMed" id="16354721"/>
      <dbReference type="DOI" id="10.1096/fj.05-4742fje"/>
    </citation>
    <scope>BIOTECHNOLOGY (EXPRESSION IN NICOTIANA TABACUM)</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2010" name="Appl. Microbiol. Biotechnol." volume="85" first="1051" last="1060">
      <title>Soluble cytoplasmic expression, rapid purification, and characterization of cyanovirin-N as a His-SUMO fusion.</title>
      <authorList>
        <person name="Gao X."/>
        <person name="Chen W."/>
        <person name="Guo C."/>
        <person name="Qian C."/>
        <person name="Liu G."/>
        <person name="Ge F."/>
        <person name="Huang Y."/>
        <person name="Kitazato K."/>
        <person name="Wang Y."/>
        <person name="Xiong S."/>
      </authorList>
      <dbReference type="PubMed" id="19547966"/>
      <dbReference type="DOI" id="10.1007/s00253-009-2078-5"/>
    </citation>
    <scope>BIOTECHNOLOGY (EXPRESSION IN ESCHERICHIA COLI)</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2010" name="Appl. Microbiol. Biotechnol." volume="86" first="805" last="812">
      <title>The antiviral protein cyanovirin-N: the current state of its production and applications.</title>
      <authorList>
        <person name="Xiong S."/>
        <person name="Fan J."/>
        <person name="Kitazato K."/>
      </authorList>
      <dbReference type="PubMed" id="20162270"/>
      <dbReference type="DOI" id="10.1007/s00253-010-2470-1"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="1998" name="Nat. Struct. Biol." volume="5" first="571" last="578">
      <title>Solution structure of cyanovirin-N, a potent HIV-inactivating protein.</title>
      <authorList>
        <person name="Bewley C.A."/>
        <person name="Gustafson K.R."/>
        <person name="Boyd M.R."/>
        <person name="Covell D.G."/>
        <person name="Bax A."/>
        <person name="Clore G.M."/>
        <person name="Gronenborn A.M."/>
      </authorList>
      <dbReference type="PubMed" id="9665171"/>
      <dbReference type="DOI" id="10.1038/828"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="1999" name="J. Mol. Biol." volume="288" first="403" last="412">
      <title>Crystal structure of cyanovirin-N, a potent HIV-inactivating protein, shows unexpected domain swapping.</title>
      <authorList>
        <person name="Yang F."/>
        <person name="Bewley C.A."/>
        <person name="Louis J.M."/>
        <person name="Gustafson K.R."/>
        <person name="Boyd M.R."/>
        <person name="Gronenborn A.M."/>
        <person name="Clore G.M."/>
        <person name="Wlodawer A."/>
      </authorList>
      <dbReference type="PubMed" id="10329150"/>
      <dbReference type="DOI" id="10.1006/jmbi.1999.2693"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.5 ANGSTROMS)</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2002" name="J. Biol. Chem." volume="277" first="34336" last="34342">
      <title>Structures of the complexes of a potent anti-HIV protein cyanovirin-N and high mannose oligosaccharides.</title>
      <authorList>
        <person name="Botos I."/>
        <person name="O'Keefe B.R."/>
        <person name="Shenoy S.R."/>
        <person name="Cartner L.K."/>
        <person name="Ratner D.M."/>
        <person name="Seeberger P.H."/>
        <person name="Boyd M.R."/>
        <person name="Wlodawer A."/>
      </authorList>
      <dbReference type="PubMed" id="12110688"/>
      <dbReference type="DOI" id="10.1074/jbc.m205909200"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.4 ANGSTROMS) IN COMPLEX WITH HIGH-MANNOSE OLIGOSACCHARIDES</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2002" name="Structure" volume="10" first="673" last="686">
      <title>The domain-swapped dimer of cyanovirin-N is in a metastable folded state: reconciliation of X-ray and NMR structures.</title>
      <authorList>
        <person name="Barrientos L.G."/>
        <person name="Louis J.M."/>
        <person name="Botos I."/>
        <person name="Mori T."/>
        <person name="Han Z."/>
        <person name="O'Keefe B.R."/>
        <person name="Boyd M.R."/>
        <person name="Wlodawer A."/>
        <person name="Gronenborn A.M."/>
      </authorList>
      <dbReference type="PubMed" id="12015150"/>
      <dbReference type="DOI" id="10.1016/s0969-2126(02)00758-x"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.0 ANGSTROMS)</scope>
    <scope>STRUCTURE BY NMR</scope>
    <scope>SUBUNIT</scope>
    <scope>MUTAGENESIS OF PRO-51 AND SER-52</scope>
  </reference>
  <comment type="function">
    <text evidence="4 8">Mannose-binding lectin.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 2 8">In solution exists as a metastable domain-swapped homodimer which very slowly converts into a more stable monomeric form at room temperature. Under physiological conditions it is unlikely that the dimeric species exists and indeed the monomer is more active against HIV. Interacts with HIV-1 gp120.</text>
  </comment>
  <comment type="PTM">
    <text>Cleavage, or reduction and alkylation of the disulfide bonds results in the loss of anti-HIV activity.</text>
  </comment>
  <comment type="mass spectrometry" mass="11014.2" method="Electrospray" evidence="9"/>
  <comment type="biotechnology">
    <text evidence="3 5 6 7 8">Overexpression of this protein to provide quantities adequate for medical use as a topical microbiocide has been attempted in a number of systems including E.coli (PubMed:9210678, PubMed:19547966), Lactobacillus jensenii (PubMed:17005802), the yeast Pichia pastoris (PubMed:12356469) and Nicotiana tabacum (PubMed:16354721); see PubMed:20162270 for a review.</text>
  </comment>
  <comment type="miscellaneous">
    <text>Its activity in situ is unknown, however it acts as a viral entry inhibitor, inhibiting HIV-1, HIV-2 and simian immunodeficiency virus (and some other viruses such as feline immunodeficiency virus, measles virus and human herpesvirus) infection and replication. It prevents essential interactions between the envelope glycoprotein and target cell receptors by binding to carbohydrates on viral protein gp120 and possibly by other mechanisms as well. Addition to cells must occur before or shortly after virus addition. It also inhibits cell-to-cell fusion, and virus-to-cell and cell-to-cell transmission of a viral infection.</text>
  </comment>
  <comment type="miscellaneous">
    <text>Is remarkably stabile; the protein can withstand multiple freeze-thaw cycles, dissolution in organic solvents, treatment with salt, detergent, H(2)O(2) and boiling without significant loss of anti-HIV activity.</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the cyanovirin-N family.</text>
  </comment>
  <dbReference type="PIR" id="JC5631">
    <property type="entry name" value="JC5631"/>
  </dbReference>
  <dbReference type="PDB" id="1IIY">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="1J4V">
    <property type="method" value="NMR"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="1L5B">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="1L5E">
    <property type="method" value="NMR"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="1LOM">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.72 A"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="1N02">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="2EZM">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="2EZN">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="2PYS">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.80 A"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="2RDK">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.35 A"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="2RP3">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="2Z21">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.80 A"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="3CZZ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.36 A"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="3EZM">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.50 A"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="3GXY">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.40 A"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="3GXZ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="3LHC">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.34 A"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="3S3Y">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="3S3Z">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.75 A"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="4J4C">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.90 A"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="4J4D">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="A/B/C/D=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="4J4E">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.40 A"/>
    <property type="chains" value="A/B/C/D/E/F=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="4J4F">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.90 A"/>
    <property type="chains" value="A/B/C/D=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="4J4G">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.92 A"/>
    <property type="chains" value="A/B/C/D=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="6X7H">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.25 A"/>
    <property type="chains" value="A/B=1-101"/>
  </dbReference>
  <dbReference type="PDBsum" id="1IIY"/>
  <dbReference type="PDBsum" id="1J4V"/>
  <dbReference type="PDBsum" id="1L5B"/>
  <dbReference type="PDBsum" id="1L5E"/>
  <dbReference type="PDBsum" id="1LOM"/>
  <dbReference type="PDBsum" id="1N02"/>
  <dbReference type="PDBsum" id="2EZM"/>
  <dbReference type="PDBsum" id="2EZN"/>
  <dbReference type="PDBsum" id="2PYS"/>
  <dbReference type="PDBsum" id="2RDK"/>
  <dbReference type="PDBsum" id="2RP3"/>
  <dbReference type="PDBsum" id="2Z21"/>
  <dbReference type="PDBsum" id="3CZZ"/>
  <dbReference type="PDBsum" id="3EZM"/>
  <dbReference type="PDBsum" id="3GXY"/>
  <dbReference type="PDBsum" id="3GXZ"/>
  <dbReference type="PDBsum" id="3LHC"/>
  <dbReference type="PDBsum" id="3S3Y"/>
  <dbReference type="PDBsum" id="3S3Z"/>
  <dbReference type="PDBsum" id="4J4C"/>
  <dbReference type="PDBsum" id="4J4D"/>
  <dbReference type="PDBsum" id="4J4E"/>
  <dbReference type="PDBsum" id="4J4F"/>
  <dbReference type="PDBsum" id="4J4G"/>
  <dbReference type="PDBsum" id="6X7H"/>
  <dbReference type="AlphaFoldDB" id="P81180"/>
  <dbReference type="SMR" id="P81180"/>
  <dbReference type="DrugBank" id="DB03309">
    <property type="generic name" value="N-cyclohexyltaurine"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB02695">
    <property type="generic name" value="O1-Pentyl-Mannose"/>
  </dbReference>
  <dbReference type="UniLectin" id="P81180"/>
  <dbReference type="EvolutionaryTrace" id="P81180"/>
  <dbReference type="GO" id="GO:0030246">
    <property type="term" value="F:carbohydrate binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050688">
    <property type="term" value="P:regulation of defense response to virus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="2.30.60.10:FF:000001">
    <property type="entry name" value="Cyanovirin-N"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.30.60.10">
    <property type="entry name" value="Cyanovirin-N"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011058">
    <property type="entry name" value="Cyanovirin-N"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036673">
    <property type="entry name" value="Cyanovirin-N_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR42076:SF1">
    <property type="entry name" value="CYANOVIRIN-N DOMAIN-CONTAINING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR42076">
    <property type="entry name" value="CYANOVIRIN-N HOMOLOG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08881">
    <property type="entry name" value="CVNH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01111">
    <property type="entry name" value="CVNH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF51322">
    <property type="entry name" value="Cyanovirin-N"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0930">Antiviral protein</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0430">Lectin</keyword>
  <feature type="chain" id="PRO_0000079579" description="Cyanovirin-N">
    <location>
      <begin position="1"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="9">
    <location>
      <begin position="8"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="9">
    <location>
      <begin position="58"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Prevents N-glycosylation upon overexpression in yeast without changing anti-HIV activity." evidence="3">
    <original>N</original>
    <variation>A</variation>
    <variation>Q</variation>
    <variation>V</variation>
    <location>
      <position position="30"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Protein is mostly monomeric and has wild-type anti-HIV activity." evidence="1">
    <original>P</original>
    <variation>G</variation>
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Protein is exclusively dimeric and has moderate anti-HIV activity." evidence="1">
    <original>S</original>
    <variation>P</variation>
    <location>
      <position position="52"/>
    </location>
  </feature>
  <feature type="helix" evidence="13">
    <location>
      <begin position="4"/>
      <end position="6"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="8"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="17"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="25"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="29"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="helix" evidence="13">
    <location>
      <begin position="36"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="40"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="46"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="helix" evidence="13">
    <location>
      <begin position="54"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="57"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="turn" evidence="13">
    <location>
      <begin position="65"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="68"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="strand" evidence="12">
    <location>
      <begin position="76"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="80"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="helix" evidence="13">
    <location>
      <begin position="87"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="91"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="strand" evidence="13">
    <location>
      <begin position="97"/>
      <end position="100"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="12015150"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12110688"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12356469"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="12678493"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="16354721"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="17005802"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="19547966"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="9210678"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="9299483"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0007829" key="11">
    <source>
      <dbReference type="PDB" id="1L5B"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="12">
    <source>
      <dbReference type="PDB" id="3GXZ"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="13">
    <source>
      <dbReference type="PDB" id="6X7H"/>
    </source>
  </evidence>
  <sequence length="101" mass="11013" checksum="1F8FA5B88ACCE57F" modified="2000-12-01" version="2">LGKFSQTCYNSAIQGSVLTSTCERTNGGYNTSSIDLNSVIENVDGSLKWQPSNFIETCRNTQLAGSSELAAECKTRAQQFVSTKINLDDHIANIDGTLKYE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>