<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-04-14" modified="2024-11-27" version="84" xmlns="http://uniprot.org/uniprot">
  <accession>B2IS48</accession>
  <name>RL29_STRPS</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Large ribosomal subunit protein uL29</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">50S ribosomal protein L29</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rpmC</name>
    <name type="ordered locus">SPCG_0226</name>
  </gene>
  <organism>
    <name type="scientific">Streptococcus pneumoniae (strain CGSP14)</name>
    <dbReference type="NCBI Taxonomy" id="516950"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Streptococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="BMC Genomics" volume="10" first="158" last="158">
      <title>Genome evolution driven by host adaptations results in a more virulent and antimicrobial-resistant Streptococcus pneumoniae serotype 14.</title>
      <authorList>
        <person name="Ding F."/>
        <person name="Tang P."/>
        <person name="Hsu M.-H."/>
        <person name="Cui P."/>
        <person name="Hu S."/>
        <person name="Yu J."/>
        <person name="Chiu C.-H."/>
      </authorList>
      <dbReference type="PubMed" id="19361343"/>
      <dbReference type="DOI" id="10.1186/1471-2164-10-158"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CGSP14</strain>
    </source>
  </reference>
  <comment type="similarity">
    <text evidence="1">Belongs to the universal ribosomal protein uL29 family.</text>
  </comment>
  <dbReference type="EMBL" id="CP001033">
    <property type="protein sequence ID" value="ACB89478.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000772918.1">
    <property type="nucleotide sequence ID" value="NC_010582.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B2IS48"/>
  <dbReference type="SMR" id="B2IS48"/>
  <dbReference type="GeneID" id="66805424"/>
  <dbReference type="KEGG" id="spw:SPCG_0226"/>
  <dbReference type="HOGENOM" id="CLU_158491_5_2_9"/>
  <dbReference type="GO" id="GO:0022625">
    <property type="term" value="C:cytosolic large ribosomal subunit"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006412">
    <property type="term" value="P:translation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="CDD" id="cd00427">
    <property type="entry name" value="Ribosomal_L29_HIP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.287.310:FF:000001">
    <property type="entry name" value="50S ribosomal protein L29"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.287.310">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00374">
    <property type="entry name" value="Ribosomal_uL29"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050063">
    <property type="entry name" value="Ribosomal_protein_uL29"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001854">
    <property type="entry name" value="Ribosomal_uL29"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018254">
    <property type="entry name" value="Ribosomal_uL29_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036049">
    <property type="entry name" value="Ribosomal_uL29_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00012">
    <property type="entry name" value="L29"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10916:SF0">
    <property type="entry name" value="50S RIBOSOMAL PROTEIN L29, CHLOROPLASTIC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10916">
    <property type="entry name" value="60S RIBOSOMAL PROTEIN L35/50S RIBOSOMAL PROTEIN L29"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00831">
    <property type="entry name" value="Ribosomal_L29"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46561">
    <property type="entry name" value="Ribosomal protein L29 (L29p)"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00579">
    <property type="entry name" value="RIBOSOMAL_L29"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <feature type="chain" id="PRO_1000121825" description="Large ribosomal subunit protein uL29">
    <location>
      <begin position="1"/>
      <end position="68"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00374"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="68" mass="7987" checksum="1A324B78908A3464" modified="2008-06-10" version="1">MKLNEVKEFVKELRGLSQEELAKRENELKKELFELRFQAATGQLEQTARLKEVKKQIARIKTVQSEAK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>