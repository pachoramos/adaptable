<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-06-21" modified="2023-06-28" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>Q58T59</accession>
  <name>M6H10_BOMMX</name>
  <protein>
    <recommendedName>
      <fullName>Maximins 6/H10</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Maximin-6</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Maximin-H10</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Bombina maxima</name>
    <name type="common">Giant fire-bellied toad</name>
    <name type="synonym">Chinese red belly toad</name>
    <dbReference type="NCBI Taxonomy" id="161274"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Bombinatoridae</taxon>
      <taxon>Bombina</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Eur. J. Immunol." volume="35" first="1220" last="1229">
      <title>Variety of antimicrobial peptides in the Bombina maxima toad and evidence of their rapid diversification.</title>
      <authorList>
        <person name="Lee W.-H."/>
        <person name="Li Y."/>
        <person name="Lai R."/>
        <person name="Li S."/>
        <person name="Zhang Y."/>
        <person name="Wang W."/>
      </authorList>
      <dbReference type="PubMed" id="15770703"/>
      <dbReference type="DOI" id="10.1002/eji.200425615"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 44-70 AND 124-143</scope>
    <scope>AMIDATION AT ASN-70 AND LEU-143</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Maximin-6 shows antimicrobial activity against bacteria and against the fungus C.albicans. It has little hemolytic activity (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="1">Maximin-H10 shows antimicrobial activity against bacteria and against the fungus C.albicans. Shows strong hemolytic activity (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the bombinin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY849001">
    <property type="protein sequence ID" value="AAX50222.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q58T59"/>
  <dbReference type="SMR" id="Q58T59"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007962">
    <property type="entry name" value="Bombinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05298">
    <property type="entry name" value="Bombinin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003192" evidence="1">
    <location>
      <begin position="19"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003193" description="Maximin-6">
    <location>
      <begin position="44"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003194" evidence="1">
    <location>
      <begin position="74"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003195" description="Maximin-H10">
    <location>
      <begin position="124"/>
      <end position="143"/>
    </location>
  </feature>
  <feature type="modified residue" description="Asparagine amide" evidence="3">
    <location>
      <position position="70"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="3">
    <location>
      <position position="143"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15770703"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="144" mass="15824" checksum="29D1248A085EDB8C" modified="2005-04-26" version="1" precursor="true">MNFKYIVAVSFLIASAYARSVKNDEQSLSQRDVLDEESLREIRGIGGALLSAGKSALKGLAKGLAEHFANGKRTAEDHEVMKRLEAVMRDLDSLDHPEEASERETRGFNQEEIANRFTKKEKRILGPVLGLVSNALGGLLKNLG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>