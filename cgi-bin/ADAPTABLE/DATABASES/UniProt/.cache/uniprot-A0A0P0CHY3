<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2022-02-23" modified="2023-05-03" version="9" xmlns="http://uniprot.org/uniprot">
  <accession>A0A0P0CHY3</accession>
  <name>CXP7_PARCG</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Parbolysin P7</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Parbolysin 7</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Parborlasia corrugatus</name>
    <name type="common">Antarctic nemertean worm</name>
    <dbReference type="NCBI Taxonomy" id="187802"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Nemertea</taxon>
      <taxon>Pilidiophora</taxon>
      <taxon>Heteronemertea</taxon>
      <taxon>Lineidae</taxon>
      <taxon>Parborlasia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2015" name="Toxicon" volume="108" first="32" last="37">
      <title>Recombinant expression and predicted structure of parborlysin, a cytolytic protein from the Antarctic heteronemertine Parborlasia corrugatus.</title>
      <authorList>
        <person name="Butala M."/>
        <person name="Sega D."/>
        <person name="Tomc B."/>
        <person name="Podlesek Z."/>
        <person name="Kem W.R."/>
        <person name="Kupper F.C."/>
        <person name="Turk T."/>
      </authorList>
      <dbReference type="PubMed" id="26435341"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2015.09.044"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Cytolysin that shows hemolytic activity (on bovine erythrocytes, HC(50)=5.75 mg/ml). This hemolytic activity is completely inhibited by small unilamelar vesicles composed of PC/PG, PC/PI and PC/PS in 1:1 molar ratios (with at least 100 mg/ml concentration).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Localized within the skin and proboscis and are most readily isolated from body mucus secretions.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the worm cytolysin family.</text>
  </comment>
  <dbReference type="EMBL" id="KT693320">
    <property type="protein sequence ID" value="ALI86911.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A0P0CHY3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000454515" description="Parbolysin P7" evidence="5">
    <location>
      <begin position="1"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="15"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="21"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="46"/>
      <end position="59"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A0N7HUN6"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P01527"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="26435341"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="26435341"/>
    </source>
  </evidence>
  <sequence length="92" mass="9655" checksum="48C38C832F515CCF" modified="2016-01-20" version="1">WPAYPGPNGIRSSVCQTKLGCGKKNLATKGVCKAFCLGRKRFWQKCGKNGSSGKGSKICNAVLAHAVEKAGKGLIKVTDMAVAAIIKYAGKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>