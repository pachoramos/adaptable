<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-11" modified="2022-05-25" version="39" xmlns="http://uniprot.org/uniprot">
  <accession>P67788</accession>
  <accession>P08901</accession>
  <name>AKH_MANSE</name>
  <protein>
    <recommendedName>
      <fullName>Adipokinetic prohormone</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Adipokinetic hormone</fullName>
        <shortName>AKH</shortName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Manduca sexta</name>
    <name type="common">Tobacco hawkmoth</name>
    <name type="synonym">Tobacco hornworm</name>
    <dbReference type="NCBI Taxonomy" id="7130"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Bombycoidea</taxon>
      <taxon>Sphingidae</taxon>
      <taxon>Sphinginae</taxon>
      <taxon>Sphingini</taxon>
      <taxon>Manduca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1989" name="J. Biol. Chem." volume="264" first="12791" last="12793">
      <title>Adipokinetic hormone gene sequence from Manduca sexta.</title>
      <authorList>
        <person name="Bradfield J.Y."/>
        <person name="Keeley L.L."/>
      </authorList>
      <dbReference type="PubMed" id="2753887"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)51555-6"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1985" name="Biochem. Biophys. Res. Commun." volume="133" first="337" last="342">
      <title>Amino acid sequence of Manduca sexta adipokinetic hormone elucidated by combined fast atom bombardment (FAB)/tandem mass spectrometry.</title>
      <authorList>
        <person name="Ziegler R."/>
        <person name="Eckart K."/>
        <person name="Schwarz H."/>
        <person name="Keller R."/>
      </authorList>
      <dbReference type="PubMed" id="4074373"/>
      <dbReference type="DOI" id="10.1016/0006-291x(85)91880-7"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 20-28</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-20</scope>
    <scope>AMIDATION AT GLY-28</scope>
  </reference>
  <comment type="function">
    <text>This hormone, released from cells in the corpora cardiaca, causes release of diglycerides from the fat body and stimulation of muscles to use these diglycerides as an energy source during energy-demanding processes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the AKH/HRTH/RPCH family.</text>
  </comment>
  <dbReference type="EMBL" id="J04972">
    <property type="protein sequence ID" value="AAA29299.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A32613">
    <property type="entry name" value="A32613"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P67788"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007629">
    <property type="term" value="P:flight behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002047">
    <property type="entry name" value="Adipokinetic_hormone_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010475">
    <property type="entry name" value="AKH/RPCH_hormone"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06377">
    <property type="entry name" value="Adipokin_hormo"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00256">
    <property type="entry name" value="AKH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0286">Flight</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000000914" description="Adipokinetic prohormone">
    <location>
      <begin position="20"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000000915" description="Adipokinetic hormone">
    <location>
      <begin position="20"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glycine amide" evidence="1">
    <location>
      <position position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="4074373"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="65" mass="7401" checksum="1812E58E6F2D7852" modified="2004-10-11" version="1" precursor="true">MYKLTVFLMFIAFVIIAEAQLTFTSSWGGKRAMTNSISCRNDEAIAAIYKAIQNEAERFIMCQKN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>