<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-04-14" modified="2024-11-27" version="80" xmlns="http://uniprot.org/uniprot">
  <accession>B4TSF8</accession>
  <name>NSRR_SALSV</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">HTH-type transcriptional repressor NsrR</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">nsrR</name>
    <name type="ordered locus">SeSA_A4635</name>
  </gene>
  <organism>
    <name type="scientific">Salmonella schwarzengrund (strain CVM19633)</name>
    <dbReference type="NCBI Taxonomy" id="439843"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Salmonella</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2011" name="J. Bacteriol." volume="193" first="3556" last="3568">
      <title>Comparative genomics of 28 Salmonella enterica isolates: evidence for CRISPR-mediated adaptive sublineage evolution.</title>
      <authorList>
        <person name="Fricke W.F."/>
        <person name="Mammel M.K."/>
        <person name="McDermott P.F."/>
        <person name="Tartera C."/>
        <person name="White D.G."/>
        <person name="Leclerc J.E."/>
        <person name="Ravel J."/>
        <person name="Cebula T.A."/>
      </authorList>
      <dbReference type="PubMed" id="21602358"/>
      <dbReference type="DOI" id="10.1128/jb.00297-11"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CVM19633</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Nitric oxide-sensitive repressor of genes involved in protecting the cell against nitrosative stress. May require iron for activity.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="1">
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </cofactor>
    <text evidence="1">Binds 1 [2Fe-2S] cluster per subunit.</text>
  </comment>
  <dbReference type="EMBL" id="CP001127">
    <property type="protein sequence ID" value="ACF89856.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_001177632.1">
    <property type="nucleotide sequence ID" value="NC_011094.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B4TSF8"/>
  <dbReference type="SMR" id="B4TSF8"/>
  <dbReference type="KEGG" id="sew:SeSA_A4635"/>
  <dbReference type="HOGENOM" id="CLU_107144_2_1_6"/>
  <dbReference type="Proteomes" id="UP000001865">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051537">
    <property type="term" value="F:2 iron, 2 sulfur cluster binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003700">
    <property type="term" value="F:DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003690">
    <property type="term" value="F:double-stranded DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005506">
    <property type="term" value="F:iron ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045892">
    <property type="term" value="P:negative regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.10.10:FF:000105">
    <property type="entry name" value="HTH-type transcriptional repressor NsrR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.10.10">
    <property type="entry name" value="Winged helix-like DNA-binding domain superfamily/Winged helix DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01177">
    <property type="entry name" value="HTH_type_NsrR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000944">
    <property type="entry name" value="Tscrpt_reg_Rrf2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023761">
    <property type="entry name" value="Tscrpt_rep_HTH_NsrR"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036388">
    <property type="entry name" value="WH-like_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036390">
    <property type="entry name" value="WH_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00738">
    <property type="entry name" value="rrf2_super"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33221:SF4">
    <property type="entry name" value="HTH-TYPE TRANSCRIPTIONAL REPRESSOR NSRR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33221">
    <property type="entry name" value="WINGED HELIX-TURN-HELIX TRANSCRIPTIONAL REGULATOR, RRF2 FAMILY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02082">
    <property type="entry name" value="Rrf2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46785">
    <property type="entry name" value="Winged helix' DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51197">
    <property type="entry name" value="HTH_RRF2_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0001">2Fe-2S</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0408">Iron</keyword>
  <keyword id="KW-0411">Iron-sulfur</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0678">Repressor</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_1000138131" description="HTH-type transcriptional repressor NsrR">
    <location>
      <begin position="1"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="domain" description="HTH rrf2-type" evidence="1">
    <location>
      <begin position="2"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="DNA-binding region" description="H-T-H motif" evidence="1">
    <location>
      <begin position="28"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="91"/>
    </location>
    <ligand>
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="96"/>
    </location>
    <ligand>
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="102"/>
    </location>
    <ligand>
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </ligand>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01177"/>
    </source>
  </evidence>
  <sequence length="141" mass="15608" checksum="ABC9A219C1E81BCC" modified="2008-09-23" version="1">MQLTSFTDYGLRALIYMASLPDGRMTSISEVTEVYGVSRNHMVKIINQLSRAGFVTAVRGKNGGIRLGKPANTICIGDVVRELEPLSLVNCSSEFCHITPACRLKQALSKAVQSFLKELDNYTLADLVEENQPLYKLLLVE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>