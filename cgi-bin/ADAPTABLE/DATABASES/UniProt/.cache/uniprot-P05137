<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1987-08-13" modified="2024-07-24" version="64" xmlns="http://uniprot.org/uniprot">
  <accession>P05137</accession>
  <accession>Q0GNB7</accession>
  <accession>Q5I5X8</accession>
  <name>NS3A_IBVM</name>
  <protein>
    <recommendedName>
      <fullName>Non-structural protein 3a</fullName>
      <shortName>ns3a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Accessory protein 3a</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="ORF">3a</name>
  </gene>
  <organism>
    <name type="scientific">Avian infectious bronchitis virus (strain M41)</name>
    <name type="common">IBV</name>
    <dbReference type="NCBI Taxonomy" id="11127"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Orthornavirae</taxon>
      <taxon>Pisuviricota</taxon>
      <taxon>Pisoniviricetes</taxon>
      <taxon>Nidovirales</taxon>
      <taxon>Cornidovirineae</taxon>
      <taxon>Coronaviridae</taxon>
      <taxon>Orthocoronavirinae</taxon>
      <taxon>Gammacoronavirus</taxon>
      <taxon>Igacovirus</taxon>
      <taxon>Avian coronavirus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Gallus gallus</name>
    <name type="common">Chicken</name>
    <dbReference type="NCBI Taxonomy" id="9031"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1991" name="Virology" volume="184" first="531" last="544">
      <title>A polycistronic mRNA specified by the coronavirus infectious bronchitis virus.</title>
      <authorList>
        <person name="Liu D.X."/>
        <person name="Cavanagh D."/>
        <person name="Green P."/>
        <person name="Inglis S.C."/>
      </authorList>
      <dbReference type="PubMed" id="1653486"/>
      <dbReference type="DOI" id="10.1016/0042-6822(91)90423-9"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1986" name="Nucleic Acids Res." volume="14" first="3144" last="3144">
      <title>Infectious bronchitis virus RNA D encodes three potential translation products.</title>
      <authorList>
        <person name="Niesters H.G.M."/>
        <person name="Zijderveld A.J."/>
        <person name="Seifert W.F."/>
        <person name="Lenstra J.A."/>
        <person name="Bleumink-Pluym N.M.C."/>
        <person name="Horzinek M.C."/>
        <person name="van der Zeijst B.A.M."/>
      </authorList>
      <dbReference type="PubMed" id="3960740"/>
      <dbReference type="DOI" id="10.1093/nar/14.7.3144"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2006-06" db="EMBL/GenBank/DDBJ databases">
      <title>Avian infectious bronchitis virus strain M41.</title>
      <authorList>
        <person name="Mondal S.P."/>
        <person name="Buckles E.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2006" name="J. Virol. Methods" volume="138" first="60" last="65">
      <title>Development and evaluation of a real-time Taqman RT-PCR assay for the detection of infectious bronchitis virus from infected chickens.</title>
      <authorList>
        <person name="Callison S.A."/>
        <person name="Hilt D.A."/>
        <person name="Boynton T.O."/>
        <person name="Sample B.F."/>
        <person name="Robison R."/>
        <person name="Swayne D.E."/>
        <person name="Jackwood M.W."/>
      </authorList>
      <dbReference type="PubMed" id="16934878"/>
      <dbReference type="DOI" id="10.1016/j.jviromet.2006.07.018"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Involved in resistance to IFN.</text>
  </comment>
  <dbReference type="EMBL" id="X59542">
    <property type="protein sequence ID" value="CAA42115.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X03723">
    <property type="protein sequence ID" value="CAA27358.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ834384">
    <property type="protein sequence ID" value="ABI26424.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY851295">
    <property type="protein sequence ID" value="AAW33787.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="PIR" id="A23771">
    <property type="entry name" value="A23771"/>
  </dbReference>
  <dbReference type="SMR" id="P05137"/>
  <dbReference type="Proteomes" id="UP000007642">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000096468">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005214">
    <property type="entry name" value="IBV_3A"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03617">
    <property type="entry name" value="IBV_3A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000106109" description="Non-structural protein 3a">
    <location>
      <begin position="23"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>L</original>
    <variation>F</variation>
    <location>
      <position position="14"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P30237"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <sequence length="57" mass="6595" checksum="55B2A87080455C6A" modified="1993-04-01" version="2" precursor="true">MIQSPTSFLIVLILLWCKLVLSCFREFIIALQQLIQVLLQIINSNLQPRLTLCHSLD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>