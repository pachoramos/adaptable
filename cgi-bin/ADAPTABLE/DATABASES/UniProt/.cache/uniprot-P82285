<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2023-05-03" version="61" xmlns="http://uniprot.org/uniprot">
  <accession>P82285</accession>
  <name>BMNH5_BOMVA</name>
  <protein>
    <recommendedName>
      <fullName>Bombinin-H5</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Bombina variegata</name>
    <name type="common">Yellow-bellied toad</name>
    <dbReference type="NCBI Taxonomy" id="8348"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Bombinatoridae</taxon>
      <taxon>Bombina</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="EMBO J." volume="12" first="4829" last="4832">
      <title>Antibacterial and haemolytic peptides containing D-alloisoleucine from the skin of Bombina variegata.</title>
      <authorList>
        <person name="Mignogna G."/>
        <person name="Simmaco M."/>
        <person name="Kreil G."/>
        <person name="Barra D."/>
      </authorList>
      <dbReference type="PubMed" id="8223491"/>
      <dbReference type="DOI" id="10.1002/j.1460-2075.1993.tb06172.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>D-AMINO ACID AT ILE-2</scope>
    <scope>AMIDATION AT ILE-20</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Has antimicrobial and hemolytic activities.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the bombinin family.</text>
  </comment>
  <dbReference type="PDB" id="2AP7">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-20"/>
  </dbReference>
  <dbReference type="PDB" id="2AP8">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-20"/>
  </dbReference>
  <dbReference type="PDBsum" id="2AP7"/>
  <dbReference type="PDBsum" id="2AP8"/>
  <dbReference type="AlphaFoldDB" id="P82285"/>
  <dbReference type="BMRB" id="P82285"/>
  <dbReference type="SMR" id="P82285"/>
  <dbReference type="EvolutionaryTrace" id="P82285"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0208">D-amino acid</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000003079" description="Bombinin-H5">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="modified residue" description="D-allo-isoleucine" evidence="1">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <feature type="modified residue" description="Isoleucine amide" evidence="1">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="4"/>
      <end position="19"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="8223491"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="2AP7"/>
    </source>
  </evidence>
  <sequence length="21" mass="1975" checksum="08C7281E1BDBF3BD" modified="2000-05-30" version="1">IIGPVLGLVGSALGGLLKKIG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>