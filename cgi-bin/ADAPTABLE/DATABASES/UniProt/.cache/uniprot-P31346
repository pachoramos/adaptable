<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-07-01" modified="2024-11-27" version="124" xmlns="http://uniprot.org/uniprot">
  <accession>P31346</accession>
  <accession>A0A287A0F3</accession>
  <accession>Q1HDU1</accession>
  <name>ANGI_PIG</name>
  <protein>
    <recommendedName>
      <fullName>Angiogenin</fullName>
      <ecNumber evidence="1">3.1.27.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Ribonuclease 5</fullName>
      <shortName>RNase 5</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ANG</name>
  </gene>
  <organism>
    <name type="scientific">Sus scrofa</name>
    <name type="common">Pig</name>
    <dbReference type="NCBI Taxonomy" id="9823"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Suina</taxon>
      <taxon>Suidae</taxon>
      <taxon>Sus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="Mol. Biol. Rep." volume="36" first="2405" last="2411">
      <title>The porcine ANG, RNASE1 and RNASE6 genes: molecular cloning, polymorphism detection and the association with haematological parameters.</title>
      <authorList>
        <person name="Bai X."/>
        <person name="Liang Z."/>
        <person name="Zhao S."/>
        <person name="Liu X."/>
        <person name="Zhu M."/>
        <person name="Wu Z."/>
        <person name="Yu M."/>
      </authorList>
      <dbReference type="PubMed" id="19247805"/>
      <dbReference type="DOI" id="10.1007/s11033-009-9471-0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="2009-11" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="Porcine genome sequencing project"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1993" name="Biochim. Biophys. Acta" volume="1162" first="177" last="186">
      <title>Characterization and sequencing of rabbit, pig and mouse angiogenins: discernment of functionally important residues and regions.</title>
      <authorList>
        <person name="Bond M.D."/>
        <person name="Strydom D.J."/>
        <person name="Vallee B.L."/>
      </authorList>
      <dbReference type="PubMed" id="8448182"/>
      <dbReference type="DOI" id="10.1016/0167-4838(93)90145-h"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 25-147</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Serum</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2 3">Secreted ribonuclease that can either promote or restrict cell proliferation of target cells, depending on the context (By similarity). Endocytosed in target cells via its receptor PLXNB2 and translocates to the cytoplasm or nucleus (By similarity). Under stress conditions, localizes to the cytoplasm and promotes the assembly of stress granules (SGs): specifically cleaves a subset of tRNAs within anticodon loops to produce tRNA-derived stress-induced fragments (tiRNAs), resulting in translation repression and inhibition of cell proliferation (By similarity). tiRNas also prevent formation of apoptosome, thereby promoting cell survival (By similarity). Preferentially cleaves RNAs between a pyrimidine and an adenosine residue, suggesting that it cleaves the anticodon loop of tRNA(Ala) (32-UUAGCAU-38) after positions 33 and 36 (By similarity). Cleaves a subset of tRNAs, including tRNA(Ala), tRNA(Glu), tRNA(Gly), tRNA(Lys), tRNA(Val), tRNA(His), tRNA(Asp) and tRNA(Sec) (By similarity). Under growth conditions and in differentiated cells, translocates to the nucleus and stimulates ribosomal RNA (rRNA) transcription, including that containing the initiation site sequences of 45S rRNA, thereby promoting cell growth and proliferation (By similarity). Angiogenin induces vascularization of normal and malignant tissues via its ability to promote rRNA transcription (PubMed:8448182). Involved in hematopoietic stem and progenitor cell (HSPC) growth and survival by promoting rRNA transcription in growth conditions and inhibiting translation in response to stress, respectively (By similarity). Mediates the crosstalk between myeloid and intestinal epithelial cells to protect the intestinal epithelial barrier integrity: secreted by myeloid cells and promotes intestinal epithelial cells proliferation and survival (By similarity). Also mediates osteoclast-endothelial cell crosstalk in growing bone: produced by osteoclasts and protects the neighboring vascular cells against senescence by promoting rRNA transcription (By similarity).</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="1">Has weak tRNA ribonuclease activity by itself due to partial autoinhibition by its C-terminus, which folds into a short alpha-helix that partially occludes the substrate-binding site. In absence of stress, the ribonuclease activity is inhibited by RNH1 in the cytoplasm. In response to stress, dissociates from RNH1 in the cytoplasm and associates with cytoplasmic ribosomes with vacant A-sites: ribosomes directly activate the tRNA ribonuclease activity of ANG by refolding the C-terminal alpha-helix. In response to stress, the angiogenic activity of ANG is inhibited by RNH1 in the nucleus.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer. Interacts with RNH1; inhibiting ANG ribonuclease activity. Interacts with PCNA.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Nucleus</location>
      <location evidence="1">Nucleolus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
      <location evidence="1">Stress granule</location>
    </subcellularLocation>
    <text evidence="1">The secreted protein is rapidly endocytosed by target cells following interaction with PLXNB2 receptor and translocated to the cytoplasm and nucleus. In the nucleus, accumulates in the nucleolus and binds to DNA.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the pancreatic ribonuclease family.</text>
  </comment>
  <dbReference type="EC" id="3.1.27.-" evidence="1"/>
  <dbReference type="EMBL" id="DQ498979">
    <property type="protein sequence ID" value="ABF55381.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ498980">
    <property type="protein sequence ID" value="ABF55382.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU814501">
    <property type="protein sequence ID" value="ACJ26822.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S29834">
    <property type="entry name" value="A43825"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001038038.1">
    <property type="nucleotide sequence ID" value="NM_001044573.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_013846711.1">
    <property type="nucleotide sequence ID" value="XM_013991257.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P31346"/>
  <dbReference type="SMR" id="P31346"/>
  <dbReference type="IntAct" id="P31346">
    <property type="interactions" value="2"/>
  </dbReference>
  <dbReference type="PaxDb" id="9823-ENSSSCP00000020122"/>
  <dbReference type="PeptideAtlas" id="P31346"/>
  <dbReference type="Ensembl" id="ENSSSCT00000065893.3">
    <property type="protein sequence ID" value="ENSSSCP00000037223.2"/>
    <property type="gene ID" value="ENSSSCG00000059608.1"/>
  </dbReference>
  <dbReference type="GeneID" id="733639"/>
  <dbReference type="KEGG" id="ssc:733639"/>
  <dbReference type="CTD" id="283"/>
  <dbReference type="eggNOG" id="ENOG502S9Q1">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000162981"/>
  <dbReference type="HOGENOM" id="CLU_117006_3_1_1"/>
  <dbReference type="InParanoid" id="P31346"/>
  <dbReference type="OrthoDB" id="4612546at2759"/>
  <dbReference type="TreeFam" id="TF333393"/>
  <dbReference type="Reactome" id="R-SSC-418990">
    <property type="pathway name" value="Adherens junctions interactions"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000008227">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000314985">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694570">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694571">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694720">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694722">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694723">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694724">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694725">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694726">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694727">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694728">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSSSCG00000031538">
    <property type="expression patterns" value="Expressed in epididymis and 46 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030139">
    <property type="term" value="C:endocytic vesicle"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030426">
    <property type="term" value="C:growth cone"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043025">
    <property type="term" value="C:neuronal cell body"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005730">
    <property type="term" value="C:nucleolus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004519">
    <property type="term" value="F:endonuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042803">
    <property type="term" value="F:protein homodimerization activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004540">
    <property type="term" value="F:RNA nuclease activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004549">
    <property type="term" value="F:tRNA-specific ribonuclease activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001525">
    <property type="term" value="P:angiogenesis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071425">
    <property type="term" value="P:hematopoietic stem cell proliferation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043066">
    <property type="term" value="P:negative regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032055">
    <property type="term" value="P:negative regulation of translation in response to stress"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034063">
    <property type="term" value="P:stress granule assembly"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd06265">
    <property type="entry name" value="RNase_A_canonical"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.130.10:FF:000001">
    <property type="entry name" value="Ribonuclease pancreatic"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.130.10">
    <property type="entry name" value="Ribonuclease A-like domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001427">
    <property type="entry name" value="RNaseA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036816">
    <property type="entry name" value="RNaseA-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023411">
    <property type="entry name" value="RNaseA_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023412">
    <property type="entry name" value="RNaseA_domain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437:SF60">
    <property type="entry name" value="ANGIOGENIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437">
    <property type="entry name" value="RIBONUCLEASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00074">
    <property type="entry name" value="RnaseA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00794">
    <property type="entry name" value="RIBONUCLEASE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00092">
    <property type="entry name" value="RNAse_Pc"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54076">
    <property type="entry name" value="RNase A-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00127">
    <property type="entry name" value="RNASE_PANCREATIC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0037">Angiogenesis</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0217">Developmental protein</keyword>
  <keyword id="KW-0221">Differentiation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0255">Endonuclease</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-0652">Protein synthesis inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0346">Stress response</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000057157" description="Angiogenin" evidence="3">
    <location>
      <begin position="25"/>
      <end position="147"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Nucleolar localization signal" evidence="1">
    <location>
      <begin position="55"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="active site" description="Proton acceptor" evidence="1">
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="active site" description="Proton donor" evidence="1">
    <location>
      <position position="137"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="45"/>
    </location>
    <ligand>
      <name>tRNA</name>
      <dbReference type="ChEBI" id="CHEBI:17843"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="46"/>
    </location>
    <ligand>
      <name>tRNA</name>
      <dbReference type="ChEBI" id="CHEBI:17843"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="104"/>
    </location>
    <ligand>
      <name>tRNA</name>
      <dbReference type="ChEBI" id="CHEBI:17843"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="126"/>
    </location>
    <ligand>
      <name>tRNA</name>
      <dbReference type="ChEBI" id="CHEBI:17843"/>
    </ligand>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="50"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="63"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="81"/>
      <end position="130"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P03950"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P21570"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="8448182"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="147" mass="16505" checksum="C4CA7A36A8EF6C95" modified="2024-11-27" version="2" precursor="true">MVILLGPLLLVFMLGLGLAPLSLAKDEDRYTHFLTQHYDAKPKGRDGRYCESIMKQRGLTRPCKEVNTFIHGTRNDIKAICNDKNGEPYNNFRRSKSPFQITTCKHKGGSNRPPCGYRATAGFRTIAVACENGLPVHFDESFIITSQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>