<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-11-27" version="127" xmlns="http://uniprot.org/uniprot">
  <accession>P97885</accession>
  <name>CXCL5_RAT</name>
  <protein>
    <recommendedName>
      <fullName>C-X-C motif chemokine 5</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cytokine LIX</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Small-inducible cytokine B5</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Cxcl5</name>
    <name type="synonym">Scyb5</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1997-02" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Kelner G.S."/>
        <person name="Maciejewski-Lenoir D."/>
        <person name="Lee E.D."/>
        <person name="Maki R.A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>Sprague-Dawley</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">May participate in the recruitment of inflammatory cells by injured or infected tissue.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Monomer. Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the intercrine alpha (chemokine CxC) family.</text>
  </comment>
  <dbReference type="EMBL" id="U90448">
    <property type="protein sequence ID" value="AAB61460.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_071550.1">
    <property type="nucleotide sequence ID" value="NM_022214.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P97885"/>
  <dbReference type="SMR" id="P97885"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000003823"/>
  <dbReference type="PhosphoSitePlus" id="P97885"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000003823"/>
  <dbReference type="GeneID" id="60665"/>
  <dbReference type="KEGG" id="rno:60665"/>
  <dbReference type="UCSC" id="RGD:708540">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:708540"/>
  <dbReference type="CTD" id="6372"/>
  <dbReference type="RGD" id="708540">
    <property type="gene designation" value="Cxcl5"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S7MM">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="P97885"/>
  <dbReference type="OrthoDB" id="4170999at2759"/>
  <dbReference type="PhylomeDB" id="P97885"/>
  <dbReference type="Reactome" id="R-RNO-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P97885"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045236">
    <property type="term" value="F:CXCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031100">
    <property type="term" value="P:animal organ regeneration"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001776">
    <property type="term" value="P:leukocyte homeostasis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030901">
    <property type="term" value="P:midbrain development"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042119">
    <property type="term" value="P:neutrophil activation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030593">
    <property type="term" value="P:neutrophil chemotaxis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010976">
    <property type="term" value="P:positive regulation of neuron projection development"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046427">
    <property type="term" value="P:positive regulation of receptor signaling pathway via JAK-STAT"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032642">
    <property type="term" value="P:regulation of chemokine production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070951">
    <property type="term" value="P:regulation of neutrophil mediated killing of gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010332">
    <property type="term" value="P:response to gamma radiation"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032496">
    <property type="term" value="P:response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="CDD" id="cd00273">
    <property type="entry name" value="Chemokine_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000004">
    <property type="entry name" value="C-X-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001089">
    <property type="entry name" value="Chemokine_CXC"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018048">
    <property type="entry name" value="Chemokine_CXC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033899">
    <property type="entry name" value="CXC_Chemokine_domain"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF196">
    <property type="entry name" value="C-X-C MOTIF CHEMOKINE 6"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00436">
    <property type="entry name" value="INTERLEUKIN8"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00437">
    <property type="entry name" value="SMALLCYTKCXC"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00471">
    <property type="entry name" value="SMALL_CYTOKINES_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005081" description="C-X-C motif chemokine 5">
    <location>
      <begin position="38"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="50"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="52"/>
      <end position="93"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P42830"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="130" mass="14263" checksum="C00F6B3605524F4E" modified="1997-05-01" version="1" precursor="true">MSFQLRSSARIPSRSCSSFTLLAFLLLFTLPQHRAQAAPFSAMVATELRCVCLTLAPRINPKMIANLEVIPAGPHCPKVEVIAKLKNQKDNVCLDPQAPLIKKVIQKILGSENKKTKRNALALVRSASTQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>