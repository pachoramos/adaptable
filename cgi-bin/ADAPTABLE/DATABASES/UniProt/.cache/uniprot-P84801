<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-03-21" modified="2024-10-02" version="51" xmlns="http://uniprot.org/uniprot">
  <accession>P84801</accession>
  <name>GRFIN_GRISQ</name>
  <protein>
    <recommendedName>
      <fullName>Griffithsin</fullName>
      <shortName>GRFT</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Griffithsia sp. (strain Q66D336)</name>
    <name type="common">Red alga</name>
    <dbReference type="NCBI Taxonomy" id="373036"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Rhodophyta</taxon>
      <taxon>Florideophyceae</taxon>
      <taxon>Rhodymeniophycidae</taxon>
      <taxon>Ceramiales</taxon>
      <taxon>Ceramiaceae</taxon>
      <taxon>Griffithsia</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2005" name="J. Biol. Chem." volume="280" first="9345" last="9353">
      <title>Isolation and characterization of griffithsin, a novel HIV-inactivating protein, from the red alga Griffithsia sp.</title>
      <authorList>
        <person name="Mori T."/>
        <person name="O'Keefe B.R."/>
        <person name="Sowder R.C. II"/>
        <person name="Bringans S."/>
        <person name="Gardella R."/>
        <person name="Berg S."/>
        <person name="Cochran P."/>
        <person name="Turpin J.A."/>
        <person name="Buckheit R.W. Jr."/>
        <person name="McMahon J.B."/>
        <person name="Boyd M.R."/>
      </authorList>
      <dbReference type="PubMed" id="15613479"/>
      <dbReference type="DOI" id="10.1074/jbc.m411122200"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Mixed specificity lectin with anti-HIV activity. Binds to HIV envelope glycoproteins, including exterior membrane glycoprotein gp120, and inhibits viral entry into cells. Binding to gp120 is dependent on gp120 being glycosylated, and is inhibited by mannose, glucose and N-acetylglucosamine.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-8453570">
      <id>P84801</id>
    </interactant>
    <interactant intactId="EBI-8453570">
      <id>P84801</id>
      <label>-</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-8453570">
      <id>P84801</id>
    </interactant>
    <interactant intactId="EBI-8453491">
      <id>Q75760</id>
      <label>env</label>
    </interactant>
    <organismsDiffer>true</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="mass spectrometry" mass="12770.05" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="1 3">Belongs to the jacalin lectin family.</text>
  </comment>
  <dbReference type="PDB" id="2GTY">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.30 A"/>
    <property type="chains" value="A/B=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="2GUC">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.79 A"/>
    <property type="chains" value="A/B=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="2GUD">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="0.94 A"/>
    <property type="chains" value="A/B=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="2GUE">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.02 A"/>
    <property type="chains" value="A/B=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="2GUX">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="A=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="2HYQ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="A/B=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="2HYR">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.51 A"/>
    <property type="chains" value="A/B=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="2NU5">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.56 A"/>
    <property type="chains" value="A/B=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="2NUO">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.50 A"/>
    <property type="chains" value="A/B=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="3LKY">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.11 A"/>
    <property type="chains" value="A=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="3LL0">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="3LL1">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="0.97 A"/>
    <property type="chains" value="A=1-119"/>
  </dbReference>
  <dbReference type="PDB" id="3LL2">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="0.97 A"/>
    <property type="chains" value="A=1-121"/>
  </dbReference>
  <dbReference type="PDB" id="7RIA">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.80 A"/>
    <property type="chains" value="A/B=2-121"/>
  </dbReference>
  <dbReference type="PDB" id="7RIB">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.10 A"/>
    <property type="chains" value="A/B/C=2-121"/>
  </dbReference>
  <dbReference type="PDB" id="7RIC">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.95 A"/>
    <property type="chains" value="A=2-121"/>
  </dbReference>
  <dbReference type="PDB" id="7RID">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="A=2-121"/>
  </dbReference>
  <dbReference type="PDB" id="7RKG">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.20 A"/>
    <property type="chains" value="A/B=2-121"/>
  </dbReference>
  <dbReference type="PDB" id="7RKI">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.09 A"/>
    <property type="chains" value="A/B=2-121"/>
  </dbReference>
  <dbReference type="PDBsum" id="2GTY"/>
  <dbReference type="PDBsum" id="2GUC"/>
  <dbReference type="PDBsum" id="2GUD"/>
  <dbReference type="PDBsum" id="2GUE"/>
  <dbReference type="PDBsum" id="2GUX"/>
  <dbReference type="PDBsum" id="2HYQ"/>
  <dbReference type="PDBsum" id="2HYR"/>
  <dbReference type="PDBsum" id="2NU5"/>
  <dbReference type="PDBsum" id="2NUO"/>
  <dbReference type="PDBsum" id="3LKY"/>
  <dbReference type="PDBsum" id="3LL0"/>
  <dbReference type="PDBsum" id="3LL1"/>
  <dbReference type="PDBsum" id="3LL2"/>
  <dbReference type="PDBsum" id="7RIA"/>
  <dbReference type="PDBsum" id="7RIB"/>
  <dbReference type="PDBsum" id="7RIC"/>
  <dbReference type="PDBsum" id="7RID"/>
  <dbReference type="PDBsum" id="7RKG"/>
  <dbReference type="PDBsum" id="7RKI"/>
  <dbReference type="BMRB" id="P84801"/>
  <dbReference type="SMR" id="P84801"/>
  <dbReference type="DIP" id="DIP-29130N"/>
  <dbReference type="IntAct" id="P84801">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="MINT" id="P84801"/>
  <dbReference type="UniLectin" id="P84801"/>
  <dbReference type="EvolutionaryTrace" id="P84801"/>
  <dbReference type="GO" id="GO:0030246">
    <property type="term" value="F:carbohydrate binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005536">
    <property type="term" value="F:D-glucose binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005537">
    <property type="term" value="F:D-mannose binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0000353"/>
    <property type="project" value="IntAct"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046871">
    <property type="term" value="F:N-acetylgalactosamine binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.100.10.30">
    <property type="entry name" value="Jacalin-like lectin domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001229">
    <property type="entry name" value="Jacalin-like_lectin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036404">
    <property type="entry name" value="Jacalin-like_lectin_dom_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01419">
    <property type="entry name" value="Jacalin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00915">
    <property type="entry name" value="Jacalin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF51101">
    <property type="entry name" value="Mannose-binding lectins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51752">
    <property type="entry name" value="JACALIN_LECTIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0430">Lectin</keyword>
  <keyword id="KW-0465">Mannose-binding</keyword>
  <feature type="chain" id="PRO_0000228815" description="Griffithsin">
    <location>
      <begin position="1"/>
      <end position="121"/>
    </location>
  </feature>
  <feature type="domain" description="Jacalin-type lectin" evidence="1">
    <location>
      <begin position="1"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="2"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="12"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="20"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="37"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="44"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="58"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="81"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="90"/>
      <end position="120"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01088"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15613479"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="2GUD"/>
    </source>
  </evidence>
  <sequence length="121" mass="12731" checksum="F89843AF6DE5049D" modified="2006-03-21" version="1">SLTHRKFGGSGGSPFSGLSSIAVRSGSYLDXIIIDGVHHGGSGGNLSPTFTFGSGEYISNMTIRSGDYIDNISFETNMGRRFGPYGGSGGSANTLSNVKVIQINGSAGDYLDSLDIYYEQY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>