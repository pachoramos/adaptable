<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-05-16" modified="2024-11-27" version="58" xmlns="http://uniprot.org/uniprot">
  <accession>Q1XGV4</accession>
  <accession>A1YL65</accession>
  <name>ASIP_PONPY</name>
  <protein>
    <recommendedName>
      <fullName>Agouti-signaling protein</fullName>
      <shortName>ASP</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Agouti switch protein</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ASIP</name>
  </gene>
  <organism>
    <name type="scientific">Pongo pygmaeus</name>
    <name type="common">Bornean orangutan</name>
    <dbReference type="NCBI Taxonomy" id="9600"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Pongo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Genome Res." volume="16" first="485" last="490">
      <title>Alu-mediated 100-kb deletion in the primate genome: the loss of the agouti signaling protein gene in the lesser apes.</title>
      <authorList>
        <person name="Nakayama K."/>
        <person name="Ishida T."/>
      </authorList>
      <dbReference type="PubMed" id="16597585"/>
      <dbReference type="DOI" id="10.1101/gr.4763906"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="Mamm. Genome" volume="17" first="1205" last="1213">
      <title>Investigation of the role of the agouti signaling protein gene (ASIP) in coat color evolution in primates.</title>
      <authorList>
        <person name="Mundy N.I."/>
        <person name="Kelly J."/>
      </authorList>
      <dbReference type="PubMed" id="17143587"/>
      <dbReference type="DOI" id="10.1007/s00335-006-0056-0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Involved in the regulation of melanogenesis. The binding of ASP to MC1R precludes alpha-MSH initiated signaling and thus blocks production of cAMP, leading to a down-regulation of eumelanogenesis (brown/black pigment) and thus increasing synthesis of pheomelanin (yellow/red pigment) (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <dbReference type="EMBL" id="AB236872">
    <property type="protein sequence ID" value="BAE93020.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF094482">
    <property type="protein sequence ID" value="ABL84280.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q1XGV4"/>
  <dbReference type="GlyCosmos" id="Q1XGV4">
    <property type="glycosylation" value="1 site, No reported glycans"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031779">
    <property type="term" value="F:melanocortin receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009755">
    <property type="term" value="P:hormone-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042438">
    <property type="term" value="P:melanin biosynthetic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032438">
    <property type="term" value="P:melanosome organization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048023">
    <property type="term" value="P:positive regulation of melanin biosynthetic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.760.10:FF:000002">
    <property type="entry name" value="Agouti-signaling protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.760.10">
    <property type="entry name" value="Agouti domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007733">
    <property type="entry name" value="Agouti"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027300">
    <property type="entry name" value="Agouti_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036836">
    <property type="entry name" value="Agouti_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16551">
    <property type="entry name" value="AGOUTI RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16551:SF1">
    <property type="entry name" value="AGOUTI-SIGNALING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05039">
    <property type="entry name" value="Agouti"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00792">
    <property type="entry name" value="Agouti"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57055">
    <property type="entry name" value="Agouti-related protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60024">
    <property type="entry name" value="AGOUTI_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51150">
    <property type="entry name" value="AGOUTI_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="4">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000235192" description="Agouti-signaling protein">
    <location>
      <begin position="23"/>
      <end position="132"/>
    </location>
  </feature>
  <feature type="domain" description="Agouti" evidence="5">
    <location>
      <begin position="93"/>
      <end position="132"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="6">
    <location>
      <begin position="62"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="6">
    <location>
      <begin position="62"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="4">
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="93"/>
      <end position="108"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="100"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="107"/>
      <end position="125"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="111"/>
      <end position="132"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="116"/>
      <end position="123"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P42127"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="Q03288"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="4"/>
  <evidence type="ECO:0000255" key="5">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00494"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="6">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <sequence length="132" mass="14597" checksum="99087926CB92E0E9" modified="2006-05-02" version="1" precursor="true">MDVTRLLLATLLVFLCFFTADSHLPPEEKLRDDRSLRSNSSVNLLDFPSVSIVALNKKSKQISRKEAEKKRSSKKEASMKTVARPRTPLSAPCVATRNSCKPPAPACCDPCASCQCRFFRSACSCRVLSLNC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>