<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2015-10-14" modified="2023-02-22" version="12" xmlns="http://uniprot.org/uniprot">
  <accession>C0HJU5</accession>
  <name>AMP3_STEME</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Antimicrobial peptide 3</fullName>
      <shortName evidence="3">SmAMP3</shortName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Stellaria media</name>
    <name type="common">Common chickweed</name>
    <name type="synonym">Alsine media</name>
    <dbReference type="NCBI Taxonomy" id="13274"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>Caryophyllales</taxon>
      <taxon>Caryophyllaceae</taxon>
      <taxon>Alsineae</taxon>
      <taxon>Stellaria</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2015" name="Biochimie" volume="116" first="125" last="132">
      <title>A novel antifungal peptide from leaves of the weed Stellaria media L.</title>
      <authorList>
        <person name="Rogozhin E.A."/>
        <person name="Slezina M.P."/>
        <person name="Slavokhotova A.A."/>
        <person name="Istomina E.A."/>
        <person name="Korostyleva T.V."/>
        <person name="Smirnov A.N."/>
        <person name="Grishin E.V."/>
        <person name="Egorov T.A."/>
        <person name="Odintsova T.I."/>
      </authorList>
      <dbReference type="PubMed" id="26196691"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2015.07.014"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>PRESENCE OF DISULFIDE BONDS</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="3">Leaf</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Has antifungal activity against A.niger (IC(50)=5.4 uM), B.sorokiniana (IC(50)=2.0 uM), B.cinerea (IC(50)=1.6 uM), F.solani (IC(50)=3.7 uM) and A.alternata (IC(50)=5.0 uM). Binds chitin in vitro. Has no antibacterial activity at concentrations up to 10 uM.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed in leaf, flower, stem and seed with highest expression in leaf (at protein level).</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">Contains 3 disulfide bonds.</text>
  </comment>
  <comment type="mass spectrometry" mass="3364.9" method="MALDI" evidence="2"/>
  <dbReference type="AlphaFoldDB" id="C0HJU5"/>
  <dbReference type="SMR" id="C0HJU5"/>
  <dbReference type="GO" id="GO:0008061">
    <property type="term" value="F:chitin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.60.10">
    <property type="entry name" value="Endochitinase-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018371">
    <property type="entry name" value="Chitin-binding_1_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036861">
    <property type="entry name" value="Endochitinase-like_sf"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57016">
    <property type="entry name" value="Plant lectins/antimicrobial peptides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00026">
    <property type="entry name" value="CHIT_BIND_I_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0147">Chitin-binding</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000434197" description="Antimicrobial peptide 3">
    <location>
      <begin position="1"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="domain" description="Chitin-binding type-1" evidence="1">
    <location>
      <begin position="4"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="7"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="14"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="19"/>
      <end position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00261"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="26196691"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="26196691"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="35" mass="3371" checksum="B38BE1DC297F62F5" modified="2015-10-14" version="1">VGPGGECGGRFGGCAGGQCCSRFGFCGSGPKYCAH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>