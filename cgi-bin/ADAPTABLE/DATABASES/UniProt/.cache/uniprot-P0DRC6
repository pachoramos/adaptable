<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2024-03-27" modified="2024-10-02" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>P0DRC6</accession>
  <name>SCX13_CENSC</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Toxin NaTx-13</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Centruroides sculpturatus</name>
    <name type="common">Arizona bark scorpion</name>
    <dbReference type="NCBI Taxonomy" id="218467"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Centruroides</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2021" name="Toxins" volume="13">
      <title>Identification and characterization of novel proteins from Arizona Bark scorpion venom that inhibit Nav1.8, a voltage-gated sodium channel regulator of pain signaling.</title>
      <authorList>
        <person name="Abd El-Aziz T.M."/>
        <person name="Xiao Y."/>
        <person name="Kline J."/>
        <person name="Gridley H."/>
        <person name="Heaston A."/>
        <person name="Linse K.D."/>
        <person name="Ward M.J."/>
        <person name="Rokyta D.R."/>
        <person name="Stockand J.D."/>
        <person name="Cummins T.R."/>
        <person name="Fornelli L."/>
        <person name="Rowe A.H."/>
      </authorList>
      <dbReference type="PubMed" id="34357973"/>
      <dbReference type="DOI" id="10.3390/toxins13070501"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 36-61</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2022" name="Front. Pharmacol." volume="13" first="846992" last="846992">
      <title>Structural and functional characterization of a novel scorpion toxin that inhibits NaV1.8 via interactions with the DI voltage sensor and DII pore module.</title>
      <authorList>
        <person name="George K."/>
        <person name="Lopez-Mateos D."/>
        <person name="Abd El-Aziz T.M."/>
        <person name="Xiao Y."/>
        <person name="Kline J."/>
        <person name="Bao H."/>
        <person name="Raza S."/>
        <person name="Stockand J.D."/>
        <person name="Cummins T.R."/>
        <person name="Fornelli L."/>
        <person name="Rowe M.P."/>
        <person name="Yarov-Yarovoy V."/>
        <person name="Rowe A.H."/>
      </authorList>
      <dbReference type="PubMed" id="35662692"/>
      <dbReference type="DOI" id="10.3389/fphar.2022.846992"/>
    </citation>
    <scope>ACTIVITY ON NAV1.8/SCN10A CHANNEL</scope>
    <scope>SYNTHESIS</scope>
  </reference>
  <comment type="function">
    <text evidence="5">Probable sodium channel inhibitor.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="5">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="3">Negative results: has no effect on Nav1.8/SCN10A sodium channel from the grasshopper mouse, a species pain-resistant to C.sculpturatus venom.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family.</text>
  </comment>
  <dbReference type="SMR" id="P0DRC6"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000459710" description="Toxin NaTx-13" evidence="6">
    <location>
      <begin position="1"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="6"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="16"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="20"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="30"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="34"/>
      <end position="51"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="34357973"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="35662692"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="34357973"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="34357973"/>
    </source>
  </evidence>
  <sequence length="82" mass="9218" checksum="4B62876DC3650402" modified="2024-03-27" version="1">SKKEIPGGYPVNQFKCTYECAHADTDHIRCKNLCKKLGGSWGYCYWNTCYCEYLPDSVPQKNSIEVFSCGATIVGVPDTEQQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>