<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-01-15" modified="2024-11-27" version="50" xmlns="http://uniprot.org/uniprot">
  <accession>A7X4I7</accession>
  <name>WAP1_THRJA</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Waprin-Thr1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Thrasops jacksonii</name>
    <name type="common">Jackson's black tree snake</name>
    <dbReference type="NCBI Taxonomy" id="186611"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Colubridae</taxon>
      <taxon>Colubrinae</taxon>
      <taxon>Thrasops</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Mol. Cell. Proteomics" volume="7" first="215" last="246">
      <title>Evolution of an arsenal: structural and functional diversification of the venom system in the advanced snakes (Caenophidia).</title>
      <authorList>
        <person name="Fry B.G."/>
        <person name="Scheib H."/>
        <person name="van der Weerd L."/>
        <person name="Young B."/>
        <person name="McNaughtan J."/>
        <person name="Ramjan S.F.R."/>
        <person name="Vidal N."/>
        <person name="Poelmann R.E."/>
        <person name="Norman J.A."/>
      </authorList>
      <dbReference type="PubMed" id="17855442"/>
      <dbReference type="DOI" id="10.1074/mcp.m700094-mcp200"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Damages membranes of susceptible bacteria. Has no hemolytic activity. Not toxic to mice. Does not inhibit the proteinases elastase and cathepsin G.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the venom waprin family.</text>
  </comment>
  <dbReference type="EMBL" id="EU029740">
    <property type="protein sequence ID" value="ABU68540.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A7X4I7"/>
  <dbReference type="SMR" id="A7X4I7"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030414">
    <property type="term" value="F:peptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044278">
    <property type="term" value="P:disruption of cell wall in another organism"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.75.10:FF:000001">
    <property type="entry name" value="Anosmin 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.75.10">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036645">
    <property type="entry name" value="Elafin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008197">
    <property type="entry name" value="WAP_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00095">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00217">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57256">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51390">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000314691" description="Waprin-Thr1">
    <location>
      <begin position="20"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="domain" description="WAP" evidence="3">
    <location>
      <begin position="20"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="27"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="40"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="44"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="50"/>
      <end position="66"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P83952"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00722"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="17855442"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="17855442"/>
    </source>
  </evidence>
  <sequence length="70" mass="7488" checksum="41676CA2E4EAEAAC" modified="2007-10-23" version="1" precursor="true">MKARLLLLSVVILVGMVSAENEKAGSCPDVNQPIPPLGLCRNMCESDSGCPNNEKCCKNGCGFMTCSRPR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>