<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-06-17" modified="2022-05-25" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>A0A5Q0MU22</accession>
  <name>MDS_AGASP</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Medusin-AS</fullName>
      <shortName evidence="7">MDS-AS</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Agalychnis spurrelli</name>
    <name type="common">Gliding leaf frog</name>
    <name type="synonym">Agalychnis litodryas</name>
    <dbReference type="NCBI Taxonomy" id="317303"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Agalychnis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2019" name="Biomolecules" volume="9" first="1" last="20">
      <title>Unravelling the skin secretion peptides of the gliding leaf frog, Agalychnis spurrelli (Hylidae).</title>
      <authorList>
        <person name="Proano-Bolanos C."/>
        <person name="Blasco-Zuniga A."/>
        <person name="Almeida J.R."/>
        <person name="Wang L."/>
        <person name="Llumiquinga M.A."/>
        <person name="Rivera M."/>
        <person name="Zhou M."/>
        <person name="Chen T."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="31671555"/>
      <dbReference type="DOI" id="10.3390/biom9110667"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Antimicrobial peptide active against Gram-positive bacteria and fungi but inactive against Gram-negative bacteria. Also inhibits growth of B.dendrobatidis zoospores at high concentrations. Shows anticancer activities. Shows hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="8">Expressed by the skin glands.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="7">The primary structure of this peptide is identical to that of Phylloseptin-L1 (AC P0DQK9) and of medusin-AS (AC L0P329).</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the frog skin active peptide (FSAP) family. Medusin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=00973"/>
  </comment>
  <dbReference type="EMBL" id="MK766840">
    <property type="protein sequence ID" value="QFZ95566.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A5Q0MU22"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000449979" evidence="8">
    <location>
      <begin position="23"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000449980" description="Medusin-AS" evidence="8">
    <location>
      <begin position="50"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="4">
    <location>
      <begin position="24"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1 2">
    <location>
      <position position="67"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="L0P329"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P0DQK9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000256" key="4">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="31671555"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="31671555"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="31671555"/>
    </source>
  </evidence>
  <sequence length="68" mass="7729" checksum="14AE6852FCF37B79" modified="2020-02-26" version="1" precursor="true">MAFLKKSLFLVLFLGLVSLSVCEEEKRESEEEKNEQEEDDRDERSEEKRLLGMIPLAISAISALSKLG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>