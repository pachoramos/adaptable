<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-04-04" modified="2022-05-25" version="20" xmlns="http://uniprot.org/uniprot">
  <accession>Q5G8B0</accession>
  <name>NDB2U_TITCO</name>
  <protein>
    <recommendedName>
      <fullName>Anionic peptide clone 9</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Tityus costatus</name>
    <name type="common">Brazilian scorpion</name>
    <dbReference type="NCBI Taxonomy" id="309814"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Tityus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Toxicon" volume="45" first="273" last="283">
      <title>The Brazilian scorpion Tityus costatus Karsch: genes, peptides and function.</title>
      <authorList>
        <person name="Diego-Garcia E."/>
        <person name="Batista C.V.F."/>
        <person name="Garcia-Gomez B.I."/>
        <person name="Lucas S."/>
        <person name="Candido D.M."/>
        <person name="Gomez-Lagunas F."/>
        <person name="Possani L.D."/>
      </authorList>
      <dbReference type="PubMed" id="15683865"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2004.10.014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">May be an antimicrobial peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the non-disulfide-bridged peptide (NDBP) superfamily. Long chain multifunctional peptide (group 2) family.</text>
  </comment>
  <dbReference type="EMBL" id="AY740691">
    <property type="protein sequence ID" value="AAW72461.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5G8B0"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000231517" description="Anionic peptide clone 9">
    <location>
      <begin position="25"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="74" mass="8313" checksum="C132257F3D94C41C" modified="2005-03-01" version="1" precursor="true">MVSKSLIVLLLVSVLVSTFFTTEAYPASYDDDFDALDDLDGLDLDDLLDSEPADLVLLDMWANMLDSQDFEDFE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>