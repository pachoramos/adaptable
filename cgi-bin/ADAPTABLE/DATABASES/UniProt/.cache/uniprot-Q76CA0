<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-01-04" modified="2023-02-22" version="56" xmlns="http://uniprot.org/uniprot">
  <accession>Q76CA0</accession>
  <name>NA2G3_STIGI</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Delta-stichotoxin-Sgt3a</fullName>
      <shortName evidence="6">Delta-SHTX-Sgt3a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Gigantoxin III</fullName>
      <shortName>Gigt III</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="7">Gigantoxin-3</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Stichodactyla gigantea</name>
    <name type="common">Giant carpet anemone</name>
    <name type="synonym">Gigantic sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="230562"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Stichodactylidae</taxon>
      <taxon>Stichodactyla</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Biochim. Biophys. Acta" volume="1652" first="103" last="106">
      <title>Molecular cloning of an epidermal growth factor-like toxin and two sodium channel toxins from the sea anemone Stichodactyla gigantea.</title>
      <authorList>
        <person name="Honma T."/>
        <person name="Nagai H."/>
        <person name="Nagashima Y."/>
        <person name="Shiomi K."/>
      </authorList>
      <dbReference type="PubMed" id="14644045"/>
      <dbReference type="DOI" id="10.1016/j.bbapap.2003.08.007"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2003" name="Toxicon" volume="41" first="229" last="236">
      <title>An epidermal growth factor-like toxin and two sodium channel toxins from the sea anemone Stichodactyla gigantea.</title>
      <authorList>
        <person name="Shiomi K."/>
        <person name="Honma T."/>
        <person name="Ide M."/>
        <person name="Nagashima Y."/>
        <person name="Ishida M."/>
        <person name="Chino M."/>
      </authorList>
      <dbReference type="PubMed" id="12565742"/>
      <dbReference type="DOI" id="10.1016/s0041-0101(02)00281-7"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 36-83</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <tissue>Nematoblast</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Binds specifically to voltage-gated sodium channels (Nav), thereby delaying their inactivation during signal transduction.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="7">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="toxic dose">
    <text evidence="4">LD(50) is 120 ug/kg to crabs.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the sea anemone sodium channel inhibitory toxin family. Type II subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AB110015">
    <property type="protein sequence ID" value="BAD01580.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q76CA0"/>
  <dbReference type="SMR" id="Q76CA0"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00706">
    <property type="entry name" value="Toxin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000034827" evidence="4">
    <location>
      <begin position="20"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000034828" description="Delta-stichotoxin-Sgt3a" evidence="4">
    <location>
      <begin position="36"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000034829">
    <location>
      <position position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="38"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="40"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="61"/>
      <end position="79"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P19651"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="12565742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="12565742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="12565742"/>
    </source>
  </evidence>
  <sequence length="84" mass="9103" checksum="203B4AA977D22AD2" modified="2004-07-05" version="1" precursor="true">MAYLKIVLVALMLVVAVSAMRLSDQEDQDISVAKRAACKCDDDGPDIRSATLTGTVDLGSCNEGWEKCASFYTILADCCRRPRG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>