<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-11-15" modified="2022-05-25" version="51" xmlns="http://uniprot.org/uniprot">
  <accession>Q963C7</accession>
  <name>PEN3G_PENVA</name>
  <protein>
    <recommendedName>
      <fullName>Penaeidin-3g</fullName>
      <shortName>Pen-3g</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Penaeus vannamei</name>
    <name type="common">Whiteleg shrimp</name>
    <name type="synonym">Litopenaeus vannamei</name>
    <dbReference type="NCBI Taxonomy" id="6689"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Eucarida</taxon>
      <taxon>Decapoda</taxon>
      <taxon>Dendrobranchiata</taxon>
      <taxon>Penaeoidea</taxon>
      <taxon>Penaeidae</taxon>
      <taxon>Penaeus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Immunogenetics" volume="54" first="442" last="445">
      <title>Diversity of the penaeidin antimicrobial peptides in two shrimp species.</title>
      <authorList>
        <person name="Cuthbertson B.J."/>
        <person name="Shepard E.F."/>
        <person name="Chapman R.W."/>
        <person name="Gross P.S."/>
      </authorList>
      <dbReference type="PubMed" id="12242595"/>
      <dbReference type="DOI" id="10.1007/s00251-002-0487-z"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Hemocyte</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antibacterial and antifungal activity. Presents chitin-binding activity (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Cytoplasmic granule</location>
    </subcellularLocation>
    <text>Cytoplasmic granules of hemocytes and to a lesser extent in small granules of hemocytes.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the penaeidin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF390143">
    <property type="protein sequence ID" value="AAK77536.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q963C7"/>
  <dbReference type="SMR" id="Q963C7"/>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008061">
    <property type="term" value="F:chitin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009226">
    <property type="entry name" value="Penaeidin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05927">
    <property type="entry name" value="Penaeidin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0147">Chitin-binding</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000023512" description="Penaeidin-3g">
    <location>
      <begin position="20"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <feature type="modified residue" description="Serine amide" evidence="1">
    <location>
      <position position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="51"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="55"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="67"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="82" mass="8778" checksum="E60E09BB2C48E03D" modified="2001-12-01" version="1" precursor="true">MRLVVCLVFLASFALVCQGQVYKGGYTRPIPRPPPFVRPLPGGPISPYNGCPVSCRGISFSQARSCCSRLGRCCHVGKGYSG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>