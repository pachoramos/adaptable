<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-06-01" modified="2019-12-11" version="59" xmlns="http://uniprot.org/uniprot">
  <accession>P36885</accession>
  <name>SK1_PERAM</name>
  <protein>
    <recommendedName>
      <fullName>Sulfakinin-1</fullName>
      <shortName>PerAm-SK-1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Perisulfakinin</fullName>
      <shortName>Pea-SK-I</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Periplaneta americana</name>
    <name type="common">American cockroach</name>
    <name type="synonym">Blatta americana</name>
    <dbReference type="NCBI Taxonomy" id="6978"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Dictyoptera</taxon>
      <taxon>Blattodea</taxon>
      <taxon>Blattoidea</taxon>
      <taxon>Blattidae</taxon>
      <taxon>Blattinae</taxon>
      <taxon>Periplaneta</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1989" name="Neuropeptides" volume="14" first="145" last="149">
      <title>Isolation and structure of two gastrin/CCK-like neuropeptides from the American cockroach homologous to the leucosulfakinins.</title>
      <authorList>
        <person name="Veenstra J.A."/>
      </authorList>
      <dbReference type="PubMed" id="2615921"/>
      <dbReference type="DOI" id="10.1016/0143-4179(89)90038-3"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-11</scope>
    <scope>SULFATION AT TYR-6</scope>
    <source>
      <tissue>Corpora cardiaca</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1999" name="Eur. J. Biochem." volume="263" first="552" last="560">
      <title>Post-translational modifications of the insect sulfakinins: sulfation, pyroglutamate-formation and O-methylation of glutamic acid.</title>
      <authorList>
        <person name="Predel R."/>
        <person name="Brandt W."/>
        <person name="Kellner R."/>
        <person name="Rapus J."/>
        <person name="Nachman R.J."/>
        <person name="Gaede G."/>
      </authorList>
      <dbReference type="PubMed" id="10406966"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.1999.00532.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-11</scope>
    <scope>SULFATION AT TYR-6</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2009" name="BMC Evol. Biol." volume="9" first="50" last="50">
      <title>A proteomic approach for studying insect phylogeny: CAPA peptides of ancient insect taxa (Dictyoptera, Blattoptera) as a test case.</title>
      <authorList>
        <person name="Roth S."/>
        <person name="Fromm B."/>
        <person name="Gaede G."/>
        <person name="Predel R."/>
      </authorList>
      <dbReference type="PubMed" id="19257902"/>
      <dbReference type="DOI" id="10.1186/1471-2148-9-50"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-11</scope>
    <source>
      <tissue>Corpora cardiaca</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Stimulates hindgut contractions.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the gastrin/cholecystokinin family.</text>
  </comment>
  <comment type="caution">
    <text evidence="4">PubMed:10406966 reported, in addition to the presence of glutamate methyl ester, the partial formation of pyroglutamic acid by the N-terminal glutamic acid. These are most probably artifacts of isolation.</text>
  </comment>
  <dbReference type="PIR" id="A60656">
    <property type="entry name" value="A60656"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013152">
    <property type="entry name" value="Gastrin/cholecystokinin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013259">
    <property type="entry name" value="Sulfakinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08257">
    <property type="entry name" value="Sulfakinin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00259">
    <property type="entry name" value="GASTRIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0765">Sulfation</keyword>
  <feature type="peptide" id="PRO_0000043893" description="Sulfakinin-1">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="Sulfotyrosine" evidence="1 3">
    <location>
      <position position="6"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1 2 3">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10406966"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19257902"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="2615921"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="11" mass="1445" checksum="8B4E0680E86B5AAA" modified="1994-06-01" version="1">EQFDDYGHMRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>