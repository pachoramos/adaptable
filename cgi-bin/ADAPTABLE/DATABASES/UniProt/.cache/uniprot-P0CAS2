<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-06-16" modified="2024-11-27" version="48" xmlns="http://uniprot.org/uniprot">
  <accession>P0CAS2</accession>
  <name>PA2B6_CRODO</name>
  <protein>
    <recommendedName>
      <fullName>Phospholipase A2 crotoxin basic chain</fullName>
      <shortName>CB</shortName>
      <shortName>Crotoxin-B</shortName>
      <shortName>svPLA2</shortName>
      <ecNumber>3.1.1.4</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>CdcolF6a</fullName>
      <shortName>F6a</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Phosphatidylcholine 2-acylhydrolase</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Crotalus durissus collilineatus</name>
    <name type="common">Brazilian rattlesnake</name>
    <dbReference type="NCBI Taxonomy" id="221569"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Viperidae</taxon>
      <taxon>Crotalinae</taxon>
      <taxon>Crotalus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2007" name="Protein J." volume="26" first="221" last="230">
      <title>Biological and structural characterization of crotoxin and new isoform of crotoxin B PLA(2) (F6a) from Crotalus durissus collilineatus snake venom.</title>
      <authorList>
        <person name="Ponce-Soto L.A."/>
        <person name="Lomonte B."/>
        <person name="Rodrigues-Simioni L."/>
        <person name="Novello J.C."/>
        <person name="Marangoni S."/>
      </authorList>
      <dbReference type="PubMed" id="17203389"/>
      <dbReference type="DOI" id="10.1007/s10930-006-9063-y"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="submission" date="2016-06" db="UniProtKB">
      <title>Proteome of Crotalus durissus collilineatus venoms: analysis of individual variations.</title>
      <authorList>
        <person name="Oliveira I."/>
        <person name="Boldrini-Franca J."/>
        <person name="Bordon K."/>
        <person name="Cardoso I."/>
        <person name="Pucca M."/>
        <person name="Carone S."/>
        <person name="Zoccal K."/>
        <person name="Frantz F."/>
        <person name="Faccioli L."/>
        <person name="Sampaio S."/>
        <person name="Rosa J."/>
        <person name="Arantes E."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE OF 1-60</scope>
    <scope>VARIANTS LYS-15 AND PHE-20</scope>
    <source>
      <tissue evidence="9">Venom</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2005" name="Protein J." volume="24" first="103" last="112">
      <title>Biological and structural characterization of a new PLA2 from the Crotalus durissus collilineatus venom.</title>
      <authorList>
        <person name="Toyama M.H."/>
        <person name="Toyama D.O."/>
        <person name="Joazeiro P.P."/>
        <person name="Carneiro E.M."/>
        <person name="Beriam L.O."/>
        <person name="Marangoni L.S."/>
        <person name="Boschero A.C."/>
      </authorList>
      <dbReference type="PubMed" id="16003952"/>
      <dbReference type="DOI" id="10.1007/s10930-004-1517-5"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2008" name="Toxicon" volume="51" first="80" last="92">
      <title>Systemic and local myotoxicity induced by snake venom group II phospholipases A2: comparison between crotoxin, crotoxin B and a Lys49 PLA2 homologue.</title>
      <authorList>
        <person name="Gutierrez J.M."/>
        <person name="Ponce-Soto L.A."/>
        <person name="Marangoni S."/>
        <person name="Lomonte B."/>
      </authorList>
      <dbReference type="PubMed" id="17915277"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2007.08.007"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2009" name="Acta Crystallogr. F" volume="65" first="1011" last="1013">
      <title>Crystallization and preliminary X-ray diffraction analysis of crotoxin B from Crotalus durissus collilineatus venom.</title>
      <authorList>
        <person name="Salvador G.H."/>
        <person name="Fernandes C.A."/>
        <person name="Correa L.C."/>
        <person name="Santos-Filho N.A."/>
        <person name="Soares A.M."/>
        <person name="Fontes M.R."/>
      </authorList>
      <dbReference type="PubMed" id="19851009"/>
      <dbReference type="DOI" id="10.1107/s1744309109032631"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.2 ANGSTROMS)</scope>
  </reference>
  <comment type="function">
    <text evidence="1 5 6">Heterodimer CA-CB: Crotoxin is a potent presynaptic neurotoxin that possesses phospholipase A2 (PLA2) activity and exerts a lethal action by blocking neuromuscular transmission (PubMed:16003952, PubMed:17203389). It consists of a non-covalent association of a basic and weakly toxic PLA2 subunit (CB), with a small acidic, non-enzymatic and non-toxic subunit (CA also named crotapotin). The complex acts by binding to a specific 48-kDa protein (R48) receptor located on presynaptic membranes, forming a transient ternary complex CA-CB-R48, followed by dissociation of the CA-CB complex and release of the CA subunit. At equilibrium, only the CB subunits remain associated with the specific crotoxin receptor. In addition to neurotoxicity, crotoxin has been found to exert nephrotoxicity, and cardiovascular toxicity. Moreover, anti-inflammatory, immunomodulatory, anti-tumor and analgesic effects of crotoxin have also been reported (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="1 5 6 7">Monomer CB: The basic subunit of crotoxin is a snake venom phospholipase A2 (PLA2) that exhibits weak neurotoxicity (PubMed:17203389, PubMed:16003952) and strong anticoagulant effects by binding to factor Xa (F10) and inhibiting the prothrombinase activity (By similarity). In addition, it exerts myotoxicity (PubMed:17203389, PubMed:16003952, PubMed:17915277), nephrotoxicity, and cardiovascular toxicity as well as anti-inflammatory, immunomodulatory, anti-tumor and analgesic effects (By similarity). Also shows a strong antimicrobial activity against X.axonopodis passiforae (Gram-negative) which is completely dependent on the enzymatic activity (PubMed:16003952). PLA2 catalyzes the calcium-dependent hydrolysis of the 2- acyl groups in 3-sn-phosphoglycerides.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="3 4">
      <text>a 1,2-diacyl-sn-glycero-3-phosphocholine + H2O = a 1-acyl-sn-glycero-3-phosphocholine + a fatty acid + H(+)</text>
      <dbReference type="Rhea" id="RHEA:15801"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:28868"/>
      <dbReference type="ChEBI" id="CHEBI:57643"/>
      <dbReference type="ChEBI" id="CHEBI:58168"/>
      <dbReference type="EC" id="3.1.1.4"/>
    </reaction>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="1">
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </cofactor>
    <text evidence="1">Binds 1 Ca(2+) ion.</text>
  </comment>
  <comment type="subunit">
    <text evidence="6">Heterodimer of one acidic (CA also named crotapotin) and one basic (CB) subunits; non-covalently linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="14943.14" method="MALDI" evidence="6"/>
  <comment type="similarity">
    <text evidence="10">Belongs to the phospholipase A2 family. Group II subfamily. D49 sub-subfamily.</text>
  </comment>
  <dbReference type="EC" id="3.1.1.4"/>
  <dbReference type="AlphaFoldDB" id="P0CAS2"/>
  <dbReference type="SMR" id="P0CAS2"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0047498">
    <property type="term" value="F:calcium-dependent phospholipase A2 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005543">
    <property type="term" value="F:phospholipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050482">
    <property type="term" value="P:arachidonic acid secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016042">
    <property type="term" value="P:lipid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042130">
    <property type="term" value="P:negative regulation of T cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006644">
    <property type="term" value="P:phospholipid metabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00125">
    <property type="entry name" value="PLA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.90.10:FF:000001">
    <property type="entry name" value="Basic phospholipase A2 homolog"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.90.10">
    <property type="entry name" value="Phospholipase A2 domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001211">
    <property type="entry name" value="PLipase_A2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033112">
    <property type="entry name" value="PLipase_A2_Asp_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016090">
    <property type="entry name" value="PLipase_A2_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036444">
    <property type="entry name" value="PLipase_A2_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033113">
    <property type="entry name" value="PLipase_A2_His_AS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716:SF57">
    <property type="entry name" value="GROUP IID SECRETORY PHOSPHOLIPASE A2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716">
    <property type="entry name" value="PHOSPHOLIPASE A2 FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00068">
    <property type="entry name" value="Phospholip_A2_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00389">
    <property type="entry name" value="PHPHLIPASEA2"/>
  </dbReference>
  <dbReference type="SMART" id="SM00085">
    <property type="entry name" value="PA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48619">
    <property type="entry name" value="Phospholipase A2, PLA2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00119">
    <property type="entry name" value="PA2_ASP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00118">
    <property type="entry name" value="PA2_HIS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0442">Lipid degradation</keyword>
  <keyword id="KW-0443">Lipid metabolism</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0638">Presynaptic neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000376919" description="Phospholipase A2 crotoxin basic chain">
    <location>
      <begin position="1"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="active site" evidence="2">
    <location>
      <position position="47"/>
    </location>
  </feature>
  <feature type="active site" evidence="2">
    <location>
      <position position="89"/>
    </location>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="27"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="29"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="31"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="48"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="26"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="28"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="43"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="49"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="50"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="57"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="75"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="sequence variant" evidence="8">
    <original>R</original>
    <variation>K</variation>
    <location>
      <position position="15"/>
    </location>
  </feature>
  <feature type="sequence variant" evidence="8">
    <original>P</original>
    <variation>F</variation>
    <location>
      <position position="20"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P62022"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10035"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="4">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10036"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="16003952"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="17203389"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="17915277"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <sequence length="122" mass="14264" checksum="ABD31823D4E2EBFD" modified="2009-06-16" version="1">HLLQFNKMIKFETRRNAIPPYAFYGCYCGWGGRGRPKDATDRCCFVHDCCYGKLAKCNTKWDFYRYSLKSGYITCGKGTWCEEQICECDRVAAECLRRSLSTYRYGYMIYPDSRCRGPSETC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>