<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-11-27" version="119" xmlns="http://uniprot.org/uniprot">
  <accession>P45881</accession>
  <accession>Q6EMI4</accession>
  <name>PA2B2_BOTJR</name>
  <protein>
    <recommendedName>
      <fullName evidence="10 12">Basic phospholipase A2 homolog bothropstoxin-II</fullName>
      <shortName evidence="9">Bothropstoxin II</shortName>
      <shortName evidence="9 10 12">BthTX-II</shortName>
      <shortName>BtxtxII</shortName>
      <shortName>svPLA2 homolog</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="15">BJU-PLA2</fullName>
      <shortName evidence="11">BJUPLA2</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="9">BOJU-II</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Myotoxic phospholipase A2</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bothrops jararacussu</name>
    <name type="common">Jararacussu</name>
    <dbReference type="NCBI Taxonomy" id="8726"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Viperidae</taxon>
      <taxon>Crotalinae</taxon>
      <taxon>Bothrops</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="J. Mol. Evol." volume="41" first="174" last="179">
      <title>The molecular cloning of a phospholipase A2 from Bothrops jararacussu snake venom: evolution of venom group II phospholipase A2's may imply gene duplications.</title>
      <authorList>
        <person name="Moura da Silva A.M."/>
        <person name="Paine M.J.I."/>
        <person name="Diniz M.R.D."/>
        <person name="Theakston R.D.G."/>
        <person name="Crampton J.M."/>
      </authorList>
      <dbReference type="PubMed" id="7666446"/>
      <dbReference type="DOI" id="10.1007/bf00170670"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Biochimie" volume="86" first="211" last="219">
      <title>Analysis of Bothrops jararacussu venomous gland transcriptome focusing on structural and functional aspects: I -- gene expression profile of highly expressed phospholipases A2.</title>
      <authorList>
        <person name="Kashima S."/>
        <person name="Roberto P.G."/>
        <person name="Soares A.M."/>
        <person name="Astolfi-Filho S."/>
        <person name="Pereira J.O."/>
        <person name="Giuliati S."/>
        <person name="Faria M. Jr."/>
        <person name="Xavier M.A.S."/>
        <person name="Fontes M.R.M."/>
        <person name="Giglio J.R."/>
        <person name="Franca S.C."/>
      </authorList>
      <dbReference type="PubMed" id="15134836"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2004.02.002"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1998" name="J. Protein Chem." volume="17" first="381" last="386">
      <title>The amino acid sequence of bothropstoxin-II, an Asp-49 myotoxin from Bothrops jararacussu (Jararacucu) venom with low phospholipase A2 activity.</title>
      <authorList>
        <person name="Pereira M.F."/>
        <person name="Novello J.C."/>
        <person name="Cintra A.C."/>
        <person name="Giglio J.R."/>
        <person name="Landucci E.T."/>
        <person name="Oliveira B."/>
        <person name="Marangoni S."/>
      </authorList>
      <dbReference type="PubMed" id="9619591"/>
      <dbReference type="DOI" id="10.1023/a:1022563401413"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 17-138</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1991" name="Exp. Mol. Pathol." volume="55" first="217" last="229">
      <title>Skeletal muscle degeneration and regeneration after injection of bothropstoxin-II, a phospholipase A2 isolated from the venom of the snake Bothrops jararacussu.</title>
      <authorList>
        <person name="Gutierrez J.M."/>
        <person name="Nunez J."/>
        <person name="Diaz C."/>
        <person name="Cintra A.C."/>
        <person name="Homsi-Brandeburgo M.I."/>
        <person name="Giglio J.R."/>
      </authorList>
      <dbReference type="PubMed" id="1660822"/>
      <dbReference type="DOI" id="10.1016/0014-4800(91)90002-f"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>BIOASSAY</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2000" name="Biochimie" volume="82" first="755" last="763">
      <title>Myotoxic phospholipases A(2) in bothrops snake venoms: effect of chemical modifications on the enzymatic and pharmacological properties of bothropstoxins from Bothrops jararacussu.</title>
      <authorList>
        <person name="Andriao-Escarso S.H."/>
        <person name="Soares A.M."/>
        <person name="Rodrigues V.M."/>
        <person name="Angulo Y."/>
        <person name="Diaz C."/>
        <person name="Lomonte B."/>
        <person name="Gutierrez J.M."/>
        <person name="Giglio J.R."/>
      </authorList>
      <dbReference type="PubMed" id="11018293"/>
      <dbReference type="DOI" id="10.1016/s0300-9084(00)01150-0"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>BIOASSAY</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2018" name="Int. Immunopharmacol." volume="55" first="128" last="132">
      <title>Asp49-phospholipase A2-loaded liposomes as experimental therapy in cutaneous leishmaniasis model.</title>
      <authorList>
        <person name="de Barros N.B."/>
        <person name="Aragao Macedo S.R."/>
        <person name="Ferreira A.S."/>
        <person name="Tagliari M.P."/>
        <person name="Kayano A.M."/>
        <person name="Nicolete L.D.F."/>
        <person name="Soares A.M."/>
        <person name="Nicolete R."/>
      </authorList>
      <dbReference type="PubMed" id="29253818"/>
      <dbReference type="DOI" id="10.1016/j.intimp.2017.12.012"/>
    </citation>
    <scope>PHARMACEUTICAL</scope>
  </reference>
  <reference evidence="17" key="7">
    <citation type="journal article" date="2008" name="Biochim. Biophys. Acta" volume="1784" first="591" last="599">
      <title>Crystal structure of a myotoxic Asp49-phospholipase A2 with low catalytic activity: insights into Ca2+-independent catalytic mechanism.</title>
      <authorList>
        <person name="Correa L.C."/>
        <person name="Marchi-Salvador D.P."/>
        <person name="Cintra A.C."/>
        <person name="Sampaio S.V."/>
        <person name="Soares A.M."/>
        <person name="Fontes M.R."/>
      </authorList>
      <dbReference type="PubMed" id="18261474"/>
      <dbReference type="DOI" id="10.1016/j.bbapap.2008.01.007"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.19 ANGSTROMS) OF 17-138</scope>
    <scope>DISULFIDE BONDS</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference evidence="18" key="8">
    <citation type="journal article" date="2011" name="Proteins" volume="79" first="61" last="78">
      <title>Structural, functional, and bioinformatics studies reveal a new snake venom homologue phospholipase A(2) class.</title>
      <authorList>
        <person name="dos Santos J.I."/>
        <person name="Cintra-Francischinelli M."/>
        <person name="Borges R.J."/>
        <person name="Fernandes C.A."/>
        <person name="Pizzo P."/>
        <person name="Cintra A.C."/>
        <person name="Braz A.S."/>
        <person name="Soares A.M."/>
        <person name="Fontes M.R."/>
      </authorList>
      <dbReference type="PubMed" id="20878713"/>
      <dbReference type="DOI" id="10.1002/prot.22858"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.10 ANGSTROMS) OF 17-138</scope>
    <scope>SUBUNIT</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="1 4 6 8">Snake venom phospholipase A2 (PLA2) that shows low enzymatic activity even tough it conserves the catalytic residues (PubMed:9619591). Shows a strong myotoxic activity and induces indirect hemolysis, anticoagulant properties, and cytotoxic activities (PubMed:11018293). In vivo, it induces muscle necrosis, accompanied by polymorphonuclear cell infiltration, and edema in the mouse paw (PubMed:11018293, PubMed:1660822). It exerts its function even in the absence of extracellular calcium, indicating it is not a calcium-dependent enzyme (PubMed:20878713). A model of myotoxic mechanism has been proposed: an apo Lys49-PLA2 is activated by the entrance of a hydrophobic molecule (e.g. fatty acid) at the hydrophobic channel of the protein leading to a reorientation of a monomer (By similarity). This reorientation causes a transition between 'inactive' to 'active' states, causing alignment of C-terminal and membrane-docking sites (MDoS) side-by-side and putting the membrane-disruption sites (MDiS) in the same plane, exposed to solvent and in a symmetric position for both monomers (By similarity). The MDoS region stabilizes the toxin on membrane by the interaction of charged residues with phospholipid head groups (By similarity). Subsequently, the MDiS region destabilizes the membrane with penetration of hydrophobic residues (By similarity). This insertion causes a disorganization of the membrane, allowing an uncontrolled influx of ions (i.e. calcium and sodium), and eventually triggering irreversible intracellular alterations and cell death (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="6">Homodimer; non-covalently linked (probable alternative/compact dimer conformation).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="16">Expressed by the venom gland.</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="4">LD(50) is 7.0 mg/kg by intraperitoneal injection into mice.</text>
  </comment>
  <comment type="pharmaceutical">
    <text evidence="7">Has been tested as therapy in mice cutaneous leishmaniasis model. Incorporated in liposomes, it is able to decrease the size of the mice paw injury and induce a marked inhibition for Leishmania amazonensis amastigotes in lymph node and paw tissues. It also induces the production of inflammatory mediators, such as TNF-alpha and nitric oxid (NO), that probably are responsible of the leishmaniasis infection decrease.</text>
  </comment>
  <comment type="similarity">
    <text evidence="13">Belongs to the phospholipase A2 family. Group II subfamily. D49 sub-subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="14">Shows no or low enzymatic activity even tough it conserves the catalytic residues. This may be due to the distorsion of the calcium binding loop.</text>
  </comment>
  <dbReference type="EMBL" id="X76289">
    <property type="protein sequence ID" value="CAA53921.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY185201">
    <property type="protein sequence ID" value="AAO27454.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="I50098">
    <property type="entry name" value="I50098"/>
  </dbReference>
  <dbReference type="PDB" id="2OQD">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.19 A"/>
    <property type="chains" value="A/B=17-138"/>
  </dbReference>
  <dbReference type="PDB" id="3JR8">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.10 A"/>
    <property type="chains" value="A/B=17-138"/>
  </dbReference>
  <dbReference type="PDB" id="7RJI">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.71 A"/>
    <property type="chains" value="A=17-138"/>
  </dbReference>
  <dbReference type="PDB" id="7RJZ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A/B=17-138"/>
  </dbReference>
  <dbReference type="PDBsum" id="2OQD"/>
  <dbReference type="PDBsum" id="3JR8"/>
  <dbReference type="PDBsum" id="7RJI"/>
  <dbReference type="PDBsum" id="7RJZ"/>
  <dbReference type="AlphaFoldDB" id="P45881"/>
  <dbReference type="SMR" id="P45881"/>
  <dbReference type="ABCD" id="P45881">
    <property type="antibodies" value="5 sequenced antibodies"/>
  </dbReference>
  <dbReference type="BRENDA" id="3.1.1.4">
    <property type="organism ID" value="6809"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P45881"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0047498">
    <property type="term" value="F:calcium-dependent phospholipase A2 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005543">
    <property type="term" value="F:phospholipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050482">
    <property type="term" value="P:arachidonic acid secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016042">
    <property type="term" value="P:lipid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042130">
    <property type="term" value="P:negative regulation of T cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006644">
    <property type="term" value="P:phospholipid metabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00125">
    <property type="entry name" value="PLA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.90.10:FF:000001">
    <property type="entry name" value="Basic phospholipase A2 homolog"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.90.10">
    <property type="entry name" value="Phospholipase A2 domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001211">
    <property type="entry name" value="PLipase_A2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033112">
    <property type="entry name" value="PLipase_A2_Asp_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016090">
    <property type="entry name" value="PLipase_A2_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036444">
    <property type="entry name" value="PLipase_A2_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033113">
    <property type="entry name" value="PLipase_A2_His_AS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716:SF57">
    <property type="entry name" value="GROUP IID SECRETORY PHOSPHOLIPASE A2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716">
    <property type="entry name" value="PHOSPHOLIPASE A2 FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00068">
    <property type="entry name" value="Phospholip_A2_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00389">
    <property type="entry name" value="PHPHLIPASEA2"/>
  </dbReference>
  <dbReference type="SMART" id="SM00085">
    <property type="entry name" value="PA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48619">
    <property type="entry name" value="Phospholipase A2, PLA2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00119">
    <property type="entry name" value="PA2_ASP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00118">
    <property type="entry name" value="PA2_HIS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-1203">Blood coagulation cascade inhibiting toxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1199">Hemostasis impairing toxin</keyword>
  <keyword id="KW-0959">Myotoxin</keyword>
  <keyword id="KW-0582">Pharmaceutical</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="8">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000022821" description="Basic phospholipase A2 homolog bothropstoxin-II">
    <location>
      <begin position="17"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="region of interest" description="Important for membrane-damaging activities in eukaryotes and bacteria; heparin-binding" evidence="3">
    <location>
      <begin position="121"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="site" description="Putative hydrophobic membrane-disrupting site (MDiS)" evidence="2">
    <location>
      <position position="19"/>
    </location>
  </feature>
  <feature type="site" description="Putative hydrophobic membrane-disrupting site (MDiS)" evidence="2">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="site" description="Putative membrane-disrupting site (MDiS)" evidence="2">
    <location>
      <position position="31"/>
    </location>
  </feature>
  <feature type="site" description="Putative hydrophobic membrane-disrupting site (MDiS)" evidence="2">
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="site" description="Putative cationic membrane-docking site (MDoS)" evidence="2">
    <location>
      <position position="79"/>
    </location>
  </feature>
  <feature type="site" description="Important residue of the cationic membrane-docking site (MDoS)" evidence="1">
    <location>
      <position position="121"/>
    </location>
  </feature>
  <feature type="site" description="Hydrophobic membrane-disruption site (MDiS)" evidence="1">
    <location>
      <position position="130"/>
    </location>
  </feature>
  <feature type="site" description="Cationic membrane-docking site (MDoS)" evidence="1">
    <location>
      <position position="133"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5 6 19 20">
    <location>
      <begin position="42"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5 6 19 20">
    <location>
      <begin position="44"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5 6 19 20">
    <location>
      <begin position="59"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5 6 19 20">
    <location>
      <begin position="65"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5 6 19 20">
    <location>
      <begin position="66"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5 6 19 20">
    <location>
      <begin position="73"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5 6 19 20">
    <location>
      <begin position="91"/>
      <end position="102"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="13" ref="3">
    <original>F</original>
    <variation>W</variation>
    <location>
      <position position="21"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="13" ref="3">
    <original>QGQPKDA</original>
    <variation>RGKPVDP</variation>
    <location>
      <begin position="49"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="13" ref="3">
    <original>GKLTNCKPKT</original>
    <variation>KVTNYCPKKN</variation>
    <location>
      <begin position="68"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="13" ref="3">
    <original>ENGVIICGEGTPCEK</original>
    <variation>VSYNYCRGGPCDE</variation>
    <location>
      <begin position="85"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="13" ref="3">
    <original>A</original>
    <variation>I</variation>
    <location>
      <position position="108"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="13" ref="3">
    <original>R</original>
    <variation>G</variation>
    <location>
      <position position="117"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="13" ref="3">
    <original>KKRYMAYPDVLCKK</original>
    <variation>NKKAYYHLKPFCKE</variation>
    <location>
      <begin position="120"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="13" ref="3">
    <original>K</original>
    <variation>T</variation>
    <location>
      <position position="137"/>
    </location>
  </feature>
  <feature type="helix" evidence="22">
    <location>
      <begin position="18"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="helix" evidence="22">
    <location>
      <begin position="33"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="turn" evidence="22">
    <location>
      <begin position="37"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="turn" evidence="22">
    <location>
      <begin position="41"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="helix" evidence="22">
    <location>
      <begin position="44"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="helix" evidence="22">
    <location>
      <begin position="55"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="turn" evidence="22">
    <location>
      <begin position="75"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="strand" evidence="21">
    <location>
      <begin position="82"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="strand" evidence="21">
    <location>
      <begin position="88"/>
      <end position="91"/>
    </location>
  </feature>
  <feature type="helix" evidence="22">
    <location>
      <begin position="96"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="helix" evidence="22">
    <location>
      <begin position="116"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="helix" evidence="22">
    <location>
      <begin position="121"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="helix" evidence="22">
    <location>
      <begin position="128"/>
      <end position="130"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="I6L8L6"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P20474"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P24605"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="11018293"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="18261474"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="20878713"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="29253818"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="9619591"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="15134836"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="10">
    <source>
      <dbReference type="PubMed" id="1660822"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="11">
    <source>
      <dbReference type="PubMed" id="7666446"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="9619591"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13"/>
  <evidence type="ECO:0000305" key="14">
    <source>
      <dbReference type="PubMed" id="20878713"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="15">
    <source>
      <dbReference type="PubMed" id="7666446"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="16">
    <source>
      <dbReference type="PubMed" id="9619591"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="17">
    <source>
      <dbReference type="PDB" id="2OQD"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="18">
    <source>
      <dbReference type="PDB" id="3JR8"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="19">
    <source>
      <dbReference type="PDB" id="2OQD"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="20">
    <source>
      <dbReference type="PDB" id="3JR8"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="21">
    <source>
      <dbReference type="PDB" id="7RJI"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="22">
    <source>
      <dbReference type="PDB" id="7RJZ"/>
    </source>
  </evidence>
  <sequence length="138" mass="15765" checksum="E93515551C5ED81A" modified="1995-11-01" version="1" precursor="true">MRTLWIMAVLLVGVEGDLWQFGQMILKETGKLPFPYYTTYGCYCGWGGQGQPKDATDRCCFVHDCCYGKLTNCKPKTDRYSYSRENGVIICGEGTPCEKQICECDKAAAVCFRENLRTYKKRYMAYPDVLCKKPAEKC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>