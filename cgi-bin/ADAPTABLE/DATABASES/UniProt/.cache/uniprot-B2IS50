<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-04-14" modified="2024-11-27" version="86" xmlns="http://uniprot.org/uniprot">
  <accession>B2IS50</accession>
  <name>RL14_STRPS</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Large ribosomal subunit protein uL14</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">50S ribosomal protein L14</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rplN</name>
    <name type="ordered locus">SPCG_0228</name>
  </gene>
  <organism>
    <name type="scientific">Streptococcus pneumoniae (strain CGSP14)</name>
    <dbReference type="NCBI Taxonomy" id="516950"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Streptococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="BMC Genomics" volume="10" first="158" last="158">
      <title>Genome evolution driven by host adaptations results in a more virulent and antimicrobial-resistant Streptococcus pneumoniae serotype 14.</title>
      <authorList>
        <person name="Ding F."/>
        <person name="Tang P."/>
        <person name="Hsu M.-H."/>
        <person name="Cui P."/>
        <person name="Hu S."/>
        <person name="Yu J."/>
        <person name="Chiu C.-H."/>
      </authorList>
      <dbReference type="PubMed" id="19361343"/>
      <dbReference type="DOI" id="10.1186/1471-2164-10-158"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CGSP14</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Binds to 23S rRNA. Forms part of two intersubunit bridges in the 70S ribosome.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Part of the 50S ribosomal subunit. Forms a cluster with proteins L3 and L19. In the 70S ribosome, L14 and L19 interact and together make contacts with the 16S rRNA in bridges B5 and B8.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the universal ribosomal protein uL14 family.</text>
  </comment>
  <dbReference type="EMBL" id="CP001033">
    <property type="protein sequence ID" value="ACB89480.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000616545.1">
    <property type="nucleotide sequence ID" value="NC_010582.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B2IS50"/>
  <dbReference type="SMR" id="B2IS50"/>
  <dbReference type="GeneID" id="66805426"/>
  <dbReference type="KEGG" id="spw:SPCG_0228"/>
  <dbReference type="HOGENOM" id="CLU_095071_2_1_9"/>
  <dbReference type="GO" id="GO:0022625">
    <property type="term" value="C:cytosolic large ribosomal subunit"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070180">
    <property type="term" value="F:large ribosomal subunit rRNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006412">
    <property type="term" value="P:translation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.150.20:FF:000001">
    <property type="entry name" value="50S ribosomal protein L14"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.150.20">
    <property type="entry name" value="Ribosomal protein L14"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01367">
    <property type="entry name" value="Ribosomal_uL14"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000218">
    <property type="entry name" value="Ribosomal_uL14"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005745">
    <property type="entry name" value="Ribosomal_uL14_bac-type"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019972">
    <property type="entry name" value="Ribosomal_uL14_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036853">
    <property type="entry name" value="Ribosomal_uL14_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01067">
    <property type="entry name" value="rplN_bact"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11761">
    <property type="entry name" value="50S/60S RIBOSOMAL PROTEIN L14/L23"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11761:SF3">
    <property type="entry name" value="54S RIBOSOMAL PROTEIN L38, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00238">
    <property type="entry name" value="Ribosomal_L14"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01374">
    <property type="entry name" value="Ribosomal_L14"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50193">
    <property type="entry name" value="Ribosomal protein L14"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00049">
    <property type="entry name" value="RIBOSOMAL_L14"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0699">rRNA-binding</keyword>
  <feature type="chain" id="PRO_1000144338" description="Large ribosomal subunit protein uL14">
    <location>
      <begin position="1"/>
      <end position="122"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01367"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="122" mass="13006" checksum="E278D78E32EB9B38" modified="2008-06-10" version="1">MIQTETRLKVADNSGAREILTIKVLGGSGRKFANIGDVIVASVKQATPGGAVKKGDVVKAVIVRTKSGARRADGSYIKFDENAAVIIREDKTPRGTRIFGPVARELREGGFMKIVSLAPEVL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>