<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2016-02-17" modified="2022-05-25" version="6" xmlns="http://uniprot.org/uniprot">
  <accession>C0HJY0</accession>
  <name>PRP_CALVI</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Proline-rich peptide</fullName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Calliphora vicina</name>
    <name type="common">Blue blowfly</name>
    <name type="synonym">Calliphora erythrocephala</name>
    <dbReference type="NCBI Taxonomy" id="7373"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Oestroidea</taxon>
      <taxon>Calliphoridae</taxon>
      <taxon>Calliphorinae</taxon>
      <taxon>Calliphora</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2000" name="Entomol. Sci." volume="3" first="139" last="144">
      <title>Dia pause and immune response: induction of antimicrobial peptides synthesis in the blowfly, Calliphora vicina R.-D. (Diptera: Calliphoridae).</title>
      <authorList>
        <person name="Chernysh S."/>
        <person name="Gordja N.A."/>
        <person name="Simnonenko N.P."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue evidence="4">Hemolymph</tissue>
    </source>
  </reference>
  <reference evidence="5" key="2">
    <citation type="journal article" date="2015" name="PLoS ONE" volume="10" first="E0130788" last="E0130788">
      <title>Insect antimicrobial peptide complexes prevent resistance development in bacteria.</title>
      <authorList>
        <person name="Chernysh S."/>
        <person name="Gordya N."/>
        <person name="Suborova T."/>
      </authorList>
      <dbReference type="PubMed" id="26177023"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0130788"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>INDUCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="3">Hemolymph</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Antibacterial peptide active against Gram-positive bacterium M.luteus and Gram-negative bacterium E.coli.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text evidence="2">By bacterial infection.</text>
  </comment>
  <comment type="mass spectrometry" mass="2987.0" method="Electrospray" evidence="2"/>
  <dbReference type="AlphaFoldDB" id="C0HJY0"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000435611" description="Proline-rich peptide">
    <location>
      <begin position="1"/>
      <end position="22" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="3">
    <location>
      <position position="22"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="26177023"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="26177023"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="26177023"/>
    </source>
  </evidence>
  <sequence length="22" mass="2505" checksum="1172C616DEDD9839" modified="2016-02-17" version="1" fragment="single">FVDRNRIPRSNNGPKIPIISNP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>