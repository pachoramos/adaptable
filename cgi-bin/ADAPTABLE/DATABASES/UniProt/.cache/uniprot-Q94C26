<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2014-09-03" modified="2024-10-02" version="112" xmlns="http://uniprot.org/uniprot">
  <accession>Q94C26</accession>
  <accession>Q9LID9</accession>
  <name>PCC1_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Cysteine-rich and transmembrane domain-containing protein PCC1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Protein PATHOGEN AND CIRCADIAN CONTROLLED 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">PCC1</name>
    <name type="ordered locus">At3g22231</name>
    <name type="ORF">MKA23</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="DNA Res." volume="7" first="217" last="221">
      <title>Structural analysis of Arabidopsis thaliana chromosome 3. II. Sequence features of the 4,251,695 bp regions covered by 90 P1, TAC and BAC clones.</title>
      <authorList>
        <person name="Kaneko T."/>
        <person name="Katoh T."/>
        <person name="Sato S."/>
        <person name="Nakamura Y."/>
        <person name="Asamizu E."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="10907853"/>
      <dbReference type="DOI" id="10.1093/dnares/7.3.217"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2003" name="Science" volume="302" first="842" last="846">
      <title>Empirical analysis of transcriptional activity in the Arabidopsis genome.</title>
      <authorList>
        <person name="Yamada K."/>
        <person name="Lim J."/>
        <person name="Dale J.M."/>
        <person name="Chen H."/>
        <person name="Shinn P."/>
        <person name="Palm C.J."/>
        <person name="Southwick A.M."/>
        <person name="Wu H.C."/>
        <person name="Kim C.J."/>
        <person name="Nguyen M."/>
        <person name="Pham P.K."/>
        <person name="Cheuk R.F."/>
        <person name="Karlin-Newmann G."/>
        <person name="Liu S.X."/>
        <person name="Lam B."/>
        <person name="Sakano H."/>
        <person name="Wu T."/>
        <person name="Yu G."/>
        <person name="Miranda M."/>
        <person name="Quach H.L."/>
        <person name="Tripp M."/>
        <person name="Chang C.H."/>
        <person name="Lee J.M."/>
        <person name="Toriumi M.J."/>
        <person name="Chan M.M."/>
        <person name="Tang C.C."/>
        <person name="Onodera C.S."/>
        <person name="Deng J.M."/>
        <person name="Akiyama K."/>
        <person name="Ansari Y."/>
        <person name="Arakawa T."/>
        <person name="Banh J."/>
        <person name="Banno F."/>
        <person name="Bowser L."/>
        <person name="Brooks S.Y."/>
        <person name="Carninci P."/>
        <person name="Chao Q."/>
        <person name="Choy N."/>
        <person name="Enju A."/>
        <person name="Goldsmith A.D."/>
        <person name="Gurjal M."/>
        <person name="Hansen N.F."/>
        <person name="Hayashizaki Y."/>
        <person name="Johnson-Hopson C."/>
        <person name="Hsuan V.W."/>
        <person name="Iida K."/>
        <person name="Karnes M."/>
        <person name="Khan S."/>
        <person name="Koesema E."/>
        <person name="Ishida J."/>
        <person name="Jiang P.X."/>
        <person name="Jones T."/>
        <person name="Kawai J."/>
        <person name="Kamiya A."/>
        <person name="Meyers C."/>
        <person name="Nakajima M."/>
        <person name="Narusaka M."/>
        <person name="Seki M."/>
        <person name="Sakurai T."/>
        <person name="Satou M."/>
        <person name="Tamse R."/>
        <person name="Vaysberg M."/>
        <person name="Wallender E.K."/>
        <person name="Wong C."/>
        <person name="Yamamura Y."/>
        <person name="Yuan S."/>
        <person name="Shinozaki K."/>
        <person name="Davis R.W."/>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
      </authorList>
      <dbReference type="PubMed" id="14593172"/>
      <dbReference type="DOI" id="10.1126/science.1088305"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2009" name="DNA Res." volume="16" first="155" last="164">
      <title>Analysis of multiple occurrences of alternative splicing events in Arabidopsis thaliana using novel sequenced full-length cDNAs.</title>
      <authorList>
        <person name="Iida K."/>
        <person name="Fukami-Kobayashi K."/>
        <person name="Toyoda A."/>
        <person name="Sakaki Y."/>
        <person name="Kobayashi M."/>
        <person name="Seki M."/>
        <person name="Shinozaki K."/>
      </authorList>
      <dbReference type="PubMed" id="19423640"/>
      <dbReference type="DOI" id="10.1093/dnares/dsp009"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
      <tissue>Rosette leaf</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Planta" volume="218" first="552" last="561">
      <title>PCC1: a merging point for pathogen defence and circadian signalling in Arabidopsis.</title>
      <authorList>
        <person name="Sauerbrunn N."/>
        <person name="Schlaich N.L."/>
      </authorList>
      <dbReference type="PubMed" id="14614626"/>
      <dbReference type="DOI" id="10.1007/s00425-003-1143-z"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INDUCTION BY PATHOGEN</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2010" name="Bioinformatics" volume="26" first="149" last="152">
      <title>CYSTM, a novel cysteine-rich transmembrane module with a role in stress tolerance across eukaryotes.</title>
      <authorList>
        <person name="Venancio T.M."/>
        <person name="Aravind L."/>
      </authorList>
      <dbReference type="PubMed" id="19933165"/>
      <dbReference type="DOI" id="10.1093/bioinformatics/btp647"/>
    </citation>
    <scope>TOPOLOGY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>GENE FAMILY</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2010" name="Plant Cell Environ." volume="33" first="11" last="22">
      <title>Genome-wide analyses of the transcriptomes of salicylic acid-deficient versus wild-type plants uncover Pathogen and Circadian Controlled 1 (PCC1) as a regulator of flowering time in Arabidopsis.</title>
      <authorList>
        <person name="Segarra S."/>
        <person name="Mir R."/>
        <person name="Martinez C."/>
        <person name="Leon J."/>
      </authorList>
      <dbReference type="PubMed" id="19781011"/>
      <dbReference type="DOI" id="10.1111/j.1365-3040.2009.02045.x"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <scope>INDUCTION BY UV</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2012" name="J. Exp. Bot." volume="63" first="3603" last="3616">
      <title>Identification of lipids and lipid-binding proteins in phloem exudates from Arabidopsis thaliana.</title>
      <authorList>
        <person name="Guelette B.S."/>
        <person name="Benning U.F."/>
        <person name="Hoffmann-Benning S."/>
      </authorList>
      <dbReference type="PubMed" id="22442409"/>
      <dbReference type="DOI" id="10.1093/jxb/ers028"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2013" name="J. Exp. Bot." volume="64" first="3385" last="3395">
      <title>Pathogen and Circadian Controlled 1 (PCC1) regulates polar lipid content, ABA-related responses, and pathogen defence in Arabidopsis thaliana.</title>
      <authorList>
        <person name="Mir R."/>
        <person name="Hernandez M.L."/>
        <person name="Abou-Mansour E."/>
        <person name="Martinez-Rivas J.M."/>
        <person name="Mauch F."/>
        <person name="Metraux J.-P."/>
        <person name="Leon J."/>
      </authorList>
      <dbReference type="PubMed" id="23833195"/>
      <dbReference type="DOI" id="10.1093/jxb/ert177"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3 4 6">Modulates resistance against pathogens including oomycetes (e.g. Hyaloperonospora parasitica and Phytophthora brassicae) and fungi (e.g. Phytophthora brassicae). Controls the abscisic acid-mediated (ABA) signaling pathways. Regulator of the flowering time in response to stress (e.g. UV-C). Regulates polar lipid content; promotes phosphatidylinositol (PI) and 18:0 but prevents 18:2 and 18:3 polar lipids accumulation.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Cell membrane</location>
      <topology evidence="1">Single-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4 5">Expressed at very low levels in seedlings and petioles, and at higher levels in leaves. Also present in phloem sap.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="4">Low expression after germination followed by an abrupt level increase in 10-days old seedlings. Accumulates in senescent leaves.</text>
  </comment>
  <comment type="induction">
    <text evidence="3 4">Rapidly up-regulated after pathogen exposure (e.g. avirulent and virulent Pseudomonas syringae pv. tomato) in a salicylic acid (SA) defense-signaling pathway-dependent manner. Circadian-regulation with accumulation during the light period, peaks of expression at the end of the day, and low levels during the dark period. Up-regulated by UV-C light through a SA-dependent process and in a CONSTANS- (CO) dependent manner.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="4 6">Late flowering and defective in UV-C light acceleration of flowering. Strong reduction of FLOWERING LOCUS T (FT) expression. Hypersensitivity to abscisic acid (ABA) and alterations in polar lipid contents and their corresponding fatty acids; reduced levels of phosphatidylinositol (PI) and of 18:0, but increased levels of 18:2 and 18:3 polar lipids. Increased susceptibility to the hemi-biotrophic oomycete pathogen Phytophthora brassicae but enhanced resistance to the necrotrophic fungal pathogen Botrytis cinerea.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the CYSTM1 family.</text>
  </comment>
  <comment type="sequence caution" evidence="7">
    <conflict type="erroneous gene model prediction">
      <sequence resource="EMBL-CDS" id="BAB03071" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="AP001306">
    <property type="protein sequence ID" value="BAB03071.1"/>
    <property type="status" value="ALT_SEQ"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002686">
    <property type="protein sequence ID" value="AEE76608.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY037207">
    <property type="protein sequence ID" value="AAK59792.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT002668">
    <property type="protein sequence ID" value="AAO11584.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK316896">
    <property type="protein sequence ID" value="BAH19603.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK317758">
    <property type="protein sequence ID" value="BAH20414.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_566702.1">
    <property type="nucleotide sequence ID" value="NM_113121.5"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q94C26"/>
  <dbReference type="SMR" id="Q94C26"/>
  <dbReference type="BioGRID" id="7120">
    <property type="interactions" value="3"/>
  </dbReference>
  <dbReference type="STRING" id="3702.Q94C26"/>
  <dbReference type="iPTMnet" id="Q94C26"/>
  <dbReference type="PaxDb" id="3702-AT3G22231.1"/>
  <dbReference type="ProteomicsDB" id="236285"/>
  <dbReference type="EnsemblPlants" id="AT3G22231.1">
    <property type="protein sequence ID" value="AT3G22231.1"/>
    <property type="gene ID" value="AT3G22231"/>
  </dbReference>
  <dbReference type="GeneID" id="821788"/>
  <dbReference type="Gramene" id="AT3G22231.1">
    <property type="protein sequence ID" value="AT3G22231.1"/>
    <property type="gene ID" value="AT3G22231"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT3G22231"/>
  <dbReference type="Araport" id="AT3G22231"/>
  <dbReference type="TAIR" id="AT3G22231">
    <property type="gene designation" value="PCC1"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_128451_4_0_1"/>
  <dbReference type="InParanoid" id="Q94C26"/>
  <dbReference type="OMA" id="MECIFCC"/>
  <dbReference type="OrthoDB" id="615947at2759"/>
  <dbReference type="PhylomeDB" id="Q94C26"/>
  <dbReference type="PRO" id="PR:Q94C26"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q94C26">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009738">
    <property type="term" value="P:abscisic acid-activated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071494">
    <property type="term" value="P:cellular response to UV-C"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007623">
    <property type="term" value="P:circadian rhythm"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0055088">
    <property type="term" value="P:lipid homeostasis"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006629">
    <property type="term" value="P:lipid metabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:1902290">
    <property type="term" value="P:positive regulation of defense response to oomycetes"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010513">
    <property type="term" value="P:positive regulation of phosphatidylinositol biosynthetic process"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1900150">
    <property type="term" value="P:regulation of defense response to fungus"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046890">
    <property type="term" value="P:regulation of lipid biosynthetic process"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000028">
    <property type="term" value="P:regulation of photoperiodism, flowering"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009737">
    <property type="term" value="P:response to abscisic acid"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009617">
    <property type="term" value="P:response to bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009863">
    <property type="term" value="P:salicylic acid mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR028144">
    <property type="entry name" value="CYSTM_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF12734">
    <property type="entry name" value="CYSTM"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0938">Abscisic acid signaling pathway</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0443">Lipid metabolism</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <feature type="chain" id="PRO_0000430168" description="Cysteine-rich and transmembrane domain-containing protein PCC1">
    <location>
      <begin position="1"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical">
    <location>
      <begin position="56"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="1"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Polar residues" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="14614626"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="19781011"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="22442409"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="23833195"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="19933165"/>
    </source>
  </evidence>
  <sequence length="81" mass="8479" checksum="6DF1FB6388E2D25F" modified="2001-12-01" version="1">MNQSAQNYFSVQKPSETSSGPYTSPPPIGYPTRDAVVGDPPAAAVETNSKGVNPEAIMSCFSTCMECIFCCGVCSSLCTSE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>