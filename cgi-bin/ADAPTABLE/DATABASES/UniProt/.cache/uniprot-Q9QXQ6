<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-02-11" modified="2024-05-29" version="105" xmlns="http://uniprot.org/uniprot">
  <accession>Q9QXQ6</accession>
  <name>GALP_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Galanin-like peptide</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">Galp</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="J. Biol. Chem." volume="274" first="37041" last="37045">
      <title>Isolation and cDNA cloning of a novel galanin-like peptide (GALP) from porcine hypothalamus.</title>
      <authorList>
        <person name="Ohtaki T."/>
        <person name="Kumano S."/>
        <person name="Ishibashi Y."/>
        <person name="Ogi K."/>
        <person name="Matsui H."/>
        <person name="Harada M."/>
        <person name="Kitada C."/>
        <person name="Kurokawa T."/>
        <person name="Onda H."/>
        <person name="Fujino M."/>
      </authorList>
      <dbReference type="PubMed" id="10601261"/>
      <dbReference type="DOI" id="10.1074/jbc.274.52.37041"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Hypothalamic neuropeptide which binds to the G-protein-coupled galanin receptors (GALR1, GALR2 and GALR3). Involved in a large number of putative physiological functions in CNS homeostatic processes, including the regulation of gonadotropin-releasing hormone secretion (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the galanin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF188491">
    <property type="protein sequence ID" value="AAF19723.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_072155.1">
    <property type="nucleotide sequence ID" value="NM_022633.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9QXQ6"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000038710"/>
  <dbReference type="PhosphoSitePlus" id="Q9QXQ6"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000038710"/>
  <dbReference type="Ensembl" id="ENSRNOT00000034854.3">
    <property type="protein sequence ID" value="ENSRNOP00000038710.2"/>
    <property type="gene ID" value="ENSRNOG00000022129.3"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055023708">
    <property type="protein sequence ID" value="ENSRNOP00055019310"/>
    <property type="gene ID" value="ENSRNOG00055013791"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060046251">
    <property type="protein sequence ID" value="ENSRNOP00060038425"/>
    <property type="gene ID" value="ENSRNOG00060026705"/>
  </dbReference>
  <dbReference type="GeneID" id="64568"/>
  <dbReference type="KEGG" id="rno:64568"/>
  <dbReference type="UCSC" id="RGD:620187">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:620187"/>
  <dbReference type="CTD" id="85569"/>
  <dbReference type="RGD" id="620187">
    <property type="gene designation" value="Galp"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502TI9H">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000016349"/>
  <dbReference type="HOGENOM" id="CLU_167264_0_0_1"/>
  <dbReference type="InParanoid" id="Q9QXQ6"/>
  <dbReference type="OMA" id="PQMSDQD"/>
  <dbReference type="OrthoDB" id="5224030at2759"/>
  <dbReference type="PhylomeDB" id="Q9QXQ6"/>
  <dbReference type="TreeFam" id="TF339481"/>
  <dbReference type="PRO" id="PR:Q9QXQ6"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000022129">
    <property type="expression patterns" value="Expressed in thymus and 1 other cell type or tissue"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042595">
    <property type="term" value="P:behavioral response to starvation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032098">
    <property type="term" value="P:regulation of appetite"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009725">
    <property type="term" value="P:response to hormone"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032868">
    <property type="term" value="P:response to insulin"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008174">
    <property type="entry name" value="Galanin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039244">
    <property type="entry name" value="GALP"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20950:SF1">
    <property type="entry name" value="GALANIN-LIKE PEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20950">
    <property type="entry name" value="GALANIN-RELATED PEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01296">
    <property type="entry name" value="Galanin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00861">
    <property type="entry name" value="GALANIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000010467" description="Galanin-like peptide">
    <location>
      <begin position="24"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000010468" evidence="1">
    <location>
      <begin position="86"/>
      <end position="115"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="115" mass="12600" checksum="00BD3A2336AD06C5" modified="2000-05-01" version="1" precursor="true">MACSKHLVLFLTILLSLAETPDSAPAHRGRGGWTLNSAGYLLGPVLHLSSKANQGRKTDSALEILDLWKAIDGLPYSRSPRMTKRSMGETFVKPRTGDLRIVDKNVPDEEATLNL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>