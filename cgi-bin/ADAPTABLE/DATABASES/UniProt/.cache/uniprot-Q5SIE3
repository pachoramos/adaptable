<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2014-05-14" modified="2024-07-24" version="94" xmlns="http://uniprot.org/uniprot">
  <accession>Q5SIE3</accession>
  <name>DODEC_THET8</name>
  <protein>
    <recommendedName>
      <fullName>Dodecin</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">TTHA1431</name>
  </gene>
  <organism>
    <name type="scientific">Thermus thermophilus (strain ATCC 27634 / DSM 579 / HB8)</name>
    <dbReference type="NCBI Taxonomy" id="300852"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Deinococcota</taxon>
      <taxon>Deinococci</taxon>
      <taxon>Thermales</taxon>
      <taxon>Thermaceae</taxon>
      <taxon>Thermus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2004-11" db="EMBL/GenBank/DDBJ databases">
      <title>Complete genome sequence of Thermus thermophilus HB8.</title>
      <authorList>
        <person name="Masui R."/>
        <person name="Kurokawa K."/>
        <person name="Nakagawa N."/>
        <person name="Tokunaga F."/>
        <person name="Koyama Y."/>
        <person name="Shibata T."/>
        <person name="Oshima T."/>
        <person name="Yokoyama S."/>
        <person name="Yasunaga T."/>
        <person name="Kuramitsu S."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 27634 / DSM 579 / HB8</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="submission" date="2006-02" db="PDB data bank">
      <title>Crystal structure of TT0972 protein from Thermus thermophilus.</title>
      <authorList>
        <person name="Kumei M."/>
        <person name="Inagaki E."/>
        <person name="Nakano N."/>
        <person name="Shinkai A."/>
        <person name="Yokoyama S."/>
      </authorList>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.50 ANGSTROMS) IN COMPLEX WITH FAD</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2007" name="J. Biol. Chem." volume="282" first="33142" last="33154">
      <title>The dodecin from Thermus thermophilus, a bifunctional cofactor storage protein.</title>
      <authorList>
        <person name="Meissner B."/>
        <person name="Schleicher E."/>
        <person name="Weber S."/>
        <person name="Essen L.O."/>
      </authorList>
      <dbReference type="PubMed" id="17855371"/>
      <dbReference type="DOI" id="10.1074/jbc.m704951200"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.40 ANGSTROMS) OF WILD-TYPE AND MUTANTS ALA-45 AND ALA-65 IN COMPLEXES WITH COENZYME A AND FMN</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>MUTAGENESIS OF ARG-45 AND ARG-65</scope>
    <source>
      <strain>ATCC 27634 / DSM 579 / HB8</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="submission" date="2008-07" db="PDB data bank">
      <title>Ultrafast charge transfer dynamics in flavoprotein dodecin.</title>
      <authorList>
        <person name="Gurzadyan G.G."/>
        <person name="Meissner B."/>
        <person name="Sander B."/>
        <person name="Essen L.-O."/>
        <person name="Michel-Beyerle M.E."/>
      </authorList>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.50 ANGSTROMS) IN COMPLEX WITH COENZYME A AND FMN</scope>
  </reference>
  <comment type="function">
    <text evidence="1">May function as storage protein that sequesters various flavins and other cofactors, thereby protecting the cell against undesirable reactions mediated by the free cofactors. Binds and sequesters FMN, FAD, lumiflavin and lumichrome, and can also bind coenzyme A.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 2 3">Homododecamer; four homotrimers assemble to form a dodecameric hollow sphere with an outer diameter of about 60 Angstroms. Flavin dimers are bound between subunits with a stoichiometry of 6 flavin dimers per dodecamer. Besides, trimeric coenzyme A molecules can be bound between subunits. A dodecamer can bind simultaneously 12 flavin and 12 coenzyme A molecules.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the dodecin family.</text>
  </comment>
  <comment type="caution">
    <text evidence="4">Dodecin family members from different organisms have non-identical ligand binding specificity.</text>
  </comment>
  <dbReference type="EMBL" id="AP008226">
    <property type="protein sequence ID" value="BAD71254.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_011173482.1">
    <property type="nucleotide sequence ID" value="NC_006461.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="YP_144697.1">
    <property type="nucleotide sequence ID" value="NC_006461.1"/>
  </dbReference>
  <dbReference type="PDB" id="2CZ8">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.50 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H=1-69"/>
  </dbReference>
  <dbReference type="PDB" id="2DEG">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A/B/C/D/E/F=1-69"/>
  </dbReference>
  <dbReference type="PDB" id="2UX9">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.40 A"/>
    <property type="chains" value="A/B/C/D/E/F=1-69"/>
  </dbReference>
  <dbReference type="PDB" id="2V18">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.59 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J/K/L=2-69"/>
  </dbReference>
  <dbReference type="PDB" id="2V19">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.59 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J/K/L=2-69"/>
  </dbReference>
  <dbReference type="PDB" id="2V21">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.40 A"/>
    <property type="chains" value="A/B/C/D/E/F=1-69"/>
  </dbReference>
  <dbReference type="PDB" id="2VYX">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.50 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J/K/L=1-69"/>
  </dbReference>
  <dbReference type="PDBsum" id="2CZ8"/>
  <dbReference type="PDBsum" id="2DEG"/>
  <dbReference type="PDBsum" id="2UX9"/>
  <dbReference type="PDBsum" id="2V18"/>
  <dbReference type="PDBsum" id="2V19"/>
  <dbReference type="PDBsum" id="2V21"/>
  <dbReference type="PDBsum" id="2VYX"/>
  <dbReference type="AlphaFoldDB" id="Q5SIE3"/>
  <dbReference type="SMR" id="Q5SIE3"/>
  <dbReference type="EnsemblBacteria" id="BAD71254">
    <property type="protein sequence ID" value="BAD71254"/>
    <property type="gene ID" value="BAD71254"/>
  </dbReference>
  <dbReference type="GeneID" id="3168700"/>
  <dbReference type="KEGG" id="ttj:TTHA1431"/>
  <dbReference type="PATRIC" id="fig|300852.9.peg.1405"/>
  <dbReference type="eggNOG" id="COG3360">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_161196_1_1_0"/>
  <dbReference type="PhylomeDB" id="Q5SIE3"/>
  <dbReference type="EvolutionaryTrace" id="Q5SIE3"/>
  <dbReference type="Proteomes" id="UP000000532">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000166">
    <property type="term" value="F:nucleotide binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.1660.10">
    <property type="entry name" value="Flavin-binding protein dodecin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009923">
    <property type="entry name" value="Dodecin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025543">
    <property type="entry name" value="Dodecin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036694">
    <property type="entry name" value="Dodecin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050049">
    <property type="entry name" value="Dodecin_bact"/>
  </dbReference>
  <dbReference type="NCBIfam" id="NF043052">
    <property type="entry name" value="DodecBact"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR39324">
    <property type="entry name" value="CALCIUM DODECIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR39324:SF1">
    <property type="entry name" value="CALCIUM DODECIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07311">
    <property type="entry name" value="Dodecin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF89807">
    <property type="entry name" value="Dodecin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0274">FAD</keyword>
  <keyword id="KW-0285">Flavoprotein</keyword>
  <keyword id="KW-0288">FMN</keyword>
  <keyword id="KW-0547">Nucleotide-binding</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000429122" description="Dodecin">
    <location>
      <begin position="1"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="binding site" description="in other chain" evidence="3">
    <location>
      <begin position="3"/>
      <end position="5"/>
    </location>
    <ligand>
      <name>FMN</name>
      <dbReference type="ChEBI" id="CHEBI:58210"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="6"/>
    </location>
    <ligand>
      <name>CoA</name>
      <dbReference type="ChEBI" id="CHEBI:57287"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="binding site" description="in other chain" evidence="3">
    <location>
      <position position="28"/>
    </location>
    <ligand>
      <name>CoA</name>
      <dbReference type="ChEBI" id="CHEBI:57287"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="binding site" description="in other chain">
    <location>
      <begin position="32"/>
      <end position="34"/>
    </location>
    <ligand>
      <name>CoA</name>
      <dbReference type="ChEBI" id="CHEBI:57287"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="binding site" description="in other chain" evidence="3">
    <location>
      <position position="37"/>
    </location>
    <ligand>
      <name>FMN</name>
      <dbReference type="ChEBI" id="CHEBI:58210"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="binding site" description="in other chain" evidence="3">
    <location>
      <position position="38"/>
    </location>
    <ligand>
      <name>FMN</name>
      <dbReference type="ChEBI" id="CHEBI:58210"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="45"/>
    </location>
    <ligand>
      <name>FMN</name>
      <dbReference type="ChEBI" id="CHEBI:58210"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="57"/>
    </location>
    <ligand>
      <name>FMN</name>
      <dbReference type="ChEBI" id="CHEBI:58210"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="binding site" description="in other chain">
    <location>
      <begin position="65"/>
      <end position="67"/>
    </location>
    <ligand>
      <name>CoA</name>
      <dbReference type="ChEBI" id="CHEBI:57287"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="binding site" description="in other chain" evidence="3">
    <location>
      <position position="65"/>
    </location>
    <ligand>
      <name>FMN</name>
      <dbReference type="ChEBI" id="CHEBI:58210"/>
      <note>ligand shared between two neighboring subunits</note>
    </ligand>
  </feature>
  <feature type="site" description="May be important for ligand binding specificity and FMN binding">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on the orientation of the bound flavin." evidence="1">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="45"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on the orientation of the bound flavin." evidence="1">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="5"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="18"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="36"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="52"/>
      <end position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="17855371"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source ref="4"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0007829" key="5">
    <source>
      <dbReference type="PDB" id="2UX9"/>
    </source>
  </evidence>
  <sequence length="69" mass="7748" checksum="CC1510FD521FA4D9" modified="2004-12-21" version="1">MGKVYKKVELVGTSEEGLEAAIQAALARARKTLRHLDWFEVKEIRGTIGEAGVKEYQVVLEVGFRLEET</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>