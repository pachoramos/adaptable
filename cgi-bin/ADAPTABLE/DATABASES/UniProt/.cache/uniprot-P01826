<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="83" xmlns="http://uniprot.org/uniprot">
  <accession>P01826</accession>
  <name>HV1A_RABIT</name>
  <protein>
    <recommendedName>
      <fullName>Ig heavy chain V-A1 region BS-5</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Oryctolagus cuniculus</name>
    <name type="common">Rabbit</name>
    <dbReference type="NCBI Taxonomy" id="9986"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Lagomorpha</taxon>
      <taxon>Leporidae</taxon>
      <taxon>Oryctolagus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1972" name="Biochem. J." volume="130" first="539" last="546">
      <title>Amino acid sequence of the N-terminal sixty-nine residues of heavy chain derived from a homogeneous rabbit antibody.</title>
      <authorList>
        <person name="Jaton J.-C."/>
        <person name="Braun D.G."/>
      </authorList>
      <dbReference type="PubMed" id="4146279"/>
      <dbReference type="DOI" id="10.1042/bj1300539"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 1-69</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1974" name="Biochem. J." volume="143" first="723" last="732">
      <title>Completion of the analysis of the primary structure of the variable domain of a homogeneous rabbit antibody to type III pneumococcal polysaccharide.</title>
      <authorList>
        <person name="Jaton J.-C."/>
      </authorList>
      <dbReference type="PubMed" id="4142749"/>
      <dbReference type="DOI" id="10.1042/bj1430723"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 64-116</scope>
  </reference>
  <comment type="miscellaneous">
    <text>This gamma chain was obtained from antibody to type III pneumococci and was isolated from the serum of a single rabbit.</text>
  </comment>
  <dbReference type="PIR" id="A90264">
    <property type="entry name" value="GARB15"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01826"/>
  <dbReference type="SMR" id="P01826"/>
  <dbReference type="STRING" id="9986.ENSOCUP00000016179"/>
  <dbReference type="InParanoid" id="P01826"/>
  <dbReference type="Proteomes" id="UP000001811">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0072562">
    <property type="term" value="C:blood microparticle"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019814">
    <property type="term" value="C:immunoglobulin complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003823">
    <property type="term" value="F:antigen binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016064">
    <property type="term" value="P:immunoglobulin mediated immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.40.10:FF:001878">
    <property type="entry name" value="Immunoglobulin heavy variable 1-4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013106">
    <property type="entry name" value="Ig_V-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050199">
    <property type="entry name" value="IgHV"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23266">
    <property type="entry name" value="IMMUNOGLOBULIN HEAVY CHAIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23266:SF372">
    <property type="entry name" value="IMMUNOGLOBULIN HEAVY VARIABLE 3-23-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07686">
    <property type="entry name" value="V-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00406">
    <property type="entry name" value="IGv"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-1280">Immunoglobulin</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000059935" description="Ig heavy chain V-A1 region BS-5">
    <location>
      <begin position="1"/>
      <end position="116" status="greater than"/>
    </location>
  </feature>
  <feature type="domain" description="Ig-like">
    <location>
      <begin position="1"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="116"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="4146279"/>
    </source>
  </evidence>
  <sequence length="116" mass="12349" checksum="95C6FAC93C788C42" modified="1986-07-21" version="1">QSVEESGGRLVTPTPGLTLTCTVSGFSLSSYDMGWVRQAPGKGLEWIGIIYASGSTYYASWAKGRFTISKTSTTVDLKTSLPTEDTATYFCARQGTGLVHLAFVDVWGPGTLVTVS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>