<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-29" modified="2024-11-27" version="81" xmlns="http://uniprot.org/uniprot">
  <accession>A4K2T3</accession>
  <name>WFD12_MACMU</name>
  <protein>
    <recommendedName>
      <fullName>WAP four-disulfide core domain protein 12</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">WFDC12</name>
  </gene>
  <organism>
    <name type="scientific">Macaca mulatta</name>
    <name type="common">Rhesus macaque</name>
    <dbReference type="NCBI Taxonomy" id="9544"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Macaca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2007" name="Genome Res." volume="17" first="276" last="286">
      <title>Comparative sequence analyses reveal rapid and divergent evolutionary changes of the WFDC locus in the primate lineage.</title>
      <authorList>
        <consortium name="NISC comparative sequencing program"/>
        <person name="Hurle B."/>
        <person name="Swanson W."/>
        <person name="Green E.D."/>
      </authorList>
      <dbReference type="PubMed" id="17267810"/>
      <dbReference type="DOI" id="10.1101/gr.6004607"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Antibacterial protein. Putative acid-stable proteinase inhibitor (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EMBL" id="DP000043">
    <property type="protein sequence ID" value="ABO52968.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001162152.1">
    <property type="nucleotide sequence ID" value="NM_001168681.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A4K2T3"/>
  <dbReference type="SMR" id="A4K2T3"/>
  <dbReference type="STRING" id="9544.ENSMMUP00000017364"/>
  <dbReference type="MEROPS" id="I17.003"/>
  <dbReference type="PaxDb" id="9544-ENSMMUP00000017364"/>
  <dbReference type="Ensembl" id="ENSMMUT00000018540.4">
    <property type="protein sequence ID" value="ENSMMUP00000017364.4"/>
    <property type="gene ID" value="ENSMMUG00000013217.4"/>
  </dbReference>
  <dbReference type="GeneID" id="711552"/>
  <dbReference type="KEGG" id="mcc:711552"/>
  <dbReference type="CTD" id="128488"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSMMUG00000013217"/>
  <dbReference type="VGNC" id="VGNC:79009">
    <property type="gene designation" value="WFDC12"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502TDXW">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000012286"/>
  <dbReference type="HOGENOM" id="CLU_172659_0_0_1"/>
  <dbReference type="InParanoid" id="A4K2T3"/>
  <dbReference type="OMA" id="CIKSDPP"/>
  <dbReference type="OrthoDB" id="5472168at2759"/>
  <dbReference type="Proteomes" id="UP000006718">
    <property type="component" value="Chromosome 10"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMMUG00000013217">
    <property type="expression patterns" value="Expressed in fibroblast and 7 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.75.10:FF:000005">
    <property type="entry name" value="WAP four-disulfide core domain protein 12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.75.10">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036645">
    <property type="entry name" value="Elafin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008197">
    <property type="entry name" value="WAP_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00095">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00003">
    <property type="entry name" value="4DISULPHCORE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00217">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57256">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51390">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000289651" description="WAP four-disulfide core domain protein 12">
    <location>
      <begin position="24"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="domain" description="WAP" evidence="3">
    <location>
      <begin position="27"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="4">
    <location>
      <begin position="80"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="34"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="41"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="49"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="55"/>
      <end position="70"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00722"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="4">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="111" mass="11660" checksum="056C704B8C058E28" modified="2007-05-01" version="1" precursor="true">MGSSSFLVLMVSLALVTLVAAEGVKGGIEKAGVCPADNIRCFKSDPPQCHTDQDCLGERKCCYLHCGFKCVIPVKKLEEGGNKDEDVSGPCPEPGWEAKSPGSSSTGCPQK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>