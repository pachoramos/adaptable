<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="160" xmlns="http://uniprot.org/uniprot">
  <accession>P01556</accession>
  <accession>Q9JQ02</accession>
  <name>CHTB_VIBCH</name>
  <protein>
    <recommendedName>
      <fullName>Cholera enterotoxin subunit B</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cholera enterotoxin B chain</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Cholera enterotoxin gamma chain</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Choleragenoid</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ctxB</name>
    <name type="synonym">toxB</name>
    <name type="ordered locus">VC_1456</name>
  </gene>
  <organism>
    <name type="scientific">Vibrio cholerae serotype O1 (strain ATCC 39315 / El Tor Inaba N16961)</name>
    <dbReference type="NCBI Taxonomy" id="243277"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Vibrionales</taxon>
      <taxon>Vibrionaceae</taxon>
      <taxon>Vibrio</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1983" name="J. Biol. Chem." volume="258" first="13722" last="13726">
      <title>Nucleotide sequence analysis of the A2 and B subunits of Vibrio cholerae enterotoxin.</title>
      <authorList>
        <person name="Lockman H."/>
        <person name="Kaper J.B."/>
      </authorList>
      <dbReference type="PubMed" id="6315707"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)43977-9"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1983" name="Nature" volume="306" first="551" last="557">
      <title>Cholera toxin genes: nucleotide sequence, deletion analysis and vaccine development.</title>
      <authorList>
        <person name="Mekalanos J.J."/>
        <person name="Swartz D.J."/>
        <person name="Pearson G.D.N."/>
        <person name="Harford N."/>
        <person name="Groyne F."/>
        <person name="de Wilde M."/>
      </authorList>
      <dbReference type="PubMed" id="6646234"/>
      <dbReference type="DOI" id="10.1038/306551a0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 39050 / El Tor Inaba 2125 / Serotype O1</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1994" name="FEMS Microbiol. Lett." volume="117" first="197" last="202">
      <title>Structure and arrangement of the cholera toxin genes in Vibrio cholerae O139.</title>
      <authorList>
        <person name="Lebens M."/>
        <person name="Holmgren J."/>
      </authorList>
      <dbReference type="PubMed" id="8181723"/>
      <dbReference type="DOI" id="10.1111/j.1574-6968.1994.tb06764.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>4260B / Serotype O139</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="submission" date="1991-05" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Dams E."/>
        <person name="de Wolf M."/>
        <person name="Dierick W."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 39050 / El Tor Inaba 2125 / Serotype O1</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="submission" date="1994-05" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Yamamoto K."/>
        <person name="Do V.G.R.F."/>
        <person name="Xu M."/>
        <person name="Iida T."/>
        <person name="Miwatani T."/>
        <person name="Albert M.J."/>
        <person name="Honda T."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>1854 / O139-Bengal</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2000" name="Nature" volume="406" first="477" last="483">
      <title>DNA sequence of both chromosomes of the cholera pathogen Vibrio cholerae.</title>
      <authorList>
        <person name="Heidelberg J.F."/>
        <person name="Eisen J.A."/>
        <person name="Nelson W.C."/>
        <person name="Clayton R.A."/>
        <person name="Gwinn M.L."/>
        <person name="Dodson R.J."/>
        <person name="Haft D.H."/>
        <person name="Hickey E.K."/>
        <person name="Peterson J.D."/>
        <person name="Umayam L.A."/>
        <person name="Gill S.R."/>
        <person name="Nelson K.E."/>
        <person name="Read T.D."/>
        <person name="Tettelin H."/>
        <person name="Richardson D.L."/>
        <person name="Ermolaeva M.D."/>
        <person name="Vamathevan J.J."/>
        <person name="Bass S."/>
        <person name="Qin H."/>
        <person name="Dragoi I."/>
        <person name="Sellers P."/>
        <person name="McDonald L.A."/>
        <person name="Utterback T.R."/>
        <person name="Fleischmann R.D."/>
        <person name="Nierman W.C."/>
        <person name="White O."/>
        <person name="Salzberg S.L."/>
        <person name="Smith H.O."/>
        <person name="Colwell R.R."/>
        <person name="Mekalanos J.J."/>
        <person name="Venter J.C."/>
        <person name="Fraser C.M."/>
      </authorList>
      <dbReference type="PubMed" id="10952301"/>
      <dbReference type="DOI" id="10.1038/35020000"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 39315 / El Tor Inaba N16961</strain>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1977" name="J. Biol. Chem." volume="252" first="7257" last="7264">
      <title>Covalent structure of the beta chain of cholera enterotoxin.</title>
      <authorList>
        <person name="Kurosky A."/>
        <person name="Markel D.E."/>
        <person name="Peterson J.W."/>
      </authorList>
      <dbReference type="PubMed" id="903363"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(19)66963-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 22-124</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="1977" name="J. Biol. Chem." volume="252" first="7249" last="7256">
      <title>Determination of the primary structure of cholera toxin B subunit.</title>
      <authorList>
        <person name="Lai C.-Y."/>
      </authorList>
      <dbReference type="PubMed" id="903362"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(19)66962-0"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 22-124</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="1976" name="Biochemistry" volume="15" first="1242" last="1248">
      <title>The arrangement of subunits in cholera toxin.</title>
      <authorList>
        <person name="Gill D.M."/>
      </authorList>
      <dbReference type="PubMed" id="3214"/>
      <dbReference type="DOI" id="10.1021/bi00651a011"/>
    </citation>
    <scope>SUBUNIT</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2003" name="Mol. Biol. Cell" volume="14" first="4783" last="4793">
      <title>Gangliosides that associate with lipid rafts mediate transport of cholera and related toxins from the plasma membrane to endoplasmic reticulum.</title>
      <authorList>
        <person name="Fujinaga Y."/>
        <person name="Wolf A.A."/>
        <person name="Rodighiero C."/>
        <person name="Wheeler H."/>
        <person name="Tsai B."/>
        <person name="Allen L."/>
        <person name="Jobling M.G."/>
        <person name="Rapoport T."/>
        <person name="Holmes R.K."/>
        <person name="Lencer W.I."/>
      </authorList>
      <dbReference type="PubMed" id="13679513"/>
      <dbReference type="DOI" id="10.1091/mbc.e03-06-0354"/>
    </citation>
    <scope>TRANSPORT OF CHOLERA TOXIN WITHIN THE INTESTINAL CELL</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="1994" name="Protein Sci." volume="3" first="166" last="175">
      <title>Crystal structure of cholera toxin B-pentamer bound to receptor GM1 pentasaccharide.</title>
      <authorList>
        <person name="Merritt E.A."/>
        <person name="Sarfaty S."/>
        <person name="van den Akker F."/>
        <person name="L'Hoir C."/>
        <person name="Martial J.A."/>
        <person name="Hol W.G.J."/>
      </authorList>
      <dbReference type="PubMed" id="8003954"/>
      <dbReference type="DOI" id="10.1002/pro.5560030202"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.2 ANGSTROMS)</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="1995" name="J. Mol. Biol." volume="251" first="550" last="562">
      <title>The 2.4 A crystal structure of cholera toxin B subunit pentamer: choleragenoid.</title>
      <authorList>
        <person name="Zhang R.-G."/>
        <person name="Westbrook M.L."/>
        <person name="Westbrook E.M."/>
        <person name="Scott D.L."/>
        <person name="Otwinowski Z."/>
        <person name="Maulik P.R."/>
        <person name="Reed R.A."/>
        <person name="Shipley G.G."/>
      </authorList>
      <dbReference type="PubMed" id="7658472"/>
      <dbReference type="DOI" id="10.1006/jmbi.1995.0455"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.4 ANGSTROMS)</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="1997" name="Protein Sci." volume="6" first="1516" last="1528">
      <title>Structural studies of receptor binding by cholera toxin mutants.</title>
      <authorList>
        <person name="Merritt E.A."/>
        <person name="Sarfaty S."/>
        <person name="Jobling M.G."/>
        <person name="Chang T."/>
        <person name="Holmes R.K."/>
        <person name="Hirst T.R."/>
        <person name="Hol W.G.J."/>
      </authorList>
      <dbReference type="PubMed" id="9232653"/>
      <dbReference type="DOI" id="10.1002/pro.5560060716"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.25 ANGSTROMS)</scope>
    <source>
      <strain>Ogawa 41 / Classical biotype</strain>
    </source>
  </reference>
  <reference key="14">
    <citation type="journal article" date="2001" name="Proc. Natl. Acad. Sci. U.S.A." volume="98" first="8536" last="8541">
      <title>A mutant cholera toxin B subunit that binds GM1-ganglioside but lacks immunomodulatory or toxic activity.</title>
      <authorList>
        <person name="Aman A.T."/>
        <person name="Fraser S."/>
        <person name="Merritt E.A."/>
        <person name="Rodigherio C."/>
        <person name="Kenny M."/>
        <person name="Ahn M."/>
        <person name="Hol W.G.J."/>
        <person name="Williams N.A."/>
        <person name="Lencer W.I."/>
        <person name="Hirst T.R."/>
      </authorList>
      <dbReference type="PubMed" id="11447291"/>
      <dbReference type="DOI" id="10.1073/pnas.161273098"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.0 ANGSTROMS) OF MUTANT ALA-57</scope>
  </reference>
  <reference key="15">
    <citation type="journal article" date="2002" name="Chem. Biol." volume="9" first="215" last="224">
      <title>Anchor-based design of improved cholera toxin and E. coli heat-labile enterotoxin receptor binding antagonists that display multiple binding modes.</title>
      <authorList>
        <person name="Pickens J.C."/>
        <person name="Merritt E.A."/>
        <person name="Ahn M."/>
        <person name="Verlinde C.L.M.J."/>
        <person name="Hol W.G.J."/>
        <person name="Fan E."/>
      </authorList>
      <dbReference type="PubMed" id="11880036"/>
      <dbReference type="DOI" id="10.1016/s1074-5521(02)00097-2"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.3 ANGSTROMS) IN COMPLEX WITH INHIBITOR</scope>
  </reference>
  <reference key="16">
    <citation type="journal article" date="2002" name="J. Am. Chem. Soc." volume="124" first="12991" last="12998">
      <title>Solution and crystallographic studies of branched multivalent ligands that inhibit the receptor-binding of cholera toxin.</title>
      <authorList>
        <person name="Zhang Z."/>
        <person name="Merritt E.A."/>
        <person name="Ahn M."/>
        <person name="Roach C."/>
        <person name="Hou Z."/>
        <person name="Verlinde C.L.M.J."/>
        <person name="Hol W.G.J."/>
        <person name="Fan E."/>
      </authorList>
      <dbReference type="PubMed" id="12405825"/>
      <dbReference type="DOI" id="10.1021/ja027584k"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.45 ANGSTROMS)</scope>
  </reference>
  <comment type="function">
    <text>The B subunit pentameric ring directs the A subunit to its target by binding to the GM1 gangliosides present on the surface of the intestinal epithelial cells. It can bind five GM1 gangliosides. It has no toxic activity by itself.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 2">The holotoxin (choleragen) consists of a pentameric ring of B subunits whose central pore is occupied by the A subunit. The A subunit contains two chains, A1 and A2, linked by a disulfide bridge.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-1038383">
      <id>P01556</id>
    </interactant>
    <interactant intactId="EBI-1038392">
      <id>P01555</id>
      <label>ctxA</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>5</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="5">Host cell membrane</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EMBL" id="K01170">
    <property type="protein sequence ID" value="AAA27573.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X00171">
    <property type="protein sequence ID" value="CAA24996.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X76390">
    <property type="protein sequence ID" value="CAA53973.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X76391">
    <property type="protein sequence ID" value="CAA53976.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X58786">
    <property type="protein sequence ID" value="CAA41593.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D30053">
    <property type="protein sequence ID" value="BAA06291.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AE003852">
    <property type="protein sequence ID" value="AAF94613.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="S14624">
    <property type="entry name" value="XVVCB"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_231099.1">
    <property type="nucleotide sequence ID" value="NC_002505.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000593522.1">
    <property type="nucleotide sequence ID" value="NZ_LT906614.1"/>
  </dbReference>
  <dbReference type="PDB" id="1CHP">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1CHQ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.10 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1CT1">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1FGB">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.40 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1G8Z">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1JR0">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.30 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1MD2">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.45 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1RCV">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1RD9">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.44 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1RDP">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.35 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1RF2">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.35 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1S5B">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.13 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1S5C">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1S5D">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.75 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1S5E">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.90 A"/>
    <property type="chains" value="D/E/F/G/H/J/K/L/M/N=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1S5F">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.60 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="1XTC">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.40 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="2CHB">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="3CHB">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.25 A"/>
    <property type="chains" value="D/E/F/G/H=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="3EFX">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.94 A"/>
    <property type="chains" value="D/E/F/G/H/I/J/K/L/M=23-124"/>
  </dbReference>
  <dbReference type="PDB" id="5ELC">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.50 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="5ELE">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="5ELF">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.55 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="5LZG">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.13 A"/>
    <property type="chains" value="A/B/C/D/E=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="5LZJ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.20 A"/>
    <property type="chains" value="A/B/C/D/E=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="6HJD">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.54 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="6HMW">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.95 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="6HMY">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="6HSV">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.45 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J/K/L/M/N/O/P/Q/R/S/T=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="7LVB">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.25 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="8OXS">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="C/D/E/F/G/H/I/J/K/L=22-124"/>
  </dbReference>
  <dbReference type="PDB" id="8QRE">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="C/D/E/F/G/H/I/J/K/L=22-124"/>
  </dbReference>
  <dbReference type="PDBsum" id="1CHP"/>
  <dbReference type="PDBsum" id="1CHQ"/>
  <dbReference type="PDBsum" id="1CT1"/>
  <dbReference type="PDBsum" id="1FGB"/>
  <dbReference type="PDBsum" id="1G8Z"/>
  <dbReference type="PDBsum" id="1JR0"/>
  <dbReference type="PDBsum" id="1MD2"/>
  <dbReference type="PDBsum" id="1RCV"/>
  <dbReference type="PDBsum" id="1RD9"/>
  <dbReference type="PDBsum" id="1RDP"/>
  <dbReference type="PDBsum" id="1RF2"/>
  <dbReference type="PDBsum" id="1S5B"/>
  <dbReference type="PDBsum" id="1S5C"/>
  <dbReference type="PDBsum" id="1S5D"/>
  <dbReference type="PDBsum" id="1S5E"/>
  <dbReference type="PDBsum" id="1S5F"/>
  <dbReference type="PDBsum" id="1XTC"/>
  <dbReference type="PDBsum" id="2CHB"/>
  <dbReference type="PDBsum" id="3CHB"/>
  <dbReference type="PDBsum" id="3EFX"/>
  <dbReference type="PDBsum" id="5ELC"/>
  <dbReference type="PDBsum" id="5ELE"/>
  <dbReference type="PDBsum" id="5ELF"/>
  <dbReference type="PDBsum" id="5LZG"/>
  <dbReference type="PDBsum" id="5LZJ"/>
  <dbReference type="PDBsum" id="6HJD"/>
  <dbReference type="PDBsum" id="6HMW"/>
  <dbReference type="PDBsum" id="6HMY"/>
  <dbReference type="PDBsum" id="6HSV"/>
  <dbReference type="PDBsum" id="7LVB"/>
  <dbReference type="PDBsum" id="8OXS"/>
  <dbReference type="PDBsum" id="8QRE"/>
  <dbReference type="AlphaFoldDB" id="P01556"/>
  <dbReference type="SMR" id="P01556"/>
  <dbReference type="ComplexPortal" id="CPX-2345">
    <property type="entry name" value="Cholera toxin"/>
  </dbReference>
  <dbReference type="DIP" id="DIP-6256N"/>
  <dbReference type="IntAct" id="P01556">
    <property type="interactions" value="5"/>
  </dbReference>
  <dbReference type="MINT" id="P01556"/>
  <dbReference type="STRING" id="243277.VC_1456"/>
  <dbReference type="DrugBank" id="DB02572">
    <property type="generic name" value="1,3-bis-([3-[3-[3-(4-{3-[3-nitro-5-(galactopyranosyloxy)-benzoylamino]-propyl}-piperazin-1-yl)-propylamino-3,4-dioxo-cyclobutenyl]-amino-propoxy-ethoxy-ethoxy]-propyl-]amino-carbonyloxy)-2-amino-propane"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB02903">
    <property type="generic name" value="1,3-bis-([[3-(4-{3-[3-nitro-5-(galactopyranosyloxy)-benzoylamino]-propyl}-piperazin-1-yl)-propylamino-3,4-dioxo-cyclobutenyl]-amino-ethyl]-amino-carbonyloxy)-2-amino-propane"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB02802">
    <property type="generic name" value="3-(alpha-D-Galactopyranosyloxy)-5-nitrobenzamide"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB04210">
    <property type="generic name" value="3-(alpha-D-galactopyranosyloxy)-N-(3-{4-[3-({2-[(3-{4-[3-({[3-(hexopyranosyloxy)-5-nitrophenyl]carbonyl}amino)propyl]piperazin-1-yl}propyl)amino]-3,4-dioxocyclobut-1-en-1-yl}amino)propyl]piperazin-1-yl}propyl)-5-nitrobenzamide"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB03077">
    <property type="generic name" value="3-Amino-4-{3-[2-(2-Propoxy-Ethoxy)-Ethoxy]-Propylamino}-Cyclobut-3-Ene-1,2-Dione"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB02379">
    <property type="generic name" value="Beta-D-Glucose"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB02474">
    <property type="generic name" value="BMSC-0013"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB02574">
    <property type="generic name" value="BV2"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB02213">
    <property type="generic name" value="Metanitrophenyl-Alpha-D-Galactoside"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB03524">
    <property type="generic name" value="N-[3-[4-(3-aminopropyl)piperazin-1-yl]propyl]-3-[(2-thiophen-2-ylacetyl)amino]-5-[(2R,3R,4S,5R,6R)-3,4,5-trihydroxy-6-(hydroxymethyl)oxan-2-yl]oxybenzamide"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB03721">
    <property type="generic name" value="N-acetyl-alpha-neuraminic acid"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB04073">
    <property type="generic name" value="N-{3-[4-(3-amino-propyl)-piperazin-1-yl]-propyl}-3-nitro-5-(galactopyranosyl)-beta-benzamide"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB03235">
    <property type="generic name" value="N-{3-[4-(3-aminopropyl)piperazin-1-yl]propyl}-3-(alpha-D-galactopyranosyloxy)-5-nitrobenzamide"/>
  </dbReference>
  <dbReference type="UniLectin" id="P01556"/>
  <dbReference type="ABCD" id="P01556">
    <property type="antibodies" value="1 sequenced antibody"/>
  </dbReference>
  <dbReference type="DNASU" id="2613962"/>
  <dbReference type="EnsemblBacteria" id="AAF94613">
    <property type="protein sequence ID" value="AAF94613"/>
    <property type="gene ID" value="VC_1456"/>
  </dbReference>
  <dbReference type="KEGG" id="vch:VC_1456"/>
  <dbReference type="PATRIC" id="fig|243277.26.peg.1386"/>
  <dbReference type="HOGENOM" id="CLU_2002942_0_0_6"/>
  <dbReference type="BioCyc" id="MetaCyc:FY484_RS07325-MONOMER"/>
  <dbReference type="EvolutionaryTrace" id="P01556"/>
  <dbReference type="PHI-base" id="PHI:699"/>
  <dbReference type="PRO" id="PR:P01556"/>
  <dbReference type="Proteomes" id="UP000000584">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="GO" id="GO:1902494">
    <property type="term" value="C:catalytic complex"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="CAFA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0020002">
    <property type="term" value="C:host cell plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042597">
    <property type="term" value="C:periplasmic space"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="CAFA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005534">
    <property type="term" value="F:galactose binding"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="CAFA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046812">
    <property type="term" value="F:host cell surface binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="TIGR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042531">
    <property type="term" value="P:positive regulation of tyrosine phosphorylation of STAT protein"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="CACAO"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.110:FF:000001">
    <property type="entry name" value="Cholera enterotoxin subunit B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.110">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008992">
    <property type="entry name" value="Enterotoxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001835">
    <property type="entry name" value="Enterotoxin_B"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01376">
    <property type="entry name" value="Enterotoxin_b"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00772">
    <property type="entry name" value="ENTEROTOXINB"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50203">
    <property type="entry name" value="Bacterial enterotoxins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0260">Enterotoxin</keyword>
  <keyword id="KW-1032">Host cell membrane</keyword>
  <keyword id="KW-1043">Host membrane</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="signal peptide" evidence="3 4">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000019344" description="Cholera enterotoxin subunit B">
    <location>
      <begin position="22"/>
      <end position="124"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="30"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of toxicity.">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="78"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; CAA24996." evidence="5" ref="2">
    <original>Y</original>
    <variation>S</variation>
    <location>
      <position position="33"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 7; AA sequence and 8; AA sequence." evidence="5" ref="7 8">
    <original>Y</original>
    <variation>H</variation>
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 7; AA sequence and 8; AA sequence." evidence="5" ref="7 8">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="43"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 7; AA sequence and 8; AA sequence." evidence="5" ref="7 8">
    <original>I</original>
    <variation>T</variation>
    <location>
      <position position="68"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 8; AA sequence." evidence="5" ref="8">
    <original>Q</original>
    <variation>E</variation>
    <location>
      <position position="70"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; CAA24996." evidence="5" ref="2">
    <original>G</original>
    <variation>S</variation>
    <location>
      <position position="75"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 7; AA sequence and 8; AA sequence." evidence="5" ref="7 8">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="91"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="26"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="36"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="46"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="58"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="68"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="turn" evidence="7">
    <location>
      <begin position="75"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="82"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="103"/>
      <end position="123"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11880036"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="3214"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="903362"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="903363"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="5LZG"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="5LZJ"/>
    </source>
  </evidence>
  <sequence length="124" mass="13957" checksum="9AA393E3EA8E3EBF" modified="1987-08-13" version="1" precursor="true">MIKLKFGVFFTVLLSSAYAHGTPQNITDLCAEYHNTQIYTLNDKIFSYTESLAGKREMAIITFKNGAIFQVEVPGSQHIDSQKKAIERMKDTLRIAYLTEAKVEKLCVWNNKTPHAIAAISMAN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>