<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-04-20" modified="2022-05-25" version="18" xmlns="http://uniprot.org/uniprot">
  <accession>P0CF05</accession>
  <name>TX4B_DINAS</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">M-poneritoxin-Da4b</fullName>
      <shortName evidence="4">M-PONTX-Da4b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Dinoponeratoxin Da-3177</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="5">Poneratoxin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Dinoponera australis</name>
    <name type="common">Giant neotropical hunting ant</name>
    <dbReference type="NCBI Taxonomy" id="609289"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Ponerinae</taxon>
      <taxon>Ponerini</taxon>
      <taxon>Dinoponera</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2010" name="Toxicon" volume="55" first="702" last="710">
      <title>A biochemical characterization of the major peptides from the venom of the giant Neotropical hunting ant Dinoponera australis.</title>
      <authorList>
        <person name="Johnson S.R."/>
        <person name="Copello J.A."/>
        <person name="Evans M.S."/>
        <person name="Suarez A.V."/>
      </authorList>
      <dbReference type="PubMed" id="19879289"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2009.10.021"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SYNTHESIS</scope>
    <scope>AMIDATION AT ALA-28</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2013" name="J. Proteomics" volume="94" first="413" last="422">
      <title>Peptidomic comparison and characterization of the major components of the venom of the giant ant Dinoponera quadriceps collected in four different areas of Brazil.</title>
      <authorList>
        <person name="Cologna C.T."/>
        <person name="Cardoso Jdos S."/>
        <person name="Jourdan E."/>
        <person name="Degueldre M."/>
        <person name="Upert G."/>
        <person name="Gilles N."/>
        <person name="Uetanabaro A.P."/>
        <person name="Costa Neto E.M."/>
        <person name="Thonart P."/>
        <person name="de Pauw E."/>
        <person name="Quinton L."/>
      </authorList>
      <dbReference type="PubMed" id="24157790"/>
      <dbReference type="DOI" id="10.1016/j.jprot.2013.10.017"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2016" name="Toxins" volume="8" first="1" last="28">
      <title>The biochemical toxin arsenal from ant venoms.</title>
      <authorList>
        <person name="Touchard A."/>
        <person name="Aili S.R."/>
        <person name="Fox E.G."/>
        <person name="Escoubas P."/>
        <person name="Orivel J."/>
        <person name="Nicholson G.M."/>
        <person name="Dejean A."/>
      </authorList>
      <dbReference type="PubMed" id="26805882"/>
      <dbReference type="DOI" id="10.3390/toxins8010030"/>
    </citation>
    <scope>REVIEW</scope>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2">The synthetic peptide has antimicrobial activity against the Gram-positive bacteria B.amyloliquefacies S499 (MIC=0.05 mM), L.monocytogenes 2231 and S.aureus ATCC 29213, against the Gram-negative bacteria P.putida BTP1, P.aeruginosa PaO1 and E.coli ATCC 10536, and against the fungi S.cerevisiae, R.mucilaginosa and C.cucumerinum. It is not active against the fungi F.oxysporum and B.cinerea.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="3176.7" method="Electrospray" evidence="1"/>
  <dbReference type="AlphaFoldDB" id="P0CF05"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000393349" description="M-poneritoxin-Da4b" evidence="1">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="modified residue" description="Alanine amide" evidence="1">
    <location>
      <position position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="19879289"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="24157790"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="19879289"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="26805882"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="19879289"/>
    </source>
  </evidence>
  <sequence length="28" mass="3180" checksum="0D76AC9C71067FEA" modified="2010-04-20" version="1">GLKDWWNKHKDKIIDVVKEMGKAGLQAA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>