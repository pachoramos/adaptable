<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-29" modified="2024-07-24" version="32" xmlns="http://uniprot.org/uniprot">
  <accession>A4H245</accession>
  <name>DB126_HYLLA</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 126</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 126</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB126</name>
  </gene>
  <organism>
    <name type="scientific">Hylobates lar</name>
    <name type="common">Lar gibbon</name>
    <name type="synonym">White-handed gibbon</name>
    <dbReference type="NCBI Taxonomy" id="9580"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hylobatidae</taxon>
      <taxon>Hylobates</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2006-11" db="EMBL/GenBank/DDBJ databases">
      <title>Evolution and sequence variation of human beta-defensin genes.</title>
      <authorList>
        <person name="Hollox E.J."/>
        <person name="Armour J.A.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3">Highly glycosylated atypical beta-defensin involved in several aspects of sperm function. Facilitates sperm transport in the female reproductive tract and contributes to sperm protection against immunodetection; both functions are probably implicating the negative surface charge provided by its O-linked oligosaccharides in the sperm glycocalyx. Involved in binding of sperm to oviductal epithelial cells to form a sperm reservoir until ovulation. Release from the sperm surface during capacitation and ovaluation by an elevation of oviductal fluid pH is unmasking other surface components and allows sperm to penetrate the cumulus matrix and bind to the zona pellucida of the oocyte. In vitro has antimicrobial activity and may inhibit LPS-mediated inflammation (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Homodimer or homooligomer; disulfide-linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
    <text evidence="2">Secreted by epididymal cells and is absorbed to the surface of sperm during transit through the epididymis.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2 3">O-glycosylated; glycans contain alpha(2,3)-linked sialic acids (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AM410150">
    <property type="protein sequence ID" value="CAL68960.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A4H245"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007338">
    <property type="term" value="P:single fertilization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050544">
    <property type="entry name" value="Beta-defensin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001:SF8">
    <property type="entry name" value="BETA-DEFENSIN 121"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001">
    <property type="entry name" value="BETA-DEFENSIN 123-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0278">Fertilization</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="4">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000436300" description="Beta-defensin 126">
    <location>
      <begin position="21"/>
      <end position="112"/>
    </location>
  </feature>
  <feature type="region of interest" description="In vitro binds to LPS, mediates antimicrobial activity and inhibits LPS-mediated inflammation" evidence="3">
    <location>
      <begin position="21"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="27"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="34"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="38"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain" evidence="2">
    <location>
      <position position="72"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P59665"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q9BEE3"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="Q9BYW3"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="4"/>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="112" mass="12127" checksum="EC47830BD7C4751A" modified="2007-05-01" version="1" precursor="true">MKSLLFTLAVFMLLAQLVSGNWYVKKCLNDVGICKKKCKPEELHVKNGRAMCGKQRDCCVPADKRANYPAFCVQTKTTRTSTVTATAATTTTLVMTTASMSSMAPTPVSPTS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>