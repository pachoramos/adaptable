<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-01-16" modified="2024-01-24" version="45" xmlns="http://uniprot.org/uniprot">
  <accession>P83639</accession>
  <name>DRS2_PHYDS</name>
  <protein>
    <recommendedName>
      <fullName evidence="7">Dermaseptin-DI2</fullName>
      <shortName evidence="7">DRS-DI2</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6">Dermadistinctin-L</fullName>
      <shortName evidence="6">DD L</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Phyllomedusa distincta</name>
    <name type="common">Monkey frog</name>
    <dbReference type="NCBI Taxonomy" id="164618"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phyllomedusa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Peptides" volume="20" first="679" last="686">
      <title>Antimicrobial peptides from the Brazilian frog Phyllomedusa distincta.</title>
      <authorList>
        <person name="Batista C.V.F."/>
        <person name="da Silva L.R."/>
        <person name="Sebben A."/>
        <person name="Scaloni A."/>
        <person name="Ferrara L."/>
        <person name="Paiva G.R."/>
        <person name="Olamendi-Portugal T."/>
        <person name="Possani L.D."/>
        <person name="Bloch C. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="10477123"/>
      <dbReference type="DOI" id="10.1016/s0196-9781(99)00050-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>CIRCULAR DICHROISM ANALYSIS</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2002" name="J. Biol. Chem." volume="277" first="49332" last="49340">
      <title>Dermaseptins from Phyllomedusa oreades and Phyllomedusa distincta. Anti-Trypanosoma cruzi activity without cytotoxicity to mammalian cells.</title>
      <authorList>
        <person name="Brand G.D."/>
        <person name="Leite J.R.S.A."/>
        <person name="Silva L.P."/>
        <person name="Albuquerque S."/>
        <person name="Prates M.V."/>
        <person name="Azevedo R.B."/>
        <person name="Carregaro V."/>
        <person name="Silva J.S."/>
        <person name="Sa V.C.L."/>
        <person name="Brandao R.A."/>
        <person name="Bloch C. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="12379643"/>
      <dbReference type="DOI" id="10.1074/jbc.m209289200"/>
    </citation>
    <scope>SYNTHESIS</scope>
    <scope>FUNCTION AS AN ANTIPROTOZOAN PROTEIN</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2008" name="Comp. Biochem. Physiol." volume="151A" first="329" last="335">
      <title>Dermaseptins from Phyllomedusa oreades and Phyllomedusa distincta: liposomes fusion and/or lysis investigated by fluorescence and atomic force microscopy.</title>
      <authorList>
        <person name="Silva L.P."/>
        <person name="Leite J.R.S.A."/>
        <person name="Brand G.D."/>
        <person name="Regis W.B."/>
        <person name="Tedesco A.C."/>
        <person name="Azevedo R.B."/>
        <person name="Freitas S.M."/>
        <person name="Bloch C. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="17409003"/>
      <dbReference type="DOI" id="10.1016/j.cbpa.2007.02.031"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2008" name="Comp. Biochem. Physiol." volume="151A" first="336" last="343">
      <title>Dermaseptins from Phyllomedusa oreades and Phyllomedusa distincta: Secondary structure, antimicrobial activity, and mammalian cell toxicity.</title>
      <authorList>
        <person name="Leite J.R.S.A."/>
        <person name="Brand G.D."/>
        <person name="Silva L.P."/>
        <person name="Kuckelhaus S.A.S."/>
        <person name="Bento W.R.C."/>
        <person name="Araujo A.L.T."/>
        <person name="Martins G.R."/>
        <person name="Lazzari A.M."/>
        <person name="Bloch C. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="17442605"/>
      <dbReference type="DOI" id="10.1016/j.cbpa.2007.03.016"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2008" name="Peptides" volume="29" first="2074" last="2082">
      <title>A consistent nomenclature of antimicrobial peptides isolated from frogs of the subfamily Phyllomedusinae.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="18644413"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.06.017"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 4 5">Has antibacterial activity against the Gram-positive bacteria S.aureus and E.faecalis, and the Gram-negative bacteria P.aeruginosa and E.coli. Has antiprotozoal activity against T.cruzi. Has antifungal activity against the yeasts C.tropicalis (MIC=10.9 uM), C.guilliermondii (MIC=21.8 uM), C.albicans (MIC=21.8 uM) and C.albicans ATCC 1023 (MIC=10.9 uM). Decreases viability of murine peritoneal cells. Fuses to, and disrupts liposomes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2926.6" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=0958"/>
  </comment>
  <dbReference type="AlphaFoldDB" id="P83639"/>
  <dbReference type="SMR" id="P83639"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022731">
    <property type="entry name" value="Dermaseptin_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF12121">
    <property type="entry name" value="DD_K"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043637" description="Dermaseptin-DI2" evidence="2">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10477123"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12379643"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="17409003"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="17442605"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="10477123"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <sequence length="28" mass="2926" checksum="84E71BA9D9A57CDE" modified="2003-10-01" version="1">ALWKTLLKNVGKAAGKAALNAVTDMVNQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>