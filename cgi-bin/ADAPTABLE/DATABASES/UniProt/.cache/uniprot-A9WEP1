<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-06-13" modified="2024-03-27" version="72" xmlns="http://uniprot.org/uniprot">
  <accession>A9WEP1</accession>
  <name>CAS2A_CHLAA</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">CRISPR-associated endoribonuclease Cas2 1</fullName>
      <ecNumber evidence="1">3.1.-.-</ecNumber>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">cas2-1</name>
    <name type="ordered locus">Caur_0554</name>
  </gene>
  <organism>
    <name type="scientific">Chloroflexus aurantiacus (strain ATCC 29366 / DSM 635 / J-10-fl)</name>
    <dbReference type="NCBI Taxonomy" id="324602"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Chloroflexota</taxon>
      <taxon>Chloroflexia</taxon>
      <taxon>Chloroflexales</taxon>
      <taxon>Chloroflexineae</taxon>
      <taxon>Chloroflexaceae</taxon>
      <taxon>Chloroflexus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2011" name="BMC Genomics" volume="12" first="334" last="334">
      <title>Complete genome sequence of the filamentous anoxygenic phototrophic bacterium Chloroflexus aurantiacus.</title>
      <authorList>
        <person name="Tang K.H."/>
        <person name="Barry K."/>
        <person name="Chertkov O."/>
        <person name="Dalin E."/>
        <person name="Han C.S."/>
        <person name="Hauser L.J."/>
        <person name="Honchak B.M."/>
        <person name="Karbach L.E."/>
        <person name="Land M.L."/>
        <person name="Lapidus A."/>
        <person name="Larimer F.W."/>
        <person name="Mikhailova N."/>
        <person name="Pitluck S."/>
        <person name="Pierson B.K."/>
        <person name="Blankenship R.E."/>
      </authorList>
      <dbReference type="PubMed" id="21714912"/>
      <dbReference type="DOI" id="10.1186/1471-2164-12-334"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 29366 / DSM 635 / J-10-fl</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">CRISPR (clustered regularly interspaced short palindromic repeat), is an adaptive immune system that provides protection against mobile genetic elements (viruses, transposable elements and conjugative plasmids). CRISPR clusters contain sequences complementary to antecedent mobile elements and target invading nucleic acids. CRISPR clusters are transcribed and processed into CRISPR RNA (crRNA). Functions as a ssRNA-specific endoribonuclease. Involved in the integration of spacer DNA into the CRISPR cassette.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="1">
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </cofactor>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer, forms a heterotetramer with a Cas1 homodimer.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the CRISPR-associated endoribonuclease Cas2 protein family.</text>
  </comment>
  <dbReference type="EC" id="3.1.-.-" evidence="1"/>
  <dbReference type="EMBL" id="CP000909">
    <property type="protein sequence ID" value="ABY33800.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_012256456.1">
    <property type="nucleotide sequence ID" value="NC_010175.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="YP_001634189.1">
    <property type="nucleotide sequence ID" value="NC_010175.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A9WEP1"/>
  <dbReference type="SMR" id="A9WEP1"/>
  <dbReference type="STRING" id="324602.Caur_0554"/>
  <dbReference type="EnsemblBacteria" id="ABY33800">
    <property type="protein sequence ID" value="ABY33800"/>
    <property type="gene ID" value="Caur_0554"/>
  </dbReference>
  <dbReference type="KEGG" id="cau:Caur_0554"/>
  <dbReference type="PATRIC" id="fig|324602.8.peg.634"/>
  <dbReference type="eggNOG" id="COG1343">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_161124_3_0_0"/>
  <dbReference type="InParanoid" id="A9WEP1"/>
  <dbReference type="Proteomes" id="UP000002008">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004521">
    <property type="term" value="F:RNA endonuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051607">
    <property type="term" value="P:defense response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043571">
    <property type="term" value="P:maintenance of CRISPR repeat elements"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="CDD" id="cd09725">
    <property type="entry name" value="Cas2_I_II_III"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.70.240">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01471">
    <property type="entry name" value="Cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR021127">
    <property type="entry name" value="CRISPR_associated_Cas2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019199">
    <property type="entry name" value="Virulence_VapD/CRISPR_Cas2"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01573">
    <property type="entry name" value="cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34405">
    <property type="entry name" value="CRISPR-ASSOCIATED ENDORIBONUCLEASE CAS2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34405:SF3">
    <property type="entry name" value="CRISPR-ASSOCIATED ENDORIBONUCLEASE CAS2 3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF09827">
    <property type="entry name" value="CRISPR_Cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF032582">
    <property type="entry name" value="Cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF143430">
    <property type="entry name" value="TTP0101/SSO1404-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0051">Antiviral defense</keyword>
  <keyword id="KW-0255">Endonuclease</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0460">Magnesium</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000417708" description="CRISPR-associated endoribonuclease Cas2 1">
    <location>
      <begin position="1"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="8"/>
    </location>
    <ligand>
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
      <note>catalytic</note>
    </ligand>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01471"/>
    </source>
  </evidence>
  <sequence length="93" mass="10981" checksum="9C77CA21BB787639" modified="2008-02-05" version="1">MFYLISYDISVDQRRLKIAKLLEGYGQRVLESVFECDLELPAYRQLRQKLNRLIKDEEGDRLRIYRLCASCREQIEIIGDGPPPETSQDIYII</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>