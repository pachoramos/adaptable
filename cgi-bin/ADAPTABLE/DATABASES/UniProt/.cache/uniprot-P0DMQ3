<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2014-10-29" modified="2024-03-27" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>P0DMQ3</accession>
  <name>TO1D_HADMO</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Omega-hexatoxin-Hmo1d</fullName>
      <shortName evidence="5">Omega-HXTX-Hmo1d</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Hadronyche modesta</name>
    <name type="common">Victorian funnel-web spider</name>
    <dbReference type="NCBI Taxonomy" id="1337084"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Mygalomorphae</taxon>
      <taxon>Hexathelidae</taxon>
      <taxon>Hadronyche</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2014" name="BMC Genomics" volume="15" first="177" last="177">
      <title>Diversification of a single ancestral gene into a successful toxin superfamily in highly venomous Australian funnel-web spiders.</title>
      <authorList>
        <person name="Pineda S.S."/>
        <person name="Sollod B.L."/>
        <person name="Wilson D."/>
        <person name="Darling A."/>
        <person name="Sunagar K."/>
        <person name="Undheim E.A."/>
        <person name="Kely L."/>
        <person name="Antunes A."/>
        <person name="Fry B.G."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="24593665"/>
      <dbReference type="DOI" id="10.1186/1471-2164-15-177"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Inhibits insect, but not mammalian, voltage-gated calcium channels (Cav).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the neurotoxin 08 (Shiva) family. 01 (omega toxin) subfamily.</text>
  </comment>
  <comment type="caution">
    <text>Signal and propeptide sequences are imported from ArachnoServer. The sequence differences may reflect different paralogs.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DMQ3"/>
  <dbReference type="SMR" id="P0DMQ3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019855">
    <property type="term" value="F:calcium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009415">
    <property type="entry name" value="Omega-atracotox"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018071">
    <property type="entry name" value="Omega-atracotox_CS"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06357">
    <property type="entry name" value="Omega-toxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57059">
    <property type="entry name" value="omega toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60016">
    <property type="entry name" value="OMEGA_ACTX_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0108">Calcium channel impairing toxin</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1218">Voltage-gated calcium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000430920" evidence="1">
    <location>
      <begin position="23"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000430921" description="Omega-hexatoxin-Hmo1d">
    <location>
      <begin position="48"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="site" description="Critical for insecticidal activity" evidence="2">
    <location>
      <position position="57"/>
    </location>
  </feature>
  <feature type="site" description="Critical for insecticidal activity" evidence="2">
    <location>
      <position position="74"/>
    </location>
  </feature>
  <feature type="site" description="Critical for insecticidal activity" evidence="2">
    <location>
      <position position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="51"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="58"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="64"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1." ref="1">
    <original>C</original>
    <variation>F</variation>
    <location>
      <position position="19"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1." ref="1">
    <original>Q</original>
    <variation>E</variation>
    <location>
      <position position="21"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1." ref="1">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="24"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1." ref="1">
    <original>M</original>
    <variation>R</variation>
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1." ref="1">
    <original>D</original>
    <variation>E</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1." ref="1">
    <original>K</original>
    <variation>E</variation>
    <location>
      <position position="42"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1." ref="1">
    <original>R</original>
    <variation>K</variation>
    <location>
      <position position="43"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P56207"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="24593665"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="24593665"/>
    </source>
  </evidence>
  <sequence length="84" mass="9021" checksum="99C76281D747F849" modified="2014-10-29" version="1" precursor="true">MNTATGVIALLVLATVIGCIQAEDTMADLQGGFESYDGEAAKRIFRRSPVCIPSGQPCPYNEHCCSGSCTYKENENGNTVQRCD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>