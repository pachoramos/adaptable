<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-10-10" modified="2024-11-27" version="66" xmlns="http://uniprot.org/uniprot">
  <accession>P83430</accession>
  <name>SPHE2_APTPA</name>
  <protein>
    <recommendedName>
      <fullName>Spheniscin-2</fullName>
      <shortName>Sphe-2</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>pBD-2</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Aptenodytes patagonicus</name>
    <name type="common">King penguin</name>
    <dbReference type="NCBI Taxonomy" id="9234"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Archelosauria</taxon>
      <taxon>Archosauria</taxon>
      <taxon>Dinosauria</taxon>
      <taxon>Saurischia</taxon>
      <taxon>Theropoda</taxon>
      <taxon>Coelurosauria</taxon>
      <taxon>Aves</taxon>
      <taxon>Neognathae</taxon>
      <taxon>Neoaves</taxon>
      <taxon>Aequornithes</taxon>
      <taxon>Sphenisciformes</taxon>
      <taxon>Spheniscidae</taxon>
      <taxon>Aptenodytes</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="J. Biol. Chem." volume="278" first="51053" last="51058">
      <title>Spheniscins, avian beta-defensins in preserved stomach contents of the king penguin, Aptenodytes patagonicus.</title>
      <authorList>
        <person name="Thouzeau C."/>
        <person name="Le Maho Y."/>
        <person name="Froget G."/>
        <person name="Sabatier L."/>
        <person name="Le Bohec C."/>
        <person name="Hoffmann J.A."/>
        <person name="Bulet P."/>
      </authorList>
      <dbReference type="PubMed" id="14525994"/>
      <dbReference type="DOI" id="10.1074/jbc.m306839200"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue>Stomach</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="J. Biol. Chem." volume="279" first="30433" last="30439">
      <title>Solution structure of spheniscin, a beta-defensin from the penguin stomach.</title>
      <authorList>
        <person name="Landon C."/>
        <person name="Thouzeau C."/>
        <person name="Labbe H."/>
        <person name="Bulet P."/>
        <person name="Vovelle F."/>
      </authorList>
      <dbReference type="PubMed" id="15123713"/>
      <dbReference type="DOI" id="10.1074/jbc.m401338200"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has antifungal activity and antibacterial activity against Gram-positive and Gram-negative bacteria. Involved in the process of food preservation in the stomach during the incubation fast. May also be present during infection.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Secreted into the stomach cavity.</text>
  </comment>
  <comment type="mass spectrometry" mass="4501.7" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="PDB" id="1UT3">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-38"/>
  </dbReference>
  <dbReference type="PDBsum" id="1UT3"/>
  <dbReference type="AlphaFoldDB" id="P83430"/>
  <dbReference type="SMR" id="P83430"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044729" description="Spheniscin-2">
    <location>
      <begin position="1"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="5"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="12"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="17"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="7"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="11"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="21"/>
      <end position="36"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="14525994"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15123713"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="1UT3"/>
    </source>
  </evidence>
  <sequence length="38" mass="4507" checksum="F4239C88C6187203" modified="2003-10-10" version="1">SFGLCRLRRGFCARGRCRFPSIPIGRCSRFVQCCRRVW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>