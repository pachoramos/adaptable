<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2021-09-29" modified="2023-09-13" version="15" xmlns="http://uniprot.org/uniprot">
  <accession>D1MEJ2</accession>
  <name>VP5_EUMPO</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Venom peptide 5</fullName>
      <shortName evidence="3">EpVP5</shortName>
      <shortName evidence="6">VP5</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Eumenes pomiformis</name>
    <name type="common">Potter wasp</name>
    <name type="synonym">Vespa pomiformis</name>
    <dbReference type="NCBI Taxonomy" id="693051"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Eumeninae</taxon>
      <taxon>Eumenes</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2010" name="Toxicon" volume="55" first="1147" last="1156">
      <title>Differential gene expression profiles in the venom gland/sac of Eumenes pomiformis (Hymenoptera: Eumenidae).</title>
      <authorList>
        <person name="Baek J.H."/>
        <person name="Lee S.H."/>
      </authorList>
      <dbReference type="PubMed" id="20096300"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2010.01.004"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2011" name="Peptides" volume="32" first="568" last="572">
      <title>Venom peptides from solitary hunting wasps induce feeding disorder in lepidopteran larvae.</title>
      <authorList>
        <person name="Baek J.H."/>
        <person name="Ji Y."/>
        <person name="Shin J.S."/>
        <person name="Lee S."/>
        <person name="Lee S.H."/>
      </authorList>
      <dbReference type="PubMed" id="21184791"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2010.12.007"/>
    </citation>
    <scope>PROBABLE DISULFIDE BOND</scope>
    <scope>SYNTHESIS OF 43-56</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
    <text evidence="5">Has a coil conformation.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="5">Probably contains 1 disulfide bond, which may be crucial for activity, since the linear peptide without disulfide bond is inactive.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="4">Not found in venom, suggesting that it is a minor component.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">Negative results: The synthetic peptide (tested without disulfide bond) does not show activity against fungi (B.cinerea and C.albicans) and bacteria (E.coli and S.aureus) (PubMed:21184791). Has no hemolytic activity against human erythrocytes (PubMed:21184791). Does not show cytolytic activity against insect cell lines (PubMed:21184791). Does not induce feeding disorder in lepidopteran larvae after peptide injection in the vicinity of the head and thorax (PubMed:21184791).</text>
  </comment>
  <dbReference type="EMBL" id="GU136237">
    <property type="protein sequence ID" value="ACZ37398.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="D1MEJ2"/>
  <dbReference type="SMR" id="D1MEJ2"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000453653" evidence="4">
    <location>
      <begin position="27"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5003025095" description="Venom peptide 5" evidence="4">
    <location>
      <begin position="43"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="49"/>
      <end position="54"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="21184791"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="20096300"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="20096300"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="21184791"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="ACZ37398.1"/>
    </source>
  </evidence>
  <sequence length="56" mass="6025" checksum="ECCE9CDAB578FF8B" modified="2010-01-19" version="1" precursor="true">MKTASFILSFVVLLIVIITWIGEVSAVSEPEPVAKATAHAAAVHVPPICSHRECRK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>