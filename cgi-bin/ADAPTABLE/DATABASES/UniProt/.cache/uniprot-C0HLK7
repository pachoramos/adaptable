<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-07-31" modified="2021-06-02" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>C0HLK7</accession>
  <name>LMCSN_LUMTE</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Lumbricusin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lumbricus terrestris</name>
    <name type="common">Common earthworm</name>
    <dbReference type="NCBI Taxonomy" id="6398"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Annelida</taxon>
      <taxon>Clitellata</taxon>
      <taxon>Oligochaeta</taxon>
      <taxon>Crassiclitellata</taxon>
      <taxon>Lumbricina</taxon>
      <taxon>Lumbricidae</taxon>
      <taxon>Lumbricinae</taxon>
      <taxon>Lumbricus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2014" name="Biochem. Biophys. Res. Commun." volume="448" first="292" last="297">
      <title>Neurotropic and neuroprotective activities of the earthworm peptide Lumbricusin.</title>
      <authorList>
        <person name="Kim D.H."/>
        <person name="Lee I.H."/>
        <person name="Nam S.T."/>
        <person name="Hong J."/>
        <person name="Zhang P."/>
        <person name="Hwang J.S."/>
        <person name="Seok H."/>
        <person name="Choi H."/>
        <person name="Lee D.G."/>
        <person name="Kim J.I."/>
        <person name="Kim H."/>
      </authorList>
      <dbReference type="PubMed" id="24796676"/>
      <dbReference type="DOI" id="10.1016/j.bbrc.2014.04.105"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SYNTHESIS</scope>
  </reference>
  <reference evidence="4" key="2">
    <citation type="journal article" date="2015" name="J. Microbiol. Biotechnol." volume="25" first="1640" last="1647">
      <title>Antimicrobial peptide, Lumbricusin, ameliorates motor dysfunction and dopaminergic neurodegeneration in a mouse model of Parkinson's Disease.</title>
      <authorList>
        <person name="Kim D.H."/>
        <person name="Lee I.H."/>
        <person name="Nam S.T."/>
        <person name="Hong J."/>
        <person name="Zhang P."/>
        <person name="Lu L.F."/>
        <person name="Hwang J.S."/>
        <person name="Park K.C."/>
        <person name="Kim H."/>
      </authorList>
      <dbReference type="PubMed" id="26215270"/>
      <dbReference type="DOI" id="10.4014/jmb.1507.07011"/>
    </citation>
    <scope>SYNTHESIS</scope>
  </reference>
  <comment type="miscellaneous">
    <text evidence="1 2">Induces proliferation of human neuroblastoma cells but not of human colonic epithelial cells or human adrenal carcinoma cells (PubMed:24796676). Also increases proliferation of mouse neural stem cells (PubMed:26215270). In human neuroblastoma cells and mouse neural stem cells, inhibits apoptosis induced by the synthetic neurotoxic compound 6-hydroxydopamine (6-OHDA) (PubMed:24796676). In human neuroblastoma cells and mouse neural stem cells, increases ubiquitination of cyclin-dependent kinase inhibitor CDKN1B, promoting its degradation (PubMed:24796676, PubMed:26215270). Attenuates motor impairments in 6-OHDA-treated mice (PubMed:26215270).</text>
  </comment>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <feature type="peptide" id="PRO_0000447646" description="Lumbricusin" evidence="1">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="24796676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="26215270"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="24796676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="11" mass="1446" checksum="70007740A05EA364" modified="2019-07-31" version="1">RNRRWCIDQQA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>