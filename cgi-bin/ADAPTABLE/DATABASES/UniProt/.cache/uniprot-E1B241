<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-06-07" modified="2023-05-03" version="30" xmlns="http://uniprot.org/uniprot">
  <accession>E1B241</accession>
  <name>BR12_AMOMA</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Brevinin-1MT2</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Amolops mantzorum</name>
    <name type="common">Sichuan torrent frog</name>
    <dbReference type="NCBI Taxonomy" id="167930"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Amolops</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2014" name="Zool. Sci." volume="31" first="143" last="151">
      <title>Peptidomic analysis of antimicrobial peptides in skin secretions of Amolops mantzorum.</title>
      <authorList>
        <person name="Hu Y."/>
        <person name="Yu Z."/>
        <person name="Xu S."/>
        <person name="Hu Y."/>
        <person name="Guo C."/>
        <person name="Li F."/>
        <person name="Li J."/>
        <person name="Liu J."/>
        <person name="Wang H."/>
      </authorList>
      <dbReference type="PubMed" id="24601776"/>
      <dbReference type="DOI" id="10.2108/zsj.31.143"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 47-70</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin</tissue>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide with activity against a variety of Gram-negative and Gram-positive bacteria and against fungi (By similarity). Shows strong hemolytic activity against human erythrocytes (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the skin glands.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="5">The primary structure of this peptide is identical to that of Brevinins-ALa (AC A0SN38).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=00860"/>
  </comment>
  <dbReference type="EMBL" id="HQ128616">
    <property type="protein sequence ID" value="ADM34274.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="E1B241"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012520">
    <property type="entry name" value="Antimicrobial_frog_1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08018">
    <property type="entry name" value="Antimicrobial_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000440076" evidence="6">
    <location>
      <begin position="23"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000440077" description="Brevinin-1MT2" evidence="3">
    <location>
      <begin position="47"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="64"/>
      <end position="70"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="E1B240"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="24601776"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="24601776"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="24601776"/>
    </source>
  </evidence>
  <sequence length="70" mass="8189" checksum="5616BDAE077EAD14" modified="2010-11-02" version="1" precursor="true">MFTLKKSMLLLFFLGTINLSLCEQERNADEEERRDDDEMDVEVEKRFLPMLAGLAANFLPKLFCKITKKC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>