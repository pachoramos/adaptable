<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-11-27" version="188" xmlns="http://uniprot.org/uniprot">
  <accession>O00253</accession>
  <accession>O15459</accession>
  <accession>Q2TBD9</accession>
  <name>AGRP_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Agouti-related protein</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">AGRP</name>
    <name type="synonym">AGRT</name>
    <name type="synonym">ART</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Genes Dev." volume="11" first="593" last="602">
      <title>Hypothalamic expression of ART, a novel gene related to agouti, is up-regulated in obese and diabetic mutant mice.</title>
      <authorList>
        <person name="Shutter J.R."/>
        <person name="Graham M."/>
        <person name="Kinsey A.C."/>
        <person name="Scully S."/>
        <person name="Luethy R."/>
        <person name="Stark K.L."/>
      </authorList>
      <dbReference type="PubMed" id="9119224"/>
      <dbReference type="DOI" id="10.1101/gad.11.5.593"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="Science" volume="278" first="135" last="138">
      <title>Antagonism of central melanocortin receptors in vitro and in vivo by agouti-related protein.</title>
      <authorList>
        <person name="Ollmann M.M."/>
        <person name="Wilson B.D."/>
        <person name="Yang Y.K."/>
        <person name="Kerns J.A."/>
        <person name="Chen Y."/>
        <person name="Gantz I."/>
        <person name="Barsh G.S."/>
      </authorList>
      <dbReference type="PubMed" id="9311920"/>
      <dbReference type="DOI" id="10.1126/science.278.5335.135"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Adrenal gland</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2001" name="Gene" volume="277" first="231" last="238">
      <title>The gene structure and minimal promoter of the human agouti related protein.</title>
      <authorList>
        <person name="Brown A.M."/>
        <person name="Mayfield D.K."/>
        <person name="Volaufova J."/>
        <person name="Argyropoulos G."/>
      </authorList>
      <dbReference type="PubMed" id="11602360"/>
      <dbReference type="DOI" id="10.1016/s0378-1119(01)00705-3"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>VARIANT THR-67</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2001" name="Mol. Psychiatry" volume="6" first="325" last="328">
      <title>Association between an agouti-related protein gene polymorphism and anorexia nervosa.</title>
      <authorList>
        <person name="Vink T."/>
        <person name="Hinney A."/>
        <person name="van Elburg A.A."/>
        <person name="van Goozen S.H."/>
        <person name="Sandkuijl L.A."/>
        <person name="Sinke R.J."/>
        <person name="Herpertz-Dahlmann B.M."/>
        <person name="Hebebrand J."/>
        <person name="Remschmidt H."/>
        <person name="van Engeland H."/>
        <person name="Adan R.A."/>
      </authorList>
      <dbReference type="PubMed" id="11326303"/>
      <dbReference type="DOI" id="10.1038/sj.mp.4000854"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="5">
    <citation type="submission" date="2006-01" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="SeattleSNPs variation discovery resource"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1998" name="Biochemistry" volume="37" first="12172" last="12177">
      <title>Determination of disulfide structure in agouti-related protein (AGRP) by stepwise reduction and alkylation.</title>
      <authorList>
        <person name="Bures E.J."/>
        <person name="Hui J.O."/>
        <person name="Young Y."/>
        <person name="Chow D.T."/>
        <person name="Katta V."/>
        <person name="Rohde M.F."/>
        <person name="Zeni L."/>
        <person name="Rosenfeld R.D."/>
        <person name="Stark K.L."/>
        <person name="Haniu M."/>
      </authorList>
      <dbReference type="PubMed" id="9724530"/>
      <dbReference type="DOI" id="10.1021/bi981082v"/>
    </citation>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="1999" name="Mol. Endocrinol." volume="13" first="148" last="155">
      <title>Characterization of Agouti-related protein binding to melanocortin receptors.</title>
      <authorList>
        <person name="Yang Y.K."/>
        <person name="Thompson D.A."/>
        <person name="Dickinson C.J."/>
        <person name="Wilken J."/>
        <person name="Barsh G.S."/>
        <person name="Kent S.B."/>
        <person name="Gantz I."/>
      </authorList>
      <dbReference type="PubMed" id="9892020"/>
      <dbReference type="DOI" id="10.1210/mend.13.1.0223"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH MC3R; MC4R AND MC5R</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2001" name="Mol. Endocrinol." volume="15" first="164" last="171">
      <title>AgRP(83-132) acts as an inverse agonist on the human-melanocortin-4 receptor.</title>
      <authorList>
        <person name="Nijenhuis W.A."/>
        <person name="Oosterom J."/>
        <person name="Adan R.A."/>
      </authorList>
      <dbReference type="PubMed" id="11145747"/>
      <dbReference type="DOI" id="10.1210/mend.15.1.0578"/>
    </citation>
    <scope>FUNCTION AS INVERSE AGONIST FOR MC3R AND MC4R</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2006" name="Chem. Biol." volume="13" first="1297" last="1305">
      <title>Structural and molecular evolutionary analysis of Agouti and Agouti-related proteins.</title>
      <authorList>
        <person name="Jackson P.J."/>
        <person name="Douglas N.R."/>
        <person name="Chai B."/>
        <person name="Binkley J."/>
        <person name="Sidow A."/>
        <person name="Barsh G.S."/>
        <person name="Millhauser G.L."/>
      </authorList>
      <dbReference type="PubMed" id="17185225"/>
      <dbReference type="DOI" id="10.1016/j.chembiol.2006.10.006"/>
    </citation>
    <scope>IDENTIFICATION OF MATURE N-TERMINUS</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2006" name="Endocrinology" volume="147" first="1621" last="1631">
      <title>Agouti-related protein is posttranslationally cleaved by proprotein convertase 1 to generate agouti-related protein (AGRP)83-132: interaction between AGRP83-132 and melanocortin receptors cannot be influenced by syndecan-3.</title>
      <authorList>
        <person name="Creemers J.W."/>
        <person name="Pritchard L.E."/>
        <person name="Gyte A."/>
        <person name="Le Rouzic P."/>
        <person name="Meulemans S."/>
        <person name="Wardlaw S.L."/>
        <person name="Zhu X."/>
        <person name="Steiner D.F."/>
        <person name="Davies N."/>
        <person name="Armstrong D."/>
        <person name="Lawrence C.B."/>
        <person name="Luckman S.M."/>
        <person name="Schmitz C.A."/>
        <person name="Davies R.A."/>
        <person name="Brennand J.C."/>
        <person name="White A."/>
      </authorList>
      <dbReference type="PubMed" id="16384863"/>
      <dbReference type="DOI" id="10.1210/en.2005-1373"/>
    </citation>
    <scope>IDENTIFICATION OF MATURE N-TERMINUS</scope>
    <scope>CLEAVAGE BY PCSK1</scope>
    <scope>MUTAGENESIS OF 79-ARG--ARG-82; 85-ARG-ARG-86 AND 86-ARG--ARG-89</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2006" name="J. Biol. Chem." volume="281" first="37447" last="37456">
      <title>The natural inverse agonist agouti-related protein induces arrestin-mediated endocytosis of melanocortin-3 and -4 receptors.</title>
      <authorList>
        <person name="Breit A."/>
        <person name="Wolff K."/>
        <person name="Kalwa H."/>
        <person name="Jarry H."/>
        <person name="Buch T."/>
        <person name="Gudermann T."/>
      </authorList>
      <dbReference type="PubMed" id="17041250"/>
      <dbReference type="DOI" id="10.1074/jbc.m605982200"/>
    </citation>
    <scope>FUNCTION IN MC3R AND MC4R ENDOCYTOSIS</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="1999" name="FEBS Lett." volume="451" first="125" last="131">
      <title>NMR structure of a minimized human agouti related protein prepared by total chemical synthesis.</title>
      <authorList>
        <person name="Bolin K.A."/>
        <person name="Anderson D.J."/>
        <person name="Trulson J.A."/>
        <person name="Thompson D.A."/>
        <person name="Wilken J."/>
        <person name="Kent S.B.H."/>
        <person name="Gantz I."/>
        <person name="Millhauser G.L."/>
      </authorList>
      <dbReference type="PubMed" id="10371151"/>
      <dbReference type="DOI" id="10.1016/s0014-5793(99)00553-0"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 87-132</scope>
    <scope>FUNCTION</scope>
    <scope>CIRCULAR DICHROISM</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>MUTAGENESIS OF ARG-111</scope>
  </reference>
  <reference key="14">
    <citation type="journal article" date="2001" name="Biochemistry" volume="40" first="15520" last="15527">
      <title>High-resolution NMR structure of the chemically-synthesized melanocortin receptor binding domain AGRP(87-132) of the agouti-related protein.</title>
      <authorList>
        <person name="McNulty J.C."/>
        <person name="Thompson D.A."/>
        <person name="Bolin K.A."/>
        <person name="Wilken J."/>
        <person name="Barsh G.S."/>
        <person name="Millhauser G.L."/>
      </authorList>
      <dbReference type="PubMed" id="11747427"/>
      <dbReference type="DOI" id="10.1021/bi0117192"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 87-132</scope>
    <scope>DOMAIN</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <reference key="15">
    <citation type="journal article" date="2002" name="Biochemistry" volume="41" first="7565" last="7572">
      <title>Design, pharmacology, and NMR structure of a minimized cystine knot with agouti-related protein activity.</title>
      <authorList>
        <person name="Jackson P.J."/>
        <person name="McNulty J.C."/>
        <person name="Yang Y.K."/>
        <person name="Thompson D.A."/>
        <person name="Chai B."/>
        <person name="Gantz I."/>
        <person name="Barsh G.S."/>
        <person name="Millhauser G.L."/>
      </authorList>
      <dbReference type="PubMed" id="12056887"/>
      <dbReference type="DOI" id="10.1021/bi012000x"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 87-120</scope>
  </reference>
  <reference key="16">
    <citation type="journal article" date="2002" name="J. Clin. Endocrinol. Metab." volume="87" first="4198" last="4202">
      <title>A polymorphism in the human agouti-related protein is associated with late-onset obesity.</title>
      <authorList>
        <person name="Argyropoulos G."/>
        <person name="Rankinen T."/>
        <person name="Neufeld D.R."/>
        <person name="Rice T."/>
        <person name="Province M.A."/>
        <person name="Leon A.S."/>
        <person name="Skinner J.S."/>
        <person name="Wilmore J.H."/>
        <person name="Rao D.C."/>
        <person name="Bouchard C."/>
      </authorList>
      <dbReference type="PubMed" id="12213871"/>
      <dbReference type="DOI" id="10.1210/jc.2002-011834"/>
    </citation>
    <scope>VARIANT THR-67</scope>
    <scope>POSSIBLE ASSOCIATION WITH OBESITY</scope>
  </reference>
  <reference key="17">
    <citation type="journal article" date="2005" name="Biochem. Pharmacol." volume="70" first="308" last="316">
      <title>Functional analysis of the Ala67Thr polymorphism in agouti related protein associated with anorexia nervosa and leanness.</title>
      <authorList>
        <person name="de Rijke C.E."/>
        <person name="Jackson P.J."/>
        <person name="Garner K.M."/>
        <person name="van Rozen R.J."/>
        <person name="Douglas N.R."/>
        <person name="Kas M.J."/>
        <person name="Millhauser G.L."/>
        <person name="Adan R.A."/>
      </authorList>
      <dbReference type="PubMed" id="15927146"/>
      <dbReference type="DOI" id="10.1016/j.bcp.2005.04.033"/>
    </citation>
    <scope>CHARACTERIZATION OF VARIANT THR-67</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 4 5 9 11 14">Plays a role in weight homeostasis. Involved in the control of feeding behavior through the central melanocortin system. Acts as alpha melanocyte-stimulating hormone antagonist by inhibiting cAMP production mediated by stimulation of melanocortin receptors within the hypothalamus and adrenal gland. Has very low activity with MC5R (By similarity). Is an inverse agonist for MC3R and MC4R being able to suppress their constitutive activity. It promotes MC3R and MC4R endocytosis in an arrestin-dependent manner.</text>
  </comment>
  <comment type="subunit">
    <text evidence="14">Interacts with melanocortin receptors MC3R, MC4R and MC5R.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="9">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="9">Golgi apparatus lumen</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed primarily in the adrenal gland, subthalamic nucleus, and hypothalamus, with a lower level of expression occurring in testis, lung, and kidney.</text>
  </comment>
  <comment type="domain">
    <text evidence="7">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="disease">
    <disease id="DI-01221">
      <name>Obesity</name>
      <acronym>OBESITY</acronym>
      <description>A condition characterized by an increase of body weight beyond the limitation of skeletal and physical requirements, as the result of excessive accumulation of body fat.</description>
      <dbReference type="MIM" id="601665"/>
    </disease>
    <text>Disease susceptibility is associated with variants affecting the gene represented in this entry.</text>
  </comment>
  <dbReference type="EMBL" id="U88063">
    <property type="protein sequence ID" value="AAB52240.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U89485">
    <property type="protein sequence ID" value="AAB68621.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF314194">
    <property type="protein sequence ID" value="AAL09457.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF281309">
    <property type="protein sequence ID" value="AAK96256.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ374394">
    <property type="protein sequence ID" value="ABC88473.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC110443">
    <property type="protein sequence ID" value="AAI10444.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS10839.1"/>
  <dbReference type="RefSeq" id="NP_001129.1">
    <property type="nucleotide sequence ID" value="NM_001138.1"/>
  </dbReference>
  <dbReference type="PDB" id="1HYK">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=87-132"/>
  </dbReference>
  <dbReference type="PDB" id="1MR0">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=87-120"/>
  </dbReference>
  <dbReference type="PDBsum" id="1HYK"/>
  <dbReference type="PDBsum" id="1MR0"/>
  <dbReference type="AlphaFoldDB" id="O00253"/>
  <dbReference type="SMR" id="O00253"/>
  <dbReference type="BioGRID" id="106688">
    <property type="interactions" value="12"/>
  </dbReference>
  <dbReference type="IntAct" id="O00253">
    <property type="interactions" value="9"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000290953"/>
  <dbReference type="BioMuta" id="AGRP"/>
  <dbReference type="MassIVE" id="O00253"/>
  <dbReference type="PaxDb" id="9606-ENSP00000290953"/>
  <dbReference type="PeptideAtlas" id="O00253"/>
  <dbReference type="ProteomicsDB" id="47806"/>
  <dbReference type="Antibodypedia" id="29554">
    <property type="antibodies" value="307 antibodies from 31 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="181"/>
  <dbReference type="Ensembl" id="ENST00000290953.3">
    <property type="protein sequence ID" value="ENSP00000290953.3"/>
    <property type="gene ID" value="ENSG00000159723.5"/>
  </dbReference>
  <dbReference type="GeneID" id="181"/>
  <dbReference type="KEGG" id="hsa:181"/>
  <dbReference type="MANE-Select" id="ENST00000290953.3">
    <property type="protein sequence ID" value="ENSP00000290953.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_001138.2"/>
    <property type="RefSeq protein sequence ID" value="NP_001129.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc002etg.1">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:330"/>
  <dbReference type="CTD" id="181"/>
  <dbReference type="DisGeNET" id="181"/>
  <dbReference type="GeneCards" id="AGRP"/>
  <dbReference type="HGNC" id="HGNC:330">
    <property type="gene designation" value="AGRP"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000159723">
    <property type="expression patterns" value="Group enriched (adrenal gland, brain, epididymis)"/>
  </dbReference>
  <dbReference type="MalaCards" id="AGRP"/>
  <dbReference type="MIM" id="601665">
    <property type="type" value="phenotype"/>
  </dbReference>
  <dbReference type="MIM" id="602311">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_O00253"/>
  <dbReference type="OpenTargets" id="ENSG00000159723"/>
  <dbReference type="PharmGKB" id="PA24627"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000159723"/>
  <dbReference type="eggNOG" id="ENOG502S7K0">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000154258"/>
  <dbReference type="HOGENOM" id="CLU_103790_0_0_1"/>
  <dbReference type="InParanoid" id="O00253"/>
  <dbReference type="OMA" id="SWAMLQG"/>
  <dbReference type="OrthoDB" id="4017051at2759"/>
  <dbReference type="PhylomeDB" id="O00253"/>
  <dbReference type="TreeFam" id="TF330729"/>
  <dbReference type="PathwayCommons" id="O00253"/>
  <dbReference type="Reactome" id="R-HSA-9615017">
    <property type="pathway name" value="FOXO-mediated transcription of oxidative stress, metabolic and neuronal genes"/>
  </dbReference>
  <dbReference type="SignaLink" id="O00253"/>
  <dbReference type="SIGNOR" id="O00253"/>
  <dbReference type="BioGRID-ORCS" id="181">
    <property type="hits" value="14 hits in 1140 CRISPR screens"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="O00253"/>
  <dbReference type="GeneWiki" id="Agouti-related_peptide"/>
  <dbReference type="GenomeRNAi" id="181"/>
  <dbReference type="Pharos" id="O00253">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:O00253"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 16"/>
  </dbReference>
  <dbReference type="RNAct" id="O00253">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000159723">
    <property type="expression patterns" value="Expressed in left adrenal gland and 116 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="O00253">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005796">
    <property type="term" value="C:Golgi lumen"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043025">
    <property type="term" value="C:neuronal cell body"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005102">
    <property type="term" value="F:signaling receptor binding"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070996">
    <property type="term" value="F:type 1 melanocortin receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008343">
    <property type="term" value="P:adult feeding behavior"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007623">
    <property type="term" value="P:circadian rhythm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042755">
    <property type="term" value="P:eating behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009755">
    <property type="term" value="P:hormone-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048571">
    <property type="term" value="P:long-day photoperiodism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060135">
    <property type="term" value="P:maternal process involved in female pregnancy"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000253">
    <property type="term" value="P:positive regulation of feeding behavior"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060259">
    <property type="term" value="P:regulation of feeding behavior"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032868">
    <property type="term" value="P:response to insulin"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.760.10:FF:000001">
    <property type="entry name" value="Agouti-related protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.760.10">
    <property type="entry name" value="Agouti domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007733">
    <property type="entry name" value="Agouti"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027300">
    <property type="entry name" value="Agouti_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036836">
    <property type="entry name" value="Agouti_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16551">
    <property type="entry name" value="AGOUTI RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16551:SF4">
    <property type="entry name" value="AGOUTI-RELATED PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05039">
    <property type="entry name" value="Agouti"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00792">
    <property type="entry name" value="Agouti"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57055">
    <property type="entry name" value="Agouti-related protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60024">
    <property type="entry name" value="AGOUTI_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51150">
    <property type="entry name" value="AGOUTI_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0225">Disease variant</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0333">Golgi apparatus</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0550">Obesity</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000434044" evidence="10 12">
    <location>
      <begin position="21"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000001034" description="Agouti-related protein">
    <location>
      <begin position="83"/>
      <end position="132"/>
    </location>
  </feature>
  <feature type="domain" description="Agouti" evidence="3">
    <location>
      <begin position="87"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="region of interest" description="Interaction with melanocortin receptors">
    <location>
      <begin position="111"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="site" description="Cleavage; by PCSK1" evidence="10">
    <location>
      <begin position="82"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 7 13">
    <location>
      <begin position="87"/>
      <end position="102"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 7 13">
    <location>
      <begin position="94"/>
      <end position="108"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 7 13">
    <location>
      <begin position="101"/>
      <end position="119"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 7 13">
    <location>
      <begin position="105"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 7 13">
    <location>
      <begin position="110"/>
      <end position="117"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_015385" description="May play a role in obesity in an age-dependent manner; apparently no effect on activity; dbSNP:rs5030980." evidence="6 8 9">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="67"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Cleavage is blocked." evidence="10">
    <original>REPR</original>
    <variation>AEPA</variation>
    <location>
      <begin position="79"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on cleavage." evidence="10">
    <original>RR</original>
    <variation>AA</variation>
    <location>
      <begin position="85"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on cleavage." evidence="10">
    <original>RCVR</original>
    <variation>ACVA</variation>
    <location>
      <begin position="86"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Abolishes inhibition of cAMP production in response to melanocortin receptor stimulation." evidence="4">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="111"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AAB68621." evidence="15" ref="2">
    <original>V</original>
    <variation>L</variation>
    <location>
      <position position="6"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="95"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="101"/>
      <end position="103"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="107"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="117"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="125"/>
      <end position="127"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00494"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="10371151"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="11145747"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="11602360"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="11747427"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="12213871"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="15927146"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="16384863"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="11">
    <source>
      <dbReference type="PubMed" id="17041250"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="12">
    <source>
      <dbReference type="PubMed" id="17185225"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="13">
    <source>
      <dbReference type="PubMed" id="9724530"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="14">
    <source>
      <dbReference type="PubMed" id="9892020"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="15"/>
  <evidence type="ECO:0007829" key="16">
    <source>
      <dbReference type="PDB" id="1HYK"/>
    </source>
  </evidence>
  <sequence length="132" mass="14440" checksum="1CCBE112C3EB10F5" modified="1997-07-01" version="1" precursor="true">MLTAAVLSCALLLALPATRGAQMGLAPMEGIRRPDQALLPELPGLGLRAPLKKTTAEQAEEDLLQEAQALAEVLDLQDREPRSSRRCVRLHESCLGQQVPCCDPCATCYCRFFNAFCYCRKLGTAMNPCSRT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>