<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-06-07" modified="2024-05-29" version="17" xmlns="http://uniprot.org/uniprot">
  <accession>A0A1L1QJU3</accession>
  <accession>A0A1X8XKZ9</accession>
  <name>TP1A_HADIN</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Pi-hexatoxin-Hi1a</fullName>
      <shortName evidence="5">Pi-HXTX-Hi1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6">Double-knot toxin</fullName>
      <shortName evidence="6">DkTx</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">SF1 peptide</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Hadronyche infensa</name>
    <name type="common">Fraser island funnel-web spider</name>
    <name type="synonym">Atrax infensus</name>
    <dbReference type="NCBI Taxonomy" id="153481"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Mygalomorphae</taxon>
      <taxon>Hexathelidae</taxon>
      <taxon>Hadronyche</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2017" name="Proc. Natl. Acad. Sci. U.S.A." volume="114" first="3750" last="3755">
      <title>Potent neuroprotection after stroke afforded by a double-knot spider-venom peptide that inhibits acid-sensing ion channel 1a.</title>
      <authorList>
        <person name="Chassagnon I.R."/>
        <person name="McCarthy C.A."/>
        <person name="Chin Y.K."/>
        <person name="Pineda S.S."/>
        <person name="Keramidas A."/>
        <person name="Mobli M."/>
        <person name="Pham V."/>
        <person name="De Silva T.M."/>
        <person name="Lynch J.W."/>
        <person name="Widdop R.E."/>
        <person name="Rash L.D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="28320941"/>
      <dbReference type="DOI" id="10.1073/pnas.1614728114"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>STRUCTURE BY NMR</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>MUTAGENESIS OF 1-ASN--VAL-34; ASN-1; PRO-35 AND 36-ILE--THR-75</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2019" name="Vet. Parasitol." volume="270" first="40" last="46">
      <title>The antitrypanosomal diarylamidines, diminazene and pentamidine, show anthelmintic activity against Haemonchus contortus in vitro.</title>
      <authorList>
        <person name="Nixon S.A."/>
        <person name="Saez N.J."/>
        <person name="Herzig V."/>
        <person name="King G.F."/>
        <person name="Kotze A.C."/>
      </authorList>
      <dbReference type="PubMed" id="31213240"/>
      <dbReference type="DOI" id="10.1016/j.vetpar.2019.05.008"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4">This toxin potently and selectively inhibits ASIC1a (IC(50)=0.4 nM on rASIC1a and IC(50)=0.52 nM on hASIC1a), an isoform of the gene ASIC1 (PubMed:28320941). It incompletely inhibits ASIC1a activation in a pH-independent and slowly reversible manner (Tau(off)=14.2 minutes for rASIC1a and 31.8 minutes for hASIC1a) (PubMed:28320941). This toxin acts by binding to and stabilizing the closed state of the channel, thereby impeding the transition into a conducting state (PubMed:28320941). This toxin may bind to the acidic pocket of ASIC1a, since mutation of a key residue of this pocket (Arg-350) abolishes the ability of the toxin to inhibit ASIC1a (PubMed:28320941). In addition, it shows antiparasitic activities, since it moderately inhibits the larval development of the major pathogenic nematode of ruminants (H.contortus, IC(50)=22.9 uM) (PubMed:31213240). In vivo, this toxin protects the brain from neuronal injury when administered up to 8 hours after stroke onset (PubMed:28320941).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="3">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin. This toxin contains 2 'disulfide through disulfide knots' that are separated by a short linker.</text>
  </comment>
  <comment type="pharmaceutical">
    <text evidence="7">This toxin is a promising lead for the development of therapeutics to protect the brain from ischemic injury.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the psalmotoxin-1 family. Double-knot toxin subfamily.</text>
  </comment>
  <comment type="online information" name="Biological Magnetic Resonance Data Bank">
    <link uri="http://bmrb.wisc.edu/data_library/summary/index.php?bmrbId=25848#"/>
  </comment>
  <dbReference type="PDB" id="2N8F">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-75"/>
  </dbReference>
  <dbReference type="PDBsum" id="2N8F"/>
  <dbReference type="AlphaFoldDB" id="A0A1L1QJU3"/>
  <dbReference type="SMR" id="A0A1L1QJU3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0099106">
    <property type="term" value="F:ion channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.20.10.10">
    <property type="match status" value="2"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57059">
    <property type="entry name" value="omega toxin-like"/>
    <property type="match status" value="2"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0582">Pharmaceutical</keyword>
  <keyword id="KW-1275">Proton-gated sodium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000440131" description="Pi-hexatoxin-Hi1a" evidence="7">
    <location>
      <begin position="1"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3 8">
    <location>
      <begin position="3"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3 8">
    <location>
      <begin position="10"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3 8">
    <location>
      <begin position="17"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3 8">
    <location>
      <begin position="40"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3 8">
    <location>
      <begin position="47"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3 8">
    <location>
      <begin position="54"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Complete loss of inhibition of rASIC1a; mutant Hi1a:C." evidence="3">
    <location>
      <begin position="1"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="2600-fold decrease in inhibition of rASIC1a, and inhibition become fully reversible; mutant Hi1a:N." evidence="3">
    <original>N</original>
    <variation>SN</variation>
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Complete loss of inhibition of rASIC1a; mutant Hi1a:C." evidence="3">
    <original>P</original>
    <variation>S</variation>
    <location>
      <position position="35"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="2600-fold decrease in inhibition of rASIC1a, and inhibition become fully reversible; mutant Hi1a:N." evidence="3">
    <location>
      <begin position="36"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="10"/>
      <end position="12"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="21"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="27"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="33"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="turn" evidence="9">
    <location>
      <begin position="51"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="58"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="65"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="70"/>
      <end position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P0DP47"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P60514"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="28320941"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="31213240"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="28320941"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="28320941"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="PDB" id="2N8F"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="9">
    <source>
      <dbReference type="PDB" id="2N8F"/>
    </source>
  </evidence>
  <sequence length="75" mass="8648" checksum="15256C6BD1B68328" modified="2017-06-07" version="2">NECIRKWLSCVDRKNDCCEGLECYKRRHSFEVCVPIPGFCLVKWKQCDGRERDCCAGLECWKRSGNKSSVCAPIT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>