<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-05-08" modified="2024-11-27" version="12" xmlns="http://uniprot.org/uniprot">
  <accession>A0A348G6I7</accession>
  <name>TX11A_ODOMO</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">U-poneritoxin(01)-Om1a</fullName>
      <shortName evidence="1">U-PONTX(01)-Om1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="9">Pilosulin-like peptide 1</fullName>
      <shortName evidence="5">PLP1</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="6">Poneratoxin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Odontomachus monticola</name>
    <name type="common">Trap-jaw ant</name>
    <dbReference type="NCBI Taxonomy" id="613454"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Ponerinae</taxon>
      <taxon>Ponerini</taxon>
      <taxon>Odontomachus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2017" name="Toxins" volume="9" first="323" last="323">
      <title>Combined venom gland transcriptomic and venom peptidomic analysis of the predatory ant Odontomachus monticola.</title>
      <authorList>
        <person name="Kazuma K."/>
        <person name="Masuko K."/>
        <person name="Konno K."/>
        <person name="Inagaki H."/>
      </authorList>
      <dbReference type="PubMed" id="29027956"/>
      <dbReference type="DOI" id="10.3390/toxins9100323"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 46-62</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT MET-62</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2019" name="Toxins" volume="11">
      <title>Mass spectrometry analysis and biological characterization of the predatory ant Odontomachus monticola venom and venom sac components.</title>
      <authorList>
        <person name="Tani N."/>
        <person name="Kazuma K."/>
        <person name="Ohtsuka Y."/>
        <person name="Shigeri Y."/>
        <person name="Masuko K."/>
        <person name="Konno K."/>
        <person name="Inagaki H."/>
      </authorList>
      <dbReference type="PubMed" id="30658410"/>
      <dbReference type="DOI" id="10.3390/toxins11010050"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>SYNTHESIS OF PEPTIDE WITH UNKNOWN TERMINAL RESIDUES</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="4">Antimicrobial peptide with activities against E.coli (MIC=1.3 uM), S.aureus (MIC=3.1 uM), and S.cerevisiae (MIC=50 uM). Also shows histamine-releasing activity (32.9% at 10 uM). Does not show hemolytic activity, even at 50 uM. It is a short peptide for which no alpha-helical region has been predicted.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="7 8">Truncated sequences of this peptide have also been found in the venom. It is possible they have been cleaved in the venom.</text>
  </comment>
  <comment type="mass spectrometry" mass="2061.09" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="6">Belongs to the formicidae venom precursor-01 superfamily.</text>
  </comment>
  <dbReference type="EMBL" id="LC316119">
    <property type="protein sequence ID" value="BBF98060.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A348G6I7"/>
  <dbReference type="SMR" id="A0A348G6I7"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000447066" evidence="7">
    <location>
      <begin position="28"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5017013278" description="U-poneritoxin(01)-Om1a" evidence="3 4">
    <location>
      <begin position="46"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="3">
    <location>
      <position position="62"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A348G5W2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="29027956"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="30658410"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="29027956"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="29027956"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="30658410"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="9">
    <source>
      <dbReference type="EMBL" id="BBF98060.1"/>
    </source>
  </evidence>
  <sequence length="64" mass="6941" checksum="1360EC3ECFCCA8DA" modified="2018-11-07" version="1" precursor="true">MKPSGLTFAFLVVFMMAIMYNSVQVTADADADAEAEALANALAEAGILDWGKKVMDWIKDKMGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>