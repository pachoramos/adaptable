<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-02-26" modified="2024-11-27" version="76" xmlns="http://uniprot.org/uniprot">
  <accession>A6QIG7</accession>
  <accession>Q7WUJ0</accession>
  <name>CHIPS_STAAE</name>
  <protein>
    <recommendedName>
      <fullName>Chemotaxis inhibitory protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>CHIPS</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">chp</name>
    <name type="ordered locus">NWMN_1877</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain Newman)</name>
    <dbReference type="NCBI Taxonomy" id="426430"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="J. Exp. Med." volume="199" first="687" last="695">
      <title>Chemotaxis inhibitory protein of Staphylococcus aureus, a bacterial antiinflammatory agent.</title>
      <authorList>
        <person name="de Haas C.J.C."/>
        <person name="Veldkamp K.E."/>
        <person name="Peschel A."/>
        <person name="Weerkamp F."/>
        <person name="van Wamel W.J.B."/>
        <person name="Heezius E.C.J.M."/>
        <person name="Poppelier M.J.J.G."/>
        <person name="van Kessel K.P.M."/>
        <person name="van Strijp J.A.G."/>
      </authorList>
      <dbReference type="PubMed" id="14993252"/>
      <dbReference type="DOI" id="10.1084/jem.20031636"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>PROTEIN SEQUENCE OF 29-60</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>PROPHAGE-ENCODED PROTEIN</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="J. Bacteriol." volume="190" first="300" last="310">
      <title>Genome sequence of Staphylococcus aureus strain Newman and comparative analysis of staphylococcal genomes: polymorphism and evolution of two major pathogenicity islands.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Bae T."/>
        <person name="Schneewind O."/>
        <person name="Takeuchi F."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="17951380"/>
      <dbReference type="DOI" id="10.1128/jb.01000-07"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Newman</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="J. Immunol." volume="172" first="6994" last="7001">
      <title>Chemotaxis inhibitory protein of Staphylococcus aureus binds specifically to the C5a and formylated peptide receptor.</title>
      <authorList>
        <person name="Postma B."/>
        <person name="Poppelier M.J.J.G."/>
        <person name="van Galen J.C."/>
        <person name="Prossnitz E.R."/>
        <person name="van Strijp J.A.G."/>
        <person name="de Haas C.J.C."/>
        <person name="van Kessel K.P.M."/>
      </authorList>
      <dbReference type="PubMed" id="15153520"/>
      <dbReference type="DOI" id="10.4049/jimmunol.172.11.6994"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH HUMAN C5AR AND FPR</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2004" name="J. Immunol." volume="173" first="5704" last="5711">
      <title>N-terminal residues of the chemotaxis inhibitory protein of Staphylococcus aureus are essential for blocking formylated peptide receptor but not C5a receptor.</title>
      <authorList>
        <person name="Haas P.-J."/>
        <person name="de Haas C.J.C."/>
        <person name="Kleibeuker W."/>
        <person name="Poppelier M.J.J.G."/>
        <person name="van Kessel K.P.M."/>
        <person name="Kruijtzer J.A.W."/>
        <person name="Liskamp R.M.J."/>
        <person name="van Strijp J.A.G."/>
      </authorList>
      <dbReference type="PubMed" id="15494522"/>
      <dbReference type="DOI" id="10.4049/jimmunol.173.9.5704"/>
    </citation>
    <scope>INTERACTION WITH HUMAN FPR</scope>
    <scope>MUTAGENESIS OF PHE-29 AND PHE-31</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2005" name="J. Biol. Chem." volume="280" first="2020" last="2027">
      <title>Residues 10-18 within the C5a receptor N terminus compose a binding domain for chemotaxis inhibitory protein of Staphylococcus aureus.</title>
      <authorList>
        <person name="Postma B."/>
        <person name="Kleibeuker W."/>
        <person name="Poppelier M.J.J.G."/>
        <person name="Boonstra M."/>
        <person name="van Kessel K.P.M."/>
        <person name="van Strijp J.A.G."/>
        <person name="de Haas C.J.C."/>
      </authorList>
      <dbReference type="PubMed" id="15542591"/>
      <dbReference type="DOI" id="10.1074/jbc.m412230200"/>
    </citation>
    <scope>INTERACTION WITH HUMAN C5AR</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2006" name="J. Bacteriol." volume="188" first="1310" last="1315">
      <title>The innate immune modulators staphylococcal complement inhibitor and chemotaxis inhibitory protein of Staphylococcus aureus are located on beta-hemolysin-converting bacteriophages.</title>
      <authorList>
        <person name="van Wamel W.J.B."/>
        <person name="Rooijakkers S.H.M."/>
        <person name="Ruyken M."/>
        <person name="van Kessel K.P.M."/>
        <person name="van Strijp J.A.G."/>
      </authorList>
      <dbReference type="PubMed" id="16452413"/>
      <dbReference type="DOI" id="10.1128/jb.188.4.1310-1315.2006"/>
    </citation>
    <scope>PROPHAGE-ENCODED PROTEIN</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2006" name="Mol. Microbiol." volume="62" first="1035" last="1047">
      <title>Prophages of Staphylococcus aureus Newman and their contribution to virulence.</title>
      <authorList>
        <person name="Bae T."/>
        <person name="Baba T."/>
        <person name="Hiramatsu K."/>
        <person name="Schneewind O."/>
      </authorList>
      <dbReference type="PubMed" id="17078814"/>
      <dbReference type="DOI" id="10.1111/j.1365-2958.2006.05441.x"/>
    </citation>
    <scope>PROPHAGE-ENCODED PROTEIN</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2005" name="J. Mol. Biol." volume="353" first="859" last="872">
      <title>The structure of the C5a receptor-blocking domain of chemotaxis inhibitory protein of Staphylococcus aureus is related to a group of immune evasive molecules.</title>
      <authorList>
        <person name="Haas P.-J."/>
        <person name="de Haas C.J.C."/>
        <person name="Poppelier M.J.J.G."/>
        <person name="van Kessel K.P.M."/>
        <person name="van Strijp J.A.G."/>
        <person name="Dijkstra K."/>
        <person name="Scheek R.M."/>
        <person name="Fan H."/>
        <person name="Kruijtzer J.A.W."/>
        <person name="Liskamp R.M.J."/>
        <person name="Kemmink J."/>
      </authorList>
      <dbReference type="PubMed" id="16213522"/>
      <dbReference type="DOI" id="10.1016/j.jmb.2005.09.014"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 59-149</scope>
    <scope>MUTAGENESIS OF LYS-68; ARG-72; ARG-74; LYS-78; LYS-79; LYS-82; LYS-89; LYS-97; ARG-112; LYS-113; LYS-120; LYS-123; LYS-128; LYS-129; LYS-133 AND LYS-143</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2">Involved in countering the first line of host defense mechanisms. Specifically inhibits the response of human neutrophils and monocytes to complement anaphylatoxin C5a and formylated peptides, like N-formyl-methionyl-leucyl-phenylalanine (fMLP). Acts by binding directly to the C5a receptor (C5aR) and formylated peptide receptor (FPR), thereby blocking the C5a- and fMLP-induced calcium responses. Prevents phagocytosis of the bacterium.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text>Two distinct active sites are responsible for anti-C5aR and anti-FPR activity. They might be closely spatially related and might be overlapping.</text>
  </comment>
  <comment type="miscellaneous">
    <text>Encoded within a prophage region.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the CHIPS/FLIPr family.</text>
  </comment>
  <dbReference type="EMBL" id="AF285146">
    <property type="protein sequence ID" value="AAQ14339.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AP009351">
    <property type="protein sequence ID" value="BAF68149.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000727649.1">
    <property type="nucleotide sequence ID" value="NZ_JBBIAE010000010.1"/>
  </dbReference>
  <dbReference type="PDB" id="1XEE">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=59-149"/>
  </dbReference>
  <dbReference type="PDB" id="2K3U">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=59-149"/>
  </dbReference>
  <dbReference type="PDBsum" id="1XEE"/>
  <dbReference type="PDBsum" id="2K3U"/>
  <dbReference type="AlphaFoldDB" id="A6QIG7"/>
  <dbReference type="BMRB" id="A6QIG7"/>
  <dbReference type="SMR" id="A6QIG7"/>
  <dbReference type="KEGG" id="sae:NWMN_1877"/>
  <dbReference type="HOGENOM" id="CLU_1748521_0_0_9"/>
  <dbReference type="EvolutionaryTrace" id="A6QIG7"/>
  <dbReference type="PRO" id="PR:A6QIG7"/>
  <dbReference type="Proteomes" id="UP000006386">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.20.390">
    <property type="entry name" value="Chemotaxis-inhibiting protein CHIPS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020986">
    <property type="entry name" value="CHIPS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038529">
    <property type="entry name" value="FLIPR/CHIP_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023253">
    <property type="entry name" value="FLIPR/CHIPS"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11434">
    <property type="entry name" value="CHIPS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR02036">
    <property type="entry name" value="CHEMOTAXISIP"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR02035">
    <property type="entry name" value="FLIPRCHIPS"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000319608" description="Chemotaxis inhibitory protein">
    <location>
      <begin position="29"/>
      <end position="149"/>
    </location>
  </feature>
  <feature type="region of interest" description="FPR-blocking activity">
    <location>
      <begin position="29"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="region of interest" description="C5aR-blocking activity">
    <location>
      <begin position="59"/>
      <end position="149"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Abolishes FPR-blocking activity. No effect on C5aR-blocking activity." evidence="3">
    <original>F</original>
    <variation>A</variation>
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Abolishes FPR-blocking activity. No effect on C5aR-blocking activity." evidence="3">
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreases FPR-blocking activity. No effect on C5aR-blocking activity." evidence="3">
    <original>F</original>
    <variation>A</variation>
    <location>
      <position position="31"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="68"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreases C5aR-blocking activity. No effect on FPR-blocking activity." evidence="4">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="72"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreases both C5aR- and FPR-blocking activity." evidence="4">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="74"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="78"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Slightly decreases C5aR-blocking activity. No effect on FPR-blocking activity." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="79"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="82"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="89"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="97"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreases both C5aR- and FPR-blocking activity." evidence="4">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="112"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="113"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="120"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreases C5aR-blocking activity. No effect on FPR-blocking activity." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="123"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="128"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="129"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Slightly decreases C5aR-blocking activity. No effect on FPR-blocking activity." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="133"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="143"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="66"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="84"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="87"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="99"/>
      <end position="106"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="110"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="121"/>
      <end position="128"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="136"/>
      <end position="139"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="14993252"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15153520"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15494522"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="16213522"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="1XEE"/>
    </source>
  </evidence>
  <sequence length="149" mass="17058" checksum="6F5CC73C5BDB36AC" modified="2007-08-21" version="1" precursor="true">MKKKLATTVLALSFLTAGISTHHHSAKAFTFEPFPTNEEIESNKKMLEKEKAYKESFKNSGLPTTLGKLDERLRNYLKKGTKNSAQFEKMVILTENKGYYTVYLNTPLAEDRKNVELLGKMYKTYFFKKGESKSSYVINGPGKTNEYAY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>