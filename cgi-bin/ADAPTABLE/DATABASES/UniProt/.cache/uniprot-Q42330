<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2024-10-02" version="136" xmlns="http://uniprot.org/uniprot">
  <accession>Q42330</accession>
  <accession>Q9SX84</accession>
  <name>DF192_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 192</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Trypsin inhibitor ATTI-7</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ATTI7</name>
    <name type="ordered locus">At1g47540</name>
    <name type="ORF">F16N3.19</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Nature" volume="408" first="816" last="820">
      <title>Sequence and analysis of chromosome 1 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
        <person name="Palm C.J."/>
        <person name="Federspiel N.A."/>
        <person name="Kaul S."/>
        <person name="White O."/>
        <person name="Alonso J."/>
        <person name="Altafi H."/>
        <person name="Araujo R."/>
        <person name="Bowman C.L."/>
        <person name="Brooks S.Y."/>
        <person name="Buehler E."/>
        <person name="Chan A."/>
        <person name="Chao Q."/>
        <person name="Chen H."/>
        <person name="Cheuk R.F."/>
        <person name="Chin C.W."/>
        <person name="Chung M.K."/>
        <person name="Conn L."/>
        <person name="Conway A.B."/>
        <person name="Conway A.R."/>
        <person name="Creasy T.H."/>
        <person name="Dewar K."/>
        <person name="Dunn P."/>
        <person name="Etgu P."/>
        <person name="Feldblyum T.V."/>
        <person name="Feng J.-D."/>
        <person name="Fong B."/>
        <person name="Fujii C.Y."/>
        <person name="Gill J.E."/>
        <person name="Goldsmith A.D."/>
        <person name="Haas B."/>
        <person name="Hansen N.F."/>
        <person name="Hughes B."/>
        <person name="Huizar L."/>
        <person name="Hunter J.L."/>
        <person name="Jenkins J."/>
        <person name="Johnson-Hopson C."/>
        <person name="Khan S."/>
        <person name="Khaykin E."/>
        <person name="Kim C.J."/>
        <person name="Koo H.L."/>
        <person name="Kremenetskaia I."/>
        <person name="Kurtz D.B."/>
        <person name="Kwan A."/>
        <person name="Lam B."/>
        <person name="Langin-Hooper S."/>
        <person name="Lee A."/>
        <person name="Lee J.M."/>
        <person name="Lenz C.A."/>
        <person name="Li J.H."/>
        <person name="Li Y.-P."/>
        <person name="Lin X."/>
        <person name="Liu S.X."/>
        <person name="Liu Z.A."/>
        <person name="Luros J.S."/>
        <person name="Maiti R."/>
        <person name="Marziali A."/>
        <person name="Militscher J."/>
        <person name="Miranda M."/>
        <person name="Nguyen M."/>
        <person name="Nierman W.C."/>
        <person name="Osborne B.I."/>
        <person name="Pai G."/>
        <person name="Peterson J."/>
        <person name="Pham P.K."/>
        <person name="Rizzo M."/>
        <person name="Rooney T."/>
        <person name="Rowley D."/>
        <person name="Sakano H."/>
        <person name="Salzberg S.L."/>
        <person name="Schwartz J.R."/>
        <person name="Shinn P."/>
        <person name="Southwick A.M."/>
        <person name="Sun H."/>
        <person name="Tallon L.J."/>
        <person name="Tambunga G."/>
        <person name="Toriumi M.J."/>
        <person name="Town C.D."/>
        <person name="Utterback T."/>
        <person name="Van Aken S."/>
        <person name="Vaysberg M."/>
        <person name="Vysotskaia V.S."/>
        <person name="Walker M."/>
        <person name="Wu D."/>
        <person name="Yu G."/>
        <person name="Fraser C.M."/>
        <person name="Venter J.C."/>
        <person name="Davis R.W."/>
      </authorList>
      <dbReference type="PubMed" id="11130712"/>
      <dbReference type="DOI" id="10.1038/35048500"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2003" name="Science" volume="302" first="842" last="846">
      <title>Empirical analysis of transcriptional activity in the Arabidopsis genome.</title>
      <authorList>
        <person name="Yamada K."/>
        <person name="Lim J."/>
        <person name="Dale J.M."/>
        <person name="Chen H."/>
        <person name="Shinn P."/>
        <person name="Palm C.J."/>
        <person name="Southwick A.M."/>
        <person name="Wu H.C."/>
        <person name="Kim C.J."/>
        <person name="Nguyen M."/>
        <person name="Pham P.K."/>
        <person name="Cheuk R.F."/>
        <person name="Karlin-Newmann G."/>
        <person name="Liu S.X."/>
        <person name="Lam B."/>
        <person name="Sakano H."/>
        <person name="Wu T."/>
        <person name="Yu G."/>
        <person name="Miranda M."/>
        <person name="Quach H.L."/>
        <person name="Tripp M."/>
        <person name="Chang C.H."/>
        <person name="Lee J.M."/>
        <person name="Toriumi M.J."/>
        <person name="Chan M.M."/>
        <person name="Tang C.C."/>
        <person name="Onodera C.S."/>
        <person name="Deng J.M."/>
        <person name="Akiyama K."/>
        <person name="Ansari Y."/>
        <person name="Arakawa T."/>
        <person name="Banh J."/>
        <person name="Banno F."/>
        <person name="Bowser L."/>
        <person name="Brooks S.Y."/>
        <person name="Carninci P."/>
        <person name="Chao Q."/>
        <person name="Choy N."/>
        <person name="Enju A."/>
        <person name="Goldsmith A.D."/>
        <person name="Gurjal M."/>
        <person name="Hansen N.F."/>
        <person name="Hayashizaki Y."/>
        <person name="Johnson-Hopson C."/>
        <person name="Hsuan V.W."/>
        <person name="Iida K."/>
        <person name="Karnes M."/>
        <person name="Khan S."/>
        <person name="Koesema E."/>
        <person name="Ishida J."/>
        <person name="Jiang P.X."/>
        <person name="Jones T."/>
        <person name="Kawai J."/>
        <person name="Kamiya A."/>
        <person name="Meyers C."/>
        <person name="Nakajima M."/>
        <person name="Narusaka M."/>
        <person name="Seki M."/>
        <person name="Sakurai T."/>
        <person name="Satou M."/>
        <person name="Tamse R."/>
        <person name="Vaysberg M."/>
        <person name="Wallender E.K."/>
        <person name="Wong C."/>
        <person name="Yamamura Y."/>
        <person name="Yuan S."/>
        <person name="Shinozaki K."/>
        <person name="Davis R.W."/>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
      </authorList>
      <dbReference type="PubMed" id="14593172"/>
      <dbReference type="DOI" id="10.1126/science.1088305"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1996" name="Plant J." volume="9" first="101" last="124">
      <title>Further progress towards a catalogue of all Arabidopsis genes: analysis of a set of 5000 non-redundant ESTs.</title>
      <authorList>
        <person name="Cooke R."/>
        <person name="Raynal M."/>
        <person name="Laudie M."/>
        <person name="Grellet F."/>
        <person name="Delseny M."/>
        <person name="Morris P.-C."/>
        <person name="Guerrier D."/>
        <person name="Giraudat J."/>
        <person name="Quigley F."/>
        <person name="Clabault G."/>
        <person name="Li Y.-F."/>
        <person name="Mache R."/>
        <person name="Krivitzky M."/>
        <person name="Gy I.J.-J."/>
        <person name="Kreis M."/>
        <person name="Lecharny A."/>
        <person name="Parmentier Y."/>
        <person name="Marbach J."/>
        <person name="Fleck J."/>
        <person name="Clement B."/>
        <person name="Philipps G."/>
        <person name="Herve C."/>
        <person name="Bardet C."/>
        <person name="Tremousaygue D."/>
        <person name="Lescure B."/>
        <person name="Lacomme C."/>
        <person name="Roby D."/>
        <person name="Jourjon M.-F."/>
        <person name="Chabrier P."/>
        <person name="Charpenteau J.-L."/>
        <person name="Desprez T."/>
        <person name="Amselem J."/>
        <person name="Chiapello H."/>
        <person name="Hoefte H."/>
      </authorList>
      <dbReference type="PubMed" id="8580968"/>
      <dbReference type="DOI" id="10.1046/j.1365-313x.1996.09010101.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] OF 2-98</scope>
    <source>
      <strain>cv. Columbia</strain>
      <tissue>Dry seed</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Genetics" volume="166" first="1419" last="1436">
      <title>Functional divergence in tandemly duplicated Arabidopsis thaliana trypsin inhibitor genes.</title>
      <authorList>
        <person name="Clauss M.J."/>
        <person name="Mitchell-Olds T."/>
      </authorList>
      <dbReference type="PubMed" id="15082560"/>
      <dbReference type="DOI" id="10.1534/genetics.166.3.1419"/>
    </citation>
    <scope>GENE FAMILY</scope>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>Q42330-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <text>A number of isoforms are produced. According to EST sequences.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family. Protease inhibitor I18 (RTI/MTI-2) subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="4">Was initially thought to be a protease inhibitor.</text>
  </comment>
  <dbReference type="EMBL" id="AC007519">
    <property type="protein sequence ID" value="AAD46033.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002684">
    <property type="protein sequence ID" value="AEE32182.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY064025">
    <property type="protein sequence ID" value="AAL36381.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY117189">
    <property type="protein sequence ID" value="AAM51264.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Z47386">
    <property type="protein sequence ID" value="CAA87459.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="G96515">
    <property type="entry name" value="G96515"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_175185.1">
    <molecule id="Q42330-1"/>
    <property type="nucleotide sequence ID" value="NM_103647.5"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q42330"/>
  <dbReference type="SMR" id="Q42330"/>
  <dbReference type="BioGRID" id="26388">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="3702.Q42330"/>
  <dbReference type="PaxDb" id="3702-AT1G47540.2"/>
  <dbReference type="ProteomicsDB" id="224183">
    <molecule id="Q42330-1"/>
  </dbReference>
  <dbReference type="EnsemblPlants" id="AT1G47540.1">
    <molecule id="Q42330-1"/>
    <property type="protein sequence ID" value="AT1G47540.1"/>
    <property type="gene ID" value="AT1G47540"/>
  </dbReference>
  <dbReference type="GeneID" id="841163"/>
  <dbReference type="Gramene" id="AT1G47540.1">
    <molecule id="Q42330-1"/>
    <property type="protein sequence ID" value="AT1G47540.1"/>
    <property type="gene ID" value="AT1G47540"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT1G47540"/>
  <dbReference type="Araport" id="AT1G47540"/>
  <dbReference type="TAIR" id="AT1G47540"/>
  <dbReference type="HOGENOM" id="CLU_181760_0_0_1"/>
  <dbReference type="InParanoid" id="Q42330"/>
  <dbReference type="OMA" id="FRRCRED"/>
  <dbReference type="PhylomeDB" id="Q42330"/>
  <dbReference type="PRO" id="PR:Q42330"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q42330">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000031092" description="Defensin-like protein 192">
    <location>
      <begin position="28"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="site" description="Reactive bond" evidence="1">
    <location>
      <begin position="48"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="45"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="54"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="58"/>
      <end position="83"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="15082560"/>
    </source>
  </evidence>
  <sequence length="98" mass="10993" checksum="77118C17EA1DA9C4" modified="2000-12-01" version="2" precursor="true">MATKSVSTFAIFFILVLAIFETPEIEAYDRKCLKEYGGDVGFSYCAPRIFPTFCDQNCRKNKGAKGGVCRWEENNAIGVKCLCNFCSEEPSDQTLSRI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>