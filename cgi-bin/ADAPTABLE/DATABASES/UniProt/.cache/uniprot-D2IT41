<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-10-19" modified="2022-08-03" version="24" xmlns="http://uniprot.org/uniprot">
  <accession>D2IT41</accession>
  <name>PDH_EURPU</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Pigment-dispersing hormone peptides</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="5">PDH precursor-related peptide</fullName>
        <shortName evidence="5">PPRP</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="5">Pigment-dispersing hormone</fullName>
        <shortName evidence="5">PDH</shortName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Eurydice pulchra</name>
    <name type="common">Speckled sea louse</name>
    <dbReference type="NCBI Taxonomy" id="155694"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Peracarida</taxon>
      <taxon>Isopoda</taxon>
      <taxon>Cirolanidae</taxon>
      <taxon>Eurydice</taxon>
    </lineage>
  </organism>
  <reference evidence="6 7" key="1">
    <citation type="journal article" date="2011" name="J. Comp. Neurol." volume="519" first="562" last="575">
      <title>A novel form of pigment-dispersing hormone in the central nervous system of the intertidal marine isopod, Eurydice pulchra (leach).</title>
      <authorList>
        <person name="Wilcockson D.C."/>
        <person name="Zhang L."/>
        <person name="Hastings M.H."/>
        <person name="Kyriacou C.P."/>
        <person name="Webster S.G."/>
      </authorList>
      <dbReference type="PubMed" id="21192084"/>
      <dbReference type="DOI" id="10.1002/cne.22533"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 63-80</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="4">Head</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">The pigment-dispersing hormone causes the migration of the distal retinal pigment into the proximal end of the pigment chromatophore cells and thus decreases the amount of light entering the retinulas. May also function as a neurotransmitter and/or neuromodulator (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Strongly expressed in eyestalk tissue and cerebral ganglia (at protein level).</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="4">Does not display circadian or circatidal patterns of expression.</text>
  </comment>
  <comment type="mass spectrometry" mass="1913.83" method="MALDI" evidence="4">
    <molecule>Pigment-dispersing hormone</molecule>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the arthropod PDH family.</text>
  </comment>
  <dbReference type="EMBL" id="GQ380440">
    <property type="protein sequence ID" value="ACX49752.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="D2IT41"/>
  <dbReference type="SMR" id="D2IT41"/>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045202">
    <property type="term" value="C:synapse"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="GOC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007268">
    <property type="term" value="P:chemical synaptic transmission"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051904">
    <property type="term" value="P:pigment granule transport"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009416">
    <property type="term" value="P:response to light stimulus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009396">
    <property type="entry name" value="Pigment_DH"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06324">
    <property type="entry name" value="Pigment_DH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0529">Neurotransmitter</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5000555737" description="PDH precursor-related peptide" evidence="3 5">
    <location>
      <begin position="25"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5000555738" description="Pigment-dispersing hormone" evidence="4">
    <location>
      <begin position="63"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="modified residue" description="Alanine amide" evidence="1">
    <location>
      <position position="80"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P09929"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q06202"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="21192084"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="21192084"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="EMBL" id="ACX49752.1"/>
    </source>
  </evidence>
  <sequence length="83" mass="9173" checksum="05A94B345BF72742" modified="2010-02-09" version="1" precursor="true">MRFIILGVLFIAVASMILSNGVMAQSRDFSISEREIVASLAKQLLRVARMGYVPEGDLPRKRNAELINSLLGVPRVMSDAGRR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>