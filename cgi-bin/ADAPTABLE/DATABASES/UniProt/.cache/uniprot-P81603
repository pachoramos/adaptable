<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2024-11-27" version="134" xmlns="http://uniprot.org/uniprot">
  <accession>P81603</accession>
  <accession>Q17EE4</accession>
  <accession>Q9Y0F0</accession>
  <accession>Q9Y0F1</accession>
  <name>DEFC_AEDAE</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-C</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">DEFC</name>
    <name type="ORF">AAEL003832</name>
  </gene>
  <organism>
    <name type="scientific">Aedes aegypti</name>
    <name type="common">Yellowfever mosquito</name>
    <name type="synonym">Culex aegypti</name>
    <dbReference type="NCBI Taxonomy" id="7159"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Nematocera</taxon>
      <taxon>Culicoidea</taxon>
      <taxon>Culicidae</taxon>
      <taxon>Culicinae</taxon>
      <taxon>Aedini</taxon>
      <taxon>Aedes</taxon>
      <taxon>Stegomyia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Insect Mol. Biol." volume="8" first="107" last="118">
      <title>Insect immunity: molecular cloning, expression, and characterization of cDNAs and genomic DNA encoding three isoforms of insect defensin in Aedes aegypti.</title>
      <authorList>
        <person name="Lowenberger C.A."/>
        <person name="Smartt C.T."/>
        <person name="Bulet P."/>
        <person name="Ferdig M.T."/>
        <person name="Severson D.W."/>
        <person name="Hoffmann J.A."/>
        <person name="Christensen B.M."/>
      </authorList>
      <dbReference type="PubMed" id="9927179"/>
      <dbReference type="DOI" id="10.1046/j.1365-2583.1999.810107.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>INDUCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <strain>Liverpool</strain>
      <tissue>Fat body</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2007" name="Science" volume="316" first="1718" last="1723">
      <title>Genome sequence of Aedes aegypti, a major arbovirus vector.</title>
      <authorList>
        <person name="Nene V."/>
        <person name="Wortman J.R."/>
        <person name="Lawson D."/>
        <person name="Haas B.J."/>
        <person name="Kodira C.D."/>
        <person name="Tu Z.J."/>
        <person name="Loftus B.J."/>
        <person name="Xi Z."/>
        <person name="Megy K."/>
        <person name="Grabherr M."/>
        <person name="Ren Q."/>
        <person name="Zdobnov E.M."/>
        <person name="Lobo N.F."/>
        <person name="Campbell K.S."/>
        <person name="Brown S.E."/>
        <person name="Bonaldo M.F."/>
        <person name="Zhu J."/>
        <person name="Sinkins S.P."/>
        <person name="Hogenkamp D.G."/>
        <person name="Amedeo P."/>
        <person name="Arensburger P."/>
        <person name="Atkinson P.W."/>
        <person name="Bidwell S.L."/>
        <person name="Biedler J."/>
        <person name="Birney E."/>
        <person name="Bruggner R.V."/>
        <person name="Costas J."/>
        <person name="Coy M.R."/>
        <person name="Crabtree J."/>
        <person name="Crawford M."/>
        <person name="DeBruyn B."/>
        <person name="DeCaprio D."/>
        <person name="Eiglmeier K."/>
        <person name="Eisenstadt E."/>
        <person name="El-Dorry H."/>
        <person name="Gelbart W.M."/>
        <person name="Gomes S.L."/>
        <person name="Hammond M."/>
        <person name="Hannick L.I."/>
        <person name="Hogan J.R."/>
        <person name="Holmes M.H."/>
        <person name="Jaffe D."/>
        <person name="Johnston S.J."/>
        <person name="Kennedy R.C."/>
        <person name="Koo H."/>
        <person name="Kravitz S."/>
        <person name="Kriventseva E.V."/>
        <person name="Kulp D."/>
        <person name="Labutti K."/>
        <person name="Lee E."/>
        <person name="Li S."/>
        <person name="Lovin D.D."/>
        <person name="Mao C."/>
        <person name="Mauceli E."/>
        <person name="Menck C.F."/>
        <person name="Miller J.R."/>
        <person name="Montgomery P."/>
        <person name="Mori A."/>
        <person name="Nascimento A.L."/>
        <person name="Naveira H.F."/>
        <person name="Nusbaum C."/>
        <person name="O'Leary S.B."/>
        <person name="Orvis J."/>
        <person name="Pertea M."/>
        <person name="Quesneville H."/>
        <person name="Reidenbach K.R."/>
        <person name="Rogers Y.-H.C."/>
        <person name="Roth C.W."/>
        <person name="Schneider J.R."/>
        <person name="Schatz M."/>
        <person name="Shumway M."/>
        <person name="Stanke M."/>
        <person name="Stinson E.O."/>
        <person name="Tubio J.M.C."/>
        <person name="Vanzee J.P."/>
        <person name="Verjovski-Almeida S."/>
        <person name="Werner D."/>
        <person name="White O.R."/>
        <person name="Wyder S."/>
        <person name="Zeng Q."/>
        <person name="Zhao Q."/>
        <person name="Zhao Y."/>
        <person name="Hill C.A."/>
        <person name="Raikhel A.S."/>
        <person name="Soares M.B."/>
        <person name="Knudson D.L."/>
        <person name="Lee N.H."/>
        <person name="Galagan J."/>
        <person name="Salzberg S.L."/>
        <person name="Paulsen I.T."/>
        <person name="Dimopoulos G."/>
        <person name="Collins F.H."/>
        <person name="Bruce B."/>
        <person name="Fraser-Liggett C.M."/>
        <person name="Severson D.W."/>
      </authorList>
      <dbReference type="PubMed" id="17510324"/>
      <dbReference type="DOI" id="10.1126/science.1138878"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>LVPib12</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1995" name="Insect Biochem. Mol. Biol." volume="25" first="867" last="873">
      <title>Insect immunity: isolation of three novel inducible antibacterial defensins from the vector mosquito, Aedes aegypti.</title>
      <authorList>
        <person name="Lowenberger C."/>
        <person name="Bulet P."/>
        <person name="Charlet M."/>
        <person name="Hetru C."/>
        <person name="Hodgeman B."/>
        <person name="Christensen B.M."/>
        <person name="Hoffmann J.A."/>
      </authorList>
      <dbReference type="PubMed" id="7633471"/>
      <dbReference type="DOI" id="10.1016/0965-1748(95)00043-u"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 60-99</scope>
    <scope>INDUCTION</scope>
    <source>
      <strain>Liverpool</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1995" name="Proc. R. Soc. B" volume="261" first="217" last="221">
      <title>Full sequence and characterization of two insect defensins: immune peptides from the mosquito Aedes aegypti.</title>
      <authorList>
        <person name="Chalk R."/>
        <person name="Albuquerque C.M."/>
        <person name="Ham P.J."/>
        <person name="Townson H."/>
      </authorList>
      <dbReference type="PubMed" id="7568275"/>
      <dbReference type="DOI" id="10.1098/rspb.1995.0139"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 60-99</scope>
    <source>
      <strain>REFM</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Antibacterial peptide mostly active against Gram-positive bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2 5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Hemolymph.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="5">Expressed 30 minutes after infection and remained present through to 21 days. Expressed in white or callow pupae during metamorphosis, but no expression was seen in larvae.</text>
  </comment>
  <comment type="induction">
    <text evidence="4 5">By bacterial infection.</text>
  </comment>
  <comment type="polymorphism">
    <text>There are two defensin C isoforms, C1 and C2 (shown here).</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the invertebrate defensin family. Type 1 subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AF156092">
    <property type="protein sequence ID" value="AAD40116.2"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF156093">
    <property type="protein sequence ID" value="AAD40117.2"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH477283">
    <property type="protein sequence ID" value="EAT44832.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_001657288.1">
    <property type="nucleotide sequence ID" value="XM_001657238.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P81603"/>
  <dbReference type="STRING" id="7159.P81603"/>
  <dbReference type="PaxDb" id="7159-AAEL003832-PA"/>
  <dbReference type="GeneID" id="5579094"/>
  <dbReference type="KEGG" id="aag:5579094"/>
  <dbReference type="VEuPathDB" id="VectorBase:AAEL003832"/>
  <dbReference type="eggNOG" id="ENOG502SD3P">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_174272_0_0_1"/>
  <dbReference type="InParanoid" id="P81603"/>
  <dbReference type="OrthoDB" id="3292007at2759"/>
  <dbReference type="Proteomes" id="UP000008820">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000682892">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006959">
    <property type="term" value="P:humoral immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd21806">
    <property type="entry name" value="DEFL_defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000005">
    <property type="entry name" value="Defensin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001542">
    <property type="entry name" value="Defensin_invertebrate/fungal"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13645">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13645:SF0">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01097">
    <property type="entry name" value="Defensin_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51378">
    <property type="entry name" value="INVERT_DEFENSINS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000006736" evidence="6">
    <location>
      <begin position="24"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000006737" description="Defensin-C" evidence="3 4">
    <location>
      <begin position="60"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="62"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="75"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="79"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In isoform C1.">
    <original>G</original>
    <variation>E</variation>
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In isoform C1.">
    <original>P</original>
    <variation>S</variation>
    <location>
      <position position="34"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAD40116/AAD40117." evidence="6" ref="1">
    <original>T</original>
    <variation>I</variation>
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAD40116/AAD40117." evidence="6" ref="1">
    <original>N</original>
    <variation>S</variation>
    <location>
      <position position="22"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence and 4; AA sequence." evidence="6" ref="3 4">
    <original>R</original>
    <variation>G</variation>
    <location>
      <position position="83"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence and 4; AA sequence." evidence="6" ref="3 4">
    <original>A</original>
    <variation>S</variation>
    <location>
      <position position="91"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00710"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7568275"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="7633471"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="9927179"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="99" mass="10711" checksum="7914827671AF6709" modified="2007-08-21" version="3" precursor="true">MRTLTVVCFVALCLSAIFTTGNALPGELADDVRPYANSLFDELPEESYQAAVENFRLKRATCDLLSGFGVGDSACAAHCIARRNRGGYCNAKKVCVCRN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>