<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2019-12-11" version="35" xmlns="http://uniprot.org/uniprot">
  <accession>P43170</accession>
  <name>FAR5_ASCSU</name>
  <protein>
    <recommendedName>
      <fullName>FMRFamide-like neuropeptide AF5</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ascaris suum</name>
    <name type="common">Pig roundworm</name>
    <name type="synonym">Ascaris lumbricoides</name>
    <dbReference type="NCBI Taxonomy" id="6253"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Nematoda</taxon>
      <taxon>Chromadorea</taxon>
      <taxon>Rhabditida</taxon>
      <taxon>Spirurina</taxon>
      <taxon>Ascaridomorpha</taxon>
      <taxon>Ascaridoidea</taxon>
      <taxon>Ascarididae</taxon>
      <taxon>Ascaris</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Peptides" volume="16" first="491" last="500">
      <title>Eight novel FMRFamide-like neuropeptides isolated from the nematode Ascaris suum.</title>
      <authorList>
        <person name="Cowden C."/>
        <person name="Stretton A.O.W."/>
      </authorList>
      <dbReference type="PubMed" id="7651904"/>
      <dbReference type="DOI" id="10.1016/0196-9781(94)00211-n"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-9</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the FARP (FMRFamide related peptide) family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043658" description="FMRFamide-like neuropeptide AF5">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="7651904"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="9" mass="1052" checksum="340B0059D1B76338" modified="1995-11-01" version="1">SGKPTFIRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>