<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-10-03" modified="2021-06-02" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>B3EWJ5</accession>
  <name>ALLC1_DELRA</name>
  <protein>
    <recommendedName>
      <fullName>Allatostatin-C1</fullName>
      <shortName>AST-C1</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Delia radicum</name>
    <name type="common">Cabbage root fly</name>
    <name type="synonym">Anthomyia brassicae</name>
    <dbReference type="NCBI Taxonomy" id="30064"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Muscoidea</taxon>
      <taxon>Anthomyiidae</taxon>
      <taxon>Anthomyiinae</taxon>
      <taxon>Delia</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2012" name="PLoS ONE" volume="7" first="E41543" last="E41543">
      <title>Peptidomics of the agriculturally damaging larval stage of the cabbage root fly Delia radicum (Diptera: Anthomyiidae).</title>
      <authorList>
        <person name="Zoephel J."/>
        <person name="Reiher W."/>
        <person name="Rexer K.-H."/>
        <person name="Kahnt J."/>
        <person name="Wegener C."/>
      </authorList>
      <dbReference type="PubMed" id="22848525"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0041543"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="3">CNS</tissue>
      <tissue evidence="3">Midgut</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Strongly inhibits juvenile hormone biosynthesis. May act as a neurotransmitter or neuromodulator (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed in the CNS and midgut but not in the ring gland, thoracic perisymapthetic organs (tPSO) or abdominal perisymapthetic organs (aPSO) (at protein level).</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="3">Detected in larvae.</text>
  </comment>
  <comment type="mass spectrometry" mass="1904.9" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the allatostatin family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000419715" description="Allatostatin-C1">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="3">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="7"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="unsure residue" description="I or L" evidence="3">
    <location>
      <position position="12"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P42559"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22848525"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="15" mass="1924" checksum="1605B7647DBC838E" modified="2012-10-03" version="1">QVRYRQCYFNPISCF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>