<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-05-10" modified="2024-11-27" version="116" xmlns="http://uniprot.org/uniprot">
  <accession>Q8HYP5</accession>
  <name>CCL21_MACMU</name>
  <protein>
    <recommendedName>
      <fullName>C-C motif chemokine 21</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Small-inducible cytokine A21</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CCL21</name>
  </gene>
  <organism>
    <name type="scientific">Macaca mulatta</name>
    <name type="common">Rhesus macaque</name>
    <dbReference type="NCBI Taxonomy" id="9544"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Macaca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Cytokine" volume="18" first="140" last="148">
      <title>Molecular cloning and sequencing of 25 different rhesus macaque chemokine cDNAs reveals evolutionary conservation among C, CC, CXC, and CX3C families of chemokines.</title>
      <authorList>
        <person name="Basu S."/>
        <person name="Schaefer T.M."/>
        <person name="Ghosh M."/>
        <person name="Fuller C.L."/>
        <person name="Reinhart T.A."/>
      </authorList>
      <dbReference type="PubMed" id="12126650"/>
      <dbReference type="DOI" id="10.1006/cyto.2002.0875"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Inhibits hemopoiesis and stimulates chemotaxis. Chemotactic in vitro for thymocytes and activated T-cells, but not for B-cells, macrophages, or neutrophils. Shows preferential activity towards naive T-cells. May play a role in mediating homing of lymphocytes to secondary lymphoid organs. Binds to atypical chemokine receptor ACKR4 and mediates the recruitment of beta-arrestin (ARRB1/2) to ACKR4 (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2 3">Monomer. Binds to CCR7. Interacts with PDPN; relocalizes PDPN to the basolateral membrane. Interacts with TNFAIP6 (via Link domain). Interacts with GPR174.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the intercrine beta (chemokine CC) family.</text>
  </comment>
  <dbReference type="EMBL" id="AF449275">
    <property type="protein sequence ID" value="AAN76079.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001028027.1">
    <property type="nucleotide sequence ID" value="NM_001032855.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8HYP5"/>
  <dbReference type="SMR" id="Q8HYP5"/>
  <dbReference type="STRING" id="9544.ENSMMUP00000070168"/>
  <dbReference type="PaxDb" id="9544-ENSMMUP00000021721"/>
  <dbReference type="Ensembl" id="ENSMMUT00000082253.1">
    <property type="protein sequence ID" value="ENSMMUP00000070168.1"/>
    <property type="gene ID" value="ENSMMUG00000063081.1"/>
  </dbReference>
  <dbReference type="GeneID" id="574183"/>
  <dbReference type="KEGG" id="mcc:574183"/>
  <dbReference type="CTD" id="6366"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSMMUG00000063081"/>
  <dbReference type="VGNC" id="VGNC:104885">
    <property type="gene designation" value="CCL21"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S8D1">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT01120000271901"/>
  <dbReference type="HOGENOM" id="CLU_141716_3_2_1"/>
  <dbReference type="InParanoid" id="Q8HYP5"/>
  <dbReference type="OMA" id="CKRTEQP"/>
  <dbReference type="OrthoDB" id="5265378at2759"/>
  <dbReference type="TreeFam" id="TF338224"/>
  <dbReference type="Proteomes" id="UP000006718">
    <property type="component" value="Chromosome 15"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMMUG00000063081">
    <property type="expression patterns" value="Expressed in spleen and 16 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048020">
    <property type="term" value="F:CCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042379">
    <property type="term" value="F:chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060326">
    <property type="term" value="P:cell chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd01119">
    <property type="entry name" value="Chemokine_CC_DCCL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000024">
    <property type="entry name" value="C-C motif chemokine 21"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR034133">
    <property type="entry name" value="Chemokine_CC_DCCL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF72">
    <property type="entry name" value="C-C MOTIF CHEMOKINE 21"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="4">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005221" description="C-C motif chemokine 21">
    <location>
      <begin position="24"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="5">
    <location>
      <begin position="89"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="5">
    <location>
      <begin position="98"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="103"/>
      <end position="119"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="O00585"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P84444"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="4"/>
  <evidence type="ECO:0000256" key="5">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="131" mass="14368" checksum="B8FFF8A7E2FECC1D" modified="2003-03-01" version="1" precursor="true">MAQSLALSLLILVLAFGIPGTQGSDGGAQDCCLKYSQRKIPAKVVRSYRKQEPSLGCSIPAILFLPRKRSQAELCADPKELWVQQLMQHLDKTPTPRKPVQGCRKDRGVPKNGKKGKGCKRTEQSQTPKGP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>