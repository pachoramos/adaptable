<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-02-08" modified="2023-02-22" version="10" xmlns="http://uniprot.org/uniprot">
  <accession>P86634</accession>
  <name>DELT_PHAJA</name>
  <protein>
    <recommendedName>
      <fullName>[D-Met2]-deltorphin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Phasmahyla jandaia</name>
    <name type="common">Jandaia leaf frog</name>
    <name type="synonym">Phyllomedusa jandaia</name>
    <dbReference type="NCBI Taxonomy" id="762504"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phasmahyla</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2011" name="Toxicon" volume="57" first="35" last="52">
      <title>Peptidomic dissection of the skin secretion of Phasmahyla jandaia (Bokermann and Sazima, 1978) (Anura, Hylidae, Phyllomedusinae).</title>
      <authorList>
        <person name="Rates B."/>
        <person name="Silva L.P."/>
        <person name="Ireno I.C."/>
        <person name="Leite F.S."/>
        <person name="Borges M.H."/>
        <person name="Bloch C. Jr."/>
        <person name="De Lima M.E."/>
        <person name="Pimenta A.M."/>
      </authorList>
      <dbReference type="PubMed" id="20932854"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2010.09.010"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT ASP-7</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has a very potent opiate-like activity. It has high affinity and selectivity for delta-type opioid receptors (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="954.4" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Dermorphin subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001515">
    <property type="term" value="F:opioid peptide activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0208">D-amino acid</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0257">Endorphin</keyword>
  <keyword id="KW-0555">Opioid peptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000404605" description="[D-Met2]-deltorphin">
    <location>
      <begin position="1"/>
      <end position="7"/>
    </location>
  </feature>
  <feature type="modified residue" description="D-methionine" evidence="1">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <feature type="modified residue" description="Aspartic acid 1-amide" evidence="3">
    <location>
      <position position="7"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="3">
    <location>
      <position position="5"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P05422"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="20932854"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="7" mass="956" checksum="6AA69721E9C68B30" modified="2011-02-08" version="1">YMFHLMD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>