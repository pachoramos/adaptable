<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-10-01" modified="2024-11-27" version="166" xmlns="http://uniprot.org/uniprot">
  <accession>P49886</accession>
  <name>ESR1_MACMU</name>
  <protein>
    <recommendedName>
      <fullName>Estrogen receptor</fullName>
      <shortName>ER</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>ER-alpha</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Estradiol receptor</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Nuclear receptor subfamily 3 group A member 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ESR1</name>
    <name type="synonym">ESR</name>
    <name type="synonym">NR3A1</name>
  </gene>
  <organism>
    <name type="scientific">Macaca mulatta</name>
    <name type="common">Rhesus macaque</name>
    <dbReference type="NCBI Taxonomy" id="9544"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Macaca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Endocrinology" volume="135" first="307" last="314">
      <title>Progesterone receptor, but not estradiol receptor, messenger ribonucleic acid is expressed in luteinizing granulosa cells and the corpus luteum in rhesus monkeys.</title>
      <authorList>
        <person name="Chandrasekher Y.A."/>
        <person name="Melner M.H."/>
        <person name="Nagalla S.R."/>
        <person name="Stouffer R.L."/>
      </authorList>
      <dbReference type="PubMed" id="8013365"/>
      <dbReference type="DOI" id="10.1210/endo.135.1.8013365"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Ovary</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 3">Nuclear hormone receptor. The steroid hormones and their receptors are involved in the regulation of eukaryotic gene expression and affect cellular proliferation and differentiation in target tissues. Ligand-dependent nuclear transactivation involves either direct homodimer binding to a palindromic estrogen response element (ERE) sequence or association with other DNA-binding transcription factors, such as AP-1/c-Jun, c-Fos, ATF-2, Sp1 and Sp3, to mediate ERE-independent signaling. Ligand binding induces a conformational change allowing subsequent or combinatorial association with multiprotein coactivator complexes through LXXLL motifs of their respective components. Mutual transrepression occurs between the estrogen receptor (ER) and NF-kappa-B in a cell-type specific manner. Decreases NF-kappa-B DNA-binding activity and inhibits NF-kappa-B-mediated transcription from the IL6 promoter and displace RELA/p65 and associated coregulators from the promoter. Recruited to the NF-kappa-B response element of the CCL2 and IL8 promoters and can displace CREBBP. Present with NF-kappa-B components RELA/p65 and NFKB1/p50 on ERE sequences. Can also act synergistically with NF-kappa-B to activate transcription involving respective recruitment adjacent response elements; the function involves CREBBP. Can activate the transcriptional activity of TFF1. Also mediates membrane-initiated estrogen signaling involving various kinase cascades. Essential for MTA1-mediated transcriptional regulation of BRCA1 and BCAS3 (By similarity). Maintains neuronal survival in response to ischemic reperfusion injury when in the presence of circulating estradiol (17-beta-estradiol/E2) (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2 3 4">Binds DNA as a homodimer. Can form a heterodimer with ESR2. Interacts with coactivator NCOA5. Interacts with NCOA7; the interaction is ligand-inducible. Interacts with AKAP13, CUEDC2, HEXIM1, KDM5A, MAP1S, PELP1, SMARD1, and UBE1C. Interacts with MUC1; the interaction is stimulated by 7 beta-estradiol (E2) and enhances ERS1-mediated transcription. Interacts with DNTTIP2, and UIMC1. Interacts with KMT2D/MLL2. Interacts with ATAD2; the interaction is enhanced by estradiol. Interacts with KIF18A and LDB1. Interacts with RLIM (via its C-terminus). Interacts with MACROD1. Interacts with SH2D4A and PLCG. Interacts with SH2D4A; the interaction blocks binding to PLCG and inhibits estrogen-induced cell proliferation. Interacts with DYNLL1. Interacts with CCDC62; the interaction requires estradiol and appears to enhance the transcription of target genes. Interacts with NR2C1; the interaction prevents homodimerization of ESR1 and suppresses its transcriptional activity and cell growth. Interacts with DNAAF4. Interacts with PRMT2. Interacts with PI3KR1 or PIK3R2, SRC and PTK2/FAK1. Interacts with RBFOX2. Interacts with EP300; the interaction is estrogen-dependent and enhanced by CITED1. Interacts with CITED1; the interaction is estrogen-dependent. Interacts with FAM120B, FOXL2, PHB2 and SLC30A9. Interacts with coactivators NCOA3 and NCOA6. Interacts with STK3/MST2 only in the presence of SAV1 and vice-versa. Binds to CSNK1D. Interacts with NCOA2; NCOA2 can interact with ESR1 AF-1 and AF-2 domains simultaneously and mediate their transcriptional synergy. Interacts with DDX5. Interacts with NCOA1; the interaction seems to require a self-association of N-terminal and C-terminal regions. Interacts with ZNF366, DDX17, NFKB1, RELA, SP1 and SP3. Interacts with NRIP1. Interacts with GPER1; the interaction occurs in an estrogen-dependent manner. Interacts with CLOCK and the interaction is stimulated by estrogen. Interacts with TRIP4 (ufmylated); estrogen dependent. Interacts with LMTK3; the interaction phosphorylates ESR1 (in vitro) and protects it against proteasomal degradation. Interacts with CCAR2 (via N-terminus) in a ligand-independent manner. Interacts with ZFHX3. Interacts with SFR1 in a ligand-dependent and -independent manner. Interacts with DCAF13, LATS1 and DCAF1; regulates ESR1 ubiquitination and ubiquitin-mediated proteasomal degradation. Interacts (via DNA-binding domain) with POU4F2 (C-terminus); this interaction increases the estrogen receptor ESR1 transcriptional activity in a DNA- and ligand 17-beta-estradiol-independent manner. Interacts with ESRRB isoform 1. Interacts with UBE3A and WBP2. Interacts with GTF2B. Interacts with RBM39. In the absence of hormonal ligand, interacts with TACC1 (By similarity). Interacts with BAG1; the interaction is promoted in the absence of estradiol (17-beta-estradiol/E2) (By similarity). Interacts with and ubiquitinated by STUB1; the interaction is promoted in the absence of estradiol (17-beta-estradiol/E2) (By similarity). Interacts with NEDD8 (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Golgi apparatus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cell membrane</location>
    </subcellularLocation>
    <text evidence="1">Colocalizes with ZDHHC7 and ZDHHC21 in the Golgi apparatus where most probably palmitoylation occurs. Associated with the plasma membrane when palmitoylated.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">Composed of three domains: a modulating N-terminal domain, a DNA-binding domain and a C-terminal ligand-binding domain. The modulating domain, also known as A/B or AF-1 domain has a ligand-independent transactivation function. The C-terminus contains a ligand-dependent transactivation domain, also known as E/F or AF-2 domain which overlaps with the ligand binding domain. AF-1 and AF-2 activate transcription independently and synergistically and act in a promoter- and cell-specific manner (By similarity).</text>
  </comment>
  <comment type="PTM">
    <text evidence="2 3">Ubiquitinated; regulated by LATS1 via DCAF1 it leads to ESR1 proteasomal degradation. Deubiquitinated by OTUB1 (By similarity). Ubiquitinated by STUB1/CHIP; in the CA1 hippocampal region following loss of endogenous circulating estradiol (17-beta-estradiol/E2) (By similarity). Ubiquitinated by UBR5, leading to its degradation: UBR5 specifically recognizes and binds ligand-bound ESR1 when it is not associated with coactivators (NCOAs). In presence of NCOAs, the UBR5-degron is not accessible, preventing its ubiquitination and degradation (By similarity).</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Palmitoylated at Cys-45 by ZDHHC7 and ZDHHC21. Palmitoylation is required for plasma membrane targeting and for rapid intracellular signaling via ERK and AKT kinases and cAMP generation, but not for signaling mediated by the nuclear hormone receptor (By similarity).</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">Phosphorylated by cyclin A/CDK2 and CK1. Phosphorylation probably enhances transcriptional activity. Dephosphorylation by PPP5C inhibits its transactivation activity (By similarity). Phosphorylated by LMTK3 (in vitro) (By similarity).</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">Dimethylated by PRMT1. Demethylated by JMJD6.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the nuclear hormone receptor family. NR3 subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="S71040">
    <property type="protein sequence ID" value="AAB31102.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="I67419">
    <property type="entry name" value="I67419"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P49886"/>
  <dbReference type="SMR" id="P49886"/>
  <dbReference type="STRING" id="9544.ENSMMUP00000069490"/>
  <dbReference type="PaxDb" id="9544-ENSMMUP00000025044"/>
  <dbReference type="eggNOG" id="KOG3575">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="P49886"/>
  <dbReference type="Proteomes" id="UP000006718">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005794">
    <property type="term" value="C:Golgi apparatus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005654">
    <property type="term" value="C:nucleoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043565">
    <property type="term" value="F:sequence-specific DNA binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990837">
    <property type="term" value="F:sequence-specific double-stranded DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005496">
    <property type="term" value="F:steroid binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071392">
    <property type="term" value="P:cellular response to estradiol stimulus"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043124">
    <property type="term" value="P:negative regulation of canonical NF-kappaB signal transduction"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043433">
    <property type="term" value="P:negative regulation of DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034392">
    <property type="term" value="P:negative regulation of smooth muscle cell apoptotic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030518">
    <property type="term" value="P:nuclear receptor-mediated steroid hormone signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007200">
    <property type="term" value="P:phospholipase C-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007204">
    <property type="term" value="P:positive regulation of cytosolic calcium ion concentration"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051091">
    <property type="term" value="P:positive regulation of DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045893">
    <property type="term" value="P:positive regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045429">
    <property type="term" value="P:positive regulation of nitric oxide biosynthetic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051000">
    <property type="term" value="P:positive regulation of nitric-oxide synthase activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.565.10">
    <property type="entry name" value="Retinoid X Receptor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035500">
    <property type="entry name" value="NHR-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000536">
    <property type="entry name" value="Nucl_hrmn_rcpt_lig-bd"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050200">
    <property type="entry name" value="Nuclear_hormone_rcpt_NR3"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001723">
    <property type="entry name" value="Nuclear_hrmn_rcpt"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR48092:SF16">
    <property type="entry name" value="ESTROGEN RECEPTOR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR48092">
    <property type="entry name" value="KNIRPS-RELATED PROTEIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00104">
    <property type="entry name" value="Hormone_recep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00398">
    <property type="entry name" value="STRDHORMONER"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48508">
    <property type="entry name" value="Nuclear receptor ligand-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51843">
    <property type="entry name" value="NR_LBD"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0010">Activator</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0333">Golgi apparatus</keyword>
  <keyword id="KW-0446">Lipid-binding</keyword>
  <keyword id="KW-0449">Lipoprotein</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-0564">Palmitate</keyword>
  <keyword id="KW-0675">Receptor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0754">Steroid-binding</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0832">Ubl conjugation</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <keyword id="KW-0863">Zinc-finger</keyword>
  <feature type="chain" id="PRO_0000053619" description="Estrogen receptor">
    <location>
      <begin position="1" status="less than"/>
      <end position="121" status="greater than"/>
    </location>
  </feature>
  <feature type="domain" description="NR LBD" evidence="6">
    <location>
      <begin position="1" status="less than"/>
      <end position="121" status="greater than"/>
    </location>
  </feature>
  <feature type="lipid moiety-binding region" description="S-palmitoyl cysteine" evidence="1">
    <location>
      <position position="45"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="121"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P03372"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P06211"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="4">
    <source>
      <dbReference type="UniProtKB" id="P19785"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="5">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00407"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="6">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01189"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="121" mass="13836" checksum="E985B0C710DBC402" modified="1996-10-01" version="1" fragment="single">LFAPNLLLDRNQGKCVEGMVESFDMLLATSSRFRMMNLQGEEFVCLKSIILLNSGVYTFLSSTLKSLEEKDHIHRVLDKITDTLIHLMAKAGLTLQQQHRRLAQLLLILSHIRHMSNKGME</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>