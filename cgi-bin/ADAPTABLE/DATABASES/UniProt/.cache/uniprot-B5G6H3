<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-06-16" modified="2024-03-27" version="32" xmlns="http://uniprot.org/uniprot">
  <accession>B5G6H3</accession>
  <name>WAPA_HOPST</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Stewaprin-a</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Hoplocephalus stephensii</name>
    <name type="common">Stephens' banded snake</name>
    <dbReference type="NCBI Taxonomy" id="196418"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Elapidae</taxon>
      <taxon>Notechinae</taxon>
      <taxon>Hoplocephalus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Cell. Mol. Life Sci." volume="65" first="4039" last="4054">
      <title>Common evolution of waprin and Kunitz-like toxin families in Australian venomous snakes.</title>
      <authorList>
        <person name="St Pierre L."/>
        <person name="Earl S.T."/>
        <person name="Filippovich I."/>
        <person name="Sorokina N."/>
        <person name="Masci P.P."/>
        <person name="De Jersey J."/>
        <person name="Lavin M.F."/>
      </authorList>
      <dbReference type="PubMed" id="18979207"/>
      <dbReference type="DOI" id="10.1007/s00018-008-8573-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Damages membranes of susceptible bacteria. Has no hemolytic activity. Not toxic to mice. Does not inhibit the proteinases elastase and cathepsin G.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the venom waprin family.</text>
  </comment>
  <dbReference type="EMBL" id="DQ917563">
    <property type="protein sequence ID" value="ABK63592.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU401829">
    <property type="protein sequence ID" value="ACC77778.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B5G6H3"/>
  <dbReference type="SMR" id="B5G6H3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030414">
    <property type="term" value="F:peptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044278">
    <property type="term" value="P:disruption of cell wall in another organism"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.75.10">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036645">
    <property type="entry name" value="Elafin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008197">
    <property type="entry name" value="WAP_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00095">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00003">
    <property type="entry name" value="4DISULPHCORE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00217">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57256">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51390">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5000395573" description="Stewaprin-a">
    <location>
      <begin position="25"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="domain" description="WAP" evidence="3">
    <location>
      <begin position="27"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="34"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="43"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="47"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="53"/>
      <end position="68"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P83952"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00722"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="18979207"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="18979207"/>
    </source>
  </evidence>
  <sequence length="75" mass="8355" checksum="EFEC14473CF6431E" modified="2008-10-14" version="1" precursor="true">MSSGGLLLLLGLLTLWAELIPVSGQDHPKKPGLCPPRPQKPPCVRECKNDWSCPGEQKCCRYGCIFECRDPIFVK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>