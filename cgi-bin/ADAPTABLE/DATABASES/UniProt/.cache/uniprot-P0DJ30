<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-10-19" modified="2022-05-25" version="8" xmlns="http://uniprot.org/uniprot">
  <accession>P0DJ30</accession>
  <name>AJN10_ANGJA</name>
  <protein>
    <recommendedName>
      <fullName>Antimicrobial peptide AJN-10</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Anguilla japonica</name>
    <name type="common">Japanese eel</name>
    <dbReference type="NCBI Taxonomy" id="7937"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Actinopterygii</taxon>
      <taxon>Neopterygii</taxon>
      <taxon>Teleostei</taxon>
      <taxon>Anguilliformes</taxon>
      <taxon>Anguillidae</taxon>
      <taxon>Anguilla</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2011" name="Protein J." volume="30" first="413" last="421">
      <title>Isolation and identification of a novel inducible antibacterial peptide from the skin mucus of Japanese eel, Anguilla japonica.</title>
      <authorList>
        <person name="Liang Y."/>
        <person name="Guan R."/>
        <person name="Huang W."/>
        <person name="Xu T."/>
      </authorList>
      <dbReference type="PubMed" id="21796440"/>
      <dbReference type="DOI" id="10.1007/s10930-011-9346-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin mucus</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Displays antimicrobial activity against the Gram-negative bacterium A.hydrophila.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="6044.28" method="MALDI" evidence="1"/>
  <dbReference type="AlphaFoldDB" id="P0DJ30"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000413660" description="Antimicrobial peptide AJN-10">
    <location>
      <begin position="1"/>
      <end position="20" status="greater than"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Q or T">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="21796440"/>
    </source>
  </evidence>
  <sequence length="20" mass="2164" checksum="1907391ABC40AC6B" modified="2011-10-19" version="1" fragment="single">GCPQTPRCTNYAEKGQCPPN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>