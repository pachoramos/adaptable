<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-04-16" modified="2024-11-27" version="96" xmlns="http://uniprot.org/uniprot">
  <accession>P94416</accession>
  <name>PHRC_BACSU</name>
  <protein>
    <recommendedName>
      <fullName evidence="13">Competence and sporulation stimulating factor</fullName>
      <shortName evidence="13">CSF</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="14">Extracellular signaling peptide CSF</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="14">RapC inhibitor</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="13" type="primary">phrC</name>
    <name type="synonym">hprC</name>
    <name type="ordered locus">BSU03780</name>
  </gene>
  <organism>
    <name type="scientific">Bacillus subtilis (strain 168)</name>
    <dbReference type="NCBI Taxonomy" id="224308"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Bacillaceae</taxon>
      <taxon>Bacillus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Microbiology" volume="142" first="3047" last="3056">
      <title>The 25 degrees-36 degrees region of the Bacillus subtilis chromosome: determination of the sequence of a 146 kb segment and identification of 113 genes.</title>
      <authorList>
        <person name="Yamane K."/>
        <person name="Kumano M."/>
        <person name="Kurita K."/>
      </authorList>
      <dbReference type="PubMed" id="8969502"/>
      <dbReference type="DOI" id="10.1099/13500872-142-11-3047"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>168</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="Nature" volume="390" first="249" last="256">
      <title>The complete genome sequence of the Gram-positive bacterium Bacillus subtilis.</title>
      <authorList>
        <person name="Kunst F."/>
        <person name="Ogasawara N."/>
        <person name="Moszer I."/>
        <person name="Albertini A.M."/>
        <person name="Alloni G."/>
        <person name="Azevedo V."/>
        <person name="Bertero M.G."/>
        <person name="Bessieres P."/>
        <person name="Bolotin A."/>
        <person name="Borchert S."/>
        <person name="Borriss R."/>
        <person name="Boursier L."/>
        <person name="Brans A."/>
        <person name="Braun M."/>
        <person name="Brignell S.C."/>
        <person name="Bron S."/>
        <person name="Brouillet S."/>
        <person name="Bruschi C.V."/>
        <person name="Caldwell B."/>
        <person name="Capuano V."/>
        <person name="Carter N.M."/>
        <person name="Choi S.-K."/>
        <person name="Codani J.-J."/>
        <person name="Connerton I.F."/>
        <person name="Cummings N.J."/>
        <person name="Daniel R.A."/>
        <person name="Denizot F."/>
        <person name="Devine K.M."/>
        <person name="Duesterhoeft A."/>
        <person name="Ehrlich S.D."/>
        <person name="Emmerson P.T."/>
        <person name="Entian K.-D."/>
        <person name="Errington J."/>
        <person name="Fabret C."/>
        <person name="Ferrari E."/>
        <person name="Foulger D."/>
        <person name="Fritz C."/>
        <person name="Fujita M."/>
        <person name="Fujita Y."/>
        <person name="Fuma S."/>
        <person name="Galizzi A."/>
        <person name="Galleron N."/>
        <person name="Ghim S.-Y."/>
        <person name="Glaser P."/>
        <person name="Goffeau A."/>
        <person name="Golightly E.J."/>
        <person name="Grandi G."/>
        <person name="Guiseppi G."/>
        <person name="Guy B.J."/>
        <person name="Haga K."/>
        <person name="Haiech J."/>
        <person name="Harwood C.R."/>
        <person name="Henaut A."/>
        <person name="Hilbert H."/>
        <person name="Holsappel S."/>
        <person name="Hosono S."/>
        <person name="Hullo M.-F."/>
        <person name="Itaya M."/>
        <person name="Jones L.-M."/>
        <person name="Joris B."/>
        <person name="Karamata D."/>
        <person name="Kasahara Y."/>
        <person name="Klaerr-Blanchard M."/>
        <person name="Klein C."/>
        <person name="Kobayashi Y."/>
        <person name="Koetter P."/>
        <person name="Koningstein G."/>
        <person name="Krogh S."/>
        <person name="Kumano M."/>
        <person name="Kurita K."/>
        <person name="Lapidus A."/>
        <person name="Lardinois S."/>
        <person name="Lauber J."/>
        <person name="Lazarevic V."/>
        <person name="Lee S.-M."/>
        <person name="Levine A."/>
        <person name="Liu H."/>
        <person name="Masuda S."/>
        <person name="Mauel C."/>
        <person name="Medigue C."/>
        <person name="Medina N."/>
        <person name="Mellado R.P."/>
        <person name="Mizuno M."/>
        <person name="Moestl D."/>
        <person name="Nakai S."/>
        <person name="Noback M."/>
        <person name="Noone D."/>
        <person name="O'Reilly M."/>
        <person name="Ogawa K."/>
        <person name="Ogiwara A."/>
        <person name="Oudega B."/>
        <person name="Park S.-H."/>
        <person name="Parro V."/>
        <person name="Pohl T.M."/>
        <person name="Portetelle D."/>
        <person name="Porwollik S."/>
        <person name="Prescott A.M."/>
        <person name="Presecan E."/>
        <person name="Pujic P."/>
        <person name="Purnelle B."/>
        <person name="Rapoport G."/>
        <person name="Rey M."/>
        <person name="Reynolds S."/>
        <person name="Rieger M."/>
        <person name="Rivolta C."/>
        <person name="Rocha E."/>
        <person name="Roche B."/>
        <person name="Rose M."/>
        <person name="Sadaie Y."/>
        <person name="Sato T."/>
        <person name="Scanlan E."/>
        <person name="Schleich S."/>
        <person name="Schroeter R."/>
        <person name="Scoffone F."/>
        <person name="Sekiguchi J."/>
        <person name="Sekowska A."/>
        <person name="Seror S.J."/>
        <person name="Serror P."/>
        <person name="Shin B.-S."/>
        <person name="Soldo B."/>
        <person name="Sorokin A."/>
        <person name="Tacconi E."/>
        <person name="Takagi T."/>
        <person name="Takahashi H."/>
        <person name="Takemaru K."/>
        <person name="Takeuchi M."/>
        <person name="Tamakoshi A."/>
        <person name="Tanaka T."/>
        <person name="Terpstra P."/>
        <person name="Tognoni A."/>
        <person name="Tosato V."/>
        <person name="Uchiyama S."/>
        <person name="Vandenbol M."/>
        <person name="Vannier F."/>
        <person name="Vassarotti A."/>
        <person name="Viari A."/>
        <person name="Wambutt R."/>
        <person name="Wedler E."/>
        <person name="Wedler H."/>
        <person name="Weitzenegger T."/>
        <person name="Winters P."/>
        <person name="Wipat A."/>
        <person name="Yamamoto H."/>
        <person name="Yamane K."/>
        <person name="Yasumoto K."/>
        <person name="Yata K."/>
        <person name="Yoshida K."/>
        <person name="Yoshikawa H.-F."/>
        <person name="Zumstein E."/>
        <person name="Yoshikawa H."/>
        <person name="Danchin A."/>
      </authorList>
      <dbReference type="PubMed" id="9384377"/>
      <dbReference type="DOI" id="10.1038/36786"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>168</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1996" name="Genes Dev." volume="10" first="2014" last="2024">
      <title>Purification and characterization of an extracellular peptide factor that affects two different developmental pathways in Bacillus subtilis.</title>
      <authorList>
        <person name="Solomon J.M."/>
        <person name="Lazazzera B.A."/>
        <person name="Grossman A.D."/>
      </authorList>
      <dbReference type="PubMed" id="8769645"/>
      <dbReference type="DOI" id="10.1101/gad.10.16.2014"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 36-40</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>168 / JH642</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1997" name="Cell" volume="89" first="917" last="925">
      <title>An exported peptide functions intracellularly to contribute to cell density signaling in B. subtilis.</title>
      <authorList>
        <person name="Lazazzera B.A."/>
        <person name="Solomon J.M."/>
        <person name="Grossman A.D."/>
      </authorList>
      <dbReference type="PubMed" id="9200610"/>
      <dbReference type="DOI" id="10.1016/s0092-8674(00)80277-9"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MUTAGENESIS OF GLU-36; ARG-37; GLY-38; MET-39 AND THR-40</scope>
    <source>
      <strain>168 / JH642</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1997" name="Proc. Natl. Acad. Sci. U.S.A." volume="94" first="8612" last="8617">
      <title>A peptide export-import control circuit modulating bacterial development regulates protein phosphatases of the phosphorelay.</title>
      <authorList>
        <person name="Perego M."/>
      </authorList>
      <dbReference type="PubMed" id="9238025"/>
      <dbReference type="DOI" id="10.1073/pnas.94.16.8612"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>MUTAGENESIS OF GLU-36; GLY-38 AND MET-39</scope>
    <source>
      <strain>168 / JH642</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1999" name="J. Bacteriol." volume="181" first="5193" last="5200">
      <title>An autoregulatory circuit affecting peptide signaling in Bacillus subtilis.</title>
      <authorList>
        <person name="Lazazzera B.A."/>
        <person name="Kurtser I.G."/>
        <person name="McQuade R.S."/>
        <person name="Grossman A.D."/>
      </authorList>
      <dbReference type="PubMed" id="10464187"/>
      <dbReference type="DOI" id="10.1128/jb.181.17.5193-5200.1999"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>TRANSCRIPTIONAL REGULATION</scope>
    <source>
      <strain>168 / JH642</strain>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2003" name="Front. Biosci." volume="8" first="d32" last="d45">
      <title>The extracellular Phr peptide-Rap phosphatase signaling circuit of Bacillus subtilis.</title>
      <authorList>
        <person name="Pottathil M."/>
        <person name="Lazazzera B.A."/>
      </authorList>
      <dbReference type="PubMed" id="12456319"/>
      <dbReference type="DOI" id="10.2741/913"/>
    </citation>
    <scope>FUNCTION IN QUORUM SENSING</scope>
    <scope>REVIEW</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2003" name="J. Bacteriol." volume="185" first="4861" last="4871">
      <title>Molecular analysis of Phr peptide processing in Bacillus subtilis.</title>
      <authorList>
        <person name="Stephenson S."/>
        <person name="Mueller C."/>
        <person name="Jiang M."/>
        <person name="Perego M."/>
      </authorList>
      <dbReference type="PubMed" id="12897006"/>
      <dbReference type="DOI" id="10.1128/jb.185.16.4861-4871.2003"/>
    </citation>
    <scope>CLEAVAGE SITE</scope>
    <scope>MUTAGENESIS OF THR-35</scope>
    <source>
      <strain>168 / JH642</strain>
    </source>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2003" name="Mol. Microbiol." volume="49" first="1509" last="1522">
      <title>TPR-mediated interaction of RapC with ComA inhibits response regulator-DNA binding for competence development in Bacillus subtilis.</title>
      <authorList>
        <person name="Core L."/>
        <person name="Perego M."/>
      </authorList>
      <dbReference type="PubMed" id="12950917"/>
      <dbReference type="DOI" id="10.1046/j.1365-2958.2003.03659.x"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH RAPC</scope>
    <source>
      <strain>168 / JH642</strain>
    </source>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2006" name="J. Bacteriol." volume="188" first="5273" last="5285">
      <title>Modulation of the ComA-dependent quorum response in Bacillus subtilis by multiple Rap proteins and Phr peptides.</title>
      <authorList>
        <person name="Auchtung J.M."/>
        <person name="Lee C.A."/>
        <person name="Grossman A.D."/>
      </authorList>
      <dbReference type="PubMed" id="16816200"/>
      <dbReference type="DOI" id="10.1128/jb.00300-06"/>
    </citation>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>168 / JH642</strain>
    </source>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2007" name="Cell Host Microbe" volume="1" first="299" last="308">
      <title>The Bacillus subtilis quorum-sensing molecule CSF contributes to intestinal homeostasis via OCTN2, a host cell membrane transporter.</title>
      <authorList>
        <person name="Fujiya M."/>
        <person name="Musch M.W."/>
        <person name="Nakagawa Y."/>
        <person name="Hu S."/>
        <person name="Alverdy J."/>
        <person name="Kohgo Y."/>
        <person name="Schneewind O."/>
        <person name="Jabri B."/>
        <person name="Chang E.B."/>
      </authorList>
      <dbReference type="PubMed" id="18005709"/>
      <dbReference type="DOI" id="10.1016/j.chom.2007.05.004"/>
    </citation>
    <scope>FUNCTION IN HUMAN INTESTINAL HOMEOSTASIS</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2007" name="Mol. Microbiol." volume="65" first="1321" last="1333">
      <title>Identification of subtilisin, Epr and Vpr as enzymes that produce CSF, an extracellular signalling peptide of Bacillus subtilis.</title>
      <authorList>
        <person name="Lanigan-Gerdes S."/>
        <person name="Dooley A.N."/>
        <person name="Faull K.F."/>
        <person name="Lazazzera B.A."/>
      </authorList>
      <dbReference type="PubMed" id="17666034"/>
      <dbReference type="DOI" id="10.1111/j.1365-2958.2007.05869.x"/>
    </citation>
    <scope>PROTEOLYTIC PROCESSING</scope>
    <scope>CLEAVAGE SITE</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2008" name="J. Bacteriol." volume="190" first="4095" last="4099">
      <title>CSF, a species-specific extracellular signaling peptide for communication among strains of Bacillus subtilis and Bacillus mojavensis.</title>
      <authorList>
        <person name="Pottathil M."/>
        <person name="Jung A."/>
        <person name="Lazazzera B.A."/>
      </authorList>
      <dbReference type="PubMed" id="18375560"/>
      <dbReference type="DOI" id="10.1128/jb.00187-08"/>
    </citation>
    <scope>FUNCTION AS A SPECIES-SPECIFIC SIGNALING MOLECULE</scope>
    <source>
      <strain>168 / JH642</strain>
    </source>
  </reference>
  <reference key="14">
    <citation type="journal article" date="2008" name="J. Bacteriol." volume="190" first="6668" last="6675">
      <title>Identification of residues important for cleavage of the extracellular signaling peptide CSF of Bacillus subtilis from its precursor protein.</title>
      <authorList>
        <person name="Lanigan-Gerdes S."/>
        <person name="Briceno G."/>
        <person name="Dooley A.N."/>
        <person name="Faull K.F."/>
        <person name="Lazazzera B.A."/>
      </authorList>
      <dbReference type="PubMed" id="18689487"/>
      <dbReference type="DOI" id="10.1128/jb.00910-08"/>
    </citation>
    <scope>PROTEOLYTIC PROCESSING</scope>
    <scope>CLEAVAGE SITE</scope>
    <scope>MUTAGENESIS OF ASP-31; PHE-32; HIS-33; VAL-34 AND THR-35</scope>
  </reference>
  <reference key="15">
    <citation type="journal article" date="2009" name="Microbiology" volume="155" first="398" last="412">
      <title>Identification of genes required for different stages of dendritic swarming in Bacillus subtilis, with a novel role for phrC.</title>
      <authorList>
        <person name="Hamze K."/>
        <person name="Julkowska D."/>
        <person name="Autret S."/>
        <person name="Hinc K."/>
        <person name="Nagorska K."/>
        <person name="Sekowska A."/>
        <person name="Holland I.B."/>
        <person name="Seror S.J."/>
      </authorList>
      <dbReference type="PubMed" id="19202088"/>
      <dbReference type="DOI" id="10.1099/mic.0.021477-0"/>
    </citation>
    <scope>FUNCTION IN SWARMING STRAINS</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3 7 10 11 12 15">Signaling molecule that serves as a cell density signal for both genetic competence development and sporulation (PubMed:8769645, PubMed:9200610). Secreted during production, but the mature peptide acts intracellularly, indicating that it needs to be imported into the cell to function (PubMed:8769645, PubMed:9200610). At low concentrations, CSF stimulates expression of the genes controlled by ComA, a transcriptional factor that regulates the development of genetic competence (PubMed:10464187, PubMed:8769645, PubMed:9200610). It includes the srfA operon, which encodes a small protein, ComS, required for competence development, and the surfactin biosynthetic enzymes (PubMed:10464187, PubMed:8769645, PubMed:9200610). Acts by inhibiting RapC, which regulates the activity of ComA (PubMed:12950917, PubMed:8769645). At high concentrations, it inhibits expression of those same ComA-controlled genes, maybe by inhibiting activity of the kinase ComP (PubMed:10464187, PubMed:8769645, PubMed:9200610). In addition, high concentrations of CSF can stimulate sporulation under some conditions (PubMed:8769645, PubMed:9200610). Also inhibits RapB activity, with lower efficiency, but does not act on RapA (PubMed:9238025). Is probably involved in the quorum sensing control of sporulation (Probable). CSF is a species-specific signaling molecule that partially compensates for the lack of ComX-mediated communication between different strains of B.subtilis (PubMed:18375560).</text>
  </comment>
  <comment type="function">
    <text evidence="6 14">B.subtilis is a well-characterized soil and water saprophyte, but it is also found in enteric flora of many species, including humans (Probable). In this environment, CSF can be transported into human intestinal epithelia via OCTN2, a host cell membrane transporter, and can induce cytoprotective heat shock proteins contributing to intestinal homeostasis (PubMed:18005709).</text>
  </comment>
  <comment type="function">
    <text evidence="9">In addition, in non-domesticated swarming strains of B.subtilis, the residual propeptide exposed on the exterior of the cytoplasmic membrane may have an extracellular role in swarming (PubMed:19202088). This function is probably not dependent on CSF (PubMed:19202088).</text>
  </comment>
  <comment type="subunit">
    <text evidence="3">Interacts with RapC and inhibits its interaction with ComA.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-16042798">
      <id>P94416</id>
    </interactant>
    <interactant intactId="EBI-5246213">
      <id>O34327</id>
      <label>rapJ</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>4</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="10 11">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="10 11">Cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="6">Host cell</location>
    </subcellularLocation>
    <text evidence="6 10 11">Produced through an export-import maturation process (PubMed:8769645, PubMed:9200610). Peptides are secreted by the bacterium, and are then actively transported into the cell where they interact with intracellular receptors to regulate gene expression (PubMed:8769645, PubMed:9200610). Transported into the cell by the Opp (Spo0K) oligopeptide permease (PubMed:8769645, PubMed:9200610). In human gut, is transported into the intestinal epithelia by OCTN2 (PubMed:18005709).</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="1">By mid-exponential phase, CSF accumulates to concentrations that stimulate ComA-dependent gene expression (PubMed:10464187). Upon entry into stationary phase, CSF reaches high concentrations that stimulate sporulation and inhibit ComA-dependent gene expression (PubMed:10464187).</text>
  </comment>
  <comment type="induction">
    <text evidence="1">Part of the rapC-phrC operon, which is controlled by the P1 promoter (PubMed:10464187). Transcription from the P1 promoter is activated by high cell density through the phosphorylated form of ComA (PubMed:10464187). PhrC is part of an autoregulatory loop, and it positively regulates its own expression (PubMed:10464187). In addition, transcription of phrC is also regulated by a P2 promoter, which directs transcription of phrC only and is controlled by the sigma-H factor (PubMed:10464187).</text>
  </comment>
  <comment type="PTM">
    <text evidence="5 8 16">Secreted with a propeptide domain, which is cleaved in the cell wall by the secreted serine proteases subtilisin, Epr and Vpr to produce a mature signaling peptide (PubMed:17666034, PubMed:18689487). Contains a predicted signal peptide cleavage site in the N-terminal region, however the propeptide is probably subject to only one processing event, at the N-terminal end of the mature peptide (Probable).</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="4 9 10">Null mutant produces no detectable CSF activity, but null mutation has modest effects on competence gene expression (PubMed:8769645). Deletion of the gene results in decreased expression of genes activated by ComA, and significantly changes the expression of 66 operons (PubMed:16816200). In non-domesticated strains, the mutant is blocked early in swarming, forming stunted dendrites, with abnormal dendrite initiation morphology (PubMed:19202088). Mutant exhibits a specific migration defect that cannot be trans-complemented by CSF in a mixed swarm (PubMed:19202088).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="4">CSF, PhrF and PhrK stimulate ComA-dependent gene expression to different levels and are all required for full expression of genes activated by ComA.</text>
  </comment>
  <comment type="similarity">
    <text evidence="14">Belongs to the Phr family.</text>
  </comment>
  <dbReference type="EMBL" id="D50453">
    <property type="protein sequence ID" value="BAA09010.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL009126">
    <property type="protein sequence ID" value="CAB12186.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A69677">
    <property type="entry name" value="A69677"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_388260.1">
    <property type="nucleotide sequence ID" value="NC_000964.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003224994.1">
    <property type="nucleotide sequence ID" value="NZ_OZ025638.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P94416"/>
  <dbReference type="SMR" id="P94416"/>
  <dbReference type="DIP" id="DIP-60168N"/>
  <dbReference type="IntAct" id="P94416">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="224308.BSU03780"/>
  <dbReference type="PaxDb" id="224308-BSU03780"/>
  <dbReference type="EnsemblBacteria" id="CAB12186">
    <property type="protein sequence ID" value="CAB12186"/>
    <property type="gene ID" value="BSU_03780"/>
  </dbReference>
  <dbReference type="GeneID" id="64302239"/>
  <dbReference type="GeneID" id="938281"/>
  <dbReference type="KEGG" id="bsu:BSU03780"/>
  <dbReference type="PATRIC" id="fig|224308.179.peg.398"/>
  <dbReference type="InParanoid" id="P94416"/>
  <dbReference type="BioCyc" id="BSUB:BSU03780-MONOMER"/>
  <dbReference type="PRO" id="PR:P94416"/>
  <dbReference type="Proteomes" id="UP000001570">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043657">
    <property type="term" value="C:host cell"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030420">
    <property type="term" value="P:establishment of competence for transformation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009372">
    <property type="term" value="P:quorum sensing"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030435">
    <property type="term" value="P:sporulation resulting in formation of a cellular spore"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025899">
    <property type="entry name" value="PhrC_PhrF"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11131">
    <property type="entry name" value="PhrC_PhrF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0178">Competence</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0673">Quorum sensing</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0749">Sporulation</keyword>
  <feature type="propeptide" id="PRO_0000456997" evidence="16">
    <location>
      <begin position="1"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000161729" description="Competence and sporulation stimulating factor" evidence="5 8 10 16">
    <location>
      <begin position="36"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="site" description="Cleavage; by serine proteases" evidence="5 8 16">
    <location>
      <begin position="35"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="40% decrease in CSF production." evidence="8">
    <original>D</original>
    <variation>A</variation>
    <location>
      <position position="31"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="95% decrease in CSF production. 98% reduction in cleavage of pro-CSF by subtilisin and 90% reduction in cleavage by Vpr." evidence="8">
    <original>F</original>
    <variation>K</variation>
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Increases levels of CSF production." evidence="8">
    <original>F</original>
    <variation>M</variation>
    <variation>V</variation>
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="65% decrease in CSF production. 47% reduction in cleavage of pro-CSF by subtilisin and 79% reduction in cleavage by Vpr." evidence="8">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="33"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="50% decrease in CSF production. 97% reduction in cleavage of pro-CSF by subtilisin and 42% reduction in cleavage by Vpr." evidence="8">
    <original>V</original>
    <variation>E</variation>
    <location>
      <position position="34"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Slightly increases transcription of the ComA-controlled gene rapA. No change in CSF levels." evidence="2 8">
    <original>T</original>
    <variation>A</variation>
    <location>
      <position position="35"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="35% decrease in CSF production." evidence="8">
    <original>T</original>
    <variation>K</variation>
    <location>
      <position position="35"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreases transcription of the ComA-controlled gene rapA." evidence="2">
    <original>T</original>
    <variation>P</variation>
    <location>
      <position position="35"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Does not stimulate expression of the srfA operon. No change in inhibitory activity. Strong decrease in sporulation stimulation. Loss of activity toward RapB. Does not acquire any ability to inhibit RapA." evidence="11 12">
    <original>E</original>
    <variation>A</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Does not stimulate expression of the srfA operon. Loss of inhibitory activity. Strong decrease in sporulation stimulation." evidence="11">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Does not stimulate expression of the srfA operon. No change in inhibitory activity and in sporulation stimulation." evidence="11">
    <original>G</original>
    <variation>A</variation>
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Increases activity toward RapB. Does not acquire any ability to inhibit RapA." evidence="12">
    <original>G</original>
    <variation>N</variation>
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Does not stimulate expression of the srfA operon. Loss of inhibitory activity. Strong decrease in sporulation stimulation." evidence="11">
    <original>M</original>
    <variation>A</variation>
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of activity toward RapB. Does not acquire any ability to inhibit RapA." evidence="12">
    <original>M</original>
    <variation>Q</variation>
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Does not stimulate expression of the srfA operon. Reduced inhibitory activity. Strong decrease in sporulation stimulation." evidence="11">
    <original>T</original>
    <variation>A</variation>
    <location>
      <position position="40"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10464187"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12897006"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12950917"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="16816200"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="17666034"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="18005709"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="18375560"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="18689487"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="19202088"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="8769645"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="11">
    <source>
      <dbReference type="PubMed" id="9200610"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="12">
    <source>
      <dbReference type="PubMed" id="9238025"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="13">
    <source>
      <dbReference type="PubMed" id="8769645"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="14"/>
  <evidence type="ECO:0000305" key="15">
    <source>
      <dbReference type="PubMed" id="12456319"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="16">
    <source>
      <dbReference type="PubMed" id="12897006"/>
    </source>
  </evidence>
  <sequence length="40" mass="4198" checksum="06636C618FF3CB2D" modified="1997-05-01" version="1" precursor="true">MKLKSKLFVICLAAAAIFTAAGVSANAEALDFHVTERGMT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>