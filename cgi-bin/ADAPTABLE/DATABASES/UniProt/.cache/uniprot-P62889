<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1987-08-13" modified="2024-11-27" version="170" xmlns="http://uniprot.org/uniprot">
  <accession>P62889</accession>
  <accession>P04645</accession>
  <name>RL30_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Large ribosomal subunit protein eL30</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>60S ribosomal protein L30</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Rpl30</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1984" name="Mol. Cell. Biol." volume="4" first="2518" last="2528">
      <title>Characterization of the expressed gene and several processed pseudogenes for the mouse ribosomal protein L30 gene family.</title>
      <authorList>
        <person name="Wiedemann L.M."/>
        <person name="Perry R.P."/>
      </authorList>
      <dbReference type="PubMed" id="6513928"/>
      <dbReference type="DOI" id="10.1128/mcb.4.11.2518-2528.1984"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Science" volume="309" first="1559" last="1563">
      <title>The transcriptional landscape of the mammalian genome.</title>
      <authorList>
        <person name="Carninci P."/>
        <person name="Kasukawa T."/>
        <person name="Katayama S."/>
        <person name="Gough J."/>
        <person name="Frith M.C."/>
        <person name="Maeda N."/>
        <person name="Oyama R."/>
        <person name="Ravasi T."/>
        <person name="Lenhard B."/>
        <person name="Wells C."/>
        <person name="Kodzius R."/>
        <person name="Shimokawa K."/>
        <person name="Bajic V.B."/>
        <person name="Brenner S.E."/>
        <person name="Batalov S."/>
        <person name="Forrest A.R."/>
        <person name="Zavolan M."/>
        <person name="Davis M.J."/>
        <person name="Wilming L.G."/>
        <person name="Aidinis V."/>
        <person name="Allen J.E."/>
        <person name="Ambesi-Impiombato A."/>
        <person name="Apweiler R."/>
        <person name="Aturaliya R.N."/>
        <person name="Bailey T.L."/>
        <person name="Bansal M."/>
        <person name="Baxter L."/>
        <person name="Beisel K.W."/>
        <person name="Bersano T."/>
        <person name="Bono H."/>
        <person name="Chalk A.M."/>
        <person name="Chiu K.P."/>
        <person name="Choudhary V."/>
        <person name="Christoffels A."/>
        <person name="Clutterbuck D.R."/>
        <person name="Crowe M.L."/>
        <person name="Dalla E."/>
        <person name="Dalrymple B.P."/>
        <person name="de Bono B."/>
        <person name="Della Gatta G."/>
        <person name="di Bernardo D."/>
        <person name="Down T."/>
        <person name="Engstrom P."/>
        <person name="Fagiolini M."/>
        <person name="Faulkner G."/>
        <person name="Fletcher C.F."/>
        <person name="Fukushima T."/>
        <person name="Furuno M."/>
        <person name="Futaki S."/>
        <person name="Gariboldi M."/>
        <person name="Georgii-Hemming P."/>
        <person name="Gingeras T.R."/>
        <person name="Gojobori T."/>
        <person name="Green R.E."/>
        <person name="Gustincich S."/>
        <person name="Harbers M."/>
        <person name="Hayashi Y."/>
        <person name="Hensch T.K."/>
        <person name="Hirokawa N."/>
        <person name="Hill D."/>
        <person name="Huminiecki L."/>
        <person name="Iacono M."/>
        <person name="Ikeo K."/>
        <person name="Iwama A."/>
        <person name="Ishikawa T."/>
        <person name="Jakt M."/>
        <person name="Kanapin A."/>
        <person name="Katoh M."/>
        <person name="Kawasawa Y."/>
        <person name="Kelso J."/>
        <person name="Kitamura H."/>
        <person name="Kitano H."/>
        <person name="Kollias G."/>
        <person name="Krishnan S.P."/>
        <person name="Kruger A."/>
        <person name="Kummerfeld S.K."/>
        <person name="Kurochkin I.V."/>
        <person name="Lareau L.F."/>
        <person name="Lazarevic D."/>
        <person name="Lipovich L."/>
        <person name="Liu J."/>
        <person name="Liuni S."/>
        <person name="McWilliam S."/>
        <person name="Madan Babu M."/>
        <person name="Madera M."/>
        <person name="Marchionni L."/>
        <person name="Matsuda H."/>
        <person name="Matsuzawa S."/>
        <person name="Miki H."/>
        <person name="Mignone F."/>
        <person name="Miyake S."/>
        <person name="Morris K."/>
        <person name="Mottagui-Tabar S."/>
        <person name="Mulder N."/>
        <person name="Nakano N."/>
        <person name="Nakauchi H."/>
        <person name="Ng P."/>
        <person name="Nilsson R."/>
        <person name="Nishiguchi S."/>
        <person name="Nishikawa S."/>
        <person name="Nori F."/>
        <person name="Ohara O."/>
        <person name="Okazaki Y."/>
        <person name="Orlando V."/>
        <person name="Pang K.C."/>
        <person name="Pavan W.J."/>
        <person name="Pavesi G."/>
        <person name="Pesole G."/>
        <person name="Petrovsky N."/>
        <person name="Piazza S."/>
        <person name="Reed J."/>
        <person name="Reid J.F."/>
        <person name="Ring B.Z."/>
        <person name="Ringwald M."/>
        <person name="Rost B."/>
        <person name="Ruan Y."/>
        <person name="Salzberg S.L."/>
        <person name="Sandelin A."/>
        <person name="Schneider C."/>
        <person name="Schoenbach C."/>
        <person name="Sekiguchi K."/>
        <person name="Semple C.A."/>
        <person name="Seno S."/>
        <person name="Sessa L."/>
        <person name="Sheng Y."/>
        <person name="Shibata Y."/>
        <person name="Shimada H."/>
        <person name="Shimada K."/>
        <person name="Silva D."/>
        <person name="Sinclair B."/>
        <person name="Sperling S."/>
        <person name="Stupka E."/>
        <person name="Sugiura K."/>
        <person name="Sultana R."/>
        <person name="Takenaka Y."/>
        <person name="Taki K."/>
        <person name="Tammoja K."/>
        <person name="Tan S.L."/>
        <person name="Tang S."/>
        <person name="Taylor M.S."/>
        <person name="Tegner J."/>
        <person name="Teichmann S.A."/>
        <person name="Ueda H.R."/>
        <person name="van Nimwegen E."/>
        <person name="Verardo R."/>
        <person name="Wei C.L."/>
        <person name="Yagi K."/>
        <person name="Yamanishi H."/>
        <person name="Zabarovsky E."/>
        <person name="Zhu S."/>
        <person name="Zimmer A."/>
        <person name="Hide W."/>
        <person name="Bult C."/>
        <person name="Grimmond S.M."/>
        <person name="Teasdale R.D."/>
        <person name="Liu E.T."/>
        <person name="Brusic V."/>
        <person name="Quackenbush J."/>
        <person name="Wahlestedt C."/>
        <person name="Mattick J.S."/>
        <person name="Hume D.A."/>
        <person name="Kai C."/>
        <person name="Sasaki D."/>
        <person name="Tomaru Y."/>
        <person name="Fukuda S."/>
        <person name="Kanamori-Katayama M."/>
        <person name="Suzuki M."/>
        <person name="Aoki J."/>
        <person name="Arakawa T."/>
        <person name="Iida J."/>
        <person name="Imamura K."/>
        <person name="Itoh M."/>
        <person name="Kato T."/>
        <person name="Kawaji H."/>
        <person name="Kawagashira N."/>
        <person name="Kawashima T."/>
        <person name="Kojima M."/>
        <person name="Kondo S."/>
        <person name="Konno H."/>
        <person name="Nakano K."/>
        <person name="Ninomiya N."/>
        <person name="Nishio T."/>
        <person name="Okada M."/>
        <person name="Plessy C."/>
        <person name="Shibata K."/>
        <person name="Shiraki T."/>
        <person name="Suzuki S."/>
        <person name="Tagami M."/>
        <person name="Waki K."/>
        <person name="Watahiki A."/>
        <person name="Okamura-Oho Y."/>
        <person name="Suzuki H."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
      </authorList>
      <dbReference type="PubMed" id="16141072"/>
      <dbReference type="DOI" id="10.1126/science.1112014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>FVB/N</strain>
      <tissue>Kidney</tissue>
      <tissue>Mammary tumor</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2010" name="Cell" volume="143" first="1174" last="1189">
      <title>A tissue-specific atlas of mouse protein phosphorylation and expression.</title>
      <authorList>
        <person name="Huttlin E.L."/>
        <person name="Jedrychowski M.P."/>
        <person name="Elias J.E."/>
        <person name="Goswami T."/>
        <person name="Rad R."/>
        <person name="Beausoleil S.A."/>
        <person name="Villen J."/>
        <person name="Haas W."/>
        <person name="Sowa M.E."/>
        <person name="Gygi S.P."/>
      </authorList>
      <dbReference type="PubMed" id="21183079"/>
      <dbReference type="DOI" id="10.1016/j.cell.2010.12.001"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Brain</tissue>
      <tissue>Brown adipose tissue</tissue>
      <tissue>Heart</tissue>
      <tissue>Kidney</tissue>
      <tissue>Liver</tissue>
      <tissue>Lung</tissue>
      <tissue>Pancreas</tissue>
      <tissue>Spleen</tissue>
      <tissue>Testis</tissue>
    </source>
  </reference>
  <reference evidence="4 5" key="5">
    <citation type="journal article" date="2022" name="Nature">
      <title>A male germ-cell-specific ribosome controls male fertility.</title>
      <authorList>
        <person name="Li H."/>
        <person name="Huo Y."/>
        <person name="He X."/>
        <person name="Yao L."/>
        <person name="Zhang H."/>
        <person name="Cui Y."/>
        <person name="Xiao H."/>
        <person name="Xie W."/>
        <person name="Zhang D."/>
        <person name="Wang Y."/>
        <person name="Zhang S."/>
        <person name="Tu H."/>
        <person name="Cheng Y."/>
        <person name="Guo Y."/>
        <person name="Cao X."/>
        <person name="Zhu Y."/>
        <person name="Jiang T."/>
        <person name="Guo X."/>
        <person name="Qin Y."/>
        <person name="Sha J."/>
      </authorList>
      <dbReference type="PubMed" id="36517592"/>
      <dbReference type="DOI" id="10.1038/s41586-022-05508-0"/>
    </citation>
    <scope>STRUCTURE BY ELECTRON MICROSCOPY (3.03 ANGSTROMS) OF RIBOSOME</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Component of the large ribosomal subunit (PubMed:36517592). The ribosome is a large ribonucleoprotein complex responsible for the synthesis of proteins in the cell (PubMed:36517592).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Component of the large ribosomal subunit.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the eukaryotic ribosomal protein eL30 family.</text>
  </comment>
  <dbReference type="EMBL" id="K02928">
    <property type="protein sequence ID" value="AAA03645.1"/>
    <property type="molecule type" value="Unassigned_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK002988">
    <property type="protein sequence ID" value="BAB22500.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC002060">
    <property type="protein sequence ID" value="AAH02060.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC086890">
    <property type="protein sequence ID" value="AAH86890.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS37057.1"/>
  <dbReference type="PIR" id="S11622">
    <property type="entry name" value="R6MS30"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001156957.1">
    <property type="nucleotide sequence ID" value="NM_001163485.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_033109.1">
    <property type="nucleotide sequence ID" value="NM_009083.4"/>
  </dbReference>
  <dbReference type="PDB" id="6SWA">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.10 A"/>
    <property type="chains" value="a=1-115"/>
  </dbReference>
  <dbReference type="PDB" id="7CPU">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.82 A"/>
    <property type="chains" value="Lc=1-115"/>
  </dbReference>
  <dbReference type="PDB" id="7CPV">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.03 A"/>
    <property type="chains" value="Lc=1-115"/>
  </dbReference>
  <dbReference type="PDB" id="7LS1">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.30 A"/>
    <property type="chains" value="W2=1-115"/>
  </dbReference>
  <dbReference type="PDB" id="7LS2">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.10 A"/>
    <property type="chains" value="W2=1-115"/>
  </dbReference>
  <dbReference type="PDBsum" id="6SWA"/>
  <dbReference type="PDBsum" id="7CPU"/>
  <dbReference type="PDBsum" id="7CPV"/>
  <dbReference type="PDBsum" id="7LS1"/>
  <dbReference type="PDBsum" id="7LS2"/>
  <dbReference type="AlphaFoldDB" id="P62889"/>
  <dbReference type="EMDB" id="EMD-10321"/>
  <dbReference type="EMDB" id="EMD-23500"/>
  <dbReference type="EMDB" id="EMD-23501"/>
  <dbReference type="EMDB" id="EMD-30432"/>
  <dbReference type="EMDB" id="EMD-30433"/>
  <dbReference type="SMR" id="P62889"/>
  <dbReference type="BioGRID" id="202978">
    <property type="interactions" value="89"/>
  </dbReference>
  <dbReference type="ComplexPortal" id="CPX-5262">
    <property type="entry name" value="60S cytosolic large ribosomal subunit"/>
  </dbReference>
  <dbReference type="ComplexPortal" id="CPX-7662">
    <property type="entry name" value="60S cytosolic large ribosomal subunit, testis-specific variant"/>
  </dbReference>
  <dbReference type="ComplexPortal" id="CPX-7663">
    <property type="entry name" value="60S cytosolic large ribosomal subunit, striated muscle variant"/>
  </dbReference>
  <dbReference type="IntAct" id="P62889">
    <property type="interactions" value="7"/>
  </dbReference>
  <dbReference type="MINT" id="P62889"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000106014"/>
  <dbReference type="GlyGen" id="P62889">
    <property type="glycosylation" value="1 site, 1 O-linked glycan (1 site)"/>
  </dbReference>
  <dbReference type="iPTMnet" id="P62889"/>
  <dbReference type="MetOSite" id="P62889"/>
  <dbReference type="PhosphoSitePlus" id="P62889"/>
  <dbReference type="SwissPalm" id="P62889"/>
  <dbReference type="jPOST" id="P62889"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000106014"/>
  <dbReference type="PeptideAtlas" id="P62889"/>
  <dbReference type="ProteomicsDB" id="253307"/>
  <dbReference type="Pumba" id="P62889"/>
  <dbReference type="Antibodypedia" id="1242">
    <property type="antibodies" value="261 antibodies from 30 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="19946"/>
  <dbReference type="Ensembl" id="ENSMUST00000009039.6">
    <property type="protein sequence ID" value="ENSMUSP00000009039.6"/>
    <property type="gene ID" value="ENSMUSG00000058600.14"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSMUST00000079735.12">
    <property type="protein sequence ID" value="ENSMUSP00000106014.2"/>
    <property type="gene ID" value="ENSMUSG00000058600.14"/>
  </dbReference>
  <dbReference type="GeneID" id="19946"/>
  <dbReference type="KEGG" id="mmu:19946"/>
  <dbReference type="UCSC" id="uc007vlq.2">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:98037"/>
  <dbReference type="CTD" id="6156"/>
  <dbReference type="MGI" id="MGI:98037">
    <property type="gene designation" value="Rpl30"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000058600"/>
  <dbReference type="eggNOG" id="KOG2988">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000012138"/>
  <dbReference type="HOGENOM" id="CLU_130502_0_1_1"/>
  <dbReference type="InParanoid" id="P62889"/>
  <dbReference type="OMA" id="YFQGGNN"/>
  <dbReference type="OrthoDB" id="374852at2759"/>
  <dbReference type="PhylomeDB" id="P62889"/>
  <dbReference type="TreeFam" id="TF300252"/>
  <dbReference type="Reactome" id="R-MMU-156827">
    <property type="pathway name" value="L13a-mediated translational silencing of Ceruloplasmin expression"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-1799339">
    <property type="pathway name" value="SRP-dependent cotranslational protein targeting to membrane"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-6791226">
    <property type="pathway name" value="Major pathway of rRNA processing in the nucleolus and cytosol"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-72689">
    <property type="pathway name" value="Formation of a pool of free 40S subunits"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-72706">
    <property type="pathway name" value="GTP hydrolysis and joining of the 60S ribosomal subunit"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-975956">
    <property type="pathway name" value="Nonsense Mediated Decay (NMD) independent of the Exon Junction Complex (EJC)"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-975957">
    <property type="pathway name" value="Nonsense Mediated Decay (NMD) enhanced by the Exon Junction Complex (EJC)"/>
  </dbReference>
  <dbReference type="BioGRID-ORCS" id="19946">
    <property type="hits" value="20 hits in 63 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="Rpl30">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P62889"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 15"/>
  </dbReference>
  <dbReference type="RNAct" id="P62889">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000058600">
    <property type="expression patterns" value="Expressed in ectoplacental cone and 201 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P62889">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="ComplexPortal"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0022625">
    <property type="term" value="C:cytosolic large ribosomal subunit"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0098794">
    <property type="term" value="C:postsynapse"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="SynGO"/>
  </dbReference>
  <dbReference type="GO" id="GO:0014069">
    <property type="term" value="C:postsynaptic density"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="SynGO"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005840">
    <property type="term" value="C:ribosome"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="SynGO"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045202">
    <property type="term" value="C:synapse"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="SynGO"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003723">
    <property type="term" value="F:RNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002181">
    <property type="term" value="P:cytoplasmic translation"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="ComplexPortal"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.1330.30:FF:000001">
    <property type="entry name" value="60S ribosomal protein L30"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.1330.30">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00481">
    <property type="entry name" value="Ribosomal_eL30"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000231">
    <property type="entry name" value="Ribosomal_eL30"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039109">
    <property type="entry name" value="Ribosomal_eL30-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029064">
    <property type="entry name" value="Ribosomal_eL30-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022991">
    <property type="entry name" value="Ribosomal_eL30_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004038">
    <property type="entry name" value="Ribosomal_eL8/eL30/eS12/Gad45"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11449:SF1">
    <property type="entry name" value="60S RIBOSOMAL PROTEIN L30"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11449">
    <property type="entry name" value="RIBOSOMAL PROTEIN L30"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01248">
    <property type="entry name" value="Ribosomal_L7Ae"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55315">
    <property type="entry name" value="L30e-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00709">
    <property type="entry name" value="RIBOSOMAL_L30E_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00993">
    <property type="entry name" value="RIBOSOMAL_L30E_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-1017">Isopeptide bond</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <keyword id="KW-0832">Ubl conjugation</keyword>
  <feature type="chain" id="PRO_0000146122" description="Large ribosomal subunit protein eL30">
    <location>
      <begin position="1"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="1">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="1">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine; alternate" evidence="1">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="cross-link" description="Glycyl lysine isopeptide (Lys-Gly) (interchain with G-Cter in SUMO2); alternate" evidence="1">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P62888"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="36517592"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007744" key="4">
    <source>
      <dbReference type="PDB" id="7CPU"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="5">
    <source>
      <dbReference type="PDB" id="7CPV"/>
    </source>
  </evidence>
  <sequence length="115" mass="12784" checksum="95186B081E39748C" modified="2007-01-23" version="2">MVAAKKTKKSLESINSRLQLVMKSGKYVLGYKQTLKMIRQGKAKLVILANNCPALRKSEIEYYAMLAKTGVHHYSGNNIELGTACGKYYRVCTLAIIDPGDSDIIRSMPEQTGEK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>