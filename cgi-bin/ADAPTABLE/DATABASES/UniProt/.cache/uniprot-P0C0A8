<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-08-30" modified="2024-01-24" version="70" xmlns="http://uniprot.org/uniprot">
  <accession>P0C0A8</accession>
  <name>RL333_STAAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Large ribosomal subunit protein bL33C</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>50S ribosomal protein L33 3</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">rpmG3</name>
    <name type="ordered locus">MW0489.1</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="similarity">
    <text evidence="2">Belongs to the bacterial ribosomal protein bL33 family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_001788193.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0C0A8"/>
  <dbReference type="SMR" id="P0C0A8"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990904">
    <property type="term" value="C:ribonucleoprotein complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005840">
    <property type="term" value="C:ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006412">
    <property type="term" value="P:translation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.28.120">
    <property type="entry name" value="Ribosomal protein L33"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00294">
    <property type="entry name" value="Ribosomal_bL33"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001705">
    <property type="entry name" value="Ribosomal_bL33"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018264">
    <property type="entry name" value="Ribosomal_bL33_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038584">
    <property type="entry name" value="Ribosomal_bL33_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011332">
    <property type="entry name" value="Ribosomal_zn-bd"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01023">
    <property type="entry name" value="rpmG_bact"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00471">
    <property type="entry name" value="Ribosomal_L33"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57829">
    <property type="entry name" value="Zn-binding ribosomal proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00582">
    <property type="entry name" value="RIBOSOMAL_L33"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <feature type="chain" id="PRO_0000170228" description="Large ribosomal subunit protein bL33C">
    <location>
      <begin position="1"/>
      <end position="47"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00294"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="47" mass="5375" checksum="137E085F77341FED" modified="2005-08-30" version="1">MRKIPLNCEACGNRNYNVPKQEGSATRLTLKKYCPKCNAHTIHKESK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>