<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-06-01" modified="2024-11-27" version="86" xmlns="http://uniprot.org/uniprot">
  <accession>P35706</accession>
  <name>SSI2_STRLO</name>
  <protein>
    <recommendedName>
      <fullName>Trypsin inhibitor STI2</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">sti2</name>
  </gene>
  <organism>
    <name type="scientific">Streptomyces longisporus</name>
    <dbReference type="NCBI Taxonomy" id="1948"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Kitasatosporales</taxon>
      <taxon>Streptomycetaceae</taxon>
      <taxon>Streptomyces</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="J. Biol. Chem." volume="267" first="3236" last="3241">
      <title>Two novel Streptomyces protein protease inhibitors. Purification, activity, cloning, and expression.</title>
      <authorList>
        <person name="Strickler J.E."/>
        <person name="Berka T.R."/>
        <person name="Gorniak J."/>
        <person name="Fornwald J."/>
        <person name="Keys R."/>
        <person name="Rowland J.J."/>
        <person name="Rosenberg M."/>
        <person name="Taylor D.P."/>
      </authorList>
      <dbReference type="PubMed" id="1737780"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(19)50721-9"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>PARTIAL PROTEIN SEQUENCE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="Biosci. Biotechnol. Biochem." volume="57" first="522" last="524">
      <title>High frequency of SSI-like protease inhibitors among Streptomyces.</title>
      <authorList>
        <person name="Taguchi S."/>
        <person name="Kikuchi H."/>
        <person name="Kojima S."/>
        <person name="Kumagai I."/>
        <person name="Nakase T."/>
        <person name="Miura K."/>
        <person name="Momose H."/>
      </authorList>
      <dbReference type="PubMed" id="7763545"/>
      <dbReference type="DOI" id="10.1271/bbb.57.522"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 35-71</scope>
    <source>
      <strain>4395</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Inhibitory activity against trypsin.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the protease inhibitor I16 (SSI) family.</text>
  </comment>
  <dbReference type="EMBL" id="M80577">
    <property type="protein sequence ID" value="AAA26802.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A42585">
    <property type="entry name" value="A42585"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P35706"/>
  <dbReference type="SMR" id="P35706"/>
  <dbReference type="MEROPS" id="I16.006"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.350.10">
    <property type="entry name" value="Subtilisin inhibitor-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00778">
    <property type="entry name" value="SSI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000691">
    <property type="entry name" value="Prot_inh_I16_SSI"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020054">
    <property type="entry name" value="Prot_inh_SSI_I16_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023549">
    <property type="entry name" value="Subtilisin_inhibitor"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036819">
    <property type="entry name" value="Subtilisin_inhibitor-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00720">
    <property type="entry name" value="SSI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00294">
    <property type="entry name" value="SSBTLNINHBTR"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55399">
    <property type="entry name" value="Subtilisin inhibitor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00999">
    <property type="entry name" value="SSI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000033275" description="Trypsin inhibitor STI2">
    <location>
      <begin position="35"/>
      <end position="144"/>
    </location>
  </feature>
  <feature type="site" description="Reactive bond" evidence="1">
    <location>
      <begin position="104"/>
      <end position="105"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="66"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="102"/>
      <end position="132"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="7763545"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="144" mass="14548" checksum="BBE9A46B67E8F1C6" modified="1994-06-01" version="1" precursor="true">MRNTARWAATLALTATAVCGPLTGAALATPAAAPASLYAPSALVLTVGHGTSAAAASPLRAVTLNCAPTASGTHPAPALACADLRGVGGDIDALKARDGVICNKLYDPVVVTVDGVWQGKRVSYERTFGNECVKNSYGTSLFAF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>