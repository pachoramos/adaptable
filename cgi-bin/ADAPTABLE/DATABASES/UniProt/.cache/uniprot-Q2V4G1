<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-07-28" modified="2024-10-02" version="70" xmlns="http://uniprot.org/uniprot">
  <accession>Q2V4G1</accession>
  <name>DEF63_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Putative defensin-like protein 63</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">At1g59833</name>
    <name type="ORF">F23H11</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Nature" volume="408" first="816" last="820">
      <title>Sequence and analysis of chromosome 1 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
        <person name="Palm C.J."/>
        <person name="Federspiel N.A."/>
        <person name="Kaul S."/>
        <person name="White O."/>
        <person name="Alonso J."/>
        <person name="Altafi H."/>
        <person name="Araujo R."/>
        <person name="Bowman C.L."/>
        <person name="Brooks S.Y."/>
        <person name="Buehler E."/>
        <person name="Chan A."/>
        <person name="Chao Q."/>
        <person name="Chen H."/>
        <person name="Cheuk R.F."/>
        <person name="Chin C.W."/>
        <person name="Chung M.K."/>
        <person name="Conn L."/>
        <person name="Conway A.B."/>
        <person name="Conway A.R."/>
        <person name="Creasy T.H."/>
        <person name="Dewar K."/>
        <person name="Dunn P."/>
        <person name="Etgu P."/>
        <person name="Feldblyum T.V."/>
        <person name="Feng J.-D."/>
        <person name="Fong B."/>
        <person name="Fujii C.Y."/>
        <person name="Gill J.E."/>
        <person name="Goldsmith A.D."/>
        <person name="Haas B."/>
        <person name="Hansen N.F."/>
        <person name="Hughes B."/>
        <person name="Huizar L."/>
        <person name="Hunter J.L."/>
        <person name="Jenkins J."/>
        <person name="Johnson-Hopson C."/>
        <person name="Khan S."/>
        <person name="Khaykin E."/>
        <person name="Kim C.J."/>
        <person name="Koo H.L."/>
        <person name="Kremenetskaia I."/>
        <person name="Kurtz D.B."/>
        <person name="Kwan A."/>
        <person name="Lam B."/>
        <person name="Langin-Hooper S."/>
        <person name="Lee A."/>
        <person name="Lee J.M."/>
        <person name="Lenz C.A."/>
        <person name="Li J.H."/>
        <person name="Li Y.-P."/>
        <person name="Lin X."/>
        <person name="Liu S.X."/>
        <person name="Liu Z.A."/>
        <person name="Luros J.S."/>
        <person name="Maiti R."/>
        <person name="Marziali A."/>
        <person name="Militscher J."/>
        <person name="Miranda M."/>
        <person name="Nguyen M."/>
        <person name="Nierman W.C."/>
        <person name="Osborne B.I."/>
        <person name="Pai G."/>
        <person name="Peterson J."/>
        <person name="Pham P.K."/>
        <person name="Rizzo M."/>
        <person name="Rooney T."/>
        <person name="Rowley D."/>
        <person name="Sakano H."/>
        <person name="Salzberg S.L."/>
        <person name="Schwartz J.R."/>
        <person name="Shinn P."/>
        <person name="Southwick A.M."/>
        <person name="Sun H."/>
        <person name="Tallon L.J."/>
        <person name="Tambunga G."/>
        <person name="Toriumi M.J."/>
        <person name="Town C.D."/>
        <person name="Utterback T."/>
        <person name="Van Aken S."/>
        <person name="Vaysberg M."/>
        <person name="Vysotskaia V.S."/>
        <person name="Walker M."/>
        <person name="Wu D."/>
        <person name="Yu G."/>
        <person name="Fraser C.M."/>
        <person name="Venter J.C."/>
        <person name="Davis R.W."/>
      </authorList>
      <dbReference type="PubMed" id="11130712"/>
      <dbReference type="DOI" id="10.1038/35048500"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="EMBL" id="AC007258">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002684">
    <property type="protein sequence ID" value="AEE33624.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001031209.1">
    <property type="nucleotide sequence ID" value="NM_001036132.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q2V4G1"/>
  <dbReference type="SMR" id="Q2V4G1"/>
  <dbReference type="PaxDb" id="3702-AT1G59833.1"/>
  <dbReference type="EnsemblPlants" id="AT1G59833.1">
    <property type="protein sequence ID" value="AT1G59833.1"/>
    <property type="gene ID" value="AT1G59833"/>
  </dbReference>
  <dbReference type="GeneID" id="3767572"/>
  <dbReference type="Gramene" id="AT1G59833.1">
    <property type="protein sequence ID" value="AT1G59833.1"/>
    <property type="gene ID" value="AT1G59833"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT1G59833"/>
  <dbReference type="Araport" id="AT1G59833"/>
  <dbReference type="TAIR" id="AT1G59833"/>
  <dbReference type="HOGENOM" id="CLU_165205_0_0_1"/>
  <dbReference type="InParanoid" id="Q2V4G1"/>
  <dbReference type="OrthoDB" id="690134at2759"/>
  <dbReference type="PhylomeDB" id="Q2V4G1"/>
  <dbReference type="PRO" id="PR:Q2V4G1"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q2V4G1">
    <property type="expression patterns" value="baseline"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000379642" description="Putative defensin-like protein 63">
    <location>
      <begin position="22"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="40"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="53"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="57"/>
      <end position="80"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="84" mass="9581" checksum="B04C9454B9125ABF" modified="2006-01-10" version="1" precursor="true">MDIRKTYVIIFFVGILTISFSSYNIGQTRLVVIQEQEPHCIGPCEIAFGNRPCNEECTSQQYAYGFCDYQTKGEDNPQCCCYTS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>