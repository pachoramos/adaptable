<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1991-02-01" modified="2024-11-27" version="103" xmlns="http://uniprot.org/uniprot">
  <accession>P20639</accession>
  <name>PG041_VACCC</name>
  <protein>
    <recommendedName>
      <fullName>Protein K3</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">OPG041</name>
    <name type="ORF">K3L</name>
  </gene>
  <organism>
    <name type="scientific">Vaccinia virus (strain Copenhagen)</name>
    <name type="common">VACV</name>
    <dbReference type="NCBI Taxonomy" id="10249"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Varidnaviria</taxon>
      <taxon>Bamfordvirae</taxon>
      <taxon>Nucleocytoviricota</taxon>
      <taxon>Pokkesviricetes</taxon>
      <taxon>Chitovirales</taxon>
      <taxon>Poxviridae</taxon>
      <taxon>Chordopoxvirinae</taxon>
      <taxon>Orthopoxvirus</taxon>
      <taxon>Vaccinia virus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1990" name="Virology" volume="179" first="247" last="266">
      <title>The complete DNA sequence of vaccinia virus.</title>
      <authorList>
        <person name="Goebel S.J."/>
        <person name="Johnson G.P."/>
        <person name="Perkus M.E."/>
        <person name="Davis S.W."/>
        <person name="Winslow J.P."/>
        <person name="Paoletti E."/>
      </authorList>
      <dbReference type="PubMed" id="2219722"/>
      <dbReference type="DOI" id="10.1016/0042-6822(90)90294-2"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1990" name="Virology" volume="179" first="517" last="563">
      <title>Appendix to 'The complete DNA sequence of vaccinia virus'.</title>
      <authorList>
        <person name="Goebel S.J."/>
        <person name="Johnson G.P."/>
        <person name="Perkus M.E."/>
        <person name="Davis S.W."/>
        <person name="Winslow J.P."/>
        <person name="Paoletti E."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1991" name="Virology" volume="183" first="419" last="422">
      <title>Vaccinia virus-encoded eIF-2 alpha homolog abrogates the antiviral effect of interferon.</title>
      <authorList>
        <person name="Beattie E."/>
        <person name="Tartaglia J."/>
        <person name="Paoletti E."/>
      </authorList>
      <dbReference type="PubMed" id="1711259"/>
      <dbReference type="DOI" id="10.1016/0042-6822(91)90158-8"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Viral mimic of eIF-2-alpha that acts as a pseudosubstrate for EIF2AK2/PKR kinase. Inhibits therefore eIF-2-alpha phosphorylation by host EIF2AK2/PKR kinase and prevents protein synthesis shutoff. Determinant of host species specificity.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with host EIF2AK2/PKR kinase.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-8674942">
      <id>P20639</id>
    </interactant>
    <interactant intactId="EBI-640775">
      <id>P19525</id>
      <label>EIF2AK2</label>
    </interactant>
    <organismsDiffer>true</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="domain">
    <text evidence="2">The C-terminus is involved in the inhibition of host EIF2AK2/PKR kinase.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the orthopoxvirus OPG041 family.</text>
  </comment>
  <dbReference type="EMBL" id="M35027">
    <property type="protein sequence ID" value="AAA48009.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="B42505">
    <property type="entry name" value="B42505"/>
  </dbReference>
  <dbReference type="SMR" id="P20639"/>
  <dbReference type="DIP" id="DIP-57736N"/>
  <dbReference type="IntAct" id="P20639">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="MINT" id="P20639"/>
  <dbReference type="Proteomes" id="UP000008269">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030414">
    <property type="term" value="F:peptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030291">
    <property type="term" value="F:protein serine/threonine kinase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003723">
    <property type="term" value="F:RNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039580">
    <property type="term" value="P:symbiont-mediated suppression of host PKR/eIFalpha signaling"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039502">
    <property type="term" value="P:symbiont-mediated suppression of host type I interferon-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.140">
    <property type="entry name" value="Nucleic acid-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016397">
    <property type="entry name" value="K3-like_poxvir"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012340">
    <property type="entry name" value="NA-bd_OB-fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003029">
    <property type="entry name" value="S1_domain"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00575">
    <property type="entry name" value="S1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF003760">
    <property type="entry name" value="VAC_K3L_Serpin_prd"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00316">
    <property type="entry name" value="S1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50249">
    <property type="entry name" value="Nucleic acid-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1114">Inhibition of host interferon signaling pathway by virus</keyword>
  <keyword id="KW-1102">Inhibition of host PKR by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <feature type="chain" id="PRO_0000099598" description="Protein K3">
    <location>
      <begin position="1"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="domain" description="S1 motif">
    <location>
      <begin position="8"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="region of interest" description="Binding to host EIF2AK2/PKR" evidence="2">
    <location>
      <begin position="43"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="region of interest" description="Binding to host EIF2AK2/PKR" evidence="2">
    <location>
      <begin position="74"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="site" description="Involved in host species specificity" evidence="2">
    <location>
      <position position="45"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P18378"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="88" mass="10496" checksum="196AD69CD94AE4F0" modified="1991-02-01" version="1">MLAFCYSLPNAGDVIKGRVYEKDYALYIYLFDYPHSEAILAESVKMHMDRYVEYRDKLVGKTVKVKVIRVDYTKGYIDVNYKRMCRHQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>