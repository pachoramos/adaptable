<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-03-03" modified="2024-03-27" version="40" xmlns="http://uniprot.org/uniprot">
  <accession>Q5VJS9</accession>
  <name>NDB2T_OPICA</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Opistoporin-4</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Opistophthalmus carinatus</name>
    <name type="common">African yellow leg scorpion</name>
    <dbReference type="NCBI Taxonomy" id="190115"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Iurida</taxon>
      <taxon>Scorpionoidea</taxon>
      <taxon>Scorpionidae</taxon>
      <taxon>Opistophthalminae</taxon>
      <taxon>Opistophthalmus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2003-10" db="EMBL/GenBank/DDBJ databases">
      <title>Precursor organization and gene structure of scorpion venom antimicrobial peptides.</title>
      <authorList>
        <person name="Zhu S."/>
        <person name="Tytgat J."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">At high concentrations, acts as a pore former in cellular membranes and causes the leakage of the cells. At submicromolar concentrations, degranulates granulocytes and has a weak hemolytic activity against human erythrocytes. Also strongly inhibits the production of superoxide anions. Has a strong antibacterial activity against Gram-negative bacteria but is less active against Gram-positive bacteria. Also has antifungal activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="1">Forms a helical membrane channel in the prey.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the non-disulfide-bridged peptide (NDBP) superfamily. Long chain multifunctional peptide (group 2) family.</text>
  </comment>
  <dbReference type="EMBL" id="AY427949">
    <property type="protein sequence ID" value="AAQ94361.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5VJS9"/>
  <dbReference type="SMR" id="Q5VJS9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044179">
    <property type="term" value="P:hemolysis in another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006811">
    <property type="term" value="P:monoatomic ion transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012526">
    <property type="entry name" value="Antimicrobial_7"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08102">
    <property type="entry name" value="Antimicrobial_7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_5000092331" description="Opistoporin-4" evidence="4">
    <location>
      <begin position="1"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000366113" evidence="1">
    <location>
      <begin position="45"/>
      <end position="61"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P83313"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source ref="1"/>
  </evidence>
  <sequence length="61" mass="6886" checksum="4E0D1276D1B92D7B" modified="2004-12-07" version="1" precursor="true">GKVWDWIKKTAKDVLNSDVAKQLKNKALNAAKNFVAEKIGATPSEAGQMPFDEFMDILHYY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>