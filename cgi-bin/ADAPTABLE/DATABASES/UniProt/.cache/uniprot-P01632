<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="112" xmlns="http://uniprot.org/uniprot">
  <accession>P01632</accession>
  <name>KV1A1_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Ig kappa chain V-I region S107A</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">Igkv7-33</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1981" name="J. Exp. Med." volume="153" first="1366" last="1370">
      <title>Nucleic acid and protein sequences of phosphocholine-binding light chains.</title>
      <authorList>
        <person name="Kwan S.-P."/>
        <person name="Rudikoff S."/>
        <person name="Seidman J.G."/>
        <person name="Leder P."/>
        <person name="Scharff M.D."/>
      </authorList>
      <dbReference type="PubMed" id="6788890"/>
      <dbReference type="DOI" id="10.1084/jem.153.5.1366"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1988" name="J. Immunol." volume="141" first="4012" last="4019">
      <title>Genetics of the phosphocholine-specific antibody response to Streptococcus pneumoniae. Germ-line but not mutated T15 antibodies are dominantly selected.</title>
      <authorList>
        <person name="Claflin J.L."/>
        <person name="Berry J."/>
      </authorList>
      <dbReference type="PubMed" id="3141511"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] OF 1-113</scope>
  </reference>
  <comment type="function">
    <text>Anti-phosphocholine antibody.</text>
  </comment>
  <dbReference type="EMBL" id="U29423">
    <property type="protein sequence ID" value="AAC00033.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A01915">
    <property type="entry name" value="KVMS7A"/>
  </dbReference>
  <dbReference type="PIR" id="A30534">
    <property type="entry name" value="A30534"/>
  </dbReference>
  <dbReference type="PIR" id="C30534">
    <property type="entry name" value="C30534"/>
  </dbReference>
  <dbReference type="PIR" id="F30534">
    <property type="entry name" value="F30534"/>
  </dbReference>
  <dbReference type="PIR" id="H30534">
    <property type="entry name" value="H30534"/>
  </dbReference>
  <dbReference type="PIR" id="I30534">
    <property type="entry name" value="I30534"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01632"/>
  <dbReference type="EMDB" id="EMD-0897"/>
  <dbReference type="EMDB" id="EMD-22699"/>
  <dbReference type="EMDB" id="EMD-25487"/>
  <dbReference type="EMDB" id="EMD-25488"/>
  <dbReference type="EMDB" id="EMD-32676"/>
  <dbReference type="EMDB" id="EMD-32678"/>
  <dbReference type="EMDB" id="EMD-33994"/>
  <dbReference type="EMDB" id="EMD-34231"/>
  <dbReference type="EMDB" id="EMD-34232"/>
  <dbReference type="EMDB" id="EMD-34233"/>
  <dbReference type="EMDB" id="EMD-34234"/>
  <dbReference type="EMDB" id="EMD-7136"/>
  <dbReference type="EMDB" id="EMD-9634"/>
  <dbReference type="SMR" id="P01632"/>
  <dbReference type="ProteomicsDB" id="263682"/>
  <dbReference type="AGR" id="MGI:3577282"/>
  <dbReference type="MGI" id="MGI:3577282">
    <property type="gene designation" value="Igkv7-33"/>
  </dbReference>
  <dbReference type="InParanoid" id="P01632"/>
  <dbReference type="PRO" id="PR:P01632"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="RNAct" id="P01632">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019814">
    <property type="term" value="C:immunoglobulin complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002250">
    <property type="term" value="P:adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd04980">
    <property type="entry name" value="IgV_L_kappa"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.40.10:FF:001378">
    <property type="entry name" value="Immunoglobulin kappa variable 4-1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003599">
    <property type="entry name" value="Ig_sub"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013106">
    <property type="entry name" value="Ig_V-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050150">
    <property type="entry name" value="IgV_Light_Chain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23267:SF425">
    <property type="entry name" value="IMMUNOGLOBULIN KAPPA VARIABLE 4-1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23267">
    <property type="entry name" value="IMMUNOGLOBULIN LIGHT CHAIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07686">
    <property type="entry name" value="V-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00409">
    <property type="entry name" value="IG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00406">
    <property type="entry name" value="IGv"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-1280">Immunoglobulin</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000059770" description="Ig kappa chain V-I region S107A">
    <location>
      <begin position="1"/>
      <end position="114" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-1">
    <location>
      <begin position="24"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-2">
    <location>
      <begin position="41"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-2">
    <location>
      <begin position="56"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-3">
    <location>
      <begin position="63"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-3">
    <location>
      <begin position="95"/>
      <end position="103"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-4">
    <location>
      <begin position="104"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="23"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="114"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00114"/>
    </source>
  </evidence>
  <sequence length="114" mass="12717" checksum="32008EC8E9DBE67B" modified="1986-07-21" version="1">DIVMTQSPTFLAVTASKKVTISCTASESLYSSKHKVHYLAWYQKKPEQSPKLLIYGASNRYIGVPDRFTGSGSGTDFTLTISSVQVEDLTHYYCAQFYSYPLTFGAGTKLELKR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>