<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-09-19" modified="2023-06-28" version="58" xmlns="http://uniprot.org/uniprot">
  <accession>P83620</accession>
  <name>TXC1B_CUPSA</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Cupiennin-1b</fullName>
      <shortName evidence="3">Cu-1b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>M-ctenitoxin-Cs1b</fullName>
      <shortName>M-CNTX-Cs1b</shortName>
    </alternativeName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Cupiennius salei</name>
    <name type="common">American wandering spider</name>
    <dbReference type="NCBI Taxonomy" id="6928"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Lycosoidea</taxon>
      <taxon>Ctenidae</taxon>
      <taxon>Cupiennius</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="J. Biol. Chem." volume="277" first="11208" last="11216">
      <title>Cupiennin 1, a new family of highly basic antimicrobial peptides in the venom of the spider Cupiennius salei (Ctenidae).</title>
      <authorList>
        <person name="Kuhn-Nentwig L."/>
        <person name="Mueller J."/>
        <person name="Schaller J."/>
        <person name="Walz A."/>
        <person name="Dathe M."/>
        <person name="Nentwig W."/>
      </authorList>
      <dbReference type="PubMed" id="11792701"/>
      <dbReference type="DOI" id="10.1074/jbc.m111099200"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>TOXIC DOSE</scope>
    <scope>AMIDATION AT GLU-35</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Has antimicrobial activity against E.coli, E.faecalis, P.aeruginosa, and S.aureus. Has insecticidal and hemolytic activities. Probably acts by disturbing membrane function with its amphipathic structure.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="3800.25" error="0.28" method="Electrospray" evidence="1"/>
  <comment type="toxic dose">
    <text evidence="1">LD(50) is 4.7 pmol/mg on Drosophila.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the cationic peptide 04 (cupiennin) family. 01 subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P83620"/>
  <dbReference type="SMR" id="P83620"/>
  <dbReference type="ArachnoServer" id="AS000290">
    <property type="toxin name" value="M-ctenitoxin-Cs1b"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035164">
    <property type="entry name" value="Cupiennin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17563">
    <property type="entry name" value="Cu"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000045039" description="Cupiennin-1b">
    <location>
      <begin position="1"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glutamic acid 1-amide" evidence="1">
    <location>
      <position position="35"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11792701"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="11792701"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="35" mass="3802" checksum="AE1660038BA72A04" modified="2003-09-19" version="1">GFGSLFKFLAKKVAKTVAKQAAKQGAKYIANKQME</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>