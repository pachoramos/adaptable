<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-06-07" modified="2022-12-14" version="28" xmlns="http://uniprot.org/uniprot">
  <accession>G1CWH5</accession>
  <name>CYC7_CLITE</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Cliotide T7</fullName>
    </recommendedName>
  </protein>
  <organism evidence="6">
    <name type="scientific">Clitoria ternatea</name>
    <name type="common">Butterfly pea</name>
    <dbReference type="NCBI Taxonomy" id="43366"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Fabales</taxon>
      <taxon>Fabaceae</taxon>
      <taxon>Papilionoideae</taxon>
      <taxon>50 kb inversion clade</taxon>
      <taxon>NPAAA clade</taxon>
      <taxon>indigoferoid/millettioid clade</taxon>
      <taxon>Phaseoleae</taxon>
      <taxon>Clitoria</taxon>
    </lineage>
  </organism>
  <reference evidence="6" key="1">
    <citation type="journal article" date="2011" name="J. Biol. Chem." volume="286" first="24275" last="24287">
      <title>Discovery and characterization of novel cyclotides originated from chimeric precursors consisting of albumin-1 chain a and cyclotide domains in the fabaceae family.</title>
      <authorList>
        <person name="Nguyen G.K."/>
        <person name="Zhang S."/>
        <person name="Nguyen N.T."/>
        <person name="Nguyen P.Q."/>
        <person name="Chiu M.S."/>
        <person name="Hardjojo A."/>
        <person name="Tam J.P."/>
      </authorList>
      <dbReference type="PubMed" id="21596752"/>
      <dbReference type="DOI" id="10.1074/jbc.m111.229922"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 26-56</scope>
    <scope>PRESENCE OF DISULFIDE BONDS</scope>
    <scope>CYCLIZATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2">Probably participates in a plant defense mechanism.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed in root, seed and nodule but not in flower, stem, shoot, leaf and pod (at protein level).</text>
  </comment>
  <comment type="domain">
    <text evidence="4">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">Contains 3 disulfide bonds.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2 3">This is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="3226.0" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the cyclotide family. Bracelet subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="JF931993">
    <property type="protein sequence ID" value="AEK26407.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="G1CWH5"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR032000">
    <property type="entry name" value="Albumin_I_a"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012323">
    <property type="entry name" value="Cyclotide_bracelet_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF16720">
    <property type="entry name" value="Albumin_I_a"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60008">
    <property type="entry name" value="CYCLOTIDE_BRACELET"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000440058" description="Cliotide T7" evidence="3">
    <location>
      <begin position="26"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000440059" description="Removed in mature form" evidence="5">
    <location>
      <begin position="57"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="29"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="33"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="38"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Gly-Asn)" evidence="3">
    <location>
      <begin position="26"/>
      <end position="56"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="21596752"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="21596752"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="AEK26407.1"/>
    </source>
  </evidence>
  <sequence length="130" mass="14256" checksum="7858837F7101FF66" modified="2011-10-19" version="1" precursor="true">MAFARLALIFFLAASVMFAVKETEAGIPCGESCVFIPCTVTALLGCSCKDKVCYKNHVIAAEANTVNDHHLLCQSHEDCFKKGTGNFCAPSLKHDVKYGWCFRAESEGFLLKDFLKTPVDILKMSNVIGN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>