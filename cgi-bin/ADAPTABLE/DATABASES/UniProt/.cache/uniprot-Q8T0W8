<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-11-15" modified="2023-11-08" version="69" xmlns="http://uniprot.org/uniprot">
  <accession>Q8T0W8</accession>
  <name>DIAP_GASAT</name>
  <protein>
    <recommendedName>
      <fullName>Diapause-specific peptide</fullName>
      <shortName>DSP</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Diapausin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Gastrophysa atrocyanea</name>
    <name type="common">Leaf beetle</name>
    <dbReference type="NCBI Taxonomy" id="169758"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Coleoptera</taxon>
      <taxon>Polyphaga</taxon>
      <taxon>Cucujiformia</taxon>
      <taxon>Chrysomeloidea</taxon>
      <taxon>Chrysomelidae</taxon>
      <taxon>Chrysomelinae</taxon>
      <taxon>Chrysomelini</taxon>
      <taxon>Gastrophysa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Peptides" volume="24" first="1327" last="1333">
      <title>Insect diapause-specific peptide from the leaf beetle has consensus with a putative iridovirus peptide.</title>
      <authorList>
        <person name="Tanaka H."/>
        <person name="Sato K."/>
        <person name="Saito Y."/>
        <person name="Yamashita T."/>
        <person name="Agoh M."/>
        <person name="Okunishi J."/>
        <person name="Tachikawa E."/>
        <person name="Suzuki K."/>
      </authorList>
      <dbReference type="PubMed" id="14706547"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2003.07.021"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 25-65</scope>
    <scope>FUNCTION</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>DISULFIDE BOND</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="Appl. Entomol. Zool. (Jpn.)" volume="33" first="535" last="543">
      <title>Relationship between the introduced and indigenous parasitoids Torymus sinensis and T. beneficus (Hymenoptera: Torymidae), as inferred from mt-DNA (COI) sequences.</title>
      <authorList>
        <person name="Tanaka H."/>
        <person name="Sudo C."/>
        <person name="An Y."/>
        <person name="Yamashita T."/>
        <person name="Sato K."/>
        <person name="Kurihara M."/>
        <person name="Suzuki K."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE OF 25-60</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2005" name="J. Insect Physiol." volume="51" first="701" last="707">
      <title>Expression profiling of a diapause-specific peptide (DSP) of the leaf beetle Gastrophysa atrocyanea and silencing of DSP by double-strand RNA.</title>
      <authorList>
        <person name="Tanaka H."/>
        <person name="Suzuki K."/>
      </authorList>
      <dbReference type="PubMed" id="15936770"/>
      <dbReference type="DOI" id="10.1016/j.jinsphys.2005.03.018"/>
    </citation>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2007" name="Biochemistry" volume="46" first="13733" last="13741">
      <title>The structure of a novel insect peptide explains its Ca(2+) channel blocking and antifungal activities.</title>
      <authorList>
        <person name="Kouno T."/>
        <person name="Mizuguchi M."/>
        <person name="Tanaka H."/>
        <person name="Yang P."/>
        <person name="Mori Y."/>
        <person name="Shinoda H."/>
        <person name="Unoki K."/>
        <person name="Aizawa T."/>
        <person name="Demura M."/>
        <person name="Suzuki K."/>
        <person name="Kawano K."/>
      </authorList>
      <dbReference type="PubMed" id="17994764"/>
      <dbReference type="DOI" id="10.1021/bi701319t"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 25-65</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has antifungal activity against T.rubrum. Blocks voltage-dependent N-type calcium channels (Cav2.2 / CACNA1B).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Highly expressed in the fat body.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="1 2">Produced throughout adult diapause, and to a minor extent in pupae, but not in eggs, larvae, or post-diapausing adults. Inhibition of DSP expression does not affect the onset or maintenance of diapause, indicating that the expression of DSP accompanies but does not play a direct role in the induction or maintenance of diapause.</text>
  </comment>
  <comment type="mass spectrometry" mass="4466.3" method="MALDI" evidence="4"/>
  <comment type="similarity">
    <text evidence="5">Belongs to the diapausin family.</text>
  </comment>
  <dbReference type="EMBL" id="AB070558">
    <property type="protein sequence ID" value="BAB88222.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="2CDX">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=25-65"/>
  </dbReference>
  <dbReference type="PDB" id="2E2F">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=25-65"/>
  </dbReference>
  <dbReference type="PDBsum" id="2CDX"/>
  <dbReference type="PDBsum" id="2E2F"/>
  <dbReference type="AlphaFoldDB" id="Q8T0W8"/>
  <dbReference type="BMRB" id="Q8T0W8"/>
  <dbReference type="SMR" id="Q8T0W8"/>
  <dbReference type="EvolutionaryTrace" id="Q8T0W8"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019855">
    <property type="term" value="F:calcium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019732">
    <property type="term" value="P:antifungal humoral response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.120">
    <property type="entry name" value="Diapause-specific peptide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038203">
    <property type="entry name" value="Diapausin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08036">
    <property type="entry name" value="Antimicrobial_6"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0108">Calcium channel impairing toxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1218">Voltage-gated calcium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="1 4">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000007217" description="Diapause-specific peptide">
    <location>
      <begin position="25"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="31"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="35"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="46"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="31"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="35"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="40"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="54"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="61"/>
      <end position="64"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="14706547"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15936770"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="17994764"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="2E2F"/>
    </source>
  </evidence>
  <sequence length="65" mass="6939" checksum="72BC929D4AA31A42" modified="2002-06-01" version="1" precursor="true">MGAALKMTIFLLIVACAMIATTEAAVRIGPCDQVCPRIVPERHECCRAHGRSGYAYCSGGGMYCN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>