<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-05-08" modified="2024-03-27" version="8" xmlns="http://uniprot.org/uniprot">
  <accession>P0DSJ9</accession>
  <name>TX15B_MYRGU</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">U-myrmeciitoxin(01)-Mg5b</fullName>
      <shortName evidence="3">MIITX(01)-Mg5b</shortName>
      <shortName evidence="4">U-MIITX(01)-Mg5b</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Myrmecia gulosa</name>
    <name type="common">Red bulldog ant</name>
    <dbReference type="NCBI Taxonomy" id="36170"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Myrmeciinae</taxon>
      <taxon>Myrmeciini</taxon>
      <taxon>Myrmecia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2018" name="Sci. Adv." volume="4" first="EAAU4640" last="EAAU4640">
      <title>A comprehensive portrait of the venom of the giant red bull ant, Myrmecia gulosa, reveals a hyperdiverse hymenopteran toxin gene family.</title>
      <authorList>
        <person name="Robinson S.D."/>
        <person name="Mueller A."/>
        <person name="Clayton D."/>
        <person name="Starobova H."/>
        <person name="Hamilton B.R."/>
        <person name="Payne R.J."/>
        <person name="Vetter I."/>
        <person name="King G.F."/>
        <person name="Undheim E.A.B."/>
      </authorList>
      <dbReference type="PubMed" id="30214940"/>
      <dbReference type="DOI" id="10.1126/sciadv.aau4640"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="4">May have antimicrobial properties, like most ant linear peptides.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="2515.42" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the formicidae venom precursor-01 superfamily.</text>
  </comment>
  <comment type="online information" name="National Center for Biotechnology Information (NCBI)">
    <link uri="https://www.ncbi.nlm.nih.gov/nuccore/GGFG01000007"/>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DSJ9"/>
  <dbReference type="SMR" id="P0DSJ9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR049518">
    <property type="entry name" value="Pilosulin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17499">
    <property type="entry name" value="Pilosulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000447079" evidence="5">
    <location>
      <begin position="22"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000447080" description="U-myrmeciitoxin(01)-Mg5b" evidence="2">
    <location>
      <begin position="39"/>
      <end position="59"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="30214940"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="30214940"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="30214940"/>
    </source>
  </evidence>
  <sequence length="59" mass="6515" checksum="A1AD8AADC78410B4" modified="2019-05-08" version="1" precursor="true">MRLSYLSLALAIIFVLTIMHASNVEAKASADPEPDAVGSINVKNLMNMIREQITSRLKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>