<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-05-10" modified="2024-11-27" version="112" xmlns="http://uniprot.org/uniprot">
  <accession>O93728</accession>
  <name>OGT_PYRAE</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Methylated-DNA--protein-cysteine methyltransferase</fullName>
      <ecNumber evidence="1">2.1.1.63</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">6-O-methylguanine-DNA methyltransferase</fullName>
      <shortName evidence="1">MGMT</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">O-6-methylguanine-DNA-alkyltransferase</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ogt</name>
    <name type="ordered locus">PAE2012</name>
  </gene>
  <organism>
    <name type="scientific">Pyrobaculum aerophilum (strain ATCC 51768 / DSM 7523 / JCM 9630 / CIP 104966 / NBRC 100827 / IM2)</name>
    <dbReference type="NCBI Taxonomy" id="178306"/>
    <lineage>
      <taxon>Archaea</taxon>
      <taxon>Thermoproteota</taxon>
      <taxon>Thermoprotei</taxon>
      <taxon>Thermoproteales</taxon>
      <taxon>Thermoproteaceae</taxon>
      <taxon>Pyrobaculum</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2000-03" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Slupska M.M."/>
        <person name="Pedriquez L."/>
        <person name="Miller J.H."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 51768 / DSM 7523 / JCM 9630 / CIP 104966 / NBRC 100827 / IM2</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2002" name="Proc. Natl. Acad. Sci. U.S.A." volume="99" first="984" last="989">
      <title>Genome sequence of the hyperthermophilic crenarchaeon Pyrobaculum aerophilum.</title>
      <authorList>
        <person name="Fitz-Gibbon S.T."/>
        <person name="Ladner H."/>
        <person name="Kim U.-J."/>
        <person name="Stetter K.O."/>
        <person name="Simon M.I."/>
        <person name="Miller J.H."/>
      </authorList>
      <dbReference type="PubMed" id="11792869"/>
      <dbReference type="DOI" id="10.1073/pnas.241636498"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 51768 / DSM 7523 / JCM 9630 / CIP 104966 / NBRC 100827 / IM2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Involved in the cellular defense against the biological effects of O6-methylguanine (O6-MeG) and O4-methylthymine (O4-MeT) in DNA. Repairs the methylated nucleobase in DNA by stoichiometrically transferring the methyl group to a cysteine residue in the enzyme. This is a suicide reaction: the enzyme is irreversibly inactivated.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>a 6-O-methyl-2'-deoxyguanosine in DNA + L-cysteinyl-[protein] = S-methyl-L-cysteinyl-[protein] + a 2'-deoxyguanosine in DNA</text>
      <dbReference type="Rhea" id="RHEA:24000"/>
      <dbReference type="Rhea" id="RHEA-COMP:10131"/>
      <dbReference type="Rhea" id="RHEA-COMP:10132"/>
      <dbReference type="Rhea" id="RHEA-COMP:11367"/>
      <dbReference type="Rhea" id="RHEA-COMP:11368"/>
      <dbReference type="ChEBI" id="CHEBI:29950"/>
      <dbReference type="ChEBI" id="CHEBI:82612"/>
      <dbReference type="ChEBI" id="CHEBI:85445"/>
      <dbReference type="ChEBI" id="CHEBI:85448"/>
      <dbReference type="EC" id="2.1.1.63"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>a 4-O-methyl-thymidine in DNA + L-cysteinyl-[protein] = a thymidine in DNA + S-methyl-L-cysteinyl-[protein]</text>
      <dbReference type="Rhea" id="RHEA:53428"/>
      <dbReference type="Rhea" id="RHEA-COMP:10131"/>
      <dbReference type="Rhea" id="RHEA-COMP:10132"/>
      <dbReference type="Rhea" id="RHEA-COMP:13555"/>
      <dbReference type="Rhea" id="RHEA-COMP:13556"/>
      <dbReference type="ChEBI" id="CHEBI:29950"/>
      <dbReference type="ChEBI" id="CHEBI:82612"/>
      <dbReference type="ChEBI" id="CHEBI:137386"/>
      <dbReference type="ChEBI" id="CHEBI:137387"/>
      <dbReference type="EC" id="2.1.1.63"/>
    </reaction>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">This enzyme catalyzes only one turnover and therefore is not strictly catalytic. According to one definition, an enzyme is a biocatalyst that acts repeatedly and over many reaction cycles.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the MGMT family.</text>
  </comment>
  <dbReference type="EC" id="2.1.1.63" evidence="1"/>
  <dbReference type="EMBL" id="U82384">
    <property type="protein sequence ID" value="AAD09231.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AE009441">
    <property type="protein sequence ID" value="AAL63882.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O93728"/>
  <dbReference type="SMR" id="O93728"/>
  <dbReference type="STRING" id="178306.PAE2012"/>
  <dbReference type="EnsemblBacteria" id="AAL63882">
    <property type="protein sequence ID" value="AAL63882"/>
    <property type="gene ID" value="PAE2012"/>
  </dbReference>
  <dbReference type="KEGG" id="pai:PAE2012"/>
  <dbReference type="PATRIC" id="fig|178306.9.peg.1487"/>
  <dbReference type="eggNOG" id="arCOG02724">
    <property type="taxonomic scope" value="Archaea"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_1754770_0_0_2"/>
  <dbReference type="InParanoid" id="O93728"/>
  <dbReference type="Proteomes" id="UP000002439">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003908">
    <property type="term" value="F:methylated-DNA-[protein]-cysteine S-methyltransferase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006281">
    <property type="term" value="P:DNA repair"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032259">
    <property type="term" value="P:methylation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd06445">
    <property type="entry name" value="ATase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.10.10">
    <property type="entry name" value="Winged helix-like DNA-binding domain superfamily/Winged helix DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001497">
    <property type="entry name" value="MethylDNA_cys_MeTrfase_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR014048">
    <property type="entry name" value="MethylDNA_cys_MeTrfase_DNA-bd"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036217">
    <property type="entry name" value="MethylDNA_cys_MeTrfase_DNAb"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036388">
    <property type="entry name" value="WH-like_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00589">
    <property type="entry name" value="ogt"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46460">
    <property type="entry name" value="METHYLATED-DNA--PROTEIN-CYSTEINE METHYLTRANSFERASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46460:SF1">
    <property type="entry name" value="METHYLATED-DNA--PROTEIN-CYSTEINE METHYLTRANSFERASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01035">
    <property type="entry name" value="DNA_binding_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46767">
    <property type="entry name" value="Methylated DNA-protein cysteine methyltransferase, C-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00374">
    <property type="entry name" value="MGMT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0227">DNA damage</keyword>
  <keyword id="KW-0234">DNA repair</keyword>
  <keyword id="KW-0489">Methyltransferase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0808">Transferase</keyword>
  <feature type="chain" id="PRO_0000139382" description="Methylated-DNA--protein-cysteine methyltransferase">
    <location>
      <begin position="1"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="active site" description="Nucleophile; methyl group acceptor" evidence="1">
    <location>
      <position position="90"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="O74023"/>
    </source>
  </evidence>
  <sequence length="148" mass="16456" checksum="53E9E4348002E6BD" modified="2000-10-01" version="2">MICSQYGPVVVGWDGSKTFVNPSRCSKRVGLAEFLELINIDEIDKRLLYLLGIPRGYVTTYKLYAEVLGTSPRHVGWLMARNPLPVILPCHRVVKSDFSLGGYTGGVEVKKKLLAYEGALCGDRPCRVVRPRMIDDVRDALFKSLGLA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>