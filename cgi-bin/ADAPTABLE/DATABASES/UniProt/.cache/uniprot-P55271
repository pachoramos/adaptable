<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-10-01" modified="2024-11-27" version="186" xmlns="http://uniprot.org/uniprot">
  <accession>P55271</accession>
  <name>CDN2B_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Cyclin-dependent kinase 4 inhibitor B</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>p14-INK4b</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>p15-INK4b</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Cdkn2b</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Oncogene" volume="11" first="635" last="645">
      <title>Cloning and characterization of murine p16INK4a and p15INK4b genes.</title>
      <authorList>
        <person name="Quelle D.E."/>
        <person name="Ashmun R.A."/>
        <person name="Hannon G.J."/>
        <person name="Rehberger P.A."/>
        <person name="Trono D."/>
        <person name="Richter K.H."/>
        <person name="Walker C."/>
        <person name="Beach D."/>
        <person name="Sherr C.J."/>
        <person name="Serrano M."/>
      </authorList>
      <dbReference type="PubMed" id="7651726"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="Oncogene" volume="14" first="1361" last="1370">
      <title>Inactivation of the cyclin-dependent kinase inhibitor p15INK4b by deletion and de novo methylation with independence of p16INK4a alterations in murine primary T-cell lymphomas.</title>
      <authorList>
        <person name="Malumbres M."/>
        <person name="de Castro I.P."/>
        <person name="Santos J."/>
        <person name="Melendez B."/>
        <person name="Mangues R."/>
        <person name="Serrano M."/>
        <person name="Pellicer A."/>
        <person name="Fernandez-Piqueras J."/>
      </authorList>
      <dbReference type="PubMed" id="9178896"/>
      <dbReference type="DOI" id="10.1038/sj.onc.1200969"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>C57BL/6J X DBA</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Czech II</strain>
      <tissue>Mammary gland</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2004" name="Mol. Cell. Proteomics" volume="3" first="1093" last="1101">
      <title>Phosphoproteomic analysis of the developing mouse brain.</title>
      <authorList>
        <person name="Ballif B.A."/>
        <person name="Villen J."/>
        <person name="Beausoleil S.A."/>
        <person name="Schwartz D."/>
        <person name="Gygi S.P."/>
      </authorList>
      <dbReference type="PubMed" id="15345747"/>
      <dbReference type="DOI" id="10.1074/mcp.m400085-mcp200"/>
    </citation>
    <scope>PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT THR-12</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Embryonic brain</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1999" name="J. Mol. Biol." volume="294" first="201" last="211">
      <title>Tumor suppressor INK4: comparisons of conformational properties between p16(INK4A) and p18(INK4C).</title>
      <authorList>
        <person name="Yuan C."/>
        <person name="Li J."/>
        <person name="Selby T.L."/>
        <person name="Byeon I.-J."/>
        <person name="Tsai M.-D."/>
      </authorList>
      <dbReference type="PubMed" id="10556039"/>
      <dbReference type="DOI" id="10.1006/jmbi.1999.3231"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Interacts strongly with CDK4 and CDK6. Potent inhibitor. Potential effector of TGF-beta induced cell cycle arrest (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Heterodimer of CDKN2B with CDK4 or CDK6.</text>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed ubiquitously.</text>
  </comment>
  <comment type="induction">
    <text>By TGF-beta.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the CDKN2 cyclin-dependent kinase inhibitor family.</text>
  </comment>
  <dbReference type="EMBL" id="U66085">
    <property type="protein sequence ID" value="AAB39833.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U66084">
    <property type="protein sequence ID" value="AAB39833.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC002010">
    <property type="protein sequence ID" value="AAH02010.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS18351.1"/>
  <dbReference type="RefSeq" id="NP_031696.1">
    <property type="nucleotide sequence ID" value="NM_007670.4"/>
  </dbReference>
  <dbReference type="PDB" id="1D9S">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-130"/>
  </dbReference>
  <dbReference type="PDBsum" id="1D9S"/>
  <dbReference type="AlphaFoldDB" id="P55271"/>
  <dbReference type="BMRB" id="P55271"/>
  <dbReference type="SMR" id="P55271"/>
  <dbReference type="BioGRID" id="198655">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="DIP" id="DIP-60249N"/>
  <dbReference type="IntAct" id="P55271">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="10090.ENSMUSP00000095595"/>
  <dbReference type="iPTMnet" id="P55271"/>
  <dbReference type="PhosphoSitePlus" id="P55271"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000095595"/>
  <dbReference type="PeptideAtlas" id="P55271"/>
  <dbReference type="ProteomicsDB" id="281521"/>
  <dbReference type="Antibodypedia" id="3605">
    <property type="antibodies" value="616 antibodies from 36 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="12579"/>
  <dbReference type="Ensembl" id="ENSMUST00000097981.6">
    <property type="protein sequence ID" value="ENSMUSP00000095595.5"/>
    <property type="gene ID" value="ENSMUSG00000073802.6"/>
  </dbReference>
  <dbReference type="GeneID" id="12579"/>
  <dbReference type="KEGG" id="mmu:12579"/>
  <dbReference type="UCSC" id="uc008tok.2">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:104737"/>
  <dbReference type="CTD" id="1030"/>
  <dbReference type="MGI" id="MGI:104737">
    <property type="gene designation" value="Cdkn2b"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000073802"/>
  <dbReference type="eggNOG" id="KOG0504">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000162423"/>
  <dbReference type="HOGENOM" id="CLU_000134_37_1_1"/>
  <dbReference type="InParanoid" id="P55271"/>
  <dbReference type="OMA" id="LYEPRDN"/>
  <dbReference type="OrthoDB" id="2321802at2759"/>
  <dbReference type="PhylomeDB" id="P55271"/>
  <dbReference type="TreeFam" id="TF352389"/>
  <dbReference type="Reactome" id="R-MMU-2559580">
    <property type="pathway name" value="Oxidative Stress Induced Senescence"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2559582">
    <property type="pathway name" value="Senescence-Associated Secretory Phenotype (SASP)"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-2559585">
    <property type="pathway name" value="Oncogene Induced Senescence"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-69231">
    <property type="pathway name" value="Cyclin D associated events in G1"/>
  </dbReference>
  <dbReference type="BioGRID-ORCS" id="12579">
    <property type="hits" value="1 hit in 80 CRISPR screens"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P55271"/>
  <dbReference type="PRO" id="PR:P55271"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 4"/>
  </dbReference>
  <dbReference type="RNAct" id="P55271">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000073802">
    <property type="expression patterns" value="Expressed in small intestine Peyer's patch and 153 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P55271">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004861">
    <property type="term" value="F:cyclin-dependent protein serine/threonine kinase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019901">
    <property type="term" value="F:protein kinase binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071460">
    <property type="term" value="P:cellular response to cell-matrix adhesion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031670">
    <property type="term" value="P:cellular response to nutrient"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071560">
    <property type="term" value="P:cellular response to transforming growth factor beta stimulus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090398">
    <property type="term" value="P:cellular senescence"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001889">
    <property type="term" value="P:liver development"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030219">
    <property type="term" value="P:megakaryocyte differentiation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050680">
    <property type="term" value="P:negative regulation of epithelial cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000134">
    <property type="term" value="P:negative regulation of G1/S transition of mitotic cell cycle"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060253">
    <property type="term" value="P:negative regulation of glial cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042326">
    <property type="term" value="P:negative regulation of phosphorylation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030858">
    <property type="term" value="P:positive regulation of epithelial cell differentiation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030511">
    <property type="term" value="P:positive regulation of transforming growth factor beta receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000079">
    <property type="term" value="P:regulation of cyclin-dependent protein serine/threonine kinase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070316">
    <property type="term" value="P:regulation of G0 to G1 transition"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034097">
    <property type="term" value="P:response to cytokine"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0014070">
    <property type="term" value="P:response to organic cyclic compound"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048536">
    <property type="term" value="P:spleen development"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="FunFam" id="1.25.40.20:FF:000107">
    <property type="entry name" value="cyclin-dependent kinase 4 inhibitor B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.25.40.20">
    <property type="entry name" value="Ankyrin repeat-containing domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050776">
    <property type="entry name" value="Ank_Repeat/CDKN_Inhibitor"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002110">
    <property type="entry name" value="Ankyrin_rpt"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036770">
    <property type="entry name" value="Ankyrin_rpt-contain_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR24201">
    <property type="entry name" value="ANK_REP_REGION DOMAIN-CONTAINING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR24201:SF8">
    <property type="entry name" value="CYCLIN-DEPENDENT KINASE 4 INHIBITOR B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF12796">
    <property type="entry name" value="Ank_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00248">
    <property type="entry name" value="ANK"/>
    <property type="match status" value="3"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48403">
    <property type="entry name" value="Ankyrin repeat"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50297">
    <property type="entry name" value="ANK_REP_REGION"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50088">
    <property type="entry name" value="ANK_REPEAT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0040">ANK repeat</keyword>
  <keyword id="KW-0131">Cell cycle</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0043">Tumor suppressor</keyword>
  <feature type="chain" id="PRO_0000144185" description="Cyclin-dependent kinase 4 inhibitor B">
    <location>
      <begin position="1"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="repeat" description="ANK 1">
    <location>
      <begin position="5"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="repeat" description="ANK 2">
    <location>
      <begin position="38"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="repeat" description="ANK 3">
    <location>
      <begin position="71"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="repeat" description="ANK 4">
    <location>
      <begin position="104"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphothreonine" evidence="3">
    <location>
      <position position="12"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="9"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="19"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="41"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="51"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="69"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="75"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="85"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="102"/>
      <end position="106"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="108"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="118"/>
      <end position="128"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007744" key="3">
    <source>
      <dbReference type="PubMed" id="15345747"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="1D9S"/>
    </source>
  </evidence>
  <sequence length="130" mass="13789" checksum="7AAD60FF552BCFF9" modified="1996-10-01" version="1">MLGGSSDAGLATAAARGQVETVRQLLEAGADPNALNRFGRRPIQVMMMGSAQVAELLLLHGAEPNCADPATLTRPVHDAAREGFLDTLVVLHRAGARLDVCDAWGRLPVDLAEEQGHRDIARYLHAATGD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>