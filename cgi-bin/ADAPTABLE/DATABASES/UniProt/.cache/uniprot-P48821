<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-02-01" modified="2024-01-24" version="74" xmlns="http://uniprot.org/uniprot">
  <accession>P48821</accession>
  <name>CECE_BOMMO</name>
  <protein>
    <recommendedName>
      <fullName>Antibacterial peptide enbocin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Moricin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bombyx mori</name>
    <name type="common">Silk moth</name>
    <dbReference type="NCBI Taxonomy" id="7091"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Bombycoidea</taxon>
      <taxon>Bombycidae</taxon>
      <taxon>Bombycinae</taxon>
      <taxon>Bombyx</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1995-06" db="EMBL/GenBank/DDBJ databases">
      <title>Molecular characterization of a new antibacterial peptide gene, enbocin, from Bombyx mori.</title>
      <authorList>
        <person name="Kim S.H."/>
        <person name="Je Y.H."/>
        <person name="Kim K.Y."/>
        <person name="Kang S.K."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="1998-02" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning and expression of enbocin gene encoding a new antibacterial peptide from Bombyx mori.</title>
      <authorList>
        <person name="Kim S.H."/>
        <person name="Je Y.H."/>
        <person name="Youn E.Y."/>
        <person name="Lee D.W."/>
        <person name="Kim K.Y."/>
        <person name="Kang S.K."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>Backokjam</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Has antibacterial activity against Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the cecropin family.</text>
  </comment>
  <dbReference type="EMBL" id="U30289">
    <property type="protein sequence ID" value="AAC02238.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF049237">
    <property type="protein sequence ID" value="AAC05493.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="JE0161">
    <property type="entry name" value="JE0161"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001037472.1">
    <property type="nucleotide sequence ID" value="NM_001044007.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P48821"/>
  <dbReference type="SMR" id="P48821"/>
  <dbReference type="STRING" id="7091.P48821"/>
  <dbReference type="TCDB" id="1.C.17.1.3">
    <property type="family name" value="the cecropin (cecropin) family"/>
  </dbReference>
  <dbReference type="PaxDb" id="7091-BGIBMGA000018-TA"/>
  <dbReference type="GeneID" id="693035"/>
  <dbReference type="CTD" id="693035"/>
  <dbReference type="HOGENOM" id="CLU_187909_0_0_1"/>
  <dbReference type="InParanoid" id="P48821"/>
  <dbReference type="Proteomes" id="UP000005204">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000875">
    <property type="entry name" value="Cecropin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00272">
    <property type="entry name" value="Cecropin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00268">
    <property type="entry name" value="CECROPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000004830" evidence="1">
    <location>
      <position position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004831" description="Antibacterial peptide enbocin">
    <location>
      <begin position="22"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="modified residue" description="Serine amide" evidence="1">
    <location>
      <position position="58"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="59" mass="6330" checksum="284DF86F22E6DEB5" modified="1996-02-01" version="1" precursor="true">MNFTRIIFFLFVVVFATASGKPWNIFKEIERAVARTRDAVISAGPAVRTVAAATSVASG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>