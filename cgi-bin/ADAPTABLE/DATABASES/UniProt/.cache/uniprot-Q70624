<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2024-11-27" version="88" xmlns="http://uniprot.org/uniprot">
  <accession>Q70624</accession>
  <name>REV_HV1LW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein Rev</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">ART/TRS</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Anti-repression transactivator</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Regulator of expression of viral proteins</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rev</name>
  </gene>
  <organism>
    <name type="scientific">Human immunodeficiency virus type 1 group M subtype B (isolate LW123)</name>
    <name type="common">HIV-1</name>
    <dbReference type="NCBI Taxonomy" id="82834"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Pararnavirae</taxon>
      <taxon>Artverviricota</taxon>
      <taxon>Revtraviricetes</taxon>
      <taxon>Ortervirales</taxon>
      <taxon>Retroviridae</taxon>
      <taxon>Orthoretrovirinae</taxon>
      <taxon>Lentivirus</taxon>
      <taxon>Human immunodeficiency virus type 1</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1994" name="AIDS Res. Hum. Retroviruses" volume="10" first="1143" last="1155">
      <title>Viral variability and serum antibody response in a laboratory worker infected with HIV type 1 (HTLV type IIIB).</title>
      <authorList>
        <person name="Reitz M.S. Jr."/>
        <person name="Hall L."/>
        <person name="Robert-Guroff M."/>
        <person name="Lautenberger J.A."/>
        <person name="Hahn B.M."/>
        <person name="Shaw G.M."/>
        <person name="Kong L.I."/>
        <person name="Weiss S.H."/>
        <person name="Waters D."/>
        <person name="Gallo R.C."/>
        <person name="Blattner W."/>
      </authorList>
      <dbReference type="PubMed" id="7826699"/>
      <dbReference type="DOI" id="10.1089/aid.1994.10.1143"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1999" name="Arch. Biochem. Biophys." volume="365" first="186" last="191">
      <title>The ins and outs of HIV Rev.</title>
      <authorList>
        <person name="Hope T.J."/>
      </authorList>
      <dbReference type="PubMed" id="10328811"/>
      <dbReference type="DOI" id="10.1006/abbi.1999.1207"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Escorts unspliced or incompletely spliced viral pre-mRNAs (late transcripts) out of the nucleus of infected cells. These pre-mRNAs carry a recognition sequence called Rev responsive element (RRE) located in the env gene, that is not present in fully spliced viral mRNAs (early transcripts). This function is essential since most viral proteins are translated from unspliced or partially spliced pre-mRNAs which cannot exit the nucleus by the pathway used by fully processed cellular mRNAs. Rev itself is translated from a fully spliced mRNA that readily exits the nucleus. Rev's nuclear localization signal (NLS) binds directly to KPNB1/Importin beta-1 without previous binding to KPNA1/Importin alpha-1. KPNB1 binds to the GDP bound form of RAN (Ran-GDP) and targets Rev to the nucleus. In the nucleus, the conversion from Ran-GDP to Ran-GTP dissociates Rev from KPNB1 and allows Rev's binding to the RRE in viral pre-mRNAs. Rev multimerization on the RRE via cooperative assembly exposes its nuclear export signal (NES) to the surface. Rev can then form a complex with XPO1/CRM1 and Ran-GTP, leading to nuclear export of the complex. Conversion from Ran-GTP to Ran-GDP mediates dissociation of the Rev/RRE/XPO1/RAN complex, so that Rev can return to the nucleus for a subsequent round of export. Beside KPNB1, also seems to interact with TNPO1/Transportin-1, RANBP5/IPO5 and IPO7/RANBP7 for nuclear import. The nucleoporin-like HRB/RIP is an essential cofactor that probably indirectly interacts with Rev to release HIV RNAs from the perinuclear region to the cytoplasm.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homomultimer; when bound to the RRE. Multimeric assembly is essential for activity and may involve XPO1. Binds to human KPNB1, XPO1, TNPO1, RANBP5 and IPO7. Interacts with the viral Integrase. Interacts with human KHDRBS1. Interacts with human NAP1; this interaction decreases Rev multimerization and stimulates its activity. Interacts with human DEAD-box helicases DDX3 and DDX24; these interactions may serve for viral RNA export to the cytoplasm and packaging, respectively. Interacts with human PSIP1; this interaction may inhibit HIV-1 DNA integration by promoting dissociation of the Integrase-LEDGF/p75 complex.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
      <location evidence="1">Host nucleolus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
    </subcellularLocation>
    <text evidence="1">The presence of both nuclear import and nuclear export signals leads to continuous shuttling between the nucleus and cytoplasm.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The RNA-binding motif binds to the RRE, a 240 bp stem-and-loop structure present in incompletely spliced viral pre-mRNAs. This region also contains the NLS which mediates nuclear localization via KPNB1 binding and, when the N-terminal sequence is present, nucleolar targeting. These overlapping functions prevent Rev bound to RRE from undesirable return to the nucleus. When Rev binds the RRE, the NLS becomes masked while the NES remains accessible. The leucine-rich NES mediates binding to human XPO1.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Asymmetrically arginine dimethylated at one site by host PRMT6. Methylation impairs the RNA-binding activity and export of viral RNA from the nucleus to the cytoplasm.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Phosphorylated by protein kinase CK2. Presence of, and maybe binding to the N-terminus of the regulatory beta subunit of CK2 is necessary for CK2-mediated Rev's phosphorylation.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">HIV-1 lineages are divided in three main groups, M (for Major), O (for Outlier), and N (for New, or Non-M, Non-O). The vast majority of strains found worldwide belong to the group M. Group O seems to be endemic to and largely confined to Cameroon and neighboring countries in West Central Africa, where these viruses represent a small minority of HIV-1 strains. The group N is represented by a limited number of isolates from Cameroonian persons. The group M is further subdivided in 9 clades or subtypes (A to D, F to H, J and K).</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the HIV-1 REV protein family.</text>
  </comment>
  <dbReference type="EMBL" id="U12055">
    <property type="protein sequence ID" value="AAA76688.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="SMR" id="Q70624"/>
  <dbReference type="Proteomes" id="UP000165413">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030430">
    <property type="term" value="C:host cell cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044196">
    <property type="term" value="C:host cell nucleolus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003700">
    <property type="term" value="F:DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003723">
    <property type="term" value="F:RNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051028">
    <property type="term" value="P:mRNA transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016032">
    <property type="term" value="P:viral process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.140.630">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_04077">
    <property type="entry name" value="REV_HIV1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000625">
    <property type="entry name" value="REV_protein"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00424">
    <property type="entry name" value="REV"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0014">AIDS</keyword>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-0488">Methylation</keyword>
  <keyword id="KW-0509">mRNA transport</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000085253" description="Protein Rev">
    <location>
      <begin position="1"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="region of interest" description="Homomultimerization" evidence="1">
    <location>
      <begin position="18"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="23"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="92"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Nuclear localization signal and RNA-binding (RRE)" evidence="1">
    <location>
      <begin position="36"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Nuclear export signal and binding to XPO1" evidence="1">
    <location>
      <begin position="75"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by host CK2" evidence="1">
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by host CK2" evidence="1">
    <location>
      <position position="8"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by host" evidence="1">
    <location>
      <position position="94"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by host" evidence="1">
    <location>
      <position position="101"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_04077"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <sequence length="118" mass="13169" checksum="5A4DD6855E5CC546" modified="1996-11-01" version="1">MAGRSGDSDEDLLKAVRLIKFLYQSSSDPPPNPGGTRQARRNRRRRWRERQRQIHSISERILSTYLGRSAKPVPLQLPPLERLTLDCNEDCGTSGTQGVGSPQILVESPTVLESGTKE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>