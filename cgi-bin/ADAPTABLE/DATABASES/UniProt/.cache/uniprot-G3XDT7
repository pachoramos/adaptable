<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-09-05" modified="2024-11-27" version="41" xmlns="http://uniprot.org/uniprot">
  <accession>G3XDT7</accession>
  <name>LYSC_DRONO</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Lysozyme C</fullName>
      <ecNumber evidence="1">3.2.1.17</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">1,4-beta-N-acetylmuramidase C</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Dromaius novaehollandiae</name>
    <name type="common">Emu</name>
    <dbReference type="NCBI Taxonomy" id="8790"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Archelosauria</taxon>
      <taxon>Archosauria</taxon>
      <taxon>Dinosauria</taxon>
      <taxon>Saurischia</taxon>
      <taxon>Theropoda</taxon>
      <taxon>Coelurosauria</taxon>
      <taxon>Aves</taxon>
      <taxon>Palaeognathae</taxon>
      <taxon>Casuariiformes</taxon>
      <taxon>Dromaiidae</taxon>
      <taxon>Dromaius</taxon>
    </lineage>
  </organism>
  <reference evidence="5 6" key="1">
    <citation type="journal article" date="2012" name="Gene" volume="492" first="244" last="249">
      <title>Molecular characterization of goose- and chicken-type lysozymes in emu (Dromaius novaehollandiae): evidence for extremely low lysozyme levels in emu egg white.</title>
      <authorList>
        <person name="Maehashi K."/>
        <person name="Matano M."/>
        <person name="Irisawa T."/>
        <person name="Uchino M."/>
        <person name="Kashiwagi Y."/>
        <person name="Watanabe T."/>
      </authorList>
      <dbReference type="PubMed" id="22044478"/>
      <dbReference type="DOI" id="10.1016/j.gene.2011.10.021"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue evidence="4">Muscle</tissue>
      <tissue evidence="6">Oviduct</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3 4">Lysozymes have primarily a bacteriolytic function; those in tissues and body fluids are associated with the monocyte-macrophage system and enhance the activity of immunoagents. Has bacteriolytic activity against M.luteus.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>Hydrolysis of (1-&gt;4)-beta-linkages between N-acetylmuramic acid and N-acetyl-D-glucosamine residues in a peptidoglycan and between N-acetyl-D-glucosamine residues in chitodextrins.</text>
      <dbReference type="EC" id="3.2.1.17"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed in liver and ovary. Not expressed in bone marrow, lung, spleen, intestine or oviduct.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">Lysozyme C is capable of both hydrolysis and transglycosylation; it shows also a slight esterase activity. It acts rapidly on both peptide-substituted and unsubstituted peptidoglycan, and slowly on chitin oligosaccharides (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the glycosyl hydrolase 22 family.</text>
  </comment>
  <dbReference type="EC" id="3.2.1.17" evidence="1"/>
  <dbReference type="EMBL" id="AB513676">
    <property type="protein sequence ID" value="BAL03621.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB513854">
    <property type="protein sequence ID" value="BAL03622.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="G3XDT7"/>
  <dbReference type="SMR" id="G3XDT7"/>
  <dbReference type="CAZy" id="GH22">
    <property type="family name" value="Glycoside Hydrolase Family 22"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSDNVT00000022564.1">
    <property type="protein sequence ID" value="ENSDNVP00000018735.1"/>
    <property type="gene ID" value="ENSDNVG00000013082.1"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694423">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003796">
    <property type="term" value="F:lysozyme activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd16897">
    <property type="entry name" value="LYZ_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.530.10:FF:000001">
    <property type="entry name" value="Lysozyme C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.530.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001916">
    <property type="entry name" value="Glyco_hydro_22"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019799">
    <property type="entry name" value="Glyco_hydro_22_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000974">
    <property type="entry name" value="Glyco_hydro_22_lys"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023346">
    <property type="entry name" value="Lysozyme-like_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407">
    <property type="entry name" value="LYSOZYME C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407:SF28">
    <property type="entry name" value="LYSOZYME C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00062">
    <property type="entry name" value="Lys"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00137">
    <property type="entry name" value="LYSOZYME"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00135">
    <property type="entry name" value="LYZLACT"/>
  </dbReference>
  <dbReference type="SMART" id="SM00263">
    <property type="entry name" value="LYZ1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF53955">
    <property type="entry name" value="Lysozyme-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00128">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51348">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0081">Bacteriolytic enzyme</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0326">Glycosidase</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5000795993" description="Lysozyme C" evidence="2">
    <location>
      <begin position="19"/>
      <end position="147"/>
    </location>
  </feature>
  <feature type="domain" description="C-type lysozyme" evidence="3">
    <location>
      <begin position="19"/>
      <end position="147"/>
    </location>
  </feature>
  <feature type="active site" evidence="1 3">
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="active site" evidence="1 3">
    <location>
      <position position="70"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="119"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="24"/>
      <end position="145"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="48"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="82"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="94"/>
      <end position="112"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P00698"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00680"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="22044478"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="BAL03622.1"/>
    </source>
  </evidence>
  <sequence length="147" mass="16443" checksum="F771E0B42235C110" modified="2011-12-14" version="1" precursor="true">MKFFLILGFCLLPLIAQGKVFQRCELAAAMKKHGLSNYRGYSLGHWVCAAKYESNFNTAAINRNRDGSSDYGILQINSRWWCNDGRTSGAKNLCKISCSALLSSDITASVNCAKRVVSDKNGMNAWVAWRNHCKGRDVSQWIRGCRV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>