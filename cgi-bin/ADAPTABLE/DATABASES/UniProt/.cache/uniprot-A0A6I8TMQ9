<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2024-10-02" modified="2024-11-27" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>A0A6I8TMQ9</accession>
  <name>CYT_AEDAE</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Cystatin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Aacystatin</fullName>
    </alternativeName>
  </protein>
  <organism evidence="7">
    <name type="scientific">Aedes aegypti</name>
    <name type="common">Yellowfever mosquito</name>
    <name type="synonym">Culex aegypti</name>
    <dbReference type="NCBI Taxonomy" id="7159"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Nematocera</taxon>
      <taxon>Culicoidea</taxon>
      <taxon>Culicidae</taxon>
      <taxon>Culicinae</taxon>
      <taxon>Aedini</taxon>
      <taxon>Aedes</taxon>
      <taxon>Stegomyia</taxon>
    </lineage>
  </organism>
  <reference evidence="7" key="1">
    <citation type="journal article" date="2018" name="Nature" volume="563" first="501" last="507">
      <title>Improved reference genome of Aedes aegypti informs arbovirus vector control.</title>
      <authorList>
        <person name="Matthews B.J."/>
        <person name="Dudchenko O."/>
        <person name="Kingan S.B."/>
        <person name="Koren S."/>
        <person name="Antoshechkin I."/>
        <person name="Crawford J.E."/>
        <person name="Glassford W.J."/>
        <person name="Herre M."/>
        <person name="Redmond S.N."/>
        <person name="Rose N.H."/>
        <person name="Weedall G.D."/>
        <person name="Wu Y."/>
        <person name="Batra S.S."/>
        <person name="Brito-Sierra C.A."/>
        <person name="Buckingham S.D."/>
        <person name="Campbell C.L."/>
        <person name="Chan S."/>
        <person name="Cox E."/>
        <person name="Evans B.R."/>
        <person name="Fansiri T."/>
        <person name="Filipovic I."/>
        <person name="Fontaine A."/>
        <person name="Gloria-Soria A."/>
        <person name="Hall R."/>
        <person name="Joardar V.S."/>
        <person name="Jones A.K."/>
        <person name="Kay R.G.G."/>
        <person name="Kodali V.K."/>
        <person name="Lee J."/>
        <person name="Lycett G.J."/>
        <person name="Mitchell S.N."/>
        <person name="Muehling J."/>
        <person name="Murphy M.R."/>
        <person name="Omer A.D."/>
        <person name="Partridge F.A."/>
        <person name="Peluso P."/>
        <person name="Aiden A.P."/>
        <person name="Ramasamy V."/>
        <person name="Rasic G."/>
        <person name="Roy S."/>
        <person name="Saavedra-Rodriguez K."/>
        <person name="Sharan S."/>
        <person name="Sharma A."/>
        <person name="Smith M.L."/>
        <person name="Turner J."/>
        <person name="Weakley A.M."/>
        <person name="Zhao Z."/>
        <person name="Akbari O.S."/>
        <person name="Black W.C. IV"/>
        <person name="Cao H."/>
        <person name="Darby A.C."/>
        <person name="Hill C.A."/>
        <person name="Johnston J.S."/>
        <person name="Murphy T.D."/>
        <person name="Raikhel A.S."/>
        <person name="Sattelle D.B."/>
        <person name="Sharakhov I.V."/>
        <person name="White B.J."/>
        <person name="Zhao L."/>
        <person name="Aiden E.L."/>
        <person name="Mann R.S."/>
        <person name="Lambrechts L."/>
        <person name="Powell J.R."/>
        <person name="Sharakhova M.V."/>
        <person name="Tu Z."/>
        <person name="Robertson H.M."/>
        <person name="McBride C.S."/>
        <person name="Hastie A.R."/>
        <person name="Korlach J."/>
        <person name="Neafsey D.E."/>
        <person name="Phillippy A.M."/>
        <person name="Vosshall L.B."/>
      </authorList>
      <dbReference type="PubMed" id="30429615"/>
      <dbReference type="DOI" id="10.1038/s41586-018-0692-z"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain evidence="7">LVP_AGWG</strain>
    </source>
  </reference>
  <reference evidence="6" key="2">
    <citation type="journal article" date="2012" name="PLoS Pathog." volume="8" first="e1002631" last="e1002631">
      <title>Dengue virus infection of the Aedes aegypti salivary gland and chemosensory apparatus induces genes that modulate infection and blood-feeding behavior.</title>
      <authorList>
        <person name="Sim S."/>
        <person name="Ramirez J.L."/>
        <person name="Dimopoulos G."/>
      </authorList>
      <dbReference type="PubMed" id="22479185"/>
      <dbReference type="DOI" id="10.1371/journal.ppat.1002631"/>
    </citation>
    <scope>FUNCTION (MICROBIAL INFECTION)</scope>
    <scope>INDUCTION (MICROBIAL INFECTION)</scope>
    <scope>DISRUPTION PHENOTYPE (MICROBIAL INFECTION)</scope>
  </reference>
  <reference evidence="6" key="3">
    <citation type="journal article" date="2020" name="Int. J. Biol. Macromol." volume="146" first="141" last="149">
      <title>The first characterization of a cystatin and a cathepsin L-like peptidase from Aedes aegypti and their possible role in DENV infection by the modulation of apoptosis.</title>
      <authorList>
        <person name="Oliveira F.A.A."/>
        <person name="Buri M.V."/>
        <person name="Rodriguez B.L."/>
        <person name="Costa-da-Silva A.L."/>
        <person name="Araujo H.R.C."/>
        <person name="Capurro M.L."/>
        <person name="Lu S."/>
        <person name="Tanaka A.S."/>
      </authorList>
      <dbReference type="PubMed" id="31857170"/>
      <dbReference type="DOI" id="10.1016/j.ijbiomac.2019.12.010"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH CATHEPSIN L-LIKE PEPTIDASE</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION BY APOPTOSIS</scope>
  </reference>
  <comment type="function">
    <text evidence="4">Cysteine proteinase inhibitor (PubMed:31857170). Inhibits cathepsin L-like peptidase (PubMed:31857170). Increases cell viability following apoptosis induction by staurosporine (PubMed:31857170). Inhibits human cathepsin S (CTSS), human cathepsin L2 (CTSV), human cathepsin L (CTSL), human cathepsin B (CTSB) and papain (PubMed:31857170).</text>
  </comment>
  <comment type="function">
    <text evidence="3">(Microbial infection) Modulates dengue virus type 2 replication in salivary glands.</text>
  </comment>
  <comment type="subunit">
    <text evidence="4">Interacts with cathepsin L-like peptidase; the interaction results in inhibition of cathepsin L-like peptidase activity.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Salivary gland (PubMed:31857170). Midgut (PubMed:31857170).</text>
  </comment>
  <comment type="induction">
    <text evidence="4">Up-regulated following apoptosis induction by staurosporine.</text>
  </comment>
  <comment type="induction">
    <text evidence="3">(Microbial infection) Induced in salivary glands following dengue virus type 2 infection.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="3">(Microbial infection) RNAi-mediated knockdown results in increased titers of dengue virus type 2 in salivary glands but not in carcasses.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="4">Negative correlation between cystatin transcription and titers of dengue virus type 2 is observed in infected mosquitoes.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the cystatin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="A0A6I8TMQ9"/>
  <dbReference type="EnsemblMetazoa" id="AAEL013287-RC">
    <property type="protein sequence ID" value="AAEL013287-PC"/>
    <property type="gene ID" value="AAEL013287"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="AAEL013287-RD">
    <property type="protein sequence ID" value="AAEL013287-PD"/>
    <property type="gene ID" value="AAEL013287"/>
  </dbReference>
  <dbReference type="InParanoid" id="A0A6I8TMQ9"/>
  <dbReference type="OrthoDB" id="3449421at2759"/>
  <dbReference type="Proteomes" id="UP000008820">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00042">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.450.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000010">
    <property type="entry name" value="Cystatin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046350">
    <property type="entry name" value="Cystatin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00031">
    <property type="entry name" value="Cystatin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54403">
    <property type="entry name" value="Cystatin/monellin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <feature type="chain" id="PRO_0000461060" description="Cystatin">
    <location>
      <begin position="1"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="domain" description="Cystatin" evidence="1">
    <location>
      <begin position="22"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="2">
    <location>
      <position position="29"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00498"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22479185"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="31857170"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="31857170"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="Proteomes" id="UP000008820"/>
    </source>
  </evidence>
  <sequence length="96" mass="10934" checksum="57B4C52A077A9FE9" modified="2020-08-12" version="1">MDGQPITGGVHPTENLNAEHHDFIKAALNETGTHAGRKYKVLRSSQQVVAGMKYTFYIVFEDDESGQEYKITAWSRPWLQDKGEALKLTFDKHEPK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>