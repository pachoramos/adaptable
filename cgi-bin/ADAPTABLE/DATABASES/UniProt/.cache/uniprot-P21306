<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1991-05-01" modified="2024-11-27" version="197" xmlns="http://uniprot.org/uniprot">
  <accession>P21306</accession>
  <accession>D6W399</accession>
  <name>ATP5E_YEAST</name>
  <protein>
    <recommendedName>
      <fullName>ATP synthase subunit epsilon, mitochondrial</fullName>
      <shortName>ATPase subunit epsilon</shortName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">ATP15</name>
    <name type="ordered locus">YPL271W</name>
    <name type="ORF">P0345</name>
  </gene>
  <organism>
    <name type="scientific">Saccharomyces cerevisiae (strain ATCC 204508 / S288c)</name>
    <name type="common">Baker's yeast</name>
    <dbReference type="NCBI Taxonomy" id="559292"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Ascomycota</taxon>
      <taxon>Saccharomycotina</taxon>
      <taxon>Saccharomycetes</taxon>
      <taxon>Saccharomycetales</taxon>
      <taxon>Saccharomycetaceae</taxon>
      <taxon>Saccharomyces</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="J. Biol. Chem." volume="268" first="161" last="167">
      <title>ATP synthase of yeast mitochondria. Isolation and disruption of the ATP epsilon gene.</title>
      <authorList>
        <person name="Guelin E."/>
        <person name="Chevallier J."/>
        <person name="Rigoulet M."/>
        <person name="Guerin B."/>
        <person name="Velours J."/>
      </authorList>
      <dbReference type="PubMed" id="8416924"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)54128-4"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>D273-10B/A/H/U</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="Nature" volume="387" first="103" last="105">
      <title>The nucleotide sequence of Saccharomyces cerevisiae chromosome XVI.</title>
      <authorList>
        <person name="Bussey H."/>
        <person name="Storms R.K."/>
        <person name="Ahmed A."/>
        <person name="Albermann K."/>
        <person name="Allen E."/>
        <person name="Ansorge W."/>
        <person name="Araujo R."/>
        <person name="Aparicio A."/>
        <person name="Barrell B.G."/>
        <person name="Badcock K."/>
        <person name="Benes V."/>
        <person name="Botstein D."/>
        <person name="Bowman S."/>
        <person name="Brueckner M."/>
        <person name="Carpenter J."/>
        <person name="Cherry J.M."/>
        <person name="Chung E."/>
        <person name="Churcher C.M."/>
        <person name="Coster F."/>
        <person name="Davis K."/>
        <person name="Davis R.W."/>
        <person name="Dietrich F.S."/>
        <person name="Delius H."/>
        <person name="DiPaolo T."/>
        <person name="Dubois E."/>
        <person name="Duesterhoeft A."/>
        <person name="Duncan M."/>
        <person name="Floeth M."/>
        <person name="Fortin N."/>
        <person name="Friesen J.D."/>
        <person name="Fritz C."/>
        <person name="Goffeau A."/>
        <person name="Hall J."/>
        <person name="Hebling U."/>
        <person name="Heumann K."/>
        <person name="Hilbert H."/>
        <person name="Hillier L.W."/>
        <person name="Hunicke-Smith S."/>
        <person name="Hyman R.W."/>
        <person name="Johnston M."/>
        <person name="Kalman S."/>
        <person name="Kleine K."/>
        <person name="Komp C."/>
        <person name="Kurdi O."/>
        <person name="Lashkari D."/>
        <person name="Lew H."/>
        <person name="Lin A."/>
        <person name="Lin D."/>
        <person name="Louis E.J."/>
        <person name="Marathe R."/>
        <person name="Messenguy F."/>
        <person name="Mewes H.-W."/>
        <person name="Mirtipati S."/>
        <person name="Moestl D."/>
        <person name="Mueller-Auer S."/>
        <person name="Namath A."/>
        <person name="Nentwich U."/>
        <person name="Oefner P."/>
        <person name="Pearson D."/>
        <person name="Petel F.X."/>
        <person name="Pohl T.M."/>
        <person name="Purnelle B."/>
        <person name="Rajandream M.A."/>
        <person name="Rechmann S."/>
        <person name="Rieger M."/>
        <person name="Riles L."/>
        <person name="Roberts D."/>
        <person name="Schaefer M."/>
        <person name="Scharfe M."/>
        <person name="Scherens B."/>
        <person name="Schramm S."/>
        <person name="Schroeder M."/>
        <person name="Sdicu A.-M."/>
        <person name="Tettelin H."/>
        <person name="Urrestarazu L.A."/>
        <person name="Ushinsky S."/>
        <person name="Vierendeels F."/>
        <person name="Vissers S."/>
        <person name="Voss H."/>
        <person name="Walsh S.V."/>
        <person name="Wambutt R."/>
        <person name="Wang Y."/>
        <person name="Wedler E."/>
        <person name="Wedler H."/>
        <person name="Winnett E."/>
        <person name="Zhong W.-W."/>
        <person name="Zollner A."/>
        <person name="Vo D.H."/>
        <person name="Hani J."/>
      </authorList>
      <dbReference type="PubMed" id="9169875"/>
      <dbReference type="DOI" id="10.1038/387s103"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 204508 / S288c</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2014" name="G3 (Bethesda)" volume="4" first="389" last="398">
      <title>The reference genome sequence of Saccharomyces cerevisiae: Then and now.</title>
      <authorList>
        <person name="Engel S.R."/>
        <person name="Dietrich F.S."/>
        <person name="Fisk D.G."/>
        <person name="Binkley G."/>
        <person name="Balakrishnan R."/>
        <person name="Costanzo M.C."/>
        <person name="Dwight S.S."/>
        <person name="Hitz B.C."/>
        <person name="Karra K."/>
        <person name="Nash R.S."/>
        <person name="Weng S."/>
        <person name="Wong E.D."/>
        <person name="Lloyd P."/>
        <person name="Skrzypek M.S."/>
        <person name="Miyasato S.R."/>
        <person name="Simison M."/>
        <person name="Cherry J.M."/>
      </authorList>
      <dbReference type="PubMed" id="24374639"/>
      <dbReference type="DOI" id="10.1534/g3.113.008995"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>ATCC 204508 / S288c</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2007" name="Genome Res." volume="17" first="536" last="543">
      <title>Approaching a complete repository of sequence-verified protein-encoding clones for Saccharomyces cerevisiae.</title>
      <authorList>
        <person name="Hu Y."/>
        <person name="Rolfs A."/>
        <person name="Bhullar B."/>
        <person name="Murthy T.V.S."/>
        <person name="Zhu C."/>
        <person name="Berger M.F."/>
        <person name="Camargo A.A."/>
        <person name="Kelley F."/>
        <person name="McCarron S."/>
        <person name="Jepson D."/>
        <person name="Richardson A."/>
        <person name="Raphael J."/>
        <person name="Moreira D."/>
        <person name="Taycher E."/>
        <person name="Zuo D."/>
        <person name="Mohr S."/>
        <person name="Kane M.F."/>
        <person name="Williamson J."/>
        <person name="Simpson A.J.G."/>
        <person name="Bulyk M.L."/>
        <person name="Harlow E."/>
        <person name="Marsischky G."/>
        <person name="Kolodner R.D."/>
        <person name="LaBaer J."/>
      </authorList>
      <dbReference type="PubMed" id="17322287"/>
      <dbReference type="DOI" id="10.1101/gr.6037607"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 204508 / S288c</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1991" name="J. Biol. Chem." volume="266" first="723" last="727">
      <title>Isolation and complete amino acid sequence of the mitochondrial ATP synthase epsilon-subunit of the yeast Saccharomyces cerevisiae.</title>
      <authorList>
        <person name="Arselin G."/>
        <person name="Gandar J.-C."/>
        <person name="Guerin B."/>
        <person name="Velours J."/>
      </authorList>
      <dbReference type="PubMed" id="1985960"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)35231-6"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 2-62</scope>
    <source>
      <strain>ATCC 60782 / S / NCYC 232 / American yeast foam</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2003" name="Nature" volume="425" first="737" last="741">
      <title>Global analysis of protein expression in yeast.</title>
      <authorList>
        <person name="Ghaemmaghami S."/>
        <person name="Huh W.-K."/>
        <person name="Bower K."/>
        <person name="Howson R.W."/>
        <person name="Belle A."/>
        <person name="Dephoure N."/>
        <person name="O'Shea E.K."/>
        <person name="Weissman J.S."/>
      </authorList>
      <dbReference type="PubMed" id="14562106"/>
      <dbReference type="DOI" id="10.1038/nature02046"/>
    </citation>
    <scope>LEVEL OF PROTEIN EXPRESSION [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2007" name="Mol. Cell. Proteomics" volume="6" first="1896" last="1906">
      <title>Profiling phosphoproteins of yeast mitochondria reveals a role of phosphorylation in assembly of the ATP synthase.</title>
      <authorList>
        <person name="Reinders J."/>
        <person name="Wagner K."/>
        <person name="Zahedi R.P."/>
        <person name="Stojanovski D."/>
        <person name="Eyrich B."/>
        <person name="van der Laan M."/>
        <person name="Rehling P."/>
        <person name="Sickmann A."/>
        <person name="Pfanner N."/>
        <person name="Meisinger C."/>
      </authorList>
      <dbReference type="PubMed" id="17761666"/>
      <dbReference type="DOI" id="10.1074/mcp.m700098-mcp200"/>
    </citation>
    <scope>PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT THR-52</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <strain>ATCC 76625 / YPH499</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Mitochondrial membrane ATP synthase (F(1)F(0) ATP synthase or Complex V) produces ATP from ADP in the presence of a proton gradient across the membrane which is generated by electron transport complexes of the respiratory chain. F-type ATPases consist of two structural domains, F(1) - containing the extramembraneous catalytic core, and F(0) - containing the membrane proton channel, linked together by a central stalk and a peripheral stalk. During catalysis, ATP synthesis in the catalytic domain of F(1) is coupled via a rotary mechanism of the central stalk subunits to proton translocation. Part of the complex F(1) domain and of the central stalk which is part of the complex rotary element. Rotation of the central stalk against the surrounding alpha(3)beta(3) subunits leads to hydrolysis of ATP in three separate catalytic sites on the beta subunits.</text>
  </comment>
  <comment type="subunit">
    <text>F-type ATPases have 2 components, CF(1) - the catalytic core - and CF(0) - the membrane proton channel. CF(1) has five subunits: alpha(3), beta(3), gamma(1), delta(1), epsilon(1). CF(0) has three main subunits: a, b and c.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Mitochondrion</location>
    </subcellularLocation>
    <subcellularLocation>
      <location>Mitochondrion inner membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">Present with 4280 molecules/cell in log phase SD medium.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the eukaryotic ATPase epsilon family.</text>
  </comment>
  <dbReference type="EMBL" id="X64767">
    <property type="protein sequence ID" value="CAA46014.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Z73627">
    <property type="protein sequence ID" value="CAA98007.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY558140">
    <property type="protein sequence ID" value="AAS56466.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BK006949">
    <property type="protein sequence ID" value="DAA11165.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A45315">
    <property type="entry name" value="A45315"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_015052.1">
    <property type="nucleotide sequence ID" value="NM_001184085.1"/>
  </dbReference>
  <dbReference type="PDB" id="2HLD">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.80 A"/>
    <property type="chains" value="1/I/R=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="2WPD">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.43 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="3FKS">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.59 A"/>
    <property type="chains" value="1/I/R=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="3OE7">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.19 A"/>
    <property type="chains" value="1/I/R=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="3OEE">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.74 A"/>
    <property type="chains" value="1/I/R=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="3OEH">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.00 A"/>
    <property type="chains" value="1/I/R=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="3OFN">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.20 A"/>
    <property type="chains" value="I/R=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="3ZIA">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="I/S=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="4B2Q">
    <property type="method" value="EM"/>
    <property type="resolution" value="37.00 A"/>
    <property type="chains" value="I/i=2-60"/>
  </dbReference>
  <dbReference type="PDB" id="6CP3">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.80 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="6CP6">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.60 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7MD2">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.10 A"/>
    <property type="chains" value="H=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7MD3">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.30 A"/>
    <property type="chains" value="H=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TJY">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.80 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TJZ">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.40 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK0">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.40 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK1">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.10 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK2">
    <property type="method" value="EM"/>
    <property type="resolution" value="6.50 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK3">
    <property type="method" value="EM"/>
    <property type="resolution" value="6.30 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK4">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.00 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK5">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.80 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK6">
    <property type="method" value="EM"/>
    <property type="resolution" value="6.50 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK7">
    <property type="method" value="EM"/>
    <property type="resolution" value="6.70 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK8">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.70 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TK9">
    <property type="method" value="EM"/>
    <property type="resolution" value="6.00 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKA">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.10 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKB">
    <property type="method" value="EM"/>
    <property type="resolution" value="6.30 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKC">
    <property type="method" value="EM"/>
    <property type="resolution" value="5.80 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKD">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.70 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKE">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.10 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKF">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.10 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKG">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.50 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKH">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.40 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKI">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.10 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKJ">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.50 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKK">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.30 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKL">
    <property type="method" value="EM"/>
    <property type="resolution" value="6.40 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKM">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.50 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKN">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.10 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKO">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.80 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKP">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.60 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKQ">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.50 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKR">
    <property type="method" value="EM"/>
    <property type="resolution" value="6.50 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="7TKS">
    <property type="method" value="EM"/>
    <property type="resolution" value="7.50 A"/>
    <property type="chains" value="I=2-62"/>
  </dbReference>
  <dbReference type="PDB" id="8F29">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.00 A"/>
    <property type="chains" value="I=2-60"/>
  </dbReference>
  <dbReference type="PDB" id="8F39">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.50 A"/>
    <property type="chains" value="I=2-60"/>
  </dbReference>
  <dbReference type="PDB" id="8FKJ">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.20 A"/>
    <property type="chains" value="I=2-60"/>
  </dbReference>
  <dbReference type="PDB" id="8FL8">
    <property type="method" value="EM"/>
    <property type="resolution" value="4.20 A"/>
    <property type="chains" value="I=2-60"/>
  </dbReference>
  <dbReference type="PDBsum" id="2HLD"/>
  <dbReference type="PDBsum" id="2WPD"/>
  <dbReference type="PDBsum" id="3FKS"/>
  <dbReference type="PDBsum" id="3OE7"/>
  <dbReference type="PDBsum" id="3OEE"/>
  <dbReference type="PDBsum" id="3OEH"/>
  <dbReference type="PDBsum" id="3OFN"/>
  <dbReference type="PDBsum" id="3ZIA"/>
  <dbReference type="PDBsum" id="4B2Q"/>
  <dbReference type="PDBsum" id="6CP3"/>
  <dbReference type="PDBsum" id="6CP6"/>
  <dbReference type="PDBsum" id="7MD2"/>
  <dbReference type="PDBsum" id="7MD3"/>
  <dbReference type="PDBsum" id="7TJY"/>
  <dbReference type="PDBsum" id="7TJZ"/>
  <dbReference type="PDBsum" id="7TK0"/>
  <dbReference type="PDBsum" id="7TK1"/>
  <dbReference type="PDBsum" id="7TK2"/>
  <dbReference type="PDBsum" id="7TK3"/>
  <dbReference type="PDBsum" id="7TK4"/>
  <dbReference type="PDBsum" id="7TK5"/>
  <dbReference type="PDBsum" id="7TK6"/>
  <dbReference type="PDBsum" id="7TK7"/>
  <dbReference type="PDBsum" id="7TK8"/>
  <dbReference type="PDBsum" id="7TK9"/>
  <dbReference type="PDBsum" id="7TKA"/>
  <dbReference type="PDBsum" id="7TKB"/>
  <dbReference type="PDBsum" id="7TKC"/>
  <dbReference type="PDBsum" id="7TKD"/>
  <dbReference type="PDBsum" id="7TKE"/>
  <dbReference type="PDBsum" id="7TKF"/>
  <dbReference type="PDBsum" id="7TKG"/>
  <dbReference type="PDBsum" id="7TKH"/>
  <dbReference type="PDBsum" id="7TKI"/>
  <dbReference type="PDBsum" id="7TKJ"/>
  <dbReference type="PDBsum" id="7TKK"/>
  <dbReference type="PDBsum" id="7TKL"/>
  <dbReference type="PDBsum" id="7TKM"/>
  <dbReference type="PDBsum" id="7TKN"/>
  <dbReference type="PDBsum" id="7TKO"/>
  <dbReference type="PDBsum" id="7TKP"/>
  <dbReference type="PDBsum" id="7TKQ"/>
  <dbReference type="PDBsum" id="7TKR"/>
  <dbReference type="PDBsum" id="7TKS"/>
  <dbReference type="PDBsum" id="8F29"/>
  <dbReference type="PDBsum" id="8F39"/>
  <dbReference type="PDBsum" id="8FKJ"/>
  <dbReference type="PDBsum" id="8FL8"/>
  <dbReference type="AlphaFoldDB" id="P21306"/>
  <dbReference type="EMDB" id="EMD-23763"/>
  <dbReference type="EMDB" id="EMD-23764"/>
  <dbReference type="EMDB" id="EMD-25946"/>
  <dbReference type="EMDB" id="EMD-25947"/>
  <dbReference type="EMDB" id="EMD-25948"/>
  <dbReference type="EMDB" id="EMD-25949"/>
  <dbReference type="EMDB" id="EMD-25954"/>
  <dbReference type="EMDB" id="EMD-25955"/>
  <dbReference type="EMDB" id="EMD-25956"/>
  <dbReference type="EMDB" id="EMD-25957"/>
  <dbReference type="EMDB" id="EMD-25958"/>
  <dbReference type="EMDB" id="EMD-25959"/>
  <dbReference type="EMDB" id="EMD-25960"/>
  <dbReference type="EMDB" id="EMD-25961"/>
  <dbReference type="EMDB" id="EMD-25962"/>
  <dbReference type="EMDB" id="EMD-25963"/>
  <dbReference type="EMDB" id="EMD-25964"/>
  <dbReference type="EMDB" id="EMD-25965"/>
  <dbReference type="EMDB" id="EMD-25966"/>
  <dbReference type="EMDB" id="EMD-25967"/>
  <dbReference type="EMDB" id="EMD-25968"/>
  <dbReference type="EMDB" id="EMD-25969"/>
  <dbReference type="EMDB" id="EMD-25970"/>
  <dbReference type="EMDB" id="EMD-25971"/>
  <dbReference type="EMDB" id="EMD-25972"/>
  <dbReference type="EMDB" id="EMD-25973"/>
  <dbReference type="EMDB" id="EMD-25974"/>
  <dbReference type="EMDB" id="EMD-25975"/>
  <dbReference type="EMDB" id="EMD-25976"/>
  <dbReference type="EMDB" id="EMD-25977"/>
  <dbReference type="EMDB" id="EMD-25978"/>
  <dbReference type="EMDB" id="EMD-25979"/>
  <dbReference type="EMDB" id="EMD-25980"/>
  <dbReference type="EMDB" id="EMD-28809"/>
  <dbReference type="EMDB" id="EMD-28835"/>
  <dbReference type="EMDB" id="EMD-29250"/>
  <dbReference type="EMDB" id="EMD-29270"/>
  <dbReference type="EMDB" id="EMD-7546"/>
  <dbReference type="EMDB" id="EMD-7548"/>
  <dbReference type="SMR" id="P21306"/>
  <dbReference type="BioGRID" id="35942">
    <property type="interactions" value="93"/>
  </dbReference>
  <dbReference type="ComplexPortal" id="CPX-3281">
    <property type="entry name" value="Mitochondrial proton-transporting ATP synthase complex"/>
  </dbReference>
  <dbReference type="DIP" id="DIP-3031N"/>
  <dbReference type="IntAct" id="P21306">
    <property type="interactions" value="26"/>
  </dbReference>
  <dbReference type="MINT" id="P21306"/>
  <dbReference type="STRING" id="4932.YPL271W"/>
  <dbReference type="TCDB" id="3.A.2.1.3">
    <property type="family name" value="the h+- or na+-translocating f-type, v-type and a-type atpase (f-atpase) superfamily"/>
  </dbReference>
  <dbReference type="iPTMnet" id="P21306"/>
  <dbReference type="PaxDb" id="4932-YPL271W"/>
  <dbReference type="PeptideAtlas" id="P21306"/>
  <dbReference type="EnsemblFungi" id="YPL271W_mRNA">
    <property type="protein sequence ID" value="YPL271W"/>
    <property type="gene ID" value="YPL271W"/>
  </dbReference>
  <dbReference type="GeneID" id="855857"/>
  <dbReference type="KEGG" id="sce:YPL271W"/>
  <dbReference type="AGR" id="SGD:S000006192"/>
  <dbReference type="SGD" id="S000006192">
    <property type="gene designation" value="ATP15"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="FungiDB:YPL271W"/>
  <dbReference type="eggNOG" id="KOG3495">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_187039_1_0_1"/>
  <dbReference type="InParanoid" id="P21306"/>
  <dbReference type="OMA" id="YTKYEKG"/>
  <dbReference type="OrthoDB" id="1330818at2759"/>
  <dbReference type="BioCyc" id="YEAST:G3O-34153-MONOMER"/>
  <dbReference type="BioGRID-ORCS" id="855857">
    <property type="hits" value="10 hits in 10 CRISPR screens"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P21306"/>
  <dbReference type="PRO" id="PR:P21306"/>
  <dbReference type="Proteomes" id="UP000002311">
    <property type="component" value="Chromosome XVI"/>
  </dbReference>
  <dbReference type="RNAct" id="P21306">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005743">
    <property type="term" value="C:mitochondrial inner membrane"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="ComplexPortal"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="SGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045261">
    <property type="term" value="C:proton-transporting ATP synthase complex, catalytic core F(1)"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="SGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045269">
    <property type="term" value="C:proton-transporting ATP synthase, central stalk"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="SGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046933">
    <property type="term" value="F:proton-transporting ATP synthase activity, rotational mechanism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015986">
    <property type="term" value="P:proton motive force-driven ATP synthesis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="ComplexPortal"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042776">
    <property type="term" value="P:proton motive force-driven mitochondrial ATP synthesis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd12153">
    <property type="entry name" value="F1-ATPase_epsilon"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.1620.20:FF:000007">
    <property type="entry name" value="ATP synthase subunit epsilon, mitochondrial"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.1620.20">
    <property type="entry name" value="ATP synthase, F1 complex, epsilon subunit superfamily, mitochondrial"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006721">
    <property type="entry name" value="ATP_synth_F1_esu_mt"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036742">
    <property type="entry name" value="ATP_synth_F1_esu_sf_mt"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12448">
    <property type="entry name" value="ATP SYNTHASE EPSILON CHAIN, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12448:SF0">
    <property type="entry name" value="ATP SYNTHASE SUBUNIT EPSILON, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF04627">
    <property type="entry name" value="ATP-synt_Eps"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48690">
    <property type="entry name" value="Epsilon subunit of mitochondrial F1F0-ATP synthase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0066">ATP synthesis</keyword>
  <keyword id="KW-0139">CF(1)</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0375">Hydrogen ion transport</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0496">Mitochondrion</keyword>
  <keyword id="KW-0999">Mitochondrion inner membrane</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="initiator methionine" description="Removed" evidence="2">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000071667" description="ATP synthase subunit epsilon, mitochondrial">
    <location>
      <begin position="2"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphothreonine" evidence="4">
    <location>
      <position position="52"/>
    </location>
  </feature>
  <feature type="turn" evidence="5">
    <location>
      <begin position="4"/>
      <end position="6"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="11"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="28"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="33"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="44"/>
      <end position="47"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="14562106"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="1985960"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007744" key="4">
    <source>
      <dbReference type="PubMed" id="17761666"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="5">
    <source>
      <dbReference type="PDB" id="3ZIA"/>
    </source>
  </evidence>
  <sequence length="62" mass="6743" checksum="0CD754A9DEFFD08C" modified="2007-01-23" version="2">MSAWRKAGISYAAYLNVAAQAIRSSLKTELQTASVLNRSQTDAFYTQYKNGTAASEPTPITK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>