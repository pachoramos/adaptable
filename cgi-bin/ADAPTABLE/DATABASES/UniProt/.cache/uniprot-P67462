<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-11" modified="2024-11-27" version="104" xmlns="http://uniprot.org/uniprot">
  <accession>P67462</accession>
  <accession>Q99TX1</accession>
  <name>EX7S_STAAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Exodeoxyribonuclease 7 small subunit</fullName>
      <ecNumber evidence="1">3.1.11.6</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Exodeoxyribonuclease VII small subunit</fullName>
      <shortName evidence="1">Exonuclease VII small subunit</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">xseB</name>
    <name type="ordered locus">MW1475</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Bidirectionally degrades single-stranded DNA into large acid-insoluble oligonucleotides, which are then degraded further into small acid-soluble oligonucleotides.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>Exonucleolytic cleavage in either 5'- to 3'- or 3'- to 5'-direction to yield nucleoside 5'-phosphates.</text>
      <dbReference type="EC" id="3.1.11.6"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Heterooligomer composed of large and small subunits.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the XseB family.</text>
  </comment>
  <dbReference type="EC" id="3.1.11.6" evidence="1"/>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB95340.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000159865.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P67462"/>
  <dbReference type="SMR" id="P67462"/>
  <dbReference type="KEGG" id="sam:MW1475"/>
  <dbReference type="HOGENOM" id="CLU_145918_3_2_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009318">
    <property type="term" value="C:exodeoxyribonuclease VII complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008855">
    <property type="term" value="F:exodeoxyribonuclease VII activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006308">
    <property type="term" value="P:DNA catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.287.1040:FF:000006">
    <property type="entry name" value="Exodeoxyribonuclease 7 small subunit"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.287.1040">
    <property type="entry name" value="Exonuclease VII, small subunit"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00337">
    <property type="entry name" value="Exonuc_7_S"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003761">
    <property type="entry name" value="Exonuc_VII_S"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR037004">
    <property type="entry name" value="Exonuc_VII_ssu_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01280">
    <property type="entry name" value="xseB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34137">
    <property type="entry name" value="EXODEOXYRIBONUCLEASE 7 SMALL SUBUNIT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34137:SF1">
    <property type="entry name" value="EXODEOXYRIBONUCLEASE 7 SMALL SUBUNIT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02609">
    <property type="entry name" value="Exonuc_VII_S"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF006488">
    <property type="entry name" value="Exonuc_VII_S"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF116842">
    <property type="entry name" value="XseB-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0269">Exonuclease</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <feature type="chain" id="PRO_0000207008" description="Exodeoxyribonuclease 7 small subunit">
    <location>
      <begin position="1"/>
      <end position="76"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00337"/>
    </source>
  </evidence>
  <sequence length="76" mass="8760" checksum="96BB7039F1126E5B" modified="2004-10-11" version="1">MTKETQSFEEMMQELEQIVQKLDNETVSLEESLDLYQRGMKLSAACDTTLKNAEKKVNDLIKEEAEDVKNDESTDE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>