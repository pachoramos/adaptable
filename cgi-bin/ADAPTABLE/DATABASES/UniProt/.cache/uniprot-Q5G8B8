<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-04-04" modified="2024-11-27" version="47" xmlns="http://uniprot.org/uniprot">
  <accession>Q5G8B8</accession>
  <name>SCX7_TITCO</name>
  <protein>
    <recommendedName>
      <fullName>Toxin Tco-gamma</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>T-beta* NaTx6.4</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Toxin gamma-like</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Tityus costatus</name>
    <name type="common">Brazilian scorpion</name>
    <dbReference type="NCBI Taxonomy" id="309814"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Tityus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Toxicon" volume="45" first="273" last="283">
      <title>The Brazilian scorpion Tityus costatus Karsch: genes, peptides and function.</title>
      <authorList>
        <person name="Diego-Garcia E."/>
        <person name="Batista C.V.F."/>
        <person name="Garcia-Gomez B.I."/>
        <person name="Lucas S."/>
        <person name="Candido D.M."/>
        <person name="Gomez-Lagunas F."/>
        <person name="Possani L.D."/>
      </authorList>
      <dbReference type="PubMed" id="15683865"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2004.10.014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="PLoS ONE" volume="7" first="E30478" last="E30478">
      <title>Identification and phylogenetic analysis of Tityus pachyurus and Tityus obscurus novel putative Na+-channel scorpion toxins.</title>
      <authorList>
        <person name="Guerrero-Vargas J.A."/>
        <person name="Mourao C.B."/>
        <person name="Quintero-Hernandez V."/>
        <person name="Possani L.D."/>
        <person name="Schwartz E.F."/>
      </authorList>
      <dbReference type="PubMed" id="22355312"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0030478"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Beta toxins bind voltage-independently at site-4 of sodium channels (Nav) and shift the voltage of activation toward more negative potentials thereby affecting sodium channel activation and promoting spontaneous and repetitive firing.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="3">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AY740683">
    <property type="protein sequence ID" value="AAW72453.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5G8B8"/>
  <dbReference type="SMR" id="Q5G8B8"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000231502" description="Toxin Tco-gamma">
    <location>
      <begin position="1"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="2">
    <location>
      <begin position="1"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="modified residue" description="Cysteine amide" evidence="1">
    <location>
      <position position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="11"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="15"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="23"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="27"/>
      <end position="44"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="64" mass="7254" checksum="809B62D661FF3540" modified="2005-03-01" version="1">KEGYAMDHEGCKLSCFIRPSGYCGRECGYKKGSSGYCAWPACYCYGLPNWVKVWERATNRCGKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>