<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-26" modified="2022-12-14" version="52" xmlns="http://uniprot.org/uniprot">
  <accession>P60977</accession>
  <name>CALB_CALS5</name>
  <protein>
    <recommendedName>
      <fullName>U1-nemetoxin-Csp1b</fullName>
      <shortName>U1-NETX-Csp1b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Toxic peptide B</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Calisoga sp.</name>
    <name type="common">Spider</name>
    <dbReference type="NCBI Taxonomy" id="269418"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Mygalomorphae</taxon>
      <taxon>Nemesiidae</taxon>
      <taxon>Calisoga</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="patent" date="1997-11-18" number="US5688764">
      <title>Insecticidal peptides from spider venom.</title>
      <authorList>
        <person name="Johnson J.H."/>
        <person name="Kral R.M. Jr."/>
        <person name="Krapcho K."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TOXIC DOSE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Causes paralysis to insect larvae (H.virescens). This toxin is active only on insects.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="4287.89" method="Electrospray" evidence="3"/>
  <comment type="toxic dose">
    <text evidence="3">PD(50) is 3.70 +/- 0.19 mg/kg by injection into the abdomen of larvae of H.virescens.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the neurotoxin 13 (insecticidal toxin ABC) family. 02 (Calisoga) subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P60977"/>
  <dbReference type="SMR" id="P60977"/>
  <dbReference type="TCDB" id="8.B.21.1.3">
    <property type="family name" value="the spider insecticidal neurotoxin cyrtautoxin (cyrautoxin) family"/>
  </dbReference>
  <dbReference type="ArachnoServer" id="AS000029">
    <property type="toxin name" value="U1-nemetoxin-Csp1b"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012626">
    <property type="entry name" value="Spider_insecticidal_peptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08091">
    <property type="entry name" value="Toxin_21"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000045036" description="U1-nemetoxin-Csp1b">
    <location>
      <begin position="1"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="8"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="14"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="25"/>
      <end position="32"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P49268"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="39" mass="4296" checksum="34C7C761E84B8230" modified="2004-04-26" version="1">CISARYPCSNSKDCCSGNCGTFWTCFIRKDPCSKECLAP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>