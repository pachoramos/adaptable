<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-02-26" modified="2024-11-27" version="71" xmlns="http://uniprot.org/uniprot">
  <accession>A6QIG6</accession>
  <name>SCIN_STAAE</name>
  <protein>
    <recommendedName>
      <fullName>Staphylococcal complement inhibitor</fullName>
      <shortName>SCIN</shortName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">scn</name>
    <name type="ordered locus">NWMN_1876</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain Newman)</name>
    <dbReference type="NCBI Taxonomy" id="426430"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="J. Bacteriol." volume="190" first="300" last="310">
      <title>Genome sequence of Staphylococcus aureus strain Newman and comparative analysis of staphylococcal genomes: polymorphism and evolution of two major pathogenicity islands.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Bae T."/>
        <person name="Schneewind O."/>
        <person name="Takeuchi F."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="17951380"/>
      <dbReference type="DOI" id="10.1128/jb.01000-07"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Newman</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="J. Bacteriol." volume="188" first="1310" last="1315">
      <title>The innate immune modulators staphylococcal complement inhibitor and chemotaxis inhibitory protein of Staphylococcus aureus are located on beta-hemolysin-converting bacteriophages.</title>
      <authorList>
        <person name="van Wamel W.J.B."/>
        <person name="Rooijakkers S.H.M."/>
        <person name="Ruyken M."/>
        <person name="van Kessel K.P.M."/>
        <person name="van Strijp J.A.G."/>
      </authorList>
      <dbReference type="PubMed" id="16452413"/>
      <dbReference type="DOI" id="10.1128/jb.188.4.1310-1315.2006"/>
    </citation>
    <scope>PROPHAGE-ENCODED PROTEIN</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2006" name="Mol. Microbiol." volume="62" first="1035" last="1047">
      <title>Prophages of Staphylococcus aureus Newman and their contribution to virulence.</title>
      <authorList>
        <person name="Bae T."/>
        <person name="Baba T."/>
        <person name="Hiramatsu K."/>
        <person name="Schneewind O."/>
      </authorList>
      <dbReference type="PubMed" id="17078814"/>
      <dbReference type="DOI" id="10.1111/j.1365-2958.2006.05441.x"/>
    </citation>
    <scope>PROPHAGE-ENCODED PROTEIN</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Involved in countering the first line of host defense mechanisms. Efficiently inhibits opsonization, phagocytosis and killing of S.aureus by human neutrophils. Acts by binding and stabilizing human C3 convertases (C4b2a and C3bBb), leading to their inactivation. The convertases are no longer able to cleave complement C3, therefore preventing further C3b deposition on the bacterial surface and phagocytosis of the bacterium. Also prevents C5a-induced neutrophil responses (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text>Encoded within a prophage region.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the SCIN family.</text>
  </comment>
  <dbReference type="EMBL" id="AP009351">
    <property type="protein sequence ID" value="BAF68148.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000702263.1">
    <property type="nucleotide sequence ID" value="NZ_JBBIAE010000010.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A6QIG6"/>
  <dbReference type="BMRB" id="A6QIG6"/>
  <dbReference type="SMR" id="A6QIG6"/>
  <dbReference type="KEGG" id="sae:NWMN_1876"/>
  <dbReference type="HOGENOM" id="CLU_166895_0_0_9"/>
  <dbReference type="PRO" id="PR:A6QIG6"/>
  <dbReference type="Proteomes" id="UP000006386">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.1270.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029048">
    <property type="entry name" value="HSP70_C_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR021612">
    <property type="entry name" value="SCIN"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11546">
    <property type="entry name" value="CompInhib_SCIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000319875" description="Staphylococcal complement inhibitor">
    <location>
      <begin position="32"/>
      <end position="116"/>
    </location>
  </feature>
  <feature type="region of interest" description="Essential for activity" evidence="1">
    <location>
      <begin position="62"/>
      <end position="79"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="116" mass="13067" checksum="6C30E481973D19E9" modified="2007-08-21" version="1" precursor="true">MKIRKSILAGTLAIVLASPLVTNLDKNEAQASTSLPTSNEYQNEKLANELKSLLDELNVNELATGSLNTYYKRTIKISGQKAMYALKSKDFKKMSEAKYQLQKIYNEIDEALKSKY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>