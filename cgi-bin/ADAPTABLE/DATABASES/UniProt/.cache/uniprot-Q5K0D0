<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-03-15" modified="2022-05-25" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>Q5K0D0</accession>
  <name>O16C_CONVX</name>
  <protein>
    <recommendedName>
      <fullName>Conotoxin 12</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Conus vexillum</name>
    <name type="common">Flag cone</name>
    <dbReference type="NCBI Taxonomy" id="89431"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Caenogastropoda</taxon>
      <taxon>Neogastropoda</taxon>
      <taxon>Conoidea</taxon>
      <taxon>Conidae</taxon>
      <taxon>Conus</taxon>
      <taxon>Rhizoconus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Peptides" volume="26" first="361" last="367">
      <title>Direct cDNA cloning of novel conopeptide precursors of the O-superfamily.</title>
      <authorList>
        <person name="Kauferstein S."/>
        <person name="Melaun C."/>
        <person name="Mebs D."/>
      </authorList>
      <dbReference type="PubMed" id="15652641"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2004.10.027"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom duct</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom duct.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="domain">
    <text>The cysteine framework is VI/VII (C-C-CC-C-C).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the conotoxin O1 superfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AJ851178">
    <property type="protein sequence ID" value="CAH64851.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5K0D0"/>
  <dbReference type="SMR" id="Q5K0D0"/>
  <dbReference type="ConoServer" id="1067">
    <property type="toxin name" value="Conotoxin-12 precursor"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008200">
    <property type="term" value="F:ion channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004214">
    <property type="entry name" value="Conotoxin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02950">
    <property type="entry name" value="Conotoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000035007" evidence="1">
    <location>
      <begin position="23"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000035008" description="Conotoxin 12">
    <location>
      <begin position="48"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="49"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="56"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="61"/>
      <end position="77"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="79" mass="8519" checksum="5CC7A78B7E0E5C50" modified="2005-02-15" version="1" precursor="true">MKLTCVLIITVLFLTASQLITADYSRDQRQYRAVRLGDEMRNFKGARDCGGQGKGCYTQPCCPGLRCRGGGTGGGVCQP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>