<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2015-09-16" modified="2023-02-22" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>P0DMY2</accession>
  <name>BDS8_ANEVI</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Kappa-actitoxin-Avd4h</fullName>
      <shortName evidence="4">Kappa-AITX-Avd4h</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Antihypertensive protein BDS-8</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="3">Blood depressing substance 8</fullName>
      <shortName evidence="3">BDS-8</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Anemonia viridis</name>
    <name type="common">Snakelocks anemone</name>
    <dbReference type="NCBI Taxonomy" id="51769"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Anemonia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="BMC Genomics" volume="10" first="333" last="333">
      <title>Comprehensive EST analysis of the symbiotic sea anemone, Anemonia viridis.</title>
      <authorList>
        <person name="Sabourault C."/>
        <person name="Ganot P."/>
        <person name="Deleury E."/>
        <person name="Allemand D."/>
        <person name="Furla P."/>
      </authorList>
      <dbReference type="PubMed" id="19627569"/>
      <dbReference type="DOI" id="10.1186/1471-2164-10-333"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2011" name="BMC Genomics" volume="12" first="88" last="88">
      <title>The mining of toxin-like polypeptides from EST database by single residue distribution analysis.</title>
      <authorList>
        <person name="Kozlov S."/>
        <person name="Grishin E."/>
      </authorList>
      <dbReference type="PubMed" id="21281459"/>
      <dbReference type="DOI" id="10.1186/1471-2164-12-88"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2013" name="Mar. Drugs" volume="11" first="4213" last="4231">
      <title>Evidence of accelerated evolution and ectodermal-specific expression of presumptive BDS toxin cDNAs from Anemonia viridis.</title>
      <authorList>
        <person name="Nicosia A."/>
        <person name="Maggio T."/>
        <person name="Mazzola S."/>
        <person name="Cuttitta A."/>
      </authorList>
      <dbReference type="PubMed" id="24177670"/>
      <dbReference type="DOI" id="10.3390/md11114213"/>
    </citation>
    <scope>3D-STRUCTURE MODELING</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Blocks Kv3 voltage-gated potassium channels. Reduces blood pressure.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="5">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Experimental results show no expression in the ectodermal tissue from the distal and proximal tentacles, body wall, and oral disk. Since paralogs are expressed in this tissue, an expression of this toxin in this tissue is probable. The negative results could be explained by the very low abundance of EST sequences.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the sea anemone type 3 (BDS) potassium channel toxin family.</text>
  </comment>
  <comment type="caution">
    <text evidence="5">Opinions are divided on whether Anemonia viridis (Forsskal, 1775) and Anemonia sulcata (Pennant, 1777) are separate species.</text>
  </comment>
  <dbReference type="EMBL" id="FK723172">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0DMY2"/>
  <dbReference type="SMR" id="P0DMY2"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008200">
    <property type="term" value="F:ion channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008217">
    <property type="term" value="P:regulation of blood pressure"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012414">
    <property type="entry name" value="BDS_K_chnl_tox"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07936">
    <property type="entry name" value="Defensin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0382">Hypotensive agent</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000433660" evidence="1">
    <location>
      <begin position="20"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000433661" description="Kappa-actitoxin-Avd4h">
    <location>
      <begin position="34"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="38"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="40"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="56"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P11494"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="21281459"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="24177670"/>
    </source>
  </evidence>
  <sequence length="77" mass="8428" checksum="B1EFC17F675A8E4E" modified="2015-09-16" version="1" precursor="true">MNKALFLCLVVLCAAVVFAAEDLQKAKHAPFKRGAAPCFCPGKADRGDLWILRGTCPGGYGYTSNCYNWPNICCYPH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>