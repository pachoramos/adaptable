<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-03-03" modified="2022-12-14" version="24" xmlns="http://uniprot.org/uniprot">
  <accession>P0C8T4</accession>
  <name>BR2B_PULPI</name>
  <protein>
    <recommendedName>
      <fullName>Brevinin-2PTb</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pulchrana picturata</name>
    <name type="common">Malaysian fire frog</name>
    <name type="synonym">Hylarana picturata</name>
    <dbReference type="NCBI Taxonomy" id="395594"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Pulchrana</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Toxicon" volume="52" first="465" last="473">
      <title>Characterization of antimicrobial peptides from the skin secretions of the Malaysian frogs, Odorrana hosii and Hylarana picturata (Anura:Ranidae).</title>
      <authorList>
        <person name="Conlon J.M."/>
        <person name="Kolodziejek J."/>
        <person name="Nowotny N."/>
        <person name="Leprince J."/>
        <person name="Vaudry H."/>
        <person name="Coquet L."/>
        <person name="Jouenne T."/>
        <person name="King J.D."/>
      </authorList>
      <dbReference type="PubMed" id="18621071"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2008.06.017"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antibacterial activity against the Gram-positive bacterium S.aureus ATCC 25923 (MIC=9 uM) and the Gram-negative bacterium E.coli ATCC 25726 (MIC=9 uM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3372.9" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0C8T4"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000366042" description="Brevinin-2PTb">
    <location>
      <begin position="1"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="27"/>
      <end position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="18621071"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="33" mass="3377" checksum="4B31325738BE8DA5" modified="2009-03-03" version="1">GFKGAFKNVMFGIAKSAGKSALNALACKIDKSC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>