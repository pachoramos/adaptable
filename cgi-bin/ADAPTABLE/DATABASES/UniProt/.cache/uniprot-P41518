<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2021-06-02" version="43" xmlns="http://uniprot.org/uniprot">
  <accession>P41518</accession>
  <name>TKC2_CALVO</name>
  <protein>
    <recommendedName>
      <fullName>Callitachykinin-2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Callitachykinin II</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Calliphora vomitoria</name>
    <name type="common">Blue bottle fly</name>
    <name type="synonym">Musca vomitoria</name>
    <dbReference type="NCBI Taxonomy" id="27454"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Oestroidea</taxon>
      <taxon>Calliphoridae</taxon>
      <taxon>Calliphorinae</taxon>
      <taxon>Calliphora</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Peptides" volume="15" first="761" last="768">
      <title>Callitachykinin I and II, two novel myotropic peptides isolated from the blowfly, Calliphora vomitoria, that have resemblances to tachykinins.</title>
      <authorList>
        <person name="Lundquist C.T."/>
        <person name="Clottens F.L."/>
        <person name="Holman G.M."/>
        <person name="Nichols R."/>
        <person name="Nachman R.J."/>
        <person name="Naessel D.R."/>
      </authorList>
      <dbReference type="PubMed" id="7984492"/>
      <dbReference type="DOI" id="10.1016/0196-9781(94)90027-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT ARG-11</scope>
    <scope>SYNTHESIS</scope>
  </reference>
  <comment type="function">
    <text>Myoactive peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044437" description="Callitachykinin-2">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="Arginine amide" evidence="1">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="7984492"/>
    </source>
  </evidence>
  <sequence length="11" mass="1103" checksum="15D7E3F9C9CDD444" modified="1995-11-01" version="1">GLGNNAFVGVR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>