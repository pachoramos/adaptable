<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-11-08" modified="2024-05-29" version="65" xmlns="http://uniprot.org/uniprot">
  <accession>P59084</accession>
  <name>BDS2_ANESU</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">DeltaKappa-actitoxin-Avd4b</fullName>
      <shortName evidence="5">DeltaKappa-AITX-Avd4b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Antihypertensive protein BDS-2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="6">Blood depressing substance II</fullName>
      <shortName evidence="6">BDS-II</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Anemonia sulcata</name>
    <name type="common">Mediterranean snakelocks sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="6108"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Anemonia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="patent" date="1985-01-17" number="DE3324689">
      <title>Polypeptides, process for their preparation, and their use as hypotensive active compounds.</title>
      <authorList>
        <person name="Beress L."/>
        <person name="Doppelfeld I.-S."/>
        <person name="Etschenberg E."/>
        <person name="Graf E."/>
        <person name="Henschen-Edman A."/>
        <person name="Zwick J."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="J. Biol. Chem." volume="273" first="6744" last="6749">
      <title>Sea anemone peptides with a specific blocking activity against the fast inactivating potassium channel Kv3.4.</title>
      <authorList>
        <person name="Diochot S."/>
        <person name="Schweitz H."/>
        <person name="Beress L."/>
        <person name="Lazdunski M."/>
      </authorList>
      <dbReference type="PubMed" id="9506974"/>
      <dbReference type="DOI" id="10.1074/jbc.273.12.6744"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2005" name="J. Neurosci." volume="25" first="8735" last="8745">
      <title>Modulation of Kv3 subfamily potassium currents by the sea anemone toxin BDS: significance for CNS and biophysical studies.</title>
      <authorList>
        <person name="Yeung S.Y."/>
        <person name="Thompson D."/>
        <person name="Wang Z."/>
        <person name="Fedida D."/>
        <person name="Robertson B."/>
      </authorList>
      <dbReference type="PubMed" id="16177043"/>
      <dbReference type="DOI" id="10.1523/jneurosci.2119-05.2005"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3 4">Acts as a gating modifier on both Kv and Nav ion channels. Voltage-dependently inhibits voltage-gated potassium channels Kv3 (Kv3.1/KCNC1, Kv3.2/KCNC2 and Kv3.4/KCNC4) (PubMed:16177043, PubMed:9506974). Slows inactivation of the voltage-gated sodium channel Nav1.7/SCN9A (By similarity). Inhibits all Kv3.1, Kv3.2 and Kv3.4 by about 50% when tested at a voltage of +40 mV (PubMed:16177043). May act by binding residues in voltage-sensing domains S3b and S4 of Kv3 (PubMed:16177043). Tests have been done on human Nav1.7/SCN9A and rat SCG neurons that mostly carry Nav1.7 channels (EC(50)=300 nM) (By similarity). This toxin also reduces blood pressure (Ref.1).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="7">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="7">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the sea anemone type 3 (BDS) potassium channel toxin family.</text>
  </comment>
  <comment type="caution">
    <text evidence="7">Opinions are divided on whether Anemonia viridis (Forsskal, 1775) and Anemonia sulcata (Pennant, 1777) are separate species.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P59084"/>
  <dbReference type="SMR" id="P59084"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008200">
    <property type="term" value="F:ion channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012414">
    <property type="entry name" value="BDS_K_chnl_tox"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07936">
    <property type="entry name" value="Defensin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000221542" description="DeltaKappa-actitoxin-Avd4b" evidence="3 4">
    <location>
      <begin position="1"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="6"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="22"/>
      <end position="40"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P11494"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="16177043"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="9506974"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="9506974"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="43" mass="4782" checksum="7EEAABBE8A1FDE08" modified="2002-11-08" version="1">AAPCFCPGKPDRGDLWILRGTCPGGYGYTSNCYKWPNICCYPH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>