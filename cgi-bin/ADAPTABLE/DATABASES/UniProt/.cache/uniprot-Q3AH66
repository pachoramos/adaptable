<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-07-24" modified="2024-03-27" version="83" xmlns="http://uniprot.org/uniprot">
  <accession>Q3AH66</accession>
  <name>PSBU_SYNSC</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Photosystem II extrinsic protein U</fullName>
      <shortName evidence="1">PSII-U</shortName>
      <shortName evidence="1">PsbU</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Photosystem II 12 kDa extrinsic protein</fullName>
      <shortName evidence="1">PS II complex 12 kDa extrinsic protein</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">psbU</name>
    <name type="ordered locus">Syncc9605_2334</name>
  </gene>
  <organism>
    <name type="scientific">Synechococcus sp. (strain CC9605)</name>
    <dbReference type="NCBI Taxonomy" id="110662"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Cyanobacteriota</taxon>
      <taxon>Cyanophyceae</taxon>
      <taxon>Synechococcales</taxon>
      <taxon>Synechococcaceae</taxon>
      <taxon>Synechococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2005-07" db="EMBL/GenBank/DDBJ databases">
      <title>Complete sequence of Synechococcus sp. CC9605.</title>
      <authorList>
        <consortium name="US DOE Joint Genome Institute"/>
        <person name="Copeland A."/>
        <person name="Lucas S."/>
        <person name="Lapidus A."/>
        <person name="Barry K."/>
        <person name="Detter J.C."/>
        <person name="Glavina T."/>
        <person name="Hammon N."/>
        <person name="Israni S."/>
        <person name="Pitluck S."/>
        <person name="Schmutz J."/>
        <person name="Martinez M."/>
        <person name="Larimer F."/>
        <person name="Land M."/>
        <person name="Kyrpides N."/>
        <person name="Ivanova N."/>
        <person name="Richardson P."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CC9605</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">One of the extrinsic, lumenal subunits of photosystem II (PSII). PSII is a light-driven water plastoquinone oxidoreductase, using light energy to abstract electrons from H(2)O, generating a proton gradient subsequently used for ATP formation. The extrinsic proteins stabilize the structure of photosystem II oxygen-evolving complex (OEC), the ion environment of oxygen evolution and protect the OEC against heat-induced inactivation.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">PSII is composed of 1 copy each of membrane proteins PsbA, PsbB, PsbC, PsbD, PsbE, PsbF, PsbH, PsbI, PsbJ, PsbK, PsbL, PsbM, PsbT, PsbX, PsbY, PsbZ, Psb30/Ycf12, peripheral proteins PsbO, CyanoQ (PsbQ), PsbU, PsbV and a large number of cofactors. It forms dimeric complexes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cellular thylakoid membrane</location>
      <topology evidence="1">Peripheral membrane protein</topology>
      <orientation evidence="1">Lumenal side</orientation>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the PsbU family.</text>
  </comment>
  <dbReference type="EMBL" id="CP000110">
    <property type="protein sequence ID" value="ABB36066.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_011365264.1">
    <property type="nucleotide sequence ID" value="NC_007516.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q3AH66"/>
  <dbReference type="SMR" id="Q3AH66"/>
  <dbReference type="STRING" id="110662.Syncc9605_2334"/>
  <dbReference type="KEGG" id="syd:Syncc9605_2334"/>
  <dbReference type="eggNOG" id="COG1555">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_141240_1_0_3"/>
  <dbReference type="OrthoDB" id="463369at2"/>
  <dbReference type="GO" id="GO:0019898">
    <property type="term" value="C:extrinsic component of membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009654">
    <property type="term" value="C:photosystem II oxygen evolving complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031676">
    <property type="term" value="C:plasma membrane-derived thylakoid membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015979">
    <property type="term" value="P:photosynthesis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042549">
    <property type="term" value="P:photosystem II stabilization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.150.320">
    <property type="entry name" value="Photosystem II 12 kDa extrinsic protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00589">
    <property type="entry name" value="PSII_PsbU"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010527">
    <property type="entry name" value="PSII_PsbU"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06514">
    <property type="entry name" value="PsbU"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF81585">
    <property type="entry name" value="PsbU/PolX domain-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0249">Electron transport</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0602">Photosynthesis</keyword>
  <keyword id="KW-0604">Photosystem II</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0793">Thylakoid</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5000102464" description="Photosystem II extrinsic protein U">
    <location>
      <begin position="30"/>
      <end position="135"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00589"/>
    </source>
  </evidence>
  <sequence length="135" mass="15082" checksum="EFD837B84843804F" modified="2005-11-22" version="1" precursor="true">MKRLLSWLTGALVMASLMAGLVMPSSVYADDDLRNKYSGNEIRNIADDKIAARDGKVDLNNSSVRRFQQFPGMYPTMAGKIVLGGPYDSVDDVLSLDLTERQQELFAKYRDNFTVTPPSIALNEGDDRINDGQYR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>