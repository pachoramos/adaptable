<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-06-20" modified="2023-02-22" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKS5</accession>
  <name>IRP4_AGRIP</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Insulin-related peptide 4</fullName>
      <shortName evidence="3">IRP-4</shortName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="5">DAGWWLTRGAARSLGGVR-amide</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Agrotis ipsilon</name>
    <name type="common">Black cutworm moth</name>
    <dbReference type="NCBI Taxonomy" id="56364"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Noctuoidea</taxon>
      <taxon>Noctuidae</taxon>
      <taxon>Noctuinae</taxon>
      <taxon>Noctuini</taxon>
      <taxon>Agrotis</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2018" name="J. Proteome Res." volume="17" first="1397" last="1414">
      <title>Mating-induced differential peptidomics of neuropeptides and protein hormones in Agrotis ipsilon moths.</title>
      <authorList>
        <person name="Diesner M."/>
        <person name="Gallot A."/>
        <person name="Binz H."/>
        <person name="Gaertner C."/>
        <person name="Vitecek S."/>
        <person name="Kahnt J."/>
        <person name="Schachtner J."/>
        <person name="Jacquin-Joly E."/>
        <person name="Gadenne C."/>
      </authorList>
      <dbReference type="PubMed" id="29466015"/>
      <dbReference type="DOI" id="10.1021/acs.jproteome.7b00779"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 48-65</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT ARG-65</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">DAGWWLTRGAARSLGGVR-amide: Expressed in corpora cardiaca (CC), corpora allata (CA), antennal lobe (AL) and gnathal ganglion (GNG) (at protein level). Expression in CC and CA detected in most animals, in AL and GNG in few animals (at protein level).</text>
  </comment>
  <comment type="mass spectrometry" mass="1928.03" error="0.01" method="MALDI" evidence="2">
    <text>DAGWWLTRGAARSLGGVR-amide.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the insulin family.</text>
  </comment>
  <comment type="caution">
    <text evidence="4">Further mature peptides might exist.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HKS5"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd04366">
    <property type="entry name" value="IlGF_insulin_bombyxin_like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.100.10">
    <property type="entry name" value="Insulin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016179">
    <property type="entry name" value="Insulin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036438">
    <property type="entry name" value="Insulin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022353">
    <property type="entry name" value="Insulin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022352">
    <property type="entry name" value="Insulin_family"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00049">
    <property type="entry name" value="Insulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00276">
    <property type="entry name" value="INSULINFAMLY"/>
  </dbReference>
  <dbReference type="SMART" id="SM00078">
    <property type="entry name" value="IlGF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF56994">
    <property type="entry name" value="Insulin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00262">
    <property type="entry name" value="INSULIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444523" evidence="5">
    <location>
      <begin position="20"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000444524" description="DAGWWLTRGAARSLGGVR-amide" evidence="2">
    <location>
      <begin position="48"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444525" evidence="5">
    <location>
      <begin position="69"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="modified residue" description="Arginine amide" evidence="2">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <sequence length="88" mass="9682" checksum="085811083EDD7B6E" modified="2018-06-20" version="1" precursor="true">MKLTLIILLVVAYSWCSEAQNEARVFCGRVLSERLAALCWGPNSVKRDAGWWLTRGAARSLGGVRGKRGLATECCDKACTVEELLSYC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>