<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-04-14" modified="2024-11-27" version="78" xmlns="http://uniprot.org/uniprot">
  <accession>B2INM2</accession>
  <name>RL9_STRPS</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Large ribosomal subunit protein bL9</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">50S ribosomal protein L9</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rplI</name>
    <name type="ordered locus">SPCG_2171</name>
  </gene>
  <organism>
    <name type="scientific">Streptococcus pneumoniae (strain CGSP14)</name>
    <dbReference type="NCBI Taxonomy" id="516950"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Streptococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="BMC Genomics" volume="10" first="158" last="158">
      <title>Genome evolution driven by host adaptations results in a more virulent and antimicrobial-resistant Streptococcus pneumoniae serotype 14.</title>
      <authorList>
        <person name="Ding F."/>
        <person name="Tang P."/>
        <person name="Hsu M.-H."/>
        <person name="Cui P."/>
        <person name="Hu S."/>
        <person name="Yu J."/>
        <person name="Chiu C.-H."/>
      </authorList>
      <dbReference type="PubMed" id="19361343"/>
      <dbReference type="DOI" id="10.1186/1471-2164-10-158"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CGSP14</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Binds to the 23S rRNA.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the bacterial ribosomal protein bL9 family.</text>
  </comment>
  <dbReference type="EMBL" id="CP001033">
    <property type="protein sequence ID" value="ACB91423.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000864220.1">
    <property type="nucleotide sequence ID" value="NC_010582.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B2INM2"/>
  <dbReference type="SMR" id="B2INM2"/>
  <dbReference type="GeneID" id="66807278"/>
  <dbReference type="KEGG" id="spw:SPCG_2171"/>
  <dbReference type="HOGENOM" id="CLU_078938_3_2_9"/>
  <dbReference type="GO" id="GO:0022625">
    <property type="term" value="C:cytosolic large ribosomal subunit"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019843">
    <property type="term" value="F:rRNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006412">
    <property type="term" value="P:translation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.430.100:FF:000009">
    <property type="entry name" value="50S ribosomal protein L9"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.40.5.10:FF:000002">
    <property type="entry name" value="50S ribosomal protein L9"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.430.100">
    <property type="entry name" value="Ribosomal protein L9, C-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.5.10">
    <property type="entry name" value="Ribosomal protein L9, N-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00503">
    <property type="entry name" value="Ribosomal_bL9"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000244">
    <property type="entry name" value="Ribosomal_bL9"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009027">
    <property type="entry name" value="Ribosomal_bL9/RNase_H1_N"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020594">
    <property type="entry name" value="Ribosomal_bL9_bac/chp"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020069">
    <property type="entry name" value="Ribosomal_bL9_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036791">
    <property type="entry name" value="Ribosomal_bL9_C_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020070">
    <property type="entry name" value="Ribosomal_bL9_N"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036935">
    <property type="entry name" value="Ribosomal_bL9_N_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00158">
    <property type="entry name" value="L9"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21368:SF18">
    <property type="entry name" value="39S RIBOSOMAL PROTEIN L9, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21368">
    <property type="entry name" value="50S RIBOSOMAL PROTEIN L9"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03948">
    <property type="entry name" value="Ribosomal_L9_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01281">
    <property type="entry name" value="Ribosomal_L9_N"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55658">
    <property type="entry name" value="L9 N-domain-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55653">
    <property type="entry name" value="Ribosomal protein L9 C-domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00651">
    <property type="entry name" value="RIBOSOMAL_L9"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0699">rRNA-binding</keyword>
  <feature type="chain" id="PRO_1000126980" description="Large ribosomal subunit protein bL9">
    <location>
      <begin position="1"/>
      <end position="150"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00503"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="150" mass="16523" checksum="31EDBD26CF0662F1" modified="2008-06-10" version="1">MKVIFLADVKGKGKKGEIKEVPTGYAQNFLIKKNLAKEATAQAVGELRGKQKSEEKAHAEMIAEGKAIKAQLEAEETVVEFVEKVGPDGRTFGSITNKKIAEELQKQFGIKIDKRHIQVQAPIRAVGLIDVPVKIYQDITSVINLRVKEG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>