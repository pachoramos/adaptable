<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-01-04" modified="2024-11-27" version="104" xmlns="http://uniprot.org/uniprot">
  <accession>P69989</accession>
  <accession>Q66A44</accession>
  <accession>Q9AM39</accession>
  <name>SLYA_YERPS</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Transcriptional regulator SlyA</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Regulator of virulence protein A</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Transcriptional regulator for cryptic hemolysin</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">slyA</name>
    <name type="synonym">rovA</name>
    <name type="ordered locus">YPTB2288</name>
  </gene>
  <organism>
    <name type="scientific">Yersinia pseudotuberculosis serotype I (strain IP32953)</name>
    <dbReference type="NCBI Taxonomy" id="273123"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Yersiniaceae</taxon>
      <taxon>Yersinia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Mol. Microbiol." volume="41" first="1249" last="1269">
      <title>Environmental control of invasin expression in Yersinia pseudotuberculosis is mediated by regulation of RovA, a transcriptional activator of the SlyA/Hor family.</title>
      <authorList>
        <person name="Nagel G."/>
        <person name="Lahrz A."/>
        <person name="Dersch P."/>
      </authorList>
      <dbReference type="PubMed" id="11580832"/>
      <dbReference type="DOI" id="10.1046/j.1365-2958.2001.02522.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Proc. Natl. Acad. Sci. U.S.A." volume="101" first="13826" last="13831">
      <title>Insights into the evolution of Yersinia pestis through whole-genome comparison with Yersinia pseudotuberculosis.</title>
      <authorList>
        <person name="Chain P.S.G."/>
        <person name="Carniel E."/>
        <person name="Larimer F.W."/>
        <person name="Lamerdin J."/>
        <person name="Stoutland P.O."/>
        <person name="Regala W.M."/>
        <person name="Georgescu A.M."/>
        <person name="Vergez L.M."/>
        <person name="Land M.L."/>
        <person name="Motin V.L."/>
        <person name="Brubaker R.R."/>
        <person name="Fowler J."/>
        <person name="Hinnebusch J."/>
        <person name="Marceau M."/>
        <person name="Medigue C."/>
        <person name="Simonet M."/>
        <person name="Chenal-Francisque V."/>
        <person name="Souza B."/>
        <person name="Dacheux D."/>
        <person name="Elliott J.M."/>
        <person name="Derbise A."/>
        <person name="Hauser L.J."/>
        <person name="Garcia E."/>
      </authorList>
      <dbReference type="PubMed" id="15358858"/>
      <dbReference type="DOI" id="10.1073/pnas.0404012101"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>IP32953</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Transcription regulator that can specifically activate or repress expression of target genes. Required for expression of the virulence gene inv.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer.</text>
  </comment>
  <comment type="induction">
    <text evidence="2">Autoregulated.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the SlyA family.</text>
  </comment>
  <dbReference type="EMBL" id="AF330139">
    <property type="protein sequence ID" value="AAK01704.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BX936398">
    <property type="protein sequence ID" value="CAH21526.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_002210955.1">
    <property type="nucleotide sequence ID" value="NZ_CP009712.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P69989"/>
  <dbReference type="SMR" id="P69989"/>
  <dbReference type="GeneID" id="66841281"/>
  <dbReference type="KEGG" id="ypo:BZ17_172"/>
  <dbReference type="KEGG" id="yps:YPTB2288"/>
  <dbReference type="PATRIC" id="fig|273123.14.peg.179"/>
  <dbReference type="Proteomes" id="UP000001011">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003700">
    <property type="term" value="F:DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006950">
    <property type="term" value="P:response to stress"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.10.10:FF:000261">
    <property type="entry name" value="Transcriptional regulator SlyA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.10.10">
    <property type="entry name" value="Winged helix-like DNA-binding domain superfamily/Winged helix DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01819">
    <property type="entry name" value="HTH_type_SlyA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000835">
    <property type="entry name" value="HTH_MarR-typ"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039422">
    <property type="entry name" value="MarR/SlyA-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023187">
    <property type="entry name" value="Tscrpt_reg_MarR-type_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023071">
    <property type="entry name" value="Tscrpt_reg_SlyA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036388">
    <property type="entry name" value="WH-like_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036390">
    <property type="entry name" value="WH_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33164:SF64">
    <property type="entry name" value="TRANSCRIPTIONAL REGULATOR SLYA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33164">
    <property type="entry name" value="TRANSCRIPTIONAL REGULATOR, MARR FAMILY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01047">
    <property type="entry name" value="MarR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00598">
    <property type="entry name" value="HTHMARR"/>
  </dbReference>
  <dbReference type="SMART" id="SM00347">
    <property type="entry name" value="HTH_MARR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46785">
    <property type="entry name" value="Winged helix' DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01117">
    <property type="entry name" value="HTH_MARR_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50995">
    <property type="entry name" value="HTH_MARR_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0010">Activator</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0678">Repressor</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="chain" id="PRO_0000054399" description="Transcriptional regulator SlyA">
    <location>
      <begin position="1"/>
      <end position="143"/>
    </location>
  </feature>
  <feature type="domain" description="HTH marR-type" evidence="1">
    <location>
      <begin position="2"/>
      <end position="135"/>
    </location>
  </feature>
  <feature type="DNA-binding region" description="H-T-H motif" evidence="1">
    <location>
      <begin position="49"/>
      <end position="72"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01819"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11580832"/>
    </source>
  </evidence>
  <sequence length="143" mass="16170" checksum="62485065D1F5A451" modified="2005-01-04" version="1">MESTLGSDLARLVRVWRALIDHRLKPLELTQTHWVTLYNINRLPPEQSQIQLAKAIGIEQPSLVRTLDQLEEKGLITRHTCANDRRAKRIKLTEQSSPIIEQVDGVICSTRKEILGGISSDEIAVLSGLIDKLEKNIIQLQTK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>