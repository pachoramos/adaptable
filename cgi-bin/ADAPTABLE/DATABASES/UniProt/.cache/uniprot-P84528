<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-07-19" modified="2022-05-25" version="18" xmlns="http://uniprot.org/uniprot">
  <accession>P84528</accession>
  <name>RNAM_PLESA</name>
  <protein>
    <recommendedName>
      <fullName>Antimicrobial ribonuclease</fullName>
      <ecNumber>3.1.-.-</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pleurotus sajor-caju</name>
    <name type="common">Oyster mushroom</name>
    <dbReference type="NCBI Taxonomy" id="50053"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Basidiomycota</taxon>
      <taxon>Agaricomycotina</taxon>
      <taxon>Agaricomycetes</taxon>
      <taxon>Polyporales</taxon>
      <taxon>Polyporaceae</taxon>
      <taxon>Lentinus</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2004" name="Peptides" volume="25" first="11" last="17">
      <title>A ribonuclease with antimicrobial, antimitogenic and antiproliferative activities from the edible mushroom Pleurotus sajor-caju.</title>
      <authorList>
        <person name="Ngai P.H.K."/>
        <person name="Ng T.B."/>
      </authorList>
      <dbReference type="PubMed" id="15003351"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2003.11.012"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>SUBUNIT</scope>
    <source>
      <tissue evidence="1">Fruiting body</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has ribonuclease activity towards yeast tRNA and polyuracil and weak ribonuclease activity towards other polyhomoribonucleotides. Has weak deoxyribonuclease activity. Displays antimitogenic activity towards murine splenocytes and antiproliferative activity towards HepG2 hepatoma cells and L1210 leukemia cells. Has antibacterial activity against Gram-negative bacteria P.aeruginosa and P.fluorescens, and the Gram-positive bacterium S.aureus. No antibacterial activity against the Gram-positive bacteria B.subtilis, B.cereus, B.megaterium, M.luteus and M.phei, or the Gram-negative bacteria E.coli and E.aerogenes. Has antifungal activity against F.oxysporum and M.oxysporum.</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="1">Inhibited by sodium ion, potassium ion, magnesium ion, calcium ion, Cu(2+) ion, zinc ion and Fe(3+) ion.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <phDependence>
      <text evidence="1">Optimum pH is from 5.5 to 6.0.</text>
    </phDependence>
    <temperatureDependence>
      <text evidence="1">Optimum temperature below 60 degrees Celsius. Active from 0 to 100 degrees Celsius.</text>
    </temperatureDependence>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer.</text>
  </comment>
  <dbReference type="EC" id="3.1.-.-"/>
  <dbReference type="GO" id="GO:0016787">
    <property type="term" value="F:hydrolase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <feature type="chain" id="PRO_0000137380" description="Antimicrobial ribonuclease">
    <location>
      <begin position="1"/>
      <end position="10" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="2">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="15003351"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="15003351"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="10" mass="1016" checksum="33AF89C4087DDB18" modified="2005-07-19" version="1" fragment="single">DNGEAGRAAR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>