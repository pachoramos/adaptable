<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-11-01" modified="2024-01-24" version="111" xmlns="http://uniprot.org/uniprot">
  <accession>P04142</accession>
  <name>CECB_BOMMO</name>
  <protein>
    <recommendedName>
      <fullName>Cecropin-B</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Lepidopteran-A/B</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CECB1</name>
  </gene>
  <gene>
    <name type="primary">CECB2</name>
  </gene>
  <organism>
    <name type="scientific">Bombyx mori</name>
    <name type="common">Silk moth</name>
    <dbReference type="NCBI Taxonomy" id="7091"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Bombycoidea</taxon>
      <taxon>Bombycidae</taxon>
      <taxon>Bombycinae</taxon>
      <taxon>Bombyx</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Gene" volume="163" first="215" last="219">
      <title>Structure of two cecropin B-encoding genes and bacteria-inducible DNA-binding proteins which bind to the 5'-upstream regulatory region in the silkworm, Bombyx mori.</title>
      <authorList>
        <person name="Taniai K."/>
        <person name="Kadono-Okuda K."/>
        <person name="Kato Y."/>
        <person name="Yamamoto M."/>
        <person name="Shimabukuro M."/>
        <person name="Chowdhury S."/>
        <person name="Xu J."/>
        <person name="Kotani E."/>
        <person name="Tomino S."/>
        <person name="Yamakawa M."/>
      </authorList>
      <dbReference type="PubMed" id="7590269"/>
      <dbReference type="DOI" id="10.1016/0378-1119(95)00408-x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>Tokai X Asahi</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1992" name="Biochim. Biophys. Acta" volume="1132" first="203" last="206">
      <title>Isolation and nucleotide sequence of cecropin B cDNA clones from the silkworm, Bombyx mori.</title>
      <authorList>
        <person name="Taniai K."/>
        <person name="Kato Y."/>
        <person name="Hirochika H."/>
        <person name="Yamakawa M."/>
      </authorList>
      <dbReference type="PubMed" id="1390892"/>
      <dbReference type="DOI" id="10.1016/0167-4781(92)90013-p"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1993" name="Insect Biochem. Mol. Biol." volume="23" first="285" last="290">
      <title>Expression and characterization of cDNAs for cecropin B, an antibacterial protein of the silkworm, Bombyx mori.</title>
      <authorList>
        <person name="Kato Y."/>
        <person name="Taniai K."/>
        <person name="Hirochika H."/>
        <person name="Yamakawa M."/>
      </authorList>
      <dbReference type="PubMed" id="8485525"/>
      <dbReference type="DOI" id="10.1016/0965-1748(93)90009-h"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1994" name="Biosci. Biotechnol. Biochem." volume="58" first="1476" last="1478">
      <title>Cloning of cDNAs for cecropins A and B, and expression of the genes in the silkworm, Bombyx mori.</title>
      <authorList>
        <person name="Yamano Y."/>
        <person name="Matsumoto M."/>
        <person name="Inoue K."/>
        <person name="Kawabata T."/>
        <person name="Morishima I."/>
      </authorList>
      <dbReference type="PubMed" id="7765280"/>
      <dbReference type="DOI" id="10.1271/bbb.58.1476"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE</scope>
    <source>
      <strain>C108</strain>
      <tissue>Larval fat body</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1986" name="Tetrahedron" volume="42" first="829" last="834">
      <title>Structure determination of lepidopteran, self-defense substance produced by silkworm.</title>
      <authorList>
        <person name="Teshima T."/>
        <person name="Ueki Y."/>
        <person name="Nakai T."/>
        <person name="Shiba T."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE OF 27-61</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1990" name="Comp. Biochem. Physiol." volume="95B" first="551" last="554">
      <title>Isolation and structure of cecropins, inducible antibacterial peptides, from the silkworm, Bombyx mori.</title>
      <authorList>
        <person name="Morishima I."/>
        <person name="Suginaka S."/>
        <person name="Ueno T."/>
        <person name="Hirano H."/>
      </authorList>
      <dbReference type="PubMed" id="2184991"/>
      <dbReference type="DOI" id="10.1016/0305-0491(90)90019-p"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 27-61</scope>
    <scope>HYDROXYLATION AT LYS-47</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1994" name="Anal. Biochem." volume="217" first="231" last="235">
      <title>Isolation and identification of cecropin antibacterial peptides from the extracellular matrix of the insect integument.</title>
      <authorList>
        <person name="Lee W.-J."/>
        <person name="Brey P.T."/>
      </authorList>
      <dbReference type="PubMed" id="8203751"/>
      <dbReference type="DOI" id="10.1006/abio.1994.1113"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 27-61</scope>
    <scope>AMIDATION AT ILE-61</scope>
    <source>
      <tissue>Cuticle</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Cecropins have lytic and antibacterial activity against several Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Highest expression in fat body and hemocytes. Is also expressed in Malpighian tubules and to a much lesser extent in midgut. Not present in silk gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">Lepidopteran-B differs from lepidopteran-A by its hydroxylated residue.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the cecropin family.</text>
  </comment>
  <dbReference type="EMBL" id="D25320">
    <property type="protein sequence ID" value="BAA04990.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D25321">
    <property type="protein sequence ID" value="BAA04991.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D11114">
    <property type="protein sequence ID" value="BAA01890.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D11113">
    <property type="protein sequence ID" value="BAA01889.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="S60579">
    <property type="protein sequence ID" value="AAC60501.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="S74297">
    <property type="protein sequence ID" value="AAC60514.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="JC2296">
    <property type="entry name" value="CKMTB"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001037032.1">
    <property type="nucleotide sequence ID" value="NM_001043567.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001037460.1">
    <property type="nucleotide sequence ID" value="NM_001043995.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001096031.1">
    <property type="nucleotide sequence ID" value="NM_001102561.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_004926103.1">
    <property type="nucleotide sequence ID" value="XM_004926046.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_004926104.1">
    <property type="nucleotide sequence ID" value="XM_004926047.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_004926106.1">
    <property type="nucleotide sequence ID" value="XM_004926049.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_004926107.1">
    <property type="nucleotide sequence ID" value="XM_004926050.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_012545761.1">
    <property type="nucleotide sequence ID" value="XM_012690307.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P04142"/>
  <dbReference type="SMR" id="P04142"/>
  <dbReference type="STRING" id="7091.P04142"/>
  <dbReference type="PaxDb" id="7091-BGIBMGA000021-TA"/>
  <dbReference type="EnsemblMetazoa" id="NM_001043567.2">
    <property type="protein sequence ID" value="NP_001037032.2"/>
    <property type="gene ID" value="LOC101739536"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="NM_001043995.1">
    <property type="protein sequence ID" value="NP_001037460.1"/>
    <property type="gene ID" value="GeneID_693028"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="NM_001102561.1">
    <property type="protein sequence ID" value="NP_001096031.1"/>
    <property type="gene ID" value="GeneID_732858"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_004926047.3">
    <property type="protein sequence ID" value="XP_004926104.1"/>
    <property type="gene ID" value="LOC101739681"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_004926049.4">
    <property type="protein sequence ID" value="XP_004926106.1"/>
    <property type="gene ID" value="LOC101739958"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_004926050.4">
    <property type="protein sequence ID" value="XP_004926107.1"/>
    <property type="gene ID" value="LOC101740092"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_038020535.1">
    <property type="protein sequence ID" value="XP_037876463.1"/>
    <property type="gene ID" value="GeneID_693028"/>
  </dbReference>
  <dbReference type="GeneID" id="101739536"/>
  <dbReference type="GeneID" id="101739681"/>
  <dbReference type="GeneID" id="101739958"/>
  <dbReference type="GeneID" id="101740092"/>
  <dbReference type="GeneID" id="693028"/>
  <dbReference type="GeneID" id="732858"/>
  <dbReference type="KEGG" id="bmor:101739536"/>
  <dbReference type="KEGG" id="bmor:101739681"/>
  <dbReference type="KEGG" id="bmor:101739958"/>
  <dbReference type="KEGG" id="bmor:101740092"/>
  <dbReference type="KEGG" id="bmor:693028"/>
  <dbReference type="KEGG" id="bmor:732858"/>
  <dbReference type="CTD" id="693028"/>
  <dbReference type="eggNOG" id="ENOG502T7RS">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_187909_0_0_1"/>
  <dbReference type="InParanoid" id="P04142"/>
  <dbReference type="OMA" id="SPKWKIF"/>
  <dbReference type="OrthoDB" id="3267574at2759"/>
  <dbReference type="Proteomes" id="UP000005204">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000875">
    <property type="entry name" value="Cecropin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00272">
    <property type="entry name" value="Cecropin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00268">
    <property type="entry name" value="CECROPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0379">Hydroxylation</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000004826" description="Removed by a dipeptidylpeptidase" evidence="2 4 5">
    <location>
      <begin position="23"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004827" description="Cecropin-B" evidence="3">
    <location>
      <begin position="27"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="modified residue" description="5-hydroxylysine; partial" evidence="2">
    <location>
      <position position="47"/>
    </location>
  </feature>
  <feature type="modified residue" description="Isoleucine amide" evidence="4">
    <location>
      <position position="61"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="2184991"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7765280"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="8203751"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source ref="5"/>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="63" mass="6831" checksum="C79C5540E45A84C8" modified="1996-10-01" version="2" precursor="true">MNFAKILSFVFALVLALSMTSAAPEPRWKIFKKIEKMGRNIRDGIVKAGPAIEVLGSAKAIGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>