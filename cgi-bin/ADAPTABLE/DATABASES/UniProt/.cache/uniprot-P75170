<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-07-24" version="110" xmlns="http://uniprot.org/uniprot">
  <accession>P75170</accession>
  <name>HPRR_MYCPN</name>
  <protein>
    <recommendedName>
      <fullName>Hydroperoxide reductase</fullName>
      <ecNumber>1.11.1.-</ecNumber>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">MPN_625</name>
    <name type="ORF">C12_orf141</name>
    <name type="ORF">MP217</name>
  </gene>
  <organism>
    <name type="scientific">Mycoplasma pneumoniae (strain ATCC 29342 / M129 / Subtype 1)</name>
    <name type="common">Mycoplasmoides pneumoniae</name>
    <dbReference type="NCBI Taxonomy" id="272634"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Mycoplasmatota</taxon>
      <taxon>Mycoplasmoidales</taxon>
      <taxon>Mycoplasmoidaceae</taxon>
      <taxon>Mycoplasmoides</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Nucleic Acids Res." volume="24" first="4420" last="4449">
      <title>Complete sequence analysis of the genome of the bacterium Mycoplasma pneumoniae.</title>
      <authorList>
        <person name="Himmelreich R."/>
        <person name="Hilbert H."/>
        <person name="Plagens H."/>
        <person name="Pirkl E."/>
        <person name="Li B.-C."/>
        <person name="Herrmann R."/>
      </authorList>
      <dbReference type="PubMed" id="8948633"/>
      <dbReference type="DOI" id="10.1093/nar/24.22.4420"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 29342 / M129 / Subtype 1</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2003" name="J. Struct. Funct. Genomics" volume="4" first="31" last="34">
      <title>Crystal structure of a stress inducible protein from Mycoplasma pneumoniae at 2.85 A resolution.</title>
      <authorList>
        <person name="Choi I.G."/>
        <person name="Shin D.H."/>
        <person name="Brandsen J."/>
        <person name="Jancarik J."/>
        <person name="Busso D."/>
        <person name="Yokota H."/>
        <person name="Kim R."/>
        <person name="Kim S.H."/>
      </authorList>
      <dbReference type="PubMed" id="12943365"/>
      <dbReference type="DOI" id="10.1023/a:1024625122089"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.85 ANGSTROMS)</scope>
    <scope>SUBUNIT</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Reduces organic and inorganic peroxide substrates. Protects the cell against oxidative stress (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the OsmC/Ohr family.</text>
  </comment>
  <dbReference type="EC" id="1.11.1.-"/>
  <dbReference type="EMBL" id="U00089">
    <property type="protein sequence ID" value="AAB95865.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="S73543">
    <property type="entry name" value="S73543"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_110314.1">
    <property type="nucleotide sequence ID" value="NC_000912.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_010874982.1">
    <property type="nucleotide sequence ID" value="NZ_OU342337.1"/>
  </dbReference>
  <dbReference type="PDB" id="1LQL">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.85 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J=1-141"/>
  </dbReference>
  <dbReference type="PDBsum" id="1LQL"/>
  <dbReference type="AlphaFoldDB" id="P75170"/>
  <dbReference type="SMR" id="P75170"/>
  <dbReference type="STRING" id="272634.MPN_625"/>
  <dbReference type="EnsemblBacteria" id="AAB95865">
    <property type="protein sequence ID" value="AAB95865"/>
    <property type="gene ID" value="MPN_625"/>
  </dbReference>
  <dbReference type="GeneID" id="66608689"/>
  <dbReference type="KEGG" id="mpn:MPN_625"/>
  <dbReference type="PATRIC" id="fig|272634.6.peg.689"/>
  <dbReference type="HOGENOM" id="CLU_1862951_0_0_14"/>
  <dbReference type="OrthoDB" id="384648at2"/>
  <dbReference type="BioCyc" id="MPNE272634:G1GJ3-1005-MONOMER"/>
  <dbReference type="EvolutionaryTrace" id="P75170"/>
  <dbReference type="Proteomes" id="UP000000808">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004601">
    <property type="term" value="F:peroxidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.25.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.300.20">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015946">
    <property type="entry name" value="KH_dom-like_a/b"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003718">
    <property type="entry name" value="OsmC/Ohr_fam"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR052924">
    <property type="entry name" value="OsmC/Ohr_hydroprdx_reductase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036102">
    <property type="entry name" value="OsmC/Ohrsf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35368">
    <property type="entry name" value="HYDROPEROXIDE REDUCTASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35368:SF1">
    <property type="entry name" value="HYDROPEROXIDE REDUCTASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02566">
    <property type="entry name" value="OsmC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF82784">
    <property type="entry name" value="OsmC-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-0575">Peroxidase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000210607" description="Hydroperoxide reductase">
    <location>
      <begin position="1"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="3"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="13"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="17"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="24"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="33"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="41"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="69"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="84"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="89"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="105"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="120"/>
      <end position="126"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="132"/>
      <end position="140"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12943365"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="1LQL"/>
    </source>
  </evidence>
  <sequence length="141" mass="15469" checksum="07958C371E69A0A4" modified="1997-02-01" version="1">MDKKYDITAVLNEDSSMTAISDQFQITLDARPKHTAKGFGPLAALLSGLAACELATANLMAPAKMITINKLLMNVTGSRSTNPTDGYFGLREINLHWEIHSPNSETEIKEFIDFVSKRCPAHNTLQGVSQLKINVNVTLVH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>