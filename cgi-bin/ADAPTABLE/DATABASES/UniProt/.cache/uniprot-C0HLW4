<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2021-09-29" modified="2022-12-14" version="3" xmlns="http://uniprot.org/uniprot">
  <accession>C0HLW4</accession>
  <name>SEP1C_OSTSE</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Septenin 1c</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Osteopilus septentrionalis</name>
    <name type="common">Cuban treefrog</name>
    <dbReference type="NCBI Taxonomy" id="317373"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Hylinae</taxon>
      <taxon>Lophiohylini</taxon>
      <taxon>Osteopilus</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2021" name="Rapid Commun. Mass Spectrom." volume="35" first="e9061" last="e9061">
      <title>Manual mass spectrometry de novo sequencing of the anionic host defense peptides of the Cuban Treefrog Osteopilus septentrionalis.</title>
      <authorList>
        <person name="Samgina T.Y."/>
        <person name="Tolpina M.D."/>
        <person name="Surin A.K."/>
        <person name="Kovalev S.V."/>
        <person name="Bosch R.A."/>
        <person name="Alonso I.P."/>
        <person name="Garcia F.A."/>
        <person name="Gonzalez Lopez L.J."/>
        <person name="Lebedev A.T."/>
      </authorList>
      <dbReference type="PubMed" id="33527491"/>
      <dbReference type="DOI" id="10.1002/rcm.9061"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text evidence="2">May act as an antimicrobial peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed in skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1597.782" method="Electrospray" evidence="1"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the Frog skin active peptide (FSAP) family. Septenin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HLW4"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000453937" description="Septenin 1c">
    <location>
      <begin position="1"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="1">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="33527491"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="33527491"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="33527491"/>
    </source>
  </evidence>
  <sequence length="17" mass="1599" checksum="C356FB1DCC226E0C" modified="2021-09-29" version="1">SDAVADGVHAISGVVDS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>