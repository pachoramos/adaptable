<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="124" xmlns="http://uniprot.org/uniprot">
  <accession>P01660</accession>
  <name>KV3A8_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Ig kappa chain V-III region PC 3741/TEPC 111</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1978" name="Nature" volume="276" first="785" last="790">
      <title>Rearrangement of genetic information may produce immunoglobulin diversity.</title>
      <authorList>
        <person name="Weigert M."/>
        <person name="Gatmaitan L."/>
        <person name="Loh E."/>
        <person name="Schilling J."/>
        <person name="Hood L.E."/>
      </authorList>
      <dbReference type="PubMed" id="103003"/>
      <dbReference type="DOI" id="10.1038/276785a0"/>
    </citation>
    <scope>PROTEIN SEQUENCE (PC 3741)</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1978" name="Proc. Natl. Acad. Sci. U.S.A." volume="75" first="3913" last="3917">
      <title>Mechanisms of antibody diversity: multiple genes encode structurally related mouse kappa variable regions.</title>
      <authorList>
        <person name="McKean D.J."/>
        <person name="Bell M."/>
        <person name="Potter M."/>
      </authorList>
      <dbReference type="PubMed" id="99744"/>
      <dbReference type="DOI" id="10.1073/pnas.75.8.3913"/>
    </citation>
    <scope>PROTEIN SEQUENCE (TEPC 111)</scope>
  </reference>
  <comment type="miscellaneous">
    <text>The PC 3741 and TEPC 111 sequences are identical.</text>
  </comment>
  <dbReference type="PIR" id="A93204">
    <property type="entry name" value="KVMS37"/>
  </dbReference>
  <dbReference type="PDB" id="1EJO">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="L=1-111"/>
  </dbReference>
  <dbReference type="PDB" id="1I8I">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.40 A"/>
    <property type="chains" value="A=4-111"/>
  </dbReference>
  <dbReference type="PDB" id="1I8K">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.80 A"/>
    <property type="chains" value="A=4-111"/>
  </dbReference>
  <dbReference type="PDB" id="1J05">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.50 A"/>
    <property type="chains" value="A/L=1-111"/>
  </dbReference>
  <dbReference type="PDB" id="1MOE">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.60 A"/>
    <property type="chains" value="A/B=2-111"/>
  </dbReference>
  <dbReference type="PDB" id="1QFW">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.50 A"/>
    <property type="chains" value="L=1-111"/>
  </dbReference>
  <dbReference type="PDB" id="2R29">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.00 A"/>
    <property type="chains" value="L=1-111"/>
  </dbReference>
  <dbReference type="PDB" id="2R69">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.80 A"/>
    <property type="chains" value="L=1-111"/>
  </dbReference>
  <dbReference type="PDB" id="2R6P">
    <property type="method" value="EM"/>
    <property type="resolution" value="24.00 A"/>
    <property type="chains" value="E/G=1-111"/>
  </dbReference>
  <dbReference type="PDB" id="2VL5">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.10 A"/>
    <property type="chains" value="B/D=1-111"/>
  </dbReference>
  <dbReference type="PDB" id="3BSZ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.38 A"/>
    <property type="chains" value="L/M=1-111"/>
  </dbReference>
  <dbReference type="PDB" id="4NJA">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.20 A"/>
    <property type="chains" value="L=1-111"/>
  </dbReference>
  <dbReference type="PDBsum" id="1EJO"/>
  <dbReference type="PDBsum" id="1I8I"/>
  <dbReference type="PDBsum" id="1I8K"/>
  <dbReference type="PDBsum" id="1J05"/>
  <dbReference type="PDBsum" id="1MOE"/>
  <dbReference type="PDBsum" id="1QFW"/>
  <dbReference type="PDBsum" id="2R29"/>
  <dbReference type="PDBsum" id="2R69"/>
  <dbReference type="PDBsum" id="2R6P"/>
  <dbReference type="PDBsum" id="2VL5"/>
  <dbReference type="PDBsum" id="3BSZ"/>
  <dbReference type="PDBsum" id="4NJA"/>
  <dbReference type="AlphaFoldDB" id="P01660"/>
  <dbReference type="SMR" id="P01660"/>
  <dbReference type="MINT" id="P01660"/>
  <dbReference type="InParanoid" id="P01660"/>
  <dbReference type="EvolutionaryTrace" id="P01660"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="RNAct" id="P01660">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019814">
    <property type="term" value="C:immunoglobulin complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002250">
    <property type="term" value="P:adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006955">
    <property type="term" value="P:immune response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd04980">
    <property type="entry name" value="IgV_L_kappa"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.40.10:FF:000350">
    <property type="entry name" value="Immunoglobulin kappa chain variable 18-36"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003599">
    <property type="entry name" value="Ig_sub"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013106">
    <property type="entry name" value="Ig_V-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050150">
    <property type="entry name" value="IgV_Light_Chain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23267:SF343">
    <property type="entry name" value="IG KAPPA CHAIN V-III REGION 50S10.1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23267">
    <property type="entry name" value="IMMUNOGLOBULIN LIGHT CHAIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07686">
    <property type="entry name" value="V-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00409">
    <property type="entry name" value="IG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00406">
    <property type="entry name" value="IGv"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-1280">Immunoglobulin</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000059783" description="Ig kappa chain V-III region PC 3741/TEPC 111">
    <location>
      <begin position="1"/>
      <end position="111" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-1">
    <location>
      <begin position="24"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-2">
    <location>
      <begin position="39"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-2">
    <location>
      <begin position="54"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-3">
    <location>
      <begin position="61"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-3">
    <location>
      <begin position="93"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-4">
    <location>
      <begin position="102"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="23"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="111"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="4"/>
      <end position="7"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="9"/>
      <end position="12"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="19"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="37"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="44"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="48"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="turn" evidence="2">
    <location>
      <begin position="54"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="66"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="74"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="helix" evidence="2">
    <location>
      <begin position="84"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="88"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="96"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="106"/>
      <end position="109"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00114"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="2">
    <source>
      <dbReference type="PDB" id="1J05"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="1QFW"/>
    </source>
  </evidence>
  <sequence length="111" mass="12099" checksum="EC46C9D259213BE4" modified="1986-07-21" version="1">DIVLTQSPASLAVSLGQRATISCRASESVDSYGNSFMHWYQQKPGQPPKLLIYRASNLESGIPARFSGSGSRTDFTLTINPVEADDVATYYCQQSNEDPYTFGGGTKLEIK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>