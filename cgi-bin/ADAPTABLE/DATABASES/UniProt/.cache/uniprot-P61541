<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-05-24" modified="2024-05-29" version="70" xmlns="http://uniprot.org/uniprot">
  <accession>P61541</accession>
  <name>BDS1_ANTEL</name>
  <protein>
    <recommendedName>
      <fullName evidence="7">Kappa-actitoxin-Ael2a</fullName>
      <shortName evidence="7">Kappa-AITX-Ael2a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6">Toxin APETx1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Anthopleura elegantissima</name>
    <name type="common">Green aggregating anemone</name>
    <name type="synonym">Actinia elegantissima</name>
    <dbReference type="NCBI Taxonomy" id="6110"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Anthopleura</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Mol. Pharmacol." volume="64" first="59" last="69">
      <title>APETx1, a new toxin from the sea anemone Anthopleura elegantissima, blocks voltage-gated human ether-a-go-go-related gene potassium channels.</title>
      <authorList>
        <person name="Diochot S."/>
        <person name="Loret E."/>
        <person name="Bruhn T."/>
        <person name="Beress L."/>
        <person name="Lazdunski M."/>
      </authorList>
      <dbReference type="PubMed" id="12815161"/>
      <dbReference type="DOI" id="10.1124/mol.64.1.59"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>3D-STRUCTURE MODELING</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="Mol. Pharmacol." volume="69" first="1673" last="1683">
      <title>Species diversity and peptide toxins blocking selectivity of ether-a-go-go-related gene subfamily K+ channels in the central nervous system.</title>
      <authorList>
        <person name="Restano-Cassulini R."/>
        <person name="Korolkova Y.V."/>
        <person name="Diochot S."/>
        <person name="Gurrola G."/>
        <person name="Guasti L."/>
        <person name="Possani L.D."/>
        <person name="Lazdunski M."/>
        <person name="Grishin E.V."/>
        <person name="Arcangeli A."/>
        <person name="Wanke E."/>
      </authorList>
      <dbReference type="PubMed" id="16497878"/>
      <dbReference type="DOI" id="10.1124/mol.105.019729"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2007" name="Mol. Pharmacol." volume="72" first="259" last="268">
      <title>APETx1 from sea anemone Anthopleura elegantissima is a gating modifier peptide toxin of the human ether-a-go-go- related potassium channel.</title>
      <authorList>
        <person name="Zhang M."/>
        <person name="Liu X.S."/>
        <person name="Diochot S."/>
        <person name="Lazdunski M."/>
        <person name="Tseng G.N."/>
      </authorList>
      <dbReference type="PubMed" id="17473056"/>
      <dbReference type="DOI" id="10.1124/mol.107.035840"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2012" name="FASEB J." volume="26" first="5141" last="5151">
      <title>A natural point mutation changes both target selectivity and mechanism of action of sea anemone toxins.</title>
      <authorList>
        <person name="Peigneur S."/>
        <person name="Beress L."/>
        <person name="Moeller C."/>
        <person name="Mari F."/>
        <person name="Forssmann W.G."/>
        <person name="Tytgat J."/>
      </authorList>
      <dbReference type="PubMed" id="22972919"/>
      <dbReference type="DOI" id="10.1096/fj.12-218479"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2017" name="FEBS J." volume="284" first="3320" last="3338">
      <title>Defensin-neurotoxin dyad in a basally branching metazoan sea anemone.</title>
      <authorList>
        <person name="Kim C.H."/>
        <person name="Lee Y.J."/>
        <person name="Go H.J."/>
        <person name="Oh H.Y."/>
        <person name="Lee T.K."/>
        <person name="Park J.B."/>
        <person name="Park N.G."/>
      </authorList>
      <dbReference type="PubMed" id="28796463"/>
      <dbReference type="DOI" id="10.1111/febs.14194"/>
    </citation>
    <scope>FUNCTION AS ANTIBIOTIC</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2005" name="Proteins" volume="59" first="380" last="386">
      <title>Solution structure of APETx1 from the sea anemone Anthopleura elegantissima: a new fold for an HERG toxin.</title>
      <authorList>
        <person name="Chagot B."/>
        <person name="Diochot S."/>
        <person name="Pimentel C."/>
        <person name="Lazdunski M."/>
        <person name="Darbon H."/>
      </authorList>
      <dbReference type="PubMed" id="15726634"/>
      <dbReference type="DOI" id="10.1002/prot.20425"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3 4 5">Peptide with both antimicrobial and neurotoxin activities. This toxin acts both on ERG potassium channels and sodium channels (PubMed:12815161, PubMed:16497878, PubMed:17473056, PubMed:22972919). It potently and reversibly inhibits human Kv11.1/KCNH2/ERG1 (IC(50)=34 nM) (PubMed:12815161, PubMed:16497878, PubMed:17473056), rat Kv11.1/KCNH2/ERG1 (PubMed:16497878) and Kv11.3/KCNH7/ERG3 (PubMed:17473056) voltage-gated potassium channels in a similar potency. It acts as a gating-modifier toxin that shifts the voltage-dependence of ERG activation in the positive direction and suppresses its current amplitudes elicited by strong depolarizing pulses (PubMed:12815161, PubMed:17473056). On sodium channels, it blocks Nav1.2/SCN2A (EC(50)=31 nM), Nav1.3/SCN3A, Nav1.4/SCN4A, Nav1.5/SCN5A, Nav1.6/SCN8A, Nav1.8/SCN10A (EC(50)=92 nM) (PubMed:22972919). It may act by binding at site 1 or close by, only when the pore is in an open configuration (PubMed:22972919). Shows antibacterial activity against the Gram-negative bacterium S.typhimurium, but not on the bacteria B.subtilis, S.aureus, and P.aeruginosa (PubMed:28796463). In vivo, this toxin does not induce neurotoxic symptoms when injected into mice (PubMed:12815161).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1 5">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="8">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="2">Has the CSbeta/beta fold, which comprises anti-parallel beta-sheets stabilized by three or four disulfide bonds.</text>
  </comment>
  <comment type="mass spectrometry" mass="4552.21" method="Electrospray" evidence="1"/>
  <comment type="miscellaneous">
    <text evidence="1 4 5">Negative results: does not block Kv11.2/KCNH6/ERG2 (PubMed:17473056), Kv1.1/KCNA1, Kv1.2/KCNA2, Kv1.3/KCNA3, Kv1.5/KCNA5, Kv1.6/KCNA6, Kv2.1/KCNB1, Kv3.4/KCNC4, Kv4.2/KCND2, Kv7.1/KCNQ1, Kv7.2/KCNQ2, Kv7.3/KCNQ3, Kv10.1/EAG1/KCNH1 and Kv12.3/ELK1/KCNH4 (PubMed:12815161). Weakly inhibits Kv1.4 KCNA4 (27%) (PubMed:12815161). Does not inhibit Nav1.7/SCN9A and insect DmNav1 and BgNav1 (PubMed:22972919).</text>
  </comment>
  <comment type="similarity">
    <text evidence="8">Belongs to the sea anemone type 3 (BDS) potassium channel toxin family.</text>
  </comment>
  <dbReference type="PDB" id="1WQK">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-42"/>
  </dbReference>
  <dbReference type="PDB" id="7BWI">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-42"/>
  </dbReference>
  <dbReference type="PDBsum" id="1WQK"/>
  <dbReference type="PDBsum" id="7BWI"/>
  <dbReference type="AlphaFoldDB" id="P61541"/>
  <dbReference type="SMR" id="P61541"/>
  <dbReference type="TCDB" id="8.B.11.1.1">
    <property type="family name" value="the sea anemone peptide toxin (apetx) family"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P61541"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008200">
    <property type="term" value="F:ion channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012414">
    <property type="entry name" value="BDS_K_chnl_tox"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07936">
    <property type="entry name" value="Defensin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000221539" description="Kappa-actitoxin-Ael2a" evidence="1">
    <location>
      <begin position="1"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 9">
    <location>
      <begin position="4"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 9">
    <location>
      <begin position="6"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 9">
    <location>
      <begin position="20"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="3"/>
      <end position="6"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="9"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="helix" evidence="11">
    <location>
      <begin position="22"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="28"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="35"/>
      <end position="40"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="12815161"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15726634"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="16497878"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="17473056"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="22972919"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="12815161"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <evidence type="ECO:0000312" key="9">
    <source>
      <dbReference type="PDB" id="1WQK"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="10">
    <source>
      <dbReference type="PDB" id="1WQK"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="11">
    <source>
      <dbReference type="PDB" id="7BWI"/>
    </source>
  </evidence>
  <sequence length="42" mass="4558" checksum="55069008814B3715" modified="2004-05-24" version="1">GTTCYCGKTIGIYWFGTKTCPSNRGYTGSCGYFLGICCYPVD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>