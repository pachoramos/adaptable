<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2015-12-09" modified="2024-11-27" version="22" xmlns="http://uniprot.org/uniprot">
  <accession>C0HJU7</accession>
  <name>VKT2G_RADCR</name>
  <protein>
    <recommendedName>
      <fullName evidence="8">PI-stichotoxin-Hcr2g</fullName>
      <shortName evidence="8">PI-SHTX-Hcr2g</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6 7">Kunitz-type serine protease inhibitor HCRG2</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Radianthus crispa</name>
    <name type="common">Leathery sea anemone</name>
    <name type="synonym">Heteractis crispa</name>
    <dbReference type="NCBI Taxonomy" id="3122430"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Stichodactylidae</taxon>
      <taxon>Radianthus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2015" name="Mar. Drugs" volume="13" first="6038" last="6063">
      <title>New Kunitz-type HCRG polypeptides from the sea anemone Heteractis crispa.</title>
      <authorList>
        <person name="Gladkikh I."/>
        <person name="Monastyrnaya M."/>
        <person name="Zelepuga E."/>
        <person name="Sintsova O."/>
        <person name="Tabakmakher V."/>
        <person name="Gnedenko O."/>
        <person name="Ivanov A."/>
        <person name="Hua K.F."/>
        <person name="Kozlovskaya E."/>
      </authorList>
      <dbReference type="PubMed" id="26404319"/>
      <dbReference type="DOI" id="10.3390/md13106038"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>PRESENCE OF DISULFIDE BONDS</scope>
    <scope>3D-STRUCTURE MODELING</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2020" name="Biomedicines" volume="8">
      <title>Kunitz-type peptides from the sea anemone Heteractis crispa demonstrate potassium channel blocking and anti-inflammatory activities.</title>
      <authorList>
        <person name="Gladkikh I."/>
        <person name="Peigneur S."/>
        <person name="Sintsova O."/>
        <person name="Lopes Pinheiro-Junior E."/>
        <person name="Klimovich A."/>
        <person name="Menshov A."/>
        <person name="Kalinovsky A."/>
        <person name="Isaeva M."/>
        <person name="Monastyrnaya M."/>
        <person name="Kozlovskaya E."/>
        <person name="Tytgat J."/>
        <person name="Leychenko E."/>
      </authorList>
      <dbReference type="PubMed" id="33158163"/>
      <dbReference type="DOI" id="10.3390/biomedicines8110473"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>3D-STRUCTURE MODELING</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2021" name="Biomedicines" volume="9">
      <title>Sea anemone kunitz-type peptides demonstrate neuroprotective activity in the 6-hydroxydopamine induced neurotoxicity model.</title>
      <authorList>
        <person name="Sintsova O."/>
        <person name="Gladkikh I."/>
        <person name="Monastyrnaya M."/>
        <person name="Tabakmakher V."/>
        <person name="Yurchenko E."/>
        <person name="Menchinskaya E."/>
        <person name="Pislyagin E."/>
        <person name="Andreev Y."/>
        <person name="Kozlov S."/>
        <person name="Peigneur S."/>
        <person name="Tytgat J."/>
        <person name="Aminin D."/>
        <person name="Kozlovskaya E."/>
        <person name="Leychenko E."/>
      </authorList>
      <dbReference type="PubMed" id="33802055"/>
      <dbReference type="DOI" id="10.3390/biomedicines9030283"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="4 5">Dual-function toxin that inhibits both serine proteases and voltage-gated potassium channels (PubMed:26404319, PubMed:33158163). Has potent activity on both trypsin (Ki=50 nM) and chymotrypsin (Kd=1.6 nM) (PubMed:26404319). Shows inhibitory activity against 5 of the 7 potassium channels tested (rKv1.1/KCNA1; IC(50)=12.6, hKv1.2/KCNA2; IC(50)=181.7, hKv1.3/KCNA3; IC(50)=29.7, rKv1.6/KCNA6; IC(50)=43.9, drosophila Shaker; IC(50)=114.9) (PubMed:33158163). Has an anti-inflammatory effect in LPS-activated macrophages in vitro, specifically reducing release of TNF and IL6 but not nitric oxide and reducing expression of IL1B precursor (PubMed:26404319). In contrast to some paralogs, this protein decreases reactive oxygen species (ROS) level in the oxidative stress agent 6-hydroxydopamine (6-OHDA)-induced neurotoxicity model, but does not show cytoprotective activity on neuroblastoma cells (PubMed:33802055). This protein also shows a weak free-radical scavenging activity (PubMed:33802055).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="8">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="PTM">
    <text evidence="4">Contains 3 disulfide bonds.</text>
  </comment>
  <comment type="mass spectrometry" mass="6148.0" method="MALDI" evidence="4 5"/>
  <comment type="miscellaneous">
    <text evidence="4 5">Negative results: does not bind to and, hence, probably does not inhibit serine proteases plasmin, kallikrein and thrombin (PubMed:26404319). Has no activity on Kv1.4/KCNA4 and Kv1.5/KCNA5 (PubMed:33158163).</text>
  </comment>
  <comment type="similarity">
    <text evidence="8">Belongs to the venom Kunitz-type family. Sea anemone type 2 potassium channel toxin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HJU7"/>
  <dbReference type="SMR" id="C0HJU7"/>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd22618">
    <property type="entry name" value="Kunitz_SHPI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.410.10:FF:000021">
    <property type="entry name" value="Serine protease inhibitor, putative"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.410.10">
    <property type="entry name" value="Pancreatic trypsin inhibitor Kunitz domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050098">
    <property type="entry name" value="Kunitz-type_SerProtInhib"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002223">
    <property type="entry name" value="Kunitz_BPTI"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036880">
    <property type="entry name" value="Kunitz_BPTI_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020901">
    <property type="entry name" value="Prtase_inh_Kunz-CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10083">
    <property type="entry name" value="KUNITZ-TYPE PROTEASE INHIBITOR-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10083:SF217">
    <property type="entry name" value="PROTEIN CBG23496"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00014">
    <property type="entry name" value="Kunitz_BPTI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00759">
    <property type="entry name" value="BASICPTASE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00131">
    <property type="entry name" value="KU"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57362">
    <property type="entry name" value="BPTI-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00280">
    <property type="entry name" value="BPTI_KUNITZ_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50279">
    <property type="entry name" value="BPTI_KUNITZ_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000434950" description="PI-stichotoxin-Hcr2g" evidence="4">
    <location>
      <begin position="1"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="domain" description="BPTI/Kunitz inhibitor" evidence="3">
    <location>
      <begin position="4"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="site" description="Reactive bond for trypsin" evidence="1">
    <location>
      <begin position="14"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="4"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="13"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="29"/>
      <end position="50"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P00974"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P31713"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00031"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="26404319"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="33158163"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="26404319"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="33158163"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <sequence length="56" mass="6157" checksum="784379275B8F3369" modified="2015-12-09" version="1">RGICLEPKVVGPCKARIRRFYYDSETGKCTPFIYGGCGGNGNNFETLHACRGICRA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>