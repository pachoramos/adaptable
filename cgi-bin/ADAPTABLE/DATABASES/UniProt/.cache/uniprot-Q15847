<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-12-01" modified="2024-11-27" version="164" xmlns="http://uniprot.org/uniprot">
  <accession>Q15847</accession>
  <name>ADIRF_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Adipogenesis regulatory factor</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Adipogenesis factor rich in obesity</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Adipose most abundant gene transcript 2 protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Adipose-specific protein 2</fullName>
      <shortName>apM-2</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ADIRF</name>
    <name type="synonym">AFRO</name>
    <name type="synonym">APM2</name>
    <name type="synonym">C10orf116</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1995-01" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Maeda K."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Adipose tissue</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Nature" volume="429" first="375" last="381">
      <title>The DNA sequence and comparative analysis of human chromosome 10.</title>
      <authorList>
        <person name="Deloukas P."/>
        <person name="Earthrowl M.E."/>
        <person name="Grafham D.V."/>
        <person name="Rubenfield M."/>
        <person name="French L."/>
        <person name="Steward C.A."/>
        <person name="Sims S.K."/>
        <person name="Jones M.C."/>
        <person name="Searle S."/>
        <person name="Scott C."/>
        <person name="Howe K."/>
        <person name="Hunt S.E."/>
        <person name="Andrews T.D."/>
        <person name="Gilbert J.G.R."/>
        <person name="Swarbreck D."/>
        <person name="Ashurst J.L."/>
        <person name="Taylor A."/>
        <person name="Battles J."/>
        <person name="Bird C.P."/>
        <person name="Ainscough R."/>
        <person name="Almeida J.P."/>
        <person name="Ashwell R.I.S."/>
        <person name="Ambrose K.D."/>
        <person name="Babbage A.K."/>
        <person name="Bagguley C.L."/>
        <person name="Bailey J."/>
        <person name="Banerjee R."/>
        <person name="Bates K."/>
        <person name="Beasley H."/>
        <person name="Bray-Allen S."/>
        <person name="Brown A.J."/>
        <person name="Brown J.Y."/>
        <person name="Burford D.C."/>
        <person name="Burrill W."/>
        <person name="Burton J."/>
        <person name="Cahill P."/>
        <person name="Camire D."/>
        <person name="Carter N.P."/>
        <person name="Chapman J.C."/>
        <person name="Clark S.Y."/>
        <person name="Clarke G."/>
        <person name="Clee C.M."/>
        <person name="Clegg S."/>
        <person name="Corby N."/>
        <person name="Coulson A."/>
        <person name="Dhami P."/>
        <person name="Dutta I."/>
        <person name="Dunn M."/>
        <person name="Faulkner L."/>
        <person name="Frankish A."/>
        <person name="Frankland J.A."/>
        <person name="Garner P."/>
        <person name="Garnett J."/>
        <person name="Gribble S."/>
        <person name="Griffiths C."/>
        <person name="Grocock R."/>
        <person name="Gustafson E."/>
        <person name="Hammond S."/>
        <person name="Harley J.L."/>
        <person name="Hart E."/>
        <person name="Heath P.D."/>
        <person name="Ho T.P."/>
        <person name="Hopkins B."/>
        <person name="Horne J."/>
        <person name="Howden P.J."/>
        <person name="Huckle E."/>
        <person name="Hynds C."/>
        <person name="Johnson C."/>
        <person name="Johnson D."/>
        <person name="Kana A."/>
        <person name="Kay M."/>
        <person name="Kimberley A.M."/>
        <person name="Kershaw J.K."/>
        <person name="Kokkinaki M."/>
        <person name="Laird G.K."/>
        <person name="Lawlor S."/>
        <person name="Lee H.M."/>
        <person name="Leongamornlert D.A."/>
        <person name="Laird G."/>
        <person name="Lloyd C."/>
        <person name="Lloyd D.M."/>
        <person name="Loveland J."/>
        <person name="Lovell J."/>
        <person name="McLaren S."/>
        <person name="McLay K.E."/>
        <person name="McMurray A."/>
        <person name="Mashreghi-Mohammadi M."/>
        <person name="Matthews L."/>
        <person name="Milne S."/>
        <person name="Nickerson T."/>
        <person name="Nguyen M."/>
        <person name="Overton-Larty E."/>
        <person name="Palmer S.A."/>
        <person name="Pearce A.V."/>
        <person name="Peck A.I."/>
        <person name="Pelan S."/>
        <person name="Phillimore B."/>
        <person name="Porter K."/>
        <person name="Rice C.M."/>
        <person name="Rogosin A."/>
        <person name="Ross M.T."/>
        <person name="Sarafidou T."/>
        <person name="Sehra H.K."/>
        <person name="Shownkeen R."/>
        <person name="Skuce C.D."/>
        <person name="Smith M."/>
        <person name="Standring L."/>
        <person name="Sycamore N."/>
        <person name="Tester J."/>
        <person name="Thorpe A."/>
        <person name="Torcasso W."/>
        <person name="Tracey A."/>
        <person name="Tromans A."/>
        <person name="Tsolas J."/>
        <person name="Wall M."/>
        <person name="Walsh J."/>
        <person name="Wang H."/>
        <person name="Weinstock K."/>
        <person name="West A.P."/>
        <person name="Willey D.L."/>
        <person name="Whitehead S.L."/>
        <person name="Wilming L."/>
        <person name="Wray P.W."/>
        <person name="Young L."/>
        <person name="Chen Y."/>
        <person name="Lovering R.C."/>
        <person name="Moschonas N.K."/>
        <person name="Siebert R."/>
        <person name="Fechtel K."/>
        <person name="Bentley D."/>
        <person name="Durbin R.M."/>
        <person name="Hubbard T."/>
        <person name="Doucette-Stamm L."/>
        <person name="Beck S."/>
        <person name="Smith D.R."/>
        <person name="Rogers J."/>
      </authorList>
      <dbReference type="PubMed" id="15164054"/>
      <dbReference type="DOI" id="10.1038/nature02462"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Pancreas</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="submission" date="2004-09" db="UniProtKB">
      <authorList>
        <person name="Richardson M."/>
      </authorList>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2009" name="Int. J. Cancer" volume="125" first="1193" last="1204">
      <title>APM2 is a novel mediator of cisplatin resistance in a variety of cancer cell types regardless of p53 or MMR status.</title>
      <authorList>
        <person name="Scott B.J."/>
        <person name="Qutob S."/>
        <person name="Liu Q.Y."/>
        <person name="Ng C.E."/>
      </authorList>
      <dbReference type="PubMed" id="19444912"/>
      <dbReference type="DOI" id="10.1002/ijc.24465"/>
    </citation>
    <scope>FUNCTION IN RESISTANCE TO CISPLATIN</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2011" name="BMC Syst. Biol." volume="5" first="17" last="17">
      <title>Initial characterization of the human central proteome.</title>
      <authorList>
        <person name="Burkard T.R."/>
        <person name="Planyavsky M."/>
        <person name="Kaupe I."/>
        <person name="Breitwieser F.P."/>
        <person name="Buerckstuemmer T."/>
        <person name="Bennett K.L."/>
        <person name="Superti-Furga G."/>
        <person name="Colinge J."/>
      </authorList>
      <dbReference type="PubMed" id="21269460"/>
      <dbReference type="DOI" id="10.1186/1752-0509-5-17"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2013" name="J. Bioenerg. Biomembr." volume="45" first="219" last="228">
      <title>A Novel pro-adipogenesis factor abundant in adipose tissues and over-expressed in obesity acts upstream of PPARgamma and C/EBPalpha.</title>
      <authorList>
        <person name="Ni Y."/>
        <person name="Ji C."/>
        <person name="Wang B."/>
        <person name="Qiu J."/>
        <person name="Wang J."/>
        <person name="Guo X."/>
      </authorList>
      <dbReference type="PubMed" id="23239344"/>
      <dbReference type="DOI" id="10.1007/s10863-012-9492-6"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2014" name="J. Proteomics" volume="96" first="253" last="262">
      <title>An enzyme assisted RP-RPLC approach for in-depth analysis of human liver phosphoproteome.</title>
      <authorList>
        <person name="Bian Y."/>
        <person name="Song C."/>
        <person name="Cheng K."/>
        <person name="Dong M."/>
        <person name="Wang F."/>
        <person name="Huang J."/>
        <person name="Sun D."/>
        <person name="Wang L."/>
        <person name="Ye M."/>
        <person name="Zou H."/>
      </authorList>
      <dbReference type="PubMed" id="24275569"/>
      <dbReference type="DOI" id="10.1016/j.jprot.2013.11.014"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Plays a role in fat cell development; promotes adipogenic differentiation and stimulates transcription initiation of master adipogenesis factors like PPARG and CEBPA at early stages of preadipocyte differentiation. Its overexpression confers resistance to the anticancer chemotherapeutic drug cisplatin.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-7162516">
      <id>Q15847</id>
    </interactant>
    <interactant intactId="EBI-3910835">
      <id>Q14116</id>
      <label>IL18</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Nucleus</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2 3">Expressed in adipose tissue (at protein level). Highly expressed in omental and subcutaneous adipose tissues. Expressed in heart, cornea, liver, kidney and spleen.</text>
  </comment>
  <comment type="induction">
    <text evidence="1 2">Up-regulated during pre-adipocyte differentiation. Up-regulated following DNA damage induced by UV irradiation.</text>
  </comment>
  <dbReference type="EMBL" id="D45370">
    <property type="protein sequence ID" value="BAA08226.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL136982">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC004471">
    <property type="protein sequence ID" value="AAH04471.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS7381.1"/>
  <dbReference type="RefSeq" id="NP_006820.1">
    <property type="nucleotide sequence ID" value="NM_006829.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q15847"/>
  <dbReference type="SMR" id="Q15847"/>
  <dbReference type="BioGRID" id="116171">
    <property type="interactions" value="11"/>
  </dbReference>
  <dbReference type="IntAct" id="Q15847">
    <property type="interactions" value="4"/>
  </dbReference>
  <dbReference type="MINT" id="Q15847"/>
  <dbReference type="STRING" id="9606.ENSP00000361083"/>
  <dbReference type="GlyCosmos" id="Q15847">
    <property type="glycosylation" value="1 site, 1 glycan"/>
  </dbReference>
  <dbReference type="GlyGen" id="Q15847">
    <property type="glycosylation" value="2 sites, 2 O-linked glycans (2 sites)"/>
  </dbReference>
  <dbReference type="iPTMnet" id="Q15847"/>
  <dbReference type="PhosphoSitePlus" id="Q15847"/>
  <dbReference type="BioMuta" id="ADIRF"/>
  <dbReference type="DMDM" id="8927989"/>
  <dbReference type="jPOST" id="Q15847"/>
  <dbReference type="MassIVE" id="Q15847"/>
  <dbReference type="PaxDb" id="9606-ENSP00000361083"/>
  <dbReference type="PeptideAtlas" id="Q15847"/>
  <dbReference type="ProteomicsDB" id="60790"/>
  <dbReference type="Pumba" id="Q15847"/>
  <dbReference type="Antibodypedia" id="56434">
    <property type="antibodies" value="54 antibodies from 13 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="10974"/>
  <dbReference type="Ensembl" id="ENST00000372013.8">
    <property type="protein sequence ID" value="ENSP00000361083.3"/>
    <property type="gene ID" value="ENSG00000148671.14"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000561504.1">
    <property type="protein sequence ID" value="ENSP00000475582.1"/>
    <property type="gene ID" value="ENSG00000148671.14"/>
  </dbReference>
  <dbReference type="GeneID" id="10974"/>
  <dbReference type="KEGG" id="hsa:10974"/>
  <dbReference type="MANE-Select" id="ENST00000372013.8">
    <property type="protein sequence ID" value="ENSP00000361083.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_006829.3"/>
    <property type="RefSeq protein sequence ID" value="NP_006820.1"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:24043"/>
  <dbReference type="CTD" id="10974"/>
  <dbReference type="DisGeNET" id="10974"/>
  <dbReference type="GeneCards" id="ADIRF"/>
  <dbReference type="HGNC" id="HGNC:24043">
    <property type="gene designation" value="ADIRF"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000148671">
    <property type="expression patterns" value="Tissue enhanced (adipose)"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_Q15847"/>
  <dbReference type="OpenTargets" id="ENSG00000148671"/>
  <dbReference type="PharmGKB" id="PA134930409"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000148671"/>
  <dbReference type="eggNOG" id="ENOG502SYWB">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000017446"/>
  <dbReference type="HOGENOM" id="CLU_199186_0_0_1"/>
  <dbReference type="InParanoid" id="Q15847"/>
  <dbReference type="OMA" id="XDQATET"/>
  <dbReference type="OrthoDB" id="4641953at2759"/>
  <dbReference type="PhylomeDB" id="Q15847"/>
  <dbReference type="TreeFam" id="TF341403"/>
  <dbReference type="PathwayCommons" id="Q15847"/>
  <dbReference type="Reactome" id="R-HSA-381340">
    <property type="pathway name" value="Transcriptional regulation of white adipocyte differentiation"/>
  </dbReference>
  <dbReference type="SignaLink" id="Q15847"/>
  <dbReference type="BioGRID-ORCS" id="10974">
    <property type="hits" value="8 hits in 1148 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="ADIRF">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="GenomeRNAi" id="10974"/>
  <dbReference type="Pharos" id="Q15847">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q15847"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 10"/>
  </dbReference>
  <dbReference type="RNAct" id="Q15847">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000148671">
    <property type="expression patterns" value="Expressed in popliteal artery and 200 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q15847">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HPA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070062">
    <property type="term" value="C:extracellular exosome"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005654">
    <property type="term" value="C:nucleoplasm"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HPA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030154">
    <property type="term" value="P:cell differentiation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045600">
    <property type="term" value="P:positive regulation of fat cell differentiation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045944">
    <property type="term" value="P:positive regulation of transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR034450">
    <property type="entry name" value="ADIRF"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR39227">
    <property type="entry name" value="ADIPOGENESIS REGULATORY FACTOR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR39227:SF1">
    <property type="entry name" value="ADIPOGENESIS REGULATORY FACTOR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0221">Differentiation</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_0000064637" description="Adipogenesis regulatory factor">
    <location>
      <begin position="1"/>
      <end position="76"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="19444912"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="23239344"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source ref="4"/>
  </evidence>
  <sequence length="76" mass="7855" checksum="C95B41F13C4BA7B1" modified="1996-11-01" version="1">MASKGLQDLKQQVEGTAQEAVSAAGAAAQQVVDQATEAGQKAMDQLAKTTQETIDKTANQASDTFSGIGKKFGLLK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>