<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-08-30" modified="2024-10-02" version="29" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKQ8</accession>
  <accession>B3DN02</accession>
  <accession>B3DN08</accession>
  <accession>O16815</accession>
  <accession>O16816</accession>
  <accession>O16817</accession>
  <accession>O16818</accession>
  <accession>O16819</accession>
  <accession>O16820</accession>
  <accession>O16821</accession>
  <accession>O61275</accession>
  <accession>O61276</accession>
  <accession>O61277</accession>
  <accession>O61279</accession>
  <accession>O62590</accession>
  <accession>P14954</accession>
  <accession>P82449</accession>
  <name>CECA2_DROME</name>
  <protein>
    <recommendedName>
      <fullName evidence="8">Cecropin-A2</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="8 10" type="primary">CecA2</name>
    <name evidence="10" type="ORF">CG1367</name>
  </gene>
  <organism>
    <name type="scientific">Drosophila melanogaster</name>
    <name type="common">Fruit fly</name>
    <dbReference type="NCBI Taxonomy" id="7227"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Ephydroidea</taxon>
      <taxon>Drosophilidae</taxon>
      <taxon>Drosophila</taxon>
      <taxon>Sophophora</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="EMBO J." volume="9" first="217" last="224">
      <title>The cecropin locus in Drosophila; a compact gene cluster involved in the response to infection.</title>
      <authorList>
        <person name="Kylsten P."/>
        <person name="Samakovlis C."/>
        <person name="Hultmark D."/>
      </authorList>
      <dbReference type="PubMed" id="2104802"/>
      <dbReference type="DOI" id="10.1002/j.1460-2075.1990.tb08098.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>INDUCTION</scope>
    <source>
      <strain>Canton-S</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="Genetics" volume="147" first="713" last="724">
      <title>Molecular population genetics of Drosophila immune system genes.</title>
      <authorList>
        <person name="Clark A.G."/>
        <person name="Wang L."/>
      </authorList>
      <dbReference type="PubMed" id="9335607"/>
      <dbReference type="DOI" id="10.1093/genetics/147.2.713"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>VARIANTS LYS-2; LEU-7; GLY-8 AND THR-11</scope>
    <source>
      <strain>B115</strain>
      <strain>B141</strain>
      <strain>B208</strain>
      <strain>B225</strain>
      <strain>B226</strain>
      <strain>M13</strain>
      <strain>Z10</strain>
      <strain>Z22</strain>
      <strain>Z24</strain>
      <strain>Z5</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1998" name="Immunogenetics" volume="47" first="417" last="429">
      <title>Evolutionary history and mechanism of the Drosophila cecropin gene family.</title>
      <authorList>
        <person name="Date A."/>
        <person name="Satta Y."/>
        <person name="Takahata N."/>
        <person name="Chigusa S.I."/>
      </authorList>
      <dbReference type="PubMed" id="9553148"/>
      <dbReference type="DOI" id="10.1007/s002510050379"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>VARIANTS LEU-3 AND ALA-36</scope>
    <source>
      <strain>3RC</strain>
      <strain>MA1</strain>
      <strain>MA2</strain>
      <strain>MA3</strain>
      <strain>MJ1</strain>
      <strain>MJ2</strain>
      <strain>MJ3</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1998" name="Genetics" volume="150" first="157" last="171">
      <title>Molecular evolution of the Cecropin multigene family in Drosophila: functional genes vs pseudogenes.</title>
      <authorList>
        <person name="Ramos-Onsins S."/>
        <person name="Aguade M."/>
      </authorList>
      <dbReference type="PubMed" id="9725836"/>
      <dbReference type="DOI" id="10.1093/genetics/150.1.157"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>VARIANTS LEU-3 AND THR-11</scope>
    <source>
      <strain>M11</strain>
      <strain>M2</strain>
      <strain>M26</strain>
      <strain>M36</strain>
      <strain>M40</strain>
      <strain>M47</strain>
      <strain>M54</strain>
      <strain>M55</strain>
      <strain>M66</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2000" name="Science" volume="287" first="2185" last="2195">
      <title>The genome sequence of Drosophila melanogaster.</title>
      <authorList>
        <person name="Adams M.D."/>
        <person name="Celniker S.E."/>
        <person name="Holt R.A."/>
        <person name="Evans C.A."/>
        <person name="Gocayne J.D."/>
        <person name="Amanatides P.G."/>
        <person name="Scherer S.E."/>
        <person name="Li P.W."/>
        <person name="Hoskins R.A."/>
        <person name="Galle R.F."/>
        <person name="George R.A."/>
        <person name="Lewis S.E."/>
        <person name="Richards S."/>
        <person name="Ashburner M."/>
        <person name="Henderson S.N."/>
        <person name="Sutton G.G."/>
        <person name="Wortman J.R."/>
        <person name="Yandell M.D."/>
        <person name="Zhang Q."/>
        <person name="Chen L.X."/>
        <person name="Brandon R.C."/>
        <person name="Rogers Y.-H.C."/>
        <person name="Blazej R.G."/>
        <person name="Champe M."/>
        <person name="Pfeiffer B.D."/>
        <person name="Wan K.H."/>
        <person name="Doyle C."/>
        <person name="Baxter E.G."/>
        <person name="Helt G."/>
        <person name="Nelson C.R."/>
        <person name="Miklos G.L.G."/>
        <person name="Abril J.F."/>
        <person name="Agbayani A."/>
        <person name="An H.-J."/>
        <person name="Andrews-Pfannkoch C."/>
        <person name="Baldwin D."/>
        <person name="Ballew R.M."/>
        <person name="Basu A."/>
        <person name="Baxendale J."/>
        <person name="Bayraktaroglu L."/>
        <person name="Beasley E.M."/>
        <person name="Beeson K.Y."/>
        <person name="Benos P.V."/>
        <person name="Berman B.P."/>
        <person name="Bhandari D."/>
        <person name="Bolshakov S."/>
        <person name="Borkova D."/>
        <person name="Botchan M.R."/>
        <person name="Bouck J."/>
        <person name="Brokstein P."/>
        <person name="Brottier P."/>
        <person name="Burtis K.C."/>
        <person name="Busam D.A."/>
        <person name="Butler H."/>
        <person name="Cadieu E."/>
        <person name="Center A."/>
        <person name="Chandra I."/>
        <person name="Cherry J.M."/>
        <person name="Cawley S."/>
        <person name="Dahlke C."/>
        <person name="Davenport L.B."/>
        <person name="Davies P."/>
        <person name="de Pablos B."/>
        <person name="Delcher A."/>
        <person name="Deng Z."/>
        <person name="Mays A.D."/>
        <person name="Dew I."/>
        <person name="Dietz S.M."/>
        <person name="Dodson K."/>
        <person name="Doup L.E."/>
        <person name="Downes M."/>
        <person name="Dugan-Rocha S."/>
        <person name="Dunkov B.C."/>
        <person name="Dunn P."/>
        <person name="Durbin K.J."/>
        <person name="Evangelista C.C."/>
        <person name="Ferraz C."/>
        <person name="Ferriera S."/>
        <person name="Fleischmann W."/>
        <person name="Fosler C."/>
        <person name="Gabrielian A.E."/>
        <person name="Garg N.S."/>
        <person name="Gelbart W.M."/>
        <person name="Glasser K."/>
        <person name="Glodek A."/>
        <person name="Gong F."/>
        <person name="Gorrell J.H."/>
        <person name="Gu Z."/>
        <person name="Guan P."/>
        <person name="Harris M."/>
        <person name="Harris N.L."/>
        <person name="Harvey D.A."/>
        <person name="Heiman T.J."/>
        <person name="Hernandez J.R."/>
        <person name="Houck J."/>
        <person name="Hostin D."/>
        <person name="Houston K.A."/>
        <person name="Howland T.J."/>
        <person name="Wei M.-H."/>
        <person name="Ibegwam C."/>
        <person name="Jalali M."/>
        <person name="Kalush F."/>
        <person name="Karpen G.H."/>
        <person name="Ke Z."/>
        <person name="Kennison J.A."/>
        <person name="Ketchum K.A."/>
        <person name="Kimmel B.E."/>
        <person name="Kodira C.D."/>
        <person name="Kraft C.L."/>
        <person name="Kravitz S."/>
        <person name="Kulp D."/>
        <person name="Lai Z."/>
        <person name="Lasko P."/>
        <person name="Lei Y."/>
        <person name="Levitsky A.A."/>
        <person name="Li J.H."/>
        <person name="Li Z."/>
        <person name="Liang Y."/>
        <person name="Lin X."/>
        <person name="Liu X."/>
        <person name="Mattei B."/>
        <person name="McIntosh T.C."/>
        <person name="McLeod M.P."/>
        <person name="McPherson D."/>
        <person name="Merkulov G."/>
        <person name="Milshina N.V."/>
        <person name="Mobarry C."/>
        <person name="Morris J."/>
        <person name="Moshrefi A."/>
        <person name="Mount S.M."/>
        <person name="Moy M."/>
        <person name="Murphy B."/>
        <person name="Murphy L."/>
        <person name="Muzny D.M."/>
        <person name="Nelson D.L."/>
        <person name="Nelson D.R."/>
        <person name="Nelson K.A."/>
        <person name="Nixon K."/>
        <person name="Nusskern D.R."/>
        <person name="Pacleb J.M."/>
        <person name="Palazzolo M."/>
        <person name="Pittman G.S."/>
        <person name="Pan S."/>
        <person name="Pollard J."/>
        <person name="Puri V."/>
        <person name="Reese M.G."/>
        <person name="Reinert K."/>
        <person name="Remington K."/>
        <person name="Saunders R.D.C."/>
        <person name="Scheeler F."/>
        <person name="Shen H."/>
        <person name="Shue B.C."/>
        <person name="Siden-Kiamos I."/>
        <person name="Simpson M."/>
        <person name="Skupski M.P."/>
        <person name="Smith T.J."/>
        <person name="Spier E."/>
        <person name="Spradling A.C."/>
        <person name="Stapleton M."/>
        <person name="Strong R."/>
        <person name="Sun E."/>
        <person name="Svirskas R."/>
        <person name="Tector C."/>
        <person name="Turner R."/>
        <person name="Venter E."/>
        <person name="Wang A.H."/>
        <person name="Wang X."/>
        <person name="Wang Z.-Y."/>
        <person name="Wassarman D.A."/>
        <person name="Weinstock G.M."/>
        <person name="Weissenbach J."/>
        <person name="Williams S.M."/>
        <person name="Woodage T."/>
        <person name="Worley K.C."/>
        <person name="Wu D."/>
        <person name="Yang S."/>
        <person name="Yao Q.A."/>
        <person name="Ye J."/>
        <person name="Yeh R.-F."/>
        <person name="Zaveri J.S."/>
        <person name="Zhan M."/>
        <person name="Zhang G."/>
        <person name="Zhao Q."/>
        <person name="Zheng L."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F.N."/>
        <person name="Zhong W."/>
        <person name="Zhou X."/>
        <person name="Zhu S.C."/>
        <person name="Zhu X."/>
        <person name="Smith H.O."/>
        <person name="Gibbs R.A."/>
        <person name="Myers E.W."/>
        <person name="Rubin G.M."/>
        <person name="Venter J.C."/>
      </authorList>
      <dbReference type="PubMed" id="10731132"/>
      <dbReference type="DOI" id="10.1126/science.287.5461.2185"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Berkeley</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2002" name="Genome Biol." volume="3" first="RESEARCH0083.1" last="RESEARCH0083.22">
      <title>Annotation of the Drosophila melanogaster euchromatic genome: a systematic review.</title>
      <authorList>
        <person name="Misra S."/>
        <person name="Crosby M.A."/>
        <person name="Mungall C.J."/>
        <person name="Matthews B.B."/>
        <person name="Campbell K.S."/>
        <person name="Hradecky P."/>
        <person name="Huang Y."/>
        <person name="Kaminker J.S."/>
        <person name="Millburn G.H."/>
        <person name="Prochnik S.E."/>
        <person name="Smith C.D."/>
        <person name="Tupy J.L."/>
        <person name="Whitfield E.J."/>
        <person name="Bayraktaroglu L."/>
        <person name="Berman B.P."/>
        <person name="Bettencourt B.R."/>
        <person name="Celniker S.E."/>
        <person name="de Grey A.D.N.J."/>
        <person name="Drysdale R.A."/>
        <person name="Harris N.L."/>
        <person name="Richter J."/>
        <person name="Russo S."/>
        <person name="Schroeder A.J."/>
        <person name="Shu S.Q."/>
        <person name="Stapleton M."/>
        <person name="Yamada C."/>
        <person name="Ashburner M."/>
        <person name="Gelbart W.M."/>
        <person name="Rubin G.M."/>
        <person name="Lewis S.E."/>
      </authorList>
      <dbReference type="PubMed" id="12537572"/>
      <dbReference type="DOI" id="10.1186/gb-2002-3-12-research0083"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>Berkeley</strain>
    </source>
  </reference>
  <reference key="7">
    <citation type="submission" date="2008-05" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Carlson J.W."/>
        <person name="Booth B."/>
        <person name="Frise E."/>
        <person name="Park S."/>
        <person name="Wan K.H."/>
        <person name="Yu C."/>
        <person name="Celniker S.E."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Berkeley</strain>
    </source>
  </reference>
  <reference key="8">
    <citation type="journal article" date="1990" name="EMBO J." volume="9" first="2969" last="2976">
      <title>The immune response in Drosophila: pattern of cecropin expression and biological activity.</title>
      <authorList>
        <person name="Samakovlis C."/>
        <person name="Kimbrell D.A."/>
        <person name="Kylsten P."/>
        <person name="Engstrom A."/>
        <person name="Hultmark D."/>
      </authorList>
      <dbReference type="PubMed" id="2390977"/>
      <dbReference type="DOI" id="10.1002/j.1460-2075.1990.tb07489.x"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INDUCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <strain>Canton-S</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="4">Cecropins have lytic and antibacterial activity against several Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Strongly expressed in larval, pupal and adult fat body and hemocytes after injection of bacteria. Maximal expression in the adult involves fat body cells of the head, thorax and abdomen.</text>
  </comment>
  <comment type="induction">
    <text evidence="3 4">Induced as part of the humoral response to a bacterial invasion (PubMed:2390977). Transcripts appear within one hour after injection of bacteria into the hemocoel, reach a maximum after 2-6 hours and have almost disappeared after 24 hours (PubMed:2104802). Similar response is seen when flies ingest bacteria present in their food (PubMed:2104802).</text>
  </comment>
  <comment type="similarity">
    <text evidence="9">Belongs to the cecropin family.</text>
  </comment>
  <comment type="sequence caution" evidence="9">
    <conflict type="frameshift">
      <sequence resource="EMBL-CDS" id="ACD81810" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="X16972">
    <property type="protein sequence ID" value="CAA34844.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF018976">
    <property type="protein sequence ID" value="AAB82473.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF018977">
    <property type="protein sequence ID" value="AAB82474.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF018978">
    <property type="protein sequence ID" value="AAB82475.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF018979">
    <property type="protein sequence ID" value="AAB82476.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF018980">
    <property type="protein sequence ID" value="AAB82477.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF018981">
    <property type="protein sequence ID" value="AAB82478.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF018982">
    <property type="protein sequence ID" value="AAB82479.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF018983">
    <property type="protein sequence ID" value="AAB82480.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF018984">
    <property type="protein sequence ID" value="AAB82481.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB010791">
    <property type="protein sequence ID" value="BAA28721.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB010792">
    <property type="protein sequence ID" value="BAA28725.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB010793">
    <property type="protein sequence ID" value="BAA28728.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB010794">
    <property type="protein sequence ID" value="BAA28732.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB010795">
    <property type="protein sequence ID" value="BAA28734.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB010796">
    <property type="protein sequence ID" value="BAA28738.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB010797">
    <property type="protein sequence ID" value="BAA28742.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Y16852">
    <property type="protein sequence ID" value="CAA76427.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Y16853">
    <property type="protein sequence ID" value="CAA76433.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Y16855">
    <property type="protein sequence ID" value="CAA76445.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Y16857">
    <property type="protein sequence ID" value="CAA76457.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Y16858">
    <property type="protein sequence ID" value="CAA76463.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Y16859">
    <property type="protein sequence ID" value="CAA76469.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Y16861">
    <property type="protein sequence ID" value="CAA76479.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AE014297">
    <property type="protein sequence ID" value="AAF57026.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT032790">
    <property type="protein sequence ID" value="ACD81804.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT032796">
    <property type="protein sequence ID" value="ACD81810.1"/>
    <property type="status" value="ALT_FRAME"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S07664">
    <property type="entry name" value="S07664"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_524588.1">
    <property type="nucleotide sequence ID" value="NM_079849.4"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_524589.1">
    <property type="nucleotide sequence ID" value="NM_079850.4"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="C0HKQ8"/>
  <dbReference type="SMR" id="C0HKQ8"/>
  <dbReference type="DNASU" id="43597"/>
  <dbReference type="EnsemblMetazoa" id="FBtr0085612">
    <property type="protein sequence ID" value="FBpp0084977"/>
    <property type="gene ID" value="FBgn0000276"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="FBtr0085614">
    <property type="protein sequence ID" value="FBpp0084978"/>
    <property type="gene ID" value="FBgn0000277"/>
  </dbReference>
  <dbReference type="GeneID" id="43596"/>
  <dbReference type="GeneID" id="43597"/>
  <dbReference type="KEGG" id="dme:Dmel_CG1365"/>
  <dbReference type="KEGG" id="dme:Dmel_CG1367"/>
  <dbReference type="AGR" id="FB:FBgn0000277"/>
  <dbReference type="CTD" id="43596"/>
  <dbReference type="CTD" id="43597"/>
  <dbReference type="FlyBase" id="FBgn0000277">
    <property type="gene designation" value="CecA2"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="VectorBase:FBgn0000276"/>
  <dbReference type="VEuPathDB" id="VectorBase:FBgn0000277"/>
  <dbReference type="InParanoid" id="C0HKQ8"/>
  <dbReference type="OMA" id="NRIFVFV"/>
  <dbReference type="OrthoDB" id="3613173at2759"/>
  <dbReference type="PRO" id="PR:C0HKQ8"/>
  <dbReference type="Proteomes" id="UP000000803">
    <property type="component" value="Chromosome 3R"/>
  </dbReference>
  <dbReference type="Bgee" id="FBgn0000276">
    <property type="expression patterns" value="Expressed in male reproductive gland and 12 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="C0HKQ8">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002213">
    <property type="term" value="P:defense response to insect"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051607">
    <property type="term" value="P:defense response to virus"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006959">
    <property type="term" value="P:humoral immune response"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0140460">
    <property type="term" value="P:response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000316"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000875">
    <property type="entry name" value="Cecropin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020400">
    <property type="entry name" value="Cecropin_insect"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR38329">
    <property type="entry name" value="CECROPIN-A1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR38329:SF1">
    <property type="entry name" value="CECROPIN-A1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00272">
    <property type="entry name" value="Cecropin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00268">
    <property type="entry name" value="CECROPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000441213" description="Cecropin-A2">
    <location>
      <begin position="24"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="modified residue" description="Arginine amide" evidence="1">
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In strain: Z5." evidence="5">
    <original>N</original>
    <variation>D</variation>
    <location>
      <position position="2"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In strain: Z24." evidence="5">
    <original>N</original>
    <variation>K</variation>
    <location>
      <position position="2"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In strain: M2, MJ1 and MJ3." evidence="6 7">
    <original>F</original>
    <variation>L</variation>
    <location>
      <position position="3"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In strain: B226." evidence="5">
    <original>F</original>
    <variation>L</variation>
    <location>
      <position position="7"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In strain: Z5." evidence="5 7">
    <original>V</original>
    <variation>G</variation>
    <location>
      <position position="8"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In strain: B115, M11 and M47." evidence="5 7">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="11"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In strain: MJ2." evidence="6">
    <original>V</original>
    <variation>A</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 7; ACD81810." evidence="9" ref="7">
    <original>K</original>
    <variation>R</variation>
    <location>
      <position position="31"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P08375"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="2104802"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="2390977"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="9335607"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="9553148"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="9725836"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="2104802"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="9"/>
  <evidence type="ECO:0000312" key="10">
    <source>
      <dbReference type="FlyBase" id="FBgn0000277"/>
    </source>
  </evidence>
  <sequence length="63" mass="6771" checksum="F709070C5BEC7D74" modified="2017-08-30" version="1" precursor="true">MNFYNIFVFVALILAITIGQSEAGWLKKIGKKIERVGQHTRDATIQGLGIAQQAANVAATARG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>