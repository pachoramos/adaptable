<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-12-06" modified="2024-10-02" version="133" xmlns="http://uniprot.org/uniprot">
  <accession>O96690</accession>
  <accession>Q7JYY1</accession>
  <accession>Q9VBE9</accession>
  <name>PDF_DROME</name>
  <protein>
    <recommendedName>
      <fullName>Protein PDF</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>PDF precursor-related peptide</fullName>
        <shortName>PAP</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Neuropeptide PDF</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Pigment-dispersing factor homolog</fullName>
      </alternativeName>
    </component>
  </protein>
  <gene>
    <name type="primary">Pdf</name>
    <name type="ORF">CG6496</name>
  </gene>
  <organism>
    <name type="scientific">Drosophila melanogaster</name>
    <name type="common">Fruit fly</name>
    <dbReference type="NCBI Taxonomy" id="7227"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Ephydroidea</taxon>
      <taxon>Drosophilidae</taxon>
      <taxon>Drosophila</taxon>
      <taxon>Sophophora</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="J. Biol. Rhythms" volume="13" first="219" last="228">
      <title>Isolation and chronobiological analysis of a neuropeptide pigment-dispersing factor gene in Drosophila melanogaster.</title>
      <authorList>
        <person name="Park J.H."/>
        <person name="Hall J.C."/>
      </authorList>
      <dbReference type="PubMed" id="9615286"/>
      <dbReference type="DOI" id="10.1177/074873098129000066"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <source>
      <strain>Canton-S</strain>
      <tissue>Head</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2000" name="Science" volume="287" first="2185" last="2195">
      <title>The genome sequence of Drosophila melanogaster.</title>
      <authorList>
        <person name="Adams M.D."/>
        <person name="Celniker S.E."/>
        <person name="Holt R.A."/>
        <person name="Evans C.A."/>
        <person name="Gocayne J.D."/>
        <person name="Amanatides P.G."/>
        <person name="Scherer S.E."/>
        <person name="Li P.W."/>
        <person name="Hoskins R.A."/>
        <person name="Galle R.F."/>
        <person name="George R.A."/>
        <person name="Lewis S.E."/>
        <person name="Richards S."/>
        <person name="Ashburner M."/>
        <person name="Henderson S.N."/>
        <person name="Sutton G.G."/>
        <person name="Wortman J.R."/>
        <person name="Yandell M.D."/>
        <person name="Zhang Q."/>
        <person name="Chen L.X."/>
        <person name="Brandon R.C."/>
        <person name="Rogers Y.-H.C."/>
        <person name="Blazej R.G."/>
        <person name="Champe M."/>
        <person name="Pfeiffer B.D."/>
        <person name="Wan K.H."/>
        <person name="Doyle C."/>
        <person name="Baxter E.G."/>
        <person name="Helt G."/>
        <person name="Nelson C.R."/>
        <person name="Miklos G.L.G."/>
        <person name="Abril J.F."/>
        <person name="Agbayani A."/>
        <person name="An H.-J."/>
        <person name="Andrews-Pfannkoch C."/>
        <person name="Baldwin D."/>
        <person name="Ballew R.M."/>
        <person name="Basu A."/>
        <person name="Baxendale J."/>
        <person name="Bayraktaroglu L."/>
        <person name="Beasley E.M."/>
        <person name="Beeson K.Y."/>
        <person name="Benos P.V."/>
        <person name="Berman B.P."/>
        <person name="Bhandari D."/>
        <person name="Bolshakov S."/>
        <person name="Borkova D."/>
        <person name="Botchan M.R."/>
        <person name="Bouck J."/>
        <person name="Brokstein P."/>
        <person name="Brottier P."/>
        <person name="Burtis K.C."/>
        <person name="Busam D.A."/>
        <person name="Butler H."/>
        <person name="Cadieu E."/>
        <person name="Center A."/>
        <person name="Chandra I."/>
        <person name="Cherry J.M."/>
        <person name="Cawley S."/>
        <person name="Dahlke C."/>
        <person name="Davenport L.B."/>
        <person name="Davies P."/>
        <person name="de Pablos B."/>
        <person name="Delcher A."/>
        <person name="Deng Z."/>
        <person name="Mays A.D."/>
        <person name="Dew I."/>
        <person name="Dietz S.M."/>
        <person name="Dodson K."/>
        <person name="Doup L.E."/>
        <person name="Downes M."/>
        <person name="Dugan-Rocha S."/>
        <person name="Dunkov B.C."/>
        <person name="Dunn P."/>
        <person name="Durbin K.J."/>
        <person name="Evangelista C.C."/>
        <person name="Ferraz C."/>
        <person name="Ferriera S."/>
        <person name="Fleischmann W."/>
        <person name="Fosler C."/>
        <person name="Gabrielian A.E."/>
        <person name="Garg N.S."/>
        <person name="Gelbart W.M."/>
        <person name="Glasser K."/>
        <person name="Glodek A."/>
        <person name="Gong F."/>
        <person name="Gorrell J.H."/>
        <person name="Gu Z."/>
        <person name="Guan P."/>
        <person name="Harris M."/>
        <person name="Harris N.L."/>
        <person name="Harvey D.A."/>
        <person name="Heiman T.J."/>
        <person name="Hernandez J.R."/>
        <person name="Houck J."/>
        <person name="Hostin D."/>
        <person name="Houston K.A."/>
        <person name="Howland T.J."/>
        <person name="Wei M.-H."/>
        <person name="Ibegwam C."/>
        <person name="Jalali M."/>
        <person name="Kalush F."/>
        <person name="Karpen G.H."/>
        <person name="Ke Z."/>
        <person name="Kennison J.A."/>
        <person name="Ketchum K.A."/>
        <person name="Kimmel B.E."/>
        <person name="Kodira C.D."/>
        <person name="Kraft C.L."/>
        <person name="Kravitz S."/>
        <person name="Kulp D."/>
        <person name="Lai Z."/>
        <person name="Lasko P."/>
        <person name="Lei Y."/>
        <person name="Levitsky A.A."/>
        <person name="Li J.H."/>
        <person name="Li Z."/>
        <person name="Liang Y."/>
        <person name="Lin X."/>
        <person name="Liu X."/>
        <person name="Mattei B."/>
        <person name="McIntosh T.C."/>
        <person name="McLeod M.P."/>
        <person name="McPherson D."/>
        <person name="Merkulov G."/>
        <person name="Milshina N.V."/>
        <person name="Mobarry C."/>
        <person name="Morris J."/>
        <person name="Moshrefi A."/>
        <person name="Mount S.M."/>
        <person name="Moy M."/>
        <person name="Murphy B."/>
        <person name="Murphy L."/>
        <person name="Muzny D.M."/>
        <person name="Nelson D.L."/>
        <person name="Nelson D.R."/>
        <person name="Nelson K.A."/>
        <person name="Nixon K."/>
        <person name="Nusskern D.R."/>
        <person name="Pacleb J.M."/>
        <person name="Palazzolo M."/>
        <person name="Pittman G.S."/>
        <person name="Pan S."/>
        <person name="Pollard J."/>
        <person name="Puri V."/>
        <person name="Reese M.G."/>
        <person name="Reinert K."/>
        <person name="Remington K."/>
        <person name="Saunders R.D.C."/>
        <person name="Scheeler F."/>
        <person name="Shen H."/>
        <person name="Shue B.C."/>
        <person name="Siden-Kiamos I."/>
        <person name="Simpson M."/>
        <person name="Skupski M.P."/>
        <person name="Smith T.J."/>
        <person name="Spier E."/>
        <person name="Spradling A.C."/>
        <person name="Stapleton M."/>
        <person name="Strong R."/>
        <person name="Sun E."/>
        <person name="Svirskas R."/>
        <person name="Tector C."/>
        <person name="Turner R."/>
        <person name="Venter E."/>
        <person name="Wang A.H."/>
        <person name="Wang X."/>
        <person name="Wang Z.-Y."/>
        <person name="Wassarman D.A."/>
        <person name="Weinstock G.M."/>
        <person name="Weissenbach J."/>
        <person name="Williams S.M."/>
        <person name="Woodage T."/>
        <person name="Worley K.C."/>
        <person name="Wu D."/>
        <person name="Yang S."/>
        <person name="Yao Q.A."/>
        <person name="Ye J."/>
        <person name="Yeh R.-F."/>
        <person name="Zaveri J.S."/>
        <person name="Zhan M."/>
        <person name="Zhang G."/>
        <person name="Zhao Q."/>
        <person name="Zheng L."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F.N."/>
        <person name="Zhong W."/>
        <person name="Zhou X."/>
        <person name="Zhu S.C."/>
        <person name="Zhu X."/>
        <person name="Smith H.O."/>
        <person name="Gibbs R.A."/>
        <person name="Myers E.W."/>
        <person name="Rubin G.M."/>
        <person name="Venter J.C."/>
      </authorList>
      <dbReference type="PubMed" id="10731132"/>
      <dbReference type="DOI" id="10.1126/science.287.5461.2185"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Berkeley</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Genome Biol." volume="3" first="RESEARCH0083.1" last="RESEARCH0083.22">
      <title>Annotation of the Drosophila melanogaster euchromatic genome: a systematic review.</title>
      <authorList>
        <person name="Misra S."/>
        <person name="Crosby M.A."/>
        <person name="Mungall C.J."/>
        <person name="Matthews B.B."/>
        <person name="Campbell K.S."/>
        <person name="Hradecky P."/>
        <person name="Huang Y."/>
        <person name="Kaminker J.S."/>
        <person name="Millburn G.H."/>
        <person name="Prochnik S.E."/>
        <person name="Smith C.D."/>
        <person name="Tupy J.L."/>
        <person name="Whitfield E.J."/>
        <person name="Bayraktaroglu L."/>
        <person name="Berman B.P."/>
        <person name="Bettencourt B.R."/>
        <person name="Celniker S.E."/>
        <person name="de Grey A.D.N.J."/>
        <person name="Drysdale R.A."/>
        <person name="Harris N.L."/>
        <person name="Richter J."/>
        <person name="Russo S."/>
        <person name="Schroeder A.J."/>
        <person name="Shu S.Q."/>
        <person name="Stapleton M."/>
        <person name="Yamada C."/>
        <person name="Ashburner M."/>
        <person name="Gelbart W.M."/>
        <person name="Rubin G.M."/>
        <person name="Lewis S.E."/>
      </authorList>
      <dbReference type="PubMed" id="12537572"/>
      <dbReference type="DOI" id="10.1186/gb-2002-3-12-research0083"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>Berkeley</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2002" name="Genome Biol." volume="3" first="RESEARCH0080.1" last="RESEARCH0080.8">
      <title>A Drosophila full-length cDNA resource.</title>
      <authorList>
        <person name="Stapleton M."/>
        <person name="Carlson J.W."/>
        <person name="Brokstein P."/>
        <person name="Yu C."/>
        <person name="Champe M."/>
        <person name="George R.A."/>
        <person name="Guarin H."/>
        <person name="Kronmiller B."/>
        <person name="Pacleb J.M."/>
        <person name="Park S."/>
        <person name="Wan K.H."/>
        <person name="Rubin G.M."/>
        <person name="Celniker S.E."/>
      </authorList>
      <dbReference type="PubMed" id="12537569"/>
      <dbReference type="DOI" id="10.1186/gb-2002-3-12-research0080"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Berkeley</strain>
      <tissue>Head</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2011" name="J. Proteome Res." volume="10" first="1881" last="1892">
      <title>Peptidomics and peptide hormone processing in the Drosophila midgut.</title>
      <authorList>
        <person name="Reiher W."/>
        <person name="Shirras C."/>
        <person name="Kahnt J."/>
        <person name="Baumeister S."/>
        <person name="Isaac R.E."/>
        <person name="Wegener C."/>
      </authorList>
      <dbReference type="PubMed" id="21214272"/>
      <dbReference type="DOI" id="10.1021/pr101116g"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 83-100</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT ALA-100</scope>
    <source>
      <tissue evidence="12">Midgut</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1999" name="Cell" volume="99" first="791" last="802">
      <title>A pdf neuropeptide gene mutation and ablation of PDF neurons each cause severe abnormalities of behavioral circadian rhythms in Drosophila.</title>
      <authorList>
        <person name="Renn S.C.C."/>
        <person name="Park J.H."/>
        <person name="Rosbash M."/>
        <person name="Hall J.C."/>
        <person name="Taghert P.H."/>
      </authorList>
      <dbReference type="PubMed" id="10619432"/>
      <dbReference type="DOI" id="10.1016/s0092-8674(00)81676-1"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1999" name="Cell" volume="101" first="114" last="114">
      <authorList>
        <person name="Renn S.C.C."/>
        <person name="Park J.H."/>
        <person name="Rosbash M."/>
        <person name="Hall J.C."/>
        <person name="Taghert P.H."/>
      </authorList>
    </citation>
    <scope>ERRATUM OF PUBMED:10619432</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2000" name="Proc. Natl. Acad. Sci. U.S.A." volume="97" first="3608" last="3613">
      <title>Differential regulation of circadian pacemaker output by separate clock genes in Drosophila.</title>
      <authorList>
        <person name="Park J.H."/>
        <person name="Helfrich-Foerster C."/>
        <person name="Lee G."/>
        <person name="Liu L."/>
        <person name="Rosbash M."/>
        <person name="Hall J.C."/>
      </authorList>
      <dbReference type="PubMed" id="10725392"/>
      <dbReference type="DOI" id="10.1073/pnas.97.7.3608"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2000" name="J. Neurosci." volume="20" first="3339" last="3353">
      <title>Ectopic expression of the neuropeptide pigment-dispersing factor alters behavioral rhythms in Drosophila melanogaster.</title>
      <authorList>
        <person name="Helfrich-Foerster C."/>
        <person name="Taueber M."/>
        <person name="Park J.H."/>
        <person name="Muehlig-Versen M."/>
        <person name="Schneuwly S."/>
        <person name="Hofbauer A."/>
      </authorList>
      <dbReference type="PubMed" id="10777797"/>
      <dbReference type="DOI" id="10.1523/jneurosci.20-09-03339.2000"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2004" name="J. Neurosci." volume="24" first="7951" last="7957">
      <title>The neuropeptide pigment-dispersing factor coordinates pacemaker interactions in the Drosophila circadian system.</title>
      <authorList>
        <person name="Lin Y."/>
        <person name="Stormo G.D."/>
        <person name="Taghert P.H."/>
      </authorList>
      <dbReference type="PubMed" id="15356209"/>
      <dbReference type="DOI" id="10.1523/jneurosci.2370-04.2004"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2008" name="Neuron" volume="60" first="672" last="682">
      <title>PDF cells are a GABA-responsive wake-promoting component of the Drosophila sleep circuit.</title>
      <authorList>
        <person name="Parisky K.M."/>
        <person name="Agosto J."/>
        <person name="Pulver S.R."/>
        <person name="Shang Y."/>
        <person name="Kuklin E."/>
        <person name="Hodge J.J."/>
        <person name="Kang K."/>
        <person name="Kang K."/>
        <person name="Liu X."/>
        <person name="Garrity P.A."/>
        <person name="Rosbash M."/>
        <person name="Griffith L.C."/>
      </authorList>
      <dbReference type="PubMed" id="19038223"/>
      <dbReference type="DOI" id="10.1016/j.neuron.2008.10.042"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2009" name="Curr. Biol." volume="19" first="386" last="390">
      <title>The GABA(A) receptor RDL acts in peptidergic PDF neurons to promote sleep in Drosophila.</title>
      <authorList>
        <person name="Chung B.Y."/>
        <person name="Kilman V.L."/>
        <person name="Keath J.R."/>
        <person name="Pitman J.L."/>
        <person name="Allada R."/>
      </authorList>
      <dbReference type="PubMed" id="19230663"/>
      <dbReference type="DOI" id="10.1016/j.cub.2009.01.040"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2023" name="Chronobiol. Int." volume="40" first="1" last="16">
      <title>Pigment-dispersing factor and CCHamide1 in the Drosophila circadian clock network.</title>
      <authorList>
        <person name="Kuwano R."/>
        <person name="Katsura M."/>
        <person name="Iwata M."/>
        <person name="Yokosako T."/>
        <person name="Yoshii T."/>
      </authorList>
      <dbReference type="PubMed" id="36786215"/>
      <dbReference type="DOI" id="10.1080/07420528.2023.2166416"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <comment type="function">
    <text evidence="3 5 6 7 8 10">Neuropeptide PDF is the main transmitter regulating circadian locomotor rhythms. Required to maintain behavioral rhythms under constant conditions by coordinating pacemaker interactions in the circadian system (PubMed:10619432, PubMed:10777797, PubMed:15356209). Together with CCHa1, involved in regulating intensity and periodicity of daytime activity, possibly by modulating rhythmic expression of circadian protein PER/period in a subset of clock neurons, but not TIM/timeless (PubMed:36786215). Acts on small and large ventral lateral neurons to control sleep and regulates the state transition from sleep to wake (PubMed:19038223, PubMed:19230663).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3 4 11">Predominantly expressed in adult head. Expressed at higher level in males than in females. In adult brain, it is specifically expressed in the ventral lateral neurons (LNvs) as well as in 2-4 tritocerebral cells and 4-6 abdominal cells.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="4 11">Expressed at constant level, without strong circadian variations. Its amount nevertheless oscillates along day and night cycles in the termini of dorsally projected axons of LNvs.</text>
  </comment>
  <comment type="mass spectrometry" mass="1972.02" method="MALDI" evidence="9">
    <molecule>Neuropeptide PDF</molecule>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="3 7 8 10">Increases total sleep time, decreases sleep latency, leads to loss of morning activity peak, and a failure to wake up in advance of light-on anticipation (PubMed:19038223, PubMed:19230663). In a CCHa1 mutant background, diminished morning activity and phase-advanced evening activity compared to single mutant (PubMed:36786215). No significant effect on free-running period but reduced power and rhythmicity of free-running activity, which is further reduced in a CCHa1 mutant background (PubMed:10619432, PubMed:36786215). No effect on photic entrainment of circadian rhythms (PubMed:36786215).</text>
  </comment>
  <comment type="similarity">
    <text evidence="13">Belongs to the arthropod PDH family.</text>
  </comment>
  <comment type="sequence caution" evidence="13">
    <conflict type="erroneous initiation">
      <sequence resource="EMBL-CDS" id="AAL49303" version="2"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="AF110059">
    <property type="protein sequence ID" value="AAC98309.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AE014297">
    <property type="protein sequence ID" value="AAF56593.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY071681">
    <property type="protein sequence ID" value="AAL49303.2"/>
    <property type="status" value="ALT_INIT"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_524517.1">
    <property type="nucleotide sequence ID" value="NM_079793.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O96690"/>
  <dbReference type="BioGRID" id="68089">
    <property type="interactions" value="4"/>
  </dbReference>
  <dbReference type="IntAct" id="O96690">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="7227.FBpp0084396"/>
  <dbReference type="PaxDb" id="7227-FBpp0084396"/>
  <dbReference type="DNASU" id="43193"/>
  <dbReference type="EnsemblMetazoa" id="FBtr0085024">
    <property type="protein sequence ID" value="FBpp0084396"/>
    <property type="gene ID" value="FBgn0023178"/>
  </dbReference>
  <dbReference type="GeneID" id="43193"/>
  <dbReference type="KEGG" id="dme:Dmel_CG6496"/>
  <dbReference type="AGR" id="FB:FBgn0023178"/>
  <dbReference type="CTD" id="64146"/>
  <dbReference type="FlyBase" id="FBgn0023178">
    <property type="gene designation" value="Pdf"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="VectorBase:FBgn0023178"/>
  <dbReference type="eggNOG" id="ENOG502T6YK">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_2335781_0_0_1"/>
  <dbReference type="InParanoid" id="O96690"/>
  <dbReference type="OMA" id="MPDEERY"/>
  <dbReference type="OrthoDB" id="3681181at2759"/>
  <dbReference type="PhylomeDB" id="O96690"/>
  <dbReference type="BioGRID-ORCS" id="43193">
    <property type="hits" value="0 hits in 1 CRISPR screen"/>
  </dbReference>
  <dbReference type="GenomeRNAi" id="43193"/>
  <dbReference type="PRO" id="PR:O96690"/>
  <dbReference type="Proteomes" id="UP000000803">
    <property type="component" value="Chromosome 3R"/>
  </dbReference>
  <dbReference type="Bgee" id="FBgn0023178">
    <property type="expression patterns" value="Expressed in brain and 10 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="O96690">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043005">
    <property type="term" value="C:neuron projection"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043025">
    <property type="term" value="C:neuronal cell body"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005102">
    <property type="term" value="F:signaling receptor binding"/>
    <property type="evidence" value="ECO:0000353"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007189">
    <property type="term" value="P:adenylate cyclase-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007623">
    <property type="term" value="P:circadian rhythm"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042745">
    <property type="term" value="P:circadian sleep/wake cycle"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008062">
    <property type="term" value="P:eclosion rhythm"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043153">
    <property type="term" value="P:entrainment of circadian clock by photoperiod"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042332">
    <property type="term" value="P:gravitaxis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045475">
    <property type="term" value="P:locomotor rhythm"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007617">
    <property type="term" value="P:mating behavior"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010841">
    <property type="term" value="P:positive regulation of circadian sleep/wake cycle, wakefulness"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042749">
    <property type="term" value="P:regulation of circadian sleep/wake cycle"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:1904059">
    <property type="term" value="P:regulation of locomotor rhythm"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:1901562">
    <property type="term" value="P:response to paraquat"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009396">
    <property type="entry name" value="Pigment_DH"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06324">
    <property type="entry name" value="Pigment_DH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0090">Biological rhythms</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000043162" description="PDF precursor-related peptide" evidence="1">
    <location>
      <begin position="25"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000043163" description="Neuropeptide PDF" evidence="9">
    <location>
      <begin position="83"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="modified residue" description="Alanine amide" evidence="9">
    <location>
      <position position="100"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAC98309." evidence="13" ref="1">
    <original>Y</original>
    <variation>F</variation>
    <location>
      <position position="4"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="10619432"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="10725392"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="10777797"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="15356209"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="19038223"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="19230663"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="21214272"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="36786215"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="11">
    <source>
      <dbReference type="PubMed" id="9615286"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="21214272"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13"/>
  <sequence length="102" mass="11545" checksum="7F7E0CAA40AE39C2" modified="2005-12-06" version="2" precursor="true">MARYTYLVALVLLAICCQWGYCGAMAMPDEERYVRKEYNRDLLDWFNNVGVGQFSPGQVATLCRYPLILENSLGPSVPIRKRNSELINSLLSLPKNMNDAGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>