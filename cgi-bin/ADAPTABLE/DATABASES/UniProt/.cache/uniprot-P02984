<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-05-29" version="121" xmlns="http://uniprot.org/uniprot">
  <accession>P02984</accession>
  <name>IMM3_ECOLX</name>
  <protein>
    <recommendedName>
      <fullName>Colicin E3 immunity protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6 7">Colicin E3 chain B</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>ImmE3</fullName>
      <shortName evidence="5">Im3</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Microcin-E3 immunity protein</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="7" type="primary">imm</name>
    <name type="synonym">ceiC</name>
    <name type="synonym">immB</name>
  </gene>
  <organism>
    <name type="scientific">Escherichia coli</name>
    <dbReference type="NCBI Taxonomy" id="562"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Escherichia</taxon>
    </lineage>
  </organism>
  <geneLocation type="plasmid">
    <name>ColE3-CA38</name>
  </geneLocation>
  <reference key="1">
    <citation type="journal article" date="1982" name="FEBS Lett." volume="149" first="129" last="132">
      <title>A plasmid region encoding the active fragment and the inhibitor protein of colicin E3-CA38.</title>
      <authorList>
        <person name="Masaki H."/>
        <person name="Ohta T."/>
      </authorList>
      <dbReference type="PubMed" id="6295812"/>
      <dbReference type="DOI" id="10.1016/0014-5793(82)81087-9"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
    <source>
      <plasmid>ColE3-CA38</plasmid>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1985" name="J. Mol. Biol." volume="182" first="217" last="227">
      <title>Colicin E3 and its immunity genes.</title>
      <authorList>
        <person name="Masaki H."/>
        <person name="Ohta T."/>
      </authorList>
      <dbReference type="PubMed" id="3889348"/>
      <dbReference type="DOI" id="10.1016/0022-2836(85)90340-7"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>INDUCTION</scope>
    <source>
      <plasmid>ColE3-CA38</plasmid>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1983" name="Nucleic Acids Res." volume="11" first="3547" last="3557">
      <title>Nucleotide sequence for the catalytic domain of colicin E3 and its immunity protein. Evidence for a third gene overlapping colicin.</title>
      <authorList>
        <person name="Mock M."/>
        <person name="Miyada C.G."/>
        <person name="Gunsalus R.P."/>
      </authorList>
      <dbReference type="PubMed" id="6344012"/>
      <dbReference type="DOI" id="10.1093/nar/11.11.3547"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1984" name="Nucleic Acids Res." volume="12" first="8733" last="8745">
      <title>Comparative nucleotide sequences encoding the immunity proteins and the carboxyl-terminal peptides of colicins E2 and E3.</title>
      <authorList>
        <person name="Lau P.C.K."/>
        <person name="Rowsome R.W."/>
        <person name="Zuker M."/>
        <person name="Visentin L.P."/>
      </authorList>
      <dbReference type="PubMed" id="6095211"/>
      <dbReference type="DOI" id="10.1093/nar/12.22.8733"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <plasmid>ColE3-CA38</plasmid>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1984" name="Biosci. Rep." volume="4" first="565" last="572">
      <title>The immunity genes of colicins E2 and E8 are closely related.</title>
      <authorList>
        <person name="Lau P.C."/>
        <person name="Rowsome R.W."/>
        <person name="Watson R.J."/>
        <person name="Visentin L.P."/>
      </authorList>
      <dbReference type="PubMed" id="6383494"/>
      <dbReference type="DOI" id="10.1007/bf01121913"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1977" name="J. Biochem." volume="82" first="1045" last="1053">
      <title>Purification and characterization of active component and active fragment of colicin E3.</title>
      <authorList>
        <person name="Ohno S."/>
        <person name="Ohno-Iwashita Y."/>
        <person name="Suzuki K."/>
        <person name="Imahori K."/>
      </authorList>
      <dbReference type="PubMed" id="336615"/>
      <dbReference type="DOI" id="10.1093/oxfordjournals.jbchem.a131775"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1992" name="Biochemistry" volume="31" first="5578" last="5586">
      <title>The secondary structure of the colicin E3 immunity protein as studied by 1H-1H and 1H-15N two-dimensional NMR spectroscopy.</title>
      <authorList>
        <person name="Yajima S."/>
        <person name="Muto Y."/>
        <person name="Yokoyama S."/>
        <person name="Masaki H."/>
        <person name="Uozumi T."/>
      </authorList>
      <dbReference type="PubMed" id="1610804"/>
      <dbReference type="DOI" id="10.1021/bi00139a022"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <reference evidence="13" key="8">
    <citation type="journal article" date="1999" name="Structure" volume="7" first="1365" last="1372">
      <title>Crystal structure of colicin E3 immunity protein: an inhibitor of a ribosome-inactivating RNase.</title>
      <authorList>
        <person name="Li C."/>
        <person name="Zhao D."/>
        <person name="Djebli A."/>
        <person name="Shoham M."/>
      </authorList>
      <dbReference type="PubMed" id="10574790"/>
      <dbReference type="DOI" id="10.1016/s0969-2126(00)80026-x"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.80 ANGSTROMS) OF 2-85</scope>
  </reference>
  <reference evidence="11" key="9">
    <citation type="journal article" date="2000" name="Structure" volume="8" first="949" last="960">
      <title>Inhibition of a ribosome-inactivating ribonuclease: the crystal structure of the cytotoxic domain of colicin E3 in complex with its immunity protein.</title>
      <authorList>
        <person name="Carr S."/>
        <person name="Walker D."/>
        <person name="James R."/>
        <person name="Kleanthous C."/>
        <person name="Hemmings A.M."/>
      </authorList>
      <dbReference type="PubMed" id="10986462"/>
      <dbReference type="DOI" id="10.1016/s0969-2126(00)00186-6"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.40 ANGSTROMS) IN COMPLEX WITH TOXIC COLICIN E3 FRAGMENT</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>DOMAIN</scope>
  </reference>
  <reference evidence="12" key="10">
    <citation type="journal article" date="2001" name="Mol. Cell" volume="8" first="1053" last="1062">
      <title>Crystal structure of colicin E3: implications for cell entry and ribosome inactivation.</title>
      <authorList>
        <person name="Soelaiman S."/>
        <person name="Jakes K."/>
        <person name="Wu N."/>
        <person name="Li C."/>
        <person name="Shoham M."/>
      </authorList>
      <dbReference type="PubMed" id="11741540"/>
      <dbReference type="DOI" id="10.1016/s1097-2765(01)00396-3"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (3.02 ANGSTROMS) OF 2-85 IN COMPLEX WITH COLICIN E3</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>DOMAIN</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3 4">The cognate immunity protein for colicin E3 (ColE3), protects cells which harbor the plasmid ColE3 against the toxic action of ColE3 (PubMed:10986462, PubMed:11741540, PubMed:336615, PubMed:6295812). This protein inhibits the 16S RNA hydrolyzing activity of ColE3 by binding with very high affinity to the C-terminal catalytic domain of ColE3 (PubMed:10986462, PubMed:11741540).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 2 3">Native colicin E3 is a 1:1 complex of A chain and protein B (Im3) (PubMed:10986462, PubMed:11741540, PubMed:336615). Binds between the translocation and cytotoxic RNase domains of intact ColE3, blocking access to the 16S rRNA substrate (PubMed:11741540). Forms a very tight 1:1 complex with the cytotoxic fragment (residues 456-551) of ColE3 (ceaC) (PubMed:10986462, PubMed:11741540).</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-1029912">
      <id>P02984</id>
    </interactant>
    <interactant intactId="EBI-1029919">
      <id>P00646</id>
      <label>ceaC</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="induction">
    <text evidence="10">Probably induced by SOS-stress.</text>
  </comment>
  <comment type="domain">
    <text evidence="1 2">In complex with the cytotoxic fragment of colicin E3, Im3 residues 39-45 move outward to form a wide hydrophobic groove at the top, which interacts with an alpha-helix of colicin E3 (PubMed:10986462, PubMed:11741540).</text>
  </comment>
  <comment type="similarity">
    <text evidence="8">Belongs to the cloacin immunity protein family.</text>
  </comment>
  <dbReference type="EMBL" id="J01574">
    <property type="protein sequence ID" value="AAA88417.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X02397">
    <property type="protein sequence ID" value="CAA26242.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X01162">
    <property type="protein sequence ID" value="CAA25608.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M26136">
    <property type="protein sequence ID" value="AAA98285.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="B91307">
    <property type="entry name" value="IMECE3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000523346.1">
    <property type="nucleotide sequence ID" value="NZ_WXYX01000010.1"/>
  </dbReference>
  <dbReference type="PDB" id="1E44">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.40 A"/>
    <property type="chains" value="A=1-85"/>
  </dbReference>
  <dbReference type="PDB" id="1JCH">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.02 A"/>
    <property type="chains" value="B/D=2-85"/>
  </dbReference>
  <dbReference type="PDB" id="2B5U">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="B/D=2-85"/>
  </dbReference>
  <dbReference type="PDB" id="3EIP">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.80 A"/>
    <property type="chains" value="A/B=2-85"/>
  </dbReference>
  <dbReference type="PDB" id="4UDM">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.96 A"/>
    <property type="chains" value="A=1-85"/>
  </dbReference>
  <dbReference type="PDBsum" id="1E44"/>
  <dbReference type="PDBsum" id="1JCH"/>
  <dbReference type="PDBsum" id="2B5U"/>
  <dbReference type="PDBsum" id="3EIP"/>
  <dbReference type="PDBsum" id="4UDM"/>
  <dbReference type="AlphaFoldDB" id="P02984"/>
  <dbReference type="SMR" id="P02984"/>
  <dbReference type="DIP" id="DIP-16993N"/>
  <dbReference type="IntAct" id="P02984">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="TCDB" id="8.B.24.4.1">
    <property type="family name" value="the colicin immunity protein (colip) functional family"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P02984"/>
  <dbReference type="GO" id="GO:0015643">
    <property type="term" value="F:toxic substance binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030153">
    <property type="term" value="P:bacteriocin immunity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.50.20">
    <property type="entry name" value="Cloacin immunity protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003063">
    <property type="entry name" value="Cloacn_immnty_fam"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036528">
    <property type="entry name" value="Cloacn_immnty_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03513">
    <property type="entry name" value="Cloacin_immun"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01296">
    <property type="entry name" value="CLOACNIMMNTY"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54552">
    <property type="entry name" value="Colicin E3 immunity protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0079">Bacteriocin immunity</keyword>
  <keyword id="KW-0614">Plasmid</keyword>
  <feature type="initiator methionine" description="Removed" evidence="9">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000218704" description="Colicin E3 immunity protein">
    <location>
      <begin position="2"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="strand" evidence="14">
    <location>
      <begin position="3"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="turn" evidence="14">
    <location>
      <begin position="12"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="strand" evidence="14">
    <location>
      <begin position="17"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="helix" evidence="14">
    <location>
      <begin position="31"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="turn" evidence="14">
    <location>
      <begin position="35"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="helix" evidence="14">
    <location>
      <begin position="40"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="strand" evidence="14">
    <location>
      <begin position="48"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="helix" evidence="14">
    <location>
      <begin position="53"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="helix" evidence="14">
    <location>
      <begin position="56"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="helix" evidence="14">
    <location>
      <begin position="60"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="turn" evidence="14">
    <location>
      <begin position="69"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="strand" evidence="14">
    <location>
      <begin position="72"/>
      <end position="80"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10986462"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11741540"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="336615"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="6295812"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="10574790"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="336615"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="6295812"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <evidence type="ECO:0000305" key="9">
    <source>
      <dbReference type="PubMed" id="10574790"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10">
    <source>
      <dbReference type="PubMed" id="3889348"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="11">
    <source>
      <dbReference type="PDB" id="1E44"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="12">
    <source>
      <dbReference type="PDB" id="1JCH"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="13">
    <source>
      <dbReference type="PDB" id="3EIP"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="14">
    <source>
      <dbReference type="PDB" id="3EIP"/>
    </source>
  </evidence>
  <sequence length="85" mass="9904" checksum="DF5794D36B67ED3D" modified="2007-01-23" version="3">MGLKLDLTWFDKSTEDFKGEEYSKDFGDDGSVMESLGVPFKDNVNNGCFDVIAEWVPLLQPYFNHQIDISDNEYFVSFDYRDGDW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>