<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-07-22" modified="2023-05-03" version="29" xmlns="http://uniprot.org/uniprot">
  <accession>A9JX08</accession>
  <name>PSMA4_STAAW</name>
  <protein>
    <recommendedName>
      <fullName>Phenol-soluble modulin alpha 4 peptide</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">psmA4</name>
    <name type="ordered locus">MW0406.1</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2007" name="Nat. Med." volume="13" first="1510" last="1514">
      <title>Identification of novel cytolytic peptides as key virulence determinants for community-associated MRSA.</title>
      <authorList>
        <person name="Wang R."/>
        <person name="Braughton K.R."/>
        <person name="Kretschmer D."/>
        <person name="Bach T.-H.L."/>
        <person name="Queck S.Y."/>
        <person name="Li M."/>
        <person name="Kennedy A.D."/>
        <person name="Dorward D.W."/>
        <person name="Klebanoff S.J."/>
        <person name="Peschel A."/>
        <person name="DeLeo F.R."/>
        <person name="Otto M."/>
      </authorList>
      <dbReference type="PubMed" id="17994102"/>
      <dbReference type="DOI" id="10.1038/nm1656"/>
    </citation>
    <scope>PARTIAL PROTEIN SEQUENCE</scope>
    <scope>FORMYLATION AT MET-1</scope>
    <scope>FUNCTION AS A VIRULENCE FACTOR</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>INDUCTION BY AGR</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Peptide which can recruit, activate and subsequently lyse human neutrophils, thus eliminating the main cellular defense against infection. Stimulates the secretion of the chemotactic factor interleukin-8 (IL-8). The ensuing activation process triggers an inflammatory response in the host, thus contributing greatly to virulence. Also possesses hemolytic activity, which may contribute to the development of disease.</text>
  </comment>
  <comment type="induction">
    <text evidence="1">Up-regulated by agr.</text>
  </comment>
  <comment type="miscellaneous">
    <text>Peptide production is higher in most prevalent community-associated MRSA strains than in hospital-associated MRSA strains.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the phenol-soluble modulin alpha peptides family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BK006301">
    <property type="protein sequence ID" value="DAA06125.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A9JX08"/>
  <dbReference type="SMR" id="A9JX08"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031429">
    <property type="entry name" value="PSM_alpha"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17063">
    <property type="entry name" value="PSMalpha"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0291">Formylation</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="peptide" id="PRO_0000345084" description="Phenol-soluble modulin alpha 4 peptide">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="modified residue" description="N-formylmethionine" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="17994102"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="20" mass="2172" checksum="6D59253C66E73D0E" modified="2008-02-05" version="1">MAIVGTIIKIIKAIIDIFAK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>