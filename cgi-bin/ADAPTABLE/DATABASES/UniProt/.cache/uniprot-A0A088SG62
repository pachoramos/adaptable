<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-05-08" modified="2024-03-27" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>A0A088SG62</accession>
  <name>B12A_TETBN</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">U-myrmicitoxin(01)-Tb2a</fullName>
      <shortName evidence="5">MYRTX(01)-Tb2</shortName>
      <shortName evidence="6">U-MYRTX(01)-Tb2a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="8">Ant peptide P17</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="4">U1-myrmicitoxin-Tb2a</fullName>
      <shortName evidence="4">U1-MYRTX-Tb2a</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Tetramorium bicarinatum</name>
    <name type="common">Tramp ant</name>
    <dbReference type="NCBI Taxonomy" id="219812"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Myrmicinae</taxon>
      <taxon>Tetramorium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2013" name="Toxicon" volume="70" first="70" last="81">
      <title>Profiling the venom gland transcriptome of Tetramorium bicarinatum (Hymenoptera: Formicidae): the first transcriptome analysis of an ant species.</title>
      <authorList>
        <person name="Bouzid W."/>
        <person name="Klopp C."/>
        <person name="Verdenaud M."/>
        <person name="Ducancel F."/>
        <person name="Vetillard A."/>
      </authorList>
      <dbReference type="PubMed" id="23584016"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2013.03.010"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="submission" date="2014-06" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning of total p17 cDNA using a modified RACE PCR strategy.</title>
      <authorList>
        <person name="Bonnafe E."/>
        <person name="Allaoua M."/>
        <person name="Rifflet A."/>
        <person name="Treilhou M."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2012" name="Peptides" volume="38" first="363" last="370">
      <title>Identification and characterization of a novel antimicrobial peptide from the venom of the ant Tetramorium bicarinatum.</title>
      <authorList>
        <person name="Rifflet A."/>
        <person name="Gavalda S."/>
        <person name="Tene N."/>
        <person name="Orivel J."/>
        <person name="Leprince J."/>
        <person name="Guilhaudis L."/>
        <person name="Genin E."/>
        <person name="Vetillard A."/>
        <person name="Treilhou M."/>
      </authorList>
      <dbReference type="PubMed" id="22960382"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2012.08.018"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 53-65</scope>
    <scope>SYNTHESIS OF 53-65</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-65</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2016" name="Toxins" volume="8" first="1" last="28">
      <title>The biochemical toxin arsenal from ant venoms.</title>
      <authorList>
        <person name="Touchard A."/>
        <person name="Aili S.R."/>
        <person name="Fox E.G."/>
        <person name="Escoubas P."/>
        <person name="Orivel J."/>
        <person name="Nicholson G.M."/>
        <person name="Dejean A."/>
      </authorList>
      <dbReference type="PubMed" id="26805882"/>
      <dbReference type="DOI" id="10.3390/toxins8010030"/>
    </citation>
    <scope>REVIEW</scope>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2018" name="Sci. Adv." volume="4" first="EAAU4640" last="EAAU4640">
      <title>A comprehensive portrait of the venom of the giant red bull ant, Myrmecia gulosa, reveals a hyperdiverse hymenopteran toxin gene family.</title>
      <authorList>
        <person name="Robinson S.D."/>
        <person name="Mueller A."/>
        <person name="Clayton D."/>
        <person name="Starobova H."/>
        <person name="Hamilton B.R."/>
        <person name="Payne R.J."/>
        <person name="Vetter I."/>
        <person name="King G.F."/>
        <person name="Undheim E.A.B."/>
      </authorList>
      <dbReference type="PubMed" id="30214940"/>
      <dbReference type="DOI" id="10.1126/sciadv.aau4640"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Unknown. Does not show antimicrobial activity when tested on S.aureus and S.xylosus (Ref.2). Does not show hemolytic activity (Ref.2).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="1571.49" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="6">Belongs to the formicidae venom precursor-01 superfamily.</text>
  </comment>
  <dbReference type="EMBL" id="JZ168552">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KM030176">
    <property type="protein sequence ID" value="AIO11144.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A088SG62"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR049518">
    <property type="entry name" value="Pilosulin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17499">
    <property type="entry name" value="Pilosulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000447050" evidence="7">
    <location>
      <begin position="18"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5001839399" description="U-myrmicitoxin(01)-Tb2a" evidence="6">
    <location>
      <begin position="53"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="2">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="22960382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="26805882"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="30214940"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="22960382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="AIO11144.1"/>
    </source>
  </evidence>
  <sequence length="68" mass="7103" checksum="DE1491BAB9E26129" modified="2014-11-26" version="1" precursor="true">MKLSFLSLALATIFVMAIIYAPQMEARASSDADADAAASADADADALAEASALFKEILEKIKAKLGKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>