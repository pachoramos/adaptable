<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-09-13" modified="2024-11-27" version="124" xmlns="http://uniprot.org/uniprot">
  <accession>P63083</accession>
  <accession>O88945</accession>
  <accession>P82540</accession>
  <name>S10A5_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Protein S100-A5</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Protein S-100D</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>S100 calcium-binding protein A5</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">S100a5</name>
    <name type="synonym">S100d</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="J. Biol. Chem." volume="275" first="30623" last="30630">
      <title>Brain S100A5 is a novel calcium-, zinc-, and copper ion-binding protein of the EF-hand superfamily.</title>
      <authorList>
        <person name="Schaefer B.W."/>
        <person name="Fritschy J.-M."/>
        <person name="Murmann P."/>
        <person name="Troxler H."/>
        <person name="Durussel I."/>
        <person name="Heizmann C.W."/>
        <person name="Cox J.A."/>
      </authorList>
      <dbReference type="PubMed" id="10882717"/>
      <dbReference type="DOI" id="10.1074/jbc.m002260200"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <strain>Sprague-Dawley</strain>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Binds calcium, zinc and copper. One subunit can simultaneously bind 2 calcium ions or 2 copper ions plus 1 zinc ion. Calcium and copper ions compete for the same binding sites.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Homodimer.</text>
  </comment>
  <comment type="mass spectrometry" mass="10878.0" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the S-100 family.</text>
  </comment>
  <dbReference type="RefSeq" id="NP_001099908.1">
    <property type="nucleotide sequence ID" value="NM_001106438.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_006232663.1">
    <property type="nucleotide sequence ID" value="XM_006232601.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_017446250.1">
    <property type="nucleotide sequence ID" value="XM_017590761.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_017446251.1">
    <property type="nucleotide sequence ID" value="XM_017590762.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_017446252.1">
    <property type="nucleotide sequence ID" value="XM_017590763.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P63083"/>
  <dbReference type="SMR" id="P63083"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000015712"/>
  <dbReference type="PhosphoSitePlus" id="P63083"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000015712"/>
  <dbReference type="ABCD" id="P63083">
    <property type="antibodies" value="1 sequenced antibody"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00000015712.7">
    <property type="protein sequence ID" value="ENSRNOP00000015712.4"/>
    <property type="gene ID" value="ENSRNOG00000011748.7"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055038712">
    <property type="protein sequence ID" value="ENSRNOP00055031431"/>
    <property type="gene ID" value="ENSRNOG00055022554"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060056249">
    <property type="protein sequence ID" value="ENSRNOP00060046455"/>
    <property type="gene ID" value="ENSRNOG00060032476"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065047287">
    <property type="protein sequence ID" value="ENSRNOP00065038773"/>
    <property type="gene ID" value="ENSRNOG00065027434"/>
  </dbReference>
  <dbReference type="GeneID" id="295211"/>
  <dbReference type="KEGG" id="rno:295211"/>
  <dbReference type="UCSC" id="RGD:1308996">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:1308996"/>
  <dbReference type="CTD" id="6276"/>
  <dbReference type="RGD" id="1308996">
    <property type="gene designation" value="S100a5"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S40V">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000161986"/>
  <dbReference type="HOGENOM" id="CLU_138624_2_0_1"/>
  <dbReference type="InParanoid" id="P63083"/>
  <dbReference type="OMA" id="HAVMETP"/>
  <dbReference type="OrthoDB" id="4575435at2759"/>
  <dbReference type="PhylomeDB" id="P63083"/>
  <dbReference type="TreeFam" id="TF332727"/>
  <dbReference type="PRO" id="PR:P63083"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000011748">
    <property type="expression patterns" value="Expressed in thymus and 15 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043025">
    <property type="term" value="C:neuronal cell body"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048306">
    <property type="term" value="F:calcium-dependent protein binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005507">
    <property type="term" value="F:copper ion binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042803">
    <property type="term" value="F:protein homodimerization activity"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008270">
    <property type="term" value="F:zinc ion binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00213">
    <property type="entry name" value="S-100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.238.10:FF:000044">
    <property type="entry name" value="Protein S100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.238.10">
    <property type="entry name" value="EF-hand"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011992">
    <property type="entry name" value="EF-hand-dom_pair"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018247">
    <property type="entry name" value="EF_Hand_1_Ca_BS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002048">
    <property type="entry name" value="EF_hand_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR034325">
    <property type="entry name" value="S-100_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001751">
    <property type="entry name" value="S100/CaBP7/8-like_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013787">
    <property type="entry name" value="S100_Ca-bd_sub"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11639:SF65">
    <property type="entry name" value="PROTEIN S100-A5"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11639">
    <property type="entry name" value="S100 CALCIUM-BINDING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01023">
    <property type="entry name" value="S_100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00054">
    <property type="entry name" value="EFh"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01394">
    <property type="entry name" value="S_100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47473">
    <property type="entry name" value="EF-hand"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00018">
    <property type="entry name" value="EF_HAND_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50222">
    <property type="entry name" value="EF_HAND_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00303">
    <property type="entry name" value="S100_CABP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-0186">Copper</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <feature type="chain" id="PRO_0000143982" description="Protein S100-A5">
    <location>
      <begin position="1"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 1" evidence="3">
    <location>
      <begin position="12"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 2" evidence="1">
    <location>
      <begin position="48"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="28"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>1</label>
      <note>low affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="33"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>1</label>
      <note>low affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="61"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>2</label>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="63"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>2</label>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="65"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>2</label>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="67"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>2</label>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="72"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <label>2</label>
      <note>high affinity</note>
    </ligand>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00448"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10882717"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="93" mass="10812" checksum="9A922E1898D202A4" modified="2004-09-13" version="1">METPLEKALTTMVTTFHKYSGREGSKLTLSRKELKELIKTELSLAEKMKESSIDNLMKSLDKNSDQEIDFKEYSVFLTTLCMAYNDFFLEDNK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>