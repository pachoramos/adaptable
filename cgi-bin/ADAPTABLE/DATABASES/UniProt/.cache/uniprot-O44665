<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-05-10" modified="2024-10-02" version="102" xmlns="http://uniprot.org/uniprot">
  <accession>O44665</accession>
  <name>NLP28_CAEEL</name>
  <protein>
    <recommendedName>
      <fullName>Neuropeptide-like protein 28</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>QWGYGGY-amide</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>GYGGYGGY-amide</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>GMYGGY-amide</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>GMYGGW-amide</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">nlp-28</name>
    <name type="ORF">B0213.3</name>
  </gene>
  <organism>
    <name type="scientific">Caenorhabditis elegans</name>
    <dbReference type="NCBI Taxonomy" id="6239"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Nematoda</taxon>
      <taxon>Chromadorea</taxon>
      <taxon>Rhabditida</taxon>
      <taxon>Rhabditina</taxon>
      <taxon>Rhabditomorpha</taxon>
      <taxon>Rhabditoidea</taxon>
      <taxon>Rhabditidae</taxon>
      <taxon>Peloderinae</taxon>
      <taxon>Caenorhabditis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Science" volume="282" first="2012" last="2018">
      <title>Genome sequence of the nematode C. elegans: a platform for investigating biology.</title>
      <authorList>
        <consortium name="The C. elegans sequencing consortium"/>
      </authorList>
      <dbReference type="PubMed" id="9851916"/>
      <dbReference type="DOI" id="10.1126/science.282.5396.2012"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Bristol N2</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2001" name="Proc. Natl. Acad. Sci. U.S.A." volume="98" first="14000" last="14005">
      <title>Identification of neuropeptide-like protein gene families in Caenorhabditis elegans and other species.</title>
      <authorList>
        <person name="Nathoo A.N."/>
        <person name="Moeller R.A."/>
        <person name="Westlund B.A."/>
        <person name="Hart A.C."/>
      </authorList>
      <dbReference type="PubMed" id="11717458"/>
      <dbReference type="DOI" id="10.1073/pnas.241231298"/>
    </citation>
    <scope>IDENTIFICATION</scope>
  </reference>
  <comment type="function">
    <text>May have antimicrobial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the YARP (YGGW-amide related peptide) family.</text>
  </comment>
  <dbReference type="EMBL" id="FO080121">
    <property type="protein sequence ID" value="CCD61353.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="D89016">
    <property type="entry name" value="D89016"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_504110.1">
    <property type="nucleotide sequence ID" value="NM_071709.4"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O44665"/>
  <dbReference type="BioGRID" id="46713">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="6239.B0213.3.1"/>
  <dbReference type="PaxDb" id="6239-B0213.3"/>
  <dbReference type="EnsemblMetazoa" id="B0213.3.1">
    <property type="protein sequence ID" value="B0213.3.1"/>
    <property type="gene ID" value="WBGene00003766"/>
  </dbReference>
  <dbReference type="GeneID" id="181845"/>
  <dbReference type="KEGG" id="cel:CELE_B0213.3"/>
  <dbReference type="UCSC" id="B0213.3">
    <property type="organism name" value="c. elegans"/>
  </dbReference>
  <dbReference type="AGR" id="WB:WBGene00003766"/>
  <dbReference type="CTD" id="181845"/>
  <dbReference type="WormBase" id="B0213.3">
    <property type="protein sequence ID" value="CE16774"/>
    <property type="gene ID" value="WBGene00003766"/>
    <property type="gene designation" value="nlp-28"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_193227_0_0_1"/>
  <dbReference type="InParanoid" id="O44665"/>
  <dbReference type="OMA" id="MATSAQW"/>
  <dbReference type="PRO" id="PR:O44665"/>
  <dbReference type="Proteomes" id="UP000001940">
    <property type="component" value="Chromosome V"/>
  </dbReference>
  <dbReference type="Bgee" id="WBGene00003766">
    <property type="expression patterns" value="Expressed in adult organism and 3 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000041488" description="QWGYGGY-amide" evidence="1">
    <location>
      <begin position="23"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000041489" description="GYGGYGGY-amide" evidence="1">
    <location>
      <begin position="32"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000041490" description="GMYGGY-amide" evidence="1">
    <location>
      <begin position="42"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000041491" description="GMYGGY-amide" evidence="1">
    <location>
      <begin position="50"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000041492" description="GMYGGW-amide" evidence="1">
    <location>
      <begin position="58"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="1">
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="1">
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="1">
    <location>
      <position position="47"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="1">
    <location>
      <position position="55"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tryptophan amide" evidence="1">
    <location>
      <position position="63"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="65" mass="6867" checksum="B149BDCA7C4CADDC" modified="1998-06-01" version="1" precursor="true">MISTSSILILVFLLACFMATSAQWGYGGYGRGYGGYGGYGRGMYGGYGRGMYGGYGRGMYGGWGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>