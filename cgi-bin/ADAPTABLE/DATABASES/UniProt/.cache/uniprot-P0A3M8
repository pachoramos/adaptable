<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-03-15" modified="2023-05-03" version="25" xmlns="http://uniprot.org/uniprot">
  <accession>P0A3M8</accession>
  <accession>Q00561</accession>
  <name>LCIA_LACLC</name>
  <protein>
    <recommendedName>
      <fullName>Lactococcin-A immunity protein</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">lciA</name>
  </gene>
  <organism>
    <name type="scientific">Lactococcus lactis subsp. cremoris</name>
    <name type="common">Streptococcus cremoris</name>
    <dbReference type="NCBI Taxonomy" id="1359"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Lactococcus</taxon>
    </lineage>
  </organism>
  <geneLocation type="plasmid">
    <name>p9B4-6</name>
  </geneLocation>
  <reference key="1">
    <citation type="journal article" date="1991" name="Appl. Environ. Microbiol." volume="57" first="492" last="498">
      <title>Organization and nucleotide sequences of two lactococcal bacteriocin operons.</title>
      <authorList>
        <person name="van Belkum M.J."/>
        <person name="Hayema B.J."/>
        <person name="Jeeninga R.E."/>
        <person name="Kok J."/>
        <person name="Venema G."/>
      </authorList>
      <dbReference type="PubMed" id="1901707"/>
      <dbReference type="DOI" id="10.1128/aem.57.2.492-498.1991"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>9B4</strain>
      <plasmid>p9B4-6</plasmid>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1991" name="J. Bacteriol." volume="173" first="3879" last="3887">
      <title>Lactococcin A, a new bacteriocin from Lactococcus lactis subsp. cremoris: isolation and characterization of the protein and its gene.</title>
      <authorList>
        <person name="Holo H."/>
        <person name="Nilssen O."/>
        <person name="Nes I.F."/>
      </authorList>
      <dbReference type="PubMed" id="1904860"/>
      <dbReference type="DOI" id="10.1128/jb.173.12.3879-3887.1991"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>LMG 2130</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1992" name="Appl. Environ. Microbiol." volume="58" first="572" last="577">
      <title>Cloning, sequencing, and expression in Escherichia coli of lcnB, a third bacteriocin determinant from the lactococcal bacteriocin plasmid p9B4-6.</title>
      <authorList>
        <person name="van Belkum M.J."/>
        <person name="Kok J."/>
        <person name="Venema G."/>
      </authorList>
      <dbReference type="PubMed" id="1610182"/>
      <dbReference type="DOI" id="10.1128/aem.58.2.572-577.1992"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] OF 75-98</scope>
    <source>
      <strain>9B4</strain>
      <plasmid>p9B4-6</plasmid>
    </source>
  </reference>
  <comment type="function">
    <text>Imparts immunity to lactococcin-A to naturally sensitive host strains.</text>
  </comment>
  <dbReference type="EMBL" id="S38128">
    <property type="protein sequence ID" value="AAB22371.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M63675">
    <property type="protein sequence ID" value="AAA25164.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="B39443">
    <property type="entry name" value="B39443"/>
  </dbReference>
  <dbReference type="PIR" id="E43943">
    <property type="entry name" value="E43943"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_015081787.1">
    <property type="nucleotide sequence ID" value="NZ_WJUW01000098.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0A3M8"/>
  <dbReference type="SMR" id="P0A3M8"/>
  <dbReference type="GO" id="GO:0030153">
    <property type="term" value="P:bacteriocin immunity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd21059">
    <property type="entry name" value="LciA-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015046">
    <property type="entry name" value="LciA_Immunity-like"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08951">
    <property type="entry name" value="EntA_Immun"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="predicted"/>
  <keyword id="KW-0079">Bacteriocin immunity</keyword>
  <keyword id="KW-0614">Plasmid</keyword>
  <feature type="chain" id="PRO_0000206198" description="Lactococcin-A immunity protein">
    <location>
      <begin position="1"/>
      <end position="98"/>
    </location>
  </feature>
  <sequence length="98" mass="11164" checksum="587FE4DB9ED136E7" modified="2005-03-15" version="1">MKKKQIEFENELRSMLATALEKDISQEERNALNIAEKALDNSEYLPKIILNLRKALTPLAINRTLNHDLSELYKFITSSKASNKNLGGGLIMSWGRLF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>