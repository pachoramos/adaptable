<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-02-06" modified="2023-05-03" version="19" xmlns="http://uniprot.org/uniprot">
  <accession>B3EWS6</accession>
  <accession>A0A4Y5UH66</accession>
  <name>TXC12_CUPSA</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Toxin CSTX-12</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="5">CSTX-12 A chain</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="5">CSTX-12 B chain</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Cupiennius salei</name>
    <name type="common">American wandering spider</name>
    <dbReference type="NCBI Taxonomy" id="6928"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Lycosoidea</taxon>
      <taxon>Ctenidae</taxon>
      <taxon>Cupiennius</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2019" name="Toxins" volume="11">
      <title>The dual prey-inactivation strategy of spiders-in-depth venomic analysis of Cupiennius salei.</title>
      <authorList>
        <person name="Kuhn-Nentwig L."/>
        <person name="Langenegger N."/>
        <person name="Heller M."/>
        <person name="Koua D."/>
        <person name="Nentwig W."/>
      </authorList>
      <dbReference type="PubMed" id="30893800"/>
      <dbReference type="DOI" id="10.3390/toxins11030167"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="FEBS J." volume="279" first="2683" last="2694">
      <title>Multicomponent venom of the spider Cupiennius salei: a bioanalytical investigation applying different strategies.</title>
      <authorList>
        <person name="Trachsel C."/>
        <person name="Siegemund D."/>
        <person name="Kampfer U."/>
        <person name="Kopp L.S."/>
        <person name="Buhr C."/>
        <person name="Grossmann J."/>
        <person name="Luthi C."/>
        <person name="Cunningham M."/>
        <person name="Nentwig W."/>
        <person name="Kuhn-Nentwig L."/>
        <person name="Schurch S."/>
        <person name="Schaller J."/>
      </authorList>
      <dbReference type="PubMed" id="22672445"/>
      <dbReference type="DOI" id="10.1111/j.1742-4658.2012.08650.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 48-81 AND 88-116</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT ALA-116</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Synergistic toxin that induces or increases a cytolytic effect when combined with CSTX-1 (AC P81694) or CSTX-9 (AC P58604). When alone, has a weak insecticidal activity, with an unknown molecular target.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Heterodimer of A and B chains; disulfide-linked (By similarity). Interacts with CSTX-1 (AC P81694), and with CSTX-9 (AC P58604) (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Target cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="4369.887" method="Electrospray" evidence="4">
    <molecule>CSTX-12 A chain</molecule>
  </comment>
  <comment type="mass spectrometry" mass="3409.862" method="Electrospray" evidence="4">
    <molecule>CSTX-12 B chain</molecule>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the neurotoxin 19 (CSTX) family. 12 subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="MH754557">
    <property type="protein sequence ID" value="QDC23092.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="MH754558">
    <property type="protein sequence ID" value="QDC23093.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B3EWS6"/>
  <dbReference type="SMR" id="B3EWS6"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019553">
    <property type="entry name" value="Spider_toxin_CSTX_knottin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011142">
    <property type="entry name" value="Spider_toxin_CSTX_Knottin_CS"/>
  </dbReference>
  <dbReference type="Pfam" id="PF10530">
    <property type="entry name" value="Toxin_35"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60029">
    <property type="entry name" value="SPIDER_CSTX"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000452337" evidence="6">
    <location>
      <begin position="21"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000421181" description="CSTX-12 A chain" evidence="4">
    <location>
      <begin position="48"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000452338" evidence="6">
    <location>
      <begin position="82"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000421182" description="CSTX-12 B chain" evidence="4">
    <location>
      <begin position="88"/>
      <end position="116"/>
    </location>
  </feature>
  <feature type="modified residue" description="Alanine amide" evidence="4">
    <location>
      <position position="116"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="50"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="57"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (between A and B chains)" evidence="2">
    <location>
      <begin position="64"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (between A and B chains)" evidence="2">
    <location>
      <begin position="76"/>
      <end position="93"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P58604"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P83919"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="22672445"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="22672445"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="22672445"/>
    </source>
  </evidence>
  <sequence length="117" mass="13391" checksum="57DA913CC4443776" modified="2021-04-07" version="2" precursor="true">MKVLVICAVLFLTIFSNSSAETEDDFLEDESFEADDVIPFLAREQVRSDCTLRNHDCTDDRHSCCRSKMFKDVCKCFYPSQRSDTARAKKELCTCQQDKHLKFIEKGLQKAKVLVAG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>