<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-05-02" modified="2024-11-27" version="85" xmlns="http://uniprot.org/uniprot">
  <accession>O94746</accession>
  <accession>J9VFP5</accession>
  <name>FKBP_CRYNH</name>
  <protein>
    <recommendedName>
      <fullName>FK506-binding protein 1</fullName>
      <shortName>FKBP</shortName>
      <ecNumber>5.2.1.8</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Peptidyl-prolyl cis-trans isomerase</fullName>
      <shortName>PPIase</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Rapamycin-binding protein</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">FRR1</name>
    <name type="ORF">CNAG_03682</name>
  </gene>
  <organism>
    <name type="scientific">Cryptococcus neoformans var. grubii serotype A (strain H99 / ATCC 208821 / CBS 10515 / FGSC 9487)</name>
    <name type="common">Filobasidiella neoformans var. grubii</name>
    <dbReference type="NCBI Taxonomy" id="235443"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Basidiomycota</taxon>
      <taxon>Agaricomycotina</taxon>
      <taxon>Tremellomycetes</taxon>
      <taxon>Tremellales</taxon>
      <taxon>Cryptococcaceae</taxon>
      <taxon>Cryptococcus</taxon>
      <taxon>Cryptococcus neoformans species complex</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Mol. Cell. Biol." volume="19" first="4101" last="4112">
      <title>Rapamycin antifungal action is mediated via conserved complexes with FKBP12 and TOR kinase homologs in Cryptococcus neoformans.</title>
      <authorList>
        <person name="Cruz M.C."/>
        <person name="Cavallo L.M."/>
        <person name="Goerlach J.M."/>
        <person name="Cox G."/>
        <person name="Perfect J.R."/>
        <person name="Cardenas M.E."/>
        <person name="Heitman J."/>
      </authorList>
      <dbReference type="PubMed" id="10330150"/>
      <dbReference type="DOI" id="10.1128/mcb.19.6.4101"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <source>
      <strain>H99 / ATCC 208821 / CBS 10515 / FGSC 9487</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2014" name="PLoS Genet." volume="10" first="E1004261" last="E1004261">
      <title>Analysis of the genome and transcriptome of Cryptococcus neoformans var. grubii reveals complex RNA expression and microevolution leading to virulence attenuation.</title>
      <authorList>
        <person name="Janbon G."/>
        <person name="Ormerod K.L."/>
        <person name="Paulet D."/>
        <person name="Byrnes E.J. III"/>
        <person name="Yadav V."/>
        <person name="Chatterjee G."/>
        <person name="Mullapudi N."/>
        <person name="Hon C.-C."/>
        <person name="Billmyre R.B."/>
        <person name="Brunel F."/>
        <person name="Bahn Y.-S."/>
        <person name="Chen W."/>
        <person name="Chen Y."/>
        <person name="Chow E.W.L."/>
        <person name="Coppee J.-Y."/>
        <person name="Floyd-Averette A."/>
        <person name="Gaillardin C."/>
        <person name="Gerik K.J."/>
        <person name="Goldberg J."/>
        <person name="Gonzalez-Hilarion S."/>
        <person name="Gujja S."/>
        <person name="Hamlin J.L."/>
        <person name="Hsueh Y.-P."/>
        <person name="Ianiri G."/>
        <person name="Jones S."/>
        <person name="Kodira C.D."/>
        <person name="Kozubowski L."/>
        <person name="Lam W."/>
        <person name="Marra M."/>
        <person name="Mesner L.D."/>
        <person name="Mieczkowski P.A."/>
        <person name="Moyrand F."/>
        <person name="Nielsen K."/>
        <person name="Proux C."/>
        <person name="Rossignol T."/>
        <person name="Schein J.E."/>
        <person name="Sun S."/>
        <person name="Wollschlaeger C."/>
        <person name="Wood I.A."/>
        <person name="Zeng Q."/>
        <person name="Neuveglise C."/>
        <person name="Newlon C.S."/>
        <person name="Perfect J.R."/>
        <person name="Lodge J.K."/>
        <person name="Idnurm A."/>
        <person name="Stajich J.E."/>
        <person name="Kronstad J.W."/>
        <person name="Sanyal K."/>
        <person name="Heitman J."/>
        <person name="Fraser J.A."/>
        <person name="Cuomo C.A."/>
        <person name="Dietrich F.S."/>
      </authorList>
      <dbReference type="PubMed" id="24743168"/>
      <dbReference type="DOI" id="10.1371/journal.pgen.1004261"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>H99 / ATCC 208821 / CBS 10515 / FGSC 9487</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">PPIases accelerate the folding of proteins. It catalyzes the cis-trans isomerization of proline imidic peptide bonds in oligopeptides (By similarity).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>[protein]-peptidylproline (omega=180) = [protein]-peptidylproline (omega=0)</text>
      <dbReference type="Rhea" id="RHEA:16237"/>
      <dbReference type="Rhea" id="RHEA-COMP:10747"/>
      <dbReference type="Rhea" id="RHEA-COMP:10748"/>
      <dbReference type="ChEBI" id="CHEBI:83833"/>
      <dbReference type="ChEBI" id="CHEBI:83834"/>
      <dbReference type="EC" id="5.2.1.8"/>
    </reaction>
  </comment>
  <comment type="activity regulation">
    <text evidence="1">Inhibited by both FK506 and rapamycin.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the FKBP-type PPIase family. FKBP1 subfamily.</text>
  </comment>
  <comment type="sequence caution" evidence="3">
    <conflict type="erroneous initiation">
      <sequence resource="EMBL-CDS" id="AFR93187" version="1"/>
    </conflict>
    <text>Extended N-terminus.</text>
  </comment>
  <dbReference type="EC" id="5.2.1.8"/>
  <dbReference type="EMBL" id="AF097889">
    <property type="protein sequence ID" value="AAD16172.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF097888">
    <property type="protein sequence ID" value="AAD16171.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP003821">
    <property type="protein sequence ID" value="AFR93187.1"/>
    <property type="status" value="ALT_INIT"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_012047386.1">
    <property type="nucleotide sequence ID" value="XM_012191996.1"/>
  </dbReference>
  <dbReference type="PDB" id="6TZ8">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="3.30 A"/>
    <property type="chains" value="C/F=2-108"/>
  </dbReference>
  <dbReference type="PDBsum" id="6TZ8"/>
  <dbReference type="AlphaFoldDB" id="O94746"/>
  <dbReference type="SMR" id="O94746"/>
  <dbReference type="SwissPalm" id="O94746"/>
  <dbReference type="GeneID" id="23887158"/>
  <dbReference type="KEGG" id="cng:CNAG_03682"/>
  <dbReference type="HOGENOM" id="CLU_013615_12_1_1"/>
  <dbReference type="OrthoDB" id="25281at2759"/>
  <dbReference type="Proteomes" id="UP000010091">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003755">
    <property type="term" value="F:peptidyl-prolyl cis-trans isomerase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000413">
    <property type="term" value="P:protein peptidyl-prolyl isomerization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.50.40:FF:000025">
    <property type="entry name" value="Peptidylprolyl isomerase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050689">
    <property type="entry name" value="FKBP-type_PPIase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046357">
    <property type="entry name" value="PPIase_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001179">
    <property type="entry name" value="PPIase_FKBP_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10516">
    <property type="entry name" value="PEPTIDYL-PROLYL CIS-TRANS ISOMERASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10516:SF464">
    <property type="entry name" value="PEPTIDYL-PROLYL CIS-TRANS ISOMERASE FKBP12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00254">
    <property type="entry name" value="FKBP_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54534">
    <property type="entry name" value="FKBP-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50059">
    <property type="entry name" value="FKBP_PPIASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0413">Isomerase</keyword>
  <keyword id="KW-0697">Rotamase</keyword>
  <feature type="chain" id="PRO_0000233323" description="FK506-binding protein 1">
    <location>
      <begin position="1"/>
      <end position="108"/>
    </location>
  </feature>
  <feature type="domain" description="PPIase FKBP-type" evidence="2">
    <location>
      <begin position="20"/>
      <end position="108"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="3"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="22"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="28"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="36"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="40"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="47"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="51"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="58"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="64"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="72"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="79"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="82"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="89"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="98"/>
      <end position="107"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00277"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="6TZ8"/>
    </source>
  </evidence>
  <sequence length="108" mass="11608" checksum="40BC60788B0BF26D" modified="1999-05-01" version="1">MGVTVENISAGDGKTFPQPGDNVTIHYVGTLLDGSKFDSSRDRGTPFVCRIGQGQVIRGWDEGVPQLSVGQKANLICTPDYAYGARGFPPVIPPNSTLKFEVELLKVN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>