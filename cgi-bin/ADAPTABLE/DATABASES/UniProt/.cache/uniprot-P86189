<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-05-05" modified="2022-05-25" version="16" xmlns="http://uniprot.org/uniprot">
  <accession>P86189</accession>
  <name>RNSP9_BOARA</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Raniseptin-9</fullName>
      <shortName evidence="5">Rsp-9</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Boana raniceps</name>
    <name type="common">Chaco tree frog</name>
    <name type="synonym">Hyla roeschmanni</name>
    <dbReference type="NCBI Taxonomy" id="192750"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Hylinae</taxon>
      <taxon>Cophomantini</taxon>
      <taxon>Boana</taxon>
    </lineage>
  </organism>
  <reference evidence="6" key="1">
    <citation type="journal article" date="2008" name="Biochem. Biophys. Res. Commun." volume="377" first="1057" last="1061">
      <title>Post-secretory events alter the peptide content of the skin secretion of Hypsiboas raniceps.</title>
      <authorList>
        <person name="Magalhaes B.S."/>
        <person name="Melo J.A.T."/>
        <person name="Leite J.R.S.A."/>
        <person name="Silva L.P."/>
        <person name="Prates M.V."/>
        <person name="Vinecky F."/>
        <person name="Barbosa E.A."/>
        <person name="Verly R.M."/>
        <person name="Mehta A."/>
        <person name="Nicoli J.R."/>
        <person name="Bemquerer M.P."/>
        <person name="Andrade A.C."/>
        <person name="Bloch C. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="18976634"/>
      <dbReference type="DOI" id="10.1016/j.bbrc.2008.10.102"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue evidence="4">Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P86189"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016322">
    <property type="entry name" value="FSAP"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001822">
    <property type="entry name" value="Dermaseptin_precursor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000371453" evidence="1">
    <location>
      <begin position="23"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000371454" description="Raniseptin-9" evidence="1">
    <location>
      <begin position="52"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="27"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Acidic residues" evidence="3">
    <location>
      <begin position="29"/>
      <end position="43"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P86037"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="18976634"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="18976634"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="79" mass="9074" checksum="6A69538FDD0E81C1" modified="2009-05-05" version="1" precursor="true">MAFLKKSLFLVLFLGIVSLSICEEEKREGEEEEKQEEENEELSEEELRERRALLDKLKSLGKVVGKVAIGVAQHYLNPQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>