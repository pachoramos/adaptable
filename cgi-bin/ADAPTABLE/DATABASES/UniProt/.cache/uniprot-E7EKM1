<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-05-10" modified="2022-05-25" version="24" xmlns="http://uniprot.org/uniprot">
  <accession>E7EKM1</accession>
  <name>TP4_SYLSP</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Temporin-SN4</fullName>
    </recommendedName>
  </protein>
  <organism evidence="4">
    <name type="scientific">Sylvirana spinulosa</name>
    <name type="common">Fine-spined frog</name>
    <name type="synonym">Hylarana spinulosa</name>
    <dbReference type="NCBI Taxonomy" id="369515"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Sylvirana</taxon>
    </lineage>
  </organism>
  <reference evidence="7" key="1">
    <citation type="journal article" date="2013" name="Biochimie" volume="95" first="2429" last="2436">
      <title>Identification of multiple antimicrobial peptides from the skin of fine-spined frog, Hylarana spinulosa (Ranidae).</title>
      <authorList>
        <person name="Yang X."/>
        <person name="Hu Y."/>
        <person name="Xu S."/>
        <person name="Hu Y."/>
        <person name="Meng H."/>
        <person name="Guo C."/>
        <person name="Liu Y."/>
        <person name="Liu J."/>
        <person name="Yu Z."/>
        <person name="Wang H."/>
      </authorList>
      <dbReference type="PubMed" id="24055160"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2013.09.002"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue evidence="4">Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Antimicrobial peptide. Active against some Gram-positive and Gram-negative bacterial strains. Active against fungus C.glabrata 090902 but not against C.albicans ATCC 12231. Shows weak hemolytic activity against human erythrocytes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the frog skin active peptide (FSAP) family. Temporin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="HQ735198">
    <property type="protein sequence ID" value="ADV36221.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="E7EKM1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044179">
    <property type="term" value="P:hemolysis in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439792" description="Removed in mature form" evidence="6">
    <location>
      <begin position="23"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000439793" description="Temporin-SN4" evidence="4">
    <location>
      <begin position="47"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="modified residue" description="Lysine amide" evidence="1">
    <location>
      <position position="61"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="E7EKJ7"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="24055160"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="24055160"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="24055160"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="EMBL" id="ADV36221.1"/>
    </source>
  </evidence>
  <sequence length="61" mass="7007" checksum="75039B533165E9C6" modified="2011-03-08" version="1" precursor="true">MFTLKKTLLLLFFLGTINLSLCEEERNAEEERRDGDDEMDVEVKKRFITGLISGLMKALGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>