<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-06-13" modified="2024-03-27" version="68" xmlns="http://uniprot.org/uniprot">
  <accession>A6GZS1</accession>
  <name>CAS2_FLAPJ</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">CRISPR-associated endoribonuclease Cas2</fullName>
      <ecNumber evidence="1">3.1.-.-</ecNumber>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">cas2</name>
    <name type="ordered locus">FP1526</name>
  </gene>
  <organism>
    <name type="scientific">Flavobacterium psychrophilum (strain ATCC 49511 / DSM 21280 / CIP 103535 / JIP02/86)</name>
    <dbReference type="NCBI Taxonomy" id="402612"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacteroidota</taxon>
      <taxon>Flavobacteriia</taxon>
      <taxon>Flavobacteriales</taxon>
      <taxon>Flavobacteriaceae</taxon>
      <taxon>Flavobacterium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2007" name="Nat. Biotechnol." volume="25" first="763" last="769">
      <title>Complete genome sequence of the fish pathogen Flavobacterium psychrophilum.</title>
      <authorList>
        <person name="Duchaud E."/>
        <person name="Boussaha M."/>
        <person name="Loux V."/>
        <person name="Bernardet J.-F."/>
        <person name="Michel C."/>
        <person name="Kerouault B."/>
        <person name="Mondot S."/>
        <person name="Nicolas P."/>
        <person name="Bossy R."/>
        <person name="Caron C."/>
        <person name="Bessieres P."/>
        <person name="Gibrat J.-F."/>
        <person name="Claverol S."/>
        <person name="Dumetz F."/>
        <person name="Le Henaff M."/>
        <person name="Benmansour A."/>
      </authorList>
      <dbReference type="PubMed" id="17592475"/>
      <dbReference type="DOI" id="10.1038/nbt1313"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 49511 / DSM 21280 / CIP 103535 / JIP02/86</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">CRISPR (clustered regularly interspaced short palindromic repeat), is an adaptive immune system that provides protection against mobile genetic elements (viruses, transposable elements and conjugative plasmids). CRISPR clusters contain sequences complementary to antecedent mobile elements and target invading nucleic acids. CRISPR clusters are transcribed and processed into CRISPR RNA (crRNA). Functions as a ssRNA-specific endoribonuclease. Involved in the integration of spacer DNA into the CRISPR cassette.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="1">
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </cofactor>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer, forms a heterotetramer with a Cas1 homodimer.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the CRISPR-associated endoribonuclease Cas2 protein family.</text>
  </comment>
  <dbReference type="EC" id="3.1.-.-" evidence="1"/>
  <dbReference type="EMBL" id="AM398681">
    <property type="protein sequence ID" value="CAL43594.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_011963639.1">
    <property type="nucleotide sequence ID" value="NC_009613.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="YP_001296403.1">
    <property type="nucleotide sequence ID" value="NC_009613.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A6GZS1"/>
  <dbReference type="SMR" id="A6GZS1"/>
  <dbReference type="STRING" id="402612.FP1526"/>
  <dbReference type="EnsemblBacteria" id="CAL43594">
    <property type="protein sequence ID" value="CAL43594"/>
    <property type="gene ID" value="FP1526"/>
  </dbReference>
  <dbReference type="GeneID" id="66552288"/>
  <dbReference type="KEGG" id="fps:FP1526"/>
  <dbReference type="PATRIC" id="fig|402612.5.peg.1535"/>
  <dbReference type="eggNOG" id="COG3512">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_150500_0_0_10"/>
  <dbReference type="OrthoDB" id="9791737at2"/>
  <dbReference type="Proteomes" id="UP000006394">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004521">
    <property type="term" value="F:RNA endonuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051607">
    <property type="term" value="P:defense response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043571">
    <property type="term" value="P:maintenance of CRISPR repeat elements"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01471">
    <property type="entry name" value="Cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR021127">
    <property type="entry name" value="CRISPR_associated_Cas2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019199">
    <property type="entry name" value="Virulence_VapD/CRISPR_Cas2"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01573">
    <property type="entry name" value="cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF09827">
    <property type="entry name" value="CRISPR_Cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF143430">
    <property type="entry name" value="TTP0101/SSO1404-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0051">Antiviral defense</keyword>
  <keyword id="KW-0255">Endonuclease</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0460">Magnesium</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000417712" description="CRISPR-associated endoribonuclease Cas2">
    <location>
      <begin position="1"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="22"/>
    </location>
    <ligand>
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
      <note>catalytic</note>
    </ligand>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01471"/>
    </source>
  </evidence>
  <sequence length="115" mass="13866" checksum="76AEA0D08695FBF9" modified="2007-07-24" version="1">MYDEHYTRLNQYRSLWILVFFDLPTETRKERKIASEFRKKLLDDGFSMFQFSIYIRFCASRENAEVHTKRIRNSLPEHGKIGVMQITDKQFGMMELFYGKKPVETDKPSQQLELF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>