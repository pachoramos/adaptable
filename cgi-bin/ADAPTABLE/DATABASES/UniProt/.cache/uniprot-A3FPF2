<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-06-16" modified="2024-03-27" version="37" xmlns="http://uniprot.org/uniprot">
  <accession>A3FPF2</accession>
  <name>DEF_NELNU</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Gamma-thionin homolog</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Nelumbo nucifera</name>
    <name type="common">Sacred lotus</name>
    <dbReference type="NCBI Taxonomy" id="4432"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>Proteales</taxon>
      <taxon>Nelumbonaceae</taxon>
      <taxon>Nelumbo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2007-02" db="EMBL/GenBank/DDBJ databases">
      <title>Molecular cloning and expression of defensin genes from sacred lotus embryonic axis.</title>
      <authorList>
        <person name="Qi L."/>
        <person name="Fu T.H."/>
        <person name="Long J."/>
        <person name="Ma Z.Q."/>
        <person name="Cai A.M."/>
        <person name="Huang S.Z."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="EMBL" id="EF421192">
    <property type="protein sequence ID" value="ABN46979.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_010270777.1">
    <property type="nucleotide sequence ID" value="XM_010272475.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A3FPF2"/>
  <dbReference type="SMR" id="A3FPF2"/>
  <dbReference type="GeneID" id="104606994"/>
  <dbReference type="KEGG" id="nnu:104606994"/>
  <dbReference type="eggNOG" id="ENOG502S7HM">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="A3FPF2"/>
  <dbReference type="OrthoDB" id="385272at2759"/>
  <dbReference type="Proteomes" id="UP000189703">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008176">
    <property type="entry name" value="Defensin_plant"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33147">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33147:SF39">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 2-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00304">
    <property type="entry name" value="Gamma-thionin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00288">
    <property type="entry name" value="PUROTHIONIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00940">
    <property type="entry name" value="GAMMA_THIONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000378145" description="Defensin-like protein">
    <location>
      <begin position="31"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="33"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="50"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="54"/>
      <end position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="77" mass="8361" checksum="74B45014666D1E55" modified="2007-03-20" version="1" precursor="true">MERGMRLFSSLVLVLLLVTATEMGPKVAEARTCESQSHRFKGACLSDTNCASVCQTEGFPAGDCKGARRRCFCVKPC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>