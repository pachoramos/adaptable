<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-09-05" modified="2022-05-25" version="16" xmlns="http://uniprot.org/uniprot">
  <accession>P84702</accession>
  <name>ASTAE_ASTSM</name>
  <protein>
    <recommendedName>
      <fullName>Asteropin-A</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Asteropine-A</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Asteropus simplex</name>
    <name type="common">Marine sponge</name>
    <name type="synonym">Stellettinopsis simplex</name>
    <dbReference type="NCBI Taxonomy" id="350939"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Porifera</taxon>
      <taxon>Demospongiae</taxon>
      <taxon>Heteroscleromorpha</taxon>
      <taxon>Tetractinellida</taxon>
      <taxon>Astrophorina</taxon>
      <taxon>Ancorinidae</taxon>
      <taxon>Asteropus</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2006" name="Chem. Biol." volume="13" first="569" last="574">
      <title>Asteropine A, a sialidase-inhibiting conotoxin-like peptide from the marine sponge Asteropus simplex.</title>
      <authorList>
        <person name="Takada K."/>
        <person name="Hamada T."/>
        <person name="Hirota H."/>
        <person name="Nakao Y."/>
        <person name="Matsunaga S."/>
        <person name="van Soest R.W.M."/>
        <person name="Fusetani N."/>
      </authorList>
      <dbReference type="PubMed" id="16793514"/>
      <dbReference type="DOI" id="10.1016/j.chembiol.2006.05.010"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>STRUCTURE BY NMR</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Sialidase inhibitor. Competitively inhibits bacterial sialidases, but not viral sialidases. Does not inhibit glycosidases or proteases. Has no antitumor activity.</text>
  </comment>
  <comment type="domain">
    <text>The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="3817.5" method="MALDI" evidence="1"/>
  <dbReference type="AlphaFoldDB" id="P84702"/>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <feature type="peptide" id="PRO_0000249178" description="Asteropin-A">
    <location>
      <begin position="1"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="2"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="9"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="17"/>
      <end position="35"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="16793514"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="36" mass="3826" checksum="76ADFF7291812EEC" modified="2006-09-05" version="1">YCGLFGDLCTLDGTLACCIALELECIPLNDFVGICL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>