<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-07-19" modified="2024-11-27" version="137" xmlns="http://uniprot.org/uniprot">
  <accession>P83859</accession>
  <accession>A3KFJ6</accession>
  <accession>Q495K6</accession>
  <name>OX26_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Orexigenic neuropeptide QRFP</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>P518</fullName>
    </alternativeName>
    <component>
      <recommendedName>
        <fullName>QRF-amide</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Neuropeptide RF-amide</fullName>
      </alternativeName>
      <alternativeName>
        <fullName>Pyroglutamylated arginine-phenylalanine-amide peptide</fullName>
      </alternativeName>
    </component>
  </protein>
  <gene>
    <name evidence="9" type="primary">QRFP</name>
  </gene>
  <organism evidence="8">
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference evidence="7" key="1">
    <citation type="journal article" date="2003" name="J. Biol. Chem." volume="278" first="27652" last="27657">
      <title>Identification and characterization of a novel RF-amide peptide ligand for orphan G-protein-coupled receptor SP9155.</title>
      <authorList>
        <person name="Jiang Y."/>
        <person name="Luo L."/>
        <person name="Gustafson E.L."/>
        <person name="Yadav D."/>
        <person name="Laverty M."/>
        <person name="Murgolo N."/>
        <person name="Vassileva G."/>
        <person name="Zeng M."/>
        <person name="Laz T.M."/>
        <person name="Behan J."/>
        <person name="Qiu P."/>
        <person name="Wang L."/>
        <person name="Wang S."/>
        <person name="Bayne M."/>
        <person name="Greene J."/>
        <person name="Monsma F.J. Jr."/>
        <person name="Zhang F.L."/>
      </authorList>
      <dbReference type="PubMed" id="12714592"/>
      <dbReference type="DOI" id="10.1074/jbc.m302945200"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference evidence="7" key="2">
    <citation type="journal article" date="2003" name="J. Biol. Chem." volume="278" first="46387" last="46395">
      <title>A new peptidic ligand and its receptor regulating adrenal function in rats.</title>
      <authorList>
        <person name="Fukusumi S."/>
        <person name="Yoshida H."/>
        <person name="Fujii R."/>
        <person name="Maruyama M."/>
        <person name="Komatsu H."/>
        <person name="Habata Y."/>
        <person name="Shintani Y."/>
        <person name="Hinuma S."/>
        <person name="Fujino M."/>
      </authorList>
      <dbReference type="PubMed" id="12960173"/>
      <dbReference type="DOI" id="10.1074/jbc.m305270200"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue evidence="4">Brain</tissue>
    </source>
  </reference>
  <reference evidence="7" key="3">
    <citation type="journal article" date="2003" name="Proc. Natl. Acad. Sci. U.S.A." volume="100" first="15247" last="15252">
      <title>Identification of 26RFa, a hypothalamic neuropeptide of the RFamide peptide family with orexigenic activity.</title>
      <authorList>
        <person name="Chartrel N."/>
        <person name="Dujardin C."/>
        <person name="Anouar Y."/>
        <person name="Leprince J."/>
        <person name="Decker A."/>
        <person name="Clerens S."/>
        <person name="Do-Rego J.-C."/>
        <person name="Vandesande F."/>
        <person name="Llorens-Cortes C."/>
        <person name="Costentin J."/>
        <person name="Beauvillain J.-C."/>
        <person name="Vaudry H."/>
      </authorList>
      <dbReference type="PubMed" id="14657341"/>
      <dbReference type="DOI" id="10.1073/pnas.2434676100"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue evidence="8">Hypothalamus</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2003" name="Nature" volume="425" first="805" last="811">
      <title>The DNA sequence and analysis of human chromosome 6.</title>
      <authorList>
        <person name="Mungall A.J."/>
        <person name="Palmer S.A."/>
        <person name="Sims S.K."/>
        <person name="Edwards C.A."/>
        <person name="Ashurst J.L."/>
        <person name="Wilming L."/>
        <person name="Jones M.C."/>
        <person name="Horton R."/>
        <person name="Hunt S.E."/>
        <person name="Scott C.E."/>
        <person name="Gilbert J.G.R."/>
        <person name="Clamp M.E."/>
        <person name="Bethel G."/>
        <person name="Milne S."/>
        <person name="Ainscough R."/>
        <person name="Almeida J.P."/>
        <person name="Ambrose K.D."/>
        <person name="Andrews T.D."/>
        <person name="Ashwell R.I.S."/>
        <person name="Babbage A.K."/>
        <person name="Bagguley C.L."/>
        <person name="Bailey J."/>
        <person name="Banerjee R."/>
        <person name="Barker D.J."/>
        <person name="Barlow K.F."/>
        <person name="Bates K."/>
        <person name="Beare D.M."/>
        <person name="Beasley H."/>
        <person name="Beasley O."/>
        <person name="Bird C.P."/>
        <person name="Blakey S.E."/>
        <person name="Bray-Allen S."/>
        <person name="Brook J."/>
        <person name="Brown A.J."/>
        <person name="Brown J.Y."/>
        <person name="Burford D.C."/>
        <person name="Burrill W."/>
        <person name="Burton J."/>
        <person name="Carder C."/>
        <person name="Carter N.P."/>
        <person name="Chapman J.C."/>
        <person name="Clark S.Y."/>
        <person name="Clark G."/>
        <person name="Clee C.M."/>
        <person name="Clegg S."/>
        <person name="Cobley V."/>
        <person name="Collier R.E."/>
        <person name="Collins J.E."/>
        <person name="Colman L.K."/>
        <person name="Corby N.R."/>
        <person name="Coville G.J."/>
        <person name="Culley K.M."/>
        <person name="Dhami P."/>
        <person name="Davies J."/>
        <person name="Dunn M."/>
        <person name="Earthrowl M.E."/>
        <person name="Ellington A.E."/>
        <person name="Evans K.A."/>
        <person name="Faulkner L."/>
        <person name="Francis M.D."/>
        <person name="Frankish A."/>
        <person name="Frankland J."/>
        <person name="French L."/>
        <person name="Garner P."/>
        <person name="Garnett J."/>
        <person name="Ghori M.J."/>
        <person name="Gilby L.M."/>
        <person name="Gillson C.J."/>
        <person name="Glithero R.J."/>
        <person name="Grafham D.V."/>
        <person name="Grant M."/>
        <person name="Gribble S."/>
        <person name="Griffiths C."/>
        <person name="Griffiths M.N.D."/>
        <person name="Hall R."/>
        <person name="Halls K.S."/>
        <person name="Hammond S."/>
        <person name="Harley J.L."/>
        <person name="Hart E.A."/>
        <person name="Heath P.D."/>
        <person name="Heathcott R."/>
        <person name="Holmes S.J."/>
        <person name="Howden P.J."/>
        <person name="Howe K.L."/>
        <person name="Howell G.R."/>
        <person name="Huckle E."/>
        <person name="Humphray S.J."/>
        <person name="Humphries M.D."/>
        <person name="Hunt A.R."/>
        <person name="Johnson C.M."/>
        <person name="Joy A.A."/>
        <person name="Kay M."/>
        <person name="Keenan S.J."/>
        <person name="Kimberley A.M."/>
        <person name="King A."/>
        <person name="Laird G.K."/>
        <person name="Langford C."/>
        <person name="Lawlor S."/>
        <person name="Leongamornlert D.A."/>
        <person name="Leversha M."/>
        <person name="Lloyd C.R."/>
        <person name="Lloyd D.M."/>
        <person name="Loveland J.E."/>
        <person name="Lovell J."/>
        <person name="Martin S."/>
        <person name="Mashreghi-Mohammadi M."/>
        <person name="Maslen G.L."/>
        <person name="Matthews L."/>
        <person name="McCann O.T."/>
        <person name="McLaren S.J."/>
        <person name="McLay K."/>
        <person name="McMurray A."/>
        <person name="Moore M.J.F."/>
        <person name="Mullikin J.C."/>
        <person name="Niblett D."/>
        <person name="Nickerson T."/>
        <person name="Novik K.L."/>
        <person name="Oliver K."/>
        <person name="Overton-Larty E.K."/>
        <person name="Parker A."/>
        <person name="Patel R."/>
        <person name="Pearce A.V."/>
        <person name="Peck A.I."/>
        <person name="Phillimore B.J.C.T."/>
        <person name="Phillips S."/>
        <person name="Plumb R.W."/>
        <person name="Porter K.M."/>
        <person name="Ramsey Y."/>
        <person name="Ranby S.A."/>
        <person name="Rice C.M."/>
        <person name="Ross M.T."/>
        <person name="Searle S.M."/>
        <person name="Sehra H.K."/>
        <person name="Sheridan E."/>
        <person name="Skuce C.D."/>
        <person name="Smith S."/>
        <person name="Smith M."/>
        <person name="Spraggon L."/>
        <person name="Squares S.L."/>
        <person name="Steward C.A."/>
        <person name="Sycamore N."/>
        <person name="Tamlyn-Hall G."/>
        <person name="Tester J."/>
        <person name="Theaker A.J."/>
        <person name="Thomas D.W."/>
        <person name="Thorpe A."/>
        <person name="Tracey A."/>
        <person name="Tromans A."/>
        <person name="Tubby B."/>
        <person name="Wall M."/>
        <person name="Wallis J.M."/>
        <person name="West A.P."/>
        <person name="White S.S."/>
        <person name="Whitehead S.L."/>
        <person name="Whittaker H."/>
        <person name="Wild A."/>
        <person name="Willey D.J."/>
        <person name="Wilmer T.E."/>
        <person name="Wood J.M."/>
        <person name="Wray P.W."/>
        <person name="Wyatt J.C."/>
        <person name="Young L."/>
        <person name="Younger R.M."/>
        <person name="Bentley D.R."/>
        <person name="Coulson A."/>
        <person name="Durbin R.M."/>
        <person name="Hubbard T."/>
        <person name="Sulston J.E."/>
        <person name="Dunham I."/>
        <person name="Rogers J."/>
        <person name="Beck S."/>
      </authorList>
      <dbReference type="PubMed" id="14574404"/>
      <dbReference type="DOI" id="10.1038/nature02055"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1 5 6">Stimulates feeding behavior, metabolic rate and locomotor activity and increases blood pressure. May have orexigenic activity. May promote aldosterone secretion by the adrenal gland (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Ligand for the G-protein coupled receptor QRFPR/GPR103.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed widely in the brain with highest expression levels in the cerebellum, medulla, pituitary, retina, vestibular nucleus, and white matter. Also expressed in the bladder, colon, coronary artery, parathyroid gland, prostate, testis, and thyroid.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the RFamide neuropeptide family.</text>
  </comment>
  <dbReference type="EMBL" id="AB109625">
    <property type="protein sequence ID" value="BAC98934.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY438326">
    <property type="protein sequence ID" value="AAR24354.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL161733">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC101127">
    <property type="protein sequence ID" value="AAI01128.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS6936.1"/>
  <dbReference type="RefSeq" id="NP_937823.1">
    <property type="nucleotide sequence ID" value="NM_198180.2"/>
  </dbReference>
  <dbReference type="PDB" id="8ZH8">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.19 A"/>
    <property type="chains" value="Q=108-133"/>
  </dbReference>
  <dbReference type="PDBsum" id="8ZH8"/>
  <dbReference type="AlphaFoldDB" id="P83859"/>
  <dbReference type="EMDB" id="EMD-60096"/>
  <dbReference type="SMR" id="P83859"/>
  <dbReference type="STRING" id="9606.ENSP00000485512"/>
  <dbReference type="BioMuta" id="QRFP"/>
  <dbReference type="DMDM" id="50400831"/>
  <dbReference type="PaxDb" id="9606-ENSP00000485512"/>
  <dbReference type="PeptideAtlas" id="P83859"/>
  <dbReference type="Antibodypedia" id="48446">
    <property type="antibodies" value="22 antibodies from 6 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="347148"/>
  <dbReference type="Ensembl" id="ENST00000343079.1">
    <property type="protein sequence ID" value="ENSP00000345487.1"/>
    <property type="gene ID" value="ENSG00000188710.3"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000623824.2">
    <property type="protein sequence ID" value="ENSP00000485512.1"/>
    <property type="gene ID" value="ENSG00000188710.3"/>
  </dbReference>
  <dbReference type="GeneID" id="347148"/>
  <dbReference type="KEGG" id="hsa:347148"/>
  <dbReference type="MANE-Select" id="ENST00000623824.2">
    <property type="protein sequence ID" value="ENSP00000485512.1"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_198180.3"/>
    <property type="RefSeq protein sequence ID" value="NP_937823.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc011mcb.2">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:29982"/>
  <dbReference type="CTD" id="347148"/>
  <dbReference type="DisGeNET" id="347148"/>
  <dbReference type="GeneCards" id="QRFP"/>
  <dbReference type="HGNC" id="HGNC:29982">
    <property type="gene designation" value="QRFP"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000188710">
    <property type="expression patterns" value="Low tissue specificity"/>
  </dbReference>
  <dbReference type="MIM" id="609795">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P83859"/>
  <dbReference type="OpenTargets" id="ENSG00000188710"/>
  <dbReference type="PharmGKB" id="PA162400554"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000188710"/>
  <dbReference type="eggNOG" id="ENOG502S84J">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000015756"/>
  <dbReference type="HOGENOM" id="CLU_155319_0_0_1"/>
  <dbReference type="InParanoid" id="P83859"/>
  <dbReference type="OMA" id="PLGTCFP"/>
  <dbReference type="OrthoDB" id="5315787at2759"/>
  <dbReference type="PhylomeDB" id="P83859"/>
  <dbReference type="TreeFam" id="TF336317"/>
  <dbReference type="PathwayCommons" id="P83859"/>
  <dbReference type="Reactome" id="R-HSA-389397">
    <property type="pathway name" value="Orexin and neuropeptides FF and QRFP bind to their respective receptors"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-416476">
    <property type="pathway name" value="G alpha (q) signalling events"/>
  </dbReference>
  <dbReference type="BioGRID-ORCS" id="347148">
    <property type="hits" value="21 hits in 1135 CRISPR screens"/>
  </dbReference>
  <dbReference type="GeneWiki" id="QRFP"/>
  <dbReference type="GenomeRNAi" id="347148"/>
  <dbReference type="Pharos" id="P83859">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P83859"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 9"/>
  </dbReference>
  <dbReference type="RNAct" id="P83859">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000188710">
    <property type="expression patterns" value="Expressed in male germ line stem cell (sensu Vertebrata) in testis and 88 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031854">
    <property type="term" value="F:orexigenic neuropeptide QRFP receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007625">
    <property type="term" value="P:grooming behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007626">
    <property type="term" value="P:locomotory behavior"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045777">
    <property type="term" value="P:positive regulation of blood pressure"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060259">
    <property type="term" value="P:regulation of feeding behavior"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR024565">
    <property type="entry name" value="P518"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR36476">
    <property type="entry name" value="OREXIGENIC NEUROPEPTIDE QRFP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR36476:SF1">
    <property type="entry name" value="OREXIGENIC NEUROPEPTIDE QRFP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11109">
    <property type="entry name" value="RFamide_26RFa"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000010086" evidence="3 7">
    <location>
      <begin position="19"/>
      <end position="90"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000010087" description="QRF-amide" evidence="7">
    <location>
      <begin position="91"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="2 7">
    <location>
      <position position="91"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="133"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_049184" description="In dbSNP:rs12340616.">
    <original>L</original>
    <variation>H</variation>
    <location>
      <position position="68"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P83860"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="12714592"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="12960173"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="14657341"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="AAR24354.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="9">
    <source>
      <dbReference type="EMBL" id="BAC98934.1"/>
    </source>
  </evidence>
  <sequence length="136" mass="14941" checksum="4FC036F7ADF4551F" modified="2004-05-10" version="1" precursor="true">MVRPYPLIYFLFLPLGACFPLLDRREPTDAMGGLGAGERWADLAMGPRPHSVWGSSRWLRASQPQALLVIARGLQTSGREHAGCRFRFGRQDEGSEATGFLPAAGEKTSGPLGNLAEELNGYSRKKGGFSFRFGRR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>