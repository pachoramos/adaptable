<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2024-11-27" version="81" xmlns="http://uniprot.org/uniprot">
  <accession>P56928</accession>
  <name>ENA2_HORSE</name>
  <protein>
    <recommendedName>
      <fullName>Antimicrobial peptide eNAP-2</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Equus caballus</name>
    <name type="common">Horse</name>
    <dbReference type="NCBI Taxonomy" id="9796"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Perissodactyla</taxon>
      <taxon>Equidae</taxon>
      <taxon>Equus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="Infect. Immun." volume="60" first="5042" last="5047">
      <title>eNAP-2, a novel cysteine-rich bactericidal peptide from equine leukocytes.</title>
      <authorList>
        <person name="Couto M.A."/>
        <person name="Harwig S.S.L."/>
        <person name="Cullor J.S."/>
        <person name="Hughes J.P."/>
        <person name="Lehrer R.I."/>
      </authorList>
      <dbReference type="PubMed" id="1452336"/>
      <dbReference type="DOI" id="10.1128/iai.60.12.5042-5047.1992"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>ANTIBACTERIAL ACTIVITY</scope>
    <source>
      <tissue>Neutrophil</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="Infect. Immun." volume="61" first="2991" last="2994">
      <title>Selective inhibition of microbial serine proteases by eNAP-2, an antimicrobial peptide from equine neutrophils.</title>
      <authorList>
        <person name="Couto M.A."/>
        <person name="Harwig S.S.L."/>
        <person name="Lehrer R.I."/>
      </authorList>
      <dbReference type="PubMed" id="8514405"/>
      <dbReference type="DOI" id="10.1128/iai.61.7.2991-2994.1993"/>
    </citation>
    <scope>ANTIPROTEASE ACTIVITY</scope>
  </reference>
  <comment type="function">
    <text>Has antibiotic activity against several equine uterine pathogens; S.zooepidemicus, E.coli and P.aeruginosa. Highly efficient against S.zoopedemicus. Not active against K.pneumoniae. Selectively inactivates microbial serine proteases (subtilisin A and proteinase K) without inhibiting mammalian serine proteases (human neutrophil elastase, human cathepsin G and bovine pancreatic trypsin).</text>
  </comment>
  <dbReference type="PIR" id="A49212">
    <property type="entry name" value="A49212"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P56928"/>
  <dbReference type="SMR" id="P56928"/>
  <dbReference type="InParanoid" id="P56928"/>
  <dbReference type="Proteomes" id="UP000002281">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036645">
    <property type="entry name" value="Elafin-like_sf"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57256">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000188987" description="Antimicrobial peptide eNAP-2">
    <location>
      <begin position="1"/>
      <end position="46" status="greater than"/>
    </location>
  </feature>
  <feature type="domain" description="WAP" evidence="1">
    <location>
      <begin position="12"/>
      <end position="46" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="46"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00722"/>
    </source>
  </evidence>
  <sequence length="46" mass="4769" checksum="8C30936DA38A7AEC" modified="2000-05-30" version="1" fragment="single">EVERKHPLGGSRPGRCPTVPPGTFGHCACLCTGDASEPKGQKCCSN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>