<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-01-01" modified="2024-07-24" version="106" xmlns="http://uniprot.org/uniprot">
  <accession>P06884</accession>
  <name>PAHO_FELCA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Pancreatic polypeptide prohormone</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="1">Pancreatic polypeptide</fullName>
        <shortName evidence="1">PP</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="1">Pancreatic icosapeptide</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">PPY</name>
  </gene>
  <organism>
    <name type="scientific">Felis catus</name>
    <name type="common">Cat</name>
    <name type="synonym">Felis silvestris catus</name>
    <dbReference type="NCBI Taxonomy" id="9685"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Carnivora</taxon>
      <taxon>Feliformia</taxon>
      <taxon>Felidae</taxon>
      <taxon>Felinae</taxon>
      <taxon>Felis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1986" name="Biochem. J." volume="240" first="69" last="74">
      <title>Cat pancreatic eicosapeptide and its biosynthetic intermediate. Conservation of a monobasic processing site.</title>
      <authorList>
        <person name="Nielsen H.V."/>
        <person name="Gether U."/>
        <person name="Schwartz T.W."/>
      </authorList>
      <dbReference type="PubMed" id="3827854"/>
      <dbReference type="DOI" id="10.1042/bj2400069"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT TYR-36</scope>
  </reference>
  <comment type="function">
    <molecule>Pancreatic polypeptide</molecule>
    <text evidence="1">Hormone secreted by pancreatic cells that acts as a regulator of pancreatic and gastrointestinal functions probably by signaling through the G protein-coupled receptor NPY4R2.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text>Gly-Lys-Arg at positions 37 to 39 were included by homology with other pancreatic hormone type precursor sequence.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the NPY family.</text>
  </comment>
  <dbReference type="PIR" id="A26073">
    <property type="entry name" value="PCCT"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P06884"/>
  <dbReference type="BMRB" id="P06884"/>
  <dbReference type="SMR" id="P06884"/>
  <dbReference type="STRING" id="9685.ENSFCAP00000032528"/>
  <dbReference type="PaxDb" id="9685-ENSFCAP00000020367"/>
  <dbReference type="eggNOG" id="ENOG502TD4B">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_165150_1_0_1"/>
  <dbReference type="InParanoid" id="P06884"/>
  <dbReference type="Proteomes" id="UP000011712">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00126">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.900">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001955">
    <property type="entry name" value="Pancreatic_hormone-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020392">
    <property type="entry name" value="Pancreatic_hormone-like_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533">
    <property type="entry name" value="NEUROPEPTIDE Y/PANCREATIC HORMONE/PEPTIDE YY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533:SF2">
    <property type="entry name" value="PANCREATIC PROHORMONE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00159">
    <property type="entry name" value="Hormone_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00278">
    <property type="entry name" value="PANCHORMONE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00309">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00265">
    <property type="entry name" value="PANCREATIC_HORMONE_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50276">
    <property type="entry name" value="PANCREATIC_HORMONE_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000025363" description="Pancreatic polypeptide">
    <location>
      <begin position="1"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000025364" description="Pancreatic icosapeptide">
    <location>
      <begin position="40"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000045868">
    <location>
      <begin position="60"/>
      <end position="66" status="greater than"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="2">
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="66"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01298"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="3827854"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="66" mass="7483" checksum="2D3A94BD9063A83D" modified="1988-01-01" version="1" precursor="true" fragment="single">APLEPVYPGDNATPEQMAQYAAELRRYINMLTRPRYGKRDRGETLDILEWGSPHAAAPRELSPMDV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>