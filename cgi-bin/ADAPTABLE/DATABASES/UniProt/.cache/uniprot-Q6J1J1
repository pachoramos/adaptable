<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-03-07" modified="2024-11-27" version="137" xmlns="http://uniprot.org/uniprot">
  <accession>Q6J1J1</accession>
  <accession>Q32LI0</accession>
  <name>BIRC5_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>Baculoviral IAP repeat-containing protein 5</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Apoptosis inhibitor survivin</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">BIRC5</name>
  </gene>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2004-04" db="EMBL/GenBank/DDBJ databases">
      <title>mRNA expression in sheep and cattle blastocysts.</title>
      <authorList>
        <person name="Gutierrez-Adan A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="2005-11" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="NIH - Mammalian Gene Collection (MGC) project"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Crossbred X Angus</strain>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Multitasking protein that has dual roles in promoting cell proliferation and preventing apoptosis (By similarity). Component of a chromosome passage protein complex (CPC) which is essential for chromosome alignment and segregation during mitosis and cytokinesis (By similarity). Acts as an important regulator of the localization of this complex; directs CPC movement to different locations from the inner centromere during prometaphase to midbody during cytokinesis and participates in the organization of the center spindle by associating with polymerized microtubules (By similarity). Involved in the recruitment of CPC to centromeres during early mitosis via association with histone H3 phosphorylated at 'Thr-3' (H3pT3) during mitosis (By similarity). The complex with RAN plays a role in mitotic spindle formation by serving as a physical scaffold to help deliver the RAN effector molecule TPX2 to microtubules (By similarity). May counteract a default induction of apoptosis in G2/M phase (By similarity). The acetylated form represses STAT3 transactivation of target gene promoters (By similarity). May play a role in neoplasia. Inhibitor of CASP3 and CASP7 (By similarity). Essential for the maintenance of mitochondrial integrity and function (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Monomer or homodimer. Exists as a homodimer in the apo state and as a monomer in the CPC-bound state. The monomer protects cells against apoptosis more efficiently than the dimer. Only the dimeric form is capable of enhancing tubulin stability in cells. When phosphorylated, interacts with LAMTOR5/HBXIP; the resulting complex binds pro-CASP9, as well as active CASP9, but much less efficiently. Component of the chromosomal passenger complex (CPC) composed of at least BIRC5/survivin, CDCA8/borealin, INCENP, AURKB or AURKC; in the complex forms a triple-helix bundle-based subcomplex with INCENP and CDCA8. Interacts with JTB. Interacts (via BIR domain) with histone H3 phosphorylated at 'Thr-3' (H3pT3). Interacts with EVI5. Interacts with GTP-bound RAN in both the S and M phases of the cell cycle. Interacts with USP9X. Interacts with tubulin. Interacts with BIRC2/c-IAP1. The acetylated form at Lys-129 interacts with STAT3. The monomeric form deacetylated at Lys-129 interacts with XPO1/CRM1. The monomeric form interacts with XIAP/BIRC4. Both the dimeric and monomeric form can interact with DIABLO/SMAC. Interacts with BIRC6/bruce. Interacts with FBXL7; this interaction facilitates the polyubiquitination and subsequent proteasomal degradation of BIRC5 by the SCF(FBXL7) E3 ubiquitin-protein ligase complex (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Chromosome</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Chromosome</location>
      <location evidence="2">Centromere</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Cytoplasm</location>
      <location evidence="2">Cytoskeleton</location>
      <location evidence="2">Spindle</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Chromosome</location>
      <location evidence="2">Centromere</location>
      <location evidence="2">Kinetochore</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Midbody</location>
    </subcellularLocation>
    <text evidence="1 2">Localizes at the centromeres from prophase to metaphase, at the spindle midzone during anaphase and a the midbody during telophase and cytokinesis. Accumulates in the nucleus upon treatment with leptomycin B (LMB), a XPO1/CRM1 nuclear export inhibitor (By similarity). Localizes on chromosome arms and inner centromeres from prophase through metaphase. Localizes to kinetochores in metaphase, distributes to the midzone microtubules in anaphase and at telophase, localizes exclusively to the midbody. Colocalizes with AURKB at mitotic chromosomes. Acetylation at Lys-129 directs its localization to the nucleus by enhancing homodimerization and thereby inhibiting XPO1/CRM1-mediated nuclear export (By similarity).</text>
  </comment>
  <comment type="domain">
    <text evidence="2">The BIR repeat is necessary and sufficient for LAMTOR5 binding.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">Ubiquitinated by the Cul9-RING ubiquitin-protein ligase complex, leading to its degradation. Ubiquitination is required for centrosomal targeting. Deubiquitinated by USP35 or USP38; leading to stabilization.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">Acetylation at Lys-129 results in its homodimerization, while deacetylation promotes the formation of monomers which heterodimerize with XPO1/CRM1 which facilitates its nuclear export. The acetylated form represses STAT3 transactivation. The dynamic equilibrium between its acetylation and deacetylation at Lys-129 determines its interaction with XPO1/CRM1, its subsequent subcellular localization, and its ability to inhibit STAT3 transactivation.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">In vitro phosphorylation at Thr-117 by AURKB prevents interaction with INCENP and localization to mitotic chromosomes. Phosphorylation at Thr-48 by CK2 is critical for its mitotic and anti-apoptotic activities. Phosphorylation at Thr-34 by CDK15 is critical for its anti-apoptotic activity. Phosphorylation at Ser-20 by AURKC is critical for regulation of proper chromosome alignment and segregation, and possibly cytokinesis.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the IAP family.</text>
  </comment>
  <dbReference type="EMBL" id="AY606044">
    <property type="protein sequence ID" value="AAT37504.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC109566">
    <property type="protein sequence ID" value="AAI09567.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001001855.2">
    <property type="nucleotide sequence ID" value="NM_001001855.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q6J1J1"/>
  <dbReference type="SMR" id="Q6J1J1"/>
  <dbReference type="STRING" id="9913.ENSBTAP00000018046"/>
  <dbReference type="MEROPS" id="I32.005"/>
  <dbReference type="PaxDb" id="9913-ENSBTAP00000018046"/>
  <dbReference type="Ensembl" id="ENSBTAT00000018046.4">
    <property type="protein sequence ID" value="ENSBTAP00000018046.2"/>
    <property type="gene ID" value="ENSBTAG00000013573.4"/>
  </dbReference>
  <dbReference type="GeneID" id="414925"/>
  <dbReference type="KEGG" id="bta:414925"/>
  <dbReference type="CTD" id="332"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSBTAG00000013573"/>
  <dbReference type="VGNC" id="VGNC:26501">
    <property type="gene designation" value="BIRC5"/>
  </dbReference>
  <dbReference type="eggNOG" id="KOG1101">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00510000047537"/>
  <dbReference type="HOGENOM" id="CLU_016347_0_1_1"/>
  <dbReference type="InParanoid" id="Q6J1J1"/>
  <dbReference type="OMA" id="IKMYFYE"/>
  <dbReference type="OrthoDB" id="2882335at2759"/>
  <dbReference type="TreeFam" id="TF342652"/>
  <dbReference type="Reactome" id="R-BTA-141444">
    <property type="pathway name" value="Amplification of signal from unattached kinetochores via a MAD2 inhibitory signal"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-2467813">
    <property type="pathway name" value="Separation of Sister Chromatids"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-2500257">
    <property type="pathway name" value="Resolution of Sister Chromatid Cohesion"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-4615885">
    <property type="pathway name" value="SUMOylation of DNA replication proteins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-5663220">
    <property type="pathway name" value="RHO GTPases Activate Formins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-68877">
    <property type="pathway name" value="Mitotic Prometaphase"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-8951664">
    <property type="pathway name" value="Neddylation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-9648025">
    <property type="pathway name" value="EML4 and NUDC in mitotic spindle formation"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Chromosome 19"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSBTAG00000013573">
    <property type="expression patterns" value="Expressed in spermatid and 105 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005814">
    <property type="term" value="C:centriole"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032133">
    <property type="term" value="C:chromosome passenger complex"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000775">
    <property type="term" value="C:chromosome, centromeric region"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005881">
    <property type="term" value="C:cytoplasmic microtubule"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031021">
    <property type="term" value="C:interphase microtubule organizing center"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000776">
    <property type="term" value="C:kinetochore"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030496">
    <property type="term" value="C:midbody"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005876">
    <property type="term" value="C:spindle microtubule"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043027">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity involved in apoptotic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008017">
    <property type="term" value="F:microtubule binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042803">
    <property type="term" value="F:protein homodimerization activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015631">
    <property type="term" value="F:tubulin binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008270">
    <property type="term" value="F:zinc ion binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051301">
    <property type="term" value="P:cell division"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007059">
    <property type="term" value="P:chromosome segregation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051303">
    <property type="term" value="P:establishment of chromosome localization"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000086">
    <property type="term" value="P:G2/M transition of mitotic cell cycle"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000281">
    <property type="term" value="P:mitotic cytokinesis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007094">
    <property type="term" value="P:mitotic spindle assembly checkpoint signaling"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043066">
    <property type="term" value="P:negative regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043154">
    <property type="term" value="P:negative regulation of cysteine-type endopeptidase activity involved in apoptotic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045892">
    <property type="term" value="P:negative regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031536">
    <property type="term" value="P:positive regulation of exit from mitosis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045931">
    <property type="term" value="P:positive regulation of mitotic cell cycle"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006468">
    <property type="term" value="P:protein phosphorylation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031503">
    <property type="term" value="P:protein-containing complex localization"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00022">
    <property type="entry name" value="BIR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.1170.10:FF:000009">
    <property type="entry name" value="Baculoviral IAP repeat-containing protein 5"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR051190">
    <property type="entry name" value="Baculoviral_IAP"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001370">
    <property type="entry name" value="BIR_rpt"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46771:SF3">
    <property type="entry name" value="BACULOVIRAL IAP REPEAT-CONTAINING PROTEIN 5"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46771">
    <property type="entry name" value="DETERIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00653">
    <property type="entry name" value="BIR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00238">
    <property type="entry name" value="BIR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57924">
    <property type="entry name" value="Inhibitor of apoptosis (IAP) repeat"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50143">
    <property type="entry name" value="BIR_REPEAT_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-0131">Cell cycle</keyword>
  <keyword id="KW-0132">Cell division</keyword>
  <keyword id="KW-0137">Centromere</keyword>
  <keyword id="KW-0158">Chromosome</keyword>
  <keyword id="KW-0159">Chromosome partition</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0206">Cytoskeleton</keyword>
  <keyword id="KW-0995">Kinetochore</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0493">Microtubule</keyword>
  <keyword id="KW-0498">Mitosis</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0678">Repressor</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0832">Ubl conjugation</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <feature type="chain" id="PRO_0000226729" description="Baculoviral IAP repeat-containing protein 5">
    <location>
      <begin position="1"/>
      <end position="142"/>
    </location>
  </feature>
  <feature type="repeat" description="BIR">
    <location>
      <begin position="18"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="57"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="60"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="77"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="84"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="site" description="Interaction with FBXL7" evidence="2">
    <location>
      <position position="126"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by AURKC" evidence="2">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="2">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphothreonine; by CDK1 and CDK15" evidence="2">
    <location>
      <position position="34"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphothreonine" evidence="2">
    <location>
      <position position="48"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="2">
    <location>
      <position position="90"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="2">
    <location>
      <position position="110"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="2">
    <location>
      <position position="112"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="2">
    <location>
      <position position="115"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphothreonine; by AURKB" evidence="2">
    <location>
      <position position="117"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="2">
    <location>
      <position position="129"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAT37504." evidence="4" ref="1">
    <original>GAQ</original>
    <variation>SAP</variation>
    <location>
      <begin position="2"/>
      <end position="4"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAT37504." evidence="4" ref="1">
    <original>V</original>
    <variation>I</variation>
    <location>
      <position position="19"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAT37504." evidence="4" ref="1">
    <original>A</original>
    <variation>E</variation>
    <location>
      <position position="40"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAT37504." evidence="4" ref="1">
    <original>F</original>
    <variation>L</variation>
    <location>
      <position position="59"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAT37504." evidence="4" ref="1">
    <original>T</original>
    <variation>A</variation>
    <location>
      <position position="109"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAT37504." evidence="4" ref="1">
    <original>AALE</original>
    <variation>VAMD</variation>
    <location>
      <begin position="139"/>
      <end position="142"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="E3SCZ8"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="O15392"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00029"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="142" mass="16341" checksum="577CA9AC3FDB9E68" modified="2006-09-19" version="2">MGAQSLPPAWQLYLKDHRVSTFKNWPFLEGCACTPERMAAAGFIHCPTENEPDLAQCFFCFKELEGWEPDDDPIEEHKKHSSGCAFLSVKKQFEELTLSEFLKLDKERTKNKIAKETNNKQKEFEETAKKVRCAIEQLAALE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>