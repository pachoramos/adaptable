<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-02-01" modified="2023-05-03" version="64" xmlns="http://uniprot.org/uniprot">
  <accession>P40845</accession>
  <name>ES2A_PELLE</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Esculentin-2A</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pelophylax lessonae</name>
    <name type="common">Pool frog</name>
    <name type="synonym">Rana lessonae</name>
    <dbReference type="NCBI Taxonomy" id="45623"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Pelophylax</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="J. Biol. Chem." volume="269" first="11956" last="11961">
      <title>Antimicrobial peptides from skin secretions of Rana esculenta. Molecular cloning of cDNAs encoding esculentin and brevinins and isolation of new active peptides.</title>
      <authorList>
        <person name="Simmaco M."/>
        <person name="Mignogna G."/>
        <person name="Barra D."/>
        <person name="Bossa F."/>
      </authorList>
      <dbReference type="PubMed" id="8163497"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)32666-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Shows antibacterial activity against representative Gram-negative and Gram-positive bacterial species, and hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the frog skin active peptide (FSAP) family. Esculentin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=00083"/>
  </comment>
  <dbReference type="PIR" id="E53578">
    <property type="entry name" value="E53578"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P40845"/>
  <dbReference type="SMR" id="P40845"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044648" description="Esculentin-2A" evidence="1">
    <location>
      <begin position="1"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="37"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="8163497"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="8163497"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="8163497"/>
    </source>
  </evidence>
  <sequence length="37" mass="3714" checksum="2E0A389EB7348AC4" modified="1995-02-01" version="1">GILSLVKGVAKLAGKGLAKEGGKFGLELIACKIAKQC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>