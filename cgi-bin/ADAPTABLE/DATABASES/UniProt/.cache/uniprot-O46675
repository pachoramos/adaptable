<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-12-15" modified="2024-11-27" version="129" xmlns="http://uniprot.org/uniprot">
  <accession>O46675</accession>
  <name>GROG_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>Growth-regulated protein homolog gamma</fullName>
      <shortName>GRO-gamma</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Mol. Biol. Evol." volume="16" first="180" last="193">
      <title>Isolation of novel GRO genes and a phylogenetic analysis of the CXC chemokine subfamily in mammals.</title>
      <authorList>
        <person name="Modi W.S."/>
        <person name="Yoshimura T."/>
      </authorList>
      <dbReference type="PubMed" id="10028286"/>
      <dbReference type="DOI" id="10.1093/oxfordjournals.molbev.a026101"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the intercrine alpha (chemokine CxC) family.</text>
  </comment>
  <dbReference type="EMBL" id="U95811">
    <property type="protein sequence ID" value="AAB93927.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_776724.1">
    <property type="nucleotide sequence ID" value="NM_174299.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_005208119.1">
    <property type="nucleotide sequence ID" value="XM_005208062.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O46675"/>
  <dbReference type="SMR" id="O46675"/>
  <dbReference type="STRING" id="9913.ENSBTAP00000039327"/>
  <dbReference type="PaxDb" id="9913-ENSBTAP00000039327"/>
  <dbReference type="Ensembl" id="ENSBTAT00000039536.4">
    <property type="protein sequence ID" value="ENSBTAP00000039327.3"/>
    <property type="gene ID" value="ENSBTAG00000027513.5"/>
  </dbReference>
  <dbReference type="GeneID" id="281214"/>
  <dbReference type="KEGG" id="bta:281214"/>
  <dbReference type="CTD" id="2920"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSBTAG00000027513"/>
  <dbReference type="eggNOG" id="ENOG502S7MM">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000155233"/>
  <dbReference type="HOGENOM" id="CLU_143902_1_0_1"/>
  <dbReference type="InParanoid" id="O46675"/>
  <dbReference type="OMA" id="CIETESQ"/>
  <dbReference type="OrthoDB" id="4170999at2759"/>
  <dbReference type="TreeFam" id="TF333433"/>
  <dbReference type="Reactome" id="R-BTA-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-6798695">
    <property type="pathway name" value="Neutrophil degranulation"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Chromosome 6"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSBTAG00000027513">
    <property type="expression patterns" value="Expressed in intramuscular adipose tissue and 57 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045236">
    <property type="term" value="F:CXCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008083">
    <property type="term" value="F:growth factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030593">
    <property type="term" value="P:neutrophil chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd00273">
    <property type="entry name" value="Chemokine_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000004">
    <property type="entry name" value="C-X-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001089">
    <property type="entry name" value="Chemokine_CXC"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018048">
    <property type="entry name" value="Chemokine_CXC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033899">
    <property type="entry name" value="CXC_Chemokine_domain"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF194">
    <property type="entry name" value="GROWTH-REGULATED ALPHA PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00436">
    <property type="entry name" value="INTERLEUKIN8"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00437">
    <property type="entry name" value="SMALLCYTKCXC"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00471">
    <property type="entry name" value="SMALL_CYTOKINES_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0339">Growth factor</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005060" description="Growth-regulated protein homolog gamma">
    <location>
      <begin position="30"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="39"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="41"/>
      <end position="81"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="98" mass="10393" checksum="942CD6897C21EDE9" modified="1998-06-01" version="1" precursor="true">MAPAASSAPRLLRAAMLLLLLVAAGRRAAGAPVVNELRCQCLQTLQGIHLKNIQSVKVTTPGPHCDQTEVIATLKTGQEVCLNPAAPMVKKIIDKMLN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>