<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-01-15" modified="2024-01-24" version="36" xmlns="http://uniprot.org/uniprot">
  <accession>Q1A3R1</accession>
  <accession>Q1A3Q3</accession>
  <accession>Q1A3Q4</accession>
  <name>CT54_CONLT</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Mu-conotoxin Lt5d</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Lt5.4</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Conus litteratus</name>
    <name type="common">Lettered cone</name>
    <dbReference type="NCBI Taxonomy" id="89445"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Caenogastropoda</taxon>
      <taxon>Neogastropoda</taxon>
      <taxon>Conoidea</taxon>
      <taxon>Conidae</taxon>
      <taxon>Conus</taxon>
      <taxon>Elisaconus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Genomics" volume="88" first="809" last="819">
      <title>Diversity and evolution of conotoxins based on gene expression profiling of Conus litteratus.</title>
      <authorList>
        <person name="Pi C."/>
        <person name="Liu J."/>
        <person name="Peng C."/>
        <person name="Liu Y."/>
        <person name="Jiang X."/>
        <person name="Zhao Y."/>
        <person name="Tang S."/>
        <person name="Wang L."/>
        <person name="Dong M."/>
        <person name="Chen S."/>
        <person name="Xu A."/>
      </authorList>
      <dbReference type="PubMed" id="16908117"/>
      <dbReference type="DOI" id="10.1016/j.ygeno.2006.06.014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>VARIANTS SER-55 AND ASN-56</scope>
    <source>
      <tissue>Venom duct</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2013" name="Biochem. Pharmacol." volume="85" first="1663" last="1671">
      <title>Identification, structural and pharmacological characterization of tau-CnVA, a conopeptide that selectively interacts with somatostatin sst receptor.</title>
      <authorList>
        <person name="Petrel C."/>
        <person name="Hocking H.G."/>
        <person name="Reynaud M."/>
        <person name="Upert G."/>
        <person name="Favreau P."/>
        <person name="Biass D."/>
        <person name="Paolini-Bertrand M."/>
        <person name="Peigneur S."/>
        <person name="Tytgat J."/>
        <person name="Gilles N."/>
        <person name="Hartley O."/>
        <person name="Boelens R."/>
        <person name="Stocklin R."/>
        <person name="Servent D."/>
      </authorList>
      <dbReference type="PubMed" id="23567999"/>
      <dbReference type="DOI" id="10.1016/j.bcp.2013.03.019"/>
    </citation>
    <scope>SYNTHESIS OF 51-62</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2007" name="Peptides" volume="28" first="2313" last="2319">
      <title>Isolation and characterization of a T-superfamily conotoxin from Conus litteratus with targeting tetrodotoxin-sensitive sodium channels.</title>
      <authorList>
        <person name="Liu J."/>
        <person name="Wu Q."/>
        <person name="Pi C."/>
        <person name="Zhao Y."/>
        <person name="Zhou M."/>
        <person name="Wang L."/>
        <person name="Chen S."/>
        <person name="Xu A."/>
      </authorList>
      <dbReference type="PubMed" id="17961831"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2007.09.006"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 51-62</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Mu-conotoxins block voltage-gated sodium channels (Nav). This toxin inhibits tetrodotoxin(TTX)-sensitive sodium channels, but does not affect TTX-resistant sodium channels. Reduces the amplitude of currents without changing the activation and inactivation kinetics of currents.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the venom duct.</text>
  </comment>
  <comment type="domain">
    <text evidence="6">The cysteine framework is V (CC-CC).</text>
  </comment>
  <comment type="PTM">
    <text evidence="6">Contains 2 disulfide bonds that can be either 'C1-C3, C2-C4' or 'C1-C4, C2-C3', since these disulfide connectivities have been observed for conotoxins with cysteine framework V (for examples, see AC P0DQQ7 and AC P81755).</text>
  </comment>
  <comment type="mass spectrometry" mass="1274.8778" method="MALDI" evidence="3"/>
  <comment type="miscellaneous">
    <text evidence="8">Negative results: does not have the ability to interact with the G-protein coupled somatostatin type 3 receptor (SSTR3).</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the conotoxin T superfamily.</text>
  </comment>
  <dbReference type="EMBL" id="DQ345353">
    <property type="protein sequence ID" value="ABC70189.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ345360">
    <property type="protein sequence ID" value="ABC70196.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ345361">
    <property type="protein sequence ID" value="ABC70197.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q1A3R1"/>
  <dbReference type="ConoServer" id="1140">
    <property type="toxin name" value="Lt5d precursor"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044489">
    <property type="term" value="P:negative regulation of voltage-gated sodium channel activity in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031565">
    <property type="entry name" value="T-conotoxin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF16981">
    <property type="entry name" value="Chi-conotoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000315427">
    <location>
      <begin position="23"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000315428" description="Mu-conotoxin Lt5d">
    <location>
      <begin position="51"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Lt5.4.1." evidence="2">
    <original>A</original>
    <variation>S</variation>
    <location>
      <position position="55"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Lt5.4.2." evidence="2">
    <original>K</original>
    <variation>N</variation>
    <location>
      <position position="56"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="16908117"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="17961831"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="16908117"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="17961831"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="17961831"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="23567999"/>
    </source>
  </evidence>
  <sequence length="62" mass="6907" checksum="D9F0575D4EAD468E" modified="2006-07-11" version="1" precursor="true">MRCLPVFIILLLLIPSAPSVDAQPTTKDDVPLASLHDNAKRALQMFWNKRDCCPAKLLCCNP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>