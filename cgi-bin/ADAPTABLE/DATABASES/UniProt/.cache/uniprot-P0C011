<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-06-07" modified="2022-05-25" version="25" xmlns="http://uniprot.org/uniprot">
  <accession>P0C011</accession>
  <name>KAS2_KASSE</name>
  <protein>
    <recommendedName>
      <fullName>Kassinatuerin-2</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Kassina senegalensis</name>
    <name type="common">Senegal running frog</name>
    <dbReference type="NCBI Taxonomy" id="8415"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Microhyloidea</taxon>
      <taxon>Hyperoliidae</taxon>
      <taxon>Kassina</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Biochem. Biophys. Res. Commun." volume="268" first="433" last="436">
      <title>Kassinatuerin-1: a peptide with broad-spectrum antimicrobial activity isolated from the skin of the hyperoliid frog, Kassina senegalensis.</title>
      <authorList>
        <person name="Mattute B."/>
        <person name="Knoop F.C."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="10679222"/>
      <dbReference type="DOI" id="10.1006/bbrc.2000.2136"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SYNTHESIS</scope>
    <scope>AMIDATION AT ILE-20</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Has no antimicrobial activities against bacteria (E.coli and S.aureus) nor against the fungus C.albicans.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin dorsal glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2221.3" method="Electrospray" evidence="1"/>
  <dbReference type="AlphaFoldDB" id="P0C011"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043809" description="Kassinatuerin-2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="modified residue" description="Isoleucine amide" evidence="1">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10679222"/>
    </source>
  </evidence>
  <sequence length="20" mass="2223" checksum="F0483E2134449379" modified="2005-06-07" version="1">FIQYLAPLIPHAVKAISDLI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>