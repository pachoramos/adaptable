<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-01-24" version="91" xmlns="http://uniprot.org/uniprot">
  <accession>Q27239</accession>
  <name>CECA_BOMMO</name>
  <protein>
    <recommendedName>
      <fullName>Cecropin-A</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">CECA</name>
  </gene>
  <organism>
    <name type="scientific">Bombyx mori</name>
    <name type="common">Silk moth</name>
    <dbReference type="NCBI Taxonomy" id="7091"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Bombycoidea</taxon>
      <taxon>Bombycidae</taxon>
      <taxon>Bombycinae</taxon>
      <taxon>Bombyx</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Biosci. Biotechnol. Biochem." volume="58" first="1476" last="1478">
      <title>Cloning of cDNAs for cecropins A and B, and expression of the genes in the silkworm, Bombyx mori.</title>
      <authorList>
        <person name="Yamano Y."/>
        <person name="Matsumoto M."/>
        <person name="Inoue K."/>
        <person name="Kawabata T."/>
        <person name="Morishima I."/>
      </authorList>
      <dbReference type="PubMed" id="7765280"/>
      <dbReference type="DOI" id="10.1271/bbb.58.1476"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>C108</strain>
      <tissue>Larval fat body</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Cecropins have lytic and antibacterial activity against several Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Highest expression in fat body and hemocytes. Is also expressed in Malpighian tubules and to a much lesser extent in midgut. Not present in silk gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the cecropin family.</text>
  </comment>
  <dbReference type="EMBL" id="D17394">
    <property type="protein sequence ID" value="BAA04217.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="S74376">
    <property type="protein sequence ID" value="AAC60515.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="JC2295">
    <property type="entry name" value="CKMTA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001037030.1">
    <property type="nucleotide sequence ID" value="NM_001043565.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001037462.1">
    <property type="nucleotide sequence ID" value="NM_001043997.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_004933962.1">
    <property type="nucleotide sequence ID" value="XM_004933905.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q27239"/>
  <dbReference type="SMR" id="Q27239"/>
  <dbReference type="STRING" id="7091.Q27239"/>
  <dbReference type="PaxDb" id="7091-BGIBMGA006280-TA"/>
  <dbReference type="EnsemblMetazoa" id="NM_001043565.2">
    <property type="protein sequence ID" value="NP_001037030.2"/>
    <property type="gene ID" value="LOC101743336"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="NM_001043997.1">
    <property type="protein sequence ID" value="NP_001037462.1"/>
    <property type="gene ID" value="GeneID_693029"/>
  </dbReference>
  <dbReference type="GeneID" id="101743336"/>
  <dbReference type="GeneID" id="693029"/>
  <dbReference type="KEGG" id="bmor:101743336"/>
  <dbReference type="KEGG" id="bmor:693029"/>
  <dbReference type="CTD" id="693029"/>
  <dbReference type="eggNOG" id="ENOG502T7RS">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_187909_0_0_1"/>
  <dbReference type="InParanoid" id="Q27239"/>
  <dbReference type="OMA" id="FACIMAF"/>
  <dbReference type="OrthoDB" id="3267574at2759"/>
  <dbReference type="Proteomes" id="UP000005204">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000875">
    <property type="entry name" value="Cecropin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00272">
    <property type="entry name" value="Cecropin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00268">
    <property type="entry name" value="CECROPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000004824" evidence="1">
    <location>
      <begin position="23"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004825" description="Cecropin-A" evidence="3">
    <location>
      <begin position="27"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="61"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7765280"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="63" mass="6762" checksum="6A1C39975516D86A" modified="1997-11-01" version="1" precursor="true">MNFVRILSFVFALVLALGAVSAAPEPRWKLFKKIEKVGRNVRDGLIKAGPAIAVIGQAKSLGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>