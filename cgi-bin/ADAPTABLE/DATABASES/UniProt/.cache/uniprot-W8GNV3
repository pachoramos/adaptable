<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-05-08" modified="2024-05-29" version="17" xmlns="http://uniprot.org/uniprot">
  <accession>W8GNV3</accession>
  <name>B11A_TETBN</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">M-myrmicitoxin(01)-Tb1a</fullName>
      <shortName evidence="6">M-MYRTX(01)-Tb1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Ant peptide P16</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="4">Bicarinalin</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="9">Bicarinaline</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="5">M-myrmicitoxin-Tb1a</fullName>
      <shortName evidence="5">M-MYRTX-Tb1a</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Tetramorium bicarinatum</name>
    <name type="common">Tramp ant</name>
    <dbReference type="NCBI Taxonomy" id="219812"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Myrmicinae</taxon>
      <taxon>Tetramorium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2016" name="Peptides" volume="79" first="103" last="113">
      <title>Biochemical and biophysical combined study of bicarinalin, an ant venom antimicrobial peptide.</title>
      <authorList>
        <person name="Tene N."/>
        <person name="Bonnafe E."/>
        <person name="Berger F."/>
        <person name="Rifflet A."/>
        <person name="Guilhaudis L."/>
        <person name="Segalas-Milazzo I."/>
        <person name="Pipy B."/>
        <person name="Coste A."/>
        <person name="Leprince J."/>
        <person name="Treilhou M."/>
      </authorList>
      <dbReference type="PubMed" id="27058430"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2016.04.001"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 57-66</scope>
    <scope>3D-STRUCTURE MODELING</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2013" name="Toxicon" volume="70" first="70" last="81">
      <title>Profiling the venom gland transcriptome of Tetramorium bicarinatum (Hymenoptera: Formicidae): the first transcriptome analysis of an ant species.</title>
      <authorList>
        <person name="Bouzid W."/>
        <person name="Klopp C."/>
        <person name="Verdenaud M."/>
        <person name="Ducancel F."/>
        <person name="Vetillard A."/>
      </authorList>
      <dbReference type="PubMed" id="23584016"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2013.03.010"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2012" name="Peptides" volume="38" first="363" last="370">
      <title>Identification and characterization of a novel antimicrobial peptide from the venom of the ant Tetramorium bicarinatum.</title>
      <authorList>
        <person name="Rifflet A."/>
        <person name="Gavalda S."/>
        <person name="Tene N."/>
        <person name="Orivel J."/>
        <person name="Leprince J."/>
        <person name="Guilhaudis L."/>
        <person name="Genin E."/>
        <person name="Vetillard A."/>
        <person name="Treilhou M."/>
      </authorList>
      <dbReference type="PubMed" id="22960382"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2012.08.018"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 57-76</scope>
    <scope>SYNTHESIS OF 57-76</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT VAL-76</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>CIRCULAR DICHROISM</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2017" name="Toxins" volume="10" first="1" last="10">
      <title>Anti-helicobacter pylori properties of the ant-venom peptide bicarinalin.</title>
      <authorList>
        <person name="Guzman J."/>
        <person name="Tene N."/>
        <person name="Touchard A."/>
        <person name="Castillo D."/>
        <person name="Belkhelfa H."/>
        <person name="Haddioui-Hbabi L."/>
        <person name="Treilhou M."/>
        <person name="Sauvain M."/>
      </authorList>
      <dbReference type="PubMed" id="29286296"/>
      <dbReference type="DOI" id="10.3390/toxins10010021"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 57-76</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2016" name="Toxins" volume="8" first="1" last="28">
      <title>The biochemical toxin arsenal from ant venoms.</title>
      <authorList>
        <person name="Touchard A."/>
        <person name="Aili S.R."/>
        <person name="Fox E.G."/>
        <person name="Escoubas P."/>
        <person name="Orivel J."/>
        <person name="Nicholson G.M."/>
        <person name="Dejean A."/>
      </authorList>
      <dbReference type="PubMed" id="26805882"/>
      <dbReference type="DOI" id="10.3390/toxins8010030"/>
    </citation>
    <scope>REVIEW</scope>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3">Antimicrobial peptide that shows antimicrobial activities against all microorganisms tested with minimal inhibitory concentrations (MICs) values ranging from 0.45 to 97.5 umol/L (PubMed:27058430). This peptide kills the microorganisms by permeabilizating the membranes (PubMed:27058430). It shows a very weak hemolytic activity (HC(50)=325 umol/L) and weak cytotoxicity against human lymphocytes (LC(50)=67.8 umol/L) (PubMed:22960382, PubMed:27058430). Gram-negative bacteria tested are E.coli (MIC=24.4 umol/L), C.sakazakii (MIC=5.8 umol/L), P.aeruginosa (MIC=8.7-12.2 umol/L), S.enterica (MIC=5.4 umol/L), and H.pylori (MIC=0.99-3.9 umol/L) (PubMed:22960382, PubMed:29286296). Gram-positive bacteria tested are E.hirae (MIC=12.2 umol/L), S.aureus (MIC=3.0-6.4 umol/L), methicillin-resistant S.aureus (MRSA) (MIC=8.7 umol/L), S.xylosus (MIC=0.45-1.3 umol/L), and B.subtilis (MIC=24.4 umol/L) (PubMed:22960382, PubMed:27058430). Fungi tested are A.niger (MIC=0.75 umol/L), C.albicans (MIC=17.3 umol/L), G.candidum (MIC=97.5 umol/L), and S.cerevisiae (MIC=6.1 umol/L) (PubMed:27058430). Finally the parasite tested is L.infantum (MIC=1.5 umol/L) (PubMed:27058430).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="8">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="7">Forms a helical membrane channel in the prey.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">The C-terminal amidation is important for antimicrobial activity, since a non-amidated synthetic peptide shows a reduced antimicrobial activity (2-20-fold depending on the strain tested). The amidation may play a positive role in the peptide conformation, since amidated peptide shows an increase of about 5% of helical content.</text>
  </comment>
  <comment type="mass spectrometry" mass="2213.06" method="Electrospray" evidence="2"/>
  <comment type="miscellaneous">
    <text evidence="8">This is the most abundant peptide from the venom of T.bicarinatum.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the formicidae venom precursor-01 superfamily.</text>
  </comment>
  <dbReference type="EMBL" id="KF929552">
    <property type="protein sequence ID" value="AHK22716.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="JZ168535">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="W8GNV3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR049518">
    <property type="entry name" value="Pilosulin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17499">
    <property type="entry name" value="Pilosulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000447049" evidence="7">
    <location>
      <begin position="27"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5004909437" description="M-myrmicitoxin(01)-Tb1a" evidence="2">
    <location>
      <begin position="57"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="2">
    <location>
      <position position="76"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="22960382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="27058430"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="22960382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="26805882"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="22960382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="27058430"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="9">
    <source>
      <dbReference type="EMBL" id="AHK22716.1"/>
    </source>
  </evidence>
  <sequence length="79" mass="8172" checksum="37319082CCED2E49" modified="2014-05-14" version="1" precursor="true">MKLSFLSLVLAIILVMALMYTPHAEAKAWADADADATAAADADADAVADALADAVAKIKIPWGKVKDFLVGGMKAVGKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>