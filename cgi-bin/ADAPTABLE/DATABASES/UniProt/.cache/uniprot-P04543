<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1987-08-13" modified="2024-10-02" version="96" xmlns="http://uniprot.org/uniprot">
  <accession>P04543</accession>
  <accession>Q77YB6</accession>
  <name>NS2_HRSVA</name>
  <protein>
    <recommendedName>
      <fullName>Non-structural protein 2</fullName>
      <shortName>NS2</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Non-structural protein 1B</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">1B</name>
    <name type="synonym">NS2</name>
  </gene>
  <organism>
    <name type="scientific">Human respiratory syncytial virus A (strain A2)</name>
    <dbReference type="NCBI Taxonomy" id="11259"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Orthornavirae</taxon>
      <taxon>Negarnaviricota</taxon>
      <taxon>Haploviricotina</taxon>
      <taxon>Monjiviricetes</taxon>
      <taxon>Mononegavirales</taxon>
      <taxon>Pneumoviridae</taxon>
      <taxon>Orthopneumovirus</taxon>
      <taxon>Orthopneumovirus hominis</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1985" name="Virology" volume="143" first="442" last="451">
      <title>Nucleotide sequences of the 1B and 1C nonstructural protein mRNAs of human respiratory syncytial virus.</title>
      <authorList>
        <person name="Collins P.L."/>
        <person name="Wertz G.W."/>
      </authorList>
      <dbReference type="PubMed" id="2998021"/>
      <dbReference type="DOI" id="10.1016/0042-6822(85)90384-8"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1985" name="J. Virol." volume="55" first="101" last="110">
      <title>mRNA sequence of three respiratory syncytial virus genes encoding two nonstructural proteins and a 22K structural protein.</title>
      <authorList>
        <person name="Elango N."/>
        <person name="Satake M."/>
        <person name="Venkatesan S."/>
      </authorList>
      <dbReference type="PubMed" id="4009789"/>
      <dbReference type="DOI" id="10.1128/jvi.55.1.101-110.1985"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1995" name="Virology" volume="208" first="478" last="484">
      <title>A cold-passaged, attenuated strain of human respiratory syncytial virus contains mutations in the F and L genes.</title>
      <authorList>
        <person name="Connors M."/>
        <person name="Crowe J.E. Jr."/>
        <person name="Firestone C.Y."/>
        <person name="Murphy B.R."/>
        <person name="Collins P.L."/>
      </authorList>
      <dbReference type="PubMed" id="7747420"/>
      <dbReference type="DOI" id="10.1006/viro.1995.1178"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1996" name="Virus Genes" volume="13" first="269" last="273">
      <title>Acquisition of the ts phenotype by a chemically mutagenized cold-passaged human respiratory syncytial virus vaccine candidate results from the acquisition of a single mutation in the polymerase (L) gene.</title>
      <authorList>
        <person name="Crowe J.E. Jr."/>
        <person name="Firestone C.Y."/>
        <person name="Whitehead S.S."/>
        <person name="Collins P.L."/>
        <person name="Murphy B.R."/>
      </authorList>
      <dbReference type="PubMed" id="9035372"/>
      <dbReference type="DOI" id="10.1007/bf00366988"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1998" name="J. Virol." volume="72" first="4467" last="4471">
      <title>Recombinant respiratory syncytial virus (RSV) bearing a set of mutations from cold-passaged RSV is attenuated in chimpanzees.</title>
      <authorList>
        <person name="Whitehead S.S."/>
        <person name="Juhasz K."/>
        <person name="Firestone C.Y."/>
        <person name="Collins P.L."/>
        <person name="Murphy B.R."/>
      </authorList>
      <dbReference type="PubMed" id="9557743"/>
      <dbReference type="DOI" id="10.1128/jvi.72.5.4467-4471.1998"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
    <source>
      <strain>Cold-passage attenuated</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1996" name="Virus Res." volume="43" first="155" last="161">
      <title>Expression and characterisation of the NS1 and NS2 proteins of respiratory syncytial virus.</title>
      <authorList>
        <person name="Evans J.E."/>
        <person name="Cane P.A."/>
        <person name="Pringle C.R."/>
      </authorList>
      <dbReference type="PubMed" id="8864205"/>
      <dbReference type="DOI" id="10.1016/0168-1702(96)01327-5"/>
    </citation>
    <scope>SUBUNIT</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2004" name="J. Virol." volume="78" first="4363" last="4369">
      <title>Suppression of the induction of alpha, beta, and lambda interferons by the NS1 and NS2 proteins of human respiratory syncytial virus in human epithelial cells and macrophages.</title>
      <authorList>
        <person name="Spann K.M."/>
        <person name="Tran K.C."/>
        <person name="Chi B."/>
        <person name="Rabin R.L."/>
        <person name="Collins P.L."/>
      </authorList>
      <dbReference type="PubMed" id="15047850"/>
      <dbReference type="DOI" id="10.1128/jvi.78.8.4363-4369.2004"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2005" name="J. Virol." volume="79" first="5353" last="5362">
      <title>Effects of nonstructural proteins NS1 and NS2 of human respiratory syncytial virus on interferon regulatory factor 3, NF-kappaB, and proinflammatory cytokines.</title>
      <authorList>
        <person name="Spann K.M."/>
        <person name="Tran K.C."/>
        <person name="Collins P.L."/>
      </authorList>
      <dbReference type="PubMed" id="15827150"/>
      <dbReference type="DOI" id="10.1128/jvi.79.9.5353-5362.2005"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2007" name="J. Virol." volume="81" first="1786" last="1795">
      <title>Nonstructural proteins of respiratory syncytial virus suppress premature apoptosis by an NF-kappaB-dependent, interferon-independent mechanism and facilitate virus growth.</title>
      <authorList>
        <person name="Bitko V."/>
        <person name="Shulyayeva O."/>
        <person name="Mazumder B."/>
        <person name="Musiyenko A."/>
        <person name="Ramaswamy M."/>
        <person name="Look D.C."/>
        <person name="Barik S."/>
      </authorList>
      <dbReference type="PubMed" id="17151097"/>
      <dbReference type="DOI" id="10.1128/jvi.01420-06"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2007" name="J. Virol." volume="81" first="3428" last="3436">
      <title>Respiratory syncytial virus NS1 protein degrades STAT2 by using the Elongin-Cullin E3 ligase.</title>
      <authorList>
        <person name="Elliott J."/>
        <person name="Lynch O.T."/>
        <person name="Suessmuth Y."/>
        <person name="Qian P."/>
        <person name="Boyd C.R."/>
        <person name="Burrows J.F."/>
        <person name="Buick R."/>
        <person name="Stevenson N.J."/>
        <person name="Touzelet O."/>
        <person name="Gadina M."/>
        <person name="Power U.F."/>
        <person name="Johnston J.A."/>
      </authorList>
      <dbReference type="PubMed" id="17251292"/>
      <dbReference type="DOI" id="10.1128/jvi.02303-06"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2009" name="J. Virol." volume="83" first="3734" last="3742">
      <title>Human respiratory syncytial virus nonstructural protein NS2 antagonizes the activation of beta interferon transcription by interacting with RIG-I.</title>
      <authorList>
        <person name="Ling Z."/>
        <person name="Tran K.C."/>
        <person name="Teng M.N."/>
      </authorList>
      <dbReference type="PubMed" id="19193793"/>
      <dbReference type="DOI" id="10.1128/jvi.02434-08"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH HOST RIGI</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2011" name="J. Virol." volume="85" first="10090" last="10100">
      <title>Multiple functional domains and complexes of the two nonstructural proteins of human respiratory syncytial virus contribute to interferon suppression and cellular location.</title>
      <authorList>
        <person name="Swedan S."/>
        <person name="Andrews J."/>
        <person name="Majumdar T."/>
        <person name="Musiyenko A."/>
        <person name="Barik S."/>
      </authorList>
      <dbReference type="PubMed" id="21795342"/>
      <dbReference type="DOI" id="10.1128/jvi.00413-11"/>
    </citation>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>DOMAIN</scope>
    <scope>SUBUNIT</scope>
    <scope>INTERACTION WITH HOST MAP1B</scope>
    <scope>INTERACTION WITH NS1</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2013" name="Cell Res." volume="23" first="1025" last="1042">
      <title>Viral degradasome hijacks mitochondria to suppress innate immunity.</title>
      <authorList>
        <person name="Goswami R."/>
        <person name="Majumdar T."/>
        <person name="Dhar J."/>
        <person name="Chattopadhyay S."/>
        <person name="Bandyopadhyay S.K."/>
        <person name="Verbovetskaya V."/>
        <person name="Sen G.C."/>
        <person name="Barik S."/>
      </authorList>
      <dbReference type="PubMed" id="23877405"/>
      <dbReference type="DOI" id="10.1038/cr.2013.98"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="14">
    <citation type="journal article" date="2015" name="J. Immunol. Res." volume="2015" first="738547" last="738547">
      <title>Respiratory Syncytial Virus Nonstructural Proteins Upregulate SOCS1 and SOCS3 in the Different Manner from Endogenous IFN Signaling.</title>
      <authorList>
        <person name="Zheng J."/>
        <person name="Yang P."/>
        <person name="Tang Y."/>
        <person name="Pan Z."/>
        <person name="Zhao D."/>
      </authorList>
      <dbReference type="PubMed" id="26557722"/>
      <dbReference type="DOI" id="10.1155/2015/738547"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="15">
    <citation type="journal article" date="2019" name="PLoS Pathog." volume="15" first="e1007984" last="e1007984">
      <title>Respiratory syncytial virus nonstructural proteins 1 and 2: Exceptional disrupters of innate immune responses.</title>
      <authorList>
        <person name="Sedeyn K."/>
        <person name="Schepens B."/>
        <person name="Saelens X."/>
      </authorList>
      <dbReference type="PubMed" id="31622448"/>
      <dbReference type="DOI" id="10.1371/journal.ppat.1007984"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <reference key="16">
    <citation type="journal article" date="2020" name="Front. Cell. Infect. Microbiol." volume="10" first="225" last="225">
      <title>Respiratory Syncytial Virus's Non-structural Proteins: Masters of Interference.</title>
      <authorList>
        <person name="Thornhill E.M."/>
        <person name="Verhoeven D."/>
      </authorList>
      <dbReference type="PubMed" id="32509597"/>
      <dbReference type="DOI" id="10.3389/fcimb.2020.00225"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3 4 5 7 8">Plays a major role in antagonizing the type I IFN-mediated antiviral response (PubMed:15047850). Acts cooperatively with NS1 to repress activation and nuclear translocation of host IFN-regulatory factor IRF3 (PubMed:15827150). Interacts with the host cytoplasmic sensor of viral nucleic acids RIGI and prevents the interaction with its downstream partner MAVS (PubMed:19193793). Together with NS2, participates in the proteasomal degradation of host STAT2, IRF3, IRF7, TBK1 and RIGI through a NS-degradasome involving CUL2 and Elongin-C (PubMed:17251292, PubMed:23877405). The degradasome requires an intact mitochondrial MAVS (PubMed:23877405). Induces host SOCS1 expression (PubMed:26557722). Induces activation of NF-kappa-B. Suppresses premature apoptosis by an NF-kappa-B-dependent, interferon-independent mechanism promoting continued viral replication (PubMed:17151097).</text>
  </comment>
  <comment type="subunit">
    <text evidence="5 6 9">Monomer (instable) (PubMed:8864205). Homomultimer (PubMed:8864205). Heteromultimer with NS1 (PubMed:21795342). Interacts with host RIGI (via N-terminus); this interaction prevents host signaling pathway involved in interferon production (PubMed:19193793). Interacts with host MAP1B/microtubule-associated protein 1B (PubMed:21795342).</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-3648048">
      <id>P04543</id>
    </interactant>
    <interactant intactId="EBI-995350">
      <id>O95786</id>
      <label>RIGI</label>
    </interactant>
    <organismsDiffer>true</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6 7">Host mitochondrion</location>
    </subcellularLocation>
    <text evidence="6 7">Most NS2 resides in the mitochondria as a heteromer with NS1.</text>
  </comment>
  <comment type="domain">
    <text evidence="6">The DNLP motif has IFN suppressive functions like binding to host MAP1B.</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the pneumovirus non-structural protein 2 family.</text>
  </comment>
  <dbReference type="EMBL" id="M11486">
    <property type="protein sequence ID" value="AAB59851.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U50362">
    <property type="protein sequence ID" value="AAB86657.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U50363">
    <property type="protein sequence ID" value="AAB86669.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U63644">
    <property type="protein sequence ID" value="AAC55963.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF035006">
    <property type="protein sequence ID" value="AAC14895.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="PIR" id="B94336">
    <property type="entry name" value="MNNZ1B"/>
  </dbReference>
  <dbReference type="SMR" id="P04543"/>
  <dbReference type="IntAct" id="P04543">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9828721">
    <property type="pathway name" value="Translation of respiratory syncytial virus mRNAs"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9828806">
    <property type="pathway name" value="Maturation of hRSV A proteins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9833109">
    <property type="pathway name" value="Evasion by RSV of host interferon responses"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9833110">
    <property type="pathway name" value="RSV-host interactions"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000007678">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000134464">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000181145">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000181262">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000181559">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033650">
    <property type="term" value="C:host cell mitochondrion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052150">
    <property type="term" value="P:symbiont-mediated perturbation of host apoptosis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039548">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of IRF3 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039557">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of IRF7 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039540">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of RIG-I activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039723">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of TBK1 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039564">
    <property type="term" value="P:symbiont-mediated suppression of host JAK-STAT cascade via inhibition of STAT2 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039722">
    <property type="term" value="P:symbiont-mediated suppression of host toll-like receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039502">
    <property type="term" value="P:symbiont-mediated suppression of host type I interferon-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004336">
    <property type="entry name" value="RSV_NS2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03113">
    <property type="entry name" value="RSV_NS2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1045">Host mitochondrion</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1114">Inhibition of host interferon signaling pathway by virus</keyword>
  <keyword id="KW-1092">Inhibition of host IRF3 by virus</keyword>
  <keyword id="KW-1093">Inhibition of host IRF7 by virus</keyword>
  <keyword id="KW-1088">Inhibition of host RIG-I by virus</keyword>
  <keyword id="KW-1113">Inhibition of host RLR pathway by virus</keyword>
  <keyword id="KW-1106">Inhibition of host STAT2 by virus</keyword>
  <keyword id="KW-1223">Inhibition of host TBK1 by virus</keyword>
  <keyword id="KW-1225">Inhibition of host TLR pathway by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-1119">Modulation of host cell apoptosis by virus</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <feature type="chain" id="PRO_0000142789" description="Non-structural protein 2">
    <location>
      <begin position="1"/>
      <end position="124"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="DLNP; interaction with MAP1B" evidence="6">
    <location>
      <begin position="121"/>
      <end position="124"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAB59851." evidence="10" ref="1">
    <original>R</original>
    <variation>K</variation>
    <location>
      <position position="55"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="15047850"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15827150"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="17151097"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="17251292"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="19193793"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="21795342"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="23877405"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="26557722"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="8864205"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <sequence length="124" mass="14702" checksum="C14DF0FE9C9E5512" modified="1998-07-15" version="2">MDTTHNDNTPQRLMITDMRPLSLETIITSLTRDIITHKFIYLINHECIVRKLDERQATFTFLVNYEMKLLHKVGSTKYKKYTEYNTKYGTFPMPIFINHDGFLECIGIKPTKHTPIIYKYDLNP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>