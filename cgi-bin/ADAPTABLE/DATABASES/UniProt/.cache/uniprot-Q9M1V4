<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-07-28" modified="2024-10-02" version="94" xmlns="http://uniprot.org/uniprot">
  <accession>Q9M1V4</accession>
  <accession>A0MF42</accession>
  <name>DEF11_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 11</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">At3g63360</name>
    <name type="ORF">F16M2_210</name>
    <name type="ORF">MAA21.5</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Nature" volume="408" first="820" last="822">
      <title>Sequence and analysis of chromosome 3 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Salanoubat M."/>
        <person name="Lemcke K."/>
        <person name="Rieger M."/>
        <person name="Ansorge W."/>
        <person name="Unseld M."/>
        <person name="Fartmann B."/>
        <person name="Valle G."/>
        <person name="Bloecker H."/>
        <person name="Perez-Alonso M."/>
        <person name="Obermaier B."/>
        <person name="Delseny M."/>
        <person name="Boutry M."/>
        <person name="Grivell L.A."/>
        <person name="Mache R."/>
        <person name="Puigdomenech P."/>
        <person name="De Simone V."/>
        <person name="Choisne N."/>
        <person name="Artiguenave F."/>
        <person name="Robert C."/>
        <person name="Brottier P."/>
        <person name="Wincker P."/>
        <person name="Cattolico L."/>
        <person name="Weissenbach J."/>
        <person name="Saurin W."/>
        <person name="Quetier F."/>
        <person name="Schaefer M."/>
        <person name="Mueller-Auer S."/>
        <person name="Gabel C."/>
        <person name="Fuchs M."/>
        <person name="Benes V."/>
        <person name="Wurmbach E."/>
        <person name="Drzonek H."/>
        <person name="Erfle H."/>
        <person name="Jordan N."/>
        <person name="Bangert S."/>
        <person name="Wiedelmann R."/>
        <person name="Kranz H."/>
        <person name="Voss H."/>
        <person name="Holland R."/>
        <person name="Brandt P."/>
        <person name="Nyakatura G."/>
        <person name="Vezzi A."/>
        <person name="D'Angelo M."/>
        <person name="Pallavicini A."/>
        <person name="Toppo S."/>
        <person name="Simionati B."/>
        <person name="Conrad A."/>
        <person name="Hornischer K."/>
        <person name="Kauer G."/>
        <person name="Loehnert T.-H."/>
        <person name="Nordsiek G."/>
        <person name="Reichelt J."/>
        <person name="Scharfe M."/>
        <person name="Schoen O."/>
        <person name="Bargues M."/>
        <person name="Terol J."/>
        <person name="Climent J."/>
        <person name="Navarro P."/>
        <person name="Collado C."/>
        <person name="Perez-Perez A."/>
        <person name="Ottenwaelder B."/>
        <person name="Duchemin D."/>
        <person name="Cooke R."/>
        <person name="Laudie M."/>
        <person name="Berger-Llauro C."/>
        <person name="Purnelle B."/>
        <person name="Masuy D."/>
        <person name="de Haan M."/>
        <person name="Maarse A.C."/>
        <person name="Alcaraz J.-P."/>
        <person name="Cottet A."/>
        <person name="Casacuberta E."/>
        <person name="Monfort A."/>
        <person name="Argiriou A."/>
        <person name="Flores M."/>
        <person name="Liguori R."/>
        <person name="Vitale D."/>
        <person name="Mannhaupt G."/>
        <person name="Haase D."/>
        <person name="Schoof H."/>
        <person name="Rudd S."/>
        <person name="Zaccaria P."/>
        <person name="Mewes H.-W."/>
        <person name="Mayer K.F.X."/>
        <person name="Kaul S."/>
        <person name="Town C.D."/>
        <person name="Koo H.L."/>
        <person name="Tallon L.J."/>
        <person name="Jenkins J."/>
        <person name="Rooney T."/>
        <person name="Rizzo M."/>
        <person name="Walts A."/>
        <person name="Utterback T."/>
        <person name="Fujii C.Y."/>
        <person name="Shea T.P."/>
        <person name="Creasy T.H."/>
        <person name="Haas B."/>
        <person name="Maiti R."/>
        <person name="Wu D."/>
        <person name="Peterson J."/>
        <person name="Van Aken S."/>
        <person name="Pai G."/>
        <person name="Militscher J."/>
        <person name="Sellers P."/>
        <person name="Gill J.E."/>
        <person name="Feldblyum T.V."/>
        <person name="Preuss D."/>
        <person name="Lin X."/>
        <person name="Nierman W.C."/>
        <person name="Salzberg S.L."/>
        <person name="White O."/>
        <person name="Venter J.C."/>
        <person name="Fraser C.M."/>
        <person name="Kaneko T."/>
        <person name="Nakamura Y."/>
        <person name="Sato S."/>
        <person name="Kato T."/>
        <person name="Asamizu E."/>
        <person name="Sasamoto S."/>
        <person name="Kimura T."/>
        <person name="Idesawa K."/>
        <person name="Kawashima K."/>
        <person name="Kishida Y."/>
        <person name="Kiyokawa C."/>
        <person name="Kohara M."/>
        <person name="Matsumoto M."/>
        <person name="Matsuno A."/>
        <person name="Muraki A."/>
        <person name="Nakayama S."/>
        <person name="Nakazaki N."/>
        <person name="Shinpo S."/>
        <person name="Takeuchi C."/>
        <person name="Wada T."/>
        <person name="Watanabe A."/>
        <person name="Yamada M."/>
        <person name="Yasuda M."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="11130713"/>
      <dbReference type="DOI" id="10.1038/35048706"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2006" name="Plant Biotechnol. J." volume="4" first="317" last="324">
      <title>Simultaneous high-throughput recombinational cloning of open reading frames in closed and open configurations.</title>
      <authorList>
        <person name="Underwood B.A."/>
        <person name="Vanderhaeghen R."/>
        <person name="Whitford R."/>
        <person name="Town C.D."/>
        <person name="Hilson P."/>
      </authorList>
      <dbReference type="PubMed" id="17147637"/>
      <dbReference type="DOI" id="10.1111/j.1467-7652.2006.00183.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <comment type="sequence caution" evidence="3">
    <conflict type="erroneous termination">
      <sequence resource="EMBL-CDS" id="ABK28615" version="1"/>
    </conflict>
    <text>Extended C-terminus.</text>
  </comment>
  <dbReference type="EMBL" id="AL138648">
    <property type="protein sequence ID" value="CAB86437.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL163818">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002686">
    <property type="protein sequence ID" value="AEE80471.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ446785">
    <property type="protein sequence ID" value="ABE66035.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ653167">
    <property type="protein sequence ID" value="ABK28615.1"/>
    <property type="status" value="ALT_SEQ"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="T48125">
    <property type="entry name" value="T48125"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_191895.1">
    <property type="nucleotide sequence ID" value="NM_116201.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9M1V4"/>
  <dbReference type="SMR" id="Q9M1V4"/>
  <dbReference type="STRING" id="3702.Q9M1V4"/>
  <dbReference type="PaxDb" id="3702-AT3G63360.1"/>
  <dbReference type="EnsemblPlants" id="AT3G63360.1">
    <property type="protein sequence ID" value="AT3G63360.1"/>
    <property type="gene ID" value="AT3G63360"/>
  </dbReference>
  <dbReference type="GeneID" id="825511"/>
  <dbReference type="Gramene" id="AT3G63360.1">
    <property type="protein sequence ID" value="AT3G63360.1"/>
    <property type="gene ID" value="AT3G63360"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT3G63360"/>
  <dbReference type="Araport" id="AT3G63360"/>
  <dbReference type="TAIR" id="AT3G63360"/>
  <dbReference type="HOGENOM" id="CLU_2515737_0_0_1"/>
  <dbReference type="InParanoid" id="Q9M1V4"/>
  <dbReference type="OMA" id="HRCLCRN"/>
  <dbReference type="OrthoDB" id="691933at2759"/>
  <dbReference type="PRO" id="PR:Q9M1V4"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q9M1V4">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000379598" description="Defensin-like protein 11">
    <location>
      <begin position="30"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="54"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="58"/>
      <end position="77"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="85" mass="9240" checksum="3F3DF46AC4CE63AF" modified="2000-10-01" version="1" precursor="true">MGKTISFSAIILVFLLVSTGLMKQGDAQAQKCEWECKLLPNFPCWLKGAGEGLCDNLCKYEGAISGVCVSDPHRCLCRNRKPGCS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>