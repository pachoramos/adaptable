<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-11" modified="2021-06-02" version="32" xmlns="http://uniprot.org/uniprot">
  <accession>P67790</accession>
  <accession>P25419</accession>
  <name>HTF_ZOPAT</name>
  <protein>
    <recommendedName>
      <fullName>Hypertrehalosaemic factor</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>HOTH</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Hypertrehalosaemic neuropeptide</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Zophobas atratus</name>
    <name type="common">Giant mealworm beetle</name>
    <name type="synonym">Zophobas rugipes</name>
    <dbReference type="NCBI Taxonomy" id="7074"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Coleoptera</taxon>
      <taxon>Polyphaga</taxon>
      <taxon>Cucujiformia</taxon>
      <taxon>Tenebrionidae</taxon>
      <taxon>Zophobas</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="Peptides" volume="11" first="455" last="459">
      <title>The primary structure of the hypertrehalosemic neuropeptide from tenebrionid beetles: a novel member of the AKH/RPCH family.</title>
      <authorList>
        <person name="Gaede G."/>
        <person name="Rosinski G."/>
      </authorList>
      <dbReference type="PubMed" id="2381871"/>
      <dbReference type="DOI" id="10.1016/0196-9781(90)90042-4"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>AMIDATION AT TRP-8</scope>
    <source>
      <tissue>Corpora cardiaca</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Hypertrehalosaemic factors are neuropeptides that elevate the level of trehalose in the hemolymph (trehalose is the major carbohydrate in the hemolymph of insects).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the AKH/HRTH/RPCH family.</text>
  </comment>
  <dbReference type="PIR" id="B43976">
    <property type="entry name" value="B43976"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002047">
    <property type="entry name" value="Adipokinetic_hormone_CS"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00256">
    <property type="entry name" value="AKH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043442" description="Hypertrehalosaemic factor">
    <location>
      <begin position="1"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tryptophan amide" evidence="1">
    <location>
      <position position="8"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2381871"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="8" mass="1005" checksum="86745775B9C44736" modified="2004-10-11" version="1">QLNFSPNW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>