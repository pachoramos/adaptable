<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2016-11-30" modified="2023-05-03" version="5" xmlns="http://uniprot.org/uniprot">
  <accession>C0HK87</accession>
  <name>PGLB2_XENBO</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Peptide PGLa-B2</fullName>
    </recommendedName>
  </protein>
  <organism evidence="2">
    <name type="scientific">Xenopus borealis</name>
    <name type="common">Kenyan clawed frog</name>
    <dbReference type="NCBI Taxonomy" id="8354"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Pipoidea</taxon>
      <taxon>Pipidae</taxon>
      <taxon>Xenopodinae</taxon>
      <taxon>Xenopus</taxon>
      <taxon>Xenopus</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2010" name="Comp. Biochem. Physiol." volume="152" first="467" last="472">
      <title>Antimicrobial peptides with therapeutic potential from skin secretions of the Marsabit clawed frog Xenopus borealis (Pipidae).</title>
      <authorList>
        <person name="Mechkarska M."/>
        <person name="Ahmed E."/>
        <person name="Coquet L."/>
        <person name="Leprince J."/>
        <person name="Jouenne T."/>
        <person name="Vaudry H."/>
        <person name="King J.D."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="20656059"/>
      <dbReference type="DOI" id="10.1016/j.cbpc.2010.07.007"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-21</scope>
    <source>
      <tissue evidence="2">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antimicrobial activity against Gram-negative bacterium E.coli ATCC 25922 (MIC=25 uM), Gram-positive bacterium S.auerus ATCC 25923 (MIC=50 uM) and against fungus C.albicans ATCC 90028 (MIC=25 uM). Has some hemolytic activity against human erythrocytes at high concentration.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1955.2" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the gastrin/cholecystokinin family. Magainin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HK87"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000438427" description="Peptide PGLa-B2" evidence="1">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="21"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="20656059"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="20656059"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="20656059"/>
    </source>
  </evidence>
  <sequence length="21" mass="1956" checksum="B52B49474B578BF6" modified="2016-11-30" version="1">GMASKAGSIVGKIAKIALGAL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>