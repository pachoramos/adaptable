<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-03-28" modified="2023-09-13" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>U5L409</accession>
  <name>MSD2_AMAEX</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">MSDIN-like toxin proprotein 2</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="3">Toxin MSD2</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Amanita exitialis</name>
    <name type="common">Guangzhou destroying angel</name>
    <dbReference type="NCBI Taxonomy" id="262245"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Basidiomycota</taxon>
      <taxon>Agaricomycotina</taxon>
      <taxon>Agaricomycetes</taxon>
      <taxon>Agaricomycetidae</taxon>
      <taxon>Agaricales</taxon>
      <taxon>Pluteineae</taxon>
      <taxon>Amanitaceae</taxon>
      <taxon>Amanita</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2013" name="Gene" volume="532" first="63" last="71">
      <title>Illumina-based de novo transcriptome sequencing and analysis of Amanita exitialis basidiocarps.</title>
      <authorList>
        <person name="Li P."/>
        <person name="Deng W.Q."/>
        <person name="Li T.H."/>
        <person name="Song B."/>
        <person name="Shen Y.H."/>
      </authorList>
      <dbReference type="PubMed" id="24050899"/>
      <dbReference type="DOI" id="10.1016/j.gene.2013.09.014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="5">Probable toxin that belongs to the MSDIN-like toxin family responsible for a large number of food poisoning cases and deaths (PubMed:24050899).</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed in basidiocarps (PubMed:24050899).</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Processed by the macrocyclase-peptidase enzyme POPB to yield a toxic cyclic octapeptide (By similarity). POPB first removes 10 residues from the N-terminus (By similarity). Conformational trapping of the remaining peptide forces the enzyme to release this intermediate rather than proceed to macrocyclization (By similarity). The enzyme rebinds the remaining peptide in a different conformation and catalyzes macrocyclization of the N-terminal 8 residues (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the MSDIN fungal toxin family.</text>
  </comment>
  <dbReference type="EMBL" id="KF387491">
    <property type="protein sequence ID" value="AGW83715.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="U5L409"/>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027582">
    <property type="entry name" value="Amanitin/phalloidin"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR04309">
    <property type="entry name" value="amanitin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="propeptide" id="PRO_0000443758" evidence="5">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000443759" description="Toxin MSD2" evidence="5">
    <location>
      <begin position="11"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000443760" evidence="5">
    <location>
      <begin position="19"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Ile-Pro)" evidence="5">
    <location>
      <begin position="11"/>
      <end position="18"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A067SLB9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="24050899"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="24050899"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="24050899"/>
    </source>
  </evidence>
  <sequence length="33" mass="3656" checksum="3373A0DA20B8C1F3" modified="2014-01-22" version="1" precursor="true">MSDINATRLPIIWAPVVPCISDDNDSTLTRGQR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>