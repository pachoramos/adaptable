<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-02-01" modified="2023-09-13" version="83" xmlns="http://uniprot.org/uniprot">
  <accession>P41249</accession>
  <name>KEDA_ACTSL</name>
  <protein>
    <recommendedName>
      <fullName>Apokedarcidin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Actinomycete sp. (strain L585-6 / ATCC 53650)</name>
    <dbReference type="NCBI Taxonomy" id="38989"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Actinomycetales</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="J. Antibiot." volume="45" first="1250" last="1254">
      <title>Kedarcidin, a new chromoprotein antitumor antibiotic. II. Isolation, purification and physico-chemical properties.</title>
      <authorList>
        <person name="Hofstead S.J."/>
        <person name="Matson J.A."/>
        <person name="Malacko A.R."/>
        <person name="Marquardt H."/>
      </authorList>
      <dbReference type="PubMed" id="1399845"/>
      <dbReference type="DOI" id="10.7164/antibiotics.45.1250"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <strain>ATCC 53650 / L585-6</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="Proc. Natl. Acad. Sci. U.S.A." volume="90" first="8009" last="8012">
      <title>Selective proteolytic activity of the antitumor agent kedarcidin.</title>
      <authorList>
        <person name="Zein N."/>
        <person name="Casazza A.M."/>
        <person name="Doyle T.W."/>
        <person name="Leet J.E."/>
        <person name="Scheoeder D.R."/>
        <person name="Solomon W."/>
        <person name="Nadler S.G."/>
      </authorList>
      <dbReference type="PubMed" id="8367457"/>
      <dbReference type="DOI" id="10.1073/pnas.90.17.8009"/>
    </citation>
    <scope>CHARACTERIZATION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1994" name="Biochemistry" volume="33" first="11438" last="11452">
      <title>Sequential 1H, 13C, and 15N NMR assignments and solution conformation of apokedarcidin.</title>
      <authorList>
        <person name="Constantine K.L."/>
        <person name="Colson K.L."/>
        <person name="Wittekind M."/>
        <person name="Friedrichs M.S."/>
        <person name="Zein N."/>
        <person name="Tuttle J."/>
        <person name="Langley D.R."/>
        <person name="Leet J.E."/>
        <person name="Schroeder D.R."/>
        <person name="Lam K.S."/>
        <person name="Farmer B.T. II"/>
        <person name="Metzler W.J."/>
        <person name="Bruccoleri R.E."/>
        <person name="Mueller L."/>
      </authorList>
      <dbReference type="PubMed" id="7918358"/>
      <dbReference type="DOI" id="10.1021/bi00204a006"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
    <source>
      <strain>ATCC 53650 / L585-6</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Binds non-covalently to an enediyne chromophore which is the cytotoxic and mutagenic component of the antibiotic. The chromophore cleaves duplex DNA site-specifically in a single-stranded manner. The apoprotein cleaves proteins selectively, in particular highly basic histones, with H1 proteins being cleaved the more readily.</text>
  </comment>
  <comment type="domain">
    <text>This protein consists of an immunoglobulin-like seven-stranded antiparallel beta-barrel domain linked to a subdomain composed of two beta-hairpin ribbons.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the neocarzinostatin family.</text>
  </comment>
  <dbReference type="PDB" id="1AKP">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-114"/>
  </dbReference>
  <dbReference type="PDBsum" id="1AKP"/>
  <dbReference type="AlphaFoldDB" id="P41249"/>
  <dbReference type="BMRB" id="P41249"/>
  <dbReference type="SMR" id="P41249"/>
  <dbReference type="EvolutionaryTrace" id="P41249"/>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.230">
    <property type="entry name" value="Neocarzinostatin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027273">
    <property type="entry name" value="Neocarzinostatin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002186">
    <property type="entry name" value="Neocarzinostatin_fam"/>
  </dbReference>
  <dbReference type="NCBIfam" id="NF040680">
    <property type="entry name" value="chromo_anti"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00960">
    <property type="entry name" value="Neocarzinostat"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01885">
    <property type="entry name" value="MACROMOMYCIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF49319">
    <property type="entry name" value="Actinoxanthin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <feature type="chain" id="PRO_0000213119" description="Apokedarcidin">
    <location>
      <begin position="1"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="37"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="88"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="3"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="11"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="21"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="36"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="58"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="70"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="77"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="84"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="102"/>
      <end position="104"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="7918358"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007744" key="3">
    <source>
      <dbReference type="PDB" id="1AKP"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="1AKP"/>
    </source>
  </evidence>
  <sequence length="114" mass="10969" checksum="1901E2B14E4197B4" modified="1995-02-01" version="1">ASAAVSVSPATGLADGATVTVSASGFATSTSATALQCAILADGRGACNVAEFHDFSLSGGEGTTSVVVRRSFTGYVMPDGPEVGAVDCDTAPGGCEIVVGGNTGEYGNAAISFG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>