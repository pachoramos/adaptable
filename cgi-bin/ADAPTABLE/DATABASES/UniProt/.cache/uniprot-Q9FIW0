<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-02-22" modified="2024-10-02" version="89" xmlns="http://uniprot.org/uniprot">
  <accession>Q9FIW0</accession>
  <accession>Q8GWS2</accession>
  <name>ACIN1_ARATH</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Peptide ARACIN 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="7">Phytocytokine ARACIN1</fullName>
      <shortName evidence="6">Mature form of ARACIN1</shortName>
      <shortName evidence="6">mARACIN1</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="7">Precursor of Peptide ARACIN 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="6" type="primary">ARACIN1</name>
    <name evidence="9" type="ordered locus">At5g36925</name>
    <name evidence="10" type="ORF">MLF18.7</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="DNA Res." volume="5" first="379" last="391">
      <title>Structural analysis of Arabidopsis thaliana chromosome 5. VIII. Sequence features of the regions of 1,081,958 bp covered by seventeen physically assigned P1 and TAC clones.</title>
      <authorList>
        <person name="Asamizu E."/>
        <person name="Sato S."/>
        <person name="Kaneko T."/>
        <person name="Nakamura Y."/>
        <person name="Kotani H."/>
        <person name="Miyajima N."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="10048488"/>
      <dbReference type="DOI" id="10.1093/dnares/5.6.379"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Science" volume="296" first="141" last="145">
      <title>Functional annotation of a full-length Arabidopsis cDNA collection.</title>
      <authorList>
        <person name="Seki M."/>
        <person name="Narusaka M."/>
        <person name="Kamiya A."/>
        <person name="Ishida J."/>
        <person name="Satou M."/>
        <person name="Sakurai T."/>
        <person name="Nakajima M."/>
        <person name="Enju A."/>
        <person name="Akiyama K."/>
        <person name="Oono Y."/>
        <person name="Muramatsu M."/>
        <person name="Hayashizaki Y."/>
        <person name="Kawai J."/>
        <person name="Carninci P."/>
        <person name="Itoh M."/>
        <person name="Ishii Y."/>
        <person name="Arakawa T."/>
        <person name="Shibata K."/>
        <person name="Shinagawa A."/>
        <person name="Shinozaki K."/>
      </authorList>
      <dbReference type="PubMed" id="11910074"/>
      <dbReference type="DOI" id="10.1126/science.1071006"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] OF 13-76</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2003" name="Science" volume="302" first="842" last="846">
      <title>Empirical analysis of transcriptional activity in the Arabidopsis genome.</title>
      <authorList>
        <person name="Yamada K."/>
        <person name="Lim J."/>
        <person name="Dale J.M."/>
        <person name="Chen H."/>
        <person name="Shinn P."/>
        <person name="Palm C.J."/>
        <person name="Southwick A.M."/>
        <person name="Wu H.C."/>
        <person name="Kim C.J."/>
        <person name="Nguyen M."/>
        <person name="Pham P.K."/>
        <person name="Cheuk R.F."/>
        <person name="Karlin-Newmann G."/>
        <person name="Liu S.X."/>
        <person name="Lam B."/>
        <person name="Sakano H."/>
        <person name="Wu T."/>
        <person name="Yu G."/>
        <person name="Miranda M."/>
        <person name="Quach H.L."/>
        <person name="Tripp M."/>
        <person name="Chang C.H."/>
        <person name="Lee J.M."/>
        <person name="Toriumi M.J."/>
        <person name="Chan M.M."/>
        <person name="Tang C.C."/>
        <person name="Onodera C.S."/>
        <person name="Deng J.M."/>
        <person name="Akiyama K."/>
        <person name="Ansari Y."/>
        <person name="Arakawa T."/>
        <person name="Banh J."/>
        <person name="Banno F."/>
        <person name="Bowser L."/>
        <person name="Brooks S.Y."/>
        <person name="Carninci P."/>
        <person name="Chao Q."/>
        <person name="Choy N."/>
        <person name="Enju A."/>
        <person name="Goldsmith A.D."/>
        <person name="Gurjal M."/>
        <person name="Hansen N.F."/>
        <person name="Hayashizaki Y."/>
        <person name="Johnson-Hopson C."/>
        <person name="Hsuan V.W."/>
        <person name="Iida K."/>
        <person name="Karnes M."/>
        <person name="Khan S."/>
        <person name="Koesema E."/>
        <person name="Ishida J."/>
        <person name="Jiang P.X."/>
        <person name="Jones T."/>
        <person name="Kawai J."/>
        <person name="Kamiya A."/>
        <person name="Meyers C."/>
        <person name="Nakajima M."/>
        <person name="Narusaka M."/>
        <person name="Seki M."/>
        <person name="Sakurai T."/>
        <person name="Satou M."/>
        <person name="Tamse R."/>
        <person name="Vaysberg M."/>
        <person name="Wallender E.K."/>
        <person name="Wong C."/>
        <person name="Yamamura Y."/>
        <person name="Yuan S."/>
        <person name="Shinozaki K."/>
        <person name="Davis R.W."/>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
      </authorList>
      <dbReference type="PubMed" id="14593172"/>
      <dbReference type="DOI" id="10.1126/science.1088305"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] OF 14-76</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="Plant Cell Environ." volume="35" first="308" last="320">
      <title>A subcellular localization compendium of hydrogen peroxide-induced proteins.</title>
      <authorList>
        <person name="Inze A."/>
        <person name="Vanderauwera S."/>
        <person name="Hoeberichts F.A."/>
        <person name="Vandorpe M."/>
        <person name="Van Gaever T."/>
        <person name="Van Breusegem F."/>
      </authorList>
      <dbReference type="PubMed" id="21443605"/>
      <dbReference type="DOI" id="10.1111/j.1365-3040.2011.02323.x"/>
    </citation>
    <scope>INDUCTION BY HYDROGEN PEROXIDE</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2015" name="Plant Physiol." volume="167" first="1017" last="1029">
      <title>ARACINs, Brassicaceae-specific peptides exhibiting antifungal activities against necrotrophic pathogens in Arabidopsis.</title>
      <authorList>
        <person name="Neukermans J."/>
        <person name="Inze A."/>
        <person name="Mathys J."/>
        <person name="De Coninck B."/>
        <person name="van de Cotte B."/>
        <person name="Cammue B.P."/>
        <person name="Van Breusegem F."/>
      </authorList>
      <dbReference type="PubMed" id="25593351"/>
      <dbReference type="DOI" id="10.1104/pp.114.255505"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INDUCTION BY BOTRYTIS CINEREA; SALICYLIC ACID; JASMONIC ACID; HEAT AND COLD</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 5">Brassicaceae-specific phytocytokine (plant endogenous peptide released into the apoplast) perceived by MIK2 in a BAK1/SERK3 and SERK4 coreceptors-dependent manner, that modulates various physiological and antimicrobial processes including growth prevention and reactive oxygen species (ROS) response regulation (By similarity). Inhibits the fungal growth of Alternaria brassicicola, Sclerotinia sclerotiorum, Fusarium graminearum, yeast (Saccharomyces) and Botrytis cinerea, thus being an antimicrobial peptide (AMP) (PubMed:25593351). Promotes resistance to A.brassicicola and B.cinerea (PubMed:25593351).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with MIK2 (via extracellular leucine-rich repeat domain); this interaction triggers the formation of complex between MIK2 and the BAK1/SERK3 and SERK4 coreceptors, and subsequent BAK1 activation by phosphorylation.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cell membrane</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Secreted</location>
      <location evidence="1">Extracellular space</location>
      <location evidence="1">Apoplast</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="5">Endoplasmic reticulum</location>
    </subcellularLocation>
    <text evidence="1 5">Observed in a reticular pattern and a perinuclear ring (PubMed:25593351). The precursor of ARACIN1 accumulates at the plasma membrane and is proteolytically cleaved to release the ARACIN1 in the apoplasm (By similarity).</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Mainly expressed in young developing leaves, hydathodes, immature flowers and elongating pollen tubes.</text>
  </comment>
  <comment type="induction">
    <text evidence="4 5">Induced by increased levels of photorespiratory hydrogen peroxide H(2)O(2) (PubMed:21443605). Transiently up-regulated by cold stress but down-regulated by heat stress (PubMed:25593351). Induced by the necrotrophic fungus Botrytis cinerea in systemically infected leaves (PubMed:25593351). Accumulates in response to salicylic acid (SA, benzothiadiazole (BTH)) and jasmonic acid (MeJA) (PubMed:25593351).</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the serine rich endogenous peptide (SCOOP) phytocytokine family.</text>
  </comment>
  <comment type="sequence caution" evidence="7">
    <conflict type="erroneous initiation">
      <sequence resource="EMBL-CDS" id="BAC43263" version="1"/>
    </conflict>
    <text>Truncated N-terminus.</text>
  </comment>
  <dbReference type="EMBL" id="AB016877">
    <property type="protein sequence ID" value="BAB11634.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002688">
    <property type="protein sequence ID" value="AED94127.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK118668">
    <property type="protein sequence ID" value="BAC43263.1"/>
    <property type="status" value="ALT_INIT"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT004648">
    <property type="protein sequence ID" value="AAO42894.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_850917.2">
    <property type="nucleotide sequence ID" value="NM_180586.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9FIW0"/>
  <dbReference type="PaxDb" id="3702-AT5G36925.1"/>
  <dbReference type="EnsemblPlants" id="AT5G36925.1">
    <property type="protein sequence ID" value="AT5G36925.1"/>
    <property type="gene ID" value="AT5G36925"/>
  </dbReference>
  <dbReference type="GeneID" id="833661"/>
  <dbReference type="Gramene" id="AT5G36925.1">
    <property type="protein sequence ID" value="AT5G36925.1"/>
    <property type="gene ID" value="AT5G36925"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT5G36925"/>
  <dbReference type="Araport" id="AT5G36925"/>
  <dbReference type="TAIR" id="AT5G36925">
    <property type="gene designation" value="ARACIN1"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_2625348_0_0_1"/>
  <dbReference type="InParanoid" id="Q9FIW0"/>
  <dbReference type="OMA" id="NSEDVTH"/>
  <dbReference type="OrthoDB" id="674793at2759"/>
  <dbReference type="PRO" id="PR:Q9FIW0"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 5"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q9FIW0">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048046">
    <property type="term" value="C:apoplast"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005783">
    <property type="term" value="C:endoplasmic reticulum"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030275">
    <property type="term" value="F:LRR domain binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033612">
    <property type="term" value="F:receptor serine/threonine kinase binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009409">
    <property type="term" value="P:response to cold"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009620">
    <property type="term" value="P:response to fungus"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009408">
    <property type="term" value="P:response to heat"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042542">
    <property type="term" value="P:response to hydrogen peroxide"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009753">
    <property type="term" value="P:response to jasmonic acid"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009751">
    <property type="term" value="P:response to salicylic acid"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0052">Apoplast</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0256">Endoplasmic reticulum</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000457272" description="Removed in mature form" evidence="8">
    <location>
      <begin position="23"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000457273" description="Peptide ARACIN 1" evidence="8">
    <location>
      <begin position="36"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="56"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="SCOOP motif" evidence="8">
    <location>
      <begin position="36"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="SxS motif essential for MIK2 binding" evidence="1">
    <location>
      <begin position="36"/>
      <end position="38"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="B3H7I1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="21443605"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="25593351"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="25593351"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="25593351"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="9">
    <source>
      <dbReference type="Araport" id="AT5G36925"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="10">
    <source>
      <dbReference type="EMBL" id="BAB11634.1"/>
    </source>
  </evidence>
  <sequence length="76" mass="8213" checksum="8ED6238EFF567BBD" modified="2001-03-01" version="1" precursor="true">MAMKTSHVLLLCLMFVIGFVEARRSDTGPDISTPPSGSCGASIAEFNSSQILAKRAPPCRRPRLQNSEDVTHTTLP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>