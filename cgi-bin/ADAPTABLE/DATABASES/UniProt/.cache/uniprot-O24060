<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1999-07-15" modified="2024-07-24" version="85" xmlns="http://uniprot.org/uniprot">
  <accession>O24060</accession>
  <name>DAD1_MALDO</name>
  <protein>
    <recommendedName>
      <fullName>Dolichyl-diphosphooligosaccharide--protein glycosyltransferase subunit DAD1</fullName>
      <shortName>Oligosaccharyl transferase subunit DAD1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Defender against cell death 1</fullName>
      <shortName>DAD-1</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DAD1</name>
  </gene>
  <organism>
    <name type="scientific">Malus domestica</name>
    <name type="common">Apple</name>
    <name type="synonym">Pyrus malus</name>
    <dbReference type="NCBI Taxonomy" id="3750"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Rosales</taxon>
      <taxon>Rosaceae</taxon>
      <taxon>Amygdaloideae</taxon>
      <taxon>Maleae</taxon>
      <taxon>Malus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Plant Sci." volume="139" first="165" last="174">
      <title>Expression of a cDNA from apple encoding a homologue of DAD1, an inhibitor of programmed cell death.</title>
      <authorList>
        <person name="Dong Y.-H."/>
        <person name="Zhan X.-C."/>
        <person name="Kvarnheden A."/>
        <person name="Atkinson R.G."/>
        <person name="Morris B.A."/>
        <person name="Gardner R.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>cv. Granny Smith</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Subunit of the oligosaccharyl transferase (OST) complex that catalyzes the initial transfer of a defined glycan (Glc(3)Man(9)GlcNAc(2) in eukaryotes) from the lipid carrier dolichol-pyrophosphate to an asparagine residue within an Asn-X-Ser/Thr consensus motif in nascent polypeptide chains, the first step in protein N-glycosylation. N-glycosylation occurs cotranslationally and the complex associates with the Sec61 complex at the channel-forming translocon complex that mediates protein translocation across the endoplasmic reticulum (ER). All subunits are required for a maximal enzyme activity.</text>
  </comment>
  <comment type="pathway">
    <text>Protein modification; protein glycosylation.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Component of the oligosaccharyltransferase (OST) complex.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Endoplasmic reticulum membrane</location>
      <topology evidence="1">Multi-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the DAD/OST2 family.</text>
  </comment>
  <dbReference type="EMBL" id="U68560">
    <property type="protein sequence ID" value="AAB16804.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="T17016">
    <property type="entry name" value="T17016"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001280846.1">
    <property type="nucleotide sequence ID" value="NM_001293917.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O24060"/>
  <dbReference type="SMR" id="O24060"/>
  <dbReference type="EnsemblPlants" id="mRNA:MD05G0006500">
    <property type="protein sequence ID" value="mRNA:MD05G0006500"/>
    <property type="gene ID" value="MD05G0006500"/>
  </dbReference>
  <dbReference type="GeneID" id="103436627"/>
  <dbReference type="Gramene" id="mRNA:MD05G0006500">
    <property type="protein sequence ID" value="mRNA:MD05G0006500"/>
    <property type="gene ID" value="MD05G0006500"/>
  </dbReference>
  <dbReference type="KEGG" id="mdm:103436627"/>
  <dbReference type="OrthoDB" id="206600at2759"/>
  <dbReference type="UniPathway" id="UPA00378"/>
  <dbReference type="GO" id="GO:0008250">
    <property type="term" value="C:oligosaccharyltransferase complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006487">
    <property type="term" value="P:protein N-linked glycosylation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003038">
    <property type="entry name" value="DAD/Ost2"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10705">
    <property type="entry name" value="DOLICHYL-DIPHOSPHOOLIGOSACCHARIDE--PROTEIN GLYCOSYLTRANSFERASE SUBUNIT DAD1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10705:SF0">
    <property type="entry name" value="DOLICHYL-DIPHOSPHOOLIGOSACCHARIDE--PROTEIN GLYCOSYLTRANSFERASE SUBUNIT DAD1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02109">
    <property type="entry name" value="DAD"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF005588">
    <property type="entry name" value="DAD"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-0256">Endoplasmic reticulum</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <feature type="chain" id="PRO_0000124026" description="Dolichyl-diphosphooligosaccharide--protein glycosyltransferase subunit DAD1">
    <location>
      <begin position="1"/>
      <end position="119"/>
    </location>
  </feature>
  <feature type="topological domain" description="Cytoplasmic" evidence="3">
    <location>
      <begin position="1"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="3">
    <location>
      <begin position="36"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="topological domain" description="Lumenal" evidence="3">
    <location>
      <begin position="57"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="3">
    <location>
      <begin position="60"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="topological domain" description="Cytoplasmic" evidence="3">
    <location>
      <begin position="81"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="3">
    <location>
      <begin position="99"/>
      <end position="119"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P46964"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="119" mass="12840" checksum="EA6B12C74E1A4227" modified="1998-01-01" version="1">MGKASHSSTAQDAVALFDSLRSAYSATPTTLKIIDLYIGFAVSTALIQVVYMAIVGSFPFNSFLSGVLSCIGTAVLAVCLRIQVNKENKEFKDLAPERAFADFVLCNLVLHMVIMNFLG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>