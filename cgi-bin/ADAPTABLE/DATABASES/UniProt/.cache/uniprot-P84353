<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-15" modified="2021-06-02" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>P84353</accession>
  <name>PVK2_SARBU</name>
  <protein>
    <recommendedName>
      <fullName>Periviscerokinin-2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Neobu-PVK-2</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Sarcophaga bullata</name>
    <name type="common">Grey flesh fly</name>
    <name type="synonym">Neobellieria bullata</name>
    <dbReference type="NCBI Taxonomy" id="7385"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Oestroidea</taxon>
      <taxon>Sarcophagidae</taxon>
      <taxon>Sarcophaga</taxon>
      <taxon>Neobellieria</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2003" name="Peptides" volume="24" first="1487" last="1491">
      <title>Mass spectrometric analysis of putative capa-gene products in Musca domestica and Neobellieria bullata.</title>
      <authorList>
        <person name="Predel R."/>
        <person name="Russell W.K."/>
        <person name="Tichy S.E."/>
        <person name="Russell D.H."/>
        <person name="Nachman R.J."/>
      </authorList>
      <dbReference type="PubMed" id="14706527"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2003.06.009"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-9</scope>
    <source>
      <tissue evidence="1">Ganglion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Mediates visceral muscle contractile activity (myotropic activity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Dorsal ganglionic sheath of fused ventral nerve cord.</text>
  </comment>
  <comment type="mass spectrometry" mass="1000.4" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the periviscerokinin family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044278" description="Periviscerokinin-2">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="1">
    <location>
      <position position="3"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="1">
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="14706527"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="14706527"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="9" mass="1001" checksum="2C31177B42C72728" modified="2005-02-15" version="1">AGLLVYPRL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>