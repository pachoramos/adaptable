<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-01-24" version="86" xmlns="http://uniprot.org/uniprot">
  <accession>P81008</accession>
  <name>DEF1_MAIZE</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Gamma-zeathionin-1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Zea mays</name>
    <name type="common">Maize</name>
    <dbReference type="NCBI Taxonomy" id="4577"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>Liliopsida</taxon>
      <taxon>Poales</taxon>
      <taxon>Poaceae</taxon>
      <taxon>PACMAD clade</taxon>
      <taxon>Panicoideae</taxon>
      <taxon>Andropogonodae</taxon>
      <taxon>Andropogoneae</taxon>
      <taxon>Tripsacinae</taxon>
      <taxon>Zea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Protein Pept. Lett." volume="3" first="267" last="274">
      <title>Complete amino acid sequences of two gamma-thionins from maize (Zea mays L.) seeds.</title>
      <authorList>
        <person name="Castro M.S."/>
        <person name="Fontes W."/>
        <person name="Morhy L."/>
        <person name="Bloch C. Jr."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <comment type="similarity">
    <text evidence="2">Belongs to the DEFL family.</text>
  </comment>
  <comment type="caution">
    <text evidence="2">Was initially thought (Ref.1) to be a thionin.</text>
  </comment>
  <dbReference type="PIR" id="A58319">
    <property type="entry name" value="A58319"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P81008"/>
  <dbReference type="SMR" id="P81008"/>
  <dbReference type="STRING" id="4577.P81008"/>
  <dbReference type="PaxDb" id="4577-GRMZM2G368890_P01"/>
  <dbReference type="MaizeGDB" id="139775"/>
  <dbReference type="eggNOG" id="ENOG502S7BC">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="P81008"/>
  <dbReference type="Proteomes" id="UP000007305">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008176">
    <property type="entry name" value="Defensin_plant"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00304">
    <property type="entry name" value="Gamma-thionin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00288">
    <property type="entry name" value="PUROTHIONIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00940">
    <property type="entry name" value="GAMMA_THIONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000074240" description="Defensin-like protein 1">
    <location>
      <begin position="1"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="3"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="14"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="20"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="43"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="47" mass="5199" checksum="0F3A74A58C3BBDFE" modified="1997-11-01" version="1">RVCRRRSAGFKGVCMSDHNCAQVCLQEGYGGGNCDGIMRQCKCIRQC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>