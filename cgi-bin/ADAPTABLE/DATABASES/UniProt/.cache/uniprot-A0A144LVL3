<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-02-15" modified="2023-11-08" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>A0A144LVL3</accession>
  <name>CTCN2_ECHES</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Centrocin 2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">EeCentrocin 2</fullName>
    </alternativeName>
    <component>
      <recommendedName>
        <fullName evidence="3">Centrocin 2, heavy chain</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="3">Centrocin 2, light chain</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism evidence="5">
    <name type="scientific">Echinus esculentus</name>
    <name type="common">Sea urchin</name>
    <dbReference type="NCBI Taxonomy" id="7648"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Echinodermata</taxon>
      <taxon>Eleutherozoa</taxon>
      <taxon>Echinozoa</taxon>
      <taxon>Echinoidea</taxon>
      <taxon>Euechinoidea</taxon>
      <taxon>Echinacea</taxon>
      <taxon>Camarodonta</taxon>
      <taxon>Echinidea</taxon>
      <taxon>Echinidae</taxon>
      <taxon>Echinus</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2016" name="PLoS ONE" volume="11" first="E0151820" last="E0151820">
      <title>Novel Antimicrobial Peptides EeCentrocins 1, 2 and EeStrongylocin 2 from the Edible Sea Urchin Echinus esculentus Have 6-Br-Trp Post-Translational Modifications.</title>
      <authorList>
        <person name="Solstad R.G."/>
        <person name="Li C."/>
        <person name="Isaksson J."/>
        <person name="Johansen J."/>
        <person name="Svenson J."/>
        <person name="Stensvag K."/>
        <person name="Haug T."/>
      </authorList>
      <dbReference type="PubMed" id="27007817"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0151820"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 51-82 AND 105-117</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>BROMINATION AT TRP-51 AND TRP-59</scope>
    <scope>AMIDATION AT HIS-119</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-107</scope>
    <source>
      <tissue evidence="3">Coelomocyte</tissue>
    </source>
  </reference>
  <comment type="function">
    <molecule>Centrocin 2, heavy chain</molecule>
    <text evidence="2">Has antimicrobial activity against Gram-negative bacteria, Gram-positive bacteria and against fungi with minimum inhibitory concentration (MIC) between 0.78 uM and 50 uM. Shows little hemolytic activity at concentrations up to 12.5 uM but &gt;50% lysis at 100 uM.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Heterodimer of a light and a heavy chain, probably disulfide-linked.</text>
  </comment>
  <comment type="mass spectrometry" mass="5019.37" method="Electrospray" evidence="2">
    <text>The measured ranges are 51-82, 107-119.</text>
  </comment>
  <dbReference type="EMBL" id="KR494263">
    <property type="protein sequence ID" value="AMT92375.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A144LVL3"/>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0102">Bromination</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000438842" evidence="4">
    <location>
      <begin position="21"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000438843" description="Centrocin 2, heavy chain" evidence="2">
    <location>
      <begin position="51"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000438844" evidence="4">
    <location>
      <begin position="83"/>
      <end position="106"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000438845" description="Centrocin 2, light chain" evidence="2">
    <location>
      <begin position="107"/>
      <end position="119"/>
    </location>
  </feature>
  <feature type="modified residue" description="6'-bromotryptophan" evidence="2">
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="modified residue" description="6'-bromotryptophan" evidence="2">
    <location>
      <position position="59"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="2">
    <location>
      <position position="107"/>
    </location>
  </feature>
  <feature type="modified residue" description="Histidine amide" evidence="2">
    <location>
      <position position="119"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="77"/>
      <end position="112"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27007817"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="27007817"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="27007817"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="5">
    <source>
      <dbReference type="EMBL" id="AMT92375.1"/>
    </source>
  </evidence>
  <sequence length="121" mass="13151" checksum="E386FC153EFE6838" modified="2016-06-08" version="1" precursor="true">MMIKIAVVLCAVMATSMVFANDVKEQELADLLDLLISEEVSSPDDAVAESWGHKLRSSWNKVKHAVKKGAGYASGACRVLGHSPQEARAKVLEAFPEMKESDLDEEQVGKYCAVAHAIHGR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>