<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-05-23" modified="2022-05-25" version="6" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKU2</accession>
  <name>NEMS_AGRIP</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Myosuppressin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Agrotis ipsilon</name>
    <name type="common">Black cutworm moth</name>
    <dbReference type="NCBI Taxonomy" id="56364"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Noctuoidea</taxon>
      <taxon>Noctuidae</taxon>
      <taxon>Noctuinae</taxon>
      <taxon>Noctuini</taxon>
      <taxon>Agrotis</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2018" name="J. Proteome Res." volume="17" first="1397" last="1414">
      <title>Mating-induced differential peptidomics of neuropeptides and protein hormones in Agrotis ipsilon moths.</title>
      <authorList>
        <person name="Diesner M."/>
        <person name="Gallot A."/>
        <person name="Binz H."/>
        <person name="Gaertner C."/>
        <person name="Vitecek S."/>
        <person name="Kahnt J."/>
        <person name="Schachtner J."/>
        <person name="Jacquin-Joly E."/>
        <person name="Gadenne C."/>
      </authorList>
      <dbReference type="PubMed" id="29466015"/>
      <dbReference type="DOI" id="10.1021/acs.jproteome.7b00779"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 83-92</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT PHE-92</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-83</scope>
  </reference>
  <comment type="function">
    <text evidence="4">Myoinhibiting neuropeptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed in corpora cardiaca (CC), corpora allata (CA), antennal lobe (AL) and gnathal ganglion (GNG) (at protein level). In its non-pyroglutamate form, expression in GNG detected in all animals, in AL, CC and in CA in most animals (at protein level). In its pyroglutamate form, expression in CC, CA and GNG detected in all animals, in AL in some animals (at protein level).</text>
  </comment>
  <comment type="mass spectrometry" mass="1246.66" method="MALDI" evidence="2">
    <text>Non-pyroglutamate form.</text>
  </comment>
  <comment type="mass spectrometry" mass="1229.65" method="MALDI" evidence="2">
    <text>Pyroglutamate form.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the myosuppressin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HKU2"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444239" evidence="4">
    <location>
      <begin position="25"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000444240" description="Myosuppressin" evidence="2">
    <location>
      <begin position="83"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444241" evidence="4">
    <location>
      <position position="96"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid; partial" evidence="2">
    <location>
      <position position="83"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="2">
    <location>
      <position position="92"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="96" mass="10503" checksum="FC0F68E4F927F48A" modified="2018-05-23" version="1" precursor="true">MALGNGYYCAVVCVVLACASVVLCAPAQLCAGAADDDPRAARFCQALNTFLELYAEAAGEQVPEYQALVRDYPQLLDTGMKRQDVVHSFLRFGRRR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>