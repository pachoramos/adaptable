<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2021-02-10" version="42" xmlns="http://uniprot.org/uniprot">
  <accession>P82077</accession>
  <name>CDN5_RANCA</name>
  <protein>
    <recommendedName>
      <fullName>Caeridin-5</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ranoidea caerulea</name>
    <name type="common">Green tree frog</name>
    <name type="synonym">Litoria caerulea</name>
    <dbReference type="NCBI Taxonomy" id="30344"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="J. Chem. Soc. Perkin Trans. I" volume="1" first="573" last="576">
      <title>Peptides from Australian frogs. Structures of the caeridins from Litoria caerulea.</title>
      <authorList>
        <person name="Waugh R.J."/>
        <person name="Stone D.J.M."/>
        <person name="Bowie J.H."/>
        <person name="Wallace J.C."/>
        <person name="Tyler M.J."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT LEU-15</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Parotoid gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Caeridins show neither neuropeptide activity nor antibiotic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin parotoid and/or rostral glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1355.0" method="FAB" evidence="1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043764" description="Caeridin-5">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source ref="1"/>
  </evidence>
  <sequence length="15" mass="1357" checksum="9FF1A646BD550CAE" modified="2000-05-01" version="1">GLLGMVGSLLGGLGL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>