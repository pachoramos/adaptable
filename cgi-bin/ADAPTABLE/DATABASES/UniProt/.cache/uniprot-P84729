<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-06-27" modified="2024-11-27" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>P84729</accession>
  <name>BAS1_PINST</name>
  <protein>
    <recommendedName>
      <fullName>Putative 2-Cys peroxiredoxin BAS1</fullName>
      <ecNumber evidence="2">1.11.1.24</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>PS13</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Thiol-specific antioxidant protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="5">Thioredoxin-dependent peroxiredoxin BAS1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Pinus strobus</name>
    <name type="common">Eastern white pine</name>
    <dbReference type="NCBI Taxonomy" id="3348"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Pinopsida</taxon>
      <taxon>Pinidae</taxon>
      <taxon>Conifers I</taxon>
      <taxon>Pinales</taxon>
      <taxon>Pinaceae</taxon>
      <taxon>Pinus</taxon>
      <taxon>Pinus subgen. Strobus</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2006" name="Mol. Plant Microbe Interact." volume="19" first="150" last="160">
      <title>Proteomic comparison of needles from blister rust-resistant and susceptible Pinus strobus seedlings reveals upregulation of putative disease resistance proteins.</title>
      <authorList>
        <person name="Smith J.A."/>
        <person name="Blanchette R.A."/>
        <person name="Burnes T.A."/>
        <person name="Jacobs J.J."/>
        <person name="Higgins L."/>
        <person name="Witthuhn B.A."/>
        <person name="David A.J."/>
        <person name="Gillman J.H."/>
      </authorList>
      <dbReference type="PubMed" id="16529377"/>
      <dbReference type="DOI" id="10.1094/mpmi-19-0150"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue evidence="3">Leaf</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Thiol-specific peroxidase that catalyzes the reduction of hydrogen peroxide and organic hydroperoxides to water and alcohols, respectively. Plays a role in cell protection against oxidative stress by detoxifying peroxides. May be an antioxidant enzyme particularly in the developing shoot and photosynthesizing leaf.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>a hydroperoxide + [thioredoxin]-dithiol = an alcohol + [thioredoxin]-disulfide + H2O</text>
      <dbReference type="Rhea" id="RHEA:62620"/>
      <dbReference type="Rhea" id="RHEA-COMP:10698"/>
      <dbReference type="Rhea" id="RHEA-COMP:10700"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:29950"/>
      <dbReference type="ChEBI" id="CHEBI:30879"/>
      <dbReference type="ChEBI" id="CHEBI:35924"/>
      <dbReference type="ChEBI" id="CHEBI:50058"/>
      <dbReference type="EC" id="1.11.1.24"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer; disulfide-linked, upon oxidation.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Plastid</location>
      <location evidence="2">Chloroplast</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text evidence="3">On the 2D-gel the determined pI of this protein is: 5.7, its MW is: 22.8 kDa.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">The active site is a conserved redox-active cysteine residue, the peroxidatic cysteine (C(P)), which makes the nucleophilic attack on the peroxide substrate. The peroxide oxidizes the C(P)-SH to cysteine sulfenic acid (C(P)-SOH), which then reacts with another cysteine residue, the resolving cysteine (C(R)), to form a disulfide bridge. The disulfide is subsequently reduced by an appropriate electron donor to complete the catalytic cycle. In this typical 2-Cys peroxiredoxin, C(R) is provided by the other dimeric subunit to form an intersubunit disulfide. The disulfide is subsequently reduced by thioredoxin.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the peroxiredoxin family. AhpC/Prx1 subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="3">The order of the peptides shown is unknown.</text>
  </comment>
  <dbReference type="EC" id="1.11.1.24" evidence="2"/>
  <dbReference type="AlphaFoldDB" id="P84729"/>
  <dbReference type="GO" id="GO:0009507">
    <property type="term" value="C:chloroplast"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0140824">
    <property type="term" value="F:thioredoxin-dependent peroxiredoxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0049">Antioxidant</keyword>
  <keyword id="KW-0150">Chloroplast</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-0575">Peroxidase</keyword>
  <keyword id="KW-0934">Plastid</keyword>
  <keyword id="KW-0676">Redox-active center</keyword>
  <feature type="chain" id="PRO_0000240611" description="Putative 2-Cys peroxiredoxin BAS1">
    <location>
      <begin position="1" status="less than"/>
      <end position="56" status="greater than"/>
    </location>
  </feature>
  <feature type="non-consecutive residues" evidence="4">
    <location>
      <begin position="18"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="non-consecutive residues" evidence="4">
    <location>
      <begin position="34"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="non-consecutive residues" evidence="4">
    <location>
      <begin position="47"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="4">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="4">
    <location>
      <position position="56"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q06830"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q96291"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="16529377"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="16529377"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="56" mass="5894" checksum="FCC2ECB15318133E" modified="2006-06-27" version="1" fragment="multiple">GLFIIDKEGVIQHSTINNEGVIQHSTINNLAIGRFGVLLADQGLALRSIPNGPSAL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>