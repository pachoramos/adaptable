<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-03-07" modified="2023-02-22" version="30" xmlns="http://uniprot.org/uniprot">
  <accession>P84789</accession>
  <name>PHIG1_PHIGI</name>
  <protein>
    <recommendedName>
      <fullName>Philibertain g 1</fullName>
      <ecNumber evidence="2">3.4.22.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Philibertain g I</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Philibertia gilliesii</name>
    <name type="common">Milkweed</name>
    <name type="synonym">Sarcostemma gilliesii</name>
    <dbReference type="NCBI Taxonomy" id="126767"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>asterids</taxon>
      <taxon>lamiids</taxon>
      <taxon>Gentianales</taxon>
      <taxon>Apocynaceae</taxon>
      <taxon>Asclepiadoideae</taxon>
      <taxon>Asclepiadeae</taxon>
      <taxon>MOOG clade</taxon>
      <taxon>Oxypetalinae</taxon>
      <taxon>Philibertia</taxon>
    </lineage>
  </organism>
  <reference evidence="9" key="1">
    <citation type="journal article" date="2005" name="Protein J." volume="24" first="445" last="453">
      <title>Philibertain g I, the most basic cysteine endopeptidase purified from the latex of Philibertia gilliesii Hook. et Arn. (Apocynaceae).</title>
      <authorList>
        <person name="Sequeiros C."/>
        <person name="Torres M.J."/>
        <person name="Trejo S.A."/>
        <person name="Esteves J.L."/>
        <person name="Natalucci C.L."/>
        <person name="Lopez L.M.I."/>
      </authorList>
      <dbReference type="PubMed" id="16328737"/>
      <dbReference type="DOI" id="10.1007/s10930-005-7640-0"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="7">Fruit</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Thiol protease.</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="7">Strongly inhibited by the cysteine protease inhibitor E64 (L-trans-epoxysuccinyl-leucylamide-(4-guanido)-butane). Not inhibited by the serine protease inhibitor PMSF, the aspartic protease inhibitor pepstatin A, or by the metal ion chelator 1,10-phenanthroline.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <kinetics>
      <KM evidence="7">0.15 mM for PFLNA</KM>
    </kinetics>
    <phDependence>
      <text evidence="7">Optimum pH for casein is 7.6 and philibertain g 1 retains more than 80% of maximum activity between pH 6.7 and pH 8.7, and 50% of maximum activity between pH 6.1 and pH 9.8. Optimum pH for PFLNA is 6.2-7.2 and philibertain g I retains more than 80% of maximum activity between pH 5.8 and pH 7.8, and 50% of maximum activity between pH 5.2 and pH 8.6.</text>
    </phDependence>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer.</text>
  </comment>
  <comment type="mass spectrometry" mass="23530.0" method="MALDI" evidence="7"/>
  <comment type="similarity">
    <text evidence="4 5 6">Belongs to the peptidase C1 family.</text>
  </comment>
  <dbReference type="EC" id="3.4.22.-" evidence="2"/>
  <dbReference type="AlphaFoldDB" id="P84789"/>
  <dbReference type="GO" id="GO:0008234">
    <property type="term" value="F:cysteine-type peptidase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006508">
    <property type="term" value="P:proteolysis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.90.70.10">
    <property type="entry name" value="Cysteine proteinases"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038765">
    <property type="entry name" value="Papain-like_cys_pep_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000668">
    <property type="entry name" value="Peptidase_C1A_C"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00112">
    <property type="entry name" value="Peptidase_C1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54001">
    <property type="entry name" value="Cysteine proteinases"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0645">Protease</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0788">Thiol protease</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000227534" description="Philibertain g 1">
    <location>
      <begin status="unknown"/>
      <end position="23" status="greater than"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="22"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="8">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P10056"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P80884"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000255" key="4">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10088"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="5">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10089"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="6">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10090"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="16328737"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="16328737"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="9"/>
  <sequence length="23" mass="2518" checksum="CFCA161F65E3D98A" modified="2006-03-07" version="1" precursor="true" fragment="single">LPASVDWRKEGAVLPIRHQGQCG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>