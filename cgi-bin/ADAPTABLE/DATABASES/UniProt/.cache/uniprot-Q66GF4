<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-15" modified="2024-11-27" version="104" xmlns="http://uniprot.org/uniprot">
  <accession>Q66GF4</accession>
  <name>DTD_YERPS</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">D-aminoacyl-tRNA deacylase</fullName>
      <shortName evidence="1">DTD</shortName>
      <ecNumber evidence="1">3.1.1.96</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Gly-tRNA(Ala) deacylase</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">dtd</name>
    <name type="ordered locus">YPTB0028</name>
  </gene>
  <organism>
    <name type="scientific">Yersinia pseudotuberculosis serotype I (strain IP32953)</name>
    <dbReference type="NCBI Taxonomy" id="273123"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Yersiniaceae</taxon>
      <taxon>Yersinia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Proc. Natl. Acad. Sci. U.S.A." volume="101" first="13826" last="13831">
      <title>Insights into the evolution of Yersinia pestis through whole-genome comparison with Yersinia pseudotuberculosis.</title>
      <authorList>
        <person name="Chain P.S.G."/>
        <person name="Carniel E."/>
        <person name="Larimer F.W."/>
        <person name="Lamerdin J."/>
        <person name="Stoutland P.O."/>
        <person name="Regala W.M."/>
        <person name="Georgescu A.M."/>
        <person name="Vergez L.M."/>
        <person name="Land M.L."/>
        <person name="Motin V.L."/>
        <person name="Brubaker R.R."/>
        <person name="Fowler J."/>
        <person name="Hinnebusch J."/>
        <person name="Marceau M."/>
        <person name="Medigue C."/>
        <person name="Simonet M."/>
        <person name="Chenal-Francisque V."/>
        <person name="Souza B."/>
        <person name="Dacheux D."/>
        <person name="Elliott J.M."/>
        <person name="Derbise A."/>
        <person name="Hauser L.J."/>
        <person name="Garcia E."/>
      </authorList>
      <dbReference type="PubMed" id="15358858"/>
      <dbReference type="DOI" id="10.1073/pnas.0404012101"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>IP32953</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">An aminoacyl-tRNA editing enzyme that deacylates mischarged D-aminoacyl-tRNAs. Also deacylates mischarged glycyl-tRNA(Ala), protecting cells against glycine mischarging by AlaRS. Acts via tRNA-based rather than protein-based catalysis; rejects L-amino acids rather than detecting D-amino acids in the active site. By recycling D-aminoacyl-tRNA to D-amino acids and free tRNA molecules, this enzyme counteracts the toxicity associated with the formation of D-aminoacyl-tRNA entities in vivo and helps enforce protein L-homochirality.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>glycyl-tRNA(Ala) + H2O = tRNA(Ala) + glycine + H(+)</text>
      <dbReference type="Rhea" id="RHEA:53744"/>
      <dbReference type="Rhea" id="RHEA-COMP:9657"/>
      <dbReference type="Rhea" id="RHEA-COMP:13640"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:57305"/>
      <dbReference type="ChEBI" id="CHEBI:78442"/>
      <dbReference type="ChEBI" id="CHEBI:78522"/>
      <dbReference type="EC" id="3.1.1.96"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>a D-aminoacyl-tRNA + H2O = a tRNA + a D-alpha-amino acid + H(+)</text>
      <dbReference type="Rhea" id="RHEA:13953"/>
      <dbReference type="Rhea" id="RHEA-COMP:10123"/>
      <dbReference type="Rhea" id="RHEA-COMP:10124"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:59871"/>
      <dbReference type="ChEBI" id="CHEBI:78442"/>
      <dbReference type="ChEBI" id="CHEBI:79333"/>
      <dbReference type="EC" id="3.1.1.96"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="1">A Gly-cisPro motif from one monomer fits into the active site of the other monomer to allow specific chiral rejection of L-amino acids.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the DTD family.</text>
  </comment>
  <dbReference type="EC" id="3.1.1.96" evidence="1"/>
  <dbReference type="EMBL" id="BX936398">
    <property type="protein sequence ID" value="CAH19268.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_002209009.1">
    <property type="nucleotide sequence ID" value="NZ_CP009712.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q66GF4"/>
  <dbReference type="SMR" id="Q66GF4"/>
  <dbReference type="GeneID" id="66843576"/>
  <dbReference type="KEGG" id="ypo:BZ17_2567"/>
  <dbReference type="KEGG" id="yps:YPTB0028"/>
  <dbReference type="PATRIC" id="fig|273123.14.peg.2692"/>
  <dbReference type="Proteomes" id="UP000001011">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051500">
    <property type="term" value="F:D-tyrosyl-tRNA(Tyr) deacylase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0106026">
    <property type="term" value="F:Gly-tRNA(Ala) hydrolase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043908">
    <property type="term" value="F:Ser(Gly)-tRNA(Ala) hydrolase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000049">
    <property type="term" value="F:tRNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019478">
    <property type="term" value="P:D-amino acid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="CDD" id="cd00563">
    <property type="entry name" value="Dtyr_deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.50.80.10:FF:000001">
    <property type="entry name" value="D-aminoacyl-tRNA deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.50.80.10">
    <property type="entry name" value="D-tyrosyl-tRNA(Tyr) deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00518">
    <property type="entry name" value="Deacylase_Dtd"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003732">
    <property type="entry name" value="Daa-tRNA_deacyls_DTD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023509">
    <property type="entry name" value="DTD-like_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00256">
    <property type="entry name" value="D-aminoacyl-tRNA deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10472:SF5">
    <property type="entry name" value="D-AMINOACYL-TRNA DEACYLASE 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10472">
    <property type="entry name" value="D-TYROSYL-TRNA TYR DEACYLASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02580">
    <property type="entry name" value="Tyr_Deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF69500">
    <property type="entry name" value="DTD-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0820">tRNA-binding</keyword>
  <feature type="chain" id="PRO_0000164624" description="D-aminoacyl-tRNA deacylase">
    <location>
      <begin position="1"/>
      <end position="145"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Gly-cisPro motif, important for rejection of L-amino acids" evidence="1">
    <location>
      <begin position="137"/>
      <end position="138"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00518"/>
    </source>
  </evidence>
  <sequence length="145" mass="15863" checksum="E378394DC0B2C89D" modified="2004-10-11" version="1">MIALIQRALSASVVVEGNIVGEIGPGLLVLLGVEQGDTEQKAQRLCERVLGYRIFSDENDKMNLNVQQAGGSVLVVSQFTLVADTQKGMRPSFSRGAIPQEADRLYQYFVAQCRERGVKTETGLFAADMKVSLVNDGPVTFWLQV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>