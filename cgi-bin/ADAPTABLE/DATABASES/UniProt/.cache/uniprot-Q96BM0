<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-01-17" modified="2024-10-02" version="144" xmlns="http://uniprot.org/uniprot">
  <accession>Q96BM0</accession>
  <name>I27L1_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="7">Interferon alpha-inducible protein 27-like protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Interferon-stimulated gene 12c protein</fullName>
      <shortName evidence="5">ISG12(c)</shortName>
      <shortName evidence="6">ISG12C</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="8" type="primary">IFI27L1</name>
    <name evidence="8" type="synonym">FAM14B</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Duodenum</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="BMC Genomics" volume="5" first="8" last="8">
      <title>Identification of a novel gene family that includes the interferon-inducible human genes 6-16 and ISG12.</title>
      <authorList>
        <person name="Parker N."/>
        <person name="Porter A.C.G."/>
      </authorList>
      <dbReference type="PubMed" id="14728724"/>
      <dbReference type="DOI" id="10.1186/1471-2164-5-8"/>
    </citation>
    <scope>IDENTIFICATION</scope>
    <scope>INDUCTION BY INTERFERON</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2017" name="Biol. Cell" volume="109" first="94" last="112">
      <title>Apoptotic properties of the type 1 interferon induced family of human mitochondrial membrane ISG12 proteins.</title>
      <authorList>
        <person name="Gytz H."/>
        <person name="Hansen M.F."/>
        <person name="Skovbjerg S."/>
        <person name="Kristensen A.C."/>
        <person name="Hoerlyck S."/>
        <person name="Jensen M.B."/>
        <person name="Fredborg M."/>
        <person name="Markert L.D."/>
        <person name="McMillan N.A."/>
        <person name="Christensen E.I."/>
        <person name="Martensen P.M."/>
      </authorList>
      <dbReference type="PubMed" id="27673746"/>
      <dbReference type="DOI" id="10.1111/boc.201600034"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2017" name="Virus Res." volume="227" first="231" last="239">
      <title>ISG12a inhibits HCV replication and potentiates the anti-HCV activity of IFN-alpha through activation of the Jak/STAT signaling pathway independent of autophagy and apoptosis.</title>
      <authorList>
        <person name="Chen Y."/>
        <person name="Jiao B."/>
        <person name="Yao M."/>
        <person name="Shi X."/>
        <person name="Zheng Z."/>
        <person name="Li S."/>
        <person name="Chen L."/>
      </authorList>
      <dbReference type="PubMed" id="27777077"/>
      <dbReference type="DOI" id="10.1016/j.virusres.2016.10.013"/>
    </citation>
    <scope>INDUCTION BY INTERFERON</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="Nat. Methods" volume="9" first="834" last="839">
      <title>Facile backbone structure determination of human membrane proteins by NMR spectroscopy.</title>
      <authorList>
        <person name="Klammt C."/>
        <person name="Maslennikov I."/>
        <person name="Bayrhuber M."/>
        <person name="Eichmann C."/>
        <person name="Vajpai N."/>
        <person name="Chiu E.J."/>
        <person name="Blain K.Y."/>
        <person name="Esquivies L."/>
        <person name="Kwon J.H."/>
        <person name="Balana B."/>
        <person name="Pieper U."/>
        <person name="Sali A."/>
        <person name="Slesinger P.A."/>
        <person name="Kwiatkowski W."/>
        <person name="Riek R."/>
        <person name="Choe S."/>
      </authorList>
      <dbReference type="PubMed" id="22609626"/>
      <dbReference type="DOI" id="10.1038/nmeth.2033"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Plays a role in the apoptotic process and has a pro-apoptotic activity.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-17589287">
      <id>Q96BM0</id>
    </interactant>
    <interactant intactId="EBI-17589229">
      <id>Q6NTF9-3</id>
      <label>RHBDD2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Membrane</location>
      <topology evidence="1">Multi-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text evidence="2 4">Not up-regulated by type-I interferon.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the IFI6/IFI27 family.</text>
  </comment>
  <dbReference type="EMBL" id="BC015423">
    <property type="protein sequence ID" value="AAH15423.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BN000225">
    <property type="protein sequence ID" value="CAE00392.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS9919.1"/>
  <dbReference type="RefSeq" id="NP_660292.1">
    <property type="nucleotide sequence ID" value="NM_145249.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_996832.1">
    <property type="nucleotide sequence ID" value="NM_206949.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_011534707.1">
    <property type="nucleotide sequence ID" value="XM_011536405.2"/>
  </dbReference>
  <dbReference type="PDB" id="2LOQ">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-104"/>
  </dbReference>
  <dbReference type="PDBsum" id="2LOQ"/>
  <dbReference type="AlphaFoldDB" id="Q96BM0"/>
  <dbReference type="BMRB" id="Q96BM0"/>
  <dbReference type="SMR" id="Q96BM0"/>
  <dbReference type="BioGRID" id="125774">
    <property type="interactions" value="64"/>
  </dbReference>
  <dbReference type="IntAct" id="Q96BM0">
    <property type="interactions" value="64"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000376824"/>
  <dbReference type="iPTMnet" id="Q96BM0"/>
  <dbReference type="PhosphoSitePlus" id="Q96BM0"/>
  <dbReference type="BioMuta" id="IFI27L1"/>
  <dbReference type="DMDM" id="27805467"/>
  <dbReference type="PaxDb" id="9606-ENSP00000451851"/>
  <dbReference type="PeptideAtlas" id="Q96BM0"/>
  <dbReference type="DNASU" id="122509"/>
  <dbReference type="Ensembl" id="ENST00000393115.7">
    <property type="protein sequence ID" value="ENSP00000376824.3"/>
    <property type="gene ID" value="ENSG00000165948.11"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000555523.6">
    <property type="protein sequence ID" value="ENSP00000451851.1"/>
    <property type="gene ID" value="ENSG00000165948.11"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000611681.3">
    <property type="protein sequence ID" value="ENSP00000480235.1"/>
    <property type="gene ID" value="ENSG00000276880.3"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000629591.2">
    <property type="protein sequence ID" value="ENSP00000485930.1"/>
    <property type="gene ID" value="ENSG00000276880.3"/>
  </dbReference>
  <dbReference type="GeneID" id="122509"/>
  <dbReference type="KEGG" id="hsa:122509"/>
  <dbReference type="MANE-Select" id="ENST00000555523.6">
    <property type="protein sequence ID" value="ENSP00000451851.1"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_206949.3"/>
    <property type="RefSeq protein sequence ID" value="NP_996832.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc001yck.4">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:19754"/>
  <dbReference type="CTD" id="122509"/>
  <dbReference type="DisGeNET" id="122509"/>
  <dbReference type="GeneCards" id="IFI27L1"/>
  <dbReference type="HGNC" id="HGNC:19754">
    <property type="gene designation" value="IFI27L1"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000165948">
    <property type="expression patterns" value="Tissue enhanced (choroid plexus, testis)"/>
  </dbReference>
  <dbReference type="MIM" id="611320">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_Q96BM0"/>
  <dbReference type="OpenTargets" id="ENSG00000165948"/>
  <dbReference type="PharmGKB" id="PA164720846"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000165948"/>
  <dbReference type="eggNOG" id="ENOG502S85T">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000165490"/>
  <dbReference type="HOGENOM" id="CLU_142338_4_0_1"/>
  <dbReference type="InParanoid" id="Q96BM0"/>
  <dbReference type="OMA" id="AIANGGX"/>
  <dbReference type="OrthoDB" id="4700521at2759"/>
  <dbReference type="PhylomeDB" id="Q96BM0"/>
  <dbReference type="TreeFam" id="TF340510"/>
  <dbReference type="PathwayCommons" id="Q96BM0"/>
  <dbReference type="SignaLink" id="Q96BM0"/>
  <dbReference type="BioGRID-ORCS" id="122509">
    <property type="hits" value="14 hits in 1146 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="IFI27L1">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="Q96BM0"/>
  <dbReference type="GenomeRNAi" id="122509"/>
  <dbReference type="Pharos" id="Q96BM0">
    <property type="development level" value="Tdark"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q96BM0"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 14"/>
  </dbReference>
  <dbReference type="RNAct" id="Q96BM0">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000165948">
    <property type="expression patterns" value="Expressed in right testis and 94 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q96BM0">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097190">
    <property type="term" value="P:apoptotic signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.110.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009311">
    <property type="entry name" value="IFI6/IFI27-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038213">
    <property type="entry name" value="IFI6/IFI27-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16932">
    <property type="entry name" value="INTERFERON ALPHA-INDUCIBLE PROTEIN 27"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16932:SF36">
    <property type="entry name" value="INTERFERON ALPHA-INDUCIBLE PROTEIN 27-LIKE PROTEIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06140">
    <property type="entry name" value="Ifi-6-16"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <feature type="chain" id="PRO_0000147371" description="Interferon alpha-inducible protein 27-like protein 1">
    <location>
      <begin position="1"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="1">
    <location>
      <begin position="14"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="1">
    <location>
      <begin position="59"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="1">
    <location>
      <begin position="81"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_062245" description="In dbSNP:rs57677258.">
    <original>G</original>
    <variation>S</variation>
    <location>
      <position position="35"/>
    </location>
  </feature>
  <feature type="helix" evidence="9">
    <location>
      <begin position="11"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="helix" evidence="9">
    <location>
      <begin position="21"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="34"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="helix" evidence="9">
    <location>
      <begin position="39"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="turn" evidence="9">
    <location>
      <begin position="56"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="60"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="helix" evidence="9">
    <location>
      <begin position="67"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="88"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="94"/>
      <end position="97"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="14728724"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="27673746"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="27777077"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="14728724"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="27673746"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="HGNC" id="HGNC:19754"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="9">
    <source>
      <dbReference type="PDB" id="2LOQ"/>
    </source>
  </evidence>
  <sequence length="104" mass="9549" checksum="C54AF9381B74A236" modified="2001-12-01" version="1">MGKESGWDSGRAAVAAVVGGVVAVGTVLVALSAMGFTSVGIAASSIAAKMMSTAAIANGGGVAAGSLVAILQSVGAAGLSVTSKVIGGFAGTALGAWLGSPPSS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>