<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-05-04" modified="2022-05-25" version="58" xmlns="http://uniprot.org/uniprot">
  <accession>P82357</accession>
  <name>SPING_PSEUS</name>
  <protein>
    <recommendedName>
      <fullName>Spinigerin</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Spinigerin N-3</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Spinigerin C-4</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Pseudacanthotermes spiniger</name>
    <dbReference type="NCBI Taxonomy" id="115113"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Dictyoptera</taxon>
      <taxon>Blattodea</taxon>
      <taxon>Blattoidea</taxon>
      <taxon>Termitoidae</taxon>
      <taxon>Termitidae</taxon>
      <taxon>Macrotermitinae</taxon>
      <taxon>Pseudacanthotermes</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="J. Biol. Chem." volume="276" first="4085" last="4092">
      <title>Insect immunity. Constitutive expression of a cysteine-rich antifungal and a linear antibacterial peptide in a termite insect.</title>
      <authorList>
        <person name="Lamberty M."/>
        <person name="Zachary D."/>
        <person name="Lanot R."/>
        <person name="Bordereau C."/>
        <person name="Robert A."/>
        <person name="Hoffmann J.A."/>
        <person name="Bulet P."/>
      </authorList>
      <dbReference type="PubMed" id="11053427"/>
      <dbReference type="DOI" id="10.1074/jbc.m002998200"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Blood</tissue>
      <tissue>Salivary gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="Biopolymers" volume="81" first="92" last="103">
      <title>Solution structures of stomoxyn and spinigerin, two insect antimicrobial peptides with an alpha-helical conformation.</title>
      <authorList>
        <person name="Landon C."/>
        <person name="Meudal H."/>
        <person name="Boulanger N."/>
        <person name="Bulet P."/>
        <person name="Vovelle F."/>
      </authorList>
      <dbReference type="PubMed" id="16170803"/>
      <dbReference type="DOI" id="10.1002/bip.20370"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Active against Gram-positive bacteria B.megaterium and M.luteus, Gram-negative bacteria E.coli SBS363 and D22, K.pneumoniae, S.typhimurium and P.aeruginosa, yeast C.albicans and filamentous fungi F.culmorum, N.crassa, N.hematococca and T.viridae. Inactive against Gram-positive bacteria B.subtilis, S.pyogenes, B.thuringiensis and S.aureus, Gram-negative bacteria E.cloacae and E.carotovora and filamentous fungus B.bassiana.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text>By bacterial infection.</text>
  </comment>
  <comment type="mass spectrometry" mass="3001.8" method="MALDI" evidence="1">
    <molecule>Spinigerin</molecule>
  </comment>
  <dbReference type="PDB" id="1ZRV">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-25"/>
  </dbReference>
  <dbReference type="PDB" id="1ZRW">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-25"/>
  </dbReference>
  <dbReference type="PDBsum" id="1ZRV"/>
  <dbReference type="PDBsum" id="1ZRW"/>
  <dbReference type="AlphaFoldDB" id="P82357"/>
  <dbReference type="SMR" id="P82357"/>
  <dbReference type="EvolutionaryTrace" id="P82357"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000004947" description="Spinigerin">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000004948" description="Spinigerin C-4">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000004949" description="Spinigerin N-3">
    <location>
      <begin position="4"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="helix" evidence="2">
    <location>
      <begin position="4"/>
      <end position="23"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11053427"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="2">
    <source>
      <dbReference type="PDB" id="1ZRV"/>
    </source>
  </evidence>
  <sequence length="25" mass="3001" checksum="AA79370264262F60" modified="2001-05-04" version="1">HVDKKVADKVLLLKQLRIMRLLTRL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>