<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2024-05-29" version="70" xmlns="http://uniprot.org/uniprot">
  <accession>P81034</accession>
  <name>MOIH1_CANPG</name>
  <protein>
    <recommendedName>
      <fullName>Mandibular organ-inhibiting hormone 1</fullName>
      <shortName>MOIH-1</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Cancer pagurus</name>
    <name type="common">Rock crab</name>
    <dbReference type="NCBI Taxonomy" id="6755"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Eucarida</taxon>
      <taxon>Decapoda</taxon>
      <taxon>Pleocyemata</taxon>
      <taxon>Brachyura</taxon>
      <taxon>Eubrachyura</taxon>
      <taxon>Cancroidea</taxon>
      <taxon>Cancridae</taxon>
      <taxon>Cancer</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="J. Biol. Chem." volume="271" first="12749" last="12754">
      <title>Structure and significance of mandibular organ-inhibiting hormone in the crab, Cancer pagurus. Involvement in multihormonal regulation of growth and reproduction.</title>
      <authorList>
        <person name="Wainwright G."/>
        <person name="Webster S.G."/>
        <person name="Wilkinson M.C."/>
        <person name="Chung J.S."/>
        <person name="Rees H.H."/>
      </authorList>
      <dbReference type="PubMed" id="8662685"/>
      <dbReference type="DOI" id="10.1074/jbc.271.22.12749"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Sinus gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Represses the synthesis of methyl farnesoate, the precursor of insect juvenile hormone III in the mandibular organ.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Produced by the medulla terminalis X-organ in the eyestalks and transported to the sinus gland where it is stored and released.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the arthropod CHH/MIH/GIH/VIH hormone family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P81034"/>
  <dbReference type="SMR" id="P81034"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007623">
    <property type="term" value="P:circadian rhythm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.2010.10">
    <property type="entry name" value="Crustacean CHH/MIH/GIH neurohormone"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018251">
    <property type="entry name" value="Crust_neurhormone_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031098">
    <property type="entry name" value="Crust_neurohorm"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035957">
    <property type="entry name" value="Crust_neurohorm_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001166">
    <property type="entry name" value="Hyperglycemic"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001262">
    <property type="entry name" value="Hyperglycemic2"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35981">
    <property type="entry name" value="ION TRANSPORT PEPTIDE, ISOFORM C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35981:SF2">
    <property type="entry name" value="ION TRANSPORT PEPTIDE, ISOFORM C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01147">
    <property type="entry name" value="Crust_neurohorm"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00549">
    <property type="entry name" value="HYPRGLYCEMC2"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00550">
    <property type="entry name" value="HYPRGLYCEMIC"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF81778">
    <property type="entry name" value="Crustacean CHH/MIH/GIH neurohormone"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01250">
    <property type="entry name" value="CHH_MIH_GIH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000209866" description="Mandibular organ-inhibiting hormone 1">
    <location>
      <begin position="1"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="7"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="27"/>
      <end position="53"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="78" mass="9241" checksum="8DB338A39058A62D" modified="1998-07-15" version="1">RRINNDCQNFIGNRAMYEKVDWICKDCANIFRKDGLLNNCRSNCFYNTEFLWCIDATENTRNKEQLEQWAAILGAGWN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>