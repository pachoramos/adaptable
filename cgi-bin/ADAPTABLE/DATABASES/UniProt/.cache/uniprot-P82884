<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-01-16" modified="2020-12-02" version="45" xmlns="http://uniprot.org/uniprot">
  <accession>P82884</accession>
  <name>TP1E_LITCL</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Temporin-1Ce</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lithobates clamitans</name>
    <name type="common">Green frog</name>
    <name type="synonym">Rana clamitans</name>
    <dbReference type="NCBI Taxonomy" id="145282"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Lithobates</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Peptides" volume="21" first="469" last="476">
      <title>Purification and characterization of antimicrobial peptides from the skin of the North American green frog Rana clamitans.</title>
      <authorList>
        <person name="Halverson T."/>
        <person name="Basir Y.J."/>
        <person name="Knoop F.C."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="10822101"/>
      <dbReference type="DOI" id="10.1016/s0196-9781(00)00178-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>AMIDATION AT LEU-13</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antibacterial activity against Gram-positive bacterium S.aureus (MIC=100 uM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1461.0" error="0.02" method="Electrospray" evidence="1"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the frog skin active peptide (FSAP) family. Temporin subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043574" description="Temporin-1Ce" evidence="1">
    <location>
      <begin position="1"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10822101"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="10822101"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="10822101"/>
    </source>
  </evidence>
  <sequence length="13" mass="1462" checksum="CC18586B9DF931AD" modified="2001-03-01" version="1">FLPFLATLLSKVL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>