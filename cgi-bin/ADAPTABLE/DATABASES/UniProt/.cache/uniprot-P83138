<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-12-06" modified="2022-05-25" version="41" xmlns="http://uniprot.org/uniprot">
  <accession>P83138</accession>
  <name>AFP4_MALPA</name>
  <protein>
    <recommendedName>
      <fullName>Antifungal protein 4</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>CW-4</fullName>
    </alternativeName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Malva parviflora</name>
    <name type="common">Little mallow</name>
    <name type="synonym">Cheeseweed mallow</name>
    <dbReference type="NCBI Taxonomy" id="145753"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Malvales</taxon>
      <taxon>Malvaceae</taxon>
      <taxon>Malvoideae</taxon>
      <taxon>Malva</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2001" name="Biochem. Biophys. Res. Commun." volume="282" first="1224" last="1228">
      <title>Purification and characterization of three antifungal proteins from cheeseweed (Malva parviflora).</title>
      <authorList>
        <person name="Wang X."/>
        <person name="Bunkers G.J."/>
        <person name="Walters M.R."/>
        <person name="Thoma R.S."/>
      </authorList>
      <dbReference type="PubMed" id="11302747"/>
      <dbReference type="DOI" id="10.1006/bbrc.2001.4716"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Possesses antifungal activity against P.infestans but not F.graminearum.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <temperatureDependence>
      <text>Thermostable. Still active after heating at 100 degrees Celsius for 20 minutes.</text>
    </temperatureDependence>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text>Antimicrobial activity is not affected by salt concentration.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006805">
    <property type="term" value="P:xenobiotic metabolic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000064483" description="Antifungal protein 4">
    <location>
      <begin position="1"/>
      <end position="37" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="2">
    <location>
      <position position="37"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11302747"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="11302747"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="37" mass="4441" checksum="52293EEA2C77B6D6" modified="2001-12-01" version="1" fragment="single">DRQIDMEEQQLEKLNKQDRXPGLRYAAKQQMXTXRMG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>