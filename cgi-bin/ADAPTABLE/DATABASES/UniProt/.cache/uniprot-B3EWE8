<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-03-21" modified="2022-05-25" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>B3EWE8</accession>
  <name>AFP1_TRANT</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Antifungal peptide 1</fullName>
      <shortName evidence="2">Tn-AFP1</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Trapa natans</name>
    <name type="common">Water chestnut</name>
    <dbReference type="NCBI Taxonomy" id="22666"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Myrtales</taxon>
      <taxon>Lythraceae</taxon>
      <taxon>Trapa</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2011" name="Peptides" volume="32" first="1741" last="1747">
      <title>Identification of an antifungal peptide from Trapa natans fruits with inhibitory effects on Candida tropicalis biofilm formation.</title>
      <authorList>
        <person name="Mandal S.M."/>
        <person name="Migliolo L."/>
        <person name="Franco O.L."/>
        <person name="Ghosh A.K."/>
      </authorList>
      <dbReference type="PubMed" id="21736910"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2011.06.020"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="1">Fruit flesh</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antifungal activity against C.tropicalis (MIC=32 ug/ml) including the inhibition of biofilm formation. Has virtually no hemolytic activity against mouse erythrocytes.</text>
  </comment>
  <comment type="mass spectrometry" mass="1230.237" method="MALDI" evidence="1"/>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <feature type="peptide" id="PRO_0000415957" description="Antifungal peptide 1" evidence="1">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="21736910"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="21736910"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="11" mass="1233" checksum="8285D9C6A73761F1" modified="2012-03-21" version="1">LMCTHPLDCSN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>