<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-10-01" modified="2023-02-22" version="75" xmlns="http://uniprot.org/uniprot">
  <accession>P33045</accession>
  <name>THHS_HORVU</name>
  <protein>
    <recommendedName>
      <fullName>Antifungal protein S</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Hordeum vulgare</name>
    <name type="common">Barley</name>
    <dbReference type="NCBI Taxonomy" id="4513"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>Liliopsida</taxon>
      <taxon>Poales</taxon>
      <taxon>Poaceae</taxon>
      <taxon>BOP clade</taxon>
      <taxon>Pooideae</taxon>
      <taxon>Triticodae</taxon>
      <taxon>Triticeae</taxon>
      <taxon>Hordeinae</taxon>
      <taxon>Hordeum</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="FEBS Lett." volume="291" first="127" last="131">
      <title>Two antifungal thaumatin-like proteins from barley grain.</title>
      <authorList>
        <person name="Hejgaard J."/>
        <person name="Jacobsen S."/>
        <person name="Svendsen I."/>
      </authorList>
      <dbReference type="PubMed" id="1936240"/>
      <dbReference type="DOI" id="10.1016/0014-5793(91)81119-s"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <strain>cv. Bomi Riso 1508</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Has antifungal activity. Inhibits the growth of Trichoderma viridae and Candida albicans.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the thaumatin family.</text>
  </comment>
  <dbReference type="PIR" id="S17684">
    <property type="entry name" value="S17684"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.110.10">
    <property type="entry name" value="Thaumatin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR037176">
    <property type="entry name" value="Osmotin/thaumatin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001938">
    <property type="entry name" value="Thaumatin"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00347">
    <property type="entry name" value="THAUMATIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF49870">
    <property type="entry name" value="Osmotin, thaumatin-like protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51367">
    <property type="entry name" value="THAUMATIN_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="chain" id="PRO_0000096231" description="Antifungal protein S">
    <location>
      <begin position="1"/>
      <end position="37" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="37"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00699"/>
    </source>
  </evidence>
  <sequence length="37" mass="3874" checksum="7BE164CCBE8A9881" modified="1993-10-01" version="1" fragment="single">ATFTVINKCQYTVWAAAVPAGGGQKLDAGQTWSIXXP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>