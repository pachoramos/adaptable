<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2022-02-23" modified="2024-10-02" version="89" xmlns="http://uniprot.org/uniprot">
  <accession>G5EGC3</accession>
  <name>PDF2_CAEEL</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Pigment dispersing factor homolog pdf-2</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="9" type="primary">pdf-2</name>
    <name evidence="9" type="synonym">nlp-37</name>
    <name evidence="7" type="synonym">pdf2</name>
    <name evidence="9" type="ORF">F48B9.4</name>
  </gene>
  <organism evidence="8">
    <name type="scientific">Caenorhabditis elegans</name>
    <dbReference type="NCBI Taxonomy" id="6239"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Nematoda</taxon>
      <taxon>Chromadorea</taxon>
      <taxon>Rhabditida</taxon>
      <taxon>Rhabditina</taxon>
      <taxon>Rhabditomorpha</taxon>
      <taxon>Rhabditoidea</taxon>
      <taxon>Rhabditidae</taxon>
      <taxon>Peloderinae</taxon>
      <taxon>Caenorhabditis</taxon>
    </lineage>
  </organism>
  <reference evidence="7" key="1">
    <citation type="journal article" date="2009" name="J. Neurochem." volume="111" first="228" last="241">
      <title>Discovery and characterization of a conserved pigment dispersing factor-like neuropeptide pathway in Caenorhabditis elegans.</title>
      <authorList>
        <person name="Janssen T."/>
        <person name="Husson S.J."/>
        <person name="Meelkop E."/>
        <person name="Temmerman L."/>
        <person name="Lindemans M."/>
        <person name="Verstraelen K."/>
        <person name="Rademakers S."/>
        <person name="Mertens I."/>
        <person name="Nitabach M."/>
        <person name="Jansen G."/>
        <person name="Schoofs L."/>
      </authorList>
      <dbReference type="PubMed" id="19686386"/>
      <dbReference type="DOI" id="10.1111/j.1471-4159.2009.06323.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <reference evidence="8" key="2">
    <citation type="journal article" date="1998" name="Science" volume="282" first="2012" last="2018">
      <title>Genome sequence of the nematode C. elegans: a platform for investigating biology.</title>
      <authorList>
        <consortium name="The C. elegans sequencing consortium"/>
      </authorList>
      <dbReference type="PubMed" id="9851916"/>
      <dbReference type="DOI" id="10.1126/science.282.5396.2012"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain evidence="8">Bristol N2</strain>
    </source>
  </reference>
  <reference evidence="6" key="3">
    <citation type="journal article" date="2008" name="J. Biol. Chem." volume="283" first="15241" last="15249">
      <title>Functional characterization of three G protein-coupled receptors for pigment dispersing factors in Caenorhabditis elegans.</title>
      <authorList>
        <person name="Janssen T."/>
        <person name="Husson S.J."/>
        <person name="Lindemans M."/>
        <person name="Mertens I."/>
        <person name="Rademakers S."/>
        <person name="Donck K.V."/>
        <person name="Geysen J."/>
        <person name="Jansen G."/>
        <person name="Schoofs L."/>
      </authorList>
      <dbReference type="PubMed" id="18390545"/>
      <dbReference type="DOI" id="10.1074/jbc.m709060200"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference evidence="6" key="4">
    <citation type="journal article" date="2012" name="Mol. Cell. Endocrinol." volume="361" first="232" last="240">
      <title>PDF receptor signaling in Caenorhabditis elegans modulates locomotion and egg-laying.</title>
      <authorList>
        <person name="Meelkop E."/>
        <person name="Temmerman L."/>
        <person name="Janssen T."/>
        <person name="Suetens N."/>
        <person name="Beets I."/>
        <person name="Van Rompay L."/>
        <person name="Shanmugam N."/>
        <person name="Husson S.J."/>
        <person name="Schoofs L."/>
      </authorList>
      <dbReference type="PubMed" id="22579613"/>
      <dbReference type="DOI" id="10.1016/j.mce.2012.05.001"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference evidence="6" key="5">
    <citation type="journal article" date="2015" name="Genes Brain Behav." volume="14" first="493" last="501">
      <title>Pigment-dispersing factor signaling in the circadian system of Caenorhabditis elegans.</title>
      <authorList>
        <person name="Herrero A."/>
        <person name="Romanowski A."/>
        <person name="Meelkop E."/>
        <person name="Caldart C.S."/>
        <person name="Schoofs L."/>
        <person name="Golombek D.A."/>
      </authorList>
      <dbReference type="PubMed" id="26113231"/>
      <dbReference type="DOI" id="10.1111/gbb.12231"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 4 5">Probable ligand of isoforms a and b of the calcitonin receptor-like protein, pdfr-1, a G-protein coupled receptor (PubMed:18390545). May not signal through isoform c of pdfr-1 (PubMed:18390545). Involved in locomotion; may play a role in circadian rhythms of locomotor activity (PubMed:18390545, PubMed:22579613, PubMed:26113231). Modulator of egg-laying (PubMed:22579613).</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="3">Expressed in the interneurons BDUL/R, AVG, AIML/R, RIS, AVD and PVT, the chemosensory neuron pairs PHA and PHB, the motor neurons RID and RIML/R, the sensory neurons AQR and PQR, and in the PVPL/R interneurons (PubMed:19686386). Also expressed in rectal gland cells rectD and rectVL/R, the intestino-rectal valve cells virL/R and three posterior arcade cells in the head (PubMed:19686386).</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="4">Delay in the timing of the reproductive peak of egg-laying.</text>
  </comment>
  <dbReference type="EMBL" id="EF141321">
    <property type="protein sequence ID" value="ABO42260.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BX284606">
    <property type="protein sequence ID" value="CCD68823.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="T16394">
    <property type="entry name" value="T16394"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_508397.2">
    <property type="nucleotide sequence ID" value="NM_075996.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="G5EGC3"/>
  <dbReference type="STRING" id="6239.F48B9.4.1"/>
  <dbReference type="PaxDb" id="6239-F48B9.4"/>
  <dbReference type="EnsemblMetazoa" id="F48B9.4.1">
    <property type="protein sequence ID" value="F48B9.4.1"/>
    <property type="gene ID" value="WBGene00018590"/>
  </dbReference>
  <dbReference type="GeneID" id="185966"/>
  <dbReference type="KEGG" id="cel:CELE_F48B9.4"/>
  <dbReference type="AGR" id="WB:WBGene00018590"/>
  <dbReference type="CTD" id="185966"/>
  <dbReference type="WormBase" id="F48B9.4">
    <property type="protein sequence ID" value="CE30276"/>
    <property type="gene ID" value="WBGene00018590"/>
    <property type="gene designation" value="pdf-2"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502SXAU">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_2471188_0_0_1"/>
  <dbReference type="InParanoid" id="G5EGC3"/>
  <dbReference type="OMA" id="FYNSRQF"/>
  <dbReference type="OrthoDB" id="2878360at2759"/>
  <dbReference type="PRO" id="PR:G5EGC3"/>
  <dbReference type="Proteomes" id="UP000001940">
    <property type="component" value="Chromosome X"/>
  </dbReference>
  <dbReference type="Bgee" id="WBGene00018590">
    <property type="expression patterns" value="Expressed in larva and 3 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007189">
    <property type="term" value="P:adenylate cyclase-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0040011">
    <property type="term" value="P:locomotion"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045475">
    <property type="term" value="P:locomotor rhythm"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035641">
    <property type="term" value="P:locomotory exploration behavior"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007567">
    <property type="term" value="P:parturition"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5015092027" description="Pigment dispersing factor homolog pdf-2" evidence="1">
    <location>
      <begin position="28"/>
      <end position="89"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="18390545"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="19686386"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="22579613"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="26113231"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="EMBL" id="ABO42260.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="Proteomes" id="UP000001940"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="9">
    <source>
      <dbReference type="WormBase" id="F48B9.4"/>
    </source>
  </evidence>
  <sequence length="89" mass="9917" checksum="E6645470792F1A71" modified="2011-12-14" version="1" precursor="true">MSSRISVSLLLLAVVATMFFTANVVDATPRSQGNMMRYGNSLPAYAPHVLYRFYNSRQFAPINKRNNAEVVNHILKNFGALDRLGDVGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>