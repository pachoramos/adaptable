<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1992-12-01" modified="2024-11-27" version="134" xmlns="http://uniprot.org/uniprot">
  <accession>P29032</accession>
  <name>CHIC_POPTR</name>
  <protein>
    <recommendedName>
      <fullName>Acidic endochitinase WIN6.2C</fullName>
      <ecNumber>3.2.1.14</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Populus trichocarpa</name>
    <name type="common">Western balsam poplar</name>
    <name type="synonym">Populus balsamifera subsp. trichocarpa</name>
    <dbReference type="NCBI Taxonomy" id="3694"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Malpighiales</taxon>
      <taxon>Salicaceae</taxon>
      <taxon>Saliceae</taxon>
      <taxon>Populus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="Plant Mol. Biol." volume="17" first="631" last="639">
      <title>Populus chitinase genes: structure, organization, and similarity of translated sequences to herbaceous plant chitinases.</title>
      <authorList>
        <person name="Davis J.M."/>
        <person name="Clarke H.R.G."/>
        <person name="Bradshaw H.D. Jr."/>
        <person name="Gordon M.P."/>
      </authorList>
      <dbReference type="PubMed" id="1912489"/>
      <dbReference type="DOI" id="10.1007/bf00037049"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="Science" volume="313" first="1596" last="1604">
      <title>The genome of black cottonwood, Populus trichocarpa (Torr. &amp; Gray).</title>
      <authorList>
        <person name="Tuskan G.A."/>
        <person name="Difazio S."/>
        <person name="Jansson S."/>
        <person name="Bohlmann J."/>
        <person name="Grigoriev I."/>
        <person name="Hellsten U."/>
        <person name="Putnam N."/>
        <person name="Ralph S."/>
        <person name="Rombauts S."/>
        <person name="Salamov A."/>
        <person name="Schein J."/>
        <person name="Sterck L."/>
        <person name="Aerts A."/>
        <person name="Bhalerao R.R."/>
        <person name="Bhalerao R.P."/>
        <person name="Blaudez D."/>
        <person name="Boerjan W."/>
        <person name="Brun A."/>
        <person name="Brunner A."/>
        <person name="Busov V."/>
        <person name="Campbell M."/>
        <person name="Carlson J."/>
        <person name="Chalot M."/>
        <person name="Chapman J."/>
        <person name="Chen G.-L."/>
        <person name="Cooper D."/>
        <person name="Coutinho P.M."/>
        <person name="Couturier J."/>
        <person name="Covert S."/>
        <person name="Cronk Q."/>
        <person name="Cunningham R."/>
        <person name="Davis J."/>
        <person name="Degroeve S."/>
        <person name="Dejardin A."/>
        <person name="dePamphilis C.W."/>
        <person name="Detter J."/>
        <person name="Dirks B."/>
        <person name="Dubchak I."/>
        <person name="Duplessis S."/>
        <person name="Ehlting J."/>
        <person name="Ellis B."/>
        <person name="Gendler K."/>
        <person name="Goodstein D."/>
        <person name="Gribskov M."/>
        <person name="Grimwood J."/>
        <person name="Groover A."/>
        <person name="Gunter L."/>
        <person name="Hamberger B."/>
        <person name="Heinze B."/>
        <person name="Helariutta Y."/>
        <person name="Henrissat B."/>
        <person name="Holligan D."/>
        <person name="Holt R."/>
        <person name="Huang W."/>
        <person name="Islam-Faridi N."/>
        <person name="Jones S."/>
        <person name="Jones-Rhoades M."/>
        <person name="Jorgensen R."/>
        <person name="Joshi C."/>
        <person name="Kangasjaervi J."/>
        <person name="Karlsson J."/>
        <person name="Kelleher C."/>
        <person name="Kirkpatrick R."/>
        <person name="Kirst M."/>
        <person name="Kohler A."/>
        <person name="Kalluri U."/>
        <person name="Larimer F."/>
        <person name="Leebens-Mack J."/>
        <person name="Leple J.-C."/>
        <person name="Locascio P."/>
        <person name="Lou Y."/>
        <person name="Lucas S."/>
        <person name="Martin F."/>
        <person name="Montanini B."/>
        <person name="Napoli C."/>
        <person name="Nelson D.R."/>
        <person name="Nelson C."/>
        <person name="Nieminen K."/>
        <person name="Nilsson O."/>
        <person name="Pereda V."/>
        <person name="Peter G."/>
        <person name="Philippe R."/>
        <person name="Pilate G."/>
        <person name="Poliakov A."/>
        <person name="Razumovskaya J."/>
        <person name="Richardson P."/>
        <person name="Rinaldi C."/>
        <person name="Ritland K."/>
        <person name="Rouze P."/>
        <person name="Ryaboy D."/>
        <person name="Schmutz J."/>
        <person name="Schrader J."/>
        <person name="Segerman B."/>
        <person name="Shin H."/>
        <person name="Siddiqui A."/>
        <person name="Sterky F."/>
        <person name="Terry A."/>
        <person name="Tsai C.-J."/>
        <person name="Uberbacher E."/>
        <person name="Unneberg P."/>
        <person name="Vahala J."/>
        <person name="Wall K."/>
        <person name="Wessler S."/>
        <person name="Yang G."/>
        <person name="Yin T."/>
        <person name="Douglas C."/>
        <person name="Marra M."/>
        <person name="Sandberg G."/>
        <person name="Van de Peer Y."/>
        <person name="Rokhsar D.S."/>
      </authorList>
      <dbReference type="PubMed" id="16973872"/>
      <dbReference type="DOI" id="10.1126/science.1128691"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Nisqually</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Defense against chitin-containing fungal pathogens.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>Random endo-hydrolysis of N-acetyl-beta-D-glucosaminide (1-&gt;4)-beta-linkages in chitin and chitodextrins.</text>
      <dbReference type="EC" id="3.2.1.14"/>
    </reaction>
  </comment>
  <comment type="induction">
    <text>By wounding.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the glycosyl hydrolase 19 family. Chitinase class I subfamily.</text>
  </comment>
  <dbReference type="EC" id="3.2.1.14"/>
  <dbReference type="EMBL" id="X59995">
    <property type="protein sequence ID" value="CAA42614.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CM009298">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P29032"/>
  <dbReference type="SMR" id="P29032"/>
  <dbReference type="STRING" id="3694.P29032"/>
  <dbReference type="CAZy" id="CBM18">
    <property type="family name" value="Carbohydrate-Binding Module Family 18"/>
  </dbReference>
  <dbReference type="CAZy" id="GH19">
    <property type="family name" value="Glycoside Hydrolase Family 19"/>
  </dbReference>
  <dbReference type="InParanoid" id="P29032"/>
  <dbReference type="Proteomes" id="UP000006729">
    <property type="component" value="Chromosome 9"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P29032">
    <property type="expression patterns" value="differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008061">
    <property type="term" value="F:chitin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004568">
    <property type="term" value="F:chitinase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016998">
    <property type="term" value="P:cell wall macromolecule catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006032">
    <property type="term" value="P:chitin catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0098542">
    <property type="term" value="P:defense response to other organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000272">
    <property type="term" value="P:polysaccharide catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd06921">
    <property type="entry name" value="ChtBD1_GH19_hevein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.60.10:FF:000001">
    <property type="entry name" value="Basic endochitinase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.530.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.60.10">
    <property type="entry name" value="Endochitinase-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001002">
    <property type="entry name" value="Chitin-bd_1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018371">
    <property type="entry name" value="Chitin-binding_1_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036861">
    <property type="entry name" value="Endochitinase-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000726">
    <property type="entry name" value="Glyco_hydro_19_cat"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023346">
    <property type="entry name" value="Lysozyme-like_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR22595:SF79">
    <property type="entry name" value="BASIC ENDOCHITINASE B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR22595">
    <property type="entry name" value="CHITINASE-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00187">
    <property type="entry name" value="Chitin_bind_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00182">
    <property type="entry name" value="Glyco_hydro_19"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00451">
    <property type="entry name" value="CHITINBINDNG"/>
  </dbReference>
  <dbReference type="SMART" id="SM00270">
    <property type="entry name" value="ChtBD1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF53955">
    <property type="entry name" value="Lysozyme-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57016">
    <property type="entry name" value="Plant lectins/antimicrobial peptides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00026">
    <property type="entry name" value="CHIT_BIND_I_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50941">
    <property type="entry name" value="CHIT_BIND_I_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0119">Carbohydrate metabolism</keyword>
  <keyword id="KW-0146">Chitin degradation</keyword>
  <keyword id="KW-0147">Chitin-binding</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0326">Glycosidase</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0624">Polysaccharide degradation</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005318" description="Acidic endochitinase WIN6.2C">
    <location>
      <begin position="23"/>
      <end position="121" status="greater than"/>
    </location>
  </feature>
  <feature type="domain" description="Chitin-binding type-1" evidence="2">
    <location>
      <begin position="23"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="region of interest" description="Catalytic">
    <location>
      <begin position="78"/>
      <end position="121"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="25"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="34"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="39"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="57"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="121"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00261"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="121" mass="12689" checksum="A89DFC96795FE87F" modified="1992-12-01" version="1" precursor="true" fragment="single">MSVWAFFAFFSLFLSLSVRGSAEQCGRQAGDALCPGGLCCSFYGWCGTTVDYCGDGCQSQCDGGDGCDGGGGGGGDGDDGYLSDIIPKSTFDALLKFRNDPRCHAVGFYTYDAFISAAKEF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>