<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-10-01" modified="2024-11-27" version="127" xmlns="http://uniprot.org/uniprot">
  <accession>P50230</accession>
  <accession>A1EC89</accession>
  <name>CCL4_RAT</name>
  <protein>
    <recommendedName>
      <fullName>C-C motif chemokine 4</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Macrophage inflammatory protein 1-beta</fullName>
      <shortName>MIP-1-beta</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Small-inducible cytokine A4</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Ccl4</name>
    <name type="synonym">Mip1b</name>
    <name type="synonym">Scya4</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1994-02" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Jones M.L."/>
        <person name="Shanley T.P."/>
        <person name="Schmal H."/>
        <person name="Friedl H.P."/>
        <person name="Ward P.A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>Long Evans</strain>
      <tissue>Lung</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2007" name="Genomics" volume="89" first="343" last="353">
      <title>Fine-mapping and comprehensive transcript analysis reveals nonsynonymous variants within a novel 1.17 Mb blood pressure QTL region on rat chromosome 10.</title>
      <authorList>
        <person name="Saad Y."/>
        <person name="Garrett M.R."/>
        <person name="Manickavasagam E."/>
        <person name="Yerga-Woolwine S."/>
        <person name="Farms P."/>
        <person name="Radecki T."/>
        <person name="Joe B."/>
      </authorList>
      <dbReference type="PubMed" id="17218081"/>
      <dbReference type="DOI" id="10.1016/j.ygeno.2006.12.005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>Dahl salt-sensitive</strain>
      <strain>Lewis</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Monokine with inflammatory and chemokinetic properties.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the intercrine beta (chemokine CC) family.</text>
  </comment>
  <dbReference type="EMBL" id="U06434">
    <property type="protein sequence ID" value="AAA96497.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF121996">
    <property type="protein sequence ID" value="ABL63435.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF121997">
    <property type="protein sequence ID" value="ABL63436.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_446310.1">
    <property type="nucleotide sequence ID" value="NM_053858.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P50230"/>
  <dbReference type="SMR" id="P50230"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000015199"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000015199"/>
  <dbReference type="GeneID" id="116637"/>
  <dbReference type="KEGG" id="rno:116637"/>
  <dbReference type="AGR" id="RGD:620441"/>
  <dbReference type="CTD" id="6351"/>
  <dbReference type="RGD" id="620441">
    <property type="gene designation" value="Ccl4"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S8M4">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="P50230"/>
  <dbReference type="OrthoDB" id="4265193at2759"/>
  <dbReference type="PhylomeDB" id="P50230"/>
  <dbReference type="Reactome" id="R-RNO-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P50230"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048020">
    <property type="term" value="F:CCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031726">
    <property type="term" value="F:CCR1 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031730">
    <property type="term" value="F:CCR5 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071466">
    <property type="term" value="P:cellular response to xenobiotic stimulus"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048245">
    <property type="term" value="P:eosinophil chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030595">
    <property type="term" value="P:leukocyte chemotaxis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051928">
    <property type="term" value="P:positive regulation of calcium ion transport"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050850">
    <property type="term" value="P:positive regulation of calcium-mediated signaling"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000503">
    <property type="term" value="P:positive regulation of natural killer cell chemotaxis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032760">
    <property type="term" value="P:positive regulation of tumor necrosis factor production"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043117">
    <property type="term" value="P:positive regulation of vascular permeability"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009636">
    <property type="term" value="P:response to toxic substance"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="CDD" id="cd00272">
    <property type="entry name" value="Chemokine_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000002">
    <property type="entry name" value="C-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000827">
    <property type="entry name" value="Chemokine_CC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF103">
    <property type="entry name" value="C-C MOTIF CHEMOKINE 4-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00472">
    <property type="entry name" value="SMALL_CYTOKINES_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005167" description="C-C motif chemokine 4">
    <location>
      <begin position="24"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="34"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="35"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="92" mass="10234" checksum="60B451EEEBC7103D" modified="1996-10-01" version="1" precursor="true">MKLCVSAFSLLLLVAAFCDSVLSAPIGSDPPTSCCFSYTSRKIHRNFVMDYYETSSLCSQPAVVFLTKKGRQICADPSEPWVNEYVNDLELN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>