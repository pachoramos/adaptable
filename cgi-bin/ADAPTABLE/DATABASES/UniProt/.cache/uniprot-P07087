<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-04-01" modified="2022-12-14" version="46" xmlns="http://uniprot.org/uniprot">
  <accession>P07087</accession>
  <name>ALPS_TACTR</name>
  <protein>
    <recommendedName>
      <fullName>Anti-lipopolysaccharide factor</fullName>
      <shortName>anti-LPS</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Tachypleus tridentatus</name>
    <name type="common">Japanese horseshoe crab</name>
    <dbReference type="NCBI Taxonomy" id="6853"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Merostomata</taxon>
      <taxon>Xiphosura</taxon>
      <taxon>Limulidae</taxon>
      <taxon>Tachypleus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1986" name="J. Biol. Chem." volume="261" first="7357" last="7365">
      <title>Primary structure of limulus anticoagulant anti-lipopolysaccharide factor.</title>
      <authorList>
        <person name="Aketagawa J."/>
        <person name="Miyata T."/>
        <person name="Ohtsubo S."/>
        <person name="Nakamura T."/>
        <person name="Morita T."/>
        <person name="Hayashida H."/>
        <person name="Iwanaga S."/>
        <person name="Takao T."/>
        <person name="Shimonishi Y."/>
      </authorList>
      <dbReference type="PubMed" id="3711091"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)38399-0"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <comment type="function">
    <text>Binds tightly to LPS and thus specifically inhibits the LPS-mediated activation of the hemolymph coagulation. It has a strong antibacterial effect especially on the growth of Gram-negative bacteria.</text>
  </comment>
  <dbReference type="PIR" id="A23931">
    <property type="entry name" value="A23931"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P07087"/>
  <dbReference type="SMR" id="P07087"/>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.160.320">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR024509">
    <property type="entry name" value="Anti-LPS_factor/Scygonadin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038539">
    <property type="entry name" value="Anti-LPS_factor/Scygonadin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11630">
    <property type="entry name" value="Anti-LPS-SCYG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <feature type="chain" id="PRO_0000064573" description="Anti-lipopolysaccharide factor">
    <location>
      <begin position="1"/>
      <end position="102"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="32"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>V</original>
    <variation>I</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>E</original>
    <variation>Q</variation>
    <location>
      <position position="102"/>
    </location>
  </feature>
  <sequence length="102" mass="11596" checksum="7FB76B00B296F349" modified="1988-04-01" version="1">EGGIWTQLALALVKNLATLWQSGDFQFLGHECHYRVNPTVKRLKWKYKGKFWCPSWTSITGRATKSSRSGAVEHSVRDFVSQAKSSGLITEKEAQTFISQYE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>