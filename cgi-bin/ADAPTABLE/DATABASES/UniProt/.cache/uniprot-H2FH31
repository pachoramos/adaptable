<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2021-09-29" modified="2024-07-24" version="30" xmlns="http://uniprot.org/uniprot">
  <accession>H2FH31</accession>
  <name>LEC_CREGR</name>
  <protein>
    <recommendedName>
      <fullName evidence="17 19">Galactose-binding lectin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="16 17 18 19 20 21 23">CGL</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="13 14 15 19 20 21 22">GalNAc/Gal-specific lectin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Crenomytilus grayanus</name>
    <name type="common">Gray mussel</name>
    <name type="synonym">Mytilus grayanus</name>
    <dbReference type="NCBI Taxonomy" id="151218"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Bivalvia</taxon>
      <taxon>Autobranchia</taxon>
      <taxon>Pteriomorphia</taxon>
      <taxon>Mytilida</taxon>
      <taxon>Mytiloidea</taxon>
      <taxon>Mytilidae</taxon>
      <taxon>Mytilinae</taxon>
      <taxon>Crenomytilus</taxon>
    </lineage>
  </organism>
  <reference evidence="30" key="1">
    <citation type="journal article" date="2013" name="Fish Shellfish Immunol." volume="35" first="1320" last="1324">
      <title>cDNA cloning and structural characterization of a lectin from the mussel Crenomytilus grayanus with a unique amino acid sequence and antibacterial activity.</title>
      <authorList>
        <person name="Kovalchuk S.N."/>
        <person name="Chikalovets I.V."/>
        <person name="Chernikov O.V."/>
        <person name="Molchanova V.I."/>
        <person name="Li W."/>
        <person name="Rasskazov V.A."/>
        <person name="Lukyanov P.A."/>
      </authorList>
      <dbReference type="PubMed" id="23886951"/>
      <dbReference type="DOI" id="10.1016/j.fsi.2013.07.011"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 14-20; 30-50; 68-84; 88-102; 106-110 AND 129-140</scope>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>CIRCULAR DICHROISM ANALYSIS</scope>
    <scope>PHYLOGENETIC ANALYSIS</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="Comp. Biochem. Physiol." volume="119C" first="45" last="50">
      <title>Isolation and characterization of new GalNAc/Gal-specific lectin from the sea mussel Crenomytilus grayanus.</title>
      <authorList>
        <person name="Belogortseva N.I."/>
        <person name="Molchanova V.I."/>
        <person name="Kurika A.V."/>
        <person name="Skobun A.S."/>
        <person name="Glazkova V.E."/>
      </authorList>
      <dbReference type="PubMed" id="9568372"/>
      <dbReference type="DOI" id="10.1016/s0742-8413(97)00180-1"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2014" name="Chem. Nat. Comp." volume="50" first="706" last="709">
      <title>Domain organization of lectin from the mussel Crenomytilus grayanus.</title>
      <authorList>
        <person name="Chikalovets I.V."/>
        <person name="Molchanova V.I."/>
        <person name="Chernikov O.V."/>
        <person name="Lukyanov P.A."/>
      </authorList>
    </citation>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>SUBUNIT</scope>
    <scope>DOMAIN</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2015" name="Fish Shellfish Immunol." volume="42" first="503" last="507">
      <title>A lectin with antifungal activity from the mussel Crenomytilus grayanus.</title>
      <authorList>
        <person name="Chikalovets I.V."/>
        <person name="Chernikov O.V."/>
        <person name="Pivkin M.V."/>
        <person name="Molchanova V.I."/>
        <person name="Litovchenko A.P."/>
        <person name="Li W."/>
        <person name="Lukyanov P.A."/>
      </authorList>
      <dbReference type="PubMed" id="25482060"/>
      <dbReference type="DOI" id="10.1016/j.fsi.2014.11.036"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2015" name="Fish Shellfish Immunol." volume="47" first="565" last="571">
      <title>Carbohydrate-binding motifs in a novel type lectin from the sea mussel Crenomytilus grayanus: Homology modeling study and site-specific mutagenesis.</title>
      <authorList>
        <person name="Kovalchuk S.N."/>
        <person name="Golotin V.A."/>
        <person name="Balabanova L.A."/>
        <person name="Buinovskaya N.S."/>
        <person name="Likhatskaya G.N."/>
        <person name="Rasskazov V.A."/>
      </authorList>
      <dbReference type="PubMed" id="26439416"/>
      <dbReference type="DOI" id="10.1016/j.fsi.2015.09.045"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>MUTAGENESIS OF HIS-16; PRO-17; GLY-19; HIS-64; PRO-65; GLY-67; HIS-108; PRO-109 AND GLY-111</scope>
    <scope>3D-STRUCTURE MODELING</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2017" name="Int. J. Biol. Macromol." volume="104" first="508" last="514">
      <title>Lectin CGL from the sea mussel Crenomytilus grayanus induces Burkitt's lymphoma cells death via interaction with surface glycan.</title>
      <authorList>
        <person name="Chernikov O."/>
        <person name="Kuzmich A."/>
        <person name="Chikalovets I."/>
        <person name="Molchanova V."/>
        <person name="Hua K.F."/>
      </authorList>
      <dbReference type="PubMed" id="28636877"/>
      <dbReference type="DOI" id="10.1016/j.ijbiomac.2017.06.074"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>BIOTECHNOLOGY</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2017" name="Sci. Rep." volume="7" first="6315" last="6315">
      <title>A GalNAc/Gal-specific lectin from the sea mussel Crenomytilus grayanus modulates immune response in macrophages and in mice.</title>
      <authorList>
        <person name="Chernikov O.V."/>
        <person name="Wong W.T."/>
        <person name="Li L.H."/>
        <person name="Chikalovets I.V."/>
        <person name="Molchanova V.I."/>
        <person name="Wu S.H."/>
        <person name="Liao J.H."/>
        <person name="Hua K.F."/>
      </authorList>
      <dbReference type="PubMed" id="28740170"/>
      <dbReference type="DOI" id="10.1038/s41598-017-06647-5"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>BIOTECHNOLOGY</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2018" name="Mar. Drugs" volume="16">
      <title>Mutagenesis Studies and Structure-function Relationships for GalNAc/Gal-Specific Lectin from the Sea Mussel Crenomytilus grayanus.</title>
      <authorList>
        <person name="Kovalchuk S.N."/>
        <person name="Buinovskaya N.S."/>
        <person name="Likhatskaya G.N."/>
        <person name="Rasskazov V.A."/>
        <person name="Son O.M."/>
        <person name="Tekutyeva L.A."/>
        <person name="Balabanova L.A."/>
      </authorList>
      <dbReference type="PubMed" id="30486373"/>
      <dbReference type="DOI" id="10.3390/md16120471"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>BIOTECHNOLOGY</scope>
    <scope>MUTAGENESIS OF ASN-27; HIS-37; GLU-75; HIS-85; ASN-119; ASP-127 AND HIS-129</scope>
    <scope>IN SILICO ANALYSIS OF BINDING TO GALACTOSE; GLOBOTRIOSE GB3 AND PORCINE STOMACH MUCIN TRISACCHARIDE</scope>
    <scope>3D-STRUCTURE MODELING OF THE LIGAND BINDING SITE IN COMPLEX WITH GLOBOTRIOSE GB3 AND PORCINE STOMACH MUCIN TRISACCHARIDE</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2019" name="Molecules" volume="25">
      <title>Activity Dependence of a Novel Lectin Family on Structure and Carbohydrate-Binding Properties.</title>
      <authorList>
        <person name="Chikalovets I."/>
        <person name="Filshtein A."/>
        <person name="Molchanova V."/>
        <person name="Mizgina T."/>
        <person name="Lukyanov P."/>
        <person name="Nedashkovskaya O."/>
        <person name="Hua K.F."/>
        <person name="Chernikov O."/>
      </authorList>
      <dbReference type="PubMed" id="31905927"/>
      <dbReference type="DOI" id="10.3390/molecules25010150"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>BIOTECHNOLOGY</scope>
    <scope>REVIEW</scope>
  </reference>
  <reference evidence="31" key="10">
    <citation type="journal article" date="2015" name="Acta Crystallogr. F Struct. Biol. Commun." volume="71" first="1429" last="1436">
      <title>Structure of a lectin from the sea mussel Crenomytilus grayanus (CGL).</title>
      <authorList>
        <person name="Jakob M."/>
        <person name="Lubkowski J."/>
        <person name="O'Keefe B.R."/>
        <person name="Wlodawer A."/>
      </authorList>
      <dbReference type="PubMed" id="26527272"/>
      <dbReference type="DOI" id="10.1107/s2053230x15019858"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.12 ANGSTROMS)</scope>
    <scope>SUBUNIT</scope>
  </reference>
  <reference evidence="32 33 34 35" key="11">
    <citation type="journal article" date="2016" name="J. Am. Chem. Soc." volume="138" first="4787" last="4795">
      <title>A Multivalent Marine Lectin from Crenomytilus grayanus Possesses Anti-cancer Activity through Recognizing Globotriose Gb3.</title>
      <authorList>
        <person name="Liao J.H."/>
        <person name="Chien C.T."/>
        <person name="Wu H.Y."/>
        <person name="Huang K.F."/>
        <person name="Wang I."/>
        <person name="Ho M.R."/>
        <person name="Tu I.F."/>
        <person name="Lee I.M."/>
        <person name="Li W."/>
        <person name="Shih Y.L."/>
        <person name="Wu C.Y."/>
        <person name="Lukyanov P.A."/>
        <person name="Hsu S.T."/>
        <person name="Wu S.H."/>
      </authorList>
      <dbReference type="PubMed" id="27010847"/>
      <dbReference type="DOI" id="10.1021/jacs.6b00111"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.08 ANGSTROMS) AND IN COMPLEX WITH GALACTOSE; GALACTOSAMINE AND GLOBOTRIOSE GB3</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>BIOTECHNOLOGY</scope>
    <scope>NMR SPECTROSCOPY</scope>
    <scope>PHYLOGENETIC ANALYSIS</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 4 6 7 8 9 10 11 12">Galactose-binding lectin (PubMed:23886951, PubMed:27010847, PubMed:28636877, PubMed:9568372). Binds both alpha and beta anomer of galactose (Gal), but has a stronger interaction with the glycans having alpha Gal at the non-reducing end and binds beta Gal weakly only in highly branched glycans. Has high affinity to Galalpha1-4Galbeta1-4GlcNAc (PubMed:28636877). Binds N-acetyl-2-deoxy-2-amino-galactose (2-deoxy-GalNAc) (PubMed:23886951, PubMed:9568372). Binds N-acetylgalactosamine (GalNAc) (PubMed:26439416). Binds porcine stomach mucin (PSM) with high affinity (PubMed:26439416, PubMed:30486373). Binds galactosamine (PubMed:27010847). Binds laminin, bovine submaxillary mucin (BSM), fibronectin, type I collagen and gelatin with a decreasing affinity, respectively (Ref.3). Has hemagglutinating activity towards human type A erythrocytes (PubMed:26439416, PubMed:9568372, Ref.3). Hemagglutinates also human type 0, B and AB erythrocytes as well as rabbit and mouse erythrocytes (PubMed:9568372). Agglutinates both Gram-positive and Gram-negative bacteria including B.subtilis ATCC 6633, S.aureus ATCC 21027 and E.coli 3254, respectively. No agglutination activity towards Gram-positive S.amurskyense CMM 3673. Has bacteriostatic activity on S.amurskyense CMM 3673, B.subtilis ATCC 6633, S.aureus ATCC 21027 and E.coli 3254. However, has no agglutination nor bacteriostatic activity on Gram-negative C.scophthalmum CIP 104199 or A.troitsensis KMM 3674 (PubMed:23886951). Inhibits growth of fungi from the genera Aspergillus, Penicillium, Trichoderma and st. Mycelia. Inhibits germination of spores and hyphal growth of them (PubMed:25482060). Has dose-dependent cytotoxic effect on the human globotriaosylceramide (Gb3)-expressing Burkitt's lymphoma (Raji) cell line. Binds to Gb3 in these cells leading to activation of caspase-9/3 and PARP (PubMed:28636877). Has dose-dependent cytotoxic effect on the Gb3-expressing human MCF-7 breast cancer cell line (PubMed:27010847). No cytotoxic effect on myelogenous leukemia K562 cell line, which does not express Gb3 (PubMed:28636877). Activates immune responses in mice and increases cytokine production of TNF-alpha, IL-6 and MCP-1 in the serum and the peritoneal lavage of mice. Induces TNF-alpha and IL-6 secretion in mouse RAW264.7 macrophages, mouse bone marrow-derived macrophages, human THP-1 macrophages, human peripheral blood mononuclear cells (PBMCs) and human blood monocyte-derived macrophages. TNF-alpha production in macrophages could not be inhibited by GalNAc, GalN or Gal, indicating that induced cytokine production is separate from its sugar binding activity. Increases intracellular reactive oxygen species levels, expression and phosphorylation of protein kinases PKC alpha/delta, expression of COX-2 and NF-kappaB, and activates the MAPK pathway by increasing the phosphorylation of ERK1/2, JNK1/2 and p38 in mouse RAW264.7 macrophages. Induces endotoxin tolerance in lipopolysaccharide(LPS)-activated macrophages by down-regulating IRAK2 expression, reducing JNK1/2 phosphorylation and NF-kappaB activation. Can slightly increase the bactericidal activity of RAW264.7 macrophages (PubMed:28740170). Has DNA-binding activity (PubMed:27010847). Recognizes pathogen-associated molecular patterns (PAMPs) and binds to LPS from E.coli, but has only little binding to beta-1,3-glucan from E.gracilis and peptidoglycan from S.aureus. Activates secretion of TNF-alpha and IFN-gamma by the human peripheral blood cells (HPBCs) (PubMed:31905927). May be involved in innate immunity acting as an antibacterial and antifungal agent involved in the recognition and clearance of pathogens (PubMed:23886951, PubMed:25482060, PubMed:31905927).</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="2 3 4 7 11 12">Bacterial binding activity is inhibited by D-galactose (PubMed:23886951). Hemagglutinating activity is independent of divalent cations Ca2(+) or Mg2(+). It is strongly inhibited by N-acetyl-D-galactosamine (GalNAc), D-galactose and D-talose, and to a lesser extent by melibiose and raffinose. Also inhibited by glycoprotein asialo-bovine submaxillary mucin (BSM). Not inhibited by D-glucose, D-fucose, D-galactitol, N-acetyl-D-glucosamine or lactose (PubMed:26439416, PubMed:9568372). Fungal binding activity is inhibited by D-galactose (PubMed:25482060). Cytotoxic activity against Raji cell line is completely inhibited by galactose, melibiose and raffinose, but not by glucose or lactose (PubMed:28636877). Galactose inhibits binding to laminin and BSM, but not to collagen, gelatin or fibronectin (Ref.3).</text>
  </comment>
  <comment type="biophysicochemical properties">
    <phDependence>
      <text evidence="11">Optimum pH is between 8-10 for the hemagglutinating activity of the human erythrocytes.</text>
    </phDependence>
    <temperatureDependence>
      <text evidence="11">Hemagglutinating activity of the human erythrocytes is fully maintained after heating at 50 degrees Celsius for 3 hours. The activity is partially lost after heating at 55 degrees Celsius and completely lost after heating at 65 degrees Celsius for 30 minutes.</text>
    </temperatureDependence>
  </comment>
  <comment type="subunit">
    <text evidence="5 6 12">Monomer in solution (PubMed:26527272). Homodimer in solution (PubMed:27010847). Exists as a monomer in solution when a low concentration (0.001 mg/ml) of it is present. Homodimers start to appear at a concentration of 0.01 mg/ml and tetramers at a concentration of 0.1 mg/ml (Ref.3).</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Highly expressed in mantle and to a lesser extent in muscle, hepatopancreas, gill and hemocytes.</text>
  </comment>
  <comment type="induction">
    <text evidence="3">Up-regulated in mantle by challenge of yeast P.pastoris reaching the maximum level at 12 hours post challenge and recovering to the original level at 24 hours post challenge.</text>
  </comment>
  <comment type="domain">
    <text evidence="12">Contains a collagen like domain, which probably promotes oligomerization.</text>
  </comment>
  <comment type="biotechnology">
    <text evidence="24 25 26 27 28">This protein may have potential in cancer diagnosis and treatment (PubMed:27010847, PubMed:28636877, PubMed:30486373, PubMed:31905927). A synthetic analog of this protein with enhanced carbohydrate-binding properties may be designed for the purpose of constructing a biosensor for cancer diagnostics or anticancer therapy (PubMed:30486373). May potentially be used as an immune modulator in mammals, because of its effects on macrophages and its ability to promote secretion of cytokines (PubMed:28740170).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="29">Cleavage of the collagen like domain may decrease the agglutinating ability and alter carbohydrate-binding properties. The function of this protein could potentially be regulated by proteolysis in vivo.</text>
  </comment>
  <dbReference type="EMBL" id="JQ314213">
    <property type="protein sequence ID" value="AEY80387.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="5DUY">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.12 A"/>
    <property type="chains" value="A/B/C/D/E/F=1-150"/>
  </dbReference>
  <dbReference type="PDB" id="5F8S">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.08 A"/>
    <property type="chains" value="A/B=1-150"/>
  </dbReference>
  <dbReference type="PDB" id="5F8W">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.56 A"/>
    <property type="chains" value="A/B=1-150"/>
  </dbReference>
  <dbReference type="PDB" id="5F8Y">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A/B=1-150"/>
  </dbReference>
  <dbReference type="PDB" id="5F90">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.64 A"/>
    <property type="chains" value="A/B=1-150"/>
  </dbReference>
  <dbReference type="PDBsum" id="5DUY"/>
  <dbReference type="PDBsum" id="5F8S"/>
  <dbReference type="PDBsum" id="5F8W"/>
  <dbReference type="PDBsum" id="5F8Y"/>
  <dbReference type="PDBsum" id="5F90"/>
  <dbReference type="AlphaFoldDB" id="H2FH31"/>
  <dbReference type="SMR" id="H2FH31"/>
  <dbReference type="UniLectin" id="H2FH31"/>
  <dbReference type="EvolutionaryTrace" id="H2FH31"/>
  <dbReference type="GO" id="GO:0030246">
    <property type="term" value="F:carbohydrate binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.80.10.50">
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0348">Hemagglutinin</keyword>
  <keyword id="KW-0430">Lectin</keyword>
  <feature type="chain" id="PRO_0000453467" description="Galactose-binding lectin">
    <location>
      <begin position="1"/>
      <end position="150"/>
    </location>
  </feature>
  <feature type="binding site" evidence="6 33">
    <location>
      <position position="16"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 33">
    <location>
      <position position="19"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 35">
    <location>
      <position position="27"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 33">
    <location>
      <begin position="35"/>
      <end position="37"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 33">
    <location>
      <position position="64"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 33">
    <location>
      <position position="67"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 33">
    <location>
      <position position="75"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>3</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 33">
    <location>
      <begin position="83"/>
      <end position="85"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 33">
    <location>
      <position position="108"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>3</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="33">
    <location>
      <position position="111"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>3</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 35">
    <location>
      <position position="119"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 33">
    <location>
      <begin position="127"/>
      <end position="129"/>
    </location>
    <ligand>
      <name>D-galactose</name>
      <dbReference type="ChEBI" id="CHEBI:4139"/>
      <label>3</label>
    </ligand>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="1">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="1">
    <location>
      <position position="74"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="1">
    <location>
      <position position="118"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of hemagglutinating and porcine stomach mucin-binding activities; when associated with A-17 and A-19." evidence="4">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="16"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of hemagglutinating and porcine stomach mucin-binding activities; when associated with A-16 and A-19." evidence="4">
    <original>P</original>
    <variation>A</variation>
    <location>
      <position position="17"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of hemagglutinating and porcine stomach mucin-binding activities; when associated with A-16 and A-17." evidence="4">
    <original>G</original>
    <variation>A</variation>
    <location>
      <position position="19"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="5.9-fold decreased porcine stomach mucin-binding activity compared to wild-type." evidence="9">
    <original>N</original>
    <variation>A</variation>
    <location>
      <position position="27"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="1.4-fold decreased porcine stomach mucin-binding activity compared to wild-type." evidence="9">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of hemagglutinating and porcine stomach mucin-binding activities; when associated with A-65 and A-67." evidence="4">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="64"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of hemagglutinating and porcine stomach mucin-binding activities; when associated with A-64 and A-67." evidence="4">
    <original>P</original>
    <variation>A</variation>
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of hemagglutinating and porcine stomach mucin-binding activities; when associated with A-64 and A-65." evidence="4">
    <original>G</original>
    <variation>A</variation>
    <location>
      <position position="67"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="3.2-fold decreased porcine stomach mucin-binding activity compared to wild-type." evidence="9">
    <original>E</original>
    <variation>A</variation>
    <location>
      <position position="75"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="5.0-fold decreased porcine stomach mucin-binding activity compared to wild-type." evidence="9">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="85"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Retains slight hemagglutinating activity and has 6-fold decreased porcine stomach mucin-binding activity; when associated with A-109 and A-111." evidence="4">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="108"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Retains slight hemagglutinating activity and has 6-fold decreased porcine stomach mucin-binding activity; when associated with A-108 and A-111." evidence="4">
    <original>P</original>
    <variation>A</variation>
    <location>
      <position position="109"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Retains slight hemagglutinating activity and has 6-fold decreased porcine stomach mucin-binding activity; when associated with A-108 and A-109." evidence="4">
    <original>G</original>
    <variation>A</variation>
    <location>
      <position position="111"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="11.1-fold decreased porcine stomach mucin-binding activity compared to wild-type." evidence="9">
    <original>N</original>
    <variation>A</variation>
    <location>
      <position position="119"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="4.5-fold decreased porcine stomach mucin-binding activity compared to wild-type." evidence="9">
    <original>D</original>
    <variation>A</variation>
    <location>
      <position position="127"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="2.3-fold decreased porcine stomach mucin-binding activity compared to wild-type." evidence="9">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="129"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="4"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="turn" evidence="36">
    <location>
      <begin position="9"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="14"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="28"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="helix" evidence="36">
    <location>
      <begin position="38"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="42"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="51"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="turn" evidence="36">
    <location>
      <begin position="57"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="62"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="77"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="helix" evidence="36">
    <location>
      <begin position="86"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="90"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="turn" evidence="36">
    <location>
      <begin position="94"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="98"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="106"/>
      <end position="109"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="120"/>
      <end position="125"/>
    </location>
  </feature>
  <feature type="helix" evidence="36">
    <location>
      <begin position="130"/>
      <end position="132"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="134"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="strand" evidence="36">
    <location>
      <begin position="141"/>
      <end position="145"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00498"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="23886951"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="25482060"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="26439416"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="26527272"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="27010847"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="28636877"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="28740170"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="30486373"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="31905927"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="11">
    <source>
      <dbReference type="PubMed" id="9568372"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="12">
    <source ref="3"/>
  </evidence>
  <evidence type="ECO:0000303" key="13">
    <source>
      <dbReference type="PubMed" id="23886951"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="14">
    <source>
      <dbReference type="PubMed" id="25482060"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="15">
    <source>
      <dbReference type="PubMed" id="26439416"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="16">
    <source>
      <dbReference type="PubMed" id="26527272"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="17">
    <source>
      <dbReference type="PubMed" id="27010847"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="18">
    <source>
      <dbReference type="PubMed" id="28636877"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="19">
    <source>
      <dbReference type="PubMed" id="28740170"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="20">
    <source>
      <dbReference type="PubMed" id="30486373"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="21">
    <source>
      <dbReference type="PubMed" id="31905927"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="22">
    <source>
      <dbReference type="PubMed" id="9568372"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="23">
    <source ref="3"/>
  </evidence>
  <evidence type="ECO:0000305" key="24">
    <source>
      <dbReference type="PubMed" id="27010847"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="25">
    <source>
      <dbReference type="PubMed" id="28636877"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="26">
    <source>
      <dbReference type="PubMed" id="28740170"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="27">
    <source>
      <dbReference type="PubMed" id="30486373"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="28">
    <source>
      <dbReference type="PubMed" id="31905927"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="29">
    <source ref="3"/>
  </evidence>
  <evidence type="ECO:0000312" key="30">
    <source>
      <dbReference type="EMBL" id="AEY80387.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="31">
    <source>
      <dbReference type="PDB" id="5DUY"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="32">
    <source>
      <dbReference type="PDB" id="5F8S"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="33">
    <source>
      <dbReference type="PDB" id="5F8W"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="34">
    <source>
      <dbReference type="PDB" id="5F8Y"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="35">
    <source>
      <dbReference type="PDB" id="5F90"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="36">
    <source>
      <dbReference type="PDB" id="5F8S"/>
    </source>
  </evidence>
  <sequence length="150" mass="17023" checksum="9DA43920298898C9" modified="2012-03-21" version="1">MTTFLIKHKASGKFLHPYGGSSNPANNTKLVLHSDIHERMYFQFDVVDERWGYIKHVASGKIVHPYGGQANPPNETNMVLHQDRHDRALFAMDFFNDNIMHKGGKYIHPKGGSPNPPNNTETVIHGDKHAAMEFIFVSPKNKDKRVLVYA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>