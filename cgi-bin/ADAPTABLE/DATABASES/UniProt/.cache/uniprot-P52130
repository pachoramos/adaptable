<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-10-01" modified="2024-11-27" version="122" xmlns="http://uniprot.org/uniprot">
  <accession>P52130</accession>
  <accession>Q2MAE0</accession>
  <name>RNLB_ECOLI</name>
  <protein>
    <recommendedName>
      <fullName>Antitoxin RnlB</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">rnlB</name>
    <name type="synonym">yfjO</name>
    <name type="ordered locus">b2631</name>
    <name type="ordered locus">JW5418</name>
  </gene>
  <organism>
    <name type="scientific">Escherichia coli (strain K12)</name>
    <dbReference type="NCBI Taxonomy" id="83333"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Escherichia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Science" volume="277" first="1453" last="1462">
      <title>The complete genome sequence of Escherichia coli K-12.</title>
      <authorList>
        <person name="Blattner F.R."/>
        <person name="Plunkett G. III"/>
        <person name="Bloch C.A."/>
        <person name="Perna N.T."/>
        <person name="Burland V."/>
        <person name="Riley M."/>
        <person name="Collado-Vides J."/>
        <person name="Glasner J.D."/>
        <person name="Rode C.K."/>
        <person name="Mayhew G.F."/>
        <person name="Gregor J."/>
        <person name="Davis N.W."/>
        <person name="Kirkpatrick H.A."/>
        <person name="Goeden M.A."/>
        <person name="Rose D.J."/>
        <person name="Mau B."/>
        <person name="Shao Y."/>
      </authorList>
      <dbReference type="PubMed" id="9278503"/>
      <dbReference type="DOI" id="10.1126/science.277.5331.1453"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>K12 / MG1655 / ATCC 47076</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="Mol. Syst. Biol." volume="2" first="E1" last="E5">
      <title>Highly accurate genome sequences of Escherichia coli K-12 strains MG1655 and W3110.</title>
      <authorList>
        <person name="Hayashi K."/>
        <person name="Morooka N."/>
        <person name="Yamamoto Y."/>
        <person name="Fujita K."/>
        <person name="Isono K."/>
        <person name="Choi S."/>
        <person name="Ohtsubo E."/>
        <person name="Baba T."/>
        <person name="Wanner B.L."/>
        <person name="Mori H."/>
        <person name="Horiuchi T."/>
      </authorList>
      <dbReference type="PubMed" id="16738553"/>
      <dbReference type="DOI" id="10.1038/msb4100049"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>K12 / W3110 / ATCC 27325 / DSM 5911</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2010" name="Genetics" volume="185" first="823" last="830">
      <title>IscR regulates RNase LS activity by repressing rnlA transcription.</title>
      <authorList>
        <person name="Otsuka Y."/>
        <person name="Miki K."/>
        <person name="Koga M."/>
        <person name="Katayama N."/>
        <person name="Morimoto W."/>
        <person name="Takahashi Y."/>
        <person name="Yonesaki T."/>
      </authorList>
      <dbReference type="PubMed" id="20421606"/>
      <dbReference type="DOI" id="10.1534/genetics.110.114462"/>
    </citation>
    <scope>INDUCTION</scope>
    <source>
      <strain>K12</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2011" name="Genetics" volume="187" first="123" last="130">
      <title>Escherichia coli rnlA and rnlB compose a novel toxin-antitoxin system.</title>
      <authorList>
        <person name="Koga M."/>
        <person name="Otsuka Y."/>
        <person name="Lemire S."/>
        <person name="Yonesaki T."/>
      </authorList>
      <dbReference type="PubMed" id="20980243"/>
      <dbReference type="DOI" id="10.1534/genetics.110.121798"/>
    </citation>
    <scope>FUNCTION AS AN ANTITOXIN</scope>
    <scope>INTERACTION WITH RNLA</scope>
    <scope>PROBABLE CLEAVAGE BY LON AND CPLXP PROTEASES</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>K12</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="Mol. Microbiol." volume="83" first="669" last="681">
      <title>Dmd of bacteriophage T4 functions as an antitoxin against Escherichia coli LsoA and RnlA toxins.</title>
      <authorList>
        <person name="Otsuka Y."/>
        <person name="Yonesaki T."/>
      </authorList>
      <dbReference type="PubMed" id="22403819"/>
      <dbReference type="DOI" id="10.1111/j.1365-2958.2012.07975.x"/>
    </citation>
    <scope>FUNCTION AS AN ANTITOXIN</scope>
    <source>
      <strain>K12 / W3110 / ATCC 27325 / DSM 5911</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 3">Antitoxin component of a type II toxin-antitoxin (TA) system. A labile antitoxin (half-life of 2.1 minutes) that inhibits the endonuclease activity of cognate toxin RnlA but not that of non-cognate toxin LsoA.</text>
  </comment>
  <comment type="subunit">
    <text>Can form a complex with cognate toxin RnlA.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-21183386">
      <id>P52130</id>
    </interactant>
    <interactant intactId="EBI-560462">
      <id>P52129</id>
      <label>rnlA</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="induction">
    <text evidence="1">Not repressed by IscR. rnlA-rnlB forms an operon, the downstream rnlB also has its own promoter.</text>
  </comment>
  <comment type="PTM">
    <text>Probably degraded by CplXP and Lon proteases.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="2">Essential, it cannot be deleted unless rnlA is also disrupted.</text>
  </comment>
  <comment type="sequence caution" evidence="4">
    <conflict type="erroneous initiation">
      <sequence resource="EMBL-CDS" id="AAA79800" version="1"/>
    </conflict>
    <text>Truncated N-terminus.</text>
  </comment>
  <dbReference type="EMBL" id="U36840">
    <property type="protein sequence ID" value="AAA79800.1"/>
    <property type="status" value="ALT_INIT"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U00096">
    <property type="protein sequence ID" value="AAC75679.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AP009048">
    <property type="protein sequence ID" value="BAE76766.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="T08643">
    <property type="entry name" value="T08643"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_417120.2">
    <property type="nucleotide sequence ID" value="NC_000913.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000461704.1">
    <property type="nucleotide sequence ID" value="NZ_LN832404.1"/>
  </dbReference>
  <dbReference type="PDB" id="6Y2P">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.64 A"/>
    <property type="chains" value="C/D=1-123"/>
  </dbReference>
  <dbReference type="PDBsum" id="6Y2P"/>
  <dbReference type="AlphaFoldDB" id="P52130"/>
  <dbReference type="SASBDB" id="P52130"/>
  <dbReference type="SMR" id="P52130"/>
  <dbReference type="BioGRID" id="4261565">
    <property type="interactions" value="16"/>
  </dbReference>
  <dbReference type="ComplexPortal" id="CPX-4115">
    <property type="entry name" value="RnlAB toxin-antitoxin complex"/>
  </dbReference>
  <dbReference type="IntAct" id="P52130">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="511145.b2631"/>
  <dbReference type="PaxDb" id="511145-b2631"/>
  <dbReference type="EnsemblBacteria" id="AAC75679">
    <property type="protein sequence ID" value="AAC75679"/>
    <property type="gene ID" value="b2631"/>
  </dbReference>
  <dbReference type="GeneID" id="947113"/>
  <dbReference type="KEGG" id="ecj:JW5418"/>
  <dbReference type="KEGG" id="eco:b2631"/>
  <dbReference type="KEGG" id="ecoc:C3026_14555"/>
  <dbReference type="PATRIC" id="fig|511145.12.peg.2725"/>
  <dbReference type="EchoBASE" id="EB2993"/>
  <dbReference type="eggNOG" id="ENOG50331J3">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_163902_1_0_6"/>
  <dbReference type="InParanoid" id="P52130"/>
  <dbReference type="OMA" id="CSNGFEW"/>
  <dbReference type="OrthoDB" id="7069026at2"/>
  <dbReference type="BioCyc" id="EcoCyc:G7366-MONOMER"/>
  <dbReference type="BioCyc" id="MetaCyc:G7366-MONOMER"/>
  <dbReference type="PRO" id="PR:P52130"/>
  <dbReference type="Proteomes" id="UP000000318">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000000625">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0110001">
    <property type="term" value="C:toxin-antitoxin complex"/>
    <property type="evidence" value="ECO:0000353"/>
    <property type="project" value="ComplexPortal"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006355">
    <property type="term" value="P:regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="ComplexPortal"/>
  </dbReference>
  <dbReference type="GO" id="GO:0040008">
    <property type="term" value="P:regulation of growth"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="ComplexPortal"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044010">
    <property type="term" value="P:single-species biofilm formation"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="ComplexPortal"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031834">
    <property type="entry name" value="RnlB/LsoB_antitoxin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF15933">
    <property type="entry name" value="RnlB_antitoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-1277">Toxin-antitoxin system</keyword>
  <feature type="chain" id="PRO_0000169280" description="Antitoxin RnlB">
    <location>
      <begin position="1"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="2"/>
      <end position="7"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="11"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="21"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="25"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="29"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="46"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="53"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="62"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="72"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="83"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="88"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="102"/>
      <end position="106"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="112"/>
      <end position="115"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="20421606"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="20980243"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22403819"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0007829" key="5">
    <source>
      <dbReference type="PDB" id="6Y2P"/>
    </source>
  </evidence>
  <sequence length="123" mass="13665" checksum="721D9EDE233E8D12" modified="2000-12-01" version="2">MFEITGINVSGALKAVVMATGFENPLSSVNEIETKLSALLGSETTGEILFDLLCANGPEWNRFVTLEMKYGRIMLDTAKIIDEQDVPTHILSKLTFTLRNHPEYLEASVLSPDDVRQVLSMDF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>