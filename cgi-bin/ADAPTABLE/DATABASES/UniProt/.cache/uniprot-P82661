<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-09-26" modified="2024-11-27" version="32" xmlns="http://uniprot.org/uniprot">
  <accession>P82661</accession>
  <name>FAR5_PANRE</name>
  <protein>
    <recommendedName>
      <fullName>FMRFamide-like neuropeptide PF5</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>AMRNALVRF-amide</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Panagrellus redivivus</name>
    <name type="common">Microworm</name>
    <dbReference type="NCBI Taxonomy" id="6233"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Nematoda</taxon>
      <taxon>Chromadorea</taxon>
      <taxon>Rhabditida</taxon>
      <taxon>Tylenchina</taxon>
      <taxon>Panagrolaimomorpha</taxon>
      <taxon>Panagrolaimoidea</taxon>
      <taxon>Panagrolaimidae</taxon>
      <taxon>Panagrellus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2000-07" db="UniProtKB">
      <title>Isolation, characterization and pharmacology of FMRFamide-related peptides (FaRPs) from free-living nematode, Panagrellus redivivus.</title>
      <authorList>
        <person name="Moffet C.L."/>
        <person name="Marks N.J."/>
        <person name="Halton D.W."/>
        <person name="Thomson D.P."/>
        <person name="Geary T.G."/>
        <person name="Maule A.G."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>AMIDATION AT PHE-9</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Myoactive.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the FARP (FMRFamide related peptide) family.</text>
  </comment>
  <dbReference type="Proteomes" id="UP000492821">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043710" description="FMRFamide-like neuropeptide PF5">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="9" mass="1077" checksum="A0D112C72DD45406" modified="2001-09-26" version="1">AMRNALVRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>