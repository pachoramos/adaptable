<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2021-06-02" modified="2024-11-27" version="10" xmlns="http://uniprot.org/uniprot">
  <accession>P0DUN7</accession>
  <name>PA2H_LACMU</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Basic phospholipase A2 homolog LmutTX</fullName>
      <shortName>svPLA2 homolog</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">LmmV toxin</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Lys49 PLA2-like</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Lachesis muta muta</name>
    <name type="common">Bushmaster</name>
    <dbReference type="NCBI Taxonomy" id="8753"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Viperidae</taxon>
      <taxon>Crotalinae</taxon>
      <taxon>Lachesis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2018" name="Basic Clin. Pharmacol. Toxicol." volume="122" first="413" last="423">
      <title>Identification of the molecular determinants of the antibacterial activity of LmutTX, a Lys49 phospholipase A2 homologue isolated from Lachesis muta muta snake venom (Linnaeus, 1766).</title>
      <authorList>
        <person name="Diniz-Sousa R."/>
        <person name="Caldeira C.A.S."/>
        <person name="Kayano A.M."/>
        <person name="Paloschi M.V."/>
        <person name="Pimenta D.C."/>
        <person name="Simoes-Silva R."/>
        <person name="Ferreira A.S."/>
        <person name="Zanchi F.B."/>
        <person name="Matos N.B."/>
        <person name="Grabner F.P."/>
        <person name="Calderon L.A."/>
        <person name="Zuliani J.P."/>
        <person name="Soares A.M."/>
      </authorList>
      <dbReference type="PubMed" id="29067765"/>
      <dbReference type="DOI" id="10.1111/bcpt.12921"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SYNTHESIS</scope>
    <scope>3D-STRUCTURE MODELING</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Snake venom phospholipase A2 homolog that lacks enzymatic activity (PubMed:29067765). Shows moderate cytotoxicity against C2C12 myotubes (activity above 200 ug/mL) (PubMed:29067765). Also shows antibacterial activity against both Gram-positive and Gram-negative bacteria (PubMed:29067765). A model of myotoxic mechanism has been proposed: an apo Lys49-PLA2 is activated by the entrance of a hydrophobic molecule (e.g. fatty acid) at the hydrophobic channel of the protein leading to a reorientation of a monomer (By similarity). This reorientation causes a transition between 'inactive' to 'active' states, causing alignment of C-terminal and membrane-docking sites (MDoS) side-by-side and putting the membrane-disruption sites (MDiS) in the same plane, exposed to solvent and in a symmetric position for both monomers (By similarity). The MDoS region stabilizes the toxin on membrane by the interaction of charged residues with phospholipid head groups (By similarity). Subsequently, the MDiS region destabilizes the membrane with penetration of hydrophobic residues (By similarity). This insertion causes a disorganization of the membrane, allowing an uncontrolled influx of ions (i.e. calcium and sodium), and eventually triggering irreversible intracellular alterations and cell death (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="5">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="13889.0" method="MALDI" evidence="2"/>
  <comment type="miscellaneous">
    <text evidence="2">This protein corresponds to 0.8% of the venom protein content.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the phospholipase A2 family. Group II subfamily. K49 sub-subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="4">Does not bind calcium as one of the calcium-binding sites is lost (Asp-&gt;Lys in position 48, which corresponds to 'Lys-49' in the current nomenclature).</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DUN7"/>
  <dbReference type="SMR" id="P0DUN7"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0047498">
    <property type="term" value="F:calcium-dependent phospholipase A2 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005543">
    <property type="term" value="F:phospholipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050482">
    <property type="term" value="P:arachidonic acid secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016042">
    <property type="term" value="P:lipid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042130">
    <property type="term" value="P:negative regulation of T cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006644">
    <property type="term" value="P:phospholipid metabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00125">
    <property type="entry name" value="PLA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.90.10:FF:000001">
    <property type="entry name" value="Basic phospholipase A2 homolog"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.90.10">
    <property type="entry name" value="Phospholipase A2 domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001211">
    <property type="entry name" value="PLipase_A2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033112">
    <property type="entry name" value="PLipase_A2_Asp_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016090">
    <property type="entry name" value="PLipase_A2_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036444">
    <property type="entry name" value="PLipase_A2_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033113">
    <property type="entry name" value="PLipase_A2_His_AS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716:SF57">
    <property type="entry name" value="GROUP IID SECRETORY PHOSPHOLIPASE A2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716">
    <property type="entry name" value="PHOSPHOLIPASE A2 FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00068">
    <property type="entry name" value="Phospholip_A2_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00389">
    <property type="entry name" value="PHPHLIPASEA2"/>
  </dbReference>
  <dbReference type="SMART" id="SM00085">
    <property type="entry name" value="PA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48619">
    <property type="entry name" value="Phospholipase A2, PLA2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00119">
    <property type="entry name" value="PA2_ASP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00118">
    <property type="entry name" value="PA2_HIS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0959">Myotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000452898" description="Basic phospholipase A2 homolog LmutTX" evidence="2">
    <location>
      <begin position="1"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="26"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="28"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="49"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="50"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="57"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="75"/>
      <end position="86"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="I6L8L6"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="29067765"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="29067765"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="29067765"/>
    </source>
  </evidence>
  <sequence length="120" mass="13538" checksum="5B894F3B5E9D749A" modified="2021-06-02" version="1">SLVELGKMILQETGKNPVTSYGAYGCNCGVLGSGKPKDATDRCCYVHKCCYKKLTDCDPKKDRYSYSWKDKTIVCGENNSCLKELCECDKAVAICLRENLDTYNKKYNYLKPFCKKADPC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>