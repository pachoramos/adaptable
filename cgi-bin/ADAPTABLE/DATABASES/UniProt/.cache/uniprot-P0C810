<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-07-22" modified="2024-07-24" version="34" xmlns="http://uniprot.org/uniprot">
  <accession>P0C810</accession>
  <name>PSMA3_STAAM</name>
  <protein>
    <recommendedName>
      <fullName>Phenol-soluble modulin alpha 3 peptide</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">psmA3</name>
    <name type="ordered locus">SAV0451.2</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain Mu50 / ATCC 700699)</name>
    <dbReference type="NCBI Taxonomy" id="158878"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Lancet" volume="357" first="1225" last="1240">
      <title>Whole genome sequencing of meticillin-resistant Staphylococcus aureus.</title>
      <authorList>
        <person name="Kuroda M."/>
        <person name="Ohta T."/>
        <person name="Uchiyama I."/>
        <person name="Baba T."/>
        <person name="Yuzawa H."/>
        <person name="Kobayashi I."/>
        <person name="Cui L."/>
        <person name="Oguchi A."/>
        <person name="Aoki K."/>
        <person name="Nagai Y."/>
        <person name="Lian J.-Q."/>
        <person name="Ito T."/>
        <person name="Kanamori M."/>
        <person name="Matsumaru H."/>
        <person name="Maruyama A."/>
        <person name="Murakami H."/>
        <person name="Hosoyama A."/>
        <person name="Mizutani-Ui Y."/>
        <person name="Takahashi N.K."/>
        <person name="Sawano T."/>
        <person name="Inoue R."/>
        <person name="Kaito C."/>
        <person name="Sekimizu K."/>
        <person name="Hirakawa H."/>
        <person name="Kuhara S."/>
        <person name="Goto S."/>
        <person name="Yabuzaki J."/>
        <person name="Kanehisa M."/>
        <person name="Yamashita A."/>
        <person name="Oshima K."/>
        <person name="Furuya K."/>
        <person name="Yoshino C."/>
        <person name="Shiba T."/>
        <person name="Hattori M."/>
        <person name="Ogasawara N."/>
        <person name="Hayashi H."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="11418146"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(00)04403-2"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Mu50 / ATCC 700699</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Peptide which can recruit, activate and subsequently lyse human neutrophils, thus eliminating the main cellular defense against infection.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the phenol-soluble modulin alpha peptides family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000017">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_014373779.1">
    <property type="nucleotide sequence ID" value="NC_002758.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0C810"/>
  <dbReference type="SMR" id="P0C810"/>
  <dbReference type="GeneID" id="66840985"/>
  <dbReference type="Proteomes" id="UP000002481">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031429">
    <property type="entry name" value="PSM_alpha"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR053383">
    <property type="entry name" value="PSM_alpha_peptides"/>
  </dbReference>
  <dbReference type="NCBIfam" id="NF033426">
    <property type="entry name" value="PSM_alpha_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17063">
    <property type="entry name" value="PSMalpha"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="peptide" id="PRO_0000345065" description="Phenol-soluble modulin alpha 3 peptide">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A9JX07"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="22" mass="2607" checksum="66313C34F671C8C7" modified="2008-07-22" version="1">MEFVAKLFKFFKDLLGKFLGNN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>