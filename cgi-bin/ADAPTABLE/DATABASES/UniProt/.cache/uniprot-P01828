<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="76" xmlns="http://uniprot.org/uniprot">
  <accession>P01828</accession>
  <name>HV2B_RABIT</name>
  <protein>
    <recommendedName>
      <fullName>Ig heavy chain V-A2 region K-25</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Oryctolagus cuniculus</name>
    <name type="common">Rabbit</name>
    <dbReference type="NCBI Taxonomy" id="9986"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Lagomorpha</taxon>
      <taxon>Leporidae</taxon>
      <taxon>Oryctolagus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1975" name="Biochem. J." volume="147" first="235" last="247">
      <title>Comparison of the amino acid sequences of the variable domains of two homogeneous rabbit antibodies to type III pneumococcal polysaccharide.</title>
      <authorList>
        <person name="Jaton J.-C."/>
      </authorList>
      <dbReference type="PubMed" id="241319"/>
      <dbReference type="DOI" id="10.1042/bj1470235"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
  </reference>
  <comment type="miscellaneous">
    <text>This chain was obtained from antibody to type III pneumococci and was isolated from the serum of a single rabbit.</text>
  </comment>
  <dbReference type="PIR" id="A02104">
    <property type="entry name" value="GARB2K"/>
  </dbReference>
  <dbReference type="InParanoid" id="P01828"/>
  <dbReference type="Proteomes" id="UP000001811">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0072562">
    <property type="term" value="C:blood microparticle"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019814">
    <property type="term" value="C:immunoglobulin complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003823">
    <property type="term" value="F:antigen binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016064">
    <property type="term" value="P:immunoglobulin mediated immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.40.10:FF:001878">
    <property type="entry name" value="Immunoglobulin heavy variable 1-4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003599">
    <property type="entry name" value="Ig_sub"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013106">
    <property type="entry name" value="Ig_V-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050199">
    <property type="entry name" value="IgHV"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23266:SF269">
    <property type="entry name" value="IG HEAVY CHAIN V REGION MC101-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23266">
    <property type="entry name" value="IMMUNOGLOBULIN HEAVY CHAIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07686">
    <property type="entry name" value="V-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00409">
    <property type="entry name" value="IG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00406">
    <property type="entry name" value="IGv"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-1280">Immunoglobulin</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000059937" description="Ig heavy chain V-A2 region K-25">
    <location>
      <begin position="1"/>
      <end position="117" status="greater than"/>
    </location>
  </feature>
  <feature type="domain" description="Ig-like">
    <location>
      <begin position="1"/>
      <end position="106"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="2">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 2">
    <location>
      <begin position="21"/>
      <end position="91"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="117"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00114"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="241319"/>
    </source>
  </evidence>
  <sequence length="117" mass="12580" checksum="28DD87FDB7AEE9B8" modified="1986-07-21" version="1">QSVKESEGGLFKPTDTLTLTCTVSGFSLSGYDMSWVRQAPGKGLEWIGVIYASGSTYYATWAKSRSTITRTSBTVBLMDSLTAQDTATYFCARGHTGLSYLKSSVDVWGPGTLVTVS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>