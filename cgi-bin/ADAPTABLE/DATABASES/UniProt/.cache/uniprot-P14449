<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-01-01" modified="2023-11-08" version="50" xmlns="http://uniprot.org/uniprot">
  <accession>P14449</accession>
  <name>FIBA_EQUAS</name>
  <protein>
    <recommendedName>
      <fullName>Fibrinogen alpha chain</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Fibrinopeptide A</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">FGA</name>
  </gene>
  <organism>
    <name type="scientific">Equus asinus</name>
    <name type="common">Donkey</name>
    <name type="synonym">Equus africanus asinus</name>
    <dbReference type="NCBI Taxonomy" id="9793"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Perissodactyla</taxon>
      <taxon>Equidae</taxon>
      <taxon>Equus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1965" name="Acta Chem. Scand." volume="19" first="1789" last="1791">
      <title>Studies on fibrinopeptides from mammals.</title>
      <authorList>
        <person name="Blombaeck B."/>
        <person name="Blombaeck M."/>
        <person name="Grondahl N.J."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Cleaved by the protease thrombin to yield monomers which, together with fibrinogen beta (FGB) and fibrinogen gamma (FGG), polymerize to form an insoluble fibrin matrix. Fibrin has a major function in hemostasis as one of the primary components of blood clots. In addition, functions during the early stages of wound repair to stabilize the lesion and guide cell migration during re-epithelialization. Was originally thought to be essential for platelet aggregation, based on in vitro studies using anticoagulated blood. However, subsequent studies have shown that it is not absolutely required for thrombus formation in vivo. Enhances expression of SELP in activated platelets via an ITGB3-dependent pathway. Maternal fibrinogen is essential for successful pregnancy. Fibrin deposition is also associated with infection, where it protects against IFNG-mediated hemorrhage. May also facilitate the immune response via both innate and T-cell mediated pathways.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Heterohexamer; disulfide linked. Contains 2 sets of 3 non-identical chains (alpha, beta and gamma). The 2 heterotrimers are in head to head conformation with the N-termini in a small central domain (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="2">A long coiled coil structure formed by 3 polypeptide chains connects the central nodule to the C-terminal domains (distal nodules). The long C-terminal ends of the alpha chains fold back, contributing a fourth strand to the coiled coil structure.</text>
  </comment>
  <comment type="PTM">
    <text>Conversion of fibrinogen to fibrin is triggered by thrombin, which cleaves fibrinopeptides A and B from alpha and beta chains, and thus exposes the N-terminal polymerization sites responsible for the formation of the soft clot. The soft clot is converted into the hard clot by factor XIIIA which catalyzes the epsilon-(gamma-glutamyl)lysine cross-linking between gamma chains (stronger) and between alpha chains (weaker) of different monomers.</text>
  </comment>
  <comment type="PTM">
    <text>Forms F13A-mediated cross-links between a glutamine and the epsilon-amino group of a lysine residue, forming fibronectin-fibrinogen heteropolymers.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P14449"/>
  <dbReference type="Proteomes" id="UP000694387">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002250">
    <property type="term" value="P:adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007596">
    <property type="term" value="P:blood coagulation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-0094">Blood coagulation</keyword>
  <keyword id="KW-0175">Coiled coil</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0356">Hemostasis</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000009017" description="Fibrinopeptide A">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="E9PV24"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P02671"/>
    </source>
  </evidence>
  <sequence length="16" mass="1696" checksum="09598EB63C2A5957" modified="1990-01-01" version="1" fragment="single">TKTEEGEFISEGGGVR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>