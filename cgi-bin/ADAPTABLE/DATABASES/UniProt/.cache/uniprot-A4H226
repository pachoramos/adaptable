<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-29" modified="2024-11-27" version="42" xmlns="http://uniprot.org/uniprot">
  <accession>A4H226</accession>
  <accession>A4H231</accession>
  <name>DB119_PONPY</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 119</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Beta-defensin 120</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin, beta 119</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin, beta 120</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB119</name>
    <name type="synonym">DEFB120</name>
  </gene>
  <organism>
    <name type="scientific">Pongo pygmaeus</name>
    <name type="common">Bornean orangutan</name>
    <dbReference type="NCBI Taxonomy" id="9600"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Pongo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2006-11" db="EMBL/GenBank/DDBJ databases">
      <title>Evolution and sequence variation of human beta-defensin genes.</title>
      <authorList>
        <person name="Hollox E.J."/>
        <person name="Armour J.A.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] (ISOFORMS 1 AND 2)</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>A4H226-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>A4H226-2</id>
      <name>2</name>
      <sequence type="described" ref="VSP_029876"/>
    </isoform>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AM410131">
    <property type="protein sequence ID" value="CAL68946.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AM410136">
    <property type="protein sequence ID" value="CAL68951.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A4H226"/>
  <dbReference type="SMR" id="A4H226"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001530">
    <property type="term" value="F:lipopolysaccharide binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061760">
    <property type="term" value="P:antifungal innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR028060">
    <property type="entry name" value="Defensin_big_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47902">
    <property type="entry name" value="BETA-DEFENSIN 119"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47902:SF1">
    <property type="entry name" value="BETA-DEFENSIN 119"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF14862">
    <property type="entry name" value="Defensin_big"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000289833" description="Beta-defensin 119">
    <location>
      <begin position="22"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="28"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="35"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="39"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_029876" description="In isoform 2." evidence="3">
    <original>GKRYILRCMGNSGICRASCKRNEQPYLYCKNYQSCCLQSYMRISISGKEENTDWSYEKQWPKLP</original>
    <variation>VECWMDGHCRLLCKDGEDSIIRCRNRKRCCVPSHYLTIQPVTIHGILGWTTPQMPTTAPKAKRNITNR</variation>
    <location>
      <begin position="21"/>
      <end position="84"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="84" mass="9908" checksum="0BAC70741B7AA7BF" modified="2007-05-01" version="1" precursor="true">MKLLYLFLAILLVIEEPVISGKRYILRCMGNSGICRASCKRNEQPYLYCKNYQSCCLQSYMRISISGKEENTDWSYEKQWPKLP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>