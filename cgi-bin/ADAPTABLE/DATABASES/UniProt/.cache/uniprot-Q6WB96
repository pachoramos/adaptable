<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-06-15" modified="2024-05-29" version="62" xmlns="http://uniprot.org/uniprot">
  <accession>Q6WB96</accession>
  <name>M22_HMPVC</name>
  <protein>
    <recommendedName>
      <fullName>Protein M2-2</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">M2</name>
  </gene>
  <organism>
    <name type="scientific">Human metapneumovirus (strain CAN97-83)</name>
    <name type="common">HMPV</name>
    <dbReference type="NCBI Taxonomy" id="694067"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Orthornavirae</taxon>
      <taxon>Negarnaviricota</taxon>
      <taxon>Haploviricotina</taxon>
      <taxon>Monjiviricetes</taxon>
      <taxon>Mononegavirales</taxon>
      <taxon>Pneumoviridae</taxon>
      <taxon>Metapneumovirus</taxon>
      <taxon>Metapneumovirus hominis</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="2003" name="Virology" volume="315" first="1" last="9">
      <title>Genetic diversity between human metapneumovirus subgroups.</title>
      <authorList>
        <person name="Biacchesi S."/>
        <person name="Skiadopoulos M.H."/>
        <person name="Boivin G."/>
        <person name="Hanson C.T."/>
        <person name="Murphy B.R."/>
        <person name="Collins P.L."/>
        <person name="Buchholz U.J."/>
      </authorList>
      <dbReference type="PubMed" id="14592754"/>
      <dbReference type="DOI" id="10.1016/s0042-6822(03)00528-2"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="J. Virol." volume="79" first="15114" last="15122">
      <title>Chimeric recombinant human metapneumoviruses with the nucleoprotein or phosphoprotein open reading frame replaced by that of avian metapneumovirus exhibit improved growth in vitro and attenuation in vivo.</title>
      <authorList>
        <person name="Pham Q.N."/>
        <person name="Biacchesi S."/>
        <person name="Skiadopoulos M.H."/>
        <person name="Murphy B.R."/>
        <person name="Collins P.L."/>
        <person name="Buchholz U.J."/>
      </authorList>
      <dbReference type="PubMed" id="16306583"/>
      <dbReference type="DOI" id="10.1128/jvi.79.24.15114-15122.2005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2012" name="J. Virol." volume="86" first="13049" last="13061">
      <title>Human metapneumovirus M2-2 protein inhibits innate cellular signaling by targeting MAVS.</title>
      <authorList>
        <person name="Ren J."/>
        <person name="Wang Q."/>
        <person name="Kolli D."/>
        <person name="Prusak D.J."/>
        <person name="Tseng C.T."/>
        <person name="Chen Z.J."/>
        <person name="Li K."/>
        <person name="Wood T.G."/>
        <person name="Bao X."/>
      </authorList>
      <dbReference type="PubMed" id="23015697"/>
      <dbReference type="DOI" id="10.1128/jvi.01248-12"/>
    </citation>
    <scope>INTERACTION WITH HOST MAVS</scope>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2014" name="PLoS ONE" volume="9" first="e91865" last="e91865">
      <title>Human metapneumovirus M2-2 protein inhibits innate immune response in monocyte-derived dendritic cells.</title>
      <authorList>
        <person name="Ren J."/>
        <person name="Liu G."/>
        <person name="Go J."/>
        <person name="Kolli D."/>
        <person name="Zhang G."/>
        <person name="Bao X."/>
      </authorList>
      <dbReference type="PubMed" id="24618691"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0091865"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH HOST MYD88</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2017" name="J. Virol." volume="91">
      <title>Human Metapneumovirus M2-2 Protein Acts as a Negative Regulator of Alpha Interferon Production by Plasmacytoid Dendritic Cells.</title>
      <authorList>
        <person name="Kitagawa Y."/>
        <person name="Sakai M."/>
        <person name="Funayama M."/>
        <person name="Itoh M."/>
        <person name="Gotoh B."/>
      </authorList>
      <dbReference type="PubMed" id="28768858"/>
      <dbReference type="DOI" id="10.1128/jvi.00579-17"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH HOST IRF7</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2022" name="Front. Immunol." volume="13" first="970750" last="970750">
      <title>Human metapneumovirus M2-2 protein inhibits RIG-I signaling by preventing TRIM25-mediated RIG-I ubiquitination.</title>
      <authorList>
        <person name="Tanaka Y."/>
        <person name="Morita N."/>
        <person name="Kitagawa Y."/>
        <person name="Gotoh B."/>
        <person name="Komatsu T."/>
      </authorList>
      <dbReference type="PubMed" id="36045682"/>
      <dbReference type="DOI" id="10.3389/fimmu.2022.970750"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH HOST TRIM25</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3 4 5">Mediates the regulatory switch from transcription to RNA replication (By similarity). Acts late in infection by inhibiting viral transcription and up-regulating RNA replication (By similarity). Plays a major role in antagonizing the type I IFN-mediated antiviral response (PubMed:23015697). Interacts with host MAVS and prevents the interaction with its upstream partner RIGI in the signaling pathway leading to interferon production (PubMed:23015697). Inhibits host RIGI signaling by preventing TRIM25-mediated RIGI ubiquitination (PubMed:36045682). Inhibits also viral-induced cytokine production in dendritic cells by targeting MYD88 thereby inhibiting MYD88-dependent gene transcription (PubMed:24618691). Inhibits also the phosphorylation of host IRF7 to prevent type I interferon production in dendritic cells (PubMed:28768858).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2 3 4 5">Interacts with host MAVS; this interaction inhibits signaling pathway leading to interferon production (PubMed:23015697). Interacts with host MYD88; this interaction inhibits innate response in dendritic cells (PubMed:24618691). Interacts with TRIM25 (PubMed:36045682). Interacts with IRF7 (PubMed:28768858).</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6863628">
      <id>Q6WB96</id>
    </interactant>
    <interactant intactId="EBI-995373">
      <id>Q7Z434</id>
      <label>MAVS</label>
    </interactant>
    <organismsDiffer>true</organismsDiffer>
    <experiments>4</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the metapneumovirus M2-2 protein family.</text>
  </comment>
  <dbReference type="EMBL" id="AY297749">
    <property type="protein sequence ID" value="AAQ67697.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="YP_012610.1">
    <property type="nucleotide sequence ID" value="NC_004148.2"/>
  </dbReference>
  <dbReference type="IntAct" id="Q6WB96">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000001398">
    <property type="component" value="Segment"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030430">
    <property type="term" value="C:host cell cytoplasm"/>
    <property type="evidence" value="ECO:0000305"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0140311">
    <property type="term" value="F:protein sequestering activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039532">
    <property type="term" value="P:negative regulation of cytoplasmic pattern recognition receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032480">
    <property type="term" value="P:negative regulation of type I interferon production"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039545">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of MAVS activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1097">Inhibition of host MAVS by virus</keyword>
  <keyword id="KW-1113">Inhibition of host RLR pathway by virus</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <keyword id="KW-0693">Viral RNA replication</keyword>
  <feature type="chain" id="PRO_0000394813" description="Protein M2-2">
    <location>
      <begin position="1"/>
      <end position="71"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P88812"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="23015697"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="24618691"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="28768858"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="36045682"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="71" mass="8189" checksum="0887B853B81A8125" modified="2004-07-05" version="1">MTLHMPCKTVKALIKCSEHGPVFITIEVDEMIWTQKELKEALSDGIVKSHTNIYNCYLENIEIIYVKAYLS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>