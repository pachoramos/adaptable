<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2024-01-24" version="67" xmlns="http://uniprot.org/uniprot">
  <accession>O33690</accession>
  <name>CSP2_STROR</name>
  <protein>
    <recommendedName>
      <fullName>Competence-stimulating peptide</fullName>
      <shortName>CSP</shortName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">comC</name>
  </gene>
  <organism>
    <name type="scientific">Streptococcus oralis</name>
    <dbReference type="NCBI Taxonomy" id="1303"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Streptococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="J. Bacteriol." volume="179" first="6589" last="6594">
      <title>Natural competence in the genus Streptococcus: evidence that streptococci can change pherotype by interspecies recombinational exchanges.</title>
      <authorList>
        <person name="Haevarstein L.S."/>
        <person name="Hakenbeck R."/>
        <person name="Gaustad P."/>
      </authorList>
      <dbReference type="PubMed" id="9352904"/>
      <dbReference type="DOI" id="10.1128/jb.179.21.6589-6594.1997"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>DSM 20066 / 25826</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Acts as a pheromone, induces cells to develop competence for genetic transformation.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the ComC family.</text>
  </comment>
  <dbReference type="EMBL" id="AJ000874">
    <property type="protein sequence ID" value="CAA04364.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000799678.1">
    <property type="nucleotide sequence ID" value="NZ_VOXA01000097.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O33690"/>
  <dbReference type="STRING" id="1303.SORDD17_01790"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005186">
    <property type="term" value="F:pheromone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030420">
    <property type="term" value="P:establishment of competence for transformation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010133">
    <property type="entry name" value="Bacteriocin_signal_seq"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004288">
    <property type="entry name" value="Competence_ComC"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01847">
    <property type="entry name" value="bacteriocin_sig"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="NCBIfam" id="NF033214">
    <property type="entry name" value="ComC_Streptocco"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03047">
    <property type="entry name" value="ComC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0178">Competence</keyword>
  <keyword id="KW-0588">Pheromone</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="propeptide" id="PRO_0000005879" evidence="1">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000005880" description="Competence-stimulating peptide">
    <location>
      <begin position="25"/>
      <end position="41"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="41" mass="4961" checksum="B202A064BCD451AD" modified="1998-01-01" version="1" precursor="true">MKNTVKLEQFKEVTEAELQEIRGGDWRISETIRNLIFPRRK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>