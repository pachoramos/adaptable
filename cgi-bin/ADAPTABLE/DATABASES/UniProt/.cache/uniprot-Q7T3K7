<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-04-22" modified="2024-01-24" version="68" xmlns="http://uniprot.org/uniprot">
  <accession>Q7T3K7</accession>
  <name>DRS8_PHYSA</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Dermaseptin-S8</fullName>
      <shortName evidence="5">DRS-S8</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Dermaseptin DS VIII</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Phyllomedusa sauvagei</name>
    <name type="common">Sauvage's leaf frog</name>
    <dbReference type="NCBI Taxonomy" id="8395"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phyllomedusa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Regul. Pept." volume="116" first="139" last="146">
      <title>Identification of three novel Phyllomedusa sauvagei dermaseptins (sVI-sVIII) by cloning from a skin secretion-derived cDNA library.</title>
      <authorList>
        <person name="Chen T."/>
        <person name="Tang L."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="14599725"/>
      <dbReference type="DOI" id="10.1016/j.regpep.2003.08.001"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 46-76</scope>
    <scope>AMIDATION AT GLN-76</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="Peptides" volume="29" first="2074" last="2082">
      <title>A consistent nomenclature of antimicrobial peptides isolated from frogs of the subfamily Phyllomedusinae.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="18644413"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.06.017"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Potent antimicrobial peptide with activity against bacteria, fungi and protozoa. Probably acts by disturbing membrane functions with its amphipathic structure.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=0935"/>
  </comment>
  <dbReference type="EMBL" id="AJ564793">
    <property type="protein sequence ID" value="CAD92231.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q7T3K7"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022731">
    <property type="entry name" value="Dermaseptin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016322">
    <property type="entry name" value="FSAP"/>
  </dbReference>
  <dbReference type="Pfam" id="PF12121">
    <property type="entry name" value="DD_K"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001822">
    <property type="entry name" value="Dermaseptin_precursor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0175">Coiled coil</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000449587" evidence="3">
    <location>
      <begin position="23"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5004291442" description="Dermaseptin-S8" evidence="3">
    <location>
      <begin position="46"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000449588" evidence="3">
    <location>
      <begin position="78"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glutamine amide" evidence="3">
    <location>
      <position position="76"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P24302"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="14599725"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="14599725"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="14599725"/>
    </source>
  </evidence>
  <sequence length="79" mass="8750" checksum="F8ADE66A144F8004" modified="2003-10-01" version="1" precursor="true">MDILKKSLFLVLFLGLVSLSICEEEKRENEDEEKQEDDEQSEMKRALWKTMLKKLGTVALHAGKAALGAAADTISQGAQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>