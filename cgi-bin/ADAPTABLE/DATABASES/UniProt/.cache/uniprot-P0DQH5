<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-06-05" modified="2024-11-27" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>P0DQH5</accession>
  <name>SCX1A_TITFA</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Beta-toxin Tf1a</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Tityus fasciolatus</name>
    <name type="common">Central Brazilian scorpion</name>
    <dbReference type="NCBI Taxonomy" id="203543"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Tityus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2018" name="Toxins" volume="10" first="1" last="17">
      <title>Subtype specificity of beta-toxin Tf1a from Tityus fasciolatus in voltage gated sodium channels.</title>
      <authorList>
        <person name="Mata D.O.D."/>
        <person name="Tibery D.V."/>
        <person name="Campos L.A."/>
        <person name="Camargos T.S."/>
        <person name="Peigneur S."/>
        <person name="Tytgat J."/>
        <person name="Schwartz E.F."/>
      </authorList>
      <dbReference type="PubMed" id="30131471"/>
      <dbReference type="DOI" id="10.3390/toxins10090339"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 30-46 AND 59-73</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT CYS-81</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Beta toxins bind voltage-independently at site-4 of sodium channels (Nav) and shift the voltage of activation toward more negative potentials thereby affecting sodium channel activation and promoting spontaneous and repetitive firing. The toxin induces a leftward shift, on all channels tested (including Blattella germanica and Varroa destructor Nav1), displacing a change in voltage dependence activation to more hyperpolarized potentials (PubMed:30131471). In addition, the toxin mostly inhibits peak current of hNav1.4/SCN4A (53% inhibition of peak current at 100 nM) and hNav1.5/SCN5A (71% inhibition) (PubMed:30131471).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="5">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="mass spectrometry" mass="6927.5" method="MALDI" evidence="3"/>
  <comment type="miscellaneous">
    <text evidence="3">Negative results: shows a weak peak current inhibition of Nav1.1/SCN1A, Nav1.2/SCN2A, Nav1.3/SCN3A, Nav1.6/SCN8A, Nav1.7/SCN9A (17-30% inhibition of peak current at 100 nM). It does not inhibit peak current on insect (BgNaV1 from Blattella germanica) and arachnidan (VdNaV1 from Varroa destructor) sodium channel subtypes (tested at 100 nM) (PubMed:30131471).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DQH5"/>
  <dbReference type="SMR" id="P0DQH5"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000447418" description="Beta-toxin Tf1a" evidence="6">
    <location>
      <begin position="21"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="2">
    <location>
      <begin position="21"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="modified residue" description="Cysteine amide" evidence="3">
    <location>
      <position position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="31"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="35"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="43"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="47"/>
      <end position="64"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="30131471"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="30131471"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="30131471"/>
    </source>
  </evidence>
  <sequence length="84" mass="9426" checksum="A24B6FCA68468136" modified="2019-06-05" version="1" precursor="true">MKGMILFISCLLLIGIVVECKEGYLMDHEGCKLSCFIRPSGYCGSECKIKKGSSGYCAWPACYCYGLPNWVKVWERATNRCGKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>