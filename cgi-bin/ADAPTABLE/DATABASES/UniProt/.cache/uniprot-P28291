<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1992-12-01" modified="2024-11-27" version="147" xmlns="http://uniprot.org/uniprot">
  <accession>P28291</accession>
  <accession>A9QWP7</accession>
  <name>CCL2_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>C-C motif chemokine 2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Acidic seminal fluid protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Monocyte chemotactic protein 1A</fullName>
      <shortName>MCP-1</shortName>
      <shortName>MCP-1A</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Small-inducible cytokine A2</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CCL2</name>
    <name type="synonym">SCYA2</name>
  </gene>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="DNA Cell Biol." volume="10" first="671" last="679">
      <title>Gene expression and cDNA cloning identified a major basic protein constituent of bovine seminal plasma as bovine monocyte-chemoattractant protein-1 (MCP-1).</title>
      <authorList>
        <person name="Wempe F."/>
        <person name="Henschen A."/>
        <person name="Scheit K.H."/>
      </authorList>
      <dbReference type="PubMed" id="1721821"/>
      <dbReference type="DOI" id="10.1089/dna.1991.10.671"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Seminal plasma</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1992" name="Biochem. Biophys. Res. Commun." volume="183" first="232" last="237">
      <title>Characterization by cDNA cloning of the mRNA of a new growth factor from bovine seminal plasma: acidic seminal fluid protein.</title>
      <authorList>
        <person name="Wempe F."/>
        <person name="Einspanier R."/>
        <person name="Scheit K.H."/>
      </authorList>
      <dbReference type="PubMed" id="1543494"/>
      <dbReference type="DOI" id="10.1016/0006-291x(92)91633-2"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Seminal plasma</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1994" name="Biochem. Biophys. Res. Commun." volume="202" first="1272" last="1279">
      <title>Characterization of the bovine monocyte chemoattractant protein-1 gene.</title>
      <authorList>
        <person name="Wempe F."/>
        <person name="Kuhlmann J.K."/>
        <person name="Scheit K.H."/>
      </authorList>
      <dbReference type="PubMed" id="8060303"/>
      <dbReference type="DOI" id="10.1006/bbrc.1994.2068"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="submission" date="2007-11" db="EMBL/GenBank/DDBJ databases">
      <title>U.S. veterinary immune reagent network: expressed bovine gene sequences.</title>
      <authorList>
        <consortium name="U.S. Veterinary Immune Reagent Network"/>
        <person name="Hudgens T."/>
        <person name="Tompkins D."/>
        <person name="Baldwin C.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Belted Galloway</strain>
      <tissue>Peripheral blood</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 3">Acts as a ligand for C-C chemokine receptor CCR2 (By similarity). Signals through binding and activation of CCR2 and induces a strong chemotactic response and mobilization of intracellular calcium ions (By similarity). Exhibits a chemotactic activity for monocytes and basophils but not neutrophils or eosinophils (By similarity). Plays an important role in mediating peripheral nerve injury-induced neuropathic pain (By similarity). Increases NMDA-mediated synaptic transmission in both dopamine D1 and D2 receptor-containing neurons, which may be caused by MAPK/ERK-dependent phosphorylation of GRIN2B/NMDAR2B (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="3">Monomer or homodimer; in equilibrium. Is tethered on endothelial cells by glycosaminoglycan (GAG) side chains of proteoglycans. Interacts with TNFAIP6 (via Link domain).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="PTM">
    <text evidence="3">Processing at the N-terminus can regulate receptor and target cell selectivity (By similarity). Deletion of the N-terminal residue converts it from an activator of basophil to an eosinophil chemoattractant (By similarity).</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">N-Glycosylated.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the intercrine beta (chemokine CC) family.</text>
  </comment>
  <dbReference type="EMBL" id="M84602">
    <property type="protein sequence ID" value="AAA30651.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="L32659">
    <property type="protein sequence ID" value="AAA60956.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU276059">
    <property type="protein sequence ID" value="ABX72057.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A39296">
    <property type="entry name" value="A39296"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_776431.1">
    <property type="nucleotide sequence ID" value="NM_174006.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P28291"/>
  <dbReference type="SMR" id="P28291"/>
  <dbReference type="STRING" id="9913.ENSBTAP00000013146"/>
  <dbReference type="PaxDb" id="9913-ENSBTAP00000013146"/>
  <dbReference type="Ensembl" id="ENSBTAT00000135570.1">
    <property type="protein sequence ID" value="ENSBTAP00000076954.1"/>
    <property type="gene ID" value="ENSBTAG00000060433.1"/>
  </dbReference>
  <dbReference type="GeneID" id="281043"/>
  <dbReference type="KEGG" id="bta:281043"/>
  <dbReference type="CTD" id="6347"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSBTAG00000037811"/>
  <dbReference type="eggNOG" id="ENOG502S6ZP">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT01100000263482"/>
  <dbReference type="HOGENOM" id="CLU_141716_1_0_1"/>
  <dbReference type="InParanoid" id="P28291"/>
  <dbReference type="OMA" id="PITCCYT"/>
  <dbReference type="OrthoDB" id="4265193at2759"/>
  <dbReference type="TreeFam" id="TF334888"/>
  <dbReference type="Reactome" id="R-BTA-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Chromosome 19"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSBTAG00000037811">
    <property type="expression patterns" value="Expressed in intramuscular adipose tissue and 98 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048020">
    <property type="term" value="F:CCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048245">
    <property type="term" value="P:eosinophil chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051968">
    <property type="term" value="P:positive regulation of synaptic transmission, glutamatergic"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019233">
    <property type="term" value="P:sensory perception of pain"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00272">
    <property type="entry name" value="Chemokine_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000002">
    <property type="entry name" value="C-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000827">
    <property type="entry name" value="Chemokine_CC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF98">
    <property type="entry name" value="C-C MOTIF CHEMOKINE 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00472">
    <property type="entry name" value="SMALL_CYTOKINES_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005244" description="C-C motif chemokine 2">
    <location>
      <begin position="24"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="3">
    <location>
      <position position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="34"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="35"/>
      <end position="75"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P10148"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P13500"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="99" mass="11114" checksum="0FD79FC1AB0CBE88" modified="1992-12-01" version="1" precursor="true">MKVSAALLCLLLTVAAFSTEVLAQPDAINSQVACCYTFNSKKISMQRLMNYRRVTSSKCPKEAVIFKTILGKELCADPKQKWVQDSINYLNKKNQTPKP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>