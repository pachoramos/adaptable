<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-29" modified="2024-01-24" version="78" xmlns="http://uniprot.org/uniprot">
  <accession>Q6IV18</accession>
  <accession>Q66VU2</accession>
  <name>GLL13_CHICK</name>
  <protein>
    <recommendedName>
      <fullName>Gallinacin-13</fullName>
      <shortName>Gal-13</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Beta-defensin 13</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Gallinacin-11</fullName>
      <shortName>Gal-11</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">GAL13</name>
  </gene>
  <organism>
    <name type="scientific">Gallus gallus</name>
    <name type="common">Chicken</name>
    <dbReference type="NCBI Taxonomy" id="9031"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Archelosauria</taxon>
      <taxon>Archosauria</taxon>
      <taxon>Dinosauria</taxon>
      <taxon>Saurischia</taxon>
      <taxon>Theropoda</taxon>
      <taxon>Coelurosauria</taxon>
      <taxon>Aves</taxon>
      <taxon>Neognathae</taxon>
      <taxon>Galloanserae</taxon>
      <taxon>Galliformes</taxon>
      <taxon>Phasianidae</taxon>
      <taxon>Phasianinae</taxon>
      <taxon>Gallus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="BMC Genomics" volume="5" first="56" last="56">
      <title>A genome-wide screen identifies a single beta-defensin gene cluster in the chicken: implications for the origin and evolution of mammalian defensins.</title>
      <authorList>
        <person name="Xiao Y."/>
        <person name="Hughes A.L."/>
        <person name="Ando J."/>
        <person name="Matsuda Y."/>
        <person name="Cheng J.-F."/>
        <person name="Skinner-Noble D."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="15310403"/>
      <dbReference type="DOI" id="10.1186/1471-2164-5-56"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA] (ISOFORM 1)</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Immunogenetics" volume="57" first="90" last="98">
      <title>The synthetic form of a novel chicken beta-defensin identified in silico is predominantly active against intestinal pathogens.</title>
      <authorList>
        <person name="Higgs R."/>
        <person name="Lynn D.J."/>
        <person name="Gaines S."/>
        <person name="McMahon J."/>
        <person name="Tierney J."/>
        <person name="James T."/>
        <person name="Lloyd A.T."/>
        <person name="Mulcahy G."/>
        <person name="O'Farrelly C."/>
      </authorList>
      <dbReference type="PubMed" id="15744537"/>
      <dbReference type="DOI" id="10.1007/s00251-005-0777-3"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 2)</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Gall bladder</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="submission" date="2006-07" db="EMBL/GenBank/DDBJ databases">
      <title>Chicken beta-defensin in China chicken breeds.</title>
      <authorList>
        <person name="Chen Y."/>
        <person name="Cao Y."/>
        <person name="Xie Q."/>
        <person name="Bi Y."/>
        <person name="Chen J."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 2)</scope>
    <source>
      <strain>Guangxi Huang</strain>
      <strain>Huiyang bearded</strain>
      <strain>Qingyuan Ma</strain>
      <strain>Taihe silkies</strain>
      <strain>Xinghua</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2007" name="Reproduction" volume="133" first="127" last="133">
      <title>Changes in the expression of gallinacins, antimicrobial peptides, in ovarian follicles during follicular growth and in response to lipopolysaccharide in laying hens (Gallus domesticus).</title>
      <authorList>
        <person name="Subedi K."/>
        <person name="Isobe N."/>
        <person name="Nishibori M."/>
        <person name="Yoshimura Y."/>
      </authorList>
      <dbReference type="PubMed" id="17244739"/>
      <dbReference type="DOI" id="10.1530/rep-06-0083"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="5">Has bactericidal activity. Potent activity against E.coli, L.monocytogenes, S.typhimurium and S.pyogenes but mot against S.aureus.</text>
  </comment>
  <comment type="function">
    <text evidence="1">Has bactericidal activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasmic granule</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>Q6IV18-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>Q6IV18-2</id>
      <name>2</name>
      <sequence type="described" ref="VSP_025714"/>
    </isoform>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4 5 6">Expressed in the liver, gall bladder, kidney, small intestine, spleen, testis, ovary and male and female reproductive tracts. Not detected in the ovarian stroma and the theca and granulosa layers of the ovarian follicle.</text>
  </comment>
  <comment type="similarity">
    <text evidence="9">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY621315">
    <property type="protein sequence ID" value="AAT45553.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY621328">
    <property type="protein sequence ID" value="AAT48937.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY701473">
    <property type="protein sequence ID" value="AAU07921.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ677644">
    <property type="protein sequence ID" value="ABG73378.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858310">
    <property type="protein sequence ID" value="ABI48225.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858323">
    <property type="protein sequence ID" value="ABI48239.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858336">
    <property type="protein sequence ID" value="ABI48252.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858350">
    <property type="protein sequence ID" value="ABI48266.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001001780.1">
    <molecule id="Q6IV18-1"/>
    <property type="nucleotide sequence ID" value="NM_001001780.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q6IV18"/>
  <dbReference type="SMR" id="Q6IV18"/>
  <dbReference type="STRING" id="9031.ENSGALP00000030910"/>
  <dbReference type="PaxDb" id="9031-ENSGALP00000030910"/>
  <dbReference type="Ensembl" id="ENSGALT00000031546">
    <molecule id="Q6IV18-1"/>
    <property type="protein sequence ID" value="ENSGALP00000030910"/>
    <property type="gene ID" value="ENSGALG00000019848"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00010027254.1">
    <molecule id="Q6IV18-1"/>
    <property type="protein sequence ID" value="ENSGALP00010015517.1"/>
    <property type="gene ID" value="ENSGALG00010011392.1"/>
  </dbReference>
  <dbReference type="GeneID" id="414877"/>
  <dbReference type="KEGG" id="gga:414877"/>
  <dbReference type="CTD" id="414877"/>
  <dbReference type="VEuPathDB" id="HostDB:geneid_414877"/>
  <dbReference type="eggNOG" id="ENOG502TDIW">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000165539"/>
  <dbReference type="HOGENOM" id="CLU_189296_0_0_1"/>
  <dbReference type="InParanoid" id="Q6IV18"/>
  <dbReference type="OMA" id="ERWEGSC"/>
  <dbReference type="OrthoDB" id="4668935at2759"/>
  <dbReference type="PRO" id="PR:Q6IV18"/>
  <dbReference type="Proteomes" id="UP000000539">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSGALG00000019848">
    <property type="expression patterns" value="Expressed in liver and 6 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd21908">
    <property type="entry name" value="BDD_Gal13"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388:SF9">
    <property type="entry name" value="BETA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388">
    <property type="entry name" value="BETA-DEFENSIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000288577" description="Gallinacin-13">
    <location>
      <begin position="24"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="66"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="30"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="41"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_025714" description="In isoform 2." evidence="7 8">
    <location>
      <begin position="61"/>
      <end position="89"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="15310403"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="15744537"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="17244739"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="15744537"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source ref="3"/>
  </evidence>
  <evidence type="ECO:0000305" key="9"/>
  <sequence length="89" mass="9997" checksum="CB1000E95296E558" modified="2004-07-05" version="1" precursor="true">MRILQLLFAIVVILLLQDAPARGFSDSQLCRNNHGHCRRLCFHMESWAGSCMNGRLRCCRFSTKQPFSNPKHSVLHTAEQDPSPSLGGT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>