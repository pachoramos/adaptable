<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1991-05-01" modified="2024-01-24" version="89" xmlns="http://uniprot.org/uniprot">
  <accession>P21227</accession>
  <name>CHIB_PEA</name>
  <protein>
    <recommendedName>
      <fullName>Endochitinase B</fullName>
      <ecNumber>3.2.1.14</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pisum sativum</name>
    <name type="common">Garden pea</name>
    <name type="synonym">Lathyrus oleraceus</name>
    <dbReference type="NCBI Taxonomy" id="3888"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Fabales</taxon>
      <taxon>Fabaceae</taxon>
      <taxon>Papilionoideae</taxon>
      <taxon>50 kb inversion clade</taxon>
      <taxon>NPAAA clade</taxon>
      <taxon>Hologalegina</taxon>
      <taxon>IRL clade</taxon>
      <taxon>Fabeae</taxon>
      <taxon>Pisum</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="Planta" volume="184" first="24" last="29">
      <title>Induction, purification and characterization of chitinase isolated from pea leaves inoculated with Ascochyta pisi.</title>
      <authorList>
        <person name="Vad K."/>
        <person name="Mikkelsen J.D."/>
        <person name="Collinge D.B."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <strain>cv. Birte</strain>
      <tissue>Leaf</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Defense against chitin-containing fungal pathogens.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>Random endo-hydrolysis of N-acetyl-beta-D-glucosaminide (1-&gt;4)-beta-linkages in chitin and chitodextrins.</text>
      <dbReference type="EC" id="3.2.1.14"/>
    </reaction>
  </comment>
  <comment type="induction">
    <text>By infection with the fungal pathogen Ascochyta pisi.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the glycosyl hydrolase 19 family. Chitinase class I subfamily.</text>
  </comment>
  <dbReference type="EC" id="3.2.1.14"/>
  <dbReference type="AlphaFoldDB" id="P21227"/>
  <dbReference type="GO" id="GO:0008061">
    <property type="term" value="F:chitin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004568">
    <property type="term" value="F:chitinase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006032">
    <property type="term" value="P:chitin catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000272">
    <property type="term" value="P:polysaccharide catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.60.10">
    <property type="entry name" value="Endochitinase-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001002">
    <property type="entry name" value="Chitin-bd_1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036861">
    <property type="entry name" value="Endochitinase-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00187">
    <property type="entry name" value="Chitin_bind_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57016">
    <property type="entry name" value="Plant lectins/antimicrobial peptides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0119">Carbohydrate metabolism</keyword>
  <keyword id="KW-0146">Chitin degradation</keyword>
  <keyword id="KW-0147">Chitin-binding</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0326">Glycosidase</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0624">Polysaccharide degradation</keyword>
  <feature type="chain" id="PRO_0000124829" description="Endochitinase B">
    <location>
      <begin position="1"/>
      <end position="23" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <evidence type="ECO:0000305" key="1"/>
  <sequence length="23" mass="2424" checksum="05CF70DA4261BC61" modified="1991-05-01" version="1" fragment="single">EQCGRQAGGATCPNNLCCSQYGY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>