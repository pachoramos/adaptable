<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-12-04" modified="2024-11-27" version="30" xmlns="http://uniprot.org/uniprot">
  <accession>P83200</accession>
  <name>PRDX5_VULVU</name>
  <protein>
    <recommendedName>
      <fullName>Peroxiredoxin-5</fullName>
      <ecNumber evidence="1">1.11.1.24</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Autoantigenic sperm protein 5</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Peroxiredoxin V</fullName>
      <shortName>Prx-V</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Thioredoxin peroxidase</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="5">Thioredoxin-dependent peroxiredoxin 5</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>fSP5</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">PRDX5</name>
  </gene>
  <organism>
    <name type="scientific">Vulpes vulpes</name>
    <name type="common">Red fox</name>
    <dbReference type="NCBI Taxonomy" id="9627"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Carnivora</taxon>
      <taxon>Caniformia</taxon>
      <taxon>Canidae</taxon>
      <taxon>Vulpes</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="submission" date="2001-12" db="UniProtKB">
      <title>Partial characterisation of antigenic sperm proteins in foxes (Vulpes vulpes).</title>
      <authorList>
        <person name="Verdier Y."/>
        <person name="Rouet N."/>
        <person name="Artois M."/>
        <person name="Boue F."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue evidence="3">Sperm</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2002" name="J. Androl." volume="23" first="529" last="536">
      <title>Partial characterization of antigenic sperm proteins in foxes (Vulpes vulpes).</title>
      <authorList>
        <person name="Verdier Y."/>
        <person name="Rouet N."/>
        <person name="Artois M."/>
        <person name="Boue F."/>
      </authorList>
      <dbReference type="PubMed" id="12065460"/>
    </citation>
    <scope>IDENTIFICATION BY 2D-PAGE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Thiol-specific peroxidase that catalyzes the reduction of hydrogen peroxide and organic hydroperoxides to water and alcohols, respectively. Plays a role in cell protection against oxidative stress by detoxifying peroxides and as sensor of hydrogen peroxide-mediated signaling events.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>a hydroperoxide + [thioredoxin]-dithiol = an alcohol + [thioredoxin]-disulfide + H2O</text>
      <dbReference type="Rhea" id="RHEA:62620"/>
      <dbReference type="Rhea" id="RHEA-COMP:10698"/>
      <dbReference type="Rhea" id="RHEA-COMP:10700"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:29950"/>
      <dbReference type="ChEBI" id="CHEBI:30879"/>
      <dbReference type="ChEBI" id="CHEBI:35924"/>
      <dbReference type="ChEBI" id="CHEBI:50058"/>
      <dbReference type="EC" id="1.11.1.24"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Mitochondrion</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Peroxisome matrix</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">On the 2D-gel the determined pI of this protein is: 5.8, its MW is: 16.9 kDa.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">The active site is a conserved redox-active cysteine residue, the peroxidatic cysteine (C(P)), which makes the nucleophilic attack on the peroxide substrate. The peroxide oxidizes the C(P)-SH to cysteine sulfenic acid (C(P)-SOH), which then reacts with another cysteine residue, the resolving cysteine (C(R)), to form a disulfide bridge. The disulfide is subsequently reduced by an appropriate electron donor to complete the catalytic cycle. In this atypical 2-Cys Prx, C(R) is present in the same subunit to form an intramolecular disulfide. The disulfide is subsequently reduced by thioredoxin.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the peroxiredoxin family. Prx5 subfamily.</text>
  </comment>
  <dbReference type="EC" id="1.11.1.24" evidence="1"/>
  <dbReference type="Proteomes" id="UP000286640">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005782">
    <property type="term" value="C:peroxisomal matrix"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0140824">
    <property type="term" value="F:thioredoxin-dependent peroxiredoxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0049">Antioxidant</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0496">Mitochondrion</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-0575">Peroxidase</keyword>
  <keyword id="KW-0576">Peroxisome</keyword>
  <keyword id="KW-0676">Redox-active center</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000312688" description="Peroxiredoxin-5">
    <location>
      <begin position="1"/>
      <end position="16" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="4">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P30044"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12065460"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="16" mass="1677" checksum="1138D10324279F20" modified="2007-12-04" version="1" fragment="single">APIKVGDAIPXVXVFE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>