<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-01-01" modified="2019-12-11" version="40" xmlns="http://uniprot.org/uniprot">
  <accession>P13270</accession>
  <name>LABA_JATMU</name>
  <protein>
    <recommendedName>
      <fullName>Labaditin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Jatropha multifida</name>
    <name type="common">Coralbush</name>
    <dbReference type="NCBI Taxonomy" id="3996"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Malpighiales</taxon>
      <taxon>Euphorbiaceae</taxon>
      <taxon>Crotonoideae</taxon>
      <taxon>Jatropheae</taxon>
      <taxon>Jatropha</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1989" name="FEBS Lett." volume="256" first="91" last="96">
      <title>Labaditin, a novel cyclic decapeptide from the latex of Jatropha multifida L. (Euphorbiaceae). Isolation and sequence determination by means of two-dimensional NMR.</title>
      <authorList>
        <person name="Kosasi S."/>
        <person name="van der Sluis W.G."/>
        <person name="Boelens R."/>
        <person name="T'Hart L.A."/>
        <person name="Labadie R.P."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>CROSS-LINK</scope>
    <source>
      <tissue>Latex</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Labaditin is an active peptide which inhibits the classical pathway of complement activation in vitro. Activity seems to be based on an interaction with C1.</text>
  </comment>
  <comment type="PTM">
    <text>This is a cyclic peptide.</text>
  </comment>
  <comment type="miscellaneous">
    <text>Latex of this plant is used in folkloric medicine for treatment of infected wounds, skins infections and scabies.</text>
  </comment>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <feature type="peptide" id="PRO_0000044151" description="Labaditin">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Ala-Ile)">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <sequence length="10" mass="1089" checksum="D98AAD6362D1B362" modified="1990-01-01" version="1">AGVWTVWGTI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>