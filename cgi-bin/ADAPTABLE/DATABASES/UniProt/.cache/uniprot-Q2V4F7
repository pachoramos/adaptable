<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-07-28" modified="2024-10-02" version="83" xmlns="http://uniprot.org/uniprot">
  <accession>Q2V4F7</accession>
  <name>DF278_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 278</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">At1g63522</name>
    <name type="ORF">F2K11</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Nature" volume="408" first="816" last="820">
      <title>Sequence and analysis of chromosome 1 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
        <person name="Palm C.J."/>
        <person name="Federspiel N.A."/>
        <person name="Kaul S."/>
        <person name="White O."/>
        <person name="Alonso J."/>
        <person name="Altafi H."/>
        <person name="Araujo R."/>
        <person name="Bowman C.L."/>
        <person name="Brooks S.Y."/>
        <person name="Buehler E."/>
        <person name="Chan A."/>
        <person name="Chao Q."/>
        <person name="Chen H."/>
        <person name="Cheuk R.F."/>
        <person name="Chin C.W."/>
        <person name="Chung M.K."/>
        <person name="Conn L."/>
        <person name="Conway A.B."/>
        <person name="Conway A.R."/>
        <person name="Creasy T.H."/>
        <person name="Dewar K."/>
        <person name="Dunn P."/>
        <person name="Etgu P."/>
        <person name="Feldblyum T.V."/>
        <person name="Feng J.-D."/>
        <person name="Fong B."/>
        <person name="Fujii C.Y."/>
        <person name="Gill J.E."/>
        <person name="Goldsmith A.D."/>
        <person name="Haas B."/>
        <person name="Hansen N.F."/>
        <person name="Hughes B."/>
        <person name="Huizar L."/>
        <person name="Hunter J.L."/>
        <person name="Jenkins J."/>
        <person name="Johnson-Hopson C."/>
        <person name="Khan S."/>
        <person name="Khaykin E."/>
        <person name="Kim C.J."/>
        <person name="Koo H.L."/>
        <person name="Kremenetskaia I."/>
        <person name="Kurtz D.B."/>
        <person name="Kwan A."/>
        <person name="Lam B."/>
        <person name="Langin-Hooper S."/>
        <person name="Lee A."/>
        <person name="Lee J.M."/>
        <person name="Lenz C.A."/>
        <person name="Li J.H."/>
        <person name="Li Y.-P."/>
        <person name="Lin X."/>
        <person name="Liu S.X."/>
        <person name="Liu Z.A."/>
        <person name="Luros J.S."/>
        <person name="Maiti R."/>
        <person name="Marziali A."/>
        <person name="Militscher J."/>
        <person name="Miranda M."/>
        <person name="Nguyen M."/>
        <person name="Nierman W.C."/>
        <person name="Osborne B.I."/>
        <person name="Pai G."/>
        <person name="Peterson J."/>
        <person name="Pham P.K."/>
        <person name="Rizzo M."/>
        <person name="Rooney T."/>
        <person name="Rowley D."/>
        <person name="Sakano H."/>
        <person name="Salzberg S.L."/>
        <person name="Schwartz J.R."/>
        <person name="Shinn P."/>
        <person name="Southwick A.M."/>
        <person name="Sun H."/>
        <person name="Tallon L.J."/>
        <person name="Tambunga G."/>
        <person name="Toriumi M.J."/>
        <person name="Town C.D."/>
        <person name="Utterback T."/>
        <person name="Van Aken S."/>
        <person name="Vaysberg M."/>
        <person name="Vysotskaia V.S."/>
        <person name="Walker M."/>
        <person name="Wu D."/>
        <person name="Yu G."/>
        <person name="Fraser C.M."/>
        <person name="Venter J.C."/>
        <person name="Davis R.W."/>
      </authorList>
      <dbReference type="PubMed" id="11130712"/>
      <dbReference type="DOI" id="10.1038/35048500"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2007" name="Plant J." volume="51" first="262" last="280">
      <title>Small cysteine-rich peptides resembling antimicrobial peptides have been under-predicted in plants.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Moskal W.A. Jr."/>
        <person name="Wu H.C."/>
        <person name="Underwood B.A."/>
        <person name="Graham M.A."/>
        <person name="Town C.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="17565583"/>
      <dbReference type="DOI" id="10.1111/j.1365-313x.2007.03136.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] OF 11-63</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <comment type="caution">
    <text evidence="3">Lacks 1 of the 4 disulfide bonds, which are conserved features of the family.</text>
  </comment>
  <dbReference type="EMBL" id="AC008047">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002684">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF182844">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q2V4F7"/>
  <dbReference type="SMR" id="Q2V4F7"/>
  <dbReference type="PaxDb" id="3702-AT1G63522.1"/>
  <dbReference type="Araport" id="AT1G63522"/>
  <dbReference type="TAIR" id="AT1G63522"/>
  <dbReference type="HOGENOM" id="CLU_202089_0_0_1"/>
  <dbReference type="InParanoid" id="Q2V4F7"/>
  <dbReference type="PhylomeDB" id="Q2V4F7"/>
  <dbReference type="PRO" id="PR:Q2V4F7"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q2V4F7">
    <property type="expression patterns" value="baseline"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000379739" description="Defensin-like protein 278">
    <location>
      <begin position="16"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="41"/>
      <end position="55"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="63" mass="7032" checksum="007499C4397015BE" modified="2006-01-10" version="1" precursor="true">MSLVYMYMYIGVVMSARIQESTNDILKPITCNTNADCAKFCKGPIHNCVYHTCQCVPGNPHCC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>