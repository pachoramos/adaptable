<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-07-13" modified="2024-05-29" version="23" xmlns="http://uniprot.org/uniprot">
  <accession>P86520</accession>
  <name>SBTXA_SOYBN</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Soybean toxin 27 kDa chain</fullName>
      <shortName evidence="5">SBTX 27 kDa chain</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Glycine max</name>
    <name type="common">Soybean</name>
    <name type="synonym">Glycine hispida</name>
    <dbReference type="NCBI Taxonomy" id="3847"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Fabales</taxon>
      <taxon>Fabaceae</taxon>
      <taxon>Papilionoideae</taxon>
      <taxon>50 kb inversion clade</taxon>
      <taxon>NPAAA clade</taxon>
      <taxon>indigoferoid/millettioid clade</taxon>
      <taxon>Phaseoleae</taxon>
      <taxon>Glycine</taxon>
      <taxon>Glycine subgen. Soja</taxon>
    </lineage>
  </organism>
  <reference evidence="7" key="1">
    <citation type="journal article" date="2008" name="Toxicon" volume="51" first="952" last="963">
      <title>SBTX, a new toxic protein distinct from soyatoxin and other toxic soybean [Glycine max] proteins, and its inhibitory effect on Cercospora sojina growth.</title>
      <authorList>
        <person name="Vasconcelos I.M."/>
        <person name="Morais J.K."/>
        <person name="Siebra E.A."/>
        <person name="Carlini C.R."/>
        <person name="Sousa D.O."/>
        <person name="Beltramini L.M."/>
        <person name="Melo V.M."/>
        <person name="Oliveira J.T."/>
      </authorList>
      <dbReference type="PubMed" id="18328522"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2007.10.005"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>SUBUNIT</scope>
    <scope>GLYCOSYLATION</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <strain evidence="1">cv. BR-10</strain>
      <tissue evidence="1">Seed</tissue>
    </source>
  </reference>
  <reference evidence="7" key="2">
    <citation type="submission" date="2010-03" db="UniProtKB">
      <authorList>
        <person name="Oliveira H.D."/>
      </authorList>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2010" name="J. Agric. Food Chem." volume="58" first="10356" last="10363">
      <title>Soybean toxin (SBTX), a protein from soybeans that inhibits the life cycle of plant and human pathogenic fungi.</title>
      <authorList>
        <person name="Morais J.K."/>
        <person name="Gomes V.M."/>
        <person name="Oliveira J.T."/>
        <person name="Santos I.S."/>
        <person name="Da Cunha M."/>
        <person name="Oliveira H.D."/>
        <person name="Oliveira H.P."/>
        <person name="Sousa D.O."/>
        <person name="Vasconcelos I.M."/>
      </authorList>
      <dbReference type="PubMed" id="20831249"/>
      <dbReference type="DOI" id="10.1021/jf101688k"/>
    </citation>
    <scope>PARTIAL PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <tissue evidence="6">Seed</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2013" name="PLoS ONE" volume="8" first="E70425" last="E70425">
      <title>Soybean toxin (SBTX) impairs fungal growth by interfering with molecular transport, carbohydrate/amino acid metabolism and drug/stress responses.</title>
      <authorList>
        <person name="Morais J.K."/>
        <person name="Bader O."/>
        <person name="Weig M."/>
        <person name="Oliveira J.T."/>
        <person name="Arantes M.R."/>
        <person name="Gomes V.M."/>
        <person name="Da Cunha M."/>
        <person name="Oliveira H.D."/>
        <person name="Sousa D.O."/>
        <person name="Lourencao A.L."/>
        <person name="Vasconcelos I.M."/>
      </authorList>
      <dbReference type="PubMed" id="23894655"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0070425"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3">Involved in plant defense (PubMed:18328522). Inhibits spore germination in C.sojina, A.niger (at concentrations &gt;50 ug/ml) and P.herguei but not in F.oxysporum and F.solani (PubMed:18328522, PubMed:20831249). Does not inhibit vegetative mycelial growth (PubMed:20831249). Inhibits growth of C.albicans and K.marxiannus but not P.membranifaciens or C.parapsilosis (PubMed:20831249, PubMed:23894655). Probably acts by affecting the cell membrane (PubMed:20831249). Does not have urease, chitinase, beta-1,3-glucanase or hemagglutination activities (PubMed:18328522, PubMed:20831249). Does not inhibit trypsin (PubMed:18328522). Injection into mice produces toxic effects such as dyspnea, tonic-clonic convulsion and death (PubMed:18328522).</text>
  </comment>
  <comment type="biophysicochemical properties">
    <phDependence>
      <text evidence="1">Activity decreases below pH 5.5 and above pH 8.0.</text>
    </phDependence>
  </comment>
  <comment type="subunit">
    <text evidence="1">Heterodimer of a 27 kDa subunit and a 17 kDa subunit; disulfide-linked.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed in seeds, leaves, roots and stem (at protein level).</text>
  </comment>
  <comment type="induction">
    <text evidence="4">In seeds, induced by jasmonate.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">SBTX is known to be glycosylated but it is not known which one of its two subunits is modified; contains 5% carbohydrates.</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="1 2">LD(50) is 5.6 mg/kg by intraperitoneal injection into mice.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">On the 2D-gel the determined pI of SBTX is: 8.2, its MW is: 44 kDa.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P86520"/>
  <dbReference type="InParanoid" id="P86520"/>
  <dbReference type="Proteomes" id="UP000008827">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000395397" description="Soybean toxin 27 kDa chain">
    <location>
      <begin position="1"/>
      <end position="25" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="5">
    <location>
      <position position="25"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="18328522"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="20831249"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="23894655"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="18328522"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="20831249"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="25" mass="2728" checksum="CA4A2595EF2A20C8" modified="2010-07-13" version="1" fragment="single">ADPTFGFTPLGLSEKANLQIMKAYD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>