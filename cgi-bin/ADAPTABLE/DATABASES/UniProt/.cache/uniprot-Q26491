<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1999-07-15" modified="2024-11-27" version="77" xmlns="http://uniprot.org/uniprot">
  <accession>Q26491</accession>
  <name>ITP_SCHGR</name>
  <protein>
    <recommendedName>
      <fullName>Ion transport peptide</fullName>
      <shortName>ITP</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Schistocerca gregaria</name>
    <name type="common">Desert locust</name>
    <name type="synonym">Gryllus gregarius</name>
    <dbReference type="NCBI Taxonomy" id="7010"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Orthoptera</taxon>
      <taxon>Caelifera</taxon>
      <taxon>Acrididea</taxon>
      <taxon>Acridomorpha</taxon>
      <taxon>Acridoidea</taxon>
      <taxon>Acrididae</taxon>
      <taxon>Cyrtacanthacridinae</taxon>
      <taxon>Schistocerca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="J. Exp. Biol." volume="199" first="1053" last="1061">
      <title>Locust ion transport peptide (ITP): primary structure, cDNA and expression in a baculovirus system.</title>
      <authorList>
        <person name="Meredith J."/>
        <person name="Ring M."/>
        <person name="Macins A."/>
        <person name="Marschall J."/>
        <person name="Cheng N.N."/>
        <person name="Theilmann D."/>
        <person name="Brock H.W."/>
        <person name="Phillips J.E."/>
      </authorList>
      <dbReference type="PubMed" id="8786332"/>
      <dbReference type="DOI" id="10.1242/jeb.199.5.1053"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text>Stimulates salt and water reabsorption and inhibits acid secretion in the ileum of S.gregaria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Brain and corpus cardiacum.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the arthropod CHH/MIH/GIH/VIH hormone family.</text>
  </comment>
  <dbReference type="EMBL" id="U36919">
    <property type="protein sequence ID" value="AAB16822.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q26491"/>
  <dbReference type="SMR" id="Q26491"/>
  <dbReference type="EnsemblMetazoa" id="XM_050003938.1">
    <property type="protein sequence ID" value="XP_049859895.1"/>
    <property type="gene ID" value="LOC126354358"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_050003939.1">
    <property type="protein sequence ID" value="XP_049859896.1"/>
    <property type="gene ID" value="LOC126354358"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007623">
    <property type="term" value="P:circadian rhythm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.2010.10:FF:000001">
    <property type="entry name" value="Ion transport peptide isoform C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.2010.10">
    <property type="entry name" value="Crustacean CHH/MIH/GIH neurohormone"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018251">
    <property type="entry name" value="Crust_neurhormone_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031098">
    <property type="entry name" value="Crust_neurohorm"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035957">
    <property type="entry name" value="Crust_neurohorm_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001166">
    <property type="entry name" value="Hyperglycemic"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000346">
    <property type="entry name" value="Hyperglycemic1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35981">
    <property type="entry name" value="ION TRANSPORT PEPTIDE, ISOFORM C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35981:SF2">
    <property type="entry name" value="ION TRANSPORT PEPTIDE, ISOFORM C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01147">
    <property type="entry name" value="Crust_neurohorm"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00548">
    <property type="entry name" value="HYPRGLYCEMC1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00550">
    <property type="entry name" value="HYPRGLYCEMIC"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF81778">
    <property type="entry name" value="Crustacean CHH/MIH/GIH neurohormone"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01250">
    <property type="entry name" value="CHH_MIH_GIH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000019086">
    <location>
      <begin status="unknown"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000019087" description="Ion transport peptide">
    <location>
      <begin position="56"/>
      <end position="127"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="2">
    <location>
      <position position="127"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="62"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="78"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="81"/>
      <end position="107"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="130" mass="15022" checksum="9499D8C1B0A93896" modified="1996-11-01" version="1" precursor="true">MHHQKQQQQQKQQGEAPCRHLQWRLSGVVLCVLVVASLVSTAASSPLDPHHLAKRSFFDIQCKGVYDKSIFARLDRICEDCYNLFREPQLHSLCRSDCFKSPYFKGCLQALLLIDEEEKFNQMVEILGKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>