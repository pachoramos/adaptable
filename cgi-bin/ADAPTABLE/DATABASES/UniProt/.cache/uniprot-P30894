<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-07-01" modified="2024-11-27" version="94" xmlns="http://uniprot.org/uniprot">
  <accession>P30894</accession>
  <name>NGFV_DABRR</name>
  <protein>
    <recommendedName>
      <fullName>Venom nerve growth factor</fullName>
      <shortName>v-NGF</shortName>
      <shortName>vNGF</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Daboia russelii</name>
    <name type="common">Russel's viper</name>
    <name type="synonym">Vipera russelii</name>
    <dbReference type="NCBI Taxonomy" id="8707"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Viperidae</taxon>
      <taxon>Viperinae</taxon>
      <taxon>Daboia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="Biochim. Biophys. Acta" volume="1160" first="287" last="292">
      <title>Purification and amino-acid sequence of a nerve growth factor from the venom of Vipera russelli russelli.</title>
      <authorList>
        <person name="Koyama J."/>
        <person name="Inoue S."/>
        <person name="Ikeda K."/>
        <person name="Hayashi K."/>
      </authorList>
      <dbReference type="PubMed" id="1477101"/>
      <dbReference type="DOI" id="10.1016/0167-4838(92)90090-z"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>GLYCOSYLATION AT ASN-21</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2011" name="Toxicon" volume="58" first="363" last="368">
      <title>Molecular diversity of snake venom nerve growth factors.</title>
      <authorList>
        <person name="Trummal K."/>
        <person name="Tonismagi K."/>
        <person name="Paalme V."/>
        <person name="Jarvekulg L."/>
        <person name="Siigur J."/>
        <person name="Siigur E."/>
      </authorList>
      <dbReference type="PubMed" id="21801740"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2011.07.005"/>
    </citation>
    <scope>CHARACTERIZATION</scope>
    <scope>GLYCOSYLATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Nerve growth factor is important for the development and maintenance of the sympathetic and sensory nervous systems. It stimulates division and differentiation of sympathetic and embryonic sensory neurons as well as basal forebrain cholinergic neurons in the brain. Its relevance in the snake venom is not clear. However, it has been shown to inhibit metalloproteinase-dependent proteolysis of platelet glycoprotein Ib alpha, suggesting a metalloproteinase inhibition to prevent metalloprotease autodigestion and/or protection against prey proteases (By similarity). Binds a lipid between the two protein chains in the homodimer. The lipid-bound form promotes histamine relase from mouse mast cells, contrary to the lipid-free form (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer; non-covalently linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the NGF-beta family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P30894"/>
  <dbReference type="SMR" id="P30894"/>
  <dbReference type="iPTMnet" id="P30894"/>
  <dbReference type="GO" id="GO:0030424">
    <property type="term" value="C:axon"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030425">
    <property type="term" value="C:dendrite"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008021">
    <property type="term" value="C:synaptic vesicle"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008083">
    <property type="term" value="F:growth factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008289">
    <property type="term" value="F:lipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008191">
    <property type="term" value="F:metalloendopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005163">
    <property type="term" value="F:nerve growth factor receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007169">
    <property type="term" value="P:cell surface receptor protein tyrosine kinase signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007613">
    <property type="term" value="P:memory"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050804">
    <property type="term" value="P:modulation of chemical synaptic transmission"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043524">
    <property type="term" value="P:negative regulation of neuron apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0021675">
    <property type="term" value="P:nerve development"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0038180">
    <property type="term" value="P:nerve growth factor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048812">
    <property type="term" value="P:neuron projection morphogenesis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007422">
    <property type="term" value="P:peripheral nervous system development"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048672">
    <property type="term" value="P:positive regulation of collateral sprouting"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033138">
    <property type="term" value="P:positive regulation of peptidyl-serine phosphorylation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045664">
    <property type="term" value="P:regulation of neuron differentiation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="FunFam" id="2.10.90.10:FF:000002">
    <property type="entry name" value="Brain-derived neurotrophic factor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.10.90.10">
    <property type="entry name" value="Cystine-knot cytokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029034">
    <property type="entry name" value="Cystine-knot_cytokine"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020408">
    <property type="entry name" value="Nerve_growth_factor-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002072">
    <property type="entry name" value="Nerve_growth_factor-rel"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020425">
    <property type="entry name" value="Nerve_growth_factor_bsu"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019846">
    <property type="entry name" value="Nerve_growth_factor_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11589:SF10">
    <property type="entry name" value="BETA-NERVE GROWTH FACTOR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11589">
    <property type="entry name" value="NERVE GROWTH FACTOR NGF -RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00243">
    <property type="entry name" value="NGF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00268">
    <property type="entry name" value="NGF"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01913">
    <property type="entry name" value="NGFBETA"/>
  </dbReference>
  <dbReference type="SMART" id="SM00140">
    <property type="entry name" value="NGF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57501">
    <property type="entry name" value="Cystine-knot cytokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00248">
    <property type="entry name" value="NGF_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50270">
    <property type="entry name" value="NGF_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0339">Growth factor</keyword>
  <keyword id="KW-0446">Lipid-binding</keyword>
  <keyword id="KW-0481">Metalloenzyme inhibitor</keyword>
  <keyword id="KW-0483">Metalloprotease inhibitor</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000159604" description="Venom nerve growth factor">
    <location>
      <begin position="1"/>
      <end position="117"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="3">
    <location>
      <position position="21"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="12"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="55"/>
      <end position="105"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="65"/>
      <end position="107"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P61898"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P61899"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="1477101"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="21801740"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="21801740"/>
    </source>
  </evidence>
  <sequence length="117" mass="13283" checksum="A64559C5FEC11F66" modified="1993-07-01" version="1">HPVHNQGEFSVCDSVSVWVANKTTATDMRGNVVTVMVDVNLNNNVYKQYFFETKCKNPNPVPSGCRGIDAKHWNSYCTTTDTFVRALTMERNQASWRFIRINTACVCVISRKNDNFG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>