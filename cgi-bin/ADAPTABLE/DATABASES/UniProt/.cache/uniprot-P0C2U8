<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-01" modified="2023-05-03" version="34" xmlns="http://uniprot.org/uniprot">
  <accession>P0C2U8</accession>
  <name>LYC3_LYCSI</name>
  <protein>
    <recommendedName>
      <fullName>M-lycotoxin-Ls2a</fullName>
      <shortName>M-LCTX-Ls2a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Lycocitin-3</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Lycosa singoriensis</name>
    <name type="common">Wolf spider</name>
    <name type="synonym">Aranea singoriensis</name>
    <dbReference type="NCBI Taxonomy" id="434756"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Lycosoidea</taxon>
      <taxon>Lycosidae</taxon>
      <taxon>Lycosa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="J. Mass Spectrom." volume="39" first="193" last="201">
      <title>De novo sequencing of antimicrobial peptides isolated from the venom glands of the wolf spider Lycosa singoriensis.</title>
      <authorList>
        <person name="Budnik B.A."/>
        <person name="Olsen J.V."/>
        <person name="Egorov T.A."/>
        <person name="Anisimova V.E."/>
        <person name="Galkina T.G."/>
        <person name="Musolyamov A.K."/>
        <person name="Grishin E.V."/>
        <person name="Zubarev R.A."/>
      </authorList>
      <dbReference type="PubMed" id="14991689"/>
      <dbReference type="DOI" id="10.1002/jms.577"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Forms pore that permeabilize the cell membrane. Promotes efflux of calcium from synaptosomes, causes hemolysis, and dissipates voltage gradients across muscle membrane. Potently inhibits the growth of bacteria and yeast. May function both in the prey capture strategy as well as protection from infectious organisms arising from prey ingestion (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="1">Forms a membrane channel in the prey.</text>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="3149.75" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text>Belongs to the cationic peptide 04 (cupiennin) family. 05 subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0C2U8"/>
  <dbReference type="ArachnoServer" id="AS000064">
    <property type="toxin name" value="M-lycotoxin-Ls2a"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006811">
    <property type="term" value="P:monoatomic ion transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="peptide" id="PRO_0000285258" description="M-lycotoxin-Ls2a">
    <location>
      <begin position="1"/>
      <end position="26"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="14991689"/>
    </source>
  </evidence>
  <sequence length="26" mass="3150" checksum="864D694C856DB8EC" modified="2007-05-01" version="1">KIKWFKTMKSLAKFLAKEQMKKHLGE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>