<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-01-01" modified="2024-07-24" version="108" xmlns="http://uniprot.org/uniprot">
  <accession>P06429</accession>
  <name>VE7_HPV33</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein E7</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">E7</name>
  </gene>
  <organism>
    <name type="scientific">Human papillomavirus 33</name>
    <dbReference type="NCBI Taxonomy" id="10586"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Monodnaviria</taxon>
      <taxon>Shotokuvirae</taxon>
      <taxon>Cossaviricota</taxon>
      <taxon>Papovaviricetes</taxon>
      <taxon>Zurhausenvirales</taxon>
      <taxon>Papillomaviridae</taxon>
      <taxon>Firstpapillomavirinae</taxon>
      <taxon>Alphapapillomavirus</taxon>
      <taxon>Alphapapillomavirus 9</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1986" name="J. Virol." volume="58" first="991" last="995">
      <title>Genome organization and nucleotide sequence of human papillomavirus type 33, which is associated with cervical cancer.</title>
      <authorList>
        <person name="Cole S.T."/>
        <person name="Streeck R.E."/>
      </authorList>
      <dbReference type="PubMed" id="3009902"/>
      <dbReference type="DOI" id="10.1128/jvi.58.3.991-995.1986"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1992" name="J. Virol." volume="66" first="3172" last="3178">
      <title>Human papillomavirus type 33 in a tonsillar carcinoma generates its putative E7 mRNA via two E6* transcript species which are terminated at different early region poly(A) sites.</title>
      <authorList>
        <person name="Snijders P.J.F."/>
        <person name="van den Brule A.J.C."/>
        <person name="Schrijnemakers H.F.J."/>
        <person name="Raaphorst P.M.C."/>
        <person name="Meijer C.J.L.M."/>
        <person name="Walboomers J.M.M."/>
      </authorList>
      <dbReference type="PubMed" id="1313922"/>
      <dbReference type="DOI" id="10.1128/jvi.66.5.3172-3178.1992"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Rev. Med. Virol." volume="12" first="81" last="92">
      <title>Interactions of SV40 large T antigen and other viral proteins with retinoblastoma tumour suppressor.</title>
      <authorList>
        <person name="Lee C."/>
        <person name="Cho Y."/>
      </authorList>
      <dbReference type="PubMed" id="11921304"/>
      <dbReference type="DOI" id="10.1002/rmv.340"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays a role in viral genome replication by driving entry of quiescent cells into the cell cycle. Stimulation of progression from G1 to S phase allows the virus to efficiently use the cellular DNA replicating machinery to achieve viral genome replication. E7 protein has both transforming and trans-activating activities. Induces the disassembly of the E2F1 transcription factor from RB1, with subsequent transcriptional activation of E2F1-regulated S-phase genes. Interferes with host histone deacetylation mediated by HDAC1 and HDAC2, leading to transcription activation. Also plays a role in the inhibition of both antiviral and antiproliferative functions of host interferon alpha. Interaction with host TMEM173/STING impairs the ability of TMEM173/STING to sense cytosolic DNA and promote the production of type I interferon (IFN-alpha and IFN-beta).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer. Homooligomer. Interacts with host RB1; this interaction induces dissociation of RB1-E2F1 complex thereby disrupting RB1 activity. Interacts with host EP300; this interaction represses EP300 transcriptional activity. Interacts with protein E2; this interaction inhibits E7 oncogenic activity. Interacts with host TMEM173/STING; this interaction impairs the ability of TMEM173/STING to sense cytosolic DNA and promote the production of type I interferon (IFN-alpha and IFN-beta).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
    </subcellularLocation>
    <text evidence="1">Predominantly found in the host nucleus.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The E7 terminal domain is an intrinsically disordered domain, whose flexibility and conformational transitions confer target adaptability to the oncoprotein. It allows adaptation to a variety of protein targets and exposes the PEST degradation sequence that regulates its turnover in the cell.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Highly phosphorylated.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the papillomaviridae E7 protein family.</text>
  </comment>
  <dbReference type="EMBL" id="M12732">
    <property type="protein sequence ID" value="AAA46959.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X64084">
    <property type="protein sequence ID" value="CAA45430.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X64085">
    <property type="protein sequence ID" value="CAA45434.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A03689">
    <property type="entry name" value="W7WL33"/>
  </dbReference>
  <dbReference type="SMR" id="P06429"/>
  <dbReference type="IntAct" id="P06429">
    <property type="interactions" value="82"/>
  </dbReference>
  <dbReference type="MINT" id="P06429"/>
  <dbReference type="Proteomes" id="UP000009118">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030430">
    <property type="term" value="C:host cell cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042025">
    <property type="term" value="C:host cell nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003700">
    <property type="term" value="F:DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019904">
    <property type="term" value="F:protein domain specific binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006351">
    <property type="term" value="P:DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039645">
    <property type="term" value="P:symbiont-mediated perturbation of host cell cycle G1/S transition checkpoint"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039502">
    <property type="term" value="P:symbiont-mediated suppression of host type I interferon-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.160.330">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_04004">
    <property type="entry name" value="PPV_E7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000148">
    <property type="entry name" value="Papilloma_E7"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00527">
    <property type="entry name" value="E7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF003407">
    <property type="entry name" value="Papvi_E7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF161234">
    <property type="entry name" value="E7 C-terminal domain-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0010">Activator</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0244">Early protein</keyword>
  <keyword id="KW-1078">G1/S host cell cycle checkpoint dysregulation by virus</keyword>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1114">Inhibition of host interferon signaling pathway by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-1121">Modulation of host cell cycle by virus</keyword>
  <keyword id="KW-0553">Oncogene</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <keyword id="KW-0863">Zinc-finger</keyword>
  <feature type="chain" id="PRO_0000133431" description="Protein E7">
    <location>
      <begin position="1"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="zinc finger region" evidence="1">
    <location>
      <begin position="58"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="region of interest" description="E7 terminal domain" evidence="1">
    <location>
      <begin position="1"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="LXCXE motif; interaction with host RB1 and TMEM173/STING" evidence="1">
    <location>
      <begin position="22"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Nuclear export signal" evidence="1">
    <location>
      <begin position="76"/>
      <end position="84"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_04004"/>
    </source>
  </evidence>
  <sequence length="97" mass="10837" checksum="699DCF74343243C8" modified="1988-01-01" version="1">MRGHKPTLKEYVLDLYPEPTDLYCYEQLSDSSDEDEGLDRPDGQAQPATADYYIVTCCHTCNTTVRLCVNSTASDLRTIQQLLMGTVNIVCPTCAQQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>