<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-10-31" modified="2023-06-28" version="16" xmlns="http://uniprot.org/uniprot">
  <accession>P0DKN0</accession>
  <name>TU135_IOTOL</name>
  <protein>
    <recommendedName>
      <fullName>Turripeptide OL135</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Iotyrris olangoensis</name>
    <name type="common">Sea snail</name>
    <name type="synonym">Lophiotoma olangoensis</name>
    <dbReference type="NCBI Taxonomy" id="2420066"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Caenogastropoda</taxon>
      <taxon>Neogastropoda</taxon>
      <taxon>Conoidea</taxon>
      <taxon>Turridae</taxon>
      <taxon>Iotyrris</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="J. Mol. Evol." volume="62" first="247" last="256">
      <title>Genes expressed in a turrid venom duct: divergence and similarity to conotoxins.</title>
      <authorList>
        <person name="Watkins M."/>
        <person name="Hillyard D.R."/>
        <person name="Olivera B.M."/>
      </authorList>
      <dbReference type="PubMed" id="16477526"/>
      <dbReference type="DOI" id="10.1007/s00239-005-0010-x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom duct</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Acts as a neurotoxin by inhibiting an ion channel.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom duct.</text>
  </comment>
  <comment type="domain">
    <text>The cysteine framework is IX (C-C-C-C-C-C).</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Contains 3 disulfide bonds.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the conopeptide P-like superfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0099106">
    <property type="term" value="F:ion channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000419844" evidence="2">
    <location>
      <begin position="21"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000419845" description="Turripeptide OL135">
    <location>
      <begin position="29"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="74" mass="8589" checksum="E6A10394CA385C83" modified="2012-10-31" version="1" precursor="true">MKVPIVLMLVLLLIMPLSDGYERKRXXXVECNETCEEFCTYCDDNNAEETENCRTDQTDHSRCVDFYTANNLPT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>