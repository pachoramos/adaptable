<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1987-08-13" modified="2024-11-27" version="53" xmlns="http://uniprot.org/uniprot">
  <accession>P63290</accession>
  <accession>P04565</accession>
  <name>VIP_CAPHI</name>
  <protein>
    <recommendedName>
      <fullName>Vasoactive intestinal peptide</fullName>
      <shortName>VIP</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Vasoactive intestinal polypeptide</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">VIP</name>
  </gene>
  <organism>
    <name type="scientific">Capra hircus</name>
    <name type="common">Goat</name>
    <dbReference type="NCBI Taxonomy" id="9925"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Caprinae</taxon>
      <taxon>Capra</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1986" name="Peptides 7 Suppl." volume="1" first="17" last="20">
      <title>Purification and amino acid sequences of dog, goat and guinea pig VIPs.</title>
      <authorList>
        <person name="Eng J."/>
        <person name="Du B.-H."/>
        <person name="Raufman J.-P."/>
        <person name="Yalow R.S."/>
      </authorList>
      <dbReference type="PubMed" id="3748846"/>
      <dbReference type="DOI" id="10.1016/0196-9781(86)90158-0"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT ASN-28</scope>
  </reference>
  <comment type="function">
    <text>VIP causes vasodilation, lowers arterial blood pressure, stimulates myocardial contractility, increases glycogenolysis and relaxes the smooth muscle of trachea, stomach and gall bladder.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the glucagon family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P63290"/>
  <dbReference type="SMR" id="P63290"/>
  <dbReference type="Proteomes" id="UP000291000">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694566">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694900">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043005">
    <property type="term" value="C:neuron projection"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051428">
    <property type="term" value="F:peptide hormone receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007189">
    <property type="term" value="P:adenylate cyclase-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048242">
    <property type="term" value="P:epinephrine secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007611">
    <property type="term" value="P:learning or memory"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048255">
    <property type="term" value="P:mRNA stabilization"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="AgBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043066">
    <property type="term" value="P:negative regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043267">
    <property type="term" value="P:negative regulation of potassium ion transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048662">
    <property type="term" value="P:negative regulation of smooth muscle cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007200">
    <property type="term" value="P:phospholipase C-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001938">
    <property type="term" value="P:positive regulation of endothelial cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032812">
    <property type="term" value="P:positive regulation of epinephrine secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060406">
    <property type="term" value="P:positive regulation of penile erection"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045732">
    <property type="term" value="P:positive regulation of protein catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070459">
    <property type="term" value="P:prolactin secretion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="AgBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032880">
    <property type="term" value="P:regulation of protein localization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.590">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000532">
    <property type="entry name" value="Glucagon_GIP_secretin_VIP"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046963">
    <property type="entry name" value="VIP/GHRH-like"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11213">
    <property type="entry name" value="GLUCAGON-FAMILY NEUROPEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11213:SF5">
    <property type="entry name" value="VIP PEPTIDES"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00123">
    <property type="entry name" value="Hormone_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00070">
    <property type="entry name" value="GLUCA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00260">
    <property type="entry name" value="GLUCAGON"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043940" description="Vasoactive intestinal peptide">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="modified residue" description="Asparagine amide" evidence="1">
    <location>
      <position position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="3748846"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="28" mass="3327" checksum="EF313FB573FF6F3F" modified="1987-08-13" version="1">HSDAVFTDNYTRLRKQMAVKKYLNSILN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>