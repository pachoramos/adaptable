<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-01-16" modified="2024-11-27" version="15" xmlns="http://uniprot.org/uniprot">
  <accession>P0DPR9</accession>
  <name>MELN_APICE</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Melittin-N</fullName>
      <shortName evidence="4">MEL-N</shortName>
      <shortName>MLT</shortName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">MELT</name>
  </gene>
  <organism>
    <name type="scientific">Apis cerana</name>
    <name type="common">Indian honeybee</name>
    <dbReference type="NCBI Taxonomy" id="7461"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Apoidea</taxon>
      <taxon>Anthophila</taxon>
      <taxon>Apidae</taxon>
      <taxon>Apis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2014" name="Peptides" volume="53" first="185" last="193">
      <title>Functional characterization of naturally occurring melittin peptide isoforms in two honey bee species, Apis mellifera and Apis cerana.</title>
      <authorList>
        <person name="Park D."/>
        <person name="Jung J.W."/>
        <person name="Lee M.O."/>
        <person name="Lee S.Y."/>
        <person name="Kim B."/>
        <person name="Jin H.J."/>
        <person name="Kim J."/>
        <person name="Ahn Y.J."/>
        <person name="Lee K.W."/>
        <person name="Song Y.S."/>
        <person name="Hong S."/>
        <person name="Womack J.E."/>
        <person name="Kwon H.W."/>
      </authorList>
      <dbReference type="PubMed" id="24512991"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2014.01.026"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>SYNTHESIS OF 44-69</scope>
    <scope>FUNCTION</scope>
    <scope>MUTAGENESIS OF ASN-61</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3">Main toxin of bee venom with strong hemolytic activity and antimicrobial activity. It has enhancing effects on bee venom phospholipase A2 activity. This amphipathic toxin binds to negatively charged membrane surface and forms pore by inserting into lipid bilayers inducing the leakage of ions and molecules and the enhancement of permeability that ultimately leads to cell lysis. It acts as a voltage-gated pore with higher selectivity for anions over cations. The ion conductance has been shown to be voltage-dependent. Self-association of melittin in membranes is promoted by high ionic strength, but not by the presence of negatively charged lipids. In vivo, intradermal injection into healthy human volunteers produce sharp pain sensation and an inflammatory response. It produces pain by activating primary nociceptor cells directly and indirectly due to its ability to activate plasma membrane phospholipase A2 and its pore-forming activity (By similarity). Shows lower cytotoxicity when tested on E.coli and cancer cell lines than melittin, as well as lower anti-inflammatory properties and lower properties to interact to small unilamellar liposomes (PubMed:24512991).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer (in solution and for integration into membranes), homotetramer (in solution and potentially as a toroidal pore in membranes), and potenially homomultimer (as a toroidal pore in membranes).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="1">Alpha-helical peptides form toroidal pores in the prey.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the melittin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DPR9"/>
  <dbReference type="SMR" id="P0DPR9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046930">
    <property type="term" value="C:pore complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015288">
    <property type="term" value="F:porin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004860">
    <property type="term" value="F:protein kinase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006811">
    <property type="term" value="P:monoatomic ion transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002116">
    <property type="entry name" value="Melittin/Api_allergen"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01372">
    <property type="entry name" value="Melittin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0020">Allergen</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0291">Formylation</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0626">Porin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000446007" description="Removed by a dipeptidylpeptidase" evidence="1">
    <location>
      <begin position="22"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000446008" description="Melittin-N" evidence="1">
    <location>
      <begin position="44"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="site" description="Important for the flexibility at the center of the helix, flexibility that is important for the stability of the voltage-gated pore" evidence="1">
    <location>
      <position position="57"/>
    </location>
  </feature>
  <feature type="modified residue" description="N-formylglycine; partial" evidence="1">
    <location>
      <position position="44"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glutamine amide" evidence="1">
    <location>
      <position position="69"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Increase in cytotoxicity when tested on E.coli and cancer cell lines, increase in anti-inflammatory properties since it inhibits more potently IL-6 and TNF-alpha (but not IL-1 beta) production in melittin-treated cells, and increase in interaction to small unilamellar liposomes." evidence="3">
    <original>N</original>
    <variation>S</variation>
    <location>
      <position position="61"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01501"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="24512991"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="24512991"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="70" mass="7612" checksum="60657BC091C23BB6" modified="2019-01-16" version="1" precursor="true">MKFLVNVALVFMVVYISYIYAAPEPEPAPEPEAEADAEADPEAGIGAVLKVLTTGLPALINWIKRKRQQG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>