<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-03-01" modified="2024-01-24" version="23" xmlns="http://uniprot.org/uniprot">
  <accession>P84442</accession>
  <name>PVK_PHYMO</name>
  <protein>
    <recommendedName>
      <fullName>Periviscerokinin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Phymateus morbillosus</name>
    <name type="common">Gaudy grasshopper</name>
    <dbReference type="NCBI Taxonomy" id="310747"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Orthoptera</taxon>
      <taxon>Caelifera</taxon>
      <taxon>Acrididea</taxon>
      <taxon>Acridomorpha</taxon>
      <taxon>Pyrgomorphoidea</taxon>
      <taxon>Pyrgomorphidae</taxon>
      <taxon>Pyrgomorphinae</taxon>
      <taxon>Phymateus</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2002" name="Peptides" volume="23" first="621" last="627">
      <title>Identification of the abundant neuropeptide from abdominal perisympathetic organs of locusts.</title>
      <authorList>
        <person name="Predel R."/>
        <person name="Gaede G."/>
      </authorList>
      <dbReference type="PubMed" id="11897380"/>
      <dbReference type="DOI" id="10.1016/s0196-9781(01)00669-6"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT VAL-10</scope>
    <source>
      <tissue evidence="2">Abdominal perisympathetic organs</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Myotropic peptide; increases the frequency of contraction of the heart and stimulates amplitude and tonus of the foregut.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="1104.5" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the periviscerokinin family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044238" description="Periviscerokinin">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="2">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11897380"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="10" mass="1105" checksum="39811269D6D9C728" modified="2005-03-01" version="1">AAGLFQFPRV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>