<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="138" xmlns="http://uniprot.org/uniprot">
  <accession>P01718</accession>
  <accession>A0A075B6J3</accession>
  <name>LV327_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="4 9">Immunoglobulin lambda variable 3-27</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="11">Ig lambda chain V-IV region Kern</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="4 9" type="primary">IGLV3-27</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Nature" volume="402" first="489" last="495">
      <title>The DNA sequence of human chromosome 22.</title>
      <authorList>
        <person name="Dunham I."/>
        <person name="Hunt A.R."/>
        <person name="Collins J.E."/>
        <person name="Bruskiewich R."/>
        <person name="Beare D.M."/>
        <person name="Clamp M."/>
        <person name="Smink L.J."/>
        <person name="Ainscough R."/>
        <person name="Almeida J.P."/>
        <person name="Babbage A.K."/>
        <person name="Bagguley C."/>
        <person name="Bailey J."/>
        <person name="Barlow K.F."/>
        <person name="Bates K.N."/>
        <person name="Beasley O.P."/>
        <person name="Bird C.P."/>
        <person name="Blakey S.E."/>
        <person name="Bridgeman A.M."/>
        <person name="Buck D."/>
        <person name="Burgess J."/>
        <person name="Burrill W.D."/>
        <person name="Burton J."/>
        <person name="Carder C."/>
        <person name="Carter N.P."/>
        <person name="Chen Y."/>
        <person name="Clark G."/>
        <person name="Clegg S.M."/>
        <person name="Cobley V.E."/>
        <person name="Cole C.G."/>
        <person name="Collier R.E."/>
        <person name="Connor R."/>
        <person name="Conroy D."/>
        <person name="Corby N.R."/>
        <person name="Coville G.J."/>
        <person name="Cox A.V."/>
        <person name="Davis J."/>
        <person name="Dawson E."/>
        <person name="Dhami P.D."/>
        <person name="Dockree C."/>
        <person name="Dodsworth S.J."/>
        <person name="Durbin R.M."/>
        <person name="Ellington A.G."/>
        <person name="Evans K.L."/>
        <person name="Fey J.M."/>
        <person name="Fleming K."/>
        <person name="French L."/>
        <person name="Garner A.A."/>
        <person name="Gilbert J.G.R."/>
        <person name="Goward M.E."/>
        <person name="Grafham D.V."/>
        <person name="Griffiths M.N.D."/>
        <person name="Hall C."/>
        <person name="Hall R.E."/>
        <person name="Hall-Tamlyn G."/>
        <person name="Heathcott R.W."/>
        <person name="Ho S."/>
        <person name="Holmes S."/>
        <person name="Hunt S.E."/>
        <person name="Jones M.C."/>
        <person name="Kershaw J."/>
        <person name="Kimberley A.M."/>
        <person name="King A."/>
        <person name="Laird G.K."/>
        <person name="Langford C.F."/>
        <person name="Leversha M.A."/>
        <person name="Lloyd C."/>
        <person name="Lloyd D.M."/>
        <person name="Martyn I.D."/>
        <person name="Mashreghi-Mohammadi M."/>
        <person name="Matthews L.H."/>
        <person name="Mccann O.T."/>
        <person name="Mcclay J."/>
        <person name="Mclaren S."/>
        <person name="McMurray A.A."/>
        <person name="Milne S.A."/>
        <person name="Mortimore B.J."/>
        <person name="Odell C.N."/>
        <person name="Pavitt R."/>
        <person name="Pearce A.V."/>
        <person name="Pearson D."/>
        <person name="Phillimore B.J.C.T."/>
        <person name="Phillips S.H."/>
        <person name="Plumb R.W."/>
        <person name="Ramsay H."/>
        <person name="Ramsey Y."/>
        <person name="Rogers L."/>
        <person name="Ross M.T."/>
        <person name="Scott C.E."/>
        <person name="Sehra H.K."/>
        <person name="Skuce C.D."/>
        <person name="Smalley S."/>
        <person name="Smith M.L."/>
        <person name="Soderlund C."/>
        <person name="Spragon L."/>
        <person name="Steward C.A."/>
        <person name="Sulston J.E."/>
        <person name="Swann R.M."/>
        <person name="Vaudin M."/>
        <person name="Wall M."/>
        <person name="Wallis J.M."/>
        <person name="Whiteley M.N."/>
        <person name="Willey D.L."/>
        <person name="Williams L."/>
        <person name="Williams S.A."/>
        <person name="Williamson H."/>
        <person name="Wilmer T.E."/>
        <person name="Wilming L."/>
        <person name="Wright C.L."/>
        <person name="Hubbard T."/>
        <person name="Bentley D.R."/>
        <person name="Beck S."/>
        <person name="Rogers J."/>
        <person name="Shimizu N."/>
        <person name="Minoshima S."/>
        <person name="Kawasaki K."/>
        <person name="Sasaki T."/>
        <person name="Asakawa S."/>
        <person name="Kudoh J."/>
        <person name="Shintani A."/>
        <person name="Shibuya K."/>
        <person name="Yoshizaki Y."/>
        <person name="Aoki N."/>
        <person name="Mitsuyama S."/>
        <person name="Roe B.A."/>
        <person name="Chen F."/>
        <person name="Chu L."/>
        <person name="Crabtree J."/>
        <person name="Deschamps S."/>
        <person name="Do A."/>
        <person name="Do T."/>
        <person name="Dorman A."/>
        <person name="Fang F."/>
        <person name="Fu Y."/>
        <person name="Hu P."/>
        <person name="Hua A."/>
        <person name="Kenton S."/>
        <person name="Lai H."/>
        <person name="Lao H.I."/>
        <person name="Lewis J."/>
        <person name="Lewis S."/>
        <person name="Lin S.-P."/>
        <person name="Loh P."/>
        <person name="Malaj E."/>
        <person name="Nguyen T."/>
        <person name="Pan H."/>
        <person name="Phan S."/>
        <person name="Qi S."/>
        <person name="Qian Y."/>
        <person name="Ray L."/>
        <person name="Ren Q."/>
        <person name="Shaull S."/>
        <person name="Sloan D."/>
        <person name="Song L."/>
        <person name="Wang Q."/>
        <person name="Wang Y."/>
        <person name="Wang Z."/>
        <person name="White J."/>
        <person name="Willingham D."/>
        <person name="Wu H."/>
        <person name="Yao Z."/>
        <person name="Zhan M."/>
        <person name="Zhang G."/>
        <person name="Chissoe S."/>
        <person name="Murray J."/>
        <person name="Miller N."/>
        <person name="Minx P."/>
        <person name="Fulton R."/>
        <person name="Johnson D."/>
        <person name="Bemis G."/>
        <person name="Bentley D."/>
        <person name="Bradshaw H."/>
        <person name="Bourne S."/>
        <person name="Cordes M."/>
        <person name="Du Z."/>
        <person name="Fulton L."/>
        <person name="Goela D."/>
        <person name="Graves T."/>
        <person name="Hawkins J."/>
        <person name="Hinds K."/>
        <person name="Kemp K."/>
        <person name="Latreille P."/>
        <person name="Layman D."/>
        <person name="Ozersky P."/>
        <person name="Rohlfing T."/>
        <person name="Scheet P."/>
        <person name="Walker C."/>
        <person name="Wamsley A."/>
        <person name="Wohldmann P."/>
        <person name="Pepin K."/>
        <person name="Nelson J."/>
        <person name="Korf I."/>
        <person name="Bedell J.A."/>
        <person name="Hillier L.W."/>
        <person name="Mardis E."/>
        <person name="Waterston R."/>
        <person name="Wilson R."/>
        <person name="Emanuel B.S."/>
        <person name="Shaikh T."/>
        <person name="Kurahashi H."/>
        <person name="Saitta S."/>
        <person name="Budarf M.L."/>
        <person name="McDermid H.E."/>
        <person name="Johnson A."/>
        <person name="Wong A.C.C."/>
        <person name="Morrow B.E."/>
        <person name="Edelmann L."/>
        <person name="Kim U.J."/>
        <person name="Shizuya H."/>
        <person name="Simon M.I."/>
        <person name="Dumanski J.P."/>
        <person name="Peyrard M."/>
        <person name="Kedra D."/>
        <person name="Seroussi E."/>
        <person name="Fransson I."/>
        <person name="Tapia I."/>
        <person name="Bruder C.E."/>
        <person name="O'Brien K.P."/>
        <person name="Wilkinson P."/>
        <person name="Bodenteich A."/>
        <person name="Hartman K."/>
        <person name="Hu X."/>
        <person name="Khan A.S."/>
        <person name="Lane L."/>
        <person name="Tilahun Y."/>
        <person name="Wright H."/>
      </authorList>
      <dbReference type="PubMed" id="10591208"/>
      <dbReference type="DOI" id="10.1038/990031"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA] (IMGT ALLELE IGLV3-27*01)</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1971" name="Hoppe-Seyler's Z. Physiol. Chem." volume="352" first="247" last="266">
      <title>Structural rule of antibodies. Primary structure of a monoclonal immunoglobulin-L-chain of the lambda type, subgroup IV (Bence-Jones-protein Kern). V. The complete amino acid sequence and its genetic interpretation.</title>
      <authorList>
        <person name="Ponstingl H."/>
        <person name="Hess M."/>
        <person name="Hilschmann N."/>
      </authorList>
      <dbReference type="PubMed" id="5549568"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-113</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2001" name="Exp. Clin. Immunogenet." volume="18" first="242" last="254">
      <title>Nomenclature of the human immunoglobulin lambda (IGL) genes.</title>
      <authorList>
        <person name="Lefranc M.P."/>
      </authorList>
      <dbReference type="PubMed" id="11872955"/>
      <dbReference type="DOI" id="10.1159/000049203"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="4">
    <citation type="book" date="2001" name="The Immunoglobulin FactsBook." first="1" last="458" publisher="Academic Press" city="London.">
      <title>The Immunoglobulin FactsBook.</title>
      <editorList>
        <person name="Lefranc M.P."/>
        <person name="Lefranc G."/>
      </editorList>
      <authorList>
        <person name="Lefranc M.P."/>
        <person name="Lefranc G."/>
      </authorList>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2007" name="Annu. Rev. Genet." volume="41" first="107" last="120">
      <title>Immunoglobulin somatic hypermutation.</title>
      <authorList>
        <person name="Teng G."/>
        <person name="Papavasiliou F.N."/>
      </authorList>
      <dbReference type="PubMed" id="17576170"/>
      <dbReference type="DOI" id="10.1146/annurev.genet.41.110306.130340"/>
    </citation>
    <scope>REVIEW ON SOMATIC HYPERMUTATION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2010" name="J. Allergy Clin. Immunol." volume="125" first="S41" last="S52">
      <title>Structure and function of immunoglobulins.</title>
      <authorList>
        <person name="Schroeder H.W. Jr."/>
        <person name="Cavacini L."/>
      </authorList>
      <dbReference type="PubMed" id="20176268"/>
      <dbReference type="DOI" id="10.1016/j.jaci.2009.09.046"/>
    </citation>
    <scope>REVIEW ON IMMUNOGLOBULINS</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2012" name="Nat. Rev. Immunol." volume="12" first="24" last="34">
      <title>Molecular programming of B cell memory.</title>
      <authorList>
        <person name="McHeyzer-Williams M."/>
        <person name="Okitsu S."/>
        <person name="Wang N."/>
        <person name="McHeyzer-Williams L."/>
      </authorList>
      <dbReference type="PubMed" id="22158414"/>
      <dbReference type="DOI" id="10.1038/nri3128"/>
    </citation>
    <scope>REVIEW ON FUNCTION</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2014" name="Front. Immunol." volume="5" first="22" last="22">
      <title>Immunoglobulin and T Cell Receptor Genes: IMGT((R)) and the Birth and Rise of Immunoinformatics.</title>
      <authorList>
        <person name="Lefranc M.P."/>
      </authorList>
      <dbReference type="PubMed" id="24600447"/>
      <dbReference type="DOI" id="10.3389/fimmu.2014.00022"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="5 6 7 8">V region of the variable domain of immunoglobulin light chains that participates in the antigen recognition (PubMed:24600447). Immunoglobulins, also known as antibodies, are membrane-bound or secreted glycoproteins produced by B lymphocytes. In the recognition phase of humoral immunity, the membrane-bound immunoglobulins serve as receptors which, upon binding of a specific antigen, trigger the clonal expansion and differentiation of B lymphocytes into immunoglobulins-secreting plasma cells. Secreted immunoglobulins mediate the effector phase of humoral immunity, which results in the elimination of bound antigens (PubMed:20176268, PubMed:22158414). The antigen binding site is formed by the variable domain of one heavy chain, together with that of its associated light chain. Thus, each immunoglobulin has two antigen binding sites with remarkable affinity for a particular antigen. The variable domains are assembled by a process called V-(D)-J rearrangement and can then be subjected to somatic hypermutations which, after exposure to antigen and selection, allow affinity maturation for a particular antigen (PubMed:17576170, PubMed:20176268).</text>
  </comment>
  <comment type="subunit">
    <text evidence="6">Immunoglobulins are composed of two identical heavy chains and two identical light chains; disulfide-linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6 7">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="6 7">Cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="polymorphism">
    <text>There are several alleles. The sequence shown is that of IMGT allele IGLV3-27*01.</text>
  </comment>
  <comment type="caution">
    <text evidence="10">For an example of a full-length immunoglobulin lambda light chain see AC P0DOX8.</text>
  </comment>
  <dbReference type="EMBL" id="AC244250">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A01984">
    <property type="entry name" value="L4HUKN"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01718"/>
  <dbReference type="SMR" id="P01718"/>
  <dbReference type="IMGT_GENE-DB" id="IGLV3-27"/>
  <dbReference type="BioMuta" id="IGLV3-27"/>
  <dbReference type="jPOST" id="P01718"/>
  <dbReference type="MassIVE" id="P01718"/>
  <dbReference type="PeptideAtlas" id="P01718"/>
  <dbReference type="TopDownProteomics" id="P01718"/>
  <dbReference type="Ensembl" id="ENST00000390304.2">
    <property type="protein sequence ID" value="ENSP00000374839.2"/>
    <property type="gene ID" value="ENSG00000211658.2"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:5910"/>
  <dbReference type="GeneCards" id="IGLV3-27"/>
  <dbReference type="HGNC" id="HGNC:5910">
    <property type="gene designation" value="IGLV3-27"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000211658">
    <property type="expression patterns" value="Tissue enriched (lymphoid)"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P01718"/>
  <dbReference type="OpenTargets" id="ENSG00000211658"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000211658"/>
  <dbReference type="GeneTree" id="ENSGT00940000153120"/>
  <dbReference type="InParanoid" id="P01718"/>
  <dbReference type="OMA" id="CQTFDNS"/>
  <dbReference type="PhylomeDB" id="P01718"/>
  <dbReference type="PathwayCommons" id="P01718"/>
  <dbReference type="Reactome" id="R-HSA-166663">
    <property type="pathway name" value="Initial triggering of complement"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-173623">
    <property type="pathway name" value="Classical antibody-mediated complement activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-198933">
    <property type="pathway name" value="Immunoregulatory interactions between a Lymphoid and a non-Lymphoid cell"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-202733">
    <property type="pathway name" value="Cell surface interactions at the vascular wall"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2029481">
    <property type="pathway name" value="FCGR activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2029482">
    <property type="pathway name" value="Regulation of actin dynamics for phagocytic cup formation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2029485">
    <property type="pathway name" value="Role of phospholipids in phagocytosis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2168880">
    <property type="pathway name" value="Scavenging of heme from plasma"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2454202">
    <property type="pathway name" value="Fc epsilon receptor (FCERI) signaling"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2730905">
    <property type="pathway name" value="Role of LAT2/NTAL/LAB on calcium mobilization"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2871796">
    <property type="pathway name" value="FCERI mediated MAPK activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2871809">
    <property type="pathway name" value="FCERI mediated Ca+2 mobilization"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2871837">
    <property type="pathway name" value="FCERI mediated NF-kB activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-5690714">
    <property type="pathway name" value="CD22 mediated BCR regulation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9664323">
    <property type="pathway name" value="FCGR3A-mediated IL10 synthesis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9664422">
    <property type="pathway name" value="FCGR3A-mediated phagocytosis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9679191">
    <property type="pathway name" value="Potential therapeutics for SARS"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-977606">
    <property type="pathway name" value="Regulation of Complement cascade"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-983695">
    <property type="pathway name" value="Antigen activates B Cell Receptor (BCR) leading to generation of second messengers"/>
  </dbReference>
  <dbReference type="Pharos" id="P01718">
    <property type="development level" value="Tdark"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P01718"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 22"/>
  </dbReference>
  <dbReference type="RNAct" id="P01718">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000211658">
    <property type="expression patterns" value="Expressed in lymph node and 82 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019814">
    <property type="term" value="C:immunoglobulin complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003823">
    <property type="term" value="F:antigen binding"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002250">
    <property type="term" value="P:adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006955">
    <property type="term" value="P:immune response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.40.10:FF:000620">
    <property type="entry name" value="Immunoglobulin lambda locus"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003599">
    <property type="entry name" value="Ig_sub"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013106">
    <property type="entry name" value="Ig_V-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050150">
    <property type="entry name" value="IgV_Light_Chain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23267:SF454">
    <property type="entry name" value="IMMUNOGLOBULIN LAMBDA VARIABLE 3-27"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23267">
    <property type="entry name" value="IMMUNOGLOBULIN LIGHT CHAIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07686">
    <property type="entry name" value="V-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00409">
    <property type="entry name" value="IG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00406">
    <property type="entry name" value="IGv"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-1280">Immunoglobulin</keyword>
  <keyword id="KW-0393">Immunoglobulin domain</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000059847" description="Immunoglobulin lambda variable 3-27" evidence="3">
    <location>
      <begin position="21"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="domain" description="Ig-like" evidence="2">
    <location>
      <begin position="21"/>
      <end position="113" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-1" evidence="1">
    <location>
      <begin position="20"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-1" evidence="1">
    <location>
      <begin position="42"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-2" evidence="1">
    <location>
      <begin position="51"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-2" evidence="1">
    <location>
      <begin position="68"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-3" evidence="1">
    <location>
      <begin position="71"/>
      <end position="106"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-3" evidence="1">
    <location>
      <begin position="107"/>
      <end position="113" status="greater than"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="41"/>
      <end position="106"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>E</original>
    <variation>A</variation>
    <location>
      <position position="22"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>S</original>
    <variation>P</variation>
    <location>
      <position position="27"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>R</original>
    <variation>V</variation>
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>VLAKKYAR</original>
    <variation>NLEKTFVS</variation>
    <location>
      <begin position="45"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>K</original>
    <variation>R</variation>
    <location>
      <position position="57"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>APV</original>
    <variation>SPL</variation>
    <location>
      <begin position="61"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>KD</original>
    <variation>HT</variation>
    <location>
      <begin position="68"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>G</original>
    <variation>E</variation>
    <location>
      <position position="75"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>TTV</original>
    <variation>ATA</variation>
    <location>
      <begin position="87"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>VE</original>
    <variation>SV</variation>
    <location>
      <begin position="98"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>YCYSAADNN</original>
    <variation>FCQTWDTITA</variation>
    <location>
      <begin position="105"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="113"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01721"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00114"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="5549568"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="11872955"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="17576170"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="20176268"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="22158414"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="24600447"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source ref="4"/>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="5549568"/>
    </source>
  </evidence>
  <sequence length="113" mass="12165" checksum="2C653D17B5D75F7F" modified="2016-11-02" version="2" precursor="true">MAWIPLLLPLLILCTVSVASYELTQPSSVSVSPGQTARITCSGDVLAKKYARWFQQKPGQAPVLVIYKDSERPSGIPERFSGSSSGTTVTLTISGAQVEDEADYYCYSAADNN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>