<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-09-26" modified="2024-11-27" version="177" xmlns="http://uniprot.org/uniprot">
  <accession>Q86SG5</accession>
  <accession>D3DV38</accession>
  <accession>Q5SY69</accession>
  <name>S1A7A_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Protein S100-A7A</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>S100 calcium-binding protein A15</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>S100 calcium-binding protein A7-like 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>S100 calcium-binding protein A7A</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">S100A7A</name>
    <name type="synonym">S100A15</name>
    <name type="synonym">S100A7L1</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="FASEB J." volume="17" first="1969" last="1971">
      <title>Molecular cloning and characterization of alternatively spliced mRNA isoforms from psoriatic skin encoding a novel member of the S100 family.</title>
      <authorList>
        <person name="Wolf R."/>
        <person name="Mirmohammadsadegh A."/>
        <person name="Walz M."/>
        <person name="Lysa B."/>
        <person name="Tartler U."/>
        <person name="Remus R."/>
        <person name="Hengge U."/>
        <person name="Michel G."/>
        <person name="Ruzicka T."/>
      </authorList>
      <dbReference type="PubMed" id="12923069"/>
      <dbReference type="DOI" id="10.1096/fj.03-0148fje"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <source>
      <tissue>Epidermis</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="Nature" volume="441" first="315" last="321">
      <title>The DNA sequence and biological annotation of human chromosome 1.</title>
      <authorList>
        <person name="Gregory S.G."/>
        <person name="Barlow K.F."/>
        <person name="McLay K.E."/>
        <person name="Kaul R."/>
        <person name="Swarbreck D."/>
        <person name="Dunham A."/>
        <person name="Scott C.E."/>
        <person name="Howe K.L."/>
        <person name="Woodfine K."/>
        <person name="Spencer C.C.A."/>
        <person name="Jones M.C."/>
        <person name="Gillson C."/>
        <person name="Searle S."/>
        <person name="Zhou Y."/>
        <person name="Kokocinski F."/>
        <person name="McDonald L."/>
        <person name="Evans R."/>
        <person name="Phillips K."/>
        <person name="Atkinson A."/>
        <person name="Cooper R."/>
        <person name="Jones C."/>
        <person name="Hall R.E."/>
        <person name="Andrews T.D."/>
        <person name="Lloyd C."/>
        <person name="Ainscough R."/>
        <person name="Almeida J.P."/>
        <person name="Ambrose K.D."/>
        <person name="Anderson F."/>
        <person name="Andrew R.W."/>
        <person name="Ashwell R.I.S."/>
        <person name="Aubin K."/>
        <person name="Babbage A.K."/>
        <person name="Bagguley C.L."/>
        <person name="Bailey J."/>
        <person name="Beasley H."/>
        <person name="Bethel G."/>
        <person name="Bird C.P."/>
        <person name="Bray-Allen S."/>
        <person name="Brown J.Y."/>
        <person name="Brown A.J."/>
        <person name="Buckley D."/>
        <person name="Burton J."/>
        <person name="Bye J."/>
        <person name="Carder C."/>
        <person name="Chapman J.C."/>
        <person name="Clark S.Y."/>
        <person name="Clarke G."/>
        <person name="Clee C."/>
        <person name="Cobley V."/>
        <person name="Collier R.E."/>
        <person name="Corby N."/>
        <person name="Coville G.J."/>
        <person name="Davies J."/>
        <person name="Deadman R."/>
        <person name="Dunn M."/>
        <person name="Earthrowl M."/>
        <person name="Ellington A.G."/>
        <person name="Errington H."/>
        <person name="Frankish A."/>
        <person name="Frankland J."/>
        <person name="French L."/>
        <person name="Garner P."/>
        <person name="Garnett J."/>
        <person name="Gay L."/>
        <person name="Ghori M.R.J."/>
        <person name="Gibson R."/>
        <person name="Gilby L.M."/>
        <person name="Gillett W."/>
        <person name="Glithero R.J."/>
        <person name="Grafham D.V."/>
        <person name="Griffiths C."/>
        <person name="Griffiths-Jones S."/>
        <person name="Grocock R."/>
        <person name="Hammond S."/>
        <person name="Harrison E.S.I."/>
        <person name="Hart E."/>
        <person name="Haugen E."/>
        <person name="Heath P.D."/>
        <person name="Holmes S."/>
        <person name="Holt K."/>
        <person name="Howden P.J."/>
        <person name="Hunt A.R."/>
        <person name="Hunt S.E."/>
        <person name="Hunter G."/>
        <person name="Isherwood J."/>
        <person name="James R."/>
        <person name="Johnson C."/>
        <person name="Johnson D."/>
        <person name="Joy A."/>
        <person name="Kay M."/>
        <person name="Kershaw J.K."/>
        <person name="Kibukawa M."/>
        <person name="Kimberley A.M."/>
        <person name="King A."/>
        <person name="Knights A.J."/>
        <person name="Lad H."/>
        <person name="Laird G."/>
        <person name="Lawlor S."/>
        <person name="Leongamornlert D.A."/>
        <person name="Lloyd D.M."/>
        <person name="Loveland J."/>
        <person name="Lovell J."/>
        <person name="Lush M.J."/>
        <person name="Lyne R."/>
        <person name="Martin S."/>
        <person name="Mashreghi-Mohammadi M."/>
        <person name="Matthews L."/>
        <person name="Matthews N.S.W."/>
        <person name="McLaren S."/>
        <person name="Milne S."/>
        <person name="Mistry S."/>
        <person name="Moore M.J.F."/>
        <person name="Nickerson T."/>
        <person name="O'Dell C.N."/>
        <person name="Oliver K."/>
        <person name="Palmeiri A."/>
        <person name="Palmer S.A."/>
        <person name="Parker A."/>
        <person name="Patel D."/>
        <person name="Pearce A.V."/>
        <person name="Peck A.I."/>
        <person name="Pelan S."/>
        <person name="Phelps K."/>
        <person name="Phillimore B.J."/>
        <person name="Plumb R."/>
        <person name="Rajan J."/>
        <person name="Raymond C."/>
        <person name="Rouse G."/>
        <person name="Saenphimmachak C."/>
        <person name="Sehra H.K."/>
        <person name="Sheridan E."/>
        <person name="Shownkeen R."/>
        <person name="Sims S."/>
        <person name="Skuce C.D."/>
        <person name="Smith M."/>
        <person name="Steward C."/>
        <person name="Subramanian S."/>
        <person name="Sycamore N."/>
        <person name="Tracey A."/>
        <person name="Tromans A."/>
        <person name="Van Helmond Z."/>
        <person name="Wall M."/>
        <person name="Wallis J.M."/>
        <person name="White S."/>
        <person name="Whitehead S.L."/>
        <person name="Wilkinson J.E."/>
        <person name="Willey D.L."/>
        <person name="Williams H."/>
        <person name="Wilming L."/>
        <person name="Wray P.W."/>
        <person name="Wu Z."/>
        <person name="Coulson A."/>
        <person name="Vaudin M."/>
        <person name="Sulston J.E."/>
        <person name="Durbin R.M."/>
        <person name="Hubbard T."/>
        <person name="Wooster R."/>
        <person name="Dunham I."/>
        <person name="Carter N.P."/>
        <person name="McVean G."/>
        <person name="Ross M.T."/>
        <person name="Harrow J."/>
        <person name="Olson M.V."/>
        <person name="Beck S."/>
        <person name="Rogers J."/>
        <person name="Bentley D.R."/>
      </authorList>
      <dbReference type="PubMed" id="16710414"/>
      <dbReference type="DOI" id="10.1038/nature04727"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2005-09" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Istrail S."/>
        <person name="Sutton G.G."/>
        <person name="Florea L."/>
        <person name="Halpern A.L."/>
        <person name="Mobarry C.M."/>
        <person name="Lippert R."/>
        <person name="Walenz B."/>
        <person name="Shatkay H."/>
        <person name="Dew I."/>
        <person name="Miller J.R."/>
        <person name="Flanigan M.J."/>
        <person name="Edwards N.J."/>
        <person name="Bolanos R."/>
        <person name="Fasulo D."/>
        <person name="Halldorsson B.V."/>
        <person name="Hannenhalli S."/>
        <person name="Turner R."/>
        <person name="Yooseph S."/>
        <person name="Lu F."/>
        <person name="Nusskern D.R."/>
        <person name="Shue B.C."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F."/>
        <person name="Delcher A.L."/>
        <person name="Huson D.H."/>
        <person name="Kravitz S.A."/>
        <person name="Mouchard L."/>
        <person name="Reinert K."/>
        <person name="Remington K.A."/>
        <person name="Clark A.G."/>
        <person name="Waterman M.S."/>
        <person name="Eichler E.E."/>
        <person name="Adams M.D."/>
        <person name="Hunkapiller M.W."/>
        <person name="Myers E.W."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2003" name="J. Mol. Evol." volume="56" first="397" last="406">
      <title>Genomic and phylogenetic analysis of the S100A7 (psoriasin) gene duplications within the region of the S100 gene cluster on human chromosome 1q21.</title>
      <authorList>
        <person name="Kulski J.K."/>
        <person name="Lim C.P."/>
        <person name="Dunn D.S."/>
        <person name="Bellgard M."/>
      </authorList>
      <dbReference type="PubMed" id="12664160"/>
      <dbReference type="DOI" id="10.1007/s00239-002-2410-5"/>
    </citation>
    <scope>GENOMIC ORGANIZATION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="BMC Struct. Biol." volume="12" first="16" last="16">
      <title>Structural characterization of S100A15 reveals a novel zinc coordination site among S100 proteins and altered surface chemistry with functional implications for receptor binding.</title>
      <authorList>
        <person name="Murray J.I."/>
        <person name="Tonkin M.L."/>
        <person name="Whiting A.L."/>
        <person name="Peng F."/>
        <person name="Farnell B."/>
        <person name="Cullen J.T."/>
        <person name="Hof F."/>
        <person name="Boulanger M.J."/>
      </authorList>
      <dbReference type="PubMed" id="22747601"/>
      <dbReference type="DOI" id="10.1186/1472-6807-12-16"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.7 ANGSTROMS)</scope>
    <scope>CALCIUM-BINDING SITES</scope>
    <scope>ZINC-BINDING SITES</scope>
  </reference>
  <comment type="function">
    <text>May be involved in epidermal differentiation and inflammation and might therefore be important for the pathogenesis of psoriasis and other diseases.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-13077820">
      <id>Q86SG5</id>
    </interactant>
    <interactant intactId="EBI-12357161">
      <id>Q5SYC1</id>
      <label>CLVS2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-13077820">
      <id>Q86SG5</id>
    </interactant>
    <interactant intactId="EBI-12006206">
      <id>Q5SY68</id>
      <label>S100A7L2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>8</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Overexpressed in psoriasis.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the S-100 family.</text>
  </comment>
  <dbReference type="EMBL" id="AY189117">
    <property type="protein sequence ID" value="AAO40032.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY189118">
    <property type="protein sequence ID" value="AAO40033.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY189119">
    <property type="protein sequence ID" value="AAO40034.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL591704">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH471121">
    <property type="protein sequence ID" value="EAW53328.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH471121">
    <property type="protein sequence ID" value="EAW53329.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BR000043">
    <property type="protein sequence ID" value="FAA00014.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS30872.1"/>
  <dbReference type="RefSeq" id="NP_789793.1">
    <property type="nucleotide sequence ID" value="NM_176823.3"/>
  </dbReference>
  <dbReference type="PDB" id="4AQI">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDBsum" id="4AQI"/>
  <dbReference type="AlphaFoldDB" id="Q86SG5"/>
  <dbReference type="SMR" id="Q86SG5"/>
  <dbReference type="BioGRID" id="130711">
    <property type="interactions" value="108"/>
  </dbReference>
  <dbReference type="IntAct" id="Q86SG5">
    <property type="interactions" value="75"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000357718"/>
  <dbReference type="GlyGen" id="Q86SG5">
    <property type="glycosylation" value="1 site, 1 O-linked glycan (1 site)"/>
  </dbReference>
  <dbReference type="iPTMnet" id="Q86SG5"/>
  <dbReference type="PhosphoSitePlus" id="Q86SG5"/>
  <dbReference type="BioMuta" id="S100A7A"/>
  <dbReference type="DMDM" id="37088296"/>
  <dbReference type="jPOST" id="Q86SG5"/>
  <dbReference type="MassIVE" id="Q86SG5"/>
  <dbReference type="PaxDb" id="9606-ENSP00000357718"/>
  <dbReference type="PeptideAtlas" id="Q86SG5"/>
  <dbReference type="PRIDE" id="Q86SG5"/>
  <dbReference type="ProteomicsDB" id="69587"/>
  <dbReference type="Pumba" id="Q86SG5"/>
  <dbReference type="Antibodypedia" id="56992">
    <property type="antibodies" value="38 antibodies from 11 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="338324"/>
  <dbReference type="Ensembl" id="ENST00000329256.2">
    <property type="protein sequence ID" value="ENSP00000329008.2"/>
    <property type="gene ID" value="ENSG00000184330.12"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000368728.2">
    <property type="protein sequence ID" value="ENSP00000357717.1"/>
    <property type="gene ID" value="ENSG00000184330.12"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000368729.9">
    <property type="protein sequence ID" value="ENSP00000357718.3"/>
    <property type="gene ID" value="ENSG00000184330.12"/>
  </dbReference>
  <dbReference type="GeneID" id="338324"/>
  <dbReference type="KEGG" id="hsa:338324"/>
  <dbReference type="MANE-Select" id="ENST00000368729.9">
    <property type="protein sequence ID" value="ENSP00000357718.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_176823.4"/>
    <property type="RefSeq protein sequence ID" value="NP_789793.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc001fbt.1">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:21657"/>
  <dbReference type="CTD" id="338324"/>
  <dbReference type="DisGeNET" id="338324"/>
  <dbReference type="GeneCards" id="S100A7A"/>
  <dbReference type="HGNC" id="HGNC:21657">
    <property type="gene designation" value="S100A7A"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000184330">
    <property type="expression patterns" value="Group enriched (esophagus, lymphoid tissue, skin, vagina)"/>
  </dbReference>
  <dbReference type="MIM" id="617427">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_Q86SG5"/>
  <dbReference type="OpenTargets" id="ENSG00000184330"/>
  <dbReference type="PharmGKB" id="PA162402333"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000184330"/>
  <dbReference type="eggNOG" id="ENOG502SZJ5">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000163488"/>
  <dbReference type="HOGENOM" id="CLU_138624_5_0_1"/>
  <dbReference type="InParanoid" id="Q86SG5"/>
  <dbReference type="OMA" id="NTQARRF"/>
  <dbReference type="OrthoDB" id="4637762at2759"/>
  <dbReference type="PhylomeDB" id="Q86SG5"/>
  <dbReference type="TreeFam" id="TF341148"/>
  <dbReference type="PathwayCommons" id="Q86SG5"/>
  <dbReference type="Reactome" id="R-HSA-6799990">
    <property type="pathway name" value="Metal sequestration by antimicrobial proteins"/>
  </dbReference>
  <dbReference type="SignaLink" id="Q86SG5"/>
  <dbReference type="BioGRID-ORCS" id="338324">
    <property type="hits" value="23 hits in 1045 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="S100A7A">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="Q86SG5"/>
  <dbReference type="GeneWiki" id="S100A15"/>
  <dbReference type="GenomeRNAi" id="338324"/>
  <dbReference type="Pharos" id="Q86SG5">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q86SG5"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="RNAct" id="Q86SG5">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000184330">
    <property type="expression patterns" value="Expressed in male germ line stem cell (sensu Vertebrata) in testis and 56 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HPA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048306">
    <property type="term" value="F:calcium-dependent protein binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046914">
    <property type="term" value="F:transition metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00213">
    <property type="entry name" value="S-100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.238.10:FF:000296">
    <property type="entry name" value="Protein S100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.238.10">
    <property type="entry name" value="EF-hand"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011992">
    <property type="entry name" value="EF-hand-dom_pair"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018247">
    <property type="entry name" value="EF_Hand_1_Ca_BS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002048">
    <property type="entry name" value="EF_hand_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR034325">
    <property type="entry name" value="S-100_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001751">
    <property type="entry name" value="S100/CaBP7/8-like_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013787">
    <property type="entry name" value="S100_Ca-bd_sub"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11639:SF67">
    <property type="entry name" value="PROTEIN S100-A7A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11639">
    <property type="entry name" value="S100 CALCIUM-BINDING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01023">
    <property type="entry name" value="S_100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01394">
    <property type="entry name" value="S_100"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47473">
    <property type="entry name" value="EF-hand"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00018">
    <property type="entry name" value="EF_HAND_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50222">
    <property type="entry name" value="EF_HAND_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00303">
    <property type="entry name" value="S100_CABP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <feature type="chain" id="PRO_0000143991" description="Protein S100-A7A">
    <location>
      <begin position="1"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 1" evidence="3">
    <location>
      <begin position="13"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 2" evidence="2">
    <location>
      <begin position="50"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="binding site">
    <location>
      <position position="18"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site">
    <location>
      <position position="28"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site">
    <location>
      <position position="38"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="63"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="65"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site">
    <location>
      <position position="66"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="67"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="69"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="74"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
      <note>high affinity</note>
    </ligand>
  </feature>
  <feature type="binding site">
    <location>
      <position position="87"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site">
    <location>
      <position position="91"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="sequence variant" id="VAR_061048" description="In dbSNP:rs55985140.">
    <original>R</original>
    <variation>H</variation>
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_048468" description="In dbSNP:rs3006414.">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="84"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="5"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="29"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="41"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="54"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="67"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="72"/>
      <end position="90"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00448"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="4AQI"/>
    </source>
  </evidence>
  <sequence length="101" mass="11305" checksum="A311A921414C9732" modified="2007-01-23" version="3">MSNTQAERSIIGMIDMFHKYTGRDGKIEKPSLLTMMKENFPNFLSACDKKGIHYLATVFEKKDKNEDKKIDFSEFLSLLGDIAADYHKQSHGAAPCSGGSQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>