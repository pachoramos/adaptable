<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1992-03-01" modified="2024-11-27" version="98" xmlns="http://uniprot.org/uniprot">
  <accession>P24336</accession>
  <name>SIX2_HOTJU</name>
  <protein>
    <recommendedName>
      <fullName>Beta-insect depressant toxin BjIT2</fullName>
      <shortName>IT-2</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Hottentotta judaicus</name>
    <name type="common">Black scorpion</name>
    <name type="synonym">Buthotus judaicus</name>
    <dbReference type="NCBI Taxonomy" id="6863"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Hottentotta</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="Toxicon" volume="29" first="1155" last="1158">
      <title>The cDNA sequence of a depressant insect selective neurotoxin from the scorpion Buthotus judaicus.</title>
      <authorList>
        <person name="Zilberberg N."/>
        <person name="Zlotkin E."/>
        <person name="Gurevitz M."/>
      </authorList>
      <dbReference type="PubMed" id="1796479"/>
      <dbReference type="DOI" id="10.1016/0041-0101(91)90213-b"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="Arch. Insect Biochem. Physiol." volume="22" first="55" last="73">
      <title>Depressant insect selective neurotoxins from scorpion venom: chemistry, action, and gene cloning.</title>
      <authorList>
        <person name="Zlotkin E."/>
        <person name="Gurevitz M."/>
        <person name="Fowler E."/>
        <person name="Adams M.E."/>
      </authorList>
      <dbReference type="PubMed" id="8431601"/>
      <dbReference type="DOI" id="10.1002/arch.940220107"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1990" name="Toxicon" volume="28" first="170" last="170">
      <title>On the chemistry and action of the depressant insect toxins.</title>
      <authorList>
        <person name="Zlotkin E."/>
        <person name="Fowler E."/>
        <person name="Eitan M."/>
        <person name="Moyer M."/>
        <person name="Adams M.E."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE OF 22-82</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1991" name="Biochemistry" volume="30" first="4814" last="4821">
      <title>Functional duality and structural uniqueness of depressant insect-selective neurotoxins.</title>
      <authorList>
        <person name="Zlotkin E."/>
        <person name="Eitan M."/>
        <person name="Bindokas V.P."/>
        <person name="Adams M.E."/>
        <person name="Moyer M."/>
        <person name="Burkhart W."/>
        <person name="Fowler E."/>
      </authorList>
      <dbReference type="PubMed" id="2029523"/>
      <dbReference type="DOI" id="10.1021/bi00233a025"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 22-82</scope>
    <scope>CHARACTERIZATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Depressant insect beta-toxins cause a transient contraction paralysis followed by a slow flaccid paralysis. They bind voltage-independently at site-4 of sodium channels (Nav) and shift the voltage of activation toward more negative potentials thereby affecting sodium channel activation and promoting spontaneous and repetitive firing. This toxin is active only on insects.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="4">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="PTM">
    <text>C-terminal basic residues are removed by a carboxypeptidase.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="S55535">
    <property type="protein sequence ID" value="AAB25385.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A40472">
    <property type="entry name" value="A40472"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P24336"/>
  <dbReference type="SMR" id="P24336"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2 3">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000035197" description="Beta-insect depressant toxin BjIT2">
    <location>
      <begin position="22"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000035198" description="Removed by a carboxypeptidase">
    <location>
      <begin position="83"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="22"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="35"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="42"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="46"/>
      <end position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="2029523"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source ref="3"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="85" mass="9349" checksum="7E8CE58BBD211EB6" modified="1992-03-01" version="1" precursor="true">MKLLLLLVISASMLLECLVNADGYIRKKDGCKVSCIIGNEGCRKECVAHGGSFGYCWTWGLACWCENLPDAVTWKSSTNTCGRKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>