<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-05-20" modified="2024-01-24" version="32" xmlns="http://uniprot.org/uniprot">
  <accession>P85254</accession>
  <accession>B3W6I0</accession>
  <name>CTX12_LACTA</name>
  <protein>
    <recommendedName>
      <fullName>M-zodatoxin-Lt8b</fullName>
      <shortName>M-ZDTX-Lt8b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Cytoinsectotoxin-1b</fullName>
      <shortName evidence="5">CIT-1b</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">cit 1-2</name>
  </gene>
  <organism>
    <name type="scientific">Lachesana tarabaevi</name>
    <name type="common">Spider</name>
    <dbReference type="NCBI Taxonomy" id="379576"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Entelegynae incertae sedis</taxon>
      <taxon>Zodariidae</taxon>
      <taxon>Lachesana</taxon>
    </lineage>
  </organism>
  <reference evidence="7" key="1">
    <citation type="journal article" date="2008" name="Biochem. J." volume="411" first="687" last="696">
      <title>Cyto-insectotoxins, a novel class of cytolytic and insecticidal peptides from spider venom.</title>
      <authorList>
        <person name="Vassilevski A.A."/>
        <person name="Kozlov S.A."/>
        <person name="Samsonova O.V."/>
        <person name="Egorova N.S."/>
        <person name="Karpunin D.V."/>
        <person name="Pluzhnikov K.A."/>
        <person name="Feofanov A.V."/>
        <person name="Grishin E.V."/>
      </authorList>
      <dbReference type="PubMed" id="18215128"/>
      <dbReference type="DOI" id="10.1042/bj20071123"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 61-129</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue evidence="3">Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2016" name="Biochem. J." volume="473" first="2495" last="2506">
      <title>Lachesana tarabaevi, an expert in membrane-active toxins.</title>
      <authorList>
        <person name="Kuzmenkov A.I."/>
        <person name="Sachkova M.Y."/>
        <person name="Kovalchuk S.I."/>
        <person name="Grishin E.V."/>
        <person name="Vassilevski A.A."/>
      </authorList>
      <dbReference type="PubMed" id="27287558"/>
      <dbReference type="DOI" id="10.1042/bcj20160436"/>
    </citation>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>PQM MOTIF</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="6">Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Insecticidal, cytolytic and antimicrobial peptide. Forms voltage-dependent, ion-permeable channels in membranes. At high concentration causes cell membrane lysis (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3 4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">Both the N-terminus (61-94) and the C-terminus (99-129) of the mature peptide form alpha-helices which probably disrupt target cell membranes. The linker region (95-98) probably derives from a processing quadruplet motif (PQM), found in propeptides of many zodatoxins, hinting at a fusion of two originally separate membrane-active peptides.</text>
  </comment>
  <comment type="PTM">
    <text evidence="6">Cleavage of the propeptide depends on the processing quadruplet motif (XXXR, with at least one of X being E).</text>
  </comment>
  <comment type="mass spectrometry" mass="7906.1" method="MALDI" evidence="4"/>
  <comment type="similarity">
    <text evidence="7">Belongs to the cationic peptide 06 (cytoinsectotoxin) family.</text>
  </comment>
  <dbReference type="EMBL" id="FM165475">
    <property type="protein sequence ID" value="CAQ63551.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P85254"/>
  <dbReference type="SMR" id="P85254"/>
  <dbReference type="ArachnoServer" id="AS000439">
    <property type="toxin name" value="M-zodatoxin-Lt8b"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018802">
    <property type="entry name" value="Latarcin_precursor"/>
  </dbReference>
  <dbReference type="Pfam" id="PF10279">
    <property type="entry name" value="Latarcin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000366075" evidence="3">
    <location>
      <begin position="21"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000337159" description="M-zodatoxin-Lt8b">
    <location>
      <begin position="61"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Processing quadruplet motif" evidence="6">
    <location>
      <begin position="57"/>
      <end position="60"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P85253"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18215128"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="27287558"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="18215128"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="27287558"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="129" mass="14562" checksum="B59C8F7EF126F3F0" modified="2009-03-03" version="2" precursor="true">MKYFVVALALVAAFACIAESKPAESEHELAEVEEENELADLEDAVWLEHLADLSDLEEARGFFGNTWKKIKGKADKIMLKKAVKLMVKKEGISKEEAQAKVDAMSKKQIRLYLLKYYGKKALQKASEKL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>