<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-05-24" modified="2024-11-27" version="95" xmlns="http://uniprot.org/uniprot">
  <accession>Q8WNR9</accession>
  <name>CYTA_FELCA</name>
  <protein>
    <recommendedName>
      <fullName>Cystatin-A</fullName>
    </recommendedName>
    <allergenName>Fel d 3</allergenName>
  </protein>
  <gene>
    <name type="primary">CSTA</name>
  </gene>
  <organism>
    <name type="scientific">Felis catus</name>
    <name type="common">Cat</name>
    <name type="synonym">Felis silvestris catus</name>
    <dbReference type="NCBI Taxonomy" id="9685"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Carnivora</taxon>
      <taxon>Feliformia</taxon>
      <taxon>Felidae</taxon>
      <taxon>Felinae</taxon>
      <taxon>Felis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Clin. Exp. Allergy" volume="31" first="1279" last="1286">
      <title>Molecular cloning, expression and modelling of cat allergen, cystatin (Fel d 3), a cysteine protease inhibitor.</title>
      <authorList>
        <person name="Ichikawa K."/>
        <person name="Vailes L.D."/>
        <person name="Pomes A."/>
        <person name="Chapman M.D."/>
      </authorList>
      <dbReference type="PubMed" id="11529899"/>
      <dbReference type="DOI" id="10.1046/j.1365-2222.2001.01169.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>ALLERGEN</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">This is an intracellular thiol proteinase inhibitor.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="allergen">
    <text evidence="3">Causes an allergic reaction in human. Binds to IgE.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the cystatin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF238996">
    <property type="protein sequence ID" value="AAL49391.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001009864.1">
    <property type="nucleotide sequence ID" value="NM_001009864.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8WNR9"/>
  <dbReference type="SMR" id="Q8WNR9"/>
  <dbReference type="STRING" id="9685.ENSFCAP00000008082"/>
  <dbReference type="Allergome" id="3280">
    <property type="allergen name" value="Fel d 3.0101"/>
  </dbReference>
  <dbReference type="Allergome" id="347">
    <property type="allergen name" value="Fel d 3"/>
  </dbReference>
  <dbReference type="MEROPS" id="I25.001"/>
  <dbReference type="PaxDb" id="9685-ENSFCAP00000008082"/>
  <dbReference type="GeneID" id="493942"/>
  <dbReference type="KEGG" id="fca:493942"/>
  <dbReference type="CTD" id="1475"/>
  <dbReference type="eggNOG" id="ENOG502SF2X">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="Q8WNR9"/>
  <dbReference type="OrthoDB" id="179019at2759"/>
  <dbReference type="Proteomes" id="UP000011712">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00042">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.450.10:FF:000001">
    <property type="entry name" value="Cystatin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.450.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000010">
    <property type="entry name" value="Cystatin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046350">
    <property type="entry name" value="Cystatin_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018073">
    <property type="entry name" value="Prot_inh_cystat_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001713">
    <property type="entry name" value="Prot_inh_stefin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11414">
    <property type="entry name" value="CYSTATIN FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11414:SF20">
    <property type="entry name" value="CYSTATIN-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00031">
    <property type="entry name" value="Cystatin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00295">
    <property type="entry name" value="STEFINA"/>
  </dbReference>
  <dbReference type="SMART" id="SM00043">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54403">
    <property type="entry name" value="Cystatin/monellin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00287">
    <property type="entry name" value="CYSTATIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0020">Allergen</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <feature type="chain" id="PRO_0000207127" description="Cystatin-A">
    <location>
      <begin position="1"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Secondary area of contact">
    <location>
      <begin position="46"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="site" description="Reactive site" evidence="1">
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="modified residue" description="N-acetylmethionine" evidence="2">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P01039"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="11529899"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="98" mass="11041" checksum="77E25F5567E0624D" modified="2002-03-01" version="1">MIPGGLSEAKPATPEIQEIANEVKPQLEEKTNETYQKFEAIEYKTQVVAGINYYIKVQVDDNRYIHIKVFKGLPVQDSSLTLTGYQTGKSEDDELTGF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>