<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-03-01" modified="2024-11-27" version="155" xmlns="http://uniprot.org/uniprot">
  <accession>P60521</accession>
  <accession>O08765</accession>
  <accession>Q9DCP8</accession>
  <accession>Q9UQF7</accession>
  <name>GBRL2_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName evidence="10">Gamma-aminobutyric acid receptor-associated protein-like 2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>GABA(A) receptor-associated protein-like 2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Golgi-associated ATPase enhancer of 16 kDa</fullName>
      <shortName>GATE-16</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="11" type="primary">Gabarapl2</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Genomics" volume="74" first="408" last="413">
      <title>Cloning, expression patterns, and chromosome localization of three human and two mouse homologues of GABA(A) receptor-associated protein.</title>
      <authorList>
        <person name="Xin Y."/>
        <person name="Yu L."/>
        <person name="Chen Z."/>
        <person name="Zheng L."/>
        <person name="Fu Q."/>
        <person name="Jiang J."/>
        <person name="Zhang P."/>
        <person name="Gong R."/>
        <person name="Zhao S."/>
      </authorList>
      <dbReference type="PubMed" id="11414770"/>
      <dbReference type="DOI" id="10.1006/geno.2001.6555"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Science" volume="309" first="1559" last="1563">
      <title>The transcriptional landscape of the mammalian genome.</title>
      <authorList>
        <person name="Carninci P."/>
        <person name="Kasukawa T."/>
        <person name="Katayama S."/>
        <person name="Gough J."/>
        <person name="Frith M.C."/>
        <person name="Maeda N."/>
        <person name="Oyama R."/>
        <person name="Ravasi T."/>
        <person name="Lenhard B."/>
        <person name="Wells C."/>
        <person name="Kodzius R."/>
        <person name="Shimokawa K."/>
        <person name="Bajic V.B."/>
        <person name="Brenner S.E."/>
        <person name="Batalov S."/>
        <person name="Forrest A.R."/>
        <person name="Zavolan M."/>
        <person name="Davis M.J."/>
        <person name="Wilming L.G."/>
        <person name="Aidinis V."/>
        <person name="Allen J.E."/>
        <person name="Ambesi-Impiombato A."/>
        <person name="Apweiler R."/>
        <person name="Aturaliya R.N."/>
        <person name="Bailey T.L."/>
        <person name="Bansal M."/>
        <person name="Baxter L."/>
        <person name="Beisel K.W."/>
        <person name="Bersano T."/>
        <person name="Bono H."/>
        <person name="Chalk A.M."/>
        <person name="Chiu K.P."/>
        <person name="Choudhary V."/>
        <person name="Christoffels A."/>
        <person name="Clutterbuck D.R."/>
        <person name="Crowe M.L."/>
        <person name="Dalla E."/>
        <person name="Dalrymple B.P."/>
        <person name="de Bono B."/>
        <person name="Della Gatta G."/>
        <person name="di Bernardo D."/>
        <person name="Down T."/>
        <person name="Engstrom P."/>
        <person name="Fagiolini M."/>
        <person name="Faulkner G."/>
        <person name="Fletcher C.F."/>
        <person name="Fukushima T."/>
        <person name="Furuno M."/>
        <person name="Futaki S."/>
        <person name="Gariboldi M."/>
        <person name="Georgii-Hemming P."/>
        <person name="Gingeras T.R."/>
        <person name="Gojobori T."/>
        <person name="Green R.E."/>
        <person name="Gustincich S."/>
        <person name="Harbers M."/>
        <person name="Hayashi Y."/>
        <person name="Hensch T.K."/>
        <person name="Hirokawa N."/>
        <person name="Hill D."/>
        <person name="Huminiecki L."/>
        <person name="Iacono M."/>
        <person name="Ikeo K."/>
        <person name="Iwama A."/>
        <person name="Ishikawa T."/>
        <person name="Jakt M."/>
        <person name="Kanapin A."/>
        <person name="Katoh M."/>
        <person name="Kawasawa Y."/>
        <person name="Kelso J."/>
        <person name="Kitamura H."/>
        <person name="Kitano H."/>
        <person name="Kollias G."/>
        <person name="Krishnan S.P."/>
        <person name="Kruger A."/>
        <person name="Kummerfeld S.K."/>
        <person name="Kurochkin I.V."/>
        <person name="Lareau L.F."/>
        <person name="Lazarevic D."/>
        <person name="Lipovich L."/>
        <person name="Liu J."/>
        <person name="Liuni S."/>
        <person name="McWilliam S."/>
        <person name="Madan Babu M."/>
        <person name="Madera M."/>
        <person name="Marchionni L."/>
        <person name="Matsuda H."/>
        <person name="Matsuzawa S."/>
        <person name="Miki H."/>
        <person name="Mignone F."/>
        <person name="Miyake S."/>
        <person name="Morris K."/>
        <person name="Mottagui-Tabar S."/>
        <person name="Mulder N."/>
        <person name="Nakano N."/>
        <person name="Nakauchi H."/>
        <person name="Ng P."/>
        <person name="Nilsson R."/>
        <person name="Nishiguchi S."/>
        <person name="Nishikawa S."/>
        <person name="Nori F."/>
        <person name="Ohara O."/>
        <person name="Okazaki Y."/>
        <person name="Orlando V."/>
        <person name="Pang K.C."/>
        <person name="Pavan W.J."/>
        <person name="Pavesi G."/>
        <person name="Pesole G."/>
        <person name="Petrovsky N."/>
        <person name="Piazza S."/>
        <person name="Reed J."/>
        <person name="Reid J.F."/>
        <person name="Ring B.Z."/>
        <person name="Ringwald M."/>
        <person name="Rost B."/>
        <person name="Ruan Y."/>
        <person name="Salzberg S.L."/>
        <person name="Sandelin A."/>
        <person name="Schneider C."/>
        <person name="Schoenbach C."/>
        <person name="Sekiguchi K."/>
        <person name="Semple C.A."/>
        <person name="Seno S."/>
        <person name="Sessa L."/>
        <person name="Sheng Y."/>
        <person name="Shibata Y."/>
        <person name="Shimada H."/>
        <person name="Shimada K."/>
        <person name="Silva D."/>
        <person name="Sinclair B."/>
        <person name="Sperling S."/>
        <person name="Stupka E."/>
        <person name="Sugiura K."/>
        <person name="Sultana R."/>
        <person name="Takenaka Y."/>
        <person name="Taki K."/>
        <person name="Tammoja K."/>
        <person name="Tan S.L."/>
        <person name="Tang S."/>
        <person name="Taylor M.S."/>
        <person name="Tegner J."/>
        <person name="Teichmann S.A."/>
        <person name="Ueda H.R."/>
        <person name="van Nimwegen E."/>
        <person name="Verardo R."/>
        <person name="Wei C.L."/>
        <person name="Yagi K."/>
        <person name="Yamanishi H."/>
        <person name="Zabarovsky E."/>
        <person name="Zhu S."/>
        <person name="Zimmer A."/>
        <person name="Hide W."/>
        <person name="Bult C."/>
        <person name="Grimmond S.M."/>
        <person name="Teasdale R.D."/>
        <person name="Liu E.T."/>
        <person name="Brusic V."/>
        <person name="Quackenbush J."/>
        <person name="Wahlestedt C."/>
        <person name="Mattick J.S."/>
        <person name="Hume D.A."/>
        <person name="Kai C."/>
        <person name="Sasaki D."/>
        <person name="Tomaru Y."/>
        <person name="Fukuda S."/>
        <person name="Kanamori-Katayama M."/>
        <person name="Suzuki M."/>
        <person name="Aoki J."/>
        <person name="Arakawa T."/>
        <person name="Iida J."/>
        <person name="Imamura K."/>
        <person name="Itoh M."/>
        <person name="Kato T."/>
        <person name="Kawaji H."/>
        <person name="Kawagashira N."/>
        <person name="Kawashima T."/>
        <person name="Kojima M."/>
        <person name="Kondo S."/>
        <person name="Konno H."/>
        <person name="Nakano K."/>
        <person name="Ninomiya N."/>
        <person name="Nishio T."/>
        <person name="Okada M."/>
        <person name="Plessy C."/>
        <person name="Shibata K."/>
        <person name="Shiraki T."/>
        <person name="Suzuki S."/>
        <person name="Tagami M."/>
        <person name="Waki K."/>
        <person name="Watahiki A."/>
        <person name="Okamura-Oho Y."/>
        <person name="Suzuki H."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
      </authorList>
      <dbReference type="PubMed" id="16141072"/>
      <dbReference type="DOI" id="10.1126/science.1112014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
      <tissue>Hippocampus</tissue>
      <tissue>Kidney</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
      <tissue>Brain</tissue>
      <tissue>Mammary gland</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2003" name="J. Biol. Chem." volume="278" first="51841" last="51850">
      <title>A single protease, Apg4B, is specific for the autophagy-related ubiquitin-like proteins GATE-16, MAP1-LC3, GABARAP, and Apg8L.</title>
      <authorList>
        <person name="Hemelaar J."/>
        <person name="Lelyveld V.S."/>
        <person name="Kessler B.M."/>
        <person name="Ploegh H.L."/>
      </authorList>
      <dbReference type="PubMed" id="14530254"/>
      <dbReference type="DOI" id="10.1074/jbc.m308762200"/>
    </citation>
    <scope>CLEAVAGE BY ATG4B</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2010" name="Cell" volume="143" first="1174" last="1189">
      <title>A tissue-specific atlas of mouse protein phosphorylation and expression.</title>
      <authorList>
        <person name="Huttlin E.L."/>
        <person name="Jedrychowski M.P."/>
        <person name="Elias J.E."/>
        <person name="Goswami T."/>
        <person name="Rad R."/>
        <person name="Beausoleil S.A."/>
        <person name="Villen J."/>
        <person name="Haas W."/>
        <person name="Sowa M.E."/>
        <person name="Gygi S.P."/>
      </authorList>
      <dbReference type="PubMed" id="21183079"/>
      <dbReference type="DOI" id="10.1016/j.cell.2010.12.001"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Brain</tissue>
      <tissue>Heart</tissue>
      <tissue>Kidney</tissue>
      <tissue>Lung</tissue>
      <tissue>Testis</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2011" name="J. Cell Biol." volume="192" first="839" last="853">
      <title>OATL1, a novel autophagosome-resident Rab33B-GAP, regulates autophagosomal maturation.</title>
      <authorList>
        <person name="Itoh T."/>
        <person name="Kanno E."/>
        <person name="Uemura T."/>
        <person name="Waguri S."/>
        <person name="Fukuda M."/>
      </authorList>
      <dbReference type="PubMed" id="21383079"/>
      <dbReference type="DOI" id="10.1083/jcb.201008107"/>
    </citation>
    <scope>INTERACTION WITH TBC1D25</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2002" name="Biochem. Biophys. Res. Commun." volume="292" first="256" last="262">
      <title>Murine Apg12p has a substrate preference for murine Apg7p over three Apg8p homologs.</title>
      <authorList>
        <person name="Tanida I."/>
        <person name="Tanida-Miyake E."/>
        <person name="Nishitani T."/>
        <person name="Komatsu M."/>
        <person name="Yamazaki H."/>
        <person name="Ueno T."/>
        <person name="Kominami E."/>
      </authorList>
      <dbReference type="PubMed" id="11890701"/>
      <dbReference type="DOI" id="10.1006/bbrc.2002.6645"/>
    </citation>
    <scope>INTERACTION WITH ATG7</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2016" name="Cell Rep." volume="14" first="411" last="421">
      <title>NCOA4 Deficiency Impairs Systemic Iron Homeostasis.</title>
      <authorList>
        <person name="Bellelli R."/>
        <person name="Federico G."/>
        <person name="Matte' A."/>
        <person name="Colecchia D."/>
        <person name="Iolascon A."/>
        <person name="Chiariello M."/>
        <person name="Santoro M."/>
        <person name="De Franceschi L."/>
        <person name="Carlomagno F."/>
      </authorList>
      <dbReference type="PubMed" id="26776506"/>
      <dbReference type="DOI" id="10.1016/j.celrep.2015.12.065"/>
    </citation>
    <scope>INTERACTION WITH NCOA4</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2021" name="Cell Death Differ." volume="28" first="2651" last="2672">
      <title>ATG4D is the main ATG8 delipidating enzyme in mammalian cells and protects against cerebellar neurodegeneration.</title>
      <authorList>
        <person name="Tamargo-Gomez I."/>
        <person name="Martinez-Garcia G.G."/>
        <person name="Suarez M.F."/>
        <person name="Rey V."/>
        <person name="Fueyo A."/>
        <person name="Codina-Martinez H."/>
        <person name="Bretones G."/>
        <person name="Caravia X.M."/>
        <person name="Morel E."/>
        <person name="Dupont N."/>
        <person name="Cabo R."/>
        <person name="Tomas-Zapico C."/>
        <person name="Souquere S."/>
        <person name="Pierron G."/>
        <person name="Codogno P."/>
        <person name="Lopez-Otin C."/>
        <person name="Fernandez A.F."/>
        <person name="Marino G."/>
      </authorList>
      <dbReference type="PubMed" id="33795848"/>
      <dbReference type="DOI" id="10.1038/s41418-021-00776-1"/>
    </citation>
    <scope>LIPIDATION</scope>
    <scope>DELIPIDATION</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2021" name="EMBO Rep." volume="22" first="e52289" last="e52289">
      <title>Role of FAM134 paralogues in endoplasmic reticulum remodeling, ER-phagy, and Collagen quality control.</title>
      <authorList>
        <person name="Reggio A."/>
        <person name="Buonomo V."/>
        <person name="Berkane R."/>
        <person name="Bhaskara R.M."/>
        <person name="Tellechea M."/>
        <person name="Peluso I."/>
        <person name="Polishchuk E."/>
        <person name="Di Lorenzo G."/>
        <person name="Cirillo C."/>
        <person name="Esposito M."/>
        <person name="Hussain A."/>
        <person name="Huebner A.K."/>
        <person name="Huebner C.A."/>
        <person name="Settembre C."/>
        <person name="Hummer G."/>
        <person name="Grumati P."/>
        <person name="Stolz A."/>
      </authorList>
      <dbReference type="PubMed" id="34338405"/>
      <dbReference type="DOI" id="10.15252/embr.202052289"/>
    </citation>
    <scope>INTERACTION WITH RETREG1; RETREG2 AND RETREG3</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2">Ubiquitin-like modifier involved in intra-Golgi traffic. Modulates intra-Golgi transport through coupling between NSF activity and SNAREs activation. It first stimulates the ATPase activity of NSF which in turn stimulates the association with GOSR1 (By similarity). Involved in autophagy. Plays a role in mitophagy which contributes to regulate mitochondrial quantity and quality by eliminating the mitochondria to a basal level to fulfill cellular energy requirements and preventing excess ROS production. Whereas LC3s are involved in elongation of the phagophore membrane, the GABARAP/GATE-16 subfamily is essential for a later stage in autophagosome maturation (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 2 4 6 7 9">Monomer (By similarity). Interacts with ATG3, ATG13 and ULK1 (By similarity). Interacts with ATG7 (PubMed:11890701). Interacts with TP53INP1 and TP53INP2 (By similarity). Interacts with TBC1D25 (PubMed:21383079). Directly interacts with SQSTM1 and BNIP3 (By similarity). Interacts with TECPR2 and PCM1 (By similarity). Interacts with TBC1D5 (By similarity). Interacts with TRIM5 (By similarity). Interacts with MEFV and TRIM21 (By similarity). Interacts with WDFY3 (By similarity). Interacts with UBA5; promoting recruitment of UBA5 to the endoplasmic reticulum membrane (By similarity). Interacts with GOSR1 (By similarity). Interacts with KBTBD6 and KBTBD7; the interaction is direct (By similarity). Interacts with reticulophagy regulators RETREG1, RETREG2 and RETREG3 (PubMed:34338405). Interacts with Irgm1 (By similarity). Interacts with DNM2 (By similarity). Interacts with NCOA4 (PubMed:26776506).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Cytoplasmic vesicle</location>
      <location evidence="5">Autophagosome</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Endoplasmic reticulum membrane</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Golgi apparatus</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Ubiquitous. A high level expression is seen in brain, thymus, lung, heart, liver and kidney.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2 5 8">The precursor molecule is cleaved by ATG4 (ATG4A, ATG4B, ATG4C or ATG4D) to expose the glycine at the C-terminus and form the cytosolic form, GABARAPL2-I (PubMed:14530254). The processed form is then activated by APG7L/ATG7, transferred to ATG3 and conjugated to phosphatidylethanolamine (PE) phospholipid to form the membrane-bound form, GABARAPL2-II (By similarity). During non-canonical autophagy, the processed form is conjugated to phosphatidylserine (PS) phospholipid (By similarity). ATG4 proteins also mediate the delipidation of PE-conjugated forms required for GABARAPL2 recycling when autophagosomes fuse with lysosomes (PubMed:33795848). In addition, ATG4B and ATG4D mediate delipidation of ATG8 proteins conjugated to PS during non-canonical autophagy (By similarity). ATG4B constitutes the major protein for proteolytic activation (By similarity). ATG4D is the main enzyme for delipidation activity (PubMed:33795848).</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">Phosphorylation at Ser-87 and Ser-88 by TBK1 prevents interaction with ATG4 (ATG4A, ATG4B, ATG4C or ATG4D). Phosphorylation by TBK1 on autophagosomes prevents their delipidation by ATG4 and premature removal from nascent autophagosomes.</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the ATG8 family.</text>
  </comment>
  <dbReference type="EMBL" id="AF190644">
    <property type="protein sequence ID" value="AAK16238.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK002596">
    <property type="protein sequence ID" value="BAB22217.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK049819">
    <property type="protein sequence ID" value="BAC33933.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC026798">
    <property type="protein sequence ID" value="AAH26798.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC081436">
    <property type="protein sequence ID" value="AAH81436.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS40480.1"/>
  <dbReference type="RefSeq" id="NP_080969.2">
    <property type="nucleotide sequence ID" value="NM_026693.5"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P60521"/>
  <dbReference type="SMR" id="P60521"/>
  <dbReference type="BioGRID" id="220282">
    <property type="interactions" value="24"/>
  </dbReference>
  <dbReference type="ELM" id="P60521"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000034428"/>
  <dbReference type="iPTMnet" id="P60521"/>
  <dbReference type="PhosphoSitePlus" id="P60521"/>
  <dbReference type="SwissPalm" id="P60521"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000034428"/>
  <dbReference type="PeptideAtlas" id="P60521"/>
  <dbReference type="ProteomicsDB" id="268853"/>
  <dbReference type="Pumba" id="P60521"/>
  <dbReference type="Antibodypedia" id="30328">
    <property type="antibodies" value="558 antibodies from 34 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="93739"/>
  <dbReference type="Ensembl" id="ENSMUST00000034428.8">
    <property type="protein sequence ID" value="ENSMUSP00000034428.8"/>
    <property type="gene ID" value="ENSMUSG00000031950.8"/>
  </dbReference>
  <dbReference type="GeneID" id="93739"/>
  <dbReference type="KEGG" id="mmu:93739"/>
  <dbReference type="UCSC" id="uc009nnb.2">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:1890602"/>
  <dbReference type="CTD" id="11345"/>
  <dbReference type="MGI" id="MGI:1890602">
    <property type="gene designation" value="Gabarapl2"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000031950"/>
  <dbReference type="eggNOG" id="KOG1654">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000155010"/>
  <dbReference type="HOGENOM" id="CLU_119276_0_1_1"/>
  <dbReference type="InParanoid" id="P60521"/>
  <dbReference type="OMA" id="AKMKWMF"/>
  <dbReference type="OrthoDB" id="652940at2759"/>
  <dbReference type="PhylomeDB" id="P60521"/>
  <dbReference type="TreeFam" id="TF312964"/>
  <dbReference type="Reactome" id="R-MMU-1632852">
    <property type="pathway name" value="Macroautophagy"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-8854214">
    <property type="pathway name" value="TBC/RABGAPs"/>
  </dbReference>
  <dbReference type="BioGRID-ORCS" id="93739">
    <property type="hits" value="9 hits in 76 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="Gabarapl2">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P60521"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 8"/>
  </dbReference>
  <dbReference type="RNAct" id="P60521">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000031950">
    <property type="expression patterns" value="Expressed in embryonic cell in blastocyst and 64 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005776">
    <property type="term" value="C:autophagosome"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000421">
    <property type="term" value="C:autophagosome membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031410">
    <property type="term" value="C:cytoplasmic vesicle"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005789">
    <property type="term" value="C:endoplasmic reticulum membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005794">
    <property type="term" value="C:Golgi apparatus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000139">
    <property type="term" value="C:Golgi membrane"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008429">
    <property type="term" value="F:phosphatidylethanolamine binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031625">
    <property type="term" value="F:ubiquitin protein ligase binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006914">
    <property type="term" value="P:autophagy"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006891">
    <property type="term" value="P:intra-Golgi vesicle-mediated transport"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:1901799">
    <property type="term" value="P:negative regulation of proteasomal protein catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070972">
    <property type="term" value="P:protein localization to endoplasmic reticulum"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015031">
    <property type="term" value="P:protein transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd17163">
    <property type="entry name" value="Ubl_ATG8_GABARAPL2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.20.90:FF:000077">
    <property type="entry name" value="gamma-aminobutyric acid receptor-associated protein-like 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004241">
    <property type="entry name" value="Atg8-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029071">
    <property type="entry name" value="Ubiquitin-like_domsf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10969:SF4">
    <property type="entry name" value="GAMMA-AMINOBUTYRIC ACID RECEPTOR-ASSOCIATED PROTEIN-LIKE 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10969">
    <property type="entry name" value="MICROTUBULE-ASSOCIATED PROTEINS 1A/1B LIGHT CHAIN 3-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02991">
    <property type="entry name" value="ATG8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54236">
    <property type="entry name" value="Ubiquitin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0072">Autophagy</keyword>
  <keyword id="KW-0968">Cytoplasmic vesicle</keyword>
  <keyword id="KW-0256">Endoplasmic reticulum</keyword>
  <keyword id="KW-0333">Golgi apparatus</keyword>
  <keyword id="KW-0449">Lipoprotein</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-0653">Protein transport</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000212374" description="Gamma-aminobutyric acid receptor-associated protein-like 2">
    <location>
      <begin position="1"/>
      <end position="116"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000423071" description="Removed in mature form" evidence="5">
    <location>
      <position position="117"/>
    </location>
  </feature>
  <feature type="site" description="Cleavage; by ATG4B" evidence="5">
    <location>
      <begin position="116"/>
      <end position="117"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="2">
    <location>
      <position position="24"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="2">
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="2">
    <location>
      <position position="87"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="2">
    <location>
      <position position="88"/>
    </location>
  </feature>
  <feature type="lipid moiety-binding region" description="Phosphatidylethanolamine amidated glycine; alternate" evidence="2">
    <location>
      <position position="116"/>
    </location>
  </feature>
  <feature type="lipid moiety-binding region" description="Phosphatidylserine amidated glycine; alternate" evidence="2">
    <location>
      <position position="116"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; BAB22217." evidence="10" ref="2">
    <original>T</original>
    <variation>A</variation>
    <location>
      <position position="83"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P60519"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P60520"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="11414770"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="11890701"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="14530254"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="21383079"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="26776506"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="33795848"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="34338405"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000312" key="11">
    <source>
      <dbReference type="MGI" id="MGI:1890602"/>
    </source>
  </evidence>
  <sequence length="117" mass="13667" checksum="17ACB540FD5B975B" modified="2004-03-01" version="1" precursor="true">MKWMFKEDHSLEHRCVESAKIRAKYPDRVPVIVEKVSGSQIVDIDKRKYLVPSDITVAQFMWIIRKRIQLPSEKAIFLFVDKTVPQSSLTMGQLYEKEKDEDGFLYVAYSGENTFGF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>