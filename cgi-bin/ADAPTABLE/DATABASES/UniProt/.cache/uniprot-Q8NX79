<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-12-20" modified="2024-11-27" version="93" xmlns="http://uniprot.org/uniprot">
  <accession>Q8NX79</accession>
  <name>RPOY_STAAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">DNA-directed RNA polymerase subunit epsilon</fullName>
      <shortName evidence="1">RNAP epsilon subunit</shortName>
      <ecNumber evidence="1">2.7.7.6</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">RNA polymerase epsilon subunit</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Transcriptase subunit epsilon</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rpoY</name>
    <name type="ordered locus">MW0973</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">A non-essential component of RNA polymerase (RNAP).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>RNA(n) + a ribonucleoside 5'-triphosphate = RNA(n+1) + diphosphate</text>
      <dbReference type="Rhea" id="RHEA:21248"/>
      <dbReference type="Rhea" id="RHEA-COMP:14527"/>
      <dbReference type="Rhea" id="RHEA-COMP:17342"/>
      <dbReference type="ChEBI" id="CHEBI:33019"/>
      <dbReference type="ChEBI" id="CHEBI:61557"/>
      <dbReference type="ChEBI" id="CHEBI:140395"/>
      <dbReference type="EC" id="2.7.7.6"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">RNAP is composed of a core of 2 alpha, a beta and a beta' subunit. The core is associated with a delta subunit, and at least one of epsilon or omega. When a sigma factor is associated with the core the holoenzyme is formed, which can initiate transcription.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the RNA polymerase subunit epsilon family.</text>
  </comment>
  <dbReference type="EC" id="2.7.7.6" evidence="1"/>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB94838.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000257888.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8NX79"/>
  <dbReference type="SMR" id="Q8NX79"/>
  <dbReference type="GeneID" id="66839285"/>
  <dbReference type="KEGG" id="sam:MW0973"/>
  <dbReference type="HOGENOM" id="CLU_187518_1_0_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000428">
    <property type="term" value="C:DNA-directed RNA polymerase complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003899">
    <property type="term" value="F:DNA-directed 5'-3' RNA polymerase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006351">
    <property type="term" value="P:DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.20.730">
    <property type="entry name" value="RNAP, epsilon subunit-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01553">
    <property type="entry name" value="RNApol_bact_RpoY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009907">
    <property type="entry name" value="RpoY"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07288">
    <property type="entry name" value="RpoY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0240">DNA-directed RNA polymerase</keyword>
  <keyword id="KW-0548">Nucleotidyltransferase</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0808">Transferase</keyword>
  <feature type="chain" id="PRO_0000163137" description="DNA-directed RNA polymerase subunit epsilon">
    <location>
      <begin position="1"/>
      <end position="72"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01553"/>
    </source>
  </evidence>
  <sequence length="72" mass="8752" checksum="CF72E1A6D2BAAB56" modified="2002-10-01" version="1">MAVFKVFYQHNRDEVIVRENTQSLYVEAQTEEQVRRYLKDRNFNIEFITKLEGAHLDYEKENSEHFNVEIAK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>