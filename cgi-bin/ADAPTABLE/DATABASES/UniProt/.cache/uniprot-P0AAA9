<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-09-13" modified="2024-11-27" version="112" xmlns="http://uniprot.org/uniprot">
  <accession>P0AAA9</accession>
  <accession>P32682</accession>
  <accession>Q2M8T9</accession>
  <accession>Q8X6X2</accession>
  <name>ZRAP_ECOLI</name>
  <protein>
    <recommendedName>
      <fullName evidence="10">Signaling pathway modulator ZraP</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="9">Zinc resistance-associated protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="8">Zra periplasmic repressor partner</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="10">Zra system accessory protein ZraP</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="9" type="primary">zraP</name>
    <name type="synonym">yjaI</name>
    <name type="synonym">zra</name>
    <name type="ordered locus">b4002</name>
    <name type="ordered locus">JW5546</name>
  </gene>
  <organism>
    <name type="scientific">Escherichia coli (strain K12)</name>
    <dbReference type="NCBI Taxonomy" id="83333"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Escherichia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Nucleic Acids Res." volume="21" first="5408" last="5417">
      <title>Analysis of the Escherichia coli genome. IV. DNA sequence of the region from 89.2 to 92.8 minutes.</title>
      <authorList>
        <person name="Blattner F.R."/>
        <person name="Burland V.D."/>
        <person name="Plunkett G. III"/>
        <person name="Sofia H.J."/>
        <person name="Daniels D.L."/>
      </authorList>
      <dbReference type="PubMed" id="8265357"/>
      <dbReference type="DOI" id="10.1093/nar/21.23.5408"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>K12 / MG1655 / ATCC 47076</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="Science" volume="277" first="1453" last="1462">
      <title>The complete genome sequence of Escherichia coli K-12.</title>
      <authorList>
        <person name="Blattner F.R."/>
        <person name="Plunkett G. III"/>
        <person name="Bloch C.A."/>
        <person name="Perna N.T."/>
        <person name="Burland V."/>
        <person name="Riley M."/>
        <person name="Collado-Vides J."/>
        <person name="Glasner J.D."/>
        <person name="Rode C.K."/>
        <person name="Mayhew G.F."/>
        <person name="Gregor J."/>
        <person name="Davis N.W."/>
        <person name="Kirkpatrick H.A."/>
        <person name="Goeden M.A."/>
        <person name="Rose D.J."/>
        <person name="Mau B."/>
        <person name="Shao Y."/>
      </authorList>
      <dbReference type="PubMed" id="9278503"/>
      <dbReference type="DOI" id="10.1126/science.277.5331.1453"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>K12 / MG1655 / ATCC 47076</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2006" name="Mol. Syst. Biol." volume="2" first="E1" last="E5">
      <title>Highly accurate genome sequences of Escherichia coli K-12 strains MG1655 and W3110.</title>
      <authorList>
        <person name="Hayashi K."/>
        <person name="Morooka N."/>
        <person name="Yamamoto Y."/>
        <person name="Fujita K."/>
        <person name="Isono K."/>
        <person name="Choi S."/>
        <person name="Ohtsubo E."/>
        <person name="Baba T."/>
        <person name="Wanner B.L."/>
        <person name="Mori H."/>
        <person name="Horiuchi T."/>
      </authorList>
      <dbReference type="PubMed" id="16738553"/>
      <dbReference type="DOI" id="10.1038/msb4100049"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>K12 / W3110 / ATCC 27325 / DSM 5911</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1998" name="J. Biol. Chem." volume="273" first="21393" last="21401">
      <title>Identification of a novel transcription regulator from Proteus mirabilis, PMTR, revealed a possible role of YJAI protein in balancing zinc in Escherichia coli.</title>
      <authorList>
        <person name="Noll M."/>
        <person name="Petrukhin K."/>
        <person name="Lutsenko S."/>
      </authorList>
      <dbReference type="PubMed" id="9694902"/>
      <dbReference type="DOI" id="10.1074/jbc.273.33.21393"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 27-41</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION</scope>
    <scope>DOMAIN</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2001" name="J. Mol. Biol." volume="307" first="93" last="105">
      <title>The hydH/G genes from Escherichia coli code for a zinc and lead responsive two-component regulatory system.</title>
      <authorList>
        <person name="Leonhartsberger S."/>
        <person name="Huber A."/>
        <person name="Lottspeich F."/>
        <person name="Boeck A."/>
      </authorList>
      <dbReference type="PubMed" id="11243806"/>
      <dbReference type="DOI" id="10.1006/jmbi.2000.4451"/>
    </citation>
    <scope>TRANSCRIPTIONAL REGULATION</scope>
    <source>
      <strain>K12 / MC4100 / ATCC 35695 / DSM 6574</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2011" name="Metallomics" volume="3" first="1324" last="1330">
      <title>Exploring the microbial metalloproteome using MIRAGE.</title>
      <authorList>
        <person name="Sevcenco A.M."/>
        <person name="Pinkse M.W."/>
        <person name="Wolterbeek H.T."/>
        <person name="Verhaert P.D."/>
        <person name="Hagen W.R."/>
        <person name="Hagedoorn P.L."/>
      </authorList>
      <dbReference type="PubMed" id="22094925"/>
      <dbReference type="DOI" id="10.1039/c1mt00154j"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2015" name="Biochem. J." volume="472" first="205" last="216">
      <title>Biophysical and physiological characterization of ZraP from Escherichia coli, the periplasmic accessory protein of the atypical ZraSR two-component system.</title>
      <authorList>
        <person name="Petit-Haertlein I."/>
        <person name="Rome K."/>
        <person name="de Rosny E."/>
        <person name="Molton F."/>
        <person name="Duboc C."/>
        <person name="Gueguen E."/>
        <person name="Rodrigue A."/>
        <person name="Coves J."/>
      </authorList>
      <dbReference type="PubMed" id="26438879"/>
      <dbReference type="DOI" id="10.1042/bj20150827"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>SUBUNIT</scope>
    <scope>INDUCTION</scope>
    <scope>DOMAIN</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>MUTAGENESIS OF 27-HIS--HIS-30; HIS-27; HIS-30; CYS-130 AND 135-HIS--TRP-141</scope>
    <source>
      <strain>K12</strain>
    </source>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2018" name="J. Mol. Biol." volume="430" first="4971" last="4985">
      <title>The Two-Component System ZraPSR Is a Novel ESR that Contributes to Intrinsic Antibiotic Tolerance in Escherichia coli.</title>
      <authorList>
        <person name="Rome K."/>
        <person name="Borde C."/>
        <person name="Taher R."/>
        <person name="Cayron J."/>
        <person name="Lesterlin C."/>
        <person name="Gueguen E."/>
        <person name="De Rosny E."/>
        <person name="Rodrigue A."/>
      </authorList>
      <dbReference type="PubMed" id="30389436"/>
      <dbReference type="DOI" id="10.1016/j.jmb.2018.10.021"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>K12</strain>
    </source>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2019" name="J. Inorg. Biochem." volume="192" first="98" last="106">
      <title>ZraP, the most prominent zinc protein under zinc stress conditions has no direct role in in-vivo zinc tolerance in Escherichia coli.</title>
      <authorList>
        <person name="van der Weel L."/>
        <person name="As K.S."/>
        <person name="Dekker W.J.C."/>
        <person name="van den Eijnden L."/>
        <person name="van Helmond W."/>
        <person name="Schiphorst C."/>
        <person name="Hagen W.R."/>
        <person name="Hagedoorn P.L."/>
      </authorList>
      <dbReference type="PubMed" id="30616070"/>
      <dbReference type="DOI" id="10.1016/j.jinorgbio.2018.12.013"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <scope>MUTAGENESIS OF CYS-130 AND HIS-135</scope>
    <source>
      <strain>TOP10</strain>
    </source>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2021" name="Biochim. Biophys. Acta" volume="1865" first="129810" last="129810">
      <title>A structure-function study of ZraP and ZraS provides new insights into the two-component system Zra.</title>
      <authorList>
        <person name="Taher R."/>
        <person name="de Rosny E."/>
      </authorList>
      <dbReference type="PubMed" id="33309686"/>
      <dbReference type="DOI" id="10.1016/j.bbagen.2020.129810"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 4 5 6 7">Part of the Zra signaling pathway, an envelope stress response (ESR) system composed of the periplasmic accessory protein ZraP, the histidine kinase ZraS and the transcriptional regulator ZraR (PubMed:26438879, PubMed:30389436, PubMed:33309686). The ZraPSR system contributes to antibiotic resistance and is important for membrane integrity in the presence of membrane-targeting biocides (PubMed:30389436). ZraP acts as a modulator which has both a regulatory and a chaperone function (PubMed:26438879, PubMed:30389436). The zinc-bound form of ZraP modulates the response of the ZraPSR system by inhibiting the expression of the zra genes, probably by interacting with ZraS (PubMed:26438879). Participation to the cell protection arises mainly from this repressor function (PubMed:30389436). Also displays chaperone properties in vitro and protects malate dehydrogenase (MDH) from thermal denaturation at 45 degrees Celsius (PubMed:26438879). Binds zinc, copper, nickel, cobalt but not cadmium or manganese (PubMed:22094925, PubMed:26438879, PubMed:30616070, PubMed:9694902). In vitro, has a higher affinity for copper than for zinc (PubMed:26438879, PubMed:30616070). Is the main zinc containing protein under zinc stress conditions (PubMed:22094925). However, the system has no direct role in zinc or copper resistance (PubMed:26438879, PubMed:30616070).</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="3">Zinc binding is required for repressor activity and enhances chaperone activity.</text>
  </comment>
  <comment type="subunit">
    <text evidence="3 5 6">Homooctomer (PubMed:26438879, PubMed:30616070). Organized as a tetramer of dimers (PubMed:26438879). Dimers are formed through a disulfide bridge, which stabilizes the oligomeric state (PubMed:26438879, PubMed:30616070). In vivo, the cysteine residue is at least partially reduced in the periplasm, i.e. it does not form a sulfide bridge with the cysteine residue from another monomer (PubMed:33309686). Disulfide bridges might not be essential for the function of ZraP as a repressor of ZraS (PubMed:33309686).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="7">Periplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text evidence="1 3 7">Expression is stimulated by Zn(2+) in a concentration-dependent manner (PubMed:9694902). Expression is up-regulated in response to high periplasmic concentrations of Zn(2+) or Pb(2+) via the ZraS/ZraR two-component regulatory system (PubMed:11243806, PubMed:26438879). Transcription is also dependent on the RNA polymerase sigma-54 factor (PubMed:11243806).</text>
  </comment>
  <comment type="domain">
    <text evidence="3 11">The N-terminal region contains the metal-binding site and the C-terminal region is involved in oligomerization (PubMed:26438879). Contains two zinc-binding motifs (Probable). Binds four zinc or copper equivalents per octomer with high affinity and without change in quaternary structure (PubMed:26438879).</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="3 4 5">Deletion of the gene confers increased susceptibility to five classes of antibiotics and to some environmental stresses targeting the envelope (PubMed:30389436). In the absence of stress, the mutants show no default in membrane integrity or in cell morphology as compared to the wild-type cells (PubMed:30389436). In the presence of drugs that target the envelope, the mutants show enhanced membrane disruption and abnormal cell shape (PubMed:30389436). Deletion does not affect the minimum inhibitory concentration (MIC) of E.coli for zinc or copper (PubMed:26438879). In strain TOP10, disruption mutant exhibits a higher tolerance to zinc concentrations above 0.5 mM (PubMed:30616070).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="7">Expression of the P.mirabilis transcriptional regulator PMTR in E.coli results in increased production of ZraP and increased cell resistance to zinc (PubMed:9694902). PMTR has no effect on resistance to nickel, cobalt, copper, manganese or iron (PubMed:9694902).</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the ZraP family.</text>
  </comment>
  <comment type="sequence caution" evidence="10">
    <conflict type="erroneous initiation">
      <sequence resource="EMBL-CDS" id="AAC43100" version="1"/>
    </conflict>
    <text>Extended N-terminus.</text>
  </comment>
  <dbReference type="EMBL" id="U00006">
    <property type="protein sequence ID" value="AAC43100.1"/>
    <property type="status" value="ALT_INIT"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U00096">
    <property type="protein sequence ID" value="AAC76976.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AP009048">
    <property type="protein sequence ID" value="BAE77317.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="E65207">
    <property type="entry name" value="E65207"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_418430.2">
    <property type="nucleotide sequence ID" value="NC_000913.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000828222.1">
    <property type="nucleotide sequence ID" value="NZ_STEB01000045.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0AAA9"/>
  <dbReference type="SMR" id="P0AAA9"/>
  <dbReference type="BioGRID" id="4259525">
    <property type="interactions" value="8"/>
  </dbReference>
  <dbReference type="STRING" id="511145.b4002"/>
  <dbReference type="PaxDb" id="511145-b4002"/>
  <dbReference type="EnsemblBacteria" id="AAC76976">
    <property type="protein sequence ID" value="AAC76976"/>
    <property type="gene ID" value="b4002"/>
  </dbReference>
  <dbReference type="GeneID" id="75205520"/>
  <dbReference type="GeneID" id="948507"/>
  <dbReference type="KEGG" id="ecj:JW5546"/>
  <dbReference type="KEGG" id="eco:b4002"/>
  <dbReference type="KEGG" id="ecoc:C3026_21615"/>
  <dbReference type="PATRIC" id="fig|1411691.4.peg.2709"/>
  <dbReference type="EchoBASE" id="EB1862"/>
  <dbReference type="eggNOG" id="COG3678">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_124884_0_0_6"/>
  <dbReference type="InParanoid" id="P0AAA9"/>
  <dbReference type="OMA" id="GMGYGDC"/>
  <dbReference type="OrthoDB" id="6572911at2"/>
  <dbReference type="PhylomeDB" id="P0AAA9"/>
  <dbReference type="BioCyc" id="EcoCyc:EG11918-MONOMER"/>
  <dbReference type="PRO" id="PR:P0AAA9"/>
  <dbReference type="Proteomes" id="UP000000318">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000000625">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030288">
    <property type="term" value="C:outer membrane-bounded periplasmic space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="EcoCyc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050897">
    <property type="term" value="F:cobalt ion binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="EcoCyc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005507">
    <property type="term" value="F:copper ion binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="EcoCyc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="EcoCyc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016151">
    <property type="term" value="F:nickel cation binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="EcoCyc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008270">
    <property type="term" value="F:zinc ion binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="EcoCyc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0036460">
    <property type="term" value="P:cellular response to cell envelope stress"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="EcoCyc"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.120.1490:FF:000003">
    <property type="entry name" value="Zinc resistance-associated protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.120.1490">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025961">
    <property type="entry name" value="Metal_resist"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13801">
    <property type="entry name" value="Metal_resist"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0574">Periplasm</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0346">Stress response</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <feature type="signal peptide" evidence="7">
    <location>
      <begin position="1"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000022720" description="Signaling pathway modulator ZraP">
    <location>
      <begin position="27"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain" evidence="3">
    <location>
      <position position="130"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Forms homooctomers. Abolishes the binding of copper and strongly decreases the number of zinc ions. Loses repressor activity." evidence="3">
    <location>
      <begin position="27"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Forms homooctomers. Strongly decreases the number of copper or zinc ions. Loses repressor activity." evidence="3">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="27"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Forms homooctomers. Does not change the metal-binding properties." evidence="3">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="30"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Destabilizes the octomeric organization. Probably forms monomers. Can still act as a repressor." evidence="3 5">
    <original>C</original>
    <variation>A</variation>
    <location>
      <position position="130"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Destabilizes the octomeric organization. Probably forms monomers." evidence="5">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="130"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Forms dimers. Does not change significantly the number of bound metal ions. Does not affect chaperone activity but abolishes repressor activity." evidence="3">
    <location>
      <begin position="135"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Does not affect the zinc binding affinity or the oligomeric state of the protein." evidence="5">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="135"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11243806"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="22094925"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="26438879"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="30389436"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="30616070"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="33309686"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="9694902"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="33309686"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="9694902"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="9694902"/>
    </source>
  </evidence>
  <sequence length="141" mass="15199" checksum="392A8B29CEBECC1B" modified="2005-09-13" version="1" precursor="true">MKRNTKIALVMMALSAMAMGSTSAFAHGGHGMWQQNAAPLTSEQQTAWQKIHNDFYAQSSALQQQLVTKRYEYNALLAANPPDSSKINAVAKEMENLRQSLDELRVKRDIAMAEAGIPRGAGMGMGYGGCGGGGHMGMGHW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>