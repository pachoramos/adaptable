<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-12-04" modified="2024-11-27" version="83" xmlns="http://uniprot.org/uniprot">
  <accession>Q32GM0</accession>
  <name>STXB_SHIDS</name>
  <protein>
    <recommendedName>
      <fullName>Shiga toxin subunit B</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">stxB</name>
    <name type="ordered locus">SDY_1390</name>
  </gene>
  <organism>
    <name type="scientific">Shigella dysenteriae serotype 1 (strain Sd197)</name>
    <dbReference type="NCBI Taxonomy" id="300267"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Shigella</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Nucleic Acids Res." volume="33" first="6445" last="6458">
      <title>Genome dynamics and diversity of Shigella species, the etiologic agents of bacillary dysentery.</title>
      <authorList>
        <person name="Yang F."/>
        <person name="Yang J."/>
        <person name="Zhang X."/>
        <person name="Chen L."/>
        <person name="Jiang Y."/>
        <person name="Yan Y."/>
        <person name="Tang X."/>
        <person name="Wang J."/>
        <person name="Xiong Z."/>
        <person name="Dong J."/>
        <person name="Xue Y."/>
        <person name="Zhu Y."/>
        <person name="Xu X."/>
        <person name="Sun L."/>
        <person name="Chen S."/>
        <person name="Nie H."/>
        <person name="Peng J."/>
        <person name="Xu J."/>
        <person name="Wang Y."/>
        <person name="Yuan Z."/>
        <person name="Wen Y."/>
        <person name="Yao Z."/>
        <person name="Shen Y."/>
        <person name="Qiang B."/>
        <person name="Hou Y."/>
        <person name="Yu J."/>
        <person name="Jin Q."/>
      </authorList>
      <dbReference type="PubMed" id="16275786"/>
      <dbReference type="DOI" id="10.1093/nar/gki954"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Sd197</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">The B subunit is responsible for the binding of the holotoxin to specific receptors on the target cell surface, such as globotriaosylceramide (Gb3) in human intestinal microvilli.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Shiga toxin contains a single subunit A and five copies of subunit B.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">There are three Gb3-binding sites in each subunit B monomer, allowing for a tighter binding to the target cell. Binding sites 1 and 2 have higher binding affinities than site 3 (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the StxB family.</text>
  </comment>
  <dbReference type="EMBL" id="CP000034">
    <property type="protein sequence ID" value="ABB61535.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000752026.1">
    <property type="nucleotide sequence ID" value="NC_007606.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="YP_403026.1">
    <property type="nucleotide sequence ID" value="NC_007606.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q32GM0"/>
  <dbReference type="SMR" id="Q32GM0"/>
  <dbReference type="STRING" id="300267.SDY_1390"/>
  <dbReference type="EnsemblBacteria" id="ABB61535">
    <property type="protein sequence ID" value="ABB61535"/>
    <property type="gene ID" value="SDY_1390"/>
  </dbReference>
  <dbReference type="KEGG" id="sdy:SDY_1390"/>
  <dbReference type="PATRIC" id="fig|300267.13.peg.1651"/>
  <dbReference type="HOGENOM" id="CLU_191376_0_0_6"/>
  <dbReference type="Proteomes" id="UP000002716">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044177">
    <property type="term" value="C:host cell Golgi apparatus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019836">
    <property type="term" value="P:hemolysis by symbiont of host erythrocytes"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.70:FF:000001">
    <property type="entry name" value="Shiga toxin 1 subunit B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.70">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008992">
    <property type="entry name" value="Enterotoxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003189">
    <property type="entry name" value="SLT_beta"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02258">
    <property type="entry name" value="SLT_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50203">
    <property type="entry name" value="Bacterial enterotoxins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000312306" description="Shiga toxin subunit B">
    <location>
      <begin position="21"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="77"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="89" mass="9743" checksum="C78F7795CCD7242E" modified="2005-12-06" version="1" precursor="true">MKKTLLIAASLSFFSASALATPDCVTGKVEYTKYNDDDTFTVKVGDKELFTNRWNLQSLLLSAQITGMTVTIKTNACHNGGGFSEVIFR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>