<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-04-01" modified="2024-11-27" version="201" xmlns="http://uniprot.org/uniprot">
  <accession>P07492</accession>
  <accession>P07491</accession>
  <accession>P81553</accession>
  <accession>Q14454</accession>
  <accession>Q53YA0</accession>
  <accession>Q9BSY7</accession>
  <name>GRP_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Gastrin-releasing peptide</fullName>
      <shortName>GRP</shortName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Neuromedin-C</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>GRP-10</fullName>
      </alternativeName>
      <alternativeName>
        <fullName evidence="5">GRP18-27</fullName>
      </alternativeName>
    </component>
  </protein>
  <gene>
    <name type="primary">GRP</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1986" name="Proc. Natl. Acad. Sci. U.S.A." volume="83" first="19" last="23">
      <title>Two prohormones for gastrin-releasing peptide are encoded by two mRNAs differing by 19 nucleotides.</title>
      <authorList>
        <person name="Spindel E.R."/>
        <person name="Zilberberg M.D."/>
        <person name="Habener J.F."/>
        <person name="Chin W.W."/>
      </authorList>
      <dbReference type="PubMed" id="3001723"/>
      <dbReference type="DOI" id="10.1073/pnas.83.1.19"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 1)</scope>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] OF 70-148 (ISOFORM 2)</scope>
    <scope>VARIANT SER-4</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1988" name="Mol. Cell. Biol." volume="8" first="3129" last="3135">
      <title>Posttranslational processing of endogenous and of baculovirus-expressed human gastrin-releasing peptide precursor.</title>
      <authorList>
        <person name="Lebacq-Verheyden A.-M."/>
        <person name="Kasprzyk P.G."/>
        <person name="Raum M.G."/>
        <person name="van Wyke Coelingh K."/>
        <person name="Lebacq J.A."/>
        <person name="Battey J.F."/>
      </authorList>
      <dbReference type="PubMed" id="3211139"/>
      <dbReference type="DOI" id="10.1128/mcb.8.8.3129-3135.1988"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>AMIDATION AT MET-50</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1986" name="J. Biol. Chem." volume="261" first="2451" last="2457">
      <title>Expression of the gastrin-releasing peptide gene in human small cell lung cancer. Evidence for alternative processing resulting in three distinct mRNAs.</title>
      <authorList>
        <person name="Sausville E.A."/>
        <person name="Lebacq-Verheyden A.-M."/>
        <person name="Spindel E.R."/>
        <person name="Cuttitta F."/>
        <person name="Gazdar A.F."/>
        <person name="Battey J.F."/>
      </authorList>
      <dbReference type="PubMed" id="3003116"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)35956-2"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>ALTERNATIVE SPLICING</scope>
  </reference>
  <reference key="4">
    <citation type="submission" date="2003-05" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning of human full-length CDSs in BD Creator(TM) system donor vector.</title>
      <authorList>
        <person name="Kalnine N."/>
        <person name="Chen X."/>
        <person name="Rolfs A."/>
        <person name="Halleck A."/>
        <person name="Hines L."/>
        <person name="Eisenstein S."/>
        <person name="Koundinya M."/>
        <person name="Raphael J."/>
        <person name="Moreira D."/>
        <person name="Kelley T."/>
        <person name="LaBaer J."/>
        <person name="Lin Y."/>
        <person name="Phelan M."/>
        <person name="Farmer A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM 1)</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2005" name="Nature" volume="437" first="551" last="555">
      <title>DNA sequence and analysis of human chromosome 18.</title>
      <authorList>
        <person name="Nusbaum C."/>
        <person name="Zody M.C."/>
        <person name="Borowsky M.L."/>
        <person name="Kamal M."/>
        <person name="Kodira C.D."/>
        <person name="Taylor T.D."/>
        <person name="Whittaker C.A."/>
        <person name="Chang J.L."/>
        <person name="Cuomo C.A."/>
        <person name="Dewar K."/>
        <person name="FitzGerald M.G."/>
        <person name="Yang X."/>
        <person name="Abouelleil A."/>
        <person name="Allen N.R."/>
        <person name="Anderson S."/>
        <person name="Bloom T."/>
        <person name="Bugalter B."/>
        <person name="Butler J."/>
        <person name="Cook A."/>
        <person name="DeCaprio D."/>
        <person name="Engels R."/>
        <person name="Garber M."/>
        <person name="Gnirke A."/>
        <person name="Hafez N."/>
        <person name="Hall J.L."/>
        <person name="Norman C.H."/>
        <person name="Itoh T."/>
        <person name="Jaffe D.B."/>
        <person name="Kuroki Y."/>
        <person name="Lehoczky J."/>
        <person name="Lui A."/>
        <person name="Macdonald P."/>
        <person name="Mauceli E."/>
        <person name="Mikkelsen T.S."/>
        <person name="Naylor J.W."/>
        <person name="Nicol R."/>
        <person name="Nguyen C."/>
        <person name="Noguchi H."/>
        <person name="O'Leary S.B."/>
        <person name="Piqani B."/>
        <person name="Smith C.L."/>
        <person name="Talamas J.A."/>
        <person name="Topham K."/>
        <person name="Totoki Y."/>
        <person name="Toyoda A."/>
        <person name="Wain H.M."/>
        <person name="Young S.K."/>
        <person name="Zeng Q."/>
        <person name="Zimmer A.R."/>
        <person name="Fujiyama A."/>
        <person name="Hattori M."/>
        <person name="Birren B.W."/>
        <person name="Sakaki Y."/>
        <person name="Lander E.S."/>
      </authorList>
      <dbReference type="PubMed" id="16177791"/>
      <dbReference type="DOI" id="10.1038/nature03983"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="6">
    <citation type="submission" date="2005-07" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Istrail S."/>
        <person name="Sutton G.G."/>
        <person name="Florea L."/>
        <person name="Halpern A.L."/>
        <person name="Mobarry C.M."/>
        <person name="Lippert R."/>
        <person name="Walenz B."/>
        <person name="Shatkay H."/>
        <person name="Dew I."/>
        <person name="Miller J.R."/>
        <person name="Flanigan M.J."/>
        <person name="Edwards N.J."/>
        <person name="Bolanos R."/>
        <person name="Fasulo D."/>
        <person name="Halldorsson B.V."/>
        <person name="Hannenhalli S."/>
        <person name="Turner R."/>
        <person name="Yooseph S."/>
        <person name="Lu F."/>
        <person name="Nusskern D.R."/>
        <person name="Shue B.C."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F."/>
        <person name="Delcher A.L."/>
        <person name="Huson D.H."/>
        <person name="Kravitz S.A."/>
        <person name="Mouchard L."/>
        <person name="Reinert K."/>
        <person name="Remington K.A."/>
        <person name="Clark A.G."/>
        <person name="Waterman M.S."/>
        <person name="Eichler E.E."/>
        <person name="Adams M.D."/>
        <person name="Hunkapiller M.W."/>
        <person name="Myers E.W."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM 1)</scope>
    <source>
      <tissue>Lung</tissue>
    </source>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2015" name="RSC Adv." volume="5" first="83074" last="83088">
      <title>Conformational ensembles of neuromedin C reveal a progressive coil-helix transition within a binding-induced folding mechanism.</title>
      <authorList>
        <person name="Adrover M."/>
        <person name="Sanchis P."/>
        <person name="Vilanova B."/>
        <person name="Pauwels K."/>
        <person name="Martorell G."/>
        <person name="Perez J.J."/>
      </authorList>
      <dbReference type="DOI" id="10.1039/C5RA12753J"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 41-50</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 5">Stimulates the release of gastrin and other gastrointestinal hormones (By similarity). Contributes to the perception of prurient stimuli and to the transmission of itch signals in the spinal cord that promote scratching behavior (By similarity). Contributes primarily to nonhistaminergic itch sensation (By similarity). In one study, shown to act in the amygdala as part of an inhibitory network which inhibits memory specifically related to learned fear (By similarity). In another study, shown to act on vasoactive intestinal peptide (VIP)-expressing cells in the auditory cortex, most likely via extrasynaptic diffusion from local and long-range sources, to mediate disinhibition of glutamatergic cells via VIP cell-specific GRPR signaling which leads to enhanced auditory fear memories (By similarity). Contributes to the regulation of food intake (By similarity). Inhibits voltage-gated sodium channels but enhances voltage-gated potassium channels in hippocampal neurons (By similarity). Induces sighing by acting directly on the pre-Botzinger complex, a cluster of several thousand neurons in the ventrolateral medulla responsible for inspiration during respiratory activity (By similarity).</text>
  </comment>
  <comment type="function">
    <molecule>Neuromedin-C</molecule>
    <text evidence="5">Induces an itch response through activation of receptors present on mast cells, triggering mast cell degranulation.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-12821367">
      <id>P07492</id>
    </interactant>
    <interactant intactId="EBI-750671">
      <id>Q15699</id>
      <label>ALX1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="4">Cytoplasmic vesicle</location>
      <location evidence="4">Secretory vesicle lumen</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="5">Cell projection</location>
      <location evidence="5">Neuron projection</location>
    </subcellularLocation>
    <text evidence="5">In neurons of the retrotrapezoid nucleus/parafacial respiratory group, expressed on neuron projections which project into the pre-Botzinger complex.</text>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>P07492-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>P07492-2</id>
      <name>2</name>
      <sequence type="described" ref="VSP_000549"/>
    </isoform>
    <isoform>
      <id>P07492-3</id>
      <name>3</name>
      <sequence type="described" ref="VSP_000550"/>
    </isoform>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the bombesin/neuromedin-B/ranatensin family.</text>
  </comment>
  <comment type="sequence caution" evidence="10">
    <conflict type="erroneous gene model prediction">
      <sequence resource="EMBL-CDS" id="AAA52612" version="1"/>
    </conflict>
  </comment>
  <comment type="online information" name="Wikipedia">
    <link uri="https://en.wikipedia.org/wiki/Gastrin_releasing_peptide"/>
    <text>Gastrin-releasing peptide entry</text>
  </comment>
  <dbReference type="EMBL" id="K02054">
    <property type="protein sequence ID" value="AAA52613.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M12550">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M12512">
    <property type="protein sequence ID" value="AAA52612.1"/>
    <property type="status" value="ALT_SEQ"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M12511">
    <property type="protein sequence ID" value="AAA52612.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M12512">
    <property type="protein sequence ID" value="AAA52611.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M12511">
    <property type="protein sequence ID" value="AAA52611.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT006803">
    <property type="protein sequence ID" value="AAP35449.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC067859">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH471096">
    <property type="protein sequence ID" value="EAW63091.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC004488">
    <property type="protein sequence ID" value="AAH04488.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS11971.1">
    <molecule id="P07492-1"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS45877.1">
    <molecule id="P07492-3"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS45878.1">
    <molecule id="P07492-2"/>
  </dbReference>
  <dbReference type="PIR" id="A26182">
    <property type="entry name" value="A26182"/>
  </dbReference>
  <dbReference type="PIR" id="B26182">
    <property type="entry name" value="B26182"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001012530.1">
    <molecule id="P07492-3"/>
    <property type="nucleotide sequence ID" value="NM_001012512.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001012531.1">
    <molecule id="P07492-2"/>
    <property type="nucleotide sequence ID" value="NM_001012513.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_002082.2">
    <molecule id="P07492-1"/>
    <property type="nucleotide sequence ID" value="NM_002091.4"/>
  </dbReference>
  <dbReference type="PDB" id="2N0B">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=41-50"/>
  </dbReference>
  <dbReference type="PDB" id="2N0C">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=41-50"/>
  </dbReference>
  <dbReference type="PDB" id="2N0D">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=41-50"/>
  </dbReference>
  <dbReference type="PDB" id="2N0E">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=41-50"/>
  </dbReference>
  <dbReference type="PDB" id="2N0F">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=41-50"/>
  </dbReference>
  <dbReference type="PDB" id="2N0G">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=41-50"/>
  </dbReference>
  <dbReference type="PDB" id="2N0H">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=41-50"/>
  </dbReference>
  <dbReference type="PDB" id="7W3Z">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.00 A"/>
    <property type="chains" value="L=39-50"/>
  </dbReference>
  <dbReference type="PDB" id="8H0Q">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.30 A"/>
    <property type="chains" value="L=42-50"/>
  </dbReference>
  <dbReference type="PDBsum" id="2N0B"/>
  <dbReference type="PDBsum" id="2N0C"/>
  <dbReference type="PDBsum" id="2N0D"/>
  <dbReference type="PDBsum" id="2N0E"/>
  <dbReference type="PDBsum" id="2N0F"/>
  <dbReference type="PDBsum" id="2N0G"/>
  <dbReference type="PDBsum" id="2N0H"/>
  <dbReference type="PDBsum" id="7W3Z"/>
  <dbReference type="PDBsum" id="8H0Q"/>
  <dbReference type="AlphaFoldDB" id="P07492"/>
  <dbReference type="EMDB" id="EMD-34414"/>
  <dbReference type="SMR" id="P07492"/>
  <dbReference type="BioGRID" id="109179">
    <property type="interactions" value="4"/>
  </dbReference>
  <dbReference type="IntAct" id="P07492">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000256857"/>
  <dbReference type="BindingDB" id="P07492"/>
  <dbReference type="GlyGen" id="P07492">
    <property type="glycosylation" value="2 sites, 1 O-linked glycan (2 sites)"/>
  </dbReference>
  <dbReference type="iPTMnet" id="P07492"/>
  <dbReference type="PhosphoSitePlus" id="P07492"/>
  <dbReference type="BioMuta" id="GRP"/>
  <dbReference type="DMDM" id="308153451"/>
  <dbReference type="MassIVE" id="P07492"/>
  <dbReference type="PaxDb" id="9606-ENSP00000256857"/>
  <dbReference type="PeptideAtlas" id="P07492"/>
  <dbReference type="ProteomicsDB" id="52008">
    <molecule id="P07492-1"/>
  </dbReference>
  <dbReference type="ProteomicsDB" id="52009">
    <molecule id="P07492-2"/>
  </dbReference>
  <dbReference type="ProteomicsDB" id="52010">
    <molecule id="P07492-3"/>
  </dbReference>
  <dbReference type="Antibodypedia" id="1534">
    <property type="antibodies" value="308 antibodies from 28 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="2922"/>
  <dbReference type="Ensembl" id="ENST00000256857.7">
    <molecule id="P07492-1"/>
    <property type="protein sequence ID" value="ENSP00000256857.2"/>
    <property type="gene ID" value="ENSG00000134443.10"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000420468.6">
    <molecule id="P07492-3"/>
    <property type="protein sequence ID" value="ENSP00000389696.2"/>
    <property type="gene ID" value="ENSG00000134443.10"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000529320.2">
    <molecule id="P07492-2"/>
    <property type="protein sequence ID" value="ENSP00000434101.1"/>
    <property type="gene ID" value="ENSG00000134443.10"/>
  </dbReference>
  <dbReference type="GeneID" id="2922"/>
  <dbReference type="KEGG" id="hsa:2922"/>
  <dbReference type="MANE-Select" id="ENST00000256857.7">
    <property type="protein sequence ID" value="ENSP00000256857.2"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_002091.5"/>
    <property type="RefSeq protein sequence ID" value="NP_002082.2"/>
  </dbReference>
  <dbReference type="UCSC" id="uc002lhu.4">
    <molecule id="P07492-1"/>
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:4605"/>
  <dbReference type="CTD" id="2922"/>
  <dbReference type="DisGeNET" id="2922"/>
  <dbReference type="GeneCards" id="GRP"/>
  <dbReference type="HGNC" id="HGNC:4605">
    <property type="gene designation" value="GRP"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000134443">
    <property type="expression patterns" value="Tissue enhanced (brain, lung, stomach)"/>
  </dbReference>
  <dbReference type="MIM" id="137260">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P07492"/>
  <dbReference type="OpenTargets" id="ENSG00000134443"/>
  <dbReference type="PharmGKB" id="PA28999"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000134443"/>
  <dbReference type="eggNOG" id="ENOG502S4DG">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000154470"/>
  <dbReference type="HOGENOM" id="CLU_144892_0_0_1"/>
  <dbReference type="InParanoid" id="P07492"/>
  <dbReference type="OMA" id="KDMMDYL"/>
  <dbReference type="OrthoDB" id="4332967at2759"/>
  <dbReference type="PhylomeDB" id="P07492"/>
  <dbReference type="TreeFam" id="TF336391"/>
  <dbReference type="PathwayCommons" id="P07492"/>
  <dbReference type="Reactome" id="R-HSA-375276">
    <property type="pathway name" value="Peptide ligand-binding receptors"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-381771">
    <property type="pathway name" value="Synthesis, secretion, and inactivation of Glucagon-like Peptide-1 (GLP-1)"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-416476">
    <property type="pathway name" value="G alpha (q) signalling events"/>
  </dbReference>
  <dbReference type="SignaLink" id="P07492"/>
  <dbReference type="SIGNOR" id="P07492"/>
  <dbReference type="BioGRID-ORCS" id="2922">
    <property type="hits" value="12 hits in 1151 CRISPR screens"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P07492"/>
  <dbReference type="GeneWiki" id="Gastrin-releasing_peptide"/>
  <dbReference type="GenomeRNAi" id="2922"/>
  <dbReference type="Pharos" id="P07492">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P07492"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 18"/>
  </dbReference>
  <dbReference type="RNAct" id="P07492">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000134443">
    <property type="expression patterns" value="Expressed in male germ line stem cell (sensu Vertebrata) in testis and 123 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P07492">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043005">
    <property type="term" value="C:neuron projection"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0098992">
    <property type="term" value="C:neuronal dense core vesicle"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034774">
    <property type="term" value="C:secretory granule lumen"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005102">
    <property type="term" value="F:signaling receptor binding"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:1904384">
    <property type="term" value="P:cellular response to sodium phosphate"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043303">
    <property type="term" value="P:mast cell degranulation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1903817">
    <property type="term" value="P:negative regulation of voltage-gated potassium channel activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1905151">
    <property type="term" value="P:negative regulation of voltage-gated sodium channel activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001503">
    <property type="term" value="P:ossification"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000987">
    <property type="term" value="P:positive regulation of behavioral fear response"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090277">
    <property type="term" value="P:positive regulation of peptide hormone secretion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1900738">
    <property type="term" value="P:positive regulation of phospholipase C-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1903942">
    <property type="term" value="P:positive regulation of respiratory gaseous exchange"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1905461">
    <property type="term" value="P:positive regulation of vascular associated smooth muscle cell apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0036343">
    <property type="term" value="P:psychomotor behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010468">
    <property type="term" value="P:regulation of gene expression"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043207">
    <property type="term" value="P:response to external biotic stimulus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007165">
    <property type="term" value="P:signal transduction"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035176">
    <property type="term" value="P:social behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000874">
    <property type="entry name" value="Bombesin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16866">
    <property type="entry name" value="GASTRIN-RELEASING PEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16866:SF2">
    <property type="entry name" value="GASTRIN-RELEASING PEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02044">
    <property type="entry name" value="Bombesin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00257">
    <property type="entry name" value="BOMBESIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0966">Cell projection</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0968">Cytoplasmic vesicle</keyword>
  <keyword id="KW-0467">Mast cell degranulation</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003035" description="Gastrin-releasing peptide" evidence="1">
    <location>
      <begin position="24"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003036" description="Neuromedin-C" evidence="4">
    <location>
      <begin position="41"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003037" evidence="1">
    <location>
      <begin position="54"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="6">
    <location>
      <begin position="89"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Polar residues" evidence="6">
    <location>
      <begin position="99"/>
      <end position="117"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="8">
    <location>
      <position position="50"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_000549" description="In isoform 2." evidence="9">
    <original>VGSKGKVGRLSAPGSQREGRNPQLNQQ</original>
    <variation>LVDSLLQVLNVKEGTPS</variation>
    <location>
      <begin position="122"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_000550" description="In isoform 3." evidence="10">
    <location>
      <begin position="128"/>
      <end position="134"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_027834" description="In dbSNP:rs1062557." evidence="7">
    <original>R</original>
    <variation>S</variation>
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="helix" evidence="11">
    <location>
      <begin position="45"/>
      <end position="48"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P08989"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P24393"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P63153"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="4">
    <source>
      <dbReference type="UniProtKB" id="Q863C3"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="5">
    <source>
      <dbReference type="UniProtKB" id="Q8R1I2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="6">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="3001723"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="3211139"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="3001723"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0007829" key="11">
    <source>
      <dbReference type="PDB" id="2N0E"/>
    </source>
  </evidence>
  <sequence length="148" mass="16213" checksum="6A9ED9E2748255BD" modified="2010-10-05" version="2" precursor="true">MRGRELPLVLLALVLCLAPRGRAVPLPAGGGTVLTKMYPRGNHWAVGHLMGKKSTGESSSVSERGSLKQQLREYIRWEEAARNLLGLIEAKENRNHQPPQPKALGNQQPSWDSEDSSNFKDVGSKGKVGRLSAPGSQREGRNPQLNQQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>