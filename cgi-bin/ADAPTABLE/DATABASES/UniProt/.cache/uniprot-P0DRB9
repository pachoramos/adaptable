<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2024-01-24" modified="2024-07-24" version="3" xmlns="http://uniprot.org/uniprot">
  <accession>P0DRB9</accession>
  <name>IPTX2_TELST</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">U-isophellitoxin-Tst2</fullName>
      <shortName evidence="3">Tst2</shortName>
      <shortName evidence="3">U-IPTX-Tst2</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Telmatactis stephensoni</name>
    <name type="common">Sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="2835637"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Isophelliidae</taxon>
      <taxon>Telmatactis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2022" name="Mol. Ecol." volume="31" first="866" last="883">
      <title>Venoms for all occasions: the functional toxin profiles of different anatomical regions in sea anemones are related to their ecological function.</title>
      <authorList>
        <person name="Ashwood L.M."/>
        <person name="Undheim E.A.B."/>
        <person name="Madio B."/>
        <person name="Hamilton B.R."/>
        <person name="Daly M."/>
        <person name="Hurwood D.A."/>
        <person name="King G.F."/>
        <person name="Prentis P.J."/>
      </authorList>
      <dbReference type="PubMed" id="34837433"/>
      <dbReference type="DOI" id="10.1111/mec.16286"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2023" name="Biochim. Biophys. Acta" volume="1872" first="140952" last="140952">
      <title>Structural and functional characterisation of Tst2, a novel TRPV1 inhibitory peptide from the Australian sea anemone Telmatactis stephensoni.</title>
      <authorList>
        <person name="Elnahriry K.A."/>
        <person name="Wai D.C.C."/>
        <person name="Ashwood L.M."/>
        <person name="Naseem M.U."/>
        <person name="Szanto T.G."/>
        <person name="Guo S."/>
        <person name="Panyi G."/>
        <person name="Prentis P.J."/>
        <person name="Norton R.S."/>
      </authorList>
      <dbReference type="PubMed" id="37640250"/>
      <dbReference type="DOI" id="10.1016/j.bbapap.2023.140952"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>RECOMBINANT EXPRESSION</scope>
    <scope>BIOASSAY</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>STRUCTURE BY NMR OF 47-84</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Toxin with activity against pain receptors. The recombinant protein (which contains an additional Gly at the N-terminus) at 100 nM shows &gt;50% inhibition of the transient receptor potential subfamily V member 1 (hTRPV1) and slight (~10%) inhibition of transient receptor potential subfamily A member 1 (hTRPA1). Is inactive against voltage-gated sodium channels and the human voltage-gated proton channel. In vivo, is not toxic to insect, since it does not possess cytotoxic activity when assessed against Drosophila melanogaster flies.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="4">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">No specific localization in a particular morphological structure, with a slightly higher expression in the epidermis structures (column and pedal disk), consistent with this peptide having a role in predator deterrence.</text>
  </comment>
  <comment type="domain">
    <text evidence="2">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="3968.03" method="MALDI" evidence="2"/>
  <comment type="miscellaneous">
    <text evidence="2">Negative results: does not show activity on human Kv1.2/KCNA2, Nav1.3/SCN3A, Nav1.4/SCN4A, Nav1.5/SCN5A, Nav1.7/SCN9A, and the human voltage-gated proton channel HVCN1.</text>
  </comment>
  <dbReference type="PDB" id="8SEM">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=47-84"/>
  </dbReference>
  <dbReference type="PDBsum" id="8SEM"/>
  <dbReference type="SMR" id="P0DRB9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000459525" evidence="4">
    <location>
      <begin position="26"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000459526" description="U-isophellitoxin-Tst2" evidence="2">
    <location>
      <begin position="47"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="47"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="54"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="60"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="57"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="62"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="67"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="71"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="75"/>
      <end position="77"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="37640250"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="37640250"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="34837433"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="37640250"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="8SEM"/>
    </source>
  </evidence>
  <sequence length="84" mass="9459" checksum="53103A54AE2DEAD9" modified="2024-01-24" version="1" precursor="true">MVQMKGFGILCLVFLVVFLMNVAESRRYPEFPMPYNDDYDKPFVKRCKGQDAPCSKSKDCCGEASMCSKGADGKKTCMIDKLWG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>