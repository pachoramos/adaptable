<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-09-13" modified="2023-11-08" version="2" xmlns="http://uniprot.org/uniprot">
  <accession>P0DX30</accession>
  <name>VP4_ORADR</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Venom peptide 4</fullName>
      <shortName evidence="3">OdVP4</shortName>
      <shortName evidence="4">VP4</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Orancistrocerus drewseni</name>
    <name type="common">Solitary wasp</name>
    <dbReference type="NCBI Taxonomy" id="529024"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Eumeninae</taxon>
      <taxon>Orancistrocerus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2011" name="Peptides" volume="32" first="568" last="572">
      <title>Venom peptides from solitary hunting wasps induce feeding disorder in lepidopteran larvae.</title>
      <authorList>
        <person name="Baek J.H."/>
        <person name="Ji Y."/>
        <person name="Shin J.S."/>
        <person name="Lee S."/>
        <person name="Lee S.H."/>
      </authorList>
      <dbReference type="PubMed" id="21184791"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2010.12.007"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>SYNTHESIS</scope>
    <scope>PROBABLE AMIDATION AT LEU-10</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="4">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="5">May have both amphipathic alpha-helical and coil conformation.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">Negative results: has no activity against fungi (B.cinerea and C.albicans) and bacteria (E.coli and S.aureus) (PubMed:21184791). Has no hemolytic activity against human erythrocytes (PubMed:21184791). Does not show cytolytic activity against insect cell lines (PubMed:21184791). Does not induce feeding disorder in lepidopteran larvae after peptide injection in the vicinity of the head and thorax (PubMed:21184791).</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the MCD family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <feature type="peptide" id="PRO_0000458775" description="Venom peptide 4" evidence="2">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="5">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="21184791"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="21184791"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="21184791"/>
    </source>
  </evidence>
  <sequence length="10" mass="1111" checksum="332D49A6C2D2C327" modified="2023-09-13" version="1">LDPKVVQSLL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>