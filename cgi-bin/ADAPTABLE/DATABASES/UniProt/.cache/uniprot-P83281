<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-12-06" modified="2019-12-11" version="36" xmlns="http://uniprot.org/uniprot">
  <accession>P83281</accession>
  <name>FAR8_MACRS</name>
  <protein>
    <recommendedName>
      <fullName>FMRFamide-like neuropeptide FLP8</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>VSHNNFLRF-amide</fullName>
    </alternativeName>
  </protein>
  <organism evidence="2">
    <name type="scientific">Macrobrachium rosenbergii</name>
    <name type="common">Giant fresh water prawn</name>
    <dbReference type="NCBI Taxonomy" id="79674"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Eucarida</taxon>
      <taxon>Decapoda</taxon>
      <taxon>Pleocyemata</taxon>
      <taxon>Caridea</taxon>
      <taxon>Palaemonoidea</taxon>
      <taxon>Palaemonidae</taxon>
      <taxon>Macrobrachium</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2001" name="Peptides" volume="22" first="191" last="197">
      <title>Three more novel FMRFamide-like neuropeptide sequences from the eyestalk of the giant freshwater prawn Macrobrachium rosenbergii.</title>
      <authorList>
        <person name="Sithigorngul P."/>
        <person name="Saraithongkum W."/>
        <person name="Longyant S."/>
        <person name="Panchan N."/>
        <person name="Sithigorngul W."/>
        <person name="Petsom A."/>
      </authorList>
      <dbReference type="PubMed" id="11179812"/>
      <dbReference type="DOI" id="10.1016/s0196-9781(00)00382-x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-9</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Eyestalk</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="1133.8" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the FARP (FMRFamide related peptide) family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043691" description="FMRFamide-like neuropeptide FLP8">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11179812"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="9" mass="1133" checksum="845A0729C44441F5" modified="2002-03-01" version="1">VSHNNFLRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>