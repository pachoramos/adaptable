<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-07-05" modified="2022-05-25" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKP2</accession>
  <name>PGLR4_XENRU</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Peptide PGLa-R4</fullName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Xenopus ruwenzoriensis</name>
    <name type="common">Uganda clawed frog</name>
    <dbReference type="NCBI Taxonomy" id="105430"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Pipoidea</taxon>
      <taxon>Pipidae</taxon>
      <taxon>Xenopodinae</taxon>
      <taxon>Xenopus</taxon>
      <taxon>Xenopus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2016" name="Comp. Biochem. Physiol." volume="19" first="18" last="24">
      <title>Peptidomic analysis of the extensive array of host-defense peptides in skin secretions of the dodecaploid frog Xenopus ruwenzoriensis (Pipidae).</title>
      <authorList>
        <person name="Coquet L."/>
        <person name="Kolodziejek J."/>
        <person name="Jouenne T."/>
        <person name="Nowotny N."/>
        <person name="King J.D."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="27290612"/>
      <dbReference type="DOI" id="10.1016/j.cbd.2016.04.006"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-21</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2026.2" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the gastrin/cholecystokinin family. Magainin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HKP2"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000440929" description="Peptide PGLa-R4" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="2">
    <location>
      <position position="21"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HK87"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27290612"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="27290612"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="27290612"/>
    </source>
  </evidence>
  <sequence length="21" mass="2027" checksum="B52E45274B43C0F6" modified="2017-07-05" version="1">GMATKAGTIVGKIAKVALNAL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>