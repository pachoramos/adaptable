<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-11-27" version="79" xmlns="http://uniprot.org/uniprot">
  <accession>Q27023</accession>
  <name>DEFI_TENMO</name>
  <protein>
    <recommendedName>
      <fullName>Tenecin-1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Tenebrio molitor</name>
    <name type="common">Yellow mealworm beetle</name>
    <dbReference type="NCBI Taxonomy" id="7067"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Coleoptera</taxon>
      <taxon>Polyphaga</taxon>
      <taxon>Cucujiformia</taxon>
      <taxon>Tenebrionidae</taxon>
      <taxon>Tenebrio</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="J. Biochem." volume="116" first="53" last="58">
      <title>Purification and molecular cloning of cDNA for an inducible antibacterial protein from larvae of the coleopteran, Tenebrio molitor.</title>
      <authorList>
        <person name="Moon H.J."/>
        <person name="Lee S.Y."/>
        <person name="Kurata S."/>
        <person name="Natori S."/>
        <person name="Lee B.L."/>
      </authorList>
      <dbReference type="PubMed" id="7798186"/>
      <dbReference type="DOI" id="10.1093/oxfordjournals.jbchem.a124502"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 49-59</scope>
    <scope>DISULFIDE BONDS</scope>
    <source>
      <tissue>Larval hemolymph</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Bactericidal protein produced in response to injury. It is cytotoxic to Gram-positive bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the invertebrate defensin family. Type 1 subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="D17670">
    <property type="protein sequence ID" value="BAA04552.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="JX0332">
    <property type="entry name" value="JX0332"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q27023"/>
  <dbReference type="TCDB" id="1.C.47.1.4">
    <property type="family name" value="the insect/fungal defensin (insect/fungal defensin) family"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006959">
    <property type="term" value="P:humoral immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd21806">
    <property type="entry name" value="DEFL_defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000005">
    <property type="entry name" value="Defensin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001542">
    <property type="entry name" value="Defensin_invertebrate/fungal"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13645">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13645:SF0">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01097">
    <property type="entry name" value="Defensin_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51378">
    <property type="entry name" value="INVERT_DEFENSINS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000006754">
    <location>
      <begin position="20"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000006755" description="Tenecin-1">
    <location>
      <begin position="42"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="44"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="61"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="65"/>
      <end position="83"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00710"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7798186"/>
    </source>
  </evidence>
  <sequence length="84" mass="9176" checksum="0367C5070468BE60" modified="1997-11-01" version="1" precursor="true">MKLTIFALVACFFILQIAAFPLEEAATAEEIEQGEHIRVKRVTCDILSVEAKGVKLNDAACAAHCLFRGRSGGYCNGKRVCVCR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>