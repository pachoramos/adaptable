<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-29" modified="2024-01-24" version="84" xmlns="http://uniprot.org/uniprot">
  <accession>Q6IV20</accession>
  <accession>A0A1W6</accession>
  <accession>Q09MT4</accession>
  <accession>Q9PS49</accession>
  <name>GLL11_CHICK</name>
  <protein>
    <recommendedName>
      <fullName>Gallinacin-11</fullName>
      <shortName>Gal-11</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Beta-defensin 11</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Vitelline membrane outer layer protein 2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Vitelline membrane outer layer protein II</fullName>
      <shortName>VMO-II</shortName>
      <shortName>VMOII</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">GAL11</name>
  </gene>
  <organism>
    <name type="scientific">Gallus gallus</name>
    <name type="common">Chicken</name>
    <dbReference type="NCBI Taxonomy" id="9031"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Archelosauria</taxon>
      <taxon>Archosauria</taxon>
      <taxon>Dinosauria</taxon>
      <taxon>Saurischia</taxon>
      <taxon>Theropoda</taxon>
      <taxon>Coelurosauria</taxon>
      <taxon>Aves</taxon>
      <taxon>Neognathae</taxon>
      <taxon>Galloanserae</taxon>
      <taxon>Galliformes</taxon>
      <taxon>Phasianidae</taxon>
      <taxon>Phasianinae</taxon>
      <taxon>Gallus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="BMC Genomics" volume="5" first="56" last="56">
      <title>A genome-wide screen identifies a single beta-defensin gene cluster in the chicken: implications for the origin and evolution of mammalian defensins.</title>
      <authorList>
        <person name="Xiao Y."/>
        <person name="Hughes A.L."/>
        <person name="Ando J."/>
        <person name="Matsuda Y."/>
        <person name="Cheng J.-F."/>
        <person name="Skinner-Noble D."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="15310403"/>
      <dbReference type="DOI" id="10.1186/1471-2164-5-56"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="2006-07" db="EMBL/GenBank/DDBJ databases">
      <title>Chicken beta-defensin in China chicken breeds.</title>
      <authorList>
        <person name="Chen Y."/>
        <person name="Cao Y."/>
        <person name="Xie Q."/>
        <person name="Bi Y."/>
        <person name="Chen J."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>VARIANT ARG-35</scope>
    <source>
      <strain>Guangxi Huang</strain>
      <strain>Huiyang bearded</strain>
      <strain>Qingyuan Ma</strain>
      <strain>Taihe silkies</strain>
      <strain>Xinghua</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="submission" date="2006-09" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning and structure analysis of the Chongren Ma chicken beta-defensin 11 (GAL11).</title>
      <authorList>
        <person name="Kang X."/>
        <person name="Jiang R."/>
        <person name="Han R."/>
        <person name="Chen Q."/>
        <person name="Tian Y."/>
        <person name="Li G."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1992" name="Biochem. J." volume="286" first="17" last="22">
      <title>Isolation of a novel protein from the outer layer of the vitelline membrane.</title>
      <authorList>
        <person name="Kido S."/>
        <person name="Morimoto A."/>
        <person name="Kim F."/>
        <person name="Doi Y."/>
      </authorList>
      <dbReference type="PubMed" id="1520265"/>
      <dbReference type="DOI" id="10.1042/bj2860017"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 23-42</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Egg yolk</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2007" name="Reproduction" volume="133" first="127" last="133">
      <title>Changes in the expression of gallinacins, antimicrobial peptides, in ovarian follicles during follicular growth and in response to lipopolysaccharide in laying hens (Gallus domesticus).</title>
      <authorList>
        <person name="Subedi K."/>
        <person name="Isobe N."/>
        <person name="Nishibori M."/>
        <person name="Yoshimura Y."/>
      </authorList>
      <dbReference type="PubMed" id="17244739"/>
      <dbReference type="DOI" id="10.1530/rep-06-0083"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has bactericidal activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasmic granule</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2 3 4">Detected in outer membrane of the vitelline layer of the egg (at protein level). Expressed in the liver, gall bladder, kidney, testis, ovary and male and female reproductive tracts. Expressed in the ovarian stroma, but not in the ovarian follicles. No expression is detected in bone marrow.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY621313">
    <property type="protein sequence ID" value="AAT45551.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY621326">
    <property type="protein sequence ID" value="AAT48935.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ677642">
    <property type="protein sequence ID" value="ABG73376.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858308">
    <property type="protein sequence ID" value="ABI48223.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858321">
    <property type="protein sequence ID" value="ABI48237.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858334">
    <property type="protein sequence ID" value="ABI48250.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858348">
    <property type="protein sequence ID" value="ABI48264.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ997047">
    <property type="protein sequence ID" value="ABJ90336.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S23981">
    <property type="entry name" value="S23981"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001001779.1">
    <property type="nucleotide sequence ID" value="NM_001001779.1"/>
  </dbReference>
  <dbReference type="PDB" id="6QES">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=23-62"/>
  </dbReference>
  <dbReference type="PDB" id="6QET">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=63-104"/>
  </dbReference>
  <dbReference type="PDB" id="6QEU">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=23-104"/>
  </dbReference>
  <dbReference type="PDBsum" id="6QES"/>
  <dbReference type="PDBsum" id="6QET"/>
  <dbReference type="PDBsum" id="6QEU"/>
  <dbReference type="AlphaFoldDB" id="Q6IV20"/>
  <dbReference type="SMR" id="Q6IV20"/>
  <dbReference type="STRING" id="9031.ENSGALP00000030908"/>
  <dbReference type="PaxDb" id="9031-ENSGALP00000030908"/>
  <dbReference type="Ensembl" id="ENSGALT00000031544">
    <property type="protein sequence ID" value="ENSGALP00000030908"/>
    <property type="gene ID" value="ENSGALG00000019846"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00010027363.1">
    <property type="protein sequence ID" value="ENSGALP00010015617.1"/>
    <property type="gene ID" value="ENSGALG00010011435.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00015040696">
    <property type="protein sequence ID" value="ENSGALP00015023774"/>
    <property type="gene ID" value="ENSGALG00015016707"/>
  </dbReference>
  <dbReference type="GeneID" id="414876"/>
  <dbReference type="KEGG" id="gga:414876"/>
  <dbReference type="CTD" id="414876"/>
  <dbReference type="VEuPathDB" id="HostDB:geneid_414876"/>
  <dbReference type="eggNOG" id="ENOG502R6J6">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00530000069149"/>
  <dbReference type="HOGENOM" id="CLU_2249188_0_0_1"/>
  <dbReference type="InParanoid" id="Q6IV20"/>
  <dbReference type="OMA" id="CQDEGGH"/>
  <dbReference type="PRO" id="PR:Q6IV20"/>
  <dbReference type="Proteomes" id="UP000000539">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSGALG00000019846">
    <property type="expression patterns" value="Expressed in ovary and 1 other cell type or tissue"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd21907">
    <property type="entry name" value="BDD1_Gal11"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="CDD" id="cd21906">
    <property type="entry name" value="BDD2_Gal11"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388:SF9">
    <property type="entry name" value="BETA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388">
    <property type="entry name" value="BETA-DEFENSIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000288575" description="Gallinacin-11">
    <location>
      <begin position="23"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="30"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In strain: Huiyang bearded." evidence="5">
    <original>G</original>
    <variation>R</variation>
    <location>
      <position position="35"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; ABJ90336." evidence="6" ref="3">
    <original>V</original>
    <variation>A</variation>
    <location>
      <position position="31"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; ABJ90336." evidence="6" ref="3">
    <original>H</original>
    <variation>L</variation>
    <location>
      <position position="34"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; ABJ90336." evidence="6" ref="3">
    <original>P</original>
    <variation>H</variation>
    <location>
      <position position="94"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="27"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="33"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="48"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="turn" evidence="7">
    <location>
      <begin position="55"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="58"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="65"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="turn" evidence="8">
    <location>
      <begin position="72"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="77"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="86"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="94"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="99"/>
      <end position="102"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="1520265"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15310403"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="17244739"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="6QES"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="8">
    <source>
      <dbReference type="PDB" id="6QET"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="9">
    <source>
      <dbReference type="PDB" id="6QEU"/>
    </source>
  </evidence>
  <sequence length="104" mass="11650" checksum="1D3E63B15D37F36A" modified="2004-07-05" version="1" precursor="true">MKLFSCLMALLLFLLQAVPGLGLPRDTSRCVGYHGYCIRSKVCPKPFAAFGTCSWRQKTCCVDTTSDFHTCQDKGGHCVSPKIRCLEEQLGLCPLKRWTCCKEI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>