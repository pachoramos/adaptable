<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-02-28" modified="2023-02-22" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>C0HL07</accession>
  <name>RN2_NIDOK</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Ranatuerin-2OK</fullName>
    </recommendedName>
  </protein>
  <organism evidence="2">
    <name type="scientific">Nidirana okinavana</name>
    <name type="common">Kampira Falls frog</name>
    <name type="synonym">Babina okinavana</name>
    <dbReference type="NCBI Taxonomy" id="156870"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Nidirana</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2005" name="Peptides" volume="26" first="185" last="190">
      <title>A family of acyclic brevinin-1 peptides from the skin of the Ryukyu brown frog Rana okinavana.</title>
      <authorList>
        <person name="Conlon J.M."/>
        <person name="Sonnevend A."/>
        <person name="Jouenne T."/>
        <person name="Coquet L."/>
        <person name="Cosquer D."/>
        <person name="Vaudry H."/>
        <person name="Iwamuro S."/>
      </authorList>
      <dbReference type="PubMed" id="15629529"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2004.08.008"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue evidence="2">Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide. Active against Gram-negative bacterium E.coli (MIC=12.5 uM) and against Gram-positive bacterium S.aureus (MIC=50 uM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3183.7" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the frog skin active peptide (FSAP) family. Ranatuerin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HL07"/>
  <dbReference type="SMR" id="C0HL07"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000443441" description="Ranatuerin-2OK" evidence="1">
    <location>
      <begin position="1"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="23"/>
      <end position="30"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="15629529"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="15629529"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="15629529"/>
    </source>
  </evidence>
  <sequence length="30" mass="3188" checksum="80E7BB37FA458FDE" modified="2018-02-28" version="1">SFLNFFKGAAKNLLAAGLDKLKCKISGTQC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>