<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-09-21" modified="2024-01-24" version="14" xmlns="http://uniprot.org/uniprot">
  <accession>P86941</accession>
  <name>DRS1_AGASP</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Dermaseptin-SP1</fullName>
      <shortName evidence="5">DRS-SP1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Dermaseptin-LI1</fullName>
      <shortName evidence="4">DRS-LI1</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="3">Insulinotropic peptide 1</fullName>
      <shortName evidence="3">FSIP</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Agalychnis spurrelli</name>
    <name type="common">Gliding leaf frog</name>
    <name type="synonym">Agalychnis litodryas</name>
    <dbReference type="NCBI Taxonomy" id="317303"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Agalychnis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Regul. Pept." volume="120" first="33" last="38">
      <title>Isolation and characterisation of an unexpected class of insulinotropic peptides in the skin of the frog Agalychnis litodryas.</title>
      <authorList>
        <person name="Marenah L."/>
        <person name="Shaw C."/>
        <person name="Orr D.F."/>
        <person name="McClean S."/>
        <person name="Flatt P.R."/>
        <person name="Abdel-Wahab Y.H."/>
      </authorList>
      <dbReference type="PubMed" id="15177918"/>
      <dbReference type="DOI" id="10.1016/j.regpep.2004.02.007"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="Peptides" volume="29" first="2074" last="2082">
      <title>A consistent nomenclature of antimicrobial peptides isolated from frogs of the subfamily Phyllomedusinae.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="18644413"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.06.017"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2019" name="Biomolecules" volume="9" first="1" last="20">
      <title>Unravelling the skin secretion peptides of the gliding leaf frog, Agalychnis spurrelli (Hylidae).</title>
      <authorList>
        <person name="Proano-Bolanos C."/>
        <person name="Blasco-Zuniga A."/>
        <person name="Almeida J.R."/>
        <person name="Wang L."/>
        <person name="Llumiquinga M.A."/>
        <person name="Rivera M."/>
        <person name="Zhou M."/>
        <person name="Chen T."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="31671555"/>
      <dbReference type="DOI" id="10.3390/biom9110667"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2 6">Probable antimicrobial peptide which stimulates insulin-release in glucose-responsive BRIN-BD 11 cells.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3020.0" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=0960"/>
  </comment>
  <dbReference type="AlphaFoldDB" id="P86941"/>
  <dbReference type="SMR" id="P86941"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022731">
    <property type="entry name" value="Dermaseptin_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF12121">
    <property type="entry name" value="DD_K"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000412976" description="Dermaseptin-SP1" evidence="2">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15177918"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="15177918"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="31671555"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="28" mass="3020" checksum="683704EFB3A571B6" modified="2011-09-21" version="1">AVWKDFLKNIGKAAGKAVLNSVTDMVNE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>