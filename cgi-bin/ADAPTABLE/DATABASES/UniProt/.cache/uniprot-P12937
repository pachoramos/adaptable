<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1989-10-01" modified="2024-10-02" version="75" xmlns="http://uniprot.org/uniprot">
  <accession>P12937</accession>
  <name>X_WHV2</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein X</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">HBx</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Peptide X</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">pX</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">X</name>
  </gene>
  <organism>
    <name type="scientific">Woodchuck hepatitis B virus (isolate 2)</name>
    <name type="common">WHV</name>
    <dbReference type="NCBI Taxonomy" id="341946"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Pararnavirae</taxon>
      <taxon>Artverviricota</taxon>
      <taxon>Revtraviricetes</taxon>
      <taxon>Blubervirales</taxon>
      <taxon>Hepadnaviridae</taxon>
      <taxon>Orthohepadnavirus</taxon>
      <taxon>Woodchuck hepatitis virus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Marmota monax</name>
    <name type="common">Woodchuck</name>
    <dbReference type="NCBI Taxonomy" id="9995"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1985" name="J. Virol." volume="56" first="978" last="986">
      <title>Nucleotide sequence of a cloned woodchuck hepatitis virus genome: evolutional relationship between hepadnaviruses.</title>
      <authorList>
        <person name="Kodama K."/>
        <person name="Ogasawara N."/>
        <person name="Yoshikawa H."/>
        <person name="Murakami S."/>
      </authorList>
      <dbReference type="PubMed" id="3855246"/>
      <dbReference type="DOI" id="10.1128/jvi.56.3.978-986.1985"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Multifunctional protein that plays a role in silencing host antiviral defenses and promoting viral transcription. Does not seem to be essential for HBV infection. May be directly involved in development of cirrhosis and liver cancer (hepatocellular carcinoma). Most of cytosolic activities involve modulation of cytosolic calcium. The effect on apoptosis is controversial depending on the cell types in which the studies have been conducted. May induce apoptosis by localizing in mitochondria and causing loss of mitochondrial membrane potential. May also modulate apoptosis by binding host CFLAR, a key regulator of the death-inducing signaling complex (DISC). Promotes viral transcription by using the host E3 ubiquitin ligase DDB1 to target the SMC5-SMC6 complex to proteasomal degradation. This host complex would otherwise bind to viral episomal DNA, and prevents its transcription. Moderately stimulates transcription of many different viral and cellular transcription elements. Promoters and enhancers stimulated by HBx contain DNA binding sites for NF-kappa-B, AP-1, AP-2, c-EBP, ATF/CREB, or the calcium-activated factor NF-AT.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">May form homodimer. May interact with host CEBPA, CFLAR, CREB1, DDB1, E4F1, HBXIP, HSPD1/HSP60, NFKBIA, POLR2E and SMAD4. Interacts with host SMC5-SMC6 complex and induces its degradation. Interacts with host TRPC4AP; leading to prevent ubiquitination of TRPC4AP. Interacts with host PLSCR1; this interaction promotes ubiquitination and degradation of HBx and impairs HBx-mediated cell proliferation.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host mitochondrion</location>
    </subcellularLocation>
    <text evidence="1">Mainly cytoplasmic as only a fraction is detected in the nucleus. In cytoplasm, a minor fraction associates with mitochondria or proteasomes.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">A fraction may be phosphorylated in insect cells and HepG2 cells, a human hepatoblastoma cell line. Phosphorylated in vitro by host protein kinase C or mitogen-activated protein kinase. N-acetylated in insect cells.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the orthohepadnavirus protein X family.</text>
  </comment>
  <dbReference type="EMBL" id="M11082">
    <property type="protein sequence ID" value="AAA19184.1"/>
    <property type="molecule type" value="Unassigned_DNA"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000007632">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033650">
    <property type="term" value="C:host cell mitochondrion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042025">
    <property type="term" value="C:host cell nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006351">
    <property type="term" value="P:DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0085033">
    <property type="term" value="P:symbiont-mediated activation of host NF-kappaB cascade"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039592">
    <property type="term" value="P:symbiont-mediated arrest of host cell cycle during G2/M transition"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019079">
    <property type="term" value="P:viral genome replication"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_04074">
    <property type="entry name" value="HBV_X"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000236">
    <property type="entry name" value="Transactivation_prot_X"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00739">
    <property type="entry name" value="X"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1074">Activation of host NF-kappa-B by virus</keyword>
  <keyword id="KW-0010">Activator</keyword>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1079">Host G2/M cell cycle arrest by virus</keyword>
  <keyword id="KW-1045">Host mitochondrion</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1121">Modulation of host cell cycle by virus</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_0000222372" description="Protein X">
    <location>
      <begin position="1"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="25"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="region of interest" description="Mitochondrial targeting sequence" evidence="1">
    <location>
      <begin position="68"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Polar residues" evidence="2">
    <location>
      <begin position="36"/>
      <end position="50"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_04074"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <sequence length="141" mass="15213" checksum="23718D55038A6695" modified="1989-10-01" version="1">MAARLCCHLDSARDVLLLRPFGPQSSGPSFPRPAAGSAASSASSPSPSDDSDLPLGRLPACFASGSGPCCLVFTCADLRTMDSTVNFVSWHANRQLGMPSKDLWTPYIKDQLLTKWEEGSIDPRLSIFLLGGCRHKCMRLL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>