<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-02-22" modified="2024-11-27" version="134" xmlns="http://uniprot.org/uniprot">
  <accession>Q3TMQ6</accession>
  <accession>Q3UWE7</accession>
  <accession>Q80XS4</accession>
  <accession>Q80Z85</accession>
  <name>ANG4_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Angiogenin-4</fullName>
      <ecNumber evidence="4">3.1.27.-</ecNumber>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">Ang4</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Nat. Immunol." volume="4" first="269" last="273">
      <title>Angiogenins: a new class of microbicidal proteins involved in innate immunity.</title>
      <authorList>
        <person name="Hooper L.V."/>
        <person name="Stappenbeck T.S."/>
        <person name="Hong C.V."/>
        <person name="Gordon J.I."/>
      </authorList>
      <dbReference type="PubMed" id="12548285"/>
      <dbReference type="DOI" id="10.1038/ni888"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <strain>NMRI</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Science" volume="309" first="1559" last="1563">
      <title>The transcriptional landscape of the mammalian genome.</title>
      <authorList>
        <person name="Carninci P."/>
        <person name="Kasukawa T."/>
        <person name="Katayama S."/>
        <person name="Gough J."/>
        <person name="Frith M.C."/>
        <person name="Maeda N."/>
        <person name="Oyama R."/>
        <person name="Ravasi T."/>
        <person name="Lenhard B."/>
        <person name="Wells C."/>
        <person name="Kodzius R."/>
        <person name="Shimokawa K."/>
        <person name="Bajic V.B."/>
        <person name="Brenner S.E."/>
        <person name="Batalov S."/>
        <person name="Forrest A.R."/>
        <person name="Zavolan M."/>
        <person name="Davis M.J."/>
        <person name="Wilming L.G."/>
        <person name="Aidinis V."/>
        <person name="Allen J.E."/>
        <person name="Ambesi-Impiombato A."/>
        <person name="Apweiler R."/>
        <person name="Aturaliya R.N."/>
        <person name="Bailey T.L."/>
        <person name="Bansal M."/>
        <person name="Baxter L."/>
        <person name="Beisel K.W."/>
        <person name="Bersano T."/>
        <person name="Bono H."/>
        <person name="Chalk A.M."/>
        <person name="Chiu K.P."/>
        <person name="Choudhary V."/>
        <person name="Christoffels A."/>
        <person name="Clutterbuck D.R."/>
        <person name="Crowe M.L."/>
        <person name="Dalla E."/>
        <person name="Dalrymple B.P."/>
        <person name="de Bono B."/>
        <person name="Della Gatta G."/>
        <person name="di Bernardo D."/>
        <person name="Down T."/>
        <person name="Engstrom P."/>
        <person name="Fagiolini M."/>
        <person name="Faulkner G."/>
        <person name="Fletcher C.F."/>
        <person name="Fukushima T."/>
        <person name="Furuno M."/>
        <person name="Futaki S."/>
        <person name="Gariboldi M."/>
        <person name="Georgii-Hemming P."/>
        <person name="Gingeras T.R."/>
        <person name="Gojobori T."/>
        <person name="Green R.E."/>
        <person name="Gustincich S."/>
        <person name="Harbers M."/>
        <person name="Hayashi Y."/>
        <person name="Hensch T.K."/>
        <person name="Hirokawa N."/>
        <person name="Hill D."/>
        <person name="Huminiecki L."/>
        <person name="Iacono M."/>
        <person name="Ikeo K."/>
        <person name="Iwama A."/>
        <person name="Ishikawa T."/>
        <person name="Jakt M."/>
        <person name="Kanapin A."/>
        <person name="Katoh M."/>
        <person name="Kawasawa Y."/>
        <person name="Kelso J."/>
        <person name="Kitamura H."/>
        <person name="Kitano H."/>
        <person name="Kollias G."/>
        <person name="Krishnan S.P."/>
        <person name="Kruger A."/>
        <person name="Kummerfeld S.K."/>
        <person name="Kurochkin I.V."/>
        <person name="Lareau L.F."/>
        <person name="Lazarevic D."/>
        <person name="Lipovich L."/>
        <person name="Liu J."/>
        <person name="Liuni S."/>
        <person name="McWilliam S."/>
        <person name="Madan Babu M."/>
        <person name="Madera M."/>
        <person name="Marchionni L."/>
        <person name="Matsuda H."/>
        <person name="Matsuzawa S."/>
        <person name="Miki H."/>
        <person name="Mignone F."/>
        <person name="Miyake S."/>
        <person name="Morris K."/>
        <person name="Mottagui-Tabar S."/>
        <person name="Mulder N."/>
        <person name="Nakano N."/>
        <person name="Nakauchi H."/>
        <person name="Ng P."/>
        <person name="Nilsson R."/>
        <person name="Nishiguchi S."/>
        <person name="Nishikawa S."/>
        <person name="Nori F."/>
        <person name="Ohara O."/>
        <person name="Okazaki Y."/>
        <person name="Orlando V."/>
        <person name="Pang K.C."/>
        <person name="Pavan W.J."/>
        <person name="Pavesi G."/>
        <person name="Pesole G."/>
        <person name="Petrovsky N."/>
        <person name="Piazza S."/>
        <person name="Reed J."/>
        <person name="Reid J.F."/>
        <person name="Ring B.Z."/>
        <person name="Ringwald M."/>
        <person name="Rost B."/>
        <person name="Ruan Y."/>
        <person name="Salzberg S.L."/>
        <person name="Sandelin A."/>
        <person name="Schneider C."/>
        <person name="Schoenbach C."/>
        <person name="Sekiguchi K."/>
        <person name="Semple C.A."/>
        <person name="Seno S."/>
        <person name="Sessa L."/>
        <person name="Sheng Y."/>
        <person name="Shibata Y."/>
        <person name="Shimada H."/>
        <person name="Shimada K."/>
        <person name="Silva D."/>
        <person name="Sinclair B."/>
        <person name="Sperling S."/>
        <person name="Stupka E."/>
        <person name="Sugiura K."/>
        <person name="Sultana R."/>
        <person name="Takenaka Y."/>
        <person name="Taki K."/>
        <person name="Tammoja K."/>
        <person name="Tan S.L."/>
        <person name="Tang S."/>
        <person name="Taylor M.S."/>
        <person name="Tegner J."/>
        <person name="Teichmann S.A."/>
        <person name="Ueda H.R."/>
        <person name="van Nimwegen E."/>
        <person name="Verardo R."/>
        <person name="Wei C.L."/>
        <person name="Yagi K."/>
        <person name="Yamanishi H."/>
        <person name="Zabarovsky E."/>
        <person name="Zhu S."/>
        <person name="Zimmer A."/>
        <person name="Hide W."/>
        <person name="Bult C."/>
        <person name="Grimmond S.M."/>
        <person name="Teasdale R.D."/>
        <person name="Liu E.T."/>
        <person name="Brusic V."/>
        <person name="Quackenbush J."/>
        <person name="Wahlestedt C."/>
        <person name="Mattick J.S."/>
        <person name="Hume D.A."/>
        <person name="Kai C."/>
        <person name="Sasaki D."/>
        <person name="Tomaru Y."/>
        <person name="Fukuda S."/>
        <person name="Kanamori-Katayama M."/>
        <person name="Suzuki M."/>
        <person name="Aoki J."/>
        <person name="Arakawa T."/>
        <person name="Iida J."/>
        <person name="Imamura K."/>
        <person name="Itoh M."/>
        <person name="Kato T."/>
        <person name="Kawaji H."/>
        <person name="Kawagashira N."/>
        <person name="Kawashima T."/>
        <person name="Kojima M."/>
        <person name="Kondo S."/>
        <person name="Konno H."/>
        <person name="Nakano K."/>
        <person name="Ninomiya N."/>
        <person name="Nishio T."/>
        <person name="Okada M."/>
        <person name="Plessy C."/>
        <person name="Shibata K."/>
        <person name="Shiraki T."/>
        <person name="Suzuki S."/>
        <person name="Tagami M."/>
        <person name="Waki K."/>
        <person name="Watahiki A."/>
        <person name="Okamura-Oho Y."/>
        <person name="Suzuki H."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
      </authorList>
      <dbReference type="PubMed" id="16141072"/>
      <dbReference type="DOI" id="10.1126/science.1112014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
      <tissue>Colon</tissue>
      <tissue>Intestinal mucosa</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2009" name="PLoS Biol." volume="7" first="E1000112" last="E1000112">
      <title>Lineage-specific biology revealed by a finished genome assembly of the mouse.</title>
      <authorList>
        <person name="Church D.M."/>
        <person name="Goodstadt L."/>
        <person name="Hillier L.W."/>
        <person name="Zody M.C."/>
        <person name="Goldstein S."/>
        <person name="She X."/>
        <person name="Bult C.J."/>
        <person name="Agarwala R."/>
        <person name="Cherry J.L."/>
        <person name="DiCuccio M."/>
        <person name="Hlavina W."/>
        <person name="Kapustin Y."/>
        <person name="Meric P."/>
        <person name="Maglott D."/>
        <person name="Birtle Z."/>
        <person name="Marques A.C."/>
        <person name="Graves T."/>
        <person name="Zhou S."/>
        <person name="Teague B."/>
        <person name="Potamousis K."/>
        <person name="Churas C."/>
        <person name="Place M."/>
        <person name="Herschleb J."/>
        <person name="Runnheim R."/>
        <person name="Forrest D."/>
        <person name="Amos-Landgraf J."/>
        <person name="Schwartz D.C."/>
        <person name="Cheng Z."/>
        <person name="Lindblad-Toh K."/>
        <person name="Eichler E.E."/>
        <person name="Ponting C.P."/>
      </authorList>
      <dbReference type="PubMed" id="19468303"/>
      <dbReference type="DOI" id="10.1371/journal.pbio.1000112"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="submission" date="2005-07" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Adams M.D."/>
        <person name="Myers E.W."/>
        <person name="Smith H.O."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>FVB/N</strain>
      <tissue>Brain</tissue>
      <tissue>Colon</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2007" name="Biochemistry" volume="46" first="2431" last="2443">
      <title>Biological and structural features of murine angiogenin-4, an angiogenic protein.</title>
      <authorList>
        <person name="Crabtree B."/>
        <person name="Holloway D.E."/>
        <person name="Baker M.D."/>
        <person name="Acharya K.R."/>
        <person name="Subramanian V."/>
      </authorList>
      <dbReference type="PubMed" id="17279775"/>
      <dbReference type="DOI" id="10.1021/bi062158n"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.02 ANGSTROMS)</scope>
    <scope>FUNCTION</scope>
    <scope>MUTAGENESIS OF HIS-36; ARG-56; LYS-83; HIS-136 AND GLU-139</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4">Has bactericidal activity against E.faecalis and L.monocytogenes, but not against L.innocua and E.coli. Promotes angiogenesis (in vitro). Has low ribonuclease activity (in vitro). Promotes proliferation of melanoma cells, but not of endothelial cells or fibroblasts (in vitro).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="3">Cytoplasmic vesicle</location>
      <location evidence="3">Secretory vesicle lumen</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Nucleus</location>
      <location evidence="1">Nucleolus</location>
    </subcellularLocation>
    <text evidence="1 3">Exposure to bacterial lipopolysaccharide (LPS) triggers secretion from intestinal epithelium cells (PubMed:12548285). Rapidly endocytosed by target cells and translocated to the nucleus where it accumulates in the nucleolus and binds to DNA (By similarity).</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Detected in small intestine, caecum and colon, with the highest expression in Paneth cells in the intestinal epithelium.</text>
  </comment>
  <comment type="induction">
    <text evidence="3">Up-regulated in small intestine by contact with normal intestinal microflora. Expressed at very low levels in intestine from germ-free mice.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the pancreatic ribonuclease family.</text>
  </comment>
  <dbReference type="EC" id="3.1.27.-" evidence="4"/>
  <dbReference type="EMBL" id="AY219870">
    <property type="protein sequence ID" value="AAO62354.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK136415">
    <property type="protein sequence ID" value="BAE22968.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK165799">
    <property type="protein sequence ID" value="BAE38385.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC122877">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH466659">
    <property type="protein sequence ID" value="EDL42283.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC042938">
    <property type="protein sequence ID" value="AAH42938.2"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC132444">
    <property type="protein sequence ID" value="AAI32445.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC132448">
    <property type="protein sequence ID" value="AAI32449.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS36911.1"/>
  <dbReference type="RefSeq" id="NP_808212.2">
    <property type="nucleotide sequence ID" value="NM_177544.4"/>
  </dbReference>
  <dbReference type="PDB" id="2J4T">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.02 A"/>
    <property type="chains" value="A/B=1-144"/>
  </dbReference>
  <dbReference type="PDBsum" id="2J4T"/>
  <dbReference type="AlphaFoldDB" id="Q3TMQ6"/>
  <dbReference type="SMR" id="Q3TMQ6"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000073525"/>
  <dbReference type="iPTMnet" id="Q3TMQ6"/>
  <dbReference type="PhosphoSitePlus" id="Q3TMQ6"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000073525"/>
  <dbReference type="PeptideAtlas" id="Q3TMQ6"/>
  <dbReference type="ProteomicsDB" id="281980"/>
  <dbReference type="DNASU" id="219033"/>
  <dbReference type="Ensembl" id="ENSMUST00000073860.6">
    <property type="protein sequence ID" value="ENSMUSP00000073525.6"/>
    <property type="gene ID" value="ENSMUSG00000060615.6"/>
  </dbReference>
  <dbReference type="GeneID" id="219033"/>
  <dbReference type="KEGG" id="mmu:219033"/>
  <dbReference type="UCSC" id="uc007tnd.1">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:2656551"/>
  <dbReference type="CTD" id="219033"/>
  <dbReference type="MGI" id="MGI:2656551">
    <property type="gene designation" value="Ang4"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000060615"/>
  <dbReference type="eggNOG" id="ENOG502S9Q1">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000162981"/>
  <dbReference type="HOGENOM" id="CLU_117006_3_1_1"/>
  <dbReference type="InParanoid" id="Q3TMQ6"/>
  <dbReference type="OMA" id="ACDEHEH"/>
  <dbReference type="OrthoDB" id="4612546at2759"/>
  <dbReference type="PhylomeDB" id="Q3TMQ6"/>
  <dbReference type="TreeFam" id="TF333393"/>
  <dbReference type="BioGRID-ORCS" id="219033">
    <property type="hits" value="2 hits in 74 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="Ang4">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="Q3TMQ6"/>
  <dbReference type="PRO" id="PR:Q3TMQ6"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 14"/>
  </dbReference>
  <dbReference type="RNAct" id="Q3TMQ6">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000060615">
    <property type="expression patterns" value="Expressed in paneth cell and 30 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q3TMQ6">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005730">
    <property type="term" value="C:nucleolus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030141">
    <property type="term" value="C:secretory granule"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004519">
    <property type="term" value="F:endonuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003676">
    <property type="term" value="F:nucleic acid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004540">
    <property type="term" value="F:RNA nuclease activity"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001525">
    <property type="term" value="P:angiogenesis"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008284">
    <property type="term" value="P:positive regulation of cell population proliferation"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009617">
    <property type="term" value="P:response to bacterium"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006401">
    <property type="term" value="P:RNA catabolic process"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd06265">
    <property type="entry name" value="RNase_A_canonical"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.130.10:FF:000001">
    <property type="entry name" value="Ribonuclease pancreatic"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.130.10">
    <property type="entry name" value="Ribonuclease A-like domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001427">
    <property type="entry name" value="RNaseA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036816">
    <property type="entry name" value="RNaseA-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023411">
    <property type="entry name" value="RNaseA_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023412">
    <property type="entry name" value="RNaseA_domain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437:SF60">
    <property type="entry name" value="ANGIOGENIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437">
    <property type="entry name" value="RIBONUCLEASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00074">
    <property type="entry name" value="RnaseA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00794">
    <property type="entry name" value="RIBONUCLEASE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00092">
    <property type="entry name" value="RNAse_Pc"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54076">
    <property type="entry name" value="RNase A-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00127">
    <property type="entry name" value="RNASE_PANCREATIC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0037">Angiogenesis</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0968">Cytoplasmic vesicle</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0255">Endonuclease</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000415440" description="Angiogenin-4">
    <location>
      <begin position="25"/>
      <end position="144"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Nucleolar localization signal" evidence="1">
    <location>
      <begin position="54"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="active site" description="Proton acceptor" evidence="6">
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="active site" description="Proton donor" evidence="6">
    <location>
      <position position="136"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="49"/>
      <end position="103"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="62"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="80"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of ribonuclease activity. Loss of angiogenic activity." evidence="4">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of angiogenic activity. No effect on ribonuclease activity." evidence="4">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="56"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of angiogenic activity. No effect on ribonuclease activity." evidence="4">
    <original>K</original>
    <variation>N</variation>
    <location>
      <position position="83"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of ribonuclease activity. Loss of angiogenic activity." evidence="4">
    <original>H</original>
    <variation>A</variation>
    <location>
      <position position="136"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of angiogenic activity. Increased ribonuclease activity." evidence="4">
    <original>E</original>
    <variation>A</variation>
    <location>
      <position position="139"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAO62354, 4; EDL42283 and 5; AAH42938/AAI32445/AAI32449." evidence="5" ref="1 4 5">
    <original>N</original>
    <variation>K</variation>
    <location>
      <position position="42"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; BAE22968." evidence="5" ref="2">
    <original>F</original>
    <variation>V</variation>
    <location>
      <position position="68"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAO62354, 4; EDL42283 and 5; AAH42938/AAI32445/AAI32449." evidence="5" ref="1 4 5">
    <original>GA</original>
    <variation>RG</variation>
    <location>
      <begin position="107"/>
      <end position="108"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAO62354, 4; EDL42283 and 5; AAH42938/AAI32445/AAI32449." evidence="5" ref="1 4 5">
    <original>R</original>
    <variation>W</variation>
    <location>
      <position position="111"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="27"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="46"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="turn" evidence="7">
    <location>
      <begin position="59"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="64"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="74"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="78"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="turn" evidence="7">
    <location>
      <begin position="81"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="84"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="88"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="98"/>
      <end position="105"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="111"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="116"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="126"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="133"/>
      <end position="137"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="139"/>
      <end position="142"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P03950"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12548285"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="17279775"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="17279775"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="2J4T"/>
    </source>
  </evidence>
  <sequence length="144" mass="16425" checksum="0D93DE32A5A4CABA" modified="2005-10-11" version="1" precursor="true">MTMSPCPLLLVFVLGLVVIPPTLAQNERYEKFLRQHYDAKPNGRDDRYCESMMKERKLTSPCKDVNTFIHGTKKNIRAICGKKGSPYGENFRISNSPFQITTCTHSGASPRPPCGYRAFKDFRYIVIACEDGWPVHFDESFISP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>