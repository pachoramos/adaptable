<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-10-31" modified="2023-02-22" version="57" xmlns="http://uniprot.org/uniprot">
  <accession>Q9U8X3</accession>
  <name>TACA2_TACTR</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Tachystatin-A2</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Tachypleus tridentatus</name>
    <name type="common">Japanese horseshoe crab</name>
    <dbReference type="NCBI Taxonomy" id="6853"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Merostomata</taxon>
      <taxon>Xiphosura</taxon>
      <taxon>Limulidae</taxon>
      <taxon>Tachypleus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="J. Biol. Chem." volume="274" first="26172" last="26178">
      <title>Horseshoe crab hemocyte-derived antimicrobial polypeptides, tachystatins, with sequence similarity to spider neurotoxins.</title>
      <authorList>
        <person name="Osaki T."/>
        <person name="Omotezako M."/>
        <person name="Nagayama R."/>
        <person name="Hirata M."/>
        <person name="Iwanaga S."/>
        <person name="Kasahara J."/>
        <person name="Hattori J."/>
        <person name="Ito I."/>
        <person name="Sugiyama H."/>
        <person name="Kawabata S."/>
      </authorList>
      <dbReference type="PubMed" id="10473569"/>
      <dbReference type="DOI" id="10.1074/jbc.274.37.26172"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 24-67</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Hemocyte</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2018" name="Nat. Struct. Mol. Biol." volume="25" first="270" last="278">
      <title>Screening, large-scale production and structure-based classification of cystine-dense peptides.</title>
      <authorList>
        <person name="Correnti C.E."/>
        <person name="Gewe M.M."/>
        <person name="Mehlin C."/>
        <person name="Bandaranayake A.D."/>
        <person name="Johnsen W.A."/>
        <person name="Rupert P.B."/>
        <person name="Brusniak M.Y."/>
        <person name="Clarke M."/>
        <person name="Burke S.E."/>
        <person name="De Van Der Schueren W."/>
        <person name="Pilat K."/>
        <person name="Turnbaugh S.M."/>
        <person name="May D."/>
        <person name="Watson A."/>
        <person name="Chan M.K."/>
        <person name="Bahl C.D."/>
        <person name="Olson J.M."/>
        <person name="Strong R.K."/>
      </authorList>
      <dbReference type="PubMed" id="29483648"/>
      <dbReference type="DOI" id="10.1038/s41594-018-0033-9"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 24-67</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="J. Biol. Chem." volume="277" first="23651" last="23657">
      <title>Structure of the antimicrobial peptide tachystatin A.</title>
      <authorList>
        <person name="Fujitani N."/>
        <person name="Kawabata S."/>
        <person name="Osaki T."/>
        <person name="Kumaki Y."/>
        <person name="Demura M."/>
        <person name="Nitta K."/>
        <person name="Kawano K."/>
      </authorList>
      <dbReference type="PubMed" id="11959852"/>
      <dbReference type="DOI" id="10.1074/jbc.m111120200"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 24-67</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3">Exhibits stronger antimicrobial activity against the Gram-positive bacteria (S.aureus (IC(50)=4.2 ug/ml)) and fungi (C.albicans (IC(50)=3.0 ug/ml) and P.pastoris (IC(50)=0.5 ug/ml)) than Gram-negative bacteria (E.coli (IC(50)=25 ug/ml)) (PubMed:10473569). Binds to chitin (8.4 uM are required to obtain 50% of binding) (PubMed:10473569). Does not cause hemolysis on sheep erythrocytes (PubMed:10473569). Has no blocking activity on the P-type calcium channel (PubMed:10473569). Has also been shown to weakly inhibit Kv1.2/KCNA2 voltage-gated potassium channels and TRPV1 receptors (PubMed:29483648).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Granular hemocytes, small secretory granules.</text>
  </comment>
  <comment type="domain">
    <text evidence="2">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="5055.5" method="Electrospray" evidence="1"/>
  <dbReference type="EMBL" id="AB023783">
    <property type="protein sequence ID" value="BAA85250.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="1CIX">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=24-67"/>
  </dbReference>
  <dbReference type="PDBsum" id="1CIX"/>
  <dbReference type="AlphaFoldDB" id="Q9U8X3"/>
  <dbReference type="SMR" id="Q9U8X3"/>
  <dbReference type="EvolutionaryTrace" id="Q9U8X3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.40.20">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022717">
    <property type="entry name" value="Antimicrobial_tachystatin_A"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11406">
    <property type="entry name" value="Tachystatin_A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57059">
    <property type="entry name" value="omega toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000256690" description="Tachystatin-A2" evidence="1">
    <location>
      <begin position="24"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="site" description="May be important for binding to chitin">
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 5">
    <location>
      <begin position="27"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 5">
    <location>
      <begin position="34"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 5">
    <location>
      <begin position="46"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="32"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="51"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="62"/>
      <end position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10473569"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11959852"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="29483648"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="10473569"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="5">
    <source>
      <dbReference type="PDB" id="1CIX"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="1CIX"/>
    </source>
  </evidence>
  <sequence length="67" mass="7511" checksum="6477DF0556E91310" modified="2000-05-01" version="1" precursor="true">MKLQNTLILIGCLFLMGAMIGDAYSRCQLQGFNCVVRSYGLPTIPCCRGLTCRSYFPGSTYGRCQRY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>