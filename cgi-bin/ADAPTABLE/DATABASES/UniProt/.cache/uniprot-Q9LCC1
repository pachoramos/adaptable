<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-01-11" modified="2024-11-27" version="30" xmlns="http://uniprot.org/uniprot">
  <accession>Q9LCC1</accession>
  <name>DMFAA_ALCSP</name>
  <protein>
    <recommendedName>
      <fullName evidence="3 6">N,N-dimethylformamidase alpha subunit</fullName>
      <ecNumber>3.5.1.56</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">N,N-dimethylformamidase light chain</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="6" type="primary">dmfA1</name>
  </gene>
  <organism>
    <name type="scientific">Alcaligenes sp</name>
    <dbReference type="NCBI Taxonomy" id="512"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Betaproteobacteria</taxon>
      <taxon>Burkholderiales</taxon>
      <taxon>Alcaligenaceae</taxon>
      <taxon>Alcaligenes</taxon>
    </lineage>
  </organism>
  <reference evidence="5 6" key="1">
    <citation type="journal article" date="1999" name="Biosci. Biotechnol. Biochem." volume="63" first="2091" last="2096">
      <title>Cloning and expression of the N,N-dimethylformamidase gene from Alcaligenes sp. strain KUFA-1.</title>
      <authorList>
        <person name="Hasegawa Y."/>
        <person name="Tokuyama T."/>
        <person name="Iwaki H."/>
      </authorList>
      <dbReference type="PubMed" id="10664842"/>
      <dbReference type="DOI" id="10.1271/bbb.63.2091"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>PROTEIN SEQUENCE OF 2-26</scope>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>SUBUNIT</scope>
    <source>
      <strain evidence="6">KUFA-1</strain>
    </source>
  </reference>
  <reference evidence="5" key="2">
    <citation type="journal article" date="1999" name="J. Ferment. Bioeng." volume="63" first="2091" last="2096">
      <title>Purification and characterization of N,N-dimethylformamidase from Alcaligenes sp. KUFA-1.</title>
      <authorList>
        <person name="Hasegawa Y."/>
        <person name="Matsuo M."/>
        <person name="Sigemoto Y."/>
        <person name="Sakai T."/>
        <person name="Tokuyama T."/>
      </authorList>
    </citation>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>SUBUNIT</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2">Hydrolyzes N,N-dimethylformamide, and to a lesser extent N,N-dimethylacetamide and N,N-diethylacetamide. Has no activity against the substituted amides N-methylformamide, N-ethylformamide, N-ethylformamide and N-methylacetamide or the unsubstituted amides formamide, nicotinamide, acetoamide, benzamide, acetamide and acrylamide.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1 2">
      <text>N,N-dimethylformamide + H2O = dimethylamine + formate</text>
      <dbReference type="Rhea" id="RHEA:19517"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15740"/>
      <dbReference type="ChEBI" id="CHEBI:17741"/>
      <dbReference type="ChEBI" id="CHEBI:58040"/>
      <dbReference type="EC" id="3.5.1.56"/>
    </reaction>
  </comment>
  <comment type="activity regulation">
    <text evidence="2">Activity is slightly inhibited by Mg(2+) and Mn(2+), and slightly increased by Cu(2+). Activity is slightly inhibited by the chelating agents 8-hydroxyquinoline, ethylenediaminetetraacetate, o-phenanthroline and 2,2'-bipyridyl.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <kinetics>
      <KM evidence="1 2">1.28 mM for N,N-dimethylformamide</KM>
      <Vmax evidence="1 2">83.3 umol/min/mg enzyme toward N,N-dimethylformamide</Vmax>
    </kinetics>
    <phDependence>
      <text evidence="1 2">Optimum pH is 6.0-7.0.</text>
    </phDependence>
    <temperatureDependence>
      <text evidence="1 2">Optimum temperature is 50-55 degrees Celsius. Stable up to 50 degrees Celsius, inactivated after incubation at 60 degrees Celsius for 30 minutes. Activity decreases below 50 degrees Celsius, with 20% of activity retained at 25 degrees Celsius.</text>
    </temperatureDependence>
  </comment>
  <comment type="subunit">
    <text evidence="1 2">Heterotetramer of two DmfA1 (alpha) and two DmfA2 (beta) subunits.</text>
  </comment>
  <comment type="caution">
    <text evidence="5">It is not known which subunit of DmfA1 or DmfA2 possesses catalytic activity.</text>
  </comment>
  <dbReference type="EC" id="3.5.1.56"/>
  <dbReference type="EMBL" id="AB028874">
    <property type="protein sequence ID" value="BAA90663.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="JC7173">
    <property type="entry name" value="JC7173"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9LCC1"/>
  <dbReference type="SMR" id="Q9LCC1"/>
  <dbReference type="KEGG" id="ag:BAA90663"/>
  <dbReference type="GO" id="GO:0050116">
    <property type="term" value="F:N,N-dimethylformamidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <feature type="initiator methionine" description="Removed" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000403315" description="N,N-dimethylformamidase alpha subunit" evidence="1">
    <location>
      <begin position="2"/>
      <end position="132"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10664842"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="10664842"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="BAA90663.1"/>
    </source>
  </evidence>
  <sequence length="132" mass="16031" checksum="099F783002EB99E2" modified="2000-10-01" version="1">MTEASESCVRDPSNYRDRSADWYAFYDERRRKEIIDIIDEHPEIVEEHAANPFGYRKHPSPYLQRVHNYFRMQPTFGKYYIYSEREWDAYRIATIREFGELPELGDERFKTEEEAMHAVFLRRIEDVRAELA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>