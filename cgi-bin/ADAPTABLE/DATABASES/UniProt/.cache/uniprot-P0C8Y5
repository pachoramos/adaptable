<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-03-24" modified="2024-07-24" version="35" xmlns="http://uniprot.org/uniprot">
  <accession>P0C8Y5</accession>
  <accession>Q7M1F4</accession>
  <name>DEF1_HEUSA</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cysteine-rich antifungal protein 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin AFP1</fullName>
      <shortName>HsAFP1</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Heuchera sanguinea</name>
    <name type="common">Coralbells</name>
    <dbReference type="NCBI Taxonomy" id="43368"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>Saxifragales</taxon>
      <taxon>Saxifragaceae</taxon>
      <taxon>Heuchereae</taxon>
      <taxon>Heuchera</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="FEBS Lett." volume="368" first="257" last="262">
      <title>Isolation and characterisation of plant defensins from seeds of Asteraceae, Fabaceae, Hippocastanaceae and Saxifragaceae.</title>
      <authorList>
        <person name="Osborn R.W."/>
        <person name="De Samblanx G.W."/>
        <person name="Thevissen K."/>
        <person name="Goderis I."/>
        <person name="Torrekens S."/>
        <person name="Van Leuven F."/>
        <person name="Attenborough S."/>
        <person name="Rees S.B."/>
        <person name="Broekaert W.F."/>
      </authorList>
      <dbReference type="PubMed" id="7628617"/>
      <dbReference type="DOI" id="10.1016/0014-5793(95)00666-w"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="J. Biol. Chem." volume="272" first="32176" last="32181">
      <title>Specific, high affinity binding sites for an antifungal plant defensin on Neurospora crassa hyphae and microsomal membranes.</title>
      <authorList>
        <person name="Thevissen K."/>
        <person name="Osborn R.W."/>
        <person name="Acland D.P."/>
        <person name="Broekaert W.F."/>
      </authorList>
      <dbReference type="PubMed" id="9405418"/>
      <dbReference type="DOI" id="10.1074/jbc.272.51.32176"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1998" name="Mycopathologia" volume="144" first="87" last="91">
      <title>Fungicidal and binding properties of three plant peptides.</title>
      <authorList>
        <person name="De Lucca A.J."/>
        <person name="Jacks T.J."/>
        <person name="Broekaert W.J."/>
      </authorList>
      <dbReference type="PubMed" id="16284838"/>
      <dbReference type="DOI" id="10.1023/a:1007018423603"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 4">Possesses antifungal activity insensitive to inorganic cations. Causes germ tubes and hyphae to swell and form multiple hyphal buds. Binds to the plasma membrane of the fungus. Has no inhibitory effect on insect gut alpha-amylase.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="PIR" id="S66220">
    <property type="entry name" value="S66220"/>
  </dbReference>
  <dbReference type="PDB" id="2N2Q">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-54"/>
  </dbReference>
  <dbReference type="PDBsum" id="2N2Q"/>
  <dbReference type="AlphaFoldDB" id="P0C8Y5"/>
  <dbReference type="SMR" id="P0C8Y5"/>
  <dbReference type="EvolutionaryTrace" id="P0C8Y5"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008176">
    <property type="entry name" value="Defensin_plant"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00304">
    <property type="entry name" value="Gamma-thionin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00940">
    <property type="entry name" value="GAMMA_THIONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000366953" description="Defensin-like protein 1">
    <location>
      <begin position="1"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="6"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="17"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="23"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="27"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="5"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="20"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="37"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="43"/>
      <end position="53"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="16284838"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7628617"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="9405418"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="2N2Q"/>
    </source>
  </evidence>
  <sequence length="54" mass="5949" checksum="67FA1CCFBB92A7C9" modified="2009-03-24" version="1">DGVKLCDVPSGTWSGHCGSSSKCSQQCKDREHFAYGGACHYQFPSVKCFCKRQC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>