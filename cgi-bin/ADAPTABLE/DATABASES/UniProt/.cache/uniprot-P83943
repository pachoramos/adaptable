<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-06-07" modified="2024-11-27" version="77" xmlns="http://uniprot.org/uniprot">
  <accession>P83943</accession>
  <name>DEFB1_CHILA</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 1</fullName>
      <shortName>BD-1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>cBD-1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB1</name>
  </gene>
  <organism evidence="6">
    <name type="scientific">Chinchilla lanigera</name>
    <name type="common">Long-tailed chinchilla</name>
    <name type="synonym">Chinchilla villidera</name>
    <dbReference type="NCBI Taxonomy" id="34839"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Hystricomorpha</taxon>
      <taxon>Chinchillidae</taxon>
      <taxon>Chinchilla</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2004" name="J. Biol. Chem." volume="279" first="20250" last="20256">
      <title>Identification and characterization of mucosal antimicrobial peptides expressed by the chinchilla (Chinchilla lanigera) airway.</title>
      <authorList>
        <person name="Harris R.H."/>
        <person name="Wilk D."/>
        <person name="Bevins C.L."/>
        <person name="Munson R.S. Jr."/>
        <person name="Bakaletz L.O."/>
      </authorList>
      <dbReference type="PubMed" id="14996845"/>
      <dbReference type="DOI" id="10.1074/jbc.m400499200"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="4">Tongue</tissue>
      <tissue evidence="4">Trachea</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 4">Has antibacterial activity against Gram-positive bacterium S.pneumoniae Serotype 14. Is also active against Gram-negative bacteria M.catarrhalis 1857, and non-typeable H.influenzae strains 86-028NP and 1128. Has antifungal activity against C.albicans. May have a role in maintaining sterility in the middle ear (PubMed:14996845). May act as a ligand for C-C chemokine receptor CCR6. Positively regulates the sperm motility and bactericidal activity in a CCR6-dependent manner. Binds to CCR6 and triggers Ca2+ mobilization in the sperm which is important for its motility (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer. Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Membrane</location>
    </subcellularLocation>
    <text evidence="1">Associates with tumor cell membrane-derived microvesicles.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Highly expressed in tongue, nasopharyngeal mucosa and skin, and to a lower extent in the Eustachian tube, lung and trachea.</text>
  </comment>
  <comment type="mass spectrometry" mass="5123.0" method="Electrospray" evidence="4"/>
  <comment type="similarity">
    <text evidence="5">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY128668">
    <property type="protein sequence ID" value="AAM97293.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_005373890.1">
    <property type="nucleotide sequence ID" value="XM_005373833.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P83943"/>
  <dbReference type="SMR" id="P83943"/>
  <dbReference type="Ensembl" id="ENSCLAT00000023558.1">
    <property type="protein sequence ID" value="ENSCLAP00000023337.1"/>
    <property type="gene ID" value="ENSCLAG00000016003.1"/>
  </dbReference>
  <dbReference type="GeneID" id="102004444"/>
  <dbReference type="GeneTree" id="ENSGT00530000064280"/>
  <dbReference type="OMA" id="RETQIGH"/>
  <dbReference type="OrthoDB" id="4865046at2759"/>
  <dbReference type="Proteomes" id="UP000694398">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694882">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990742">
    <property type="term" value="C:microvesicle"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097225">
    <property type="term" value="C:sperm midpiece"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042056">
    <property type="term" value="F:chemoattractant activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019722">
    <property type="term" value="P:calcium-mediated signaling"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060326">
    <property type="term" value="P:cell chemotaxis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051873">
    <property type="term" value="P:killing by host of symbiont cells"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="CACAO"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060474">
    <property type="term" value="P:positive regulation of flagellated sperm motility involved in capacitation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.360.10:FF:000001">
    <property type="entry name" value="Beta-defensin 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515:SF0">
    <property type="entry name" value="BETA-DEFENSIN 103"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006896" description="Beta-defensin 1">
    <location>
      <begin position="23"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="33"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="40"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="45"/>
      <end position="63"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P60022"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P81534"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="14996845"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="AAM97293.1"/>
    </source>
  </evidence>
  <sequence length="67" mass="7676" checksum="30A611CDCCD5BA8D" modified="2004-06-07" version="1" precursor="true">MRIHYLLFAVLFLFLMPVPGEGGIINTIQRYFCRVRGGRCAALTCLPRETQIGRCSVKGRKCCRTRK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>