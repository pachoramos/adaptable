<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-05-18" modified="2019-12-11" version="6" xmlns="http://uniprot.org/uniprot">
  <accession>P86488</accession>
  <name>PE11A_LITPE</name>
  <protein>
    <recommendedName>
      <fullName>Peroniin-1.1a</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Litoria peronii</name>
    <name type="common">Emerald spotted tree frog</name>
    <name type="synonym">Hyla peronii</name>
    <dbReference type="NCBI Taxonomy" id="317363"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Litoria</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2009" name="Rapid Commun. Mass Spectrom." volume="23" first="2628" last="2636">
      <title>The host-defence skin peptide profiles of Peron's Tree Frog Litoria peronii in winter and summer. Sequence determination by electrospray mass spectrometry and activities of the peptides.</title>
      <authorList>
        <person name="Bilusich D."/>
        <person name="Jackway R.J."/>
        <person name="Musgrave I.F."/>
        <person name="Tyler M.J."/>
        <person name="Bowie J.H."/>
      </authorList>
      <dbReference type="PubMed" id="19642086"/>
      <dbReference type="DOI" id="10.1002/rcm.4164"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT GLY-13</scope>
    <source>
      <tissue evidence="1">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Induces contraction of intestinal smooth muscle in isolated guinea pig ileum.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Expressed by the skin dorsal glands.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="1">Expressed during summer.</text>
  </comment>
  <comment type="mass spectrometry" mass="1567.0" method="Electrospray" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Peroniin subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045987">
    <property type="term" value="P:positive regulation of smooth muscle contraction"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000394179" description="Peroniin-1.1a">
    <location>
      <begin position="1"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glycine amide" evidence="1">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="19642086"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="13" mass="1572" checksum="6A8B8262623376C4" modified="2010-05-18" version="1">DAQEKRQPWLPFG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>