<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-09-13" modified="2024-03-27" version="82" xmlns="http://uniprot.org/uniprot">
  <accession>Q7A1C9</accession>
  <name>Y821_STAAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">UPF0349 protein MW0821</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">MW0821</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="similarity">
    <text evidence="1">Belongs to the UPF0349 family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB94686.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_001068337.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q7A1C9"/>
  <dbReference type="SMR" id="Q7A1C9"/>
  <dbReference type="KEGG" id="sam:MW0821"/>
  <dbReference type="HOGENOM" id="CLU_182025_0_0_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01542">
    <property type="entry name" value="UPF0349"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009910">
    <property type="entry name" value="DUF1450"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022916">
    <property type="entry name" value="UPF0349"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07293">
    <property type="entry name" value="DUF1450"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <feature type="chain" id="PRO_0000165899" description="UPF0349 protein MW0821">
    <location>
      <begin position="1"/>
      <end position="78"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01542"/>
    </source>
  </evidence>
  <sequence length="78" mass="8657" checksum="9F126F0F9CACAB48" modified="2004-07-05" version="1">MNPIVEFCLSNMAKGGDYVFNQLENDPDVDVLEYGCLTHCGICSAGLYALVNGDIVEGDSPEELLQNIYAHIKETWIF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>