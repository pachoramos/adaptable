<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-05-10" modified="2022-12-14" version="28" xmlns="http://uniprot.org/uniprot">
  <accession>E7EKH6</accession>
  <name>B2SN3_SYLSP</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Brevinin-2SN3</fullName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Sylvirana spinulosa</name>
    <name type="common">Fine-spined frog</name>
    <name type="synonym">Hylarana spinulosa</name>
    <dbReference type="NCBI Taxonomy" id="369515"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Sylvirana</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2013" name="Biochimie" volume="95" first="2429" last="2436">
      <title>Identification of multiple antimicrobial peptides from the skin of fine-spined frog, Hylarana spinulosa (Ranidae).</title>
      <authorList>
        <person name="Yang X."/>
        <person name="Hu Y."/>
        <person name="Xu S."/>
        <person name="Hu Y."/>
        <person name="Meng H."/>
        <person name="Guo C."/>
        <person name="Liu Y."/>
        <person name="Liu J."/>
        <person name="Yu Z."/>
        <person name="Wang H."/>
      </authorList>
      <dbReference type="PubMed" id="24055160"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2013.09.002"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue evidence="3">Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Antimicrobial peptide. Active against a variety of Gram-negative and Gram-positive bacterial strains. Active against fungus C.glabrata 090902 but not against C.albicans ATCC 10231. Shows hemolytic activity against human erythrocytes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="HQ735153">
    <property type="protein sequence ID" value="ADV36176.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="E7EKH6"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044179">
    <property type="term" value="P:hemolysis in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439778" description="Removed in mature form" evidence="4">
    <location>
      <begin position="23"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000439779" description="Brevinin-2SN3" evidence="3">
    <location>
      <begin position="43"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="66"/>
      <end position="72"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="24055160"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="24055160"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="24055160"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="5">
    <source>
      <dbReference type="EMBL" id="ADV36176.1"/>
    </source>
  </evidence>
  <sequence length="72" mass="7732" checksum="7EF6F9F3AB618DEB" modified="2011-03-08" version="1" precursor="true">MFTLKKPLLLLVFLGMISLSLCQDERGADEDDGGEMTEEEKRGAFGNLLKGVAKKAGLKILSIAQCKLSGTC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>