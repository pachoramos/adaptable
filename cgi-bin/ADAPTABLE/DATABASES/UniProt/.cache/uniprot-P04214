<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1987-03-20" modified="2024-10-02" version="129" xmlns="http://uniprot.org/uniprot">
  <accession>P04214</accession>
  <name>TVB6_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>T-cell receptor beta chain V region E1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1984" name="Nature" volume="312" first="40" last="46">
      <title>Structure, expression and divergence of T-cell receptor beta-chain variable regions.</title>
      <authorList>
        <person name="Patten P."/>
        <person name="Yokota T."/>
        <person name="Rothbard J."/>
        <person name="Chien Y."/>
        <person name="Arai K."/>
        <person name="Davis M.M."/>
      </authorList>
      <dbReference type="PubMed" id="6092964"/>
      <dbReference type="DOI" id="10.1038/312040a0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="miscellaneous">
    <text>This sequence was derived from a T-helper clone.</text>
  </comment>
  <dbReference type="EMBL" id="X01642">
    <property type="protein sequence ID" value="CAA25799.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A02009">
    <property type="entry name" value="RWMSE1"/>
  </dbReference>
  <dbReference type="PDB" id="1FO0">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="B=22-131"/>
  </dbReference>
  <dbReference type="PDB" id="1KB5">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="B=22-116"/>
  </dbReference>
  <dbReference type="PDB" id="1KJ2">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.71 A"/>
    <property type="chains" value="B/E=22-136"/>
  </dbReference>
  <dbReference type="PDB" id="1NAM">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.70 A"/>
    <property type="chains" value="B=22-135"/>
  </dbReference>
  <dbReference type="PDB" id="2OL3">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.90 A"/>
    <property type="chains" value="B=22-136"/>
  </dbReference>
  <dbReference type="PDBsum" id="1FO0"/>
  <dbReference type="PDBsum" id="1KB5"/>
  <dbReference type="PDBsum" id="1KJ2"/>
  <dbReference type="PDBsum" id="1NAM"/>
  <dbReference type="PDBsum" id="2OL3"/>
  <dbReference type="AlphaFoldDB" id="P04214"/>
  <dbReference type="SMR" id="P04214"/>
  <dbReference type="IntAct" id="P04214">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="ABCD" id="P04214">
    <property type="antibodies" value="1 sequenced antibody"/>
  </dbReference>
  <dbReference type="UCSC" id="uc009bni.2">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="InParanoid" id="P04214"/>
  <dbReference type="EvolutionaryTrace" id="P04214"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="RNAct" id="P04214">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042101">
    <property type="term" value="C:T cell receptor complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042605">
    <property type="term" value="F:peptide antigen binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002250">
    <property type="term" value="P:adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003599">
    <property type="entry name" value="Ig_sub"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013106">
    <property type="entry name" value="Ig_V-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR051006">
    <property type="entry name" value="TCR_variable_domain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR19343">
    <property type="entry name" value="T CELL RECEPTOR ALPHA VARIABLE 1-2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR19343:SF2">
    <property type="entry name" value="T-CELL RECEPTOR BETA CHAIN V REGION E1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07686">
    <property type="entry name" value="V-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00409">
    <property type="entry name" value="IG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0393">Immunoglobulin domain</keyword>
  <keyword id="KW-0675">Receptor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1279">T cell receptor</keyword>
  <feature type="signal peptide">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000033604" description="T-cell receptor beta chain V region E1">
    <location>
      <begin position="21"/>
      <end position="136"/>
    </location>
  </feature>
  <feature type="region of interest" description="V segment">
    <location>
      <begin position="21"/>
      <end position="116"/>
    </location>
  </feature>
  <feature type="region of interest" description="D segment">
    <location>
      <begin position="117"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="region of interest" description="J segment">
    <location>
      <begin position="121"/>
      <end position="136"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="112"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="136"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="24"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="30"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="36"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="40"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="53"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="65"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="77"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="85"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="96"/>
      <end position="103"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="108"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="118"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="strand" evidence="2">
    <location>
      <begin position="131"/>
      <end position="133"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00114"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="2">
    <source>
      <dbReference type="PDB" id="1FO0"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="1KB5"/>
    </source>
  </evidence>
  <sequence length="136" mass="15538" checksum="2B93C4912E19B705" modified="1987-03-20" version="1" precursor="true">MWQFCILCLCVLMASVATDPTVTLLEQNPRWRLVPRGQAVNLRCILKNSQYPWMSWYQQDLQKQLQWLFTLRSPRDKEVKSLPGADYLATRVTDTELRLQVANMSQGRTLYCTCSATGGLNTGQLYFGEGSKLTVL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>