<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1992-03-01" modified="2024-11-27" version="78" xmlns="http://uniprot.org/uniprot">
  <accession>P24076</accession>
  <name>BGIA_MOMCH</name>
  <protein>
    <recommendedName>
      <fullName>Glu S.griseus protease inhibitor</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>BGIA</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Momordica charantia</name>
    <name type="common">Bitter gourd</name>
    <name type="synonym">Balsam pear</name>
    <dbReference type="NCBI Taxonomy" id="3673"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Cucurbitales</taxon>
      <taxon>Cucurbitaceae</taxon>
      <taxon>Momordiceae</taxon>
      <taxon>Momordica</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="J. Biol. Chem." volume="266" first="16715" last="16721">
      <title>Purification and amino acid sequence of a bitter gourd inhibitor against an acidic amino acid-specific endopeptidase of Streptomyces griseus.</title>
      <authorList>
        <person name="Ogata F."/>
        <person name="Miyata T."/>
        <person name="Fujii N."/>
        <person name="Yoshida N."/>
        <person name="Noda K."/>
        <person name="Makisumi S."/>
        <person name="Ito A."/>
      </authorList>
      <dbReference type="PubMed" id="1679433"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)55360-6"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>ACETYLATION AT SER-1</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Competitively inhibits Glu S.griseus protease by forming probably a 1:1 complex. BGIA has no inhibitory activity against 2 other acidic amino acid-specific endopeptidases (S.aureus protease V8 and B.subtilis proteinase), chymotrypsin, trypsin, pancreatic elastase, and papain, although subtilisin Carlsberg was strongly inhibited.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the protease inhibitor I13 (potato type I serine protease inhibitor) family.</text>
  </comment>
  <dbReference type="PIR" id="A41174">
    <property type="entry name" value="A41174"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P24076"/>
  <dbReference type="SMR" id="P24076"/>
  <dbReference type="MEROPS" id="I13.004"/>
  <dbReference type="iPTMnet" id="P24076"/>
  <dbReference type="Proteomes" id="UP000504603">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009611">
    <property type="term" value="P:response to wounding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.10.10">
    <property type="entry name" value="Trypsin Inhibitor V, subunit A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000864">
    <property type="entry name" value="Prot_inh_pot1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036354">
    <property type="entry name" value="Prot_inh_pot1_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33091">
    <property type="entry name" value="PROTEIN, PUTATIVE, EXPRESSED-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33091:SF103">
    <property type="entry name" value="SERINE PROTEASE INHIBITOR, POTATO INHIBITOR I-TYPE FAMILY PROTEIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00280">
    <property type="entry name" value="potato_inhibit"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00292">
    <property type="entry name" value="POTATOINHBTR"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54654">
    <property type="entry name" value="CI-2 family of serine protease inhibitors"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00285">
    <property type="entry name" value="POTATO_INHIBITOR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <feature type="chain" id="PRO_0000217644" description="Glu S.griseus protease inhibitor">
    <location>
      <begin position="1"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="site" description="Reactive bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="modified residue" description="N-acetylserine" evidence="2">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="3"/>
      <end position="48"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="1679433"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="68" mass="7383" checksum="97C98BF4AD71E933" modified="1992-03-01" version="1">SQCQGKRSWPQLVGSTGAAAKAVIERENPRVRAVIVRVGSPVTADFRCDRVRVWVTERGIVARPPAIG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>