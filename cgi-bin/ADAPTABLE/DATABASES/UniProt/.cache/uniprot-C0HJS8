<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2016-06-08" modified="2022-05-25" version="8" xmlns="http://uniprot.org/uniprot">
  <accession>C0HJS8</accession>
  <name>CYC21_CLITE</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Cliotide T21</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Cyclotide cT21</fullName>
    </alternativeName>
  </protein>
  <organism evidence="4">
    <name type="scientific">Clitoria ternatea</name>
    <name type="common">Butterfly pea</name>
    <dbReference type="NCBI Taxonomy" id="43366"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Fabales</taxon>
      <taxon>Fabaceae</taxon>
      <taxon>Papilionoideae</taxon>
      <taxon>50 kb inversion clade</taxon>
      <taxon>NPAAA clade</taxon>
      <taxon>indigoferoid/millettioid clade</taxon>
      <taxon>Phaseoleae</taxon>
      <taxon>Clitoria</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2016" name="FEBS J." volume="283" first="2067" last="2090">
      <title>Immunostimulating and Gram-negative-specific antibacterial cyclotides from the butterfly pea Clitoria ternatea.</title>
      <authorList>
        <person name="Nguyen K.N."/>
        <person name="Nguyen G.K."/>
        <person name="Nguyen P.Q."/>
        <person name="Ang K.H."/>
        <person name="Dedon P.C."/>
        <person name="Tam J.P."/>
      </authorList>
      <dbReference type="PubMed" id="27007913"/>
      <dbReference type="DOI" id="10.1111/febs.13720"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>CYCLIZATION</scope>
    <scope>PRESENCE OF DISULFIDE BONDS</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Probably participates in a plant defense mechanism. Not active against Gram-negative bacterium E.coli ATCC 700926 or Gram-positive bacterium S.aureus ATCC 12600 up to a concentration of 100 uM under low-salt conditions.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed in root nodules but not in seed.</text>
  </comment>
  <comment type="domain">
    <text evidence="5">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">Contains 3 disulfide bonds.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2 3">This is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="3101.297" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the cyclotide family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HJS8"/>
  <dbReference type="SMR" id="C0HJS8"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000436318" description="Cliotide T21" evidence="3">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="8"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="13"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Asp-Asn)" evidence="3">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P58442"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="27007913"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="27007913"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="28" mass="3128" checksum="E8028A4CF6E90FD3" modified="2016-06-08" version="1">DLQCAETCVHSPCIGPCYCKHGLICYRN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>