<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-09-27" modified="2024-11-27" version="123" xmlns="http://uniprot.org/uniprot">
  <accession>Q91V82</accession>
  <accession>Q8R556</accession>
  <name>DEFB8_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 8</fullName>
      <shortName>BD-8</shortName>
      <shortName>mBD-8</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 8</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin-related peptide</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Defr1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Defb8</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Mamm. Genome" volume="13" first="445" last="451">
      <title>Identification and characterisation a novel murine beta defensin related gene.</title>
      <authorList>
        <person name="Morrison G.M."/>
        <person name="Rolfe M."/>
        <person name="Kilanowski F.M."/>
        <person name="Cross S.H."/>
        <person name="Dorin J.R."/>
      </authorList>
      <dbReference type="PubMed" id="12226710"/>
      <dbReference type="DOI" id="10.1007/s00335-002-3014-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2002" name="Mamm. Genome" volume="13" first="603" last="603">
      <authorList>
        <person name="Morrison G.M."/>
        <person name="Rolfe M."/>
        <person name="Kilanowski F.M."/>
        <person name="Cross S.H."/>
        <person name="Dorin J.R."/>
      </authorList>
      <dbReference type="DOI" id="10.1007/s00335-002-0016-2"/>
    </citation>
    <scope>ERRATUM OF PUBMED:12226710</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2001-07" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning and characterization of mBD-7 and mBD-8, two novel mouse beta-defensins.</title>
      <authorList>
        <person name="Conejo-Garcia J.-R."/>
        <person name="Nehls M.C."/>
        <person name="Wattler S."/>
        <person name="Bals R."/>
        <person name="Heitland A."/>
        <person name="Kluever E."/>
        <person name="Liepke C."/>
        <person name="Adermann K."/>
        <person name="Forssmann W.-G."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <source>
      <tissue>Lung</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2001" name="Protein Sci." volume="10" first="2470" last="2479">
      <title>Structure determination of human and murine beta-defensins reveals structural conservation in the absence of significant sequence similarity.</title>
      <authorList>
        <person name="Bauer F."/>
        <person name="Schweimer K."/>
        <person name="Kluever E."/>
        <person name="Conejo-Garcia J.-R."/>
        <person name="Forssmann W.-G."/>
        <person name="Roesch P."/>
        <person name="Adermann K."/>
        <person name="Sticht H."/>
      </authorList>
      <dbReference type="PubMed" id="11714914"/>
      <dbReference type="DOI" id="10.1110/ps.24401"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 26-60</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text>A synthetic peptide displays antimicrobial activities against S.aureus, P.aeruginosa, E.coli and B.cepacia. The antimicrobial activity against S.aureus, E.coli and B.cepacia is reduced in raised concentration of NaCl, but its action against P.aeruginosa is independent of NaCl concentration.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Most highly expressed in testis and heart.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AJ344114">
    <property type="protein sequence ID" value="CAC86998.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AJ300674">
    <property type="protein sequence ID" value="CAC44635.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AJ300673">
    <property type="protein sequence ID" value="CAC44634.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_694748.3">
    <property type="nucleotide sequence ID" value="NM_153108.4"/>
  </dbReference>
  <dbReference type="PDB" id="1E4R">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=26-60"/>
  </dbReference>
  <dbReference type="PDBsum" id="1E4R"/>
  <dbReference type="AlphaFoldDB" id="Q91V82"/>
  <dbReference type="SMR" id="Q91V82"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000033854"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000033854"/>
  <dbReference type="DNASU" id="244334"/>
  <dbReference type="GeneID" id="244334"/>
  <dbReference type="KEGG" id="mmu:244334"/>
  <dbReference type="AGR" id="MGI:2654206"/>
  <dbReference type="CTD" id="244334"/>
  <dbReference type="MGI" id="MGI:2654206">
    <property type="gene designation" value="Defb8"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502SYUI">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="Q91V82"/>
  <dbReference type="OrthoDB" id="4840034at2759"/>
  <dbReference type="BioGRID-ORCS" id="244334">
    <property type="hits" value="1 hit in 75 CRISPR screens"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="Q91V82"/>
  <dbReference type="PRO" id="PR:Q91V82"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="RNAct" id="Q91V82">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.360.10:FF:000001">
    <property type="entry name" value="Beta-defensin 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515:SF12">
    <property type="entry name" value="BETA-DEFENSIN 3-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000006935">
    <location>
      <begin position="23"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006936" description="Beta-defensin 8">
    <location>
      <begin position="26"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="31"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="38"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="42"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; CAC86998." evidence="4" ref="1">
    <original>E</original>
    <variation>D</variation>
    <location>
      <position position="27"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; CAC86998." evidence="4" ref="1">
    <original>SC</original>
    <variation>TY</variation>
    <location>
      <begin position="30"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="31"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="37"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="46"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="54"/>
      <end position="60"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="11714914"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0007829" key="5">
    <source>
      <dbReference type="PDB" id="1E4R"/>
    </source>
  </evidence>
  <sequence length="60" mass="6760" checksum="7213024CF909A59B" modified="2001-12-01" version="1" precursor="true">MRIHYLLFTFLLVLLSPLAAFSQKINEPVSCIRNGGICQYRCIGLRHKIGTCGSPFKCCK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>