<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-04-08" modified="2024-10-02" version="94" xmlns="http://uniprot.org/uniprot">
  <accession>Q6MZN7</accession>
  <accession>Q04490</accession>
  <name>HCP5_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>HLA class I histocompatibility antigen protein P5</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>HLA complex protein P5</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Protein P5-1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">HCP5</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Immunogenetics" volume="38" first="47" last="53">
      <title>A novel coding sequence belonging to a new multicopy gene family mapping within the human MHC class I region.</title>
      <authorList>
        <person name="Vernet C."/>
        <person name="Ribouchon M.-T."/>
        <person name="Chimini G."/>
        <person name="Jouanolle A.-M."/>
        <person name="Sidibe I."/>
        <person name="Pontarotti P."/>
      </authorList>
      <dbReference type="PubMed" id="8462994"/>
      <dbReference type="DOI" id="10.1007/bf00216390"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Spleen</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Nat. Genet." volume="36" first="40" last="45">
      <title>Complete sequencing and characterization of 21,243 full-length human cDNAs.</title>
      <authorList>
        <person name="Ota T."/>
        <person name="Suzuki Y."/>
        <person name="Nishikawa T."/>
        <person name="Otsuki T."/>
        <person name="Sugiyama T."/>
        <person name="Irie R."/>
        <person name="Wakamatsu A."/>
        <person name="Hayashi K."/>
        <person name="Sato H."/>
        <person name="Nagai K."/>
        <person name="Kimura K."/>
        <person name="Makita H."/>
        <person name="Sekine M."/>
        <person name="Obayashi M."/>
        <person name="Nishi T."/>
        <person name="Shibahara T."/>
        <person name="Tanaka T."/>
        <person name="Ishii S."/>
        <person name="Yamamoto J."/>
        <person name="Saito K."/>
        <person name="Kawai Y."/>
        <person name="Isono Y."/>
        <person name="Nakamura Y."/>
        <person name="Nagahari K."/>
        <person name="Murakami K."/>
        <person name="Yasuda T."/>
        <person name="Iwayanagi T."/>
        <person name="Wagatsuma M."/>
        <person name="Shiratori A."/>
        <person name="Sudo H."/>
        <person name="Hosoiri T."/>
        <person name="Kaku Y."/>
        <person name="Kodaira H."/>
        <person name="Kondo H."/>
        <person name="Sugawara M."/>
        <person name="Takahashi M."/>
        <person name="Kanda K."/>
        <person name="Yokoi T."/>
        <person name="Furuya T."/>
        <person name="Kikkawa E."/>
        <person name="Omura Y."/>
        <person name="Abe K."/>
        <person name="Kamihara K."/>
        <person name="Katsuta N."/>
        <person name="Sato K."/>
        <person name="Tanikawa M."/>
        <person name="Yamazaki M."/>
        <person name="Ninomiya K."/>
        <person name="Ishibashi T."/>
        <person name="Yamashita H."/>
        <person name="Murakawa K."/>
        <person name="Fujimori K."/>
        <person name="Tanai H."/>
        <person name="Kimata M."/>
        <person name="Watanabe M."/>
        <person name="Hiraoka S."/>
        <person name="Chiba Y."/>
        <person name="Ishida S."/>
        <person name="Ono Y."/>
        <person name="Takiguchi S."/>
        <person name="Watanabe S."/>
        <person name="Yosida M."/>
        <person name="Hotuta T."/>
        <person name="Kusano J."/>
        <person name="Kanehori K."/>
        <person name="Takahashi-Fujii A."/>
        <person name="Hara H."/>
        <person name="Tanase T.-O."/>
        <person name="Nomura Y."/>
        <person name="Togiya S."/>
        <person name="Komai F."/>
        <person name="Hara R."/>
        <person name="Takeuchi K."/>
        <person name="Arita M."/>
        <person name="Imose N."/>
        <person name="Musashino K."/>
        <person name="Yuuki H."/>
        <person name="Oshima A."/>
        <person name="Sasaki N."/>
        <person name="Aotsuka S."/>
        <person name="Yoshikawa Y."/>
        <person name="Matsunawa H."/>
        <person name="Ichihara T."/>
        <person name="Shiohata N."/>
        <person name="Sano S."/>
        <person name="Moriya S."/>
        <person name="Momiyama H."/>
        <person name="Satoh N."/>
        <person name="Takami S."/>
        <person name="Terashima Y."/>
        <person name="Suzuki O."/>
        <person name="Nakagawa S."/>
        <person name="Senoh A."/>
        <person name="Mizoguchi H."/>
        <person name="Goto Y."/>
        <person name="Shimizu F."/>
        <person name="Wakebe H."/>
        <person name="Hishigaki H."/>
        <person name="Watanabe T."/>
        <person name="Sugiyama A."/>
        <person name="Takemoto M."/>
        <person name="Kawakami B."/>
        <person name="Yamazaki M."/>
        <person name="Watanabe K."/>
        <person name="Kumagai A."/>
        <person name="Itakura S."/>
        <person name="Fukuzumi Y."/>
        <person name="Fujimori Y."/>
        <person name="Komiyama M."/>
        <person name="Tashiro H."/>
        <person name="Tanigami A."/>
        <person name="Fujiwara T."/>
        <person name="Ono T."/>
        <person name="Yamada K."/>
        <person name="Fujii Y."/>
        <person name="Ozaki K."/>
        <person name="Hirao M."/>
        <person name="Ohmori Y."/>
        <person name="Kawabata A."/>
        <person name="Hikiji T."/>
        <person name="Kobatake N."/>
        <person name="Inagaki H."/>
        <person name="Ikema Y."/>
        <person name="Okamoto S."/>
        <person name="Okitani R."/>
        <person name="Kawakami T."/>
        <person name="Noguchi S."/>
        <person name="Itoh T."/>
        <person name="Shigeta K."/>
        <person name="Senba T."/>
        <person name="Matsumura K."/>
        <person name="Nakajima Y."/>
        <person name="Mizuno T."/>
        <person name="Morinaga M."/>
        <person name="Sasaki M."/>
        <person name="Togashi T."/>
        <person name="Oyama M."/>
        <person name="Hata H."/>
        <person name="Watanabe M."/>
        <person name="Komatsu T."/>
        <person name="Mizushima-Sugano J."/>
        <person name="Satoh T."/>
        <person name="Shirai Y."/>
        <person name="Takahashi Y."/>
        <person name="Nakagawa K."/>
        <person name="Okumura K."/>
        <person name="Nagase T."/>
        <person name="Nomura N."/>
        <person name="Kikuchi H."/>
        <person name="Masuho Y."/>
        <person name="Yamashita R."/>
        <person name="Nakai K."/>
        <person name="Yada T."/>
        <person name="Nakamura Y."/>
        <person name="Ohara O."/>
        <person name="Isogai T."/>
        <person name="Sugano S."/>
      </authorList>
      <dbReference type="PubMed" id="14702039"/>
      <dbReference type="DOI" id="10.1038/ng1285"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2007" name="BMC Genomics" volume="8" first="399" last="399">
      <title>The full-ORF clone resource of the German cDNA consortium.</title>
      <authorList>
        <person name="Bechtel S."/>
        <person name="Rosenfelder H."/>
        <person name="Duda A."/>
        <person name="Schmidt C.P."/>
        <person name="Ernst U."/>
        <person name="Wellenreuther R."/>
        <person name="Mehrle A."/>
        <person name="Schuster C."/>
        <person name="Bahr A."/>
        <person name="Bloecker H."/>
        <person name="Heubner D."/>
        <person name="Hoerlein A."/>
        <person name="Michel G."/>
        <person name="Wedler H."/>
        <person name="Koehrer K."/>
        <person name="Ottenwaelder B."/>
        <person name="Poustka A."/>
        <person name="Wiemann S."/>
        <person name="Schupp I."/>
      </authorList>
      <dbReference type="PubMed" id="17974005"/>
      <dbReference type="DOI" id="10.1186/1471-2164-8-399"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Rectum tumor</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="submission" date="2005-07" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Istrail S."/>
        <person name="Sutton G.G."/>
        <person name="Florea L."/>
        <person name="Halpern A.L."/>
        <person name="Mobarry C.M."/>
        <person name="Lippert R."/>
        <person name="Walenz B."/>
        <person name="Shatkay H."/>
        <person name="Dew I."/>
        <person name="Miller J.R."/>
        <person name="Flanigan M.J."/>
        <person name="Edwards N.J."/>
        <person name="Bolanos R."/>
        <person name="Fasulo D."/>
        <person name="Halldorsson B.V."/>
        <person name="Hannenhalli S."/>
        <person name="Turner R."/>
        <person name="Yooseph S."/>
        <person name="Lu F."/>
        <person name="Nusskern D.R."/>
        <person name="Shue B.C."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F."/>
        <person name="Delcher A.L."/>
        <person name="Huson D.H."/>
        <person name="Kravitz S.A."/>
        <person name="Mouchard L."/>
        <person name="Reinert K."/>
        <person name="Remington K.A."/>
        <person name="Clark A.G."/>
        <person name="Waterman M.S."/>
        <person name="Eichler E.E."/>
        <person name="Adams M.D."/>
        <person name="Hunkapiller M.W."/>
        <person name="Myers E.W."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1999" name="Immunogenetics" volume="49" first="404" last="412">
      <title>The P5 multicopy gene family in the MHC is related in sequence to human endogenous retroviruses HERV-L and HERV-16.</title>
      <authorList>
        <person name="Kulski J.K."/>
        <person name="Dawkins R.L."/>
      </authorList>
      <dbReference type="PubMed" id="10199916"/>
      <dbReference type="DOI" id="10.1007/s002510050513"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2007" name="Science" volume="317" first="944" last="947">
      <title>A whole-genome association study of major determinants for host control of HIV-1.</title>
      <authorList>
        <person name="Fellay J."/>
        <person name="Shianna K.V."/>
        <person name="Ge D."/>
        <person name="Colombo S."/>
        <person name="Ledergerber B."/>
        <person name="Weale M."/>
        <person name="Zhang K."/>
        <person name="Gumbs C."/>
        <person name="Castagna A."/>
        <person name="Cossarizza A."/>
        <person name="Cozzi-Lepri A."/>
        <person name="De Luca A."/>
        <person name="Easterbrook P."/>
        <person name="Francioli P."/>
        <person name="Mallal S."/>
        <person name="Martinez-Picado J."/>
        <person name="Miro J.M."/>
        <person name="Obel N."/>
        <person name="Smith J.P."/>
        <person name="Wyniger J."/>
        <person name="Descombes P."/>
        <person name="Antonarakis S.E."/>
        <person name="Letvin N.L."/>
        <person name="McMichael A.J."/>
        <person name="Haynes B.F."/>
        <person name="Telenti A."/>
        <person name="Goldstein D.B."/>
      </authorList>
      <dbReference type="PubMed" id="17641165"/>
      <dbReference type="DOI" id="10.1126/science.1143767"/>
    </citation>
    <scope>VARIANT GLY-112</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2008" name="AIDS" volume="22" first="541" last="544">
      <title>The role of protective HCP5 and HLA-C associated polymorphisms in the control of HIV-1 replication in a subset of elite suppressors.</title>
      <authorList>
        <person name="Han Y."/>
        <person name="Lai J."/>
        <person name="Barditch-Crovo P."/>
        <person name="Gallant J.E."/>
        <person name="Williams T.M."/>
        <person name="Siliciano R.F."/>
        <person name="Blankson J.N."/>
      </authorList>
      <dbReference type="PubMed" id="18301071"/>
      <dbReference type="DOI" id="10.1097/qad.0b013e3282f470e4"/>
    </citation>
    <scope>VARIANT GLY-112</scope>
  </reference>
  <comment type="tissue specificity">
    <text evidence="3">Expressed in lymphoid tissues; Detected in spleen as well as in B-cell lines, NK cell lines and activated lymphocytes.</text>
  </comment>
  <comment type="miscellaneous">
    <text>Variation Gly-112 is associated with low viral loads in untreated HIV patients. The level of circulating virus in the plasma of HIV patients (viral set point) varies among individuals during the nonsymptomatic phase preceding the progression to AIDS. This polymorphism explains 9.6% of the total variation in set point and is associated with the HLA-B*5701 allele, which has the strongest described protective impact on HIV disease progression. However, it is possible that HPC5 polymorphism, and not HLA-B*5701 allele, plays a direct role in the control in viremia in HIV patients.</text>
  </comment>
  <comment type="miscellaneous">
    <text>HCP5 is localized within the MHC class I region, but is not structurally related to MHC class I genes. HCP5 is related in sequence to human endogenous retroviruses HERV-L and HERV-16. It has sequence homology with retroviral Pol genes in HERV retroviral element; thus, it is itself a good candidate to interact with HIV-1, possibly through an antisense mechanism against retroviral transcript.</text>
  </comment>
  <comment type="sequence caution" evidence="4">
    <conflict type="frameshift">
      <sequence resource="EMBL-CDS" id="AAA59986" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="L06175">
    <property type="protein sequence ID" value="AAA59986.1"/>
    <property type="status" value="ALT_FRAME"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK290875">
    <property type="protein sequence ID" value="BAF83564.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BX640979">
    <property type="protein sequence ID" value="CAE45991.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH471081">
    <property type="protein sequence ID" value="EAX03398.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC106759">
    <property type="protein sequence ID" value="AAI06760.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q6MZN7"/>
  <dbReference type="IntAct" id="Q6MZN7">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="MINT" id="Q6MZN7"/>
  <dbReference type="iPTMnet" id="Q6MZN7"/>
  <dbReference type="PhosphoSitePlus" id="Q6MZN7"/>
  <dbReference type="BioMuta" id="HGNC:21659"/>
  <dbReference type="PeptideAtlas" id="Q6MZN7"/>
  <dbReference type="ProteomicsDB" id="66575"/>
  <dbReference type="AGR" id="HGNC:21659"/>
  <dbReference type="GeneCards" id="HCP5"/>
  <dbReference type="HGNC" id="HGNC:21659">
    <property type="gene designation" value="HCP5"/>
  </dbReference>
  <dbReference type="MIM" id="604676">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_Q6MZN7"/>
  <dbReference type="PharmGKB" id="PA134951826"/>
  <dbReference type="InParanoid" id="Q6MZN7"/>
  <dbReference type="PathwayCommons" id="Q6MZN7"/>
  <dbReference type="ChiTaRS" id="HCP5">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="Pharos" id="Q6MZN7">
    <property type="development level" value="Tdark"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q6MZN7"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="RNAct" id="Q6MZN7">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000329059" description="HLA class I histocompatibility antigen protein P5">
    <location>
      <begin position="1"/>
      <end position="132"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_042638" description="In dbSNP:rs17206855.">
    <original>W</original>
    <variation>R</variation>
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_042639" description="In dbSNP:rs2255221.">
    <original>W</original>
    <variation>C</variation>
    <location>
      <position position="82"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_042640" description="In dbSNP:rs2255223.">
    <original>G</original>
    <variation>E</variation>
    <location>
      <position position="93"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_042641" description="Associated with low viral load in HIV patients; dbSNP:rs2395029." evidence="1 2">
    <original>V</original>
    <variation>G</variation>
    <location>
      <position position="112"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_042642" description="In dbSNP:rs3130907.">
    <original>H</original>
    <variation>R</variation>
    <location>
      <position position="123"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAA59986." evidence="4" ref="1">
    <original>QG</original>
    <variation>K</variation>
    <location>
      <begin position="59"/>
      <end position="60"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="17641165"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="18301071"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="8462994"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="132" mass="14098" checksum="33BFBD82E613F773" modified="2004-07-05" version="1">MLLRMSEHRNEALGNYLEMRLKSSFLRGLGSWKSNPLRLGGWTILLTLTMGQGEPGGPQGDPWVPHELLLPSLCDSSHASSWGSGSITCAWRGGDSSSHPLVSGHILSNSPVAAVMCSSMGTHLSPFKGTLL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>