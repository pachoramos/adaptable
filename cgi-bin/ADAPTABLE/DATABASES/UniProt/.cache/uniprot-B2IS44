<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-04-14" modified="2024-11-27" version="83" xmlns="http://uniprot.org/uniprot">
  <accession>B2IS44</accession>
  <name>RS19_STRPS</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Small ribosomal subunit protein uS19</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">30S ribosomal protein S19</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rpsS</name>
    <name type="ordered locus">SPCG_0222</name>
  </gene>
  <organism>
    <name type="scientific">Streptococcus pneumoniae (strain CGSP14)</name>
    <dbReference type="NCBI Taxonomy" id="516950"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Streptococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="BMC Genomics" volume="10" first="158" last="158">
      <title>Genome evolution driven by host adaptations results in a more virulent and antimicrobial-resistant Streptococcus pneumoniae serotype 14.</title>
      <authorList>
        <person name="Ding F."/>
        <person name="Tang P."/>
        <person name="Hsu M.-H."/>
        <person name="Cui P."/>
        <person name="Hu S."/>
        <person name="Yu J."/>
        <person name="Chiu C.-H."/>
      </authorList>
      <dbReference type="PubMed" id="19361343"/>
      <dbReference type="DOI" id="10.1186/1471-2164-10-158"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CGSP14</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Protein S19 forms a complex with S13 that binds strongly to the 16S ribosomal RNA.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the universal ribosomal protein uS19 family.</text>
  </comment>
  <dbReference type="EMBL" id="CP001033">
    <property type="protein sequence ID" value="ACB89474.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000533766.1">
    <property type="nucleotide sequence ID" value="NC_010582.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B2IS44"/>
  <dbReference type="SMR" id="B2IS44"/>
  <dbReference type="GeneID" id="86986782"/>
  <dbReference type="KEGG" id="spw:SPCG_0222"/>
  <dbReference type="HOGENOM" id="CLU_144911_0_1_9"/>
  <dbReference type="GO" id="GO:0022627">
    <property type="term" value="C:cytosolic small ribosomal subunit"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019843">
    <property type="term" value="F:rRNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000028">
    <property type="term" value="P:ribosomal small subunit assembly"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006412">
    <property type="term" value="P:translation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.860.10:FF:000001">
    <property type="entry name" value="30S ribosomal protein S19"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00531">
    <property type="entry name" value="Ribosomal_uS19"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002222">
    <property type="entry name" value="Ribosomal_uS19"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005732">
    <property type="entry name" value="Ribosomal_uS19_bac-type"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020934">
    <property type="entry name" value="Ribosomal_uS19_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023575">
    <property type="entry name" value="Ribosomal_uS19_SF"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01050">
    <property type="entry name" value="rpsS_bact"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11880:SF8">
    <property type="entry name" value="37S RIBOSOMAL PROTEIN S19, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11880">
    <property type="entry name" value="RIBOSOMAL PROTEIN S19P FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00203">
    <property type="entry name" value="Ribosomal_S19"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF002144">
    <property type="entry name" value="Ribosomal_S19"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00975">
    <property type="entry name" value="RIBOSOMALS19"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54570">
    <property type="entry name" value="Ribosomal protein S19"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00323">
    <property type="entry name" value="RIBOSOMAL_S19"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0699">rRNA-binding</keyword>
  <feature type="chain" id="PRO_1000128044" description="Small ribosomal subunit protein uS19">
    <location>
      <begin position="1"/>
      <end position="93"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00531"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="93" mass="10750" checksum="1A7BF3EAB30A13ED" modified="2008-06-10" version="1">MGRSLKKGPFVDEHLMKKVEAQANDEKKKVIKTWSRRSTIFPSFIGYTIAVYDGRKHVPVYIQEDMVGHKLGEFAPTRTYKGHAADDKKTRRK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>