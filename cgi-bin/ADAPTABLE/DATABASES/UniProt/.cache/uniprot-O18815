<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1999-07-15" modified="2024-11-27" version="88" xmlns="http://uniprot.org/uniprot">
  <accession>O18815</accession>
  <name>DFBC7_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin C7</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>BBD(C7)</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Infect. Immun." volume="66" first="1045" last="1056">
      <title>Enteric beta-defensin: molecular cloning and characterization of a gene with inducible intestinal epithelial cell expression associated with Cryptosporidium parvum infection.</title>
      <authorList>
        <person name="Tarver A.P."/>
        <person name="Clark D.P."/>
        <person name="Diamond G."/>
        <person name="Russell J.P."/>
        <person name="Erdjument-Bromage H."/>
        <person name="Tempst P."/>
        <person name="Cohen K.S."/>
        <person name="Jones D.E."/>
        <person name="Sweeney R.W."/>
        <person name="Wines M."/>
        <person name="Hwang S."/>
        <person name="Bevins C.L."/>
      </authorList>
      <dbReference type="PubMed" id="9488394"/>
      <dbReference type="DOI" id="10.1128/iai.66.3.1045-1056.1998"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Small intestine</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Has bactericidal activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF016395">
    <property type="protein sequence ID" value="AAC48802.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O18815"/>
  <dbReference type="SMR" id="O18815"/>
  <dbReference type="InParanoid" id="O18815"/>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042056">
    <property type="term" value="F:chemoattractant activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060326">
    <property type="term" value="P:cell chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.360.10:FF:000001">
    <property type="entry name" value="Beta-defensin 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006080">
    <property type="entry name" value="Beta/alpha-defensin_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515:SF2">
    <property type="entry name" value="BETA-DEFENSIN 4A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00048">
    <property type="entry name" value="DEFSN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1" status="less than"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000006886" evidence="2">
    <location>
      <begin status="unknown"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006887" description="Beta-defensin C7">
    <location>
      <begin position="16"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="20"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="27"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="53" mass="5650" checksum="34659DF3A0489F4A" modified="1998-01-01" version="1" precursor="true" fragment="single">LALLFLVLSAGSGISGPLSCRRKGGICILIRCPGPMRQIGTCFGRPVKCCRSW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>