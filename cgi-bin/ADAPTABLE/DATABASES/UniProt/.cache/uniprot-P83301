<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-08-13" modified="2022-10-12" version="59" xmlns="http://uniprot.org/uniprot">
  <accession>P83301</accession>
  <name>U6VN_CONVE</name>
  <protein>
    <recommendedName>
      <fullName>Omega-conotoxin-like Vn2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Conotoxin Vn2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="4">Conotoxin-Vn</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Vn6A</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Conus ventricosus</name>
    <name type="common">Mediterranean cone</name>
    <dbReference type="NCBI Taxonomy" id="117992"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Caenogastropoda</taxon>
      <taxon>Neogastropoda</taxon>
      <taxon>Conoidea</taxon>
      <taxon>Conidae</taxon>
      <taxon>Conus</taxon>
      <taxon>Lautoconus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="J. Sep. Sci." volume="31" first="488" last="498">
      <title>Conus ventricosus venom peptides profiling by HPLC-MS: a new insight in the intraspecific variation.</title>
      <authorList>
        <person name="Romeo C."/>
        <person name="Di Francesco L."/>
        <person name="Oliverio M."/>
        <person name="Palazzo P."/>
        <person name="Massilia G.R."/>
        <person name="Ascenzi P."/>
        <person name="Polticelli F."/>
        <person name="Schinina M.E."/>
      </authorList>
      <dbReference type="PubMed" id="18266261"/>
      <dbReference type="DOI" id="10.1002/jssc.200700448"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PRO-33</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="744" last="751">
      <title>Recombinant expression and insecticidal properties of a Conus ventricosus conotoxin-GST fusion protein.</title>
      <authorList>
        <person name="Spiezia M.C."/>
        <person name="Chiarabelli C."/>
        <person name="Polticelli F."/>
      </authorList>
      <dbReference type="PubMed" id="22728460"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.06.008"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>TOXIC DOSE</scope>
    <scope>MUTAGENESIS OF ASP-2</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3">Omega-conotoxins act at presynaptic membranes, they bind and block voltage-gated calcium channels (Cav) (By similarity). Has strong insecticidal properties at a dose of only 100 pmol/g of body weight (when injected into the haemocoel of the wax moth G. mellonella larvae). Provoques tremor and uncontrolled movements in insect larvae, that are typical symptoms caused by neurotoxins (PubMed:22728460). On fish G.niger, intraperitoneal injection of the toxin causes full extension of the fins, change in posture, breathing difficulties (at 30 and 100 pmol/g body weight) and death (at 100 pmol/g body weight) (PubMed:22728460).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom duct.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="domain">
    <text>The cysteine framework is VI/VII (C-C-CC-C-C).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">LD(50) is more than 100 pmol/g body weight (at 24 hours) when injected into the haemocoel of the wax moth G. mellonella larvae.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P83301"/>
  <dbReference type="SMR" id="P83301"/>
  <dbReference type="TCDB" id="8.B.4.1.1">
    <property type="family name" value="the conotoxin t (conotoxin t) family"/>
  </dbReference>
  <dbReference type="ConoServer" id="1390">
    <property type="toxin name" value="Vn6A"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044231">
    <property type="term" value="C:host cell presynaptic membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005246">
    <property type="term" value="F:calcium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0108">Calcium channel impairing toxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0638">Presynaptic neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1218">Voltage-gated calcium channel impairing toxin</keyword>
  <feature type="peptide" id="PRO_0000044881" description="Omega-conotoxin-like Vn2">
    <location>
      <begin position="1"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="modified residue" description="Proline amide" evidence="2">
    <location>
      <position position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="3"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="10"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="19"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Increase in toxicity in insect larvae." evidence="3">
    <original>D</original>
    <variation>A</variation>
    <location>
      <position position="2"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="18266261"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22728460"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="18266261"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="22728460"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="33" mass="3546" checksum="662171EF59F5DA5A" modified="2002-08-13" version="1">EDCIAVGQLCVFWNIGRPCCSGLCVFACTVKLP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>