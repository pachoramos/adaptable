<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-02-08" modified="2023-02-22" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>P86630</accession>
  <name>BRK4_PHAJA</name>
  <protein>
    <recommendedName>
      <fullName>Des-Arg9-[Thr6]-bradykinin</fullName>
      <shortName evidence="3">Des-Arg-[Thr6]-bradykinin</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Phasmahyla jandaia</name>
    <name type="common">Jandaia leaf frog</name>
    <name type="synonym">Phyllomedusa jandaia</name>
    <dbReference type="NCBI Taxonomy" id="762504"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phasmahyla</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2011" name="Toxicon" volume="57" first="35" last="52">
      <title>Peptidomic dissection of the skin secretion of Phasmahyla jandaia (Bokermann and Sazima, 1978) (Anura, Hylidae, Phyllomedusinae).</title>
      <authorList>
        <person name="Rates B."/>
        <person name="Silva L.P."/>
        <person name="Ireno I.C."/>
        <person name="Leite F.S."/>
        <person name="Borges M.H."/>
        <person name="Bloch C. Jr."/>
        <person name="De Lima M.E."/>
        <person name="Pimenta A.M."/>
      </authorList>
      <dbReference type="PubMed" id="20932854"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2010.09.010"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="2">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Produces in vitro relaxation of rat arterial smooth muscle and constriction of intestinal smooth muscle (By similarity). May target bradykinin receptors (BDKRB).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="917.4" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the bradykinin-related peptide family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042311">
    <property type="term" value="P:vasodilation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0838">Vasoactive</keyword>
  <keyword id="KW-0840">Vasodilator</keyword>
  <feature type="peptide" id="PRO_0000404633" description="Des-Arg9-[Thr6]-bradykinin" evidence="2">
    <location>
      <begin position="1"/>
      <end position="8"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="20932854"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="20932854"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="8" mass="918" checksum="33D771A9C8677774" modified="2011-02-08" version="1">RPPGFTPF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>