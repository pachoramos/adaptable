<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1989-10-01" modified="2022-05-25" version="48" xmlns="http://uniprot.org/uniprot">
  <accession>P11975</accession>
  <name>NEUH_CARCA</name>
  <protein>
    <recommendedName>
      <fullName>Neuropeptide H</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Neuropeptide C</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Neuropeptide D</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Neuropeptide E</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Neuropeptide F</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Neuropeptide I</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Cardisoma carnifex</name>
    <name type="common">Blue land crab</name>
    <dbReference type="NCBI Taxonomy" id="6766"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Eucarida</taxon>
      <taxon>Decapoda</taxon>
      <taxon>Pleocyemata</taxon>
      <taxon>Brachyura</taxon>
      <taxon>Eubrachyura</taxon>
      <taxon>Grapsoidea</taxon>
      <taxon>Gecarcinidae</taxon>
      <taxon>Cardisoma</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1987" name="J. Neurochem." volume="49" first="574" last="583">
      <title>Amino acid sequences of neuropeptides in the sinus gland of the land crab Cardisoma carnifex: a novel neuropeptide proteolysis site.</title>
      <authorList>
        <person name="Newcomb R.W."/>
      </authorList>
      <dbReference type="PubMed" id="3298549"/>
      <dbReference type="DOI" id="10.1111/j.1471-4159.1987.tb02902.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Sinus gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Neuropeptide H is the precursor molecule for neuropeptides C, D, E, F and I, which arise by incomplete proteolysis of neuropeptide H.</text>
  </comment>
  <dbReference type="PIR" id="A28503">
    <property type="entry name" value="A28503"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P11975"/>
  <dbReference type="SMR" id="P11975"/>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005558">
    <property type="entry name" value="Crust_neurhormone_H"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03858">
    <property type="entry name" value="Crust_neuro_H"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <feature type="peptide" id="PRO_0000021803" description="Neuropeptide H">
    <location>
      <begin position="1"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000021804" description="Neuropeptide I">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000021805" description="Neuropeptide C">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000021806" description="Neuropeptide F">
    <location>
      <begin position="12"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000021807" description="Neuropeptide E">
    <location>
      <begin position="12"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000021808" description="Neuropeptide D">
    <location>
      <begin position="18"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="17"/>
      <end position="36"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <sequence length="36" mass="3643" checksum="16D9C69865679DCE" modified="1989-10-01" version="1">RSADGFGRMESLLTSLRGSAESPAALGEASAAHPLE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>