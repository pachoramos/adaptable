<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-04-26" modified="2024-01-24" version="24" xmlns="http://uniprot.org/uniprot">
  <accession>P84517</accession>
  <name>NOX_BOMMO</name>
  <protein>
    <recommendedName>
      <fullName>NADPH oxidoreductase</fullName>
      <ecNumber>1.6.3.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>BmNOX</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bombyx mori</name>
    <name type="common">Silk moth</name>
    <dbReference type="NCBI Taxonomy" id="7091"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Bombycoidea</taxon>
      <taxon>Bombycidae</taxon>
      <taxon>Bombycinae</taxon>
      <taxon>Bombyx</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2007" name="Biosci. Biotechnol. Biochem." volume="71" first="200" last="205">
      <title>Identification of a soluble NADPH oxidoreductase (BmNOX) with antiviral activities in the gut juice of Bombyx mori.</title>
      <authorList>
        <person name="Selot R."/>
        <person name="Kumar V."/>
        <person name="Shukla S."/>
        <person name="Chandrakuntal K."/>
        <person name="Brahmaraju M."/>
        <person name="Dandin S.B."/>
        <person name="Laloraya M."/>
        <person name="Kumar P.G."/>
      </authorList>
      <dbReference type="PubMed" id="17213661"/>
      <dbReference type="DOI" id="10.1271/bbb.60450"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <strain evidence="1">TX</strain>
      <tissue evidence="1">Gut juice</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">NADPH oxidoreductase. Has antiviral activity against BmNPV.</text>
  </comment>
  <comment type="mass spectrometry" mass="26559.8" method="MALDI" evidence="1"/>
  <comment type="miscellaneous">
    <text evidence="1">Expressed at high levels in BmNPV-resistant strains. Expressed at lower levels in moderately resistant and susceptible strains.</text>
  </comment>
  <dbReference type="EC" id="1.6.3.-"/>
  <dbReference type="STRING" id="7091.P84517"/>
  <dbReference type="InParanoid" id="P84517"/>
  <dbReference type="Proteomes" id="UP000005204">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016491">
    <property type="term" value="F:oxidoreductase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050688">
    <property type="term" value="P:regulation of defense response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0930">Antiviral protein</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0521">NADP</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000118007" description="NADPH oxidoreductase">
    <location>
      <begin position="1"/>
      <end position="10" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="2">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="17213661"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="17213661"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="10" mass="966" checksum="1A0CFAB682C87870" modified="2005-04-26" version="1" fragment="single">SMIGGVMSKG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>