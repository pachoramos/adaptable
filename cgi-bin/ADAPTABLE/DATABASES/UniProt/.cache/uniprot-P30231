<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-04-01" modified="2024-11-27" version="85" xmlns="http://uniprot.org/uniprot">
  <accession>P30231</accession>
  <name>DEF1_SINAL</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cysteine-rich antifungal protein 1</fullName>
      <shortName>AFP1</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>M1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Sinapis alba</name>
    <name type="common">White mustard</name>
    <name type="synonym">Brassica hirta</name>
    <dbReference type="NCBI Taxonomy" id="3728"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Brassiceae</taxon>
      <taxon>Sinapis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Int. J. Pept. Protein Res." volume="47" first="437" last="446">
      <title>Purification and mass spectrometry-based sequencing of yellow mustard (Sinapis alba L.) 6 kDa proteins. Identification as antifungal proteins.</title>
      <authorList>
        <person name="Neumann G.M."/>
        <person name="Condron R."/>
        <person name="Polya G.M."/>
      </authorList>
      <dbReference type="PubMed" id="8836771"/>
      <dbReference type="DOI" id="10.1111/j.1399-3011.1996.tb01094.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="FEBS Lett." volume="316" first="233" last="240">
      <title>A new family of basic cysteine-rich plant antifungal proteins from Brassicaceae species.</title>
      <authorList>
        <person name="Terras F.R.G."/>
        <person name="Torrekens S."/>
        <person name="van Leuven F."/>
        <person name="Osborn R.W."/>
        <person name="Vanderleyden J."/>
        <person name="Cammue B.P.A."/>
        <person name="Broekaert W.F."/>
      </authorList>
      <dbReference type="PubMed" id="8422949"/>
      <dbReference type="DOI" id="10.1016/0014-5793(93)81299-f"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 1-25</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Possesses antifungal activity sensitive to inorganic cations.</text>
  </comment>
  <comment type="subunit">
    <text>Forms oligomers in its native state.</text>
  </comment>
  <comment type="mass spectrometry" mass="5677.0" error="1.0" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="PIR" id="S28993">
    <property type="entry name" value="S28993"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P30231"/>
  <dbReference type="BMRB" id="P30231"/>
  <dbReference type="SMR" id="P30231"/>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000003">
    <property type="entry name" value="Defensin-like protein 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008176">
    <property type="entry name" value="Defensin_plant"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00304">
    <property type="entry name" value="Gamma-thionin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00940">
    <property type="entry name" value="GAMMA_THIONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <feature type="chain" id="PRO_0000074246" description="Defensin-like protein 1">
    <location>
      <begin position="1"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="2">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="15"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="21"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="25"/>
      <end position="47"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="8422949"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="8836771"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="51" mass="5695" checksum="770990E72DD1C469" modified="1996-10-01" version="3">QKLCERPSGTWSGVCGNNNACKNQCINLEKARHGSCNYVFPAHKCICYFPC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>