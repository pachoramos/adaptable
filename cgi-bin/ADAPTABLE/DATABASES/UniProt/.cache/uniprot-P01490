<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2023-02-22" version="91" xmlns="http://uniprot.org/uniprot">
  <accession>P01490</accession>
  <name>SCXA_MESEU</name>
  <protein>
    <recommendedName>
      <fullName>Alpha-toxin BeM10</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Neurotoxin M10</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Mesobuthus eupeus</name>
    <name type="common">Lesser Asian scorpion</name>
    <name type="synonym">Buthus eupeus</name>
    <dbReference type="NCBI Taxonomy" id="34648"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Mesobuthus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1979" name="Toxicon 17 Suppl." volume="1" first="60" last="60">
      <title>Studies of the toxins from Buthus eupeus scorpion venom.</title>
      <authorList>
        <person name="Grishin E.V."/>
        <person name="Soldatov N.M."/>
        <person name="Soldatova L.N."/>
        <person name="Ovchinnikov Y.A."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1980" name="Bioorg. Khim." volume="6" first="714" last="723">
      <title>The amino acid sequence of neurotoxin M10 from the venom of the Central Asian scorpion Buthus eupeus.</title>
      <authorList>
        <person name="Grishin E.V."/>
        <person name="Soldatova L.N."/>
        <person name="Shakhparonov M.I."/>
        <person name="Kazakov V.K."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1985" name="Bioorg. Khim." volume="11" first="1445" last="1456">
      <title>Neurotoxins from the venom of the Central Asian scorpion Buthus eupeus.</title>
      <authorList>
        <person name="Volkova T.M."/>
        <person name="Garsia A.F."/>
        <person name="Telezhinskaia I.N."/>
        <person name="Potapenko N.A."/>
        <person name="Grishin E.V."/>
      </authorList>
      <dbReference type="PubMed" id="4091860"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Alpha toxins bind voltage-independently at site-3 of sodium channels (Nav) and inhibit the inactivation of the activated channels, thereby blocking neuronal transmission. Has paralytic activity in mice.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="2">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Alpha subfamily.</text>
  </comment>
  <dbReference type="PIR" id="JT0019">
    <property type="entry name" value="NTSR0E"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01490"/>
  <dbReference type="SMR" id="P01490"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00284">
    <property type="entry name" value="TOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000066730" description="Alpha-toxin BeM10">
    <location>
      <begin position="1"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="2"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="12"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="16"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="22"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="26"/>
      <end position="47"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="65" mass="7373" checksum="E79FFE5DEE6CB694" modified="1986-07-21" version="1">VRDGYIADDKDCAYFCGRNAYCDEECKKGAESGKCWYAGQYGNACWCYKLPDWVPIKQKVSGKCN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>