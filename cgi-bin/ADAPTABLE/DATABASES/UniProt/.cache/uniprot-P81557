<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-03-15" modified="2023-09-13" version="25" xmlns="http://uniprot.org/uniprot">
  <accession>P81557</accession>
  <name>MUEP_SCAVE</name>
  <protein>
    <recommendedName>
      <fullName>Mucus envelope protein</fullName>
    </recommendedName>
  </protein>
  <organism evidence="2">
    <name type="scientific">Scarus vetula</name>
    <name type="common">Queen parrotfish</name>
    <dbReference type="NCBI Taxonomy" id="84543"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Actinopterygii</taxon>
      <taxon>Neopterygii</taxon>
      <taxon>Teleostei</taxon>
      <taxon>Neoteleostei</taxon>
      <taxon>Acanthomorphata</taxon>
      <taxon>Eupercaria</taxon>
      <taxon>Labriformes</taxon>
      <taxon>Labridae</taxon>
      <taxon>Scarines</taxon>
      <taxon>Scarus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="J. Fish Biol." volume="54" first="1124" last="1127">
      <title>Biochemical characteristics and antibiotic properties of the mucus envelope of the queen parrotfish.</title>
      <authorList>
        <person name="Videler H."/>
        <person name="Geertjes G.J."/>
        <person name="Videler J.J."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>GLYCOSYLATION</scope>
    <source>
      <tissue evidence="2">Mucus cocoon</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Exhibits antibacterial activity. May play a role in protection against parasite settlement.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Produced by the opercular gland in the gill cavity and secreted as part of the mucus cocoon.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Glycosylated.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000096644" description="Mucus envelope protein">
    <location>
      <begin position="1"/>
      <end position="26" status="greater than"/>
    </location>
  </feature>
  <feature type="unsure residue" description="G or D" evidence="1">
    <location>
      <position position="6"/>
    </location>
  </feature>
  <feature type="unsure residue" description="R or Y" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="unsure residue" description="P or E" evidence="1">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <feature type="unsure residue" description="M or W" evidence="1">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <feature type="unsure residue" evidence="1">
    <location>
      <position position="22"/>
    </location>
  </feature>
  <feature type="unsure residue" evidence="1">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="unsure residue" description="R or K" evidence="1">
    <location>
      <position position="25"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="2">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="26" mass="3057" checksum="C2A633AD25A91E15" modified="2004-03-15" version="1" fragment="single">KCCKKGGPRKCPPEGMTXFYERIFRI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>