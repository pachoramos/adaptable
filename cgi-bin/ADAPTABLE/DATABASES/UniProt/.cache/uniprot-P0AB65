<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-11-08" modified="2024-11-27" version="130" xmlns="http://uniprot.org/uniprot">
  <accession>P0AB65</accession>
  <accession>P75877</accession>
  <accession>Q9R7Q1</accession>
  <name>ACYP_ECOLI</name>
  <protein>
    <recommendedName>
      <fullName evidence="1 3 4">Acylphosphatase</fullName>
      <ecNumber evidence="1 2">3.6.1.7</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1 5">Acylphosphate phosphohydrolase</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">yccX</name>
    <name type="ordered locus">b0968</name>
    <name type="ordered locus">JW5131</name>
  </gene>
  <organism>
    <name type="scientific">Escherichia coli (strain K12)</name>
    <dbReference type="NCBI Taxonomy" id="83333"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Escherichia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="DNA Res." volume="3" first="137" last="155">
      <title>A 718-kb DNA sequence of the Escherichia coli K-12 genome corresponding to the 12.7-28.0 min region on the linkage map.</title>
      <authorList>
        <person name="Oshima T."/>
        <person name="Aiba H."/>
        <person name="Baba T."/>
        <person name="Fujita K."/>
        <person name="Hayashi K."/>
        <person name="Honjo A."/>
        <person name="Ikemoto K."/>
        <person name="Inada T."/>
        <person name="Itoh T."/>
        <person name="Kajihara M."/>
        <person name="Kanai K."/>
        <person name="Kashimoto K."/>
        <person name="Kimura S."/>
        <person name="Kitagawa M."/>
        <person name="Makino K."/>
        <person name="Masuda S."/>
        <person name="Miki T."/>
        <person name="Mizobuchi K."/>
        <person name="Mori H."/>
        <person name="Motomura K."/>
        <person name="Nakamura Y."/>
        <person name="Nashimoto H."/>
        <person name="Nishio Y."/>
        <person name="Saito N."/>
        <person name="Sampei G."/>
        <person name="Seki Y."/>
        <person name="Tagami H."/>
        <person name="Takemoto K."/>
        <person name="Wada C."/>
        <person name="Yamamoto Y."/>
        <person name="Yano M."/>
        <person name="Horiuchi T."/>
      </authorList>
      <dbReference type="PubMed" id="8905232"/>
      <dbReference type="DOI" id="10.1093/dnares/3.3.137"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>K12 / W3110 / ATCC 27325 / DSM 5911</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="Science" volume="277" first="1453" last="1462">
      <title>The complete genome sequence of Escherichia coli K-12.</title>
      <authorList>
        <person name="Blattner F.R."/>
        <person name="Plunkett G. III"/>
        <person name="Bloch C.A."/>
        <person name="Perna N.T."/>
        <person name="Burland V."/>
        <person name="Riley M."/>
        <person name="Collado-Vides J."/>
        <person name="Glasner J.D."/>
        <person name="Rode C.K."/>
        <person name="Mayhew G.F."/>
        <person name="Gregor J."/>
        <person name="Davis N.W."/>
        <person name="Kirkpatrick H.A."/>
        <person name="Goeden M.A."/>
        <person name="Rose D.J."/>
        <person name="Mau B."/>
        <person name="Shao Y."/>
      </authorList>
      <dbReference type="PubMed" id="9278503"/>
      <dbReference type="DOI" id="10.1126/science.277.5331.1453"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>K12 / MG1655 / ATCC 47076</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2006" name="Mol. Syst. Biol." volume="2" first="E1" last="E5">
      <title>Highly accurate genome sequences of Escherichia coli K-12 strains MG1655 and W3110.</title>
      <authorList>
        <person name="Hayashi K."/>
        <person name="Morooka N."/>
        <person name="Yamamoto Y."/>
        <person name="Fujita K."/>
        <person name="Isono K."/>
        <person name="Choi S."/>
        <person name="Ohtsubo E."/>
        <person name="Baba T."/>
        <person name="Wanner B.L."/>
        <person name="Mori H."/>
        <person name="Horiuchi T."/>
      </authorList>
      <dbReference type="PubMed" id="16738553"/>
      <dbReference type="DOI" id="10.1038/msb4100049"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>K12 / W3110 / ATCC 27325 / DSM 5911</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2006" name="FEBS Lett." volume="580" first="6763" last="6768">
      <title>The intrachain disulfide bridge is responsible of the unusual stability properties of novel acylphosphatase from Escherichia coli.</title>
      <authorList>
        <person name="Ramazzotti M."/>
        <person name="Parrini C."/>
        <person name="Stefani M."/>
        <person name="Manao G."/>
        <person name="Degl'Innocenti D."/>
      </authorList>
      <dbReference type="PubMed" id="17134700"/>
      <dbReference type="DOI" id="10.1016/j.febslet.2006.11.033"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>MUTAGENESIS OF CYS-5</scope>
    <source>
      <strain>K12 / DH5-alpha</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2006" name="J. Biomol. NMR" volume="36" first="199" last="204">
      <title>NMR solution structure of the acylphosphatase from Escherichia coli.</title>
      <authorList>
        <person name="Pagano K."/>
        <person name="Ramazzotti M."/>
        <person name="Viglino P."/>
        <person name="Esposito G."/>
        <person name="Degl'Innocenti D."/>
        <person name="Taddei N."/>
        <person name="Corazza A."/>
      </authorList>
      <dbReference type="PubMed" id="17021943"/>
      <dbReference type="DOI" id="10.1007/s10858-006-9073-2"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <comment type="catalytic activity">
    <reaction evidence="1 2">
      <text>an acyl phosphate + H2O = a carboxylate + phosphate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:14965"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:29067"/>
      <dbReference type="ChEBI" id="CHEBI:43474"/>
      <dbReference type="ChEBI" id="CHEBI:59918"/>
      <dbReference type="EC" id="3.6.1.7"/>
    </reaction>
  </comment>
  <comment type="biophysicochemical properties">
    <kinetics>
      <KM evidence="2">0.54 mM for benzoylphosphate</KM>
    </kinetics>
    <phDependence>
      <text evidence="2">Optimum pH is 5.2-6.5.</text>
    </phDependence>
    <temperatureDependence>
      <text evidence="2">Thermostable. Retains 85-90 % of its activity after 3 hours of incubation at 90 degrees Celsius.</text>
    </temperatureDependence>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">Has a considerably reduced catalytic efficiency compared to other mesophilic acylphosphatases. Shows a considerable resistance against urea denaturation since the full enzymatic activity is maintained in the presence of urea concentrations approaching 6.0 M and that only a slight decreae of 10-15 % was observed with higher urea concentrations.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the acylphosphatase family.</text>
  </comment>
  <dbReference type="EC" id="3.6.1.7" evidence="1 2"/>
  <dbReference type="EMBL" id="U00096">
    <property type="protein sequence ID" value="AAC74054.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AP009048">
    <property type="protein sequence ID" value="BAA35733.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="G64837">
    <property type="entry name" value="G64837"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_415488.1">
    <property type="nucleotide sequence ID" value="NC_000913.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000048252.1">
    <property type="nucleotide sequence ID" value="NZ_SSZK01000002.1"/>
  </dbReference>
  <dbReference type="PDB" id="2GV1">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-92"/>
  </dbReference>
  <dbReference type="PDB" id="8BV9">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.55 A"/>
    <property type="chains" value="A=1-92"/>
  </dbReference>
  <dbReference type="PDBsum" id="2GV1"/>
  <dbReference type="PDBsum" id="8BV9"/>
  <dbReference type="AlphaFoldDB" id="P0AB65"/>
  <dbReference type="BMRB" id="P0AB65"/>
  <dbReference type="SMR" id="P0AB65"/>
  <dbReference type="BioGRID" id="4260041">
    <property type="interactions" value="15"/>
  </dbReference>
  <dbReference type="BioGRID" id="849681">
    <property type="interactions" value="3"/>
  </dbReference>
  <dbReference type="DIP" id="DIP-11502N"/>
  <dbReference type="IntAct" id="P0AB65">
    <property type="interactions" value="4"/>
  </dbReference>
  <dbReference type="STRING" id="511145.b0968"/>
  <dbReference type="jPOST" id="P0AB65"/>
  <dbReference type="PaxDb" id="511145-b0968"/>
  <dbReference type="EnsemblBacteria" id="AAC74054">
    <property type="protein sequence ID" value="AAC74054"/>
    <property type="gene ID" value="b0968"/>
  </dbReference>
  <dbReference type="GeneID" id="75204059"/>
  <dbReference type="GeneID" id="945304"/>
  <dbReference type="KEGG" id="ecj:JW5131"/>
  <dbReference type="KEGG" id="eco:b0968"/>
  <dbReference type="KEGG" id="ecoc:C3026_05915"/>
  <dbReference type="PATRIC" id="fig|1411691.4.peg.1305"/>
  <dbReference type="EchoBASE" id="EB3490"/>
  <dbReference type="eggNOG" id="COG1254">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_141932_1_2_6"/>
  <dbReference type="InParanoid" id="P0AB65"/>
  <dbReference type="OMA" id="VGFRWSM"/>
  <dbReference type="OrthoDB" id="5295388at2"/>
  <dbReference type="PhylomeDB" id="P0AB65"/>
  <dbReference type="BioCyc" id="EcoCyc:G6502-MONOMER"/>
  <dbReference type="BioCyc" id="MetaCyc:G6502-MONOMER"/>
  <dbReference type="BRENDA" id="3.6.1.7">
    <property type="organism ID" value="2026"/>
  </dbReference>
  <dbReference type="SABIO-RK" id="P0AB65"/>
  <dbReference type="EvolutionaryTrace" id="P0AB65"/>
  <dbReference type="PRO" id="PR:P0AB65"/>
  <dbReference type="Proteomes" id="UP000000318">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000000625">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003998">
    <property type="term" value="F:acylphosphatase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="EcoCyc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009408">
    <property type="term" value="P:response to heat"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="EcoCyc"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.70.100:FF:000012">
    <property type="entry name" value="Acylphosphatase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.70.100">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01450">
    <property type="entry name" value="Acylphosphatase_entero"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020456">
    <property type="entry name" value="Acylphosphatase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001792">
    <property type="entry name" value="Acylphosphatase-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036046">
    <property type="entry name" value="Acylphosphatase-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR028627">
    <property type="entry name" value="Acylphosphatase_bac"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR017968">
    <property type="entry name" value="Acylphosphatase_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47268">
    <property type="entry name" value="ACYLPHOSPHATASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47268:SF4">
    <property type="entry name" value="ACYLPHOSPHATASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00708">
    <property type="entry name" value="Acylphosphatase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00112">
    <property type="entry name" value="ACYLPHPHTASE"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54975">
    <property type="entry name" value="Acylphosphatase/BLUF domain-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00150">
    <property type="entry name" value="ACYLPHOSPHATASE_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00151">
    <property type="entry name" value="ACYLPHOSPHATASE_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51160">
    <property type="entry name" value="ACYLPHOSPHATASE_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000158553" description="Acylphosphatase">
    <location>
      <begin position="1"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="domain" description="Acylphosphatase-like" evidence="1">
    <location>
      <begin position="5"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="active site" evidence="1">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <feature type="active site" evidence="1">
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 2">
    <location>
      <begin position="5"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No change in secondary structure. Nearly identical enzymatic properties. Loss of activity in 8.0 M urea. Reduced thermodynamic activity. Inactivated after 3 hours of incubation at 90 degrees Celsius." evidence="2">
    <original>C</original>
    <variation>A</variation>
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="3"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="15"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="19"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="33"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="40"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="44"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="52"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="71"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="82"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="86"/>
      <end position="89"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01450"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="17134700"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="17021943"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="17134700"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="2GV1"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="8BV9"/>
    </source>
  </evidence>
  <sequence length="92" mass="10300" checksum="C39D3D145598D873" modified="2005-11-08" version="1">MSKVCIIAWVYGRVQGVGFRYTTQYEAKRLGLTGYAKNLDDGSVEVVACGEEGQVEKLMQWLKSGGPRSARVERVLSEPHHPSGELTDFRIR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>