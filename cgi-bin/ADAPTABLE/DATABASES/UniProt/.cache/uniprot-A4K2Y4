<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-29" modified="2024-11-27" version="50" xmlns="http://uniprot.org/uniprot">
  <accession>A4K2Y4</accession>
  <name>WFD12_CHLAE</name>
  <protein>
    <recommendedName>
      <fullName>WAP four-disulfide core domain protein 12</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">WFDC12</name>
  </gene>
  <organism>
    <name type="scientific">Chlorocebus aethiops</name>
    <name type="common">Green monkey</name>
    <name type="synonym">Cercopithecus aethiops</name>
    <dbReference type="NCBI Taxonomy" id="9534"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Chlorocebus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2007" name="Genome Res." volume="17" first="276" last="286">
      <title>Comparative sequence analyses reveal rapid and divergent evolutionary changes of the WFDC locus in the primate lineage.</title>
      <authorList>
        <consortium name="NISC comparative sequencing program"/>
        <person name="Hurle B."/>
        <person name="Swanson W."/>
        <person name="Green E.D."/>
      </authorList>
      <dbReference type="PubMed" id="17267810"/>
      <dbReference type="DOI" id="10.1101/gr.6004607"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Antibacterial protein. Putative acid-stable proteinase inhibitor (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EMBL" id="DP000048">
    <property type="protein sequence ID" value="ABO53019.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A4K2Y4"/>
  <dbReference type="SMR" id="A4K2Y4"/>
  <dbReference type="MEROPS" id="I17.003"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.75.10:FF:000005">
    <property type="entry name" value="WAP four-disulfide core domain protein 12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.75.10">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036645">
    <property type="entry name" value="Elafin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008197">
    <property type="entry name" value="WAP_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00095">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00003">
    <property type="entry name" value="4DISULPHCORE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00217">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57256">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51390">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000289647" description="WAP four-disulfide core domain protein 12">
    <location>
      <begin position="24"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="domain" description="WAP" evidence="3">
    <location>
      <begin position="27"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="4">
    <location>
      <begin position="80"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="34"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="41"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="49"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="55"/>
      <end position="70"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00722"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="4">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="111" mass="11676" checksum="5CCA783E817DE383" modified="2007-05-01" version="1" precursor="true">MGSSSFLVLMVSLTLVTLVAAEGVKGGIEKAGVCPADNVRCFKSDPPQCHTDQDCLGERKCCYLHCGFKCVIPVKKLEEGGNKDEDVSGPCPEPGWEAKSPGSSSTGCPQK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>