<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-10-07" modified="2022-05-25" version="9" xmlns="http://uniprot.org/uniprot">
  <accession>A0A125S9G3</accession>
  <name>CX6A_CONIM</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Conotoxin Im6.10</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3 6">Conopeptide im029</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Conus imperialis</name>
    <name type="common">Imperial cone</name>
    <dbReference type="NCBI Taxonomy" id="35631"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Caenogastropoda</taxon>
      <taxon>Neogastropoda</taxon>
      <taxon>Conoidea</taxon>
      <taxon>Conidae</taxon>
      <taxon>Conus</taxon>
      <taxon>Stephanoconus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2019" name="Mar. Drugs" volume="17">
      <title>Transcriptomic-proteomic correlation in the predation-evoked venom of the cone snail, Conus imperialis.</title>
      <authorList>
        <person name="Jin A.H."/>
        <person name="Dutertre S."/>
        <person name="Dutt M."/>
        <person name="Lavergne V."/>
        <person name="Jones A."/>
        <person name="Lewis R.J."/>
        <person name="Alewood P.F."/>
      </authorList>
      <dbReference type="PubMed" id="30893765"/>
      <dbReference type="DOI" id="10.3390/md17030177"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom duct</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="4">Probable neurotoxin.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom duct.</text>
  </comment>
  <comment type="domain">
    <text evidence="4">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="domain">
    <text evidence="4">The cysteine framework is VI/VII (C-C-CC-C-C).</text>
  </comment>
  <dbReference type="EMBL" id="KT377423">
    <property type="protein sequence ID" value="AME17687.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A125S9G3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000451009" evidence="4">
    <location>
      <begin position="20"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5007179698" description="Conotoxin Im6.10">
    <location>
      <begin position="48"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="49"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="52"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="57"/>
      <end position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="30893765"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="30893765"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="30893765"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="AME17687.1"/>
    </source>
  </evidence>
  <sequence length="74" mass="8066" checksum="E9129FA450409E54" modified="2016-04-13" version="1" precursor="true">MKTGMIICLLLIAFMDADGSPGDTLYSQKTADTDSGMKRFQKTFQKRRCVFCPKEPCCDGDQCMTAPGTGPFCG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>