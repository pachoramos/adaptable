<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-02-13" modified="2024-03-27" version="87" xmlns="http://uniprot.org/uniprot">
  <accession>Q9S0H7</accession>
  <name>MLPC_BORBU</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Lipoprotein MlpC</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="3" type="primary">mlpC</name>
    <name type="ordered locus">BB_S30</name>
  </gene>
  <organism>
    <name type="scientific">Borreliella burgdorferi (strain ATCC 35210 / DSM 4680 / CIP 102532 / B31)</name>
    <name type="common">Borrelia burgdorferi</name>
    <dbReference type="NCBI Taxonomy" id="224326"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Spirochaetota</taxon>
      <taxon>Spirochaetia</taxon>
      <taxon>Spirochaetales</taxon>
      <taxon>Borreliaceae</taxon>
      <taxon>Borreliella</taxon>
    </lineage>
  </organism>
  <geneLocation type="plasmid">
    <name>cp32-3</name>
  </geneLocation>
  <reference key="1">
    <citation type="journal article" date="1997" name="Nature" volume="390" first="580" last="586">
      <title>Genomic sequence of a Lyme disease spirochaete, Borrelia burgdorferi.</title>
      <authorList>
        <person name="Fraser C.M."/>
        <person name="Casjens S."/>
        <person name="Huang W.M."/>
        <person name="Sutton G.G."/>
        <person name="Clayton R.A."/>
        <person name="Lathigra R."/>
        <person name="White O."/>
        <person name="Ketchum K.A."/>
        <person name="Dodson R.J."/>
        <person name="Hickey E.K."/>
        <person name="Gwinn M.L."/>
        <person name="Dougherty B.A."/>
        <person name="Tomb J.-F."/>
        <person name="Fleischmann R.D."/>
        <person name="Richardson D.L."/>
        <person name="Peterson J.D."/>
        <person name="Kerlavage A.R."/>
        <person name="Quackenbush J."/>
        <person name="Salzberg S.L."/>
        <person name="Hanson M."/>
        <person name="van Vugt R."/>
        <person name="Palmer N."/>
        <person name="Adams M.D."/>
        <person name="Gocayne J.D."/>
        <person name="Weidman J.F."/>
        <person name="Utterback T.R."/>
        <person name="Watthey L."/>
        <person name="McDonald L.A."/>
        <person name="Artiach P."/>
        <person name="Bowman C."/>
        <person name="Garland S.A."/>
        <person name="Fujii C."/>
        <person name="Cotton M.D."/>
        <person name="Horst K."/>
        <person name="Roberts K.M."/>
        <person name="Hatch B."/>
        <person name="Smith H.O."/>
        <person name="Venter J.C."/>
      </authorList>
      <dbReference type="PubMed" id="9403685"/>
      <dbReference type="DOI" id="10.1038/37551"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 35210 / DSM 4680 / CIP 102532 / B31</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2000" name="Mol. Microbiol." volume="35" first="490" last="516">
      <title>A bacterial genome in flux: the twelve linear and nine circular extrachromosomal DNAs in an infectious isolate of the Lyme disease spirochete Borrelia burgdorferi.</title>
      <authorList>
        <person name="Casjens S."/>
        <person name="Palmer N."/>
        <person name="van Vugt R."/>
        <person name="Huang W.M."/>
        <person name="Stevenson B."/>
        <person name="Rosa P."/>
        <person name="Lathigra R."/>
        <person name="Sutton G.G."/>
        <person name="Peterson J.D."/>
        <person name="Dodson R.J."/>
        <person name="Haft D.H."/>
        <person name="Hickey E.K."/>
        <person name="Gwinn M.L."/>
        <person name="White O."/>
        <person name="Fraser C.M."/>
      </authorList>
      <dbReference type="PubMed" id="10672174"/>
      <dbReference type="DOI" id="10.1046/j.1365-2958.2000.01698.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 35210 / DSM 4680 / CIP 102532 / B31</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2000" name="Infect. Immun." volume="68" first="4992" last="5001">
      <title>Expression and immunological analysis of the plasmid-borne mlp genes of Borrelia burgdorferi strain B31.</title>
      <authorList>
        <person name="Porcella S.F."/>
        <person name="Fitzpatrick C.A."/>
        <person name="Bono J.L."/>
      </authorList>
      <dbReference type="PubMed" id="10948116"/>
      <dbReference type="DOI" id="10.1128/iai.68.9.4992-5001.2000"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>ANTIGENICITY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION AT 35 DEGREES CELSIUS</scope>
    <source>
      <strain>B31-4A</strain>
      <plasmid>cp32-3</plasmid>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 5">An outer membrane protein that may participate in pathogenesis. Some human Lyme disease patients have antibodies against this protein (PubMed:10948116). The Mlp proteins probably undergo intragenic recombination, generating new alleles (Probable).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Cell outer membrane</location>
      <topology evidence="5">Lipid-anchor</topology>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text evidence="2">Induced when grown at 35 degrees Celsius.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the Multicopy lipoprotein (Mlp) family.</text>
  </comment>
  <dbReference type="EMBL" id="AE001576">
    <property type="protein sequence ID" value="AAF07449.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_051233.1">
    <property type="nucleotide sequence ID" value="NC_000949.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_010883753.1">
    <property type="nucleotide sequence ID" value="NC_000949.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9S0H7"/>
  <dbReference type="SMR" id="Q9S0H7"/>
  <dbReference type="EnsemblBacteria" id="AAF07449">
    <property type="protein sequence ID" value="AAF07449"/>
    <property type="gene ID" value="BB_S30"/>
  </dbReference>
  <dbReference type="KEGG" id="bbu:BB_S30"/>
  <dbReference type="PATRIC" id="fig|224326.49.peg.71"/>
  <dbReference type="HOGENOM" id="CLU_134260_0_0_12"/>
  <dbReference type="OrthoDB" id="351076at2"/>
  <dbReference type="Proteomes" id="UP000001807">
    <property type="component" value="Plasmid cp32-3"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009279">
    <property type="term" value="C:cell outer membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004983">
    <property type="entry name" value="Mlp"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03304">
    <property type="entry name" value="Mlp"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0998">Cell outer membrane</keyword>
  <keyword id="KW-0449">Lipoprotein</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0564">Palmitate</keyword>
  <keyword id="KW-0614">Plasmid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="4">
    <location>
      <begin position="1"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5004332389" description="Lipoprotein MlpC" evidence="4">
    <location>
      <begin position="18"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="26"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="1">
    <location>
      <begin position="29"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="lipid moiety-binding region" description="N-palmitoyl cysteine" evidence="4">
    <location>
      <position position="18"/>
    </location>
  </feature>
  <feature type="lipid moiety-binding region" description="S-diacylglycerol cysteine" evidence="4">
    <location>
      <position position="18"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10948116"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="10948116"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="10948116"/>
    </source>
  </evidence>
  <sequence length="148" mass="16731" checksum="1D406F6BC310895C" modified="2000-05-01" version="1" precursor="true">MKIINILFCLFLLMLNGCNSNDNDTLKNNAQQTKRRGKRDLTQKETTQEKPKSKEELLREKLSDDQKTHLDWLKPALTGAGEFDKFLENDDDKIKSALDHIKTQLDSCNGDQAEQQKTTFKTVVTEFFKNGDIDNFATGAVSNCNNGG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>