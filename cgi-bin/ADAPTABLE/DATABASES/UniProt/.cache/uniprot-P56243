<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2022-05-25" version="46" xmlns="http://uniprot.org/uniprot">
  <accession>P56243</accession>
  <name>CR42_RANCA</name>
  <protein>
    <recommendedName>
      <fullName>Caerin-4.2</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ranoidea caerulea</name>
    <name type="common">Green tree frog</name>
    <name type="synonym">Litoria caerulea</name>
    <dbReference type="NCBI Taxonomy" id="30344"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="J. Chem. Res." volume="138" first="910" last="936">
      <title>Peptides from Australian frogs. The structures of the caerins from Litoria caerula.</title>
      <authorList>
        <person name="Stone D.J.M."/>
        <person name="Waugh R.J."/>
        <person name="Bowie J.H."/>
        <person name="Wallace J.C."/>
        <person name="Tyler M.J."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Parotoid gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Antibacterial peptide, that adopts an alpha helical conformation which can disrupt bacterial membranes. Each caerin displays a different antimicrobial specificity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin parotoid and/or rostral glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2340.0" method="FAB" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Caerin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P56243"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043753" description="Caerin-4.2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="23" mass="2343" checksum="83BFDD8516ADDC87" modified="1998-07-15" version="1">GLWQKIKSAAGDLASGIVEAIKS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>