<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-03-28" modified="2023-09-13" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>A0A023IWI6</accession>
  <name>BAMAT_AMAFU</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Beta-amanitin proprotein</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="5">Beta-amanitin</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Amanita fuliginea</name>
    <name type="common">East Asian brown death cap</name>
    <dbReference type="NCBI Taxonomy" id="67708"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Basidiomycota</taxon>
      <taxon>Agaricomycotina</taxon>
      <taxon>Agaricomycetes</taxon>
      <taxon>Agaricomycetidae</taxon>
      <taxon>Agaricales</taxon>
      <taxon>Pluteineae</taxon>
      <taxon>Amanitaceae</taxon>
      <taxon>Amanita</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2014" name="Toxicon" volume="83" first="59" last="68">
      <title>The molecular diversity of toxin gene families in lethal Amanita mushrooms.</title>
      <authorList>
        <person name="Li P."/>
        <person name="Deng W."/>
        <person name="Li T."/>
      </authorList>
      <dbReference type="PubMed" id="24613547"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2014.02.020"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2002" name="J. Toxicol. Clin. Toxicol." volume="40" first="715" last="757">
      <title>Treatment of amatoxin poisoning: 20-year retrospective analysis.</title>
      <authorList>
        <person name="Enjalbert F."/>
        <person name="Rapior S."/>
        <person name="Nouguier-Soule J."/>
        <person name="Guillon S."/>
        <person name="Amouroux N."/>
        <person name="Cabot C."/>
      </authorList>
      <dbReference type="PubMed" id="12475187"/>
      <dbReference type="DOI" id="10.1081/clt-120014646"/>
    </citation>
    <scope>REVIEW ON TOXICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="7">Toxin belonging to the bicyclic octapeptides amatoxins that acts by binding non-competitively to RNA polymerase II and greatly slowing the elongation of transcripts from target promoters (PubMed:24613547).</text>
  </comment>
  <comment type="PTM">
    <text evidence="1 7">Processed by the macrocyclase-peptidase enzyme POPB to yield a toxic cyclic decapeptide (PubMed:24613547). POPB first removes 10 residues from the N-terminus (By similarity). Conformational trapping of the remaining peptide forces the enzyme to release this intermediate rather than proceed to macrocyclization (By similarity). The enzyme rebinds the remaining peptide in a different conformation and catalyzes macrocyclization of the N-terminal 8 residues (By similarity).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="4">The typical symptoms of amatoxin poisoning are gastro-intestinal distress beginning 6-12 hours after ingestion, a remission phase lasting 12-24 hours, and progressive loss of liver function culminating in death within 3-5 days (PubMed:12475187). One of the few effective treatments is liver transplantation (PubMed:12475187).</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the MSDIN fungal toxin family.</text>
  </comment>
  <dbReference type="EMBL" id="KF552088">
    <property type="protein sequence ID" value="AHB18716.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A023IWI6"/>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027582">
    <property type="entry name" value="Amanitin/phalloidin"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR04309">
    <property type="entry name" value="amanitin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0883">Thioether bond</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="propeptide" id="PRO_0000443599" evidence="2">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000443600" description="Beta-amanitin" evidence="2">
    <location>
      <begin position="11"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000443601" evidence="2">
    <location>
      <begin position="19"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Ile-Pro)" evidence="2">
    <location>
      <begin position="11"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="cross-link" description="2'-cysteinyl-6'-hydroxytryptophan sulfoxide (Trp-Cys)" evidence="3">
    <location>
      <begin position="12"/>
      <end position="16"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A067SLB9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="A8W7M4"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P85421"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="12475187"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="24613547"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="24613547"/>
    </source>
  </evidence>
  <sequence length="33" mass="3475" checksum="7AEAE096A7C4C3CB" modified="2014-07-09" version="1" precursor="true">MSDINATRLPIWGIGCDPCVGDEVTALLTRGEA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>