<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-12-15" modified="2023-02-22" version="64" xmlns="http://uniprot.org/uniprot">
  <accession>P81467</accession>
  <name>DEF3_MESAU</name>
  <protein>
    <recommendedName>
      <fullName>Neutrophil defensin 3</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>HANP-3</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Mesocricetus auratus</name>
    <name type="common">Golden hamster</name>
    <dbReference type="NCBI Taxonomy" id="10036"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Cricetidae</taxon>
      <taxon>Cricetinae</taxon>
      <taxon>Mesocricetus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Infect. Immun." volume="64" first="4444" last="4449">
      <title>Isolation, antimicrobial activities, and primary structures of hamster neutrophil defensins.</title>
      <authorList>
        <person name="Mak P."/>
        <person name="Wojcik K."/>
        <person name="Thogersen I.B."/>
        <person name="Dubin A."/>
      </authorList>
      <dbReference type="PubMed" id="8890190"/>
      <dbReference type="DOI" id="10.1128/iai.64.11.4444-4449.1996"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <comment type="function">
    <text>Anti-fungal and bactericidal activity, greater against Gram-positive bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the alpha-defensin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P81467"/>
  <dbReference type="SMR" id="P81467"/>
  <dbReference type="Proteomes" id="UP000189706">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006081">
    <property type="entry name" value="Alpha-defensin_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006080">
    <property type="entry name" value="Beta/alpha-defensin_C"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00323">
    <property type="entry name" value="Defensin_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00048">
    <property type="entry name" value="DEFSN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00269">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044716" description="Neutrophil defensin 3">
    <location>
      <begin position="1"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="3"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="5"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="10"/>
      <end position="30"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="33" mass="3891" checksum="07DF803EDCE06BDA" modified="1998-12-15" version="1">VTCFCRRRGCASRERLIGYCRFGNTIYGLCCRR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>