<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-04-01" modified="2023-02-22" version="86" xmlns="http://uniprot.org/uniprot">
  <accession>P15968</accession>
  <name>T3C3_HOLCU</name>
  <protein>
    <recommendedName>
      <fullName>Mu-agatoxin-Hc1c</fullName>
      <shortName>Mu-AGTX-Hc1c</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Curtatoxin-3</fullName>
      <shortName evidence="3">CT-III</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Hololena curta</name>
    <name type="common">Funnel-web spider</name>
    <name type="synonym">Agelena curta</name>
    <dbReference type="NCBI Taxonomy" id="6910"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Agelenidae</taxon>
      <taxon>Hololena</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="J. Biol. Chem." volume="265" first="2054" last="2059">
      <title>Curtatoxins. Neurotoxic insecticidal polypeptides isolated from the funnel-web spider Hololena curta.</title>
      <authorList>
        <person name="Stapleton A."/>
        <person name="Blankenship D.T."/>
        <person name="Ackermann D.L."/>
        <person name="Chen T.-M."/>
        <person name="Gorder G.W."/>
        <person name="Manley G.D."/>
        <person name="Palfreyman M.G."/>
        <person name="Coutant J.E."/>
        <person name="Cardin A.D."/>
      </authorList>
      <dbReference type="PubMed" id="2298738"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(19)39939-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>AMIDATION AT SER-38</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Toxicon" volume="43" first="509" last="525">
      <title>Agatoxins: ion channel specific toxins from the American funnel web spider, Agelenopsis aperta.</title>
      <authorList>
        <person name="Adams M.E."/>
      </authorList>
      <dbReference type="PubMed" id="15066410"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2004.02.004"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Insecticidal neurotoxin that induces irreversible neuromuscular blockade in house crickets (A.domesticus). Modifies presynaptic voltage-gated sodium channels (Nav), causing them to open at the normal resting potential of the nerve. This leads to spontaneous release of neurotransmitter and repetitive action potentials in motor neurons.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="2">LD(50) is 4 mg/kg in house crickets (Acheta domesticus).</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the neurotoxin 07 (Beta/delta-agtx) family. 02 (aga-3) subfamily.</text>
  </comment>
  <dbReference type="PIR" id="C35030">
    <property type="entry name" value="C35030"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P15968"/>
  <dbReference type="SMR" id="P15968"/>
  <dbReference type="ArachnoServer" id="AS000296">
    <property type="toxin name" value="mu-agatoxin-Hc1c"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016328">
    <property type="entry name" value="Beta/delta-agatoxin_fam"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05980">
    <property type="entry name" value="Toxin_7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001882">
    <property type="entry name" value="Curtatoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57059">
    <property type="entry name" value="omega toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60015">
    <property type="entry name" value="MU_AGATOXIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="peptide" id="PRO_0000044957" description="Mu-agatoxin-Hc1c">
    <location>
      <begin position="1"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="modified residue" description="Serine amide" evidence="2">
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="3"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="10"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="18"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="26"/>
      <end position="32"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="2298738"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="2298738"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="2298738"/>
    </source>
  </evidence>
  <sequence length="38" mass="4245" checksum="6741A8F4DB20FB59" modified="1990-04-01" version="1">ADCVGDGQKCADWFGPYCCSGYYCSCRSMPYCRCRSDS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>