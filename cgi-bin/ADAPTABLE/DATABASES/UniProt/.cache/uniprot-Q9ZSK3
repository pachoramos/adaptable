<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-11-02" modified="2024-11-27" version="158" xmlns="http://uniprot.org/uniprot">
  <accession>Q9ZSK3</accession>
  <accession>B9DGG7</accession>
  <accession>Q94A13</accession>
  <accession>Q9FJE7</accession>
  <name>ADF4_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Actin-depolymerizing factor 4</fullName>
      <shortName>ADF-4</shortName>
      <shortName>AtADF4</shortName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">ADF4</name>
    <name type="ordered locus">At5g59890</name>
    <name type="ORF">MMN10.13</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Plant Mol. Biol." volume="45" first="517" last="527">
      <title>Molecular identification and characterization of the Arabidopsis AtADF1, AtADF5 and AtADF6 genes.</title>
      <authorList>
        <person name="Dong C.-H."/>
        <person name="Kost B."/>
        <person name="Xia G.-X."/>
        <person name="Chua N.-H."/>
      </authorList>
      <dbReference type="PubMed" id="11414611"/>
      <dbReference type="DOI" id="10.1023/a:1010687911374"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 1)</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="DNA Res." volume="5" first="297" last="308">
      <title>Structural analysis of Arabidopsis thaliana chromosome 5. VII. Sequence features of the regions of 1,013,767 bp covered by sixteen physically assigned P1 and TAC clones.</title>
      <authorList>
        <person name="Nakamura Y."/>
        <person name="Sato S."/>
        <person name="Asamizu E."/>
        <person name="Kaneko T."/>
        <person name="Kotani H."/>
        <person name="Miyajima N."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="9872454"/>
      <dbReference type="DOI" id="10.1093/dnares/5.5.297"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2003" name="Science" volume="302" first="842" last="846">
      <title>Empirical analysis of transcriptional activity in the Arabidopsis genome.</title>
      <authorList>
        <person name="Yamada K."/>
        <person name="Lim J."/>
        <person name="Dale J.M."/>
        <person name="Chen H."/>
        <person name="Shinn P."/>
        <person name="Palm C.J."/>
        <person name="Southwick A.M."/>
        <person name="Wu H.C."/>
        <person name="Kim C.J."/>
        <person name="Nguyen M."/>
        <person name="Pham P.K."/>
        <person name="Cheuk R.F."/>
        <person name="Karlin-Newmann G."/>
        <person name="Liu S.X."/>
        <person name="Lam B."/>
        <person name="Sakano H."/>
        <person name="Wu T."/>
        <person name="Yu G."/>
        <person name="Miranda M."/>
        <person name="Quach H.L."/>
        <person name="Tripp M."/>
        <person name="Chang C.H."/>
        <person name="Lee J.M."/>
        <person name="Toriumi M.J."/>
        <person name="Chan M.M."/>
        <person name="Tang C.C."/>
        <person name="Onodera C.S."/>
        <person name="Deng J.M."/>
        <person name="Akiyama K."/>
        <person name="Ansari Y."/>
        <person name="Arakawa T."/>
        <person name="Banh J."/>
        <person name="Banno F."/>
        <person name="Bowser L."/>
        <person name="Brooks S.Y."/>
        <person name="Carninci P."/>
        <person name="Chao Q."/>
        <person name="Choy N."/>
        <person name="Enju A."/>
        <person name="Goldsmith A.D."/>
        <person name="Gurjal M."/>
        <person name="Hansen N.F."/>
        <person name="Hayashizaki Y."/>
        <person name="Johnson-Hopson C."/>
        <person name="Hsuan V.W."/>
        <person name="Iida K."/>
        <person name="Karnes M."/>
        <person name="Khan S."/>
        <person name="Koesema E."/>
        <person name="Ishida J."/>
        <person name="Jiang P.X."/>
        <person name="Jones T."/>
        <person name="Kawai J."/>
        <person name="Kamiya A."/>
        <person name="Meyers C."/>
        <person name="Nakajima M."/>
        <person name="Narusaka M."/>
        <person name="Seki M."/>
        <person name="Sakurai T."/>
        <person name="Satou M."/>
        <person name="Tamse R."/>
        <person name="Vaysberg M."/>
        <person name="Wallender E.K."/>
        <person name="Wong C."/>
        <person name="Yamamura Y."/>
        <person name="Yuan S."/>
        <person name="Shinozaki K."/>
        <person name="Davis R.W."/>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
      </authorList>
      <dbReference type="PubMed" id="14593172"/>
      <dbReference type="DOI" id="10.1126/science.1088305"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORMS 1 AND 2)</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2009" name="DNA Res." volume="16" first="155" last="164">
      <title>Analysis of multiple occurrences of alternative splicing events in Arabidopsis thaliana using novel sequenced full-length cDNAs.</title>
      <authorList>
        <person name="Iida K."/>
        <person name="Fukami-Kobayashi K."/>
        <person name="Toyoda A."/>
        <person name="Sakaki Y."/>
        <person name="Kobayashi M."/>
        <person name="Seki M."/>
        <person name="Shinozaki K."/>
      </authorList>
      <dbReference type="PubMed" id="19423640"/>
      <dbReference type="DOI" id="10.1093/dnares/dsp009"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM 1)</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="submission" date="2002-03" db="EMBL/GenBank/DDBJ databases">
      <title>Full-length cDNA from Arabidopsis thaliana.</title>
      <authorList>
        <person name="Brover V.V."/>
        <person name="Troukhan M.E."/>
        <person name="Alexandrov N.A."/>
        <person name="Lu Y.-P."/>
        <person name="Flavell R.B."/>
        <person name="Feldmann K.A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM 1)</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2006" name="J. Plant Physiol." volume="163" first="69" last="79">
      <title>Comparative study of rice and Arabidopsis actin-depolymerizing factors gene families.</title>
      <authorList>
        <person name="Feng Y."/>
        <person name="Liu Q."/>
        <person name="Xue Q."/>
      </authorList>
      <dbReference type="PubMed" id="16360805"/>
      <dbReference type="DOI" id="10.1016/j.jplph.2005.01.015"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2009" name="Plant Cell" volume="21" first="2963" last="2979">
      <title>Actin-depolymerizing factor2-mediated actin dynamics are essential for root-knot nematode infection of Arabidopsis.</title>
      <authorList>
        <person name="Clement M."/>
        <person name="Ketelaar T."/>
        <person name="Rodiuc N."/>
        <person name="Banora M.Y."/>
        <person name="Smertenko A."/>
        <person name="Engler G."/>
        <person name="Abad P."/>
        <person name="Hussey P.J."/>
        <person name="de Almeida Engler J."/>
      </authorList>
      <dbReference type="PubMed" id="19794115"/>
      <dbReference type="DOI" id="10.1105/tpc.109.069104"/>
    </citation>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2009" name="Plant Physiol." volume="150" first="815" last="824">
      <title>Arabidopsis actin-depolymerizing factor AtADF4 mediates defense signal transduction triggered by the Pseudomonas syringae effector AvrPphB.</title>
      <authorList>
        <person name="Tian M."/>
        <person name="Chaudhry F."/>
        <person name="Ruzicka D.R."/>
        <person name="Meagher R.B."/>
        <person name="Staiger C.J."/>
        <person name="Day B."/>
      </authorList>
      <dbReference type="PubMed" id="19346440"/>
      <dbReference type="DOI" id="10.1104/pp.109.137604"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2011" name="Plant Cell" volume="23" first="3711" last="3726">
      <title>Arabidopsis actin depolymerizing factor4 modulates the stochastic dynamic behavior of actin filaments in the cortical array of epidermal cells.</title>
      <authorList>
        <person name="Henty J.L."/>
        <person name="Bledsoe S.W."/>
        <person name="Khurana P."/>
        <person name="Meagher R.B."/>
        <person name="Day B."/>
        <person name="Blanchoin L."/>
        <person name="Staiger C.J."/>
      </authorList>
      <dbReference type="PubMed" id="22010035"/>
      <dbReference type="DOI" id="10.1105/tpc.111.090670"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2012" name="PLoS Pathog." volume="8" first="E1003006" last="E1003006">
      <title>Arabidopsis Actin-Depolymerizing Factor-4 links pathogen perception, defense activation and transcription to cytoskeletal dynamics.</title>
      <authorList>
        <person name="Porter K."/>
        <person name="Shimono M."/>
        <person name="Tian M."/>
        <person name="Day B."/>
      </authorList>
      <dbReference type="PubMed" id="23144618"/>
      <dbReference type="DOI" id="10.1371/journal.ppat.1003006"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>PHOSPHORYLATION AT SER-6</scope>
    <scope>MUTAGENESIS OF SER-6</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2014" name="Plant Cell" volume="26" first="340" last="352">
      <title>ACTIN DEPOLYMERIZING FACTOR4 regulates actin dynamics during innate immune signaling in Arabidopsis.</title>
      <authorList>
        <person name="Henty-Ridilla J.L."/>
        <person name="Li J."/>
        <person name="Day B."/>
        <person name="Staiger C.J."/>
      </authorList>
      <dbReference type="PubMed" id="24464292"/>
      <dbReference type="DOI" id="10.1105/tpc.113.122499"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 4 5 6">Actin-depolymerizing protein. Severs actin filaments (F-actin) and binds to actin monomers (PubMed:19346440, PubMed:22010035). Contributes to the stochastic dynamic turnover of actin filaments (PubMed:22010035). Binds monomeric actin (G-actin) with a marked preference for the ADP-loaded form and inhibits the rate of nucleotide exchange on G-actin. Involved in resistance triggered by the effector AvrPphB of Pseudomonas syringae pv tomato (Pst). May modulate the AvrPphB-RPS5-mediated defense signal transduction pathway (PubMed:19346440). During AvrPphB-triggered resistance signaling pathway, involved in the control of MPK3 and MPK6 activation, via the coordinated regulation of actin cytoskeletal dynamics and RPS5 resistance gene transcription (PubMed:23144618). During innate immune response triggered by the bacterial elf26 peptide, the inhibition of ADF4 regulates actin dynamics in order to execute key events associated with pattern-triggered immunity (PTI), such as cell wall fortification and transcriptional activation of defense gene markers (PubMed:24464292).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Cytoplasm</location>
      <location evidence="2">Cytoskeleton</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>Q9ZSK3-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>Q9ZSK3-2</id>
      <name>2</name>
      <sequence type="described" ref="VSP_008903"/>
    </isoform>
  </comment>
  <comment type="induction">
    <text evidence="3">By the root-knot nematode Meloidogyne incognita.</text>
  </comment>
  <comment type="PTM">
    <text evidence="5">Phosphorylation at Ser-6 is required for resistance to Pseudomonas syringae pv tomato AvrPphB.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="2 4">Increased root length under light-grown conditions. Increased length of hypocotyls under dark-grown conditions. Altered architecture of the actin cytoskeleton in hypocotyl cells (PubMed:22010035). Compromised resistance in response to Pseudomonas syringae pv tomato expressing AvrPphB (PubMed:19346440).</text>
  </comment>
  <comment type="similarity">
    <text evidence="8">Belongs to the actin-binding proteins ADF family.</text>
  </comment>
  <dbReference type="EMBL" id="AF102822">
    <property type="protein sequence ID" value="AAD09110.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB015475">
    <property type="protein sequence ID" value="BAB08357.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002688">
    <property type="protein sequence ID" value="AED97247.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002688">
    <property type="protein sequence ID" value="AED97248.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002688">
    <property type="protein sequence ID" value="ANM70922.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY050460">
    <property type="protein sequence ID" value="AAK91473.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY090336">
    <property type="protein sequence ID" value="AAL90997.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY084757">
    <property type="protein sequence ID" value="AAM61326.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK317148">
    <property type="protein sequence ID" value="BAH19834.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001318843.1">
    <molecule id="Q9ZSK3-2"/>
    <property type="nucleotide sequence ID" value="NM_001345374.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_568916.2">
    <molecule id="Q9ZSK3-2"/>
    <property type="nucleotide sequence ID" value="NM_125382.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_851228.1">
    <molecule id="Q9ZSK3-1"/>
    <property type="nucleotide sequence ID" value="NM_180897.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9ZSK3"/>
  <dbReference type="SMR" id="Q9ZSK3"/>
  <dbReference type="BioGRID" id="21355">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="3702.Q9ZSK3"/>
  <dbReference type="iPTMnet" id="Q9ZSK3"/>
  <dbReference type="PaxDb" id="3702-AT5G59890.1"/>
  <dbReference type="ProteomicsDB" id="244693">
    <molecule id="Q9ZSK3-1"/>
  </dbReference>
  <dbReference type="EnsemblPlants" id="AT5G59890.1">
    <molecule id="Q9ZSK3-1"/>
    <property type="protein sequence ID" value="AT5G59890.1"/>
    <property type="gene ID" value="AT5G59890"/>
  </dbReference>
  <dbReference type="EnsemblPlants" id="AT5G59890.2">
    <molecule id="Q9ZSK3-2"/>
    <property type="protein sequence ID" value="AT5G59890.2"/>
    <property type="gene ID" value="AT5G59890"/>
  </dbReference>
  <dbReference type="EnsemblPlants" id="AT5G59890.3">
    <molecule id="Q9ZSK3-2"/>
    <property type="protein sequence ID" value="AT5G59890.3"/>
    <property type="gene ID" value="AT5G59890"/>
  </dbReference>
  <dbReference type="GeneID" id="836111"/>
  <dbReference type="Gramene" id="AT5G59890.1">
    <molecule id="Q9ZSK3-1"/>
    <property type="protein sequence ID" value="AT5G59890.1"/>
    <property type="gene ID" value="AT5G59890"/>
  </dbReference>
  <dbReference type="Gramene" id="AT5G59890.2">
    <molecule id="Q9ZSK3-2"/>
    <property type="protein sequence ID" value="AT5G59890.2"/>
    <property type="gene ID" value="AT5G59890"/>
  </dbReference>
  <dbReference type="Gramene" id="AT5G59890.3">
    <molecule id="Q9ZSK3-2"/>
    <property type="protein sequence ID" value="AT5G59890.3"/>
    <property type="gene ID" value="AT5G59890"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT5G59890"/>
  <dbReference type="Araport" id="AT5G59890"/>
  <dbReference type="TAIR" id="AT5G59890">
    <property type="gene designation" value="ADF4"/>
  </dbReference>
  <dbReference type="eggNOG" id="KOG1735">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_094004_2_2_1"/>
  <dbReference type="InParanoid" id="Q9ZSK3"/>
  <dbReference type="OMA" id="WSMIYAT"/>
  <dbReference type="OrthoDB" id="3380386at2759"/>
  <dbReference type="PhylomeDB" id="Q9ZSK3"/>
  <dbReference type="PRO" id="PR:Q9ZSK3"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 5"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q9ZSK3">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015629">
    <property type="term" value="C:actin cytoskeleton"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009536">
    <property type="term" value="C:plastid"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003779">
    <property type="term" value="F:actin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030042">
    <property type="term" value="P:actin filament depolymerization"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002758">
    <property type="term" value="P:innate immune response-activating signaling pathway"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="CDD" id="cd11286">
    <property type="entry name" value="ADF_cofilin_like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.40.20.10:FF:000025">
    <property type="entry name" value="Actin-depolymerizing factor 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.20.10">
    <property type="entry name" value="Severin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002108">
    <property type="entry name" value="ADF-H"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029006">
    <property type="entry name" value="ADF-H/Gelsolin-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR017904">
    <property type="entry name" value="ADF/Cofilin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11913:SF112">
    <property type="entry name" value="ACTIN-DEPOLYMERIZING FACTOR 1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11913">
    <property type="entry name" value="COFILIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00241">
    <property type="entry name" value="Cofilin_ADF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00102">
    <property type="entry name" value="ADF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55753">
    <property type="entry name" value="Actin depolymerizing proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51263">
    <property type="entry name" value="ADF_H"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0009">Actin-binding</keyword>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0206">Cytoskeleton</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000214926" description="Actin-depolymerizing factor 4">
    <location>
      <begin position="1"/>
      <end position="139"/>
    </location>
  </feature>
  <feature type="domain" description="ADF-H" evidence="1">
    <location>
      <begin position="5"/>
      <end position="139"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="5">
    <location>
      <position position="6"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_008903" description="In isoform 2." evidence="7">
    <location>
      <begin position="1"/>
      <end position="7"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of phosphorylation." evidence="5">
    <original>S</original>
    <variation>A</variation>
    <variation>D</variation>
    <location>
      <position position="6"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAD09110." evidence="8" ref="1">
    <original>I</original>
    <variation>R</variation>
    <location>
      <position position="117"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAD09110." evidence="8" ref="1">
    <original>L</original>
    <variation>W</variation>
    <location>
      <position position="134"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00599"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19346440"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="19794115"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="22010035"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="23144618"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="24464292"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="14593172"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <sequence length="139" mass="16034" checksum="57F95F557F42F863" modified="2001-11-02" version="2">MANAASGMAVHDDCKLRFLELKAKRTHRFIVYKIEEKQKQVIVEKVGEPILTYEDFAASLPADECRYAIYDFDFVTAENCQKSKIFFIAWCPDVAKVRSKMIYASSKDRFKRELDGIQVELQATDPTEMDLDVLKSRVN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>