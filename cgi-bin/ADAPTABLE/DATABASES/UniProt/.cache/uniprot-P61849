<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-06-07" modified="2024-10-02" version="109" xmlns="http://uniprot.org/uniprot">
  <accession>P61849</accession>
  <accession>P41494</accession>
  <accession>Q9VC91</accession>
  <name>NEMS_DROME</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Dromyosuppressin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">TDVDHVFLRFamide</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="6" type="primary">Ms</name>
    <name evidence="6" type="ORF">CG6440</name>
  </gene>
  <organism>
    <name type="scientific">Drosophila melanogaster</name>
    <name type="common">Fruit fly</name>
    <dbReference type="NCBI Taxonomy" id="7227"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Ephydroidea</taxon>
      <taxon>Drosophilidae</taxon>
      <taxon>Drosophila</taxon>
      <taxon>Sophophora</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Science" volume="287" first="2185" last="2195">
      <title>The genome sequence of Drosophila melanogaster.</title>
      <authorList>
        <person name="Adams M.D."/>
        <person name="Celniker S.E."/>
        <person name="Holt R.A."/>
        <person name="Evans C.A."/>
        <person name="Gocayne J.D."/>
        <person name="Amanatides P.G."/>
        <person name="Scherer S.E."/>
        <person name="Li P.W."/>
        <person name="Hoskins R.A."/>
        <person name="Galle R.F."/>
        <person name="George R.A."/>
        <person name="Lewis S.E."/>
        <person name="Richards S."/>
        <person name="Ashburner M."/>
        <person name="Henderson S.N."/>
        <person name="Sutton G.G."/>
        <person name="Wortman J.R."/>
        <person name="Yandell M.D."/>
        <person name="Zhang Q."/>
        <person name="Chen L.X."/>
        <person name="Brandon R.C."/>
        <person name="Rogers Y.-H.C."/>
        <person name="Blazej R.G."/>
        <person name="Champe M."/>
        <person name="Pfeiffer B.D."/>
        <person name="Wan K.H."/>
        <person name="Doyle C."/>
        <person name="Baxter E.G."/>
        <person name="Helt G."/>
        <person name="Nelson C.R."/>
        <person name="Miklos G.L.G."/>
        <person name="Abril J.F."/>
        <person name="Agbayani A."/>
        <person name="An H.-J."/>
        <person name="Andrews-Pfannkoch C."/>
        <person name="Baldwin D."/>
        <person name="Ballew R.M."/>
        <person name="Basu A."/>
        <person name="Baxendale J."/>
        <person name="Bayraktaroglu L."/>
        <person name="Beasley E.M."/>
        <person name="Beeson K.Y."/>
        <person name="Benos P.V."/>
        <person name="Berman B.P."/>
        <person name="Bhandari D."/>
        <person name="Bolshakov S."/>
        <person name="Borkova D."/>
        <person name="Botchan M.R."/>
        <person name="Bouck J."/>
        <person name="Brokstein P."/>
        <person name="Brottier P."/>
        <person name="Burtis K.C."/>
        <person name="Busam D.A."/>
        <person name="Butler H."/>
        <person name="Cadieu E."/>
        <person name="Center A."/>
        <person name="Chandra I."/>
        <person name="Cherry J.M."/>
        <person name="Cawley S."/>
        <person name="Dahlke C."/>
        <person name="Davenport L.B."/>
        <person name="Davies P."/>
        <person name="de Pablos B."/>
        <person name="Delcher A."/>
        <person name="Deng Z."/>
        <person name="Mays A.D."/>
        <person name="Dew I."/>
        <person name="Dietz S.M."/>
        <person name="Dodson K."/>
        <person name="Doup L.E."/>
        <person name="Downes M."/>
        <person name="Dugan-Rocha S."/>
        <person name="Dunkov B.C."/>
        <person name="Dunn P."/>
        <person name="Durbin K.J."/>
        <person name="Evangelista C.C."/>
        <person name="Ferraz C."/>
        <person name="Ferriera S."/>
        <person name="Fleischmann W."/>
        <person name="Fosler C."/>
        <person name="Gabrielian A.E."/>
        <person name="Garg N.S."/>
        <person name="Gelbart W.M."/>
        <person name="Glasser K."/>
        <person name="Glodek A."/>
        <person name="Gong F."/>
        <person name="Gorrell J.H."/>
        <person name="Gu Z."/>
        <person name="Guan P."/>
        <person name="Harris M."/>
        <person name="Harris N.L."/>
        <person name="Harvey D.A."/>
        <person name="Heiman T.J."/>
        <person name="Hernandez J.R."/>
        <person name="Houck J."/>
        <person name="Hostin D."/>
        <person name="Houston K.A."/>
        <person name="Howland T.J."/>
        <person name="Wei M.-H."/>
        <person name="Ibegwam C."/>
        <person name="Jalali M."/>
        <person name="Kalush F."/>
        <person name="Karpen G.H."/>
        <person name="Ke Z."/>
        <person name="Kennison J.A."/>
        <person name="Ketchum K.A."/>
        <person name="Kimmel B.E."/>
        <person name="Kodira C.D."/>
        <person name="Kraft C.L."/>
        <person name="Kravitz S."/>
        <person name="Kulp D."/>
        <person name="Lai Z."/>
        <person name="Lasko P."/>
        <person name="Lei Y."/>
        <person name="Levitsky A.A."/>
        <person name="Li J.H."/>
        <person name="Li Z."/>
        <person name="Liang Y."/>
        <person name="Lin X."/>
        <person name="Liu X."/>
        <person name="Mattei B."/>
        <person name="McIntosh T.C."/>
        <person name="McLeod M.P."/>
        <person name="McPherson D."/>
        <person name="Merkulov G."/>
        <person name="Milshina N.V."/>
        <person name="Mobarry C."/>
        <person name="Morris J."/>
        <person name="Moshrefi A."/>
        <person name="Mount S.M."/>
        <person name="Moy M."/>
        <person name="Murphy B."/>
        <person name="Murphy L."/>
        <person name="Muzny D.M."/>
        <person name="Nelson D.L."/>
        <person name="Nelson D.R."/>
        <person name="Nelson K.A."/>
        <person name="Nixon K."/>
        <person name="Nusskern D.R."/>
        <person name="Pacleb J.M."/>
        <person name="Palazzolo M."/>
        <person name="Pittman G.S."/>
        <person name="Pan S."/>
        <person name="Pollard J."/>
        <person name="Puri V."/>
        <person name="Reese M.G."/>
        <person name="Reinert K."/>
        <person name="Remington K."/>
        <person name="Saunders R.D.C."/>
        <person name="Scheeler F."/>
        <person name="Shen H."/>
        <person name="Shue B.C."/>
        <person name="Siden-Kiamos I."/>
        <person name="Simpson M."/>
        <person name="Skupski M.P."/>
        <person name="Smith T.J."/>
        <person name="Spier E."/>
        <person name="Spradling A.C."/>
        <person name="Stapleton M."/>
        <person name="Strong R."/>
        <person name="Sun E."/>
        <person name="Svirskas R."/>
        <person name="Tector C."/>
        <person name="Turner R."/>
        <person name="Venter E."/>
        <person name="Wang A.H."/>
        <person name="Wang X."/>
        <person name="Wang Z.-Y."/>
        <person name="Wassarman D.A."/>
        <person name="Weinstock G.M."/>
        <person name="Weissenbach J."/>
        <person name="Williams S.M."/>
        <person name="Woodage T."/>
        <person name="Worley K.C."/>
        <person name="Wu D."/>
        <person name="Yang S."/>
        <person name="Yao Q.A."/>
        <person name="Ye J."/>
        <person name="Yeh R.-F."/>
        <person name="Zaveri J.S."/>
        <person name="Zhan M."/>
        <person name="Zhang G."/>
        <person name="Zhao Q."/>
        <person name="Zheng L."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F.N."/>
        <person name="Zhong W."/>
        <person name="Zhou X."/>
        <person name="Zhu S.C."/>
        <person name="Zhu X."/>
        <person name="Smith H.O."/>
        <person name="Gibbs R.A."/>
        <person name="Myers E.W."/>
        <person name="Rubin G.M."/>
        <person name="Venter J.C."/>
      </authorList>
      <dbReference type="PubMed" id="10731132"/>
      <dbReference type="DOI" id="10.1126/science.287.5461.2185"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Berkeley</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2002" name="Genome Biol." volume="3" first="RESEARCH0083.1" last="RESEARCH0083.22">
      <title>Annotation of the Drosophila melanogaster euchromatic genome: a systematic review.</title>
      <authorList>
        <person name="Misra S."/>
        <person name="Crosby M.A."/>
        <person name="Mungall C.J."/>
        <person name="Matthews B.B."/>
        <person name="Campbell K.S."/>
        <person name="Hradecky P."/>
        <person name="Huang Y."/>
        <person name="Kaminker J.S."/>
        <person name="Millburn G.H."/>
        <person name="Prochnik S.E."/>
        <person name="Smith C.D."/>
        <person name="Tupy J.L."/>
        <person name="Whitfield E.J."/>
        <person name="Bayraktaroglu L."/>
        <person name="Berman B.P."/>
        <person name="Bettencourt B.R."/>
        <person name="Celniker S.E."/>
        <person name="de Grey A.D.N.J."/>
        <person name="Drysdale R.A."/>
        <person name="Harris N.L."/>
        <person name="Richter J."/>
        <person name="Russo S."/>
        <person name="Schroeder A.J."/>
        <person name="Shu S.Q."/>
        <person name="Stapleton M."/>
        <person name="Yamada C."/>
        <person name="Ashburner M."/>
        <person name="Gelbart W.M."/>
        <person name="Rubin G.M."/>
        <person name="Lewis S.E."/>
      </authorList>
      <dbReference type="PubMed" id="12537572"/>
      <dbReference type="DOI" id="10.1186/gb-2002-3-12-research0083"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>Berkeley</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Genome Biol." volume="3" first="RESEARCH0080.1" last="RESEARCH0080.8">
      <title>A Drosophila full-length cDNA resource.</title>
      <authorList>
        <person name="Stapleton M."/>
        <person name="Carlson J.W."/>
        <person name="Brokstein P."/>
        <person name="Yu C."/>
        <person name="Champe M."/>
        <person name="George R.A."/>
        <person name="Guarin H."/>
        <person name="Kronmiller B."/>
        <person name="Pacleb J.M."/>
        <person name="Park S."/>
        <person name="Wan K.H."/>
        <person name="Rubin G.M."/>
        <person name="Celniker S.E."/>
      </authorList>
      <dbReference type="PubMed" id="12537569"/>
      <dbReference type="DOI" id="10.1186/gb-2002-3-12-research0080"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Berkeley</strain>
      <tissue>Ovary</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1992" name="J. Mol. Neurosci." volume="3" first="213" last="218">
      <title>Isolation and structural characterization of Drosophila TDVDHVFLRFamide and FMRFamide-containing neural peptides.</title>
      <authorList>
        <person name="Nichols R."/>
      </authorList>
      <dbReference type="PubMed" id="1390001"/>
      <dbReference type="DOI" id="10.1007/bf03380141"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 87-96</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2002" name="J. Biol. Chem." volume="277" first="40368" last="40374">
      <title>Peptidomics of the larval Drosophila melanogaster central nervous system.</title>
      <authorList>
        <person name="Baggerman G."/>
        <person name="Cerstiaens A."/>
        <person name="De Loof A."/>
        <person name="Schoofs L."/>
      </authorList>
      <dbReference type="PubMed" id="12171930"/>
      <dbReference type="DOI" id="10.1074/jbc.m206257200"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 87-96</scope>
    <scope>AMIDATION AT PHE-96</scope>
    <source>
      <tissue>Larva</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Myoinhibiting neuropeptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the myosuppressin family.</text>
  </comment>
  <dbReference type="EMBL" id="AE014297">
    <property type="protein sequence ID" value="AAF56283.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY075325">
    <property type="protein sequence ID" value="AAL68192.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001262911.1">
    <property type="nucleotide sequence ID" value="NM_001275982.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001287503.1">
    <property type="nucleotide sequence ID" value="NM_001300574.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_536772.1">
    <property type="nucleotide sequence ID" value="NM_080511.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P61849"/>
  <dbReference type="STRING" id="7227.FBpp0312058"/>
  <dbReference type="PaxDb" id="7227-FBpp0083991"/>
  <dbReference type="DNASU" id="44324"/>
  <dbReference type="EnsemblMetazoa" id="FBtr0084607">
    <property type="protein sequence ID" value="FBpp0083991"/>
    <property type="gene ID" value="FBgn0011581"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="FBtr0334600">
    <property type="protein sequence ID" value="FBpp0306667"/>
    <property type="gene ID" value="FBgn0011581"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="FBtr0346341">
    <property type="protein sequence ID" value="FBpp0312058"/>
    <property type="gene ID" value="FBgn0011581"/>
  </dbReference>
  <dbReference type="GeneID" id="44324"/>
  <dbReference type="KEGG" id="dme:Dmel_CG6440"/>
  <dbReference type="UCSC" id="CG6440-RA">
    <property type="organism name" value="d. melanogaster"/>
  </dbReference>
  <dbReference type="AGR" id="FB:FBgn0011581"/>
  <dbReference type="CTD" id="4397"/>
  <dbReference type="FlyBase" id="FBgn0011581">
    <property type="gene designation" value="Ms"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="VectorBase:FBgn0011581"/>
  <dbReference type="eggNOG" id="ENOG502SDA3">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_177213_0_0_1"/>
  <dbReference type="InParanoid" id="P61849"/>
  <dbReference type="OMA" id="HIKKVCM"/>
  <dbReference type="OrthoDB" id="3677432at2759"/>
  <dbReference type="PhylomeDB" id="P61849"/>
  <dbReference type="BioGRID-ORCS" id="44324">
    <property type="hits" value="0 hits in 1 CRISPR screen"/>
  </dbReference>
  <dbReference type="GenomeRNAi" id="44324"/>
  <dbReference type="PRO" id="PR:P61849"/>
  <dbReference type="Proteomes" id="UP000000803">
    <property type="component" value="Chromosome 3R"/>
  </dbReference>
  <dbReference type="Bgee" id="FBgn0011581">
    <property type="expression patterns" value="Expressed in head capsule and 17 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P61849">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071855">
    <property type="term" value="F:neuropeptide receptor binding"/>
    <property type="evidence" value="ECO:0000353"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008344">
    <property type="term" value="P:adult locomotory behavior"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060457">
    <property type="term" value="P:negative regulation of digestive system process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045822">
    <property type="term" value="P:negative regulation of heart contraction"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010459">
    <property type="term" value="P:negative regulation of heart rate"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045986">
    <property type="term" value="P:negative regulation of smooth muscle contraction"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000021797">
    <location>
      <begin position="25"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000021798" description="Dromyosuppressin">
    <location>
      <begin position="87"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="2">
    <location>
      <position position="96"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12171930"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="12171930"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="1390001"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="FlyBase" id="FBgn0011581"/>
    </source>
  </evidence>
  <sequence length="100" mass="11070" checksum="177CBFA528D8BE58" modified="2004-06-07" version="1" precursor="true">MSFAQFFVACCLAIVLLAVSNTRAAVQGPPLCQSGIVEEMPPHIRKVCQALENSDQLTSALKSYINNEASALVANSDDLLKNYNKRTDVDHVFLRFGKRR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>