<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-11" modified="2024-11-27" version="91" xmlns="http://uniprot.org/uniprot">
  <accession>P66254</accession>
  <accession>Q99QT1</accession>
  <name>RL34_STAAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Large ribosomal subunit protein bL34</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">50S ribosomal protein L34</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rpmH</name>
    <name type="ordered locus">MW2632</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="similarity">
    <text evidence="1">Belongs to the bacterial ribosomal protein bL34 family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB96497.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000240855.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="PDB" id="8Y36">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.65 A"/>
    <property type="chains" value="2=2-44"/>
  </dbReference>
  <dbReference type="PDB" id="8Y37">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.53 A"/>
    <property type="chains" value="2=2-44"/>
  </dbReference>
  <dbReference type="PDB" id="8Y38">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.58 A"/>
    <property type="chains" value="2=2-44"/>
  </dbReference>
  <dbReference type="PDB" id="8Y39">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.60 A"/>
    <property type="chains" value="2=2-44"/>
  </dbReference>
  <dbReference type="PDBsum" id="8Y36"/>
  <dbReference type="PDBsum" id="8Y37"/>
  <dbReference type="PDBsum" id="8Y38"/>
  <dbReference type="PDBsum" id="8Y39"/>
  <dbReference type="AlphaFoldDB" id="P66254"/>
  <dbReference type="EMDB" id="EMD-38873"/>
  <dbReference type="EMDB" id="EMD-38874"/>
  <dbReference type="EMDB" id="EMD-38875"/>
  <dbReference type="EMDB" id="EMD-38876"/>
  <dbReference type="SMR" id="P66254"/>
  <dbReference type="GeneID" id="82529741"/>
  <dbReference type="KEGG" id="sam:MW2632"/>
  <dbReference type="HOGENOM" id="CLU_129938_2_0_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990904">
    <property type="term" value="C:ribonucleoprotein complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005840">
    <property type="term" value="C:ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006412">
    <property type="term" value="P:translation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.287.3980:FF:000001">
    <property type="entry name" value="Mitochondrial ribosomal protein L34"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.287.3980">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00391">
    <property type="entry name" value="Ribosomal_bL34"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000271">
    <property type="entry name" value="Ribosomal_bL34"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020939">
    <property type="entry name" value="Ribosomal_bL34_CS"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01030">
    <property type="entry name" value="rpmH_bact"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR14503:SF4">
    <property type="entry name" value="39S RIBOSOMAL PROTEIN L34, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR14503">
    <property type="entry name" value="MITOCHONDRIAL RIBOSOMAL PROTEIN 34 FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00468">
    <property type="entry name" value="Ribosomal_L34"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00784">
    <property type="entry name" value="RIBOSOMAL_L34"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <feature type="chain" id="PRO_0000187465" description="Large ribosomal subunit protein bL34">
    <location>
      <begin position="1"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="1"/>
      <end position="45"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00391"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="45" mass="5434" checksum="488B186AB2504E16" modified="2004-10-11" version="1">MVKRTYQPNKRKHSKVHGFRKRMSTKNGRKVLARRRRKGRKVLSA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>