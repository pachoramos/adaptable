<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-10-01" modified="2024-11-27" version="137" xmlns="http://uniprot.org/uniprot">
  <accession>Q10746</accession>
  <accession>Q10747</accession>
  <accession>Q5CZ55</accession>
  <accession>Q9EP62</accession>
  <name>CXCL3_RAT</name>
  <protein>
    <recommendedName>
      <fullName>C-X-C motif chemokine 3</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cytokine-induced neutrophil chemoattractant 2</fullName>
      <shortName>CINC-2</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Macrophage inflammatory protein 2-alpha/beta</fullName>
      <shortName>MIP2-alpha/beta</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Cxcl3</name>
    <name type="synonym">Cinc2</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Biochem. J." volume="301" first="545" last="550">
      <title>Identification of cytokine-induced neutrophil chemoattractants (CINC), rat GRO/CINC-2 alpha and CINC-2 beta, produced by granulation tissue in culture: purification, complete amino acid sequences and characterization.</title>
      <authorList>
        <person name="Nakagawa H."/>
        <person name="Komorita N."/>
        <person name="Shibata F."/>
        <person name="Ikesue A."/>
        <person name="Konishi K."/>
        <person name="Fujioka M."/>
        <person name="Kato H."/>
      </authorList>
      <dbReference type="PubMed" id="8043001"/>
      <dbReference type="DOI" id="10.1042/bj3010545"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 2)</scope>
    <scope>PROTEIN SEQUENCE OF 33-100 (ISOFORM 1)</scope>
    <scope>PROTEIN SEQUENCE OF 33-67 (ISOFORM 2)</scope>
    <scope>FUNCTION</scope>
    <source>
      <strain>Wistar</strain>
      <tissue>Peritoneal cavity</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="Cytokine" volume="10" first="169" last="174">
      <title>Gene structure, cDNA cloning, and expression of the rat cytokine-induced neutrophil chemoattractant-2 (CINC-2) gene.</title>
      <authorList>
        <person name="Shibata F."/>
        <person name="Konishi K."/>
        <person name="Nakagawa H."/>
      </authorList>
      <dbReference type="PubMed" id="9576061"/>
      <dbReference type="DOI" id="10.1006/cyto.1997.0271"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA] (ISOFORM 1)</scope>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] (ISOFORM 2)</scope>
    <scope>INDUCTION</scope>
    <source>
      <strain>Wistar</strain>
      <tissue>Peritoneal cavity</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1993" name="Biochem. Pharmacol." volume="45" first="1425" last="1430">
      <title>Production of an interleukin-8-like chemokine by cytokine-stimulated rat NRK-49F fibroblasts and its suppression by anti-inflammatory steroids.</title>
      <authorList>
        <person name="Nakagawa H."/>
        <person name="Ikesue A."/>
        <person name="Hatakeyama S."/>
        <person name="Kato H."/>
        <person name="Gotoda T."/>
        <person name="Komorita N."/>
        <person name="Watanabe K."/>
        <person name="Miyai H."/>
      </authorList>
      <dbReference type="PubMed" id="8471066"/>
      <dbReference type="DOI" id="10.1016/0006-2952(93)90041-t"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 33-56</scope>
    <source>
      <tissue>Fibroblast</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1996" name="Biochem. Biophys. Res. Commun." volume="220" first="945" last="948">
      <title>Cytokine-induced neutrophil chemoattractant (CINC)-2 alpha, a novel member of rat GRO/CINCs, is a predominant chemokine produced by lipopolysaccharide-stimulated rat macrophages in culture.</title>
      <authorList>
        <person name="Nakagawa H."/>
        <person name="Shiota S."/>
        <person name="Takano K."/>
        <person name="Shibata F."/>
        <person name="Kato H."/>
      </authorList>
      <dbReference type="PubMed" id="8607872"/>
      <dbReference type="DOI" id="10.1006/bbrc.1996.0511"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 33-61</scope>
    <scope>FUNCTION</scope>
    <source>
      <strain>Wistar</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2 4">Ligand for CXCR2 (By similarity). Has chemotactic activity for neutrophils. May play a role in inflammation and exert its effects on endothelial cells in an autocrine fashion.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>Q10746-1</id>
      <name>1</name>
      <name>CINC-2A</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>Q10746-2</id>
      <name>2</name>
      <name>CINC-2B</name>
      <sequence type="described" ref="VSP_035735"/>
    </isoform>
  </comment>
  <comment type="induction">
    <text evidence="5">By lipopolysaccharide. Expression increases until 4 hours after treatment and then gradually decreases, remaining high 24 hours after stimulation.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the intercrine alpha (chemokine CxC) family.</text>
  </comment>
  <dbReference type="EMBL" id="D21095">
    <property type="protein sequence ID" value="BAA04657.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D87926">
    <property type="protein sequence ID" value="BAB12336.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D87926">
    <property type="protein sequence ID" value="BAB12279.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D87927">
    <property type="protein sequence ID" value="BAB12280.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S46198">
    <property type="entry name" value="S46198"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_612531.1">
    <property type="nucleotide sequence ID" value="NM_138522.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_006250783.1">
    <property type="nucleotide sequence ID" value="XM_006250721.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q10746"/>
  <dbReference type="SMR" id="Q10746"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000070985"/>
  <dbReference type="PhosphoSitePlus" id="Q10746"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000038520"/>
  <dbReference type="Ensembl" id="ENSRNOT00000034090.4">
    <molecule id="Q10746-2"/>
    <property type="protein sequence ID" value="ENSRNOP00000038520.2"/>
    <property type="gene ID" value="ENSRNOG00000028043.5"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00000098120.1">
    <molecule id="Q10746-1"/>
    <property type="protein sequence ID" value="ENSRNOP00000077076.1"/>
    <property type="gene ID" value="ENSRNOG00000028043.5"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055010412">
    <molecule id="Q10746-1"/>
    <property type="protein sequence ID" value="ENSRNOP00055008087"/>
    <property type="gene ID" value="ENSRNOG00055006407"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060031454">
    <molecule id="Q10746-1"/>
    <property type="protein sequence ID" value="ENSRNOP00060025524"/>
    <property type="gene ID" value="ENSRNOG00060018323"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065028732">
    <molecule id="Q10746-1"/>
    <property type="protein sequence ID" value="ENSRNOP00065022750"/>
    <property type="gene ID" value="ENSRNOG00065017239"/>
  </dbReference>
  <dbReference type="UCSC" id="RGD:621812">
    <molecule id="Q10746-1"/>
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:621812"/>
  <dbReference type="RGD" id="621812">
    <property type="gene designation" value="Cxcl3"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S7MM">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000155233"/>
  <dbReference type="HOGENOM" id="CLU_143902_3_0_1"/>
  <dbReference type="InParanoid" id="Q10746"/>
  <dbReference type="OrthoDB" id="4170999at2759"/>
  <dbReference type="PhylomeDB" id="Q10746"/>
  <dbReference type="TreeFam" id="TF333433"/>
  <dbReference type="Reactome" id="R-RNO-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q10746"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 14"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000028043">
    <property type="expression patterns" value="Expressed in lung and 6 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045236">
    <property type="term" value="F:CXCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030593">
    <property type="term" value="P:neutrophil chemotaxis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007204">
    <property type="term" value="P:positive regulation of cytosolic calcium ion concentration"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032496">
    <property type="term" value="P:response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="CDD" id="cd00273">
    <property type="entry name" value="Chemokine_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000004">
    <property type="entry name" value="C-X-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001089">
    <property type="entry name" value="Chemokine_CXC"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018048">
    <property type="entry name" value="Chemokine_CXC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033899">
    <property type="entry name" value="CXC_Chemokine_domain"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF192">
    <property type="entry name" value="C-X-C MOTIF CHEMOKINE 3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00436">
    <property type="entry name" value="INTERLEUKIN8"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00437">
    <property type="entry name" value="SMALLCYTKCXC"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00471">
    <property type="entry name" value="SMALL_CYTOKINES_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3 4">
    <location>
      <begin position="1"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000144297" description="C-X-C motif chemokine 3">
    <location>
      <begin position="33"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="39"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_035735" description="In isoform 2." evidence="6">
    <original>DKSS</original>
    <variation>PSL</variation>
    <location>
      <begin position="98"/>
      <end position="101"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="8043001"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="8471066"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="8607872"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="9576061"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="8043001"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="101" mass="11109" checksum="D949D5712FE30909" modified="2008-11-25" version="2" precursor="true">MAPPTRRLLNAALLLLLLLMATSHQPSGTVVARELRCQCLKTLPRVDFENIQSLTVTPPGPHCTQTEVIATLKDGQEVCLNPQAPRLQKIIQKLLKSDKSS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>