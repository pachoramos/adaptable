<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-05-03" modified="2024-01-24" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>C0HM68</accession>
  <name>5BPIA_HETMG</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Pi/alpha-stichotoxin-Hmg5b</fullName>
      <shortName evidence="5">Pi/Alpha-SHTX-Hmg5b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Alpha-AnmTX Hmg 1b-2</fullName>
    </alternativeName>
  </protein>
  <organism evidence="4">
    <name type="scientific">Heteractis magnifica</name>
    <name type="common">Magnificent sea anemone</name>
    <name type="synonym">Radianthus magnifica</name>
    <dbReference type="NCBI Taxonomy" id="38281"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Stichodactylidae</taxon>
      <taxon>Heteractis</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2022" name="Toxins" volume="14" first="697" last="697">
      <title>Nicotinic Acetylcholine Receptors Are Novel Targets of APETx-like Toxins from the Sea Anemone Heteractis magnifica.</title>
      <authorList>
        <person name="Kalina R.S."/>
        <person name="Kasheverov I.E."/>
        <person name="Koshelev S.G."/>
        <person name="Sintsova O.V."/>
        <person name="Peigneur S."/>
        <person name="Pinheiro-Junior E.L."/>
        <person name="Popov R.S."/>
        <person name="Chausova V.E."/>
        <person name="Monastyrnaya M.M."/>
        <person name="Dmitrenok P.S."/>
        <person name="Isaeva M.P."/>
        <person name="Tytgat J."/>
        <person name="Kozlov S.A."/>
        <person name="Kozlovskaya E.P."/>
        <person name="Leychenko E.V."/>
        <person name="Gladkikh I.N."/>
      </authorList>
      <dbReference type="PubMed" id="36287966"/>
      <dbReference type="DOI" id="10.3390/toxins14100697"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>OXIDATION AT MET-16</scope>
  </reference>
  <comment type="function">
    <text evidence="3">The non-oxidized toxin potentiates ACh-elicited current of human alpha-7/CHRNA7 nicotinic acetylcholine receptors (nAChR) (PubMed:36287966). Also able to bind T. californica muscle-type nAChRs (PubMed:36287966).</text>
  </comment>
  <comment type="function">
    <text evidence="1 3">Forms an oxidized toxin derivative (Hmg 1b-2 MetOx) (PubMed:36287966). Able to bind T. californica muscle-type (alpha-1-beta-1-delta-epsilon (CHRNA1-CHRNB1-CHRND-CHRNE)) nAChRs (PubMed:36287966). Weakly and reversibly inhibits rat homomeric ASIC1 (isoform ASIC1a) (IC(50)=4.8 uM), and ASIC3 (By similarity). In vivo, induces an antihyperalgesic effect in the acid-induced muscle pain mice model (By similarity). Molecular modeling interaction with ASIC1a suggests that this peptide hinders the collapse of acidic pockets and stabilizes nonconducting channels state (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="6">Nematocyst</location>
    </subcellularLocation>
    <text evidence="3">Present in the mucus.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">Toxin occurs in two forms in the mucus, Hmg 1b-2 which is not oxidized and Hmg 1b-2 MetOx which is oxidized at Met-16.</text>
  </comment>
  <comment type="mass spectrometry" mass="4519.02" method="Electrospray" evidence="3"/>
  <comment type="mass spectrometry" mass="4534.97" method="Electrospray" evidence="3">
    <text>With oxidation.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="3">The non-oxidized toxin does not significantly enhance the current of human muscle alpha-1-beta-1-delta-epsilon/CHRNA1-CHRNB1-CHRND-CHRNE nAChRs (PubMed:36287966). The oxidized toxin has no modulatory effect on ASIC1a and ASIC3 channels (PubMed:36287966).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the sea anemone type 3 (BDS) potassium channel toxin family.</text>
  </comment>
  <dbReference type="SMR" id="C0HM68"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035792">
    <property type="term" value="C:host cell postsynaptic membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030549">
    <property type="term" value="F:acetylcholine receptor activator activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030550">
    <property type="term" value="F:acetylcholine receptor inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008200">
    <property type="term" value="F:ion channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012414">
    <property type="entry name" value="BDS_K_chnl_tox"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07936">
    <property type="entry name" value="Defensin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0008">Acetylcholine receptor inhibiting toxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0558">Oxidation</keyword>
  <keyword id="KW-0629">Postsynaptic neurotoxin</keyword>
  <keyword id="KW-1275">Proton-gated sodium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000458047" description="Pi/alpha-stichotoxin-Hmg5b" evidence="3">
    <location>
      <begin position="1"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine sulfoxide" evidence="3">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="4"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="6"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="20"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="non-consecutive residues" evidence="4">
    <location>
      <begin position="20"/>
      <end position="21"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HL52"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P61541"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="36287966"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="36287966"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="36287966"/>
    </source>
  </evidence>
  <sequence length="37" mass="4097" checksum="A912AA6AC2A02372" modified="2023-05-03" version="1" fragment="multiple">GTPCKCHGYIGVYWFMLAGCGYNLSCPYFLGICCVKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>