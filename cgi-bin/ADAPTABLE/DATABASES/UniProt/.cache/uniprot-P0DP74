<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-07-05" modified="2024-10-02" version="37" xmlns="http://uniprot.org/uniprot">
  <accession>P0DP74</accession>
  <accession>Q30KQ2</accession>
  <name>D130A_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Beta-defensin 130A</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Beta-defensin 130</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Beta-defensin 30</fullName>
      <shortName>DEFB-30</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="6">Defensin, beta 130</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="6" type="primary">DEFB130A</name>
    <name evidence="6" type="synonym">DEFB130</name>
    <name type="synonym">DEFB30</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Physiol. Genomics" volume="23" first="5" last="17">
      <title>Cross-species analysis of the mammalian beta-defensin gene family: presence of syntenic gene clusters and preferential expression in the male reproductive tract.</title>
      <authorList>
        <person name="Patil A.A."/>
        <person name="Cai Y."/>
        <person name="Sang Y."/>
        <person name="Blecha F."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="16033865"/>
      <dbReference type="DOI" id="10.1152/physiolgenomics.00104.2005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="Nature" volume="439" first="331" last="335">
      <title>DNA sequence and analysis of human chromosome 8.</title>
      <authorList>
        <person name="Nusbaum C."/>
        <person name="Mikkelsen T.S."/>
        <person name="Zody M.C."/>
        <person name="Asakawa S."/>
        <person name="Taudien S."/>
        <person name="Garber M."/>
        <person name="Kodira C.D."/>
        <person name="Schueler M.G."/>
        <person name="Shimizu A."/>
        <person name="Whittaker C.A."/>
        <person name="Chang J.L."/>
        <person name="Cuomo C.A."/>
        <person name="Dewar K."/>
        <person name="FitzGerald M.G."/>
        <person name="Yang X."/>
        <person name="Allen N.R."/>
        <person name="Anderson S."/>
        <person name="Asakawa T."/>
        <person name="Blechschmidt K."/>
        <person name="Bloom T."/>
        <person name="Borowsky M.L."/>
        <person name="Butler J."/>
        <person name="Cook A."/>
        <person name="Corum B."/>
        <person name="DeArellano K."/>
        <person name="DeCaprio D."/>
        <person name="Dooley K.T."/>
        <person name="Dorris L. III"/>
        <person name="Engels R."/>
        <person name="Gloeckner G."/>
        <person name="Hafez N."/>
        <person name="Hagopian D.S."/>
        <person name="Hall J.L."/>
        <person name="Ishikawa S.K."/>
        <person name="Jaffe D.B."/>
        <person name="Kamat A."/>
        <person name="Kudoh J."/>
        <person name="Lehmann R."/>
        <person name="Lokitsang T."/>
        <person name="Macdonald P."/>
        <person name="Major J.E."/>
        <person name="Matthews C.D."/>
        <person name="Mauceli E."/>
        <person name="Menzel U."/>
        <person name="Mihalev A.H."/>
        <person name="Minoshima S."/>
        <person name="Murayama Y."/>
        <person name="Naylor J.W."/>
        <person name="Nicol R."/>
        <person name="Nguyen C."/>
        <person name="O'Leary S.B."/>
        <person name="O'Neill K."/>
        <person name="Parker S.C.J."/>
        <person name="Polley A."/>
        <person name="Raymond C.K."/>
        <person name="Reichwald K."/>
        <person name="Rodriguez J."/>
        <person name="Sasaki T."/>
        <person name="Schilhabel M."/>
        <person name="Siddiqui R."/>
        <person name="Smith C.L."/>
        <person name="Sneddon T.P."/>
        <person name="Talamas J.A."/>
        <person name="Tenzin P."/>
        <person name="Topham K."/>
        <person name="Venkataraman V."/>
        <person name="Wen G."/>
        <person name="Yamazaki S."/>
        <person name="Young S.K."/>
        <person name="Zeng Q."/>
        <person name="Zimmer A.R."/>
        <person name="Rosenthal A."/>
        <person name="Birren B.W."/>
        <person name="Platzer M."/>
        <person name="Shimizu N."/>
        <person name="Lander E.S."/>
      </authorList>
      <dbReference type="PubMed" id="16421571"/>
      <dbReference type="DOI" id="10.1038/nature04406"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2017" name="Sci. Rep." volume="7" first="41772" last="41772">
      <title>Involvement of beta-defensin 130 (DEFB130) in the macrophage microbicidal mechanisms for killing Plasmodium falciparum.</title>
      <authorList>
        <person name="Terkawi M.A."/>
        <person name="Takano R."/>
        <person name="Furukawa A."/>
        <person name="Murakoshi F."/>
        <person name="Kato K."/>
      </authorList>
      <dbReference type="PubMed" id="28181499"/>
      <dbReference type="DOI" id="10.1038/srep41772"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Antimicrobial host-defense peptide. Has an antiplasmodial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed on differentiated macrophage phagocytizing plasmodium falciparum-parasitized erythrocytes.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="DQ012022">
    <property type="protein sequence ID" value="AAY59758.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC232308">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS43714.1"/>
  <dbReference type="RefSeq" id="NP_001032893.1">
    <property type="nucleotide sequence ID" value="NM_001037804.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001182186.1">
    <property type="nucleotide sequence ID" value="NM_001195257.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0DP74"/>
  <dbReference type="SMR" id="P0DP74"/>
  <dbReference type="STRING" id="9606.ENSP00000382951"/>
  <dbReference type="BioMuta" id="DEFB130A"/>
  <dbReference type="MassIVE" id="P0DP74"/>
  <dbReference type="PaxDb" id="9606-ENSP00000382951"/>
  <dbReference type="Antibodypedia" id="77986">
    <property type="antibodies" value="2 antibodies from 2 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="245940"/>
  <dbReference type="Ensembl" id="ENST00000400079.1">
    <property type="protein sequence ID" value="ENSP00000382951.1"/>
    <property type="gene ID" value="ENSG00000232948.1"/>
  </dbReference>
  <dbReference type="GeneID" id="100133267"/>
  <dbReference type="GeneID" id="245940"/>
  <dbReference type="KEGG" id="hsa:100133267"/>
  <dbReference type="KEGG" id="hsa:245940"/>
  <dbReference type="MANE-Select" id="ENST00000400079.1">
    <property type="protein sequence ID" value="ENSP00000382951.1"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_001037804.1"/>
    <property type="RefSeq protein sequence ID" value="NP_001032893.1"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:18107"/>
  <dbReference type="AGR" id="HGNC:39814"/>
  <dbReference type="CTD" id="100133267"/>
  <dbReference type="CTD" id="245940"/>
  <dbReference type="DisGeNET" id="100133267"/>
  <dbReference type="GeneCards" id="DEFB130A"/>
  <dbReference type="HGNC" id="HGNC:18107">
    <property type="gene designation" value="DEFB130A"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000232948">
    <property type="expression patterns" value="Tissue enriched (epididymis)"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P0DP74"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000232948"/>
  <dbReference type="eggNOG" id="ENOG502TEXC">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000160995"/>
  <dbReference type="InParanoid" id="P0DP74"/>
  <dbReference type="OMA" id="KCCRIWW"/>
  <dbReference type="OrthoDB" id="4640037at2759"/>
  <dbReference type="PathwayCommons" id="P0DP74"/>
  <dbReference type="Reactome" id="R-HSA-1461957">
    <property type="pathway name" value="Beta defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-1461973">
    <property type="pathway name" value="Defensins"/>
  </dbReference>
  <dbReference type="Pharos" id="P0DP74">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P0DP74"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 8"/>
  </dbReference>
  <dbReference type="RNAct" id="P0DP74">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000232948">
    <property type="expression patterns" value="Expressed in urinary bladder and 3 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042056">
    <property type="term" value="F:chemoattractant activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060326">
    <property type="term" value="P:cell chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515:SF1">
    <property type="entry name" value="BETA-DEFENSIN 130A-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000045369" description="Beta-defensin 130A">
    <location>
      <begin position="23"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="38"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="60"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="28181499"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="16033865"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="HGNC" id="HGNC:18107"/>
    </source>
  </evidence>
  <sequence length="79" mass="8736" checksum="65DF21FA1C40D507" modified="2017-07-05" version="1" precursor="true">MKLHSLISVLLLFVTLIPKGKTGVIPGQKQCIALKGVCRDKLCSTLDDTIGICNEGKKCCRRWWILEPYPTPVPKGKSP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>