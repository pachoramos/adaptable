<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-01-01" modified="2023-02-22" version="71" xmlns="http://uniprot.org/uniprot">
  <accession>P06116</accession>
  <name>HMC1_METBA</name>
  <protein>
    <recommendedName>
      <fullName>Chromosomal protein MC1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>HMB</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Methanosarcina barkeri</name>
    <dbReference type="NCBI Taxonomy" id="2208"/>
    <lineage>
      <taxon>Archaea</taxon>
      <taxon>Euryarchaeota</taxon>
      <taxon>Stenosarchaea group</taxon>
      <taxon>Methanomicrobia</taxon>
      <taxon>Methanosarcinales</taxon>
      <taxon>Methanosarcinaceae</taxon>
      <taxon>Methanosarcina</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1986" name="Eur. J. Biochem." volume="161" first="681" last="687">
      <title>Primary structure of the chromosomal protein HMb from the archaebacteria Methanosarcina barkeri.</title>
      <authorList>
        <person name="Laine B."/>
        <person name="Chartier F."/>
        <person name="Imbert M."/>
        <person name="Lewis R."/>
        <person name="Sautiere P."/>
      </authorList>
      <dbReference type="PubMed" id="3098561"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1986.tb10493.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <strain>ATCC 43569 / MS / DSM 800 / JCM 10043 / NBRC 100474</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1990" name="Biochim. Biophys. Acta" volume="1038" first="346" last="354">
      <title>Conformational study of the chromosomal protein MC1 from the archaebacterium Methanosarcina barkeri.</title>
      <authorList>
        <person name="Imbert M."/>
        <person name="Laine B."/>
        <person name="Helbecque N."/>
        <person name="Mornon J.-P."/>
        <person name="Henichart J.-P."/>
        <person name="Sautiere P."/>
      </authorList>
      <dbReference type="PubMed" id="2111171"/>
      <dbReference type="DOI" id="10.1016/0167-4838(90)90247-d"/>
    </citation>
    <scope>CONFORMATIONAL STUDIES</scope>
  </reference>
  <comment type="function">
    <text>Protects DNA against thermal denaturation and modulates transcription.</text>
  </comment>
  <dbReference type="PIR" id="A25343">
    <property type="entry name" value="A25343"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P06116"/>
  <dbReference type="SMR" id="P06116"/>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042262">
    <property type="term" value="P:DNA protection"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.470.10">
    <property type="entry name" value="Chromosomal protein MC1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008674">
    <property type="entry name" value="MC1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036620">
    <property type="entry name" value="MC1_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05854">
    <property type="entry name" value="MC1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF102875">
    <property type="entry name" value="Chromosomal protein MC1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <feature type="chain" id="PRO_0000084002" description="Chromosomal protein MC1">
    <location>
      <begin position="1"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="1"/>
      <end position="43"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <sequence length="93" mass="10755" checksum="3B5E17405CE2171C" modified="1988-01-01" version="1">SNTRNFVLRDEEGNEHGVFTGKQPRQAALKAANRGDGTKSNPDVIRLRERGTKKVHVFKAWKEMVEAPKNRPDWMPEKISKPFVKKEKIEKIE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>