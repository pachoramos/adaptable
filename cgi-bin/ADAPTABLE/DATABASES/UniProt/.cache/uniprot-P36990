<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-06-01" modified="2022-05-25" version="53" xmlns="http://uniprot.org/uniprot">
  <accession>P36990</accession>
  <name>TXP10_PLETR</name>
  <protein>
    <recommendedName>
      <fullName>U3-plectoxin-Pt1a</fullName>
      <shortName>U3-PLTX-Pt1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Plectoxin X</fullName>
      <shortName>PLT-X</shortName>
      <shortName>PLTX</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Plectoxin-10</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Plectreurys tristis</name>
    <name type="common">Spider</name>
    <name type="synonym">Plectreurys bispinosus</name>
    <dbReference type="NCBI Taxonomy" id="33319"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Haplogynae</taxon>
      <taxon>Pholcoidea</taxon>
      <taxon>Plectreuridae</taxon>
      <taxon>Plectreurys</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="J. Biol. Chem." volume="269" first="11098" last="11101">
      <title>Isolation and sequencing of insecticidal peptides from the primitive hunting spider, Plectreurys tristis (Simon).</title>
      <authorList>
        <person name="Quistad G.B."/>
        <person name="Skinner W.S."/>
      </authorList>
      <dbReference type="PubMed" id="8157635"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(19)78096-x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Potent toxin that may paralyze and/or kill insect pests such as H.virescens (lepidoptera), S.exigua (beet armyworm) and M.sexta (tobacco hornworm).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <dbReference type="PIR" id="H53613">
    <property type="entry name" value="H53613"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P36990"/>
  <dbReference type="SMR" id="P36990"/>
  <dbReference type="ArachnoServer" id="AS000391">
    <property type="toxin name" value="U3-plectoxin-Pt1a"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000087662" description="U3-plectoxin-Pt1a">
    <location>
      <begin position="1"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="2"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="9"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="15"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="45"/>
      <end position="49"/>
    </location>
  </feature>
  <evidence type="ECO:0000305" key="1"/>
  <sequence length="49" mass="5140" checksum="86294DF6DDDB3597" modified="1994-06-01" version="1">GCKGFLVKCDSNSECCKTAIVKGKKKQLSCLCGAWGAGCSCSFRCGNRC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>