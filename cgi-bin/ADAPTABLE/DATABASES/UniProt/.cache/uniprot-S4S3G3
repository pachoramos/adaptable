<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-09-13" modified="2024-03-27" version="12" xmlns="http://uniprot.org/uniprot">
  <accession>S4S3G3</accession>
  <name>MAST_VESDU</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Mastoparan-D</fullName>
      <shortName evidence="5">MP-D</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Vespa ducalis</name>
    <name type="common">Black-tailed hornet</name>
    <dbReference type="NCBI Taxonomy" id="1075778"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Vespinae</taxon>
      <taxon>Vespa</taxon>
    </lineage>
  </organism>
  <reference evidence="8" key="1">
    <citation type="journal article" date="2011" name="Peptides" volume="32" first="2027" last="2036">
      <title>Structural and biological characterization of mastoparans in the venom of Vespa species in Taiwan.</title>
      <authorList>
        <person name="Lin C.H."/>
        <person name="Tzen J.T."/>
        <person name="Shyu C.L."/>
        <person name="Yang M.J."/>
        <person name="Tu W.C."/>
      </authorList>
      <dbReference type="PubMed" id="21884742"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2011.08.015"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>PROBABLE AMIDATION AT LEU-59</scope>
    <scope>SYNTHESIS OF 46-59</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2 4">Antimicrobial and mast cell degranulating peptide. Has broad spectrum antibacterial activity against both Gram-positive and Gram-negative bacteria (S.aureus MIC=24-32 ug/ml, S.xylosus MIC=2 ug/ml, S.alactolyticus MIC=16 ug/ml, C.koseri MIC=4 ug/ml, E.coli MIC=8 ug/ml, K.pneumoniae MIC=32 ug/ml, P.aerugiosa MIC=128 ug/ml, S.choleraesuis MIC=16 ug/ml, S.typhimurium MIC=32 ug/ml, V.parahamelytics MIC=32 ug/ml). Affects membrane permeability of E.coli. Shows hemolytic activities on sheep, chicken and human erythrocytes (PubMed:21884742). Its mast cell degranulation activity may be related to the activation of G-protein coupled receptors in mast cells as well as interaction with other proteins located in cell endosomal membranes in the mast cells (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="7">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="6">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="7">Assumes an amphipathic alpha-helical conformation in a membrane-like environment.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the MCD family. Mastoparan subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="HQ168024">
    <property type="protein sequence ID" value="AEM43052.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="S4S3G3"/>
  <dbReference type="SMR" id="S4S3G3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000458811" evidence="7">
    <location>
      <begin position="28"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5004532739" description="Mastoparan-D" evidence="7">
    <location>
      <begin position="46"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 1" evidence="6">
    <location>
      <begin position="27"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 2" evidence="6">
    <location>
      <begin position="31"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 3" evidence="6">
    <location>
      <begin position="35"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 4" evidence="6">
    <location>
      <begin position="41"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="7">
    <location>
      <position position="59"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01514"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P84914"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="21884742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="21884742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="21884742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="AEM43052.1"/>
    </source>
  </evidence>
  <sequence length="60" mass="6214" checksum="9C0DE1C9ABFE85BC" modified="2013-10-16" version="1" precursor="true">MKNTILILFTAFIALLGFFGMSAEALADPIADPVAGPNPEADPEAINLKAIAAFAKKLLG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>