<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2022-05-25" modified="2023-06-28" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>P0DV62</accession>
  <name>TX4A_PARAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Lycotoxin-Pa4a</fullName>
      <shortName evidence="5">LCTX-Pa4a</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pardosa astrigera</name>
    <name type="common">Wolf spider</name>
    <dbReference type="NCBI Taxonomy" id="317848"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Lycosoidea</taxon>
      <taxon>Lycosidae</taxon>
      <taxon>Pardosa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2020" name="Antibiotics" volume="9">
      <title>Antibacterial and anti-inflammatory effects of novel peptide toxin from the spider Pardosa astrigera.</title>
      <authorList>
        <person name="Shin M.K."/>
        <person name="Hwang I.W."/>
        <person name="Kim Y."/>
        <person name="Kim S.T."/>
        <person name="Jang W."/>
        <person name="Lee S."/>
        <person name="Bang W.Y."/>
        <person name="Bae C.H."/>
        <person name="Sung J.S."/>
      </authorList>
      <dbReference type="PubMed" id="32707636"/>
      <dbReference type="DOI" id="10.3390/antibiotics9070422"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 48-122</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Potent antibacterial peptide with anti-inflammatory properties. Inhibits both Gram-negative and Gram-positive bacteria by disrupting both the outer membrane and the cytosolic membrane of bacteria. Also downregulates the expression of pro-inflammatory mediators (cyclooxygenase-2 (PTGS2/COX2), nitric oxide-induced synthase (NOS2), IL-1 beta (IL1B), TNF-alpha (TNF)) and upregulates the level of anti-inflammatory cytokine (IL10) by inactivating mitogen-activated protein kinase signaling in a lipopolysaccharide-stimulated murine macrophage cell line.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="5">Target cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="5">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the neurotoxin 19 (CSTX) family.</text>
  </comment>
  <dbReference type="SMR" id="P0DV62"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019553">
    <property type="entry name" value="Spider_toxin_CSTX_knottin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011142">
    <property type="entry name" value="Spider_toxin_CSTX_Knottin_CS"/>
  </dbReference>
  <dbReference type="Pfam" id="PF10530">
    <property type="entry name" value="Toxin_35"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60029">
    <property type="entry name" value="SPIDER_CSTX"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000455162" evidence="6">
    <location>
      <begin position="21"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000455163" description="Lycotoxin-Pa4a" evidence="6">
    <location>
      <begin position="48"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 6">
    <location>
      <begin position="58"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 6">
    <location>
      <begin position="65"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 6">
    <location>
      <begin position="72"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 6">
    <location>
      <begin position="84"/>
      <end position="98"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P58604"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="32707636"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="32707636"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="32707636"/>
    </source>
  </evidence>
  <sequence length="122" mass="14037" checksum="2FAE4905E91EA05C" modified="2022-05-25" version="1" precursor="true">MKLGIFFSVFFLAMIHSCLSETNEDKNLESYFREDDLKALSFGEYARAMMAESRKDNCIPKHHECTSRPKDCCKQNLMQFKCSCMTIIDKNNKETERCKCDNSIFQKVAKTSVNIGKAVVTK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>