<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1999-07-15" modified="2024-11-27" version="193" xmlns="http://uniprot.org/uniprot">
  <accession>O00626</accession>
  <accession>A0N0Q6</accession>
  <accession>B2R4W2</accession>
  <name>CCL22_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>C-C motif chemokine 22</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>CC chemokine STCP-1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>MDC(1-69)</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Macrophage-derived chemokine</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Small-inducible cytokine A22</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Stimulated T-cell chemotactic protein 1</fullName>
    </alternativeName>
    <component>
      <recommendedName>
        <fullName>MDC(3-69)</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>MDC(5-69)</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>MDC(7-69)</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">CCL22</name>
    <name type="synonym">MDC</name>
    <name type="synonym">SCYA22</name>
    <name type="ORF">A-152E5.1</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="J. Biol. Chem." volume="272" first="25229" last="25237">
      <title>Molecular cloning and functional characterization of a novel CC chemokine, stimulated T cell chemotactic protein (STCP-1) that specifically acts on activated T lymphocytes.</title>
      <authorList>
        <person name="Chang M.-S."/>
        <person name="McNinch J."/>
        <person name="Elias C. III"/>
        <person name="Manthey C.L."/>
        <person name="Grosshans D."/>
        <person name="Meng T."/>
        <person name="Boone T."/>
        <person name="Andrew D.P."/>
      </authorList>
      <dbReference type="PubMed" id="9312138"/>
      <dbReference type="DOI" id="10.1074/jbc.272.40.25229"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>VARIANT ALA-2</scope>
    <source>
      <tissue>Macrophage</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="J. Exp. Med." volume="185" first="1595" last="1604">
      <title>Human macrophage-derived chemokine (MDC), a novel chemoattractant for monocytes, monocyte-derived dendritic cells, and natural killer cells.</title>
      <authorList>
        <person name="Godiska R."/>
        <person name="Chantry D."/>
        <person name="Raport C.J."/>
        <person name="Sozzani S."/>
        <person name="Allavena P."/>
        <person name="Leviten D."/>
        <person name="Mantovani A."/>
        <person name="Gray P.W."/>
      </authorList>
      <dbReference type="PubMed" id="9151897"/>
      <dbReference type="DOI" id="10.1084/jem.185.9.1595"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 25-35</scope>
    <scope>VARIANT ALA-2</scope>
    <source>
      <tissue>Macrophage</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="submission" date="2006-10" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Livingston R.J."/>
        <person name="Shaffer T."/>
        <person name="McFarland I."/>
        <person name="Nguyen C.P."/>
        <person name="Stanaway I.B."/>
        <person name="Rajkumar N."/>
        <person name="Johnson E.J."/>
        <person name="da Ponte S.H."/>
        <person name="Willa H."/>
        <person name="Ahearn M.O."/>
        <person name="Bertucci C."/>
        <person name="Acklestad J."/>
        <person name="Carroll A."/>
        <person name="Swanson J."/>
        <person name="Gildersleeve H.I."/>
        <person name="Nickerson D.A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>VARIANT ALA-2</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2004" name="Nat. Genet." volume="36" first="40" last="45">
      <title>Complete sequencing and characterization of 21,243 full-length human cDNAs.</title>
      <authorList>
        <person name="Ota T."/>
        <person name="Suzuki Y."/>
        <person name="Nishikawa T."/>
        <person name="Otsuki T."/>
        <person name="Sugiyama T."/>
        <person name="Irie R."/>
        <person name="Wakamatsu A."/>
        <person name="Hayashi K."/>
        <person name="Sato H."/>
        <person name="Nagai K."/>
        <person name="Kimura K."/>
        <person name="Makita H."/>
        <person name="Sekine M."/>
        <person name="Obayashi M."/>
        <person name="Nishi T."/>
        <person name="Shibahara T."/>
        <person name="Tanaka T."/>
        <person name="Ishii S."/>
        <person name="Yamamoto J."/>
        <person name="Saito K."/>
        <person name="Kawai Y."/>
        <person name="Isono Y."/>
        <person name="Nakamura Y."/>
        <person name="Nagahari K."/>
        <person name="Murakami K."/>
        <person name="Yasuda T."/>
        <person name="Iwayanagi T."/>
        <person name="Wagatsuma M."/>
        <person name="Shiratori A."/>
        <person name="Sudo H."/>
        <person name="Hosoiri T."/>
        <person name="Kaku Y."/>
        <person name="Kodaira H."/>
        <person name="Kondo H."/>
        <person name="Sugawara M."/>
        <person name="Takahashi M."/>
        <person name="Kanda K."/>
        <person name="Yokoi T."/>
        <person name="Furuya T."/>
        <person name="Kikkawa E."/>
        <person name="Omura Y."/>
        <person name="Abe K."/>
        <person name="Kamihara K."/>
        <person name="Katsuta N."/>
        <person name="Sato K."/>
        <person name="Tanikawa M."/>
        <person name="Yamazaki M."/>
        <person name="Ninomiya K."/>
        <person name="Ishibashi T."/>
        <person name="Yamashita H."/>
        <person name="Murakawa K."/>
        <person name="Fujimori K."/>
        <person name="Tanai H."/>
        <person name="Kimata M."/>
        <person name="Watanabe M."/>
        <person name="Hiraoka S."/>
        <person name="Chiba Y."/>
        <person name="Ishida S."/>
        <person name="Ono Y."/>
        <person name="Takiguchi S."/>
        <person name="Watanabe S."/>
        <person name="Yosida M."/>
        <person name="Hotuta T."/>
        <person name="Kusano J."/>
        <person name="Kanehori K."/>
        <person name="Takahashi-Fujii A."/>
        <person name="Hara H."/>
        <person name="Tanase T.-O."/>
        <person name="Nomura Y."/>
        <person name="Togiya S."/>
        <person name="Komai F."/>
        <person name="Hara R."/>
        <person name="Takeuchi K."/>
        <person name="Arita M."/>
        <person name="Imose N."/>
        <person name="Musashino K."/>
        <person name="Yuuki H."/>
        <person name="Oshima A."/>
        <person name="Sasaki N."/>
        <person name="Aotsuka S."/>
        <person name="Yoshikawa Y."/>
        <person name="Matsunawa H."/>
        <person name="Ichihara T."/>
        <person name="Shiohata N."/>
        <person name="Sano S."/>
        <person name="Moriya S."/>
        <person name="Momiyama H."/>
        <person name="Satoh N."/>
        <person name="Takami S."/>
        <person name="Terashima Y."/>
        <person name="Suzuki O."/>
        <person name="Nakagawa S."/>
        <person name="Senoh A."/>
        <person name="Mizoguchi H."/>
        <person name="Goto Y."/>
        <person name="Shimizu F."/>
        <person name="Wakebe H."/>
        <person name="Hishigaki H."/>
        <person name="Watanabe T."/>
        <person name="Sugiyama A."/>
        <person name="Takemoto M."/>
        <person name="Kawakami B."/>
        <person name="Yamazaki M."/>
        <person name="Watanabe K."/>
        <person name="Kumagai A."/>
        <person name="Itakura S."/>
        <person name="Fukuzumi Y."/>
        <person name="Fujimori Y."/>
        <person name="Komiyama M."/>
        <person name="Tashiro H."/>
        <person name="Tanigami A."/>
        <person name="Fujiwara T."/>
        <person name="Ono T."/>
        <person name="Yamada K."/>
        <person name="Fujii Y."/>
        <person name="Ozaki K."/>
        <person name="Hirao M."/>
        <person name="Ohmori Y."/>
        <person name="Kawabata A."/>
        <person name="Hikiji T."/>
        <person name="Kobatake N."/>
        <person name="Inagaki H."/>
        <person name="Ikema Y."/>
        <person name="Okamoto S."/>
        <person name="Okitani R."/>
        <person name="Kawakami T."/>
        <person name="Noguchi S."/>
        <person name="Itoh T."/>
        <person name="Shigeta K."/>
        <person name="Senba T."/>
        <person name="Matsumura K."/>
        <person name="Nakajima Y."/>
        <person name="Mizuno T."/>
        <person name="Morinaga M."/>
        <person name="Sasaki M."/>
        <person name="Togashi T."/>
        <person name="Oyama M."/>
        <person name="Hata H."/>
        <person name="Watanabe M."/>
        <person name="Komatsu T."/>
        <person name="Mizushima-Sugano J."/>
        <person name="Satoh T."/>
        <person name="Shirai Y."/>
        <person name="Takahashi Y."/>
        <person name="Nakagawa K."/>
        <person name="Okumura K."/>
        <person name="Nagase T."/>
        <person name="Nomura N."/>
        <person name="Kikuchi H."/>
        <person name="Masuho Y."/>
        <person name="Yamashita R."/>
        <person name="Nakai K."/>
        <person name="Yada T."/>
        <person name="Nakamura Y."/>
        <person name="Ohara O."/>
        <person name="Isogai T."/>
        <person name="Sugano S."/>
      </authorList>
      <dbReference type="PubMed" id="14702039"/>
      <dbReference type="DOI" id="10.1038/ng1285"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Peripheral blood</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1999" name="Genomics" volume="60" first="295" last="308">
      <title>Genome duplications and other features in 12 Mb of DNA sequence from human chromosome 16p and 16q.</title>
      <authorList>
        <person name="Loftus B.J."/>
        <person name="Kim U.-J."/>
        <person name="Sneddon V.P."/>
        <person name="Kalush F."/>
        <person name="Brandon R."/>
        <person name="Fuhrmann J."/>
        <person name="Mason T."/>
        <person name="Crosby M.L."/>
        <person name="Barnstead M."/>
        <person name="Cronin L."/>
        <person name="Mays A.D."/>
        <person name="Cao Y."/>
        <person name="Xu R.X."/>
        <person name="Kang H.-L."/>
        <person name="Mitchell S."/>
        <person name="Eichler E.E."/>
        <person name="Harris P.C."/>
        <person name="Venter J.C."/>
        <person name="Adams M.D."/>
      </authorList>
      <dbReference type="PubMed" id="10493829"/>
      <dbReference type="DOI" id="10.1006/geno.1999.5927"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <scope>VARIANT ALA-2</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2004" name="Nature" volume="432" first="988" last="994">
      <title>The sequence and analysis of duplication-rich human chromosome 16.</title>
      <authorList>
        <person name="Martin J."/>
        <person name="Han C."/>
        <person name="Gordon L.A."/>
        <person name="Terry A."/>
        <person name="Prabhakar S."/>
        <person name="She X."/>
        <person name="Xie G."/>
        <person name="Hellsten U."/>
        <person name="Chan Y.M."/>
        <person name="Altherr M."/>
        <person name="Couronne O."/>
        <person name="Aerts A."/>
        <person name="Bajorek E."/>
        <person name="Black S."/>
        <person name="Blumer H."/>
        <person name="Branscomb E."/>
        <person name="Brown N.C."/>
        <person name="Bruno W.J."/>
        <person name="Buckingham J.M."/>
        <person name="Callen D.F."/>
        <person name="Campbell C.S."/>
        <person name="Campbell M.L."/>
        <person name="Campbell E.W."/>
        <person name="Caoile C."/>
        <person name="Challacombe J.F."/>
        <person name="Chasteen L.A."/>
        <person name="Chertkov O."/>
        <person name="Chi H.C."/>
        <person name="Christensen M."/>
        <person name="Clark L.M."/>
        <person name="Cohn J.D."/>
        <person name="Denys M."/>
        <person name="Detter J.C."/>
        <person name="Dickson M."/>
        <person name="Dimitrijevic-Bussod M."/>
        <person name="Escobar J."/>
        <person name="Fawcett J.J."/>
        <person name="Flowers D."/>
        <person name="Fotopulos D."/>
        <person name="Glavina T."/>
        <person name="Gomez M."/>
        <person name="Gonzales E."/>
        <person name="Goodstein D."/>
        <person name="Goodwin L.A."/>
        <person name="Grady D.L."/>
        <person name="Grigoriev I."/>
        <person name="Groza M."/>
        <person name="Hammon N."/>
        <person name="Hawkins T."/>
        <person name="Haydu L."/>
        <person name="Hildebrand C.E."/>
        <person name="Huang W."/>
        <person name="Israni S."/>
        <person name="Jett J."/>
        <person name="Jewett P.B."/>
        <person name="Kadner K."/>
        <person name="Kimball H."/>
        <person name="Kobayashi A."/>
        <person name="Krawczyk M.-C."/>
        <person name="Leyba T."/>
        <person name="Longmire J.L."/>
        <person name="Lopez F."/>
        <person name="Lou Y."/>
        <person name="Lowry S."/>
        <person name="Ludeman T."/>
        <person name="Manohar C.F."/>
        <person name="Mark G.A."/>
        <person name="McMurray K.L."/>
        <person name="Meincke L.J."/>
        <person name="Morgan J."/>
        <person name="Moyzis R.K."/>
        <person name="Mundt M.O."/>
        <person name="Munk A.C."/>
        <person name="Nandkeshwar R.D."/>
        <person name="Pitluck S."/>
        <person name="Pollard M."/>
        <person name="Predki P."/>
        <person name="Parson-Quintana B."/>
        <person name="Ramirez L."/>
        <person name="Rash S."/>
        <person name="Retterer J."/>
        <person name="Ricke D.O."/>
        <person name="Robinson D.L."/>
        <person name="Rodriguez A."/>
        <person name="Salamov A."/>
        <person name="Saunders E.H."/>
        <person name="Scott D."/>
        <person name="Shough T."/>
        <person name="Stallings R.L."/>
        <person name="Stalvey M."/>
        <person name="Sutherland R.D."/>
        <person name="Tapia R."/>
        <person name="Tesmer J.G."/>
        <person name="Thayer N."/>
        <person name="Thompson L.S."/>
        <person name="Tice H."/>
        <person name="Torney D.C."/>
        <person name="Tran-Gyamfi M."/>
        <person name="Tsai M."/>
        <person name="Ulanovsky L.E."/>
        <person name="Ustaszewska A."/>
        <person name="Vo N."/>
        <person name="White P.S."/>
        <person name="Williams A.L."/>
        <person name="Wills P.L."/>
        <person name="Wu J.-R."/>
        <person name="Wu K."/>
        <person name="Yang J."/>
        <person name="DeJong P."/>
        <person name="Bruce D."/>
        <person name="Doggett N.A."/>
        <person name="Deaven L."/>
        <person name="Schmutz J."/>
        <person name="Grimwood J."/>
        <person name="Richardson P."/>
        <person name="Rokhsar D.S."/>
        <person name="Eichler E.E."/>
        <person name="Gilna P."/>
        <person name="Lucas S.M."/>
        <person name="Myers R.M."/>
        <person name="Rubin E.M."/>
        <person name="Pennacchio L.A."/>
      </authorList>
      <dbReference type="PubMed" id="15616553"/>
      <dbReference type="DOI" id="10.1038/nature03187"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="7">
    <citation type="submission" date="2005-07" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Istrail S."/>
        <person name="Sutton G.G."/>
        <person name="Florea L."/>
        <person name="Halpern A.L."/>
        <person name="Mobarry C.M."/>
        <person name="Lippert R."/>
        <person name="Walenz B."/>
        <person name="Shatkay H."/>
        <person name="Dew I."/>
        <person name="Miller J.R."/>
        <person name="Flanigan M.J."/>
        <person name="Edwards N.J."/>
        <person name="Bolanos R."/>
        <person name="Fasulo D."/>
        <person name="Halldorsson B.V."/>
        <person name="Hannenhalli S."/>
        <person name="Turner R."/>
        <person name="Yooseph S."/>
        <person name="Lu F."/>
        <person name="Nusskern D.R."/>
        <person name="Shue B.C."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F."/>
        <person name="Delcher A.L."/>
        <person name="Huson D.H."/>
        <person name="Kravitz S.A."/>
        <person name="Mouchard L."/>
        <person name="Reinert K."/>
        <person name="Remington K.A."/>
        <person name="Clark A.G."/>
        <person name="Waterman M.S."/>
        <person name="Eichler E.E."/>
        <person name="Adams M.D."/>
        <person name="Hunkapiller M.W."/>
        <person name="Myers E.W."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <scope>VARIANT ALA-2</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <scope>VARIANT ALA-2</scope>
    <source>
      <tissue>Pancreas</tissue>
      <tissue>Spleen</tissue>
    </source>
  </reference>
  <reference key="9">
    <citation type="journal article" date="1998" name="J. Biol. Chem." volume="273" first="1764" last="1768">
      <title>Macrophage-derived chemokine is a functional ligand for the CC chemokine receptor 4.</title>
      <authorList>
        <person name="Imai T."/>
        <person name="Chantry D."/>
        <person name="Raport C.J."/>
        <person name="Wood C.L."/>
        <person name="Nishimura M."/>
        <person name="Godiska R."/>
        <person name="Yoshie O."/>
        <person name="Gray P.W."/>
      </authorList>
      <dbReference type="PubMed" id="9430724"/>
      <dbReference type="DOI" id="10.1074/jbc.273.3.1764"/>
    </citation>
    <scope>RECEPTOR INTERACTION</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2001" name="Eur. J. Immunol." volume="31" first="812" last="822">
      <title>Dendritic cells as a major source of macrophage-derived chemokine/CCL22 in vitro and in vivo.</title>
      <authorList>
        <person name="Vulcano M."/>
        <person name="Albanesi C."/>
        <person name="Stoppacciaro A."/>
        <person name="Bagnati R."/>
        <person name="D'Amico G."/>
        <person name="Struyf S."/>
        <person name="Transidico P."/>
        <person name="Bonecchi R."/>
        <person name="Del Prete A."/>
        <person name="Allavena P."/>
        <person name="Ruco L.P."/>
        <person name="Chiabrando C."/>
        <person name="Girolomoni G."/>
        <person name="Mantovani A."/>
        <person name="Sozzani S."/>
      </authorList>
      <dbReference type="PubMed" id="11241286"/>
      <dbReference type="DOI" id="10.1002/1521-4141(200103)31:3&lt;812::aid-immu812&gt;3.0.co;2-l"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>PROTEOLYTIC PROCESSING OF N-TERMINUS</scope>
    <scope>IDENTIFICATION OF MDC(3-69); MDC(5-69) AND MDC(7-69)</scope>
    <source>
      <tissue>Dendritic cell</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>May play a role in the trafficking of activated/effector T-lymphocytes to inflammatory sites and other aspects of activated T-lymphocyte physiology. Chemotactic for monocytes, dendritic cells and natural killer cells. Mild chemoattractant for primary activated T-lymphocytes and a potent chemoattractant for chronically activated T-lymphocytes but has no chemoattractant activity for neutrophils, eosinophils, and resting T-lymphocytes. Binds to CCR4. Processed forms MDC(3-69), MDC(5-69) and MDC(7-69) seem not be active.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-3907871">
      <id>O00626</id>
    </interactant>
    <interactant intactId="EBI-13059134">
      <id>Q13520</id>
      <label>AQP6</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Highly expressed in macrophage and in monocyte-derived dendritic cells, and thymus. Also found in lymph node, appendix, activated monocytes, resting and activated macrophages. Lower expression in lung and spleen. Very weak expression in small intestine. In lymph node expressed in a mature subset of Langerhans' cells (CD1a+ and CD83+). Expressed in Langerhans' cell histiocytosis but not in dermatopathic lymphadenopathy. Expressed in atopic dermatitis, allergic contact dermatitis skin, and psoriasis, in both the epidermis and dermis.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">The N-terminal processed forms MDC(3-69), MDC(5-69) and MDC(7-69) are produced by proteolytic cleavage after secretion from monocyte derived dendrocytes.</text>
  </comment>
  <comment type="similarity">
    <text evidence="9">Belongs to the intercrine beta (chemokine CC) family.</text>
  </comment>
  <comment type="online information" name="Wikipedia">
    <link uri="https://en.wikipedia.org/wiki/CCL22"/>
    <text>CCL22 entry</text>
  </comment>
  <dbReference type="EMBL" id="U83239">
    <property type="protein sequence ID" value="AAB53372.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U83171">
    <property type="protein sequence ID" value="AAB58360.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF064764">
    <property type="protein sequence ID" value="ABK41947.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK311970">
    <property type="protein sequence ID" value="BAG34909.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC004382">
    <property type="protein sequence ID" value="AAC24306.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC009052">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH471092">
    <property type="protein sequence ID" value="EAW82918.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC027952">
    <property type="protein sequence ID" value="AAH27952.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS10778.1"/>
  <dbReference type="RefSeq" id="NP_002981.2">
    <property type="nucleotide sequence ID" value="NM_002990.4"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_016879020.1">
    <property type="nucleotide sequence ID" value="XM_017023531.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O00626"/>
  <dbReference type="SMR" id="O00626"/>
  <dbReference type="BioGRID" id="112270">
    <property type="interactions" value="53"/>
  </dbReference>
  <dbReference type="DIP" id="DIP-5849N"/>
  <dbReference type="IntAct" id="O00626">
    <property type="interactions" value="38"/>
  </dbReference>
  <dbReference type="MINT" id="O00626"/>
  <dbReference type="STRING" id="9606.ENSP00000219235"/>
  <dbReference type="ChEMBL" id="CHEMBL4295649"/>
  <dbReference type="iPTMnet" id="O00626"/>
  <dbReference type="PhosphoSitePlus" id="O00626"/>
  <dbReference type="BioMuta" id="CCL22"/>
  <dbReference type="MassIVE" id="O00626"/>
  <dbReference type="PaxDb" id="9606-ENSP00000219235"/>
  <dbReference type="PeptideAtlas" id="O00626"/>
  <dbReference type="Antibodypedia" id="15019">
    <property type="antibodies" value="514 antibodies from 32 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="6367"/>
  <dbReference type="Ensembl" id="ENST00000219235.5">
    <property type="protein sequence ID" value="ENSP00000219235.4"/>
    <property type="gene ID" value="ENSG00000102962.5"/>
  </dbReference>
  <dbReference type="GeneID" id="6367"/>
  <dbReference type="KEGG" id="hsa:6367"/>
  <dbReference type="MANE-Select" id="ENST00000219235.5">
    <property type="protein sequence ID" value="ENSP00000219235.4"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_002990.5"/>
    <property type="RefSeq protein sequence ID" value="NP_002981.2"/>
  </dbReference>
  <dbReference type="UCSC" id="uc002elh.4">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:10621"/>
  <dbReference type="CTD" id="6367"/>
  <dbReference type="DisGeNET" id="6367"/>
  <dbReference type="GeneCards" id="CCL22"/>
  <dbReference type="HGNC" id="HGNC:10621">
    <property type="gene designation" value="CCL22"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000102962">
    <property type="expression patterns" value="Tissue enhanced (lymphoid tissue, skin)"/>
  </dbReference>
  <dbReference type="MIM" id="602957">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_O00626"/>
  <dbReference type="OpenTargets" id="ENSG00000102962"/>
  <dbReference type="PharmGKB" id="PA35553"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000102962"/>
  <dbReference type="eggNOG" id="ENOG502SWZ0">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT01100000263482"/>
  <dbReference type="HOGENOM" id="CLU_141716_4_2_1"/>
  <dbReference type="InParanoid" id="O00626"/>
  <dbReference type="OMA" id="RDYIRHP"/>
  <dbReference type="OrthoDB" id="4265193at2759"/>
  <dbReference type="PhylomeDB" id="O00626"/>
  <dbReference type="TreeFam" id="TF334888"/>
  <dbReference type="PathwayCommons" id="O00626"/>
  <dbReference type="Reactome" id="R-HSA-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-6783783">
    <property type="pathway name" value="Interleukin-10 signaling"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-6785807">
    <property type="pathway name" value="Interleukin-4 and Interleukin-13 signaling"/>
  </dbReference>
  <dbReference type="SignaLink" id="O00626"/>
  <dbReference type="BioGRID-ORCS" id="6367">
    <property type="hits" value="5 hits in 1144 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="CCL22">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="GeneWiki" id="CCL22"/>
  <dbReference type="GenomeRNAi" id="6367"/>
  <dbReference type="Pharos" id="O00626">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:O00626"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 16"/>
  </dbReference>
  <dbReference type="RNAct" id="O00626">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000102962">
    <property type="expression patterns" value="Expressed in vermiform appendix and 111 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048020">
    <property type="term" value="F:CCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007267">
    <property type="term" value="P:cell-cell signaling"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006935">
    <property type="term" value="P:chemotaxis"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048245">
    <property type="term" value="P:eosinophil chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006955">
    <property type="term" value="P:immune response"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009615">
    <property type="term" value="P:response to virus"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007165">
    <property type="term" value="P:signal transduction"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="CDD" id="cd00272">
    <property type="entry name" value="Chemokine_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000025">
    <property type="entry name" value="C-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000827">
    <property type="entry name" value="Chemokine_CC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF102">
    <property type="entry name" value="C-C MOTIF CHEMOKINE 22"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00436">
    <property type="entry name" value="INTERLEUKIN8"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00472">
    <property type="entry name" value="SMALL_CYTOKINES_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="5">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005224" description="C-C motif chemokine 22">
    <location>
      <begin position="25"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005225" description="MDC(3-69)">
    <location>
      <begin position="27"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005226" description="MDC(5-69)">
    <location>
      <begin position="29"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005227" description="MDC(7-69)">
    <location>
      <begin position="31"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="36"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_055117" description="In dbSNP:rs4359426." evidence="2 4 5 6 7 8">
    <original>D</original>
    <variation>A</variation>
    <location>
      <position position="2"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10493829"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="11241286"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="15489334"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="9151897"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="9312138"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source ref="3"/>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source ref="7"/>
  </evidence>
  <evidence type="ECO:0000305" key="9"/>
  <sequence length="93" mass="10625" checksum="641FCE999586A7D2" modified="2010-11-02" version="2" precursor="true">MDRLQTALLVVLVLLAVALQATEAGPYGANMEDSVCCRDYVRYRLPLRVVKHFYWTSDSCPRPGVVLLTFRDKEICADPRVPWVKMILNKLSQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>