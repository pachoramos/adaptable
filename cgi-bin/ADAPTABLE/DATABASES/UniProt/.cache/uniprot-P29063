<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1992-12-01" modified="2024-11-27" version="97" xmlns="http://uniprot.org/uniprot">
  <accession>P29063</accession>
  <name>PR4B_TOBAC</name>
  <protein>
    <recommendedName>
      <fullName>Pathogenesis-related protein PR-4B</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Nicotiana tabacum</name>
    <name type="common">Common tobacco</name>
    <dbReference type="NCBI Taxonomy" id="4097"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>asterids</taxon>
      <taxon>lamiids</taxon>
      <taxon>Solanales</taxon>
      <taxon>Solanaceae</taxon>
      <taxon>Nicotianoideae</taxon>
      <taxon>Nicotianeae</taxon>
      <taxon>Nicotiana</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="Mol. Gen. Genet." volume="230" first="113" last="119">
      <title>Pathogenesis-related protein 4 is structurally homologous to the carboxy-terminal domains of hevein, Win-1 and Win-2.</title>
      <authorList>
        <person name="Friedrich L."/>
        <person name="Moyer M."/>
        <person name="Ward E."/>
        <person name="Ryals J."/>
      </authorList>
      <dbReference type="PubMed" id="1745223"/>
      <dbReference type="DOI" id="10.1007/bf00290658"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>cv. Xanthi</strain>
      <tissue>Leaf</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
      <location evidence="1">Cell wall</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text>By TMV infection.</text>
  </comment>
  <dbReference type="EMBL" id="X60282">
    <property type="protein sequence ID" value="CAA42821.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_016440550.1">
    <property type="nucleotide sequence ID" value="XM_016585064.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_016440551.1">
    <property type="nucleotide sequence ID" value="XM_016585065.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P29063"/>
  <dbReference type="SMR" id="P29063"/>
  <dbReference type="STRING" id="4097.P29063"/>
  <dbReference type="PaxDb" id="4097-P29063"/>
  <dbReference type="GeneID" id="107766311"/>
  <dbReference type="KEGG" id="nta:107766311"/>
  <dbReference type="OMA" id="VVDQCAN"/>
  <dbReference type="OrthoDB" id="1204437at2759"/>
  <dbReference type="Proteomes" id="UP000084051">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004540">
    <property type="term" value="F:RNA nuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd22777">
    <property type="entry name" value="DPBB_barwin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.40.10:FF:000007">
    <property type="entry name" value="Papaya barwin-like protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.40.10">
    <property type="entry name" value="RlpA-like domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018226">
    <property type="entry name" value="Barwin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001153">
    <property type="entry name" value="Barwin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044301">
    <property type="entry name" value="PR4"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036908">
    <property type="entry name" value="RlpA-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46351:SF6">
    <property type="entry name" value="PATHOGENESIS-RELATED PROTEIN P2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46351">
    <property type="entry name" value="WOUND-INDUCED PROTEIN WIN2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00967">
    <property type="entry name" value="Barwin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00602">
    <property type="entry name" value="BARWIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50685">
    <property type="entry name" value="Barwin-like endoglucanases"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00771">
    <property type="entry name" value="BARWIN_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00772">
    <property type="entry name" value="BARWIN_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51174">
    <property type="entry name" value="BARWIN_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0134">Cell wall</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0568">Pathogenesis-related protein</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000002792" description="Pathogenesis-related protein PR-4B">
    <location>
      <begin position="26"/>
      <end position="147"/>
    </location>
  </feature>
  <feature type="domain" description="Barwin" evidence="3">
    <location>
      <begin position="26"/>
      <end position="147"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="54"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="75"/>
      <end position="109"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="89"/>
      <end position="145"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00527"/>
    </source>
  </evidence>
  <sequence length="147" mass="16235" checksum="FEF7579E1FB2C874" modified="1992-12-01" version="1" precursor="true">MERVNNYKLCVALLIMSVMMAMAAAQSATNVRSTYHLYNPQNINWDLRAASAFCATWDADKPLAWRQKYGWTAFCGPAGPRGQDSCGRCLRVTNTGTGTQATVRIVDQCSNGGLDLDVNVFNQLDTNGLGYQQGHLIVNYEFVNCND</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>