<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-10-31" modified="2022-05-25" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>B3A0M9</accession>
  <name>ES2JB_ODOJI</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Esculentin-2JDb</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Odorrana jingdongensis</name>
    <name type="common">Jingdong frog</name>
    <name type="synonym">Rana jingdongensis</name>
    <dbReference type="NCBI Taxonomy" id="431936"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Odorrana</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2012" name="J. Proteomics" volume="75" first="5807" last="5821">
      <title>Antimicrobial peptides from the skin of the Asian frog, Odorrana jingdongensis: De novo sequencing and analysis of tandem mass spectrometry data.</title>
      <authorList>
        <person name="Liu J."/>
        <person name="Jiang J."/>
        <person name="Wu Z."/>
        <person name="Xie F."/>
      </authorList>
      <dbReference type="PubMed" id="22917879"/>
      <dbReference type="DOI" id="10.1016/j.jprot.2012.08.004"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PROBABLE FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="2">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 4">Has antibacterial activity against E.coli and S.aureus strains.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3818.1155" error="0.0013" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the frog skin active peptide (FSAP) family. Esculentin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=01866"/>
  </comment>
  <dbReference type="AlphaFoldDB" id="B3A0M9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000420134" description="Esculentin-2JDb" evidence="2">
    <location>
      <begin position="1"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="31"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="2">
    <location>
      <position position="12"/>
    </location>
  </feature>
  <feature type="unsure residue" description="I or L" evidence="2">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <feature type="unsure residue" description="G or K" evidence="2">
    <location>
      <position position="14"/>
    </location>
  </feature>
  <feature type="unsure residue" description="K or G" evidence="2">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <feature type="unsure residue" description="G or K" evidence="2">
    <location>
      <position position="22"/>
    </location>
  </feature>
  <feature type="unsure residue" description="K or G" evidence="2">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="2">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="2">
    <location>
      <position position="28"/>
    </location>
  </feature>
  <feature type="unsure residue" description="I or L" evidence="2">
    <location>
      <position position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="22917879"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="22917879"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="22917879"/>
    </source>
  </evidence>
  <sequence length="37" mass="3822" checksum="BF7A58B931768D4A" modified="2012-10-31" version="1">GIFTLIKGAAKLIGKTVAKEAGKTGLELMACKITNQC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>