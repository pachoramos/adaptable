<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-03-27" version="76" xmlns="http://uniprot.org/uniprot">
  <accession>P43443</accession>
  <name>NMB_XENLA</name>
  <protein>
    <recommendedName>
      <fullName>Neuromedin-B</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">nmb</name>
  </gene>
  <organism>
    <name type="scientific">Xenopus laevis</name>
    <name type="common">African clawed frog</name>
    <dbReference type="NCBI Taxonomy" id="8355"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Pipoidea</taxon>
      <taxon>Pipidae</taxon>
      <taxon>Xenopodinae</taxon>
      <taxon>Xenopus</taxon>
      <taxon>Xenopus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="Proc. Natl. Acad. Sci. U.S.A." volume="89" first="9819" last="9822">
      <title>Isolation and sequence of a cDNA encoding the precursor of a bombesin-like peptide from brain and early embryos of Xenopus laevis.</title>
      <authorList>
        <person name="Wechselberger C."/>
        <person name="Kreil G."/>
        <person name="Richter K."/>
      </authorList>
      <dbReference type="PubMed" id="1409705"/>
      <dbReference type="DOI" id="10.1073/pnas.89.20.9819"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Stimulates smooth muscle contraction.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Brain, intestine, and ovaries and early embryos (stages 2 and 10).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the bombesin/neuromedin-B/ranatensin family.</text>
  </comment>
  <dbReference type="EMBL" id="L01530">
    <property type="protein sequence ID" value="AAA49912.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A47201">
    <property type="entry name" value="A47201"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001079342.1">
    <property type="nucleotide sequence ID" value="NM_001085873.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P43443"/>
  <dbReference type="SMR" id="P43443"/>
  <dbReference type="GeneID" id="378672"/>
  <dbReference type="KEGG" id="xla:378672"/>
  <dbReference type="AGR" id="Xenbase:XB-GENE-5967572"/>
  <dbReference type="CTD" id="378672"/>
  <dbReference type="Xenbase" id="XB-GENE-5967572">
    <property type="gene designation" value="nmb.S"/>
  </dbReference>
  <dbReference type="OrthoDB" id="4741465at2759"/>
  <dbReference type="Proteomes" id="UP000186698">
    <property type="component" value="Chromosome 3S"/>
  </dbReference>
  <dbReference type="Bgee" id="378672">
    <property type="expression patterns" value="Expressed in brain and 11 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000874">
    <property type="entry name" value="Bombesin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02044">
    <property type="entry name" value="Bombesin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00257">
    <property type="entry name" value="BOMBESIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003024" evidence="5">
    <location>
      <begin position="30"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003025" description="Neuromedin-B" evidence="1">
    <location>
      <begin position="45"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003026" evidence="1">
    <location>
      <begin position="58"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="1">
    <location>
      <position position="54"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01297"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q9CR53"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="1409705"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="120" mass="14384" checksum="2CB146BA082A2442" modified="1995-11-01" version="1" precursor="true">MSAVPLTRMLPLRFLTHLLLLSFIPLYFCMEFSEDARNIEKIRRGNQWAIGHFMGKKSLQDTYNPSEQDMDSEDFRPRIIEMIRGTFRQEPIRALSPRKQDEIQWMLKKIMDDYIKTTQK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>