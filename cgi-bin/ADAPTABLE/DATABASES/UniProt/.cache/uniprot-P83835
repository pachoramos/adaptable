<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-26" modified="2023-02-22" version="48" xmlns="http://uniprot.org/uniprot">
  <accession>P83835</accession>
  <accession>P58444</accession>
  <accession>P58450</accession>
  <name>CYO12_VIOAR</name>
  <protein>
    <recommendedName>
      <fullName>Varv peptide E</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Viola arvensis</name>
    <name type="common">European field pansy</name>
    <name type="synonym">Field violet</name>
    <dbReference type="NCBI Taxonomy" id="97415"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Malpighiales</taxon>
      <taxon>Violaceae</taxon>
      <taxon>Viola</taxon>
      <taxon>Viola subgen. Viola</taxon>
      <taxon>Viola sect. Melanium</taxon>
      <taxon>Viola subsect. Bracteolatae</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="J. Nat. Prod." volume="62" first="283" last="286">
      <title>Seven novel macrocyclic polypeptides from Viola arvensis.</title>
      <authorList>
        <person name="Goeransson U."/>
        <person name="Luijendijk T."/>
        <person name="Johansson S."/>
        <person name="Bohlin L."/>
        <person name="Claeson P."/>
      </authorList>
      <dbReference type="PubMed" id="10075760"/>
      <dbReference type="DOI" id="10.1021/np9803878"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="J. Nat. Prod." volume="67" first="144" last="147">
      <title>Cytotoxic cyclotides from Viola tricolor.</title>
      <authorList>
        <person name="Svangard E."/>
        <person name="Goransson U."/>
        <person name="Hocaoglu Z."/>
        <person name="Gullbo J."/>
        <person name="Larsson R."/>
        <person name="Claeson P."/>
        <person name="Bohlin L."/>
      </authorList>
      <dbReference type="PubMed" id="14987049"/>
      <dbReference type="DOI" id="10.1021/np030101l"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3">Probably participates in a plant defense mechanism. Has cytotoxic activity against human lymphoma U-937 GTB and human myeloma RPMI-8226/s cell lines.</text>
  </comment>
  <comment type="domain">
    <text>The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text>This is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="2894.9" error="3.0" method="MALDI" evidence="2"/>
  <comment type="mass spectrometry" mass="2892.2" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the cyclotide family. Moebius subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="4">This peptide is cyclic. The start position was chosen by similarity to OAK1 (kalata-B1) for which the DNA sequence is known.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P83835"/>
  <dbReference type="SMR" id="P83835"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012324">
    <property type="entry name" value="Cyclotide_moebius_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF037891">
    <property type="entry name" value="Cycloviolacin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60009">
    <property type="entry name" value="CYCLOTIDE_MOEBIUS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000043618" description="Varv peptide E">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="5"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="9"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="14"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Gly-Asn)">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10075760"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="14987049"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="29" mass="2916" checksum="323641013F82FA18" modified="2004-04-26" version="1">GLPICGETCVGGTCNTPGCSCSWPVCTRN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>