<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-04-29" modified="2024-07-24" version="71" xmlns="http://uniprot.org/uniprot">
  <accession>Q8WNQ9</accession>
  <name>HCST_MACMU</name>
  <protein>
    <recommendedName>
      <fullName>Hematopoietic cell signal transducer</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>DNAX-activation protein 10</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Membrane protein DAP10</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">HCST</name>
    <name type="synonym">DAP10</name>
  </gene>
  <organism>
    <name type="scientific">Macaca mulatta</name>
    <name type="common">Rhesus macaque</name>
    <dbReference type="NCBI Taxonomy" id="9544"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Macaca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2000-11" db="EMBL/GenBank/DDBJ databases">
      <title>Identification of Rhesus Monkey DAP10 and DAP12.</title>
      <authorList>
        <person name="LaBonte M.L."/>
        <person name="Letvin N.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Transmembrane adapter protein which associates with KLRK1 to form an activation receptor KLRK1-HCST in lymphoid and myeloid cells; this receptor plays a major role in triggering cytotoxicity against target cells expressing cell surface ligands such as MHC class I chain-related MICA and MICB, and UL16-binding proteins (ULBPs); these ligands are up-regulated by stress conditions and pathological state such as viral infection and tumor transformation. Functions as a docking site for PI3-kinase PIK3R1 and GRB2. Interaction of ULBPs with KLRK1-HCST triggers calcium mobilization and activation of the PIK3R1, MAP2K/ERK, and JAK2/STAT5 signaling pathways. Both PIK3R1 and GRB2 are required for full KLRK1-HCST-mediated activation and ultimate killing of target cells. In NK cells, KLRK1-HCST signaling directly induces cytotoxicity and enhances cytokine production initiated via DAP12/TYROBP-associated receptors. In T-cells, it provides primarily costimulation for TCR-induced signals. KLRK1-HCST receptor plays a role in immune surveillance against tumors and is required for cytolysis of tumors cells; indeed, melanoma cells that do not express KLRK1 ligands escape from immune surveillance mediated by NK cells (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 2">Homodimer; Disulfide-linked. Heterohexamer composed of four subunits of HCST/DAP10 and two subunits of KLRK1. Interacts (via transmembrane domain) with KLRK1 (via transmembrane domain); the interaction is required for KLRK1 NK cell surface and induces NK cell-mediated cytotoxicity. Interacts with PIK3R1 and GRB2. Interacts with CLEC5A. Forms an CLEC5A/TYROBP/HCST trimolecular complex depending almost solely on TYROBP (By similarity). Interacts with CD300H (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Membrane</location>
      <topology evidence="4">Single-pass type I membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="PTM">
    <text evidence="1">Phosphorylated; PIK3R1 and GRB2 associate specifically with tyrosine-phosphorylated HCST.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">O-glycosylated.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the DAP10 family.</text>
  </comment>
  <dbReference type="EMBL" id="AF321610">
    <property type="protein sequence ID" value="AAL37223.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001028007.1">
    <property type="nucleotide sequence ID" value="NM_001032835.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8WNQ9"/>
  <dbReference type="SMR" id="Q8WNQ9"/>
  <dbReference type="STRING" id="9544.ENSMMUP00000025981"/>
  <dbReference type="GeneID" id="574152"/>
  <dbReference type="KEGG" id="mcc:574152"/>
  <dbReference type="CTD" id="10870"/>
  <dbReference type="InParanoid" id="Q8WNQ9"/>
  <dbReference type="OrthoDB" id="5295812at2759"/>
  <dbReference type="Proteomes" id="UP000006718">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009986">
    <property type="term" value="C:cell surface"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043548">
    <property type="term" value="F:phosphatidylinositol 3-kinase binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005102">
    <property type="term" value="F:signaling receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051897">
    <property type="term" value="P:positive regulation of phosphatidylinositol 3-kinase/protein kinase B signal transduction"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050776">
    <property type="term" value="P:regulation of immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009861">
    <property type="entry name" value="HCST"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21409">
    <property type="entry name" value="HEMATOPOIETIC CELL SIGNAL TRANSDUCER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21409:SF1">
    <property type="entry name" value="HEMATOPOIETIC CELL SIGNAL TRANSDUCER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07213">
    <property type="entry name" value="DAP10"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000330289" description="Hematopoietic cell signal transducer">
    <location>
      <begin position="19"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="topological domain" description="Extracellular" evidence="3">
    <location>
      <begin position="19"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="3">
    <location>
      <begin position="35"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="topological domain" description="Cytoplasmic" evidence="3">
    <location>
      <begin position="56"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="region of interest" description="PIK3R1 binding site" evidence="1">
    <location>
      <begin position="72"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="region of interest" description="GRB2 binding site" evidence="1">
    <location>
      <begin position="72"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphotyrosine" evidence="2">
    <location>
      <position position="72"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q9UBK5"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="79" mass="7934" checksum="EACECCD07838B8C9" modified="2002-03-01" version="1" precursor="true">MIHPGHILFLLLLPVAAAQTTPGSCSGCGSLSLPLLAGLVAADAVASPLIVGAVFLCARPRRSPAQGDGKVYINMPGRG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>