<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1991-08-01" modified="2024-11-27" version="202" xmlns="http://uniprot.org/uniprot">
  <accession>P22466</accession>
  <accession>Q14413</accession>
  <name>GALA_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Galanin peptides</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Galanin</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Galanin message-associated peptide</fullName>
        <shortName>GMAP</shortName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">GAL</name>
    <name type="synonym">GAL1</name>
    <name type="synonym">GALN</name>
    <name type="synonym">GLNN</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Genomics" volume="18" first="473" last="477">
      <title>Genomic organization and localization of the gene encoding human preprogalanin.</title>
      <authorList>
        <person name="Evans H."/>
        <person name="Baumgartner M."/>
        <person name="Shine J."/>
        <person name="Herzog H."/>
      </authorList>
      <dbReference type="PubMed" id="7508413"/>
      <dbReference type="DOI" id="10.1016/s0888-7543(11)80002-9"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] OF 1-45</scope>
    <source>
      <tissue>Blood</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1992" name="Diabetes" volume="41" first="82" last="87">
      <title>Sequence of human galanin and its inhibition of glucose-stimulated insulin secretion from RIN cells.</title>
      <authorList>
        <person name="McKnight G.L."/>
        <person name="Karlsen A.E."/>
        <person name="Kowalyk S."/>
        <person name="Mathewes S.L."/>
        <person name="Sheppard P.O."/>
        <person name="O'Hara P.J."/>
        <person name="Taborsky G.L."/>
      </authorList>
      <dbReference type="PubMed" id="1370155"/>
      <dbReference type="DOI" id="10.2337/diab.41.1.82"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] OF 16-123</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Cervix</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1991" name="FEBS Lett." volume="283" first="189" last="194">
      <title>Human galanin: primary structure and identification of two molecular forms.</title>
      <authorList>
        <person name="Bersani M."/>
        <person name="Johnsen A.H."/>
        <person name="Hoejrup P."/>
        <person name="Dunning B.E."/>
        <person name="Andreasen J.J."/>
        <person name="Holst J.J."/>
      </authorList>
      <dbReference type="PubMed" id="1710578"/>
      <dbReference type="DOI" id="10.1016/0014-5793(91)80585-q"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 33-62</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1991" name="Proc. Natl. Acad. Sci. U.S.A." volume="88" first="11435" last="11439">
      <title>Isolation and primary structure of pituitary human galanin, a 30-residue nonamidated neuropeptide.</title>
      <authorList>
        <person name="Schmidt W.E."/>
        <person name="Kratzin H."/>
        <person name="Eckart K."/>
        <person name="Drevs D."/>
        <person name="Mundkowski G."/>
        <person name="Clemens A."/>
        <person name="Katsoulis S."/>
        <person name="Schaefer H."/>
        <person name="Gallwitz B."/>
        <person name="Creutzfeldt W."/>
      </authorList>
      <dbReference type="PubMed" id="1722333"/>
      <dbReference type="DOI" id="10.1073/pnas.88.24.11435"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 33-62</scope>
    <scope>SYNTHESIS</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Pituitary</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2004" name="Proteomics" volume="4" first="587" last="598">
      <title>Identification and characterization of phosphorylated proteins in the human pituitary.</title>
      <authorList>
        <person name="Giorgianni F."/>
        <person name="Beranova-Giorgianni S."/>
        <person name="Desiderio D.M."/>
      </authorList>
      <dbReference type="PubMed" id="14997482"/>
      <dbReference type="DOI" id="10.1002/pmic.200300584"/>
    </citation>
    <scope>PHOSPHORYLATION AT SER-117</scope>
    <source>
      <tissue>Pituitary</tissue>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2006" name="Pituitary" volume="9" first="109" last="120">
      <title>Phosphoproteomic analysis of the human pituitary.</title>
      <authorList>
        <person name="Beranova-Giorgianni S."/>
        <person name="Zhao Y."/>
        <person name="Desiderio D.M."/>
        <person name="Giorgianni F."/>
      </authorList>
      <dbReference type="PubMed" id="16807684"/>
      <dbReference type="DOI" id="10.1007/s11102-006-8916-x"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Pituitary</tissue>
    </source>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2008" name="Proc. Natl. Acad. Sci. U.S.A." volume="105" first="10762" last="10767">
      <title>A quantitative atlas of mitotic phosphorylation.</title>
      <authorList>
        <person name="Dephoure N."/>
        <person name="Zhou C."/>
        <person name="Villen J."/>
        <person name="Beausoleil S.A."/>
        <person name="Bakalarski C.E."/>
        <person name="Elledge S.J."/>
        <person name="Gygi S.P."/>
      </authorList>
      <dbReference type="PubMed" id="18669648"/>
      <dbReference type="DOI" id="10.1073/pnas.0805139105"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Cervix carcinoma</tissue>
    </source>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2011" name="Sci. Signal." volume="4" first="RS3" last="RS3">
      <title>System-wide temporal characterization of the proteome and phosphoproteome of human embryonic stem cell differentiation.</title>
      <authorList>
        <person name="Rigbolt K.T."/>
        <person name="Prokhorova T.A."/>
        <person name="Akimov V."/>
        <person name="Henningsen J."/>
        <person name="Johansen P.T."/>
        <person name="Kratchmarova I."/>
        <person name="Kassem M."/>
        <person name="Mann M."/>
        <person name="Olsen J.V."/>
        <person name="Blagoev B."/>
      </authorList>
      <dbReference type="PubMed" id="21406692"/>
      <dbReference type="DOI" id="10.1126/scisignal.2001570"/>
    </citation>
    <scope>PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-117</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2013" name="J. Proteome Res." volume="12" first="260" last="271">
      <title>Toward a comprehensive characterization of a human cancer cell phosphoproteome.</title>
      <authorList>
        <person name="Zhou H."/>
        <person name="Di Palma S."/>
        <person name="Preisinger C."/>
        <person name="Peng M."/>
        <person name="Polat A.N."/>
        <person name="Heck A.J."/>
        <person name="Mohammed S."/>
      </authorList>
      <dbReference type="PubMed" id="23186163"/>
      <dbReference type="DOI" id="10.1021/pr300630k"/>
    </citation>
    <scope>PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-116</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Cervix carcinoma</tissue>
    </source>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2014" name="J. Proteomics" volume="96" first="253" last="262">
      <title>An enzyme assisted RP-RPLC approach for in-depth analysis of human liver phosphoproteome.</title>
      <authorList>
        <person name="Bian Y."/>
        <person name="Song C."/>
        <person name="Cheng K."/>
        <person name="Dong M."/>
        <person name="Wang F."/>
        <person name="Huang J."/>
        <person name="Sun D."/>
        <person name="Wang L."/>
        <person name="Ye M."/>
        <person name="Zou H."/>
      </authorList>
      <dbReference type="PubMed" id="24275569"/>
      <dbReference type="DOI" id="10.1016/j.jprot.2013.11.014"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2015" name="Hum. Mol. Genet." volume="24" first="3082" last="3091">
      <title>Galanin pathogenic mutations in temporal lobe epilepsy.</title>
      <authorList>
        <person name="Guipponi M."/>
        <person name="Chentouf A."/>
        <person name="Webling K.E."/>
        <person name="Freimann K."/>
        <person name="Crespel A."/>
        <person name="Nobile C."/>
        <person name="Lemke J.R."/>
        <person name="Hansen J."/>
        <person name="Dorn T."/>
        <person name="Lesca G."/>
        <person name="Ryvlin P."/>
        <person name="Hirsch E."/>
        <person name="Rudolf G."/>
        <person name="Rosenberg D.S."/>
        <person name="Weber Y."/>
        <person name="Becker F."/>
        <person name="Helbig I."/>
        <person name="Muhle H."/>
        <person name="Salzmann A."/>
        <person name="Chaouch M."/>
        <person name="Oubaiche M.L."/>
        <person name="Ziglio S."/>
        <person name="Gehrig C."/>
        <person name="Santoni F."/>
        <person name="Pizzato M."/>
        <person name="Langel U."/>
        <person name="Antonarakis S.E."/>
      </authorList>
      <dbReference type="PubMed" id="25691535"/>
      <dbReference type="DOI" id="10.1093/hmg/ddv060"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INVOLVEMENT IN ETL8</scope>
    <scope>VARIANT ETL8 GLU-39</scope>
    <scope>CHARACTERIZATION OF VARIANT ETL8 GLU-39</scope>
  </reference>
  <comment type="function">
    <text evidence="3 5 6">Endocrine hormone of the central and peripheral nervous systems that binds and activates the G protein-coupled receptors GALR1, GALR2, and GALR3. This small neuropeptide may regulate diverse physiologic functions including contraction of smooth muscle of the gastrointestinal and genitourinary tract, growth hormone and insulin release and adrenal secretion.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624768">
      <id>P22466</id>
    </interactant>
    <interactant intactId="EBI-296087">
      <id>P31749</id>
      <label>AKT1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624768">
      <id>P22466</id>
    </interactant>
    <interactant intactId="EBI-12092171">
      <id>Q12797-6</id>
      <label>ASPH</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624768">
      <id>P22466</id>
    </interactant>
    <interactant intactId="EBI-12275524">
      <id>P23560-2</id>
      <label>BDNF</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624768">
      <id>P22466</id>
    </interactant>
    <interactant intactId="EBI-740135">
      <id>P35520</id>
      <label>CBS</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624768">
      <id>P22466</id>
    </interactant>
    <interactant intactId="EBI-347996">
      <id>O43765</id>
      <label>SGTA</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>6</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624768">
      <id>P22466</id>
    </interactant>
    <interactant intactId="EBI-357085">
      <id>Q9UNE7</id>
      <label>STUB1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624768">
      <id>P22466</id>
    </interactant>
    <interactant intactId="EBI-741480">
      <id>Q9UMX0</id>
      <label>UBQLN1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624768">
      <id>P22466</id>
    </interactant>
    <interactant intactId="EBI-947187">
      <id>Q9UHD9</id>
      <label>UBQLN2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624800">
      <id>PRO_0000010449</id>
    </interactant>
    <interactant intactId="EBI-6624741">
      <id>P47211</id>
      <label>GALR1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-6624800">
      <id>PRO_0000010449</id>
    </interactant>
    <interactant intactId="EBI-6624855">
      <id>O43603</id>
      <label>GALR2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3 5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="disease" evidence="6">
    <disease id="DI-04482">
      <name>Epilepsy, familial temporal lobe, 8</name>
      <acronym>ETL8</acronym>
      <description>A focal form of epilepsy characterized by recurrent seizures that arise from foci within the temporal lobe. Seizures are usually accompanied by sensory symptoms, most often auditory in nature.</description>
      <dbReference type="MIM" id="616461"/>
    </disease>
    <text>The disease is caused by variants affecting the gene represented in this entry.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the galanin family.</text>
  </comment>
  <dbReference type="EMBL" id="L11144">
    <property type="protein sequence ID" value="AAA18248.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M77140">
    <property type="protein sequence ID" value="AAA60178.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC030241">
    <property type="protein sequence ID" value="AAH30241.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS8183.1"/>
  <dbReference type="PIR" id="A49353">
    <property type="entry name" value="RHHUN"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_057057.2">
    <property type="nucleotide sequence ID" value="NM_015973.4"/>
  </dbReference>
  <dbReference type="PDB" id="7S3O">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=34-46"/>
  </dbReference>
  <dbReference type="PDB" id="7S3Q">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=33-46"/>
  </dbReference>
  <dbReference type="PDB" id="7S3R">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=33-50"/>
  </dbReference>
  <dbReference type="PDB" id="7WQ3">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.70 A"/>
    <property type="chains" value="L=33-62"/>
  </dbReference>
  <dbReference type="PDB" id="7WQ4">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.60 A"/>
    <property type="chains" value="L=33-62"/>
  </dbReference>
  <dbReference type="PDB" id="7XBD">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.11 A"/>
    <property type="chains" value="F=33-62"/>
  </dbReference>
  <dbReference type="PDB" id="7XJJ">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.30 A"/>
    <property type="chains" value="C=33-62"/>
  </dbReference>
  <dbReference type="PDB" id="7XJK">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.30 A"/>
    <property type="chains" value="A=33-62"/>
  </dbReference>
  <dbReference type="PDB" id="8DHZ">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=49-62"/>
  </dbReference>
  <dbReference type="PDB" id="8DJ4">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=33-62"/>
  </dbReference>
  <dbReference type="PDBsum" id="7S3O"/>
  <dbReference type="PDBsum" id="7S3Q"/>
  <dbReference type="PDBsum" id="7S3R"/>
  <dbReference type="PDBsum" id="7WQ3"/>
  <dbReference type="PDBsum" id="7WQ4"/>
  <dbReference type="PDBsum" id="7XBD"/>
  <dbReference type="PDBsum" id="7XJJ"/>
  <dbReference type="PDBsum" id="7XJK"/>
  <dbReference type="PDBsum" id="8DHZ"/>
  <dbReference type="PDBsum" id="8DJ4"/>
  <dbReference type="AlphaFoldDB" id="P22466"/>
  <dbReference type="EMDB" id="EMD-32698"/>
  <dbReference type="EMDB" id="EMD-33229"/>
  <dbReference type="SMR" id="P22466"/>
  <dbReference type="BioGRID" id="119273">
    <property type="interactions" value="25"/>
  </dbReference>
  <dbReference type="IntAct" id="P22466">
    <property type="interactions" value="29"/>
  </dbReference>
  <dbReference type="MINT" id="P22466"/>
  <dbReference type="STRING" id="9606.ENSP00000265643"/>
  <dbReference type="BindingDB" id="P22466"/>
  <dbReference type="GlyGen" id="P22466">
    <property type="glycosylation" value="2 sites, 1 O-linked glycan (2 sites)"/>
  </dbReference>
  <dbReference type="iPTMnet" id="P22466"/>
  <dbReference type="PhosphoSitePlus" id="P22466"/>
  <dbReference type="BioMuta" id="GAL"/>
  <dbReference type="DMDM" id="2506449"/>
  <dbReference type="jPOST" id="P22466"/>
  <dbReference type="MassIVE" id="P22466"/>
  <dbReference type="PaxDb" id="9606-ENSP00000265643"/>
  <dbReference type="PeptideAtlas" id="P22466"/>
  <dbReference type="ProteomicsDB" id="53995"/>
  <dbReference type="Pumba" id="P22466"/>
  <dbReference type="ABCD" id="P22466">
    <property type="antibodies" value="16 sequenced antibodies"/>
  </dbReference>
  <dbReference type="Antibodypedia" id="30599">
    <property type="antibodies" value="356 antibodies from 36 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="51083"/>
  <dbReference type="Ensembl" id="ENST00000265643.4">
    <property type="protein sequence ID" value="ENSP00000265643.3"/>
    <property type="gene ID" value="ENSG00000069482.7"/>
  </dbReference>
  <dbReference type="GeneID" id="51083"/>
  <dbReference type="KEGG" id="hsa:51083"/>
  <dbReference type="MANE-Select" id="ENST00000265643.4">
    <property type="protein sequence ID" value="ENSP00000265643.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_015973.5"/>
    <property type="RefSeq protein sequence ID" value="NP_057057.2"/>
  </dbReference>
  <dbReference type="UCSC" id="uc001oob.4">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:4114"/>
  <dbReference type="CTD" id="51083"/>
  <dbReference type="DisGeNET" id="51083"/>
  <dbReference type="GeneCards" id="GAL"/>
  <dbReference type="HGNC" id="HGNC:4114">
    <property type="gene designation" value="GAL"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000069482">
    <property type="expression patterns" value="Tissue enriched (pituitary)"/>
  </dbReference>
  <dbReference type="MalaCards" id="GAL"/>
  <dbReference type="MIM" id="137035">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="MIM" id="616461">
    <property type="type" value="phenotype"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P22466"/>
  <dbReference type="OpenTargets" id="ENSG00000069482"/>
  <dbReference type="PharmGKB" id="PA28529"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000069482"/>
  <dbReference type="eggNOG" id="ENOG502RZ1E">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000009663"/>
  <dbReference type="HOGENOM" id="CLU_166244_0_0_1"/>
  <dbReference type="InParanoid" id="P22466"/>
  <dbReference type="OMA" id="PHAVDSH"/>
  <dbReference type="OrthoDB" id="4119714at2759"/>
  <dbReference type="PhylomeDB" id="P22466"/>
  <dbReference type="TreeFam" id="TF335850"/>
  <dbReference type="PathwayCommons" id="P22466"/>
  <dbReference type="Reactome" id="R-HSA-375276">
    <property type="pathway name" value="Peptide ligand-binding receptors"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="SignaLink" id="P22466"/>
  <dbReference type="SIGNOR" id="P22466"/>
  <dbReference type="BioGRID-ORCS" id="51083">
    <property type="hits" value="22 hits in 1154 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="GAL">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="GeneWiki" id="Galanin"/>
  <dbReference type="GenomeRNAi" id="51083"/>
  <dbReference type="Pharos" id="P22466">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P22466"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 11"/>
  </dbReference>
  <dbReference type="RNAct" id="P22466">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000069482">
    <property type="expression patterns" value="Expressed in adenohypophysis and 105 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043025">
    <property type="term" value="C:neuronal cell body"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030141">
    <property type="term" value="C:secretory granule"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004966">
    <property type="term" value="F:galanin receptor activity"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031763">
    <property type="term" value="F:galanin receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031764">
    <property type="term" value="F:type 1 galanin receptor binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031765">
    <property type="term" value="F:type 2 galanin receptor binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031766">
    <property type="term" value="F:type 3 galanin receptor binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019933">
    <property type="term" value="P:cAMP-mediated signaling"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030073">
    <property type="term" value="P:insulin secretion"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050672">
    <property type="term" value="P:negative regulation of lymphocyte proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043065">
    <property type="term" value="P:positive regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051464">
    <property type="term" value="P:positive regulation of cortisol secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1902608">
    <property type="term" value="P:positive regulation of large conductance calcium-activated potassium channel activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051795">
    <property type="term" value="P:positive regulation of timing of catagen"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045944">
    <property type="term" value="P:positive regulation of transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010737">
    <property type="term" value="P:protein kinase A signaling"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031943">
    <property type="term" value="P:regulation of glucocorticoid metabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043627">
    <property type="term" value="P:response to estrogen"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035902">
    <property type="term" value="P:response to immobilization stress"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032868">
    <property type="term" value="P:response to insulin"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009410">
    <property type="term" value="P:response to xenobiotic stimulus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008174">
    <property type="entry name" value="Galanin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008175">
    <property type="entry name" value="Galanin_pre"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013068">
    <property type="entry name" value="GMAP"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16839">
    <property type="entry name" value="GALANIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16839:SF1">
    <property type="entry name" value="GALANIN PEPTIDES"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01296">
    <property type="entry name" value="Galanin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06540">
    <property type="entry name" value="GMAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00273">
    <property type="entry name" value="GALANIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00071">
    <property type="entry name" value="Galanin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00861">
    <property type="entry name" value="GALANIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0225">Disease variant</keyword>
  <keyword id="KW-0887">Epilepsy</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000010448">
    <location>
      <begin position="20"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000010449" description="Galanin">
    <location>
      <begin position="33"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000010450" description="Galanin message-associated peptide">
    <location>
      <begin position="65"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="46"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="2">
    <location>
      <begin position="57"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="9">
    <location>
      <position position="116"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="4 8">
    <location>
      <position position="117"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_049121" description="In dbSNP:rs34725707.">
    <original>A</original>
    <variation>V</variation>
    <location>
      <position position="16"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_074671" description="In ETL8; decreased affinity for GALR2; but no effect on affinity for GALR1 and GALR3; decreased activity in GALR2-mediated signaling; dominant-negative that inhibits GALR1-mediated signaling; dbSNP:rs1057517661." evidence="6">
    <original>A</original>
    <variation>E</variation>
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="36"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="helix" evidence="11">
    <location>
      <begin position="45"/>
      <end position="48"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="1370155"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="14997482"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="1722333"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="25691535"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0007744" key="8">
    <source>
      <dbReference type="PubMed" id="21406692"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="9">
    <source>
      <dbReference type="PubMed" id="23186163"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="10">
    <source>
      <dbReference type="PDB" id="7WQ4"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="11">
    <source>
      <dbReference type="PDB" id="7XJJ"/>
    </source>
  </evidence>
  <sequence length="123" mass="13302" checksum="48D2170AF1248E6D" modified="1997-11-01" version="3" precursor="true">MARGSALLLASLLLAAALSASAGLWSPAKEKRGWTLNSAGYLLGPHAVGNHRSFSDKNGLTSKRELRPEDDMKPGSFDRSIPENNIMRTIIEFLSFLHLKEAGALDRLLDLPAAASSEDIERS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>