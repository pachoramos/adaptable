<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-06-01" modified="2024-05-29" version="81" xmlns="http://uniprot.org/uniprot">
  <accession>Q06015</accession>
  <name>CHI3_ARAHY</name>
  <protein>
    <recommendedName>
      <fullName>Endochitinase 3</fullName>
      <shortName>CHIT 3</shortName>
      <ecNumber>3.2.1.14</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Arachis hypogaea</name>
    <name type="common">Peanut</name>
    <dbReference type="NCBI Taxonomy" id="3818"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Fabales</taxon>
      <taxon>Fabaceae</taxon>
      <taxon>Papilionoideae</taxon>
      <taxon>50 kb inversion clade</taxon>
      <taxon>dalbergioids sensu lato</taxon>
      <taxon>Dalbergieae</taxon>
      <taxon>Pterocarpus clade</taxon>
      <taxon>Arachis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="Mol. Gen. Genet." volume="224" first="469" last="476">
      <title>Elicitor-specific induction of one member of the chitinase gene family in Arachis hypogaea.</title>
      <authorList>
        <person name="Herget T."/>
        <person name="Schell J."/>
        <person name="Schreier P.H."/>
      </authorList>
      <dbReference type="PubMed" id="1980004"/>
      <dbReference type="DOI" id="10.1007/bf00262442"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text>Defense against chitin-containing fungal and bacterial pathogens.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>Random endo-hydrolysis of N-acetyl-beta-D-glucosaminide (1-&gt;4)-beta-linkages in chitin and chitodextrins.</text>
      <dbReference type="EC" id="3.2.1.14"/>
    </reaction>
  </comment>
  <comment type="induction">
    <text>Constitutively expressed at low levels.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the glycosyl hydrolase 19 family. Chitinase class I subfamily.</text>
  </comment>
  <dbReference type="EC" id="3.2.1.14"/>
  <dbReference type="EMBL" id="X56892">
    <property type="protein sequence ID" value="CAA40211.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q06015"/>
  <dbReference type="SMR" id="Q06015"/>
  <dbReference type="CAZy" id="GH19">
    <property type="family name" value="Glycoside Hydrolase Family 19"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008061">
    <property type="term" value="F:chitin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004568">
    <property type="term" value="F:chitinase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016998">
    <property type="term" value="P:cell wall macromolecule catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006032">
    <property type="term" value="P:chitin catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000272">
    <property type="term" value="P:polysaccharide catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.530.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000726">
    <property type="entry name" value="Glyco_hydro_19_cat"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023346">
    <property type="entry name" value="Lysozyme-like_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR22595:SF79">
    <property type="entry name" value="BASIC ENDOCHITINASE B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR22595">
    <property type="entry name" value="CHITINASE-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00182">
    <property type="entry name" value="Glyco_hydro_19"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF53955">
    <property type="entry name" value="Lysozyme-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0119">Carbohydrate metabolism</keyword>
  <keyword id="KW-0146">Chitin degradation</keyword>
  <keyword id="KW-0147">Chitin-binding</keyword>
  <keyword id="KW-0326">Glycosidase</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0624">Polysaccharide degradation</keyword>
  <feature type="chain" id="PRO_0000124822" description="Endochitinase 3">
    <location>
      <begin position="1" status="less than"/>
      <end position="46" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Polar residues" evidence="1">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="46"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="46" mass="4770" checksum="4AD6D203379AD96E" modified="1994-06-01" version="1" fragment="single">MTPQGNKPSSHDVITGRWTPSDADRAAGRVSGFGVITNIINGGLDC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>