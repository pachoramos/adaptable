<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-11-27" version="78" xmlns="http://uniprot.org/uniprot">
  <accession>Q60435</accession>
  <name>UBIM_CRIGR</name>
  <protein>
    <recommendedName>
      <fullName>Ubiquitin-like protein FUBI</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Arsenite-resistance protein</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">FAU</name>
    <name type="synonym">ASR1</name>
  </gene>
  <organism>
    <name type="scientific">Cricetulus griseus</name>
    <name type="common">Chinese hamster</name>
    <name type="synonym">Cricetulus barabensis griseus</name>
    <dbReference type="NCBI Taxonomy" id="10029"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Cricetidae</taxon>
      <taxon>Cricetinae</taxon>
      <taxon>Cricetulus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Carcinogenesis" volume="20" first="311" last="316">
      <title>Expression cloning for arsenite-resistance resulted in isolation of tumor-suppressor fau cDNA: possible involvement of the ubiquitin system in arsenic carcinogenesis.</title>
      <authorList>
        <person name="Rossman T.G."/>
        <person name="Wang Z."/>
      </authorList>
      <dbReference type="PubMed" id="10069470"/>
      <dbReference type="DOI" id="10.1093/carcin/20.2.311"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text>Confers arsenite resistance.</text>
  </comment>
  <comment type="miscellaneous">
    <text>This protein is synthesized with ribosomal S30 as its C-terminal extension.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the ubiquitin family.</text>
  </comment>
  <dbReference type="EMBL" id="U41499">
    <property type="protein sequence ID" value="AAA83776.1"/>
    <property type="status" value="ALT_TERM"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001233753.1">
    <property type="nucleotide sequence ID" value="NM_001246824.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_007652861.1">
    <property type="nucleotide sequence ID" value="XM_007654671.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q60435"/>
  <dbReference type="SMR" id="Q60435"/>
  <dbReference type="PaxDb" id="10029-NP_001233753.1"/>
  <dbReference type="GeneID" id="100689400"/>
  <dbReference type="KEGG" id="cge:100689400"/>
  <dbReference type="CTD" id="2197"/>
  <dbReference type="eggNOG" id="KOG0001">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="eggNOG" id="KOG0009">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="OrthoDB" id="177691at2759"/>
  <dbReference type="Proteomes" id="UP000694386">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP001108280">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="CDD" id="cd01793">
    <property type="entry name" value="Ubl_FUBI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.20.90:FF:000114">
    <property type="entry name" value="40S ribosomal protein S30"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039415">
    <property type="entry name" value="FUBI"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000626">
    <property type="entry name" value="Ubiquitin-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029071">
    <property type="entry name" value="Ubiquitin-like_domsf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019956">
    <property type="entry name" value="Ubiquitin_dom"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00240">
    <property type="entry name" value="ubiquitin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00348">
    <property type="entry name" value="UBIQUITIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00213">
    <property type="entry name" value="UBQ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54236">
    <property type="entry name" value="Ubiquitin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50053">
    <property type="entry name" value="UBIQUITIN_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <feature type="chain" id="PRO_0000114881" description="Ubiquitin-like protein FUBI">
    <location>
      <begin position="1"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000305" key="1"/>
  <sequence length="74" mass="7686" checksum="B1589EF195688B9E" modified="1997-11-01" version="2">MQLFVRAQGLHTLEVTGQETVAQIKAHVASLEGISPEDQVVLLAGSPLEDEATLGQCGVEALTTLEVAGRMLGG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>