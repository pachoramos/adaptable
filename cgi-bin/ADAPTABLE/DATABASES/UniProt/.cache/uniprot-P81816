<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2022-05-25" version="34" xmlns="http://uniprot.org/uniprot">
  <accession>P81816</accession>
  <name>ALL13_CARMA</name>
  <protein>
    <recommendedName>
      <fullName>Carcinustatin-13</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Carcinus maenas</name>
    <name type="common">Common shore crab</name>
    <name type="synonym">Green crab</name>
    <dbReference type="NCBI Taxonomy" id="6759"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Eucarida</taxon>
      <taxon>Decapoda</taxon>
      <taxon>Pleocyemata</taxon>
      <taxon>Brachyura</taxon>
      <taxon>Eubrachyura</taxon>
      <taxon>Portunoidea</taxon>
      <taxon>Carcinidae</taxon>
      <taxon>Carcinus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Eur. J. Biochem." volume="250" first="727" last="734">
      <title>Isolation and identification of multiple neuropeptides of the allatostatin superfamily in the shore crab Carcinus maenas.</title>
      <authorList>
        <person name="Duve H."/>
        <person name="Johnsen A.H."/>
        <person name="Maestro J.-L."/>
        <person name="Scott A.G."/>
        <person name="Jaros P.P."/>
        <person name="Thorpe A."/>
      </authorList>
      <dbReference type="PubMed" id="9461295"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1997.00727.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT LEU-18</scope>
    <source>
      <tissue>Cerebral ganglion</tissue>
      <tissue>Thoracic ganglion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>May act as a neurotransmitter or neuromodulator.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the allatostatin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P81816"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043468" description="Carcinustatin-13">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="18"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="9461295"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="18" mass="2226" checksum="3531112C8160AE27" modified="2000-05-30" version="1">EYDDMYTEKRPKVYAFGL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>