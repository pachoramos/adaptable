<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="159" xmlns="http://uniprot.org/uniprot">
  <accession>P01256</accession>
  <name>CALCA_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Calcitonin gene-related peptide 1</fullName>
      <shortName evidence="2">CGRP1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Alpha-type CGRP</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Calcitonin gene-related peptide I</fullName>
      <shortName>CGRP-I</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="4" type="primary">Calca</name>
    <name type="synonym">Calc</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1985" name="Proc. Natl. Acad. Sci. U.S.A." volume="82" first="1994" last="1998">
      <title>Alternative RNA processing events in human calcitonin/calcitonin gene-related peptide gene expression.</title>
      <authorList>
        <person name="Jonas V."/>
        <person name="Lin C.R."/>
        <person name="Kawashima E."/>
        <person name="Semon D."/>
        <person name="Swanson L.W."/>
        <person name="Mermod J.-J."/>
        <person name="Evans R.M."/>
        <person name="Rosenfeld M.G."/>
      </authorList>
      <dbReference type="PubMed" id="3872459"/>
      <dbReference type="DOI" id="10.1073/pnas.82.7.1994"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1982" name="Nature" volume="298" first="240" last="244">
      <title>Alternative RNA processing in calcitonin gene expression generates mRNAs encoding different polypeptide products.</title>
      <authorList>
        <person name="Amara S.G."/>
        <person name="Jonas V."/>
        <person name="Rosenfeld M.G."/>
        <person name="Ong E.S."/>
        <person name="Evans R.M."/>
      </authorList>
      <dbReference type="PubMed" id="6283379"/>
      <dbReference type="DOI" id="10.1038/298240a0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1985" name="Science" volume="229" first="1094" last="1097">
      <title>Expression in brain of a messenger RNA encoding a novel neuropeptide homologous to calcitonin gene-related peptide.</title>
      <authorList>
        <person name="Amara S.G."/>
        <person name="Arriza J.L."/>
        <person name="Leff S.E."/>
        <person name="Swanson L.W."/>
        <person name="Evans R.M."/>
        <person name="Rosenfeld M.G."/>
      </authorList>
      <dbReference type="PubMed" id="2994212"/>
      <dbReference type="DOI" id="10.1126/science.2994212"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="2">CGRP1/CALCA is a peptide hormone that induces vasodilation mediated by the CALCRL-RAMP1 receptor complex. Dilates a variety of vessels including the coronary, cerebral and systemic vasculature. Its abundance in the CNS also points toward a neurotransmitter or neuromodulator role. It also elevates platelet cAMP. CGRP1 can also bind and activate CALCR-RAMP1 (AMYR1) receptor complex.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Interacts with CALCRL; functions as ligand for the CALCRL-RAMP1 receptor complex which modulates the activity of downstream effectors. Interacts with CALCR; functions as ligand for the CALCR-RAMP1 (AMYR1) receptor complex.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>P01256-1</id>
      <name>Calcitonin-gene related peptide I</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>P01257-1</id>
      <name>Calcitonin</name>
      <sequence type="external"/>
    </isoform>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the calcitonin family.</text>
  </comment>
  <dbReference type="EMBL" id="L29188">
    <property type="protein sequence ID" value="AAB59682.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="L00109">
    <property type="protein sequence ID" value="AAB59682.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="L00110">
    <property type="protein sequence ID" value="AAB59682.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="V01231">
    <property type="protein sequence ID" value="CAA24541.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M11597">
    <property type="protein sequence ID" value="AAA40847.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A01524">
    <property type="entry name" value="TCRTR"/>
  </dbReference>
  <dbReference type="PIR" id="B44173">
    <property type="entry name" value="B44173"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001029127.1">
    <property type="nucleotide sequence ID" value="NM_001033955.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001029128.1">
    <property type="nucleotide sequence ID" value="NM_001033956.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01256"/>
  <dbReference type="BioGRID" id="246427">
    <property type="interactions" value="3"/>
  </dbReference>
  <dbReference type="BindingDB" id="P01256"/>
  <dbReference type="ChEMBL" id="CHEMBL5735"/>
  <dbReference type="ABCD" id="P01256">
    <property type="antibodies" value="1 sequenced antibody"/>
  </dbReference>
  <dbReference type="GeneID" id="24241"/>
  <dbReference type="KEGG" id="rno:24241"/>
  <dbReference type="AGR" id="RGD:2254"/>
  <dbReference type="CTD" id="796"/>
  <dbReference type="RGD" id="2254">
    <property type="gene designation" value="Calca"/>
  </dbReference>
  <dbReference type="OrthoDB" id="4002581at2759"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030424">
    <property type="term" value="C:axon"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0098686">
    <property type="term" value="C:hippocampal mossy fiber to CA3 synapse"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="SynGO"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043005">
    <property type="term" value="C:neuron projection"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043025">
    <property type="term" value="C:neuronal cell body"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0098992">
    <property type="term" value="C:neuronal dense core vesicle"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="SynGO"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043195">
    <property type="term" value="C:terminal bouton"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031716">
    <property type="term" value="F:calcitonin receptor binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044877">
    <property type="term" value="F:protein-containing complex binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005102">
    <property type="term" value="F:signaling receptor binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007189">
    <property type="term" value="P:adenylate cyclase-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001984">
    <property type="term" value="P:artery vasodilation involved in baroreceptor response to increased systemic arterial blood pressure"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990408">
    <property type="term" value="P:calcitonin gene-related peptide receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007155">
    <property type="term" value="P:cell adhesion"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990090">
    <property type="term" value="P:cellular response to nerve growth factor stimulus"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071356">
    <property type="term" value="P:cellular response to tumor necrosis factor"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050965">
    <property type="term" value="P:detection of temperature stimulus involved in sensory perception of pain"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007566">
    <property type="term" value="P:embryo implantation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043542">
    <property type="term" value="P:endothelial cell migration"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001935">
    <property type="term" value="P:endothelial cell proliferation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002031">
    <property type="term" value="P:G protein-coupled receptor internalization"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006874">
    <property type="term" value="P:intracellular calcium ion homeostasis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007159">
    <property type="term" value="P:leukocyte cell-cell adhesion"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002548">
    <property type="term" value="P:monocyte chemotaxis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045776">
    <property type="term" value="P:negative regulation of blood pressure"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045779">
    <property type="term" value="P:negative regulation of bone resorption"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010523">
    <property type="term" value="P:negative regulation of calcium ion transport into cytosol"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045892">
    <property type="term" value="P:negative regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030279">
    <property type="term" value="P:negative regulation of ossification"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045671">
    <property type="term" value="P:negative regulation of osteoclast differentiation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045986">
    <property type="term" value="P:negative regulation of smooth muscle contraction"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001503">
    <property type="term" value="P:ossification"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007200">
    <property type="term" value="P:phospholipase C-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045785">
    <property type="term" value="P:positive regulation of cell adhesion"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007204">
    <property type="term" value="P:positive regulation of cytosolic calcium ion concentration"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032730">
    <property type="term" value="P:positive regulation of interleukin-1 alpha production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032757">
    <property type="term" value="P:positive regulation of interleukin-8 production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045651">
    <property type="term" value="P:positive regulation of macrophage differentiation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031623">
    <property type="term" value="P:receptor internalization"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051480">
    <property type="term" value="P:regulation of cytosolic calcium ion concentration"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009408">
    <property type="term" value="P:response to heat"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048265">
    <property type="term" value="P:response to pain"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006939">
    <property type="term" value="P:smooth muscle contraction"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001944">
    <property type="term" value="P:vasculature development"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042311">
    <property type="term" value="P:vasodilation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.2190">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR021117">
    <property type="entry name" value="Calcitonin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR021116">
    <property type="entry name" value="Calcitonin/adrenomedullin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018360">
    <property type="entry name" value="Calcitonin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015476">
    <property type="entry name" value="Calcitonin_gene-rel_peptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001693">
    <property type="entry name" value="Calcitonin_peptide-like"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10505:SF3">
    <property type="entry name" value="CALCITONIN GENE-RELATED PEPTIDE 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10505">
    <property type="entry name" value="CALCITONIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00214">
    <property type="entry name" value="Calc_CGRP_IAPP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00817">
    <property type="entry name" value="CALCITONINB"/>
  </dbReference>
  <dbReference type="SMART" id="SM00113">
    <property type="entry name" value="CALCITONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00258">
    <property type="entry name" value="CALCITONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000004065">
    <location>
      <begin position="26"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000004066" description="Calcitonin gene-related peptide 1">
    <location>
      <begin position="83"/>
      <end position="119"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000004067">
    <location>
      <begin position="125"/>
      <end position="128"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="119"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="84"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AAB59682/CAA24541 and 3; AAA40847." evidence="3" ref="2 3">
    <location>
      <position position="40"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AAB59682/CAA24541 and 3; AAA40847." evidence="3" ref="2 3">
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AAB59682/CAA24541 and 3; AAA40847." evidence="3" ref="2 3">
    <original>Q</original>
    <variation>EEQ</variation>
    <location>
      <position position="70"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AAA40847." evidence="3" ref="3">
    <original>S</original>
    <variation>R</variation>
    <location>
      <position position="99"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P06881"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000312" key="4">
    <source>
      <dbReference type="RGD" id="2254"/>
    </source>
  </evidence>
  <sequence length="128" mass="13948" checksum="75D14869C17078D3" modified="1988-04-01" version="1" precursor="true">MGFLKFSPFLVVSILLLYQACGLQAVPLRSTLESSPGMAATLSEEEARLLLAALVQNYMQMKVRELEQEQEAEGSSVTAQKRSCNTATCVTHRLAGLLSRSGGVVKDNFVPTNVGSEAFGRRRRDLQA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>