<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2024-11-27" modified="2024-11-27" version="1" xmlns="http://uniprot.org/uniprot">
  <accession>P0DRC9</accession>
  <name>TX44A_SCOMU</name>
  <protein>
    <recommendedName>
      <fullName evidence="4 5">Kappa-scoloptoxin SsmTx-I</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Kappa-scoloptoxin(04)-Ssm4a</fullName>
      <shortName evidence="5">Kappa-SLPTX(04)-Ssm4a</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Scolopendra mutilans</name>
    <name type="common">Chinese red-headed centipede</name>
    <name type="synonym">Scolopendra subspinipes mutilans</name>
    <dbReference type="NCBI Taxonomy" id="2836329"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Myriapoda</taxon>
      <taxon>Chilopoda</taxon>
      <taxon>Pleurostigmophora</taxon>
      <taxon>Scolopendromorpha</taxon>
      <taxon>Scolopendridae</taxon>
      <taxon>Scolopendra</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2017" name="J. Pept. Sci." volume="23" first="384" last="391">
      <title>Centipede venom peptide SsmTX-I with two intramolecular disulfide bonds shows analgesic activities in animal models.</title>
      <authorList>
        <person name="Wang Y."/>
        <person name="Li X."/>
        <person name="Yang M."/>
        <person name="Wu C."/>
        <person name="Zou Z."/>
        <person name="Tang J."/>
        <person name="Yang X."/>
      </authorList>
      <dbReference type="PubMed" id="28247497"/>
      <dbReference type="DOI" id="10.1002/psc.2988"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 38-73</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>SYNTHESIS OF 38-73</scope>
    <scope>BIOASSAY</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2014" name="J. Pept. Sci." volume="20" first="159" last="164">
      <title>Isolation and characterization of SsmTx-I, a specific Kv2.1 blocker from the venom of the centipede Scolopendra Subspinipes Mutilans L. Koch.</title>
      <authorList>
        <person name="Chen M."/>
        <person name="Li J."/>
        <person name="Zhang F."/>
        <person name="Liu Z."/>
      </authorList>
      <dbReference type="PubMed" id="24464516"/>
      <dbReference type="DOI" id="10.1002/psc.2588"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 38-73</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 3">Exhibits highly specific blockage of Kv2.1/KCNB1 (IC(50)=41.7 nM) voltage-gated potassium channels (PubMed:24464516). This blockage is not associated with a significant change in steady-state activation, suggesting that this toxin acts as a channel blocker rather than a gating-modifier (PubMed:24464516). Shows potential analgesic activities in formalin-induced paw licking, thermal pain, and acetic acid-induced abdominal writhing mice models (PubMed:28247497).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2 3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6 7">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="4114.6" method="MALDI" evidence="3">
    <text>Average mass.</text>
  </comment>
  <comment type="mass spectrometry" mass="4114.1" method="MALDI" evidence="2"/>
  <comment type="miscellaneous">
    <text evidence="2">Negative results: Does not affect sodium currents in rat DRG cells. Shows little or no effect against Kv1.1/KCNA1, Kv1.3/KCNA3, Kv1.4/KCNA4, Kv2.2/KCNB2, Kv3.1/KCNC1, Kv4.1/KCND1, Kv4.2/KCND2, or Kv4.3/KCND3 potassium channels.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the scoloptoxin-04 family.</text>
  </comment>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000461169" evidence="5">
    <location>
      <begin position="26"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000461170" description="Kappa-scoloptoxin SsmTx-I" evidence="2">
    <location>
      <begin position="38"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="45"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="50"/>
      <end position="63"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="24464516"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="28247497"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="24464516"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="24464516"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="28247497"/>
    </source>
  </evidence>
  <sequence length="73" mass="8473" checksum="33568AFBDC8A37CC" modified="2024-11-27" version="1" precursor="true">MMMMFSVVSVFLMLLLLKFHDLSMGEEISLLKKVVRREESMLLSCPDLSCPTGYTCDVLTKKCKRLSDELWDH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>