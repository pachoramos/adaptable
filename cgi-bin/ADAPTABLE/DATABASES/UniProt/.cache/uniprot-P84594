<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-08-16" modified="2022-05-25" version="31" xmlns="http://uniprot.org/uniprot">
  <accession>P84594</accession>
  <name>PPK5A_BLADU</name>
  <protein>
    <recommendedName>
      <fullName>Pyrokinin-5a</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>FXPRL-amide</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Blaptica dubia</name>
    <name type="common">Argentinian wood cockroach</name>
    <dbReference type="NCBI Taxonomy" id="132935"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Dictyoptera</taxon>
      <taxon>Blattodea</taxon>
      <taxon>Blaberoidea</taxon>
      <taxon>Blaberidae</taxon>
      <taxon>Blaberinae</taxon>
      <taxon>Blaptica</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="submission" date="2005-05" db="UniProtKB">
      <authorList>
        <person name="Predel R."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-17</scope>
    <source>
      <tissue>Abdominal perisympathetic organs</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Myoactive.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed in abdominal perisympathetic organs and abdominal ganglia.</text>
  </comment>
  <comment type="mass spectrometry" mass="1821.87" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the pyrokinin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84594"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001484">
    <property type="entry name" value="Pyrokinin_CS"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00539">
    <property type="entry name" value="PYROKININ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044342" description="Pyrokinin-5a">
    <location>
      <begin position="1"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="3">
    <location>
      <position position="17"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P84362"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="17" mass="1837" checksum="9F943725C94AFCDE" modified="2005-08-16" version="1">AGESSNEAKGMWFGPRL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>