<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-10-14" modified="2024-07-24" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>P0C8A3</accession>
  <name>DEFA3_ORNAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="7 8">Defensin-A3</fullName>
      <shortName evidence="4">DefA3</shortName>
      <shortName evidence="5">OaDefA3</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ornithorhynchus anatinus</name>
    <name type="common">Duckbill platypus</name>
    <dbReference type="NCBI Taxonomy" id="9258"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Monotremata</taxon>
      <taxon>Ornithorhynchidae</taxon>
      <taxon>Ornithorhynchus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Genome Res." volume="18" first="986" last="994">
      <title>Defensins and the convergent evolution of platypus and reptile venom genes.</title>
      <authorList>
        <person name="Whittington C.M."/>
        <person name="Papenfuss A.T."/>
        <person name="Bansal P."/>
        <person name="Torres A.M."/>
        <person name="Wong E.S."/>
        <person name="Deakin J.E."/>
        <person name="Graves T."/>
        <person name="Alsop A."/>
        <person name="Schatzkamer K."/>
        <person name="Kremitzki C."/>
        <person name="Ponting C.P."/>
        <person name="Temple-Smith P."/>
        <person name="Warren W.C."/>
        <person name="Kuchel P.W."/>
        <person name="Belov K."/>
      </authorList>
      <dbReference type="PubMed" id="18463304"/>
      <dbReference type="DOI" id="10.1101/gr.7149808"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="Toxicon" volume="52" first="559" last="565">
      <title>Expression patterns of platypus defensin and related venom genes across a range of tissue types reveal the possibility of broader functions for OvDLPs than previously suspected.</title>
      <authorList>
        <person name="Whittington C.M."/>
        <person name="Papenfuss A.T."/>
        <person name="Kuchel P.W."/>
        <person name="Belov K."/>
      </authorList>
      <dbReference type="PubMed" id="18662710"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2008.07.002"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has antimicrobial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Highly expressed in spleen, and expressed at lower levels in intestin and lung.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the alpha-defensin family.</text>
  </comment>
  <comment type="online information" name="Platypus resources">
    <link uri="https://www.twinkl.ch/search?q=platypus"/>
  </comment>
  <dbReference type="RefSeq" id="XP_007661568.1">
    <property type="nucleotide sequence ID" value="XM_007663378.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0C8A3"/>
  <dbReference type="SMR" id="P0C8A3"/>
  <dbReference type="Ensembl" id="ENSOANT00000076305.1">
    <property type="protein sequence ID" value="ENSOANP00000052529.1"/>
    <property type="gene ID" value="ENSOANG00000050290.1"/>
  </dbReference>
  <dbReference type="GeneID" id="103168722"/>
  <dbReference type="KEGG" id="oaa:103168722"/>
  <dbReference type="GeneTree" id="ENSGT01000000220353"/>
  <dbReference type="InParanoid" id="P0C8A3"/>
  <dbReference type="OrthoDB" id="5648100at2759"/>
  <dbReference type="Proteomes" id="UP000002279">
    <property type="component" value="Chromosome X2"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSOANG00000050290">
    <property type="expression patterns" value="Expressed in ovary and 2 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051673">
    <property type="term" value="P:disruption of plasma membrane integrity in another organism"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016327">
    <property type="entry name" value="Alpha-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006081">
    <property type="entry name" value="Alpha-defensin_C"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876">
    <property type="entry name" value="ALPHA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876:SF28">
    <property type="entry name" value="ALPHA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001875">
    <property type="entry name" value="Alpha-defensin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00269">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000352687" evidence="2">
    <location>
      <begin position="20"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000352688" description="Defensin-A3">
    <location>
      <begin position="62"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="66"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="68"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="72"/>
      <end position="92"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18662710"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="18463304"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="18662710"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="18463304"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="18662710"/>
    </source>
  </evidence>
  <sequence length="95" mass="10441" checksum="7E980CA4133B239D" modified="2008-10-14" version="1" precursor="true">MRTLGLLLALLFLAAQTPAQLMGEEAEEATGRPEATEAQEAAAALMAARAADRHVTDPEQQRIITCSCRTFCFLGERISGRCYQSVFIYRLCCRG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>