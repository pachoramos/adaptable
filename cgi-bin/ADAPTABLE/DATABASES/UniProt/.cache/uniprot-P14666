<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-04-01" modified="2024-01-24" version="70" xmlns="http://uniprot.org/uniprot">
  <accession>P14666</accession>
  <name>CEC4_BOMMO</name>
  <protein>
    <recommendedName>
      <fullName>Cecropin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Antibacterial peptide CM-IV</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bombyx mori</name>
    <name type="common">Silk moth</name>
    <dbReference type="NCBI Taxonomy" id="7091"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Bombycoidea</taxon>
      <taxon>Bombycidae</taxon>
      <taxon>Bombycinae</taxon>
      <taxon>Bombyx</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1987" name="Annu. Rev. Microbiol." volume="41" first="103" last="126">
      <title>Cell-free immunity in insects.</title>
      <authorList>
        <person name="Boman H.G."/>
        <person name="Hultmark D."/>
      </authorList>
      <dbReference type="PubMed" id="3318666"/>
      <dbReference type="DOI" id="10.1146/annurev.mi.41.100187.000535"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT ILE-35</scope>
  </reference>
  <comment type="function">
    <text>Cecropins have lytic and antibacterial activity against several Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the cecropin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P14666"/>
  <dbReference type="SMR" id="P14666"/>
  <dbReference type="STRING" id="7091.P14666"/>
  <dbReference type="InParanoid" id="P14666"/>
  <dbReference type="Proteomes" id="UP000005204">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000875">
    <property type="entry name" value="Cecropin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00272">
    <property type="entry name" value="Cecropin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00268">
    <property type="entry name" value="CECROPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044679" description="Cecropin">
    <location>
      <begin position="1"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="modified residue" description="Isoleucine amide" evidence="1">
    <location>
      <position position="35"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="3318666"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="35" mass="3763" checksum="5240DEC509117486" modified="1990-04-01" version="1">RWKIFKKIEKVGQNIRDGIVKAGPAVAVVGQAATI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>