<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-15" modified="2024-11-27" version="132" xmlns="http://uniprot.org/uniprot">
  <accession>O14274</accession>
  <name>DTD_SCHPO</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">D-aminoacyl-tRNA deacylase</fullName>
      <shortName>DTD</shortName>
      <ecNumber evidence="1">3.1.1.96</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Gly-tRNA(Ala) deacylase</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">dtd1</name>
    <name type="ORF">SPAC8C9.05</name>
  </gene>
  <organism>
    <name type="scientific">Schizosaccharomyces pombe (strain 972 / ATCC 24843)</name>
    <name type="common">Fission yeast</name>
    <dbReference type="NCBI Taxonomy" id="284812"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Ascomycota</taxon>
      <taxon>Taphrinomycotina</taxon>
      <taxon>Schizosaccharomycetes</taxon>
      <taxon>Schizosaccharomycetales</taxon>
      <taxon>Schizosaccharomycetaceae</taxon>
      <taxon>Schizosaccharomyces</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Nature" volume="415" first="871" last="880">
      <title>The genome sequence of Schizosaccharomyces pombe.</title>
      <authorList>
        <person name="Wood V."/>
        <person name="Gwilliam R."/>
        <person name="Rajandream M.A."/>
        <person name="Lyne M.H."/>
        <person name="Lyne R."/>
        <person name="Stewart A."/>
        <person name="Sgouros J.G."/>
        <person name="Peat N."/>
        <person name="Hayles J."/>
        <person name="Baker S.G."/>
        <person name="Basham D."/>
        <person name="Bowman S."/>
        <person name="Brooks K."/>
        <person name="Brown D."/>
        <person name="Brown S."/>
        <person name="Chillingworth T."/>
        <person name="Churcher C.M."/>
        <person name="Collins M."/>
        <person name="Connor R."/>
        <person name="Cronin A."/>
        <person name="Davis P."/>
        <person name="Feltwell T."/>
        <person name="Fraser A."/>
        <person name="Gentles S."/>
        <person name="Goble A."/>
        <person name="Hamlin N."/>
        <person name="Harris D.E."/>
        <person name="Hidalgo J."/>
        <person name="Hodgson G."/>
        <person name="Holroyd S."/>
        <person name="Hornsby T."/>
        <person name="Howarth S."/>
        <person name="Huckle E.J."/>
        <person name="Hunt S."/>
        <person name="Jagels K."/>
        <person name="James K.D."/>
        <person name="Jones L."/>
        <person name="Jones M."/>
        <person name="Leather S."/>
        <person name="McDonald S."/>
        <person name="McLean J."/>
        <person name="Mooney P."/>
        <person name="Moule S."/>
        <person name="Mungall K.L."/>
        <person name="Murphy L.D."/>
        <person name="Niblett D."/>
        <person name="Odell C."/>
        <person name="Oliver K."/>
        <person name="O'Neil S."/>
        <person name="Pearson D."/>
        <person name="Quail M.A."/>
        <person name="Rabbinowitsch E."/>
        <person name="Rutherford K.M."/>
        <person name="Rutter S."/>
        <person name="Saunders D."/>
        <person name="Seeger K."/>
        <person name="Sharp S."/>
        <person name="Skelton J."/>
        <person name="Simmonds M.N."/>
        <person name="Squares R."/>
        <person name="Squares S."/>
        <person name="Stevens K."/>
        <person name="Taylor K."/>
        <person name="Taylor R.G."/>
        <person name="Tivey A."/>
        <person name="Walsh S.V."/>
        <person name="Warren T."/>
        <person name="Whitehead S."/>
        <person name="Woodward J.R."/>
        <person name="Volckaert G."/>
        <person name="Aert R."/>
        <person name="Robben J."/>
        <person name="Grymonprez B."/>
        <person name="Weltjens I."/>
        <person name="Vanstreels E."/>
        <person name="Rieger M."/>
        <person name="Schaefer M."/>
        <person name="Mueller-Auer S."/>
        <person name="Gabel C."/>
        <person name="Fuchs M."/>
        <person name="Duesterhoeft A."/>
        <person name="Fritzc C."/>
        <person name="Holzer E."/>
        <person name="Moestl D."/>
        <person name="Hilbert H."/>
        <person name="Borzym K."/>
        <person name="Langer I."/>
        <person name="Beck A."/>
        <person name="Lehrach H."/>
        <person name="Reinhardt R."/>
        <person name="Pohl T.M."/>
        <person name="Eger P."/>
        <person name="Zimmermann W."/>
        <person name="Wedler H."/>
        <person name="Wambutt R."/>
        <person name="Purnelle B."/>
        <person name="Goffeau A."/>
        <person name="Cadieu E."/>
        <person name="Dreano S."/>
        <person name="Gloux S."/>
        <person name="Lelaure V."/>
        <person name="Mottier S."/>
        <person name="Galibert F."/>
        <person name="Aves S.J."/>
        <person name="Xiang Z."/>
        <person name="Hunt C."/>
        <person name="Moore K."/>
        <person name="Hurst S.M."/>
        <person name="Lucas M."/>
        <person name="Rochet M."/>
        <person name="Gaillardin C."/>
        <person name="Tallada V.A."/>
        <person name="Garzon A."/>
        <person name="Thode G."/>
        <person name="Daga R.R."/>
        <person name="Cruzado L."/>
        <person name="Jimenez J."/>
        <person name="Sanchez M."/>
        <person name="del Rey F."/>
        <person name="Benito J."/>
        <person name="Dominguez A."/>
        <person name="Revuelta J.L."/>
        <person name="Moreno S."/>
        <person name="Armstrong J."/>
        <person name="Forsburg S.L."/>
        <person name="Cerutti L."/>
        <person name="Lowe T."/>
        <person name="McCombie W.R."/>
        <person name="Paulsen I."/>
        <person name="Potashkin J."/>
        <person name="Shpakovski G.V."/>
        <person name="Ussery D."/>
        <person name="Barrell B.G."/>
        <person name="Nurse P."/>
      </authorList>
      <dbReference type="PubMed" id="11859360"/>
      <dbReference type="DOI" id="10.1038/nature724"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>972 / ATCC 24843</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">An aminoacyl-tRNA editing enzyme that deacylates mischarged D-aminoacyl-tRNAs. Also deacylates mischarged glycyl-tRNA(Ala), protecting cells against glycine mischarging by AlaRS. Acts via tRNA-based rather than protein-based catalysis; rejects L-amino acids rather than detecting D-amino acids in the active site. By recycling D-aminoacyl-tRNA to D-amino acids and free tRNA molecules, this enzyme counteracts the toxicity associated with the formation of D-aminoacyl-tRNA entities in vivo and helps enforce protein L-homochirality.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>glycyl-tRNA(Ala) + H2O = tRNA(Ala) + glycine + H(+)</text>
      <dbReference type="Rhea" id="RHEA:53744"/>
      <dbReference type="Rhea" id="RHEA-COMP:9657"/>
      <dbReference type="Rhea" id="RHEA-COMP:13640"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:57305"/>
      <dbReference type="ChEBI" id="CHEBI:78442"/>
      <dbReference type="ChEBI" id="CHEBI:78522"/>
      <dbReference type="EC" id="3.1.1.96"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>a D-aminoacyl-tRNA + H2O = a tRNA + a D-alpha-amino acid + H(+)</text>
      <dbReference type="Rhea" id="RHEA:13953"/>
      <dbReference type="Rhea" id="RHEA-COMP:10123"/>
      <dbReference type="Rhea" id="RHEA-COMP:10124"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:59871"/>
      <dbReference type="ChEBI" id="CHEBI:78442"/>
      <dbReference type="ChEBI" id="CHEBI:79333"/>
      <dbReference type="EC" id="3.1.1.96"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="1">A Gly-cisPro motif from one monomer fits into the active site of the other monomer to allow specific chiral rejection of L-amino acids.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the DTD family.</text>
  </comment>
  <dbReference type="EC" id="3.1.1.96" evidence="1"/>
  <dbReference type="EMBL" id="CU329670">
    <property type="protein sequence ID" value="CAB16293.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="T39142">
    <property type="entry name" value="T39142"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_594276.1">
    <property type="nucleotide sequence ID" value="NM_001019699.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O14274"/>
  <dbReference type="SMR" id="O14274"/>
  <dbReference type="BioGRID" id="279681">
    <property type="interactions" value="12"/>
  </dbReference>
  <dbReference type="STRING" id="284812.O14274"/>
  <dbReference type="iPTMnet" id="O14274"/>
  <dbReference type="PaxDb" id="4896-SPAC8C9.05.1"/>
  <dbReference type="EnsemblFungi" id="SPAC8C9.05.1">
    <property type="protein sequence ID" value="SPAC8C9.05.1:pep"/>
    <property type="gene ID" value="SPAC8C9.05"/>
  </dbReference>
  <dbReference type="GeneID" id="2543253"/>
  <dbReference type="KEGG" id="spo:2543253"/>
  <dbReference type="PomBase" id="SPAC8C9.05">
    <property type="gene designation" value="dtd1"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="FungiDB:SPAC8C9.05"/>
  <dbReference type="eggNOG" id="KOG3323">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_076901_0_4_1"/>
  <dbReference type="InParanoid" id="O14274"/>
  <dbReference type="OMA" id="VFGADMK"/>
  <dbReference type="PhylomeDB" id="O14274"/>
  <dbReference type="PRO" id="PR:O14274"/>
  <dbReference type="Proteomes" id="UP000002485">
    <property type="component" value="Chromosome I"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="PomBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="PomBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051500">
    <property type="term" value="F:D-tyrosyl-tRNA(Tyr) deacylase activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000049">
    <property type="term" value="F:tRNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002181">
    <property type="term" value="P:cytoplasmic translation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="PomBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006399">
    <property type="term" value="P:tRNA metabolic process"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd00563">
    <property type="entry name" value="Dtyr_deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.50.80.10:FF:000001">
    <property type="entry name" value="D-aminoacyl-tRNA deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.50.80.10">
    <property type="entry name" value="D-tyrosyl-tRNA(Tyr) deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00518">
    <property type="entry name" value="Deacylase_Dtd"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003732">
    <property type="entry name" value="Daa-tRNA_deacyls_DTD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023509">
    <property type="entry name" value="DTD-like_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00256">
    <property type="entry name" value="D-aminoacyl-tRNA deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10472:SF5">
    <property type="entry name" value="D-AMINOACYL-TRNA DEACYLASE 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10472">
    <property type="entry name" value="D-TYROSYL-TRNA TYR DEACYLASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02580">
    <property type="entry name" value="Tyr_Deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF69500">
    <property type="entry name" value="DTD-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0820">tRNA-binding</keyword>
  <feature type="chain" id="PRO_0000164632" description="D-aminoacyl-tRNA deacylase">
    <location>
      <begin position="1"/>
      <end position="149"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Gly-cisPro motif, important for rejection of L-amino acids" evidence="1">
    <location>
      <begin position="139"/>
      <end position="140"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q8IIS0"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="149" mass="16334" checksum="95611E644956983A" modified="1998-01-01" version="1">MKAVIQRVLNASVSVDDKIVSAIQQGYCILLGVGSDDTPEDVTKLSNKILKLKLFDNAEQPWKSTIADIQGEILCVSQFTLHARVNKGAKPDFHRSMKGPEAIELYEQVVKTLGESLGSDKIKKGVFGAMMNVQLVNNGPVTILYDTKE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>