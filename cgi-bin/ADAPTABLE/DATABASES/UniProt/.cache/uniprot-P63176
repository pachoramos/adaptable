<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-09-27" modified="2024-11-27" version="57" xmlns="http://uniprot.org/uniprot">
  <accession>P63176</accession>
  <accession>P19861</accession>
  <name>MYX3_CROVV</name>
  <protein>
    <recommendedName>
      <fullName>Myotoxin-3</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Crotalus viridis viridis</name>
    <name type="common">Prairie rattlesnake</name>
    <dbReference type="NCBI Taxonomy" id="8742"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Viperidae</taxon>
      <taxon>Crotalinae</taxon>
      <taxon>Crotalus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="FEBS Lett." volume="274" first="43" last="47">
      <title>A new small myotoxin from the venom of the prairie rattlesnake (Crotalus viridis viridis).</title>
      <authorList>
        <person name="Griffin P.R."/>
        <person name="Aird S.D."/>
      </authorList>
      <dbReference type="PubMed" id="2253781"/>
      <dbReference type="DOI" id="10.1016/0014-5793(90)81325-i"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Cationic peptide that possesses multiple functions. It acts as a cell-penetrating peptide (CPP), and as a potent voltage-gated potassium channel (Kv) inhibitor. It exhibits antimicrobial activities, hind limb paralysis, and severe muscle necrosis by a non-enzymatic mechanism.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="5168.9" error="1.55" method="FAB" evidence="3"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the crotamine-myotoxin family.</text>
  </comment>
  <dbReference type="PIR" id="S12909">
    <property type="entry name" value="S12909"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P63176"/>
  <dbReference type="SMR" id="P63176"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044564">
    <property type="term" value="P:envenomation resulting in occlusion of the pore of voltage-gated potassium channel in another organism"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="2.20.20.10:FF:000001">
    <property type="entry name" value="Crotamine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000881">
    <property type="entry name" value="Myotoxin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00819">
    <property type="entry name" value="Myotoxins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00283">
    <property type="entry name" value="MYOTOXIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00459">
    <property type="entry name" value="MYOTOXINS_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51345">
    <property type="entry name" value="MYOTOXINS_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0959">Myotoxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000221567" description="Myotoxin-3" evidence="3">
    <location>
      <begin position="1"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="4"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="11"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="18"/>
      <end position="37"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q9PWF3"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="2253781"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="2253781"/>
    </source>
  </evidence>
  <sequence length="45" mass="5174" checksum="8A12DD3B5D98DF3A" modified="2004-09-27" version="1">YKRCHKKGGHCFPKTVICLPPSSDFGKMDCRWKWKCCKKGSVNNA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>