<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2021-06-02" version="36" xmlns="http://uniprot.org/uniprot">
  <accession>P62933</accession>
  <accession>P01293</accession>
  <name>TKN_ELECI</name>
  <protein>
    <recommendedName>
      <fullName>Eledoisin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Eledone cirrhosa</name>
    <name type="common">Curled octopus</name>
    <name type="synonym">Ozaena cirrosa</name>
    <dbReference type="NCBI Taxonomy" id="102876"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Cephalopoda</taxon>
      <taxon>Coleoidea</taxon>
      <taxon>Octopodiformes</taxon>
      <taxon>Octopoda</taxon>
      <taxon>Incirrata</taxon>
      <taxon>Octopodidae</taxon>
      <taxon>Eledone</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1963" name="Arch. Biochem. Biophys." volume="101" first="56" last="65">
      <title>The isolation and amino acid sequence of eledoisin, the active endecapeptide of the posterior salivary glands of Eledone.</title>
      <authorList>
        <person name="Anastasi A."/>
        <person name="Erspamer V."/>
      </authorList>
      <dbReference type="PubMed" id="14012712"/>
      <dbReference type="DOI" id="10.1016/0003-9861(63)90533-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>AMIDATION AT MET-11</scope>
    <source>
      <tissue>Posterior salivary gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2003" name="Biophys. J." volume="84" first="655" last="664">
      <title>Solution structure of the tachykinin peptide eledoisin.</title>
      <authorList>
        <person name="Grace R.C."/>
        <person name="Chandrashekar I.R."/>
        <person name="Cowsik S.M."/>
      </authorList>
      <dbReference type="PubMed" id="12524318"/>
      <dbReference type="DOI" id="10.1016/s0006-3495(03)74885-1"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <comment type="function">
    <text>Tachykinins are active peptides which excite neurons, evoke behavioral responses, are potent vasodilators and secretagogues, and contract (directly or indirectly) many smooth muscles.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the tachykinin family.</text>
  </comment>
  <dbReference type="PIR" id="B01561">
    <property type="entry name" value="EOOCC"/>
  </dbReference>
  <dbReference type="PDB" id="1MXQ">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=2-11"/>
  </dbReference>
  <dbReference type="PDBsum" id="1MXQ"/>
  <dbReference type="SMR" id="P62933"/>
  <dbReference type="EvolutionaryTrace" id="P62933"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013055">
    <property type="entry name" value="Tachy_Neuro_lke_CS"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00267">
    <property type="entry name" value="TACHYKININ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044393" description="Eledoisin">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="1">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <feature type="turn" evidence="3">
    <location>
      <begin position="3"/>
      <end position="5"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="6"/>
      <end position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="14012712"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="1MXQ"/>
    </source>
  </evidence>
  <sequence length="11" mass="1206" checksum="570D7C2559CDDAA3" modified="2004-08-31" version="1">QPSKDAFIGLM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>