<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1989-10-01" modified="2024-01-24" version="67" xmlns="http://uniprot.org/uniprot">
  <accession>P11697</accession>
  <name>GGI1_STAHA</name>
  <protein>
    <recommendedName>
      <fullName>Antibacterial protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Gonococcal growth inhibitor I</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Staphylococcus haemolyticus</name>
    <dbReference type="NCBI Taxonomy" id="1283"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1988" name="Biochem. J." volume="252" first="87" last="93">
      <title>The amino acid sequence of a gonococcal growth inhibitor from Staphylococcus haemolyticus.</title>
      <authorList>
        <person name="Watson D.C."/>
        <person name="Yaguchi M."/>
        <person name="Bisaillon J.G."/>
        <person name="Beaudet R."/>
        <person name="Morosoli R."/>
      </authorList>
      <dbReference type="PubMed" id="3138972"/>
      <dbReference type="DOI" id="10.1042/bj2520087"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>FORMYLATION AT MET-1</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Has hemolytic activity and also inhibits the growth of gonococci.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the staphylococcal hemolytic protein family.</text>
  </comment>
  <dbReference type="PIR" id="S00599">
    <property type="entry name" value="BXSA1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P11697"/>
  <dbReference type="SMR" id="P11697"/>
  <dbReference type="STRING" id="1283.ShL2_01674"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008846">
    <property type="entry name" value="PSMbeta"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05480">
    <property type="entry name" value="PSMbeta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0291">Formylation</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="chain" id="PRO_0000087482" description="Antibacterial protein 1">
    <location>
      <begin position="1"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="modified residue" description="N-formylmethionine" evidence="1 3">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="3138972"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="3138972"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="44" mass="4523" checksum="FAB1702DBA3A3238" modified="1989-10-01" version="1">MQKLAEAIAAAVSAGQDKDWGKMGTSIVGIVENGITVLGKIFGF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>