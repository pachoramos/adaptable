<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2023-05-03" version="35" xmlns="http://uniprot.org/uniprot">
  <accession>P80959</accession>
  <name>LC70_LACPA</name>
  <protein>
    <recommendedName>
      <fullName>Bacteriocin lactocin-705</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lacticaseibacillus paracasei</name>
    <name type="common">Lactobacillus paracasei</name>
    <dbReference type="NCBI Taxonomy" id="1597"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Lactobacillaceae</taxon>
      <taxon>Lacticaseibacillus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Int. J. Food Microbiol." volume="29" first="397" last="402">
      <title>Control of Listeria monocytogenes in ground beef by 'Lactocin 705', a bacteriocin produced by Lactobacillus casei CRL 705.</title>
      <authorList>
        <person name="Vignolo G."/>
        <person name="Fadda S."/>
        <person name="de Kairuz M.N."/>
        <person name="de Ruiz Holgado A.A."/>
        <person name="Oliver G."/>
      </authorList>
      <dbReference type="PubMed" id="8796440"/>
      <dbReference type="DOI" id="10.1016/0168-1605(95)00038-0"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <strain>CRL 705</strain>
    </source>
  </reference>
  <comment type="function">
    <text>Antibacterial activity against several lactic acid bacteria, Listeria, Streptococci, etc.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P80959"/>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012517">
    <property type="entry name" value="Antimicrobial14"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08109">
    <property type="entry name" value="Antimicrobial14"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0078">Bacteriocin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <feature type="peptide" id="PRO_0000044638" description="Bacteriocin lactocin-705">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <sequence length="31" mass="3358" checksum="794887F19440F340" modified="1997-11-01" version="1">GMSGYIQGIPDFLKGYLHGISAANKHKKGRL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>