<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-05-29" version="88" xmlns="http://uniprot.org/uniprot">
  <accession>P68248</accession>
  <accession>P01306</accession>
  <name>PAHO_CHICK</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Pancreatic polypeptide</fullName>
      <shortName evidence="4">APP</shortName>
      <shortName evidence="5">PPP</shortName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">PPY</name>
  </gene>
  <organism>
    <name type="scientific">Gallus gallus</name>
    <name type="common">Chicken</name>
    <dbReference type="NCBI Taxonomy" id="9031"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Archelosauria</taxon>
      <taxon>Archosauria</taxon>
      <taxon>Dinosauria</taxon>
      <taxon>Saurischia</taxon>
      <taxon>Theropoda</taxon>
      <taxon>Coelurosauria</taxon>
      <taxon>Aves</taxon>
      <taxon>Neognathae</taxon>
      <taxon>Galloanserae</taxon>
      <taxon>Galliformes</taxon>
      <taxon>Phasianidae</taxon>
      <taxon>Phasianinae</taxon>
      <taxon>Gallus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Gene" volume="130" first="183" last="189">
      <title>Structure determination and evolution of the chicken cDNA and gene encoding prepropancreatic polypeptide.</title>
      <authorList>
        <person name="Nata K."/>
        <person name="Sugimoto T."/>
        <person name="Kohri K."/>
        <person name="Hidaka H."/>
        <person name="Hattori E."/>
        <person name="Yamamoto H."/>
        <person name="Yonekura H."/>
        <person name="Okamoto H."/>
      </authorList>
      <dbReference type="PubMed" id="8359685"/>
      <dbReference type="DOI" id="10.1016/0378-1119(93)90418-3"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1975" name="J. Biol. Chem." volume="250" first="9369" last="9376">
      <title>Isolation and characterization of a new pancreatic polypeptide hormone.</title>
      <authorList>
        <person name="Kimmel J.R."/>
        <person name="Hayden L.J."/>
        <person name="Pollock H.G."/>
      </authorList>
      <dbReference type="PubMed" id="1194289"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(19)40653-4"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 26-61</scope>
    <source>
      <tissue>Pancreas</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Hormone secreted by pancreatic cells that acts as a regulator of pancreatic and gastrointestinal functions.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the NPY family.</text>
  </comment>
  <dbReference type="EMBL" id="D13761">
    <property type="protein sequence ID" value="BAA02907.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D13760">
    <property type="protein sequence ID" value="BAA02906.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="JN0776">
    <property type="entry name" value="PCCH"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_990117.1">
    <property type="nucleotide sequence ID" value="NM_204786.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P68248"/>
  <dbReference type="BMRB" id="P68248"/>
  <dbReference type="SMR" id="P68248"/>
  <dbReference type="STRING" id="9031.ENSGALP00000043946"/>
  <dbReference type="PaxDb" id="9031-ENSGALP00000042719"/>
  <dbReference type="Ensembl" id="ENSGALT00000049554">
    <property type="protein sequence ID" value="ENSGALP00000043946"/>
    <property type="gene ID" value="ENSGALG00000039125"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00010061007.1">
    <property type="protein sequence ID" value="ENSGALP00010037750.1"/>
    <property type="gene ID" value="ENSGALG00010024981.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00015061692">
    <property type="protein sequence ID" value="ENSGALP00015037376"/>
    <property type="gene ID" value="ENSGALG00015025298"/>
  </dbReference>
  <dbReference type="GeneID" id="395564"/>
  <dbReference type="KEGG" id="gga:395564"/>
  <dbReference type="CTD" id="5539"/>
  <dbReference type="VEuPathDB" id="HostDB:geneid_395564"/>
  <dbReference type="eggNOG" id="ENOG502S267">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00950000185319"/>
  <dbReference type="HOGENOM" id="CLU_165150_1_0_1"/>
  <dbReference type="InParanoid" id="P68248"/>
  <dbReference type="OMA" id="MAATRRC"/>
  <dbReference type="OrthoDB" id="4638016at2759"/>
  <dbReference type="PhylomeDB" id="P68248"/>
  <dbReference type="Reactome" id="R-GGA-375276">
    <property type="pathway name" value="Peptide ligand-binding receptors"/>
  </dbReference>
  <dbReference type="Reactome" id="R-GGA-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P68248"/>
  <dbReference type="Proteomes" id="UP000000539">
    <property type="component" value="Chromosome 27"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031841">
    <property type="term" value="F:neuropeptide Y receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd00126">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.900">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001955">
    <property type="entry name" value="Pancreatic_hormone-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020392">
    <property type="entry name" value="Pancreatic_hormone-like_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533">
    <property type="entry name" value="NEUROPEPTIDE Y/PANCREATIC HORMONE/PEPTIDE YY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533:SF5">
    <property type="entry name" value="PRO-NEUROPEPTIDE Y"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00159">
    <property type="entry name" value="Hormone_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00278">
    <property type="entry name" value="PANCHORMONE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00309">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00265">
    <property type="entry name" value="PANCREATIC_HORMONE_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50276">
    <property type="entry name" value="PANCREATIC_HORMONE_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000025376" description="Pancreatic polypeptide">
    <location>
      <begin position="26"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000025377">
    <location>
      <begin position="65"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="1">
    <location>
      <position position="61"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="6" ref="2">
    <original>ND</original>
    <variation>DN</variation>
    <location>
      <begin position="47"/>
      <end position="48"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P01298"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="1194289"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="1194289"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="8359685"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="80" mass="8773" checksum="90B44E27389DB050" modified="2004-10-25" version="1" precursor="true">MPPRWASLLLLACSLLLLAVPPGTAGPSQPTYPGDDAPVEDLIRFYNDLQQYLNVVTRHRYGRRSSSRVLCEEPMGAAGC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>