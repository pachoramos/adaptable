<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-08-31" modified="2023-05-03" version="47" xmlns="http://uniprot.org/uniprot">
  <accession>P84112</accession>
  <name>BR1B_RANBO</name>
  <protein>
    <recommendedName>
      <fullName>Brevinin-1BYb</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Rana boylii</name>
    <name type="common">Foothill yellow-legged frog</name>
    <dbReference type="NCBI Taxonomy" id="160499"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Rana</taxon>
      <taxon>Rana</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2003" name="J. Pept. Res." volume="62" first="207" last="213">
      <title>Isolation of peptides of the brevinin-1 family with potent candidacidal activity from the skin secretions of the frog Rana boylii.</title>
      <authorList>
        <person name="Conlon J.M."/>
        <person name="Sonnevend A."/>
        <person name="Patel M."/>
        <person name="Davidson C."/>
        <person name="Nielsen P.F."/>
        <person name="Pal T."/>
        <person name="Rollins-Smith L.A."/>
      </authorList>
      <dbReference type="PubMed" id="14531844"/>
      <dbReference type="DOI" id="10.1034/j.1399-3011.2003.00090.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="1">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antibacterial activity against Gram-positive bacterium S.aureus and Gram-negative bacterium E.coli. Has moderate antifungal activity against C.albicans and strong hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2571.5" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84112"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012520">
    <property type="entry name" value="Antimicrobial_frog_1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08018">
    <property type="entry name" value="Antimicrobial_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043533" description="Brevinin-1BYb">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="18"/>
      <end position="24"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="14531844"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="24" mass="2575" checksum="CEC2E08E14143DF4" modified="2004-08-16" version="1">FLPILASLAAKLGPKLFCLVTKKC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>