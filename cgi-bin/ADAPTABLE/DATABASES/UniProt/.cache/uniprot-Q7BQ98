<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-12-04" modified="2024-11-27" version="93" xmlns="http://uniprot.org/uniprot">
  <accession>Q7BQ98</accession>
  <name>STXB_SHIDY</name>
  <protein>
    <recommendedName>
      <fullName>Shiga toxin subunit B</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">stxB</name>
  </gene>
  <organism>
    <name type="scientific">Shigella dysenteriae</name>
    <dbReference type="NCBI Taxonomy" id="622"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Shigella</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1986" name="J. Biol. Chem." volume="261" first="13928" last="13931">
      <title>Complete amino acid sequence of Shigella toxin B-chain. A novel polypeptide containing 69 amino acids and one disulfide bridge.</title>
      <authorList>
        <person name="Seidah N.G."/>
        <person name="Donohue-Rolfe A."/>
        <person name="Lazure C."/>
        <person name="Auclair F."/>
        <person name="Keusch G.T."/>
        <person name="Chretien M."/>
      </authorList>
      <dbReference type="PubMed" id="3771511"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)66961-3"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1988" name="Gene" volume="67" first="213" last="221">
      <title>The primary structure of the operons coding for Shigella dysenteriae toxin and temperature phage H30 shiga-like toxin.</title>
      <authorList>
        <person name="Kozlov Y.V."/>
        <person name="Kabishev A.A."/>
        <person name="Lukyanov E.V."/>
        <person name="Bayev A.A."/>
      </authorList>
      <dbReference type="PubMed" id="3049254"/>
      <dbReference type="DOI" id="10.1016/0378-1119(88)90398-8"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1988" name="J. Bacteriol." volume="170" first="1116" last="1122">
      <title>Cloning and sequencing of the genes for Shiga toxin from Shigella dysenteriae type 1.</title>
      <authorList>
        <person name="Strockbine N.A."/>
        <person name="Jackson M.P."/>
        <person name="Sung L.M."/>
        <person name="Holmes R.K."/>
        <person name="O'Brien A.D."/>
      </authorList>
      <dbReference type="PubMed" id="2830229"/>
      <dbReference type="DOI" id="10.1128/jb.170.3.1116-1122.1988"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1999" name="Mol. Microbiol." volume="34" first="1058" last="1069">
      <title>Spontaneous tandem amplification and deletion of the Shiga toxin operon in Shigella dysenteriae 1.</title>
      <authorList>
        <person name="McDonough M.A."/>
        <person name="Butterton J.R."/>
      </authorList>
      <dbReference type="PubMed" id="10594830"/>
      <dbReference type="DOI" id="10.1046/j.1365-2958.1999.01669.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>3818 / Type 1</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2000" name="Infect. Immun." volume="68" first="4856" last="4864">
      <title>Structural analysis of phage-borne stx genes and their flanking sequences in shiga toxin-producing Escherichia coli and Shigella dysenteriae type 1 strains.</title>
      <authorList>
        <person name="Unkmeir A."/>
        <person name="Schmidt H."/>
      </authorList>
      <dbReference type="PubMed" id="10948097"/>
      <dbReference type="DOI" id="10.1128/iai.68.9.4856-4864.2000"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>H2765-39/81 / Type 1</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1987" name="Dokl. Biochem." volume="295" first="744" last="749">
      <title>Cloning and primary structure of Shigella toxin genes.</title>
      <authorList>
        <person name="Kozlov Y.V."/>
        <person name="Kabishev A.A."/>
        <person name="Fedchenko V.I."/>
        <person name="Bayev A.A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] OF 1-70</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1984" name="Infect. Immun." volume="43" first="195" last="201">
      <title>Temperature-dependent expression of virulence genes in Shigella species.</title>
      <authorList>
        <person name="Maurelli A.T."/>
        <person name="Blackmon B."/>
        <person name="Curtiss R. III"/>
      </authorList>
      <dbReference type="PubMed" id="6360895"/>
      <dbReference type="DOI" id="10.1128/iai.43.1.195-201.1984"/>
    </citation>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="1989" name="Mol. Microbiol." volume="3" first="1231" last="1236">
      <title>Isolation and characterization of functional Shiga toxin subunits and renatured holotoxin.</title>
      <authorList>
        <person name="Donohue-Rolfe A."/>
        <person name="Jacewicz M."/>
        <person name="Keusch G.T."/>
      </authorList>
      <dbReference type="PubMed" id="2677606"/>
      <dbReference type="DOI" id="10.1111/j.1365-2958.1989.tb00273.x"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="1998" name="Infect. Immun." volume="66" first="4355" last="4366">
      <title>Shiga toxin binds human platelets via globotriaosylceramide (Pk antigen) and a novel platelet glycosphingolipid.</title>
      <authorList>
        <person name="Cooling L.L.W."/>
        <person name="Walker K.E."/>
        <person name="Gille T."/>
        <person name="Koerner T.A.W."/>
      </authorList>
      <dbReference type="PubMed" id="9712788"/>
      <dbReference type="DOI" id="10.1128/iai.66.9.4355-4366.1998"/>
    </citation>
    <scope>BINDING TO RECEPTOR</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="1990" name="J. Bacteriol." volume="172" first="653" last="658">
      <title>Functional analysis of the Shiga toxin and Shiga-like toxin type II variant binding subunits by using site-directed mutagenesis.</title>
      <authorList>
        <person name="Jackson M.P."/>
        <person name="Wadolkowski E.A."/>
        <person name="Weinstein D.L."/>
        <person name="Holmes R.K."/>
        <person name="O'Brien A.D."/>
      </authorList>
      <dbReference type="PubMed" id="2404947"/>
      <dbReference type="DOI" id="10.1128/jb.172.2.653-658.1990"/>
    </citation>
    <scope>MUTAGENESIS OF ASP-36; ASP-37; ASP-38; SER-58 AND LYS-73</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="1991" name="J. Bacteriol." volume="173" first="1151" last="1160">
      <title>Identification of three amino acid residues in the B subunit of Shiga toxin and Shiga-like toxin type II that are essential for holotoxin activity.</title>
      <authorList>
        <person name="Perera L.P."/>
        <person name="Samuel J.E."/>
        <person name="Holmes R.K."/>
        <person name="O'Brien A.D."/>
      </authorList>
      <dbReference type="PubMed" id="1991714"/>
      <dbReference type="DOI" id="10.1128/jb.173.3.1151-1160.1991"/>
    </citation>
    <scope>MUTAGENESIS OF ARG-53; ALA-63 AND GLY-80</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="1995" name="J. Bacteriol." volume="177" first="3128" last="3132">
      <title>Analysis of Shiga toxin subunit association by using hybrid A polypeptides and site-specific mutagenesis.</title>
      <authorList>
        <person name="Jemal C."/>
        <person name="Haddad J.E."/>
        <person name="Begum D."/>
        <person name="Jackson M.P."/>
      </authorList>
      <dbReference type="PubMed" id="7768810"/>
      <dbReference type="DOI" id="10.1128/jb.177.11.3128-3132.1995"/>
    </citation>
    <scope>MUTAGENESIS OF ARG-53 AND TRP-54</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="1993" name="J. Mol. Biol." volume="232" first="704" last="706">
      <title>Purification and crystallization of Shiga toxin from Shigella dysenteriae.</title>
      <authorList>
        <person name="Kozlov Y.V."/>
        <person name="Chernaia M.M."/>
        <person name="Fraser M.E."/>
        <person name="James M.N.G."/>
      </authorList>
      <dbReference type="PubMed" id="8345529"/>
      <dbReference type="DOI" id="10.1006/jmbi.1993.1421"/>
    </citation>
    <scope>CRYSTALLIZATION</scope>
  </reference>
  <reference key="14">
    <citation type="journal article" date="1994" name="Nat. Struct. Biol." volume="1" first="59" last="64">
      <title>Crystal structure of the holotoxin from Shigella dysenteriae at 2.5 A resolution.</title>
      <authorList>
        <person name="Fraser M.E."/>
        <person name="Chernaia M.M."/>
        <person name="Kozlov Y.V."/>
        <person name="James M.N.G."/>
      </authorList>
      <dbReference type="PubMed" id="7656009"/>
      <dbReference type="DOI" id="10.1038/nsb0194-59"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.5 ANGSTROMS) OF 21-89</scope>
  </reference>
  <reference key="15">
    <citation type="journal article" date="2004" name="J. Biol. Chem." volume="279" first="27511" last="27517">
      <title>Structure of shiga toxin type 2 (Stx2) from Escherichia coli O157:H7.</title>
      <authorList>
        <person name="Fraser M.E."/>
        <person name="Fujinaga M."/>
        <person name="Cherney M.M."/>
        <person name="Melton-Celsa A.R."/>
        <person name="Twiddy E.M."/>
        <person name="O'Brien A.D."/>
        <person name="James M.N.G."/>
      </authorList>
      <dbReference type="PubMed" id="15075327"/>
      <dbReference type="DOI" id="10.1074/jbc.m401939200"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.5 ANGSTROMS) OF 21-89</scope>
  </reference>
  <comment type="function">
    <text evidence="3">The B subunit is responsible for the binding of the holotoxin to specific receptors on the target cell surface, such as globotriaosylceramide (Gb3) in human intestinal microvilli.</text>
  </comment>
  <comment type="subunit">
    <text>Shiga toxin contains a single subunit A and five copies of subunit B.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-1025922">
      <id>Q7BQ98</id>
    </interactant>
    <interactant intactId="EBI-1025915">
      <id>Q7BQ99</id>
      <label>stxA</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="induction">
    <text evidence="4">Maximal induction occurs at 37 degrees Celsius and in low-iron environments.</text>
  </comment>
  <comment type="domain">
    <text>There are three Gb3-binding sites in each subunit B monomer, allowing for a tighter binding to the target cell. Binding sites 1 and 2 have higher binding affinities than site 3.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the StxB family.</text>
  </comment>
  <dbReference type="EMBL" id="M24352">
    <property type="protein sequence ID" value="AAA26539.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M19437">
    <property type="protein sequence ID" value="AAA98348.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF153317">
    <property type="protein sequence ID" value="AAF28122.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AJ271153">
    <property type="protein sequence ID" value="CAC05623.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X07903">
    <property type="protein sequence ID" value="CAA30742.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000752026.1">
    <property type="nucleotide sequence ID" value="NZ_UAUQ01000013.1"/>
  </dbReference>
  <dbReference type="PDB" id="1DM0">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="B/C/D/E/F/G/H/I/J/K=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="1R4Q">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="B/C/D/E/F/G/H/I/J/K=21-89"/>
  </dbReference>
  <dbReference type="PDBsum" id="1DM0"/>
  <dbReference type="PDBsum" id="1R4Q"/>
  <dbReference type="AlphaFoldDB" id="Q7BQ98"/>
  <dbReference type="SMR" id="Q7BQ98"/>
  <dbReference type="DIP" id="DIP-36367N"/>
  <dbReference type="IntAct" id="Q7BQ98">
    <property type="interactions" value="2"/>
  </dbReference>
  <dbReference type="ChEMBL" id="CHEMBL3856169"/>
  <dbReference type="BioCyc" id="MetaCyc:MONOMER-18744"/>
  <dbReference type="EvolutionaryTrace" id="Q7BQ98"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019836">
    <property type="term" value="P:hemolysis by symbiont of host erythrocytes"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.70:FF:000001">
    <property type="entry name" value="Shiga toxin 1 subunit B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.70">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008992">
    <property type="entry name" value="Enterotoxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003189">
    <property type="entry name" value="SLT_beta"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02258">
    <property type="entry name" value="SLT_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50203">
    <property type="entry name" value="Bacterial enterotoxins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="signal peptide">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000312305" description="Shiga toxin subunit B">
    <location>
      <begin position="21"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="24"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Complete loss of cytotoxicity; when associated with N-37." evidence="2">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on cytotoxicity." evidence="2">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Complete loss of cytotoxicity; when associated with N-36." evidence="2">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on cytotoxicity." evidence="2">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on cytotoxicity." evidence="2">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of cytotoxicity." evidence="1 5">
    <original>R</original>
    <variation>D</variation>
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="100-fold reduction in cytotoxicity." evidence="5">
    <original>W</original>
    <variation>G</variation>
    <variation>F</variation>
    <location>
      <position position="54"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on cytotoxicity." evidence="2">
    <original>S</original>
    <variation>P</variation>
    <location>
      <position position="58"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Great decrease in cytotoxicity." evidence="1">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="63"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="10-fold decrease in cytotoxicity." evidence="2">
    <original>K</original>
    <variation>I</variation>
    <location>
      <position position="73"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Great decrease in cytotoxicity." evidence="1">
    <original>G</original>
    <variation>D</variation>
    <location>
      <position position="80"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="25"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="29"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="40"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="48"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="54"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="69"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="86"/>
      <end position="89"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="1991714"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="2404947"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="2677606"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="6360895"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="7768810"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="1DM0"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="8">
    <source>
      <dbReference type="PDB" id="1R4Q"/>
    </source>
  </evidence>
  <sequence length="89" mass="9743" checksum="C78F7795CCD7242E" modified="2004-07-05" version="1" precursor="true">MKKTLLIAASLSFFSASALATPDCVTGKVEYTKYNDDDTFTVKVGDKELFTNRWNLQSLLLSAQITGMTVTIKTNACHNGGGFSEVIFR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>