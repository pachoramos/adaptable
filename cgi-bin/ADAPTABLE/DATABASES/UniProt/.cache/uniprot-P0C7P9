<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-07-01" modified="2023-02-22" version="32" xmlns="http://uniprot.org/uniprot">
  <accession>P0C7P9</accession>
  <name>NA1B_BUNCN</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Delta-actitoxin-Bcg1b</fullName>
      <shortName evidence="5">Delta-AITX-Bcg1b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Cangitoxin II</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="6">Cangitoxin-2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Cangitoxin-II</fullName>
      <shortName evidence="4">CGTX-II</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bunodosoma cangicum</name>
    <name type="common">Sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="138296"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Bunodosoma</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Toxicon" volume="51" first="1303" last="1307">
      <title>Revisiting cangitoxin, a sea anemone peptide: purification and characterization of cangitoxins II and III from the venom of Bunodosoma cangicum.</title>
      <authorList>
        <person name="Zaharenko A.J."/>
        <person name="Ferreira W.A. Jr."/>
        <person name="de Oliveira J.S."/>
        <person name="Konno K."/>
        <person name="Richardson M."/>
        <person name="Schiavon E."/>
        <person name="Wanke E."/>
        <person name="de Freitas J.C."/>
      </authorList>
      <dbReference type="PubMed" id="18342901"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2008.01.011"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Nematoblast</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Peptides" volume="34" first="158" last="167">
      <title>Characterization of selectivity and pharmacophores of type 1 sea anemone toxins by screening seven Na(v) sodium channel isoforms.</title>
      <authorList>
        <person name="Zaharenko A.J."/>
        <person name="Schiavon E."/>
        <person name="Ferreira W.A. Jr."/>
        <person name="Lecchi M."/>
        <person name="de Freitas J.C."/>
        <person name="Richardson M."/>
        <person name="Wanke E."/>
      </authorList>
      <dbReference type="PubMed" id="21802465"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2011.07.008"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2009" name="Toxicon" volume="54" first="1102" last="1111">
      <title>Actions of sea anemone type 1 neurotoxins on voltage-gated sodium channel isoforms.</title>
      <authorList>
        <person name="Wanke E."/>
        <person name="Zaharenko A.J."/>
        <person name="Redaelli E."/>
        <person name="Schiavon E."/>
      </authorList>
      <dbReference type="PubMed" id="19393679"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2009.04.018"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Binds to the sodium channels Nav1.1/SCN1A (EC(50)=165 nM), Nav1.5/SCN5A (EC(50)=103 nM) and Nav1.6/SCN8A (EC(50)=133 nM), thereby delaying their inactivation (PubMed:18342901, PubMed:21802465). Also inhibits Nav1.2/SCN2A, Nav1.3/SCN3A, and Nav1.4/SCN4A, but to a lesser extent (PubMed:21802465). Inhibits Nav1.5 differently from isoforms Nav1.1 and Nav1.6. In Nav1.5 the effect consists in a right-shift of inactivation; whereas in both Nav1.1 and Nav1.6 the effect consists in an incomplete inactivation (PubMed:21802465).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="4958.09" method="Electrospray" evidence="2"/>
  <comment type="miscellaneous">
    <text evidence="3">vDoes not show activity on Nav1.7/SCN9A.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the sea anemone sodium channel inhibitory toxin family. Type I subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0C7P9"/>
  <dbReference type="SMR" id="P0C7P9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009966">
    <property type="term" value="P:regulation of signal transduction"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000693">
    <property type="entry name" value="Anenome_toxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00706">
    <property type="entry name" value="Toxin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001905">
    <property type="entry name" value="Anenome_toxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000342623" description="Delta-actitoxin-Bcg1b" evidence="2">
    <location>
      <begin position="1"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="6"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="28"/>
      <end position="46"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01530"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="18342901"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="21802465"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="18342901"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="48" mass="4964" checksum="E686F00B85E1F456" modified="2008-07-01" version="1">GVACRCDSDGPTVRGDSLSGTLWLTGGCPSGWHNCRGSGPFIGYCCKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>