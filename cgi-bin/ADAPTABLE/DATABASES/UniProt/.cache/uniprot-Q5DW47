<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-06-10" modified="2024-03-27" version="51" xmlns="http://uniprot.org/uniprot">
  <accession>Q5DW47</accession>
  <name>CORZ_APIME</name>
  <protein>
    <recommendedName>
      <fullName>Pro-corazonin</fullName>
      <shortName>AmCrz</shortName>
      <shortName>Crz</shortName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Corazonin</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Corazonin precursor-related peptide</fullName>
        <shortName>CPRP</shortName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name evidence="1" type="primary">Crz</name>
    <name type="ORF">GB17245</name>
  </gene>
  <organism>
    <name type="scientific">Apis mellifera</name>
    <name type="common">Honeybee</name>
    <dbReference type="NCBI Taxonomy" id="7460"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Apoidea</taxon>
      <taxon>Anthophila</taxon>
      <taxon>Apidae</taxon>
      <taxon>Apis</taxon>
    </lineage>
  </organism>
  <reference evidence="6" key="1">
    <citation type="journal article" date="2006" name="Appl. Entomol. Zool. (Jpn.)" volume="41" first="331" last="338">
      <title>Molecular cloning of [Thr4, His7]-corazonin (Apime-corazonin) and its distribution in the central nervous system of the honey bee Apis mellifera (Hymenoptera: Apidae).</title>
      <authorList>
        <person name="Roller L."/>
        <person name="Tanaka S."/>
        <person name="Kimura K."/>
        <person name="Satake H."/>
        <person name="Tanaka Y."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue evidence="6">Brain</tissue>
    </source>
  </reference>
  <reference evidence="5" key="2">
    <citation type="journal article" date="2006" name="Peptides" volume="27" first="493" last="499">
      <title>Cloning and characterization of a third isoform of corazonin in the honey bee Apis mellifera.</title>
      <authorList>
        <person name="Verleyen P."/>
        <person name="Baggerman G."/>
        <person name="Mertens I."/>
        <person name="Vandersmissen T."/>
        <person name="Huybrechts J."/>
        <person name="Van Lommel A."/>
        <person name="De Loof A."/>
        <person name="Schoofs L."/>
      </authorList>
      <dbReference type="PubMed" id="16406615"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2005.03.065"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 22-32</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-22</scope>
    <scope>AMIDATION AT ASN-32</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <reference evidence="5" key="3">
    <citation type="journal article" date="2006" name="Science" volume="314" first="647" last="649">
      <title>From the genome to the proteome: uncovering peptides in the Apis brain.</title>
      <authorList>
        <person name="Hummon A.B."/>
        <person name="Richmond T.A."/>
        <person name="Verleyen P."/>
        <person name="Baggerman G."/>
        <person name="Huybrechts J."/>
        <person name="Ewing M.A."/>
        <person name="Vierstraete E."/>
        <person name="Rodriguez-Zas S.L."/>
        <person name="Schoofs L."/>
        <person name="Robinson G.E."/>
        <person name="Sweedler J.V."/>
      </authorList>
      <dbReference type="PubMed" id="17068263"/>
      <dbReference type="DOI" id="10.1126/science.1124128"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 22-32</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="4">Brain</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Cardioactive peptide. Corazonin is probably involved in the physiological regulation of the heart beat (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <molecule>Corazonin</molecule>
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="subcellular location">
    <molecule>Corazonin precursor-related peptide</molecule>
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">In the adult brain, expressed in four neurons of the lateral protocerebrum project axons towards the retrocerebral complex.</text>
  </comment>
  <comment type="mass spectrometry" mass="1322.57" method="MALDI" evidence="3 4">
    <molecule>Corazonin</molecule>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the corazonin family.</text>
  </comment>
  <dbReference type="EMBL" id="AB201717">
    <property type="protein sequence ID" value="BAD90662.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001012981.1">
    <property type="nucleotide sequence ID" value="NM_001012963.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5DW47"/>
  <dbReference type="STRING" id="7460.Q5DW47"/>
  <dbReference type="PaxDb" id="7460-GB53951-PA"/>
  <dbReference type="EnsemblMetazoa" id="NM_001012963">
    <property type="protein sequence ID" value="NP_001012981"/>
    <property type="gene ID" value="GeneID_503862"/>
  </dbReference>
  <dbReference type="GeneID" id="503862"/>
  <dbReference type="KEGG" id="ame:503862"/>
  <dbReference type="CTD" id="12973"/>
  <dbReference type="eggNOG" id="ENOG502T9RC">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="Q5DW47"/>
  <dbReference type="OrthoDB" id="3574805at2759"/>
  <dbReference type="PhylomeDB" id="Q5DW47"/>
  <dbReference type="Proteomes" id="UP000005203">
    <property type="component" value="Linkage group LG1"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP001105180">
    <property type="component" value="Linkage Group LG1"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071858">
    <property type="term" value="F:corazonin receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045823">
    <property type="term" value="P:positive regulation of heart contraction"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020190">
    <property type="entry name" value="Procorazonin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17308">
    <property type="entry name" value="Corazonin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3 4">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000341607" description="Pro-corazonin" evidence="2">
    <location>
      <begin position="22"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5000052220" description="Corazonin" evidence="3">
    <location>
      <begin position="22"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000341495" description="Corazonin precursor-related peptide">
    <location>
      <begin position="36"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000341496" evidence="3">
    <location>
      <begin position="88"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="3">
    <location>
      <position position="22"/>
    </location>
  </feature>
  <feature type="modified residue" description="Asparagine amide" evidence="3">
    <location>
      <position position="32"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q26377"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="16406615"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="17068263"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="BAD90662.1"/>
    </source>
  </evidence>
  <sequence length="107" mass="12305" checksum="B588CC4039BE66B7" modified="2005-03-29" version="1" precursor="true">MVNSQILILFILSLTITIVMCQTFTYSHGWTNGKRSTSLEELANRNAIQSDNVFANCELQKLRLLLQGNINNQLFQTPCELLNFPKRSFSENMINDHRQPAPTNNNY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>