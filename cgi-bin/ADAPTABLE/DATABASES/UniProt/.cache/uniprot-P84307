<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-12-07" modified="2022-12-14" version="26" xmlns="http://uniprot.org/uniprot">
  <accession>P84307</accession>
  <accession>P38553</accession>
  <name>FARP_SCHGR</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">SchistoFLRFamide</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Cardioexcitatory neuropeptide</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">PDVDHVFLRF-amide</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Schistocerca gregaria</name>
    <name type="common">Desert locust</name>
    <name type="synonym">Gryllus gregarius</name>
    <dbReference type="NCBI Taxonomy" id="7010"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Orthoptera</taxon>
      <taxon>Caelifera</taxon>
      <taxon>Acrididea</taxon>
      <taxon>Acridomorpha</taxon>
      <taxon>Acridoidea</taxon>
      <taxon>Acrididae</taxon>
      <taxon>Cyrtacanthacridinae</taxon>
      <taxon>Schistocerca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1989" name="Biochem. Biophys. Res. Commun." volume="160" first="850" last="856">
      <title>Isolation, primary structure and bioactivity of schistoFLRF-amide, a FMRF-amide-like neuropeptide from the locust, Schistocerca gregaria.</title>
      <authorList>
        <person name="Robb S."/>
        <person name="Packman L.C."/>
        <person name="Evans P.D."/>
      </authorList>
      <dbReference type="PubMed" id="2719702"/>
      <dbReference type="DOI" id="10.1016/0006-291x(89)92512-6"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-10</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Thoracic nervous system</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Muscle inhibiting agent. Involved in the neural control of the visceral muscles of the heart, accessory glands and oviduct. May be involved in the regulation of saliva secretion.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the FARP (FMRFamide related peptide) family.</text>
  </comment>
  <dbReference type="PIR" id="A32543">
    <property type="entry name" value="A32543"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043683" description="SchistoFLRFamide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="2">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P84306"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="2719702"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="2719702"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="10" mass="1244" checksum="D3C51729D2C1EAB2" modified="2004-12-07" version="1">PDVDHVFLRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>