<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-12-01" modified="2024-11-27" version="95" xmlns="http://uniprot.org/uniprot">
  <accession>P81688</accession>
  <accession>B4HZP1</accession>
  <name>CECA1_DROSE</name>
  <protein>
    <recommendedName>
      <fullName>Cecropin-A1</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">CecA1</name>
    <name type="ORF">GM12870</name>
  </gene>
  <organism>
    <name type="scientific">Drosophila sechellia</name>
    <name type="common">Fruit fly</name>
    <dbReference type="NCBI Taxonomy" id="7238"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Ephydroidea</taxon>
      <taxon>Drosophilidae</taxon>
      <taxon>Drosophila</taxon>
      <taxon>Sophophora</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Genetics" volume="150" first="157" last="171">
      <title>Molecular evolution of the Cecropin multigene family in Drosophila: functional genes vs pseudogenes.</title>
      <authorList>
        <person name="Ramos-Onsins S."/>
        <person name="Aguade M."/>
      </authorList>
      <dbReference type="PubMed" id="9725836"/>
      <dbReference type="DOI" id="10.1093/genetics/150.1.157"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>Montemayor</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2007" name="Nature" volume="450" first="203" last="218">
      <title>Evolution of genes and genomes on the Drosophila phylogeny.</title>
      <authorList>
        <consortium name="Drosophila 12 genomes consortium"/>
      </authorList>
      <dbReference type="PubMed" id="17994087"/>
      <dbReference type="DOI" id="10.1038/nature06341"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Rob3c / Tucson 14021-0248.25</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Cecropins have lytic and antibacterial activity against several Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the cecropin family.</text>
  </comment>
  <dbReference type="EMBL" id="Y16862">
    <property type="protein sequence ID" value="CAA76483.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH480819">
    <property type="protein sequence ID" value="EDW53498.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_002037339.1">
    <property type="nucleotide sequence ID" value="XM_002037303.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P81688"/>
  <dbReference type="SMR" id="P81688"/>
  <dbReference type="STRING" id="7238.P81688"/>
  <dbReference type="EnsemblMetazoa" id="FBtr0195855">
    <property type="protein sequence ID" value="FBpp0194347"/>
    <property type="gene ID" value="FBgn0025653"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_002037303.2">
    <property type="protein sequence ID" value="XP_002037339.1"/>
    <property type="gene ID" value="LOC6612845"/>
  </dbReference>
  <dbReference type="GeneID" id="6612845"/>
  <dbReference type="KEGG" id="dse:6612845"/>
  <dbReference type="HOGENOM" id="CLU_187909_1_0_1"/>
  <dbReference type="OMA" id="NRIFVFV"/>
  <dbReference type="OrthoDB" id="3613173at2759"/>
  <dbReference type="PhylomeDB" id="P81688"/>
  <dbReference type="Proteomes" id="UP000001292">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="EnsemblMetazoa"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="EnsemblMetazoa"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="EnsemblMetazoa"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="EnsemblMetazoa"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002213">
    <property type="term" value="P:defense response to insect"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="EnsemblMetazoa"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051607">
    <property type="term" value="P:defense response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="EnsemblMetazoa"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0140460">
    <property type="term" value="P:response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="EnsemblMetazoa"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000875">
    <property type="entry name" value="Cecropin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020400">
    <property type="entry name" value="Cecropin_insect"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR38329">
    <property type="entry name" value="CECROPIN-A1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR38329:SF1">
    <property type="entry name" value="CECROPIN-A1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00272">
    <property type="entry name" value="Cecropin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00268">
    <property type="entry name" value="CECROPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004841" description="Cecropin-A1">
    <location>
      <begin position="24"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="modified residue" description="Arginine amide" evidence="1">
    <location>
      <position position="62"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="63" mass="6757" checksum="F709070C5BF4DE74" modified="1999-05-01" version="1" precursor="true">MNFYNIFVFVALILAITIGQSEAGWLKKIGKKIERVGQHTRDATIQGLGVAQQAANVAATARG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>