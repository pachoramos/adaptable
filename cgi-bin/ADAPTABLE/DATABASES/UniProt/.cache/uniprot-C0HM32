<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2022-10-12" modified="2022-12-14" version="2" xmlns="http://uniprot.org/uniprot">
  <accession>C0HM32</accession>
  <name>TPR_RANTE</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Temporin-1Tr</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Temporin-R</fullName>
    </alternativeName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Rana temporaria</name>
    <name type="common">European common frog</name>
    <dbReference type="NCBI Taxonomy" id="8407"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Rana</taxon>
      <taxon>Rana</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2021" name="Anal. Bioanal. Chem." volume="413" first="5333" last="5347">
      <title>Differentiation of Central Slovenian and Moscow populations of Rana temporaria frogs using peptide biomarkers of temporins family.</title>
      <authorList>
        <person name="Samgina T.Y."/>
        <person name="Vasileva I.D."/>
        <person name="Kovalev S.V."/>
        <person name="Trebse P."/>
        <person name="Torkar G."/>
        <person name="Surin A.K."/>
        <person name="Zubarev R.A."/>
        <person name="Lebedev A.T."/>
      </authorList>
      <dbReference type="PubMed" id="34235566"/>
      <dbReference type="DOI" id="10.1007/s00216-021-03506-1"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>AMIDATION AT LEU-16</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1694.07" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the frog skin active peptide (FSAP) family. Temporin subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000456398" description="Temporin-1Tr" evidence="2">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="2">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="2">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="2">
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="2">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P79875"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="34235566"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="34235566"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="34235566"/>
    </source>
  </evidence>
  <sequence length="16" mass="1696" checksum="0137C0120DB32AA7" modified="2022-10-12" version="1">LVPFLGRTLGGLLARL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>