<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-03-15" modified="2024-11-27" version="111" xmlns="http://uniprot.org/uniprot">
  <accession>Q9TU18</accession>
  <name>AGRP_PIG</name>
  <protein>
    <recommendedName>
      <fullName>Agouti-related protein</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">AGRP</name>
  </gene>
  <organism>
    <name type="scientific">Sus scrofa</name>
    <name type="common">Pig</name>
    <dbReference type="NCBI Taxonomy" id="9823"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Suina</taxon>
      <taxon>Suidae</taxon>
      <taxon>Sus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="J. Anim. Sci." volume="80" first="1388" last="1389">
      <title>Sequencing of the porcine agouti-related protein (AGRP) gene.</title>
      <authorList>
        <person name="Halverson M.J."/>
        <person name="Donelan K.J."/>
        <person name="Granholm N.H."/>
        <person name="Cheesbrough T.M."/>
        <person name="Westby C.A."/>
        <person name="Marshall D.M."/>
      </authorList>
      <dbReference type="PubMed" id="12019634"/>
      <dbReference type="DOI" id="10.2527/2002.8051388x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays a role in weight homeostasis. Involved in the control of feeding behavior through the central melanocortin system. Acts as alpha melanocyte-stimulating hormone antagonist by inhibiting cAMP production mediated by stimulation of melanocortin receptors within the hypothalamus and adrenal gland. Has very low activity with MC5R. Is an inverse agonist for MC3R and MC4R being able to suppress their constitutive activity. It promotes MC3R and MC4R endocytosis in an arrestin-dependent manner.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Interacts with melanocortin receptors MC3R, MC4R and MC5R.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Golgi apparatus lumen</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="2">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <dbReference type="EMBL" id="AF177762">
    <property type="protein sequence ID" value="AAF13865.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001011693.1">
    <property type="nucleotide sequence ID" value="NM_001011693.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_013853683.1">
    <property type="nucleotide sequence ID" value="XM_013998229.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_013853684.1">
    <property type="nucleotide sequence ID" value="XM_013998230.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9TU18"/>
  <dbReference type="SMR" id="Q9TU18"/>
  <dbReference type="STRING" id="9823.ENSSSCP00000002994"/>
  <dbReference type="PaxDb" id="9823-ENSSSCP00000002994"/>
  <dbReference type="Ensembl" id="ENSSSCT00000003073.4">
    <property type="protein sequence ID" value="ENSSSCP00000002994.1"/>
    <property type="gene ID" value="ENSSSCG00000002775.4"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00005006187">
    <property type="protein sequence ID" value="ENSSSCP00005003568"/>
    <property type="gene ID" value="ENSSSCG00005004181"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00015109310.1">
    <property type="protein sequence ID" value="ENSSSCP00015046454.1"/>
    <property type="gene ID" value="ENSSSCG00015080413.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00025102770.1">
    <property type="protein sequence ID" value="ENSSSCP00025045512.1"/>
    <property type="gene ID" value="ENSSSCG00025074577.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00030027169.1">
    <property type="protein sequence ID" value="ENSSSCP00030012100.1"/>
    <property type="gene ID" value="ENSSSCG00030019682.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00035041982.1">
    <property type="protein sequence ID" value="ENSSSCP00035016785.1"/>
    <property type="gene ID" value="ENSSSCG00035031706.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00040004181.1">
    <property type="protein sequence ID" value="ENSSSCP00040001315.1"/>
    <property type="gene ID" value="ENSSSCG00040003344.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00045041948.1">
    <property type="protein sequence ID" value="ENSSSCP00045029124.1"/>
    <property type="gene ID" value="ENSSSCG00045024624.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00050062839.1">
    <property type="protein sequence ID" value="ENSSSCP00050026978.1"/>
    <property type="gene ID" value="ENSSSCG00050046187.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00055011086.1">
    <property type="protein sequence ID" value="ENSSSCP00055008754.1"/>
    <property type="gene ID" value="ENSSSCG00055005697.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00060069922.1">
    <property type="protein sequence ID" value="ENSSSCP00060030150.1"/>
    <property type="gene ID" value="ENSSSCG00060051379.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00065028988.1">
    <property type="protein sequence ID" value="ENSSSCP00065011837.1"/>
    <property type="gene ID" value="ENSSSCG00065021799.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00070035263.1">
    <property type="protein sequence ID" value="ENSSSCP00070029450.1"/>
    <property type="gene ID" value="ENSSSCG00070017867.1"/>
  </dbReference>
  <dbReference type="GeneID" id="397275"/>
  <dbReference type="KEGG" id="ssc:397275"/>
  <dbReference type="CTD" id="181"/>
  <dbReference type="VGNC" id="VGNC:85189">
    <property type="gene designation" value="AGRP"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S7K0">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000154258"/>
  <dbReference type="HOGENOM" id="CLU_103790_0_0_1"/>
  <dbReference type="InParanoid" id="Q9TU18"/>
  <dbReference type="OMA" id="SWAMLQG"/>
  <dbReference type="OrthoDB" id="4017051at2759"/>
  <dbReference type="TreeFam" id="TF330729"/>
  <dbReference type="Proteomes" id="UP000008227">
    <property type="component" value="Chromosome 6"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000314985">
    <property type="component" value="Chromosome 6"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694570">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694571">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694720">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694722">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694723">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694724">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694725">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694726">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694727">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694728">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSSSCG00000002775">
    <property type="expression patterns" value="Expressed in blood and 4 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005796">
    <property type="term" value="C:Golgi lumen"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070996">
    <property type="term" value="F:type 1 melanocortin receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031781">
    <property type="term" value="F:type 3 melanocortin receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031782">
    <property type="term" value="F:type 4 melanocortin receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008343">
    <property type="term" value="P:adult feeding behavior"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009755">
    <property type="term" value="P:hormone-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000253">
    <property type="term" value="P:positive regulation of feeding behavior"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060259">
    <property type="term" value="P:regulation of feeding behavior"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.760.10:FF:000001">
    <property type="entry name" value="Agouti-related protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.760.10">
    <property type="entry name" value="Agouti domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007733">
    <property type="entry name" value="Agouti"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027300">
    <property type="entry name" value="Agouti_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036836">
    <property type="entry name" value="Agouti_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16551">
    <property type="entry name" value="AGOUTI RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16551:SF4">
    <property type="entry name" value="AGOUTI-RELATED PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05039">
    <property type="entry name" value="Agouti"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00792">
    <property type="entry name" value="Agouti"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57055">
    <property type="entry name" value="Agouti-related protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60024">
    <property type="entry name" value="AGOUTI_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51150">
    <property type="entry name" value="AGOUTI_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0333">Golgi apparatus</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000434046" evidence="2">
    <location>
      <begin position="21"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000001036" description="Agouti-related protein">
    <location>
      <begin position="85"/>
      <end position="134"/>
    </location>
  </feature>
  <feature type="domain" description="Agouti" evidence="4">
    <location>
      <begin position="89"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="region of interest" description="Interaction with melanocortin receptors" evidence="1">
    <location>
      <begin position="113"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="site" description="Cleavage; by PCSK1" evidence="2">
    <location>
      <begin position="84"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 4">
    <location>
      <begin position="89"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 4">
    <location>
      <begin position="96"/>
      <end position="110"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 4">
    <location>
      <begin position="103"/>
      <end position="121"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 4">
    <location>
      <begin position="107"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 4">
    <location>
      <begin position="112"/>
      <end position="119"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="O00253"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000255" key="4">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00494"/>
    </source>
  </evidence>
  <sequence length="134" mass="14681" checksum="DA89AC087567889A" modified="2001-10-01" version="2" precursor="true">MLTTMLLSCALLLAMPTMLGAQIGLAPLEGIGRLDQALFPELQDLGLQPPLKRTTAERAEEALLQQAEAKALAEVLDPEGRKARSPRRCVRLHESCLGHQVPCCDPCATCYCRFFNAFCYCRKLGTATNPCSRT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>