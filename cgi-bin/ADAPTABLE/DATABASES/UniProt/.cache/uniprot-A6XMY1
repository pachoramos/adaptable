<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-04-22" modified="2022-05-25" version="20" xmlns="http://uniprot.org/uniprot">
  <accession>A6XMY1</accession>
  <name>ARA2_HYAAR</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Arasin 2</fullName>
      <shortName evidence="4">Ara-2</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Proline/arginine-rich antimicrobial peptide</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Hyas araneus</name>
    <name type="common">Atlantic lyre crab</name>
    <name type="synonym">Great spider crab</name>
    <dbReference type="NCBI Taxonomy" id="361634"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Eucarida</taxon>
      <taxon>Decapoda</taxon>
      <taxon>Pleocyemata</taxon>
      <taxon>Brachyura</taxon>
      <taxon>Eubrachyura</taxon>
      <taxon>Majoidea</taxon>
      <taxon>Majidae</taxon>
      <taxon>Hyas</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Dev. Comp. Immunol." volume="32" first="275" last="285">
      <title>Arasin 1, a proline-arginine-rich antimicrobial peptide isolated from the spider crab, Hyas araneus.</title>
      <authorList>
        <person name="Stensvag K."/>
        <person name="Haug T."/>
        <person name="Sperstad S.V."/>
        <person name="Rekdal O."/>
        <person name="Indrevoll B."/>
        <person name="Styrvold O.B."/>
      </authorList>
      <dbReference type="PubMed" id="17658600"/>
      <dbReference type="DOI" id="10.1016/j.dci.2007.06.002"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Hemocyte</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide that has a large activity spectrum with activity against Gram-positive, Gram-negative bacteria, as well as against fungi. Shows activity at micromolar concentrations. Displays minimal inhibitory concentration (MIC) values lower than minimal bactericidal concentrations (MBC). May have a dual mode of action depending on the peptide concentrations. At MIC concentrations, the peptide penetrates into the cytoplasm of target cells (tested on the Gram-negative E.Coli). The two inner membrane proteins YgdD and SbmA may be required for this uptake. At concentrations higher than MIC, arasin may act by disrupting membranes. Does not show hemolytic activity.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with chitin through the N-terminal region (26-48). This interaction may be important, since chitin is a component of the fungal cell wall, as well as of the crab exoskeleton (permitting a possible action of arasin in wound healing in case of lesions).</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Mainly expressed in hemocytes. No or very low expression in heart, gills, inestines, and epidermis.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Disulfide bonds are important for activity especially against Gram-negative bacteria, since the linearization of the peptide causes a strong decrease of activity on these bacteria.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=00988"/>
  </comment>
  <dbReference type="EMBL" id="DQ859905">
    <property type="protein sequence ID" value="ABI74602.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A6XMY1"/>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002376">
    <property type="term" value="P:immune system process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5002705399" description="Arasin 2" evidence="1">
    <location>
      <begin position="26"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="22"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Pro residues" evidence="2">
    <location>
      <begin position="29"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="50"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="52"/>
      <end position="57"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A6XMY0"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="17658600"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="17658600"/>
    </source>
  </evidence>
  <sequence length="67" mass="7463" checksum="5D55275BDC3FAD46" modified="2007-08-21" version="1" precursor="true">MERRTLLVVLLVCSCVVAAAAEASPSRWPSPGRPRPFPGRPNPIFRPRPCICVRQPCPCDTYGGNRW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>