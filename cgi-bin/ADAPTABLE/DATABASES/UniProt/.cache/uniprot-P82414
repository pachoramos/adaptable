<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-09-26" modified="2023-11-08" version="64" xmlns="http://uniprot.org/uniprot">
  <accession>P82414</accession>
  <name>GTX3A_NEOGO</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">M-poneritoxin-Ng3a</fullName>
      <shortName evidence="3">M-PONTX-Ng3a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Poneratoxin</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="2">Ponericin-G1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Neoponera goeldii</name>
    <name type="common">Ponerine ant</name>
    <name type="synonym">Pachycondyla goeldii</name>
    <dbReference type="NCBI Taxonomy" id="3057131"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Ponerinae</taxon>
      <taxon>Ponerini</taxon>
      <taxon>Neoponera</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="J. Biol. Chem." volume="276" first="17823" last="17829">
      <title>Ponericins, new antibacterial and insecticidal peptides from the venom of the ant Pachycondyla goeldii.</title>
      <authorList>
        <person name="Orivel J."/>
        <person name="Redeker V."/>
        <person name="Le Caer J.-P."/>
        <person name="Krier F."/>
        <person name="Revol-Junelles A.-M."/>
        <person name="Longeon A."/>
        <person name="Chafotte A."/>
        <person name="Dejean A."/>
        <person name="Rossier J."/>
      </authorList>
      <dbReference type="PubMed" id="11279030"/>
      <dbReference type="DOI" id="10.1074/jbc.m100216200"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2016" name="Toxins" volume="8" first="1" last="28">
      <title>The biochemical toxin arsenal from ant venoms.</title>
      <authorList>
        <person name="Touchard A."/>
        <person name="Aili S.R."/>
        <person name="Fox E.G."/>
        <person name="Escoubas P."/>
        <person name="Orivel J."/>
        <person name="Nicholson G.M."/>
        <person name="Dejean A."/>
      </authorList>
      <dbReference type="PubMed" id="26805882"/>
      <dbReference type="DOI" id="10.3390/toxins8010030"/>
    </citation>
    <scope>REVIEW</scope>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Shows a broad spectrum of activity against both Gram-positive and Gram-negative bacteria. Has also antimicrobial activity against S.cerevisiae. Has insecticidal and non-hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="3212.87" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the ponericin-G family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P82414"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010002">
    <property type="entry name" value="Poneritoxin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07442">
    <property type="entry name" value="Ponericin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000044185" description="M-poneritoxin-Ng3a" evidence="1">
    <location>
      <begin position="1"/>
      <end position="30"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11279030"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="11279030"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="26805882"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="11279030"/>
    </source>
  </evidence>
  <sequence length="30" mass="3214" checksum="4199B7C710469075" modified="2001-09-26" version="1">GWKDWAKKAGGWLKKKGPGMAKAALKAAMQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>