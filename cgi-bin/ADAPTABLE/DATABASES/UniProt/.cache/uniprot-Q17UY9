<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-09-05" modified="2022-05-25" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>Q17UY9</accession>
  <name>PLS4_PITAZ</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Phylloseptin-Az4</fullName>
      <shortName evidence="5">PLS-Az4</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4 6">Phylloseptin-12</fullName>
      <shortName evidence="4">PS-12</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">psn12</name>
    <name type="synonym">ppp-12</name>
    <name type="synonym">psn-12</name>
  </gene>
  <organism>
    <name type="scientific">Pithecopus azureus</name>
    <name type="common">Orange-legged monkey tree frog</name>
    <name type="synonym">Phyllomedusa azurea</name>
    <dbReference type="NCBI Taxonomy" id="2034991"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Pithecopus</taxon>
    </lineage>
  </organism>
  <reference evidence="5 6" key="1">
    <citation type="journal article" date="2007" name="Peptides" volume="28" first="1331" last="1343">
      <title>A combined mass spectrometric and cDNA sequencing approach to the isolation and characterization of novel antimicrobial peptides from the skin secretions of Phyllomedusa hypochondrialis azurea.</title>
      <authorList>
        <person name="Thompson A.H."/>
        <person name="Bjourson A.J."/>
        <person name="Orr D.F."/>
        <person name="Shaw C."/>
        <person name="McClean S."/>
      </authorList>
      <dbReference type="PubMed" id="17553595"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2007.05.001"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 38-56</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-56</scope>
    <source>
      <tissue>Skin</tissue>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Has antibacterial activity against the Gram-positive bacterium M.luteus ATCC 49732 (MIC=1.3 uM). Does not inhibit the growth of the fungus C.albicans.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1941.19" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the frog skin active peptide (FSAP) family. Phylloseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=00977"/>
  </comment>
  <dbReference type="EMBL" id="AM269411">
    <property type="protein sequence ID" value="CAK51560.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q17UY9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1" status="less than"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000248618" evidence="3">
    <location>
      <begin position="14"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000248619" description="Phylloseptin-Az4">
    <location>
      <begin position="38"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="16"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="3">
    <location>
      <position position="56"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="6">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="17553595"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="17553595"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="CAK51560.1"/>
    </source>
  </evidence>
  <sequence length="57" mass="6381" checksum="CCDCE6335D0275DF" modified="2006-07-25" version="1" precursor="true" fragment="single">LVLFLGLVSLSICEEEKRETEEEENDQEEDDKSEEKRFLSLLPSIVSGAVSLAKKLG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>