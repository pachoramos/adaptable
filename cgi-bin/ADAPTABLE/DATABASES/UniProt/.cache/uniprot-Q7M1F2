<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-03-24" modified="2023-02-22" version="51" xmlns="http://uniprot.org/uniprot">
  <accession>Q7M1F2</accession>
  <name>DEF1_CLITE</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cysteine-rich antimicrobial protein 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin AMP1</fullName>
      <shortName>CtAMP1</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Clitoria ternatea</name>
    <name type="common">Butterfly pea</name>
    <dbReference type="NCBI Taxonomy" id="43366"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Fabales</taxon>
      <taxon>Fabaceae</taxon>
      <taxon>Papilionoideae</taxon>
      <taxon>50 kb inversion clade</taxon>
      <taxon>NPAAA clade</taxon>
      <taxon>indigoferoid/millettioid clade</taxon>
      <taxon>Phaseoleae</taxon>
      <taxon>Clitoria</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="FEBS Lett." volume="368" first="257" last="262">
      <title>Isolation and characterisation of plant defensins from seeds of Asteraceae, Fabaceae, Hippocastanaceae and Saxifragaceae.</title>
      <authorList>
        <person name="Osborn R.W."/>
        <person name="De Samblanx G.W."/>
        <person name="Thevissen K."/>
        <person name="Goderis I."/>
        <person name="Torrekens S."/>
        <person name="Van Leuven F."/>
        <person name="Attenborough S."/>
        <person name="Rees S.B."/>
        <person name="Broekaert W.F."/>
      </authorList>
      <dbReference type="PubMed" id="7628617"/>
      <dbReference type="DOI" id="10.1016/0014-5793(95)00666-w"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2000" name="Mol. Plant Microbe Interact." volume="13" first="54" last="61">
      <title>Specific binding sites for an antifungal plant defensin from Dahlia (Dahlia merckii) on fungal cells are required for antifungal activity.</title>
      <authorList>
        <person name="Thevissen K."/>
        <person name="Osborn R.W."/>
        <person name="Acland D.P."/>
        <person name="Broekaert W.F."/>
      </authorList>
      <dbReference type="PubMed" id="10656585"/>
      <dbReference type="DOI" id="10.1094/mpmi.2000.13.1.54"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3">Possesses antimicrobial activity sensitive to inorganic cations. Binds specifically to the fungal plasma membrane. Has no inhibitory effect on insect gut alpha-amylase.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="PIR" id="S66219">
    <property type="entry name" value="S66219"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q7M1F2"/>
  <dbReference type="SMR" id="Q7M1F2"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00304">
    <property type="entry name" value="Gamma-thionin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000366951" description="Defensin-like protein 1">
    <location>
      <begin position="1"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="3"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="14"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="20"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="45"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10656585"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7628617"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="49" mass="5613" checksum="7958823CB255D2CE" modified="2003-12-15" version="1">NLCERASLTWTGNCGNTGHCDTQCRNWESAKHGACHKRGNWKCFCYFNC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>