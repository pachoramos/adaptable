<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2022-02-23" modified="2022-12-14" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>A0A1D3IY23</accession>
  <name>TOACP_TITOB</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Peptide ToAcP</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Tityus obscurus</name>
    <name type="common">Amazonian scorpion</name>
    <name type="synonym">Tityus cambridgei</name>
    <dbReference type="NCBI Taxonomy" id="1221240"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Tityus</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2016" name="Front. Microbiol." volume="7" first="1844" last="1844">
      <title>Activity of scorpion venom-derived antifungal peptides against planktonic cells of Candida spp. and Cryptococcus neoformans and Candida albicans biofilms.</title>
      <authorList>
        <person name="Guilhelmelli F."/>
        <person name="Vilela N."/>
        <person name="Smidt K.S."/>
        <person name="de Oliveira M.A."/>
        <person name="da Cunha Morales Alvares A."/>
        <person name="Rigonatto M.C."/>
        <person name="da Silva Costa P.H."/>
        <person name="Tavares A.H."/>
        <person name="de Freitas S.M."/>
        <person name="Nicola A.M."/>
        <person name="Franco O.L."/>
        <person name="Derengowski L.D."/>
        <person name="Schwartz E.F."/>
        <person name="Mortari M.R."/>
        <person name="Bocca A.L."/>
        <person name="Albuquerque P."/>
        <person name="Silva-Pereira I."/>
      </authorList>
      <dbReference type="PubMed" id="27917162"/>
      <dbReference type="DOI" id="10.3389/fmicb.2016.01844"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>SYNTHESIS OF 35-58</scope>
    <scope>PROBABLE AMIDATION AT LYS-58</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 4">Helical wheel projections predict no hydrophobic face, suggesting a non-amphipathic peptide (Probable). Does not show antifungal activity (PubMed:27917162).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the venom gland.</text>
  </comment>
  <dbReference type="EMBL" id="LT576031">
    <property type="protein sequence ID" value="SBQ16532.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A1D3IY23"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000454904" evidence="4">
    <location>
      <begin position="25"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5008915588" description="Peptide ToAcP" evidence="4">
    <location>
      <begin position="35"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000454905" evidence="4">
    <location>
      <begin position="59"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="modified residue" description="Alanine amide" evidence="4">
    <location>
      <position position="58"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27917162"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="27917162"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="27917162"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="5">
    <source>
      <dbReference type="EMBL" id="SBQ16532.1"/>
    </source>
  </evidence>
  <sequence length="65" mass="7368" checksum="97CDA065A71082A0" modified="2016-11-30" version="1" precursor="true">MKMKMIVVISILLIVFSLSSKAMSLEDEQESVQREEDDLLGFSEEDLKAIKEHRAKNAGRFDPAV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>