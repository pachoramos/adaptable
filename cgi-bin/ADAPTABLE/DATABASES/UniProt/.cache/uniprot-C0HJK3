<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2014-09-03" modified="2022-05-25" version="12" xmlns="http://uniprot.org/uniprot">
  <accession>C0HJK3</accession>
  <name>TX2B_DINQU</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">U1-poneritoxin-Dq2b</fullName>
      <shortName evidence="3">U1-PONTX-Dq2b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">Peptide Dq-1133</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="4">Poneratoxin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Dinoponera quadriceps</name>
    <name type="common">South American ant</name>
    <dbReference type="NCBI Taxonomy" id="609295"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Ponerinae</taxon>
      <taxon>Ponerini</taxon>
      <taxon>Dinoponera</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2013" name="J. Proteomics" volume="94" first="413" last="422">
      <title>Peptidomic comparison and characterization of the major components of the venom of the giant ant Dinoponera quadriceps collected in four different areas of Brazil.</title>
      <authorList>
        <person name="Cologna C.T."/>
        <person name="Cardoso Jdos S."/>
        <person name="Jourdan E."/>
        <person name="Degueldre M."/>
        <person name="Upert G."/>
        <person name="Gilles N."/>
        <person name="Uetanabaro A.P."/>
        <person name="Costa Neto E.M."/>
        <person name="Thonart P."/>
        <person name="de Pauw E."/>
        <person name="Quinton L."/>
      </authorList>
      <dbReference type="PubMed" id="24157790"/>
      <dbReference type="DOI" id="10.1016/j.jprot.2013.10.017"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2016" name="Toxins" volume="8" first="1" last="28">
      <title>The biochemical toxin arsenal from ant venoms.</title>
      <authorList>
        <person name="Touchard A."/>
        <person name="Aili S.R."/>
        <person name="Fox E.G."/>
        <person name="Escoubas P."/>
        <person name="Orivel J."/>
        <person name="Nicholson G.M."/>
        <person name="Dejean A."/>
      </authorList>
      <dbReference type="PubMed" id="26805882"/>
      <dbReference type="DOI" id="10.3390/toxins8010030"/>
    </citation>
    <scope>REVIEW</scope>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="4">May have antimicrobial properties, like most ant linear peptides.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="1133.7" method="Electrospray" evidence="1"/>
  <dbReference type="Proteomes" id="UP000515204">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0712">Selenocysteine</keyword>
  <feature type="peptide" id="PRO_0000430029" description="U1-poneritoxin-Dq2b" evidence="1">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="non-standard amino acid" description="Selenocysteine" evidence="1">
    <location>
      <position position="8"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="1">
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="1">
    <location>
      <position position="7"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="24157790"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="24157790"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="26805882"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="24157790"/>
    </source>
  </evidence>
  <sequence length="9" mass="1057" checksum="EF351737776729C1" modified="2014-09-03" version="1">AHFLPPLUL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>