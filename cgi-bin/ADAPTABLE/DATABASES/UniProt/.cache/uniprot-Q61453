<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-11-27" version="117" xmlns="http://uniprot.org/uniprot">
  <accession>Q61453</accession>
  <name>IL17_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Interleukin-17A</fullName>
      <shortName>IL-17</shortName>
      <shortName>IL-17A</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Cytotoxic T-lymphocyte-associated antigen 8</fullName>
      <shortName>CTLA-8</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Il17a</name>
    <name type="synonym">Ctla8</name>
    <name type="synonym">Il17</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="J. Immunol." volume="150" first="5445" last="5456">
      <title>CTLA-8, cloned from an activated T cell, bearing AU-rich messenger RNA instability sequences, and homologous to a herpesvirus saimiri gene.</title>
      <authorList>
        <person name="Rouvier E."/>
        <person name="Luciani M.-F."/>
        <person name="Mattei M.-G."/>
        <person name="Denizot F."/>
        <person name="Golstein P."/>
      </authorList>
      <dbReference type="PubMed" id="8390535"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1996" name="Gene" volume="168" first="223" last="225">
      <title>Complete nucleotide sequence of the mouse CTLA8 gene.</title>
      <authorList>
        <person name="Yao Z."/>
        <person name="Timour M."/>
        <person name="Painter S."/>
        <person name="Fanslow W."/>
        <person name="Spriggs M.K."/>
      </authorList>
      <dbReference type="PubMed" id="8654948"/>
      <dbReference type="DOI" id="10.1016/0378-1119(95)00778-4"/>
    </citation>
    <scope>ORGANISM IDENTIFICATION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1996" name="J. Interferon Cytokine Res." volume="16" first="611" last="617">
      <title>Mouse IL-17: a cytokine preferentially expressed by alpha beta TCR + CD4-CD8-T cells.</title>
      <authorList>
        <person name="Kennedy J."/>
        <person name="Rossi D.L."/>
        <person name="Zurawski S.M."/>
        <person name="Vega F. Jr."/>
        <person name="Kastelein R.A."/>
        <person name="Wagner J.L."/>
        <person name="Hannum C.H."/>
        <person name="Zlotnik A."/>
      </authorList>
      <dbReference type="PubMed" id="8877732"/>
      <dbReference type="DOI" id="10.1089/jir.1996.16.611"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>ORGANISM IDENTIFICATION</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Effector cytokine of innate and adaptive immune system involved in antimicrobial host defense and maintenance of tissue integrity. Signals via IL17RA-IL17RC heterodimeric receptor complex, triggering homotypic interaction of IL17RA and IL17RC chains with TRAF3IP2 adapter. This leads to downstream TRAF6-mediated activation of NF-kappa-B and MAPkinase pathways ultimately resulting in transcriptional activation of cytokines, chemokines, antimicrobial peptides and matrix metalloproteinases, with potential strong immune inflammation. Plays an important role in connecting T cell-mediated adaptive immunity and acute inflammatory response to destroy extracellular bacteria and fungi. As a signature effector cytokine of T-helper 17 cells (Th17), primarily induces neutrophil activation and recruitment at infection and inflammatory sites. In airway epithelium, mediates neutrophil chemotaxis via induction of CXCL1 and CXCL5 chemokines. In secondary lymphoid organs, contributes to germinal center formation by regulating the chemotactic response of B cells to CXCL12 and CXCL13, enhancing retention of B cells within the germinal centers, B cell somatic hypermutation rate and selection toward plasma cells. Effector cytokine of a subset of gamma-delta T cells that functions as part of an inflammatory circuit downstream IL1B, TLR2 and IL23A-IL12B to promote neutrophil recruitment for efficient bacterial clearance. Effector cytokine of innate immune cells including invariant natural killer cell (iNKT) and group 3 innate lymphoid cells that mediate initial neutrophilic inflammation. Involved in the maintenance of the integrity of epithelial barriers during homeostasis and pathogen infection. Upon acute injury, has a direct role in epithelial barrier formation by regulating OCLN localization and tight junction biogenesis. As part of the mucosal immune response induced by commensal bacteria, enhances host's ability to resist pathogenic bacterial and fungal infections by promoting neutrophil recruitment and antimicrobial peptides release. In synergy with IL17F, mediates the production of antimicrobial beta-defensins DEFB1, DEFB103A, and DEFB104A by mucosal epithelial cells, limiting the entry of microbes through the epithelial barriers. Involved in antiviral host defense through various mechanisms.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Homodimer. Forms complexes with IL17RA and IL17RC receptors with 2:1 binding stoichiometry: two receptor chains for one interleukin molecule. IL17A homodimer preferentially drives the formation of IL17RA-IL17RC heterodimeric receptor complex. IL17A homodimer adopts an asymmetrical ternary structure with one IL17RA molecule, allowing for high affinity interactions of one IL17A monomer with one IL17RA molecule (via D1 and D2 domains), while disfavoring binding of a second IL17RA molecule on the other IL17A monomer. Heterodimer with IL17F. IL17A-IL17F forms complexes with IL17RA-IL17RC, but with lower affinity when compared to IL17A homodimer. IL17RA and IL17RC chains cannot distinguish between IL17A and IL17F molecules, potentially enabling the formation of topologically distinct complexes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the IL-17 family.</text>
  </comment>
  <comment type="caution">
    <text evidence="7">Was originally thought to be from mouse but, on the basis of subsequent work (PubMed:8654948, PubMed:8877732) has been shown to be of rat origin.</text>
  </comment>
  <dbReference type="EMBL" id="L13839">
    <property type="protein sequence ID" value="AAA37490.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q61453"/>
  <dbReference type="SMR" id="Q61453"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000016664"/>
  <dbReference type="GlyCosmos" id="Q61453">
    <property type="glycosylation" value="1 site, No reported glycans"/>
  </dbReference>
  <dbReference type="GlyGen" id="Q61453">
    <property type="glycosylation" value="1 site"/>
  </dbReference>
  <dbReference type="PhosphoSitePlus" id="Q61453"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000016664"/>
  <dbReference type="AGR" id="RGD:2888"/>
  <dbReference type="RGD" id="2888">
    <property type="gene designation" value="Il17a"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S5A0">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="Q61453"/>
  <dbReference type="PhylomeDB" id="Q61453"/>
  <dbReference type="PRO" id="PR:Q61453"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009897">
    <property type="term" value="C:external side of plasma membrane"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005125">
    <property type="term" value="F:cytokine activity"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046982">
    <property type="term" value="F:protein heterodimerization activity"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042803">
    <property type="term" value="F:protein homodimerization activity"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002250">
    <property type="term" value="P:adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071385">
    <property type="term" value="P:cellular response to glucocorticoid stimulus"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071347">
    <property type="term" value="P:cellular response to interleukin-1"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0072537">
    <property type="term" value="P:fibroblast activation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010467">
    <property type="term" value="P:gene expression"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097530">
    <property type="term" value="P:granulocyte migration"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097400">
    <property type="term" value="P:interleukin-17-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0038173">
    <property type="term" value="P:interleukin-17A-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060729">
    <property type="term" value="P:intestinal epithelial structure maintenance"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030216">
    <property type="term" value="P:keratinocyte differentiation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043616">
    <property type="term" value="P:keratinocyte proliferation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0106015">
    <property type="term" value="P:negative regulation of inflammatory response to wounding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007219">
    <property type="term" value="P:Notch signaling pathway"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002225">
    <property type="term" value="P:positive regulation of antimicrobial peptide production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1903348">
    <property type="term" value="P:positive regulation of bicellular tight junction assembly"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000340">
    <property type="term" value="P:positive regulation of chemokine (C-X-C motif) ligand 1 production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1900017">
    <property type="term" value="P:positive regulation of cytokine production involved in inflammatory response"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032731">
    <property type="term" value="P:positive regulation of interleukin-1 beta production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032735">
    <property type="term" value="P:positive regulation of interleukin-12 production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032739">
    <property type="term" value="P:positive regulation of interleukin-16 production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032747">
    <property type="term" value="P:positive regulation of interleukin-23 production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032755">
    <property type="term" value="P:positive regulation of interleukin-6 production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045672">
    <property type="term" value="P:positive regulation of osteoclast differentiation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045944">
    <property type="term" value="P:positive regulation of transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032760">
    <property type="term" value="P:positive regulation of tumor necrosis factor production"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043200">
    <property type="term" value="P:response to amino acid"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009611">
    <property type="term" value="P:response to wounding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="FunFam" id="2.10.90.10:FF:000038">
    <property type="entry name" value="Interleukin-17A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.10.90.10">
    <property type="entry name" value="Cystine-knot cytokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029034">
    <property type="entry name" value="Cystine-knot_cytokine"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020440">
    <property type="entry name" value="IL-17_chr"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010345">
    <property type="entry name" value="IL-17_fam"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06083">
    <property type="entry name" value="IL17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01932">
    <property type="entry name" value="INTRLEUKIN17"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57501">
    <property type="entry name" value="Cystine-knot cytokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="4">
    <location>
      <begin position="1"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000015425" description="Interleukin-17A">
    <location>
      <begin position="18"/>
      <end position="150"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="5">
    <location>
      <begin position="54"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Polar residues" evidence="5">
    <location>
      <begin position="54"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="4">
    <location>
      <position position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="89"/>
      <end position="139"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="94"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3." evidence="6" ref="3">
    <original>I</original>
    <variation>L</variation>
    <location>
      <position position="46"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q16552"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="Q62386"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="4"/>
  <evidence type="ECO:0000256" key="5">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="8390535"/>
    </source>
  </evidence>
  <sequence length="150" mass="16877" checksum="EF13F33EDF9D689F" modified="1996-11-01" version="1" precursor="true">MCLMLLLLLNLEATVKAAVLIPQSSVCPNAEANNFLQNVKVNLKVINSLSSKASSRRPSDYLNRSTSPWTLSRNEDPDRYPSVIWEAQCRHQRCVNAEGKLDHHMNSVLIQQEILVLKREPEKCPFTFRVEKMLVGVGCTCVSSIVRHAS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>