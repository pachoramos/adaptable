<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-10-31" modified="2024-11-27" version="31" xmlns="http://uniprot.org/uniprot">
  <accession>P0DKM7</accession>
  <name>TU91_IOTOL</name>
  <protein>
    <recommendedName>
      <fullName>Turripeptide Lol9.1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Turripeptide OL11</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Iotyrris olangoensis</name>
    <name type="common">Sea snail</name>
    <name type="synonym">Lophiotoma olangoensis</name>
    <dbReference type="NCBI Taxonomy" id="2420066"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Caenogastropoda</taxon>
      <taxon>Neogastropoda</taxon>
      <taxon>Conoidea</taxon>
      <taxon>Turridae</taxon>
      <taxon>Iotyrris</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="J. Mol. Evol." volume="62" first="247" last="256">
      <title>Genes expressed in a turrid venom duct: divergence and similarity to conotoxins.</title>
      <authorList>
        <person name="Watkins M."/>
        <person name="Hillyard D.R."/>
        <person name="Olivera B.M."/>
      </authorList>
      <dbReference type="PubMed" id="16477526"/>
      <dbReference type="DOI" id="10.1007/s00239-005-0010-x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom duct</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Acts as a neurotoxin by inhibiting an ion channel (By similarity). May also act as a serine protease inhibitor, since it possess the kazal serine protease inhibitor signature.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom duct.</text>
  </comment>
  <comment type="domain">
    <text>The cysteine framework is IX (C-C-C-C-C-C).</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the conopeptide P-like superfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DKM7"/>
  <dbReference type="SMR" id="P0DKM7"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0099106">
    <property type="term" value="F:ion channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030154">
    <property type="term" value="P:cell differentiation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00104">
    <property type="entry name" value="KAZAL_FS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.60.30">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002350">
    <property type="entry name" value="Kazal_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036058">
    <property type="entry name" value="Kazal_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001239">
    <property type="entry name" value="Prot_inh_Kazal-m"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050653">
    <property type="entry name" value="Prot_Inhib_GrowthFact_Antg"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10913:SF45">
    <property type="entry name" value="AGRIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10913">
    <property type="entry name" value="FOLLISTATIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00050">
    <property type="entry name" value="Kazal_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00290">
    <property type="entry name" value="KAZALINHBTR"/>
  </dbReference>
  <dbReference type="SMART" id="SM00280">
    <property type="entry name" value="KAZAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF100895">
    <property type="entry name" value="Kazal-type serine protease inhibitors"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51465">
    <property type="entry name" value="KAZAL_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000419841" description="Turripeptide Lol9.1">
    <location>
      <begin position="21"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="domain" description="Kazal-like" evidence="3">
    <location>
      <begin position="21"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="site" description="Reactive bond" evidence="3">
    <location>
      <begin position="32"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="26"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="30"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="38"/>
      <end position="70"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00798"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="70" mass="7603" checksum="F8EC317B837EDCAA" modified="2012-10-31" version="1" precursor="true">MKVYCLLLVLLVGLVSQAHGKPTKRCLSVCSAEYEPVCGSDGKTYANKCHLMTEACWSPTSITLVHEGKC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>