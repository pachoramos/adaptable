<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-09-11" modified="2022-02-23" version="21" xmlns="http://uniprot.org/uniprot">
  <accession>P85262</accession>
  <name>TX35B_CTEON</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">U9-ctenitoxin-Co1a</fullName>
      <shortName evidence="4">U9-CNTX-Co1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Neurotoxin Oc FU-18</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Ctenus ornatus</name>
    <name type="common">Brazilian spider</name>
    <name type="synonym">Oligoctenus ornatus</name>
    <dbReference type="NCBI Taxonomy" id="406443"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Lycosoidea</taxon>
      <taxon>Ctenidae</taxon>
      <taxon>Oligoctenus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2007-07" db="UniProtKB">
      <authorList>
        <person name="Borges M.H."/>
        <person name="Oliveira C.F.B."/>
        <person name="Goncalves J.M."/>
        <person name="Rates B."/>
        <person name="Santos D.M."/>
        <person name="Pimenta A.M.C."/>
        <person name="Cordeiro M.N."/>
        <person name="Richardson M."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Insecticidal neurotoxin that reversibly inhibits the N-methyl-D-aspartate (NMDA)-subtype of ionotropic glutamate receptor (GRIN) and inhibits inactivation of insect sodium channels (Nav). In vivo, is highly toxic to insects.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="4">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="5173.9" error="0.02" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the neurotoxin 03 (Tx2) family. 05 subfamily.</text>
  </comment>
  <dbReference type="ArachnoServer" id="AS000320">
    <property type="toxin name" value="U9-ctenitoxin-Co1a"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035792">
    <property type="term" value="C:host cell postsynaptic membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035285">
    <property type="entry name" value="CNTX"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17492">
    <property type="entry name" value="D_CNTX"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-1028">Ionotropic glutamate receptor inhibitor</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0629">Postsynaptic neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000302109" description="U9-ctenitoxin-Co1a" evidence="5">
    <location>
      <begin position="1"/>
      <end position="44" status="greater than"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="3"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="10"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="14"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="16"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="25"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="5">
    <location>
      <position position="44"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P59367"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source ref="1"/>
  </evidence>
  <sequence length="44" mass="4631" checksum="44D832CF05964DAF" modified="2007-09-11" version="1" fragment="single">GKCGDINAPCTSACDCCGKSVECDCYWGKECSCRESFFGAATXL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>