<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-02-01" modified="2024-11-27" version="154" xmlns="http://uniprot.org/uniprot">
  <accession>P48973</accession>
  <accession>P70538</accession>
  <accession>Q6GTC7</accession>
  <name>CXL10_RAT</name>
  <protein>
    <recommendedName>
      <fullName>C-X-C motif chemokine 10</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>10 kDa interferon gamma-induced protein</fullName>
      <shortName>Gamma-IP10</shortName>
      <shortName>IP-10</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Interferon-inducible protein 10</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Protein Mob-1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Small-inducible cytokine B10</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Cxcl10</name>
    <name type="synonym">Inp10</name>
    <name type="synonym">Mob1</name>
    <name type="synonym">Scyb10</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Proc. Natl. Acad. Sci. U.S.A." volume="91" first="12515" last="12519">
      <title>Ras activation of genes: Mob-1 as a model.</title>
      <authorList>
        <person name="Liang P."/>
        <person name="Averboukh L."/>
        <person name="Zhu W."/>
        <person name="Pardee A.B."/>
      </authorList>
      <dbReference type="PubMed" id="7809069"/>
      <dbReference type="DOI" id="10.1073/pnas.91.26.12515"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1996" name="J. Biol. Chem." volume="271" first="24286" last="24293">
      <title>Interferon-inducible protein-10 involves vascular smooth muscle cell migration, proliferation, and inflammatory response.</title>
      <authorList>
        <person name="Wang X."/>
        <person name="Yue T.L."/>
        <person name="Ohlstein E.H."/>
        <person name="Sung C."/>
        <person name="Feuerstein G.Z."/>
      </authorList>
      <dbReference type="PubMed" id="8798675"/>
      <dbReference type="DOI" id="10.1074/jbc.271.39.24286"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Pituitary</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2018" name="Neurosci. Lett." volume="694" first="20" last="28">
      <title>Chemokine CXCL10/CXCR3 signaling contributes to neuropathic pain in spinal cord and dorsal root ganglia after chronic constriction injury in rats.</title>
      <authorList>
        <person name="Chen Y."/>
        <person name="Yin D."/>
        <person name="Fan B."/>
        <person name="Zhu X."/>
        <person name="Chen Q."/>
        <person name="Li Y."/>
        <person name="Zhu S."/>
        <person name="Lu R."/>
        <person name="Xu Z."/>
      </authorList>
      <dbReference type="PubMed" id="30448292"/>
      <dbReference type="DOI" id="10.1016/j.neulet.2018.11.021"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2018" name="Cell. Physiol. Biochem." volume="49" first="2214" last="2228">
      <title>Elevation of the Chemokine Pair CXCL10/CXCR3 Initiates Sequential Glial Activation and Crosstalk During the Development of Bimodal Inflammatory Pain after Spinal Cord Ischemia Reperfusion.</title>
      <authorList>
        <person name="Yu Q."/>
        <person name="Tian D.L."/>
        <person name="Tian Y."/>
        <person name="Zhao X.T."/>
        <person name="Yang X.Y."/>
      </authorList>
      <dbReference type="PubMed" id="30257241"/>
      <dbReference type="DOI" id="10.1159/000493825"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 4 5">Pro-inflammatory cytokine that is involved in a wide variety of processes such as chemotaxis, differentiation, and activation of peripheral immune cells, regulation of cell growth, apoptosis and modulation of angiostatic effects (By similarity). Plays thereby an important role during viral infections by stimulating the activation and migration of immune cells to the infected sites (By similarity). Mechanistically, binding of CXCL10 to the CXCR3 receptor activates G protein-mediated signaling and results in downstream activation of phospholipase C-dependent pathway, an increase in intracellular calcium production and actin reorganization. In turn, recruitment of activated Th1 lymphocytes occurs at sites of inflammation (By similarity). Activation of the CXCL10/CXCR3 axis also plays an important role in neurons in response to brain injury for activating microglia, the resident macrophage population of the central nervous system, and directing them to the lesion site. This recruitment is an essential element for neuronal reorganization (By similarity) (PubMed:30257241, PubMed:30448292).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer, dimer, and tetramer. Interacts with CXCR3 (via N-terminus).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4 5">In the central nervous system, CXCL10 is predominantly localized to activated neurons (PubMed:30448292). Expressed in both microglia and astrocytes (PubMed:30257241).</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the intercrine alpha (chemokine CxC) family.</text>
  </comment>
  <dbReference type="EMBL" id="U17035">
    <property type="protein sequence ID" value="AAB60485.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U22520">
    <property type="protein sequence ID" value="AAC52811.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC058444">
    <property type="protein sequence ID" value="AAH58444.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_620789.1">
    <property type="nucleotide sequence ID" value="NM_139089.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P48973"/>
  <dbReference type="SMR" id="P48973"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000003649"/>
  <dbReference type="PhosphoSitePlus" id="P48973"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000003649"/>
  <dbReference type="Ensembl" id="ENSRNOT00000003075.8">
    <property type="protein sequence ID" value="ENSRNOP00000003649.4"/>
    <property type="gene ID" value="ENSRNOG00000022256.7"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055010270">
    <property type="protein sequence ID" value="ENSRNOP00055007953"/>
    <property type="gene ID" value="ENSRNOG00055006321"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060031993">
    <property type="protein sequence ID" value="ENSRNOP00060026012"/>
    <property type="gene ID" value="ENSRNOG00060018593"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065030352">
    <property type="protein sequence ID" value="ENSRNOP00065024131"/>
    <property type="gene ID" value="ENSRNOG00065018120"/>
  </dbReference>
  <dbReference type="GeneID" id="245920"/>
  <dbReference type="KEGG" id="rno:245920"/>
  <dbReference type="UCSC" id="RGD:620209">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:620209"/>
  <dbReference type="CTD" id="3627"/>
  <dbReference type="RGD" id="620209">
    <property type="gene designation" value="Cxcl10"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S7MM">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000161759"/>
  <dbReference type="HOGENOM" id="CLU_143902_2_2_1"/>
  <dbReference type="InParanoid" id="P48973"/>
  <dbReference type="OMA" id="VRCTCIK"/>
  <dbReference type="OrthoDB" id="4264143at2759"/>
  <dbReference type="PhylomeDB" id="P48973"/>
  <dbReference type="TreeFam" id="TF333433"/>
  <dbReference type="Reactome" id="R-RNO-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P48973"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 14"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000022256">
    <property type="expression patterns" value="Expressed in spleen and 19 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009897">
    <property type="term" value="C:external side of plasma membrane"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042056">
    <property type="term" value="F:chemoattractant activity"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045236">
    <property type="term" value="F:CXCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048248">
    <property type="term" value="F:CXCR3 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005125">
    <property type="term" value="F:cytokine activity"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008201">
    <property type="term" value="F:heparin binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007189">
    <property type="term" value="P:adenylate cyclase-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0140374">
    <property type="term" value="P:antiviral innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034605">
    <property type="term" value="P:cellular response to heat"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097398">
    <property type="term" value="P:cellular response to interleukin-17"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0098586">
    <property type="term" value="P:cellular response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006935">
    <property type="term" value="P:chemotaxis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051607">
    <property type="term" value="P:defense response to virus"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042118">
    <property type="term" value="P:endothelial cell activation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007186">
    <property type="term" value="P:G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006955">
    <property type="term" value="P:immune response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016525">
    <property type="term" value="P:negative regulation of angiogenesis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045662">
    <property type="term" value="P:negative regulation of myoblast differentiation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1901740">
    <property type="term" value="P:negative regulation of myoblast fusion"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030593">
    <property type="term" value="P:neutrophil chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008284">
    <property type="term" value="P:positive regulation of cell population proliferation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002690">
    <property type="term" value="P:positive regulation of leukocyte chemotaxis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090026">
    <property type="term" value="P:positive regulation of monocyte chemotaxis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051281">
    <property type="term" value="P:positive regulation of release of sequestered calcium ion into cytosol"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000406">
    <property type="term" value="P:positive regulation of T cell migration"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042981">
    <property type="term" value="P:regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042127">
    <property type="term" value="P:regulation of cell population proliferation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1901509">
    <property type="term" value="P:regulation of endothelial tube morphogenesis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010819">
    <property type="term" value="P:regulation of T cell chemotaxis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010996">
    <property type="term" value="P:response to auditory stimulus"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009617">
    <property type="term" value="P:response to bacterium"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010332">
    <property type="term" value="P:response to gamma radiation"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032496">
    <property type="term" value="P:response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033280">
    <property type="term" value="P:response to vitamin D"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010818">
    <property type="term" value="P:T cell chemotaxis"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="CDD" id="cd00273">
    <property type="entry name" value="Chemokine_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000004">
    <property type="entry name" value="C-X-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001089">
    <property type="entry name" value="Chemokine_CXC"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018048">
    <property type="entry name" value="Chemokine_CXC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033899">
    <property type="entry name" value="CXC_Chemokine_domain"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF202">
    <property type="entry name" value="C-X-C MOTIF CHEMOKINE 10"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00436">
    <property type="entry name" value="INTERLEUKIN8"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00437">
    <property type="entry name" value="SMALLCYTKCXC"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00471">
    <property type="entry name" value="SMALL_CYTOKINES_CXC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0164">Citrullination</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005105" description="C-X-C motif chemokine 10">
    <location>
      <begin position="22"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="modified residue" description="Citrulline" evidence="1">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="30"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAB60485." evidence="6" ref="1">
    <original>P</original>
    <variation>T</variation>
    <location>
      <position position="39"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P02778"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P17515"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="30257241"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="30448292"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="98" mass="10729" checksum="5CAF8790695F72D5" modified="2007-05-01" version="2" precursor="true">MNPSAAVVLCLVLLSLSGTQGIPLARTVRCTCIDFHEQPLRPRAIGKLEIIPASLSCPHVEIIATMKKNNEKRCLNPESEAIKSLLKAVSQRRSKRAP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>