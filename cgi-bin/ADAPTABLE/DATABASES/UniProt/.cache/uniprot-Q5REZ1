<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-02-06" modified="2024-05-29" version="57" xmlns="http://uniprot.org/uniprot">
  <accession>Q5REZ1</accession>
  <name>SERP1_PONAB</name>
  <protein>
    <recommendedName>
      <fullName>Stress-associated endoplasmic reticulum protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Ribosome-attached membrane protein 4</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">SERP1</name>
    <name type="synonym">RAMP4</name>
  </gene>
  <organism>
    <name type="scientific">Pongo abelii</name>
    <name type="common">Sumatran orangutan</name>
    <name type="synonym">Pongo pygmaeus abelii</name>
    <dbReference type="NCBI Taxonomy" id="9601"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Pongo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2004-11" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="The German cDNA consortium"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Kidney</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Interacts with target proteins during their translocation into the lumen of the endoplasmic reticulum. Protects unfolded target proteins against degradation during ER stress. May facilitate glycosylation of target proteins after termination of ER stress. May modulate the use of N-glycosylation sites on target proteins.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with SEC61B, SEC61A1 and the SEC61 complex. Interacts with CANX.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Membrane</location>
      <topology evidence="1">Single-pass membrane protein</topology>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Endoplasmic reticulum membrane</location>
      <topology evidence="1">Single-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the RAMP4 family.</text>
  </comment>
  <dbReference type="EMBL" id="CR857372">
    <property type="protein sequence ID" value="CAH89666.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001124752.1">
    <property type="nucleotide sequence ID" value="NM_001131280.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5REZ1"/>
  <dbReference type="SMR" id="Q5REZ1"/>
  <dbReference type="STRING" id="9601.ENSPPYP00000015887"/>
  <dbReference type="GeneID" id="100171602"/>
  <dbReference type="KEGG" id="pon:100171602"/>
  <dbReference type="CTD" id="27230"/>
  <dbReference type="eggNOG" id="KOG3491">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="Q5REZ1"/>
  <dbReference type="OrthoDB" id="1326375at2759"/>
  <dbReference type="Proteomes" id="UP000001595">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005881">
    <property type="term" value="C:cytoplasmic microtubule"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005789">
    <property type="term" value="C:endoplasmic reticulum membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030968">
    <property type="term" value="P:endoplasmic reticulum unfolded protein response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010580">
    <property type="entry name" value="ER_stress-assoc"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15601">
    <property type="entry name" value="STRESS ASSOCIATED ENDOPLASMIC RETICULUM PROTEIN SERP1/RAMP4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15601:SF14">
    <property type="entry name" value="STRESS-ASSOCIATED ENDOPLASMIC RETICULUM PROTEIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06624">
    <property type="entry name" value="RAMP4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0256">Endoplasmic reticulum</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <keyword id="KW-0834">Unfolded protein response</keyword>
  <feature type="chain" id="PRO_0000274796" description="Stress-associated endoplasmic reticulum protein 1">
    <location>
      <begin position="1"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="topological domain" description="Cytoplasmic" evidence="2">
    <location>
      <begin position="1"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="39"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="topological domain" description="Extracellular" evidence="2">
    <location>
      <begin position="60"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="1"/>
      <end position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q9R2C1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="66" mass="7302" checksum="DC1696215AAE0D91" modified="2004-12-21" version="1">MVAKQRIRMANEKHSKNITQRGNVAKTSRNAPGEKASVGPWLLALFIFVVCGSAIFQIIQSIRMGM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>