<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-10-25" modified="2024-11-27" version="113" xmlns="http://uniprot.org/uniprot">
  <accession>P0C0L3</accession>
  <accession>P23929</accession>
  <accession>P77655</accession>
  <name>OSMC_SHIFL</name>
  <protein>
    <recommendedName>
      <fullName>Peroxiredoxin OsmC</fullName>
      <ecNumber>1.11.1.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Osmotically-inducible protein C</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">osmC</name>
    <name type="ordered locus">SF1743</name>
    <name type="ordered locus">S1876</name>
  </gene>
  <organism>
    <name type="scientific">Shigella flexneri</name>
    <dbReference type="NCBI Taxonomy" id="623"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Shigella</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Nucleic Acids Res." volume="30" first="4432" last="4441">
      <title>Genome sequence of Shigella flexneri 2a: insights into pathogenicity through comparison with genomes of Escherichia coli K12 and O157.</title>
      <authorList>
        <person name="Jin Q."/>
        <person name="Yuan Z."/>
        <person name="Xu J."/>
        <person name="Wang Y."/>
        <person name="Shen Y."/>
        <person name="Lu W."/>
        <person name="Wang J."/>
        <person name="Liu H."/>
        <person name="Yang J."/>
        <person name="Yang F."/>
        <person name="Zhang X."/>
        <person name="Zhang J."/>
        <person name="Yang G."/>
        <person name="Wu H."/>
        <person name="Qu D."/>
        <person name="Dong J."/>
        <person name="Sun L."/>
        <person name="Xue Y."/>
        <person name="Zhao A."/>
        <person name="Gao Y."/>
        <person name="Zhu J."/>
        <person name="Kan B."/>
        <person name="Ding K."/>
        <person name="Chen S."/>
        <person name="Cheng H."/>
        <person name="Yao Z."/>
        <person name="He B."/>
        <person name="Chen R."/>
        <person name="Ma D."/>
        <person name="Qiang B."/>
        <person name="Wen Y."/>
        <person name="Hou Y."/>
        <person name="Yu J."/>
      </authorList>
      <dbReference type="PubMed" id="12384590"/>
      <dbReference type="DOI" id="10.1093/nar/gkf566"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>301 / Serotype 2a</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2003" name="Infect. Immun." volume="71" first="2775" last="2786">
      <title>Complete genome sequence and comparative genomics of Shigella flexneri serotype 2a strain 2457T.</title>
      <authorList>
        <person name="Wei J."/>
        <person name="Goldberg M.B."/>
        <person name="Burland V."/>
        <person name="Venkatesan M.M."/>
        <person name="Deng W."/>
        <person name="Fournier G."/>
        <person name="Mayhew G.F."/>
        <person name="Plunkett G. III"/>
        <person name="Rose D.J."/>
        <person name="Darling A."/>
        <person name="Mau B."/>
        <person name="Perna N.T."/>
        <person name="Payne S.M."/>
        <person name="Runyen-Janecky L.J."/>
        <person name="Zhou S."/>
        <person name="Schwartz D.C."/>
        <person name="Blattner F.R."/>
      </authorList>
      <dbReference type="PubMed" id="12704152"/>
      <dbReference type="DOI" id="10.1128/iai.71.5.2775-2786.2003"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 700930 / 2457T / Serotype 2a</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Preferentially metabolizes organic hydroperoxides over inorganic hydrogen peroxide.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>a hydroperoxide + [protein]-dithiol = [protein]-disulfide + an alcohol + H2O</text>
      <dbReference type="Rhea" id="RHEA:10008"/>
      <dbReference type="Rhea" id="RHEA-COMP:10593"/>
      <dbReference type="Rhea" id="RHEA-COMP:10594"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:29950"/>
      <dbReference type="ChEBI" id="CHEBI:30879"/>
      <dbReference type="ChEBI" id="CHEBI:35924"/>
      <dbReference type="ChEBI" id="CHEBI:50058"/>
    </reaction>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the OsmC/Ohr family.</text>
  </comment>
  <dbReference type="EC" id="1.11.1.-"/>
  <dbReference type="EMBL" id="AE005674">
    <property type="protein sequence ID" value="AAN43315.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AE014073">
    <property type="protein sequence ID" value="AAP17201.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_707608.1">
    <property type="nucleotide sequence ID" value="NC_004337.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000152305.1">
    <property type="nucleotide sequence ID" value="NZ_WPGW01000136.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0C0L3"/>
  <dbReference type="SMR" id="P0C0L3"/>
  <dbReference type="STRING" id="198214.SF1743"/>
  <dbReference type="PaxDb" id="198214-SF1743"/>
  <dbReference type="GeneID" id="1024939"/>
  <dbReference type="GeneID" id="75203185"/>
  <dbReference type="KEGG" id="sfl:SF1743"/>
  <dbReference type="KEGG" id="sfx:S1876"/>
  <dbReference type="PATRIC" id="fig|198214.7.peg.2066"/>
  <dbReference type="HOGENOM" id="CLU_106355_1_0_6"/>
  <dbReference type="Proteomes" id="UP000001006">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000002673">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051920">
    <property type="term" value="F:peroxiredoxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="RHEA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006979">
    <property type="term" value="P:response to oxidative stress"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.300.20:FF:000010">
    <property type="entry name" value="OsmC family peroxiredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.300.20">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015946">
    <property type="entry name" value="KH_dom-like_a/b"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003718">
    <property type="entry name" value="OsmC/Ohr_fam"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036102">
    <property type="entry name" value="OsmC/Ohrsf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR052707">
    <property type="entry name" value="OsmC_Ohr_Peroxiredoxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019904">
    <property type="entry name" value="Peroxiredoxin_OsmC"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR03562">
    <property type="entry name" value="osmo_induc_OsmC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR42830">
    <property type="entry name" value="OSMOTICALLY INDUCIBLE FAMILY PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR42830:SF1">
    <property type="entry name" value="OSMOTICALLY INDUCIBLE FAMILY PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02566">
    <property type="entry name" value="OsmC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF82784">
    <property type="entry name" value="OsmC-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0049">Antioxidant</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-0575">Peroxidase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="initiator methionine" description="Removed" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000172730" description="Peroxiredoxin OsmC">
    <location>
      <begin position="2"/>
      <end position="143"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="1">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="143" mass="15088" checksum="A9096964BC962569" modified="2007-01-23" version="2">MTIHKKGQAHWEGDIKRGKGTVSTESGVLNQQPYGFNTRFEGEKGTNPEELIGAAHAACFSMALSLMLGEAGFTPTSIDTTADVSLDKVDAGFAITKIALKSEVAVPGIDASTFDGIIQKAKAGCPVSQVLKAEITLDYQLKS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>