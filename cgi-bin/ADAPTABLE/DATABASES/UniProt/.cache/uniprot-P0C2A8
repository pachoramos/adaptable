<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-01-09" modified="2022-05-25" version="23" xmlns="http://uniprot.org/uniprot">
  <accession>P0C2A8</accession>
  <name>CR119_RANGR</name>
  <protein>
    <recommendedName>
      <fullName>Caerin-1.19</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Caerin-1.19.1</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Caerin-1.19.2</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Caerin-1.19.3</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Ranoidea gracilenta</name>
    <name type="common">Dainty green tree frog</name>
    <name type="synonym">Litoria gracilenta</name>
    <dbReference type="NCBI Taxonomy" id="95133"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Toxicon" volume="47" first="664" last="675">
      <title>New caerin antibiotic peptides from the skin secretion of the dainty green tree frog Litoria gracilenta. Identification using positive and negative ion electrospray mass spectrometry.</title>
      <authorList>
        <person name="Maclean M.J."/>
        <person name="Brinkworth C.S."/>
        <person name="Bilusich D."/>
        <person name="Bowie J.H."/>
        <person name="Doyle J.R."/>
        <person name="Llewellyn L.E."/>
        <person name="Tyler M.J."/>
      </authorList>
      <dbReference type="PubMed" id="16554081"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2006.01.019"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT LEU-25</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SYNTHESIS OF 1-25</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Caerin-1.19 shows significant activity against Gram-positive organisms, but is less effective against Gram-negative organisms.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin dorsal glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2648.0" method="Electrospray" evidence="1">
    <molecule>Caerin-1.19</molecule>
  </comment>
  <comment type="mass spectrometry" mass="2478.0" method="Electrospray" evidence="1">
    <molecule>Caerin-1.19.1</molecule>
  </comment>
  <comment type="mass spectrometry" mass="2331.0" method="Electrospray" evidence="1">
    <molecule>Caerin-1.19.2</molecule>
  </comment>
  <comment type="mass spectrometry" mass="1992.0" method="Electrospray" evidence="1">
    <molecule>Caerin-1.19.3</molecule>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Caerin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0C2A8"/>
  <dbReference type="SMR" id="P0C2A8"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010000">
    <property type="entry name" value="Caerin_1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07440">
    <property type="entry name" value="Caerin_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000271470" description="Caerin-1.19">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000271471" description="Caerin-1.19.1">
    <location>
      <begin position="3"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000271472" description="Caerin-1.19.2">
    <location>
      <begin position="4"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000271473" description="Caerin-1.19.3">
    <location>
      <begin position="7"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="25"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="16554081"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="25" mass="2651" checksum="99A77460A7FD4FF2" modified="2007-01-09" version="1">GLFKVLGSVAKHLLPHVAPIIAEKL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>