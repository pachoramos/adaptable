<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2022-02-23" modified="2023-02-22" version="5" xmlns="http://uniprot.org/uniprot">
  <accession>C0HLY2</accession>
  <name>BR1_RANLT</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Brevinin-1LT</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Rana latastei</name>
    <name type="common">Italian agile frog</name>
    <dbReference type="NCBI Taxonomy" id="151453"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Rana</taxon>
      <taxon>Rana</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2016" name="Rapid Commun. Mass Spectrom." volume="30" first="265" last="276">
      <title>LTQ Orbitrap Velos in routine de novo sequencing of non-tryptic skin peptides from the frog Rana latastei with traditional and reliable manual spectra interpretation.</title>
      <authorList>
        <person name="Samgina T.Y."/>
        <person name="Tolpina M.D."/>
        <person name="Trebse P."/>
        <person name="Torkar G."/>
        <person name="Artemenko K.A."/>
        <person name="Bergquist J."/>
        <person name="Lebedev A.T."/>
      </authorList>
      <dbReference type="PubMed" id="27071218"/>
      <dbReference type="DOI" id="10.1002/rcm.7436"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DISULFIDE BOND</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2243.33" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012520">
    <property type="entry name" value="Antimicrobial_frog_1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08018">
    <property type="entry name" value="Antimicrobial_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000454222" description="Brevinin-1LT">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="14"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Assigned by comparison with orthologs" evidence="5">
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Assigned by comparison with orthologs" evidence="5">
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Assigned by comparison with orthologs" evidence="5">
    <location>
      <position position="6"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Assigned by comparison with orthologs" evidence="5">
    <location>
      <position position="8"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Assigned by comparison with orthologs" evidence="5">
    <location>
      <position position="12"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Assigned by comparison with orthologs" evidence="5">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Assigned by comparison with orthologs" evidence="5">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HL47"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27071218"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="27071218"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="27071218"/>
    </source>
  </evidence>
  <sequence length="20" mass="2246" checksum="9BD03806575CEB84" modified="2022-02-23" version="1">FFPIILGLVPKLVCLITKKC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>