<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-11" modified="2024-11-27" version="108" xmlns="http://uniprot.org/uniprot">
  <accession>P66062</accession>
  <accession>Q99W66</accession>
  <name>RL7_STAAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Large ribosomal subunit protein bL12</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">50S ribosomal protein L7/L12</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rplL</name>
    <name type="ordered locus">MW0495</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Forms part of the ribosomal stalk which helps the ribosome interact with GTP-bound translation factors. Is thus essential for accurate translation.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer. Part of the ribosomal stalk of the 50S ribosomal subunit. Forms a multimeric L10(L12)X complex, where L10 forms an elongated spine to which 2 to 4 L12 dimers bind in a sequential fashion. Binds GTP-bound translation factors.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the bacterial ribosomal protein bL12 family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB94360.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_001273586.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P66062"/>
  <dbReference type="SMR" id="P66062"/>
  <dbReference type="GeneID" id="66838832"/>
  <dbReference type="KEGG" id="sam:MW0495"/>
  <dbReference type="HOGENOM" id="CLU_086499_3_2_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0022625">
    <property type="term" value="C:cytosolic large ribosomal subunit"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003729">
    <property type="term" value="F:mRNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006412">
    <property type="term" value="P:translation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="CDD" id="cd00387">
    <property type="entry name" value="Ribosomal_L7_L12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.5.710:FF:000002">
    <property type="entry name" value="50S ribosomal protein L7/L12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.1390.10:FF:000001">
    <property type="entry name" value="50S ribosomal protein L7/L12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.1390.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.5.710">
    <property type="entry name" value="Single helix bin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00368">
    <property type="entry name" value="Ribosomal_bL12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000206">
    <property type="entry name" value="Ribosomal_bL12"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013823">
    <property type="entry name" value="Ribosomal_bL12_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR014719">
    <property type="entry name" value="Ribosomal_bL12_C/ClpS-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008932">
    <property type="entry name" value="Ribosomal_bL12_oligo"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036235">
    <property type="entry name" value="Ribosomal_bL12_oligo_N_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00855">
    <property type="entry name" value="L12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR45987">
    <property type="entry name" value="39S RIBOSOMAL PROTEIN L12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR45987:SF4">
    <property type="entry name" value="39S RIBOSOMAL PROTEIN L12, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00542">
    <property type="entry name" value="Ribosomal_L12"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF16320">
    <property type="entry name" value="Ribosomal_L12_N"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54736">
    <property type="entry name" value="ClpS-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48300">
    <property type="entry name" value="Ribosomal protein L7/12, oligomerisation (N-terminal) domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <feature type="chain" id="PRO_0000157579" description="Large ribosomal subunit protein bL12">
    <location>
      <begin position="1"/>
      <end position="122"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00368"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="122" mass="12712" checksum="F82ECF574579AF87" modified="2004-10-11" version="1">MANHEQIIEAIKEMSVLELNDLVKAIEEEFGVTAAAPVAVAGAAGGADAAAEKTEFDVELTSAGSSKIKVVKAVKEATGLGLKDAKELVDGAPKVIKEALPKEEAEKLKEQLEEVGATVELK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>