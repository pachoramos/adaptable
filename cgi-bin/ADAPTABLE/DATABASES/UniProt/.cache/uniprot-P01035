<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="160" xmlns="http://uniprot.org/uniprot">
  <accession>P01035</accession>
  <accession>Q54A26</accession>
  <name>CYTC_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>Cystatin-C</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Colostrum thiol proteinase inhibitor</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Cystatin-3</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CST3</name>
  </gene>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Biochim. Biophys. Acta" volume="1343" first="203" last="210">
      <title>Molecular cloning and N-terminal analysis of bovine cystatin C identification of a full-length N-terminal region.</title>
      <authorList>
        <person name="Olsson S.-L."/>
        <person name="Ek B."/>
        <person name="Wilm M."/>
        <person name="Broberg S."/>
        <person name="Rask L."/>
        <person name="Bjoerk I."/>
      </authorList>
      <dbReference type="PubMed" id="9434110"/>
      <dbReference type="DOI" id="10.1016/s0167-4838(97)00110-6"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 66-83</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-31</scope>
    <scope>CHARACTERIZATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Cerebrospinal fluid</tissue>
      <tissue>Choroid plexus</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2003" name="Mol. Reprod. Dev." volume="65" first="9" last="18">
      <title>Characterization of gene expression profiles in early bovine pregnancy using a custom cDNA microarray.</title>
      <authorList>
        <person name="Ishiwata H."/>
        <person name="Katsuma S."/>
        <person name="Kizaki K."/>
        <person name="Patel O.V."/>
        <person name="Nakano H."/>
        <person name="Takahashi T."/>
        <person name="Imai K."/>
        <person name="Hirasawa A."/>
        <person name="Shiojima S."/>
        <person name="Ikawa H."/>
        <person name="Suzuki Y."/>
        <person name="Tsujimoto G."/>
        <person name="Izaike Y."/>
        <person name="Todoroki J."/>
        <person name="Hashizume K."/>
      </authorList>
      <dbReference type="PubMed" id="12658628"/>
      <dbReference type="DOI" id="10.1002/mrd.10292"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2005-11" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="NIH - Mammalian Gene Collection (MGC) project"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Crossbred X Angus</strain>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1985" name="FEBS Lett." volume="186" first="41" last="45">
      <title>Complete amino acid sequence of bovine colostrum low-Mr cysteine proteinase inhibitor.</title>
      <authorList>
        <person name="Hirado M."/>
        <person name="Tsunasawa S."/>
        <person name="Sakiyama F."/>
        <person name="Niinobe M."/>
        <person name="Fujii S."/>
      </authorList>
      <dbReference type="PubMed" id="3891407"/>
      <dbReference type="DOI" id="10.1016/0014-5793(85)81335-1"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 37-148</scope>
  </reference>
  <comment type="function">
    <text>This is a thiol proteinase inhibitor.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="13420.0" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the cystatin family.</text>
  </comment>
  <dbReference type="EMBL" id="Y10811">
    <property type="protein sequence ID" value="CAA71771.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB098964">
    <property type="protein sequence ID" value="BAC56454.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB099041">
    <property type="protein sequence ID" value="BAC56531.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC109629">
    <property type="protein sequence ID" value="AAI09630.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A01271">
    <property type="entry name" value="UDBO"/>
  </dbReference>
  <dbReference type="PIR" id="S62326">
    <property type="entry name" value="S62326"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_776454.1">
    <property type="nucleotide sequence ID" value="NM_174029.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01035"/>
  <dbReference type="SMR" id="P01035"/>
  <dbReference type="STRING" id="9913.ENSBTAP00000057473"/>
  <dbReference type="MEROPS" id="I25.003"/>
  <dbReference type="MEROPS" id="I25.004"/>
  <dbReference type="PaxDb" id="9913-ENSBTAP00000000790"/>
  <dbReference type="PeptideAtlas" id="P01035"/>
  <dbReference type="Ensembl" id="ENSBTAT00000000790.6">
    <property type="protein sequence ID" value="ENSBTAP00000000790.4"/>
    <property type="gene ID" value="ENSBTAG00000000598.6"/>
  </dbReference>
  <dbReference type="GeneID" id="281102"/>
  <dbReference type="KEGG" id="bta:281102"/>
  <dbReference type="CTD" id="1471"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSBTAG00000000598"/>
  <dbReference type="eggNOG" id="ENOG502SC50">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000154755"/>
  <dbReference type="HOGENOM" id="CLU_118168_0_1_1"/>
  <dbReference type="InParanoid" id="P01035"/>
  <dbReference type="OMA" id="VKSSCQD"/>
  <dbReference type="OrthoDB" id="3086783at2759"/>
  <dbReference type="Reactome" id="R-BTA-381426">
    <property type="pathway name" value="Regulation of Insulin-like Growth Factor (IGF) transport and uptake by Insulin-like Growth Factor Binding Proteins (IGFBPs)"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-6798695">
    <property type="pathway name" value="Neutrophil degranulation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-8957275">
    <property type="pathway name" value="Post-translational protein phosphorylation"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Chromosome 13"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSBTAG00000000598">
    <property type="expression patterns" value="Expressed in floor plate of diencephalon and 106 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031982">
    <property type="term" value="C:vesicle"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00042">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.450.10:FF:000004">
    <property type="entry name" value="Cystatin C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.450.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000010">
    <property type="entry name" value="Cystatin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046350">
    <property type="entry name" value="Cystatin_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018073">
    <property type="entry name" value="Prot_inh_cystat_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46186">
    <property type="entry name" value="CYSTATIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46186:SF2">
    <property type="entry name" value="CYSTATIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00031">
    <property type="entry name" value="Cystatin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00043">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54403">
    <property type="entry name" value="Cystatin/monellin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00287">
    <property type="entry name" value="CYSTATIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000006638" description="Cystatin-C">
    <location>
      <begin position="31"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Secondary area of contact">
    <location>
      <begin position="84"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="site" description="Reactive site">
    <location>
      <position position="40"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="2">
    <location>
      <position position="31"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="102"/>
      <end position="112"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="126"/>
      <end position="146"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="9434110"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="148" mass="16265" checksum="EE740FE37CFB9F0E" modified="1999-07-15" version="2" precursor="true">MVGSPRAPLLLLASLIVALALALAVSPAAAQGPRKGRLLGGLMEADVNEEGVQEALSFAVSEFNKRSNDAYQSRVVRVVRARKQVVSGMNYFLDVELGRTTCTKSQANLDSCPFHNQPHLKREKLCSFQVYVVPWMNTINLVKFSCQD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>