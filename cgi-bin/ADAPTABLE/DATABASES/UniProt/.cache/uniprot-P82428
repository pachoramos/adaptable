<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-09-26" modified="2024-03-27" version="65" xmlns="http://uniprot.org/uniprot">
  <accession>P82428</accession>
  <name>WTX1F_NEOGO</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">M-poneritoxin-Ng1f</fullName>
      <shortName evidence="3">M-PONTX-Ng1f</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Poneratoxin</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="2">Ponericin-W6</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Neoponera goeldii</name>
    <name type="common">Ponerine ant</name>
    <name type="synonym">Pachycondyla goeldii</name>
    <dbReference type="NCBI Taxonomy" id="3057131"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Ponerinae</taxon>
      <taxon>Ponerini</taxon>
      <taxon>Neoponera</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="J. Biol. Chem." volume="276" first="17823" last="17829">
      <title>Ponericins, new antibacterial and insecticidal peptides from the venom of the ant Pachycondyla goeldii.</title>
      <authorList>
        <person name="Orivel J."/>
        <person name="Redeker V."/>
        <person name="Le Caer J.-P."/>
        <person name="Krier F."/>
        <person name="Revol-Junelles A.-M."/>
        <person name="Longeon A."/>
        <person name="Chafotte A."/>
        <person name="Dejean A."/>
        <person name="Rossier J."/>
      </authorList>
      <dbReference type="PubMed" id="11279030"/>
      <dbReference type="DOI" id="10.1074/jbc.m100216200"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LYS-20</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2016" name="Toxins" volume="8" first="1" last="28">
      <title>The biochemical toxin arsenal from ant venoms.</title>
      <authorList>
        <person name="Touchard A."/>
        <person name="Aili S.R."/>
        <person name="Fox E.G."/>
        <person name="Escoubas P."/>
        <person name="Orivel J."/>
        <person name="Nicholson G.M."/>
        <person name="Dejean A."/>
      </authorList>
      <dbReference type="PubMed" id="26805882"/>
      <dbReference type="DOI" id="10.3390/toxins8010030"/>
    </citation>
    <scope>REVIEW</scope>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has activity against Gram-positive bacteria. Has insecticidal and hemolytic activities. May act by disrupting the integrity of the bacterial cell membrane.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="4">Target cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="2029.4" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the non-disulfide-bridged peptide (NDBP) superfamily. Medium-length antimicrobial peptide (group 3) family. Ponericin-W subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P82428"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000044199" description="M-poneritoxin-Ng1f" evidence="1">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="modified residue" description="Lysine amide" evidence="1">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11279030"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="11279030"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="26805882"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="11279030"/>
    </source>
  </evidence>
  <sequence length="20" mass="2031" checksum="5241E1528A9A98EB" modified="2001-09-26" version="1">FIGTALGIASAIPAIVKLFK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>