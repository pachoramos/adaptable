<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-11-08" modified="2024-03-27" version="6" xmlns="http://uniprot.org/uniprot">
  <accession>A0A8K1YTT4</accession>
  <name>TP3_COTFL</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Teratocyte protein CftICK-III</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Cotesia flavipes</name>
    <name type="common">Parasitic wasp</name>
    <name type="synonym">Apanteles flavipes</name>
    <dbReference type="NCBI Taxonomy" id="89805"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Ichneumonoidea</taxon>
      <taxon>Braconidae</taxon>
      <taxon>Microgastrinae</taxon>
      <taxon>Cotesia</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2022" name="J. Insect Physiol." volume="139" first="104395" last="104395">
      <title>Proteotranscriptomics reveals the secretory dynamics of teratocytes, regulators of parasitization by an endoparasitoid wasp.</title>
      <authorList>
        <person name="Pinto C.P.G."/>
        <person name="Walker A.A."/>
        <person name="Robinson S.D."/>
        <person name="King G.F."/>
        <person name="Rossi G.D."/>
      </authorList>
      <dbReference type="PubMed" id="35413336"/>
      <dbReference type="DOI" id="10.1016/j.jinsphys.2022.104395"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2022" name="Insect Sci.">
      <title>Immunosuppressive, antimicrobial and insecticidal activities of inhibitor cystine knot peptides produced by teratocytes of the endoparasitoid wasp Cotesia flavipes (Hymenoptera: Braconidae).</title>
      <authorList>
        <person name="Pinto C.P.G."/>
        <person name="Walker A.A."/>
        <person name="King G.F."/>
        <person name="Rossi G.D."/>
      </authorList>
      <dbReference type="PubMed" id="36434808"/>
      <dbReference type="DOI" id="10.1111/1744-7917.13154"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 25-58</scope>
  </reference>
  <comment type="function">
    <text evidence="2">This endoparasitoid wasp peptide has immununosuppressive and insecticidal activities. Suppress cellular immunity which is detectable as a reduction of hemocyte encapsulation in the host. In vivo, ingestion of this peptide (probably at excessive doses) increases larval mortality and reduces leaf consumption in both lepidopteran species D.saccharalis and S.frugiperda, which are permissive and non-permissive hosts for C.flavipes, respectively.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Abundantly expressed by teratocytes, which are extra-embryonic cells released by parasitoid wasps into their hosts during larval eclosion.</text>
  </comment>
  <comment type="domain">
    <text evidence="4">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">Negative results: does not influence host total hemocyte count (PubMed:36434808). Does not influence hemocyte spread index in the host (PubMed:36434808). Has no effect on humoral immune system, since it does not influence the activities of prophenoloxidase and phenoloxidase in the hemolymph of larval Diatraea saccharalis (PubMed:36434808).</text>
  </comment>
  <dbReference type="EMBL" id="MZ746715">
    <property type="protein sequence ID" value="UEP64308.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5035418444" description="Teratocyte protein CftICK-III" evidence="4">
    <location>
      <begin position="25"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="27"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="34"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="41"/>
      <end position="55"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="36434808"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="36434808"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="36434808"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="5">
    <source>
      <dbReference type="EMBL" id="UEP64308.1"/>
    </source>
  </evidence>
  <sequence length="58" mass="6805" checksum="86FA4608F33CA09F" modified="2022-08-03" version="1" precursor="true">MQKFMRLFFLGLFFILFMTTQIKADGCLTYDSLCSTMYDKCCTSMKCKGFFFGYCKSI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>