<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-12-06" modified="2023-02-22" version="35" xmlns="http://uniprot.org/uniprot">
  <accession>P83282</accession>
  <name>CICD_CICFL</name>
  <protein>
    <recommendedName>
      <fullName>Cicadin</fullName>
    </recommendedName>
  </protein>
  <organism evidence="4">
    <name type="scientific">Cicada flammata</name>
    <dbReference type="NCBI Taxonomy" id="186872"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Paraneoptera</taxon>
      <taxon>Hemiptera</taxon>
      <taxon>Auchenorrhyncha</taxon>
      <taxon>Cicadoidea</taxon>
      <taxon>Cicadidae</taxon>
      <taxon>Cicadinae</taxon>
      <taxon>Cicadini</taxon>
      <taxon>Cicada</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2002" name="Peptides" volume="23" first="7" last="11">
      <title>Isolation of cicadin, a novel and potent antifungal peptide from dried juvenile cicadas.</title>
      <authorList>
        <person name="Wang H."/>
        <person name="Ng T.B."/>
      </authorList>
      <dbReference type="PubMed" id="11814612"/>
      <dbReference type="DOI" id="10.1016/s0196-9781(01)00573-3"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Possesses antifungal activity against B.cinerea, M.arachidicola, F.oxysporum, R.solani and C.comatus.</text>
  </comment>
  <comment type="function">
    <text evidence="2">Suppresses the activity of HIV-1 reverse transcriptase and stimulates the proliferation of murine splenocytes.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P83282"/>
  <dbReference type="SMR" id="P83282"/>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008284">
    <property type="term" value="P:positive regulation of cell population proliferation"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.60">
    <property type="entry name" value="Antifungal protein domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023112">
    <property type="entry name" value="Antifungal-protein_dom_sf"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <feature type="chain" id="PRO_0000089745" description="Cicadin">
    <location>
      <begin position="1"/>
      <end position="55" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="1"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="3">
    <location>
      <position position="55"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11814612"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="11814612"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="55" mass="6596" checksum="A232880A33519112" modified="2002-03-01" version="1" fragment="single">NEYHGFVDKANNENKRKKQQGRDDFVVKPNNFANRRRKDDYNENYYDDVDAADVV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>