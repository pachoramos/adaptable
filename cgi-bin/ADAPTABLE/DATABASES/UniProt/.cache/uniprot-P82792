<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-13" modified="2024-10-02" version="84" xmlns="http://uniprot.org/uniprot">
  <accession>P82792</accession>
  <name>DEF70_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Putative defensin-like protein 70</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Putative low-molecular-weight cysteine-rich protein 83</fullName>
      <shortName>Protein LCR83</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">LCR83</name>
    <name type="ordered locus">At5g54225</name>
    <name type="ORF">MDK4</name>
  </gene>
  <organism evidence="3">
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="1998" name="DNA Res." volume="5" first="131" last="145">
      <title>Structural analysis of Arabidopsis thaliana chromosome 5. V. Sequence features of the regions of 1,381,565 bp covered by twenty one physically assigned P1 and TAC clones.</title>
      <authorList>
        <person name="Kaneko T."/>
        <person name="Kotani H."/>
        <person name="Nakamura Y."/>
        <person name="Sato S."/>
        <person name="Asamizu E."/>
        <person name="Miyajima N."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="9679202"/>
      <dbReference type="DOI" id="10.1093/dnares/5.2.131"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference evidence="3" key="3">
    <citation type="journal article" date="2001" name="Plant Mol. Biol." volume="46" first="17" last="34">
      <title>Two large Arabidopsis thaliana gene families are homologous to the Brassica gene superfamily that encodes pollen coat proteins and the male component of the self-incompatibility response.</title>
      <authorList>
        <person name="Vanoosthuyse V."/>
        <person name="Miege C."/>
        <person name="Dumas C."/>
        <person name="Cock J.M."/>
      </authorList>
      <dbReference type="PubMed" id="11437247"/>
      <dbReference type="DOI" id="10.1023/a:1010664704926"/>
    </citation>
    <scope>IDENTIFICATION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="EMBL" id="AB010695">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002688">
    <property type="protein sequence ID" value="AED96470.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001032074.1">
    <property type="nucleotide sequence ID" value="NM_001036997.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P82792"/>
  <dbReference type="SMR" id="P82792"/>
  <dbReference type="PaxDb" id="3702-AT5G54225.1"/>
  <dbReference type="EnsemblPlants" id="AT5G54225.1">
    <property type="protein sequence ID" value="AT5G54225.1"/>
    <property type="gene ID" value="AT5G54225"/>
  </dbReference>
  <dbReference type="GeneID" id="3771507"/>
  <dbReference type="Gramene" id="AT5G54225.1">
    <property type="protein sequence ID" value="AT5G54225.1"/>
    <property type="gene ID" value="AT5G54225"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT5G54225"/>
  <dbReference type="Araport" id="AT5G54225"/>
  <dbReference type="TAIR" id="AT5G54225">
    <property type="gene designation" value="LCR83"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_2561419_0_0_1"/>
  <dbReference type="InParanoid" id="P82792"/>
  <dbReference type="OMA" id="LCFNPCT"/>
  <dbReference type="OrthoDB" id="648527at2759"/>
  <dbReference type="PhylomeDB" id="P82792"/>
  <dbReference type="PRO" id="PR:P82792"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 5"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P82792">
    <property type="expression patterns" value="baseline"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000017307" description="Putative defensin-like protein 70">
    <location>
      <begin position="28"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="39"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="52"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="56"/>
      <end position="79"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="82" mass="8817" checksum="9DB5C853B1B0A506" modified="2002-06-01" version="1" precursor="true">MKMESSKMLVVFTLMVLIAVSSDLVSGNFASGEASSQLCFNPCTPQLGNNECNTICMNKKYKEGSCVGFGIPPTSKYCCCKT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>