<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2014-04-16" modified="2024-11-27" version="52" xmlns="http://uniprot.org/uniprot">
  <accession>P9WME9</accession>
  <accession>L0TCJ1</accession>
  <accession>P67747</accession>
  <accession>Q10810</accession>
  <name>HTMR_MYCTU</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">HTH-type transcriptional repressor Rv2887</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">Rv2887</name>
    <name type="ORF">MTCY274.18</name>
  </gene>
  <organism>
    <name type="scientific">Mycobacterium tuberculosis (strain ATCC 25618 / H37Rv)</name>
    <dbReference type="NCBI Taxonomy" id="83332"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Mycobacteriales</taxon>
      <taxon>Mycobacteriaceae</taxon>
      <taxon>Mycobacterium</taxon>
      <taxon>Mycobacterium tuberculosis complex</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Nature" volume="393" first="537" last="544">
      <title>Deciphering the biology of Mycobacterium tuberculosis from the complete genome sequence.</title>
      <authorList>
        <person name="Cole S.T."/>
        <person name="Brosch R."/>
        <person name="Parkhill J."/>
        <person name="Garnier T."/>
        <person name="Churcher C.M."/>
        <person name="Harris D.E."/>
        <person name="Gordon S.V."/>
        <person name="Eiglmeier K."/>
        <person name="Gas S."/>
        <person name="Barry C.E. III"/>
        <person name="Tekaia F."/>
        <person name="Badcock K."/>
        <person name="Basham D."/>
        <person name="Brown D."/>
        <person name="Chillingworth T."/>
        <person name="Connor R."/>
        <person name="Davies R.M."/>
        <person name="Devlin K."/>
        <person name="Feltwell T."/>
        <person name="Gentles S."/>
        <person name="Hamlin N."/>
        <person name="Holroyd S."/>
        <person name="Hornsby T."/>
        <person name="Jagels K."/>
        <person name="Krogh A."/>
        <person name="McLean J."/>
        <person name="Moule S."/>
        <person name="Murphy L.D."/>
        <person name="Oliver S."/>
        <person name="Osborne J."/>
        <person name="Quail M.A."/>
        <person name="Rajandream M.A."/>
        <person name="Rogers J."/>
        <person name="Rutter S."/>
        <person name="Seeger K."/>
        <person name="Skelton S."/>
        <person name="Squares S."/>
        <person name="Squares R."/>
        <person name="Sulston J.E."/>
        <person name="Taylor K."/>
        <person name="Whitehead S."/>
        <person name="Barrell B.G."/>
      </authorList>
      <dbReference type="PubMed" id="9634230"/>
      <dbReference type="DOI" id="10.1038/31159"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 25618 / H37Rv</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2011" name="Mol. Cell. Proteomics" volume="10" first="M111.011627" last="M111.011627">
      <title>Proteogenomic analysis of Mycobacterium tuberculosis by high resolution mass spectrometry.</title>
      <authorList>
        <person name="Kelkar D.S."/>
        <person name="Kumar D."/>
        <person name="Kumar P."/>
        <person name="Balakrishnan L."/>
        <person name="Muthusamy B."/>
        <person name="Yadav A.K."/>
        <person name="Shrivastava P."/>
        <person name="Marimuthu A."/>
        <person name="Anand S."/>
        <person name="Sundaram H."/>
        <person name="Kingsbury R."/>
        <person name="Harsha H.C."/>
        <person name="Nair B."/>
        <person name="Prasad T.S."/>
        <person name="Chauhan D.S."/>
        <person name="Katoch K."/>
        <person name="Katoch V.M."/>
        <person name="Kumar P."/>
        <person name="Chaerkady R."/>
        <person name="Ramachandran S."/>
        <person name="Dash D."/>
        <person name="Pandey A."/>
      </authorList>
      <dbReference type="PubMed" id="21969609"/>
      <dbReference type="DOI" id="10.1074/mcp.m111.011445"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <strain>ATCC 25618 / H37Rv</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2015" name="Antimicrob. Agents Chemother." volume="59" first="6873" last="6881">
      <title>Mutation of Rv2887, a marR-like gene, confers Mycobacterium tuberculosis resistance to an imidazopyridine-based agent.</title>
      <authorList>
        <person name="Winglee K."/>
        <person name="Lun S."/>
        <person name="Pieroni M."/>
        <person name="Kozikowski A."/>
        <person name="Bishai W."/>
      </authorList>
      <dbReference type="PubMed" id="26303802"/>
      <dbReference type="DOI" id="10.1128/aac.01341-15"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>H37Rv</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2016" name="Proc. Natl. Acad. Sci. U.S.A." volume="113" first="E4523" last="E4530">
      <title>N-methylation of a bactericidal compound as a resistance mechanism in Mycobacterium tuberculosis.</title>
      <authorList>
        <person name="Warrier T."/>
        <person name="Kapilashrami K."/>
        <person name="Argyrou A."/>
        <person name="Ioerger T.R."/>
        <person name="Little D."/>
        <person name="Murphy K.C."/>
        <person name="Nandakumar M."/>
        <person name="Park S."/>
        <person name="Gold B."/>
        <person name="Mi J."/>
        <person name="Zhang T."/>
        <person name="Meiler E."/>
        <person name="Rees M."/>
        <person name="Somersan-Karakaya S."/>
        <person name="Porras-De Francisco E."/>
        <person name="Martinez-Hoyos M."/>
        <person name="Burns-Huang K."/>
        <person name="Roberts J."/>
        <person name="Ling Y."/>
        <person name="Rhee K.Y."/>
        <person name="Mendoza-Losana A."/>
        <person name="Luo M."/>
        <person name="Nathan C.F."/>
      </authorList>
      <dbReference type="PubMed" id="27432954"/>
      <dbReference type="DOI" id="10.1073/pnas.1606590113"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DNA-BINDING</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <scope>MUTAGENESIS OF ARG-81</scope>
    <source>
      <strain>H37Rv</strain>
    </source>
  </reference>
  <reference evidence="6 7 8 9" key="5">
    <citation type="journal article" date="2017" name="Sci. Rep." volume="7" first="6471" last="6471">
      <title>Structural analysis of the regulatory mechanism of MarR protein Rv2887 in M. tuberculosis.</title>
      <authorList>
        <person name="Gao Y.R."/>
        <person name="Li D.F."/>
        <person name="Fleming J."/>
        <person name="Zhou Y.F."/>
        <person name="Liu Y."/>
        <person name="Deng J.Y."/>
        <person name="Zhou L."/>
        <person name="Zhou J."/>
        <person name="Zhu G.F."/>
        <person name="Zhang X.E."/>
        <person name="Wang D.C."/>
        <person name="Bi L.J."/>
      </authorList>
      <dbReference type="PubMed" id="28743871"/>
      <dbReference type="DOI" id="10.1038/s41598-017-01705-4"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.90 ANGSTROMS) OF APOPROTEIN AND IN COMPLEXES WITH DNA; P-AMINOSALICYLIC ACID AND SALICYLIC ACID</scope>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>SUBUNIT</scope>
    <scope>DOMAIN</scope>
    <scope>MUTAGENESIS OF ARG-42 AND ASP-114</scope>
    <source>
      <strain>H37Rv</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 3 4">Represses expression of the HQNO methyltransferase htm gene (Rv0560c) by binding to its promoter region (PubMed:26303802, PubMed:27432954, PubMed:28743871). Also represses the expression of at least five other genes, including the methyltransferase Rv0558 (PubMed:26303802). Binds salicylate (SA), para-aminosalicylic acid (PAS) and gemfibrozil (PubMed:28743871).</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="4">Binding of effector molecule SA, or its structural analog PAS, to the binding pocket of Rv2887 induces conformational change in its DNA binding domain, preventing binding to the promoter region of htm, triggering the expression of this SAM-dependent methyltransferase.</text>
  </comment>
  <comment type="subunit">
    <text evidence="4">Homodimer.</text>
  </comment>
  <comment type="domain">
    <text evidence="4">Conformational changes take place in the N-terminal and the winged-HTH (wHTH) domain on ligand or DNA binding. SA and PAS stabilize a conformation of Rv2887 incompatible with DNA binding. Rotation of the N-terminal plays a key role in the dimerization.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="2 3">Mutation of the gene leads to derepression and increased expression of htm (PubMed:27432954). Inactivation of the gene confers resistance to the imidazo[1,2-a]pyridine-4-carbonitrile-based agent MP-III-71, an effective antimycobacterial compound that shows no cross-resistance to existing antituberculosis drugs (PubMed:26303802).</text>
  </comment>
  <dbReference type="EMBL" id="AL123456">
    <property type="protein sequence ID" value="CCP45689.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="B70925">
    <property type="entry name" value="B70925"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_217403.1">
    <property type="nucleotide sequence ID" value="NC_000962.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003899525.1">
    <property type="nucleotide sequence ID" value="NZ_NVQJ01000006.1"/>
  </dbReference>
  <dbReference type="PDB" id="5HSM">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.90 A"/>
    <property type="chains" value="A=1-139"/>
  </dbReference>
  <dbReference type="PDB" id="5HSO">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="A/B/C/D=1-139"/>
  </dbReference>
  <dbReference type="PDB" id="5X7Z">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.20 A"/>
    <property type="chains" value="A=1-139"/>
  </dbReference>
  <dbReference type="PDB" id="5X80">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.40 A"/>
    <property type="chains" value="A/B/C/D=1-139"/>
  </dbReference>
  <dbReference type="PDBsum" id="5HSM"/>
  <dbReference type="PDBsum" id="5HSO"/>
  <dbReference type="PDBsum" id="5X7Z"/>
  <dbReference type="PDBsum" id="5X80"/>
  <dbReference type="AlphaFoldDB" id="P9WME9"/>
  <dbReference type="SMR" id="P9WME9"/>
  <dbReference type="STRING" id="83332.Rv2887"/>
  <dbReference type="PaxDb" id="83332-Rv2887"/>
  <dbReference type="DNASU" id="888563"/>
  <dbReference type="GeneID" id="45426875"/>
  <dbReference type="GeneID" id="888563"/>
  <dbReference type="KEGG" id="mtu:Rv2887"/>
  <dbReference type="KEGG" id="mtv:RVBD_2887"/>
  <dbReference type="TubercuList" id="Rv2887"/>
  <dbReference type="eggNOG" id="COG1846">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="InParanoid" id="P9WME9"/>
  <dbReference type="OrthoDB" id="3177763at2"/>
  <dbReference type="PhylomeDB" id="P9WME9"/>
  <dbReference type="Proteomes" id="UP000001584">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003700">
    <property type="term" value="F:DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006355">
    <property type="term" value="P:regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006950">
    <property type="term" value="P:response to stress"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.10.10">
    <property type="entry name" value="Winged helix-like DNA-binding domain superfamily/Winged helix DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000835">
    <property type="entry name" value="HTH_MarR-typ"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039422">
    <property type="entry name" value="MarR/SlyA-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023187">
    <property type="entry name" value="Tscrpt_reg_MarR-type_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036388">
    <property type="entry name" value="WH-like_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036390">
    <property type="entry name" value="WH_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33164:SF54">
    <property type="entry name" value="HTH-TYPE TRANSCRIPTIONAL REPRESSOR RV2887"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33164">
    <property type="entry name" value="TRANSCRIPTIONAL REGULATOR, MARR FAMILY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01047">
    <property type="entry name" value="MarR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00598">
    <property type="entry name" value="HTHMARR"/>
  </dbReference>
  <dbReference type="SMART" id="SM00347">
    <property type="entry name" value="HTH_MARR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46785">
    <property type="entry name" value="Winged helix' DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01117">
    <property type="entry name" value="HTH_MARR_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50995">
    <property type="entry name" value="HTH_MARR_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0678">Repressor</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_0000054410" description="HTH-type transcriptional repressor Rv2887">
    <location>
      <begin position="1"/>
      <end position="139"/>
    </location>
  </feature>
  <feature type="domain" description="HTH marR-type" evidence="1">
    <location>
      <begin position="6"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="binding site" evidence="4">
    <location>
      <position position="42"/>
    </location>
    <ligand>
      <name>salicylate</name>
      <dbReference type="ChEBI" id="CHEBI:30762"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="4">
    <location>
      <position position="114"/>
    </location>
    <ligand>
      <name>salicylate</name>
      <dbReference type="ChEBI" id="CHEBI:30762"/>
    </ligand>
  </feature>
  <feature type="mutagenesis site" description="Attenuates the ability to bind salicylate." evidence="4">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="42"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Abolishes binding to Rv0560c promoter." evidence="3">
    <original>R</original>
    <variation>Q</variation>
    <location>
      <position position="81"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Attenuates the ability to bind salicylate." evidence="4">
    <original>D</original>
    <variation>A</variation>
    <location>
      <position position="114"/>
    </location>
  </feature>
  <feature type="turn" evidence="10">
    <location>
      <begin position="2"/>
      <end position="4"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="8"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="29"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="35"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="47"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="52"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="63"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="78"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="strand" evidence="12">
    <location>
      <begin position="85"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="97"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="123"/>
      <end position="136"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00345"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="26303802"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="27432954"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="28743871"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007744" key="6">
    <source>
      <dbReference type="PDB" id="5HSM"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="7">
    <source>
      <dbReference type="PDB" id="5HSO"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="8">
    <source>
      <dbReference type="PDB" id="5X7Z"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="9">
    <source>
      <dbReference type="PDB" id="5X80"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="10">
    <source>
      <dbReference type="PDB" id="5HSM"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="11">
    <source>
      <dbReference type="PDB" id="5X7Z"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="12">
    <source>
      <dbReference type="PDB" id="5X80"/>
    </source>
  </evidence>
  <sequence length="139" mass="14690" checksum="EB775A4CC03A24C0" modified="2014-04-16" version="1">MGLADDAPLGYLLYRVGAVLRPEVSAALSPLGLTLPEFVCLRMLSQSPGLSSAELARHASVTPQAMNTVLRKLEDAGAVARPASVSSGRSLPATLTARGRALAKRAEAVVRAADARVLARLTAPQQREFKRMLEKLGSD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>