<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-11" modified="2024-10-02" version="96" xmlns="http://uniprot.org/uniprot">
  <accession>P67527</accession>
  <accession>Q99UB8</accession>
  <name>Y1250_STAAW</name>
  <protein>
    <recommendedName>
      <fullName>Probable tautomerase MW1250</fullName>
      <ecNumber>5.3.2.-</ecNumber>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">MW1250</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="similarity">
    <text evidence="2">Belongs to the 4-oxalocrotonate tautomerase family.</text>
  </comment>
  <comment type="sequence caution" evidence="2">
    <conflict type="erroneous initiation">
      <sequence resource="EMBL-CDS" id="BAB95115" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EC" id="5.3.2.-"/>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB95115.1"/>
    <property type="status" value="ALT_INIT"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_001123276.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P67527"/>
  <dbReference type="SMR" id="P67527"/>
  <dbReference type="KEGG" id="sam:MW1250"/>
  <dbReference type="HOGENOM" id="CLU_148073_5_1_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016853">
    <property type="term" value="F:isomerase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00491">
    <property type="entry name" value="4Oxalocrotonate_Tautomerase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.429.10">
    <property type="entry name" value="Macrophage Migration Inhibitory Factor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018191">
    <property type="entry name" value="4-OT"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004370">
    <property type="entry name" value="4-OT-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR014347">
    <property type="entry name" value="Tautomerase/MIF_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00013">
    <property type="entry name" value="taut"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35530:SF1">
    <property type="entry name" value="TAUTOMERASE BSL7456-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35530">
    <property type="entry name" value="TAUTOMERASE-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01361">
    <property type="entry name" value="Tautomerase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55331">
    <property type="entry name" value="Tautomerase/MIF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0413">Isomerase</keyword>
  <feature type="initiator methionine" description="Removed" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000209545" description="Probable tautomerase MW1250">
    <location>
      <begin position="2"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="active site" description="Proton acceptor; via imino nitrogen" evidence="1">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="61" mass="6744" checksum="A62DD8FEAE2A59CE" modified="2007-01-23" version="2">MPIVNVKLLEGRSDEQLKNLVSEVTDAVEKTTGANRQAIHVVIEEMKPNHYGVAGVRKSDQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>