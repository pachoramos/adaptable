<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-12-12" modified="2024-03-27" version="46" xmlns="http://uniprot.org/uniprot">
  <accession>Q8T0Y7</accession>
  <name>CP2_APLCA</name>
  <protein>
    <recommendedName>
      <fullName>Neuropeptides CP2</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 1</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 2</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 3</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 4</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 5</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 6</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 7</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 8</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 9</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CP2-derived peptide 10</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Neuropeptide CP2</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Neuropeptide cerebral peptide 2</fullName>
      </alternativeName>
    </component>
  </protein>
  <gene>
    <name type="primary">CP2PP</name>
  </gene>
  <organism>
    <name type="scientific">Aplysia californica</name>
    <name type="common">California sea hare</name>
    <dbReference type="NCBI Taxonomy" id="6500"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Heterobranchia</taxon>
      <taxon>Euthyneura</taxon>
      <taxon>Tectipleura</taxon>
      <taxon>Aplysiida</taxon>
      <taxon>Aplysioidea</taxon>
      <taxon>Aplysiidae</taxon>
      <taxon>Aplysia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Peptides" volume="22" first="2027" last="2038">
      <title>Cloning, expression and processing of the CP2 neuropeptide precursor of Aplysia.</title>
      <authorList>
        <person name="Vilim F.S."/>
        <person name="Alexeeva V."/>
        <person name="Moroz L.L."/>
        <person name="Li L."/>
        <person name="Moroz T.P."/>
        <person name="Sweedler J.V."/>
        <person name="Weiss K.R."/>
      </authorList>
      <dbReference type="PubMed" id="11786187"/>
      <dbReference type="DOI" id="10.1016/s0196-9781(01)00561-7"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 27-68; 27-31; 34-68; 69-97; 69-87; 69-86; 69-83; 69-74; 89-97 AND 89-95</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>AMIDATION AT HIS-140</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>CNS</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2000" name="J. Neurophysiol." volume="84" first="1186" last="1193">
      <title>Intrinsic and extrinsic modulation of a single central pattern generating circuit.</title>
      <authorList>
        <person name="Morgan P.T."/>
        <person name="Perrins R."/>
        <person name="Lloyd P.E."/>
        <person name="Weiss K.R."/>
      </authorList>
      <dbReference type="PubMed" id="10979994"/>
      <dbReference type="DOI" id="10.1152/jn.2000.84.3.1186"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Mediates intrinsic neuromodulation.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Neurons.</text>
  </comment>
  <comment type="mass spectrometry" mass="622.3" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 2</molecule>
  </comment>
  <comment type="mass spectrometry" mass="636.36" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 8</molecule>
  </comment>
  <comment type="mass spectrometry" mass="832.34" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 10</molecule>
  </comment>
  <comment type="mass spectrometry" mass="1047.43" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 9</molecule>
  </comment>
  <comment type="mass spectrometry" mass="1701.85" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 7</molecule>
  </comment>
  <comment type="mass spectrometry" mass="2045.81" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 6</molecule>
  </comment>
  <comment type="mass spectrometry" mass="2133.12" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 5</molecule>
  </comment>
  <comment type="mass spectrometry" mass="3319.3" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 4</molecule>
  </comment>
  <comment type="mass spectrometry" mass="3779.2" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 3</molecule>
  </comment>
  <comment type="mass spectrometry" mass="4592.0" method="MALDI" evidence="3">
    <molecule>Neuropeptide CP2</molecule>
  </comment>
  <comment type="mass spectrometry" mass="4695.38" method="MALDI" evidence="3">
    <molecule>CP2-derived peptide 1</molecule>
  </comment>
  <dbReference type="EMBL" id="AY033828">
    <property type="protein sequence ID" value="AAK56550.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A57272">
    <property type="entry name" value="A57272"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001191491.1">
    <property type="nucleotide sequence ID" value="NM_001204562.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8T0Y7"/>
  <dbReference type="EnsemblMetazoa" id="NM_001204562.1">
    <property type="protein sequence ID" value="NP_001191491.1"/>
    <property type="gene ID" value="GeneID_100533251"/>
  </dbReference>
  <dbReference type="EnsemblMetazoa" id="XM_035971142.1">
    <property type="protein sequence ID" value="XP_035827035.1"/>
    <property type="gene ID" value="GeneID_100533251"/>
  </dbReference>
  <dbReference type="GeneID" id="100533251"/>
  <dbReference type="CTD" id="100533251"/>
  <dbReference type="Proteomes" id="UP000694888">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265124" description="CP2-derived peptide 1" evidence="3">
    <location>
      <begin position="27"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265125" description="CP2-derived peptide 2">
    <location>
      <begin position="27"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265126" description="CP2-derived peptide 3" evidence="3">
    <location>
      <begin position="34"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265127" description="CP2-derived peptide 4">
    <location>
      <begin position="69"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265128" description="CP2-derived peptide 5">
    <location>
      <begin position="69"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265129" description="CP2-derived peptide 6">
    <location>
      <begin position="69"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265130" description="CP2-derived peptide 7">
    <location>
      <begin position="69"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265131" description="CP2-derived peptide 8">
    <location>
      <begin position="69"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265132" description="CP2-derived peptide 9">
    <location>
      <begin position="89"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265133" description="CP2-derived peptide 10">
    <location>
      <begin position="89"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000265134" description="Neuropeptide CP2">
    <location>
      <begin position="100"/>
      <end position="140"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="75"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="modified residue" description="Histidine amide" evidence="3">
    <location>
      <position position="140"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10979994"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="11786187"/>
    </source>
  </evidence>
  <sequence length="141" mass="15654" checksum="B32BDC841E272BFB" modified="2002-06-01" version="1" precursor="true">MDSRICTSFARLMASALCVSTLLVTAMPFDLRRGSSDTDLDLQGHVDLGLDDLDKLRLIFPPGLIEEAFSQAQGKVDMPLPRQRTSSRSSERWAPKSKRFDFGFAGLDTYDAIHRALEQPARGTSNSGSGYNMLMKMQRHG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>