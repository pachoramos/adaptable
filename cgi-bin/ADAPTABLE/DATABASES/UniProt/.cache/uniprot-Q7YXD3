<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-01-16" modified="2024-05-29" version="70" xmlns="http://uniprot.org/uniprot">
  <accession>Q7YXD3</accession>
  <accession>Q2YHM2</accession>
  <name>SCX8_ANDMA</name>
  <protein>
    <recommendedName>
      <fullName evidence="10">Alpha-toxin Amm8</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="9">Alpha-anatoxin Amm VIII</fullName>
      <shortName evidence="6 7 8">Amm VIII</shortName>
      <shortName evidence="9">AmmVIII</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Neurotoxin 8</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>P4</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Androctonus mauritanicus mauritanicus</name>
    <name type="common">Scorpion</name>
    <dbReference type="NCBI Taxonomy" id="6860"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Androctonus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Biochem. J." volume="375" first="551" last="560">
      <title>Characterization of Amm VIII from Androctonus mauretanicus mauretanicus: a new scorpion toxin that discriminates between neuronal and skeletal sodium channels.</title>
      <authorList>
        <person name="Alami M."/>
        <person name="Vacher H."/>
        <person name="Bosmans F."/>
        <person name="Devaux C."/>
        <person name="Rosso J.-P."/>
        <person name="Bougis P.E."/>
        <person name="Tytgat J."/>
        <person name="Darbon H."/>
        <person name="Martin-Eauclaire M.-F."/>
      </authorList>
      <dbReference type="PubMed" id="12911331"/>
      <dbReference type="DOI" id="10.1042/bj20030688"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 20-84</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>TOXIC DOSE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="Toxicon" volume="47" first="531" last="536">
      <title>Genomic characterisation of the toxin Amm VIII from the scorpion Androctonus mauretanicus mauretanicus.</title>
      <authorList>
        <person name="Alami M."/>
        <person name="Ceard B."/>
        <person name="Legros C."/>
        <person name="Bougis P.E."/>
        <person name="Martin-Eauclaire M.-F."/>
      </authorList>
      <dbReference type="PubMed" id="16533515"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2006.01.005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Muscle</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2008" name="Toxicon" volume="51" first="835" last="852">
      <title>New analysis of the toxic compounds from the Androctonus mauretanicus mauretanicus scorpion venom.</title>
      <authorList>
        <person name="Oukkache N."/>
        <person name="Rosso J.-P."/>
        <person name="Alami M."/>
        <person name="Ghalim N."/>
        <person name="Saile R."/>
        <person name="Hassar M."/>
        <person name="Bougis P.E."/>
        <person name="Martin-Eauclaire M.-F."/>
      </authorList>
      <dbReference type="PubMed" id="18243273"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2007.12.012"/>
    </citation>
    <scope>PARTIAL PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2010" name="Neurosci. Lett." volume="482" first="45" last="50">
      <title>Involvement of endogenous opioid system in scorpion toxin-induced antinociception in mice.</title>
      <authorList>
        <person name="Martin-Eauclaire M.F."/>
        <person name="Abbas N."/>
        <person name="Sauze N."/>
        <person name="Mercier L."/>
        <person name="Berge-Lefranc J.L."/>
        <person name="Condo J."/>
        <person name="Bougis P.E."/>
        <person name="Guieu R."/>
      </authorList>
      <dbReference type="PubMed" id="20619318"/>
      <dbReference type="DOI" id="10.1016/j.neulet.2010.06.090"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2013" name="Pain" volume="154" first="1204" last="1215">
      <title>The scorpion toxin Amm VIII induces pain hypersensitivity through gain-of-function of TTX-sensitive Na[+] channels.</title>
      <authorList>
        <person name="Abbas N."/>
        <person name="Gaudioso-Tyzra C."/>
        <person name="Bonnet C."/>
        <person name="Gabriac M."/>
        <person name="Amsalem M."/>
        <person name="Lonigro A."/>
        <person name="Padilla F."/>
        <person name="Crest M."/>
        <person name="Martin-Eauclaire M.F."/>
        <person name="Delmas P."/>
      </authorList>
      <dbReference type="PubMed" id="23685008"/>
      <dbReference type="DOI" id="10.1016/j.pain.2013.03.037"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 4 5">Alpha toxins bind voltage-independently at site-3 of sodium channels (Nav) and inhibit the inactivation of the activated channels, thereby blocking neuronal transmission (PubMed:12911331). The toxin principally slows the inactivation process of TTX-sensitive sodium channels (PubMed:23685008). It discriminates neuronal versus muscular sodium channel, as it is more potent on rat brain Nav1.2/SCN2A (EC(50)=29 nM) than on rat skeletal muscle Nav1.4/SCN4A (EC(50)=416 nM) (PubMed:12911331). It also shows a weak activity on Nav1.7/SCN9A (EC(50)=1.76 uM) (PubMed:23685008). In vivo, the toxin produces pain hypersensibility to mechanical and thermal stimuli (PubMed:23685008). It also exhibits potent analgesic activity (when injected intraperitoneally), increasing hot plate and tail flick withdrawal latencies in a dose-dependent fashion (PubMed:20619318). This paradoxical analgesic action, is significantly suppressed by opioid receptor antagonists, suggesting a pain-induced analgesia mechanism that involves an endogenous opioid system (PubMed:20619318). This led to hypothesis that pain relief induced by peripheral administration of Amm VIII may result from sensitization of primary afferent neurons and subsequent activation of an opioid-dependent noxious inhibitory control (PubMed:20619318).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="11">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="10">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="mass spectrometry" mass="7382.57" method="Electrospray" evidence="2"/>
  <comment type="mass spectrometry" mass="7382.0" method="MALDI" evidence="3"/>
  <comment type="toxic dose">
    <text evidence="2">LD(50) is 11.25 ug/kg by intracerebroventricular injection into mice but is not toxic (&gt;1 mg/mouse) when injected subcutaneously.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="5">Negative results: does not act on Nav1.8/SCN10A and Nav1.9/SCN11A when tested on TTX-resistant sodium channels.</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Alpha subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AJ496808">
    <property type="protein sequence ID" value="CAD43222.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AM159181">
    <property type="protein sequence ID" value="CAJ43744.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q7YXD3"/>
  <dbReference type="SMR" id="Q7YXD3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000035232" description="Alpha-toxin Amm8" evidence="2">
    <location>
      <begin position="20"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000035233" description="Removed by a carboxypeptidase">
    <location>
      <position position="85"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="21"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="35"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="41"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="45"/>
      <end position="67"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12911331"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18243273"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="20619318"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="23685008"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="12911331"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="16533515"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="18243273"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="20619318"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="12911331"/>
    </source>
  </evidence>
  <sequence length="85" mass="9654" checksum="E8DC99F998D7DC8C" modified="2003-10-01" version="1" precursor="true">MNYLVMISLALLFMTGVESLKDGYIVNDINCTYFCGRNAYCNELCIKLKGESGYCQWASPYGNSCYCYKLPDHVRTKGPGRCNDR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>