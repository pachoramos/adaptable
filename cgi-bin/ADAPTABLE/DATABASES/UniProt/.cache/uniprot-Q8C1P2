<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-09-11" modified="2024-11-27" version="128" xmlns="http://uniprot.org/uniprot">
  <accession>Q8C1P2</accession>
  <name>DFA21_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Alpha-defensin 21</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin-related cryptdin-21</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Defa21</name>
    <name type="synonym">Defcr21</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Physiol. Genomics" volume="20" first="1" last="11">
      <title>Rapid evolution and diversification of mammalian alpha-defensins as revealed by comparative analysis of rodent and primate genes.</title>
      <authorList>
        <person name="Patil A."/>
        <person name="Hughes A.L."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="15494476"/>
      <dbReference type="DOI" id="10.1152/physiolgenomics.00150.2004"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Science" volume="309" first="1559" last="1563">
      <title>The transcriptional landscape of the mammalian genome.</title>
      <authorList>
        <person name="Carninci P."/>
        <person name="Kasukawa T."/>
        <person name="Katayama S."/>
        <person name="Gough J."/>
        <person name="Frith M.C."/>
        <person name="Maeda N."/>
        <person name="Oyama R."/>
        <person name="Ravasi T."/>
        <person name="Lenhard B."/>
        <person name="Wells C."/>
        <person name="Kodzius R."/>
        <person name="Shimokawa K."/>
        <person name="Bajic V.B."/>
        <person name="Brenner S.E."/>
        <person name="Batalov S."/>
        <person name="Forrest A.R."/>
        <person name="Zavolan M."/>
        <person name="Davis M.J."/>
        <person name="Wilming L.G."/>
        <person name="Aidinis V."/>
        <person name="Allen J.E."/>
        <person name="Ambesi-Impiombato A."/>
        <person name="Apweiler R."/>
        <person name="Aturaliya R.N."/>
        <person name="Bailey T.L."/>
        <person name="Bansal M."/>
        <person name="Baxter L."/>
        <person name="Beisel K.W."/>
        <person name="Bersano T."/>
        <person name="Bono H."/>
        <person name="Chalk A.M."/>
        <person name="Chiu K.P."/>
        <person name="Choudhary V."/>
        <person name="Christoffels A."/>
        <person name="Clutterbuck D.R."/>
        <person name="Crowe M.L."/>
        <person name="Dalla E."/>
        <person name="Dalrymple B.P."/>
        <person name="de Bono B."/>
        <person name="Della Gatta G."/>
        <person name="di Bernardo D."/>
        <person name="Down T."/>
        <person name="Engstrom P."/>
        <person name="Fagiolini M."/>
        <person name="Faulkner G."/>
        <person name="Fletcher C.F."/>
        <person name="Fukushima T."/>
        <person name="Furuno M."/>
        <person name="Futaki S."/>
        <person name="Gariboldi M."/>
        <person name="Georgii-Hemming P."/>
        <person name="Gingeras T.R."/>
        <person name="Gojobori T."/>
        <person name="Green R.E."/>
        <person name="Gustincich S."/>
        <person name="Harbers M."/>
        <person name="Hayashi Y."/>
        <person name="Hensch T.K."/>
        <person name="Hirokawa N."/>
        <person name="Hill D."/>
        <person name="Huminiecki L."/>
        <person name="Iacono M."/>
        <person name="Ikeo K."/>
        <person name="Iwama A."/>
        <person name="Ishikawa T."/>
        <person name="Jakt M."/>
        <person name="Kanapin A."/>
        <person name="Katoh M."/>
        <person name="Kawasawa Y."/>
        <person name="Kelso J."/>
        <person name="Kitamura H."/>
        <person name="Kitano H."/>
        <person name="Kollias G."/>
        <person name="Krishnan S.P."/>
        <person name="Kruger A."/>
        <person name="Kummerfeld S.K."/>
        <person name="Kurochkin I.V."/>
        <person name="Lareau L.F."/>
        <person name="Lazarevic D."/>
        <person name="Lipovich L."/>
        <person name="Liu J."/>
        <person name="Liuni S."/>
        <person name="McWilliam S."/>
        <person name="Madan Babu M."/>
        <person name="Madera M."/>
        <person name="Marchionni L."/>
        <person name="Matsuda H."/>
        <person name="Matsuzawa S."/>
        <person name="Miki H."/>
        <person name="Mignone F."/>
        <person name="Miyake S."/>
        <person name="Morris K."/>
        <person name="Mottagui-Tabar S."/>
        <person name="Mulder N."/>
        <person name="Nakano N."/>
        <person name="Nakauchi H."/>
        <person name="Ng P."/>
        <person name="Nilsson R."/>
        <person name="Nishiguchi S."/>
        <person name="Nishikawa S."/>
        <person name="Nori F."/>
        <person name="Ohara O."/>
        <person name="Okazaki Y."/>
        <person name="Orlando V."/>
        <person name="Pang K.C."/>
        <person name="Pavan W.J."/>
        <person name="Pavesi G."/>
        <person name="Pesole G."/>
        <person name="Petrovsky N."/>
        <person name="Piazza S."/>
        <person name="Reed J."/>
        <person name="Reid J.F."/>
        <person name="Ring B.Z."/>
        <person name="Ringwald M."/>
        <person name="Rost B."/>
        <person name="Ruan Y."/>
        <person name="Salzberg S.L."/>
        <person name="Sandelin A."/>
        <person name="Schneider C."/>
        <person name="Schoenbach C."/>
        <person name="Sekiguchi K."/>
        <person name="Semple C.A."/>
        <person name="Seno S."/>
        <person name="Sessa L."/>
        <person name="Sheng Y."/>
        <person name="Shibata Y."/>
        <person name="Shimada H."/>
        <person name="Shimada K."/>
        <person name="Silva D."/>
        <person name="Sinclair B."/>
        <person name="Sperling S."/>
        <person name="Stupka E."/>
        <person name="Sugiura K."/>
        <person name="Sultana R."/>
        <person name="Takenaka Y."/>
        <person name="Taki K."/>
        <person name="Tammoja K."/>
        <person name="Tan S.L."/>
        <person name="Tang S."/>
        <person name="Taylor M.S."/>
        <person name="Tegner J."/>
        <person name="Teichmann S.A."/>
        <person name="Ueda H.R."/>
        <person name="van Nimwegen E."/>
        <person name="Verardo R."/>
        <person name="Wei C.L."/>
        <person name="Yagi K."/>
        <person name="Yamanishi H."/>
        <person name="Zabarovsky E."/>
        <person name="Zhu S."/>
        <person name="Zimmer A."/>
        <person name="Hide W."/>
        <person name="Bult C."/>
        <person name="Grimmond S.M."/>
        <person name="Teasdale R.D."/>
        <person name="Liu E.T."/>
        <person name="Brusic V."/>
        <person name="Quackenbush J."/>
        <person name="Wahlestedt C."/>
        <person name="Mattick J.S."/>
        <person name="Hume D.A."/>
        <person name="Kai C."/>
        <person name="Sasaki D."/>
        <person name="Tomaru Y."/>
        <person name="Fukuda S."/>
        <person name="Kanamori-Katayama M."/>
        <person name="Suzuki M."/>
        <person name="Aoki J."/>
        <person name="Arakawa T."/>
        <person name="Iida J."/>
        <person name="Imamura K."/>
        <person name="Itoh M."/>
        <person name="Kato T."/>
        <person name="Kawaji H."/>
        <person name="Kawagashira N."/>
        <person name="Kawashima T."/>
        <person name="Kojima M."/>
        <person name="Kondo S."/>
        <person name="Konno H."/>
        <person name="Nakano K."/>
        <person name="Ninomiya N."/>
        <person name="Nishio T."/>
        <person name="Okada M."/>
        <person name="Plessy C."/>
        <person name="Shibata K."/>
        <person name="Shiraki T."/>
        <person name="Suzuki S."/>
        <person name="Tagami M."/>
        <person name="Waki K."/>
        <person name="Watahiki A."/>
        <person name="Okamura-Oho Y."/>
        <person name="Suzuki H."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
      </authorList>
      <dbReference type="PubMed" id="16141072"/>
      <dbReference type="DOI" id="10.1126/science.1112014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
      <tissue>Small intestine</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">May have microbicidal activities.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the alpha-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY746426">
    <property type="protein sequence ID" value="AAW78335.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK008266">
    <property type="protein sequence ID" value="BAC25211.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS40264.1"/>
  <dbReference type="RefSeq" id="NP_899076.1">
    <property type="nucleotide sequence ID" value="NM_183253.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8C1P2"/>
  <dbReference type="SMR" id="Q8C1P2"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000076041"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000076041"/>
  <dbReference type="PeptideAtlas" id="Q8C1P2"/>
  <dbReference type="DNASU" id="66298"/>
  <dbReference type="Ensembl" id="ENSMUST00000076754.3">
    <property type="protein sequence ID" value="ENSMUSP00000076041.3"/>
    <property type="gene ID" value="ENSMUSG00000074447.3"/>
  </dbReference>
  <dbReference type="GeneID" id="66298"/>
  <dbReference type="KEGG" id="mmu:66298"/>
  <dbReference type="UCSC" id="uc009lbj.2">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:1913548"/>
  <dbReference type="CTD" id="66298"/>
  <dbReference type="MGI" id="MGI:1913548">
    <property type="gene designation" value="Defa21"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000074447"/>
  <dbReference type="GeneTree" id="ENSGT00940000153268"/>
  <dbReference type="HOGENOM" id="CLU_160803_0_0_1"/>
  <dbReference type="InParanoid" id="Q8C1P2"/>
  <dbReference type="OrthoDB" id="4845046at2759"/>
  <dbReference type="PhylomeDB" id="Q8C1P2"/>
  <dbReference type="TreeFam" id="TF338414"/>
  <dbReference type="Reactome" id="R-MMU-1461973">
    <property type="pathway name" value="Defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-1462054">
    <property type="pathway name" value="Alpha-defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-6798695">
    <property type="pathway name" value="Neutrophil degranulation"/>
  </dbReference>
  <dbReference type="BioGRID-ORCS" id="66298">
    <property type="hits" value="5 hits in 43 CRISPR screens"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q8C1P2"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 8"/>
  </dbReference>
  <dbReference type="RNAct" id="Q8C1P2">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000074447">
    <property type="expression patterns" value="Expressed in ileum and 17 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016327">
    <property type="entry name" value="Alpha-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002366">
    <property type="entry name" value="Alpha-defensin_N"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876">
    <property type="entry name" value="ALPHA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876:SF2">
    <property type="entry name" value="ALPHA-DEFENSIN 1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00879">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001875">
    <property type="entry name" value="Alpha-defensin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01418">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000300068" evidence="1">
    <location>
      <begin position="20"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000300069" description="Alpha-defensin 21">
    <location>
      <begin position="59"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="22"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="64"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="66"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="71"/>
      <end position="88"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="93" mass="10507" checksum="5CCF0BD8ED2F3057" modified="2003-03-01" version="1" precursor="true">MKTLVLLSALILLAYQVQTDPIQNTDEETNTEEQPGEDDQAVSVSFGGQEGSALHEKLSRDLICLCRNRRCNRGELFYGTCAGPFLRCCRRRR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>