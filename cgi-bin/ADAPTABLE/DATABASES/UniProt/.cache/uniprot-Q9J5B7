<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-08-29" modified="2024-07-24" version="65" xmlns="http://uniprot.org/uniprot">
  <accession>Q9J5B7</accession>
  <name>PG062_FOWPN</name>
  <protein>
    <recommendedName>
      <fullName>Phosphoprotein OPG062</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Phosphoprotein F17</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">OPG062</name>
    <name type="ordered locus">FPV103</name>
  </gene>
  <organism>
    <name type="scientific">Fowlpox virus (strain NVSL)</name>
    <name type="common">FPV</name>
    <dbReference type="NCBI Taxonomy" id="928301"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Varidnaviria</taxon>
      <taxon>Bamfordvirae</taxon>
      <taxon>Nucleocytoviricota</taxon>
      <taxon>Pokkesviricetes</taxon>
      <taxon>Chitovirales</taxon>
      <taxon>Poxviridae</taxon>
      <taxon>Chordopoxvirinae</taxon>
      <taxon>Avipoxvirus</taxon>
      <taxon>Fowlpox virus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Vertebrata</name>
    <dbReference type="NCBI Taxonomy" id="7742"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="2000" name="J. Virol." volume="74" first="3815" last="3831">
      <title>The genome of fowlpox virus.</title>
      <authorList>
        <person name="Afonso C.L."/>
        <person name="Tulman E.R."/>
        <person name="Lu Z."/>
        <person name="Zsak L."/>
        <person name="Kutish G.F."/>
        <person name="Rock D.L."/>
      </authorList>
      <dbReference type="PubMed" id="10729156"/>
      <dbReference type="DOI" id="10.1128/jvi.74.8.3815-3831.2000"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays an essential role in virion assembly and morphogenesis. Also plays a role in the inhibition of host immune response by dysregulating mTOR. Sequesters host RICTOR and RPTOR, thereby disrupting mTORC1 and mTORC2 crosstalk. In turn, blocks the host antiviral response in part through mTOR-dependent degradation of cGAS, the primary poxvirus sensor.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Self-associates to form high molecular-weight forms. Interacts with protein OPG157. Interacts with host RICTOR and RPTOR; these interactions disrupt the mTORC1 and mTORC2 crosstalk.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Virion</location>
    </subcellularLocation>
    <text evidence="1">Major component of the virion comprising about 10% of the virion mass.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Phosphorylated on two serines.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the orthopoxvirus OPG062 family.</text>
  </comment>
  <dbReference type="EMBL" id="AF198100">
    <property type="protein sequence ID" value="AAF44447.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_039066.1">
    <property type="nucleotide sequence ID" value="NC_002188.1"/>
  </dbReference>
  <dbReference type="GeneID" id="1486651"/>
  <dbReference type="KEGG" id="vg:1486651"/>
  <dbReference type="Proteomes" id="UP000008597">
    <property type="component" value="Segment"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044423">
    <property type="term" value="C:virion component"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019082">
    <property type="term" value="P:viral protein processing"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006854">
    <property type="entry name" value="Phosphoprotein_F17"/>
  </dbReference>
  <dbReference type="Pfam" id="PF04767">
    <property type="entry name" value="Pox_F17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF003688">
    <property type="entry name" value="VAC_PP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-0426">Late protein</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <keyword id="KW-0946">Virion</keyword>
  <feature type="chain" id="PRO_0000099523" description="Phosphoprotein OPG062">
    <location>
      <begin position="1"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="58"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Polar residues" evidence="2">
    <location>
      <begin position="60"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="1">
    <location>
      <position position="60"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="1">
    <location>
      <position position="69"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P07396"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="114" mass="12742" checksum="FEC99BAC5487B8EE" modified="2000-10-01" version="1">MEESAGTPFHDPKYFVSPFILNTDEGRYLVLKAIKLCNVRTIDCMKQETSCVLKVDKPQSPCPISSSDSAPKCYMQQTSPQQQRQAPQLRFINTGALSNLFANTTDKAARVINQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>