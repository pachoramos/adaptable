<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-05-23" modified="2024-05-29" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKT3</accession>
  <name>DIUX_AGRIP</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Diuretic hormone class 2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">DH(31)</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Diuretic peptide</fullName>
      <shortName evidence="1">DP</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Agrotis ipsilon</name>
    <name type="common">Black cutworm moth</name>
    <dbReference type="NCBI Taxonomy" id="56364"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Noctuoidea</taxon>
      <taxon>Noctuidae</taxon>
      <taxon>Noctuinae</taxon>
      <taxon>Noctuini</taxon>
      <taxon>Agrotis</taxon>
    </lineage>
  </organism>
  <reference evidence="6" key="1">
    <citation type="journal article" date="2018" name="J. Proteome Res." volume="17" first="1397" last="1414">
      <title>Mating-induced differential peptidomics of neuropeptides and protein hormones in Agrotis ipsilon moths.</title>
      <authorList>
        <person name="Diesner M."/>
        <person name="Gallot A."/>
        <person name="Binz H."/>
        <person name="Gaertner C."/>
        <person name="Vitecek S."/>
        <person name="Kahnt J."/>
        <person name="Schachtner J."/>
        <person name="Jacquin-Joly E."/>
        <person name="Gadenne C."/>
      </authorList>
      <dbReference type="PubMed" id="29466015"/>
      <dbReference type="DOI" id="10.1021/acs.jproteome.7b00779"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 74-104</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT PRO-104</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Regulation of fluid secretion. Stimulates Malpighian tubule fluid secretion.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed in corpora cardiaca (CC), corpora allata (CA), antennal lobe (AL) and gnathal ganglion (GNG) (at protein level). Expression in CC, CA and AL detected in most animals, expression in GNG in few animals (at protein level).</text>
  </comment>
  <comment type="mass spectrometry" mass="3042.65" method="MALDI" evidence="4"/>
  <comment type="similarity">
    <text evidence="6">Belongs to the diuretic hormone class 2 family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HKT3"/>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008613">
    <property type="term" value="F:diuretic hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001664">
    <property type="term" value="F:G protein-coupled receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007589">
    <property type="term" value="P:body fluid secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR034439">
    <property type="entry name" value="DH2-like"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR41146">
    <property type="entry name" value="DIURETIC HORMONE CLASS 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR41146:SF1">
    <property type="entry name" value="DIURETIC HORMONE CLASS 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444226" evidence="6">
    <location>
      <begin position="25"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000444227" description="Diuretic hormone class 2" evidence="4">
    <location>
      <begin position="74"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444228" evidence="6">
    <location>
      <begin position="108"/>
      <end position="112"/>
    </location>
  </feature>
  <feature type="modified residue" description="Proline amide" evidence="4">
    <location>
      <position position="104"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P82372"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P85826"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="112" mass="12626" checksum="C69BB6E9FF56D6F3" modified="2018-05-23" version="1" precursor="true">MVRATCLLASCVLFALLLIVPASAYPRYPSNYFREEGQYEPEEIMDMLNRLGNLIQMERKMENYKEDITSEKRALDLGLSRGYSGALQAKHLMGLAAANYAGGPGRRRRDAH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>