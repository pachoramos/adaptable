<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-13" modified="2024-10-02" version="97" xmlns="http://uniprot.org/uniprot">
  <accession>P82769</accession>
  <name>DF121_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Putative defensin-like protein 121</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Putative low-molecular-weight cysteine-rich protein 55</fullName>
      <shortName>Protein LCR55</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">LCR55</name>
    <name type="ordered locus">At3g20997</name>
    <name type="ORF">MSA6</name>
  </gene>
  <organism evidence="3">
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2000" name="DNA Res." volume="7" first="217" last="221">
      <title>Structural analysis of Arabidopsis thaliana chromosome 3. II. Sequence features of the 4,251,695 bp regions covered by 90 P1, TAC and BAC clones.</title>
      <authorList>
        <person name="Kaneko T."/>
        <person name="Katoh T."/>
        <person name="Sato S."/>
        <person name="Nakamura Y."/>
        <person name="Asamizu E."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="10907853"/>
      <dbReference type="DOI" id="10.1093/dnares/7.3.217"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference evidence="3" key="3">
    <citation type="journal article" date="2001" name="Plant Mol. Biol." volume="46" first="17" last="34">
      <title>Two large Arabidopsis thaliana gene families are homologous to the Brassica gene superfamily that encodes pollen coat proteins and the male component of the self-incompatibility response.</title>
      <authorList>
        <person name="Vanoosthuyse V."/>
        <person name="Miege C."/>
        <person name="Dumas C."/>
        <person name="Cock J.M."/>
      </authorList>
      <dbReference type="PubMed" id="11437247"/>
      <dbReference type="DOI" id="10.1023/a:1010664704926"/>
    </citation>
    <scope>IDENTIFICATION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="EMBL" id="AP000604">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002686">
    <property type="protein sequence ID" value="AEE76452.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001030737.1">
    <property type="nucleotide sequence ID" value="NM_001035660.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P82769"/>
  <dbReference type="SMR" id="P82769"/>
  <dbReference type="STRING" id="3702.P82769"/>
  <dbReference type="PaxDb" id="3702-AT3G20997.1"/>
  <dbReference type="ProteomicsDB" id="224640"/>
  <dbReference type="EnsemblPlants" id="AT3G20997.1">
    <property type="protein sequence ID" value="AT3G20997.1"/>
    <property type="gene ID" value="AT3G20997"/>
  </dbReference>
  <dbReference type="GeneID" id="3768794"/>
  <dbReference type="Gramene" id="AT3G20997.1">
    <property type="protein sequence ID" value="AT3G20997.1"/>
    <property type="gene ID" value="AT3G20997"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT3G20997"/>
  <dbReference type="Araport" id="AT3G20997"/>
  <dbReference type="TAIR" id="AT3G20997">
    <property type="gene designation" value="LCR55"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_182511_2_0_1"/>
  <dbReference type="InParanoid" id="P82769"/>
  <dbReference type="OMA" id="QQKWKGS"/>
  <dbReference type="OrthoDB" id="638477at2759"/>
  <dbReference type="PhylomeDB" id="P82769"/>
  <dbReference type="PRO" id="PR:P82769"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P82769">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010851">
    <property type="entry name" value="DEFL"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33830:SF10">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 118-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33830">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 184-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07333">
    <property type="entry name" value="SLR1-BP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000017293" description="Putative defensin-like protein 121">
    <location>
      <begin position="27"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="30"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="39"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="48"/>
      <end position="70"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="76" mass="8519" checksum="B3D8D1D7654F5583" modified="2002-10-01" version="1" precursor="true">MTYKATILAIFMIILVLGIGTKETRGQETCHDLIMKRDCDEATCVNMCQQKWKGSGGSCFQNFNVMSCICNFPCQV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>