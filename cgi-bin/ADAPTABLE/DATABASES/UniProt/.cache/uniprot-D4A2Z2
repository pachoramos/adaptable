<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-10-31" modified="2024-11-27" version="82" xmlns="http://uniprot.org/uniprot">
  <accession>D4A2Z2</accession>
  <name>EPPI_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Eppin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Epididymal protease inhibitor</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Serine protease inhibitor-like with Kunitz and WAP domains 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Eppin</name>
    <name type="synonym">Spinlw1</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Nature" volume="428" first="493" last="521">
      <title>Genome sequence of the Brown Norway rat yields insights into mammalian evolution.</title>
      <authorList>
        <person name="Gibbs R.A."/>
        <person name="Weinstock G.M."/>
        <person name="Metzker M.L."/>
        <person name="Muzny D.M."/>
        <person name="Sodergren E.J."/>
        <person name="Scherer S."/>
        <person name="Scott G."/>
        <person name="Steffen D."/>
        <person name="Worley K.C."/>
        <person name="Burch P.E."/>
        <person name="Okwuonu G."/>
        <person name="Hines S."/>
        <person name="Lewis L."/>
        <person name="Deramo C."/>
        <person name="Delgado O."/>
        <person name="Dugan-Rocha S."/>
        <person name="Miner G."/>
        <person name="Morgan M."/>
        <person name="Hawes A."/>
        <person name="Gill R."/>
        <person name="Holt R.A."/>
        <person name="Adams M.D."/>
        <person name="Amanatides P.G."/>
        <person name="Baden-Tillson H."/>
        <person name="Barnstead M."/>
        <person name="Chin S."/>
        <person name="Evans C.A."/>
        <person name="Ferriera S."/>
        <person name="Fosler C."/>
        <person name="Glodek A."/>
        <person name="Gu Z."/>
        <person name="Jennings D."/>
        <person name="Kraft C.L."/>
        <person name="Nguyen T."/>
        <person name="Pfannkoch C.M."/>
        <person name="Sitter C."/>
        <person name="Sutton G.G."/>
        <person name="Venter J.C."/>
        <person name="Woodage T."/>
        <person name="Smith D."/>
        <person name="Lee H.-M."/>
        <person name="Gustafson E."/>
        <person name="Cahill P."/>
        <person name="Kana A."/>
        <person name="Doucette-Stamm L."/>
        <person name="Weinstock K."/>
        <person name="Fechtel K."/>
        <person name="Weiss R.B."/>
        <person name="Dunn D.M."/>
        <person name="Green E.D."/>
        <person name="Blakesley R.W."/>
        <person name="Bouffard G.G."/>
        <person name="De Jong P.J."/>
        <person name="Osoegawa K."/>
        <person name="Zhu B."/>
        <person name="Marra M."/>
        <person name="Schein J."/>
        <person name="Bosdet I."/>
        <person name="Fjell C."/>
        <person name="Jones S."/>
        <person name="Krzywinski M."/>
        <person name="Mathewson C."/>
        <person name="Siddiqui A."/>
        <person name="Wye N."/>
        <person name="McPherson J."/>
        <person name="Zhao S."/>
        <person name="Fraser C.M."/>
        <person name="Shetty J."/>
        <person name="Shatsman S."/>
        <person name="Geer K."/>
        <person name="Chen Y."/>
        <person name="Abramzon S."/>
        <person name="Nierman W.C."/>
        <person name="Havlak P.H."/>
        <person name="Chen R."/>
        <person name="Durbin K.J."/>
        <person name="Egan A."/>
        <person name="Ren Y."/>
        <person name="Song X.-Z."/>
        <person name="Li B."/>
        <person name="Liu Y."/>
        <person name="Qin X."/>
        <person name="Cawley S."/>
        <person name="Cooney A.J."/>
        <person name="D'Souza L.M."/>
        <person name="Martin K."/>
        <person name="Wu J.Q."/>
        <person name="Gonzalez-Garay M.L."/>
        <person name="Jackson A.R."/>
        <person name="Kalafus K.J."/>
        <person name="McLeod M.P."/>
        <person name="Milosavljevic A."/>
        <person name="Virk D."/>
        <person name="Volkov A."/>
        <person name="Wheeler D.A."/>
        <person name="Zhang Z."/>
        <person name="Bailey J.A."/>
        <person name="Eichler E.E."/>
        <person name="Tuzun E."/>
        <person name="Birney E."/>
        <person name="Mongin E."/>
        <person name="Ureta-Vidal A."/>
        <person name="Woodwark C."/>
        <person name="Zdobnov E."/>
        <person name="Bork P."/>
        <person name="Suyama M."/>
        <person name="Torrents D."/>
        <person name="Alexandersson M."/>
        <person name="Trask B.J."/>
        <person name="Young J.M."/>
        <person name="Huang H."/>
        <person name="Wang H."/>
        <person name="Xing H."/>
        <person name="Daniels S."/>
        <person name="Gietzen D."/>
        <person name="Schmidt J."/>
        <person name="Stevens K."/>
        <person name="Vitt U."/>
        <person name="Wingrove J."/>
        <person name="Camara F."/>
        <person name="Mar Alba M."/>
        <person name="Abril J.F."/>
        <person name="Guigo R."/>
        <person name="Smit A."/>
        <person name="Dubchak I."/>
        <person name="Rubin E.M."/>
        <person name="Couronne O."/>
        <person name="Poliakov A."/>
        <person name="Huebner N."/>
        <person name="Ganten D."/>
        <person name="Goesele C."/>
        <person name="Hummel O."/>
        <person name="Kreitler T."/>
        <person name="Lee Y.-A."/>
        <person name="Monti J."/>
        <person name="Schulz H."/>
        <person name="Zimdahl H."/>
        <person name="Himmelbauer H."/>
        <person name="Lehrach H."/>
        <person name="Jacob H.J."/>
        <person name="Bromberg S."/>
        <person name="Gullings-Handley J."/>
        <person name="Jensen-Seaman M.I."/>
        <person name="Kwitek A.E."/>
        <person name="Lazar J."/>
        <person name="Pasko D."/>
        <person name="Tonellato P.J."/>
        <person name="Twigger S."/>
        <person name="Ponting C.P."/>
        <person name="Duarte J.M."/>
        <person name="Rice S."/>
        <person name="Goodstadt L."/>
        <person name="Beatson S.A."/>
        <person name="Emes R.D."/>
        <person name="Winter E.E."/>
        <person name="Webber C."/>
        <person name="Brandt P."/>
        <person name="Nyakatura G."/>
        <person name="Adetobi M."/>
        <person name="Chiaromonte F."/>
        <person name="Elnitski L."/>
        <person name="Eswara P."/>
        <person name="Hardison R.C."/>
        <person name="Hou M."/>
        <person name="Kolbe D."/>
        <person name="Makova K."/>
        <person name="Miller W."/>
        <person name="Nekrutenko A."/>
        <person name="Riemer C."/>
        <person name="Schwartz S."/>
        <person name="Taylor J."/>
        <person name="Yang S."/>
        <person name="Zhang Y."/>
        <person name="Lindpaintner K."/>
        <person name="Andrews T.D."/>
        <person name="Caccamo M."/>
        <person name="Clamp M."/>
        <person name="Clarke L."/>
        <person name="Curwen V."/>
        <person name="Durbin R.M."/>
        <person name="Eyras E."/>
        <person name="Searle S.M."/>
        <person name="Cooper G.M."/>
        <person name="Batzoglou S."/>
        <person name="Brudno M."/>
        <person name="Sidow A."/>
        <person name="Stone E.A."/>
        <person name="Payseur B.A."/>
        <person name="Bourque G."/>
        <person name="Lopez-Otin C."/>
        <person name="Puente X.S."/>
        <person name="Chakrabarti K."/>
        <person name="Chatterji S."/>
        <person name="Dewey C."/>
        <person name="Pachter L."/>
        <person name="Bray N."/>
        <person name="Yap V.B."/>
        <person name="Caspi A."/>
        <person name="Tesler G."/>
        <person name="Pevzner P.A."/>
        <person name="Haussler D."/>
        <person name="Roskin K.M."/>
        <person name="Baertsch R."/>
        <person name="Clawson H."/>
        <person name="Furey T.S."/>
        <person name="Hinrichs A.S."/>
        <person name="Karolchik D."/>
        <person name="Kent W.J."/>
        <person name="Rosenbloom K.R."/>
        <person name="Trumbower H."/>
        <person name="Weirauch M."/>
        <person name="Cooper D.N."/>
        <person name="Stenson P.D."/>
        <person name="Ma B."/>
        <person name="Brent M."/>
        <person name="Arumugam M."/>
        <person name="Shteynberg D."/>
        <person name="Copley R.R."/>
        <person name="Taylor M.S."/>
        <person name="Riethman H."/>
        <person name="Mudunuri U."/>
        <person name="Peterson J."/>
        <person name="Guyer M."/>
        <person name="Felsenfeld A."/>
        <person name="Old S."/>
        <person name="Mockrin S."/>
        <person name="Collins F.S."/>
      </authorList>
      <dbReference type="PubMed" id="15057822"/>
      <dbReference type="DOI" id="10.1038/nature02426"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Brown Norway</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="submission" date="2005-09" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Adams M.D."/>
        <person name="Myers E.W."/>
        <person name="Smith H.O."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Brown Norway</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Serine protease inhibitor that plays an essential role in male reproduction and fertility. Modulates the hydrolysis of SEMG1 by KLK3/PSA (a serine protease), provides antimicrobial protection for spermatozoa in the ejaculate coagulum, and binds SEMG1 thereby inhibiting sperm motility (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer. Homodimer. Homomultimers. Interacts with SEMG1 (via 164-283 AA). Interacts with LTF. Found in a complex with LTF, CLU, EPPIN and SEMG1.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EMBL" id="AABR06027547">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH474005">
    <property type="protein sequence ID" value="EDL96509.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001102927.1">
    <property type="nucleotide sequence ID" value="NM_001109457.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="D4A2Z2"/>
  <dbReference type="SMR" id="D4A2Z2"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000019829"/>
  <dbReference type="MEROPS" id="I02.956"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000019829"/>
  <dbReference type="Ensembl" id="ENSRNOT00000019829.7">
    <property type="protein sequence ID" value="ENSRNOP00000019829.5"/>
    <property type="gene ID" value="ENSRNOG00000028908.5"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055007485">
    <property type="protein sequence ID" value="ENSRNOP00055005612"/>
    <property type="gene ID" value="ENSRNOG00055004711"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060001736">
    <property type="protein sequence ID" value="ENSRNOP00060000951"/>
    <property type="gene ID" value="ENSRNOG00060001222"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065022072">
    <property type="protein sequence ID" value="ENSRNOP00065017110"/>
    <property type="gene ID" value="ENSRNOG00065013417"/>
  </dbReference>
  <dbReference type="GeneID" id="685161"/>
  <dbReference type="KEGG" id="rno:685161"/>
  <dbReference type="UCSC" id="RGD:1597722">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:1597722"/>
  <dbReference type="CTD" id="57119"/>
  <dbReference type="RGD" id="1597722">
    <property type="gene designation" value="Eppin"/>
  </dbReference>
  <dbReference type="eggNOG" id="KOG4295">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000156753"/>
  <dbReference type="HOGENOM" id="CLU_127181_0_0_1"/>
  <dbReference type="InParanoid" id="D4A2Z2"/>
  <dbReference type="OMA" id="CCVFNCG"/>
  <dbReference type="OrthoDB" id="3558236at2759"/>
  <dbReference type="PhylomeDB" id="D4A2Z2"/>
  <dbReference type="TreeFam" id="TF342459"/>
  <dbReference type="Reactome" id="R-RNO-6803157">
    <property type="pathway name" value="Antimicrobial peptides"/>
  </dbReference>
  <dbReference type="PRO" id="PR:D4A2Z2"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000234681">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000028908">
    <property type="expression patterns" value="Expressed in testis and 7 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001669">
    <property type="term" value="C:acrosomal vesicle"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009986">
    <property type="term" value="C:cell surface"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032991">
    <property type="term" value="C:protein-containing complex"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090281">
    <property type="term" value="P:negative regulation of calcium ion import"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1901318">
    <property type="term" value="P:negative regulation of flagellated sperm motility"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd22611">
    <property type="entry name" value="Kunitz_eppin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.410.10:FF:000015">
    <property type="entry name" value="WAP four-disulfide core domain 6A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.75.10:FF:000004">
    <property type="entry name" value="WAP four-disulfide core domain 6A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.75.10">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.410.10">
    <property type="entry name" value="Pancreatic trypsin inhibitor Kunitz domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036645">
    <property type="entry name" value="Elafin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002223">
    <property type="entry name" value="Kunitz_BPTI"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036880">
    <property type="entry name" value="Kunitz_BPTI_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020901">
    <property type="entry name" value="Prtase_inh_Kunz-CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR051388">
    <property type="entry name" value="Serpin_venom_toxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008197">
    <property type="entry name" value="WAP_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46751">
    <property type="entry name" value="EPPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46751:SF2">
    <property type="entry name" value="EPPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00014">
    <property type="entry name" value="Kunitz_BPTI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00095">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00759">
    <property type="entry name" value="BASICPTASE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00131">
    <property type="entry name" value="KU"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00217">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57362">
    <property type="entry name" value="BPTI-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57256">
    <property type="entry name" value="Elafin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00280">
    <property type="entry name" value="BPTI_KUNITZ_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50279">
    <property type="entry name" value="BPTI_KUNITZ_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51390">
    <property type="entry name" value="WAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000419802" description="Eppin">
    <location>
      <begin position="22"/>
      <end position="134"/>
    </location>
  </feature>
  <feature type="domain" description="WAP" evidence="4">
    <location>
      <begin position="26"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="domain" description="BPTI/Kunitz inhibitor" evidence="3">
    <location>
      <begin position="77"/>
      <end position="127"/>
    </location>
  </feature>
  <feature type="region of interest" description="Interaction with SEMG1" evidence="1">
    <location>
      <begin position="102"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="region of interest" description="Interaction with LTF" evidence="1">
    <location>
      <begin position="117"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="33"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="40"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="48"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="54"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="77"/>
      <end position="127"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="86"/>
      <end position="110"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="102"/>
      <end position="123"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00031"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="4">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00722"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="134" mass="15684" checksum="402CDFB7B7CFA260" modified="2010-04-20" version="1" precursor="true">MKFSRFVSILVLFGLLTKVQGPSLTDFLFPRRCPRFREECEHRERDLCTRDRDCQKREKCCIFSCGKKCLNPQQDICSLPKDSGYCMAYFPRWWYNKKNGTCQLFIYGGCQGNNNNFQSQSICQNACEKKSNST</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>