<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-12-07" modified="2024-11-27" version="76" xmlns="http://uniprot.org/uniprot">
  <accession>Q95WX6</accession>
  <name>SIXB_MESMA</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Beta-insect depressant toxin BmKITb</fullName>
      <shortName evidence="4">BmK ITb</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Mesobuthus martensii</name>
    <name type="common">Manchurian scorpion</name>
    <name type="synonym">Buthus martensii</name>
    <dbReference type="NCBI Taxonomy" id="34649"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Mesobuthus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="J. Pept. Res." volume="61" first="7" last="16">
      <title>Purification of two depressant insect neurotoxins and their gene cloning from the scorpion Buthus martensi Karsch.</title>
      <authorList>
        <person name="Wang C.-G."/>
        <person name="Ling M.-H."/>
        <person name="Chi C.-W."/>
        <person name="Wang D.-C."/>
        <person name="Pelhate M."/>
      </authorList>
      <dbReference type="PubMed" id="12472844"/>
      <dbReference type="DOI" id="10.1034/j.1399-3011.2003.21020.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>PROTEIN SEQUENCE OF 22-56</scope>
    <scope>AMIDATION AT GLY-82</scope>
    <scope>ELECTROPHYSIOLOGICAL CHARACTERIZATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Depressant insect beta-toxins cause a transient contraction paralysis followed by a slow flaccid paralysis. They bind voltage-independently at site-4 of sodium channels (Nav) and shift the voltage of activation toward more negative potentials thereby affecting sodium channel activation and promoting spontaneous and repetitive firing. However, this toxin has some characteristics of excitatory toxins such as bursts of activity after the membrane has been hyperpolarized. This toxin is active only on insects.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="5">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="mass spectrometry" mass="6763.9" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="5">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AF272777">
    <property type="protein sequence ID" value="AAF77063.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q95WX6"/>
  <dbReference type="SMR" id="Q95WX6"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000035206" description="Beta-insect depressant toxin BmKITb" evidence="6">
    <location>
      <begin position="22"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="2">
    <location>
      <begin position="22"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glycine amide" evidence="3">
    <location>
      <position position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="31"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 2">
    <location>
      <begin position="35"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 2">
    <location>
      <begin position="42"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 2">
    <location>
      <begin position="46"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; fig.5." evidence="5" ref="1">
    <original>V</original>
    <variation>I</variation>
    <location>
      <position position="33"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; fig.5." evidence="5" ref="1">
    <original>A</original>
    <variation>G</variation>
    <location>
      <position position="48"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P56637"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12472844"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="12472844"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="12472844"/>
    </source>
  </evidence>
  <sequence length="85" mass="9330" checksum="80CDD3914956DE8C" modified="2001-12-01" version="1" precursor="true">MKLFLLLVISASMLIDGLVNADGYIRGSNGCKVSCLWGNEGCNKECKAFGAYYGYCWTWGLACWCQGLPDDKTWKSESNTCGGKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>