<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-12-15" modified="2024-11-27" version="150" xmlns="http://uniprot.org/uniprot">
  <accession>O55004</accession>
  <name>RNAS4_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Ribonuclease 4</fullName>
      <shortName>RNase 4</shortName>
      <ecNumber evidence="4">3.1.27.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>RL3</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Rnase4</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Biochim. Biophys. Acta" volume="1384" first="55" last="65">
      <title>Ribonucleases from rat and bovine liver: purification, specificity and structural characterization.</title>
      <authorList>
        <person name="Zhao W."/>
        <person name="Kote-Jarai Z."/>
        <person name="van Santen Y."/>
        <person name="Hofsteenge J."/>
        <person name="Beintema J.J."/>
      </authorList>
      <dbReference type="PubMed" id="9602056"/>
      <dbReference type="DOI" id="10.1016/s0167-4838(97)00213-6"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PARTIAL PROTEIN SEQUENCE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Heart</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="4">Cleaves preferentially after uridine bases. Has antimicrobial activity against uropathogenic E.coli (UPEC). Probably contributes to urinary tract sterility.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
    <text evidence="4">Detected in urine.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the pancreatic ribonuclease family.</text>
  </comment>
  <dbReference type="EC" id="3.1.27.-" evidence="4"/>
  <dbReference type="EMBL" id="AF041066">
    <property type="protein sequence ID" value="AAC23487.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC070953">
    <property type="protein sequence ID" value="AAH70953.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_064467.1">
    <property type="nucleotide sequence ID" value="NM_020082.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_006251968.1">
    <property type="nucleotide sequence ID" value="XM_006251906.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_006251969.1">
    <property type="nucleotide sequence ID" value="XM_006251907.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O55004"/>
  <dbReference type="SMR" id="O55004"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000043003"/>
  <dbReference type="jPOST" id="O55004"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000043003"/>
  <dbReference type="Ensembl" id="ENSRNOT00000041495.5">
    <property type="protein sequence ID" value="ENSRNOP00000043003.2"/>
    <property type="gene ID" value="ENSRNOG00000025625.6"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00000089631.2">
    <property type="protein sequence ID" value="ENSRNOP00000073131.2"/>
    <property type="gene ID" value="ENSRNOG00000025625.6"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055042281">
    <property type="protein sequence ID" value="ENSRNOP00055034513"/>
    <property type="gene ID" value="ENSRNOG00055024574"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055042293">
    <property type="protein sequence ID" value="ENSRNOP00055034525"/>
    <property type="gene ID" value="ENSRNOG00055024574"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055042314">
    <property type="protein sequence ID" value="ENSRNOP00055034546"/>
    <property type="gene ID" value="ENSRNOG00055024574"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055042319">
    <property type="protein sequence ID" value="ENSRNOP00055034551"/>
    <property type="gene ID" value="ENSRNOG00055024574"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060046679">
    <property type="protein sequence ID" value="ENSRNOP00060038815"/>
    <property type="gene ID" value="ENSRNOG00060026945"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060046708">
    <property type="protein sequence ID" value="ENSRNOP00060038840"/>
    <property type="gene ID" value="ENSRNOG00060026945"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060046729">
    <property type="protein sequence ID" value="ENSRNOP00060038859"/>
    <property type="gene ID" value="ENSRNOG00060026945"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060046748">
    <property type="protein sequence ID" value="ENSRNOP00060038877"/>
    <property type="gene ID" value="ENSRNOG00060026945"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065055665">
    <property type="protein sequence ID" value="ENSRNOP00065045831"/>
    <property type="gene ID" value="ENSRNOG00065032343"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065055675">
    <property type="protein sequence ID" value="ENSRNOP00065045841"/>
    <property type="gene ID" value="ENSRNOG00065032343"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065055680">
    <property type="protein sequence ID" value="ENSRNOP00065045846"/>
    <property type="gene ID" value="ENSRNOG00065032343"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065055685">
    <property type="protein sequence ID" value="ENSRNOP00065045851"/>
    <property type="gene ID" value="ENSRNOG00065032343"/>
  </dbReference>
  <dbReference type="GeneID" id="56759"/>
  <dbReference type="KEGG" id="rno:56759"/>
  <dbReference type="UCSC" id="RGD:61823">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:61823"/>
  <dbReference type="CTD" id="6038"/>
  <dbReference type="RGD" id="61823">
    <property type="gene designation" value="Rnase4"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S9Q1">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000157645"/>
  <dbReference type="HOGENOM" id="CLU_117006_3_1_1"/>
  <dbReference type="InParanoid" id="O55004"/>
  <dbReference type="OMA" id="MMHRRRM"/>
  <dbReference type="OrthoDB" id="4596772at2759"/>
  <dbReference type="PhylomeDB" id="O55004"/>
  <dbReference type="TreeFam" id="TF333393"/>
  <dbReference type="PRO" id="PR:O55004"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 15"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000025625">
    <property type="expression patterns" value="Expressed in liver and 18 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="O55004">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004519">
    <property type="term" value="F:endonuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003676">
    <property type="term" value="F:nucleic acid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004540">
    <property type="term" value="F:RNA nuclease activity"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009267">
    <property type="term" value="P:cellular response to starvation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd06265">
    <property type="entry name" value="RNase_A_canonical"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.130.10:FF:000001">
    <property type="entry name" value="Ribonuclease pancreatic"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.130.10">
    <property type="entry name" value="Ribonuclease A-like domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001427">
    <property type="entry name" value="RNaseA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036816">
    <property type="entry name" value="RNaseA-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023411">
    <property type="entry name" value="RNaseA_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023412">
    <property type="entry name" value="RNaseA_domain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437">
    <property type="entry name" value="RIBONUCLEASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437:SF53">
    <property type="entry name" value="RIBONUCLEASE 4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00074">
    <property type="entry name" value="RnaseA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00794">
    <property type="entry name" value="RIBONUCLEASE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00092">
    <property type="entry name" value="RNAse_Pc"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54076">
    <property type="entry name" value="RNase A-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00127">
    <property type="entry name" value="RNASE_PANCREATIC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0255">Endonuclease</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000030887" description="Ribonuclease 4">
    <location>
      <begin position="29"/>
      <end position="147"/>
    </location>
  </feature>
  <feature type="active site" description="Proton acceptor" evidence="5">
    <location>
      <position position="40"/>
    </location>
  </feature>
  <feature type="active site" description="Proton donor" evidence="5">
    <location>
      <position position="144"/>
    </location>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="35"/>
    </location>
    <ligand>
      <name>dUMP</name>
      <dbReference type="ChEBI" id="CHEBI:246422"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="40"/>
    </location>
    <ligand>
      <name>dUMP</name>
      <dbReference type="ChEBI" id="CHEBI:246422"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="68"/>
    </location>
    <ligand>
      <name>dUMP</name>
      <dbReference type="ChEBI" id="CHEBI:246422"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="71"/>
    </location>
    <ligand>
      <name>dUMP</name>
      <dbReference type="ChEBI" id="CHEBI:246422"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="72"/>
    </location>
    <ligand>
      <name>dUMP</name>
      <dbReference type="ChEBI" id="CHEBI:246422"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="145"/>
    </location>
    <ligand>
      <name>dUMP</name>
      <dbReference type="ChEBI" id="CHEBI:246422"/>
    </ligand>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="2">
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="53"/>
      <end position="109"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="67"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="85"/>
      <end position="135"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="92"/>
      <end position="99"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P15467"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P15468"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="4">
    <source>
      <dbReference type="UniProtKB" id="P34096"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="5">
    <source>
      <dbReference type="UniProtKB" id="Q9H1E1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="147" mass="16903" checksum="110D54CF7C691BB3" modified="1998-06-01" version="1" precursor="true">MDIQRTQSLLLLLLLTLLGLGLVQPSYGQDRMYQRFLRQHVDPEGTGGSDNYCNVMMQRRRMTSTQCKRFNTFIHEDIWNIRSICDTANIPCKNGNMNCHEGIVRVTDCRETGSSVPHNCRYRARASTRRVVIACEGTPEVPVHFDR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>