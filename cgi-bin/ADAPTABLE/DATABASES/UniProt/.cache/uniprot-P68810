<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-12-21" modified="2024-11-27" version="95" xmlns="http://uniprot.org/uniprot">
  <accession>P68810</accession>
  <accession>Q9RF08</accession>
  <name>GATC_STAAW</name>
  <protein>
    <recommendedName>
      <fullName>Aspartyl/glutamyl-tRNA(Asn/Gln) amidotransferase subunit C</fullName>
      <shortName>Asp/Glu-ADT subunit C</shortName>
      <ecNumber>6.3.5.-</ecNumber>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">gatC</name>
    <name type="ordered locus">MW1842</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Allows the formation of correctly charged Asn-tRNA(Asn) or Gln-tRNA(Gln) through the transamidation of misacylated Asp-tRNA(Asn) or Glu-tRNA(Gln) in organisms which lack either or both of asparaginyl-tRNA or glutaminyl-tRNA synthetases. The reaction takes place in the presence of glutamine and ATP through an activated phospho-Asp-tRNA(Asn) or phospho-Glu-tRNA(Gln) (By similarity).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>L-glutamyl-tRNA(Gln) + L-glutamine + ATP + H2O = L-glutaminyl-tRNA(Gln) + L-glutamate + ADP + phosphate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:17521"/>
      <dbReference type="Rhea" id="RHEA-COMP:9681"/>
      <dbReference type="Rhea" id="RHEA-COMP:9684"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:29985"/>
      <dbReference type="ChEBI" id="CHEBI:30616"/>
      <dbReference type="ChEBI" id="CHEBI:43474"/>
      <dbReference type="ChEBI" id="CHEBI:58359"/>
      <dbReference type="ChEBI" id="CHEBI:78520"/>
      <dbReference type="ChEBI" id="CHEBI:78521"/>
      <dbReference type="ChEBI" id="CHEBI:456216"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>L-aspartyl-tRNA(Asn) + L-glutamine + ATP + H2O = L-asparaginyl-tRNA(Asn) + L-glutamate + ADP + phosphate + 2 H(+)</text>
      <dbReference type="Rhea" id="RHEA:14513"/>
      <dbReference type="Rhea" id="RHEA-COMP:9674"/>
      <dbReference type="Rhea" id="RHEA-COMP:9677"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:29985"/>
      <dbReference type="ChEBI" id="CHEBI:30616"/>
      <dbReference type="ChEBI" id="CHEBI:43474"/>
      <dbReference type="ChEBI" id="CHEBI:58359"/>
      <dbReference type="ChEBI" id="CHEBI:78515"/>
      <dbReference type="ChEBI" id="CHEBI:78516"/>
      <dbReference type="ChEBI" id="CHEBI:456216"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Heterotrimer of A, B and C subunits.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the GatC family.</text>
  </comment>
  <dbReference type="EC" id="6.3.5.-"/>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB95707.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000170162.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P68810"/>
  <dbReference type="SMR" id="P68810"/>
  <dbReference type="GeneID" id="66840130"/>
  <dbReference type="KEGG" id="sam:MW1842"/>
  <dbReference type="HOGENOM" id="CLU_105899_1_2_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050566">
    <property type="term" value="F:asparaginyl-tRNA synthase (glutamine-hydrolyzing) activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="RHEA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005524">
    <property type="term" value="F:ATP binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050567">
    <property type="term" value="F:glutaminyl-tRNA synthase (glutamine-hydrolyzing) activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070681">
    <property type="term" value="P:glutaminyl-tRNAGln biosynthesis via transamidation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006450">
    <property type="term" value="P:regulation of translational fidelity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006412">
    <property type="term" value="P:translation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.20.60">
    <property type="entry name" value="Glu-tRNAGln amidotransferase C subunit, N-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00122">
    <property type="entry name" value="GatC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036113">
    <property type="entry name" value="Asp/Glu-ADT_sf_sub_c"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003837">
    <property type="entry name" value="GatC"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00135">
    <property type="entry name" value="gatC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15004:SF0">
    <property type="entry name" value="GLUTAMYL-TRNA(GLN) AMIDOTRANSFERASE SUBUNIT C, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15004">
    <property type="entry name" value="UNCHARACTERIZED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02686">
    <property type="entry name" value="GatC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF141000">
    <property type="entry name" value="Glu-tRNAGln amidotransferase C subunit"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0067">ATP-binding</keyword>
  <keyword id="KW-0436">Ligase</keyword>
  <keyword id="KW-0547">Nucleotide-binding</keyword>
  <keyword id="KW-0648">Protein biosynthesis</keyword>
  <feature type="chain" id="PRO_0000105335" description="Aspartyl/glutamyl-tRNA(Asn/Gln) amidotransferase subunit C">
    <location>
      <begin position="1"/>
      <end position="100"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="100" mass="11268" checksum="7E9273A6AA979250" modified="2004-12-21" version="1">MTKVTREEVEHIANLARLQISPEETEEMANTLESILDFAKQNDSADTEGVEPTYHVLDLQNVLREDKAIKGIPQELALKNAKETEDGQFKVPTIMNEEDA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>