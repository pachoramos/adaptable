<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-02-21" modified="2024-11-27" version="104" xmlns="http://uniprot.org/uniprot">
  <accession>Q7A087</accession>
  <name>RL17_STAAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Large ribosomal subunit protein bL17</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">50S ribosomal protein L17</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rplQ</name>
    <name type="ordered locus">MW2142</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="subunit">
    <text evidence="1">Part of the 50S ribosomal subunit. Contacts protein L32.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the bacterial ribosomal protein bL17 family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB96007.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000542274.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="PDB" id="8Y36">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.65 A"/>
    <property type="chains" value="L=3-122"/>
  </dbReference>
  <dbReference type="PDB" id="8Y37">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.53 A"/>
    <property type="chains" value="L=3-122"/>
  </dbReference>
  <dbReference type="PDB" id="8Y38">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.58 A"/>
    <property type="chains" value="L=3-122"/>
  </dbReference>
  <dbReference type="PDB" id="8Y39">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.60 A"/>
    <property type="chains" value="L=3-122"/>
  </dbReference>
  <dbReference type="PDBsum" id="8Y36"/>
  <dbReference type="PDBsum" id="8Y37"/>
  <dbReference type="PDBsum" id="8Y38"/>
  <dbReference type="PDBsum" id="8Y39"/>
  <dbReference type="AlphaFoldDB" id="Q7A087"/>
  <dbReference type="EMDB" id="EMD-38873"/>
  <dbReference type="EMDB" id="EMD-38874"/>
  <dbReference type="EMDB" id="EMD-38875"/>
  <dbReference type="EMDB" id="EMD-38876"/>
  <dbReference type="SMR" id="Q7A087"/>
  <dbReference type="GeneID" id="66840435"/>
  <dbReference type="KEGG" id="sam:MW2142"/>
  <dbReference type="HOGENOM" id="CLU_074407_2_2_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0022625">
    <property type="term" value="C:cytosolic large ribosomal subunit"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006412">
    <property type="term" value="P:translation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="FunFam" id="3.90.1030.10:FF:000002">
    <property type="entry name" value="50S ribosomal protein L17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.90.1030.10">
    <property type="entry name" value="Ribosomal protein L17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01368">
    <property type="entry name" value="Ribosomal_bL17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000456">
    <property type="entry name" value="Ribosomal_bL17"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR047859">
    <property type="entry name" value="Ribosomal_bL17_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036373">
    <property type="entry name" value="Ribosomal_bL17_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00059">
    <property type="entry name" value="L17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR14413:SF16">
    <property type="entry name" value="39S RIBOSOMAL PROTEIN L17, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR14413">
    <property type="entry name" value="RIBOSOMAL PROTEIN L17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01196">
    <property type="entry name" value="Ribosomal_L17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF64263">
    <property type="entry name" value="Prokaryotic ribosomal protein L17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01167">
    <property type="entry name" value="RIBOSOMAL_L17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <feature type="chain" id="PRO_0000224142" description="Large ribosomal subunit protein bL17">
    <location>
      <begin position="1"/>
      <end position="122"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01368"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="122" mass="13748" checksum="BB10FD48584A867E" modified="2004-07-05" version="1">MGYRKLGRTSDQRKAMLRDLATSLIISERIETTEARAKEVRSVVEKLITLGKKGDLASRRNAAKTLRNVEILNEDETTQTALQKLFGEIAERYTERQGGYTRILKQGPRRGDGAESVIIELV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>