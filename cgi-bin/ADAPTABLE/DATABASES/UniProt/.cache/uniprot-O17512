<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2023-06-28" version="65" xmlns="http://uniprot.org/uniprot">
  <accession>O17512</accession>
  <name>CTXA2_CERCA</name>
  <protein>
    <recommendedName>
      <fullName>Ceratotoxin-A</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">CTXA2</name>
  </gene>
  <organism>
    <name type="scientific">Ceratitis capitata</name>
    <name type="common">Mediterranean fruit fly</name>
    <name type="synonym">Tephritis capitata</name>
    <dbReference type="NCBI Taxonomy" id="7213"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Tephritoidea</taxon>
      <taxon>Tephritidae</taxon>
      <taxon>Ceratitis</taxon>
      <taxon>Ceratitis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Insect Biochem. Mol. Biol." volume="27" first="1039" last="1046">
      <title>The genes encoding the antibacterial sex-specific peptides ceratotoxins are clustered in the genome of the medfly Ceratitis capitata.</title>
      <authorList>
        <person name="Rosetto M."/>
        <person name="de Filippis T."/>
        <person name="Manetti A.G.O."/>
        <person name="Marchini D."/>
        <person name="Baldari C.T."/>
        <person name="Dallai R."/>
      </authorList>
      <dbReference type="PubMed" id="9569644"/>
      <dbReference type="DOI" id="10.1016/s0965-1748(97)00090-8"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Female accessory gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="Insect Biochem. Mol. Biol." volume="23" first="591" last="598">
      <title>Purification and primary structure of ceratotoxin A and B, two antibacterial peptides from the female reproductive accessory glands of the medfly Ceratitis capitata (Insecta: Diptera).</title>
      <authorList>
        <person name="Marchini D."/>
        <person name="Giordano P.C."/>
        <person name="Amons R."/>
        <person name="Bernini L.F."/>
        <person name="Dallai R."/>
      </authorList>
      <dbReference type="PubMed" id="8353519"/>
      <dbReference type="DOI" id="10.1016/0965-1748(93)90032-n"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 36-64</scope>
    <source>
      <tissue>Female accessory gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Female-specific peptides with potent activity against Gram-positive and Gram-negative bacteria. They have as well hemolytic activity.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <temperatureDependence>
      <text>Thermostable. Still active at 100 degrees Celsius.</text>
    </temperatureDependence>
  </comment>
  <comment type="subunit">
    <text>Homomer of four to six subunits.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EMBL" id="Y15373">
    <property type="protein sequence ID" value="CAA75595.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O17512"/>
  <dbReference type="SMR" id="O17512"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000004967" evidence="2">
    <location>
      <begin position="24"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000004968" description="Ceratotoxin-A">
    <location>
      <begin position="36"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000004969">
    <location>
      <begin position="65"/>
      <end position="71"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="8353519"/>
    </source>
  </evidence>
  <sequence length="71" mass="7199" checksum="FFF40E2FE8F1F27C" modified="1998-01-01" version="1" precursor="true">MANLKAVFLICIVAFIAFQCVVAEPAAEDSIVVKRSIGSALKKALPVAKKIGKIALPIAKAALPVAAGLVG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>