<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2016-09-07" modified="2024-11-27" version="12" xmlns="http://uniprot.org/uniprot">
  <accession>C0HK24</accession>
  <name>ELEL_ECHLU</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">L-rhamnose-binding lectin ELEL-1</fullName>
    </recommendedName>
  </protein>
  <organism evidence="5">
    <name type="scientific">Echinometra lucunter</name>
    <name type="common">Rock-boring urchin</name>
    <dbReference type="NCBI Taxonomy" id="105361"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Echinodermata</taxon>
      <taxon>Eleutherozoa</taxon>
      <taxon>Echinozoa</taxon>
      <taxon>Echinoidea</taxon>
      <taxon>Euechinoidea</taxon>
      <taxon>Echinacea</taxon>
      <taxon>Camarodonta</taxon>
      <taxon>Echinidea</taxon>
      <taxon>Echinometridae</taxon>
      <taxon>Echinometra</taxon>
    </lineage>
  </organism>
  <reference evidence="6" key="1">
    <citation type="journal article" date="2015" name="Int. J. Biol. Macromol." volume="78" first="180" last="188">
      <title>L-Rhamnose-binding lectin from eggs of the Echinometra lucunter: Amino acid sequence and molecular modeling.</title>
      <authorList>
        <person name="Carneiro R.F."/>
        <person name="Teixeira C.S."/>
        <person name="de Melo A.A."/>
        <person name="de Almeida A.S."/>
        <person name="Cavada B.S."/>
        <person name="de Sousa O.V."/>
        <person name="da Rocha B.A."/>
        <person name="Nagano C.S."/>
        <person name="Sampaio A.H."/>
      </authorList>
      <dbReference type="PubMed" id="25881955"/>
      <dbReference type="DOI" id="10.1016/j.ijbiomac.2015.03.072"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>SUBUNIT</scope>
    <scope>LACK OF GLYCOSYLATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="5">Egg</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="submission" date="2016-06" db="UniProtKB">
      <authorList>
        <person name="Carneiro R."/>
      </authorList>
    </citation>
    <scope>SEQUENCE REVISION</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Rhamnose-binding lectin. Also binds alpha-D-melibiose, alpha-D-lactose, beta-D-lactose, methyl-alpha-D-galactopyranoside, methyl-beta-D--galactopyranoside and D-galactose but not D-arabinose, L-fucose, D-glucose, D-mannose, D-maltose, D-sucrose, N-acetyl-D-galactosamine, N-acetyl-D-glucosamine, N-acetyl-D-mannosamine-D-xylose or by glycoproteins orosomucoid, thyroglobulin, ovomucoid and porcine stomach mucin. Shows cation-independent hemagglutinating activity against rabbit and human erythrocytes. Agglutinates cells of Gram-positive bacterial species S.aureus but not those of Gram-negative E.coli.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <phDependence>
      <text evidence="3">Activity is stable between pH 4 and 7 but decreases at basic pH.</text>
    </phDependence>
    <temperatureDependence>
      <text evidence="3">Activity is stable up to 60 degrees Celsius, then decreases and is lost at 100 degrees Celsius.</text>
    </temperatureDependence>
  </comment>
  <comment type="subunit">
    <text evidence="3">Homodimer; disulfide-linked.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">Not glycosylated.</text>
  </comment>
  <comment type="mass spectrometry" mass="22091.0" error="2.0" method="Electrospray" evidence="3">
    <text>Homodimer.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HK24"/>
  <dbReference type="SMR" id="C0HK24"/>
  <dbReference type="GO" id="GO:0030246">
    <property type="term" value="F:carbohydrate binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd22827">
    <property type="entry name" value="Gal_Rha_Lectin_SUL-I-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.120.740:FF:000001">
    <property type="entry name" value="Adhesion G protein-coupled receptor L2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.120.740">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000922">
    <property type="entry name" value="Lectin_gal-bd_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR043159">
    <property type="entry name" value="Lectin_gal-bd_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46780:SF25">
    <property type="entry name" value="CUB DOMAIN-CONTAINING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46780">
    <property type="entry name" value="PROTEIN EVA-1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02140">
    <property type="entry name" value="Gal_Lectin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50228">
    <property type="entry name" value="SUEL_LECTIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0348">Hemagglutinin</keyword>
  <keyword id="KW-0430">Lectin</keyword>
  <feature type="chain" id="PRO_0000437082" description="L-rhamnose-binding lectin ELEL-1" evidence="3 4">
    <location>
      <begin position="1"/>
      <end position="103"/>
    </location>
  </feature>
  <feature type="domain" description="SUEL-type lectin" evidence="2">
    <location>
      <begin position="13"/>
      <end position="102"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain" evidence="5">
    <location>
      <position position="7"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="14"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="23"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="56"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="69"/>
      <end position="75"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HK23"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00260"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="25881955"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="25881955"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="103" mass="11075" checksum="DB8358C65848C44D" modified="2016-09-07" version="1">ELVSQLCLKKERVCEGSSLTISCPQKGAGISIARAIYGRTKTQVCPSDGATSNVNCKASNALNVVRDLCRGKSSCTVEASNDVFGDPCMHTYKYLELSYDCSK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>