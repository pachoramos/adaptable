<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-02-07" modified="2023-02-22" version="33" xmlns="http://uniprot.org/uniprot">
  <accession>P84757</accession>
  <name>DEF1_PAPHA</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>PhD1</fullName>
    </alternativeName>
    <component>
      <recommendedName>
        <fullName>Defensin-2</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>PhD2</fullName>
      </alternativeName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Papio hamadryas</name>
    <name type="common">Hamadryas baboon</name>
    <dbReference type="NCBI Taxonomy" id="9557"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Papio</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Biochemistry (Mosc.)" volume="71" first="879" last="883">
      <title>Alpha-defensins from blood leukocytes of the monkey Papio hamadryas.</title>
      <authorList>
        <person name="Tsvetkova E.V."/>
        <person name="Aleshina G.M."/>
        <person name="Shamova O.V."/>
        <person name="Leonova L.E."/>
        <person name="Lehrer R.I."/>
        <person name="Kokryakov V.N."/>
      </authorList>
      <dbReference type="PubMed" id="16978151"/>
      <dbReference type="DOI" id="10.1134/s0006297906080098"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Leukocyte</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="4">Has antibacterial activity against the Gram-negative bacterium E.coli and the Gram-positive bacteria L.monocytogenes and S.aureus. Has antifungal activity against C.albicans.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="3939.0" error="1.0" method="MALDI" evidence="4">
    <molecule>Defensin-1</molecule>
  </comment>
  <comment type="mass spectrometry" mass="3781.0" error="1.0" method="MALDI" evidence="4">
    <molecule>Defensin-2</molecule>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the alpha-defensin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84757"/>
  <dbReference type="SMR" id="P84757"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006081">
    <property type="entry name" value="Alpha-defensin_C"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00269">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000046065" description="Defensin-1">
    <location>
      <begin position="1"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000046066" description="Defensin-2">
    <location>
      <begin position="2"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="6"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="11"/>
      <end position="31"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P82319"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="16978151"/>
    </source>
  </evidence>
  <sequence length="33" mass="3943" checksum="7E821E6020324BD0" modified="2006-02-07" version="1">RRICRCRIGRCLGLEVYFGVCFLHGRLARRCCR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>