<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-12-07" modified="2024-11-27" version="43" xmlns="http://uniprot.org/uniprot">
  <accession>P68642</accession>
  <accession>Q8V2Z8</accession>
  <name>PG027_CAMPS</name>
  <protein>
    <recommendedName>
      <fullName>Interferon antagonist OPG027</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Host range protein 2</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">OPG027</name>
    <name type="ORF">CMP19L</name>
  </gene>
  <organism>
    <name type="scientific">Camelpox virus (strain CMS)</name>
    <dbReference type="NCBI Taxonomy" id="203172"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Varidnaviria</taxon>
      <taxon>Bamfordvirae</taxon>
      <taxon>Nucleocytoviricota</taxon>
      <taxon>Pokkesviricetes</taxon>
      <taxon>Chitovirales</taxon>
      <taxon>Poxviridae</taxon>
      <taxon>Chordopoxvirinae</taxon>
      <taxon>Orthopoxvirus</taxon>
      <taxon>Camelpox virus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Camelus</name>
    <dbReference type="NCBI Taxonomy" id="9836"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="2002" name="J. Gen. Virol." volume="83" first="855" last="872">
      <title>The sequence of camelpox virus shows it is most closely related to variola virus, the cause of smallpox.</title>
      <authorList>
        <person name="Gubser C."/>
        <person name="Smith G.L."/>
      </authorList>
      <dbReference type="PubMed" id="11907336"/>
      <dbReference type="DOI" id="10.1099/0022-1317-83-4-855"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Inhibits antiviral activity induced by type I interferons. Does not block signal transduction of IFN, but is important to counteract the host antiviral state induced by a pre-treatment with IFN (By similarity).</text>
  </comment>
  <comment type="induction">
    <text evidence="2">Expressed in the early phase of the viral replicative cycle.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the orthopoxvirus OPG027 family.</text>
  </comment>
  <dbReference type="EMBL" id="AY009089">
    <property type="protein sequence ID" value="AAG37475.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="SMR" id="P68642"/>
  <dbReference type="Proteomes" id="UP000107153">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004967">
    <property type="entry name" value="Poxvirus_C7/F8A"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03287">
    <property type="entry name" value="Pox_C7_F8A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF003779">
    <property type="entry name" value="VAC_C7L"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0244">Early protein</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <feature type="chain" id="PRO_0000099391" description="Interferon antagonist OPG027">
    <location>
      <begin position="1"/>
      <end position="150"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P68600"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="150" mass="18069" checksum="3D5557A68AF83D95" modified="2004-12-07" version="1">MGIQHEFDIIINGDIALRNLQLHRGDNYGCKLKIISNDYKKLKFRFIIRPDWSEIDEVKGLTVFANNYVVKVNKVDDTFYYVIYEAVIHLYNKKTEILIYSDDEKELFKHYYPYISLNMISKKYKVKEENYSSPYIEHPLIPYRDYESMD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>