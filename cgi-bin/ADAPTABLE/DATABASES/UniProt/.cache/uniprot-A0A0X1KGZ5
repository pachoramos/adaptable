<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2022-02-23" modified="2022-12-14" version="14" xmlns="http://uniprot.org/uniprot">
  <accession>A0A0X1KGZ5</accession>
  <name>IAA1_STIHL</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Alpha-amylase inhibitor helianthamide</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Stichodactyla helianthus</name>
    <name type="common">Sun anemone</name>
    <name type="synonym">Stoichactis helianthus</name>
    <dbReference type="NCBI Taxonomy" id="6123"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Stichodactylidae</taxon>
      <taxon>Stichodactyla</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2016" name="ACS Cent. Sci." volume="2" first="154" last="161">
      <title>Potent human alpha-amylase inhibition by the beta-defensin-like protein helianthamide.</title>
      <authorList>
        <person name="Tysoe C."/>
        <person name="Williams L.K."/>
        <person name="Keyzers R."/>
        <person name="Nguyen N.T."/>
        <person name="Tarling C."/>
        <person name="Wicki J."/>
        <person name="Goddard-Borger E.D."/>
        <person name="Aguda A.H."/>
        <person name="Perry S."/>
        <person name="Foster L.J."/>
        <person name="Andersen R.J."/>
        <person name="Brayer G.D."/>
        <person name="Withers S.G."/>
      </authorList>
      <dbReference type="PubMed" id="27066537"/>
      <dbReference type="DOI" id="10.1021/acscentsci.5b00399"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>RECOMBINANT EXPRESSION</scope>
    <scope>X-RAY CRYSTALLOGRAPHY (2.60 ANGSTROMS) IN COMPLEX WITH PORCINE PANCREATIC ALPHA-AMYLASE</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Specific pancreatic alpha-amylase (AMY2A) inhibitor. The recombinant peptide inhibits human pancreatic (Ki=0.01 nM) and porcine pancreatic alpha-amylases (Ki=0.1 nM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="5">Is structurally homologous to beta-defensins.</text>
  </comment>
  <comment type="pharmaceutical">
    <text evidence="4">Potential drug candidate for the treatment of the type 2 diabetes mellitus.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the sea anemone alpha-amylase inhibitor family.</text>
  </comment>
  <dbReference type="PDB" id="4X0N">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.60 A"/>
    <property type="chains" value="B=1-44"/>
  </dbReference>
  <dbReference type="PDBsum" id="4X0N"/>
  <dbReference type="AlphaFoldDB" id="A0A0X1KGZ5"/>
  <dbReference type="SMR" id="A0A0X1KGZ5"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015066">
    <property type="term" value="F:alpha-amylase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0022">Alpha-amylase inhibitor</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0582">Pharmaceutical</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000454102" description="Alpha-amylase inhibitor helianthamide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="region of interest" description="Inhibitory motif" evidence="2">
    <location>
      <begin position="7"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 6">
    <location>
      <begin position="6"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 6">
    <location>
      <begin position="16"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 6">
    <location>
      <begin position="20"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="6"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="12"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="25"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="31"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="36"/>
      <end position="40"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HK71"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27066537"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="27066537"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="27066537"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="6">
    <source>
      <dbReference type="PDB" id="4X0N"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="4X0N"/>
    </source>
  </evidence>
  <sequence length="44" mass="4690" checksum="47530BD01034114E" modified="2016-03-16" version="1">ESGNSCYIYHGVSGICKASCAEDEKAMAGMGVCEGHLCCYKTPW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>