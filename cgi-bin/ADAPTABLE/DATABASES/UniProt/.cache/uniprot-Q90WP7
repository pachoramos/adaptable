<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-03-25" modified="2024-07-24" version="65" xmlns="http://uniprot.org/uniprot">
  <accession>Q90WP7</accession>
  <accession>P82110</accession>
  <name>PLR_LITPI</name>
  <protein>
    <recommendedName>
      <fullName>Peptide leucine arginine</fullName>
      <shortName>pLR</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lithobates pipiens</name>
    <name type="common">Northern leopard frog</name>
    <name type="synonym">Rana pipiens</name>
    <dbReference type="NCBI Taxonomy" id="8404"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Lithobates</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2001-09" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning of cDNAs encoding defensive skin secretion peptides from the Northern leopard frog (Rana pipiens).</title>
      <authorList>
        <person name="Farragher S.M."/>
        <person name="Bjourson A.J."/>
        <person name="McClean S."/>
        <person name="Orr D.F."/>
        <person name="Shaw C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2001" name="J. Biol. Chem." volume="276" first="10145" last="10152">
      <title>Peptide leucine arginine, a potent immunomodulatory peptide, isolated and structurally characterized from the skin of the Northern Leopard frog, Rana pipiens.</title>
      <authorList>
        <person name="Salmon A.L."/>
        <person name="Cross L.J.M."/>
        <person name="Irvine A.E."/>
        <person name="Lappin T.R.J."/>
        <person name="Dathe M."/>
        <person name="Krause G."/>
        <person name="Canning P."/>
        <person name="Thim L."/>
        <person name="Beyermann M."/>
        <person name="Rothemund S."/>
        <person name="Bienert M."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="11099505"/>
      <dbReference type="DOI" id="10.1074/jbc.m009680200"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 44-61</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>CIRCULAR DICHROISM ANALYSIS</scope>
    <scope>SYNTHESIS</scope>
    <scope>3D-STRUCTURE MODELING</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1998" name="Br. J. Cancer" volume="78" first="41" last="41">
      <title>Breast cancer cell lines express specific binding sites for plr, a novel anti-proliferative peptide from frog skin venom.</title>
      <authorList>
        <person name="Boyer M."/>
        <person name="van Den Berg H.W."/>
        <person name="Shaw C."/>
        <person name="Lynch M."/>
        <person name="Johnston P."/>
      </authorList>
      <dbReference type="PubMed" id="9673585"/>
    </citation>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2003" name="Biochemistry" volume="42" first="14023" last="14035">
      <title>Ranacyclins, a new family of short cyclic antimicrobial peptides: biological function, mode of action and parameters involved in target specificity.</title>
      <authorList>
        <person name="Mangoni M.L."/>
        <person name="Papo N."/>
        <person name="Mignogna G."/>
        <person name="Andreu D."/>
        <person name="Shai Y."/>
        <person name="Barra D."/>
        <person name="Simmaco M."/>
      </authorList>
      <dbReference type="PubMed" id="14636071"/>
      <dbReference type="DOI" id="10.1021/bi034521l"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 4">Mast cell degranulating peptide. Antiproliferative activity against human breast and ovarian tumor cell lines in vitro. Inhibits granulopoiesis in rat in vitro. Causes histamine release from rat peritoneal mast cells in vitro. Has antibacterial activity against Gram-positive bacteria B.megaterium Bm11, S.lentus and M.luteus, and antifungal activity against C.tropicalis, C.guiller-mondii and P.nicotianae spores. Has hemolytic activity. The mature peptide inserts into the hydrophobic core of the bacterial cell membrane and increases permeability without disrupting membrane integrity. Probably binds to the outer membrane surface before aggregating to form transmembrane pores.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2136.0" method="Plasma desorption" evidence="2"/>
  <comment type="similarity">
    <text evidence="5">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AJ414584">
    <property type="protein sequence ID" value="CAC93861.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="2M3N">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=44-61"/>
  </dbReference>
  <dbReference type="PDBsum" id="2M3N"/>
  <dbReference type="AlphaFoldDB" id="Q90WP7"/>
  <dbReference type="BMRB" id="Q90WP7"/>
  <dbReference type="SMR" id="Q90WP7"/>
  <dbReference type="EvolutionaryTrace" id="Q90WP7"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043303">
    <property type="term" value="P:mast cell degranulation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR032019">
    <property type="entry name" value="Frog_antimicrobial_Ranidae"/>
  </dbReference>
  <dbReference type="Pfam" id="PF16048">
    <property type="entry name" value="Antimicrobial23"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-0467">Mast cell degranulation</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003461">
    <location>
      <begin position="21"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003462" description="Peptide leucine arginine">
    <location>
      <begin position="44"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439058" description="Removed in mature form" evidence="5">
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="48"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="52"/>
      <end position="54"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11099505"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="14636071"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="9673585"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="2M3N"/>
    </source>
  </evidence>
  <sequence length="62" mass="7113" checksum="C8F7F58849A01A1C" modified="2001-12-01" version="1" precursor="true">MFTLKKSLLLLFFLGTISSSLCEQERDSDDEDQGEVTEQVVKRLVRGCWTKSYPPKPCFVRG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>