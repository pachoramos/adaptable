<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-07-01" modified="2024-05-29" version="82" xmlns="http://uniprot.org/uniprot">
  <accession>P31107</accession>
  <accession>P80283</accession>
  <name>DRS2_PHYBI</name>
  <protein>
    <recommendedName>
      <fullName evidence="13 14">Dermaseptin-B2</fullName>
      <shortName evidence="13 14">DRS-B2</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="12 16">Adenoregulin</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="15">Dermaseptin BII</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ADR</name>
  </gene>
  <organism>
    <name type="scientific">Phyllomedusa bicolor</name>
    <name type="common">Two-colored leaf frog</name>
    <name type="synonym">Rana bicolor</name>
    <dbReference type="NCBI Taxonomy" id="8393"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phyllomedusa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Biochem. Biophys. Res. Commun." volume="191" first="983" last="990">
      <title>Molecular cloning of a cDNA encoding the precursor of adenoregulin from frog skin. Relationships with the vertebrate defensive peptides, dermaseptins.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ducancel F."/>
        <person name="Lajeunesse E."/>
        <person name="Boulain J.-C."/>
        <person name="Menez A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="8466537"/>
      <dbReference type="DOI" id="10.1006/bbrc.1993.1314"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1992" name="Proc. Natl. Acad. Sci. U.S.A." volume="89" first="10960" last="10963">
      <title>Frog secretions and hunting magic in the upper Amazon: identification of a peptide that interacts with an adenosine receptor.</title>
      <authorList>
        <person name="Daly J.W."/>
        <person name="Caceres J."/>
        <person name="Moni R.W."/>
        <person name="Gusovsky F."/>
        <person name="Moos M. Jr."/>
        <person name="Seamon K.B."/>
        <person name="Milton K."/>
        <person name="Myers C.W."/>
      </authorList>
      <dbReference type="PubMed" id="1438301"/>
      <dbReference type="DOI" id="10.1073/pnas.89.22.10960"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 46-78</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SYNTHESIS OF 46-78</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1994" name="Cell. Mol. Neurobiol." volume="14" first="133" last="157">
      <title>Effects of the amphiphilic peptides mastoparan and adenoregulin on receptor binding, G proteins, phosphoinositide breakdown, cyclic AMP generation, and calcium influx.</title>
      <authorList>
        <person name="Shin Y."/>
        <person name="Moni R.W."/>
        <person name="Lueders J.E."/>
        <person name="Daly J.W."/>
      </authorList>
      <dbReference type="PubMed" id="7842473"/>
      <dbReference type="DOI" id="10.1007/bf02090781"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1994" name="Eur. J. Biochem." volume="219" first="145" last="154">
      <title>Isolation and structure of novel defensive peptides from frog skin.</title>
      <authorList>
        <person name="Mor A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="8306981"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1994.tb19924.x"/>
    </citation>
    <scope>IDENTIFICATION</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1995" name="Cell. Mol. Neurobiol." volume="15" first="465" last="493">
      <title>The amphiphilic peptide adenoregulin enhances agonist binding to A1-adenosine receptors and 35SGTP gamma S to brain membranes.</title>
      <authorList>
        <person name="Moni R.W."/>
        <person name="Romero F.S."/>
        <person name="Daly J.W."/>
      </authorList>
      <dbReference type="PubMed" id="8565049"/>
      <dbReference type="DOI" id="10.1007/bf02071881"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2003" name="Eur. J. Biochem." volume="270" first="2068" last="2081">
      <title>Antimicrobial peptides from hylid and ranin frogs originated from a 150-million-year-old ancestral precursor with a conserved signal peptide but a hypermutable antimicrobial domain.</title>
      <authorList>
        <person name="Vanhoye D."/>
        <person name="Bruston F."/>
        <person name="Nicolas P."/>
        <person name="Amiche M."/>
      </authorList>
      <dbReference type="PubMed" id="12709067"/>
      <dbReference type="DOI" id="10.1046/j.1432-1033.2003.03584.x"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 46-78</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2005" name="Protein Expr. Purif." volume="40" first="404" last="410">
      <title>Expression and purification of antimicrobial peptide adenoregulin with C-amidated terminus in Escherichia coli.</title>
      <authorList>
        <person name="Cao W."/>
        <person name="Zhou Y."/>
        <person name="Ma Y."/>
        <person name="Luo Q."/>
        <person name="Wei D."/>
      </authorList>
      <dbReference type="PubMed" id="15766883"/>
      <dbReference type="DOI" id="10.1016/j.pep.2004.12.007"/>
    </citation>
    <scope>AMIDATION AT VAL-78</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2006" name="Biochemistry" volume="45" first="468" last="480">
      <title>Dermaseptin S9, an alpha-helical antimicrobial peptide with a hydrophobic core and cationic termini.</title>
      <authorList>
        <person name="Lequin O."/>
        <person name="Ladram A."/>
        <person name="Chabbert L."/>
        <person name="Bruston F."/>
        <person name="Convert O."/>
        <person name="Vanhoye D."/>
        <person name="Chassaing G."/>
        <person name="Nicolas P."/>
        <person name="Amiche M."/>
      </authorList>
      <dbReference type="PubMed" id="16401077"/>
      <dbReference type="DOI" id="10.1021/bi051711i"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2008" name="FEBS J." volume="275" first="4134" last="4151">
      <title>Structural requirements for antimicrobial versus chemoattractant activities for dermaseptin S9.</title>
      <authorList>
        <person name="Auvynet C."/>
        <person name="El Amri C."/>
        <person name="Lacombe C."/>
        <person name="Bruston F."/>
        <person name="Bourdais J."/>
        <person name="Nicolas P."/>
        <person name="Rosenstein Y."/>
      </authorList>
      <dbReference type="PubMed" id="18637027"/>
      <dbReference type="DOI" id="10.1111/j.1742-4658.2008.06554.x"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2008" name="Peptides" volume="29" first="2074" last="2082">
      <title>A consistent nomenclature of antimicrobial peptides isolated from frogs of the subfamily Phyllomedusinae.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="18644413"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.06.017"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2009" name="Biochemistry" volume="48" first="313" last="327">
      <title>Mechanism of antibacterial action of dermaseptin B2: interplay between helix-hinge-helix structure and membrane curvature strain.</title>
      <authorList>
        <person name="Galanth C."/>
        <person name="Abbassi F."/>
        <person name="Lequin O."/>
        <person name="Ayala-Sanmartin J."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
        <person name="Amiche M."/>
      </authorList>
      <dbReference type="PubMed" id="19113844"/>
      <dbReference type="DOI" id="10.1021/bi802025a"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 46-78 AND 46-68 IN SDS MICELLES</scope>
    <scope>FUNCTION</scope>
    <scope>MECHANISM OF ACTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MUTAGENESIS OF 69-ALA--GLN-81</scope>
    <scope>SYNTHESIS OF 46-78 AND 46-68</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4 6 7 8 9 10 11">Cationic amphipathic alpha-helical antimicrobial peptide with potent activity against Gram-negative and Gram-positive bacteria, fungi and protozoa (PubMed:12709067, PubMed:16401077, PubMed:18637027, PubMed:19113844, PubMed:8306981). Acts in a synergistic effect in combination with Plasticin-B1 at doses that are not active alone (PubMed:12709067). Acts by disturbing membrane functions (PubMed:19113844). On model mebranes, induces a strong perturbation of anionic lipid bilayers, resides at the hydrocarbon core-water interface, parallel to the plane of the membrane, and interacts preferentially with the polar head groups and glycerol backbone region of the anionic phospholipids, as well as the region of the lipid acyl chain near the bilayer surface (PubMed:19113844). It induces a positive curvature of the bilayer and clustering of anionic lipids, consistent with a carpet mechanism, that may lead to the formation of mixed peptide-phospholipid toroidal, transient pores and membrane permeation/disruption once a threshold peptide accumulation is reached (PubMed:19113844). Also enhances binding of agonists to adenosine A1 receptors (ADORA1), adenosine A2a receptors (ADORA2A), alpha-2 adrenergic receptors (ADRA2A) and 5-hydroxytryptamine 1A receptors (HTR1A) (PubMed:1438301, PubMed:8565049). In addition, it enhances guanyl nucleotide exchange which may result in the conversion of receptors to a high affinity state complexed with guanyl nucleotide free G-protein (PubMed:7842473). It affects human behavior eliciting profound malaise, followed by listlessness and then euphoria. It does not show cytotoxic activity on CHO cells (PubMed:19113844). It does not act as a chemoattractant (PubMed:18637027). It does not show hemolytic activity (PubMed:19113844, PubMed:8306981).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="8">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="8">Firstly parallelly oriented to the membrane surface, the peptide may mix to phospholipids, forming toroidal and transient pores in the membrane.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="18">Expressed by the skin glands.</text>
  </comment>
  <comment type="domain">
    <text evidence="8">Has a helix-hinge-helix sructural motif.</text>
  </comment>
  <comment type="PTM">
    <text evidence="5">Amidation permits an increased antimicrobial activity against some microorganisms such as T.album and S.cerevisiae.</text>
  </comment>
  <comment type="PTM">
    <text evidence="18">May contain a D-amino acid residue, since the natural peptide is not identical in chromatographic properties to the synthetic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="3186.0" method="Plasma desorption" evidence="4"/>
  <comment type="similarity">
    <text evidence="17">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=0001"/>
  </comment>
  <comment type="online information" name="The data repository of antimicrobial peptides (DRAMP)">
    <link uri="http://dramp.cpu-bioinfor.org/browse/All_Information.php?id=DRAMP01646&amp;dataset=General"/>
  </comment>
  <dbReference type="EMBL" id="X70278">
    <property type="protein sequence ID" value="CAA49763.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="JN0462">
    <property type="entry name" value="JN0462"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P31107"/>
  <dbReference type="SMR" id="P31107"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005085">
    <property type="term" value="F:guanyl-nucleotide exchange factor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022731">
    <property type="entry name" value="Dermaseptin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF12121">
    <property type="entry name" value="DD_K"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000007092" evidence="18">
    <location>
      <begin position="23"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000007093" description="Dermaseptin-B2" evidence="4">
    <location>
      <begin position="46"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000007094" evidence="18">
    <location>
      <begin position="80"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="24"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="region of interest" description="Hinge region that separates the two alpha-helices that constitute the peptide" evidence="8">
    <location>
      <begin position="54"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="19">
    <location>
      <position position="78"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of antibacterial activity. Loss of the hinge region, the shortened peptide is constituted by only one alpha-helix. Retains cationic charges of the full-length peptide." evidence="8">
    <location>
      <begin position="69"/>
      <end position="81"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12709067"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="1438301"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="15766883"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="16401077"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="18637027"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="19113844"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="7842473"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="8306981"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="11">
    <source>
      <dbReference type="PubMed" id="8565049"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="1438301"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="13">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="14">
    <source>
      <dbReference type="PubMed" id="19113844"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="15">
    <source>
      <dbReference type="PubMed" id="8306981"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="16">
    <source>
      <dbReference type="PubMed" id="8466537"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="17"/>
  <evidence type="ECO:0000305" key="18">
    <source>
      <dbReference type="PubMed" id="1438301"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="19">
    <source>
      <dbReference type="PubMed" id="15766883"/>
    </source>
  </evidence>
  <sequence length="81" mass="8844" checksum="C26ADB4E9418272D" modified="1994-10-01" version="2" precursor="true">MAFLKKSLFLVLFLGLVSLSICEEEKRENEDEEEQEDDEQSEMKRGLWSKIKEVGKEAAKAAAKAAGKAALGAVSEAVGEQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>