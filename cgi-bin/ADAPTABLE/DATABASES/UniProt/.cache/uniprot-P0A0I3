<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-03-01" modified="2024-03-27" version="99" xmlns="http://uniprot.org/uniprot">
  <accession>P0A0I3</accession>
  <accession>O06442</accession>
  <name>SECE_STAAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein translocase subunit SecE</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">secE</name>
    <name type="ordered locus">MW0490</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Essential subunit of the Sec protein translocation channel SecYEG. Clamps together the 2 halves of SecY. May contact the channel plug during translocation.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Component of the Sec protein translocase complex. Heterotrimer consisting of SecY, SecE and SecG subunits. The heterotrimers can form oligomers, although 1 heterotrimer is thought to be able to translocate proteins. Interacts with the ribosome. Interacts with SecDF, and other proteins may be involved. Interacts with SecA.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cell membrane</location>
      <topology evidence="1">Single-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the SecE/SEC61-gamma family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB94355.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_001074473.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0A0I3"/>
  <dbReference type="SMR" id="P0A0I3"/>
  <dbReference type="GeneID" id="66838827"/>
  <dbReference type="KEGG" id="sam:MW0490"/>
  <dbReference type="HOGENOM" id="CLU_113663_8_2_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008320">
    <property type="term" value="F:protein transmembrane transporter activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0065002">
    <property type="term" value="P:intracellular protein transmembrane transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009306">
    <property type="term" value="P:protein secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006605">
    <property type="term" value="P:protein targeting"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043952">
    <property type="term" value="P:protein transport by the Sec complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.5.1030">
    <property type="entry name" value="Preprotein translocase secy subunit"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00422">
    <property type="entry name" value="SecE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005807">
    <property type="entry name" value="SecE_bac"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038379">
    <property type="entry name" value="SecE_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001901">
    <property type="entry name" value="Translocase_SecE/Sec61-g"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00964">
    <property type="entry name" value="secE_bact"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33910">
    <property type="entry name" value="PROTEIN TRANSLOCASE SUBUNIT SECE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33910:SF1">
    <property type="entry name" value="PROTEIN TRANSLOCASE SUBUNIT SECE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00584">
    <property type="entry name" value="SecE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01067">
    <property type="entry name" value="SECE_SEC61G"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0653">Protein transport</keyword>
  <keyword id="KW-0811">Translocation</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000104178" description="Protein translocase subunit SecE">
    <location>
      <begin position="1"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="1">
    <location>
      <begin position="31"/>
      <end position="51"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00422"/>
    </source>
  </evidence>
  <sequence length="60" mass="6932" checksum="5C24062DE84795A2" modified="2005-03-01" version="1">MAKKESFFKGVKSEMEKTSWPTKEELFKYTVIVVSTVIFFLVFFYALDLGITALKNLLFG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>