<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-02-01" modified="2024-01-24" version="59" xmlns="http://uniprot.org/uniprot">
  <accession>P34084</accession>
  <name>HIS1_MACFA</name>
  <protein>
    <recommendedName>
      <fullName>Histatin-1</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">HTN1</name>
  </gene>
  <organism>
    <name type="scientific">Macaca fascicularis</name>
    <name type="common">Crab-eating macaque</name>
    <name type="synonym">Cynomolgus monkey</name>
    <dbReference type="NCBI Taxonomy" id="9541"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Macaca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="J. Dent. Res." volume="69" first="1717" last="1723">
      <title>Primary structure and anticandidal activity of the major histatin from parotid secretion of the subhuman primate, Macaca fascicularis.</title>
      <authorList>
        <person name="Xu T."/>
        <person name="Telser E."/>
        <person name="Troxler R.F."/>
        <person name="Oppenheim F.G."/>
      </authorList>
      <dbReference type="PubMed" id="2229609"/>
      <dbReference type="DOI" id="10.1177/00220345900690110301"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PHOSPHORYLATION AT SER-2</scope>
    <source>
      <tissue>Parotid gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Histatins (Hsts) are cationic and histidine-rich secreted peptides mainly synthesized by saliva glands of humans and higher primates (By similarity). Hsts are considered to be major precursors of the protective proteinaceous structure on tooth surfaces (enamel pellicle).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the histatin/statherin family.</text>
  </comment>
  <dbReference type="PIR" id="A60736">
    <property type="entry name" value="A60736"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P34084"/>
  <dbReference type="iPTMnet" id="P34084"/>
  <dbReference type="Proteomes" id="UP000233100">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005783">
    <property type="term" value="C:endoplasmic reticulum"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031214">
    <property type="term" value="P:biomineral tissue development"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0022409">
    <property type="term" value="P:positive regulation of cell-cell adhesion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009893">
    <property type="term" value="P:positive regulation of metabolic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1900026">
    <property type="term" value="P:positive regulation of substrate adhesion-dependent cell spreading"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035470">
    <property type="term" value="P:positive regulation of vascular wound healing"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090303">
    <property type="term" value="P:positive regulation of wound healing"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1903691">
    <property type="term" value="P:positive regulation of wound healing, spreading of epidermal cells"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0091">Biomineralization</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044777" description="Histatin-1">
    <location>
      <begin position="1"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="1"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic residues" evidence="2">
    <location>
      <begin position="8"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="3">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P15515"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="2229609"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="38" mass="4803" checksum="CBE4B381932FB63C" modified="1994-02-01" version="1">DSHEERHHGRHGHHKYGRKFHEKHHSHRGYRSNYLYDN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>