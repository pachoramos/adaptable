<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-11-27" version="145" xmlns="http://uniprot.org/uniprot">
  <accession>P42831</accession>
  <name>CCL2_PIG</name>
  <protein>
    <recommendedName>
      <fullName>C-C motif chemokine 2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Monocyte chemoattractant protein 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Monocyte chemotactic protein 1</fullName>
      <shortName>MCP-1</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Small-inducible cytokine A2</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CCL2</name>
    <name type="synonym">SCYA2</name>
  </gene>
  <organism>
    <name type="scientific">Sus scrofa</name>
    <name type="common">Pig</name>
    <dbReference type="NCBI Taxonomy" id="9823"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Suina</taxon>
      <taxon>Suidae</taxon>
      <taxon>Sus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Biochem. Biophys. Res. Commun." volume="199" first="962" last="968">
      <title>Porcine luteal cells express monocyte chemoattractant protein-1 (MCP-1): analysis by polymerase chain reaction and cDNA cloning.</title>
      <authorList>
        <person name="Hosang K."/>
        <person name="Knoke I."/>
        <person name="Klaudiny J."/>
        <person name="Wempe F."/>
        <person name="Wuttke W."/>
        <person name="Scheit K.H."/>
      </authorList>
      <dbReference type="PubMed" id="7510962"/>
      <dbReference type="DOI" id="10.1006/bbrc.1994.1323"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="1994-07" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Zach O.R.F."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 3">Acts as a ligand for C-C chemokine receptor CCR2 (By similarity). Signals through binding and activation of CCR2 and induces a strong chemotactic response and mobilization of intracellular calcium ions (By similarity). Exhibits a chemotactic activity for monocytes and basophils but not neutrophils or eosinophils (By similarity). Plays an important role in mediating peripheral nerve injury-induced neuropathic pain (By similarity). Increases NMDA-mediated synaptic transmission in both dopamine D1 and D2 receptor-containing neurons, which may be caused by MAPK/ERK-dependent phosphorylation of GRIN2B/NMDAR2B (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="3">Monomer or homodimer; in equilibrium. Is tethered on endothelial cells by glycosaminoglycan (GAG) side chains of proteoglycans. Interacts with TNFAIP6 (via Link domain).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="PTM">
    <text evidence="3">Processing at the N-terminus can regulate receptor and target cell selectivity (By similarity). Deletion of the N-terminal residue converts it from an activator of basophil to an eosinophil chemoattractant (By similarity).</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">N-Glycosylated.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the intercrine beta (chemokine CC) family.</text>
  </comment>
  <dbReference type="EMBL" id="Z48479">
    <property type="protein sequence ID" value="CAA88370.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X79416">
    <property type="protein sequence ID" value="CAA55945.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="JC2136">
    <property type="entry name" value="JC2136"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_999379.1">
    <property type="nucleotide sequence ID" value="NM_214214.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P42831"/>
  <dbReference type="SMR" id="P42831"/>
  <dbReference type="STRING" id="9823.ENSSSCP00000018779"/>
  <dbReference type="PaxDb" id="9823-ENSSSCP00000018779"/>
  <dbReference type="Ensembl" id="ENSSSCT00000019290.5">
    <property type="protein sequence ID" value="ENSSSCP00000018779.2"/>
    <property type="gene ID" value="ENSSSCG00000017723.5"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00005020545">
    <property type="protein sequence ID" value="ENSSSCP00005012371"/>
    <property type="gene ID" value="ENSSSCG00005013096"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00015014476.1">
    <property type="protein sequence ID" value="ENSSSCP00015005616.1"/>
    <property type="gene ID" value="ENSSSCG00015010935.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00030075613.1">
    <property type="protein sequence ID" value="ENSSSCP00030034551.1"/>
    <property type="gene ID" value="ENSSSCG00030054240.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00035084936.1">
    <property type="protein sequence ID" value="ENSSSCP00035035342.1"/>
    <property type="gene ID" value="ENSSSCG00035063168.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00040012972.1">
    <property type="protein sequence ID" value="ENSSSCP00040004899.1"/>
    <property type="gene ID" value="ENSSSCG00040010004.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00045031268.1">
    <property type="protein sequence ID" value="ENSSSCP00045021669.1"/>
    <property type="gene ID" value="ENSSSCG00045018363.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00050053082.1">
    <property type="protein sequence ID" value="ENSSSCP00050022292.1"/>
    <property type="gene ID" value="ENSSSCG00050039348.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00055016609.1">
    <property type="protein sequence ID" value="ENSSSCP00055013103.1"/>
    <property type="gene ID" value="ENSSSCG00055008496.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00060077021.1">
    <property type="protein sequence ID" value="ENSSSCP00060033287.1"/>
    <property type="gene ID" value="ENSSSCG00060056544.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00065092116.1">
    <property type="protein sequence ID" value="ENSSSCP00065040308.1"/>
    <property type="gene ID" value="ENSSSCG00065067117.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00070015610.1">
    <property type="protein sequence ID" value="ENSSSCP00070012916.1"/>
    <property type="gene ID" value="ENSSSCG00070008075.1"/>
  </dbReference>
  <dbReference type="GeneID" id="397422"/>
  <dbReference type="KEGG" id="ssc:397422"/>
  <dbReference type="CTD" id="6347"/>
  <dbReference type="eggNOG" id="ENOG502S6ZP">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT01100000263482"/>
  <dbReference type="HOGENOM" id="CLU_141716_1_0_1"/>
  <dbReference type="InParanoid" id="P42831"/>
  <dbReference type="OMA" id="PITCCYT"/>
  <dbReference type="OrthoDB" id="4265193at2759"/>
  <dbReference type="TreeFam" id="TF334888"/>
  <dbReference type="Reactome" id="R-SSC-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000008227">
    <property type="component" value="Chromosome 12"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000314985">
    <property type="component" value="Chromosome 12"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694570">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694571">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694720">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694722">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694723">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694724">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694725">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694726">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694727">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694728">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSSSCG00000017723">
    <property type="expression patterns" value="Expressed in endocardial endothelium and 42 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P42831">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048020">
    <property type="term" value="F:CCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048245">
    <property type="term" value="P:eosinophil chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051968">
    <property type="term" value="P:positive regulation of synaptic transmission, glutamatergic"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019233">
    <property type="term" value="P:sensory perception of pain"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00272">
    <property type="entry name" value="Chemokine_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000002">
    <property type="entry name" value="C-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000827">
    <property type="entry name" value="Chemokine_CC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF98">
    <property type="entry name" value="C-C MOTIF CHEMOKINE 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00472">
    <property type="entry name" value="SMALL_CYTOKINES_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005153" description="C-C motif chemokine 2">
    <location>
      <begin position="24"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="3">
    <location>
      <position position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="34"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="35"/>
      <end position="75"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P10148"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P13500"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="99" mass="10976" checksum="4C0AC6278D4F0A09" modified="1995-11-01" version="1" precursor="true">MKVSAALLCLLLTAATFCTQVLAQPDAINSPVTCCYTLTSKKISMQRLMSYRRVTSSKCPKEAVIFKTIAGKEICAEPKQKWVQDSISHLDKKNQTPKP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>