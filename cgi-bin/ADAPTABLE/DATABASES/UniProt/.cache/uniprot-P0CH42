<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-10-05" modified="2023-09-13" version="26" xmlns="http://uniprot.org/uniprot">
  <accession>P0CH42</accession>
  <name>NA1B_CONGI</name>
  <protein>
    <recommendedName>
      <fullName evidence="7">Delta-actitoxin-Cgg1b</fullName>
      <shortName evidence="7">Delta-AITX-Cgg1b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="7">Cp-1</fullName>
      <shortName evidence="6">Cp I</shortName>
      <shortName evidence="4">CpI</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="5">Delta-actitoxin-Cps1a</fullName>
      <shortName evidence="5">Delta-AITX-Cps1a</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Condylactis gigantea</name>
    <name type="common">Giant Caribbean anemone</name>
    <name type="synonym">Condylactis passiflora</name>
    <dbReference type="NCBI Taxonomy" id="47073"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Condylactis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Fish. Sci." volume="61" first="1016" last="1021">
      <title>Isolation and amino acid sequences of polypeptide toxins in the Caribbean sea anemone Condylactis passiflora.</title>
      <authorList>
        <person name="Shiomi K."/>
        <person name="Lin X.Y."/>
        <person name="Nagashima Y."/>
        <person name="Ishida M."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>HYDROXYLATION AT PRO-3</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2009" name="Toxicon" volume="54" first="1102" last="1111">
      <title>Actions of sea anemone type 1 neurotoxins on voltage-gated sodium channel isoforms.</title>
      <authorList>
        <person name="Wanke E."/>
        <person name="Zaharenko A.J."/>
        <person name="Redaelli E."/>
        <person name="Schiavon E."/>
      </authorList>
      <dbReference type="PubMed" id="19393679"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2009.04.018"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3">Binds voltage-dependently at site 3 of sodium channels (Nav) and inhibits the inactivation, thereby blocking neuronal transmission (By similarity). The minimum lethal dose against crabs is 7.3 mg/kg (Ref.1).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="7">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the sea anemone sodium channel inhibitory toxin family. Type I subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="7">Since the species C.passiflora is considered as a synonym of C.gigantea, the protein name was updated from Delta-actitoxin-Cps1a to Delta-actitoxin-Cgg1b.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0CH42"/>
  <dbReference type="SMR" id="P0CH42"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00706">
    <property type="entry name" value="Toxin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0379">Hydroxylation</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000397963" description="Delta-actitoxin-Cgg1b" evidence="3">
    <location>
      <begin position="1"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="modified residue" description="Hydroxyproline" evidence="3">
    <location>
      <position position="3"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="4"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="6"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="27"/>
      <end position="45"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P0C280"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="19393679"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="47" mass="5047" checksum="B095184616759A10" modified="2010-10-05" version="1">GVPCRCDSDGPSVHGNTLSGTVWVGSCASGWHKCNTEHNIFHECCKE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>