<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-01-11" modified="2024-11-27" version="31" xmlns="http://uniprot.org/uniprot">
  <accession>P0CI43</accession>
  <name>LV1B2_LYCMC</name>
  <protein>
    <recommendedName>
      <fullName>Lipolysis-activating peptide 1-beta chain</fullName>
      <shortName>LVP1-beta</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lychas mucronatus</name>
    <name type="common">Chinese swimming scorpion</name>
    <dbReference type="NCBI Taxonomy" id="172552"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Lychas</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2010" name="BMC Genomics" volume="11" first="452" last="452">
      <title>Comparative venom gland transcriptome analysis of the scorpion Lychas mucronatus reveals intraspecific toxic gene diversity and new venomous components.</title>
      <authorList>
        <person name="Zhao R."/>
        <person name="Ma Y."/>
        <person name="He Y."/>
        <person name="Di Z."/>
        <person name="Wu Y.-L."/>
        <person name="Cao Z.-J."/>
        <person name="Li W.-X."/>
      </authorList>
      <dbReference type="PubMed" id="20663230"/>
      <dbReference type="DOI" id="10.1186/1471-2164-11-452"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>Yunnan</strain>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">The homodimer inhibits HMG-CoA reductase (HMGCR) (32% of inhibition produced by 0.6 uM), a glycoprotein involved in the control of cholesterol biosynthesis. The inhibitory effects of bumarsin are seen at much lower concentrations (0.6 uM) than that for statins such as atorvastatin (5 mM) and simvastatin (10 uM). In addition to inhibition of HMG-CoA reductase, this protein lowers cholesterol levels by inducing steroid hormone synthesis via StAR, and by increasing reverse cholesterol transport mediated by the induction of ABCA1 and APOA1 (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="3">The heterodimer non-edited LVP1 induces lipolysis in rat adipocytes. Induction of lipolysis by LVP1 appears to be mediated through the beta-2 adrenergic receptor pathway (ADRB2) (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="7">The monomer edited version, similar to alpha-toxins, may modulate voltage-gated sodium channels (Nav) and may block voltage-gated potassium channels (Kv).</text>
  </comment>
  <comment type="subunit">
    <text evidence="4">Homodimer; disulfide-linked or monomer (edited version) or heterodimer of an alpha chain (AC P0CI44 or AC P0CI45) and this beta chain (non-edited version).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="8">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="7">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="RNA editing">
    <location>
      <position position="31" evidence="1"/>
    </location>
    <text evidence="1">Partially edited. RNA editing at this position consists of an insertion of three nucleotides, restoring the first Cys residue that forms a disulfide bond with Cys-86, giving a monomeric toxin with 4 disulfide bonds (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the long (3 C-C) scorpion toxin superfamily.</text>
  </comment>
  <dbReference type="EMBL" id="GT028742">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0CI43"/>
  <dbReference type="SMR" id="P0CI43"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0691">RNA editing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="5">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000403887" description="Lipolysis-activating peptide 1-beta chain">
    <location>
      <begin position="20"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="6">
    <location>
      <begin position="20"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="34"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="42"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="46"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (with C-83 (AC P0CI44) or C-87 (AC P0CI45) in LVP1 chain alpha)" evidence="1">
    <location>
      <position position="86"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P01493"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P84809"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="4">
    <source>
      <dbReference type="UniProtKB" id="Q95P90"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="5"/>
  <evidence type="ECO:0000255" key="6">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="20663230"/>
    </source>
  </evidence>
  <sequence length="94" mass="10494" checksum="EBE8157FA33A706B" modified="2011-01-11" version="1" precursor="true">MKILAVVLISVIVLNTANGENYYPQKYTNDYYGCQQQTDAFCDKVCKLHLAESGFCDQSWGLAKACKCVNVSYDNSFYFNALESQCPLLNKSAA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>