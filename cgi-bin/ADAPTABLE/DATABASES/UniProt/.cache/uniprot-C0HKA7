<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-02-15" modified="2020-02-26" version="8" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKA7</accession>
  <name>BRKP3_PHYNA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">[Thr6]-phyllokinin</fullName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Physalaemus nattereri</name>
    <name type="common">Cuyaba dwarf frog</name>
    <name type="synonym">Eupemphix nattereri</name>
    <dbReference type="NCBI Taxonomy" id="248869"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Leptodactylidae</taxon>
      <taxon>Leiuperinae</taxon>
      <taxon>Physalaemus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2015" name="Rapid Commun. Mass Spectrom." volume="29" first="2061" last="2068">
      <title>Skin secretion peptides: the molecular facet of the deimatic behavior of the four-eyed frog, Physalaemus nattereri (Anura, Leptodactylidae).</title>
      <authorList>
        <person name="Barbosa E.A."/>
        <person name="Iembo T."/>
        <person name="Martins G.R."/>
        <person name="Silva L.P."/>
        <person name="Prates M.V."/>
        <person name="Andrade A.C."/>
        <person name="Bloch C. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="26443407"/>
      <dbReference type="DOI" id="10.1002/rcm.7313"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>HYDROXYLATION AT PRO-3</scope>
    <scope>SULFATION AT TYR-11</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">May produce in vitro relaxation of rat arterial smooth muscle and constriction of intestinal smooth muscle. May target bradykinin receptors (BDKRB).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1350.85" method="MALDI" evidence="2">
    <text>[Thr6]-Phyllokinin.</text>
  </comment>
  <comment type="mass spectrometry" mass="1366.82" method="MALDI" evidence="2">
    <text>[Hyp3,Thr6]-Phyllokinin.</text>
  </comment>
  <comment type="mass spectrometry" mass="1430.83" method="MALDI" evidence="2">
    <text>[Thr6]-Phyllokinin, sulfated.</text>
  </comment>
  <comment type="mass spectrometry" mass="1446.78" method="MALDI" evidence="2">
    <text>[Hyp3,Thr6]-Phyllokinin, sulfated.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the bradykinin-related peptide family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-1222">Bradykinin receptor impairing toxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0379">Hydroxylation</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0765">Sulfation</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000438957" description="[Thr6]-phyllokinin" evidence="2">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="4-hydroxyproline; in form [Hyp3,Thr6]-Phyllokinin" evidence="2">
    <location>
      <position position="3"/>
    </location>
  </feature>
  <feature type="modified residue" description="Sulfotyrosine; partial; in form [Thr6]-Phyllokinin and [Hyp3,Thr6]-Phyllokinin" evidence="2">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="L0PIN3"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="26443407"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="26443407"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="26443407"/>
    </source>
  </evidence>
  <sequence length="11" mass="1351" checksum="25051393D771A9C8" modified="2017-02-15" version="1">RPPGFTPFRIY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>