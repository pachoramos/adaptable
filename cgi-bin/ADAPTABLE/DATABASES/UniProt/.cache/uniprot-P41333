<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-02-01" modified="2019-12-11" version="50" xmlns="http://uniprot.org/uniprot">
  <accession>P41333</accession>
  <name>TKNA_SCYCA</name>
  <protein>
    <recommendedName>
      <fullName>Substance P</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Scyliorhinus canicula</name>
    <name type="common">Small-spotted catshark</name>
    <name type="synonym">Squalus canicula</name>
    <dbReference type="NCBI Taxonomy" id="7830"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Chondrichthyes</taxon>
      <taxon>Elasmobranchii</taxon>
      <taxon>Galeomorphii</taxon>
      <taxon>Galeoidea</taxon>
      <taxon>Carcharhiniformes</taxon>
      <taxon>Scyliorhinidae</taxon>
      <taxon>Scyliorhinus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Eur. J. Biochem." volume="214" first="469" last="474">
      <title>Primary structures and biological activities of substance-P-related peptides from the brain of the dogfish, Scyliorhinus canicula.</title>
      <authorList>
        <person name="Waugh D."/>
        <person name="Wang Y."/>
        <person name="Hazon N."/>
        <person name="Balment R.J."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="7685693"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1993.tb17943.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT MET-11</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Tachykinins are active peptides which excite neurons, evoke behavioral responses, are potent vasodilators and secretagogues, and contract (directly or indirectly) many smooth muscles.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the tachykinin family.</text>
  </comment>
  <dbReference type="PIR" id="S33300">
    <property type="entry name" value="S33300"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045202">
    <property type="term" value="C:synapse"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="GOC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007268">
    <property type="term" value="P:chemical synaptic transmission"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013055">
    <property type="entry name" value="Tachy_Neuro_lke_CS"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00267">
    <property type="entry name" value="TACHYKININ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0529">Neurotransmitter</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044424" description="Substance P">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="1">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="7685693"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="11" mass="1278" checksum="214860DEC9D6D867" modified="1995-02-01" version="1">KPRPGQFFGLM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>