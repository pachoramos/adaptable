<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-09-21" modified="2024-07-24" version="26" xmlns="http://uniprot.org/uniprot">
  <accession>P86899</accession>
  <name>CYCM_CLITE</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Cyclotide cter-M</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Clitoria ternatea</name>
    <name type="common">Butterfly pea</name>
    <dbReference type="NCBI Taxonomy" id="43366"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Fabales</taxon>
      <taxon>Fabaceae</taxon>
      <taxon>Papilionoideae</taxon>
      <taxon>50 kb inversion clade</taxon>
      <taxon>NPAAA clade</taxon>
      <taxon>indigoferoid/millettioid clade</taxon>
      <taxon>Phaseoleae</taxon>
      <taxon>Clitoria</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2011" name="Proc. Natl. Acad. Sci. U.S.A." volume="108" first="10127" last="10132">
      <title>Discovery of an unusual biosynthetic origin for circular proteins in legumes.</title>
      <authorList>
        <person name="Poth A.G."/>
        <person name="Colgrave M.L."/>
        <person name="Lyons R.E."/>
        <person name="Daly N.L."/>
        <person name="Craik D.J."/>
      </authorList>
      <dbReference type="PubMed" id="21593408"/>
      <dbReference type="DOI" id="10.1073/pnas.1103660108"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 25-53</scope>
    <scope>STRUCTURE BY NMR OF 25-53</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>CYCLIZATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>OXIDATION AT MET-51</scope>
    <source>
      <tissue evidence="3">Leaf</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 3">Probably participates in a plant defense mechanism. Displays insecticidal activity against H.armigera. Has weak hemolytic activity. Binds to phospholipid membranes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="3">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="domain">
    <text evidence="3">Displays an unusual domain structure in comparison to other known cyclotides as the cyclotide is followed by a C-terminal albumin-like region.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3">Cyclotide cter-M is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="3057.25" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the cyclotide family. Moebius subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="JF501210">
    <property type="protein sequence ID" value="AEB92229.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="2LAM">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=25-53"/>
  </dbReference>
  <dbReference type="PDBsum" id="2LAM"/>
  <dbReference type="AlphaFoldDB" id="P86899"/>
  <dbReference type="BMRB" id="P86899"/>
  <dbReference type="SMR" id="P86899"/>
  <dbReference type="EvolutionaryTrace" id="P86899"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR032000">
    <property type="entry name" value="Albumin_I_a"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012324">
    <property type="entry name" value="Cyclotide_moebius_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF16720">
    <property type="entry name" value="Albumin_I_a"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60009">
    <property type="entry name" value="CYCLOTIDE_MOEBIUS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0558">Oxidation</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1 4">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000412637" description="Cyclotide cter-M" evidence="2 3">
    <location>
      <begin position="25"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000412638" evidence="3">
    <location>
      <begin position="54"/>
      <end position="127"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine sulfoxide" evidence="3">
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="29"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="33"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="38"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Gly-Asn)" evidence="3">
    <location>
      <begin position="25"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="34"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="44"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="49"/>
      <end position="52"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="21593408"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="21593408"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="2LAM"/>
    </source>
  </evidence>
  <sequence length="127" mass="14078" checksum="F8A4C74D2270F02C" modified="2011-09-21" version="1" precursor="true">MAYVRLTSLAVLFFLAASVMKTEGGLPTCGETCTLGTCYVPDCSCSWPICMKNHIIAANAKTVNEHRLLCTSHEDCFKKGTGNYCASFPDSNIHFGWCFHAESEGYLLKDFMNMSKDDLKMPLESTN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>