<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-12-15" modified="2024-11-27" version="85" xmlns="http://uniprot.org/uniprot">
  <accession>Q91195</accession>
  <name>CYT_ONCMY</name>
  <protein>
    <recommendedName>
      <fullName>Cystatin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Oncorhynchus mykiss</name>
    <name type="common">Rainbow trout</name>
    <name type="synonym">Salmo gairdneri</name>
    <dbReference type="NCBI Taxonomy" id="8022"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Actinopterygii</taxon>
      <taxon>Neopterygii</taxon>
      <taxon>Teleostei</taxon>
      <taxon>Protacanthopterygii</taxon>
      <taxon>Salmoniformes</taxon>
      <taxon>Salmonidae</taxon>
      <taxon>Salmoninae</taxon>
      <taxon>Oncorhynchus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1995-08" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Li F."/>
        <person name="An H."/>
        <person name="Seymour T.A."/>
        <person name="Morrissey M.T."/>
        <person name="Barnes D.W."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Cysteine proteinase inhibitor.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the cystatin family.</text>
  </comment>
  <dbReference type="EMBL" id="U33555">
    <property type="protein sequence ID" value="AAA82049.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001118176.1">
    <property type="nucleotide sequence ID" value="NM_001124704.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q91195"/>
  <dbReference type="SMR" id="Q91195"/>
  <dbReference type="MEROPS" id="I25.041"/>
  <dbReference type="GeneID" id="100136752"/>
  <dbReference type="KEGG" id="omy:100136752"/>
  <dbReference type="OrthoDB" id="3086783at2759"/>
  <dbReference type="SABIO-RK" id="Q91195"/>
  <dbReference type="Proteomes" id="UP000694395">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031982">
    <property type="term" value="C:vesicle"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030414">
    <property type="term" value="F:peptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="AgBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000117">
    <property type="term" value="P:negative regulation of cysteine-type endopeptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="CDD" id="cd00042">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.450.10:FF:000004">
    <property type="entry name" value="Cystatin C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.450.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000010">
    <property type="entry name" value="Cystatin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046350">
    <property type="entry name" value="Cystatin_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018073">
    <property type="entry name" value="Prot_inh_cystat_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46186">
    <property type="entry name" value="CYSTATIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46186:SF12">
    <property type="entry name" value="HYPOTHETICAL LOC561073-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00031">
    <property type="entry name" value="Cystatin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00043">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54403">
    <property type="entry name" value="Cystatin/monellin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00287">
    <property type="entry name" value="CYSTATIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000006666" description="Cystatin">
    <location>
      <begin position="20"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Secondary area of contact" evidence="1">
    <location>
      <begin position="67"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="site" description="Reactive site" evidence="1">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="85"/>
      <end position="94"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="108"/>
      <end position="128"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="130" mass="14600" checksum="23CD7AB960BAFAC4" modified="1996-11-01" version="1" precursor="true">MEWKIVVPLFAVAFTVANAGLIGGPMDANMNDQGTRDALQFAVVEHNKKTNDMFVRQVAKVVNAQKQVVSGMKYIFTVQMGRTPCRKGGVEKVCSVHKDPQMAVPYKCTFEVWSRPWMSDIQMVKNQCES</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>