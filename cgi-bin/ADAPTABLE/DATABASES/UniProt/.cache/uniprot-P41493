<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2021-06-02" version="55" xmlns="http://uniprot.org/uniprot">
  <accession>P41493</accession>
  <name>NSK2_SARBU</name>
  <protein>
    <recommendedName>
      <fullName>Neosulfakinin-2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Neosulfakinin-II</fullName>
      <shortName>Neb-SK-II</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Sarcophaga bullata</name>
    <name type="common">Grey flesh fly</name>
    <name type="synonym">Neobellieria bullata</name>
    <dbReference type="NCBI Taxonomy" id="7385"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Oestroidea</taxon>
      <taxon>Sarcophagidae</taxon>
      <taxon>Sarcophaga</taxon>
      <taxon>Neobellieria</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="Comp. Biochem. Physiol." volume="103C" first="135" last="142">
      <title>Isolation and primary structure of two sulfakinin-like peptides from the fleshfly, Neobellieria bullata.</title>
      <authorList>
        <person name="Fonagy A."/>
        <person name="Schoofs L."/>
        <person name="Proost P."/>
        <person name="van Damme J."/>
        <person name="de Loof A."/>
      </authorList>
      <dbReference type="PubMed" id="1360367"/>
      <dbReference type="DOI" id="10.1016/0742-8413(92)90242-y"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-14</scope>
    <source>
      <tissue>Head</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Myotropic peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the gastrin/cholecystokinin family.</text>
  </comment>
  <dbReference type="PIR" id="A56632">
    <property type="entry name" value="A56632"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013152">
    <property type="entry name" value="Gastrin/cholecystokinin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013259">
    <property type="entry name" value="Sulfakinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08257">
    <property type="entry name" value="Sulfakinin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00259">
    <property type="entry name" value="GASTRIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0765">Sulfation</keyword>
  <feature type="peptide" id="PRO_0000043897" description="Neosulfakinin-2">
    <location>
      <begin position="1"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="modified residue" description="Sulfotyrosine" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="2">
    <location>
      <position position="14"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="1360367"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="14" mass="1796" checksum="8B4E06D5B61C62AA" modified="1995-11-01" version="1">XXEEQFDDYGHMRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>