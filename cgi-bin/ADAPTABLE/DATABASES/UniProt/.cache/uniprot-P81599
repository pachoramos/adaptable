<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1999-07-15" modified="2023-02-22" version="76" xmlns="http://uniprot.org/uniprot">
  <accession>P81599</accession>
  <name>TO1F_HADVE</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Omega-hexatoxin-Hv1f</fullName>
      <shortName evidence="4">Omega-HXTX-Hv1f</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Omega-atracotoxin-Hv1f</fullName>
      <shortName evidence="3">Omega-AcTx-Hv1f</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Hadronyche versuta</name>
    <name type="common">Blue mountains funnel-web spider</name>
    <name type="synonym">Atrax versutus</name>
    <dbReference type="NCBI Taxonomy" id="6904"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Mygalomorphae</taxon>
      <taxon>Hexathelidae</taxon>
      <taxon>Hadronyche</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Eur. J. Biochem." volume="264" first="488" last="494">
      <title>Structure-function studies of omega-atracotoxin, a potent antagonist of insect voltage-gated calcium channels.</title>
      <authorList>
        <person name="Wang X.-H."/>
        <person name="Smith R."/>
        <person name="Fletcher J.I."/>
        <person name="Wilson H."/>
        <person name="Wood C.J."/>
        <person name="Merlin E.H."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="10491095"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.1999.00646.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TOXIC DOSE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Inhibits insect, but not mammalian, voltage-gated calcium channels (Cav).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="2">LD(50) is 1384 pmol/g when injected into house crickets (Acheta domestica).</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the neurotoxin 08 (Shiva) family. 01 (omega toxin) subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P81599"/>
  <dbReference type="SMR" id="P81599"/>
  <dbReference type="ArachnoServer" id="AS000201">
    <property type="toxin name" value="omega-hexatoxin-Hv1f"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019855">
    <property type="term" value="F:calcium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009415">
    <property type="entry name" value="Omega-atracotox"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018071">
    <property type="entry name" value="Omega-atracotox_CS"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06357">
    <property type="entry name" value="Omega-toxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57059">
    <property type="entry name" value="omega toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60016">
    <property type="entry name" value="OMEGA_ACTX_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0108">Calcium channel impairing toxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1218">Voltage-gated calcium channel impairing toxin</keyword>
  <feature type="peptide" id="PRO_0000044993" description="Omega-hexatoxin-Hv1f" evidence="2">
    <location>
      <begin position="1"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="site" description="Critical for insecticidal activity" evidence="1">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <feature type="site" description="Critical for insecticidal activity" evidence="1">
    <location>
      <position position="27"/>
    </location>
  </feature>
  <feature type="site" description="Critical for insecticidal activity" evidence="1">
    <location>
      <position position="35"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="11"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="17"/>
      <end position="36"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P56207"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10491095"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="10491095"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="10491095"/>
    </source>
  </evidence>
  <sequence length="37" mass="3951" checksum="EEC16AE67EE3F36A" modified="1999-07-15" version="1">SAVCIPSGQPCPYSKYCCSGSCTYKTNENGNSVQRCD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>