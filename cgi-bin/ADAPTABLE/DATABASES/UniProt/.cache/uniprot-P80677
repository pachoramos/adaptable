<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2021-09-29" version="54" xmlns="http://uniprot.org/uniprot">
  <accession>P80677</accession>
  <name>GON1_CHEPR</name>
  <protein>
    <recommendedName>
      <fullName>Gonadoliberin-1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Gonadoliberin I</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Gonadotropin-releasing hormone I</fullName>
      <shortName>GnRH-I</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Luliberin I</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Chelyosoma productum</name>
    <name type="common">Solitary ascidian</name>
    <dbReference type="NCBI Taxonomy" id="71177"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Tunicata</taxon>
      <taxon>Ascidiacea</taxon>
      <taxon>Phlebobranchia</taxon>
      <taxon>Corellidae</taxon>
      <taxon>Chelyosoma</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Proc. Natl. Acad. Sci. U.S.A." volume="93" first="10461" last="10464">
      <title>Two new forms of gonadotropin-releasing hormone in a protochordate and the evolutionary implications.</title>
      <authorList>
        <person name="Powell J.F.F."/>
        <person name="Reska-Skinner S.M."/>
        <person name="Prakash M.O."/>
        <person name="Fischer W.H."/>
        <person name="Park M."/>
        <person name="Rivier J.E."/>
        <person name="Craig A.G."/>
        <person name="Mackie G.O."/>
        <person name="Sherwood N.M."/>
      </authorList>
      <dbReference type="PubMed" id="8816823"/>
      <dbReference type="DOI" id="10.1073/pnas.93.19.10461"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text>Stimulates the secretion of gonadotropins; it stimulates the secretion of both luteinizing and follicle-stimulating hormones.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>GNRH neurons lie within blood sinuses close to the gonoducts and gonads in both juveniles and adults, implying that the neuropeptide is released into the bloodstream.</text>
  </comment>
  <comment type="mass spectrometry" mass="1246.56" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the GnRH family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002012">
    <property type="entry name" value="GnRH"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00473">
    <property type="entry name" value="GNRH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043956" description="Gonadoliberin-1">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="2">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glycine amide" evidence="1">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="8816823"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="10" mass="1264" checksum="284B3639DB5AB5A3" modified="1997-11-01" version="1">QHWSDYFKPG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>