<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-06-21" modified="2024-10-02" version="33" xmlns="http://uniprot.org/uniprot">
  <accession>Q68Y23</accession>
  <name>TX1A_MYRBA</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">M-myrmeciitoxin-Mb1a</fullName>
      <shortName evidence="5">M-MIITX-Mb1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Pilosulin-3</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Myrmecia banksi</name>
    <name type="common">Jack jumper ant</name>
    <name type="synonym">Australian jumper ant</name>
    <dbReference type="NCBI Taxonomy" id="36171"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Formicoidea</taxon>
      <taxon>Formicidae</taxon>
      <taxon>Myrmeciinae</taxon>
      <taxon>Myrmeciini</taxon>
      <taxon>Myrmecia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Arch. Biochem. Biophys." volume="428" first="170" last="178">
      <title>Molecular cloning and biological characterization of novel antimicrobial peptides, pilosulin 3 and pilosulin 4, from a species of the Australian ant genus Myrmecia.</title>
      <authorList>
        <person name="Inagaki H."/>
        <person name="Akagi M."/>
        <person name="Imai H.T."/>
        <person name="Taylor R.W."/>
        <person name="Kubo T."/>
      </authorList>
      <dbReference type="PubMed" id="15246874"/>
      <dbReference type="DOI" id="10.1016/j.abb.2004.05.013"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 51-73</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Shows moderate activity against E.coli and S.aureus (MIC&lt;25 uM), slight activity against B.subtilis (MIC&lt;50 uM), and no activity against L.garvieae, P.aeruginosa, C.albicans, and S.cerevisiae. Has no hemolytic nor cytolytic activity. Causes an IgE-independent histamine release.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the formicidae venom precursor-01 superfamily. Ant pilosulin family.</text>
  </comment>
  <comment type="caution">
    <text evidence="5">M.banksi is a member of the M.pilosula complex, but is clearly distinct from M.pilosula species.</text>
  </comment>
  <comment type="caution">
    <text evidence="5">The name pilosulin-3 has also been attributed by Davies et al., 2004 to a heterodimer from M.pilosula.</text>
  </comment>
  <dbReference type="EMBL" id="AB128064">
    <property type="protein sequence ID" value="BAD36779.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q68Y23"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR049518">
    <property type="entry name" value="Pilosulin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17499">
    <property type="entry name" value="Pilosulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000035166" evidence="6">
    <location>
      <begin position="27"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000035167" description="M-myrmeciitoxin-Mb1a" evidence="6">
    <location>
      <begin position="51"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glutamine amide" evidence="1">
    <location>
      <position position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P0C023"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15246874"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="15246874"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="15246874"/>
    </source>
  </evidence>
  <sequence length="74" mass="7585" checksum="95E14DB3C7054086" modified="2004-10-11" version="1" precursor="true">MKLSCLLLTLAIIVVLTIVHAPNVEAKALADPESDAVGFADAVGEADPNAIIGLVSKGTCVLVKTVCKKVLKQG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>