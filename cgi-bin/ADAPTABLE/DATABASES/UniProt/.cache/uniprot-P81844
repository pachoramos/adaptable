<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2022-05-25" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>P81844</accession>
  <name>CT124_RANCI</name>
  <protein>
    <recommendedName>
      <fullName>Citropin-1.2.4</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ranoidea citropa</name>
    <name type="common">Australian Blue Mountains tree frog</name>
    <name type="synonym">Litoria citropa</name>
    <dbReference type="NCBI Taxonomy" id="94770"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Eur. J. Biochem." volume="265" first="627" last="637">
      <title>Host defence peptides from the skin glands of the Australian blue mountains tree-frog Litoria citropa. Solution structure of the antibacterial peptide citropin 1.1.</title>
      <authorList>
        <person name="Wegener K.L."/>
        <person name="Wabnitz P.A."/>
        <person name="Carver J.A."/>
        <person name="Bowie J.H."/>
        <person name="Chia B.C.S."/>
        <person name="Wallace J.C."/>
        <person name="Tyler M.J."/>
      </authorList>
      <dbReference type="PubMed" id="10504394"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.1999.00750.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the dorsal and submental skin glands.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P81844"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013157">
    <property type="entry name" value="Aurein_antimicrobial_peptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08256">
    <property type="entry name" value="Antimicrobial20"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043770" description="Citropin-1.2.4">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <sequence length="18" mass="1814" checksum="500BF778D515ABD7" modified="2000-05-30" version="1">GLFDIIKKVASVVGLASP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>