<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-11-28" modified="2024-11-27" version="44" xmlns="http://uniprot.org/uniprot">
  <accession>P84919</accession>
  <name>BDS2A_BUNCI</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">U-actitoxin-Bcs2a</fullName>
      <shortName evidence="5">U-AITX-Bcs2a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Neurotoxin BcIV</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bunodosoma caissarum</name>
    <name type="common">Sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="31165"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Bunodosoma</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Biochim. Biophys. Acta" volume="1764" first="1592" last="1600">
      <title>BcIV, a new paralyzing peptide obtained from the venom of the sea anemone Bunodosoma caissarum. A comparison with the Na(+) channel toxin BcIII.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Zaharenko A.J."/>
        <person name="Ferreira W.A. Jr."/>
        <person name="Konno K."/>
        <person name="Shida C.S."/>
        <person name="Richardson M."/>
        <person name="Lucio A.D."/>
        <person name="Beirao P.S.L."/>
        <person name="de Freitas J.C."/>
      </authorList>
      <dbReference type="PubMed" id="17015047"/>
      <dbReference type="DOI" id="10.1016/j.bbapap.2006.08.010"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Nematoblast</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Neurotoxin. Causes paralysis in crabs.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="4670.0" method="Electrospray" evidence="3"/>
  <comment type="mass spectrometry" mass="4670.0" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="6">Belongs to the sea anemone type 3 (BDS) potassium channel toxin family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84919"/>
  <dbReference type="SMR" id="P84919"/>
  <dbReference type="TCDB" id="8.B.11.1.4">
    <property type="family name" value="the sea anemone peptide toxin (apetx) family"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008200">
    <property type="term" value="F:ion channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030547">
    <property type="term" value="F:signaling receptor inhibitor activity"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="2.20.20.10:FF:000006">
    <property type="entry name" value="U-actitoxin-Bcs2a"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012414">
    <property type="entry name" value="BDS_K_chnl_tox"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07936">
    <property type="entry name" value="Defensin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000262965" description="U-actitoxin-Bcs2a" evidence="3">
    <location>
      <begin position="1"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="4"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="6"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="21"/>
      <end position="39"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P11494"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="17015047"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="17015047"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="41" mass="4675" checksum="D14CCEABFC4A29FB" modified="2006-11-28" version="1">GLPCDCHGHTGTYWLNYYSKCPKGYGYTGRCRYLVGSCCYK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>