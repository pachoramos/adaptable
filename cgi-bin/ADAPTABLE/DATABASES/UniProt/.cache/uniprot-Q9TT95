<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-02-11" modified="2024-11-27" version="73" xmlns="http://uniprot.org/uniprot">
  <accession>Q9TT95</accession>
  <name>GALP_PIG</name>
  <protein>
    <recommendedName>
      <fullName>Galanin-like peptide</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">GALP</name>
  </gene>
  <organism>
    <name type="scientific">Sus scrofa</name>
    <name type="common">Pig</name>
    <dbReference type="NCBI Taxonomy" id="9823"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Suina</taxon>
      <taxon>Suidae</taxon>
      <taxon>Sus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="J. Biol. Chem." volume="274" first="37041" last="37045">
      <title>Isolation and cDNA cloning of a novel galanin-like peptide (GALP) from porcine hypothalamus.</title>
      <authorList>
        <person name="Ohtaki T."/>
        <person name="Kumano S."/>
        <person name="Ishibashi Y."/>
        <person name="Ogi K."/>
        <person name="Matsui H."/>
        <person name="Harada M."/>
        <person name="Kitada C."/>
        <person name="Kurokawa T."/>
        <person name="Onda H."/>
        <person name="Fujino M."/>
      </authorList>
      <dbReference type="PubMed" id="10601261"/>
      <dbReference type="DOI" id="10.1074/jbc.274.52.37041"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 23-82</scope>
    <scope>SYNTHESIS OF 23-82</scope>
    <source>
      <tissue>Hypothalamus</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Hypothalamic neuropeptide which binds to the G-protein-coupled galanin receptors (GALR1, GALR2 and GALR3). Involved in a large number of putative physiological functions in CNS homeostatic processes, including the regulation of gonadotropin-releasing hormone secretion (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the galanin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF188490">
    <property type="protein sequence ID" value="AAF19722.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_998990.1">
    <property type="nucleotide sequence ID" value="NM_213825.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9TT95"/>
  <dbReference type="STRING" id="9823.ENSSSCP00000057469"/>
  <dbReference type="PaxDb" id="9823-ENSSSCP00000003598"/>
  <dbReference type="Ensembl" id="ENSSSCT00005073528">
    <property type="protein sequence ID" value="ENSSSCP00005046104"/>
    <property type="gene ID" value="ENSSSCG00005045481"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSSSCT00015027343.1">
    <property type="protein sequence ID" value="ENSSSCP00015010709.1"/>
    <property type="gene ID" value="ENSSSCG00015020587.1"/>
  </dbReference>
  <dbReference type="GeneID" id="396772"/>
  <dbReference type="KEGG" id="ssc:396772"/>
  <dbReference type="CTD" id="85569"/>
  <dbReference type="eggNOG" id="ENOG502TI9H">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="Q9TT95"/>
  <dbReference type="OrthoDB" id="5224030at2759"/>
  <dbReference type="Proteomes" id="UP000008227">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000314985">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694570">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694571">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694720">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694722">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694723">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694724">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694725">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694726">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694727">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694728">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042595">
    <property type="term" value="P:behavioral response to starvation"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008174">
    <property type="entry name" value="Galanin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039244">
    <property type="entry name" value="GALP"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20950:SF1">
    <property type="entry name" value="GALANIN-LIKE PEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20950">
    <property type="entry name" value="GALANIN-RELATED PEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01296">
    <property type="entry name" value="Galanin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00861">
    <property type="entry name" value="GALANIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000010465" description="Galanin-like peptide">
    <location>
      <begin position="23"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000010466">
    <location>
      <begin position="85"/>
      <end position="120"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10601261"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="120" mass="12681" checksum="709F7D8F42149054" modified="2000-05-01" version="1" precursor="true">MALTVPLIVLAVLLSLMESPASAPVHRGRGGWTLNSAGYLLGPVLHPPSRAEGGGKGKTALGILDLWKAIDGLPYPQSQLASKRSLGETFAKPDSGVTFVGVPDVVPWKRIRPGTTRFQI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>