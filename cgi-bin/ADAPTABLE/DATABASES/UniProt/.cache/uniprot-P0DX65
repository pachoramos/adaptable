<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2024-01-24" modified="2024-05-29" version="2" xmlns="http://uniprot.org/uniprot">
  <accession>P0DX65</accession>
  <name>O1202_POLOC</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Occidentalin-1202</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Polybia occidentalis</name>
    <name type="common">Paper wasp</name>
    <dbReference type="NCBI Taxonomy" id="91432"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Polistinae</taxon>
      <taxon>Epiponini</taxon>
      <taxon>Polybia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2023" name="Brain Commun." volume="5" first="fcad016" last="fcad016">
      <title>A new class of peptides from wasp venom: a pathway to antiepileptic/neuroprotective drugs.</title>
      <authorList>
        <person name="Mortari M.R."/>
        <person name="Cunha A.O.S."/>
        <person name="Dos Anjos L.C."/>
        <person name="Amaral H.O."/>
        <person name="Quintanilha M.V.T."/>
        <person name="Gelfuso E.A."/>
        <person name="Homem-de-Mello M."/>
        <person name="de Almeida H."/>
        <person name="Rego S."/>
        <person name="Maigret B."/>
        <person name="Lopes N.P."/>
        <person name="Dos Santos W.F."/>
      </authorList>
      <dbReference type="PubMed" id="36844150"/>
      <dbReference type="DOI" id="10.1093/braincomms/fcad016"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
    <scope>BIOASSAY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>AMIDATION AT MET-9</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Peptide with unknown natural function. Has neuroprotective activity since is shows potent antiseizure effects in acute (kainic acid (KA) and pentylenetetrazole (PTZ)) and chronic (temporal lobe epilepsy model induced by pilocarpine) epileptic models. Protects against the KA-induced seizures after intraperitoneal injection in a dose-dependent manner, indicating that the peptide can cross the blood-brain barrier. Is more efficient in the KA-induced than in the PKZ-induced seizures. Does not adversely affect motor and cognitive behavior. Computational analysis suggest that this peptide can be a potent blocker of kainate receptors (GRIK1-GRIK5), preventing glutamate and kainic acid from binding to the receptors active site. In view of these activities, it can be considered as an interesting drug model for the development of new medicines to treat epilepsy.</text>
  </comment>
  <comment type="subunit">
    <text evidence="3">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by the venom gland.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0099106">
    <property type="term" value="F:ion channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000459171" description="Occidentalin-1202" evidence="1">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="36844150"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="36844150"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3">
    <source>
      <dbReference type="PubMed" id="36844150"/>
    </source>
  </evidence>
  <sequence length="9" mass="1204" checksum="C69279CDC2C68B46" modified="2024-01-24" version="1">EQYMVAFWM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>