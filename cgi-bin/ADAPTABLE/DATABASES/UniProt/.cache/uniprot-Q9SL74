<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-07-28" modified="2024-10-02" version="95" xmlns="http://uniprot.org/uniprot">
  <accession>Q9SL74</accession>
  <accession>A0MJT1</accession>
  <name>DEF78_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 78</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">At2g20070</name>
    <name type="ORF">T2G17.13</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Nature" volume="402" first="761" last="768">
      <title>Sequence and analysis of chromosome 2 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Lin X."/>
        <person name="Kaul S."/>
        <person name="Rounsley S.D."/>
        <person name="Shea T.P."/>
        <person name="Benito M.-I."/>
        <person name="Town C.D."/>
        <person name="Fujii C.Y."/>
        <person name="Mason T.M."/>
        <person name="Bowman C.L."/>
        <person name="Barnstead M.E."/>
        <person name="Feldblyum T.V."/>
        <person name="Buell C.R."/>
        <person name="Ketchum K.A."/>
        <person name="Lee J.J."/>
        <person name="Ronning C.M."/>
        <person name="Koo H.L."/>
        <person name="Moffat K.S."/>
        <person name="Cronin L.A."/>
        <person name="Shen M."/>
        <person name="Pai G."/>
        <person name="Van Aken S."/>
        <person name="Umayam L."/>
        <person name="Tallon L.J."/>
        <person name="Gill J.E."/>
        <person name="Adams M.D."/>
        <person name="Carrera A.J."/>
        <person name="Creasy T.H."/>
        <person name="Goodman H.M."/>
        <person name="Somerville C.R."/>
        <person name="Copenhaver G.P."/>
        <person name="Preuss D."/>
        <person name="Nierman W.C."/>
        <person name="White O."/>
        <person name="Eisen J.A."/>
        <person name="Salzberg S.L."/>
        <person name="Fraser C.M."/>
        <person name="Venter J.C."/>
      </authorList>
      <dbReference type="PubMed" id="10617197"/>
      <dbReference type="DOI" id="10.1038/45471"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2006" name="Plant Biotechnol. J." volume="4" first="317" last="324">
      <title>Simultaneous high-throughput recombinational cloning of open reading frames in closed and open configurations.</title>
      <authorList>
        <person name="Underwood B.A."/>
        <person name="Vanderhaeghen R."/>
        <person name="Whitford R."/>
        <person name="Town C.D."/>
        <person name="Hilson P."/>
      </authorList>
      <dbReference type="PubMed" id="17147637"/>
      <dbReference type="DOI" id="10.1111/j.1467-7652.2006.00183.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2007" name="Plant J." volume="51" first="262" last="280">
      <title>Small cysteine-rich peptides resembling antimicrobial peptides have been under-predicted in plants.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Moskal W.A. Jr."/>
        <person name="Wu H.C."/>
        <person name="Underwood B.A."/>
        <person name="Graham M.A."/>
        <person name="Town C.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="17565583"/>
      <dbReference type="DOI" id="10.1111/j.1365-313x.2007.03136.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <comment type="sequence caution" evidence="3">
    <conflict type="erroneous termination">
      <sequence resource="EMBL-CDS" id="ABK27944" version="1"/>
    </conflict>
    <text>Extended C-terminus.</text>
  </comment>
  <dbReference type="EMBL" id="AC006081">
    <property type="protein sequence ID" value="AAD24389.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002685">
    <property type="protein sequence ID" value="AEC06961.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ446528">
    <property type="protein sequence ID" value="ABE65832.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ912230">
    <property type="protein sequence ID" value="ABK27944.1"/>
    <property type="status" value="ALT_SEQ"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF182808">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="F84584">
    <property type="entry name" value="F84584"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_179597.1">
    <property type="nucleotide sequence ID" value="NM_127565.4"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9SL74"/>
  <dbReference type="SMR" id="Q9SL74"/>
  <dbReference type="PaxDb" id="3702-AT2G20070.1"/>
  <dbReference type="EnsemblPlants" id="AT2G20070.1">
    <property type="protein sequence ID" value="AT2G20070.1"/>
    <property type="gene ID" value="AT2G20070"/>
  </dbReference>
  <dbReference type="GeneID" id="816526"/>
  <dbReference type="Gramene" id="AT2G20070.1">
    <property type="protein sequence ID" value="AT2G20070.1"/>
    <property type="gene ID" value="AT2G20070"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT2G20070"/>
  <dbReference type="Araport" id="AT2G20070"/>
  <dbReference type="TAIR" id="AT2G20070"/>
  <dbReference type="HOGENOM" id="CLU_2609304_0_0_1"/>
  <dbReference type="InParanoid" id="Q9SL74"/>
  <dbReference type="OMA" id="NTCCCKK"/>
  <dbReference type="OrthoDB" id="637705at2759"/>
  <dbReference type="PhylomeDB" id="Q9SL74"/>
  <dbReference type="PRO" id="PR:Q9SL74"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q9SL74">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000379649" description="Defensin-like protein 78">
    <location>
      <begin position="31"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="40"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="52"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="56"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="89" mass="9312" checksum="8B5D3D4EF7D1A581" modified="2000-05-01" version="1" precursor="true">MANNMVASPYKNTFMMIALVLILLISGSEARSNGIYDPMCPGVCGNLVKPDCNGLCHELGFPAGGYCKPGNTCCCKKKDSGPVDVPPTA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>