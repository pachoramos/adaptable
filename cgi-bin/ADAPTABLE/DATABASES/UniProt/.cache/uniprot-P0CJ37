<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-07-27" modified="2024-07-24" version="22" xmlns="http://uniprot.org/uniprot">
  <accession>P0CJ37</accession>
  <name>EUMEF_EUMFR</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Eumenitin-F</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Eumenes fraterculus</name>
    <name type="common">Solitary wasp</name>
    <dbReference type="NCBI Taxonomy" id="1035771"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Eumeninae</taxon>
      <taxon>Eumenes</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2011" name="Toxicon" volume="57" first="1081" last="1092">
      <title>Chemical and biological characterization of four new linear cationic alpha-helical peptides from the venoms of two solitary eumenine wasps.</title>
      <authorList>
        <person name="Rangel M."/>
        <person name="Dos Santos Cabrera M.P."/>
        <person name="Kazuma K."/>
        <person name="Ando K."/>
        <person name="Wang X."/>
        <person name="Kato M."/>
        <person name="Nihei K.I."/>
        <person name="Hirata I.Y."/>
        <person name="Cross T.J."/>
        <person name="Garcia A.N."/>
        <person name="Faquim-Mauro E.L."/>
        <person name="Franzolin M.R."/>
        <person name="Fuchino H."/>
        <person name="Mori-Yasumoto K."/>
        <person name="Sekita S."/>
        <person name="Kadowaki M."/>
        <person name="Satake M."/>
        <person name="Konno K."/>
      </authorList>
      <dbReference type="PubMed" id="21549739"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2011.04.014"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SYNTHESIS</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>CIRCULAR DICHROISM</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2016" name="Toxins" volume="8" first="114" last="114">
      <title>Peptide toxins in solitary wasp venoms.</title>
      <authorList>
        <person name="Konno K."/>
        <person name="Kazuma K."/>
        <person name="Nihei K."/>
      </authorList>
      <dbReference type="PubMed" id="27096870"/>
      <dbReference type="DOI" id="10.3390/toxins8040114"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3">Linear cationic alpha-helical peptide with antimicrobial activities against both Gram-positive and Gram-negative strains and against the yeast C.albicans. Shows moderate mast cell degranulation and leishmanicidal activities. Has a very low hemolytic activity. Induces an ion channel-like incorporation in artificial lipid bilayers, forming pores with relativeley high conductances (PubMed:21549739). Its mast cell degranulation activity may be related to the activation of G-protein coupled receptors in mast cells as well as interaction with other proteins located in cell endosomal membranes in the mast cells (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="3">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="6">Assumes an amphipathic alpha-helical conformation in a lipid environment (Probable). Forms pores in membranes (Probable).</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the MCD family. Eumenitin subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043303">
    <property type="term" value="P:mast cell degranulation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0467">Mast cell degranulation</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <feature type="peptide" id="PRO_0000411088" description="Eumenitin-F" evidence="3">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01514"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P84914"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="21549739"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="27096870"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="21549739"/>
    </source>
  </evidence>
  <sequence length="15" mass="1645" checksum="50270CF644E26B4F" modified="2011-07-27" version="1">LNLKGLFKKVASLLT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>