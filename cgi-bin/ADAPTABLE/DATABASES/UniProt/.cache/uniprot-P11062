<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1989-07-01" modified="2023-02-22" version="87" xmlns="http://uniprot.org/uniprot">
  <accession>P11062</accession>
  <name>T4G1F_AGEAP</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Mu-agatoxin-Aa1f</fullName>
      <shortName evidence="5">Mu-AGTX-Aa1f</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Mu-agatoxin VI</fullName>
      <shortName evidence="4">Mu-Aga VI</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="5">Mu-agatoxin-6</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Agelenopsis aperta</name>
    <name type="common">North American funnel-web spider</name>
    <name type="synonym">Agelenopsis gertschi</name>
    <dbReference type="NCBI Taxonomy" id="6908"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Agelenidae</taxon>
      <taxon>Agelenopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1989" name="J. Biol. Chem." volume="264" first="2150" last="2155">
      <title>Purification and characterization of two classes of neurotoxins from the funnel web spider, Agelenopsis aperta.</title>
      <authorList>
        <person name="Skinner W.S."/>
        <person name="Adams M.E."/>
        <person name="Quistad G.B."/>
        <person name="Kataoka H."/>
        <person name="Cesarin B.J."/>
        <person name="Enderlin F.E."/>
        <person name="Schooley D.A."/>
      </authorList>
      <dbReference type="PubMed" id="2914898"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)94154-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TOXIC DOSE</scope>
    <scope>PROBABLE AMIDATION AT ASN-37</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Toxicon" volume="43" first="509" last="525">
      <title>Agatoxins: ion channel specific toxins from the American funnel web spider, Agelenopsis aperta.</title>
      <authorList>
        <person name="Adams M.E."/>
      </authorList>
      <dbReference type="PubMed" id="15066410"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2004.02.004"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Insecticidal neurotoxin that induces an irreversible spastic paralysis when injected into insects. Modifies presynaptic voltage-gated sodium channels (Nav), causing them to open at the normal resting potential of the nerve. This leads to spontaneous release of neurotransmitter and repetitive action potentials in motor neurons.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">LD(50) is 38 +-12 mg/kg into third stadium larvae of M.sexta.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the neurotoxin 07 (Beta/delta-agtx) family. 03 (aga-4) subfamily. Aga sub-subfamily.</text>
  </comment>
  <dbReference type="PIR" id="F32038">
    <property type="entry name" value="F32038"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P11062"/>
  <dbReference type="SMR" id="P11062"/>
  <dbReference type="ArachnoServer" id="AS000385">
    <property type="toxin name" value="mu-agatoxin-Aa1f"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044231">
    <property type="term" value="C:host cell presynaptic membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016328">
    <property type="entry name" value="Beta/delta-agatoxin_fam"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05980">
    <property type="entry name" value="Toxin_7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001882">
    <property type="entry name" value="Curtatoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57059">
    <property type="entry name" value="omega toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60015">
    <property type="entry name" value="MU_AGATOXIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0638">Presynaptic neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="peptide" id="PRO_0000044960" description="Mu-agatoxin-Aa1f" evidence="3">
    <location>
      <begin position="1"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="modified residue" description="Asparagine amide" evidence="6">
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="2"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="9"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="17"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="25"/>
      <end position="31"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P11061"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="2914898"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="2914898"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="2914898"/>
    </source>
  </evidence>
  <sequence length="37" mass="4168" checksum="B2AC03A74B5ED028" modified="1989-07-01" version="1">DCVGESQQCADWAGPHCCDGYYCTCRYFPKCICVNNN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>