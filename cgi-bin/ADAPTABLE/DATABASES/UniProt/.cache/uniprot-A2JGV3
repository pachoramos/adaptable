<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-02-22" modified="2024-10-02" version="42" xmlns="http://uniprot.org/uniprot">
  <accession>A2JGV3</accession>
  <name>TARP_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="9">T-cell receptor gamma alternate reading frame protein</fullName>
      <shortName evidence="9">TARP</shortName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="12" type="primary">TRGC1</name>
  </gene>
  <organism evidence="11">
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference evidence="11" key="1">
    <citation type="journal article" date="1999" name="Proc. Natl. Acad. Sci. U.S.A." volume="96" first="9287" last="9292">
      <title>High expression of a specific T-cell receptor gamma transcript in epithelial cells of the prostate.</title>
      <authorList>
        <person name="Essand M."/>
        <person name="Vasmatzis G."/>
        <person name="Brinkmann U."/>
        <person name="Duray P."/>
        <person name="Lee B."/>
        <person name="Pastan I."/>
      </authorList>
      <dbReference type="PubMed" id="10430935"/>
      <dbReference type="DOI" id="10.1073/pnas.96.16.9287"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference evidence="11" key="2">
    <citation type="journal article" date="2000" name="Proc. Natl. Acad. Sci. U.S.A." volume="97" first="9437" last="9442">
      <title>TARP: a nuclear protein expressed in prostate and breast cancer cells derived from an alternate reading frame of the T cell receptor gamma chain locus.</title>
      <authorList>
        <person name="Wolfgang C.D."/>
        <person name="Essand M."/>
        <person name="Vincent J.J."/>
        <person name="Lee B."/>
        <person name="Pastan I."/>
      </authorList>
      <dbReference type="PubMed" id="10931945"/>
      <dbReference type="DOI" id="10.1073/pnas.160270597"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference evidence="10" key="3">
    <citation type="journal article" date="2001" name="Cancer Res." volume="61" first="8122" last="8126">
      <title>T-cell receptor gamma chain alternate reading frame protein (TARP) expression in prostate cancer cells leads to an increased growth rate and induction of caveolins and amphiregulin.</title>
      <authorList>
        <person name="Wolfgang C.D."/>
        <person name="Essand M."/>
        <person name="Lee B."/>
        <person name="Pastan I."/>
      </authorList>
      <dbReference type="PubMed" id="11719440"/>
    </citation>
    <scope>INDUCTION</scope>
    <scope>ROLE IN PROSTATE CANCER</scope>
  </reference>
  <reference evidence="10" key="4">
    <citation type="journal article" date="2003" name="Endocrinology" volume="144" first="3433" last="3440">
      <title>Characterization of the androgen-regulated prostate-specific T cell receptor gamma-chain alternate reading frame protein (TARP) promoter.</title>
      <authorList>
        <person name="Cheng W.S."/>
        <person name="Giandomenico V."/>
        <person name="Pastan I."/>
        <person name="Essand M."/>
      </authorList>
      <dbReference type="PubMed" id="12865322"/>
      <dbReference type="DOI" id="10.1210/en.2003-0121"/>
    </citation>
    <scope>INDUCTION</scope>
  </reference>
  <reference evidence="10" key="5">
    <citation type="journal article" date="2004" name="J. Biol. Chem." volume="279" first="24561" last="24568">
      <title>The T cell receptor gamma chain alternate reading frame protein (TARP), a prostate-specific protein localized in mitochondria.</title>
      <authorList>
        <person name="Maeda H."/>
        <person name="Nagata S."/>
        <person name="Wolfgang C.D."/>
        <person name="Bratthauer G.L."/>
        <person name="Bera T.K."/>
        <person name="Pastan I."/>
      </authorList>
      <dbReference type="PubMed" id="15150260"/>
      <dbReference type="DOI" id="10.1074/jbc.m402492200"/>
    </citation>
    <scope>INDUCTION</scope>
    <scope>LOCALIZATION IN PROSTATE CANCER CELLS</scope>
  </reference>
  <reference evidence="10" key="6">
    <citation type="journal article" date="2013" name="Chin. Med. J." volume="126" first="4260" last="4264">
      <title>Value of T cell receptor gamma alternate reading frame protein and keratin 5 in endometrial carcinoma.</title>
      <authorList>
        <person name="Zhao L.J."/>
        <person name="Li X.P."/>
        <person name="Qi W.J."/>
        <person name="Wang J.L."/>
        <person name="Wei L.H."/>
      </authorList>
      <dbReference type="PubMed" id="24238509"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>EXPRESSION IN ENDOMETRIAL CARCINOMA</scope>
  </reference>
  <reference evidence="10" key="7">
    <citation type="journal article" date="2017" name="Oral Surg. Oral Med. Oral Pathol. Oral Radiol." volume="123" first="468" last="476">
      <title>Elevated TARP promotes proliferation and metastasis of salivary adenoid cystic carcinoma.</title>
      <authorList>
        <person name="Yue H."/>
        <person name="Cai Y."/>
        <person name="Song Y."/>
        <person name="Meng L."/>
        <person name="Chen X."/>
        <person name="Wang M."/>
        <person name="Bian Z."/>
        <person name="Wang R."/>
      </authorList>
      <dbReference type="PubMed" id="28153567"/>
      <dbReference type="DOI" id="10.1016/j.oooo.2016.11.023"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>ROLE IN SALIVARY GLAND CANCER CELLS</scope>
  </reference>
  <reference evidence="10" key="8">
    <citation type="journal article" date="2020" name="Haematologica" volume="105" first="1306" last="1316">
      <title>TARP is an immunotherapeutic target in acute myeloid leukemia expressed in the leukemic stem cell compartment.</title>
      <authorList>
        <person name="Depreter B."/>
        <person name="Weening K.E."/>
        <person name="Vandepoele K."/>
        <person name="Essand M."/>
        <person name="De Moerloose B."/>
        <person name="Themeli M."/>
        <person name="Cloos J."/>
        <person name="Hanekamp D."/>
        <person name="Moors I."/>
        <person name="D'hont I."/>
        <person name="Denys B."/>
        <person name="Uyttebroeck A."/>
        <person name="Van Damme A."/>
        <person name="Dedeken L."/>
        <person name="Snauwaert S."/>
        <person name="Goetgeluk G."/>
        <person name="De Munter S."/>
        <person name="Kerre T."/>
        <person name="Vandekerckhove B."/>
        <person name="Lammens T."/>
        <person name="Philippe J."/>
      </authorList>
      <dbReference type="PubMed" id="31371409"/>
      <dbReference type="DOI" id="10.3324/haematol.2019.222612"/>
    </citation>
    <scope>LOCALIZATION IN ACUTE MYELOID LEUKEMIA CELLS</scope>
  </reference>
  <comment type="tissue specificity">
    <text evidence="1 2 6 7">Detected at low levels in the ductal cells of the salivary gland but not in the acinar cells (at protein level) (PubMed:28153567). Expressed in endometrium (at protein level) (PubMed:24238509). Expressed in epithelial cells within the acinar ducts of the prostate (PubMed:10430935, PubMed:10931945).</text>
  </comment>
  <comment type="induction">
    <text evidence="3 4 5">By testosterone.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1 2">Encoded in an alternative reading frame of the T-cell receptor gamma chain gene which is composed of variable (Vgamma), joining (Jgamma), and constant (Cgamma) gene segments. Originates within an intron directly upstream of the Jgamma1.2 gene segment and contains three exons from the Cgamma1 segment.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2 3 5 6 7 8">Expressed in prostate cancer and breast cancer cells with little or no expression detected in normal breast tissue (PubMed:10931945). In prostate cancer cells, increases cell growth rate and changes expression levels of a number of genes including up-regulation of CAV1, CAV2, AREG and CXCL1/GRO1 and down-regulation of IL1B (PubMed:11719440). Localizes to the mitochondrion outer membrane in prostate cancer cells (PubMed:15150260). In acute myeloid leukemia cells, shows perinuclear staining and some localization at the endoplasmic reticulum (PubMed:31371409). Expressed at significantly higher levels in primary salivary adenoid cystic carcinomas (SACC) compared with adjacent non-cancerous tissues and promotes the proliferation, migration and invasion of SACC cells (PubMed:28153567). Expressed at higher levels in endometrial carcinomas than in normal endometrial tissue (PubMed:24238509).</text>
  </comment>
  <dbReference type="EMBL" id="AF151103">
    <property type="protein sequence ID" value="AAG29337.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS94087.1"/>
  <dbReference type="RefSeq" id="NP_001003799.1">
    <property type="nucleotide sequence ID" value="NM_001003799.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001003806.1">
    <property type="nucleotide sequence ID" value="NM_001003806.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_016867718.1">
    <property type="nucleotide sequence ID" value="XM_017012229.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_016867719.1">
    <property type="nucleotide sequence ID" value="XM_017012230.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A2JGV3"/>
  <dbReference type="SMR" id="A2JGV3"/>
  <dbReference type="PRIDE" id="A2JGV3"/>
  <dbReference type="ABCD" id="A2JGV3">
    <property type="antibodies" value="1 sequenced antibody"/>
  </dbReference>
  <dbReference type="DNASU" id="445347"/>
  <dbReference type="GeneID" id="445347"/>
  <dbReference type="KEGG" id="hsa:445347"/>
  <dbReference type="MANE-Select" id="ENST00000698248.1">
    <property type="protein sequence ID" value="ENSP00000513628.1"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_001003799.2"/>
    <property type="RefSeq protein sequence ID" value="NP_001003799.1"/>
  </dbReference>
  <dbReference type="CTD" id="445347"/>
  <dbReference type="DisGeNET" id="445347"/>
  <dbReference type="GeneCards" id="TRGC1"/>
  <dbReference type="HGNC" id="HGNC:12275">
    <property type="gene designation" value="TRGC1"/>
  </dbReference>
  <dbReference type="MIM" id="609642">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00950000185265"/>
  <dbReference type="OrthoDB" id="3972376at2759"/>
  <dbReference type="BioGRID-ORCS" id="445347">
    <property type="hits" value="10 hits in 217 CRISPR screens"/>
  </dbReference>
  <dbReference type="GenomeRNAi" id="445347"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 7"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR054133">
    <property type="entry name" value="TARP"/>
  </dbReference>
  <dbReference type="Pfam" id="PF21951">
    <property type="entry name" value="TARP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000457484" description="T-cell receptor gamma alternate reading frame protein">
    <location>
      <begin position="1"/>
      <end position="58"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10430935"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10931945"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="11719440"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="12865322"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="15150260"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="24238509"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="28153567"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="31371409"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="10931945"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000312" key="11">
    <source>
      <dbReference type="EMBL" id="AAG29337.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="12">
    <source>
      <dbReference type="HGNC" id="HGNC:12275"/>
    </source>
  </evidence>
  <sequence length="58" mass="7162" checksum="2E9301BDC810EA8F" modified="2008-09-23" version="1">MQMFPPSPLFFFLQLLKQSSRRLEHTFVFLRNFSLMLLRYIGKKRRATRFWDPRRGTP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>