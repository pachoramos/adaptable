<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-01-23" modified="2022-10-12" version="49" xmlns="http://uniprot.org/uniprot">
  <accession>P82389</accession>
  <accession>Q5K0E6</accession>
  <name>AUR22_RANAE</name>
  <protein>
    <recommendedName>
      <fullName evidence="9">Aurein-2.2</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="9">Aurein-2.2.1</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Ranoidea aurea</name>
    <name type="common">Green and golden bell frog</name>
    <name type="synonym">Litoria aurea</name>
    <dbReference type="NCBI Taxonomy" id="8371"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Regul. Pept." volume="128" first="75" last="83">
      <title>The structural organization of aurein precursor cDNAs from the skin secretion of the Australian green and golden bell frog, Litoria aurea.</title>
      <authorList>
        <person name="Chen T."/>
        <person name="Xue Y."/>
        <person name="Zhou M."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="15721491"/>
      <dbReference type="DOI" id="10.1016/j.regpep.2004.12.022"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 50-65</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2000" name="Eur. J. Biochem." volume="267" first="5330" last="5341">
      <title>The antibiotic and anticancer active aurein peptides from the australian bell frogs Litoria aurea and Litoria raniformis the solution structure of aurein 1.2.</title>
      <authorList>
        <person name="Rozek T."/>
        <person name="Wegener K.L."/>
        <person name="Bowie J.H."/>
        <person name="Olver I.N."/>
        <person name="Carver J.A."/>
        <person name="Wallace J.C."/>
        <person name="Tyler M.J."/>
      </authorList>
      <dbReference type="PubMed" id="10951191"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.2000.01536.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 50-65</scope>
    <scope>FUNCTION</scope>
    <scope>AMIDATION AT LEU-65</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Eur. J. Biochem." volume="269" first="100" last="109">
      <title>Amphibian peptides that inhibit neuronal nitric oxide synthase. Isolation of lesuerin from the skin secretion of the Australian stony creek frog Litoria lesueuri.</title>
      <authorList>
        <person name="Doyle J."/>
        <person name="Llewellyn L.E."/>
        <person name="Brinkworth C.S."/>
        <person name="Bowie J.H."/>
        <person name="Wegener K.L."/>
        <person name="Rozek T."/>
        <person name="Wabnitz P.A."/>
        <person name="Wallace J.C."/>
        <person name="Tyler M.J."/>
      </authorList>
      <dbReference type="PubMed" id="11784303"/>
      <dbReference type="DOI" id="10.1046/j.0014-2956.2002.02630.x"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2007" name="Biophys. J." volume="92" first="2854" last="2864">
      <title>Characterization of the structure and membrane interaction of the antimicrobial peptides aurein 2.2 and 2.3 from Australian southern bell frogs.</title>
      <authorList>
        <person name="Pan Y.L."/>
        <person name="Cheng J.T."/>
        <person name="Hale J."/>
        <person name="Pan J."/>
        <person name="Hancock R.E."/>
        <person name="Straus S.K."/>
      </authorList>
      <dbReference type="PubMed" id="17259271"/>
      <dbReference type="DOI" id="10.1529/biophysj.106.097238"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 5 6 8">Amphipathic alpha-helical antimicrobial peptide with weak to moderate activity against Gram-positive bacteria, and no activity against Gram-negative bacteria (PubMed:10951191, PubMed:17259271). Probably acts by disturbing membrane functions with its amphipathic structure (PubMed:10951191). Strongly inhibits the formation of NO by neuronal nitric oxide synthase (nNOS) at micromolar concentrations (PubMed:11784303). Acts by a non-competitive mechanism, probably by binding to calcium/calmodulin and as a consequence blocking calmodulin attachment to nNOS (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5 7">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="8">Target cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="11 12">Expressed by the skin dorsal glands.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">Amidation is essential for antibacterial activity against Gram-positive bacteria.</text>
  </comment>
  <comment type="mass spectrometry" mass="1615.76" method="MALDI" evidence="7"/>
  <comment type="similarity">
    <text evidence="10">Belongs to the frog skin active peptide (FSAP) family. Aurein subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=00015"/>
  </comment>
  <dbReference type="EMBL" id="AJ850127">
    <property type="protein sequence ID" value="CAH61711.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P82389"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016322">
    <property type="entry name" value="FSAP"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001822">
    <property type="entry name" value="Dermaseptin_precursor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000450287" evidence="11">
    <location>
      <begin position="23"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000010147" description="Aurein-2.2" evidence="5 7">
    <location>
      <begin position="50"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000010148" description="Aurein-2.2.1" evidence="5">
    <location>
      <begin position="52"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000450288" evidence="11">
    <location>
      <begin position="69"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="4">
    <location>
      <begin position="27"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="5">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P81835"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P82390"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000256" key="4">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="10951191"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="11784303"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="15721491"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="17259271"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="10951191"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="10951191"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="12">
    <source>
      <dbReference type="PubMed" id="15721491"/>
    </source>
  </evidence>
  <sequence length="72" mass="8095" checksum="4E3B2B101A566A54" modified="2020-06-17" version="2" precursor="true">MAFLKKSLFLVLFLGLVSLSICEKEKRQNEEDEDENEAANHEEGSEEKRGLFDIVKKVVGALGSLGKRNDLE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>