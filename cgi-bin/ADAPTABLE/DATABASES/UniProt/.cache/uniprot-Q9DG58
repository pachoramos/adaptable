<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-08-30" modified="2023-11-08" version="99" xmlns="http://uniprot.org/uniprot">
  <accession>Q9DG58</accession>
  <accession>Q0PWH2</accession>
  <accession>Q6GXJ2</accession>
  <name>GLL3_CHICK</name>
  <protein>
    <recommendedName>
      <fullName>Gallinacin-3</fullName>
      <shortName>Gal-3</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Beta-defensin 3</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">GAL3</name>
  </gene>
  <organism>
    <name type="scientific">Gallus gallus</name>
    <name type="common">Chicken</name>
    <dbReference type="NCBI Taxonomy" id="9031"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Archelosauria</taxon>
      <taxon>Archosauria</taxon>
      <taxon>Dinosauria</taxon>
      <taxon>Saurischia</taxon>
      <taxon>Theropoda</taxon>
      <taxon>Coelurosauria</taxon>
      <taxon>Aves</taxon>
      <taxon>Neognathae</taxon>
      <taxon>Galloanserae</taxon>
      <taxon>Galliformes</taxon>
      <taxon>Phasianidae</taxon>
      <taxon>Phasianinae</taxon>
      <taxon>Gallus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Infect. Immun." volume="69" first="2684" last="2691">
      <title>Gallinacin-3, an inducible epithelial beta-defensin in the chicken.</title>
      <authorList>
        <person name="Zhao C."/>
        <person name="Nguyen T."/>
        <person name="Liu L."/>
        <person name="Sacco R.E."/>
        <person name="Brogden K.A."/>
        <person name="Lehrer R.I."/>
      </authorList>
      <dbReference type="PubMed" id="11254635"/>
      <dbReference type="DOI" id="10.1128/iai.69.4.2684-2691.2001"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Trachea</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="BMC Genomics" volume="5" first="56" last="56">
      <title>A genome-wide screen identifies a single beta-defensin gene cluster in the chicken: implications for the origin and evolution of mammalian defensins.</title>
      <authorList>
        <person name="Xiao Y."/>
        <person name="Hughes A.L."/>
        <person name="Ando J."/>
        <person name="Matsuda Y."/>
        <person name="Cheng J.-F."/>
        <person name="Skinner-Noble D."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="15310403"/>
      <dbReference type="DOI" id="10.1186/1471-2164-5-56"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2004-07" db="EMBL/GenBank/DDBJ databases">
      <title>Chicken defensin gene structure.</title>
      <authorList>
        <person name="Zhang H."/>
        <person name="Bi Y."/>
        <person name="Cao Y."/>
        <person name="Chen X."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="submission" date="2006-07" db="EMBL/GenBank/DDBJ databases">
      <title>Chicken beta-defensin in China chicken breeds.</title>
      <authorList>
        <person name="Chen Y."/>
        <person name="Cao Y."/>
        <person name="Xie Q."/>
        <person name="Bi Y."/>
        <person name="Chen J."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] OF 9-80</scope>
    <source>
      <strain>Guangxi Huang</strain>
      <strain>Huiyang bearded</strain>
      <strain>Qingyuan Ma</strain>
      <strain>Taihe silkies</strain>
      <strain>Xinghua</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Immunogenetics" volume="56" first="170" last="177">
      <title>Bioinformatic discovery and initial characterisation of nine novel antimicrobial peptide genes in the chicken.</title>
      <authorList>
        <person name="Lynn D.J."/>
        <person name="Higgs R."/>
        <person name="Gaines S."/>
        <person name="Tierney J."/>
        <person name="James T."/>
        <person name="Lloyd A.T."/>
        <person name="Fares M.A."/>
        <person name="Mulcahy G."/>
        <person name="O'Farrelly C."/>
      </authorList>
      <dbReference type="PubMed" id="15148642"/>
      <dbReference type="DOI" id="10.1007/s00251-004-0675-0"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2006" name="J. Reprod. Dev." volume="52" first="211" last="218">
      <title>Effects of age, egg-laying activity, and Salmonella-inoculation on the expressions of gallinacin mRNA in the vagina of the hen oviduct.</title>
      <authorList>
        <person name="Yoshimura Y."/>
        <person name="Ohashi H."/>
        <person name="Subedi K."/>
        <person name="Nishibori M."/>
        <person name="Isobe N."/>
      </authorList>
      <dbReference type="PubMed" id="16394622"/>
      <dbReference type="DOI" id="10.1262/jrd.17070"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2007" name="Reproduction" volume="133" first="127" last="133">
      <title>Changes in the expression of gallinacins, antimicrobial peptides, in ovarian follicles during follicular growth and in response to lipopolysaccharide in laying hens (Gallus domesticus).</title>
      <authorList>
        <person name="Subedi K."/>
        <person name="Isobe N."/>
        <person name="Nishibori M."/>
        <person name="Yoshimura Y."/>
      </authorList>
      <dbReference type="PubMed" id="17244739"/>
      <dbReference type="DOI" id="10.1530/rep-06-0083"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Has bactericidal activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location>Cytoplasmic granule</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3 4 5 6">Strongly expressed in the tongue, bone marrow, bursa of Fabricius and trachea. Also expressed in skin, esophagus and air sacs. Weak expression in the large intestine, kidney and ovary. Expressed in the vagina and ovarian stroma, but not in the ovarian follicle.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="6">In the vagina expression is higher in laying hens than in non-laying hens, and is higher in older laying hens than in young laying hens.</text>
  </comment>
  <comment type="induction">
    <text evidence="5">Expression in cultured vaginal cells is increased by LPS and S.enteritidis.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF181952">
    <property type="protein sequence ID" value="AAG09212.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY621318">
    <property type="protein sequence ID" value="AAT48927.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY677340">
    <property type="protein sequence ID" value="AAT77774.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ677634">
    <property type="protein sequence ID" value="ABG73368.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858300">
    <property type="protein sequence ID" value="ABI48216.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858313">
    <property type="protein sequence ID" value="ABI48229.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858326">
    <property type="protein sequence ID" value="ABI48242.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858340">
    <property type="protein sequence ID" value="ABI48256.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_989981.1">
    <property type="nucleotide sequence ID" value="NM_204650.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9DG58"/>
  <dbReference type="SMR" id="Q9DG58"/>
  <dbReference type="STRING" id="9031.ENSGALP00000026851"/>
  <dbReference type="PaxDb" id="9031-ENSGALP00000026851"/>
  <dbReference type="GeneID" id="395363"/>
  <dbReference type="KEGG" id="gga:395363"/>
  <dbReference type="CTD" id="395363"/>
  <dbReference type="VEuPathDB" id="HostDB:geneid_395363"/>
  <dbReference type="HOGENOM" id="CLU_2589016_0_0_1"/>
  <dbReference type="InParanoid" id="Q9DG58"/>
  <dbReference type="PRO" id="PR:Q9DG58"/>
  <dbReference type="Proteomes" id="UP000000539">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042056">
    <property type="term" value="F:chemoattractant activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060326">
    <property type="term" value="P:cell chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515:SF0">
    <property type="entry name" value="BETA-DEFENSIN 103"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000007014" evidence="2">
    <location>
      <begin position="21"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000007015" description="Gallinacin-3">
    <location>
      <begin position="23"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="25"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AAT48927." evidence="7" ref="2">
    <original>RIV</original>
    <variation>KIL</variation>
    <location>
      <begin position="2"/>
      <end position="4"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="11254635"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="15148642"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="16394622"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="17244739"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="80" mass="8746" checksum="496BBC6BFB3F5C3F" modified="2001-03-01" version="1" precursor="true">MRIVYLLIPFFLLFLQGAAGTATQCRIRGGFCRVGSCRFPHIAIGKCATFISCCGRAYEVDALNSVRTSPWLLAPGNNPH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>