<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1991-02-01" modified="2023-06-28" version="75" xmlns="http://uniprot.org/uniprot">
  <accession>P19434</accession>
  <name>YGL1_STRVR</name>
  <protein>
    <recommendedName>
      <fullName>Uncharacterized protein in glnII region</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>ORF1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Streptomyces viridochromogenes</name>
    <dbReference type="NCBI Taxonomy" id="1938"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Kitasatosporales</taxon>
      <taxon>Streptomycetaceae</taxon>
      <taxon>Streptomyces</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="J. Bacteriol." volume="172" first="5326" last="5334">
      <title>Overexpression of a Streptomyces viridochromogenes gene (glnII) encoding a glutamine synthetase similar to those of eucaryotes confers resistance against the antibiotic phosphinothricyl-alanyl-alanine.</title>
      <authorList>
        <person name="Behrmann I."/>
        <person name="Hillemann D."/>
        <person name="Puehler A."/>
        <person name="Strauch E."/>
        <person name="Wohlleben W."/>
      </authorList>
      <dbReference type="PubMed" id="1975583"/>
      <dbReference type="DOI" id="10.1128/jb.172.9.5326-5334.1990"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>ES2</strain>
    </source>
  </reference>
  <comment type="similarity">
    <text evidence="2">Belongs to the ArsC family.</text>
  </comment>
  <dbReference type="EMBL" id="X52842">
    <property type="protein sequence ID" value="CAA37026.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A36724">
    <property type="entry name" value="A36724"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P19434"/>
  <dbReference type="SMR" id="P19434"/>
  <dbReference type="GO" id="GO:0008794">
    <property type="term" value="F:arsenate reductase (glutaredoxin) activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd03034">
    <property type="entry name" value="ArsC_ArsC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.30.10">
    <property type="entry name" value="Glutaredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006659">
    <property type="entry name" value="Arsenate_reductase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006660">
    <property type="entry name" value="Arsenate_reductase-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036249">
    <property type="entry name" value="Thioredoxin-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR30041">
    <property type="entry name" value="ARSENATE REDUCTASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR30041:SF4">
    <property type="entry name" value="ARSENATE REDUCTASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03960">
    <property type="entry name" value="ArsC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF52833">
    <property type="entry name" value="Thioredoxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51353">
    <property type="entry name" value="ARSC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-0676">Redox-active center</keyword>
  <feature type="chain" id="PRO_0000162587" description="Uncharacterized protein in glnII region">
    <location>
      <begin position="1"/>
      <end position="119"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Redox-active" evidence="1">
    <location>
      <begin position="9"/>
      <end position="12"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01282"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="119" mass="13473" checksum="721B66917EA2A040" modified="1991-02-01" version="1">MEIWINPACSKCRSAVQLLDAEGADYTVRRYLEDVPSEDEIRQVLDRLGLEPWDITRTQEAEAKELGVKEWARDASARDQWIKALAEHPKLIQRPIITADDGTAVVGRTDEAVRDALSR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>