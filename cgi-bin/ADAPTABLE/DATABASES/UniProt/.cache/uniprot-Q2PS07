<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-10-16" modified="2024-05-29" version="52" xmlns="http://uniprot.org/uniprot">
  <accession>Q2PS07</accession>
  <name>2SS_FAGES</name>
  <protein>
    <recommendedName>
      <fullName evidence="13">2S seed storage albumin protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="8 11 12">16 kDa buckwheat protein</fullName>
      <shortName evidence="9 10 11 12">BWp16</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="13">2S albumin</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="13">2S seed storage protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="9 10 11 12">Buckwheat 16 kDa major allergen</fullName>
    </alternativeName>
    <allergenName evidence="13">Fag e 2.0101</allergenName>
  </protein>
  <organism evidence="18">
    <name type="scientific">Fagopyrum esculentum</name>
    <name type="common">Common buckwheat</name>
    <name type="synonym">Polygonum fagopyrum</name>
    <dbReference type="NCBI Taxonomy" id="3617"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>Caryophyllales</taxon>
      <taxon>Polygonaceae</taxon>
      <taxon>Polygonoideae</taxon>
      <taxon>Fagopyreae</taxon>
      <taxon>Fagopyrum</taxon>
    </lineage>
  </organism>
  <reference evidence="18" key="1">
    <citation type="journal article" date="2006" name="Int. Arch. Allergy Immunol." volume="140" first="73" last="81">
      <title>Molecular cloning of cDNA, recombinant protein expression and characterization of a buckwheat 16-kDa major allergen.</title>
      <authorList>
        <person name="Koyano S."/>
        <person name="Takagi K."/>
        <person name="Teshima R."/>
        <person name="Sawada J."/>
      </authorList>
      <dbReference type="PubMed" id="16549935"/>
      <dbReference type="DOI" id="10.1159/000092038"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>ALLERGEN</scope>
    <source>
      <tissue evidence="9">Seed</tissue>
    </source>
  </reference>
  <reference evidence="19" key="2">
    <citation type="submission" date="2008-10" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning and prokaryotic expression vector construction of common buckwheat 16 kDa major allergen gene.</title>
      <authorList>
        <person name="Wang X."/>
        <person name="Wu Y."/>
        <person name="Liu Z."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Int. Arch. Allergy Immunol." volume="129" first="49" last="56">
      <title>Pepsin-resistant 16-kD buckwheat protein is associated with immediate hypersensitivity reaction in patients with buckwheat allergy.</title>
      <authorList>
        <person name="Tanaka K."/>
        <person name="Matsumoto K."/>
        <person name="Akasawa A."/>
        <person name="Nakajima T."/>
        <person name="Nagasu T."/>
        <person name="Iikura Y."/>
        <person name="Saito H."/>
      </authorList>
      <dbReference type="PubMed" id="12372998"/>
      <dbReference type="DOI" id="10.1159/000065173"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 23-37</scope>
    <scope>ALLERGEN</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2008" name="Biol. Pharm. Bull." volume="31" first="1079" last="1085">
      <title>Immunological characterization and mutational analysis of the recombinant protein BWp16, a major allergen in buckwheat.</title>
      <authorList>
        <person name="Satoh R."/>
        <person name="Koyano S."/>
        <person name="Takagi K."/>
        <person name="Nakamura R."/>
        <person name="Teshima R."/>
        <person name="Sawada J."/>
      </authorList>
      <dbReference type="PubMed" id="18520034"/>
      <dbReference type="DOI" id="10.1248/bpb.31.1079"/>
    </citation>
    <scope>ALLERGEN</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>BIOTECHNOLOGY</scope>
    <scope>MUTAGENESIS OF CYS-38; CYS-52; CYS-77; CYS-83; CYS-87; CYS-88; CYS-98; CYS-100; CYS-133 AND CYS-140</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2009" name="Acta Crystallogr. F" volume="65" first="1267" last="1270">
      <title>Purification, crystallization and preliminary X-ray analysis of a deletion mutant of a major buckwheat allergen.</title>
      <authorList>
        <person name="Kezuka Y."/>
        <person name="Itagaki T."/>
        <person name="Satoh R."/>
        <person name="Teshima R."/>
        <person name="Nonaka T."/>
      </authorList>
      <dbReference type="PubMed" id="20054125"/>
      <dbReference type="DOI" id="10.1107/s1744309109043127"/>
    </citation>
    <scope>CRYSTALLIZATION OF 35-149</scope>
    <scope>ALLERGEN</scope>
    <scope>MUTAGENESIS OF 1-MET--MET-34</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2010" name="Int. Arch. Allergy Immunol." volume="153" first="133" last="140">
      <title>Identification of an IgE-binding epitope of a major buckwheat allergen, BWp16, by SPOTs assay and mimotope screening.</title>
      <authorList>
        <person name="Satoh R."/>
        <person name="Koyano S."/>
        <person name="Takagi K."/>
        <person name="Nakamura R."/>
        <person name="Teshima R."/>
      </authorList>
      <dbReference type="PubMed" id="20407269"/>
      <dbReference type="DOI" id="10.1159/000312630"/>
    </citation>
    <scope>ALLERGEN</scope>
    <scope>BIOTECHNOLOGY</scope>
    <scope>REGION</scope>
    <scope>SITE</scope>
    <scope>MUTAGENESIS OF LYS-120; GLU-121; GLY-122; VAL-123; ARG-124; ASP-125; LEU-126; LYS-127 AND GLU-128</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2011" name="Arch. Dermatol. Res." volume="303" first="635" last="642">
      <title>Usability of Fag e 2 ImmunoCAP in the diagnosis of buckwheat allergy.</title>
      <authorList>
        <person name="Tohgi K."/>
        <person name="Kohno K."/>
        <person name="Takahashi H."/>
        <person name="Matsuo H."/>
        <person name="Nakayama S."/>
        <person name="Morita E."/>
      </authorList>
      <dbReference type="PubMed" id="21461893"/>
      <dbReference type="DOI" id="10.1007/s00403-011-1142-z"/>
    </citation>
    <scope>ALLERGEN</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>BIOTECHNOLOGY</scope>
  </reference>
  <comment type="function">
    <text evidence="13">Seed storage protein.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3 4">Expressed in seeds (at protein level) (PubMed:16549935, PubMed:18520034).</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="7">Expressed in shoots 10 days after seeding.</text>
  </comment>
  <comment type="allergen">
    <text evidence="2 3 4 5 6 7">Causes an allergic reaction in human. Binds to IgE of patients allergic to Japanese buckwheat (PubMed:12372998, PubMed:16549935, PubMed:18520034, PubMed:20054125, PubMed:20407269, PubMed:21461893). Binds to IgE in 90% of the 10 patients tested having immediate hypersensitivity reactions (IRH) including anaphylaxis, but not to IgE from other 10 patients tested without IHR (PubMed:12372998). Natural protein binds to IgE in 76% of the 17 patients tested (PubMed:16549935). Recombinant protein binds to IgE in 83% of the 29 Japanese patients tested (PubMed:18520034). IgE-binding is unaffected by pepsin digestion (PubMed:12372998).</text>
  </comment>
  <comment type="biotechnology">
    <text evidence="15 16 17">This protein may be helpful in engineering hypoallergens for buckwheat allergy (PubMed:18520034, PubMed:20407269). Determining IgE antibody titer of the natural protein in the blood serum using the ImmunoCAP method could be a reliable way to diagnose buckwheat allergy with a sensitivity of 90% and a specificity of 89.6% using a cut-off value of 2.74 kUA/L (PubMed:21461893).</text>
  </comment>
  <comment type="similarity">
    <text evidence="13">Belongs to the 2S seed storage albumins family.</text>
  </comment>
  <dbReference type="EMBL" id="DQ304682">
    <property type="protein sequence ID" value="ABC18306.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="FJ430161">
    <property type="protein sequence ID" value="ACJ48243.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q2PS07"/>
  <dbReference type="SMR" id="Q2PS07"/>
  <dbReference type="Allergome" id="1187">
    <property type="allergen name" value="Fag e 2"/>
  </dbReference>
  <dbReference type="Allergome" id="8711">
    <property type="allergen name" value="Fag e 2.0101"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043245">
    <property type="term" value="C:extraorganismal space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019863">
    <property type="term" value="F:IgE binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045735">
    <property type="term" value="F:nutrient reservoir activity"/>
    <property type="evidence" value="ECO:0000305"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048367">
    <property type="term" value="P:shoot system development"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.110.10">
    <property type="entry name" value="Plant lipid-transfer and hydrophobic proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036312">
    <property type="entry name" value="Bifun_inhib/LTP/seed_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000617">
    <property type="entry name" value="Napin/2SS/CON"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35496">
    <property type="entry name" value="2S SEED STORAGE PROTEIN 1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35496:SF4">
    <property type="entry name" value="BIFUNCTIONAL INHIBITOR_PLANT LIPID TRANSFER PROTEIN_SEED STORAGE HELICAL DOMAIN-CONTAINING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47699">
    <property type="entry name" value="Bifunctional inhibitor/lipid-transfer protein/seed storage 2S albumin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0020">Allergen</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0389">IgE-binding protein</keyword>
  <keyword id="KW-0708">Seed storage protein</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0758">Storage protein</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5010973053" description="2S seed storage albumin protein" evidence="14">
    <location>
      <begin position="23"/>
      <end position="149"/>
    </location>
  </feature>
  <feature type="region of interest" description="IgE-binding" evidence="6">
    <location>
      <begin position="121"/>
      <end position="128"/>
    </location>
  </feature>
  <feature type="site" description="Critical for IgE-binding activity" evidence="6">
    <location>
      <position position="125"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="38"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="52"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="88"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="100"/>
      <end position="140"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect in IgE-binding activity." evidence="5">
    <location>
      <begin position="1"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect in IgE-binding activity." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="52"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="77"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="83"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity. Susceptible to pepsin digestion by simulated gastric fluid (SGF)." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="87"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity. Susceptible to pepsin digestion by simulated gastric fluid (SGF)." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="88"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="98"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect in IgE-binding activity." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="100"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="6">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="120"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="6">
    <original>E</original>
    <variation>A</variation>
    <location>
      <position position="121"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="6">
    <original>G</original>
    <variation>A</variation>
    <location>
      <position position="122"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="6">
    <original>V</original>
    <variation>A</variation>
    <location>
      <position position="123"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="6">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="124"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Significantly decreased IgE-binding activity." evidence="6">
    <original>D</original>
    <variation>A</variation>
    <location>
      <position position="125"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="6">
    <original>L</original>
    <variation>A</variation>
    <location>
      <position position="126"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="6">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="127"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="6">
    <original>E</original>
    <variation>A</variation>
    <location>
      <position position="128"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreased IgE-binding activity." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="133"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect in IgE-binding activity." evidence="4">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="140"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q647G9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12372998"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="16549935"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="18520034"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="20054125"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="20407269"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="21461893"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="12372998"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="16549935"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="10">
    <source>
      <dbReference type="PubMed" id="18520034"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="11">
    <source>
      <dbReference type="PubMed" id="20054125"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="20407269"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13"/>
  <evidence type="ECO:0000305" key="14">
    <source>
      <dbReference type="PubMed" id="12372998"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="15">
    <source>
      <dbReference type="PubMed" id="18520034"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="16">
    <source>
      <dbReference type="PubMed" id="20407269"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="17">
    <source>
      <dbReference type="PubMed" id="21461893"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="18">
    <source>
      <dbReference type="EMBL" id="ABC18306.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="19">
    <source>
      <dbReference type="EMBL" id="ACJ48243.1"/>
    </source>
  </evidence>
  <sequence length="149" mass="16941" checksum="C2F613F49DDBC10C" modified="2006-01-24" version="1" precursor="true">MKLFIILATATLLIAATQATYPRDEGFDLGETQMSSKCMRQVKMNEPHLKKCNRYIAMDILDDKYAEALSRVEGEGCKSEESCMRGCCVAMKEMDDECVCEWMKMMVENQKGRIGERLIKEGVRDLKELPSKCGLSELECGSRGNRYFV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>