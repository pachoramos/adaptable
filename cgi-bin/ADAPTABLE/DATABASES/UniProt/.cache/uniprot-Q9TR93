<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-10-10" modified="2024-05-29" version="86" xmlns="http://uniprot.org/uniprot">
  <accession>Q9TR93</accession>
  <accession>Q9TR92</accession>
  <name>PYY_RABIT</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Peptide YY</fullName>
      <shortName evidence="4">PYY</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">PYY-I</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Peptide tyrosine tyrosine</fullName>
    </alternativeName>
    <component>
      <recommendedName>
        <fullName evidence="4">Peptide YY(3-36)</fullName>
      </recommendedName>
      <alternativeName>
        <fullName evidence="4">PYY-II</fullName>
      </alternativeName>
    </component>
  </protein>
  <gene>
    <name type="primary">PYY</name>
  </gene>
  <organism>
    <name type="scientific">Oryctolagus cuniculus</name>
    <name type="common">Rabbit</name>
    <dbReference type="NCBI Taxonomy" id="9986"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Lagomorpha</taxon>
      <taxon>Leporidae</taxon>
      <taxon>Oryctolagus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Peptides" volume="15" first="815" last="820">
      <title>Characterization of two forms of peptide YY, PYY(1-36) and PYY(3-36), in the rabbit.</title>
      <authorList>
        <person name="Grandt D."/>
        <person name="Schimiczek M."/>
        <person name="Struk K."/>
        <person name="Shively J."/>
        <person name="Eysselein V.E."/>
        <person name="Goebell H."/>
        <person name="Reeve J.R. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="7984499"/>
      <dbReference type="DOI" id="10.1016/0196-9781(94)90035-3"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT TYR-36</scope>
    <source>
      <tissue>Intestinal mucosa</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>This gut peptide inhibits exocrine pancreatic secretion, has a vasoconstrictory action and inhibitis jejunal and colonic mobility.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="PTM">
    <text evidence="1">The peptide YY form is cleaved at Pro-2 by the prolyl endopeptidase FAP (seprase) activity (in vitro) to generate peptide YY(3-36).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the NPY family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="Q9TR93"/>
  <dbReference type="BMRB" id="Q9TR93"/>
  <dbReference type="SMR" id="Q9TR93"/>
  <dbReference type="STRING" id="9986.ENSOCUP00000011243"/>
  <dbReference type="PaxDb" id="9986-ENSOCUP00000011243"/>
  <dbReference type="eggNOG" id="ENOG502S267">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="Q9TR93"/>
  <dbReference type="Proteomes" id="UP000001811">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031841">
    <property type="term" value="F:neuropeptide Y receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="CDD" id="cd00126">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.900">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001955">
    <property type="entry name" value="Pancreatic_hormone-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020392">
    <property type="entry name" value="Pancreatic_hormone-like_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533">
    <property type="entry name" value="NEUROPEPTIDE Y/PANCREATIC HORMONE/PEPTIDE YY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533:SF14">
    <property type="entry name" value="PEPTIDE YY-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00159">
    <property type="entry name" value="Hormone_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00278">
    <property type="entry name" value="PANCHORMONE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00309">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00265">
    <property type="entry name" value="PANCREATIC_HORMONE_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50276">
    <property type="entry name" value="PANCREATIC_HORMONE_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000025389" description="Peptide YY">
    <location>
      <begin position="1"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000025390" description="Peptide YY(3-36)">
    <location>
      <begin position="3"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="site" description="Cleavage; by FAP" evidence="1">
    <location>
      <begin position="2"/>
      <end position="3"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="2">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="3">
    <location>
      <position position="36"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P10082"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P68005"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7984499"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="7984499"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="36" mass="4285" checksum="02D499C8086DCC8D" modified="2000-05-01" version="1">YPSKPEAPGEDASPEELNRYYASLRHYLNLVTRQRY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>