<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-01-11" modified="2023-02-22" version="30" xmlns="http://uniprot.org/uniprot">
  <accession>P0CI44</accession>
  <name>LVPAY_LYCMC</name>
  <protein>
    <recommendedName>
      <fullName>Lipolysis-activating peptide 1-alpha chain</fullName>
      <shortName>LVP1-alpha</shortName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Neurotoxin BmKBTx-like</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Lychas mucronatus</name>
    <name type="common">Chinese swimming scorpion</name>
    <dbReference type="NCBI Taxonomy" id="172552"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Lychas</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2010" name="BMC Genomics" volume="11" first="452" last="452">
      <title>Comparative venom gland transcriptome analysis of the scorpion Lychas mucronatus reveals intraspecific toxic gene diversity and new venomous components.</title>
      <authorList>
        <person name="Zhao R."/>
        <person name="Ma Y."/>
        <person name="He Y."/>
        <person name="Di Z."/>
        <person name="Wu Y.-L."/>
        <person name="Cao Z.-J."/>
        <person name="Li W.-X."/>
      </authorList>
      <dbReference type="PubMed" id="20663230"/>
      <dbReference type="DOI" id="10.1186/1471-2164-11-452"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>Yunnan</strain>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">The heterodimer non-edited LVP1 induces lipolysis in rat adipocytes. Induction of lipolysis by LVP1 appears to be mediated through the beta-2 adrenergic receptor pathway (ADRB2) (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="2">The edited BmKBTx-like, similar to beta-toxins, may modulate voltage-gated sodium channels (Nav) and may block voltage-gated potassium channels (Kv).</text>
  </comment>
  <comment type="subunit">
    <text>Monomer (edited version) and heterodimer (non-edited version) of this alpha chain and a beta chain (AC P0CI43).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="5">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="RNA editing">
    <location>
      <position position="76" evidence="1"/>
    </location>
    <text evidence="1">The stop codon (UGA) at position 76 is created by RNA editing.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the long (3 C-C) scorpion toxin superfamily.</text>
  </comment>
  <dbReference type="EMBL" id="GT028572">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0CI44"/>
  <dbReference type="SMR" id="P0CI44"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0691">RNA editing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000403883" description="Lipolysis-activating peptide 1-alpha chain">
    <location>
      <begin position="22"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000403884" description="Neurotoxin BmKBTx-like" evidence="1">
    <location>
      <begin position="22"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="4">
    <location>
      <begin position="22"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="35"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="44"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="48"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (with C-86 in LVP1 chain beta)" evidence="1">
    <location>
      <position position="83"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P84810"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000255" key="4">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="20663230"/>
    </source>
  </evidence>
  <sequence length="83" mass="9711" checksum="EA8452FB0F63B4FA" modified="2011-01-11" version="1" precursor="true">MNIILFYFMPILISLPGLLASGTYPNDVYGLTYDCGKLGENEHCLKICKIHGVEYGYCYGWRCWCDKLSDKNKLFWDVYKEHC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>