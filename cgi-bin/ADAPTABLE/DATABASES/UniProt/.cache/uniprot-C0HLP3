<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-10-07" modified="2024-01-24" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>C0HLP3</accession>
  <name>CYHEI_PIGEN</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Cyclotide hyen-I</fullName>
    </recommendedName>
  </protein>
  <organism evidence="4">
    <name type="scientific">Pigea enneasperma</name>
    <name type="common">Spade flower</name>
    <name type="synonym">Afrohybanthus enneaspermus</name>
    <dbReference type="NCBI Taxonomy" id="212266"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Malpighiales</taxon>
      <taxon>Violaceae</taxon>
      <taxon>Pigea</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2020" name="J. Biol. Chem." volume="295" first="10911" last="10925">
      <title>Discovery and mechanistic studies of cytotoxic cyclotides from the medicinal herb Hybanthus enneaspermus.</title>
      <authorList>
        <person name="Du Q."/>
        <person name="Chan L.Y."/>
        <person name="Gilding E.K."/>
        <person name="Henriques S.T."/>
        <person name="Condon N.D."/>
        <person name="Ravipati A.S."/>
        <person name="Kaas Q."/>
        <person name="Huang Y.H."/>
        <person name="Craik D.J."/>
      </authorList>
      <dbReference type="PubMed" id="32414842"/>
      <dbReference type="DOI" id="10.1074/jbc.ra120.012627"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Probably participates in a plant defense mechanism.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Detected in seeds (at protein level).</text>
  </comment>
  <comment type="domain">
    <text evidence="5">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2">This is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="3173.2" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the cyclotide family. Bracelet subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="2">This peptide is cyclic. The start position was chosen by similarity to Oak1 (kalata B1) for which the DNA sequence is known.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HLP3"/>
  <dbReference type="SMR" id="C0HLP3"/>
  <dbReference type="GO" id="GO:0051715">
    <property type="term" value="P:cytolysis in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012323">
    <property type="entry name" value="Cyclotide_bracelet_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF037891">
    <property type="entry name" value="Cycloviolacin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60008">
    <property type="entry name" value="CYCLOTIDE_BRACELET"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000450765" description="Cyclotide hyen-I" evidence="2">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="5"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="9"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="14"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Gly-Asp)" evidence="1">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HK39"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="32414842"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="32414842"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="31" mass="3200" checksum="E6330A52EEA0B867" modified="2020-10-07" version="1">GSTPCGESCVWIPCISGIVGCSCSNKVCYMD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>