<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-12-15" modified="2023-02-22" version="71" xmlns="http://uniprot.org/uniprot">
  <accession>O46166</accession>
  <name>TXI1_ERAAG</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">U1-agatoxin-Ta1a</fullName>
      <shortName evidence="3">U1-AGTX-Ta1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Insecticidal toxin 1</fullName>
      <shortName evidence="4">TaITX-1</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Eratigena agrestis</name>
    <name type="common">Hobo spider</name>
    <name type="synonym">Tegenaria agrestis</name>
    <dbReference type="NCBI Taxonomy" id="1686644"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Agelenidae</taxon>
      <taxon>Eratigena</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Arch. Insect Biochem. Physiol." volume="38" first="19" last="31">
      <title>Novel insecticidal peptides from Tegenaria agrestis spider venom may have a direct effect on the insect central nervous system.</title>
      <authorList>
        <person name="Johnson J.H."/>
        <person name="Bloomquist J.R."/>
        <person name="Krapcho K.J."/>
        <person name="Kral R.M. Jr."/>
        <person name="Trovato R."/>
        <person name="Eppler K.G."/>
        <person name="Morgan T.K."/>
        <person name="Delmar E.G."/>
      </authorList>
      <dbReference type="PubMed" id="9589602"/>
      <dbReference type="DOI" id="10.1002/(sici)1520-6327(1998)38:1&lt;19::aid-arch3&gt;3.0.co;2-q"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 18-58</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>TOXIC DOSE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2015" name="Structure" volume="23" first="1283" last="1292">
      <title>Weaponization of a hormone: convergent recruitment of hyperglycemic hormone into the venom of arthropod predators.</title>
      <authorList>
        <person name="Undheim E.A."/>
        <person name="Grimm L.L."/>
        <person name="Low C.F."/>
        <person name="Morgenstern D."/>
        <person name="Herzig V."/>
        <person name="Zobel-Thropp P."/>
        <person name="Pineda S.S."/>
        <person name="Habib R."/>
        <person name="Dziemborowicz S."/>
        <person name="Fry B.G."/>
        <person name="Nicholson G.M."/>
        <person name="Binford G.J."/>
        <person name="Mobli M."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="26073605"/>
      <dbReference type="DOI" id="10.1016/j.str.2015.05.003"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 18-67</scope>
    <scope>AMIDATION AT LYS-67</scope>
    <scope>DOMAIN</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>TOXIC DOSE</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Toxin that paralyzes insects. May have a direct effect on the insect central nervous system.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">Is exclusively composed of 4 tightly packed alpha helices (no beta strand is present).</text>
  </comment>
  <comment type="mass spectrometry" mass="5678.55" method="Electrospray" evidence="2"/>
  <comment type="toxic dose">
    <text evidence="1">LD(50) is 198 +- 33 pmol/g when injected into the ventro-lateral thoracic region of adult blowflies (L.cuprina) (at 24 hours post-injection) (PubMed:26073605).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="2">PD(50) is 0.89 nmol/g when injected into tobacco budworm (PubMed:9589602).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="2">PD(50) is 0.78 nmol/g when injected into cabbage looper (PubMed:9589602).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="2">PD(50) is 0.9 nmol/g when injected into beet army worm (PubMed:9589602).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="2">PD(50) is 2.0 nmol/g when injected into Southern corn rootworm (PubMed:9589602).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="5">Arose via modification of ancestral neuropeptide hormones (ion transport peptide/crustacean hyperglycemic hormones (ITP/CHH)).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the helical arthropod-neuropeptide-derived (HAND) family.</text>
  </comment>
  <dbReference type="EMBL" id="AJ224127">
    <property type="protein sequence ID" value="CAA11839.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="2KSL">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=18-67"/>
  </dbReference>
  <dbReference type="PDB" id="6URP">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=18-67"/>
  </dbReference>
  <dbReference type="PDBsum" id="2KSL"/>
  <dbReference type="PDBsum" id="6URP"/>
  <dbReference type="AlphaFoldDB" id="O46166"/>
  <dbReference type="BMRB" id="O46166"/>
  <dbReference type="SMR" id="O46166"/>
  <dbReference type="ArachnoServer" id="AS000349">
    <property type="toxin name" value="U1-agatoxin-Ta1a"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="O46166"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.2010.20">
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000035555" description="U1-agatoxin-Ta1a">
    <location>
      <begin position="18"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="modified residue" description="Lysine amide" evidence="1">
    <location>
      <position position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="23"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="39"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="42"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="21"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="29"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="46"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="turn" evidence="8">
    <location>
      <begin position="52"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="57"/>
      <end position="64"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="26073605"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="9589602"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="26073605"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="9589602"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="26073605"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="9589602"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="2KSL"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="8">
    <source>
      <dbReference type="PDB" id="6URP"/>
    </source>
  </evidence>
  <sequence length="68" mass="7741" checksum="19578D59D92FF268" modified="1998-06-01" version="1" precursor="true">MKLQLMICLVLLPCFFCEPDEICRARMTHKEFNYKSNVCNGCGDQVAACEAECFRNDVYTACHEAQKG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>