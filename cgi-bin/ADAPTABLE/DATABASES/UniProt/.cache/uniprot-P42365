<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-11-27" version="93" xmlns="http://uniprot.org/uniprot">
  <accession>P42365</accession>
  <name>TPX_STREE</name>
  <protein>
    <recommendedName>
      <fullName>Thiol peroxidase</fullName>
      <shortName>Tpx</shortName>
      <ecNumber evidence="2">1.11.1.24</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Peroxiredoxin tpx</fullName>
      <shortName>Prx</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Thioredoxin peroxidase</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Thioredoxin-dependent peroxiredoxin</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">tpx</name>
  </gene>
  <organism>
    <name type="scientific">Streptococcus pneumoniae</name>
    <dbReference type="NCBI Taxonomy" id="1313"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Streptococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Infect. Immun." volume="62" first="319" last="324">
      <title>Cloning and nucleotide sequence analysis of psaA, the Streptococcus pneumoniae gene encoding a 37-kilodalton protein homologous to previously reported Streptococcus sp. adhesins.</title>
      <authorList>
        <person name="Sampson J.S."/>
        <person name="O'Connor S.P."/>
        <person name="Stinson A.R."/>
        <person name="Tharpe J.A."/>
        <person name="Russell H."/>
      </authorList>
      <dbReference type="PubMed" id="7505262"/>
      <dbReference type="DOI" id="10.1128/iai.62.1.319-324.1994"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>R36A</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1997" name="FEBS Lett." volume="407" first="32" last="36">
      <title>Scavengase p20: a novel family of bacterial antioxidant enzymes.</title>
      <authorList>
        <person name="Wan X.Y."/>
        <person name="Zhou Y."/>
        <person name="Yan Z.Y."/>
        <person name="Wang H.L."/>
        <person name="Hou Y.D."/>
        <person name="Jin D.Y."/>
      </authorList>
      <dbReference type="PubMed" id="9141476"/>
      <dbReference type="DOI" id="10.1016/s0014-5793(97)00302-5"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 2-14</scope>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <source>
      <strain>R36A</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Thiol-specific peroxidase that catalyzes the reduction of hydrogen peroxide and organic hydroperoxides to water and alcohols, respectively. Plays a role in cell protection against oxidative stress by detoxifying peroxides.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>a hydroperoxide + [thioredoxin]-dithiol = an alcohol + [thioredoxin]-disulfide + H2O</text>
      <dbReference type="Rhea" id="RHEA:62620"/>
      <dbReference type="Rhea" id="RHEA-COMP:10698"/>
      <dbReference type="Rhea" id="RHEA-COMP:10700"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:29950"/>
      <dbReference type="ChEBI" id="CHEBI:30879"/>
      <dbReference type="ChEBI" id="CHEBI:35924"/>
      <dbReference type="ChEBI" id="CHEBI:50058"/>
      <dbReference type="EC" id="1.11.1.24"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the peroxiredoxin family. Tpx subfamily.</text>
  </comment>
  <dbReference type="EC" id="1.11.1.24" evidence="2"/>
  <dbReference type="EMBL" id="L19055">
    <property type="protein sequence ID" value="AAA16799.1"/>
    <property type="molecule type" value="Unassigned_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P42365"/>
  <dbReference type="SMR" id="P42365"/>
  <dbReference type="GO" id="GO:0140824">
    <property type="term" value="F:thioredoxin-dependent peroxiredoxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.30.10">
    <property type="entry name" value="Glutaredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036249">
    <property type="entry name" value="Thioredoxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050455">
    <property type="entry name" value="Tpx_Peroxidase_subfamily"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR43110">
    <property type="entry name" value="THIOL PEROXIDASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR43110:SF1">
    <property type="entry name" value="THIOL PEROXIDASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF52833">
    <property type="entry name" value="Thioredoxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0049">Antioxidant</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-0575">Peroxidase</keyword>
  <keyword id="KW-0676">Redox-active center</keyword>
  <feature type="initiator methionine" description="Removed" evidence="2">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000187914" description="Thiol peroxidase">
    <location>
      <begin position="2"/>
      <end position="50" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="50"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P0A862"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="9141476"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="50" mass="5355" checksum="4FF8FB857F648B33" modified="2007-01-23" version="2" fragment="single">MTTFLGNPVTFTGSQLQVGDTAHDFSLTTPNLEKKSLADFAGKKKVLSVI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>