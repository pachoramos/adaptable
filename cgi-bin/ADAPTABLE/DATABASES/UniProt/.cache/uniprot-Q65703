<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2024-07-24" version="68" xmlns="http://uniprot.org/uniprot">
  <accession>Q65703</accession>
  <name>NS1_ORSVW</name>
  <protein>
    <recommendedName>
      <fullName>Non-structural protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Non-structural protein 1C</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">1C</name>
  </gene>
  <organism>
    <name type="scientific">Ovine respiratory syncytial virus (strain WSU 83-1578)</name>
    <name type="common">ORSV</name>
    <dbReference type="NCBI Taxonomy" id="79699"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Orthornavirae</taxon>
      <taxon>Negarnaviricota</taxon>
      <taxon>Haploviricotina</taxon>
      <taxon>Monjiviricetes</taxon>
      <taxon>Mononegavirales</taxon>
      <taxon>Pneumoviridae</taxon>
      <taxon>Ovine respiratory syncytial virus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Ovis aries</name>
    <name type="common">Sheep</name>
    <dbReference type="NCBI Taxonomy" id="9940"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1994" name="J. Gen. Virol." volume="75" first="401" last="404">
      <title>Nucleotide and predicted amino acid sequence analysis of the ovine respiratory syncytial virus non-structural 1C and 1B genes and the small hydrophobic protein gene.</title>
      <authorList>
        <person name="Alansari H.M."/>
        <person name="Potgieter L.N.D."/>
      </authorList>
      <dbReference type="PubMed" id="8113762"/>
      <dbReference type="DOI" id="10.1099/0022-1317-75-2-401"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays a major role in antagonizing the type I IFN-mediated antiviral response by degrading or inhibiting multiple cellular factors required for either IFN induction or response pathways. Acts cooperatively with NS2 to repress activation and nuclear translocation of host IFN-regulatory factor IRF3. Also disrupts the association of IRF3 with CREBBP. Interacts with host mitochondrial-associated membrane (MAM) MAVS and prevents the interaction with RIGI. Interacts with TRIM25 to suppress TRIM25-mediated RIGI ubiquitination and thereby RIGI-MAVS interaction. Together with NS2, participates in the proteasomal degradation of host STAT2, IRF3, IRF7, TBK1 and RIGI through a NS-degradasome involving CUL2 and Elongin-C. The degradasome requires an intact mitochondrial MAVS. Decreases the levels of host TRAF3 and IKBKE/IKK-epsilon. As functions other than disruptions of the type I IFN-mediated antiviral signaling pathways, induces host SOCS1 and SOCS3 expression. Suppresses premature apoptosis by an NF-kappa-B-dependent, interferon-independent mechanism and thus facilitates virus growth. Additionally, NS1 may serve some inhibitory role in viral transcription and RNA replication. Suppresses proliferation and activation of host CD103+ CD8+ cytotoxic T-lymphocytes and Th17 helper T-lymphocytes.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer. Homomultimer. Heteromultimer with NS2. Interacts with the matrix protein M. Interacts with host ELOC and CUL2; this interaction allows NS1 to form an active E3 ligase with ELOC and CUL2. Interacts with host IRF3; this interaction leads to the disrupted association of IRF3 with CREBBP and thus reduced binding of IRF3 to the IFN-beta promoter. Interacts with host MAVS; this interaction prevents MAVS binding to RIGI and inhibits signaling pathway leading to interferon production. Interacts with host TRIM25 (via SPRY domain); this interaction suppresses RIGI ubiquitination and results in decreased interaction between RIGI and MAVS.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host mitochondrion</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
    </subcellularLocation>
    <text evidence="1">Most NS1 resides in the mitochondria as a heteromer with NS2.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the pneumovirus non-structural protein 1 family.</text>
  </comment>
  <dbReference type="EMBL" id="L15452">
    <property type="protein sequence ID" value="AAA42803.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="SMR" id="Q65703"/>
  <dbReference type="GO" id="GO:0033650">
    <property type="term" value="C:host cell mitochondrion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042025">
    <property type="term" value="C:host cell nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052150">
    <property type="term" value="P:symbiont-mediated perturbation of host apoptosis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039504">
    <property type="term" value="P:symbiont-mediated suppression of host adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039548">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of IRF3 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039557">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of IRF7 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039545">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of MAVS activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039540">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of RIG-I activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039723">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of TBK1 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039564">
    <property type="term" value="P:symbiont-mediated suppression of host JAK-STAT cascade via inhibition of STAT2 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039722">
    <property type="term" value="P:symbiont-mediated suppression of host toll-like receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039502">
    <property type="term" value="P:symbiont-mediated suppression of host type I interferon-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005099">
    <property type="entry name" value="Pneumo_NS1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03438">
    <property type="entry name" value="Pneumo_NS1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1045">Host mitochondrion</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1080">Inhibition of host adaptive immune response by virus</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1114">Inhibition of host interferon signaling pathway by virus</keyword>
  <keyword id="KW-1092">Inhibition of host IRF3 by virus</keyword>
  <keyword id="KW-1093">Inhibition of host IRF7 by virus</keyword>
  <keyword id="KW-1097">Inhibition of host MAVS by virus</keyword>
  <keyword id="KW-1088">Inhibition of host RIG-I by virus</keyword>
  <keyword id="KW-1113">Inhibition of host RLR pathway by virus</keyword>
  <keyword id="KW-1106">Inhibition of host STAT2 by virus</keyword>
  <keyword id="KW-1223">Inhibition of host TBK1 by virus</keyword>
  <keyword id="KW-1225">Inhibition of host TLR pathway by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-1119">Modulation of host cell apoptosis by virus</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <feature type="chain" id="PRO_0000142781" description="Non-structural protein 1">
    <location>
      <begin position="1"/>
      <end position="136"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P0DOE9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="136" mass="15208" checksum="B066E7DC51E6629C" modified="1996-11-01" version="1">MGSETLSVIQVRLQNIYDNDKVALLKITCNTNRLILLTHTLAKSVIHTIKLNGTVFLHIVTSSDFCPTSDIIESANFTTMPVLQNGGYIWELIELTHCFQTNGLIDDNCEVTFSKRLSDSELEKYSSQLSDLLGLN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>