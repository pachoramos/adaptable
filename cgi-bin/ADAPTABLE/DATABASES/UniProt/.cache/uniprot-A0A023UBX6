<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-03-28" modified="2023-09-13" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>A0A023UBX6</accession>
  <name>PHAT1_AMAEX</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Phallacidin proprotein 1</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="4">Phallacidin</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name evidence="4" type="primary">PHA3</name>
    <name evidence="4" type="synonym">PHA2</name>
    <name evidence="4" type="synonym">PHA4</name>
  </gene>
  <organism>
    <name type="scientific">Amanita exitialis</name>
    <name type="common">Guangzhou destroying angel</name>
    <dbReference type="NCBI Taxonomy" id="262245"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Basidiomycota</taxon>
      <taxon>Agaricomycotina</taxon>
      <taxon>Agaricomycetes</taxon>
      <taxon>Agaricomycetidae</taxon>
      <taxon>Agaricales</taxon>
      <taxon>Pluteineae</taxon>
      <taxon>Amanitaceae</taxon>
      <taxon>Amanita</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2014" name="Toxicon" volume="83" first="59" last="68">
      <title>The molecular diversity of toxin gene families in lethal Amanita mushrooms.</title>
      <authorList>
        <person name="Li P."/>
        <person name="Deng W."/>
        <person name="Li T."/>
      </authorList>
      <dbReference type="PubMed" id="24613547"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2014.02.020"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="6">Toxin that belongs to the bicyclic heptapeptides called phallotoxins (PubMed:24613547). Although structurally related to amatoxins, phallotoxins have a different mode of action, which is the stabilization of F-actin (PubMed:24613547). Phallotoxins are poisonous when administered parenterally, but not orally because of poor absorption (PubMed:24613547).</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Processed by the macrocyclase-peptidase enzyme POPB to yield a toxic cyclic heptapeptide (By similarity). POPB first removes 10 residues from the N-terminus (By similarity). Conformational trapping of the remaining peptide forces the enzyme to release this intermediate rather than proceed to macrocyclization (By similarity). The enzyme rebinds the remaining peptide in a different conformation and catalyzes macrocyclization of the N-terminal 7 residues (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the MSDIN fungal toxin family.</text>
  </comment>
  <dbReference type="EMBL" id="KC778564">
    <property type="protein sequence ID" value="AGO98220.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KC778565">
    <property type="protein sequence ID" value="AGO98221.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KC778566">
    <property type="protein sequence ID" value="AGO98222.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KF546293">
    <property type="protein sequence ID" value="AHX98317.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KF546294">
    <property type="protein sequence ID" value="AHX98318.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KF546295">
    <property type="protein sequence ID" value="AHX98319.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A023UBX6"/>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027582">
    <property type="entry name" value="Amanitin/phalloidin"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR04309">
    <property type="entry name" value="amanitin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0883">Thioether bond</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="propeptide" id="PRO_0000443614" evidence="2">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000443615" description="Phallacidin" evidence="2">
    <location>
      <begin position="2"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000443616" evidence="2">
    <location>
      <begin position="9"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Ala-Pro)" evidence="2">
    <location>
      <begin position="2"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="cross-link" description="2'-cysteinyl-6'-hydroxytryptophan sulfoxide (Trp-Cys)" evidence="3">
    <location>
      <begin position="3"/>
      <end position="7"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="5">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A067SLB9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="A8W7M4"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P85421"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="24613547"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="24613547"/>
    </source>
  </evidence>
  <sequence length="23" mass="2585" checksum="1D7F8C6B3622C7E5" modified="2014-07-09" version="1" precursor="true" fragment="single">PAWLVDCPCVGDDVNRLLTRGER</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>