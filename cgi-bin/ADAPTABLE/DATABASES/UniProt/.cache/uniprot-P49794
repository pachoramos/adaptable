<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-10-01" modified="2024-11-27" version="56" xmlns="http://uniprot.org/uniprot">
  <accession>P49794</accession>
  <name>MCH_OREMO</name>
  <protein>
    <recommendedName>
      <fullName>Pro-MCH</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Melanin-concentrating hormone</fullName>
        <shortName>MCH</shortName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">mch</name>
  </gene>
  <organism>
    <name type="scientific">Oreochromis mossambicus</name>
    <name type="common">Mozambique tilapia</name>
    <name type="synonym">Tilapia mossambica</name>
    <dbReference type="NCBI Taxonomy" id="8127"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Actinopterygii</taxon>
      <taxon>Neopterygii</taxon>
      <taxon>Teleostei</taxon>
      <taxon>Neoteleostei</taxon>
      <taxon>Acanthomorphata</taxon>
      <taxon>Ovalentaria</taxon>
      <taxon>Cichlomorphae</taxon>
      <taxon>Cichliformes</taxon>
      <taxon>Cichlidae</taxon>
      <taxon>African cichlids</taxon>
      <taxon>Pseudocrenilabrinae</taxon>
      <taxon>Oreochromini</taxon>
      <taxon>Oreochromis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Fish Physiol. Biochem." volume="11" first="117" last="124">
      <title>Cloning and sequence analysis of hypothalamus cDNA encoding tilapia melanin-concentrating hormone.</title>
      <authorList>
        <person name="Groneveld D."/>
        <person name="Hut M.J."/>
        <person name="Balm P.H.M."/>
        <person name="Martens G.J.M."/>
        <person name="Wendelaar Bonga S.E."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Hypothalamus</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Plays a role in skin pigmentation by antagonizing the action of melanotropin alpha. Induces melanin concentration within the melanophores. May participate in the control of the hypothalamo-pituitary adrenal gland axis by inhibiting the release of ACTH.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the melanin-concentrating hormone family.</text>
  </comment>
  <dbReference type="EMBL" id="X81144">
    <property type="protein sequence ID" value="CAA57050.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P49794"/>
  <dbReference type="BMRB" id="P49794"/>
  <dbReference type="GO" id="GO:0045202">
    <property type="term" value="C:synapse"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="GOC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030354">
    <property type="term" value="F:melanin-concentrating hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031777">
    <property type="term" value="F:type 1 melanin-concentrating hormone receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007268">
    <property type="term" value="P:chemical synaptic transmission"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005456">
    <property type="entry name" value="Prepro-melanin_conc_hormone"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12091">
    <property type="entry name" value="MELANIN-CONCENTRATING HORMONE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12091:SF0">
    <property type="entry name" value="PRO-MCH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05824">
    <property type="entry name" value="Pro-MCH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01641">
    <property type="entry name" value="PROMCHFAMILY"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000019141" description="Pro-MCH">
    <location>
      <begin position="21"/>
      <end position="136"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000019142" description="Melanin-concentrating hormone">
    <location>
      <begin position="119"/>
      <end position="136"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="124"/>
      <end position="133"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="136" mass="15410" checksum="91EA3AE3B91500DD" modified="1996-10-01" version="1" precursor="true">MRQSRLSIIFAAALFFKCYALTVALPMAKAEDGSLEKDAFTSLLNDEATENSLGDAELSSMTKSRAPRVIVIAADANLWRDLRVLHNGLPLYKRRVDENNQVVEHKDVGQDLTIPILRRDTMRCMVGRVYRPCWEV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>