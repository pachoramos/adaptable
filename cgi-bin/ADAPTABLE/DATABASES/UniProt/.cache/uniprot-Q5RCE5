<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-15" modified="2024-11-27" version="88" xmlns="http://uniprot.org/uniprot">
  <accession>Q5RCE5</accession>
  <name>BOLA1_PONAB</name>
  <protein>
    <recommendedName>
      <fullName>BolA-like protein 1</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">BOLA1</name>
  </gene>
  <organism>
    <name type="scientific">Pongo abelii</name>
    <name type="common">Sumatran orangutan</name>
    <name type="synonym">Pongo pygmaeus abelii</name>
    <dbReference type="NCBI Taxonomy" id="9601"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Pongo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2004-11" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="The German cDNA consortium"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Kidney</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 3">Acts as a mitochondrial iron-sulfur (Fe-S) cluster assembly factor that facilitates (Fe-S) cluster insertion into a subset of mitochondrial proteins (By similarity). Probably acts together with the monothiol glutaredoxin GLRX5. May protect cells against oxidative stress (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="3">Interacts with GLRX5.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Mitochondrion</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the BolA/IbaG family.</text>
  </comment>
  <dbReference type="EMBL" id="CR858326">
    <property type="protein sequence ID" value="CAH90562.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001125298.1">
    <property type="nucleotide sequence ID" value="NM_001131826.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5RCE5"/>
  <dbReference type="SMR" id="Q5RCE5"/>
  <dbReference type="STRING" id="9601.ENSPPYP00000001068"/>
  <dbReference type="Ensembl" id="ENSPPYT00000048740.1">
    <property type="protein sequence ID" value="ENSPPYP00000043835.1"/>
    <property type="gene ID" value="ENSPPYG00000040309.1"/>
  </dbReference>
  <dbReference type="GeneID" id="100172197"/>
  <dbReference type="KEGG" id="pon:100172197"/>
  <dbReference type="CTD" id="51027"/>
  <dbReference type="eggNOG" id="KOG2313">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00510000048165"/>
  <dbReference type="HOGENOM" id="CLU_109462_3_0_1"/>
  <dbReference type="InParanoid" id="Q5RCE5"/>
  <dbReference type="OMA" id="CLGGFGK"/>
  <dbReference type="OrthoDB" id="167034at2759"/>
  <dbReference type="TreeFam" id="TF354266"/>
  <dbReference type="Proteomes" id="UP000001595">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990229">
    <property type="term" value="C:iron-sulfur cluster assembly complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.300.90:FF:000001">
    <property type="entry name" value="Transcriptional regulator BolA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.300.90">
    <property type="entry name" value="BolA-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002634">
    <property type="entry name" value="BolA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036065">
    <property type="entry name" value="BolA-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050961">
    <property type="entry name" value="BolA/IbaG_stress_morph_reg"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46229">
    <property type="entry name" value="BOLA TRANSCRIPTION REGULATOR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46229:SF2">
    <property type="entry name" value="BOLA-LIKE PROTEIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01722">
    <property type="entry name" value="BolA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF82657">
    <property type="entry name" value="BolA-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0496">Mitochondrion</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000201235" description="BolA-like protein 1">
    <location>
      <begin position="1"/>
      <end position="137"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="4">
    <location>
      <begin position="114"/>
      <end position="137"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="2">
    <location>
      <position position="81"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q3E793"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q9D8S9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="Q9Y3E2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="4">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="137" mass="14187" checksum="8BEAEBBD5E467180" modified="2004-12-21" version="1">MLSGRLVPGLVSMAGRVCLCQGSAGSGAVGPVEAAIRTKLEQALSPEVLELRNESGGHAVPPGSETHFRVAVVSSRFEGLSPLQRHRLIHAALAEELAGPVHALAIQARTPAQWGENSQLDTSPPCLGGNKKTLGTP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>