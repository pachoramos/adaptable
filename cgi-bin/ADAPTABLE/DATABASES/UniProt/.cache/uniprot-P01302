<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-01-24" version="134" xmlns="http://uniprot.org/uniprot">
  <accession>P01302</accession>
  <name>PAHO_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Pancreatic polypeptide prohormone</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="4">Pancreatic polypeptide</fullName>
        <shortName evidence="4">PP</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>C-terminal peptide 1</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>C-terminal peptide 2</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">PPY</name>
  </gene>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Proc. Natl. Acad. Sci. U.S.A." volume="92" first="594" last="598">
      <title>Seminalplasmin: recent evolution of another member of the neuropeptide Y gene family.</title>
      <authorList>
        <person name="Herzog H."/>
        <person name="Hort Y."/>
        <person name="Schneider R."/>
        <person name="Shine J."/>
      </authorList>
      <dbReference type="PubMed" id="7831336"/>
      <dbReference type="DOI" id="10.1073/pnas.92.2.594"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="book" date="1979" name="Methods of hormone radioimmunoassay (2nd ed.)" first="657" last="672" publisher="Academic Press" city="New York and London">
      <editorList>
        <person name="Jaffe B.M."/>
        <person name="Behrman H.R."/>
      </editorList>
      <authorList>
        <person name="Chance R.E."/>
        <person name="Moon N.E."/>
        <person name="Johnson M.G."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE OF 30-65</scope>
    <scope>AMIDATION AT TYR-65</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1992" name="Biochemistry" volume="31" first="1245" last="1253">
      <title>Sequence-specific 1H NMR assignments and solution structure of bovine pancreatic polypeptide.</title>
      <authorList>
        <person name="Li X."/>
        <person name="Sutcliffe M.J."/>
        <person name="Schwartz T.W."/>
        <person name="Dodson C.M."/>
      </authorList>
      <dbReference type="PubMed" id="1734969"/>
      <dbReference type="DOI" id="10.1021/bi00119a038"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <comment type="function">
    <molecule>Pancreatic polypeptide</molecule>
    <text evidence="1">Hormone secreted by pancreatic cells that acts as a regulator of pancreatic and gastrointestinal functions probably by signaling through the G protein-coupled receptor NPY4R2.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the NPY family.</text>
  </comment>
  <dbReference type="EMBL" id="L33970">
    <property type="protein sequence ID" value="AAA98526.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A01570">
    <property type="entry name" value="PCBO"/>
  </dbReference>
  <dbReference type="PDB" id="1BBA">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=30-65"/>
  </dbReference>
  <dbReference type="PDB" id="1LJV">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=30-65"/>
  </dbReference>
  <dbReference type="PDB" id="1V1D">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=30-60"/>
  </dbReference>
  <dbReference type="PDBsum" id="1BBA"/>
  <dbReference type="PDBsum" id="1LJV"/>
  <dbReference type="PDBsum" id="1V1D"/>
  <dbReference type="AlphaFoldDB" id="P01302"/>
  <dbReference type="BMRB" id="P01302"/>
  <dbReference type="SMR" id="P01302"/>
  <dbReference type="STRING" id="9913.ENSBTAP00000040571"/>
  <dbReference type="InParanoid" id="P01302"/>
  <dbReference type="EvolutionaryTrace" id="P01302"/>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031841">
    <property type="term" value="F:neuropeptide Y receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd00126">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.900">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001955">
    <property type="entry name" value="Pancreatic_hormone-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020392">
    <property type="entry name" value="Pancreatic_hormone-like_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533">
    <property type="entry name" value="NEUROPEPTIDE Y/PANCREATIC HORMONE/PEPTIDE YY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533:SF2">
    <property type="entry name" value="PANCREATIC PROHORMONE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00159">
    <property type="entry name" value="Hormone_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00278">
    <property type="entry name" value="PANCHORMONE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00309">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00265">
    <property type="entry name" value="PANCREATIC_HORMONE_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50276">
    <property type="entry name" value="PANCREATIC_HORMONE_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000025355" description="Pancreatic polypeptide">
    <location>
      <begin position="30"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000025356" description="C-terminal peptide 1" evidence="2">
    <location>
      <begin position="69"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000025357" description="C-terminal peptide 2" evidence="2">
    <location>
      <begin position="93"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="3">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="38"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="45"/>
      <end position="60"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01298"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="1734969"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="1BBA"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="1LJV"/>
    </source>
  </evidence>
  <sequence length="131" mass="14376" checksum="DCDFE1011C67DF9B" modified="1996-10-01" version="2" precursor="true">MAAAHRCLFLLLLSTCVALLLQPPLGALGAPLEPEYPGDNATPEQMAQYAAELRRYINMLTRPRYGKRDKEGTLDFLECGSPHSAVPRYGKRDKEGTLDFLECGSPHSAVPRWVFSLSCVPRCLGQENGGV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>