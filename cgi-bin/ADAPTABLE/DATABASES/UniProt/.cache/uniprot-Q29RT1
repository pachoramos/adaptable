<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-15" modified="2024-11-27" version="92" xmlns="http://uniprot.org/uniprot">
  <accession>Q29RT1</accession>
  <name>LYZL6_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>Lysozyme-like protein 6</fullName>
      <ecNumber>3.2.1.17</ecNumber>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">LYZL6</name>
  </gene>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2006-02" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="NIH - Mammalian Gene Collection (MGC) project"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Hereford</strain>
      <tissue>Testis</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">May be involved sperm-egg plasma membrane adhesion and fusion during fertilization. Exhibits bacteriolytic activity in vitro against Micrococcus luteus and Staphylococcus aureus. Shows weak bacteriolytic activity against Gram-positive bacteria at physiological pH. Bacteriolytic activity is pH-dependent, with a maximum at around pH 5.6 (By similarity).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>Hydrolysis of (1-&gt;4)-beta-linkages between N-acetylmuramic acid and N-acetyl-D-glucosamine residues in a peptidoglycan and between N-acetyl-D-glucosamine residues in chitodextrins.</text>
      <dbReference type="EC" id="3.2.1.17"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="5">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cell surface</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Cell projection</location>
      <location evidence="2">Cilium</location>
      <location evidence="2">Flagellum</location>
    </subcellularLocation>
    <text evidence="2">Detected in the post-acrosomal area and midpiece of mature spermatozoa.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the glycosyl hydrolase 22 family.</text>
  </comment>
  <dbReference type="EC" id="3.2.1.17"/>
  <dbReference type="EMBL" id="BC114039">
    <property type="protein sequence ID" value="AAI14040.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001039931.1">
    <property type="nucleotide sequence ID" value="NM_001046466.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q29RT1"/>
  <dbReference type="SMR" id="Q29RT1"/>
  <dbReference type="STRING" id="9913.ENSBTAP00000056667"/>
  <dbReference type="CAZy" id="GH22">
    <property type="family name" value="Glycoside Hydrolase Family 22"/>
  </dbReference>
  <dbReference type="PaxDb" id="9913-ENSBTAP00000000032"/>
  <dbReference type="GeneID" id="540048"/>
  <dbReference type="KEGG" id="bta:540048"/>
  <dbReference type="CTD" id="57151"/>
  <dbReference type="eggNOG" id="ENOG502SCGK">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="Q29RT1"/>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001669">
    <property type="term" value="C:acrosomal vesicle"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009986">
    <property type="term" value="C:cell surface"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0036126">
    <property type="term" value="C:sperm flagellum"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097225">
    <property type="term" value="C:sperm midpiece"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097524">
    <property type="term" value="C:sperm plasma membrane"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003796">
    <property type="term" value="F:lysozyme activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009566">
    <property type="term" value="P:fertilization"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007342">
    <property type="term" value="P:fusion of sperm to egg plasma membrane involved in single fertilization"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd16897">
    <property type="entry name" value="LYZ_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.530.10:FF:000001">
    <property type="entry name" value="Lysozyme C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.530.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001916">
    <property type="entry name" value="Glyco_hydro_22"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019799">
    <property type="entry name" value="Glyco_hydro_22_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000974">
    <property type="entry name" value="Glyco_hydro_22_lys"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023346">
    <property type="entry name" value="Lysozyme-like_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407">
    <property type="entry name" value="LYSOZYME C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407:SF9">
    <property type="entry name" value="LYSOZYME-LIKE PROTEIN 6"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00062">
    <property type="entry name" value="Lys"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00137">
    <property type="entry name" value="LYSOZYME"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00135">
    <property type="entry name" value="LYZLACT"/>
  </dbReference>
  <dbReference type="SMART" id="SM00263">
    <property type="entry name" value="LYZ1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF53955">
    <property type="entry name" value="Lysozyme-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00128">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51348">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0081">Bacteriolytic enzyme</keyword>
  <keyword id="KW-0966">Cell projection</keyword>
  <keyword id="KW-0969">Cilium</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0278">Fertilization</keyword>
  <keyword id="KW-0282">Flagellum</keyword>
  <keyword id="KW-0326">Glycosidase</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000287125" description="Lysozyme-like protein 6">
    <location>
      <begin position="20"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="domain" description="C-type lysozyme" evidence="4">
    <location>
      <begin position="20"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="active site" evidence="4">
    <location>
      <position position="54"/>
    </location>
  </feature>
  <feature type="active site" evidence="4">
    <location>
      <position position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="25"/>
      <end position="145"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="49"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="83"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4">
    <location>
      <begin position="94"/>
      <end position="112"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="O75951"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q9DA11"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000255" key="4">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00680"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="148" mass="16559" checksum="DCE7222314F37402" modified="2006-04-04" version="1" precursor="true">MTSPLLISLASCLVAVNQASLIGRCDLAKVLHQEDLDGFEGYSLTDWLCLAFVESDFNITKVNENTDGSFDYGIFQINSHYWCNDYQSRTENNCQVDCQELLSPNLLAIINCAKKIVSGAGGMKNWVKWRLHCAGRPLSYWMTGCHLA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>