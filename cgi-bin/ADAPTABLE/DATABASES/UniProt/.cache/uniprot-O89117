<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2024-11-27" version="135" xmlns="http://uniprot.org/uniprot">
  <accession>O89117</accession>
  <name>DEFB1_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 1</fullName>
      <shortName>BD-1</shortName>
      <shortName>rBD-1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Defb1</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1998-09" db="EMBL/GenBank/DDBJ databases">
      <title>Rat beta defensin-1 peptide: candidate marker for diabetic nephropathy.</title>
      <authorList>
        <person name="Page R.A."/>
        <person name="Malik A.N."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>Goto-Kakizaki</strain>
      <tissue>Kidney</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1999" name="Infect. Immun." volume="67" first="4827" last="4833">
      <title>Molecular cloning and characterization of rat genes encoding homologues of human beta-defensins.</title>
      <authorList>
        <person name="Jia H.P."/>
        <person name="Mills J.N."/>
        <person name="Barahmand-Pour F."/>
        <person name="Nishimura D."/>
        <person name="Mallampali R.K."/>
        <person name="Wang G."/>
        <person name="Wiles K."/>
        <person name="Tack B.F."/>
        <person name="Bevins C.L."/>
        <person name="McCray P.B. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="10456937"/>
      <dbReference type="DOI" id="10.1128/iai.67.9.4827-4833.1999"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <strain>Wistar</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Has bactericidal activity. May act as a ligand for C-C chemokine receptor CCR6. Positively regulates the sperm motility and bactericidal activity in a CCR6-dependent manner. Binds to CCR6 and triggers Ca2+ mobilization in the sperm which is important for its motility.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Monomer. Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Membrane</location>
    </subcellularLocation>
    <text evidence="2">Associates with tumor cell membrane-derived microvesicles.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Highly expressed in kidney.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF093536">
    <property type="protein sequence ID" value="AAC61871.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF068860">
    <property type="protein sequence ID" value="AAC28071.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_113998.1">
    <property type="nucleotide sequence ID" value="NM_031810.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O89117"/>
  <dbReference type="SMR" id="O89117"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000018428"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000018428"/>
  <dbReference type="Ensembl" id="ENSRNOT00000018427.5">
    <property type="protein sequence ID" value="ENSRNOP00000018428.2"/>
    <property type="gene ID" value="ENSRNOG00000013768.5"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055013977">
    <property type="protein sequence ID" value="ENSRNOP00055011204"/>
    <property type="gene ID" value="ENSRNOG00055008262"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060041134">
    <property type="protein sequence ID" value="ENSRNOP00060034063"/>
    <property type="gene ID" value="ENSRNOG00060023759"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065014374">
    <property type="protein sequence ID" value="ENSRNOP00065010770"/>
    <property type="gene ID" value="ENSRNOG00065008999"/>
  </dbReference>
  <dbReference type="GeneID" id="83687"/>
  <dbReference type="KEGG" id="rno:83687"/>
  <dbReference type="UCSC" id="RGD:619943">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:619943"/>
  <dbReference type="CTD" id="1672"/>
  <dbReference type="RGD" id="619943">
    <property type="gene designation" value="Defb1"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502TDMV">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000166542"/>
  <dbReference type="HOGENOM" id="CLU_189296_1_0_1"/>
  <dbReference type="InParanoid" id="O89117"/>
  <dbReference type="OMA" id="CPLFTRI"/>
  <dbReference type="OrthoDB" id="4335232at2759"/>
  <dbReference type="PhylomeDB" id="O89117"/>
  <dbReference type="Reactome" id="R-RNO-1461957">
    <property type="pathway name" value="Beta defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-1461973">
    <property type="pathway name" value="Defensins"/>
  </dbReference>
  <dbReference type="PRO" id="PR:O89117"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 16"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000013768">
    <property type="expression patterns" value="Expressed in kidney and 13 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990742">
    <property type="term" value="C:microvesicle"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097225">
    <property type="term" value="C:sperm midpiece"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019722">
    <property type="term" value="P:calcium-mediated signaling"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060474">
    <property type="term" value="P:positive regulation of flagellated sperm motility involved in capacitation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009617">
    <property type="term" value="P:response to bacterium"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033574">
    <property type="term" value="P:response to testosterone"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.360.10:FF:000001">
    <property type="entry name" value="Beta-defensin 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388:SF9">
    <property type="entry name" value="BETA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21388">
    <property type="entry name" value="BETA-DEFENSIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000006959" evidence="1">
    <location>
      <begin position="22"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006960" description="Beta-defensin 1">
    <location>
      <begin position="33"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="49"/>
      <end position="67"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P60022"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="10456937"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="69" mass="7837" checksum="66B1F0C29BB5C991" modified="1998-11-01" version="1" precursor="true">MKTHYFLLVMLFFLFSQMELGAGILTSLGRRTDQYRCLQNGGFCLRSSCPSHTKLQGTCKPDKPNCCRS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>