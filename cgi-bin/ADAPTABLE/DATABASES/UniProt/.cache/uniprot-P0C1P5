<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-07-25" modified="2024-11-27" version="78" xmlns="http://uniprot.org/uniprot">
  <accession>P0C1P5</accession>
  <name>VPR_HV1M2</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein Vpr</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">R ORF protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Viral protein R</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">vpr</name>
  </gene>
  <organism>
    <name type="scientific">Human immunodeficiency virus type 1 group M subtype F2 (isolate MP257)</name>
    <name type="common">HIV-1</name>
    <dbReference type="NCBI Taxonomy" id="388823"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Pararnavirae</taxon>
      <taxon>Artverviricota</taxon>
      <taxon>Revtraviricetes</taxon>
      <taxon>Ortervirales</taxon>
      <taxon>Retroviridae</taxon>
      <taxon>Orthoretrovirinae</taxon>
      <taxon>Lentivirus</taxon>
      <taxon>Human immunodeficiency virus type 1</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="2000" name="AIDS Res. Hum. Retroviruses" volume="16" first="139" last="151">
      <title>Near-full-length genome sequencing of divergent African HIV type 1 subtype F viruses leads to the identification of a new HIV type 1 subtype designated K.</title>
      <authorList>
        <person name="Triques K."/>
        <person name="Bourgeois A."/>
        <person name="Vidale N."/>
        <person name="Mpoudi-Ngole E."/>
        <person name="Mulanga-Kabeya C."/>
        <person name="Nzilambi N."/>
        <person name="Torimiro N."/>
        <person name="Saman E."/>
        <person name="Delaporte E."/>
        <person name="Peeters M."/>
      </authorList>
      <dbReference type="PubMed" id="10659053"/>
      <dbReference type="DOI" id="10.1089/088922200309485"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">During virus replication, may deplete host UNG protein, and incude G2-M cell cycle arrest. Acts by targeting specific host proteins for degradation by the 26S proteasome, through association with the cellular CUL4A-DDB1 E3 ligase complex by direct interaction with host VPRPB/DCAF-1. Cell cycle arrest reportedly occurs within hours of infection and is not blocked by antiviral agents, suggesting that it is initiated by the VPR carried into the virion. Additionally, VPR induces apoptosis in a cell cycle dependent manner suggesting that these two effects are mechanistically linked. Detected in the serum and cerebrospinal fluid of AIDS patient, VPR may also induce cell death to bystander cells.</text>
  </comment>
  <comment type="function">
    <text evidence="1">During virus entry, plays a role in the transport of the viral pre-integration (PIC) complex to the host nucleus. This function is crucial for viral infection of non-dividing macrophages. May act directly at the nuclear pore complex, by binding nucleoporins phenylalanine-glycine (FG)-repeat regions.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homooligomer, may form homodimer. Interacts with p6-gag region of the Pr55 Gag precursor protein through a (Leu-X-X)4 motif near the C-terminus of the P6gag protein. Interacts with host UNG. May interact with host RAD23A/HHR23A. Interacts with host VPRBP/DCAF1, leading to hijack the CUL4A-RBX1-DDB1-DCAF1/VPRBP complex, mediating ubiquitination of host proteins such as TERT and ZGPAT and arrest of the cell cycle in G2 phase.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Virion</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host extracellular space</location>
    </subcellularLocation>
    <text evidence="1">Incorporation into virion is dependent on p6 GAG sequences. Lacks a canonical nuclear localization signal, thus import into nucleus may function independently of the human importin pathway. Detected in high quantity in the serum and cerebrospinal fluid of AIDS patient.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Phosphorylated on several residues by host. These phosphorylations regulate VPR activity for the nuclear import of the HIV-1 pre-integration complex.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">HIV-1 lineages are divided in three main groups, M (for Major), O (for Outlier), and N (for New, or Non-M, Non-O). The vast majority of strains found worldwide belong to the group M. Group O seems to be endemic to and largely confined to Cameroon and neighboring countries in West Central Africa, where these viruses represent a small minority of HIV-1 strains. The group N is represented by a limited number of isolates from Cameroonian persons. The group M is further subdivided in 9 clades or subtypes (A to D, F to H, J and K).</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the HIV-1 VPR protein family.</text>
  </comment>
  <dbReference type="EMBL" id="AJ249237">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="SMR" id="P0C1P5"/>
  <dbReference type="Proteomes" id="UP000121652">
    <property type="component" value="Segment"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043657">
    <property type="term" value="C:host cell"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="GOC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042025">
    <property type="term" value="C:host cell nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043655">
    <property type="term" value="C:host extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044423">
    <property type="term" value="C:virion component"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006351">
    <property type="term" value="P:DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034220">
    <property type="term" value="P:monoatomic ion transmembrane transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051260">
    <property type="term" value="P:protein homooligomerization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006355">
    <property type="term" value="P:regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046718">
    <property type="term" value="P:symbiont entry into host cell"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052151">
    <property type="term" value="P:symbiont-mediated activation of host apoptosis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039592">
    <property type="term" value="P:symbiont-mediated arrest of host cell cycle during G2/M transition"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0075732">
    <property type="term" value="P:viral penetration into host nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.210.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.5.90">
    <property type="entry name" value="VpR/VpX protein, C-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_04080">
    <property type="entry name" value="HIV_VPR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000012">
    <property type="entry name" value="RetroV_VpR/X"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00522">
    <property type="entry name" value="VPR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00444">
    <property type="entry name" value="HIVVPRVPX"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0010">Activator</keyword>
  <keyword id="KW-0014">AIDS</keyword>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-0131">Cell cycle</keyword>
  <keyword id="KW-1079">Host G2/M cell cycle arrest by virus</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-0407">Ion channel</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-1121">Modulation of host cell cycle by virus</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <keyword id="KW-1163">Viral penetration into host nucleus</keyword>
  <keyword id="KW-0946">Virion</keyword>
  <keyword id="KW-1160">Virus entry into host cell</keyword>
  <feature type="chain" id="PRO_0000246760" description="Protein Vpr">
    <location>
      <begin position="1"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="region of interest" description="Homooligomerization" evidence="1">
    <location>
      <begin position="1"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by host" evidence="1">
    <location>
      <position position="79"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by host" evidence="1">
    <location>
      <position position="96"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_04080"/>
    </source>
  </evidence>
  <sequence length="96" mass="11492" checksum="7ED8B5E9D86D3E5B" modified="2006-07-25" version="1">MEQPPEDQGPQREPYNEWTLELLEELKHEAVRHFPREWLHGLGQHIYNTYGDTWEGVEAIIRILQQLLFIHFRIGCHHSRIGIIRQRRIRNGSGRS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>