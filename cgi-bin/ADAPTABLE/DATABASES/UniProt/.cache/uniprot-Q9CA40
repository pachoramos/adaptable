<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-07-05" modified="2024-11-27" version="149" xmlns="http://uniprot.org/uniprot">
  <accession>Q9CA40</accession>
  <name>NUDT1_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Nudix hydrolase 1</fullName>
      <shortName>AtNUDT1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>7,8-dihydro-8-oxoguanine-triphosphatase</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>8-oxo-dGTP diphosphatase</fullName>
      <shortName>8-oxo-dGTPase</shortName>
      <ecNumber>3.6.1.55</ecNumber>
    </alternativeName>
    <alternativeName>
      <fullName>Dihydroneopterin triphosphate diphosphatase</fullName>
      <ecNumber evidence="4">3.6.1.67</ecNumber>
    </alternativeName>
    <alternativeName>
      <fullName>Dihydroneopterin triphosphate pyrophosphohydrolase</fullName>
      <shortName>DHNTP pyrophosphohydrolase</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>NADH pyrophosphatase</fullName>
      <ecNumber>3.6.1.22</ecNumber>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">NUDT1</name>
    <name type="synonym">NUDX1</name>
    <name type="ordered locus">At1g68760</name>
    <name type="ORF">F14K14.13</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Nature" volume="408" first="816" last="820">
      <title>Sequence and analysis of chromosome 1 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
        <person name="Palm C.J."/>
        <person name="Federspiel N.A."/>
        <person name="Kaul S."/>
        <person name="White O."/>
        <person name="Alonso J."/>
        <person name="Altafi H."/>
        <person name="Araujo R."/>
        <person name="Bowman C.L."/>
        <person name="Brooks S.Y."/>
        <person name="Buehler E."/>
        <person name="Chan A."/>
        <person name="Chao Q."/>
        <person name="Chen H."/>
        <person name="Cheuk R.F."/>
        <person name="Chin C.W."/>
        <person name="Chung M.K."/>
        <person name="Conn L."/>
        <person name="Conway A.B."/>
        <person name="Conway A.R."/>
        <person name="Creasy T.H."/>
        <person name="Dewar K."/>
        <person name="Dunn P."/>
        <person name="Etgu P."/>
        <person name="Feldblyum T.V."/>
        <person name="Feng J.-D."/>
        <person name="Fong B."/>
        <person name="Fujii C.Y."/>
        <person name="Gill J.E."/>
        <person name="Goldsmith A.D."/>
        <person name="Haas B."/>
        <person name="Hansen N.F."/>
        <person name="Hughes B."/>
        <person name="Huizar L."/>
        <person name="Hunter J.L."/>
        <person name="Jenkins J."/>
        <person name="Johnson-Hopson C."/>
        <person name="Khan S."/>
        <person name="Khaykin E."/>
        <person name="Kim C.J."/>
        <person name="Koo H.L."/>
        <person name="Kremenetskaia I."/>
        <person name="Kurtz D.B."/>
        <person name="Kwan A."/>
        <person name="Lam B."/>
        <person name="Langin-Hooper S."/>
        <person name="Lee A."/>
        <person name="Lee J.M."/>
        <person name="Lenz C.A."/>
        <person name="Li J.H."/>
        <person name="Li Y.-P."/>
        <person name="Lin X."/>
        <person name="Liu S.X."/>
        <person name="Liu Z.A."/>
        <person name="Luros J.S."/>
        <person name="Maiti R."/>
        <person name="Marziali A."/>
        <person name="Militscher J."/>
        <person name="Miranda M."/>
        <person name="Nguyen M."/>
        <person name="Nierman W.C."/>
        <person name="Osborne B.I."/>
        <person name="Pai G."/>
        <person name="Peterson J."/>
        <person name="Pham P.K."/>
        <person name="Rizzo M."/>
        <person name="Rooney T."/>
        <person name="Rowley D."/>
        <person name="Sakano H."/>
        <person name="Salzberg S.L."/>
        <person name="Schwartz J.R."/>
        <person name="Shinn P."/>
        <person name="Southwick A.M."/>
        <person name="Sun H."/>
        <person name="Tallon L.J."/>
        <person name="Tambunga G."/>
        <person name="Toriumi M.J."/>
        <person name="Town C.D."/>
        <person name="Utterback T."/>
        <person name="Van Aken S."/>
        <person name="Vaysberg M."/>
        <person name="Vysotskaia V.S."/>
        <person name="Walker M."/>
        <person name="Wu D."/>
        <person name="Yu G."/>
        <person name="Fraser C.M."/>
        <person name="Venter J.C."/>
        <person name="Davis R.W."/>
      </authorList>
      <dbReference type="PubMed" id="11130712"/>
      <dbReference type="DOI" id="10.1038/35048500"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Science" volume="296" first="141" last="145">
      <title>Functional annotation of a full-length Arabidopsis cDNA collection.</title>
      <authorList>
        <person name="Seki M."/>
        <person name="Narusaka M."/>
        <person name="Kamiya A."/>
        <person name="Ishida J."/>
        <person name="Satou M."/>
        <person name="Sakurai T."/>
        <person name="Nakajima M."/>
        <person name="Enju A."/>
        <person name="Akiyama K."/>
        <person name="Oono Y."/>
        <person name="Muramatsu M."/>
        <person name="Hayashizaki Y."/>
        <person name="Kawai J."/>
        <person name="Carninci P."/>
        <person name="Itoh M."/>
        <person name="Ishii Y."/>
        <person name="Arakawa T."/>
        <person name="Shibata K."/>
        <person name="Shinagawa A."/>
        <person name="Shinozaki K."/>
      </authorList>
      <dbReference type="PubMed" id="11910074"/>
      <dbReference type="DOI" id="10.1126/science.1071006"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2003" name="Science" volume="302" first="842" last="846">
      <title>Empirical analysis of transcriptional activity in the Arabidopsis genome.</title>
      <authorList>
        <person name="Yamada K."/>
        <person name="Lim J."/>
        <person name="Dale J.M."/>
        <person name="Chen H."/>
        <person name="Shinn P."/>
        <person name="Palm C.J."/>
        <person name="Southwick A.M."/>
        <person name="Wu H.C."/>
        <person name="Kim C.J."/>
        <person name="Nguyen M."/>
        <person name="Pham P.K."/>
        <person name="Cheuk R.F."/>
        <person name="Karlin-Newmann G."/>
        <person name="Liu S.X."/>
        <person name="Lam B."/>
        <person name="Sakano H."/>
        <person name="Wu T."/>
        <person name="Yu G."/>
        <person name="Miranda M."/>
        <person name="Quach H.L."/>
        <person name="Tripp M."/>
        <person name="Chang C.H."/>
        <person name="Lee J.M."/>
        <person name="Toriumi M.J."/>
        <person name="Chan M.M."/>
        <person name="Tang C.C."/>
        <person name="Onodera C.S."/>
        <person name="Deng J.M."/>
        <person name="Akiyama K."/>
        <person name="Ansari Y."/>
        <person name="Arakawa T."/>
        <person name="Banh J."/>
        <person name="Banno F."/>
        <person name="Bowser L."/>
        <person name="Brooks S.Y."/>
        <person name="Carninci P."/>
        <person name="Chao Q."/>
        <person name="Choy N."/>
        <person name="Enju A."/>
        <person name="Goldsmith A.D."/>
        <person name="Gurjal M."/>
        <person name="Hansen N.F."/>
        <person name="Hayashizaki Y."/>
        <person name="Johnson-Hopson C."/>
        <person name="Hsuan V.W."/>
        <person name="Iida K."/>
        <person name="Karnes M."/>
        <person name="Khan S."/>
        <person name="Koesema E."/>
        <person name="Ishida J."/>
        <person name="Jiang P.X."/>
        <person name="Jones T."/>
        <person name="Kawai J."/>
        <person name="Kamiya A."/>
        <person name="Meyers C."/>
        <person name="Nakajima M."/>
        <person name="Narusaka M."/>
        <person name="Seki M."/>
        <person name="Sakurai T."/>
        <person name="Satou M."/>
        <person name="Tamse R."/>
        <person name="Vaysberg M."/>
        <person name="Wallender E.K."/>
        <person name="Wong C."/>
        <person name="Yamamura Y."/>
        <person name="Yuan S."/>
        <person name="Shinozaki K."/>
        <person name="Davis R.W."/>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
      </authorList>
      <dbReference type="PubMed" id="14593172"/>
      <dbReference type="DOI" id="10.1126/science.1088305"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="submission" date="2004-09" db="EMBL/GenBank/DDBJ databases">
      <title>Large-scale analysis of RIKEN Arabidopsis full-length (RAFL) cDNAs.</title>
      <authorList>
        <person name="Totoki Y."/>
        <person name="Seki M."/>
        <person name="Ishida J."/>
        <person name="Nakajima M."/>
        <person name="Enju A."/>
        <person name="Kamiya A."/>
        <person name="Narusaka M."/>
        <person name="Shin-i T."/>
        <person name="Nakagawa M."/>
        <person name="Sakamoto N."/>
        <person name="Oishi K."/>
        <person name="Kohara Y."/>
        <person name="Kobayashi M."/>
        <person name="Toyoda A."/>
        <person name="Sakaki Y."/>
        <person name="Sakurai T."/>
        <person name="Iida K."/>
        <person name="Akiyama K."/>
        <person name="Satou M."/>
        <person name="Toyoda T."/>
        <person name="Konagaya A."/>
        <person name="Carninci P."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
        <person name="Shinozaki K."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="submission" date="2002-03" db="EMBL/GenBank/DDBJ databases">
      <title>Full-length cDNA from Arabidopsis thaliana.</title>
      <authorList>
        <person name="Brover V.V."/>
        <person name="Troukhan M.E."/>
        <person name="Alexandrov N.A."/>
        <person name="Lu Y.-P."/>
        <person name="Flavell R.B."/>
        <person name="Feldmann K.A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2002" name="J. Biol. Chem." volume="277" first="50482" last="50486">
      <title>Cloning and characterization of the first member of the Nudix family from Arabidopsis thaliana.</title>
      <authorList>
        <person name="Dobrzanska M."/>
        <person name="Szurmak B."/>
        <person name="Wyslouch-Cieszynska A."/>
        <person name="Kraszewska E."/>
      </authorList>
      <dbReference type="PubMed" id="12399474"/>
      <dbReference type="DOI" id="10.1074/jbc.m205207200"/>
    </citation>
    <scope>NADH PYROPHOSPHATASE ACTIVITY</scope>
    <scope>SUBUNIT</scope>
    <scope>COFACTOR</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2005" name="J. Biol. Chem." volume="280" first="5274" last="5280">
      <title>A nudix enzyme removes pyrophosphate from dihydroneopterin triphosphate in the folate synthesis pathway of bacteria and plants.</title>
      <authorList>
        <person name="Klaus S.M.J."/>
        <person name="Wegkamp A."/>
        <person name="Sybesma W."/>
        <person name="Hugenholtz J."/>
        <person name="Gregory J.F. III"/>
        <person name="Hanson A.D."/>
      </authorList>
      <dbReference type="PubMed" id="15611104"/>
      <dbReference type="DOI" id="10.1074/jbc.m413759200"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY AS DIHYDRONEOPTERIN PYROPHOSPHATASE</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2005" name="J. Biol. Chem." volume="280" first="25277" last="25283">
      <title>Comprehensive analysis of cytosolic nudix hydrolases in Arabidopsis thaliana.</title>
      <authorList>
        <person name="Ogawa T."/>
        <person name="Ueda Y."/>
        <person name="Yoshimura K."/>
        <person name="Shigeoka S."/>
      </authorList>
      <dbReference type="PubMed" id="15878881"/>
      <dbReference type="DOI" id="10.1074/jbc.m503536200"/>
    </citation>
    <scope>FUNCTION IN VITRO</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>COFACTOR</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2007" name="Plant Cell Physiol." volume="48" first="1438" last="1449">
      <title>AtNUDX1, an 8-oxo-7,8-dihydro-2'-deoxyguanosine 5'-triphosphate pyrophosphohydrolase, is responsible for eliminating oxidized nucleotides in Arabidopsis.</title>
      <authorList>
        <person name="Yoshimura K."/>
        <person name="Ogawa T."/>
        <person name="Ueda Y."/>
        <person name="Shigeoka S."/>
      </authorList>
      <dbReference type="PubMed" id="17804481"/>
      <dbReference type="DOI" id="10.1093/pcp/pcm112"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2012" name="Mol. Cell. Proteomics" volume="11" first="M111.015131" last="M111.015131">
      <title>Comparative large-scale characterisation of plant vs. mammal proteins reveals similar and idiosyncratic N-alpha acetylation features.</title>
      <authorList>
        <person name="Bienvenut W.V."/>
        <person name="Sumpton D."/>
        <person name="Martinez A."/>
        <person name="Lilla S."/>
        <person name="Espagne C."/>
        <person name="Meinnel T."/>
        <person name="Giglione C."/>
      </authorList>
      <dbReference type="PubMed" id="22223895"/>
      <dbReference type="DOI" id="10.1074/mcp.m111.015131"/>
    </citation>
    <scope>ACETYLATION [LARGE SCALE ANALYSIS] AT SER-2</scope>
    <scope>CLEAVAGE OF INITIATOR METHIONINE [LARGE SCALE ANALYSIS]</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <comment type="function">
    <text evidence="4 5 6">Mediates the hydrolysis of some nucleoside diphosphate derivatives. Its substrate specificity is unclear. In vitro, it can use NTP, dNTP, 8-oxo-GTP, 8-oxo-dGTP, dGTP, dATP, dTTP or dihydroneopterin triphosphate (DHNTP) as substrate. Has some NADH pyrophosphatase activity in vitro; however, such activity may not be relevant in vivo due to the high concentration of manganese used during the experiments. Plays an important role in protection against oxidative DNA and RNA damage by removing oxidatively damaged form of guanine.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="4">
      <text>7,8-dihydroneopterin 3'-triphosphate + H2O = 7,8-dihydroneopterin 3'-phosphate + diphosphate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:25302"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:33019"/>
      <dbReference type="ChEBI" id="CHEBI:58462"/>
      <dbReference type="ChEBI" id="CHEBI:58762"/>
      <dbReference type="EC" id="3.6.1.67"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="6">
      <text>NAD(+) + H2O = beta-nicotinamide D-ribonucleotide + AMP + 2 H(+)</text>
      <dbReference type="Rhea" id="RHEA:11800"/>
      <dbReference type="ChEBI" id="CHEBI:14649"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:57540"/>
      <dbReference type="ChEBI" id="CHEBI:456215"/>
      <dbReference type="EC" id="3.6.1.22"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="6">
      <text>NADH + H2O = reduced beta-nicotinamide D-ribonucleotide + AMP + 2 H(+)</text>
      <dbReference type="Rhea" id="RHEA:48868"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:57945"/>
      <dbReference type="ChEBI" id="CHEBI:90832"/>
      <dbReference type="ChEBI" id="CHEBI:456215"/>
      <dbReference type="EC" id="3.6.1.22"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="6">
      <text>8-oxo-dGTP + H2O = 8-oxo-dGMP + diphosphate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:31575"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:33019"/>
      <dbReference type="ChEBI" id="CHEBI:63224"/>
      <dbReference type="ChEBI" id="CHEBI:77896"/>
      <dbReference type="EC" id="3.6.1.55"/>
    </reaction>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="3 5">
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </cofactor>
    <cofactor evidence="3 5">
      <name>Mn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29035"/>
    </cofactor>
    <text evidence="3 5">Magnesium may be the real cofactor in vivo.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <kinetics>
      <KM evidence="3 5 6">0.36 mM for NADH (in the presence of 5 mM of manganese)</KM>
      <KM evidence="3 5 6">0.48 mM for NAD (in the presence of 5 mM of manganese)</KM>
      <KM evidence="3 5 6">6.8 uM for 8-oxo-dGTP</KM>
      <KM evidence="3 5 6">28.1 uM for 8-oxo-GTP</KM>
      <KM evidence="3 5 6">58.3 uM for dGTP</KM>
      <KM evidence="3 5 6">16.1 uM for dATP</KM>
      <KM evidence="3 5 6">15.6 uM for dTTP</KM>
      <Vmax evidence="3 5 6">12.7 umol/min/mg enzyme toward NADH (in the presence of 5 mM of manganese)</Vmax>
      <Vmax evidence="3 5 6">0.62 umol/min/mg enzyme toward NAD (in the presence of 5 mM of manganese)</Vmax>
      <Vmax evidence="3 5 6">0.8 umol/min/mg enzyme toward 8-oxo-dGTP</Vmax>
      <Vmax evidence="3 5 6">1.7 umol/min/mg enzyme toward 8-oxo-GTP</Vmax>
      <Vmax evidence="3 5 6">2.7 umol/min/mg enzyme toward dGTP</Vmax>
      <Vmax evidence="3 5 6">5.8 umol/min/mg enzyme toward dATP</Vmax>
      <Vmax evidence="3 5 6">0.8 umol/min/mg enzyme toward dTTP</Vmax>
    </kinetics>
  </comment>
  <comment type="subunit">
    <text evidence="3">Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed in roots, stems and leaves.</text>
  </comment>
  <comment type="induction">
    <text evidence="6">Not induced by paraquat, salinity, high light and drought.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="6">Increased level of 8-oxo-G in genomic DNA.</text>
  </comment>
  <comment type="miscellaneous">
    <text>Has the ability to complement a mutation of mutT in E.coli and thereby completely suppress the increased frequency of spontaneous mutations.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the Nudix hydrolase family.</text>
  </comment>
  <dbReference type="EC" id="3.6.1.55"/>
  <dbReference type="EC" id="3.6.1.67" evidence="4"/>
  <dbReference type="EC" id="3.6.1.22"/>
  <dbReference type="EMBL" id="AC011914">
    <property type="protein sequence ID" value="AAG52038.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002684">
    <property type="protein sequence ID" value="AEE34836.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK117564">
    <property type="protein sequence ID" value="BAC42225.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT005039">
    <property type="protein sequence ID" value="AAO50572.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK176885">
    <property type="protein sequence ID" value="BAD44648.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY088162">
    <property type="protein sequence ID" value="AAM65706.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="D96712">
    <property type="entry name" value="D96712"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_177044.1">
    <property type="nucleotide sequence ID" value="NM_105549.4"/>
  </dbReference>
  <dbReference type="PDB" id="5GP0">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A/E/F/I=1-147"/>
  </dbReference>
  <dbReference type="PDB" id="5WWD">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.39 A"/>
    <property type="chains" value="A/B=1-147"/>
  </dbReference>
  <dbReference type="PDB" id="5WY6">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.78 A"/>
    <property type="chains" value="A/E=1-147"/>
  </dbReference>
  <dbReference type="PDB" id="6DBY">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="A/B=1-147"/>
  </dbReference>
  <dbReference type="PDB" id="6DBZ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.90 A"/>
    <property type="chains" value="A/B=1-147"/>
  </dbReference>
  <dbReference type="PDB" id="6FL4">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="A/B=1-147"/>
  </dbReference>
  <dbReference type="PDBsum" id="5GP0"/>
  <dbReference type="PDBsum" id="5WWD"/>
  <dbReference type="PDBsum" id="5WY6"/>
  <dbReference type="PDBsum" id="6DBY"/>
  <dbReference type="PDBsum" id="6DBZ"/>
  <dbReference type="PDBsum" id="6FL4"/>
  <dbReference type="AlphaFoldDB" id="Q9CA40"/>
  <dbReference type="SMR" id="Q9CA40"/>
  <dbReference type="STRING" id="3702.Q9CA40"/>
  <dbReference type="iPTMnet" id="Q9CA40"/>
  <dbReference type="PaxDb" id="3702-AT1G68760.1"/>
  <dbReference type="ProteomicsDB" id="249351"/>
  <dbReference type="EnsemblPlants" id="AT1G68760.1">
    <property type="protein sequence ID" value="AT1G68760.1"/>
    <property type="gene ID" value="AT1G68760"/>
  </dbReference>
  <dbReference type="GeneID" id="843207"/>
  <dbReference type="Gramene" id="AT1G68760.1">
    <property type="protein sequence ID" value="AT1G68760.1"/>
    <property type="gene ID" value="AT1G68760"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT1G68760"/>
  <dbReference type="Araport" id="AT1G68760"/>
  <dbReference type="TAIR" id="AT1G68760">
    <property type="gene designation" value="NUDX1"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S3YT">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_037162_9_2_1"/>
  <dbReference type="InParanoid" id="Q9CA40"/>
  <dbReference type="OMA" id="HFEASRN"/>
  <dbReference type="OrthoDB" id="2914433at2759"/>
  <dbReference type="PhylomeDB" id="Q9CA40"/>
  <dbReference type="BioCyc" id="ARA:AT1G68760-MONOMER"/>
  <dbReference type="BioCyc" id="MetaCyc:AT1G68760-MONOMER"/>
  <dbReference type="BRENDA" id="3.6.1.55">
    <property type="organism ID" value="399"/>
  </dbReference>
  <dbReference type="BRENDA" id="3.6.1.67">
    <property type="organism ID" value="399"/>
  </dbReference>
  <dbReference type="SABIO-RK" id="Q9CA40"/>
  <dbReference type="PRO" id="PR:Q9CA40"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q9CA40">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035539">
    <property type="term" value="F:8-oxo-7,8-dihydrodeoxyguanosine triphosphate pyrophosphatase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008413">
    <property type="term" value="F:8-oxo-7,8-dihydroguanosine triphosphate pyrophosphatase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019177">
    <property type="term" value="F:dihydroneopterin triphosphate pyrophosphohydrolase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000210">
    <property type="term" value="F:NAD+ diphosphatase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035529">
    <property type="term" value="F:NADH pyrophosphatase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006974">
    <property type="term" value="P:DNA damage response"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="CDD" id="cd04678">
    <property type="entry name" value="Nudix_Hydrolase_19"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.90.79.10:FF:000034">
    <property type="entry name" value="Nucleotide triphosphate diphosphatase NUDT15"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.90.79.10">
    <property type="entry name" value="Nucleoside Triphosphate Pyrophosphohydrolase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020476">
    <property type="entry name" value="Nudix_hydrolase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015797">
    <property type="entry name" value="NUDIX_hydrolase-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020084">
    <property type="entry name" value="NUDIX_hydrolase_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000086">
    <property type="entry name" value="NUDIX_hydrolase_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16099">
    <property type="entry name" value="8-OXO-DGTP DIPHOSPHATES NUDT15"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16099:SF5">
    <property type="entry name" value="NUCLEOTIDE TRIPHOSPHATE DIPHOSPHATASE NUDT15"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00293">
    <property type="entry name" value="NUDIX"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00502">
    <property type="entry name" value="NUDIXFAMILY"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55811">
    <property type="entry name" value="Nudix"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51462">
    <property type="entry name" value="NUDIX"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00893">
    <property type="entry name" value="NUDIX_BOX"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0460">Magnesium</keyword>
  <keyword id="KW-0464">Manganese</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0520">NAD</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="initiator methionine" description="Removed" evidence="8">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000057121" description="Nudix hydrolase 1">
    <location>
      <begin position="2"/>
      <end position="147"/>
    </location>
  </feature>
  <feature type="domain" description="Nudix hydrolase" evidence="2">
    <location>
      <begin position="7"/>
      <end position="140"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Nudix box">
    <location>
      <begin position="41"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="56"/>
    </location>
    <ligand>
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="60"/>
    </location>
    <ligand>
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </ligand>
  </feature>
  <feature type="modified residue" description="N-acetylserine" evidence="8">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="9"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="21"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="29"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="39"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="helix" evidence="9">
    <location>
      <begin position="49"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="65"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="81"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="85"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="turn" evidence="9">
    <location>
      <begin position="108"/>
      <end position="110"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="111"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="119"/>
      <end position="121"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="124"/>
      <end position="126"/>
    </location>
  </feature>
  <feature type="helix" evidence="9">
    <location>
      <begin position="128"/>
      <end position="135"/>
    </location>
  </feature>
  <feature type="turn" evidence="9">
    <location>
      <begin position="140"/>
      <end position="142"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00794"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12399474"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="15611104"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="15878881"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="17804481"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0007744" key="8">
    <source>
      <dbReference type="PubMed" id="22223895"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="9">
    <source>
      <dbReference type="PDB" id="5WWD"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="10">
    <source>
      <dbReference type="PDB" id="6FL4"/>
    </source>
  </evidence>
  <sequence length="147" mass="16357" checksum="84EDDAF6D65247C9" modified="2001-06-01" version="1">MSTGEAIPRVAVVVFILNGNSILLGRRRSSIGNSTFALPGGHLEFGESFEECAAREVMEETGLKIEKMKLLTVTNNVFKEAPTPSHYVSVSIRAVLVDPSQEPKNMEPEKCEGWDWYDWENLPKPLFWPLEKLFGSGFNPFTHGGGD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>