<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-13" modified="2024-11-27" version="63" xmlns="http://uniprot.org/uniprot">
  <accession>Q963A9</accession>
  <name>CECB2_AEDAL</name>
  <protein>
    <recommendedName>
      <fullName>Cecropin-B type 2</fullName>
      <shortName>Cecropin-B2</shortName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Cecropin-B</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>AalCecB</fullName>
      </alternativeName>
    </component>
    <component>
      <recommendedName>
        <fullName>Cecropin-B amidated isoform</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">CECB2</name>
  </gene>
  <organism>
    <name type="scientific">Aedes albopictus</name>
    <name type="common">Asian tiger mosquito</name>
    <name type="synonym">Stegomyia albopicta</name>
    <dbReference type="NCBI Taxonomy" id="7160"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Nematocera</taxon>
      <taxon>Culicoidea</taxon>
      <taxon>Culicidae</taxon>
      <taxon>Culicinae</taxon>
      <taxon>Aedini</taxon>
      <taxon>Aedes</taxon>
      <taxon>Stegomyia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2001-06" db="EMBL/GenBank/DDBJ databases">
      <title>Characterization of genomic DNA encoding mosquito cecropins.</title>
      <authorList>
        <person name="Sun D."/>
        <person name="Fallon A.M."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1999" name="FEBS Lett." volume="454" first="147" last="151">
      <title>Cloning and expression of three cecropin cDNAs from a mosquito cell line.</title>
      <authorList>
        <person name="Sun D."/>
        <person name="Eccleston E.D."/>
        <person name="Fallon A.M."/>
      </authorList>
      <dbReference type="PubMed" id="10413113"/>
      <dbReference type="DOI" id="10.1016/s0014-5793(99)00799-1"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 25-59</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT ILE-58</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Cecropins have lytic and antibacterial activity against several Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="3642.2" method="MALDI" evidence="2">
    <molecule>Cecropin-B</molecule>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the cecropin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF394746">
    <property type="protein sequence ID" value="AAK81851.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q963A9"/>
  <dbReference type="SMR" id="Q963A9"/>
  <dbReference type="VEuPathDB" id="VectorBase:AALFPA_043934"/>
  <dbReference type="Proteomes" id="UP000069940">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000875">
    <property type="entry name" value="Cecropin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020400">
    <property type="entry name" value="Cecropin_insect"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR38329">
    <property type="entry name" value="CECROPIN-A1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR38329:SF1">
    <property type="entry name" value="CECROPIN-A1-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00272">
    <property type="entry name" value="Cecropin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004816" description="Cecropin-B">
    <location>
      <begin position="25"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004817" description="Cecropin-B amidated isoform">
    <location>
      <begin position="25"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="modified residue" description="Isoleucine amide" evidence="2">
    <location>
      <position position="58"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10413113"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="60" mass="6345" checksum="1F34B1DC4BAACADB" modified="2001-12-01" version="1" precursor="true">MNFSKLFALVLLIGLVLLTGQTEAGGLKKLGKKLEGVGKRVFKASEKALPVLTGYKAIGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>