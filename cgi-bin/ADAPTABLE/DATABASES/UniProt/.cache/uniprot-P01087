<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="130" xmlns="http://uniprot.org/uniprot">
  <accession>P01087</accession>
  <name>IAAT_ELECO</name>
  <protein>
    <recommendedName>
      <fullName>Alpha-amylase/trypsin inhibitor</fullName>
      <shortName>RBI</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>RATI</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Eleusine coracana</name>
    <name type="common">Indian finger millet</name>
    <name type="synonym">Ragi</name>
    <dbReference type="NCBI Taxonomy" id="4511"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>Liliopsida</taxon>
      <taxon>Poales</taxon>
      <taxon>Poaceae</taxon>
      <taxon>PACMAD clade</taxon>
      <taxon>Chloridoideae</taxon>
      <taxon>Cynodonteae</taxon>
      <taxon>Eleusininae</taxon>
      <taxon>Eleusine</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1983" name="FEBS Lett." volume="152" first="300" last="304">
      <title>The complete amino acid sequence of the bifunctional alpha-amylase/trypsin inhibitor from seeds of ragi (Indian finger millet, Eleusine coracana Gaertn.).</title>
      <authorList>
        <person name="Campos F.A.P."/>
        <person name="Richardson M."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="Structure" volume="6" first="911" last="921">
      <title>A novel strategy for inhibition of alpha-amylases: yellow meal worm alpha-amylase in complex with the Ragi bifunctional inhibitor at 2.5-A resolution.</title>
      <authorList>
        <person name="Strobl S."/>
        <person name="Maskos K."/>
        <person name="Wiegand G."/>
        <person name="Huber R."/>
        <person name="Gomis-Rueth F.-X."/>
        <person name="Glockshuber R."/>
      </authorList>
      <dbReference type="PubMed" id="9687373"/>
      <dbReference type="DOI" id="10.1016/s0969-2126(98)00092-6"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.5 ANGSTROMS)</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1999" name="Acta Crystallogr. D" volume="55" first="25" last="30">
      <title>Structure of the bifunctional inhibitor of trypsin and alpha-amylase from ragi seeds at 2.9-A resolution.</title>
      <authorList>
        <person name="Gourinath S."/>
        <person name="Srinivasan A."/>
        <person name="Singh T.P."/>
      </authorList>
      <dbReference type="PubMed" id="10089391"/>
      <dbReference type="DOI" id="10.1107/s0907444998006271"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.9 ANGSTROMS)</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2000" name="Acta Crystallogr. D" volume="56" first="287" last="293">
      <title>Structure of the bifunctional inhibitor of trypsin and alpha-amylase from ragi seeds at 2.2 A resolution.</title>
      <authorList>
        <person name="Gourinath S."/>
        <person name="Alam N."/>
        <person name="Srinivasan A."/>
        <person name="Betzel C."/>
        <person name="Singh T.P."/>
      </authorList>
      <dbReference type="PubMed" id="10713515"/>
      <dbReference type="DOI" id="10.1107/s0907444999016601"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.2 ANGSTROMS)</scope>
    <scope>DISULFIDE BONDS</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1995" name="Biochemistry" volume="34" first="8281" last="8293">
      <title>Determination of the three-dimensional structure of the bifunctional alpha-amylase/trypsin inhibitor from ragi seeds by NMR spectroscopy.</title>
      <authorList>
        <person name="Strobl S."/>
        <person name="Muehlhahn P."/>
        <person name="Bernstein R."/>
        <person name="Wiltscheck R."/>
        <person name="Maskos K."/>
        <person name="Wunderlich M."/>
        <person name="Huber R."/>
        <person name="Glockshuber R."/>
        <person name="Holak T.A."/>
      </authorList>
      <dbReference type="PubMed" id="7599120"/>
      <dbReference type="DOI" id="10.1021/bi00026a009"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>May play a protective role against endo- and exogenous hydrolytic activities in the Ragi seeds.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Seeds.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the protease inhibitor I6 (cereal trypsin/alpha-amylase inhibitor) family.</text>
  </comment>
  <dbReference type="PIR" id="A01326">
    <property type="entry name" value="WIILAI"/>
  </dbReference>
  <dbReference type="PDB" id="1B1U">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.20 A"/>
    <property type="chains" value="A=1-122"/>
  </dbReference>
  <dbReference type="PDB" id="1BIP">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-122"/>
  </dbReference>
  <dbReference type="PDB" id="1TMQ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="B=1-117"/>
  </dbReference>
  <dbReference type="PDBsum" id="1B1U"/>
  <dbReference type="PDBsum" id="1BIP"/>
  <dbReference type="PDBsum" id="1TMQ"/>
  <dbReference type="AlphaFoldDB" id="P01087"/>
  <dbReference type="SMR" id="P01087"/>
  <dbReference type="MINT" id="P01087"/>
  <dbReference type="MEROPS" id="I06.003"/>
  <dbReference type="EvolutionaryTrace" id="P01087"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015066">
    <property type="term" value="F:alpha-amylase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00261">
    <property type="entry name" value="AAI_SS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.110.10">
    <property type="entry name" value="Plant lipid-transfer and hydrophobic proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006106">
    <property type="entry name" value="Allergen/soft/tryp_amyl_inhib"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006105">
    <property type="entry name" value="Allergen/tryp_amyl_inhib_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036312">
    <property type="entry name" value="Bifun_inhib/LTP/seed_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016140">
    <property type="entry name" value="Bifunc_inhib/LTP/seed_store"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34481">
    <property type="entry name" value="TRYPSIN/FACTOR XIIA INHIBITOR-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34481:SF2">
    <property type="entry name" value="TRYPSIN_FACTOR XIIA INHIBITOR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00234">
    <property type="entry name" value="Tryp_alpha_amyl"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00808">
    <property type="entry name" value="AMLASEINHBTR"/>
  </dbReference>
  <dbReference type="SMART" id="SM00499">
    <property type="entry name" value="AAI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47699">
    <property type="entry name" value="Bifunctional inhibitor/lipid-transfer protein/seed storage 2S albumin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00426">
    <property type="entry name" value="CEREAL_TRYP_AMYL_INH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0022">Alpha-amylase inhibitor</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <feature type="chain" id="PRO_0000070490" description="Alpha-amylase/trypsin inhibitor">
    <location>
      <begin position="1"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="6"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="20"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="29"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="45"/>
      <end position="103"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 3">
    <location>
      <begin position="57"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>ST</original>
    <variation>AK</variation>
    <location>
      <begin position="25"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>T</original>
    <variation>A</variation>
    <location>
      <position position="28"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>P</original>
    <variation>S</variation>
    <location>
      <position position="70"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="2"/>
      <end position="5"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="9"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="16"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="19"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="32"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="37"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="53"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="56"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="87"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="94"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="100"/>
      <end position="103"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="110"/>
      <end position="112"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="115"/>
      <end position="117"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10713515"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007744" key="3">
    <source>
      <dbReference type="PDB" id="1B1U"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="1B1U"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="5">
    <source>
      <dbReference type="PDB" id="1BIP"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="1TMQ"/>
    </source>
  </evidence>
  <sequence length="122" mass="13138" checksum="C8ED7A01CD470E17" modified="1998-07-15" version="2">SVGTSCIPGMAIPHNPLDSCRWYVSTRTCGVGPRLATQEMKARCCRQLEAIPAYCRCEAVRILMDGVVTPSGQHEGRLLQDLPGCPRQVQRAFAPKLVTEVECNLATIHGGPFCLSLLGAGE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>