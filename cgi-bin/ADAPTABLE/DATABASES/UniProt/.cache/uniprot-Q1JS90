<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-12-12" modified="2024-03-27" version="42" xmlns="http://uniprot.org/uniprot">
  <accession>Q1JS90</accession>
  <name>RN2B_ODOVE</name>
  <protein>
    <recommendedName>
      <fullName>Ranatuerin-2Vb</fullName>
      <shortName>ranat2Vb</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>2VEb</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Odorrana versabilis</name>
    <name type="common">Chinese bamboo leaf odorous frog</name>
    <name type="synonym">Rana versabilis</name>
    <dbReference type="NCBI Taxonomy" id="326940"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Odorrana</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Peptides" volume="27" first="1738" last="1744">
      <title>The Chinese bamboo leaf odorous frog (Rana (odorrana) versabilis) and north american rana frogs share the same families of skin antimicrobial peptides.</title>
      <authorList>
        <person name="Chen T."/>
        <person name="Zhou M."/>
        <person name="Rao P."/>
        <person name="Walker B."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="16621152"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2006.02.009"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2862.68" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Ranatuerin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AM113511">
    <property type="protein sequence ID" value="CAJ34607.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q1JS90"/>
  <dbReference type="SMR" id="Q1JS90"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000268217">
    <location>
      <begin position="23"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000268218" description="Ranatuerin-2Vb">
    <location>
      <begin position="40"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="62"/>
      <end position="67"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="16621152"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="67" mass="7344" checksum="9AD3C305BC875930" modified="2006-06-13" version="1" precursor="true">MFTLKKSFLLLFFLGTITLSLCEEERGADDDDGEEEVKRGIMDTVKGVAKTVAASLLDKLKCKITGC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>