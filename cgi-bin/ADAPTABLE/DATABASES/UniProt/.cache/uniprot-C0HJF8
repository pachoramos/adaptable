<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2014-01-22" modified="2023-11-08" version="15" xmlns="http://uniprot.org/uniprot">
  <accession>C0HJF8</accession>
  <name>LNGP_ACULO</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Longipin</fullName>
    </recommendedName>
  </protein>
  <organism evidence="2">
    <name type="scientific">Acutisoma longipes</name>
    <name type="common">Neotropical harvestman</name>
    <name type="synonym">Goniosoma longipes</name>
    <dbReference type="NCBI Taxonomy" id="863974"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Opiliones</taxon>
      <taxon>Laniatores</taxon>
      <taxon>Grassatores</taxon>
      <taxon>Gonyleptoidea</taxon>
      <taxon>Gonyleptidae</taxon>
      <taxon>Acutisoma</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2016" name="PLoS ONE" volume="11" first="E0167953" last="E0167953">
      <title>Longipin: An Amyloid Antimicrobial Peptide from the Harvestman Acutisoma longipes (Arachnida: Opiliones) with Preferential Affinity for Anionic Vesicles.</title>
      <authorList>
        <person name="Sayegh R.S."/>
        <person name="Batista I.F."/>
        <person name="Melo R.L."/>
        <person name="Riske K.A."/>
        <person name="Daffre S."/>
        <person name="Montich G."/>
        <person name="da Silva Junior P.I."/>
      </authorList>
      <dbReference type="PubMed" id="27997568"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0167953"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="2">Hemolymph</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antifungal activity against yeasts like C.albicans MDM8 (MIC=15-30 uM), C.albicans IOC 4558 (MIC=7.5-15 uM), C.tropicalis IOC 4560 (MIC=3.8-7.5 uM) and C.guilliermondii IOC 4557 (MIC=3.8-7.5 uM) but not against filamentous fungi like A.niger, C.herbarum ATCC26362 or P.farinosus IBC251. Has weak activity against the Gram-positive bacterium M.luteus A270 (MIC=60-120 uM) and against Gram-negative bacteria P.aeruginosa ATCC27853 (MIC=60-120 uM) and S.marcescens ATCC4112 (MIC=60-120 uM). Not active against S.aureus ATCC 29213 and S.epidermidis ATCC 12228, M.luteus BR2, E.coli D31, E.coli SBS363 or E.cloacae beta12. Probably acts by disrupting target cell membranes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Target cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text evidence="1">Constitutively expressed.</text>
  </comment>
  <comment type="mass spectrometry" mass="2124.9" method="MALDI" evidence="1"/>
  <dbReference type="AlphaFoldDB" id="C0HJF8"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <feature type="peptide" id="PRO_0000425148" description="Longipin" evidence="1">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="27997568"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="27997568"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="18" mass="2126" checksum="3E57BDE183F51D19" modified="2014-01-22" version="1">SGYLPGKEYVYKYKGKVF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>