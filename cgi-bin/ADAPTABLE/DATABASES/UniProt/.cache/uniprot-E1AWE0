<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-05-10" modified="2023-05-03" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>E1AWE0</accession>
  <name>TP3_AMOCU</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Temporin-CG3</fullName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Amolops chunganensis</name>
    <name type="common">Chungan torrent frog</name>
    <name type="synonym">Hylorana chunganensis</name>
    <dbReference type="NCBI Taxonomy" id="325556"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Amolops</taxon>
    </lineage>
  </organism>
  <reference evidence="6" key="1">
    <citation type="journal article" date="2012" name="Peptides" volume="38" first="41" last="53">
      <title>Characterization of diverse antimicrobial peptides in skin secretions of Chungan torrent frog Amolops chunganensis.</title>
      <authorList>
        <person name="Yang X."/>
        <person name="Xia J."/>
        <person name="Yu Z."/>
        <person name="Hu Y."/>
        <person name="Li F."/>
        <person name="Meng H."/>
        <person name="Yang S."/>
        <person name="Liu J."/>
        <person name="Wang H."/>
      </authorList>
      <dbReference type="PubMed" id="22951323"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2012.08.008"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Antimicrobial peptide active against a variety of Gram-positive bacterial strains but not against Gram-negative bacteria. Has weak antifungal activity against a slime mold isolate. Has weak hemolytic activity against human erythrocytes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Temporin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="HQ009836">
    <property type="protein sequence ID" value="ADM34212.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="E1AWE0"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439744" description="Removed in mature form" evidence="5">
    <location>
      <begin position="23"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000439745" description="Temporin-CG3" evidence="3">
    <location>
      <begin position="47"/>
      <end position="61"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="22951323"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="22951323"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="22951323"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="ADM34212.1"/>
    </source>
  </evidence>
  <sequence length="61" mass="7125" checksum="9C0559924281AEF8" modified="2010-11-02" version="1" precursor="true">MFTMKKPLLLLFFLATINLSLCEQERNAEEERRDEPDERNAEVEKRFLPIVGKLLSGLFGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>