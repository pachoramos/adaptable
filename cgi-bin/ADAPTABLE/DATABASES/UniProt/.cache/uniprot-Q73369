<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-10-17" modified="2024-11-27" version="123" xmlns="http://uniprot.org/uniprot">
  <accession>Q73369</accession>
  <name>VPR_HV1B9</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein Vpr</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">R ORF protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Viral protein R</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">vpr</name>
  </gene>
  <organism>
    <name type="scientific">Human immunodeficiency virus type 1 group M subtype B (strain 89.6)</name>
    <name type="common">HIV-1</name>
    <dbReference type="NCBI Taxonomy" id="401671"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Pararnavirae</taxon>
      <taxon>Artverviricota</taxon>
      <taxon>Revtraviricetes</taxon>
      <taxon>Ortervirales</taxon>
      <taxon>Retroviridae</taxon>
      <taxon>Orthoretrovirinae</taxon>
      <taxon>Lentivirus</taxon>
      <taxon>Human immunodeficiency virus type 1</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1992" name="J. Virol." volume="66" first="7517" last="7521">
      <title>An infectious molecular clone of an unusual macrophage-tropic and highly cytopathic strain of human immunodeficiency virus type 1.</title>
      <authorList>
        <person name="Collman R."/>
        <person name="Balliet J.W."/>
        <person name="Gregory S.A."/>
        <person name="Friedman H."/>
        <person name="Kolson D.L."/>
        <person name="Nathanson N."/>
        <person name="Srinivasan A."/>
      </authorList>
      <dbReference type="PubMed" id="1433527"/>
      <dbReference type="DOI" id="10.1128/jvi.66.12.7517-7521.1992"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1994" name="J. Biol. Chem." volume="269" first="15577" last="15582">
      <title>Biochemical mechanism of HIV-I Vpr function. Specific interaction with a cellular protein.</title>
      <authorList>
        <person name="Zhao L.-J."/>
        <person name="Mukherjee S."/>
        <person name="Narayan O."/>
      </authorList>
      <dbReference type="PubMed" id="8195203"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)40719-8"/>
    </citation>
    <scope>INTERACTION WITH HUMAN COPS6/HVIP</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1994" name="J. Biol. Chem." volume="269" first="32131" last="32137">
      <title>Biochemical mechanism of HIV-1 Vpr function. Oligomerization mediated by the N-terminal domain.</title>
      <authorList>
        <person name="Zhao L.J."/>
        <person name="Wang L."/>
        <person name="Mukherjee S."/>
        <person name="Narayan O."/>
      </authorList>
      <dbReference type="PubMed" id="7798208"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)31610-7"/>
    </citation>
    <scope>HOMOOLIGOMERIZATION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2001" name="Gene" volume="263" first="131" last="140">
      <title>Cytoplasmic retention of HIV-1 regulatory protein Vpr by protein-protein interaction with a novel human cytoplasmic protein VprBP.</title>
      <authorList>
        <person name="Zhang S."/>
        <person name="Feng Y."/>
        <person name="Narayan O."/>
        <person name="Zhao L.-J."/>
      </authorList>
      <dbReference type="PubMed" id="11223251"/>
      <dbReference type="DOI" id="10.1016/s0378-1119(00)00583-7"/>
    </citation>
    <scope>INTERACTION WITH HUMAN DCAF1</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2007" name="Cell Cycle" volume="6" first="182" last="188">
      <title>HIV1 Vpr arrests the cell cycle by recruiting DCAF1/VprBP, a receptor of the Cul4-DDB1 ubiquitin ligase.</title>
      <authorList>
        <person name="Le Rouzic E."/>
        <person name="Belaiedouni N."/>
        <person name="Estrabaud E."/>
        <person name="Morel M."/>
        <person name="Rain J.-C."/>
        <person name="Transy C."/>
        <person name="Margottin-Goguet F."/>
      </authorList>
      <dbReference type="PubMed" id="17314515"/>
      <dbReference type="DOI" id="10.4161/cc.6.2.3732"/>
    </citation>
    <scope>INTERACTION WITH HUMAN DCAF1</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1999" name="Eur. J. Biochem." volume="266" first="359" last="369">
      <title>NMR structure of the (1-51) N-terminal domain of the HIV-1 regulatory protein Vpr.</title>
      <authorList>
        <person name="Wecker K."/>
        <person name="Roques B.P."/>
      </authorList>
      <dbReference type="PubMed" id="10561576"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.1999.00858.x"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 1-51</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2002" name="Eur. J. Biochem." volume="269" first="3779" last="3788">
      <title>NMR structure of the HIV-1 regulatory protein Vpr in H2O/trifluoroethanol. Comparison with the Vpr N-terminal (1-51) and C-terminal (52-96) domains.</title>
      <authorList>
        <person name="Wecker K."/>
        <person name="Morellet N."/>
        <person name="Bouaziz S."/>
        <person name="Roques B.P."/>
      </authorList>
      <dbReference type="PubMed" id="12153575"/>
      <dbReference type="DOI" id="10.1046/j.1432-1033.2002.03067.x"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2003" name="J. Mol. Biol." volume="327" first="215" last="227">
      <title>NMR structure of the HIV-1 regulatory protein VPR.</title>
      <authorList>
        <person name="Morellet N."/>
        <person name="Bouaziz S."/>
        <person name="Petitjean P."/>
        <person name="Roques B.P."/>
      </authorList>
      <dbReference type="PubMed" id="12614620"/>
      <dbReference type="DOI" id="10.1016/s0022-2836(03)00060-3"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="1999" name="J. Mol. Biol." volume="285" first="2105" last="2117">
      <title>NMR structure of the (52-96) C-terminal domain of the HIV-1 regulatory protein Vpr: molecular insights into its biological functions.</title>
      <authorList>
        <person name="Schuler W."/>
        <person name="Wecker K."/>
        <person name="de Rocquigny H."/>
        <person name="Baudat Y."/>
        <person name="Sire J."/>
        <person name="Roques B.P."/>
      </authorList>
      <dbReference type="PubMed" id="9925788"/>
      <dbReference type="DOI" id="10.1006/jmbi.1998.2381"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 52-96</scope>
  </reference>
  <comment type="function">
    <text evidence="1">During virus replication, may deplete host UNG protein, and incude G2-M cell cycle arrest. Acts by targeting specific host proteins for degradation by the 26S proteasome, through association with the cellular CUL4A-DDB1 E3 ligase complex by direct interaction with host VPRPB/DCAF-1. Cell cycle arrest reportedly occurs within hours of infection and is not blocked by antiviral agents, suggesting that it is initiated by the VPR carried into the virion. Additionally, VPR induces apoptosis in a cell cycle dependent manner suggesting that these two effects are mechanistically linked. Detected in the serum and cerebrospinal fluid of AIDS patient, VPR may also induce cell death to bystander cells.</text>
  </comment>
  <comment type="function">
    <text evidence="1">During virus entry, plays a role in the transport of the viral pre-integration (PIC) complex to the host nucleus. This function is crucial for viral infection of non-dividing macrophages. May act directly at the nuclear pore complex, by binding nucleoporins phenylalanine-glycine (FG)-repeat regions.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homooligomer, may form homodimer. Interacts with p6-gag region of the Pr55 Gag precursor protein through a (Leu-X-X)4 motif near the C-terminus of the P6gag protein. Interacts with host UNG. May interact with host RAD23A/HHR23A. Interacts with host VPRBP/DCAF1, leading to hijack the CUL4A-RBX1-DDB1-DCAF1/VPRBP complex, mediating ubiquitination of host proteins such as TERT and ZGPAT and arrest of the cell cycle in G2 phase.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Virion</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host extracellular space</location>
    </subcellularLocation>
    <text evidence="1">Incorporation into virion is dependent on p6 GAG sequences. Lacks a canonical nuclear localization signal, thus import into nucleus may function independently of the human importin pathway. Detected in high quantity in the serum and cerebrospinal fluid of AIDS patient.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Phosphorylated on several residues by host. These phosphorylations regulate VPR activity for the nuclear import of the HIV-1 pre-integration complex.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">HIV-1 lineages are divided in three main groups, M (for Major), O (for Outlier), and N (for New, or Non-M, Non-O). The vast majority of strains found worldwide belong to the group M. Group O seems to be endemic to and largely confined to Cameroon and neighboring countries in West Central Africa, where these viruses represent a small minority of HIV-1 strains. The group N is represented by a limited number of isolates from Cameroonian persons. The group M is further subdivided in 9 clades or subtypes (A to D, F to H, J and K).</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the HIV-1 VPR protein family.</text>
  </comment>
  <dbReference type="EMBL" id="U39362">
    <property type="protein sequence ID" value="AAA81039.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PDB" id="1CEU">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-51"/>
  </dbReference>
  <dbReference type="PDB" id="1ESX">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-96"/>
  </dbReference>
  <dbReference type="PDB" id="1M8L">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-96"/>
  </dbReference>
  <dbReference type="PDB" id="1VPC">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=52-96"/>
  </dbReference>
  <dbReference type="PDB" id="1X9V">
    <property type="method" value="NMR"/>
    <property type="chains" value="A/B=52-96"/>
  </dbReference>
  <dbReference type="PDB" id="5B56">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="C/D/E/F=85-96"/>
  </dbReference>
  <dbReference type="PDBsum" id="1CEU"/>
  <dbReference type="PDBsum" id="1ESX"/>
  <dbReference type="PDBsum" id="1M8L"/>
  <dbReference type="PDBsum" id="1VPC"/>
  <dbReference type="PDBsum" id="1X9V"/>
  <dbReference type="PDBsum" id="5B56"/>
  <dbReference type="BMRB" id="Q73369"/>
  <dbReference type="SMR" id="Q73369"/>
  <dbReference type="EvolutionaryTrace" id="Q73369"/>
  <dbReference type="Proteomes" id="UP000007691">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043657">
    <property type="term" value="C:host cell"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="GOC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042025">
    <property type="term" value="C:host cell nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043655">
    <property type="term" value="C:host extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044423">
    <property type="term" value="C:virion component"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006351">
    <property type="term" value="P:DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034220">
    <property type="term" value="P:monoatomic ion transmembrane transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051260">
    <property type="term" value="P:protein homooligomerization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006355">
    <property type="term" value="P:regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046718">
    <property type="term" value="P:symbiont entry into host cell"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052151">
    <property type="term" value="P:symbiont-mediated activation of host apoptosis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039592">
    <property type="term" value="P:symbiont-mediated arrest of host cell cycle during G2/M transition"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0075732">
    <property type="term" value="P:viral penetration into host nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.5.90:FF:000001">
    <property type="entry name" value="Protein Vpr"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.210.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.5.90">
    <property type="entry name" value="VpR/VpX protein, C-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_04080">
    <property type="entry name" value="HIV_VPR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000012">
    <property type="entry name" value="RetroV_VpR/X"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00522">
    <property type="entry name" value="VPR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00444">
    <property type="entry name" value="HIVVPRVPX"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0010">Activator</keyword>
  <keyword id="KW-0014">AIDS</keyword>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-0131">Cell cycle</keyword>
  <keyword id="KW-1079">Host G2/M cell cycle arrest by virus</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-0407">Ion channel</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-1121">Modulation of host cell cycle by virus</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <keyword id="KW-1163">Viral penetration into host nucleus</keyword>
  <keyword id="KW-0946">Virion</keyword>
  <keyword id="KW-1160">Virus entry into host cell</keyword>
  <feature type="chain" id="PRO_0000253570" description="Protein Vpr">
    <location>
      <begin position="1"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="region of interest" description="Homooligomerization" evidence="1">
    <location>
      <begin position="1"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by host" evidence="1">
    <location>
      <position position="79"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by host" evidence="1">
    <location>
      <position position="94"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine; by host" evidence="1">
    <location>
      <position position="96"/>
    </location>
  </feature>
  <feature type="turn" evidence="2">
    <location>
      <begin position="7"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="helix" evidence="2">
    <location>
      <begin position="17"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="turn" evidence="2">
    <location>
      <begin position="30"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="helix" evidence="2">
    <location>
      <begin position="35"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="50"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="56"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="turn" evidence="3">
    <location>
      <begin position="81"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="85"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="turn" evidence="4">
    <location>
      <begin position="90"/>
      <end position="92"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_04080"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="2">
    <source>
      <dbReference type="PDB" id="1CEU"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="1ESX"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="1X9V"/>
    </source>
  </evidence>
  <sequence length="96" mass="11395" checksum="2D380F934DDEF238" modified="1996-11-01" version="1">MEQAPEDQGPQREPYNDWTLELLEELKNEAVRHFPRIWLHSLGQHIYETYGDTWTGVEALIRILQQLLFIHFRIGCRHSRIGIIQQRRTRNGASKS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>