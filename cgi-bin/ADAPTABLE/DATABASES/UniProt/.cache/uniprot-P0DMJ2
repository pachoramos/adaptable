<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2014-06-11" modified="2024-11-27" version="22" xmlns="http://uniprot.org/uniprot">
  <accession>P0DMJ2</accession>
  <name>VKT3C_ACTEQ</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">PI-actitoxin-Aeq3c</fullName>
      <shortName evidence="5">PI-AITX-Aeq3c</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Kunitz-type protease inhibitor AEAPI</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Actinia equina</name>
    <name type="common">Beadlet anemone</name>
    <dbReference type="NCBI Taxonomy" id="6106"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Actinia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Comp. Biochem. Physiol." volume="150B" first="240" last="245">
      <title>Kunitz-type protease inhibitors from acrorhagi of three species of sea anemones.</title>
      <authorList>
        <person name="Minagawa S."/>
        <person name="Sugiyama M."/>
        <person name="Ishida M."/>
        <person name="Nagashima Y."/>
        <person name="Shiomi K."/>
      </authorList>
      <dbReference type="PubMed" id="18450492"/>
      <dbReference type="DOI" id="10.1016/j.cbpb.2008.03.010"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue>Tentacle</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Serine protease inhibitor that is strongly active against trypsin (700 IU/mg) and moderately active against plasmin.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="6">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by acrorhagi.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="7">Does not inhibit potassium channels (Kv), as well as metalloproteases, and cysteine proteases (papain and bromelain).</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the venom Kunitz-type family. Sea anemone type 2 potassium channel toxin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DMJ2"/>
  <dbReference type="SMR" id="P0DMJ2"/>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.410.10:FF:000021">
    <property type="entry name" value="Serine protease inhibitor, putative"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.410.10">
    <property type="entry name" value="Pancreatic trypsin inhibitor Kunitz domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050098">
    <property type="entry name" value="Kunitz-type_SerProtInhib"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002223">
    <property type="entry name" value="Kunitz_BPTI"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036880">
    <property type="entry name" value="Kunitz_BPTI_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020901">
    <property type="entry name" value="Prtase_inh_Kunz-CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10083">
    <property type="entry name" value="KUNITZ-TYPE PROTEASE INHIBITOR-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10083:SF217">
    <property type="entry name" value="PROTEIN CBG23496"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00014">
    <property type="entry name" value="Kunitz_BPTI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00759">
    <property type="entry name" value="BASICPTASE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00131">
    <property type="entry name" value="KU"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57362">
    <property type="entry name" value="BPTI-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00280">
    <property type="entry name" value="BPTI_KUNITZ_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50279">
    <property type="entry name" value="BPTI_KUNITZ_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <feature type="chain" id="PRO_0000429349" description="PI-actitoxin-Aeq3c" evidence="3">
    <location>
      <begin position="1"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="domain" description="BPTI/Kunitz inhibitor" evidence="2">
    <location>
      <begin position="7"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="site" description="Reactive bond for trypsin" evidence="1">
    <location>
      <begin position="17"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="7"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="16"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="32"/>
      <end position="53"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00031"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18450492"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="18450492"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="18450492"/>
    </source>
  </evidence>
  <sequence length="57" mass="6411" checksum="EECFFB82BD6C8CDF" modified="2014-06-11" version="1">YPANEGCLLPMKVGPCRAAWPSYYYNSKSEKCEEFTYGGCDANANNFQTEEECKKAC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>