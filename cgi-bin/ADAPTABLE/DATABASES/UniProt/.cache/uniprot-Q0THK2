<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-10-02" modified="2024-11-27" version="76" xmlns="http://uniprot.org/uniprot">
  <accession>Q0THK2</accession>
  <name>CNU_ECOL5</name>
  <protein>
    <recommendedName>
      <fullName>OriC-binding nucleoid-associated protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>H-NS/StpA-binding protein 2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Transcription modulator YdgT</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">cnu</name>
    <name type="synonym">ydgT</name>
    <name type="ordered locus">ECP_1570</name>
  </gene>
  <organism>
    <name type="scientific">Escherichia coli O6:K15:H31 (strain 536 / UPEC)</name>
    <dbReference type="NCBI Taxonomy" id="362663"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Escherichia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Mol. Microbiol." volume="61" first="584" last="595">
      <title>Role of pathogenicity island-associated integrases in the genome plasticity of uropathogenic Escherichia coli strain 536.</title>
      <authorList>
        <person name="Hochhut B."/>
        <person name="Wilde C."/>
        <person name="Balling G."/>
        <person name="Middendorf B."/>
        <person name="Dobrindt U."/>
        <person name="Brzuszkiewicz E."/>
        <person name="Gottschalk G."/>
        <person name="Carniel E."/>
        <person name="Hacker J."/>
      </authorList>
      <dbReference type="PubMed" id="16879640"/>
      <dbReference type="DOI" id="10.1111/j.1365-2958.2006.05255.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>536 / UPEC</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Modifies the set of genes regulated by H-NS; Hha and cnu (YdgT) increase the number of genes bound by H-NS/StpA and may also modulate the oligomerization of the H-NS/StpA-complex on DNA. The complex formed with H-NS binds to the specific 26-bp cnb site in the origin of replication oriC.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Forms complexes with both H-NS and StpA.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the Hha/YmoA/Cnu family.</text>
  </comment>
  <dbReference type="EMBL" id="CP000247">
    <property type="protein sequence ID" value="ABG69577.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000217950.1">
    <property type="nucleotide sequence ID" value="NC_008253.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q0THK2"/>
  <dbReference type="SMR" id="Q0THK2"/>
  <dbReference type="GeneID" id="75204470"/>
  <dbReference type="KEGG" id="ecp:ECP_1570"/>
  <dbReference type="HOGENOM" id="CLU_190629_0_0_6"/>
  <dbReference type="Proteomes" id="UP000009182">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.1280.40:FF:000002">
    <property type="entry name" value="OriC-binding nucleoid-associated protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.1280.40">
    <property type="entry name" value="HHA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007985">
    <property type="entry name" value="Hemolysn_expr_modulating_HHA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036666">
    <property type="entry name" value="HHA_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05321">
    <property type="entry name" value="HHA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF68989">
    <property type="entry name" value="Hemolysin expression modulating protein HHA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_0000305050" description="OriC-binding nucleoid-associated protein">
    <location>
      <begin position="1"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="site" description="Interacts with H-NS" evidence="1">
    <location>
      <position position="44"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P64467"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="71" mass="8417" checksum="CE9FF2E890E78219" modified="2006-09-05" version="1">MTVQDYLLKFRKISSLESLEKLYDHLNYTLTDDQELINMYRAADHRRAELVSGGRLFDLGQVPKSVWHYVQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>