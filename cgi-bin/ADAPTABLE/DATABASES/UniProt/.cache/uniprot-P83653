<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-01-16" modified="2022-12-14" version="70" xmlns="http://uniprot.org/uniprot">
  <accession>P83653</accession>
  <name>ALO3_ACRLO</name>
  <protein>
    <recommendedName>
      <fullName>Antimicrobial peptide Alo-3</fullName>
    </recommendedName>
  </protein>
  <organism evidence="2">
    <name type="scientific">Acrocinus longimanus</name>
    <name type="common">Giant harlequin beetle</name>
    <dbReference type="NCBI Taxonomy" id="227548"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Coleoptera</taxon>
      <taxon>Polyphaga</taxon>
      <taxon>Cucujiformia</taxon>
      <taxon>Chrysomeloidea</taxon>
      <taxon>Cerambycidae</taxon>
      <taxon>Lamiinae</taxon>
      <taxon>Acrocinini</taxon>
      <taxon>Acrocinus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Biochemistry" volume="42" first="14434" last="14442">
      <title>Solution structure of Alo-3: a new knottin-type antifungal peptide from the insect Acrocinus longimanus.</title>
      <authorList>
        <person name="Barbault F."/>
        <person name="Landon C."/>
        <person name="Guenneugues M."/>
        <person name="Meyer J.-P."/>
        <person name="Schott V."/>
        <person name="Dimarcq J.-L."/>
        <person name="Vovelle F."/>
      </authorList>
      <dbReference type="PubMed" id="14661954"/>
      <dbReference type="DOI" id="10.1021/bi035400o"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>STRUCTURE BY NMR</scope>
    <source>
      <tissue>Hemolymph</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Has antifungal activity against C.glabrata.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text>The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="3869.4" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the AMP family.</text>
  </comment>
  <dbReference type="PDB" id="1Q3J">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-36"/>
  </dbReference>
  <dbReference type="PDBsum" id="1Q3J"/>
  <dbReference type="AlphaFoldDB" id="P83653"/>
  <dbReference type="SMR" id="P83653"/>
  <dbReference type="EvolutionaryTrace" id="P83653"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013006">
    <property type="entry name" value="Antimicrobial_C6_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009101">
    <property type="entry name" value="Gurmarin/antifun_pep"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR024206">
    <property type="entry name" value="Gurmarin/antimicrobial_peptd"/>
  </dbReference>
  <dbReference type="Pfam" id="PF11410">
    <property type="entry name" value="Antifungal_pept"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57048">
    <property type="entry name" value="Gurmarin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60011">
    <property type="entry name" value="PLANT_C6_AMP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000045066" description="Antimicrobial peptide Alo-3">
    <location>
      <begin position="1"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="8"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="17"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="3"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="26"/>
      <end position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="14661954"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="1Q3J"/>
    </source>
  </evidence>
  <sequence length="36" mass="3875" checksum="32685DDBF11ECC1A" modified="2003-10-01" version="1">CIKNGNGCQPNGSQGNCCSGYCHKQPGWVAGYCRRK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>