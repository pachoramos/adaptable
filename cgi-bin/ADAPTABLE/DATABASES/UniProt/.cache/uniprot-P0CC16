<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-01-19" modified="2024-11-27" version="31" xmlns="http://uniprot.org/uniprot">
  <accession>P0CC16</accession>
  <name>OXLA_AGKCL</name>
  <protein>
    <recommendedName>
      <fullName>L-amino-acid oxidase</fullName>
      <shortName evidence="3">ACL-LAO</shortName>
      <shortName>LAAO</shortName>
      <ecNumber evidence="2">1.4.3.2</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Agkistrodon contortrix laticinctus</name>
    <name type="common">Broad-banded copperhead</name>
    <name type="synonym">Agkistrodon mokasen laticinctus</name>
    <dbReference type="NCBI Taxonomy" id="2782196"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Viperidae</taxon>
      <taxon>Crotalinae</taxon>
      <taxon>Agkistrodon</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Arch. Biochem. Biophys." volume="368" first="285" last="290">
      <title>Isolation and structural characterization of a cytotoxic L-amino acid oxidase from Agkistrodon contortrix laticinctus snake venom: preliminary crystallographic data.</title>
      <authorList>
        <person name="Souza D.H.F."/>
        <person name="Eugenio L.M."/>
        <person name="Fletcher J.E."/>
        <person name="Jiang M.-S."/>
        <person name="Garratt R.C."/>
        <person name="Oliva G."/>
        <person name="Selistre-de-Araujo H.S."/>
      </authorList>
      <dbReference type="PubMed" id="10441379"/>
      <dbReference type="DOI" id="10.1006/abbi.1999.1287"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>COFACTOR</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>SUBSTRATE SPECIFICITY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Catalyzes an oxidative deamination of predominantly hydrophobic and aromatic L-amino acids, thus producing hydrogen peroxide that may contribute to the diverse toxic effects of this enzyme (PubMed:10441379). Is active on L-Ile followed by L-Phe, L-Met, L-Val, L-Arg, L-Leu (PubMed:10441379). Exhibits diverse biological activities, such as hemorrhage (minimum hemorrhagic dose of 10 ug), and apoptosis. May also induce hemolysis, edema, antibacterial and antiparasitic activities. May also regulate platelet aggregation. Effects of snake L-amino oxidases on platelets are controversial, since they either induce aggregation or inhibit agonist-induced aggregation. These different effects are probably due to different experimental conditions.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>an L-alpha-amino acid + O2 + H2O = a 2-oxocarboxylate + H2O2 + NH4(+)</text>
      <dbReference type="Rhea" id="RHEA:13781"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15379"/>
      <dbReference type="ChEBI" id="CHEBI:16240"/>
      <dbReference type="ChEBI" id="CHEBI:28938"/>
      <dbReference type="ChEBI" id="CHEBI:35179"/>
      <dbReference type="ChEBI" id="CHEBI:59869"/>
      <dbReference type="EC" id="1.4.3.2"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>L-leucine + O2 + H2O = 4-methyl-2-oxopentanoate + H2O2 + NH4(+)</text>
      <dbReference type="Rhea" id="RHEA:60996"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15379"/>
      <dbReference type="ChEBI" id="CHEBI:16240"/>
      <dbReference type="ChEBI" id="CHEBI:17865"/>
      <dbReference type="ChEBI" id="CHEBI:28938"/>
      <dbReference type="ChEBI" id="CHEBI:57427"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>L-phenylalanine + O2 + H2O = 3-phenylpyruvate + H2O2 + NH4(+)</text>
      <dbReference type="Rhea" id="RHEA:61240"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15379"/>
      <dbReference type="ChEBI" id="CHEBI:16240"/>
      <dbReference type="ChEBI" id="CHEBI:18005"/>
      <dbReference type="ChEBI" id="CHEBI:28938"/>
      <dbReference type="ChEBI" id="CHEBI:58095"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>L-methionine + O2 + H2O = 4-methylsulfanyl-2-oxobutanoate + H2O2 + NH4(+)</text>
      <dbReference type="Rhea" id="RHEA:61236"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15379"/>
      <dbReference type="ChEBI" id="CHEBI:16240"/>
      <dbReference type="ChEBI" id="CHEBI:16723"/>
      <dbReference type="ChEBI" id="CHEBI:28938"/>
      <dbReference type="ChEBI" id="CHEBI:57844"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>L-isoleucine + O2 + H2O = (S)-3-methyl-2-oxopentanoate + H2O2 + NH4(+)</text>
      <dbReference type="Rhea" id="RHEA:61232"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15379"/>
      <dbReference type="ChEBI" id="CHEBI:16240"/>
      <dbReference type="ChEBI" id="CHEBI:28938"/>
      <dbReference type="ChEBI" id="CHEBI:35146"/>
      <dbReference type="ChEBI" id="CHEBI:58045"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>L-arginine + O2 + H2O = 5-guanidino-2-oxopentanoate + H2O2 + NH4(+)</text>
      <dbReference type="Rhea" id="RHEA:51404"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15379"/>
      <dbReference type="ChEBI" id="CHEBI:16240"/>
      <dbReference type="ChEBI" id="CHEBI:28938"/>
      <dbReference type="ChEBI" id="CHEBI:32682"/>
      <dbReference type="ChEBI" id="CHEBI:58489"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>L-valine + O2 + H2O = 3-methyl-2-oxobutanoate + H2O2 + NH4(+)</text>
      <dbReference type="Rhea" id="RHEA:61252"/>
      <dbReference type="ChEBI" id="CHEBI:11851"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15379"/>
      <dbReference type="ChEBI" id="CHEBI:16240"/>
      <dbReference type="ChEBI" id="CHEBI:28938"/>
      <dbReference type="ChEBI" id="CHEBI:57762"/>
    </reaction>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="2">
      <name>FMN</name>
      <dbReference type="ChEBI" id="CHEBI:58210"/>
    </cofactor>
    <text evidence="2">Does not use FAD as a cofactor.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer; non-covalently linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Contains 2 disulfide bonds.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">N-glycosylated.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the flavin monoamine oxidase family. FIG1 subfamily.</text>
  </comment>
  <dbReference type="EC" id="1.4.3.2" evidence="2"/>
  <dbReference type="AlphaFoldDB" id="P0CC16"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010181">
    <property type="term" value="F:FMN binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001716">
    <property type="term" value="F:L-amino-acid oxidase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0106329">
    <property type="term" value="F:L-phenylalaine oxidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="RHEA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042803">
    <property type="term" value="F:protein homodimerization activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044358">
    <property type="term" value="P:envenomation resulting in hemorrhagic damage in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0285">Flavoprotein</keyword>
  <keyword id="KW-0288">FMN</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-1200">Hemorrhagic toxin</keyword>
  <keyword id="KW-1199">Hemostasis impairing toxin</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="chain" id="PRO_0000390926" description="L-amino-acid oxidase">
    <location>
      <begin position="1"/>
      <end position="19" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="3">
    <location>
      <position position="19"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P81382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10441379"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="10441379"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="10441379"/>
    </source>
  </evidence>
  <sequence length="19" mass="2375" checksum="C1D709886B68E7B5" modified="2010-01-19" version="1" fragment="single">ADSRNPLEEEFRETNYEEF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>