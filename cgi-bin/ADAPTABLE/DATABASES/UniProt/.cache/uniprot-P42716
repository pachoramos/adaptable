<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-07-24" version="54" xmlns="http://uniprot.org/uniprot">
  <accession>P42716</accession>
  <name>MAST_PARID</name>
  <protein>
    <recommendedName>
      <fullName evidence="7">Parapolybia-mastoparan</fullName>
      <shortName evidence="5">Parapolybia-MP</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Parapolybia indica</name>
    <name type="common">Lesser paper wasp</name>
    <dbReference type="NCBI Taxonomy" id="31921"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Polistinae</taxon>
      <taxon>Ropalidiini</taxon>
      <taxon>Parapolybia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1988" name="Eisei Dobutsu" volume="39" first="105" last="111">
      <title>Isolation and sequential analysis of peptides on the venom sac of Parapolybia indica.</title>
      <authorList>
        <person name="Toki T."/>
        <person name="Yasuhara T."/>
        <person name="Nakajima T."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT ILE-14</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Toxicon" volume="45" first="101" last="106">
      <title>Structural and biological characterization of three novel mastoparan peptides from the venom of the neotropical social wasp Protopolybia exigua (Saussure).</title>
      <authorList>
        <person name="Mendes M.A."/>
        <person name="de Souza B.M."/>
        <person name="Palma M.S."/>
      </authorList>
      <dbReference type="PubMed" id="15581688"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2004.09.015"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3">Mast cell degranulating peptide with moderate activity (PubMed:15581688). Has hemolytic activity (PubMed:15581688). Its mast cell degranulation activity may be related to the activation of G-protein coupled receptors in mast cells as well as interaction with other proteins located in cell endosomal membranes in the mast cells (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="6">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="6">Assumes an amphipathic alpha-helical conformation in a membrane-like environment.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="8">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the MCD family. Mastoparan subfamily.</text>
  </comment>
  <dbReference type="TCDB" id="1.C.32.1.1">
    <property type="family name" value="the amphipathic peptide mastoparan (mastoparan) family"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043303">
    <property type="term" value="P:mast cell degranulation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013213">
    <property type="entry name" value="Mastoparan"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08249">
    <property type="entry name" value="Mastoparan"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0467">Mast cell degranulation</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000044053" description="Parapolybia-mastoparan" evidence="4">
    <location>
      <begin position="1"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="modified residue" description="Isoleucine amide" evidence="4">
    <location>
      <position position="14"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01514"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P84914"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15581688"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="15581688"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="15581688"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8">
    <source ref="1"/>
  </evidence>
  <sequence length="14" mass="1619" checksum="CA376CD3BA6D80DD" modified="1995-11-01" version="1">INWKKMAATALKMI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>