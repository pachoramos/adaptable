<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-11-23" modified="2021-02-10" version="22" xmlns="http://uniprot.org/uniprot">
  <accession>P84273</accession>
  <name>DAH11_RANDH</name>
  <protein>
    <recommendedName>
      <fullName>Dahlein-1.1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ranoidea dahlii</name>
    <name type="common">Dahl's aquatic frog</name>
    <name type="synonym">Litoria dahlii</name>
    <dbReference type="NCBI Taxonomy" id="299727"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2001" name="Rapid Commun. Mass Spectrom." volume="15" first="1726" last="1734">
      <title>Bioactive dahlein peptides from the skin secretions of the Australian aquatic frog Litoria dahlii: sequence determination by electrospray mass spectrometry.</title>
      <authorList>
        <person name="Wegener K.L."/>
        <person name="Brinkworth C.S."/>
        <person name="Bowie J.H."/>
        <person name="Wallace J.C."/>
        <person name="Tyler M.J."/>
      </authorList>
      <dbReference type="PubMed" id="11555873"/>
      <dbReference type="DOI" id="10.1002/rcm.429"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="1">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Weak wide spectrum antimicrobial activity against Gram-positive bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Expressed by the skin dorsal glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1430.0" method="Electrospray" evidence="1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013157">
    <property type="entry name" value="Aurein_antimicrobial_peptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08256">
    <property type="entry name" value="Antimicrobial20"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043772" description="Dahlein-1.1">
    <location>
      <begin position="1"/>
      <end position="13"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11555873"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="13" mass="1433" checksum="1048B722EBCF4330" modified="2004-11-23" version="1">GLFDIIKNIVSTL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>