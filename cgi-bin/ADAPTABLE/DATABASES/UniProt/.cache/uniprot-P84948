<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-10-03" modified="2019-12-11" version="15" xmlns="http://uniprot.org/uniprot">
  <accession>P84948</accession>
  <name>TY24_PITAZ</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Tryptophyllin-T2-4</fullName>
      <shortName evidence="3">Pha-T2-4</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Tryptophyllin-8</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Pithecopus azureus</name>
    <name type="common">Orange-legged monkey tree frog</name>
    <name type="synonym">Phyllomedusa azurea</name>
    <dbReference type="NCBI Taxonomy" id="2034991"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Pithecopus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2007" name="J. Proteome Res." volume="6" first="3604" last="3613">
      <title>Amphibian skin secretomics: application of parallel quadrupole time-of-flight mass spectrometry and peptide precursor cDNA cloning to rapidly characterize the skin secretory peptidome of Phyllomedusa hypochondrialis azurea: discovery of a novel peptide family, the hyposins.</title>
      <authorList>
        <person name="Thompson A.H."/>
        <person name="Bjourson A.J."/>
        <person name="Orr D.F."/>
        <person name="Shaw C."/>
        <person name="McClean S."/>
      </authorList>
      <dbReference type="PubMed" id="17696382"/>
      <dbReference type="DOI" id="10.1021/pr0702666"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT MET-5</scope>
    <source>
      <tissue evidence="2">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="675.31" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the frog skin active peptide (FSAP) family. Tryptophillin subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000250416" description="Tryptophyllin-T2-4" evidence="2">
    <location>
      <begin position="1"/>
      <end position="5"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="2">
    <location>
      <position position="5"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="17696382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="17696382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="5" mass="677" checksum="6683677769A00000" modified="2006-10-03" version="1">FPPWM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>