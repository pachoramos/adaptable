<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-10-01" modified="2024-11-27" version="103" xmlns="http://uniprot.org/uniprot">
  <accession>P37523</accession>
  <name>YYAB_BACSU</name>
  <protein>
    <recommendedName>
      <fullName>Uncharacterized protein YyaB</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">yyaB</name>
    <name type="ordered locus">BSU40980</name>
  </gene>
  <organism>
    <name type="scientific">Bacillus subtilis (strain 168)</name>
    <dbReference type="NCBI Taxonomy" id="224308"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Bacillaceae</taxon>
      <taxon>Bacillus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="Mol. Microbiol." volume="6" first="629" last="634">
      <title>Genes and their organization in the replication origin region of the bacterial chromosome.</title>
      <authorList>
        <person name="Ogasawara N."/>
        <person name="Yoshikawa H."/>
      </authorList>
      <dbReference type="PubMed" id="1552862"/>
      <dbReference type="DOI" id="10.1111/j.1365-2958.1992.tb01510.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>168 / CRK2000</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1994" name="DNA Res." volume="1" first="1" last="14">
      <title>Systematic sequencing of the 180 kilobase region of the Bacillus subtilis chromosome containing the replication origin.</title>
      <authorList>
        <person name="Ogasawara N."/>
        <person name="Nakai S."/>
        <person name="Yoshikawa H."/>
      </authorList>
      <dbReference type="PubMed" id="7584024"/>
      <dbReference type="DOI" id="10.1093/dnares/1.1.1"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>168</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1997" name="Nature" volume="390" first="249" last="256">
      <title>The complete genome sequence of the Gram-positive bacterium Bacillus subtilis.</title>
      <authorList>
        <person name="Kunst F."/>
        <person name="Ogasawara N."/>
        <person name="Moszer I."/>
        <person name="Albertini A.M."/>
        <person name="Alloni G."/>
        <person name="Azevedo V."/>
        <person name="Bertero M.G."/>
        <person name="Bessieres P."/>
        <person name="Bolotin A."/>
        <person name="Borchert S."/>
        <person name="Borriss R."/>
        <person name="Boursier L."/>
        <person name="Brans A."/>
        <person name="Braun M."/>
        <person name="Brignell S.C."/>
        <person name="Bron S."/>
        <person name="Brouillet S."/>
        <person name="Bruschi C.V."/>
        <person name="Caldwell B."/>
        <person name="Capuano V."/>
        <person name="Carter N.M."/>
        <person name="Choi S.-K."/>
        <person name="Codani J.-J."/>
        <person name="Connerton I.F."/>
        <person name="Cummings N.J."/>
        <person name="Daniel R.A."/>
        <person name="Denizot F."/>
        <person name="Devine K.M."/>
        <person name="Duesterhoeft A."/>
        <person name="Ehrlich S.D."/>
        <person name="Emmerson P.T."/>
        <person name="Entian K.-D."/>
        <person name="Errington J."/>
        <person name="Fabret C."/>
        <person name="Ferrari E."/>
        <person name="Foulger D."/>
        <person name="Fritz C."/>
        <person name="Fujita M."/>
        <person name="Fujita Y."/>
        <person name="Fuma S."/>
        <person name="Galizzi A."/>
        <person name="Galleron N."/>
        <person name="Ghim S.-Y."/>
        <person name="Glaser P."/>
        <person name="Goffeau A."/>
        <person name="Golightly E.J."/>
        <person name="Grandi G."/>
        <person name="Guiseppi G."/>
        <person name="Guy B.J."/>
        <person name="Haga K."/>
        <person name="Haiech J."/>
        <person name="Harwood C.R."/>
        <person name="Henaut A."/>
        <person name="Hilbert H."/>
        <person name="Holsappel S."/>
        <person name="Hosono S."/>
        <person name="Hullo M.-F."/>
        <person name="Itaya M."/>
        <person name="Jones L.-M."/>
        <person name="Joris B."/>
        <person name="Karamata D."/>
        <person name="Kasahara Y."/>
        <person name="Klaerr-Blanchard M."/>
        <person name="Klein C."/>
        <person name="Kobayashi Y."/>
        <person name="Koetter P."/>
        <person name="Koningstein G."/>
        <person name="Krogh S."/>
        <person name="Kumano M."/>
        <person name="Kurita K."/>
        <person name="Lapidus A."/>
        <person name="Lardinois S."/>
        <person name="Lauber J."/>
        <person name="Lazarevic V."/>
        <person name="Lee S.-M."/>
        <person name="Levine A."/>
        <person name="Liu H."/>
        <person name="Masuda S."/>
        <person name="Mauel C."/>
        <person name="Medigue C."/>
        <person name="Medina N."/>
        <person name="Mellado R.P."/>
        <person name="Mizuno M."/>
        <person name="Moestl D."/>
        <person name="Nakai S."/>
        <person name="Noback M."/>
        <person name="Noone D."/>
        <person name="O'Reilly M."/>
        <person name="Ogawa K."/>
        <person name="Ogiwara A."/>
        <person name="Oudega B."/>
        <person name="Park S.-H."/>
        <person name="Parro V."/>
        <person name="Pohl T.M."/>
        <person name="Portetelle D."/>
        <person name="Porwollik S."/>
        <person name="Prescott A.M."/>
        <person name="Presecan E."/>
        <person name="Pujic P."/>
        <person name="Purnelle B."/>
        <person name="Rapoport G."/>
        <person name="Rey M."/>
        <person name="Reynolds S."/>
        <person name="Rieger M."/>
        <person name="Rivolta C."/>
        <person name="Rocha E."/>
        <person name="Roche B."/>
        <person name="Rose M."/>
        <person name="Sadaie Y."/>
        <person name="Sato T."/>
        <person name="Scanlan E."/>
        <person name="Schleich S."/>
        <person name="Schroeter R."/>
        <person name="Scoffone F."/>
        <person name="Sekiguchi J."/>
        <person name="Sekowska A."/>
        <person name="Seror S.J."/>
        <person name="Serror P."/>
        <person name="Shin B.-S."/>
        <person name="Soldo B."/>
        <person name="Sorokin A."/>
        <person name="Tacconi E."/>
        <person name="Takagi T."/>
        <person name="Takahashi H."/>
        <person name="Takemaru K."/>
        <person name="Takeuchi M."/>
        <person name="Tamakoshi A."/>
        <person name="Tanaka T."/>
        <person name="Terpstra P."/>
        <person name="Tognoni A."/>
        <person name="Tosato V."/>
        <person name="Uchiyama S."/>
        <person name="Vandenbol M."/>
        <person name="Vannier F."/>
        <person name="Vassarotti A."/>
        <person name="Viari A."/>
        <person name="Wambutt R."/>
        <person name="Wedler E."/>
        <person name="Wedler H."/>
        <person name="Weitzenegger T."/>
        <person name="Winters P."/>
        <person name="Wipat A."/>
        <person name="Yamamoto H."/>
        <person name="Yamane K."/>
        <person name="Yasumoto K."/>
        <person name="Yata K."/>
        <person name="Yoshida K."/>
        <person name="Yoshikawa H.-F."/>
        <person name="Zumstein E."/>
        <person name="Yoshikawa H."/>
        <person name="Danchin A."/>
      </authorList>
      <dbReference type="PubMed" id="9384377"/>
      <dbReference type="DOI" id="10.1038/36786"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>168</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2009" name="Microbiology" volume="155" first="1758" last="1775">
      <title>From a consortium sequence to a unified sequence: the Bacillus subtilis 168 reference genome a decade later.</title>
      <authorList>
        <person name="Barbe V."/>
        <person name="Cruveiller S."/>
        <person name="Kunst F."/>
        <person name="Lenoble P."/>
        <person name="Meurice G."/>
        <person name="Sekowska A."/>
        <person name="Vallenet D."/>
        <person name="Wang T."/>
        <person name="Moszer I."/>
        <person name="Medigue C."/>
        <person name="Danchin A."/>
      </authorList>
      <dbReference type="PubMed" id="19383706"/>
      <dbReference type="DOI" id="10.1099/mic.0.027839-0"/>
    </citation>
    <scope>SEQUENCE REVISION TO 102</scope>
  </reference>
  <dbReference type="EMBL" id="X62539">
    <property type="protein sequence ID" value="CAA44407.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D26185">
    <property type="protein sequence ID" value="BAA05228.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL009126">
    <property type="protein sequence ID" value="CAB16135.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="I40443">
    <property type="entry name" value="I40443"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_391978.2">
    <property type="nucleotide sequence ID" value="NC_000964.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003244005.1">
    <property type="nucleotide sequence ID" value="NZ_OZ025638.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P37523"/>
  <dbReference type="SMR" id="P37523"/>
  <dbReference type="STRING" id="224308.BSU40980"/>
  <dbReference type="PaxDb" id="224308-BSU40980"/>
  <dbReference type="EnsemblBacteria" id="CAB16135">
    <property type="protein sequence ID" value="CAB16135"/>
    <property type="gene ID" value="BSU_40980"/>
  </dbReference>
  <dbReference type="GeneID" id="937929"/>
  <dbReference type="KEGG" id="bsu:BSU40980"/>
  <dbReference type="PATRIC" id="fig|224308.179.peg.4440"/>
  <dbReference type="eggNOG" id="ENOG5032VFE">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="InParanoid" id="P37523"/>
  <dbReference type="OrthoDB" id="2436858at2"/>
  <dbReference type="BioCyc" id="BSUB:BSU40980-MONOMER"/>
  <dbReference type="Proteomes" id="UP000001570">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030153">
    <property type="term" value="P:bacteriocin immunity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009589">
    <property type="entry name" value="SunI"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06713">
    <property type="entry name" value="bPH_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="predicted"/>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000050048" description="Uncharacterized protein YyaB">
    <location>
      <begin position="1"/>
      <end position="146"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; CAA44407 and 2; BAA05228." evidence="1" ref="1 2">
    <original>G</original>
    <variation>R</variation>
    <location>
      <position position="102"/>
    </location>
  </feature>
  <evidence type="ECO:0000305" key="1"/>
  <sequence length="146" mass="16894" checksum="57BE840ED21B868F" modified="2009-07-28" version="2">MVYQTKRDVPVTLMIVFLILLIQADAIVPFVLGNMRVSGWIIFILLTLLNGLIIWSFIDLKYVLKEHHLIIKAGLIKHQIPYENIDKVVQKKKLWSGFRLIGSRHAITIYYQGGWGHAVISPQKSEEFIHKLKEKNSNIIIFTKSK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>