<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-11-07" modified="2024-10-02" version="126" xmlns="http://uniprot.org/uniprot">
  <accession>Q39066</accession>
  <name>ECS1_ARATH</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Protein ECS1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Protein CXc750</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="6" type="primary">ECS1</name>
    <name evidence="5" type="synonym">CXc750</name>
    <name evidence="7" type="ordered locus">At1g31580</name>
    <name evidence="8" type="ORF">F27M3.20</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Plant Mol. Biol." volume="25" first="229" last="239">
      <title>A new, pathogen-inducible gene of Arabidopsis is expressed in an ecotype-specific manner.</title>
      <authorList>
        <person name="Aufsatz W."/>
        <person name="Grimm C."/>
      </authorList>
      <dbReference type="PubMed" id="8018872"/>
      <dbReference type="DOI" id="10.1007/bf00023240"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>INDUCTION BY XANTHOMONAS CAMPESTRIS</scope>
    <scope>MISCELLANEOUS</scope>
    <source>
      <strain>cv. Columbia</strain>
      <strain>cv. Oy-0</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2000" name="Nature" volume="408" first="816" last="820">
      <title>Sequence and analysis of chromosome 1 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
        <person name="Palm C.J."/>
        <person name="Federspiel N.A."/>
        <person name="Kaul S."/>
        <person name="White O."/>
        <person name="Alonso J."/>
        <person name="Altafi H."/>
        <person name="Araujo R."/>
        <person name="Bowman C.L."/>
        <person name="Brooks S.Y."/>
        <person name="Buehler E."/>
        <person name="Chan A."/>
        <person name="Chao Q."/>
        <person name="Chen H."/>
        <person name="Cheuk R.F."/>
        <person name="Chin C.W."/>
        <person name="Chung M.K."/>
        <person name="Conn L."/>
        <person name="Conway A.B."/>
        <person name="Conway A.R."/>
        <person name="Creasy T.H."/>
        <person name="Dewar K."/>
        <person name="Dunn P."/>
        <person name="Etgu P."/>
        <person name="Feldblyum T.V."/>
        <person name="Feng J.-D."/>
        <person name="Fong B."/>
        <person name="Fujii C.Y."/>
        <person name="Gill J.E."/>
        <person name="Goldsmith A.D."/>
        <person name="Haas B."/>
        <person name="Hansen N.F."/>
        <person name="Hughes B."/>
        <person name="Huizar L."/>
        <person name="Hunter J.L."/>
        <person name="Jenkins J."/>
        <person name="Johnson-Hopson C."/>
        <person name="Khan S."/>
        <person name="Khaykin E."/>
        <person name="Kim C.J."/>
        <person name="Koo H.L."/>
        <person name="Kremenetskaia I."/>
        <person name="Kurtz D.B."/>
        <person name="Kwan A."/>
        <person name="Lam B."/>
        <person name="Langin-Hooper S."/>
        <person name="Lee A."/>
        <person name="Lee J.M."/>
        <person name="Lenz C.A."/>
        <person name="Li J.H."/>
        <person name="Li Y.-P."/>
        <person name="Lin X."/>
        <person name="Liu S.X."/>
        <person name="Liu Z.A."/>
        <person name="Luros J.S."/>
        <person name="Maiti R."/>
        <person name="Marziali A."/>
        <person name="Militscher J."/>
        <person name="Miranda M."/>
        <person name="Nguyen M."/>
        <person name="Nierman W.C."/>
        <person name="Osborne B.I."/>
        <person name="Pai G."/>
        <person name="Peterson J."/>
        <person name="Pham P.K."/>
        <person name="Rizzo M."/>
        <person name="Rooney T."/>
        <person name="Rowley D."/>
        <person name="Sakano H."/>
        <person name="Salzberg S.L."/>
        <person name="Schwartz J.R."/>
        <person name="Shinn P."/>
        <person name="Southwick A.M."/>
        <person name="Sun H."/>
        <person name="Tallon L.J."/>
        <person name="Tambunga G."/>
        <person name="Toriumi M.J."/>
        <person name="Town C.D."/>
        <person name="Utterback T."/>
        <person name="Van Aken S."/>
        <person name="Vaysberg M."/>
        <person name="Vysotskaia V.S."/>
        <person name="Walker M."/>
        <person name="Wu D."/>
        <person name="Yu G."/>
        <person name="Fraser C.M."/>
        <person name="Venter J.C."/>
        <person name="Davis R.W."/>
      </authorList>
      <dbReference type="PubMed" id="11130712"/>
      <dbReference type="DOI" id="10.1038/35048500"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2003" name="Science" volume="302" first="842" last="846">
      <title>Empirical analysis of transcriptional activity in the Arabidopsis genome.</title>
      <authorList>
        <person name="Yamada K."/>
        <person name="Lim J."/>
        <person name="Dale J.M."/>
        <person name="Chen H."/>
        <person name="Shinn P."/>
        <person name="Palm C.J."/>
        <person name="Southwick A.M."/>
        <person name="Wu H.C."/>
        <person name="Kim C.J."/>
        <person name="Nguyen M."/>
        <person name="Pham P.K."/>
        <person name="Cheuk R.F."/>
        <person name="Karlin-Newmann G."/>
        <person name="Liu S.X."/>
        <person name="Lam B."/>
        <person name="Sakano H."/>
        <person name="Wu T."/>
        <person name="Yu G."/>
        <person name="Miranda M."/>
        <person name="Quach H.L."/>
        <person name="Tripp M."/>
        <person name="Chang C.H."/>
        <person name="Lee J.M."/>
        <person name="Toriumi M.J."/>
        <person name="Chan M.M."/>
        <person name="Tang C.C."/>
        <person name="Onodera C.S."/>
        <person name="Deng J.M."/>
        <person name="Akiyama K."/>
        <person name="Ansari Y."/>
        <person name="Arakawa T."/>
        <person name="Banh J."/>
        <person name="Banno F."/>
        <person name="Bowser L."/>
        <person name="Brooks S.Y."/>
        <person name="Carninci P."/>
        <person name="Chao Q."/>
        <person name="Choy N."/>
        <person name="Enju A."/>
        <person name="Goldsmith A.D."/>
        <person name="Gurjal M."/>
        <person name="Hansen N.F."/>
        <person name="Hayashizaki Y."/>
        <person name="Johnson-Hopson C."/>
        <person name="Hsuan V.W."/>
        <person name="Iida K."/>
        <person name="Karnes M."/>
        <person name="Khan S."/>
        <person name="Koesema E."/>
        <person name="Ishida J."/>
        <person name="Jiang P.X."/>
        <person name="Jones T."/>
        <person name="Kawai J."/>
        <person name="Kamiya A."/>
        <person name="Meyers C."/>
        <person name="Nakajima M."/>
        <person name="Narusaka M."/>
        <person name="Seki M."/>
        <person name="Sakurai T."/>
        <person name="Satou M."/>
        <person name="Tamse R."/>
        <person name="Vaysberg M."/>
        <person name="Wallender E.K."/>
        <person name="Wong C."/>
        <person name="Yamamura Y."/>
        <person name="Yuan S."/>
        <person name="Shinozaki K."/>
        <person name="Davis R.W."/>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
      </authorList>
      <dbReference type="PubMed" id="14593172"/>
      <dbReference type="DOI" id="10.1126/science.1088305"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1998" name="Plant Mol. Biol." volume="38" first="965" last="976">
      <title>The ECS1 gene of Arabidopsis encodes a plant cell wall-associated protein and is potentially linked to a locus influencing resistance to Xanthomonas campestris.</title>
      <authorList>
        <person name="Aufsatz W."/>
        <person name="Amry D."/>
        <person name="Grimm C."/>
      </authorList>
      <dbReference type="PubMed" id="9869403"/>
      <dbReference type="DOI" id="10.1023/a:1006028605413"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MISCELLANEOUS</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <strain>cv. Columbia</strain>
      <strain>cv. Oy-0</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2006" name="Plant J." volume="45" first="399" last="422">
      <title>Characterization of a novel putative zinc finger gene MIF1: involvement in multiple hormonal regulation of Arabidopsis development.</title>
      <authorList>
        <person name="Hu W."/>
        <person name="Ma H."/>
      </authorList>
      <dbReference type="PubMed" id="16412086"/>
      <dbReference type="DOI" id="10.1111/j.1365-313x.2005.02626.x"/>
    </citation>
    <scope>INDUCTION BY LIGHT</scope>
  </reference>
  <comment type="function">
    <text evidence="4">Maybe involved in defense responses to X.campestris, but probably not a X.campestris pv. campestris race 750 (e.g. Xcc750) resistance gene; according to genetic data, linked to a locus influencing resistance to Xcc750.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
      <location evidence="4">Cell wall</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed in leaves, flowers and stems, but not in roots.</text>
  </comment>
  <comment type="induction">
    <text evidence="2 3">Accumulates upon infection by the phytopathogenic bacterium X.campestris pv. campestris (both compatible and incompatible strains Xcc168 and Xcc750, respectively), agent of black rot (PubMed:8018872). Accumulates at higher levels in light than in darkness. Repressed by MIF1 (PubMed:16412086).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="3 4">Absent in the susceptible ecotype cv. Oy-0 (PubMed:8018872). Only present in ecotypes resistant against X.campestris pv. campestris race 750 (e.g. Xcc750) (PubMed:9869403).</text>
  </comment>
  <dbReference type="EMBL" id="X72022">
    <property type="protein sequence ID" value="CAA50905.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC074360">
    <property type="protein sequence ID" value="AAG60156.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002684">
    <property type="protein sequence ID" value="AEE31373.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY059950">
    <property type="protein sequence ID" value="AAL24432.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY128746">
    <property type="protein sequence ID" value="AAM91146.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S46537">
    <property type="entry name" value="S46537"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_174441.1">
    <property type="nucleotide sequence ID" value="NM_102895.4"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q39066"/>
  <dbReference type="SMR" id="Q39066"/>
  <dbReference type="STRING" id="3702.Q39066"/>
  <dbReference type="PaxDb" id="3702-AT1G31580.1"/>
  <dbReference type="ProteomicsDB" id="247058"/>
  <dbReference type="EnsemblPlants" id="AT1G31580.1">
    <property type="protein sequence ID" value="AT1G31580.1"/>
    <property type="gene ID" value="AT1G31580"/>
  </dbReference>
  <dbReference type="GeneID" id="840047"/>
  <dbReference type="Gramene" id="AT1G31580.1">
    <property type="protein sequence ID" value="AT1G31580.1"/>
    <property type="gene ID" value="AT1G31580"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT1G31580"/>
  <dbReference type="Araport" id="AT1G31580"/>
  <dbReference type="TAIR" id="AT1G31580">
    <property type="gene designation" value="ECS1"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_2375697_0_0_1"/>
  <dbReference type="InParanoid" id="Q39066"/>
  <dbReference type="PRO" id="PR:Q39066"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009617">
    <property type="term" value="P:response to bacterium"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009416">
    <property type="term" value="P:response to light stimulus"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0134">Cell wall</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5014309064" description="Protein ECS1" evidence="1">
    <location>
      <begin position="28"/>
      <end position="95"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="16412086"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="8018872"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="9869403"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="8018872"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="9869403"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="Araport" id="AT1G31580"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="AAG60156.1"/>
    </source>
  </evidence>
  <sequence length="95" mass="10742" checksum="BB8986E849E057D1" modified="1996-11-01" version="1" precursor="true">MASSIVSSMFLFLLLLLVFPHIDNVLGARMELRELGEINYADPLGFTRPIVPIHVPGFPPRRPTIPQLPPYRPRRCPFCYPPPPPKAFPKNSPSH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>