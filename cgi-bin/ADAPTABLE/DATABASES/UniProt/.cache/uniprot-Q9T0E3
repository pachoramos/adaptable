<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-13" modified="2024-10-02" version="101" xmlns="http://uniprot.org/uniprot">
  <accession>Q9T0E3</accession>
  <accession>Q6GKX9</accession>
  <name>DF151_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 151</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Low-molecular-weight cysteine-rich protein 17</fullName>
      <shortName>Protein LCR17</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">LCR17</name>
    <name type="ordered locus">At4g11760</name>
    <name type="ORF">T5C23.190</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Nature" volume="402" first="769" last="777">
      <title>Sequence and analysis of chromosome 4 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Mayer K.F.X."/>
        <person name="Schueller C."/>
        <person name="Wambutt R."/>
        <person name="Murphy G."/>
        <person name="Volckaert G."/>
        <person name="Pohl T."/>
        <person name="Duesterhoeft A."/>
        <person name="Stiekema W."/>
        <person name="Entian K.-D."/>
        <person name="Terryn N."/>
        <person name="Harris B."/>
        <person name="Ansorge W."/>
        <person name="Brandt P."/>
        <person name="Grivell L.A."/>
        <person name="Rieger M."/>
        <person name="Weichselgartner M."/>
        <person name="de Simone V."/>
        <person name="Obermaier B."/>
        <person name="Mache R."/>
        <person name="Mueller M."/>
        <person name="Kreis M."/>
        <person name="Delseny M."/>
        <person name="Puigdomenech P."/>
        <person name="Watson M."/>
        <person name="Schmidtheini T."/>
        <person name="Reichert B."/>
        <person name="Portetelle D."/>
        <person name="Perez-Alonso M."/>
        <person name="Boutry M."/>
        <person name="Bancroft I."/>
        <person name="Vos P."/>
        <person name="Hoheisel J."/>
        <person name="Zimmermann W."/>
        <person name="Wedler H."/>
        <person name="Ridley P."/>
        <person name="Langham S.-A."/>
        <person name="McCullagh B."/>
        <person name="Bilham L."/>
        <person name="Robben J."/>
        <person name="van der Schueren J."/>
        <person name="Grymonprez B."/>
        <person name="Chuang Y.-J."/>
        <person name="Vandenbussche F."/>
        <person name="Braeken M."/>
        <person name="Weltjens I."/>
        <person name="Voet M."/>
        <person name="Bastiaens I."/>
        <person name="Aert R."/>
        <person name="Defoor E."/>
        <person name="Weitzenegger T."/>
        <person name="Bothe G."/>
        <person name="Ramsperger U."/>
        <person name="Hilbert H."/>
        <person name="Braun M."/>
        <person name="Holzer E."/>
        <person name="Brandt A."/>
        <person name="Peters S."/>
        <person name="van Staveren M."/>
        <person name="Dirkse W."/>
        <person name="Mooijman P."/>
        <person name="Klein Lankhorst R."/>
        <person name="Rose M."/>
        <person name="Hauf J."/>
        <person name="Koetter P."/>
        <person name="Berneiser S."/>
        <person name="Hempel S."/>
        <person name="Feldpausch M."/>
        <person name="Lamberth S."/>
        <person name="Van den Daele H."/>
        <person name="De Keyser A."/>
        <person name="Buysshaert C."/>
        <person name="Gielen J."/>
        <person name="Villarroel R."/>
        <person name="De Clercq R."/>
        <person name="van Montagu M."/>
        <person name="Rogers J."/>
        <person name="Cronin A."/>
        <person name="Quail M.A."/>
        <person name="Bray-Allen S."/>
        <person name="Clark L."/>
        <person name="Doggett J."/>
        <person name="Hall S."/>
        <person name="Kay M."/>
        <person name="Lennard N."/>
        <person name="McLay K."/>
        <person name="Mayes R."/>
        <person name="Pettett A."/>
        <person name="Rajandream M.A."/>
        <person name="Lyne M."/>
        <person name="Benes V."/>
        <person name="Rechmann S."/>
        <person name="Borkova D."/>
        <person name="Bloecker H."/>
        <person name="Scharfe M."/>
        <person name="Grimm M."/>
        <person name="Loehnert T.-H."/>
        <person name="Dose S."/>
        <person name="de Haan M."/>
        <person name="Maarse A.C."/>
        <person name="Schaefer M."/>
        <person name="Mueller-Auer S."/>
        <person name="Gabel C."/>
        <person name="Fuchs M."/>
        <person name="Fartmann B."/>
        <person name="Granderath K."/>
        <person name="Dauner D."/>
        <person name="Herzl A."/>
        <person name="Neumann S."/>
        <person name="Argiriou A."/>
        <person name="Vitale D."/>
        <person name="Liguori R."/>
        <person name="Piravandi E."/>
        <person name="Massenet O."/>
        <person name="Quigley F."/>
        <person name="Clabauld G."/>
        <person name="Muendlein A."/>
        <person name="Felber R."/>
        <person name="Schnabl S."/>
        <person name="Hiller R."/>
        <person name="Schmidt W."/>
        <person name="Lecharny A."/>
        <person name="Aubourg S."/>
        <person name="Chefdor F."/>
        <person name="Cooke R."/>
        <person name="Berger C."/>
        <person name="Monfort A."/>
        <person name="Casacuberta E."/>
        <person name="Gibbons T."/>
        <person name="Weber N."/>
        <person name="Vandenbol M."/>
        <person name="Bargues M."/>
        <person name="Terol J."/>
        <person name="Torres A."/>
        <person name="Perez-Perez A."/>
        <person name="Purnelle B."/>
        <person name="Bent E."/>
        <person name="Johnson S."/>
        <person name="Tacon D."/>
        <person name="Jesse T."/>
        <person name="Heijnen L."/>
        <person name="Schwarz S."/>
        <person name="Scholler P."/>
        <person name="Heber S."/>
        <person name="Francs P."/>
        <person name="Bielke C."/>
        <person name="Frishman D."/>
        <person name="Haase D."/>
        <person name="Lemcke K."/>
        <person name="Mewes H.-W."/>
        <person name="Stocker S."/>
        <person name="Zaccaria P."/>
        <person name="Bevan M."/>
        <person name="Wilson R.K."/>
        <person name="de la Bastide M."/>
        <person name="Habermann K."/>
        <person name="Parnell L."/>
        <person name="Dedhia N."/>
        <person name="Gnoj L."/>
        <person name="Schutz K."/>
        <person name="Huang E."/>
        <person name="Spiegel L."/>
        <person name="Sekhon M."/>
        <person name="Murray J."/>
        <person name="Sheet P."/>
        <person name="Cordes M."/>
        <person name="Abu-Threideh J."/>
        <person name="Stoneking T."/>
        <person name="Kalicki J."/>
        <person name="Graves T."/>
        <person name="Harmon G."/>
        <person name="Edwards J."/>
        <person name="Latreille P."/>
        <person name="Courtney L."/>
        <person name="Cloud J."/>
        <person name="Abbott A."/>
        <person name="Scott K."/>
        <person name="Johnson D."/>
        <person name="Minx P."/>
        <person name="Bentley D."/>
        <person name="Fulton B."/>
        <person name="Miller N."/>
        <person name="Greco T."/>
        <person name="Kemp K."/>
        <person name="Kramer J."/>
        <person name="Fulton L."/>
        <person name="Mardis E."/>
        <person name="Dante M."/>
        <person name="Pepin K."/>
        <person name="Hillier L.W."/>
        <person name="Nelson J."/>
        <person name="Spieth J."/>
        <person name="Ryan E."/>
        <person name="Andrews S."/>
        <person name="Geisel C."/>
        <person name="Layman D."/>
        <person name="Du H."/>
        <person name="Ali J."/>
        <person name="Berghoff A."/>
        <person name="Jones K."/>
        <person name="Drone K."/>
        <person name="Cotton M."/>
        <person name="Joshu C."/>
        <person name="Antonoiu B."/>
        <person name="Zidanic M."/>
        <person name="Strong C."/>
        <person name="Sun H."/>
        <person name="Lamar B."/>
        <person name="Yordan C."/>
        <person name="Ma P."/>
        <person name="Zhong J."/>
        <person name="Preston R."/>
        <person name="Vil D."/>
        <person name="Shekher M."/>
        <person name="Matero A."/>
        <person name="Shah R."/>
        <person name="Swaby I.K."/>
        <person name="O'Shaughnessy A."/>
        <person name="Rodriguez M."/>
        <person name="Hoffman J."/>
        <person name="Till S."/>
        <person name="Granat S."/>
        <person name="Shohdy N."/>
        <person name="Hasegawa A."/>
        <person name="Hameed A."/>
        <person name="Lodhi M."/>
        <person name="Johnson A."/>
        <person name="Chen E."/>
        <person name="Marra M.A."/>
        <person name="Martienssen R."/>
        <person name="McCombie W.R."/>
      </authorList>
      <dbReference type="PubMed" id="10617198"/>
      <dbReference type="DOI" id="10.1038/47134"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="submission" date="2004-06" db="EMBL/GenBank/DDBJ databases">
      <title>Arabidopsis ORF clones.</title>
      <authorList>
        <person name="Cheuk R.F."/>
        <person name="Chen H."/>
        <person name="Kim C.J."/>
        <person name="Shinn P."/>
        <person name="Ecker J.R."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2001" name="Plant Mol. Biol." volume="46" first="17" last="34">
      <title>Two large Arabidopsis thaliana gene families are homologous to the Brassica gene superfamily that encodes pollen coat proteins and the male component of the self-incompatibility response.</title>
      <authorList>
        <person name="Vanoosthuyse V."/>
        <person name="Miege C."/>
        <person name="Dumas C."/>
        <person name="Cock J.M."/>
      </authorList>
      <dbReference type="PubMed" id="11437247"/>
      <dbReference type="DOI" id="10.1023/a:1010664704926"/>
    </citation>
    <scope>IDENTIFICATION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="EMBL" id="AL049500">
    <property type="protein sequence ID" value="CAB39947.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL161532">
    <property type="protein sequence ID" value="CAB78219.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002687">
    <property type="protein sequence ID" value="AEE83046.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT014935">
    <property type="protein sequence ID" value="AAT47786.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="T04223">
    <property type="entry name" value="T04223"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_192913.1">
    <property type="nucleotide sequence ID" value="NM_117245.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9T0E3"/>
  <dbReference type="SMR" id="Q9T0E3"/>
  <dbReference type="STRING" id="3702.Q9T0E3"/>
  <dbReference type="PaxDb" id="3702-AT4G11760.1"/>
  <dbReference type="ProteomicsDB" id="224195"/>
  <dbReference type="EnsemblPlants" id="AT4G11760.1">
    <property type="protein sequence ID" value="AT4G11760.1"/>
    <property type="gene ID" value="AT4G11760"/>
  </dbReference>
  <dbReference type="GeneID" id="826782"/>
  <dbReference type="Gramene" id="AT4G11760.1">
    <property type="protein sequence ID" value="AT4G11760.1"/>
    <property type="gene ID" value="AT4G11760"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT4G11760"/>
  <dbReference type="Araport" id="AT4G11760"/>
  <dbReference type="TAIR" id="AT4G11760">
    <property type="gene designation" value="LCR17"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_185543_0_0_1"/>
  <dbReference type="InParanoid" id="Q9T0E3"/>
  <dbReference type="OMA" id="CFAMSEC"/>
  <dbReference type="OrthoDB" id="5521226at2759"/>
  <dbReference type="PhylomeDB" id="Q9T0E3"/>
  <dbReference type="PRO" id="PR:Q9T0E3"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 4"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q9T0E3">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010851">
    <property type="entry name" value="DEFL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34783">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 144-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34783:SF1">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 144-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07333">
    <property type="entry name" value="SLR1-BP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000206204" description="Defensin-like protein 151">
    <location>
      <begin position="30"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="35"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="48"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="53"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="57"/>
      <end position="84"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="96" mass="10177" checksum="C3EB364926CFBE04" modified="2000-05-01" version="1" precursor="true">MKKPSQLSATILTIFVILAIGVMVKETLGQAPSTCFEALKDASKGASCDSELCASLCKKKSGGGVGTCRTKTTQPSKGQPECHCRFWCKSDGTPYK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>