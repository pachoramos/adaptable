<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-10-17" modified="2019-12-11" version="12" xmlns="http://uniprot.org/uniprot">
  <accession>P85003</accession>
  <name>CYCLE_ANNCH</name>
  <protein>
    <recommendedName>
      <fullName>Cyclopeptide E</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Annona cherimola</name>
    <name type="common">Custard apple</name>
    <name type="synonym">Cherimoya</name>
    <dbReference type="NCBI Taxonomy" id="49314"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>Magnoliidae</taxon>
      <taxon>Magnoliales</taxon>
      <taxon>Annonaceae</taxon>
      <taxon>Annonoideae</taxon>
      <taxon>Annoneae</taxon>
      <taxon>Annona</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2005" name="Phytochemistry" volume="66" first="2376" last="2380">
      <title>Two cyclopeptides from the seeds of Annona cherimola.</title>
      <authorList>
        <person name="Wele A."/>
        <person name="Zhang Y."/>
        <person name="Brouard J.-P."/>
        <person name="Pousset J.-L."/>
        <person name="Bodo B."/>
      </authorList>
      <dbReference type="PubMed" id="16040066"/>
      <dbReference type="DOI" id="10.1016/j.phytochem.2005.06.011"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>CROSS-LINK</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="1">Seed</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Probably participates in a plant defense mechanism. Has cytotoxic activity against human nasopharyngeal carcinoma with an IC(50) of 17 nM.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">This is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="635.0" method="Electrospray" evidence="1"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000253927" description="Cyclopeptide E" evidence="1">
    <location>
      <begin position="1"/>
      <end position="6"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Pro-Tyr)">
    <location>
      <begin position="1"/>
      <end position="6"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="16040066"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="6" mass="653" checksum="7B59C87728670000" modified="2006-10-17" version="1">PGLGFY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>