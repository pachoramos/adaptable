<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-02-01" modified="2023-09-13" version="88" xmlns="http://uniprot.org/uniprot">
  <accession>Q06110</accession>
  <name>CAGA_STRGL</name>
  <protein>
    <recommendedName>
      <fullName>Antitumor antibiotic C-1027 apoprotein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>C-1027-AG</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">cagA</name>
  </gene>
  <organism>
    <name type="scientific">Streptomyces globisporus</name>
    <dbReference type="NCBI Taxonomy" id="1908"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Kitasatosporales</taxon>
      <taxon>Streptomycetaceae</taxon>
      <taxon>Streptomyces</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="Biosci. Biotechnol. Biochem." volume="56" first="1592" last="1595">
      <title>Cloning and nucleotide sequencing of the antitumor antibiotic C-1027 apoprotein gene.</title>
      <authorList>
        <person name="Sakata N."/>
        <person name="Ikeno S."/>
        <person name="Hori M."/>
        <person name="Hamada M."/>
        <person name="Otani T."/>
      </authorList>
      <dbReference type="PubMed" id="1369059"/>
      <dbReference type="DOI" id="10.1271/bbb.56.1592"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>C-1027</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1991" name="Agric. Biol. Chem." volume="55" first="407" last="417">
      <title>Purification and primary structure of C-1027-AG, a selective antagonist of antitumor antibiotic C-1027, from Streptomyces globisporus.</title>
      <authorList>
        <person name="Otani T."/>
        <person name="Yasuhara T."/>
        <person name="Minami Y."/>
        <person name="Shimazu T."/>
        <person name="Zhang R."/>
        <person name="Xie M.Y."/>
      </authorList>
      <dbReference type="PubMed" id="1368692"/>
      <dbReference type="DOI" id="10.1271/bbb1961.55.407"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 34-143</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1993" name="Biochemistry" volume="32" first="5548" last="5553">
      <title>Some characteristics of DNA strand scission by macromolecular antitumor antibiotic C-1027 containing a novel enediyne chromophore.</title>
      <authorList>
        <person name="Sugiura Y."/>
        <person name="Matsumoto T."/>
      </authorList>
      <dbReference type="PubMed" id="8504075"/>
      <dbReference type="DOI" id="10.1021/bi00072a008"/>
    </citation>
    <scope>CHARACTERIZATION</scope>
  </reference>
  <comment type="function">
    <text>Binds non-covalently to a chromophore which is the cytotoxic and mutagenic component of the antibiotic. The chromophore binds to DNA as a weak intercalator and causes single- and double-strand breaks.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the neocarzinostatin family.</text>
  </comment>
  <dbReference type="EMBL" id="D10827">
    <property type="protein sequence ID" value="BAA01609.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="JC1315">
    <property type="entry name" value="YMSMCG"/>
  </dbReference>
  <dbReference type="PDB" id="1HZK">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=34-143"/>
  </dbReference>
  <dbReference type="PDB" id="1HZL">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=34-143"/>
  </dbReference>
  <dbReference type="PDB" id="1J48">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.80 A"/>
    <property type="chains" value="A/B=34-143"/>
  </dbReference>
  <dbReference type="PDBsum" id="1HZK"/>
  <dbReference type="PDBsum" id="1HZL"/>
  <dbReference type="PDBsum" id="1J48"/>
  <dbReference type="AlphaFoldDB" id="Q06110"/>
  <dbReference type="BMRB" id="Q06110"/>
  <dbReference type="SMR" id="Q06110"/>
  <dbReference type="DrugBank" id="DB03933">
    <property type="generic name" value="C-1027 Aromatized chromophore"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="Q06110"/>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.230">
    <property type="entry name" value="Neocarzinostatin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027273">
    <property type="entry name" value="Neocarzinostatin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002186">
    <property type="entry name" value="Neocarzinostatin_fam"/>
  </dbReference>
  <dbReference type="NCBIfam" id="NF040680">
    <property type="entry name" value="chromo_anti"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00960">
    <property type="entry name" value="Neocarzinostat"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01885">
    <property type="entry name" value="MACROMOMYCIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF49319">
    <property type="entry name" value="Actinoxanthin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000019459" description="Antitumor antibiotic C-1027 apoprotein">
    <location>
      <begin position="34"/>
      <end position="143"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="69"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="119"/>
      <end position="124"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="36"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="43"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="50"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="63"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="75"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="turn" evidence="5">
    <location>
      <begin position="80"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="84"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="94"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="102"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="109"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="113"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="turn" evidence="5">
    <location>
      <begin position="119"/>
      <end position="121"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="125"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="133"/>
      <end position="139"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="1368692"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="1HZK"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="1HZL"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="5">
    <source>
      <dbReference type="PDB" id="1J48"/>
    </source>
  </evidence>
  <sequence length="143" mass="13873" checksum="8CA3E57FA5DA842A" modified="1995-02-01" version="1" precursor="true">MSLRHMSRRASRFGVVAVASIGLAAAAQSVAFAAPAFSVSPASGLSDGQSVSVSVSGAAAGETYYIAQCAPVGGQDACNPATATSFTTDASGAASFSFVVRKSYTGSTPEGTPVGSVDCATAACNLGAGNSGLDLGHVALTFG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>