<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-10-14" modified="2024-11-27" version="101" xmlns="http://uniprot.org/uniprot">
  <accession>Q30KN4</accession>
  <name>DFB30_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 30</fullName>
      <shortName>BD-30</shortName>
      <shortName>mBD-30</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 30</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Defb30</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Physiol. Genomics" volume="23" first="5" last="17">
      <title>Cross-species analysis of the mammalian beta-defensin gene family: presence of syntenic gene clusters and preferential expression in the male reproductive tract.</title>
      <authorList>
        <person name="Patil A.A."/>
        <person name="Cai Y."/>
        <person name="Sang Y."/>
        <person name="Blecha F."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="16033865"/>
      <dbReference type="DOI" id="10.1152/physiolgenomics.00104.2005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="2005-07" db="EMBL/GenBank/DDBJ databases">
      <title>Male reproductive tract specific rodent beta defensins.</title>
      <authorList>
        <person name="Radhakrishnan Y."/>
        <person name="Yenugu S."/>
        <person name="Hamil K.G."/>
        <person name="French F.S."/>
        <person name="Hall S.H."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>CD-1</strain>
      <tissue>Epididymis</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Testis</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="DQ012040">
    <property type="protein sequence ID" value="AAY59776.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ141309">
    <property type="protein sequence ID" value="ABA26845.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC132184">
    <property type="protein sequence ID" value="AAI32185.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC132214">
    <property type="protein sequence ID" value="AAI32215.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS36947.1"/>
  <dbReference type="RefSeq" id="NP_001034655.1">
    <property type="nucleotide sequence ID" value="NM_001039566.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q30KN4"/>
  <dbReference type="SMR" id="Q30KN4"/>
  <dbReference type="STRING" id="10090.ENSMUSP00000106838"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000106838"/>
  <dbReference type="Ensembl" id="ENSMUST00000100490.3">
    <property type="protein sequence ID" value="ENSMUSP00000098059.3"/>
    <property type="gene ID" value="ENSMUSG00000075571.9"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSMUST00000111207.8">
    <property type="protein sequence ID" value="ENSMUSP00000106838.2"/>
    <property type="gene ID" value="ENSMUSG00000075571.9"/>
  </dbReference>
  <dbReference type="GeneID" id="73670"/>
  <dbReference type="KEGG" id="mmu:73670"/>
  <dbReference type="UCSC" id="uc007uhd.2">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:1920920"/>
  <dbReference type="CTD" id="73670"/>
  <dbReference type="MGI" id="MGI:1920920">
    <property type="gene designation" value="Defb30"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000075571"/>
  <dbReference type="eggNOG" id="ENOG502TF02">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00530000064429"/>
  <dbReference type="HOGENOM" id="CLU_181906_3_0_1"/>
  <dbReference type="InParanoid" id="Q30KN4"/>
  <dbReference type="OMA" id="NTCWRTK"/>
  <dbReference type="OrthoDB" id="5003090at2759"/>
  <dbReference type="PhylomeDB" id="Q30KN4"/>
  <dbReference type="Reactome" id="R-MMU-1461957">
    <property type="pathway name" value="Beta defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-MMU-1461973">
    <property type="pathway name" value="Defensins"/>
  </dbReference>
  <dbReference type="BioGRID-ORCS" id="73670">
    <property type="hits" value="4 hits in 76 CRISPR screens"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q30KN4"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 14"/>
  </dbReference>
  <dbReference type="RNAct" id="Q30KN4">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000075571">
    <property type="expression patterns" value="Expressed in spermatid and 15 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q30KN4">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050544">
    <property type="entry name" value="Beta-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025933">
    <property type="entry name" value="Beta_defensin_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001">
    <property type="entry name" value="BETA-DEFENSIN 123-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001:SF10">
    <property type="entry name" value="BETA-DEFENSIN 135"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13841">
    <property type="entry name" value="Defensin_beta_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000352710" description="Beta-defensin 30">
    <location>
      <begin position="23"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="35"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="42"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="46"/>
      <end position="63"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="75" mass="8734" checksum="60241261261D9836" modified="2005-12-06" version="1" precursor="true">MGSLQLTLVLFVLLSYVPPVRSGVNMYIKRIYDTCWKLKGICRNTCQKEEIYHIFCGIQSLCCLEKKEMPVLFVK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>