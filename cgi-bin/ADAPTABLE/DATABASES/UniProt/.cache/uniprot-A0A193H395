<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-04-12" modified="2024-03-27" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>A0A193H395</accession>
  <name>CZS1_CRUCA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Cruzioseptin-1</fullName>
      <shortName evidence="3">CZS-1</shortName>
    </recommendedName>
  </protein>
  <organism evidence="6">
    <name type="scientific">Cruziohyla calcarifer</name>
    <name type="common">Splendid leaf frog</name>
    <name type="synonym">Agalychnis calcarifer</name>
    <dbReference type="NCBI Taxonomy" id="318249"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Cruziohyla</taxon>
    </lineage>
  </organism>
  <reference evidence="6" key="1">
    <citation type="journal article" date="2016" name="J. Proteomics" volume="146" first="1" last="13">
      <title>Peptidomic approach identifies cruzioseptins, a new family of potent antimicrobial peptides in the splendid leaf frog, Cruziohyla calcarifer.</title>
      <authorList>
        <person name="Proano-Bolanos C."/>
        <person name="Zhou M."/>
        <person name="Wang L."/>
        <person name="Coloma L.A."/>
        <person name="Chen T."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="27321580"/>
      <dbReference type="DOI" id="10.1016/j.jprot.2016.06.017"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>AMIDATION AT PHE-65</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="6">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Has antimicrobial activity against Gram-negative bacterium E.coli (MIC=15.11 uM), against Gram-positive bacterium S.aureus (MIC=3.77 uM) and against fungus C.albicans (MIC=3.77 uM). At higher concentrations also has a bactericidal and fungicidal effect. Has hemagglutinating activity against horse erythrocytes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2117.26" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Cruzioseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=02713"/>
  </comment>
  <dbReference type="EMBL" id="KX065078">
    <property type="protein sequence ID" value="ANN87758.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A193H395"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016322">
    <property type="entry name" value="FSAP"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001822">
    <property type="entry name" value="Dermaseptin_precursor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0348">Hemagglutinin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439459" evidence="5">
    <location>
      <begin position="23"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000439460" description="Cruzioseptin-1" evidence="2">
    <location>
      <begin position="45"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439461" evidence="5">
    <location>
      <begin position="66"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="5">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27321580"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="27321580"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="27321580"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="ANN87758.1"/>
    </source>
  </evidence>
  <sequence length="70" mass="7935" checksum="F805FE197E26A5CE" modified="2016-10-05" version="1" precursor="true">MAFLKKSLFLVLFLGLVSLSICEEEKREENEEEQDDDEQSEEKRGFLDIVKGVGKVALGAVSKLFGQEER</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>