<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-02-22" modified="2024-10-02" version="104" xmlns="http://uniprot.org/uniprot">
  <accession>Q3E8H4</accession>
  <name>SCOP1_ARATH</name>
  <protein>
    <recommendedName>
      <fullName evidence="7 8">Serine rich endogenous peptide 1</fullName>
      <shortName evidence="7 8">AtSCOOP1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="7 8">Phytocytokine SCOOP1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="7 8">Precursor of serine rich endogenous peptide phytocytokine 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="7 8" type="primary">PROSCOOP1</name>
    <name evidence="7 8" type="synonym">SCOOP1</name>
    <name evidence="11" type="ordered locus">At5g44565</name>
    <name evidence="12" type="ORF">K15C23</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1999-02" db="EMBL/GenBank/DDBJ databases">
      <title>Structural analysis of Arabidopsis thaliana chromosome 5. XI.</title>
      <authorList>
        <person name="Kaneko T."/>
        <person name="Katoh T."/>
        <person name="Asamizu E."/>
        <person name="Sato S."/>
        <person name="Nakamura Y."/>
        <person name="Kotani H."/>
        <person name="Tabata S."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="submission" date="2006-08" db="EMBL/GenBank/DDBJ databases">
      <title>Arabidopsis ORF Clones.</title>
      <authorList>
        <person name="Quinitio C."/>
        <person name="Chen H."/>
        <person name="Kim C.J."/>
        <person name="Shinn P."/>
        <person name="Ecker J.R."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2019" name="J. Exp. Bot." volume="70" first="1349" last="1365">
      <title>The SCOOP12 peptide regulates defense response and root elongation in Arabidopsis thaliana.</title>
      <authorList>
        <person name="Gully K."/>
        <person name="Pelletier S."/>
        <person name="Guillou M.-C."/>
        <person name="Ferrand M."/>
        <person name="Aligon S."/>
        <person name="Pokotylo I."/>
        <person name="Perrin A."/>
        <person name="Vergne E."/>
        <person name="Fagard M."/>
        <person name="Ruelland E."/>
        <person name="Grappin P."/>
        <person name="Bucher E."/>
        <person name="Renou J.-P."/>
        <person name="Aubourg S."/>
      </authorList>
      <dbReference type="PubMed" id="30715439"/>
      <dbReference type="DOI" id="10.1093/jxb/ery454"/>
    </citation>
    <scope>GENE FAMILY</scope>
    <source>
      <strain>cv. Columbia</strain>
      <strain>cv. Wassilewskija</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2021" name="Nat. Commun." volume="12" first="705" last="705">
      <title>Perception of a divergent family of phytocytokines by the Arabidopsis receptor kinase MIK2.</title>
      <authorList>
        <person name="Rhodes J."/>
        <person name="Yang H."/>
        <person name="Moussu S."/>
        <person name="Boutrot F."/>
        <person name="Santiago J."/>
        <person name="Zipfel C."/>
      </authorList>
      <dbReference type="PubMed" id="33514716"/>
      <dbReference type="DOI" id="10.1038/s41467-021-20932-y"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>GENE FAMILY</scope>
    <source>
      <strain>cv. Columbia</strain>
      <strain>cv. Wassilewskija-2</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2021" name="Nat. Commun." volume="12" first="5494" last="5494">
      <title>The Arabidopsis MIK2 receptor elicits immunity by sensing a conserved signature from phytocytokines and microbes.</title>
      <authorList>
        <person name="Hou S."/>
        <person name="Liu D."/>
        <person name="Huang S."/>
        <person name="Luo D."/>
        <person name="Liu Z."/>
        <person name="Xiang Q."/>
        <person name="Wang P."/>
        <person name="Mu R."/>
        <person name="Han Z."/>
        <person name="Chen S."/>
        <person name="Chai J."/>
        <person name="Shan L."/>
        <person name="He P."/>
      </authorList>
      <dbReference type="PubMed" id="34535661"/>
      <dbReference type="DOI" id="10.1038/s41467-021-25580-w"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>GENE FAMILY</scope>
    <scope>NOMENCLATURE</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2022" name="Front. Plant Sci." volume="13" first="852808" last="852808">
      <title>The MIK2/SCOOP signaling system contributes to Arabidopsis resistance against herbivory by modulating jasmonate and indole glucosinolate biosynthesis.</title>
      <authorList>
        <person name="Stahl E."/>
        <person name="Fernandez Martin A."/>
        <person name="Glauser G."/>
        <person name="Guillou M.-C."/>
        <person name="Aubourg S."/>
        <person name="Renou J.-P."/>
        <person name="Reymond P."/>
      </authorList>
      <dbReference type="PubMed" id="35401621"/>
      <dbReference type="DOI" id="10.3389/fpls.2022.852808"/>
    </citation>
    <scope>INDUCTION BY INSECT HERBIVORY</scope>
    <source>
      <strain>cv. Columbia</strain>
      <strain>cv. Wassilewskija</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="4">Brassicaceae-specific phytocytokine (plant endogenous peptide released into the apoplast) perceived by MIK2 in a BAK1/SERK3 and SERK4 coreceptors-dependent manner, that modulates various physiological and antimicrobial processes including growth prevention and reactive oxygen species (ROS) response regulation.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with MIK2 (via extracellular leucine-rich repeat domain); this interaction triggers the formation of complex between MIK2 and the BAK1/SERK3 and SERK4 coreceptors, and subsequent BAK1 activation by phosphorylation.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cell membrane</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Secreted</location>
      <location evidence="1">Extracellular space</location>
      <location evidence="1">Apoplast</location>
    </subcellularLocation>
    <text evidence="1">The precursor of SCOOP1, PROSCOOP1, accumulates at the plasma membrane and is proteolytically cleaved to release the SCOOP1 in the apoplasm.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Mostly expressed in leaves and flowers, and, to a lower extent, in seedlings shoots.</text>
  </comment>
  <comment type="induction">
    <text evidence="6">Accumulates upon infection by generalist herbivores such as Spodoptera littoralis.</text>
  </comment>
  <comment type="similarity">
    <text evidence="9">Belongs to the serine rich endogenous peptide (SCOOP) phytocytokine family.</text>
  </comment>
  <dbReference type="EMBL" id="AB024024">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002688">
    <property type="protein sequence ID" value="AED95125.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT026395">
    <property type="protein sequence ID" value="ABH04502.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_974882.1">
    <property type="nucleotide sequence ID" value="NM_203153.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q3E8H4"/>
  <dbReference type="IntAct" id="Q3E8H4">
    <property type="interactions" value="2"/>
  </dbReference>
  <dbReference type="PaxDb" id="3702-AT5G44565.1"/>
  <dbReference type="EnsemblPlants" id="AT5G44565.1">
    <property type="protein sequence ID" value="AT5G44565.1"/>
    <property type="gene ID" value="AT5G44565"/>
  </dbReference>
  <dbReference type="GeneID" id="2746190"/>
  <dbReference type="Gramene" id="AT5G44565.1">
    <property type="protein sequence ID" value="AT5G44565.1"/>
    <property type="gene ID" value="AT5G44565"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT5G44565"/>
  <dbReference type="Araport" id="AT5G44565"/>
  <dbReference type="TAIR" id="AT5G44565"/>
  <dbReference type="HOGENOM" id="CLU_2708229_0_0_1"/>
  <dbReference type="PRO" id="PR:Q3E8H4"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 5"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q3E8H4">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048046">
    <property type="term" value="C:apoplast"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030275">
    <property type="term" value="F:LRR domain binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033612">
    <property type="term" value="F:receptor serine/threonine kinase binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009625">
    <property type="term" value="P:response to insect"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009611">
    <property type="term" value="P:response to wounding"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0052">Apoplast</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000457214" description="Removed in mature form" evidence="1">
    <location>
      <begin position="29"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000457215" description="Serine rich endogenous peptide 1" evidence="1">
    <location>
      <begin status="unknown"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="48"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="SCOOP motif" evidence="10">
    <location>
      <begin position="53"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="SxS motif essential for MIK2 binding" evidence="1">
    <location>
      <begin position="59"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Polar residues" evidence="3">
    <location>
      <begin position="52"/>
      <end position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="B3H7I1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="33514716"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="34535661"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="35401621"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="33514716"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="34535661"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="9"/>
  <evidence type="ECO:0000305" key="10">
    <source>
      <dbReference type="PubMed" id="34535661"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="11">
    <source>
      <dbReference type="Araport" id="AT5G44565"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="12">
    <source>
      <dbReference type="EMBL" id="AB024024"/>
    </source>
  </evidence>
  <sequence length="73" mass="7769" checksum="01EC6D43413BE996" modified="2005-11-08" version="1" precursor="true">MGMSGSSGLVHVLMLLLLLSILFHHTESTLPSDSEQLSITGRRMMANYKPNTAVETPPSRSRRGGGGQNTGAD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>