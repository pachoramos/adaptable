<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-10-03" modified="2024-01-24" version="20" xmlns="http://uniprot.org/uniprot">
  <accession>B3EWQ0</accession>
  <name>JURTX_AVIJU</name>
  <protein>
    <recommendedName>
      <fullName>U-theraphotoxin-Aju1a</fullName>
      <shortName>U-TRTX-Aju1a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Juruin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Avicularia juruensis</name>
    <name type="common">Yellow-banded pinktoe</name>
    <dbReference type="NCBI Taxonomy" id="548547"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Mygalomorphae</taxon>
      <taxon>Theraphosidae</taxon>
      <taxon>Avicularia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2012" name="Front. Microbiol." volume="3" first="324" last="324">
      <title>Juruin: an antifungal juruentoxin peptide from the venom of the amazonian pink toe spider, Avicularia juruensis, which contains the inhibitory cystine knot motif.</title>
      <authorList>
        <person name="Ayroza G."/>
        <person name="Ferreira I.L.C."/>
        <person name="Sayegh R.S.R."/>
        <person name="Tashima A.K."/>
        <person name="Silva jr P.I."/>
      </authorList>
      <dbReference type="PubMed" id="22973266"/>
      <dbReference type="DOI" id="10.3389/fmicb.2012.00324"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has strong antifungal activity against C.albicans MDM8, C.krusei IOC 4559 (MIC=2.5-5 uM), C.glabrata IOC 45658 (MIC=2.5-5 uM), C.albicans IOC 45588 (MIC=2.5-5 uM), C.parapsilosis IOC 456416 (MIC=2.5-5 uM), C.tropicalis IOC 45608 (MIC=2.5-5 uM), C.guilliermondii IOC 455716 (MIC=2.5-5 uM) and A.niger (MIC=5-10 uM). Lacks antifungal activity against B.bassiana. Has no antibacterial effect against Gram-positive bacteria M.luteus, S.epidermidis, S.aureus or against Gram-negative bacteria E.coli and P.aeruginosa. Has no hemolytic activity against human erythrocytes. Probable ion channel inhibitor (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="2">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="4005.83" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="5">Belongs to the neurotoxin 12 (Hwtx-2) family. 03 (juruin) subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="B3EWQ0"/>
  <dbReference type="SMR" id="B3EWQ0"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0099106">
    <property type="term" value="F:ion channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012625">
    <property type="entry name" value="Toxin_20"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08089">
    <property type="entry name" value="Toxin_20"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57059">
    <property type="entry name" value="omega toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60022">
    <property type="entry name" value="HWTX_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000419529" description="U-theraphotoxin-Aju1a" evidence="3">
    <location>
      <begin position="1"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="3"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="7"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="16"/>
      <end position="35"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="D2Y2I3"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22973266"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="22973266"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="38" mass="4013" checksum="AF08D46104672437" modified="2012-10-03" version="1">FTCAISCDIKVNGKPCKGSGEKKCSGGWSCKFNVCVKV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>