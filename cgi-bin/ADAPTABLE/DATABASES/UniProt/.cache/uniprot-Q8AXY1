<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-11-16" modified="2024-11-27" version="97" xmlns="http://uniprot.org/uniprot">
  <accession>Q8AXY1</accession>
  <name>PA2A_BOTJR</name>
  <protein>
    <recommendedName>
      <fullName>Acidic phospholipase A2 BthA-1</fullName>
      <shortName>svPLA2</shortName>
      <ecNumber evidence="3">3.1.1.4</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>BOJU-III</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="9">BthA-I-PLA2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Hypotensive phospholipase A2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Phosphatidylcholine 2-acylhydrolase</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Bothrops jararacussu</name>
    <name type="common">Jararacussu</name>
    <dbReference type="NCBI Taxonomy" id="8726"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Viperidae</taxon>
      <taxon>Crotalinae</taxon>
      <taxon>Bothrops</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Protein J." volume="23" first="273" last="285">
      <title>Cloning and identification of a complete cDNA coding for a bactericidal and antitumoral acidic phospholipase A2 from Bothrops jararacussu venom.</title>
      <authorList>
        <person name="Roberto P.G."/>
        <person name="Kashima S."/>
        <person name="Marcussi S."/>
        <person name="Pereira J.O."/>
        <person name="Astolfi-Filho S."/>
        <person name="Nomizo A."/>
        <person name="Giglio J.R."/>
        <person name="Fontes M.R."/>
        <person name="Soares A.M."/>
        <person name="Franca S.C."/>
      </authorList>
      <dbReference type="PubMed" id="15214498"/>
      <dbReference type="DOI" id="10.1023/b:jopc.0000027852.92208.60"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>SUBUNIT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Protein Expr. Purif." volume="37" first="102" last="108">
      <title>Cloning and expression of an acidic platelet aggregation inhibitor phospholipase A2 cDNA from Bothrops jararacussu venom gland.</title>
      <authorList>
        <person name="Roberto P.G."/>
        <person name="Kashima S."/>
        <person name="Soares A.M."/>
        <person name="Chioato L."/>
        <person name="Faca V.M."/>
        <person name="Fuly A.L."/>
        <person name="Astolfi-Filho S."/>
        <person name="Pereira J.O."/>
        <person name="Franca S.C."/>
      </authorList>
      <dbReference type="PubMed" id="15294287"/>
      <dbReference type="DOI" id="10.1016/j.pep.2004.05.020"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] OF 17-138</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Biochem. Pharmacol." volume="64" first="723" last="732">
      <title>Structural and functional characterization of an acidic platelet aggregation inhibitor and hypotensive phospholipase A(2) from Bothrops jararacussu snake venom.</title>
      <authorList>
        <person name="Andriao-Escarso S.H."/>
        <person name="Soares A.M."/>
        <person name="Fontes M.R."/>
        <person name="Fuly A.L."/>
        <person name="Correa F.M."/>
        <person name="Rosa J.C."/>
        <person name="Greene L.J."/>
        <person name="Giglio J.R."/>
      </authorList>
      <dbReference type="PubMed" id="12167491"/>
      <dbReference type="DOI" id="10.1016/s0006-2952(02)01210-8"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 17-50</scope>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TOXIC DOSE</scope>
    <scope>CRYSTALLIZATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2004" name="Biochem. Biophys. Res. Commun." volume="323" first="24" last="31">
      <title>Crystal structure of an acidic platelet aggregation inhibitor and hypotensive phospholipase A2 in the monomeric and dimeric states: insights into its oligomeric state.</title>
      <authorList>
        <person name="Magro A.J."/>
        <person name="Murakami M.T."/>
        <person name="Marcussi S."/>
        <person name="Soares A.M."/>
        <person name="Arni R.K."/>
        <person name="Fontes M.R."/>
      </authorList>
      <dbReference type="PubMed" id="15351695"/>
      <dbReference type="DOI" id="10.1016/j.bbrc.2004.08.046"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.79 ANGSTROMS) OF 17-138 IN COMPLEX WITH CALCIUM ION</scope>
    <scope>COFACTOR</scope>
    <scope>SUBUNIT</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2005" name="Acta Crystallogr. D" volume="61" first="1670" last="1677">
      <title>Structure of BthA-I complexed with p-bromophenacyl bromide: possible correlations with lack of pharmacological activity.</title>
      <authorList>
        <person name="Magro A.J."/>
        <person name="Takeda A.A."/>
        <person name="Soares A.M."/>
        <person name="Fontes M.R."/>
      </authorList>
      <dbReference type="PubMed" id="16301802"/>
      <dbReference type="DOI" id="10.1107/s0907444905029598"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.85 ANGSTROMS) OF 17-138 IN COMPLEX WITH BROMOPHENACYL BROMIDE</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>ACTIVITY REGULATION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2006" name="Biochimie" volume="88" first="543" last="549">
      <title>Insights into metal ion binding in phospholipases A2: ultra high-resolution crystal structures of an acidic phospholipase A2 in the Ca2+ free and bound states.</title>
      <authorList>
        <person name="Murakami M.T."/>
        <person name="Gabdoulkhakov A."/>
        <person name="Genov N."/>
        <person name="Cintra A.C.O."/>
        <person name="Betzel C."/>
        <person name="Arni R.K."/>
      </authorList>
      <dbReference type="PubMed" id="16376474"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2005.10.014"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (0.97 ANGSTROMS) OF 17-138 IN COMPLEX WITH CALCIUM ION</scope>
    <scope>COFACTOR</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4 5">Snake venom phospholipase A2 (PLA2) that displays edema-inducing activities (activity that is inhibited by EDTA and dexamethasone), inhibits phospholipid-dependent collagen/ADP-induced platelet aggregation, possess hypotensive as well as anticoagulant activities. In addition, this enzyme shows bactericidal activity against E.coli and S.aureus. PLA2 catalyzes the calcium-dependent hydrolysis of the 2-acyl groups in 3-sn-phosphoglycerides.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1 2 3">
      <text>a 1,2-diacyl-sn-glycero-3-phosphocholine + H2O = a 1-acyl-sn-glycero-3-phosphocholine + a fatty acid + H(+)</text>
      <dbReference type="Rhea" id="RHEA:15801"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:28868"/>
      <dbReference type="ChEBI" id="CHEBI:57643"/>
      <dbReference type="ChEBI" id="CHEBI:58168"/>
      <dbReference type="EC" id="3.1.1.4"/>
    </reaction>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="13 14">
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </cofactor>
    <text evidence="13 14">Binds 1 Ca(2+) ion per subunit.</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="4 7">Inhibited by EDTA and bromophenacyl bromide (BPB).</text>
  </comment>
  <comment type="subunit">
    <text evidence="4 6 8">Homodimer; non-covalently linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3 4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="11 12">Expressed by the venom gland.</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">LD(50) is 25 mg/kg by intraperitoneal injection into mice.</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">LD(50) is 7.5 mg/kg by intravenous injection into mice.</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">LD(50) is 0.5 mg/kg by intracerebroventricular injection.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="11">This enzyme has been found to be not myotoxic, not cytotoxic, not hemorrhagic and not lethal.</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the phospholipase A2 family. Group II subfamily. D49 sub-subfamily.</text>
  </comment>
  <dbReference type="EC" id="3.1.1.4" evidence="3"/>
  <dbReference type="EMBL" id="AY145836">
    <property type="protein sequence ID" value="AAN37410.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="1U73">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.90 A"/>
    <property type="chains" value="A/B=17-138"/>
  </dbReference>
  <dbReference type="PDB" id="1UMV">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.79 A"/>
    <property type="chains" value="X=17-138"/>
  </dbReference>
  <dbReference type="PDB" id="1Z76">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.85 A"/>
    <property type="chains" value="A/B=17-138"/>
  </dbReference>
  <dbReference type="PDB" id="1ZL7">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="A=17-138"/>
  </dbReference>
  <dbReference type="PDB" id="1ZLB">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="0.97 A"/>
    <property type="chains" value="A=17-138"/>
  </dbReference>
  <dbReference type="PDBsum" id="1U73"/>
  <dbReference type="PDBsum" id="1UMV"/>
  <dbReference type="PDBsum" id="1Z76"/>
  <dbReference type="PDBsum" id="1ZL7"/>
  <dbReference type="PDBsum" id="1ZLB"/>
  <dbReference type="AlphaFoldDB" id="Q8AXY1"/>
  <dbReference type="SMR" id="Q8AXY1"/>
  <dbReference type="EvolutionaryTrace" id="Q8AXY1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0047498">
    <property type="term" value="F:calcium-dependent phospholipase A2 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005543">
    <property type="term" value="F:phospholipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050482">
    <property type="term" value="P:arachidonic acid secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016042">
    <property type="term" value="P:lipid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006644">
    <property type="term" value="P:phospholipid metabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008217">
    <property type="term" value="P:regulation of blood pressure"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00125">
    <property type="entry name" value="PLA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.90.10:FF:000001">
    <property type="entry name" value="Basic phospholipase A2 homolog"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.90.10">
    <property type="entry name" value="Phospholipase A2 domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001211">
    <property type="entry name" value="PLipase_A2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033112">
    <property type="entry name" value="PLipase_A2_Asp_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016090">
    <property type="entry name" value="PLipase_A2_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036444">
    <property type="entry name" value="PLipase_A2_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033113">
    <property type="entry name" value="PLipase_A2_His_AS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716:SF57">
    <property type="entry name" value="GROUP IID SECRETORY PHOSPHOLIPASE A2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716">
    <property type="entry name" value="PHOSPHOLIPASE A2 FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00068">
    <property type="entry name" value="Phospholip_A2_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00389">
    <property type="entry name" value="PHPHLIPASEA2"/>
  </dbReference>
  <dbReference type="SMART" id="SM00085">
    <property type="entry name" value="PA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48619">
    <property type="entry name" value="Phospholipase A2, PLA2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00119">
    <property type="entry name" value="PA2_ASP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00118">
    <property type="entry name" value="PA2_HIS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1203">Blood coagulation cascade inhibiting toxin</keyword>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1199">Hemostasis impairing toxin</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0382">Hypotensive agent</keyword>
  <keyword id="KW-0442">Lipid degradation</keyword>
  <keyword id="KW-0443">Lipid metabolism</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-1201">Platelet aggregation inhibiting toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000413804" description="Acidic phospholipase A2 BthA-1">
    <location>
      <begin position="17"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="active site" evidence="14">
    <location>
      <position position="63"/>
    </location>
  </feature>
  <feature type="active site" evidence="14">
    <location>
      <position position="105"/>
    </location>
  </feature>
  <feature type="binding site" evidence="6 8 15 17">
    <location>
      <position position="43"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 8 15 17">
    <location>
      <position position="47"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 8 15 17">
    <location>
      <position position="48"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 8 15 17">
    <location>
      <position position="64"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="disulfide bond" evidence="6 7 8 15 16 17">
    <location>
      <begin position="42"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="6 7 8 15 16 17">
    <location>
      <begin position="44"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="6 7 8 15 16 17">
    <location>
      <begin position="59"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="6 7 8 15 16 17">
    <location>
      <begin position="65"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="6 7 8 15 16 17">
    <location>
      <begin position="66"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="6 7 8 15 16 17">
    <location>
      <begin position="73"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="6 7 8 15 16 17">
    <location>
      <begin position="91"/>
      <end position="102"/>
    </location>
  </feature>
  <feature type="helix" evidence="18">
    <location>
      <begin position="18"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="helix" evidence="18">
    <location>
      <begin position="33"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="turn" evidence="18">
    <location>
      <begin position="41"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="strand" evidence="18">
    <location>
      <begin position="44"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="helix" evidence="18">
    <location>
      <begin position="55"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="turn" evidence="18">
    <location>
      <begin position="75"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="strand" evidence="18">
    <location>
      <begin position="82"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="strand" evidence="18">
    <location>
      <begin position="88"/>
      <end position="91"/>
    </location>
  </feature>
  <feature type="helix" evidence="18">
    <location>
      <begin position="96"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="helix" evidence="18">
    <location>
      <begin position="115"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="helix" evidence="18">
    <location>
      <begin position="121"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="helix" evidence="18">
    <location>
      <begin position="128"/>
      <end position="130"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10035"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10036"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12167491"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="15214498"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="15294287"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="15351695"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="16301802"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="16376474"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="15294287"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="12167491"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="12">
    <source>
      <dbReference type="PubMed" id="15214498"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13">
    <source>
      <dbReference type="PubMed" id="15351695"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="14">
    <source>
      <dbReference type="PubMed" id="16376474"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="15">
    <source>
      <dbReference type="PDB" id="1UMV"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="16">
    <source>
      <dbReference type="PDB" id="1Z76"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="17">
    <source>
      <dbReference type="PDB" id="1ZL7"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="18">
    <source>
      <dbReference type="PDB" id="1ZLB"/>
    </source>
  </evidence>
  <sequence length="138" mass="15456" checksum="513647907BFD0F4E" modified="2003-03-01" version="1" precursor="true">MRTLWIMAVLLVGVEGSLWQFGKMINYVMGESGVLQYLSYGCYCGLGGQGQPTDATDRCCFVHDCCYGKVTGCDPKIDSYTYSKKNGDVVCGGDDPCKKQICECDRVATTCFRDNKDTYDIKYWFYGAKNCQEKSEPC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>