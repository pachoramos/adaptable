<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-12-01" modified="2022-05-25" version="56" xmlns="http://uniprot.org/uniprot">
  <accession>P82104</accession>
  <name>CR110_RANSP</name>
  <protein>
    <recommendedName>
      <fullName>Caerin-1.10</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ranoidea splendida</name>
    <name type="common">Magnificent tree frog</name>
    <name type="synonym">Litoria splendida</name>
    <dbReference type="NCBI Taxonomy" id="30345"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Eur. J. Biochem." volume="267" first="269" last="275">
      <title>Differences in the skin peptides of the male and female Australian tree frog Litoria splendida. The discovery of the aquatic male sex pheromone splendipherin, together with Phe8 caerulein and a new antibiotic peptide caerin 1.10.</title>
      <authorList>
        <person name="Wabnitz P.A."/>
        <person name="Bowie J.H."/>
        <person name="Tyler M.J."/>
        <person name="Wallace J.C."/>
        <person name="Smith B.P."/>
      </authorList>
      <dbReference type="PubMed" id="10601876"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.2000.01010.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT LEU-25</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antibacterial peptide with wide spectrum of activity. Active against L.lactis, L.innocua, M.luteus, S.uberis and less against P.multocida and S.epidermidis.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2573.0" method="Electrospray" evidence="1"/>
  <comment type="miscellaneous">
    <text>Caerin-1.10 is not present in the glandular secretion of female L.spendida.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Caerin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P82104"/>
  <dbReference type="SMR" id="P82104"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010000">
    <property type="entry name" value="Caerin_1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07440">
    <property type="entry name" value="Caerin_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043741" description="Caerin-1.10">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="25"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10601876"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="25" mass="2576" checksum="D8A77460BB0EBE00" modified="2000-05-01" version="1">GLLSVLGSVAKHVLPHVVPVIAEKL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>