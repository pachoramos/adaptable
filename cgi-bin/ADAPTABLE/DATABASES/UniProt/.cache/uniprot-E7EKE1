<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-09-18" modified="2024-03-27" version="23" xmlns="http://uniprot.org/uniprot">
  <accession>E7EKE1</accession>
  <name>BR22B_ODOHA</name>
  <protein>
    <recommendedName>
      <fullName>Brevinin-2HS2B</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Odorrana hainanensis</name>
    <name type="common">Odor frog</name>
    <name type="synonym">Rana hainanensis</name>
    <dbReference type="NCBI Taxonomy" id="431935"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Odorrana</taxon>
    </lineage>
  </organism>
  <reference evidence="4 6" key="1">
    <citation type="journal article" date="2012" name="Peptides" volume="35" first="285" last="290">
      <title>Novel antimicrobial peptides isolated from the skin secretions of Hainan odorous frog, Odorrana hainanensis.</title>
      <authorList>
        <person name="Wang H."/>
        <person name="Yu Z."/>
        <person name="Hu Y."/>
        <person name="Li F."/>
        <person name="Liu L."/>
        <person name="Zheng H."/>
        <person name="Meng H."/>
        <person name="Yang S."/>
        <person name="Yang X."/>
        <person name="Liu J."/>
      </authorList>
      <dbReference type="PubMed" id="22450466"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2012.03.007"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue evidence="3">Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Has antimicrobial activity against some Gram-positive bacteria and fungi but has no activity against a range of Gram-negative bacteria except P.faecalis. Has antimicrobial activity against the Gram-positive bacteria S.aureus ATCC 25923 (MIC=19 uM), B.licheniformis X39 (MIC=37.5 uM) and R.rhodochrous X15 (MIC=9.5 uM), is virtually inactive against E.faecium 091299 (MIC=150 uM) and S.carnosus KHS (MIC=150 uM) and inactive against E.faecalis 981. Active against the Gram-negative bacterium P.faecalis X29 (MIC=9.5 uM) and is inactive against E.coli, P.aeruginosa and S.typhi. Active against C.albicans ATCC 2002 (MIC=19 uM) and is also active against the slime mold 090223 (MIC=37.5 uM). Has extremely low hemolytic activity against human erythrocytes (LC(50)=300 uM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="HQ735118">
    <property type="protein sequence ID" value="ADV36141.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="E7EKE1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000423535" evidence="1">
    <location>
      <begin position="23"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000423536" description="Brevinin-2HS2B" evidence="1">
    <location>
      <begin position="43"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="69"/>
      <end position="75"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0AEI5"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22450466"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="22450466"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="ADV36141.1"/>
    </source>
  </evidence>
  <sequence length="75" mass="8123" checksum="BFD3344051188E7C" modified="2011-03-08" version="1" precursor="true">MFTMKKPLLLIVLLGIISLSLCEQERAADEEEGEMIEEEVKRSLLGTVKDLLIGAGKSAAQSVLKGLSCKLSKDC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>