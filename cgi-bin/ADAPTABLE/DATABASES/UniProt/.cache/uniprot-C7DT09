<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-01-19" modified="2023-11-08" version="20" xmlns="http://uniprot.org/uniprot">
  <accession>C7DT09</accession>
  <name>VP3_ORADR</name>
  <protein>
    <recommendedName>
      <fullName evidence="6 8">Venom peptide 3</fullName>
      <shortName evidence="4">OdVP3</shortName>
      <shortName evidence="8">VP3</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Orancistrocerus drewseni</name>
    <name type="common">Solitary wasp</name>
    <dbReference type="NCBI Taxonomy" id="529024"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Eumeninae</taxon>
      <taxon>Orancistrocerus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2010" name="Toxicon" volume="55" first="711" last="718">
      <title>Isolation and molecular cloning of venom peptides from Orancistrocerus drewseni (Hymenoptera: Eumenidae).</title>
      <authorList>
        <person name="Baek J.H."/>
        <person name="Lee S.H."/>
      </authorList>
      <dbReference type="PubMed" id="19857508"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2009.10.023"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-65</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2011" name="Peptides" volume="32" first="568" last="572">
      <title>Venom peptides from solitary hunting wasps induce feeding disorder in lepidopteran larvae.</title>
      <authorList>
        <person name="Baek J.H."/>
        <person name="Ji Y."/>
        <person name="Shin J.S."/>
        <person name="Lee S."/>
        <person name="Lee S.H."/>
      </authorList>
      <dbReference type="PubMed" id="21184791"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2010.12.007"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>BIOASSAY</scope>
    <scope>SYNTHESIS OF 52-65</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3">Antimicrobial peptide with strong activity against the fungi B.cinerea (MIC=5 uM) and C.albicans (MIC=33 uM), and no activity against the Gram-negative bacterium E.coli (MIC&gt;200 uM) and the Gram-positive bacterium S.aureus (MIC&gt;200 uM) (PubMed:19857508, PubMed:21184791). Shows cytolytic activity against insect cell lines (PubMed:21184791). Has no hemolytic activity against human erythrocytes (PubMed:21184791). In vivo, peptide injection in the vicinity of the head and thorax of lepidopteran larvae induces feeding disorder that lasts one or two days before recovering (PubMed:21184791).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="5">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="7">Has an amphipathic alpha-helical conformation.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="1506.87" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the MCD family.</text>
  </comment>
  <dbReference type="EMBL" id="GQ205582">
    <property type="protein sequence ID" value="ACU30740.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="C7DT09"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000390689" evidence="5">
    <location>
      <begin position="26"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5000502625" description="Venom peptide 3" evidence="2">
    <location>
      <begin position="52"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 1" evidence="5">
    <location>
      <begin position="25"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 2" evidence="5">
    <location>
      <begin position="33"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 3" evidence="5">
    <location>
      <begin position="37"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 4" evidence="5">
    <location>
      <begin position="41"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="repeat" description="AXPX 5" evidence="5">
    <location>
      <begin position="47"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="2">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19857508"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="21184791"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="19857508"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="19857508"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="21184791"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="ACU30740.1"/>
    </source>
  </evidence>
  <sequence length="68" mass="7111" checksum="A2AF9024BD11178A" modified="2009-09-22" version="1" precursor="true">MTKQSIVIVLFAAIAMMACLQRVTAEPAPEPIAAPIAEPYANPEAIASPEAKDLHTVVSAILQALGKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>