<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-11" modified="2019-12-11" version="36" xmlns="http://uniprot.org/uniprot">
  <accession>P67802</accession>
  <accession>P09039</accession>
  <name>LSK2_RHYMA</name>
  <protein>
    <recommendedName>
      <fullName>Leucosulfakinin-2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Leucosulfakinin-II</fullName>
      <shortName>LSK-II</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Rhyparobia maderae</name>
    <name type="common">Madeira cockroach</name>
    <name type="synonym">Leucophaea maderae</name>
    <dbReference type="NCBI Taxonomy" id="36963"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Dictyoptera</taxon>
      <taxon>Blattodea</taxon>
      <taxon>Blaberoidea</taxon>
      <taxon>Blaberidae</taxon>
      <taxon>Oxyhaloinae</taxon>
      <taxon>Rhyparobia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1986" name="Biochem. Biophys. Res. Commun." volume="140" first="357" last="364">
      <title>Leucosulfakinin-II, a blocked sulfated insect neuropeptide with homology to cholecystokinin and gastrin.</title>
      <authorList>
        <person name="Nachman R.J."/>
        <person name="Holman G.M."/>
        <person name="Cook B.J."/>
        <person name="Haddon W.F."/>
        <person name="Ling N."/>
      </authorList>
      <dbReference type="PubMed" id="3778455"/>
      <dbReference type="DOI" id="10.1016/0006-291x(86)91098-3"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>SULFATION AT TYR-5</scope>
    <scope>AMIDATION AT PHE-10</scope>
  </reference>
  <comment type="function">
    <text>Changes the frequency and amplitude of contractions of the cockroach hingut. Stimulates muscle contraction of hindgut.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the gastrin/cholecystokinin family.</text>
  </comment>
  <dbReference type="PIR" id="A26335">
    <property type="entry name" value="GMROL2"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013152">
    <property type="entry name" value="Gastrin/cholecystokinin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013259">
    <property type="entry name" value="Sulfakinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08257">
    <property type="entry name" value="Sulfakinin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00259">
    <property type="entry name" value="GASTRIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0765">Sulfation</keyword>
  <feature type="peptide" id="PRO_0000043894" description="Leucosulfakinin-2">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Sulfotyrosine" evidence="1">
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="3778455"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="10" mass="1255" checksum="9B4F5391E86B5AAA" modified="2004-10-11" version="1">QSDDYGHMRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>