<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-04-22" modified="2022-10-12" version="51" xmlns="http://uniprot.org/uniprot">
  <accession>Q1EN13</accession>
  <name>DRS11_PHYSA</name>
  <protein>
    <recommendedName>
      <fullName evidence="4 5">Dermaseptin-S11</fullName>
      <shortName evidence="4 5">DRS-S11</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Phyllomedusa sauvagei</name>
    <name type="common">Sauvage's leaf frog</name>
    <dbReference type="NCBI Taxonomy" id="8395"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phyllomedusa</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Biochemistry" volume="45" first="468" last="480">
      <title>Dermaseptin S9, an alpha-helical antimicrobial peptide with a hydrophobic core and cationic termini.</title>
      <authorList>
        <person name="Lequin O."/>
        <person name="Ladram A."/>
        <person name="Chabbert L."/>
        <person name="Bruston F."/>
        <person name="Convert O."/>
        <person name="Vanhoye D."/>
        <person name="Chassaing G."/>
        <person name="Nicolas P."/>
        <person name="Amiche M."/>
      </authorList>
      <dbReference type="PubMed" id="16401077"/>
      <dbReference type="DOI" id="10.1021/bi051711i"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="Peptides" volume="29" first="2074" last="2082">
      <title>A consistent nomenclature of antimicrobial peptides isolated from frogs of the subfamily Phyllomedusinae.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="18644413"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.06.017"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial peptide with activity against Gram-positive and Gram-negative bacteria, and fungi. Has hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="7">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="6">Target cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=0939"/>
  </comment>
  <dbReference type="EMBL" id="AJ972907">
    <property type="protein sequence ID" value="CAI99866.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q1EN13"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016322">
    <property type="entry name" value="FSAP"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001822">
    <property type="entry name" value="Dermaseptin_precursor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000449595" evidence="7">
    <location>
      <begin position="23"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5004188695" description="Dermaseptin-S11">
    <location>
      <begin position="46"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="25"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Acidic residues" evidence="3">
    <location>
      <begin position="26"/>
      <end position="40"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P84923"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="16401077"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="16401077"/>
    </source>
  </evidence>
  <sequence length="75" mass="8548" checksum="D7EC408E6F84AC05" modified="2006-07-11" version="1" precursor="true">MAFLKKSLFLVLFLGMVSLSICEEEKRENEDEEEQEDDEQSEEKRALWKTLLKGAGKVFGHVAKQFLGSQGQPES</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>