<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2024-01-24" modified="2024-07-24" version="3" xmlns="http://uniprot.org/uniprot">
  <accession>P0DX55</accession>
  <name>PPNP7_POLPI</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Peptide Ppnp7</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">Neuropolybin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Polybia paulista</name>
    <name type="common">Neotropical social wasp</name>
    <name type="synonym">Swarm-founding polistine wasp</name>
    <dbReference type="NCBI Taxonomy" id="291283"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Polistinae</taxon>
      <taxon>Epiponini</taxon>
      <taxon>Polybia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2020" name="Biochem. Pharmacol." volume="181" first="114119" last="114119">
      <title>Neuropolybin: a new antiseizure peptide obtained from wasp venom.</title>
      <authorList>
        <person name="de Castro Silva J."/>
        <person name="Lopes do Couto L."/>
        <person name="de Oliveira Amaral H."/>
        <person name="Maria Medeiros Gomes F."/>
        <person name="Avohay Alves Campos G."/>
        <person name="Paulino Silva L."/>
        <person name="Renata Mortari M."/>
      </authorList>
      <dbReference type="PubMed" id="32589997"/>
      <dbReference type="DOI" id="10.1016/j.bcp.2020.114119"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>BIOSSAY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLU-1</scope>
    <scope>SYNTHESIS</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Peptide with unknown natural function. Protects against maximum seizures in a pentylenetetrazole (PTZ)-induced epileptic model. Acts in a dose-dependent manner when intracerebroventrically injected into mice and rats, but has no effect when intraperitoneally injected. Since it does not alter the spontaneous behaviors of the animals, neuropolybin could be considered for further research as a potential treatment for seizures.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="1203.0" method="MALDI" evidence="1">
    <text>Monoisotopic mass.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="4">Neuropolybin refers to the sequence pEQWQPQLHR, it is synthesized based on the inspiration drawn from the peptide Ppnp7, which contains uncertain residues.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000459136" description="Peptide Ppnp7" evidence="1">
    <location>
      <begin position="1"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid (Glu)" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="unsure residue" description="E or Q" evidence="3">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Gln or Lys" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Gln or Lys" evidence="1">
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="unsure residue" description="Leu or Ile" evidence="1">
    <location>
      <position position="7"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="32589997"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="32589997"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="32589997"/>
    </source>
  </evidence>
  <sequence length="9" mass="1221" checksum="D410E736C776C366" modified="2024-01-24" version="1">EQWQPQLHR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>