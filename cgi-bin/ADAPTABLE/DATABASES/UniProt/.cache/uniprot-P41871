<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2020-04-22" version="42" xmlns="http://uniprot.org/uniprot">
  <accession>P41871</accession>
  <name>FAR1_PLATR</name>
  <protein>
    <recommendedName>
      <fullName>FMRFamide-like neuropeptide GDPFLRF-amide</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Planorbella trivolvis</name>
    <name type="common">Marsh rams-horn</name>
    <name type="synonym">Helisoma trivolvis</name>
    <dbReference type="NCBI Taxonomy" id="283763"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Heterobranchia</taxon>
      <taxon>Euthyneura</taxon>
      <taxon>Panpulmonata</taxon>
      <taxon>Hygrophila</taxon>
      <taxon>Lymnaeoidea</taxon>
      <taxon>Planorbidae</taxon>
      <taxon>Planorbella</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Peptides" volume="15" first="31" last="36">
      <title>FMRFamide-related peptides from the kidney of the snail, Helisoma trivolvis.</title>
      <authorList>
        <person name="Madrid K.P."/>
        <person name="Price D.A."/>
        <person name="Greenberg M.J."/>
        <person name="Khan H.R."/>
        <person name="Saleuddin A.S.M."/>
      </authorList>
      <dbReference type="PubMed" id="7912428"/>
      <dbReference type="DOI" id="10.1016/0196-9781(94)90166-x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-7</scope>
    <source>
      <tissue>Kidney</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Appears to be involved in osmoregulation by affecting the kidney, mantle and skin.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Kidney, skin, mantle and the hemolymph.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the FARP (FMRFamide related peptide) family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043678" description="FMRFamide-like neuropeptide GDPFLRF-amide">
    <location>
      <begin position="1"/>
      <end position="7"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="7"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="7912428"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="7" mass="851" checksum="69D40729D76AA810" modified="1995-11-01" version="1">GDPFLRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>