<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-08-21" modified="2024-01-24" version="22" xmlns="http://uniprot.org/uniprot">
  <accession>P85216</accession>
  <name>AP2_GALME</name>
  <protein>
    <recommendedName>
      <fullName>Anionic antimicrobial peptide 2</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Galleria mellonella</name>
    <name type="common">Greater wax moth</name>
    <dbReference type="NCBI Taxonomy" id="7137"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Pyraloidea</taxon>
      <taxon>Pyralidae</taxon>
      <taxon>Galleriinae</taxon>
      <taxon>Galleria</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2007" name="Peptides" volume="28" first="533" last="546">
      <title>Purification and characterization of eight peptides from Galleria mellonella immune hemolymph.</title>
      <authorList>
        <person name="Cytrynska M."/>
        <person name="Mak P."/>
        <person name="Zdybicka-Barabas A."/>
        <person name="Suder P."/>
        <person name="Jakubowicz T."/>
      </authorList>
      <dbReference type="PubMed" id="17194500"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2006.11.010"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="1">Larval hemolymph</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antimicrobial protein. Has antibacterial activity against the Gram-positive bacteria M.luteus (MIC=86.6 uM), L.monocytogenes (MIC=86.6 uM), and S.lutea (MIC=86.6 uM). Lacks antibacterial activity against the Gram-positive bacteria B.circulans and S.aureus, and the Gram-negative bacteria E.coli D31, E.coli ATCC 25922, and S.typhimurium. Has antifungal activity against P.pastoris (MIC=86.6 uM) and P.stipitis (MIC=90.9 uM), but lacks antifungal activity against A.niger, C.albicans, C.albidus, C.fructus, C.wickerhamii, F.oxysporum, S.cerevisiae, S.pombe, T.harzianum, and Z.marxianus.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Hemolymph.</text>
  </comment>
  <comment type="induction">
    <text evidence="1">By bacterial infection.</text>
  </comment>
  <comment type="mass spectrometry" mass="6978.9" method="Electrospray" evidence="1"/>
  <dbReference type="AlphaFoldDB" id="P85216"/>
  <dbReference type="SMR" id="P85216"/>
  <dbReference type="EnsemblMetazoa" id="XM_026904151.2">
    <property type="protein sequence ID" value="XP_026759952.1"/>
    <property type="gene ID" value="LOC113519094"/>
  </dbReference>
  <dbReference type="InParanoid" id="P85216"/>
  <dbReference type="Proteomes" id="UP000504614">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000298765" description="Anionic antimicrobial peptide 2">
    <location>
      <begin position="1"/>
      <end position="60"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="17194500"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="60" mass="6980" checksum="A8BE57B003A4F182" modified="2007-08-21" version="1">ETESTPDYLKNIQQQLEEYTKNFNTQVQNAFDSDKIKSEVNNFIESLGKILNTEKKEAPK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>