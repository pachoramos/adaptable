<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-06-28" modified="2024-11-27" version="54" xmlns="http://uniprot.org/uniprot">
  <accession>B7ZFT1</accession>
  <accession>A0A2H5R1U1</accession>
  <accession>U9T9D2</accession>
  <name>GLRX1_RHIID</name>
  <protein>
    <recommendedName>
      <fullName evidence="7">Glutaredoxin-1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Glutathione-dependent oxidoreductase 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="6" type="primary">GRX1</name>
    <name type="ORF">GLOIN_2v1633026</name>
    <name type="ORF">GLOINDRAFT_350295</name>
  </gene>
  <organism>
    <name type="scientific">Rhizophagus irregularis (strain DAOM 181602 / DAOM 197198 / MUCL 43194)</name>
    <name type="common">Arbuscular mycorrhizal fungus</name>
    <name type="synonym">Glomus intraradices</name>
    <dbReference type="NCBI Taxonomy" id="747089"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Fungi incertae sedis</taxon>
      <taxon>Mucoromycota</taxon>
      <taxon>Glomeromycotina</taxon>
      <taxon>Glomeromycetes</taxon>
      <taxon>Glomerales</taxon>
      <taxon>Glomeraceae</taxon>
      <taxon>Rhizophagus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="Fungal Genet. Biol." volume="46" first="94" last="103">
      <title>GintGRX1, the first characterized glomeromycotan glutaredoxin, is a multifunctional enzyme that responds to oxidative stress.</title>
      <authorList>
        <person name="Benabdellah K."/>
        <person name="Merlos M.A."/>
        <person name="Azcon-Aguilar C."/>
        <person name="Ferrol N."/>
      </authorList>
      <dbReference type="PubMed" id="18955149"/>
      <dbReference type="DOI" id="10.1016/j.fgb.2008.09.013"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION</scope>
    <source>
      <strain>DAOM 181602 / DAOM 197198 / MUCL 43194</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2013" name="Proc. Natl. Acad. Sci. U.S.A." volume="110" first="20117" last="20122">
      <title>Genome of an arbuscular mycorrhizal fungus provides insight into the oldest plant symbiosis.</title>
      <authorList>
        <person name="Tisserant E."/>
        <person name="Malbreil M."/>
        <person name="Kuo A."/>
        <person name="Kohler A."/>
        <person name="Symeonidi A."/>
        <person name="Balestrini R."/>
        <person name="Charron P."/>
        <person name="Duensing N."/>
        <person name="Frei dit Frey N."/>
        <person name="Gianinazzi-Pearson V."/>
        <person name="Gilbert L.B."/>
        <person name="Handa Y."/>
        <person name="Herr J.R."/>
        <person name="Hijri M."/>
        <person name="Koul R."/>
        <person name="Kawaguchi M."/>
        <person name="Krajinski F."/>
        <person name="Lammers P.J."/>
        <person name="Masclaux F.G."/>
        <person name="Murat C."/>
        <person name="Morin E."/>
        <person name="Ndikumana S."/>
        <person name="Pagni M."/>
        <person name="Petitpierre D."/>
        <person name="Requena N."/>
        <person name="Rosikiewicz P."/>
        <person name="Riley R."/>
        <person name="Saito K."/>
        <person name="San Clemente H."/>
        <person name="Shapiro H."/>
        <person name="van Tuinen D."/>
        <person name="Becard G."/>
        <person name="Bonfante P."/>
        <person name="Paszkowski U."/>
        <person name="Shachar-Hill Y.Y."/>
        <person name="Tuskan G.A."/>
        <person name="Young J.P.W."/>
        <person name="Sanders I.R."/>
        <person name="Henrissat B."/>
        <person name="Rensing S.A."/>
        <person name="Grigoriev I.V."/>
        <person name="Corradi N."/>
        <person name="Roux C."/>
        <person name="Martin F."/>
      </authorList>
      <dbReference type="PubMed" id="24277808"/>
      <dbReference type="DOI" id="10.1073/pnas.1313452110"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>DAOM 181602 / DAOM 197198 / MUCL 43194</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2018" name="New Phytol." volume="220" first="1161" last="1171">
      <title>High intraspecific genome diversity in the model arbuscular mycorrhizal symbiont Rhizophagus irregularis.</title>
      <authorList>
        <person name="Chen E.C.H."/>
        <person name="Morin E."/>
        <person name="Beaudet D."/>
        <person name="Noel J."/>
        <person name="Yildirir G."/>
        <person name="Ndikumana S."/>
        <person name="Charron P."/>
        <person name="St-Onge C."/>
        <person name="Giorgi J."/>
        <person name="Krueger M."/>
        <person name="Marton T."/>
        <person name="Ropars J."/>
        <person name="Grigoriev I.V."/>
        <person name="Hainaut M."/>
        <person name="Henrissat B."/>
        <person name="Roux C."/>
        <person name="Martin F."/>
        <person name="Corradi N."/>
      </authorList>
      <dbReference type="PubMed" id="29355972"/>
      <dbReference type="DOI" id="10.1111/nph.14989"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>DAOM 181602 / DAOM 197198 / MUCL 43194</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 5 6">Multifunctional enzyme with glutathione-dependent oxidoreductase, glutathione peroxidase and glutathione S-transferase (GST) activity. The disulfide bond functions as an electron carrier in the glutathione-dependent synthesis of deoxyribonucleotides by the enzyme ribonucleotide reductase. In addition, it is also involved in reducing cytosolic protein- and non-protein-disulfides in a coupled system with glutathione reductase. May play a role in protection against oxidative stress caused by superoxide in vivo by regulating the redox state of the protein sulfhydryl groups.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Cytoplasm</location>
      <location evidence="5">Cytosol</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text evidence="5">Up-regulation of expression first detected at 24 hours after exposure to copper and reaches to a maximum after 7 days. Up-regulation of expression reaches to a maximum at 12 hours after exposure to paraquat and returns to basal levels within 24 hours.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the glutaredoxin family.</text>
  </comment>
  <comment type="sequence caution" evidence="7">
    <conflict type="erroneous gene model prediction">
      <sequence resource="EMBL-CDS" id="CAP69667" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="AM932873">
    <property type="protein sequence ID" value="CAP69667.1"/>
    <property type="status" value="ALT_SEQ"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AUPC02000148">
    <property type="protein sequence ID" value="POG68710.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B7ZFT1"/>
  <dbReference type="SMR" id="B7ZFT1"/>
  <dbReference type="VEuPathDB" id="FungiDB:RhiirFUN_007571"/>
  <dbReference type="eggNOG" id="KOG1752">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_026126_7_2_1"/>
  <dbReference type="OrthoDB" id="203654at2759"/>
  <dbReference type="Proteomes" id="UP000018888">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015038">
    <property type="term" value="F:glutathione disulfide oxidoreductase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034599">
    <property type="term" value="P:cellular response to oxidative stress"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="CDD" id="cd03419">
    <property type="entry name" value="GRX_GRXh_1_2_like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.40.30.10:FF:000026">
    <property type="entry name" value="Glutaredoxin 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.30.10">
    <property type="entry name" value="Glutaredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011767">
    <property type="entry name" value="GLR_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002109">
    <property type="entry name" value="Glutaredoxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011899">
    <property type="entry name" value="Glutaredoxin_euk/vir"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR014025">
    <property type="entry name" value="Glutaredoxin_subgr"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036249">
    <property type="entry name" value="Thioredoxin-like_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR02180">
    <property type="entry name" value="GRX_euk"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR45694">
    <property type="entry name" value="GLUTAREDOXIN 2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR45694:SF18">
    <property type="entry name" value="GLUTAREDOXIN-1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00462">
    <property type="entry name" value="Glutaredoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00160">
    <property type="entry name" value="GLUTAREDOXIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF52833">
    <property type="entry name" value="Thioredoxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00195">
    <property type="entry name" value="GLUTAREDOXIN_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51354">
    <property type="entry name" value="GLUTAREDOXIN_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0249">Electron transport</keyword>
  <keyword id="KW-0676">Redox-active center</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000410489" description="Glutaredoxin-1">
    <location>
      <begin position="1"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="domain" description="Glutaredoxin" evidence="4">
    <location>
      <begin position="5"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Redox-active" evidence="2">
    <location>
      <begin position="25"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; CAP69667." evidence="7" ref="1">
    <original>L</original>
    <variation>P</variation>
    <location>
      <position position="94"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P25373"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P68688"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000255" key="4">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00686"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="18955149"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="18955149"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="101" mass="11180" checksum="5D9B5A10662A5430" modified="2014-03-19" version="3">MSQIKDRVEKLIQTNPVMMFSKSFCPYCKKAKATLKELNVEPGICELDEDSEGRAIQDYLKEKTSQNTVPNIFIKGQHVGGCDDLLAAKDNGSLSKMIAAL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>