<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-04-26" modified="2024-11-27" version="104" xmlns="http://uniprot.org/uniprot">
  <accession>Q863T4</accession>
  <name>TFF1_CANLF</name>
  <protein>
    <recommendedName>
      <fullName>Trefoil factor 1</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">TFF1</name>
  </gene>
  <organism>
    <name type="scientific">Canis lupus familiaris</name>
    <name type="common">Dog</name>
    <name type="synonym">Canis familiaris</name>
    <dbReference type="NCBI Taxonomy" id="9615"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Carnivora</taxon>
      <taxon>Caniformia</taxon>
      <taxon>Canidae</taxon>
      <taxon>Canis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2003-03" db="EMBL/GenBank/DDBJ databases">
      <title>Canine trefoil factor 1 (TFF1) mRNA from gastric mucosa.</title>
      <authorList>
        <person name="Campbell B.G."/>
        <person name="Jabbes M."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Stomach</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Stabilizer of the mucous gel overlying the gastrointestinal mucosa that provides a physical barrier against various noxious agents.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EMBL" id="AY258287">
    <property type="protein sequence ID" value="AAP14905.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001002992.1">
    <property type="nucleotide sequence ID" value="NM_001002992.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q863T4"/>
  <dbReference type="SMR" id="Q863T4"/>
  <dbReference type="STRING" id="9615.ENSCAFP00000050000"/>
  <dbReference type="PaxDb" id="9612-ENSCAFP00000015219"/>
  <dbReference type="GeneID" id="403491"/>
  <dbReference type="KEGG" id="cfa:403491"/>
  <dbReference type="CTD" id="7031"/>
  <dbReference type="eggNOG" id="ENOG502S00P">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_179440_1_0_1"/>
  <dbReference type="InParanoid" id="Q863T4"/>
  <dbReference type="OMA" id="FPWCFHP"/>
  <dbReference type="OrthoDB" id="5322578at2759"/>
  <dbReference type="TreeFam" id="TF336092"/>
  <dbReference type="Proteomes" id="UP000002254">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694429">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694542">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000805418">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008083">
    <property type="term" value="F:growth factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030277">
    <property type="term" value="P:maintenance of gastrointestinal epithelium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd00111">
    <property type="entry name" value="Trefoil"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.110.10:FF:000001">
    <property type="entry name" value="Trefoil factor 3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.110.10">
    <property type="entry name" value="Spasmolytic Protein, domain 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR017994">
    <property type="entry name" value="P_trefoil_chordata"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR017957">
    <property type="entry name" value="P_trefoil_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000519">
    <property type="entry name" value="P_trefoil_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044913">
    <property type="entry name" value="P_trefoil_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13826">
    <property type="entry name" value="INTESTINAL TREFOIL FACTOR-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13826:SF18">
    <property type="entry name" value="TREFOIL FACTOR 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00088">
    <property type="entry name" value="Trefoil"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00680">
    <property type="entry name" value="PTREFOIL"/>
  </dbReference>
  <dbReference type="SMART" id="SM00018">
    <property type="entry name" value="PD"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57492">
    <property type="entry name" value="Trefoil"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00025">
    <property type="entry name" value="P_TREFOIL_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51448">
    <property type="entry name" value="P_TREFOIL_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0339">Growth factor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000023455" description="Trefoil factor 1">
    <location>
      <begin position="24"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="domain" description="P-type" evidence="3">
    <location>
      <begin position="26"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="28"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="38"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="48"/>
      <end position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00779"/>
    </source>
  </evidence>
  <sequence length="81" mass="8926" checksum="BE85413B9DF82E1A" modified="2003-06-01" version="1" precursor="true">MEHRVIYVLVLVCALTLSSLAQGQQETCTVAPHHRDNCGSPGITPSQCKDKGCCFDNTVRGVPWCYYPVAVDNPPEEECPF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>