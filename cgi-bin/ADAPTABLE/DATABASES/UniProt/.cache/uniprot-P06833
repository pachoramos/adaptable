<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-01-01" modified="2024-05-29" version="115" xmlns="http://uniprot.org/uniprot">
  <accession>P06833</accession>
  <name>PYY2_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>Caltrin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Calcium transport inhibitor</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Peptide YY-2</fullName>
      <shortName>Peptide YY2</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Seminalplasmin</fullName>
      <shortName>SPLN</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">PYY2</name>
  </gene>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="DNA Cell Biol." volume="9" first="437" last="442">
      <title>Characterization by cDNA cloning of the mRNA for seminalplasmin, the major basic protein of bull semen.</title>
      <authorList>
        <person name="Wagner S."/>
        <person name="Freudenstein J."/>
        <person name="Scheit K.H."/>
      </authorList>
      <dbReference type="PubMed" id="2206400"/>
      <dbReference type="DOI" id="10.1089/dna.1990.9.437"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="Biochim. Biophys. Acta" volume="1173" first="85" last="86">
      <title>Characterization of the gene for seminalplasmin, a secretory protein of the bovine seminal vesicle.</title>
      <authorList>
        <person name="Kleine Kuhlmann J."/>
        <person name="Scheit K.K."/>
      </authorList>
      <dbReference type="PubMed" id="8485159"/>
      <dbReference type="DOI" id="10.1016/0167-4781(93)90248-c"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Seminal vesicle</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1985" name="Proc. Natl. Acad. Sci. U.S.A." volume="82" first="6490" last="6491">
      <title>The structure of caltrin, the calcium-transport inhibitor of bovine seminal plasma.</title>
      <authorList>
        <person name="Lewis R.V."/>
        <person name="San Agustin J."/>
        <person name="Kruggel W."/>
        <person name="Lardy H.A."/>
      </authorList>
      <dbReference type="PubMed" id="3863108"/>
      <dbReference type="DOI" id="10.1073/pnas.82.19.6490"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 33-79</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1983" name="EMBO J." volume="2" first="1159" last="1163">
      <title>Amino acid sequence of seminalplasmin, an antimicrobial protein from bull semen.</title>
      <authorList>
        <person name="Theil R."/>
        <person name="Scheit K.H."/>
      </authorList>
      <dbReference type="PubMed" id="16453469"/>
      <dbReference type="DOI" id="10.1002/j.1460-2075.1983.tb01561.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 33-79</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1986" name="FEBS Lett." volume="201" first="233" last="236">
      <title>Seminalplasmin and caltrin are the same protein.</title>
      <authorList>
        <person name="Sitaram N."/>
        <person name="Kumari V.K."/>
        <person name="Bhargava P.M."/>
      </authorList>
      <dbReference type="PubMed" id="2423370"/>
      <dbReference type="DOI" id="10.1016/0014-5793(86)80615-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 33-77</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1990" name="Biol. Chem. Hoppe-Seyler" volume="371" first="111" last="115">
      <title>Functional properties of peptides derived from seminalplasmin: binding to monospecific anti-seminalplasmin immunoglobulins G and calmodulin.</title>
      <authorList>
        <person name="Krauhs E."/>
        <person name="Preuss K.D."/>
        <person name="Scheit K.H."/>
      </authorList>
      <dbReference type="PubMed" id="2334517"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1995" name="Bioessays" volume="17" first="415" last="422">
      <title>Seminal plasmin.</title>
      <authorList>
        <person name="Sitaram N."/>
        <person name="Nagaraj R."/>
      </authorList>
      <dbReference type="PubMed" id="7786287"/>
      <dbReference type="DOI" id="10.1002/bies.950170509"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Inhibits calcium transport into spermatozoa; it does not bind directly to calcium. Binds to calmodulin. Inhibits the growth of microorganisms. Seem to act as an antibiotic by permeabilizing the bacterial membrane.</text>
  </comment>
  <comment type="caution">
    <text evidence="2">It is uncertain whether Met-1 or Met-11 is the initiator.</text>
  </comment>
  <comment type="sequence caution" evidence="2">
    <conflict type="miscellaneous discrepancy" ref="3"/>
    <text>Transposition of two peptides and an extra Lys.</text>
  </comment>
  <dbReference type="EMBL" id="M36982">
    <property type="protein sequence ID" value="AAA30759.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X62310">
    <property type="protein sequence ID" value="CAA44190.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A35572">
    <property type="entry name" value="A35572"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_776381.1">
    <property type="nucleotide sequence ID" value="NM_173956.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P06833"/>
  <dbReference type="STRING" id="9913.ENSBTAP00000054402"/>
  <dbReference type="TCDB" id="8.A.106.1.1">
    <property type="family name" value="the caltrin/peptide yy (caltrin) family"/>
  </dbReference>
  <dbReference type="PaxDb" id="9913-ENSBTAP00000054402"/>
  <dbReference type="Ensembl" id="ENSBTAT00000065967.3">
    <property type="protein sequence ID" value="ENSBTAP00000054402.1"/>
    <property type="gene ID" value="ENSBTAG00000045652.3"/>
  </dbReference>
  <dbReference type="GeneID" id="280905"/>
  <dbReference type="KEGG" id="bta:280905"/>
  <dbReference type="CTD" id="23615"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSBTAG00000045652"/>
  <dbReference type="GeneTree" id="ENSGT00740000117120"/>
  <dbReference type="HOGENOM" id="CLU_2573321_0_0_1"/>
  <dbReference type="InParanoid" id="P06833"/>
  <dbReference type="OMA" id="SWPAMAI"/>
  <dbReference type="OrthoDB" id="5619895at2759"/>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Chromosome 19"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSBTAG00000045652">
    <property type="expression patterns" value="Expressed in mammary gland fat and 56 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005516">
    <property type="term" value="F:calmodulin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031841">
    <property type="term" value="F:neuropeptide Y receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533">
    <property type="entry name" value="NEUROPEPTIDE Y/PANCREATIC HORMONE/PEPTIDE YY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533:SF14">
    <property type="entry name" value="PEPTIDE YY-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-0112">Calmodulin-binding</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3 4 5">
    <location>
      <begin position="1"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000025403" description="Caltrin">
    <location>
      <begin position="33"/>
      <end position="80"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2334517"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0000305" key="3">
    <source>
      <dbReference type="PubMed" id="16453469"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="2423370"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="3863108"/>
    </source>
  </evidence>
  <sequence length="80" mass="8977" checksum="BEB7627C55FDA246" modified="1991-08-01" version="2" precursor="true">MMAGRRSWPAMATVLLALLVCLGELVDSKPQPSDEKASPDKHHRFSLSRYAKLANRLANPKLLETFLSKWIGDRGNRSVK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>