<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-03-28" modified="2023-09-13" version="17" xmlns="http://uniprot.org/uniprot">
  <accession>A8W7P3</accession>
  <name>PHAT_AMAOC</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Phalloidin proprotein</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="4">Phalloidin</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name evidence="4" type="primary">PHD</name>
  </gene>
  <organism>
    <name type="scientific">Amanita ocreata</name>
    <name type="common">Western North American destroying angel</name>
    <dbReference type="NCBI Taxonomy" id="235532"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Basidiomycota</taxon>
      <taxon>Agaricomycotina</taxon>
      <taxon>Agaricomycetes</taxon>
      <taxon>Agaricomycetidae</taxon>
      <taxon>Agaricales</taxon>
      <taxon>Pluteineae</taxon>
      <taxon>Amanitaceae</taxon>
      <taxon>Amanita</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2007" name="Proc. Natl. Acad. Sci. U.S.A." volume="104" first="19097" last="19101">
      <title>Gene family encoding the major toxins of lethal Amanita mushrooms.</title>
      <authorList>
        <person name="Hallen H.E."/>
        <person name="Luo H."/>
        <person name="Scott-Craig J.S."/>
        <person name="Walton J.D."/>
      </authorList>
      <dbReference type="PubMed" id="18025465"/>
      <dbReference type="DOI" id="10.1073/pnas.0707340104"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="6">Major toxin that belongs to the bicyclic heptapeptides called phallotoxins (PubMed:18025465). Although structurally related to amatoxins, phallotoxins have a different mode of action, which is the stabilization of F-actin (PubMed:18025465). Phallotoxins are poisonous when administered parenterally, but not orally because of poor absorption (PubMed:18025465).</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Processed by the macrocyclase-peptidase enzyme POPB to yield a toxic cyclic heptapeptide (By similarity). POPB first removes 10 residues from the N-terminus (By similarity). Conformational trapping of the remaining peptide forces the enzyme to release this intermediate rather than proceed to macrocyclization (By similarity). The enzyme rebinds the remaining peptide in a different conformation and catalyzes macrocyclization of the N-terminal 7 residues (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the MSDIN fungal toxin family.</text>
  </comment>
  <dbReference type="EMBL" id="EU196158">
    <property type="protein sequence ID" value="ABW87787.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A8W7P3"/>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027582">
    <property type="entry name" value="Amanitin/phalloidin"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR04309">
    <property type="entry name" value="amanitin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0883">Thioether bond</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="propeptide" id="PRO_0000443632" evidence="2">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000443633" description="Phalloidin" evidence="2">
    <location>
      <begin position="11"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000443634" evidence="2">
    <location>
      <begin position="18"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Ala-Pro)" evidence="2">
    <location>
      <begin position="11"/>
      <end position="17"/>
    </location>
  </feature>
  <feature type="cross-link" description="2'-cysteinyl-6'-hydroxytryptophan sulfoxide (Trp-Cys)" evidence="3">
    <location>
      <begin position="12"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="5">
    <location>
      <position position="31"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A067SLB9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="A8W7M4"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P85421"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="18025465"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="18025465"/>
    </source>
  </evidence>
  <sequence length="31" mass="3302" checksum="F3A1969E4A87E193" modified="2008-01-15" version="1" precursor="true" fragment="single">MSDINATRLPAWLATCPCAGDDVNPLLTRGE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>