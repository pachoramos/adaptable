<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-11-01" modified="2024-11-27" version="216" xmlns="http://uniprot.org/uniprot">
  <accession>P05496</accession>
  <name>AT5G1_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">ATP synthase F(0) complex subunit C1, mitochondrial</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>ATP synthase lipid-binding protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="6">ATP synthase membrane subunit c locus 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>ATP synthase proteolipid P1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>ATP synthase proton-transporting mitochondrial F(0) complex subunit C1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>ATPase protein 9</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>ATPase subunit c</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="6" type="primary">ATP5MC1</name>
    <name evidence="6" type="synonym">ATP5G1</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Biochem. J." volume="293" first="51" last="64">
      <title>Sequences of members of the human gene family for the c subunit of mitochondrial ATP synthase.</title>
      <authorList>
        <person name="Dyer M.R."/>
        <person name="Walker J.E."/>
      </authorList>
      <dbReference type="PubMed" id="8328972"/>
      <dbReference type="DOI" id="10.1042/bj2930051"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="Biochim. Biophys. Acta" volume="1173" first="87" last="90">
      <title>Molecular cloning and sequence of two cDNAs for human subunit c of H(+)-ATP synthase in mitochondria.</title>
      <authorList>
        <person name="Higuti T."/>
        <person name="Kawamura Y."/>
        <person name="Kuroiwa K."/>
        <person name="Miyazaki S."/>
        <person name="Tsujita H."/>
      </authorList>
      <dbReference type="PubMed" id="8485160"/>
      <dbReference type="DOI" id="10.1016/0167-4781(93)90249-d"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2001" name="Genome Res." volume="11" first="422" last="435">
      <title>Towards a catalog of human genes and proteins: sequencing and analysis of 500 novel complete protein coding human cDNAs.</title>
      <authorList>
        <person name="Wiemann S."/>
        <person name="Weil B."/>
        <person name="Wellenreuther R."/>
        <person name="Gassenhuber J."/>
        <person name="Glassl S."/>
        <person name="Ansorge W."/>
        <person name="Boecher M."/>
        <person name="Bloecker H."/>
        <person name="Bauersachs S."/>
        <person name="Blum H."/>
        <person name="Lauber J."/>
        <person name="Duesterhoeft A."/>
        <person name="Beyer A."/>
        <person name="Koehrer K."/>
        <person name="Strack N."/>
        <person name="Mewes H.-W."/>
        <person name="Ottenwaelder B."/>
        <person name="Obermaier B."/>
        <person name="Tampe J."/>
        <person name="Heubner D."/>
        <person name="Wambutt R."/>
        <person name="Korn B."/>
        <person name="Klein M."/>
        <person name="Poustka A."/>
      </authorList>
      <dbReference type="PubMed" id="11230166"/>
      <dbReference type="DOI" id="10.1101/gr.gr1547r"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="submission" date="2004-10" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning of human full-length CDSs in BD Creator(TM) system donor vector.</title>
      <authorList>
        <person name="Kalnine N."/>
        <person name="Chen X."/>
        <person name="Rolfs A."/>
        <person name="Halleck A."/>
        <person name="Hines L."/>
        <person name="Eisenstein S."/>
        <person name="Koundinya M."/>
        <person name="Raphael J."/>
        <person name="Moreira D."/>
        <person name="Kelley T."/>
        <person name="LaBaer J."/>
        <person name="Lin Y."/>
        <person name="Phelan M."/>
        <person name="Farmer A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Lung</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1987" name="Biochem. Biophys. Res. Commun." volume="144" first="1257" last="1264">
      <title>Human liver cDNA clones encoding proteolipid subunit 9 of the mitochondrial ATPase complex.</title>
      <authorList>
        <person name="Farrell L.B."/>
        <person name="Nagley P."/>
      </authorList>
      <dbReference type="PubMed" id="2883974"/>
      <dbReference type="DOI" id="10.1016/0006-291x(87)91446-x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] OF 37-136</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2019" name="FASEB J." volume="33" first="14103" last="14117">
      <title>TMEM70 facilitates biogenesis of mammalian ATP synthase by promoting subunit c incorporation into the rotor structure of the enzyme.</title>
      <authorList>
        <person name="Kovalcikova J."/>
        <person name="Vrbacky M."/>
        <person name="Pecina P."/>
        <person name="Tauchmannova K."/>
        <person name="Nuskova H."/>
        <person name="Kaplanova V."/>
        <person name="Brazdova A."/>
        <person name="Alan L."/>
        <person name="Elias J."/>
        <person name="Cunatova K."/>
        <person name="Korinek V."/>
        <person name="Sedlacek R."/>
        <person name="Mracek T."/>
        <person name="Houstek J."/>
      </authorList>
      <dbReference type="PubMed" id="31652072"/>
      <dbReference type="DOI" id="10.1096/fj.201900685rr"/>
    </citation>
    <scope>INTERACTION WITH TMEM70</scope>
    <scope>SUBUNIT</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2019" name="J. Biol. Chem." volume="294" first="1128" last="1141">
      <title>Lysine methylation by the mitochondrial methyltransferase FAM173B optimizes the function of mitochondrial ATP synthase.</title>
      <authorList>
        <person name="Malecki J.M."/>
        <person name="Willemen H.L.D.M."/>
        <person name="Pinto R."/>
        <person name="Ho A.Y.Y."/>
        <person name="Moen A."/>
        <person name="Kjoenstad I.F."/>
        <person name="Burgering B.M.T."/>
        <person name="Zwartkruis F."/>
        <person name="Eijkelkamp N."/>
        <person name="Falnes P.O."/>
      </authorList>
      <dbReference type="PubMed" id="30530489"/>
      <dbReference type="DOI" id="10.1074/jbc.ra118.005473"/>
    </citation>
    <scope>METHYLATION AT LYS-104</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2021" name="Biochim. Biophys. Acta" volume="1868" first="118942" last="118942">
      <title>TMEM70 forms oligomeric scaffolds within mitochondrial cristae promoting in situ assembly of mammalian ATP synthase proton channel.</title>
      <authorList>
        <person name="Bahri H."/>
        <person name="Buratto J."/>
        <person name="Rojo M."/>
        <person name="Dompierre J.P."/>
        <person name="Salin B."/>
        <person name="Blancard C."/>
        <person name="Cuvellier S."/>
        <person name="Rose M."/>
        <person name="Ben Ammar Elgaaied A."/>
        <person name="Tetaud E."/>
        <person name="di Rago J.P."/>
        <person name="Devin A."/>
        <person name="Duvezin-Caubet S."/>
      </authorList>
      <dbReference type="PubMed" id="33359711"/>
      <dbReference type="DOI" id="10.1016/j.bbamcr.2020.118942"/>
    </citation>
    <scope>INTERACTION WITH TMEM70</scope>
  </reference>
  <comment type="function">
    <text>Mitochondrial membrane ATP synthase (F(1)F(0) ATP synthase or Complex V) produces ATP from ADP in the presence of a proton gradient across the membrane which is generated by electron transport complexes of the respiratory chain. F-type ATPases consist of two structural domains, F(1) - containing the extramembraneous catalytic core and F(0) - containing the membrane proton channel, linked together by a central stalk and a peripheral stalk. During catalysis, ATP synthesis in the catalytic domain of F(1) is coupled via a rotary mechanism of the central stalk subunits to proton translocation. Part of the complex F(0) domain. A homomeric c-ring of probably 10 subunits is part of the complex rotary element.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 4">Homooligomer (PubMed:31652072). F-type ATPases have 2 components, CF(1) - the catalytic core - and CF(0) - the membrane proton channel. CF(1) has five subunits: alpha(3), beta(3), gamma(1), delta(1), epsilon(1). CF(0) has three main subunits: a, b and c. Component of an ATP synthase complex composed of ATP5PB, ATP5MC1, ATP5F1E, ATP5PD, ATP5ME, ATP5PF, ATP5MF, MT-ATP6, MT-ATP8, ATP5F1A, ATP5F1B, ATP5F1D, ATP5F1C, ATP5PO, ATP5MG, ATP5MK and ATP5MJ (By similarity). Interacts with TMEM70 (homooligomer form); this interaction facilitates the oligomer formation of subunit c/ATP5MC1 (c-ring) and the c-ring membrane insertion and also protects ATP5MC1 against intramitochondrial proteolysis (PubMed:31652072).</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10194585">
      <id>P05496</id>
    </interactant>
    <interactant intactId="EBI-77797">
      <id>P35609</id>
      <label>ACTN2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10194585">
      <id>P05496</id>
    </interactant>
    <interactant intactId="EBI-749955">
      <id>Q86WT6</id>
      <label>TRIM69</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>4</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-10194585">
      <id>P05496</id>
    </interactant>
    <interactant intactId="EBI-11525489">
      <id>Q86WT6-2</id>
      <label>TRIM69</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Mitochondrion membrane</location>
      <topology>Multi-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="PTM">
    <text evidence="3">Trimethylated by ATPSCKMT at Lys-104. Methylation is required for proper incorporation of the C subunit into the ATP synthase complex and mitochondrial respiration.</text>
  </comment>
  <comment type="miscellaneous">
    <text>There are three genes which encode the mitochondrial ATP synthase proteolipid and they specify precursors with different import sequences but identical mature proteins. Is the major protein stored in the storage bodies of animals or humans affected with ceroid lipofuscinosis (Batten disease).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the ATPase C chain family.</text>
  </comment>
  <dbReference type="EMBL" id="X69907">
    <property type="protein sequence ID" value="CAA49532.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D13118">
    <property type="protein sequence ID" value="BAA02420.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL080089">
    <property type="protein sequence ID" value="CAB45704.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT007230">
    <property type="protein sequence ID" value="AAP35894.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC004963">
    <property type="protein sequence ID" value="AAH04963.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M16453">
    <property type="protein sequence ID" value="AAA51806.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS11539.1"/>
  <dbReference type="PIR" id="S34066">
    <property type="entry name" value="S34066"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001002027.1">
    <property type="nucleotide sequence ID" value="NM_001002027.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_005166.1">
    <property type="nucleotide sequence ID" value="NM_005175.2"/>
  </dbReference>
  <dbReference type="PDB" id="8H9F">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.69 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDB" id="8H9J">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.26 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDB" id="8H9M">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.00 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDB" id="8H9Q">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.47 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDB" id="8H9S">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.53 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDB" id="8H9T">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.77 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDB" id="8H9U">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.61 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDB" id="8H9V">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.02 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDB" id="8KHF">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.13 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDB" id="8KI3">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.89 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8=62-136"/>
  </dbReference>
  <dbReference type="PDBsum" id="8H9F"/>
  <dbReference type="PDBsum" id="8H9J"/>
  <dbReference type="PDBsum" id="8H9M"/>
  <dbReference type="PDBsum" id="8H9Q"/>
  <dbReference type="PDBsum" id="8H9S"/>
  <dbReference type="PDBsum" id="8H9T"/>
  <dbReference type="PDBsum" id="8H9U"/>
  <dbReference type="PDBsum" id="8H9V"/>
  <dbReference type="PDBsum" id="8KHF"/>
  <dbReference type="PDBsum" id="8KI3"/>
  <dbReference type="AlphaFoldDB" id="P05496"/>
  <dbReference type="EMDB" id="EMD-34565"/>
  <dbReference type="EMDB" id="EMD-34569"/>
  <dbReference type="EMDB" id="EMD-34573"/>
  <dbReference type="EMDB" id="EMD-34577"/>
  <dbReference type="EMDB" id="EMD-34580"/>
  <dbReference type="EMDB" id="EMD-34581"/>
  <dbReference type="EMDB" id="EMD-34582"/>
  <dbReference type="EMDB" id="EMD-34583"/>
  <dbReference type="EMDB" id="EMD-37243"/>
  <dbReference type="EMDB" id="EMD-37251"/>
  <dbReference type="SMR" id="P05496"/>
  <dbReference type="BioGRID" id="107001">
    <property type="interactions" value="39"/>
  </dbReference>
  <dbReference type="ComplexPortal" id="CPX-6151">
    <property type="entry name" value="Mitochondrial proton-transporting ATP synthase complex"/>
  </dbReference>
  <dbReference type="CORUM" id="P05496"/>
  <dbReference type="IntAct" id="P05496">
    <property type="interactions" value="27"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000377033"/>
  <dbReference type="TCDB" id="3.A.2.1.15">
    <property type="family name" value="the h+- or na+-translocating f-type, v-type and a-type atpase (f-atpase) superfamily"/>
  </dbReference>
  <dbReference type="iPTMnet" id="P05496"/>
  <dbReference type="PhosphoSitePlus" id="P05496"/>
  <dbReference type="BioMuta" id="ATP5G1"/>
  <dbReference type="jPOST" id="P05496"/>
  <dbReference type="MassIVE" id="P05496"/>
  <dbReference type="PaxDb" id="9606-ENSP00000377033"/>
  <dbReference type="PeptideAtlas" id="P05496"/>
  <dbReference type="ProteomicsDB" id="51842"/>
  <dbReference type="Pumba" id="P05496"/>
  <dbReference type="TopDownProteomics" id="P05496"/>
  <dbReference type="Antibodypedia" id="30318">
    <property type="antibodies" value="123 antibodies from 28 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="516"/>
  <dbReference type="Ensembl" id="ENST00000355938.9">
    <property type="protein sequence ID" value="ENSP00000348205.5"/>
    <property type="gene ID" value="ENSG00000159199.14"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000393366.7">
    <property type="protein sequence ID" value="ENSP00000377033.2"/>
    <property type="gene ID" value="ENSG00000159199.14"/>
  </dbReference>
  <dbReference type="GeneID" id="516"/>
  <dbReference type="KEGG" id="hsa:516"/>
  <dbReference type="MANE-Select" id="ENST00000393366.7">
    <property type="protein sequence ID" value="ENSP00000377033.2"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_005175.3"/>
    <property type="RefSeq protein sequence ID" value="NP_005166.1"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:841"/>
  <dbReference type="CTD" id="516"/>
  <dbReference type="DisGeNET" id="516"/>
  <dbReference type="GeneCards" id="ATP5MC1"/>
  <dbReference type="HGNC" id="HGNC:841">
    <property type="gene designation" value="ATP5MC1"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000159199">
    <property type="expression patterns" value="Tissue enhanced (tongue)"/>
  </dbReference>
  <dbReference type="MIM" id="603192">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P05496"/>
  <dbReference type="OpenTargets" id="ENSG00000159199"/>
  <dbReference type="PharmGKB" id="PA25131"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000159199"/>
  <dbReference type="eggNOG" id="KOG3025">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000154298"/>
  <dbReference type="InParanoid" id="P05496"/>
  <dbReference type="OMA" id="KIIGTGM"/>
  <dbReference type="OrthoDB" id="316029at2759"/>
  <dbReference type="PhylomeDB" id="P05496"/>
  <dbReference type="TreeFam" id="TF300140"/>
  <dbReference type="BioCyc" id="MetaCyc:ENSG00000159199-MONOMER"/>
  <dbReference type="PathwayCommons" id="P05496"/>
  <dbReference type="Reactome" id="R-HSA-1268020">
    <property type="pathway name" value="Mitochondrial protein import"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-163210">
    <property type="pathway name" value="Formation of ATP by chemiosmotic coupling"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-8949613">
    <property type="pathway name" value="Cristae formation"/>
  </dbReference>
  <dbReference type="SignaLink" id="P05496"/>
  <dbReference type="SIGNOR" id="P05496"/>
  <dbReference type="BioGRID-ORCS" id="516">
    <property type="hits" value="73 hits in 1146 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="ATP5G1">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="GeneWiki" id="ATP5G1"/>
  <dbReference type="GenomeRNAi" id="516"/>
  <dbReference type="Pharos" id="P05496">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P05496"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 17"/>
  </dbReference>
  <dbReference type="RNAct" id="P05496">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000159199">
    <property type="expression patterns" value="Expressed in apex of heart and 200 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P05496">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005743">
    <property type="term" value="C:mitochondrial inner membrane"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="LIFEdb"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045259">
    <property type="term" value="C:proton-transporting ATP synthase complex"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045263">
    <property type="term" value="C:proton-transporting ATP synthase complex, coupling factor F(o)"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008289">
    <property type="term" value="F:lipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015078">
    <property type="term" value="F:proton transmembrane transporter activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015986">
    <property type="term" value="P:proton motive force-driven ATP synthesis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd18182">
    <property type="entry name" value="ATP-synt_Fo_c_ATP5G3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.20.10:FF:000003">
    <property type="entry name" value="Atp synthase f complex subunit mitochondrial"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.20.10">
    <property type="entry name" value="F1F0 ATP synthase subunit C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01396">
    <property type="entry name" value="ATP_synth_c_bact"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000454">
    <property type="entry name" value="ATP_synth_F0_csu"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020537">
    <property type="entry name" value="ATP_synth_F0_csu_DDCD_BS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038662">
    <property type="entry name" value="ATP_synth_F0_csu_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002379">
    <property type="entry name" value="ATPase_proteolipid_c-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035921">
    <property type="entry name" value="F/V-ATP_Csub_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10031">
    <property type="entry name" value="ATP SYNTHASE LIPID-BINDING PROTEIN, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10031:SF0">
    <property type="entry name" value="ATPASE PROTEIN 9"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00137">
    <property type="entry name" value="ATP-synt_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00124">
    <property type="entry name" value="ATPASEC"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF81333">
    <property type="entry name" value="F1F0 ATP synthase subunit C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00605">
    <property type="entry name" value="ATPASE_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0138">CF(0)</keyword>
  <keyword id="KW-0375">Hydrogen ion transport</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-0446">Lipid-binding</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0488">Methylation</keyword>
  <keyword id="KW-0496">Mitochondrion</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0809">Transit peptide</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="transit peptide" description="Mitochondrion">
    <location>
      <begin position="1"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000002557" description="ATP synthase F(0) complex subunit C1, mitochondrial">
    <location>
      <begin position="62"/>
      <end position="136"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="77"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="112"/>
      <end position="132"/>
    </location>
  </feature>
  <feature type="site" description="Reversibly protonated during proton transport" evidence="1">
    <location>
      <position position="119"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6,N6,N6-trimethyllysine" evidence="3">
    <location>
      <position position="104"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 6; AAA51806." evidence="5" ref="6">
    <original>E</original>
    <variation>G</variation>
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="63"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="80"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="101"/>
      <end position="103"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="104"/>
      <end position="133"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="30530489"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="31652072"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="HGNC" id="HGNC:841"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="8H9M"/>
    </source>
  </evidence>
  <sequence length="136" mass="14277" checksum="1AFF1F16BB532647" modified="1994-02-01" version="2" precursor="true">MQTAGALFISPALIRCCTRGLIRPVSASFLNSPVNSSKQPSYSNFPLQVARREFQTSVVSRDIDTAAKFIGAGAATVGVAGSGAGIGTVFGSLIIGYARNPSLKQQLFSYAILGFALSEAMGLFCLMVAFLILFAM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>