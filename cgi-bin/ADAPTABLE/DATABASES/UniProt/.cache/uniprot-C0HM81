<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-09-13" modified="2023-11-08" version="2" xmlns="http://uniprot.org/uniprot">
  <accession>C0HM81</accession>
  <name>GRP1_ELYRE</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Glycine-rich cell wall structural protein</fullName>
      <shortName evidence="3">ER-GRP</shortName>
      <shortName evidence="3">GRP</shortName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Elymus repens</name>
    <name type="common">Quackgrass</name>
    <name type="synonym">Agropyron repens</name>
    <dbReference type="NCBI Taxonomy" id="52152"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>Liliopsida</taxon>
      <taxon>Poales</taxon>
      <taxon>Poaceae</taxon>
      <taxon>BOP clade</taxon>
      <taxon>Pooideae</taxon>
      <taxon>Triticodae</taxon>
      <taxon>Triticeae</taxon>
      <taxon>Hordeinae</taxon>
      <taxon>Elymus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2023" name="Appl. Biochem. Microbiol." volume="59" first="41" last="47">
      <title>The Antifungal and Reactivation Activities of a Novel Glycine/Histidine-Rich Linear Peptide from Dog-Grass (Elytrigia repens (L.) Desv. Ex Nevski) Ears.</title>
      <authorList>
        <person name="Ryazantsev D.Y."/>
        <person name="Khodzhaev E.Y."/>
        <person name="Kuvarina A.E."/>
        <person name="Barashkova A.S."/>
        <person name="Rogozhin E.A."/>
      </authorList>
      <dbReference type="DOI" id="10.1134/S000368382301009X"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text evidence="2 4">Responsible for plasticity of the cell wall (Probable). Might play a role in wound healing and plant disease resistance (Ref.1). Displays weak antifungal activity towards A.niger INA 00760, A.fumigatus CPB F-37 and C.albicans ATCC 2091 (Ref.1). May have regulatory properties as it increases the level of S.cerevisiae VKPM Y-1200 survival following severe UV irradiation (Ref.1).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
      <location evidence="4">Cell wall</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="3878.0" error="1.0" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the GRP family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0134">Cell wall</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000458747" description="Glycine-rich cell wall structural protein">
    <location>
      <begin position="1"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="1"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic residues" evidence="1">
    <location>
      <begin position="29"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="3">
    <location>
      <position position="43"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="43" mass="3878" checksum="3419038BB7C7E900" modified="2023-09-13" version="1" fragment="single">GGGGGGGGYPGHGGGGGGYPGHGGGGGGGGGRHRHRHRHRHTA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>