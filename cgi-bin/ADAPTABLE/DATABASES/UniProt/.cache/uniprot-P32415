<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-10-01" modified="2023-05-03" version="51" xmlns="http://uniprot.org/uniprot">
  <accession>P32415</accession>
  <name>HPA1_PELLE</name>
  <protein>
    <recommendedName>
      <fullName>Hemolytic protein A1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pelophylax lessonae</name>
    <name type="common">Pool frog</name>
    <name type="synonym">Rana lessonae</name>
    <dbReference type="NCBI Taxonomy" id="45623"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Pelophylax</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="Biochim. Biophys. Acta" volume="1033" first="318" last="323">
      <title>Purification and characterization of bioactive peptides from skin extracts of Rana esculenta.</title>
      <authorList>
        <person name="Simmaco M."/>
        <person name="de Biase D."/>
        <person name="Severini C."/>
        <person name="Aita M."/>
        <person name="Erspamer G.F."/>
        <person name="Barra D."/>
        <person name="Bossa F."/>
      </authorList>
      <dbReference type="PubMed" id="2317508"/>
      <dbReference type="DOI" id="10.1016/0304-4165(90)90140-r"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-13</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Shows hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <dbReference type="PIR" id="S09018">
    <property type="entry name" value="S09018"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000190092" description="Hemolytic protein A1">
    <location>
      <begin position="1"/>
      <end position="13" status="greater than"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2317508"/>
    </source>
  </evidence>
  <sequence length="13" mass="1390" checksum="C6BA768B9DFE587D" modified="1993-10-01" version="1" fragment="single">FLPAIAGILSQLF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>