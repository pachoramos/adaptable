<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-05-02" modified="2019-12-11" version="16" xmlns="http://uniprot.org/uniprot">
  <accession>P84822</accession>
  <name>TY4_ASCTR</name>
  <protein>
    <recommendedName>
      <fullName>Tryptophyllin-4</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ascaphus truei</name>
    <name type="common">Coastal tailed frog</name>
    <dbReference type="NCBI Taxonomy" id="8439"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Ascaphidae</taxon>
      <taxon>Ascaphus</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2005" name="Gen. Comp. Endocrinol." volume="143" first="193" last="199">
      <title>Bradykinin-related peptides and tryptophyllins in the skin secretions of the most primitive extant frog, Ascaphus truei.</title>
      <authorList>
        <person name="Conlon J.M."/>
        <person name="Jouenne T."/>
        <person name="Cosette P."/>
        <person name="Cosquer D."/>
        <person name="Vaudry H."/>
        <person name="Taylor C.K."/>
        <person name="Abel P.W."/>
      </authorList>
      <dbReference type="PubMed" id="15922344"/>
      <dbReference type="DOI" id="10.1016/j.ygcen.2005.04.006"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="1">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Putative defense peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1184.6" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Tryptophillin subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000233927" description="Tryptophyllin-4">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="15922344"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="9" mass="1185" checksum="27C27AB36771B417" modified="2006-05-02" version="1">EPRTPWDWV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>