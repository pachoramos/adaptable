<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2024-07-24" version="66" xmlns="http://uniprot.org/uniprot">
  <accession>Q86305</accession>
  <accession>Q4KRX0</accession>
  <name>NS2_HRSV</name>
  <protein>
    <recommendedName>
      <fullName>Non-structural protein 2</fullName>
      <shortName>NS2</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Non-structural protein 1B</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">1B</name>
    <name type="synonym">NS2</name>
  </gene>
  <organism>
    <name type="scientific">Human respiratory syncytial virus</name>
    <dbReference type="NCBI Taxonomy" id="11250"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Orthornavirae</taxon>
      <taxon>Negarnaviricota</taxon>
      <taxon>Haploviricotina</taxon>
      <taxon>Monjiviricetes</taxon>
      <taxon>Mononegavirales</taxon>
      <taxon>Pneumoviridae</taxon>
      <taxon>Orthopneumovirus</taxon>
      <taxon>Orthopneumovirus hominis</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="submission" date="1995-08" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mazumder B."/>
        <person name="Dupuy L.C."/>
        <person name="McLean T."/>
        <person name="Barik S."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="J. Virol." volume="79" first="9315" last="9319">
      <title>Respiratory syncytial virus nonstructural proteins NS1 and NS2 mediate inhibition of Stat2 expression and alpha/beta interferon responsiveness.</title>
      <authorList>
        <person name="Lo M.S."/>
        <person name="Brazas R.M."/>
        <person name="Holtzman M.J."/>
      </authorList>
      <dbReference type="PubMed" id="15994826"/>
      <dbReference type="DOI" id="10.1128/jvi.79.14.9315-9319.2005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC VR-26</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2019" name="Sci. Rep." volume="9" first="15258" last="15258">
      <title>The Interactome analysis of the Respiratory Syncytial Virus protein M2-1 suggests a new role in viral mRNA metabolism post-transcription.</title>
      <authorList>
        <person name="Bouillier C."/>
        <person name="Cosentino G."/>
        <person name="Leger T."/>
        <person name="Rincheval V."/>
        <person name="Richard C.A."/>
        <person name="Desquesnes A."/>
        <person name="Sitterlin D."/>
        <person name="Blouquit-Laye S."/>
        <person name="Eleouet J.F."/>
        <person name="Gault E."/>
        <person name="Rameix-Welti M.A."/>
      </authorList>
      <dbReference type="PubMed" id="31649314"/>
      <dbReference type="DOI" id="10.1038/s41598-019-51746-0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2021" name="Nature" volume="595" first="596" last="599">
      <title>A condensate-hardening drug blocks RSV replication in vivo.</title>
      <authorList>
        <person name="Risso-Ballester J."/>
        <person name="Galloux M."/>
        <person name="Cao J."/>
        <person name="Le Goffic R."/>
        <person name="Hontonnou F."/>
        <person name="Jobart-Malfait A."/>
        <person name="Desquesnes A."/>
        <person name="Sake S.M."/>
        <person name="Haid S."/>
        <person name="Du M."/>
        <person name="Zhang X."/>
        <person name="Zhang H."/>
        <person name="Wang Z."/>
        <person name="Rincheval V."/>
        <person name="Zhang Y."/>
        <person name="Pietschmann T."/>
        <person name="Eleouet J.F."/>
        <person name="Rameix-Welti M.A."/>
        <person name="Altmeyer R."/>
      </authorList>
      <dbReference type="PubMed" id="34234347"/>
      <dbReference type="DOI" id="10.1038/s41586-021-03703-z"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2019" name="PLoS Pathog." volume="15" first="e1007984" last="e1007984">
      <title>Respiratory syncytial virus nonstructural proteins 1 and 2: Exceptional disrupters of innate immune responses.</title>
      <authorList>
        <person name="Sedeyn K."/>
        <person name="Schepens B."/>
        <person name="Saelens X."/>
      </authorList>
      <dbReference type="PubMed" id="31622448"/>
      <dbReference type="DOI" id="10.1371/journal.ppat.1007984"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2020" name="Front. Cell. Infect. Microbiol." volume="10" first="225" last="225">
      <title>Respiratory Syncytial Virus's Non-structural Proteins: Masters of Interference.</title>
      <authorList>
        <person name="Thornhill E.M."/>
        <person name="Verhoeven D."/>
      </authorList>
      <dbReference type="PubMed" id="32509597"/>
      <dbReference type="DOI" id="10.3389/fcimb.2020.00225"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays a major role in antagonizing the type I IFN-mediated antiviral response. Acts cooperatively with NS1 to repress activation and nuclear translocation of host IFN-regulatory factor IRF3. Interacts with the host cytoplasmic sensor of viral nucleic acids RIGI and prevents the interaction with its downstream partner MAVS. Together with NS2, participates in the proteasomal degradation of host STAT2, IRF3, IRF7, TBK1 and RIGI through a NS-degradasome involving CUL2 and Elongin-C. The degradasome requires an intact mitochondrial MAVS. Induces host SOCS1 expression. Induces activation of NF-kappa-B. Suppresses premature apoptosis by an NF-kappa-B-dependent, interferon-independent mechanism promoting continued viral replication.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer (instable). Homomultimer. Heteromultimer with NS1. Interacts with host RIGI (via N-terminus); this interaction prevents host signaling pathway involved in interferon production. Interacts with host MAP1B/microtubule-associated protein 1B.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host mitochondrion</location>
    </subcellularLocation>
    <text evidence="1">Most NS2 resides in the mitochondria as a heteromer with NS1.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The DNLP motif has IFN suppressive functions like binding to host MAP1B.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the pneumovirus non-structural protein 2 family.</text>
  </comment>
  <dbReference type="EMBL" id="U35029">
    <property type="protein sequence ID" value="AAA79090.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY911262">
    <property type="protein sequence ID" value="AAX23988.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="MT994243">
    <property type="protein sequence ID" value="QXO84947.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KF713490">
    <property type="protein sequence ID" value="AHC94757.1"/>
    <property type="molecule type" value="Viral_cRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KF713491">
    <property type="protein sequence ID" value="AHC94768.1"/>
    <property type="molecule type" value="Viral_cRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KF713492">
    <property type="protein sequence ID" value="AHC94780.1"/>
    <property type="molecule type" value="Viral_cRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KP258707">
    <property type="protein sequence ID" value="AIZ95612.1"/>
    <property type="molecule type" value="Viral_cRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KU316170">
    <property type="protein sequence ID" value="AMA67212.1"/>
    <property type="molecule type" value="Viral_cRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KU707921">
    <property type="protein sequence ID" value="AMQ35397.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="KX348546">
    <property type="protein sequence ID" value="API65184.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="MK810782">
    <property type="protein sequence ID" value="QFX69106.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="MK816924">
    <property type="protein sequence ID" value="QFX69117.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="EMBL" id="MW039343">
    <property type="protein sequence ID" value="QPB74357.1"/>
    <property type="molecule type" value="Viral_cRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="MT994242">
    <property type="protein sequence ID" value="QXO84936.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="PDB" id="7LDK">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.82 A"/>
    <property type="chains" value="A/B/C=1-124"/>
  </dbReference>
  <dbReference type="PDBsum" id="7LDK"/>
  <dbReference type="SMR" id="Q86305"/>
  <dbReference type="Proteomes" id="UP000103294">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000104732">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000119304">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000130886">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000138938">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000158141">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000163705">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033650">
    <property type="term" value="C:host cell mitochondrion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052150">
    <property type="term" value="P:symbiont-mediated perturbation of host apoptosis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039548">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of IRF3 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039557">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of IRF7 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039540">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of RIG-I activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039723">
    <property type="term" value="P:symbiont-mediated suppression of host cytoplasmic pattern recognition receptor signaling pathway via inhibition of TBK1 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039564">
    <property type="term" value="P:symbiont-mediated suppression of host JAK-STAT cascade via inhibition of STAT2 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039722">
    <property type="term" value="P:symbiont-mediated suppression of host toll-like receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039502">
    <property type="term" value="P:symbiont-mediated suppression of host type I interferon-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004336">
    <property type="entry name" value="RSV_NS2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03113">
    <property type="entry name" value="RSV_NS2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-1045">Host mitochondrion</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1114">Inhibition of host interferon signaling pathway by virus</keyword>
  <keyword id="KW-1092">Inhibition of host IRF3 by virus</keyword>
  <keyword id="KW-1093">Inhibition of host IRF7 by virus</keyword>
  <keyword id="KW-1088">Inhibition of host RIG-I by virus</keyword>
  <keyword id="KW-1113">Inhibition of host RLR pathway by virus</keyword>
  <keyword id="KW-1106">Inhibition of host STAT2 by virus</keyword>
  <keyword id="KW-1223">Inhibition of host TBK1 by virus</keyword>
  <keyword id="KW-1225">Inhibition of host TLR pathway by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-1119">Modulation of host cell apoptosis by virus</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <feature type="chain" id="PRO_0000142791" description="Non-structural protein 2">
    <location>
      <begin position="1"/>
      <end position="124"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="DLNP; interaction with MAP1B" evidence="1">
    <location>
      <begin position="121"/>
      <end position="124"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="23"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="37"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="54"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="93"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="102"/>
      <end position="109"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="111"/>
      <end position="114"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="119"/>
      <end position="121"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P04543"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="7LDK"/>
    </source>
  </evidence>
  <sequence length="124" mass="14705" checksum="98657318FEB744E1" modified="1996-11-01" version="1">MDTTHNDTTPQRLMITDMRPLSLETTITSLTRDIITHRFIYLINHECIVRKLDERQATFTFLVNYEMKLLHKVGSTKYKKYTEYNTKYGTFPMPIFINHDGFLECIGIKPTKHTPIIYKYDLNP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>