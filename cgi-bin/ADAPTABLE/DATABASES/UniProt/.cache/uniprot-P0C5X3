<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-11-13" modified="2022-12-14" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>P0C5X3</accession>
  <name>BR2C_RANDY</name>
  <protein>
    <recommendedName>
      <fullName>Brevinin-2DYc</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Rana dybowskii</name>
    <name type="common">Dybovsky's frog</name>
    <name type="synonym">Korean brown frog</name>
    <dbReference type="NCBI Taxonomy" id="71582"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Rana</taxon>
      <taxon>Rana</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2007" name="Toxicon" volume="50" first="746" last="756">
      <title>Cytolytic peptides belonging to the brevinin-1 and brevinin-2 families isolated from the skin of the Japanese brown frog, Rana dybowskii.</title>
      <authorList>
        <person name="Conlon J.M."/>
        <person name="Kolodziejek J."/>
        <person name="Nowotny N."/>
        <person name="Leprince J."/>
        <person name="Vaudry H."/>
        <person name="Coquet L."/>
        <person name="Jouenne T."/>
        <person name="Iwamuro S."/>
      </authorList>
      <dbReference type="PubMed" id="17688900"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2007.06.023"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Antimicrobial peptide. A mixture of Brevinin-2DYc/2DYd is active against the Gram-positive bacterium S.aureus (MIC=15 uM) and the Gram-negative bacterium E.coli (MIC=15 uM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3314.0" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0C5X3"/>
  <dbReference type="SMR" id="P0C5X3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000311601" description="Brevinin-2DYc">
    <location>
      <begin position="1"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="27"/>
      <end position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="17688900"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="33" mass="3318" checksum="DCB25A53FB990015" modified="2007-11-13" version="1">GLFDVVKGVLKGVGKNVAGSLLEQLKCKLSGGC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>