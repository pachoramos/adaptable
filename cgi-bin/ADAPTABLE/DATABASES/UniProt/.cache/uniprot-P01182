<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="92" xmlns="http://uniprot.org/uniprot">
  <accession>P01182</accession>
  <name>NEU2_HORSE</name>
  <protein>
    <recommendedName>
      <fullName>Neurophysin 2</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">AVP</name>
  </gene>
  <organism>
    <name type="scientific">Equus caballus</name>
    <name type="common">Horse</name>
    <dbReference type="NCBI Taxonomy" id="9796"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Perissodactyla</taxon>
      <taxon>Equidae</taxon>
      <taxon>Equus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1977" name="FEBS Lett." volume="80" first="374" last="376">
      <title>Phylogeny of the neurophysins: complete amino acid sequence of horse MSEL-neurophysin.</title>
      <authorList>
        <person name="Chauvet M.-T."/>
        <person name="Codogno P."/>
        <person name="Chauvet J."/>
        <person name="Acher R."/>
      </authorList>
      <dbReference type="PubMed" id="891988"/>
      <dbReference type="DOI" id="10.1016/0014-5793(77)80479-1"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <comment type="function">
    <text>Neurophysin 2 specifically binds the midbrain peptide hormone vasopressin.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the vasopressin/oxytocin family.</text>
  </comment>
  <dbReference type="PIR" id="A01444">
    <property type="entry name" value="NFHO2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01182"/>
  <dbReference type="SMR" id="P01182"/>
  <dbReference type="STRING" id="9796.ENSECAP00000022402"/>
  <dbReference type="PaxDb" id="9796-ENSECAP00000044610"/>
  <dbReference type="InParanoid" id="P01182"/>
  <dbReference type="Proteomes" id="UP000002281">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030141">
    <property type="term" value="C:secretory granule"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005185">
    <property type="term" value="F:neurohypophyseal hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031894">
    <property type="term" value="F:V1A vasopressin receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.9.10:FF:000001">
    <property type="entry name" value="oxytocin-neurophysin 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.9.10">
    <property type="entry name" value="Neurohypophysial hormone domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000981">
    <property type="entry name" value="Neurhyp_horm"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036387">
    <property type="entry name" value="Neurhyp_horm_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11681">
    <property type="entry name" value="NEUROPHYSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11681:SF9">
    <property type="entry name" value="VASOPRESSIN-NEUROPHYSIN 2-COPEPTIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00184">
    <property type="entry name" value="Hormone_5"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00831">
    <property type="entry name" value="NEUROPHYSIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00003">
    <property type="entry name" value="NH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF49606">
    <property type="entry name" value="Neurophysin II"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000160936" description="Neurophysin 2">
    <location>
      <begin position="1" status="less than"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="7"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="10"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="18"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="25"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="58"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="64"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="71"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01175"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="92" mass="9502" checksum="8B163CBE5D8C5CF8" modified="1986-07-21" version="1" fragment="single">DLELRQCLPCGPGGKGRCFGPSICCGDELGCFVGTAEALRCQEENYLPSPCQSGQKPCGSGGRCAAAGICCNDESCVTEPECREGAGLPRRA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>