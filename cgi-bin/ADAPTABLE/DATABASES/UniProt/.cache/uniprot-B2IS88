<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-04-14" modified="2024-11-27" version="81" xmlns="http://uniprot.org/uniprot">
  <accession>B2IS88</accession>
  <name>RPOZ_STRPS</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">DNA-directed RNA polymerase subunit omega</fullName>
      <shortName evidence="1">RNAP omega subunit</shortName>
      <ecNumber evidence="1">2.7.7.6</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">RNA polymerase omega subunit</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Transcriptase subunit omega</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">rpoZ</name>
    <name type="ordered locus">SPCG_1711</name>
  </gene>
  <organism>
    <name type="scientific">Streptococcus pneumoniae (strain CGSP14)</name>
    <dbReference type="NCBI Taxonomy" id="516950"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Streptococcaceae</taxon>
      <taxon>Streptococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="BMC Genomics" volume="10" first="158" last="158">
      <title>Genome evolution driven by host adaptations results in a more virulent and antimicrobial-resistant Streptococcus pneumoniae serotype 14.</title>
      <authorList>
        <person name="Ding F."/>
        <person name="Tang P."/>
        <person name="Hsu M.-H."/>
        <person name="Cui P."/>
        <person name="Hu S."/>
        <person name="Yu J."/>
        <person name="Chiu C.-H."/>
      </authorList>
      <dbReference type="PubMed" id="19361343"/>
      <dbReference type="DOI" id="10.1186/1471-2164-10-158"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CGSP14</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Promotes RNA polymerase assembly. Latches the N- and C-terminal regions of the beta' subunit thereby facilitating its interaction with the beta and alpha subunits.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>RNA(n) + a ribonucleoside 5'-triphosphate = RNA(n+1) + diphosphate</text>
      <dbReference type="Rhea" id="RHEA:21248"/>
      <dbReference type="Rhea" id="RHEA-COMP:14527"/>
      <dbReference type="Rhea" id="RHEA-COMP:17342"/>
      <dbReference type="ChEBI" id="CHEBI:33019"/>
      <dbReference type="ChEBI" id="CHEBI:61557"/>
      <dbReference type="ChEBI" id="CHEBI:140395"/>
      <dbReference type="EC" id="2.7.7.6"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">The RNAP catalytic core consists of 2 alpha, 1 beta, 1 beta' and 1 omega subunit. When a sigma factor is associated with the core the holoenzyme is formed, which can initiate transcription.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the RNA polymerase subunit omega family.</text>
  </comment>
  <dbReference type="EC" id="2.7.7.6" evidence="1"/>
  <dbReference type="EMBL" id="CP001033">
    <property type="protein sequence ID" value="ACB90963.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_001817694.1">
    <property type="nucleotide sequence ID" value="NC_010582.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B2IS88"/>
  <dbReference type="SMR" id="B2IS88"/>
  <dbReference type="KEGG" id="spw:SPCG_1711"/>
  <dbReference type="HOGENOM" id="CLU_125406_0_0_9"/>
  <dbReference type="GO" id="GO:0000428">
    <property type="term" value="C:DNA-directed RNA polymerase complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003899">
    <property type="term" value="F:DNA-directed 5'-3' RNA polymerase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006351">
    <property type="term" value="P:DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.90.940.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00366">
    <property type="entry name" value="RNApol_bact_RpoZ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003716">
    <property type="entry name" value="DNA-dir_RNA_pol_omega"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006110">
    <property type="entry name" value="Pol_omega/Rpo6/RPB6"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036161">
    <property type="entry name" value="RPB6/omega-like_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00690">
    <property type="entry name" value="rpoZ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34476">
    <property type="entry name" value="DNA-DIRECTED RNA POLYMERASE SUBUNIT OMEGA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34476:SF1">
    <property type="entry name" value="DNA-DIRECTED RNA POLYMERASE SUBUNIT OMEGA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01192">
    <property type="entry name" value="RNA_pol_Rpb6"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01409">
    <property type="entry name" value="RNA_pol_Rpb6"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF63562">
    <property type="entry name" value="RPB6/omega subunit-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0240">DNA-directed RNA polymerase</keyword>
  <keyword id="KW-0548">Nucleotidyltransferase</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0808">Transferase</keyword>
  <feature type="chain" id="PRO_1000121281" description="DNA-directed RNA polymerase subunit omega">
    <location>
      <begin position="1"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="76"/>
      <end position="104"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00366"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <sequence length="104" mass="11940" checksum="831F0E374842B161" modified="2008-06-10" version="1">MMLKPSIDTLLDKVPSKYSLVILEAKRAHELEAGAPATQGFKSEKSTLRALEEIESGNVTIHLDPEGKREAVRRRIEEEKRRKEEEEKKIKEQIAKEKEDGEKI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>