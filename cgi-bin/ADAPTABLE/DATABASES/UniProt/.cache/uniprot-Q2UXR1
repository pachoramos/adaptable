<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-04-12" modified="2024-03-27" version="63" xmlns="http://uniprot.org/uniprot">
  <accession>Q2UXR1</accession>
  <name>LDN4_ODOLI</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Lividin-4</fullName>
    </recommendedName>
  </protein>
  <organism evidence="7">
    <name type="scientific">Odorrana livida</name>
    <name type="common">Green mountain frog</name>
    <name type="synonym">Polypedates lividus</name>
    <dbReference type="NCBI Taxonomy" id="121160"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Odorrana</taxon>
    </lineage>
  </organism>
  <reference evidence="7" key="1">
    <citation type="journal article" date="2006" name="Peptides" volume="27" first="2118" last="2123">
      <title>Lividins: novel antimicrobial peptide homologs from the skin secretion of the Chinese Large Odorous frog, Rana (Odorrana) livida. Identification by 'shotgun' cDNA cloning and sequence analysis.</title>
      <authorList>
        <person name="Zhou M."/>
        <person name="Chen T."/>
        <person name="Walker B."/>
        <person name="Shaw C."/>
      </authorList>
      <dbReference type="PubMed" id="16713657"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2006.04.007"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 42-78</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="5">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Antimicrobial peptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3 4">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3877.32" method="MALDI" evidence="4"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AM039665">
    <property type="protein sequence ID" value="CAJ01673.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q2UXR1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439440" evidence="6">
    <location>
      <begin position="23"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000439441" description="Lividin-4" evidence="4">
    <location>
      <begin position="42"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="72"/>
      <end position="78"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0AEI6"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="A7WNV5"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="16713657"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="16713657"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="16713657"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="EMBL" id="CAJ01673.1"/>
    </source>
  </evidence>
  <sequence length="78" mass="8584" checksum="072000EA03A604A7" modified="2006-01-24" version="1" precursor="true">MFTMKKSLLVLFFLGTISLSLCVEERDADEEDNGEVEEVKRGVFTLIKGATQLIGKTLGKELGKTGLELMACKITNQC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>