<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-04-01" modified="2024-11-27" version="71" xmlns="http://uniprot.org/uniprot">
  <accession>P67977</accession>
  <accession>P07232</accession>
  <name>LYSC_SEMEN</name>
  <protein>
    <recommendedName>
      <fullName>Lysozyme C</fullName>
      <ecNumber>3.2.1.17</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>1,4-beta-N-acetylmuramidase C</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">LYZ</name>
    <name type="synonym">LZM</name>
  </gene>
  <organism>
    <name type="scientific">Semnopithecus entellus</name>
    <name type="common">Northern plains gray langur</name>
    <name type="synonym">Presbytis entellus</name>
    <dbReference type="NCBI Taxonomy" id="88029"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Colobinae</taxon>
      <taxon>Semnopithecus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="J. Mol. Evol." volume="33" first="418" last="425">
      <title>Stomach lysozyme gene of the langur monkey: tests for convergence and positive selection.</title>
      <authorList>
        <person name="Swanson K.W."/>
        <person name="Irwin D.M."/>
        <person name="Wilson A.C."/>
      </authorList>
      <dbReference type="PubMed" id="1960739"/>
      <dbReference type="DOI" id="10.1007/bf02103133"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <source>
      <tissue>Stomach</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1987" name="Nature" volume="330" first="401" last="404">
      <title>Adaptive evolution in the stomach lysozymes of foregut fermenters.</title>
      <authorList>
        <person name="Stewart C.-B."/>
        <person name="Schilling J.W."/>
        <person name="Wilson A.C."/>
      </authorList>
      <dbReference type="PubMed" id="3120013"/>
      <dbReference type="DOI" id="10.1038/330401a0"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 19-148</scope>
  </reference>
  <comment type="function">
    <text>Lysozymes have primarily a bacteriolytic function; those in tissues and body fluids are associated with the monocyte-macrophage system and enhance the activity of immunoagents. Also plays a role in digestion in this species.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>Hydrolysis of (1-&gt;4)-beta-linkages between N-acetylmuramic acid and N-acetyl-D-glucosamine residues in a peptidoglycan and between N-acetyl-D-glucosamine residues in chitodextrins.</text>
      <dbReference type="EC" id="3.2.1.17"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text>Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text>Lysozyme C is capable of both hydrolysis and transglycosylation; it shows also a slight esterase activity. It acts rapidly on both peptide-substituted and unsubstituted peptidoglycan, and slowly on chitin oligosaccharides.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the glycosyl hydrolase 22 family.</text>
  </comment>
  <dbReference type="EC" id="3.2.1.17"/>
  <dbReference type="EMBL" id="X60235">
    <property type="protein sequence ID" value="CAA42795.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X60234">
    <property type="protein sequence ID" value="CAA42794.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="I61852">
    <property type="entry name" value="I61852"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P67977"/>
  <dbReference type="SMR" id="P67977"/>
  <dbReference type="CAZy" id="GH22">
    <property type="family name" value="Glycoside Hydrolase Family 22"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003796">
    <property type="term" value="F:lysozyme activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007586">
    <property type="term" value="P:digestion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd16897">
    <property type="entry name" value="LYZ_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.530.10:FF:000001">
    <property type="entry name" value="Lysozyme C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.530.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001916">
    <property type="entry name" value="Glyco_hydro_22"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019799">
    <property type="entry name" value="Glyco_hydro_22_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000974">
    <property type="entry name" value="Glyco_hydro_22_lys"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023346">
    <property type="entry name" value="Lysozyme-like_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407">
    <property type="entry name" value="LYSOZYME C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407:SF28">
    <property type="entry name" value="LYSOZYME C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00062">
    <property type="entry name" value="Lys"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00137">
    <property type="entry name" value="LYSOZYME"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00135">
    <property type="entry name" value="LYZLACT"/>
  </dbReference>
  <dbReference type="SMART" id="SM00263">
    <property type="entry name" value="LYZ1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF53955">
    <property type="entry name" value="Lysozyme-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00128">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51348">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0081">Bacteriolytic enzyme</keyword>
  <keyword id="KW-0222">Digestion</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0326">Glycosidase</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000018482" description="Lysozyme C">
    <location>
      <begin position="19"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="domain" description="C-type lysozyme" evidence="1">
    <location>
      <begin position="19"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="active site" evidence="1">
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="active site" evidence="1">
    <location>
      <position position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="146"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="48"/>
      <end position="134"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="83"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="95"/>
      <end position="113"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00680"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="3120013"/>
    </source>
  </evidence>
  <sequence length="148" mass="16219" checksum="57C9E1BA4986AA89" modified="2004-10-11" version="1" precursor="true">MKALTILGLVLLSVTVQGKIFERCELARTLKKLGLDGYKGVSLANWVCLAKWESGYNTEATNYNPGDESTDYGIFQINSRYWCNNGKTPGAVDACHISCSALLQNNIADAVACAKRVVSDPQGIRAWVAWRNHCQNKDVSQYVKGCGV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>