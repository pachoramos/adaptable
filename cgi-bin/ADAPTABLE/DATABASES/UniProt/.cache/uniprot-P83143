<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-12-06" modified="2022-05-25" version="32" xmlns="http://uniprot.org/uniprot">
  <accession>P83143</accession>
  <name>AFP2L_MALPA</name>
  <protein>
    <recommendedName>
      <fullName>Antifungal protein 2 large subunit</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>CW-2</fullName>
    </alternativeName>
  </protein>
  <organism evidence="4">
    <name type="scientific">Malva parviflora</name>
    <name type="common">Little mallow</name>
    <name type="synonym">Cheeseweed mallow</name>
    <dbReference type="NCBI Taxonomy" id="145753"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Malvales</taxon>
      <taxon>Malvaceae</taxon>
      <taxon>Malvoideae</taxon>
      <taxon>Malva</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2000" name="Biochem. Biophys. Res. Commun." volume="279" first="669" last="673">
      <title>Potent heterologous antifungal proteins from cheeseweed (Malva parviflora).</title>
      <authorList>
        <person name="Wang X."/>
        <person name="Bunkers G.J."/>
      </authorList>
      <dbReference type="PubMed" id="11118343"/>
      <dbReference type="DOI" id="10.1006/bbrc.2000.3997"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Possesses antifungal activity against P.infestans but not F.graminearum.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Heterodimer of a large and a small subunit.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">Antimicrobial activity is not affected by salt concentration.</text>
  </comment>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006805">
    <property type="term" value="P:xenobiotic metabolic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <feature type="chain" id="PRO_0000064480" description="Antifungal protein 2 large subunit">
    <location>
      <begin position="1"/>
      <end position="20" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="3">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11118343"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="11118343"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="20" mass="2606" checksum="6E766A5E342036DA" modified="2001-12-01" version="1" fragment="single">PEDPQRRYQEXQREXRXQQE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>