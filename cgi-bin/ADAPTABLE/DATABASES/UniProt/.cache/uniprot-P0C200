<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-10-31" modified="2023-05-03" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>P0C200</accession>
  <name>TACHC_TACTR</name>
  <protein>
    <recommendedName>
      <fullName>Tachystatin-C</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Tachypleus tridentatus</name>
    <name type="common">Japanese horseshoe crab</name>
    <dbReference type="NCBI Taxonomy" id="6853"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Merostomata</taxon>
      <taxon>Xiphosura</taxon>
      <taxon>Limulidae</taxon>
      <taxon>Tachypleus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="J. Biol. Chem." volume="274" first="26172" last="26178">
      <title>Horseshoe crab hemocyte-derived antimicrobial polypeptides, tachystatins, with sequence similarity to spider neurotoxins.</title>
      <authorList>
        <person name="Osaki T."/>
        <person name="Omotezako M."/>
        <person name="Nagayama R."/>
        <person name="Hirata M."/>
        <person name="Iwanaga S."/>
        <person name="Kasahara J."/>
        <person name="Hattori J."/>
        <person name="Ito I."/>
        <person name="Sugiyama H."/>
        <person name="Kawabata S."/>
      </authorList>
      <dbReference type="PubMed" id="10473569"/>
      <dbReference type="DOI" id="10.1074/jbc.274.37.26172"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text>Binds to chitin. Shows strong activity against E.coli (IC(50) is 1.2 ug/ml). Is also very active against S.aureus (IC(50) is 0.8 ug/ml), C.albicans (IC(50) is 0.9 ug/ml) and P.pastoris (IC(50) is 0.3 ug/ml). Binds to chitin (5.2 uM are required to obtain 50% of binding). Causes hemolysis on sheep erythrocytes, probably by forming ion-permeable pores.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Granular hemocytes, small secretory granules.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0C200"/>
  <dbReference type="SMR" id="P0C200"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000256693" description="Tachystatin-C">
    <location>
      <begin position="1"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="12"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="19"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="27"/>
      <end position="38"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10473569"/>
    </source>
  </evidence>
  <sequence length="41" mass="4922" checksum="A1E5C4082D64713D" modified="2006-10-31" version="1">DYDWSLRGPPKCATYGQKCRTWSPPNCCWNLRCKAFRCRPR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>