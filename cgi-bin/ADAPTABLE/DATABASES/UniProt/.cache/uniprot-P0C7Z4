<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-07-22" modified="2023-09-13" version="26" xmlns="http://uniprot.org/uniprot">
  <accession>P0C7Z4</accession>
  <name>PSMA2_STAA9</name>
  <protein>
    <recommendedName>
      <fullName>Phenol-soluble modulin alpha 2 peptide</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">psmA2</name>
    <name type="ordered locus">SaurJH9_0473.3</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain JH9)</name>
    <dbReference type="NCBI Taxonomy" id="359786"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2007-05" db="EMBL/GenBank/DDBJ databases">
      <title>Complete sequence of chromosome of Staphylococcus aureus subsp. aureus JH9.</title>
      <authorList>
        <consortium name="US DOE Joint Genome Institute"/>
        <person name="Copeland A."/>
        <person name="Lucas S."/>
        <person name="Lapidus A."/>
        <person name="Barry K."/>
        <person name="Detter J.C."/>
        <person name="Glavina del Rio T."/>
        <person name="Hammon N."/>
        <person name="Israni S."/>
        <person name="Pitluck S."/>
        <person name="Chain P."/>
        <person name="Malfatti S."/>
        <person name="Shin M."/>
        <person name="Vergez L."/>
        <person name="Schmutz J."/>
        <person name="Larimer F."/>
        <person name="Land M."/>
        <person name="Hauser L."/>
        <person name="Kyrpides N."/>
        <person name="Kim E."/>
        <person name="Tomasz A."/>
        <person name="Richardson P."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>JH9</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Peptide which can recruit, activate and subsequently lyse human neutrophils, thus eliminating the main cellular defense against infection.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the phenol-soluble modulin alpha peptides family.</text>
  </comment>
  <dbReference type="EMBL" id="CP000703">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0C7Z4"/>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031429">
    <property type="entry name" value="PSM_alpha"/>
  </dbReference>
  <dbReference type="NCBIfam" id="NF033425">
    <property type="entry name" value="PSM_alpha_1_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17063">
    <property type="entry name" value="PSMalpha"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="peptide" id="PRO_0000345047" description="Phenol-soluble modulin alpha 2 peptide">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A9JX06"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="21" mass="2278" checksum="E3D5498D1727572D" modified="2008-07-22" version="1">MGIIAGIIKFIKGLIEKFTGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>