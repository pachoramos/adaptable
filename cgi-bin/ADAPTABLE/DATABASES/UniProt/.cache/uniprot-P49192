<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-02-01" modified="2024-11-27" version="143" xmlns="http://uniprot.org/uniprot">
  <accession>P49192</accession>
  <name>CART_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Cocaine- and amphetamine-regulated transcript protein</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>CART(1-52)</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CART(55-102)</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>CART(62-102)</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">Cartpt</name>
    <name type="synonym">Cart</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="J. Neurosci." volume="15" first="2471" last="2481">
      <title>PCR differential display identifies a rat brain mRNA that is transcriptionally regulated by cocaine and amphetamine.</title>
      <authorList>
        <person name="Douglass J.O."/>
        <person name="McKinzie A.A."/>
        <person name="Couceyro P."/>
      </authorList>
      <dbReference type="PubMed" id="7891182"/>
      <dbReference type="DOI" id="10.1523/jneurosci.15-03-02471.1995"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORMS LONG AND SHORT)</scope>
    <source>
      <strain>Sprague-Dawley</strain>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="FEBS Lett." volume="428" first="263" last="268">
      <title>Purification and characterisation of a new hypothalamic satiety peptide, cocaine and amphetamine regulated transcript (CART), produced in yeast.</title>
      <authorList>
        <person name="Thim L."/>
        <person name="Nielsen P.F."/>
        <person name="Judge M.E."/>
        <person name="Andersen A.S."/>
        <person name="Diers I."/>
        <person name="Egel-Mitani M."/>
        <person name="Hastrup S."/>
      </authorList>
      <dbReference type="PubMed" id="9654146"/>
      <dbReference type="DOI" id="10.1016/s0014-5793(98)00543-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 81-97</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2012" name="Nat. Commun." volume="3" first="876" last="876">
      <title>Quantitative maps of protein phosphorylation sites across 14 different rat organs and tissues.</title>
      <authorList>
        <person name="Lundby A."/>
        <person name="Secher A."/>
        <person name="Lage K."/>
        <person name="Nordsborg N.B."/>
        <person name="Dmytriyev A."/>
        <person name="Lundby C."/>
        <person name="Olsen J.V."/>
      </authorList>
      <dbReference type="PubMed" id="22673903"/>
      <dbReference type="DOI" id="10.1038/ncomms1871"/>
    </citation>
    <scope>PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT TYR-41 AND SER-48</scope>
    <scope>PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-48 (ISOFORM SHORT)</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Satiety factor closely associated with the actions of leptin and neuropeptide y; this anorectic peptide inhibits both normal and starvation-induced feeding and completely blocks the feeding response induced by neuropeptide Y and regulated by leptin in the hypothalamus.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>P49192-1</id>
      <name>Long</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>P49192-2</id>
      <name>Short</name>
      <sequence type="described" ref="VSP_000794"/>
    </isoform>
  </comment>
  <comment type="tissue specificity">
    <text>Neuroendocrine tissues. Predominantly expressed in the hypothalamus, pituitary, and longitudinal muscle-myenteric plexus. Abundant expression is also seen in the midbrain/thalamus and eye. A lower level expression is seen in the other brain regions and adrenal.</text>
  </comment>
  <comment type="induction">
    <text>By cocaine and amphetamine.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the CART family.</text>
  </comment>
  <dbReference type="EMBL" id="U10071">
    <property type="protein sequence ID" value="AAA87897.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_058806.1">
    <molecule id="P49192-1"/>
    <property type="nucleotide sequence ID" value="NM_017110.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_006231905.1">
    <molecule id="P49192-2"/>
    <property type="nucleotide sequence ID" value="XM_006231843.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P49192"/>
  <dbReference type="SMR" id="P49192"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000023869"/>
  <dbReference type="iPTMnet" id="P49192"/>
  <dbReference type="PhosphoSitePlus" id="P49192"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000023869"/>
  <dbReference type="Ensembl" id="ENSRNOT00000023869.3">
    <molecule id="P49192-2"/>
    <property type="protein sequence ID" value="ENSRNOP00000023869.2"/>
    <property type="gene ID" value="ENSRNOG00000017712.3"/>
  </dbReference>
  <dbReference type="GeneID" id="29131"/>
  <dbReference type="KEGG" id="rno:29131"/>
  <dbReference type="UCSC" id="RGD:2272">
    <molecule id="P49192-1"/>
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:2272"/>
  <dbReference type="CTD" id="9607"/>
  <dbReference type="RGD" id="2272">
    <property type="gene designation" value="Cartpt"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSRNOG00000017712"/>
  <dbReference type="eggNOG" id="ENOG502S2YU">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000018319"/>
  <dbReference type="HOGENOM" id="CLU_157363_1_0_1"/>
  <dbReference type="InParanoid" id="P49192"/>
  <dbReference type="OMA" id="CNCPRGA"/>
  <dbReference type="OrthoDB" id="3971848at2759"/>
  <dbReference type="PhylomeDB" id="P49192"/>
  <dbReference type="TreeFam" id="TF332948"/>
  <dbReference type="PRO" id="PR:P49192"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000017712">
    <property type="expression patterns" value="Expressed in duodenum and 10 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030141">
    <property type="term" value="C:secretory granule"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045202">
    <property type="term" value="C:synapse"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="GOC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008343">
    <property type="term" value="P:adult feeding behavior"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009267">
    <property type="term" value="P:cellular response to starvation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007268">
    <property type="term" value="P:chemical synaptic transmission"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032922">
    <property type="term" value="P:circadian regulation of gene expression"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007186">
    <property type="term" value="P:G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001696">
    <property type="term" value="P:gastric acid secretion"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001678">
    <property type="term" value="P:intracellular glucose homeostasis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032099">
    <property type="term" value="P:negative regulation of appetite"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045779">
    <property type="term" value="P:negative regulation of bone resorption"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070093">
    <property type="term" value="P:negative regulation of glucagon secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045777">
    <property type="term" value="P:positive regulation of blood pressure"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032812">
    <property type="term" value="P:positive regulation of epinephrine secretion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043410">
    <property type="term" value="P:positive regulation of MAPK cascade"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045860">
    <property type="term" value="P:positive regulation of protein kinase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051971">
    <property type="term" value="P:positive regulation of transmission of nerve impulse"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="HGNC-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046850">
    <property type="term" value="P:regulation of bone remodeling"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050796">
    <property type="term" value="P:regulation of insulin secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070253">
    <property type="term" value="P:somatostatin secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="CDD" id="cd22741">
    <property type="entry name" value="CART_CTD-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.40.30:FF:000001">
    <property type="entry name" value="Cocaine-and amphetamine-regulated transcript protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.40.30">
    <property type="entry name" value="CART, C-terminal domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009106">
    <property type="entry name" value="CART"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036722">
    <property type="entry name" value="CART_C_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16655">
    <property type="entry name" value="COCAINE AND AMPHETAMINE REGULATED TRANSCRIPT PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR16655:SF0">
    <property type="entry name" value="COCAINE- AND AMPHETAMINE-REGULATED TRANSCRIPT PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06373">
    <property type="entry name" value="CART"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF64546">
    <property type="entry name" value="Satiety factor CART (cocaine and amphetamine regulated transcript)"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0529">Neurotransmitter</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004442" description="Cocaine- and amphetamine-regulated transcript protein">
    <location>
      <begin position="28"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000004443" description="CART(1-52)">
    <location>
      <begin position="28"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000004444" description="CART(55-102)">
    <location>
      <begin position="82"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000004445" description="CART(62-102)" evidence="2">
    <location>
      <begin position="89"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphotyrosine" evidence="6">
    <location>
      <position position="41"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="6">
    <location>
      <position position="48"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="95"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="101"/>
      <end position="121"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="115"/>
      <end position="128"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_000794" description="In isoform Short." evidence="4">
    <location>
      <begin position="54"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="6">
    <location sequence="P49192-2">
      <position position="48"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="9654146"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="7891182"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007744" key="6">
    <source>
      <dbReference type="PubMed" id="22673903"/>
    </source>
  </evidence>
  <sequence length="129" mass="14140" checksum="0FDE28B705BB2728" modified="1996-02-01" version="1" precursor="true">MESSRLRLLPVLGAALLLLLPLLGAGAQEDAELQPRALDIYSAVDDASHEKELPRRQLRAPGAVLQIEALQEVLKKLKSKRIPIYEKKYGQVPMCDAGEQCAVRKGARIGKLCDCPRGTSCNSFLLKCL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>