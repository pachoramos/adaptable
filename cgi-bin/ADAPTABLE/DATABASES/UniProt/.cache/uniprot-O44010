<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2000-05-30" modified="2024-11-27" version="99" xmlns="http://uniprot.org/uniprot">
  <accession>O44010</accession>
  <accession>A4HIV9</accession>
  <name>RLA2_LEIBR</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Large ribosomal subunit protein P2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>60S acidic ribosomal protein P2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Acidic ribosomal P2 beta protein</fullName>
      <shortName>P2B-protein</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">LIP2</name>
    <name type="ORF">LbrM30_V2.3760</name>
    <name type="ORF">LbrM_30_3760</name>
  </gene>
  <organism>
    <name type="scientific">Leishmania braziliensis</name>
    <dbReference type="NCBI Taxonomy" id="5660"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Discoba</taxon>
      <taxon>Euglenozoa</taxon>
      <taxon>Kinetoplastea</taxon>
      <taxon>Metakinetoplastina</taxon>
      <taxon>Trypanosomatida</taxon>
      <taxon>Trypanosomatidae</taxon>
      <taxon>Leishmaniinae</taxon>
      <taxon>Leishmania</taxon>
      <taxon>Leishmania braziliensis species complex</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1998-01" db="EMBL/GenBank/DDBJ databases">
      <title>Characterization of the ribosomal P2 beta protein from Leishmania (V.) braziliensis.</title>
      <authorList>
        <person name="Padilla C."/>
        <person name="Carrillo C."/>
        <person name="Montoya Y."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>MHOM/PE/85/LH166</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2007" name="Nat. Genet." volume="39" first="839" last="847">
      <title>Comparative genomic analysis of three Leishmania species that cause diverse human disease.</title>
      <authorList>
        <person name="Peacock C.S."/>
        <person name="Seeger K."/>
        <person name="Harris D."/>
        <person name="Murphy L."/>
        <person name="Ruiz J.C."/>
        <person name="Quail M.A."/>
        <person name="Peters N."/>
        <person name="Adlem E."/>
        <person name="Tivey A."/>
        <person name="Aslett M."/>
        <person name="Kerhornou A."/>
        <person name="Ivens A."/>
        <person name="Fraser A."/>
        <person name="Rajandream M.-A."/>
        <person name="Carver T."/>
        <person name="Norbertczak H."/>
        <person name="Chillingworth T."/>
        <person name="Hance Z."/>
        <person name="Jagels K."/>
        <person name="Moule S."/>
        <person name="Ormond D."/>
        <person name="Rutter S."/>
        <person name="Sqaures R."/>
        <person name="Whitehead S."/>
        <person name="Rabbinowitsch E."/>
        <person name="Arrowsmith C."/>
        <person name="White B."/>
        <person name="Thurston S."/>
        <person name="Bringaud F."/>
        <person name="Baldauf S.L."/>
        <person name="Faulconbridge A."/>
        <person name="Jeffares D."/>
        <person name="Depledge D.P."/>
        <person name="Oyola S.O."/>
        <person name="Hilley J.D."/>
        <person name="Brito L.O."/>
        <person name="Tosi L.R.O."/>
        <person name="Barrell B."/>
        <person name="Cruz A.K."/>
        <person name="Mottram J.C."/>
        <person name="Smith D.F."/>
        <person name="Berriman M."/>
      </authorList>
      <dbReference type="PubMed" id="17572675"/>
      <dbReference type="DOI" id="10.1038/ng2053"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MHOM/BR/75/M2904</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="FEBS Lett." volume="560" first="134" last="140">
      <title>Correlation between conformation and antibody binding: NMR structure of cross-reactive peptides from T. cruzi, human and L. braziliensis.</title>
      <authorList>
        <person name="Soares M.R."/>
        <person name="Bisch P.M."/>
        <person name="Campos de Carvalho A.C."/>
        <person name="Valente A.P."/>
        <person name="Almeida F.C."/>
      </authorList>
      <dbReference type="PubMed" id="14988012"/>
      <dbReference type="DOI" id="10.1016/s0014-5793(04)00088-2"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 93-105</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays an important role in the elongation step of protein synthesis.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">P1 and P2 exist as dimers at the large ribosomal subunit.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Phosphorylated.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the eukaryotic ribosomal protein P1/P2 family.</text>
  </comment>
  <dbReference type="EMBL" id="AF045020">
    <property type="protein sequence ID" value="AAC02540.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="FR799005">
    <property type="protein sequence ID" value="CAM40525.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_001566999.1">
    <property type="nucleotide sequence ID" value="XM_001566949.1"/>
  </dbReference>
  <dbReference type="PDB" id="1S4H">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=93-105"/>
  </dbReference>
  <dbReference type="PDBsum" id="1S4H"/>
  <dbReference type="AlphaFoldDB" id="O44010"/>
  <dbReference type="SMR" id="O44010"/>
  <dbReference type="STRING" id="5660.O44010"/>
  <dbReference type="GeneID" id="5417901"/>
  <dbReference type="KEGG" id="lbz:LBRM_30_3760"/>
  <dbReference type="VEuPathDB" id="TriTrypDB:LbrM.30.3760"/>
  <dbReference type="InParanoid" id="O44010"/>
  <dbReference type="OMA" id="MKVIASY"/>
  <dbReference type="EvolutionaryTrace" id="O44010"/>
  <dbReference type="Proteomes" id="UP000007258">
    <property type="component" value="Chromosome 30"/>
  </dbReference>
  <dbReference type="GO" id="GO:0022625">
    <property type="term" value="C:cytosolic large ribosomal subunit"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002182">
    <property type="term" value="P:cytoplasmic translational elongation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd05833">
    <property type="entry name" value="Ribosomal_P2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.10.1410:FF:000002">
    <property type="entry name" value="60S acidic ribosomal protein P2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.10.1410">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01478">
    <property type="entry name" value="Ribosomal_L12_arch"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038716">
    <property type="entry name" value="P1/P2_N_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027534">
    <property type="entry name" value="Ribosomal_P1/P2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001859">
    <property type="entry name" value="Ribosomal_P1/P2_euk"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044076">
    <property type="entry name" value="Ribosomal_P2"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21141">
    <property type="entry name" value="60S ACIDIC RIBOSOMAL PROTEIN FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR21141:SF58">
    <property type="entry name" value="ACIDIC RIBOSOMAL PROTEIN P2, PUTATIVE-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00428">
    <property type="entry name" value="Ribosomal_60s"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00456">
    <property type="entry name" value="RIBOSOMALP2"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <feature type="chain" id="PRO_0000157655" description="Large ribosomal subunit protein P2">
    <location>
      <begin position="1"/>
      <end position="105"/>
    </location>
  </feature>
  <feature type="turn" evidence="3">
    <location>
      <begin position="102"/>
      <end position="104"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="1S4H"/>
    </source>
  </evidence>
  <sequence length="105" mass="10601" checksum="16A72873D5AB3602" modified="1998-06-01" version="1">MQYLAAYALVALSGKTPCKADVQAVLKAAGVAIELSRVDALFQELEGKSFDELMTEGRSKLVGSGSAAPAAAASTAGAAVAAAADAKKEASEEEADDDMGFGLFD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>