<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-11-25" modified="2020-06-17" version="43" xmlns="http://uniprot.org/uniprot">
  <accession>P83307</accession>
  <name>TP1A_RANJA</name>
  <protein>
    <recommendedName>
      <fullName>Temporin-1Ja</fullName>
    </recommendedName>
  </protein>
  <organism evidence="2">
    <name type="scientific">Rana japonica</name>
    <name type="common">Japanese reddish frog</name>
    <dbReference type="NCBI Taxonomy" id="8402"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Rana</taxon>
      <taxon>Rana</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2002" name="Peptides" volume="23" first="419" last="425">
      <title>Antimicrobial peptides with atypical structural features from the skin of the Japanese brown frog Rana japonica.</title>
      <authorList>
        <person name="Isaacson T."/>
        <person name="Soto A."/>
        <person name="Iwamuro S."/>
        <person name="Knoop F.C."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="11835990"/>
      <dbReference type="DOI" id="10.1016/s0196-9781(01)00634-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT LEU-13</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Antibacterial activity against the Gram-negative bacterium E.coli and the Gram-positive bacterium S.aureus.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1405.0" method="Electrospray" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Temporin subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016999">
    <property type="term" value="P:antibiotic metabolic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043586" description="Temporin-1Ja">
    <location>
      <begin position="1"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="1">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11835990"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="13" mass="1407" checksum="3EF713EA610A2448" modified="2002-06-01" version="1">ILPLVGNLLNDLL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>