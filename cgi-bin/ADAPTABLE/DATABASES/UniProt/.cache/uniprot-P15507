<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-04-01" modified="2023-06-28" version="47" xmlns="http://uniprot.org/uniprot">
  <accession>P15507</accession>
  <name>NPMB_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName>Morphine-modulating neuropeptide B</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1985" name="Proc. Natl. Acad. Sci. U.S.A." volume="82" first="7757" last="7761">
      <title>Isolation, sequencing, synthesis, and pharmacological characterization of two brain neuropeptides that modulate the action of morphine.</title>
      <authorList>
        <person name="Yang H.-Y.T."/>
        <person name="Fratta W."/>
        <person name="Majane E.A."/>
        <person name="Costa E."/>
      </authorList>
      <dbReference type="PubMed" id="3865193"/>
      <dbReference type="DOI" id="10.1073/pnas.82.22.7757"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-8</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Modulates the action of morphine.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="PIR" id="B24749">
    <property type="entry name" value="B24749"/>
  </dbReference>
  <dbReference type="InParanoid" id="P15507"/>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044178" description="Morphine-modulating neuropeptide B">
    <location>
      <begin position="1"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="8"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="3865193"/>
    </source>
  </evidence>
  <sequence length="8" mass="1082" checksum="87D416C776D9C729" modified="1990-04-01" version="1">FLFQPQRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>