<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-11-01" modified="2022-12-14" version="74" xmlns="http://uniprot.org/uniprot">
  <accession>P08945</accession>
  <name>LITO_RANAE</name>
  <protein>
    <recommendedName>
      <fullName>Litorin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ranoidea aurea</name>
    <name type="common">Green and golden bell frog</name>
    <name type="synonym">Litoria aurea</name>
    <dbReference type="NCBI Taxonomy" id="8371"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1975" name="Experientia" volume="31" first="510" last="511">
      <title>Aminoacid composition and sequence of litorin, a bombesin-like nonapeptide from the skin of the Australian leptodactylid frog Litoria aurea.</title>
      <authorList>
        <person name="Anastasi A."/>
        <person name="Erspamer V."/>
        <person name="Endean R."/>
      </authorList>
      <dbReference type="PubMed" id="1140241"/>
      <dbReference type="DOI" id="10.1007/bf01932427"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>AMIDATION AT MET-9</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1977" name="Experientia" volume="33" first="1289" last="1289">
      <title>Glu(OMe)3-litorin, the second bombesin-like peptide occurring in methanol extracts of the skin of the Australian frog Litoria aurea.</title>
      <authorList>
        <person name="Anastasi A."/>
        <person name="Montecucchi P.C."/>
        <person name="Angelucci F."/>
        <person name="Erspamer V."/>
        <person name="Endean R."/>
      </authorList>
      <dbReference type="PubMed" id="908397"/>
      <dbReference type="DOI" id="10.1007/bf01920138"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the bombesin/neuromedin-B/ranatensin family.</text>
  </comment>
  <comment type="caution">
    <text evidence="2">PubMed:908397 reported the formation of a glutamate methyl ester from Gln-2. This is most probably an artifact of isolation.</text>
  </comment>
  <dbReference type="PIR" id="S07204">
    <property type="entry name" value="S07204"/>
  </dbReference>
  <dbReference type="PIR" id="S07205">
    <property type="entry name" value="S07205"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000874">
    <property type="entry name" value="Bombesin"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00257">
    <property type="entry name" value="BOMBESIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043493" description="Litorin">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="1">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="1140241"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="9" mass="1103" checksum="D7CCC1E862CDC366" modified="1994-02-01" version="2">QQWAVGHFM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>