<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-12-05" modified="2023-02-22" version="57" xmlns="http://uniprot.org/uniprot">
  <accession>P58438</accession>
  <name>CYO6_VIOOD</name>
  <protein>
    <recommendedName>
      <fullName>Cycloviolacin-O6</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Viola odorata</name>
    <name type="common">Sweet violet</name>
    <dbReference type="NCBI Taxonomy" id="97441"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Malpighiales</taxon>
      <taxon>Violaceae</taxon>
      <taxon>Viola</taxon>
      <taxon>Viola subgen. Viola</taxon>
      <taxon>Viola sect. Viola</taxon>
      <taxon>Viola subsect. Viola</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="J. Mol. Biol." volume="294" first="1327" last="1336">
      <title>Plant cyclotides: a unique family of cyclic and knotted proteins that defines the cyclic cystine knot structural motif.</title>
      <authorList>
        <person name="Craik D.J."/>
        <person name="Daly N.L."/>
        <person name="Bond T."/>
        <person name="Waine C."/>
      </authorList>
      <dbReference type="PubMed" id="10600388"/>
      <dbReference type="DOI" id="10.1006/jmbi.1999.3383"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2006" name="Biochem. J." volume="400" first="1" last="12">
      <title>A novel suite of cyclotides from Viola odorata: sequence variation and the implications for structure, function and stability.</title>
      <authorList>
        <person name="Ireland D.C."/>
        <person name="Colgrave M.L."/>
        <person name="Craik D.J."/>
      </authorList>
      <dbReference type="PubMed" id="16872274"/>
      <dbReference type="DOI" id="10.1042/bj20060627"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text>Probably participates in a plant defense mechanism.</text>
  </comment>
  <comment type="domain">
    <text>The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="PTM">
    <text>This is a cyclic peptide.</text>
  </comment>
  <comment type="mass spectrometry" mass="3181.5" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the cyclotide family. Bracelet subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="3">This peptide is cyclic. The start position was chosen by similarity to OAK1 (kalata-B1) for which the DNA sequence is known.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P58438"/>
  <dbReference type="SMR" id="P58438"/>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005535">
    <property type="entry name" value="Cyclotide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012323">
    <property type="entry name" value="Cyclotide_bracelet_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036146">
    <property type="entry name" value="Cyclotide_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03784">
    <property type="entry name" value="Cyclotide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF037891">
    <property type="entry name" value="Cycloviolacin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57038">
    <property type="entry name" value="Cyclotides"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51052">
    <property type="entry name" value="CYCLOTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60008">
    <property type="entry name" value="CYCLOTIDE_BRACELET"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <feature type="peptide" id="PRO_0000044703" description="Cycloviolacin-O6">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="5"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="9"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="14"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Gly-Asn)">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="3" ref="2">
    <original>C</original>
    <variation>V</variation>
    <location>
      <position position="14"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00395"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="16872274"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="31" mass="3208" checksum="30D6AA52F72C6E68" modified="2001-12-05" version="1">GTLPCGESCVWIPCISAAVGCSCKSKVCYKN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>