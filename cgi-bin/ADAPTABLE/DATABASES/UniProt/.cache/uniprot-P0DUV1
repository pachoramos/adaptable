<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2021-09-29" modified="2024-05-29" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>P0DUV1</accession>
  <name>FLP12_SPHAA</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">FMRFamide-like peptide Sa12b</fullName>
      <shortName evidence="4">Sa-12</shortName>
      <shortName evidence="6">Sa12b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Sa-112</fullName>
      <shortName evidence="6">Sa112</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Sphex argentatus argentatus</name>
    <name type="common">Black digger wasp</name>
    <dbReference type="NCBI Taxonomy" id="2838366"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Apoidea</taxon>
      <taxon>Sphecidae</taxon>
      <taxon>Sphecinae</taxon>
      <taxon>Sphecina</taxon>
      <taxon>Sphex</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2019" name="Toxins" volume="11">
      <title>Sa12b peptide from solitary wasp inhibits ASIC currents in rat dorsal root ganglion neurons.</title>
      <authorList>
        <person name="Hernandez C."/>
        <person name="Konno K."/>
        <person name="Salceda E."/>
        <person name="Vega R."/>
        <person name="Zaharenko A.J."/>
        <person name="Soto E."/>
      </authorList>
      <dbReference type="PubMed" id="31658776"/>
      <dbReference type="DOI" id="10.3390/toxins11100585"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="144" last="144">
      <title>96. Chemical and biological characterization of a novel neuropeptide in the venom of solitary digger wasp.</title>
      <authorList>
        <person name="Nihei K.-I."/>
        <person name="Kazuma K."/>
        <person name="Ando K."/>
        <person name="Konno K."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
    <scope>PARTIAL AMIDATION AT PHE-10</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2021" name="Peptides" volume="142" first="170575" last="170575">
      <title>Isolation and characterization of FMRFamide-like peptides in the venoms of solitary sphecid wasps.</title>
      <authorList>
        <person name="Nihei K.I."/>
        <person name="Peigneur S."/>
        <person name="Tytgat J."/>
        <person name="Lange A.B."/>
        <person name="Konno K."/>
      </authorList>
      <dbReference type="PubMed" id="34023397"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2021.170575"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>PARTIAL AMIDATION AT PHE-10</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SYNTHESIS</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2016" name="Toxins" volume="8" first="114" last="114">
      <title>Peptide toxins in solitary wasp venoms.</title>
      <authorList>
        <person name="Konno K."/>
        <person name="Kazuma K."/>
        <person name="Nihei K."/>
      </authorList>
      <dbReference type="PubMed" id="27096870"/>
      <dbReference type="DOI" id="10.3390/toxins8040114"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3 10">May be directly involved in a paralyzing effect, by inhibiting muscle contraction, or may also act centrally to modulate prey behaviors (Probable). The non-amidated form (Sa12b) potently inhibits ASIC current in rat DRG neurons (IC(50)=81 nM) when preincubated before activation by acidic pH (PubMed:31658776). Has no consistent action on the time course of desensitization or the sustained component of the current (PubMed:31658776). Has no activity when coapplied with acidic pH, suggesting that the peptide needs to interact with the channel during its closed state (PubMed:31658776). This effect is concentration-dependent and reversed after washout of the peptide (PubMed:31658776). Since the inhibition is almost complete at 1 uM and all ASIC subunits are expressed in dorsal root ganglion (DRG) neurons, it suggests that it inhibits different ASIC subunits without an apparent selectivity (PubMed:31658776). It is noteworthy that it does not show activity on the locust oviduct contraction (tested at 50 nM), in contrast to the amidated form (PubMed:34023397, Ref.2).</text>
  </comment>
  <comment type="function">
    <text evidence="2 3">The amidated form (Sa112) inhibits both the frequency and amplitude of spontaneous contractions of the locust oviducts (tested at 50 nM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1 2 3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="9 10 11">Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="8 10">Is either amidated (Sa112) or non-amidated (Sa12b).</text>
  </comment>
  <comment type="mass spectrometry" mass="1274.7" method="MALDI" evidence="2">
    <text>amidated form (Sa112), Monoisotopic mass.</text>
  </comment>
  <comment type="mass spectrometry" mass="1275.6" method="MALDI" evidence="2">
    <text>non-amidated form (Sa12b), Monoisotopic mass.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the FARP (FMRFamide related peptide) family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0099106">
    <property type="term" value="F:ion channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-1275">Proton-gated sodium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000453656" description="FMRFamide-like peptide Sa12b" evidence="1 3">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide; partial" evidence="1 3 8">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="31658776"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="34023397"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="27096870"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="31658776"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="34023397"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="27096870"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="9">
    <source>
      <dbReference type="PubMed" id="31658776"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10">
    <source>
      <dbReference type="PubMed" id="34023397"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="11">
    <source ref="2"/>
  </evidence>
  <sequence length="10" mass="1276" checksum="D3D51629D2C1EAB2" modified="2021-09-29" version="1">EDVDHVFLRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>