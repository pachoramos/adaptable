<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-09-26" modified="2024-03-27" version="91" xmlns="http://uniprot.org/uniprot">
  <accession>Q43413</accession>
  <name>DEF1_CAPAN</name>
  <protein>
    <recommendedName>
      <fullName>Defensin J1-1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Capsicum annuum</name>
    <name type="common">Capsicum pepper</name>
    <dbReference type="NCBI Taxonomy" id="4072"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>asterids</taxon>
      <taxon>lamiids</taxon>
      <taxon>Solanales</taxon>
      <taxon>Solanaceae</taxon>
      <taxon>Solanoideae</taxon>
      <taxon>Capsiceae</taxon>
      <taxon>Capsicum</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Plant Physiol." volume="112" first="615" last="622">
      <title>Fruit-specific expression of a defensin-type gene family in bell pepper. Upregulation during ripening and upon wounding.</title>
      <authorList>
        <person name="Meyer B."/>
        <person name="Houlne G."/>
        <person name="Pozueta-Romero J."/>
        <person name="Schantz M.L."/>
        <person name="Schantz R."/>
      </authorList>
      <dbReference type="PubMed" id="8883377"/>
      <dbReference type="DOI" id="10.1104/pp.112.2.615"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>PROTEIN SEQUENCE OF 28-69</scope>
    <scope>CHARACTERIZATION</scope>
    <source>
      <strain>cv. Yolo Wonder</strain>
      <tissue>Fruit</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Plant defense peptide with antifungal activity against F.oxysporum and B.cinerea.</text>
  </comment>
  <comment type="subunit">
    <text>Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed in orange and red ripe fruit and to a lesser extent in mature, green fruit. Present in trace in young, green fruit.</text>
  </comment>
  <comment type="developmental stage">
    <text>Transcripts remains very low during fruit development and dramatically increases during ripening.</text>
  </comment>
  <comment type="induction">
    <text>By wounding.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="EMBL" id="X95363">
    <property type="protein sequence ID" value="CAA64653.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_016537483.1">
    <property type="nucleotide sequence ID" value="XM_016681997.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q43413"/>
  <dbReference type="SMR" id="Q43413"/>
  <dbReference type="EnsemblPlants" id="PHT73905">
    <property type="protein sequence ID" value="PHT73905"/>
    <property type="gene ID" value="T459_21182"/>
  </dbReference>
  <dbReference type="Gramene" id="PHT73905">
    <property type="protein sequence ID" value="PHT73905"/>
    <property type="gene ID" value="T459_21182"/>
  </dbReference>
  <dbReference type="OMA" id="CHVEGFT"/>
  <dbReference type="OrthoDB" id="385272at2759"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008176">
    <property type="entry name" value="Defensin_plant"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33147">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33147:SF26">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 7-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00304">
    <property type="entry name" value="Gamma-thionin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00288">
    <property type="entry name" value="PUROTHIONIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00940">
    <property type="entry name" value="GAMMA_THIONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000007036" description="Defensin J1-1">
    <location>
      <begin position="28"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="30"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="41"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="47"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="51"/>
      <end position="70"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="8883377"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="75" mass="8118" checksum="6E2A8ECE7614A51B" modified="1996-11-01" version="1" precursor="true">MAGFSKVVATIFLMMLLVFATDMMAEAKICEALSGNFKGLCLSSRDCGNVCRREGFTDGSCIGFRLQCFCTKPCA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>