<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-02-10" modified="2022-02-23" version="20" xmlns="http://uniprot.org/uniprot">
  <accession>P86078</accession>
  <name>CHI1_CAPAA</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Endochitinase 1</fullName>
      <ecNumber>3.2.1.14</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Capsicum annuum var. annuum</name>
    <name type="common">Red pepper</name>
    <dbReference type="NCBI Taxonomy" id="40321"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>asterids</taxon>
      <taxon>lamiids</taxon>
      <taxon>Solanales</taxon>
      <taxon>Solanaceae</taxon>
      <taxon>Solanoideae</taxon>
      <taxon>Capsiceae</taxon>
      <taxon>Capsicum</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="submission" date="2008-07" db="UniProtKB">
      <authorList>
        <person name="Belchi-Navarro S."/>
        <person name="Almagro L."/>
        <person name="Pedreno M.A."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <comment type="function">
    <text evidence="4">Defense against chitin-containing fungal pathogens.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>Random endo-hydrolysis of N-acetyl-beta-D-glucosaminide (1-&gt;4)-beta-linkages in chitin and chitodextrins.</text>
      <dbReference type="EC" id="3.2.1.14"/>
    </reaction>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
      <location evidence="2">Cell wall</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the glycosyl hydrolase 19 family. Chitinase class I subfamily.</text>
  </comment>
  <dbReference type="EC" id="3.2.1.14"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004568">
    <property type="term" value="F:chitinase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006032">
    <property type="term" value="P:chitin catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000272">
    <property type="term" value="P:polysaccharide catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0119">Carbohydrate metabolism</keyword>
  <keyword id="KW-0134">Cell wall</keyword>
  <keyword id="KW-0146">Chitin degradation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0326">Glycosidase</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0624">Polysaccharide degradation</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000362987" description="Endochitinase 1">
    <location>
      <begin position="1" status="less than"/>
      <end position="15" status="greater than"/>
    </location>
  </feature>
  <feature type="unsure residue" description="I or L">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <feature type="unsure residue" description="F or M">
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I">
    <location>
      <position position="7"/>
    </location>
  </feature>
  <feature type="unsure residue" description="I or L">
    <location>
      <position position="12"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P80052"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P85484"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="15" mass="1589" checksum="1C260DC2D376017B" modified="2009-02-10" version="1" fragment="single">SIGFDGLNDPDIVAR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>