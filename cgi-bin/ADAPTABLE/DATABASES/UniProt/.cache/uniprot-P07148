<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-04-01" modified="2024-11-27" version="215" xmlns="http://uniprot.org/uniprot">
  <accession>P07148</accession>
  <name>FABPL_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Fatty acid-binding protein, liver</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Fatty acid-binding protein 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Liver-type fatty acid-binding protein</fullName>
      <shortName>L-FABP</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">FABP1</name>
    <name type="synonym">FABPL</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1985" name="J. Biol. Chem." volume="260" first="2629" last="2632">
      <title>Human liver fatty acid binding protein cDNA and amino acid sequence. Functional and evolutionary implications.</title>
      <authorList>
        <person name="Chan L."/>
        <person name="Wei C.-F."/>
        <person name="Li W.-H."/>
        <person name="Yang C.-Y."/>
        <person name="Ratner P."/>
        <person name="Pownall H."/>
        <person name="Gotto A.M. Jr."/>
        <person name="Smith L.C."/>
      </authorList>
      <dbReference type="PubMed" id="3838309"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)89406-6"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>ACETYLATION AT MET-1</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1985" name="J. Biol. Chem." volume="260" first="3413" last="3417">
      <title>Human liver fatty acid binding protein. Isolation of a full length cDNA and comparative sequence analyses of orthologous and paralogous proteins.</title>
      <authorList>
        <person name="Lowe J.B."/>
        <person name="Boguski M.S."/>
        <person name="Sweetser D.A."/>
        <person name="Elshourbagy N.A."/>
        <person name="Taylor J.M."/>
        <person name="Gordon J.I."/>
      </authorList>
      <dbReference type="PubMed" id="3838313"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(19)83637-2"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>VARIANT ALA-94</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Colon</tissue>
      <tissue>Kidney</tissue>
      <tissue>Stomach</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2011" name="BMC Syst. Biol." volume="5" first="17" last="17">
      <title>Initial characterization of the human central proteome.</title>
      <authorList>
        <person name="Burkard T.R."/>
        <person name="Planyavsky M."/>
        <person name="Kaupe I."/>
        <person name="Breitwieser F.P."/>
        <person name="Buerckstuemmer T."/>
        <person name="Bennett K.L."/>
        <person name="Superti-Furga G."/>
        <person name="Colinge J."/>
      </authorList>
      <dbReference type="PubMed" id="21269460"/>
      <dbReference type="DOI" id="10.1186/1752-0509-5-17"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2014" name="J. Proteomics" volume="96" first="253" last="262">
      <title>An enzyme assisted RP-RPLC approach for in-depth analysis of human liver phosphoproteome.</title>
      <authorList>
        <person name="Bian Y."/>
        <person name="Song C."/>
        <person name="Cheng K."/>
        <person name="Dong M."/>
        <person name="Wang F."/>
        <person name="Huang J."/>
        <person name="Sun D."/>
        <person name="Wang L."/>
        <person name="Ye M."/>
        <person name="Zou H."/>
      </authorList>
      <dbReference type="PubMed" id="24275569"/>
      <dbReference type="DOI" id="10.1016/j.jprot.2013.11.014"/>
    </citation>
    <scope>PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-11; THR-51 AND SER-56</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2015" name="Biochim. Biophys. Acta" volume="1851" first="946" last="955">
      <title>Human FABP1 T94A variant enhances cholesterol uptake.</title>
      <authorList>
        <person name="Huang H."/>
        <person name="McIntosh A.L."/>
        <person name="Landrock K.K."/>
        <person name="Landrock D."/>
        <person name="Storey S.M."/>
        <person name="Martin G.G."/>
        <person name="Gupta S."/>
        <person name="Atshaves B.P."/>
        <person name="Kier A.B."/>
        <person name="Schroeder F."/>
      </authorList>
      <dbReference type="PubMed" id="25732850"/>
      <dbReference type="DOI" id="10.1016/j.bbalip.2015.02.015"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>CHARACTERIZATION OF VARIANT ALA-94</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2007" name="J. Am. Chem. Soc." volume="129" first="7722" last="7723">
      <title>Rapid data collection for protein structure determination by NMR spectroscopy.</title>
      <authorList>
        <person name="Xu Y."/>
        <person name="Long D."/>
        <person name="Yang D."/>
      </authorList>
      <dbReference type="PubMed" id="17536800"/>
      <dbReference type="DOI" id="10.1021/ja071442e"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <reference key="8">
    <citation type="submission" date="2005-12" db="PDB data bank">
      <title>Crystal structure of human FABP1.</title>
      <authorList>
        <consortium name="Structural genomics consortium (SGC)"/>
      </authorList>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.5 ANGSTROMS)</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4">Plays a role in lipoprotein-mediated cholesterol uptake in hepatocytes (PubMed:25732850). Binds cholesterol (PubMed:25732850). Binds free fatty acids and their coenzyme A derivatives, bilirubin, and some other small molecules in the cytoplasm. May be involved in intracellular lipid transport (By similarity).</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-2115989">
      <id>P07148</id>
    </interactant>
    <interactant intactId="EBI-5773557">
      <id>P54764</id>
      <label>EPHA4</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-2115989">
      <id>P07148</id>
    </interactant>
    <interactant intactId="EBI-9641086">
      <id>P21333-2</id>
      <label>FLNA</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-2115989">
      <id>P07148</id>
    </interactant>
    <interactant intactId="EBI-3913338">
      <id>Q96EK6</id>
      <label>GNPNAT1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text>Forms a beta-barrel structure that accommodates hydrophobic ligands in its interior.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the calycin superfamily. Fatty-acid binding protein (FABP) family.</text>
  </comment>
  <dbReference type="EMBL" id="M10617">
    <property type="protein sequence ID" value="AAA52419.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M10050">
    <property type="protein sequence ID" value="AAA52418.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC032801">
    <property type="protein sequence ID" value="AAH32801.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS2001.1"/>
  <dbReference type="PIR" id="A22289">
    <property type="entry name" value="FZHUL"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001434.1">
    <property type="nucleotide sequence ID" value="NM_001443.2"/>
  </dbReference>
  <dbReference type="PDB" id="2F73">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="2L67">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="2L68">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="2LKK">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="2PY1">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="3B2H">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.55 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3B2I">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.86 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3B2J">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3B2K">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.73 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3B2L">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.25 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3STK">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.55 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3STM">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.22 A"/>
    <property type="chains" value="X=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3STN">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.60 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3VG2">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.40 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3VG3">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.22 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3VG4">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3VG5">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3VG6">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.22 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="3VG7">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.44 A"/>
    <property type="chains" value="A=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="6DO6">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="6DO7">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="6DRG">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="6MP4">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="7DZE">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.55 A"/>
    <property type="chains" value="A/B=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="7DZF">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A/B=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="7DZG">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="A/B=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="7DZH">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.65 A"/>
    <property type="chains" value="A/B=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="7DZI">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.65 A"/>
    <property type="chains" value="A/B=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="7DZJ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.63 A"/>
    <property type="chains" value="A/B=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="7DZK">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.54 A"/>
    <property type="chains" value="A/B=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="7DZL">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.64 A"/>
    <property type="chains" value="A/B=2-127"/>
  </dbReference>
  <dbReference type="PDB" id="7FXO">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.30 A"/>
    <property type="chains" value="A/B=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="7FY8">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.23 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="7FYA">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.88 A"/>
    <property type="chains" value="A/B=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="7G00">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.60 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J/K/L/M/N/O/P=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="7G0W">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.64 A"/>
    <property type="chains" value="A/B/C/D=1-127"/>
  </dbReference>
  <dbReference type="PDB" id="7G1X">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.65 A"/>
    <property type="chains" value="A/B=1-127"/>
  </dbReference>
  <dbReference type="PDBsum" id="2F73"/>
  <dbReference type="PDBsum" id="2L67"/>
  <dbReference type="PDBsum" id="2L68"/>
  <dbReference type="PDBsum" id="2LKK"/>
  <dbReference type="PDBsum" id="2PY1"/>
  <dbReference type="PDBsum" id="3B2H"/>
  <dbReference type="PDBsum" id="3B2I"/>
  <dbReference type="PDBsum" id="3B2J"/>
  <dbReference type="PDBsum" id="3B2K"/>
  <dbReference type="PDBsum" id="3B2L"/>
  <dbReference type="PDBsum" id="3STK"/>
  <dbReference type="PDBsum" id="3STM"/>
  <dbReference type="PDBsum" id="3STN"/>
  <dbReference type="PDBsum" id="3VG2"/>
  <dbReference type="PDBsum" id="3VG3"/>
  <dbReference type="PDBsum" id="3VG4"/>
  <dbReference type="PDBsum" id="3VG5"/>
  <dbReference type="PDBsum" id="3VG6"/>
  <dbReference type="PDBsum" id="3VG7"/>
  <dbReference type="PDBsum" id="6DO6"/>
  <dbReference type="PDBsum" id="6DO7"/>
  <dbReference type="PDBsum" id="6DRG"/>
  <dbReference type="PDBsum" id="6MP4"/>
  <dbReference type="PDBsum" id="7DZE"/>
  <dbReference type="PDBsum" id="7DZF"/>
  <dbReference type="PDBsum" id="7DZG"/>
  <dbReference type="PDBsum" id="7DZH"/>
  <dbReference type="PDBsum" id="7DZI"/>
  <dbReference type="PDBsum" id="7DZJ"/>
  <dbReference type="PDBsum" id="7DZK"/>
  <dbReference type="PDBsum" id="7DZL"/>
  <dbReference type="PDBsum" id="7FXO"/>
  <dbReference type="PDBsum" id="7FY8"/>
  <dbReference type="PDBsum" id="7FYA"/>
  <dbReference type="PDBsum" id="7G00"/>
  <dbReference type="PDBsum" id="7G0W"/>
  <dbReference type="PDBsum" id="7G1X"/>
  <dbReference type="AlphaFoldDB" id="P07148"/>
  <dbReference type="BMRB" id="P07148"/>
  <dbReference type="PCDDB" id="P07148"/>
  <dbReference type="SMR" id="P07148"/>
  <dbReference type="BioGRID" id="108466">
    <property type="interactions" value="12"/>
  </dbReference>
  <dbReference type="IntAct" id="P07148">
    <property type="interactions" value="7"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000295834"/>
  <dbReference type="BindingDB" id="P07148"/>
  <dbReference type="ChEMBL" id="CHEMBL5421"/>
  <dbReference type="DrugBank" id="DB02074">
    <property type="generic name" value="Butenoic Acid"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB02659">
    <property type="generic name" value="Cholic Acid"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB04224">
    <property type="generic name" value="Oleic Acid"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB02216">
    <property type="generic name" value="S-Methylcysteine"/>
  </dbReference>
  <dbReference type="DrugCentral" id="P07148"/>
  <dbReference type="SwissLipids" id="SLP:000001516"/>
  <dbReference type="GlyGen" id="P07148">
    <property type="glycosylation" value="1 site, 1 O-linked glycan (1 site)"/>
  </dbReference>
  <dbReference type="iPTMnet" id="P07148"/>
  <dbReference type="PhosphoSitePlus" id="P07148"/>
  <dbReference type="BioMuta" id="FABP1"/>
  <dbReference type="DMDM" id="119808"/>
  <dbReference type="jPOST" id="P07148"/>
  <dbReference type="MassIVE" id="P07148"/>
  <dbReference type="PaxDb" id="9606-ENSP00000295834"/>
  <dbReference type="PeptideAtlas" id="P07148"/>
  <dbReference type="ProteomicsDB" id="51959"/>
  <dbReference type="Antibodypedia" id="3707">
    <property type="antibodies" value="671 antibodies from 40 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="2168"/>
  <dbReference type="Ensembl" id="ENST00000295834.8">
    <property type="protein sequence ID" value="ENSP00000295834.3"/>
    <property type="gene ID" value="ENSG00000163586.10"/>
  </dbReference>
  <dbReference type="GeneID" id="2168"/>
  <dbReference type="KEGG" id="hsa:2168"/>
  <dbReference type="MANE-Select" id="ENST00000295834.8">
    <property type="protein sequence ID" value="ENSP00000295834.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_001443.3"/>
    <property type="RefSeq protein sequence ID" value="NP_001434.1"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:3555"/>
  <dbReference type="CTD" id="2168"/>
  <dbReference type="DisGeNET" id="2168"/>
  <dbReference type="GeneCards" id="FABP1"/>
  <dbReference type="HGNC" id="HGNC:3555">
    <property type="gene designation" value="FABP1"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000163586">
    <property type="expression patterns" value="Group enriched (intestine, liver)"/>
  </dbReference>
  <dbReference type="MIM" id="134650">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P07148"/>
  <dbReference type="OpenTargets" id="ENSG00000163586"/>
  <dbReference type="PharmGKB" id="PA27956"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000163586"/>
  <dbReference type="eggNOG" id="KOG4015">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000155135"/>
  <dbReference type="HOGENOM" id="CLU_113772_4_2_1"/>
  <dbReference type="InParanoid" id="P07148"/>
  <dbReference type="OMA" id="DTITNTM"/>
  <dbReference type="OrthoDB" id="3015340at2759"/>
  <dbReference type="PhylomeDB" id="P07148"/>
  <dbReference type="TreeFam" id="TF330348"/>
  <dbReference type="PathwayCommons" id="P07148"/>
  <dbReference type="Reactome" id="R-HSA-163560">
    <property type="pathway name" value="Triglyceride catabolism"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-189483">
    <property type="pathway name" value="Heme degradation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-1989781">
    <property type="pathway name" value="PPARA activates gene expression"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-400206">
    <property type="pathway name" value="Regulation of lipid metabolism by PPARalpha"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9707564">
    <property type="pathway name" value="Cytoprotection by HMOX1"/>
  </dbReference>
  <dbReference type="SignaLink" id="P07148"/>
  <dbReference type="SIGNOR" id="P07148"/>
  <dbReference type="BioGRID-ORCS" id="2168">
    <property type="hits" value="8 hits in 1149 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="FABP1">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P07148"/>
  <dbReference type="GeneWiki" id="FABP1"/>
  <dbReference type="GenomeRNAi" id="2168"/>
  <dbReference type="Pharos" id="P07148">
    <property type="development level" value="Tchem"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P07148"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="RNAct" id="P07148">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000163586">
    <property type="expression patterns" value="Expressed in mucosa of transverse colon and 105 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P07148">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045179">
    <property type="term" value="C:apical cortex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HPA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070062">
    <property type="term" value="C:extracellular exosome"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005654">
    <property type="term" value="C:nucleoplasm"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HPA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005782">
    <property type="term" value="C:peroxisomal matrix"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032991">
    <property type="term" value="C:protein-containing complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016209">
    <property type="term" value="F:antioxidant activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032052">
    <property type="term" value="F:bile acid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003682">
    <property type="term" value="F:chromatin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005504">
    <property type="term" value="F:fatty acid binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:1901363">
    <property type="term" value="F:heterocyclic compound binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005324">
    <property type="term" value="F:long-chain fatty acid transmembrane transporter activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070538">
    <property type="term" value="F:oleic acid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005543">
    <property type="term" value="F:phospholipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070301">
    <property type="term" value="P:cellular response to hydrogen peroxide"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071456">
    <property type="term" value="P:cellular response to hypoxia"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015908">
    <property type="term" value="P:fatty acid transport"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050892">
    <property type="term" value="P:intestinal absorption"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043066">
    <property type="term" value="P:negative regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043154">
    <property type="term" value="P:negative regulation of cysteine-type endopeptidase activity involved in apoptotic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032000">
    <property type="term" value="P:positive regulation of fatty acid beta-oxidation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0033552">
    <property type="term" value="P:response to vitamin B3"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="CDD" id="cd19444">
    <property type="entry name" value="FABP1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.128.20:FF:000006">
    <property type="entry name" value="Fatty acid-binding protein, liver"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.128.20">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012674">
    <property type="entry name" value="Calycin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000463">
    <property type="entry name" value="Fatty_acid-bd"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031259">
    <property type="entry name" value="ILBP"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11955">
    <property type="entry name" value="FATTY ACID BINDING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11955:SF96">
    <property type="entry name" value="FATTY ACID-BINDING PROTEIN, LIVER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF14651">
    <property type="entry name" value="Lipocalin_7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00178">
    <property type="entry name" value="FATTYACIDBP"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50814">
    <property type="entry name" value="Lipocalins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00214">
    <property type="entry name" value="FABP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0446">Lipid-binding</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000067334" description="Fatty acid-binding protein, liver">
    <location>
      <begin position="1"/>
      <end position="127"/>
    </location>
  </feature>
  <feature type="modified residue" description="N-acetylmethionine" evidence="5">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="8">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-succinyllysine" evidence="2">
    <location>
      <position position="31"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-succinyllysine" evidence="2">
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="1">
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-succinyllysine" evidence="2">
    <location>
      <position position="46"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphothreonine" evidence="8">
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="8">
    <location>
      <position position="56"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-succinyllysine" evidence="2">
    <location>
      <position position="57"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-succinyllysine" evidence="2">
    <location>
      <position position="78"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-succinyllysine" evidence="2">
    <location>
      <position position="90"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="2">
    <location>
      <position position="100"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-succinyllysine" evidence="2">
    <location>
      <position position="121"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_014662" description="In dbSNP:rs1801273.">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="54"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_022093" description="Increases the binding for cholesterol; increases high density lipoprotein (HDL)- and low density lipoprotein (LDL)-mediated cholesterol uptake; dbSNP:rs2241883." evidence="6">
    <original>T</original>
    <variation>A</variation>
    <location>
      <position position="94"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="5"/>
      <end position="14"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="15"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="26"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="38"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="47"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="57"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="69"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="74"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="78"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="83"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="90"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="98"/>
      <end position="105"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="108"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="118"/>
      <end position="126"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P02692"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P12710"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P82289"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="25732850"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="3838309"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="3838313"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0007744" key="8">
    <source>
      <dbReference type="PubMed" id="24275569"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="9">
    <source>
      <dbReference type="PDB" id="3B2L"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="10">
    <source>
      <dbReference type="PDB" id="7FXO"/>
    </source>
  </evidence>
  <sequence length="127" mass="14208" checksum="065DCEFB08DAB6B6" modified="1988-04-01" version="1">MSFSGKYQLQSQENFEAFMKAIGLPEELIQKGKDIKGVSEIVQNGKHFKFTITAGSKVIQNEFTVGEECELETMTGEKVKTVVQLEGDNKLVTTFKNIKSVTELNGDIITNTMTLGDIVFKRISKRI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>