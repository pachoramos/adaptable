<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-07-28" modified="2024-10-02" version="72" xmlns="http://uniprot.org/uniprot">
  <accession>Q2V315</accession>
  <accession>F4KBR3</accession>
  <name>DF285_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 285</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">At5g44973</name>
    <name type="ORF">K21C13</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="DNA Res." volume="5" first="131" last="145">
      <title>Structural analysis of Arabidopsis thaliana chromosome 5. V. Sequence features of the regions of 1,381,565 bp covered by twenty one physically assigned P1 and TAC clones.</title>
      <authorList>
        <person name="Kaneko T."/>
        <person name="Kotani H."/>
        <person name="Nakamura Y."/>
        <person name="Sato S."/>
        <person name="Asamizu E."/>
        <person name="Miyajima N."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="9679202"/>
      <dbReference type="DOI" id="10.1093/dnares/5.2.131"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2007" name="Plant J." volume="51" first="262" last="280">
      <title>Small cysteine-rich peptides resembling antimicrobial peptides have been under-predicted in plants.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Moskal W.A. Jr."/>
        <person name="Wu H.C."/>
        <person name="Underwood B.A."/>
        <person name="Graham M.A."/>
        <person name="Town C.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="17565583"/>
      <dbReference type="DOI" id="10.1111/j.1365-313x.2007.03136.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] OF 65-102</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="EMBL" id="AB010693">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002688">
    <property type="protein sequence ID" value="AED95182.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF182855">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001032012.2">
    <property type="nucleotide sequence ID" value="NM_001036935.4"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q2V315"/>
  <dbReference type="STRING" id="3702.Q2V315"/>
  <dbReference type="PaxDb" id="3702-AT5G44973.1"/>
  <dbReference type="ProteomicsDB" id="224092"/>
  <dbReference type="EnsemblPlants" id="AT5G44973.1">
    <property type="protein sequence ID" value="AT5G44973.1"/>
    <property type="gene ID" value="AT5G44973"/>
  </dbReference>
  <dbReference type="GeneID" id="3771434"/>
  <dbReference type="Gramene" id="AT5G44973.1">
    <property type="protein sequence ID" value="AT5G44973.1"/>
    <property type="gene ID" value="AT5G44973"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT5G44973"/>
  <dbReference type="Araport" id="AT5G44973"/>
  <dbReference type="TAIR" id="AT5G44973"/>
  <dbReference type="HOGENOM" id="CLU_179564_0_0_1"/>
  <dbReference type="InParanoid" id="Q2V315"/>
  <dbReference type="OMA" id="DYRRACC"/>
  <dbReference type="OrthoDB" id="688212at2759"/>
  <dbReference type="PRO" id="PR:Q2V315"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 5"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q2V315">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000379745" description="Defensin-like protein 285">
    <location>
      <begin position="29"/>
      <end position="102"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="39"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="64"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="70"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="75"/>
      <end position="90"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="102" mass="11401" checksum="D22FE47C5873DFC1" modified="2011-12-14" version="2" precursor="true">MTNLYFKTAFLLSLLLLSFSYQSKLIEAKEDKECNDSKCLPNPSRINLEEGNIKRIDVNHGGICDTYLECGGLACSDYRRACCVNGKCVCRKQGQTLPDCPN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>