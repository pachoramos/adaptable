<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2014-04-16" modified="2024-11-27" version="46" xmlns="http://uniprot.org/uniprot">
  <accession>P9WNS8</accession>
  <accession>L0TAX1</accession>
  <accession>O07735</accession>
  <accession>P63995</accession>
  <name>DTD_MYCTO</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">D-aminoacyl-tRNA deacylase</fullName>
      <shortName evidence="1">DTD</shortName>
      <ecNumber evidence="1">3.1.1.96</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Gly-tRNA(Ala) deacylase</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">dtd</name>
    <name type="ordered locus">MT1948</name>
  </gene>
  <organism>
    <name type="scientific">Mycobacterium tuberculosis (strain CDC 1551 / Oshkosh)</name>
    <dbReference type="NCBI Taxonomy" id="83331"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Mycobacteriales</taxon>
      <taxon>Mycobacteriaceae</taxon>
      <taxon>Mycobacterium</taxon>
      <taxon>Mycobacterium tuberculosis complex</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="J. Bacteriol." volume="184" first="5479" last="5490">
      <title>Whole-genome comparison of Mycobacterium tuberculosis clinical and laboratory strains.</title>
      <authorList>
        <person name="Fleischmann R.D."/>
        <person name="Alland D."/>
        <person name="Eisen J.A."/>
        <person name="Carpenter L."/>
        <person name="White O."/>
        <person name="Peterson J.D."/>
        <person name="DeBoy R.T."/>
        <person name="Dodson R.J."/>
        <person name="Gwinn M.L."/>
        <person name="Haft D.H."/>
        <person name="Hickey E.K."/>
        <person name="Kolonay J.F."/>
        <person name="Nelson W.C."/>
        <person name="Umayam L.A."/>
        <person name="Ermolaeva M.D."/>
        <person name="Salzberg S.L."/>
        <person name="Delcher A."/>
        <person name="Utterback T.R."/>
        <person name="Weidman J.F."/>
        <person name="Khouri H.M."/>
        <person name="Gill J."/>
        <person name="Mikula A."/>
        <person name="Bishai W."/>
        <person name="Jacobs W.R. Jr."/>
        <person name="Venter J.C."/>
        <person name="Fraser C.M."/>
      </authorList>
      <dbReference type="PubMed" id="12218036"/>
      <dbReference type="DOI" id="10.1128/jb.184.19.5479-5490.2002"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CDC 1551 / Oshkosh</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">An aminoacyl-tRNA editing enzyme that deacylates mischarged D-aminoacyl-tRNAs. Also deacylates mischarged glycyl-tRNA(Ala), protecting cells against glycine mischarging by AlaRS. Acts via tRNA-based rather than protein-based catalysis; rejects L-amino acids rather than detecting D-amino acids in the active site. By recycling D-aminoacyl-tRNA to D-amino acids and free tRNA molecules, this enzyme counteracts the toxicity associated with the formation of D-aminoacyl-tRNA entities in vivo and helps enforce protein L-homochirality.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>glycyl-tRNA(Ala) + H2O = tRNA(Ala) + glycine + H(+)</text>
      <dbReference type="Rhea" id="RHEA:53744"/>
      <dbReference type="Rhea" id="RHEA-COMP:9657"/>
      <dbReference type="Rhea" id="RHEA-COMP:13640"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:57305"/>
      <dbReference type="ChEBI" id="CHEBI:78442"/>
      <dbReference type="ChEBI" id="CHEBI:78522"/>
      <dbReference type="EC" id="3.1.1.96"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>a D-aminoacyl-tRNA + H2O = a tRNA + a D-alpha-amino acid + H(+)</text>
      <dbReference type="Rhea" id="RHEA:13953"/>
      <dbReference type="Rhea" id="RHEA-COMP:10123"/>
      <dbReference type="Rhea" id="RHEA-COMP:10124"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:59871"/>
      <dbReference type="ChEBI" id="CHEBI:78442"/>
      <dbReference type="ChEBI" id="CHEBI:79333"/>
      <dbReference type="EC" id="3.1.1.96"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="1">A Gly-cisPro motif from one monomer fits into the active site of the other monomer to allow specific chiral rejection of L-amino acids.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the DTD family.</text>
  </comment>
  <comment type="sequence caution" evidence="2">
    <conflict type="erroneous initiation">
      <sequence resource="EMBL-CDS" id="AAK46219" version="1"/>
    </conflict>
    <text>Extended N-terminus.</text>
  </comment>
  <dbReference type="EC" id="3.1.1.96" evidence="1"/>
  <dbReference type="EMBL" id="AE000516">
    <property type="protein sequence ID" value="AAK46219.1"/>
    <property type="status" value="ALT_INIT"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="F70517">
    <property type="entry name" value="F70517"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003409533.1">
    <property type="nucleotide sequence ID" value="NZ_KK341227.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P9WNS8"/>
  <dbReference type="SMR" id="P9WNS8"/>
  <dbReference type="KEGG" id="mtc:MT1948"/>
  <dbReference type="PATRIC" id="fig|83331.31.peg.2096"/>
  <dbReference type="HOGENOM" id="CLU_076901_1_2_11"/>
  <dbReference type="Proteomes" id="UP000001020">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051500">
    <property type="term" value="F:D-tyrosyl-tRNA(Tyr) deacylase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0106026">
    <property type="term" value="F:Gly-tRNA(Ala) hydrolase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043908">
    <property type="term" value="F:Ser(Gly)-tRNA(Ala) hydrolase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000049">
    <property type="term" value="F:tRNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019478">
    <property type="term" value="P:D-amino acid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="CDD" id="cd00563">
    <property type="entry name" value="Dtyr_deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.50.80.10:FF:000002">
    <property type="entry name" value="D-aminoacyl-tRNA deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.50.80.10">
    <property type="entry name" value="D-tyrosyl-tRNA(Tyr) deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00518">
    <property type="entry name" value="Deacylase_Dtd"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003732">
    <property type="entry name" value="Daa-tRNA_deacyls_DTD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023509">
    <property type="entry name" value="DTD-like_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00256">
    <property type="entry name" value="D-aminoacyl-tRNA deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10472:SF5">
    <property type="entry name" value="D-AMINOACYL-TRNA DEACYLASE 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10472">
    <property type="entry name" value="D-TYROSYL-TRNA TYR DEACYLASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02580">
    <property type="entry name" value="Tyr_Deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF69500">
    <property type="entry name" value="DTD-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0820">tRNA-binding</keyword>
  <feature type="chain" id="PRO_0000427074" description="D-aminoacyl-tRNA deacylase">
    <location>
      <begin position="1"/>
      <end position="143"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Gly-cisPro motif, important for rejection of L-amino acids" evidence="1">
    <location>
      <begin position="135"/>
      <end position="136"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00518"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="143" mass="15116" checksum="9D9C9D92237A4F21" modified="2014-04-16" version="1">MRVLVQRVSSAAVRVDGRVVGAIRPDGQGLVAFVGVTHGDDLDKARRLAEKLWNLRVLADEKSASDMHAPILVISQFTLYADTAKGRRPSWNAAAPGAVAQPLIAAFAAALRQLGAHVEAGVFGAHMQVELVNDGPVTVMLEG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>