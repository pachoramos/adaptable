<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-15" modified="2024-05-29" version="43" xmlns="http://uniprot.org/uniprot">
  <accession>P69095</accession>
  <accession>P09473</accession>
  <name>PYY_SCYCA</name>
  <protein>
    <recommendedName>
      <fullName>Peptide YY-like</fullName>
      <shortName>PYY</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Neuropeptide Y-related peptide</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Scyliorhinus canicula</name>
    <name type="common">Small-spotted catshark</name>
    <name type="synonym">Squalus canicula</name>
    <dbReference type="NCBI Taxonomy" id="7830"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Chondrichthyes</taxon>
      <taxon>Elasmobranchii</taxon>
      <taxon>Galeomorphii</taxon>
      <taxon>Galeoidea</taxon>
      <taxon>Carcharhiniformes</taxon>
      <taxon>Scyliorhinidae</taxon>
      <taxon>Scyliorhinus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="Endocrinology" volume="128" first="2273" last="2279">
      <title>Structural characterization and biological activity of a neuropeptide Y-related peptide from the dogfish, Scyliorhinus canicula.</title>
      <authorList>
        <person name="Conlon J.M."/>
        <person name="Balasubramaniam A."/>
        <person name="Hazon N."/>
      </authorList>
      <dbReference type="PubMed" id="2019251"/>
      <dbReference type="DOI" id="10.1210/endo-128-5-2273"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT TYR-36</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue>Pancreas</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Elicits an increase in arterial blood pressure.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the NPY family.</text>
  </comment>
  <dbReference type="PIR" id="A49743">
    <property type="entry name" value="A49743"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P69095"/>
  <dbReference type="SMR" id="P69095"/>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031841">
    <property type="term" value="F:neuropeptide Y receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="CDD" id="cd00126">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.900">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001955">
    <property type="entry name" value="Pancreatic_hormone-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020392">
    <property type="entry name" value="Pancreatic_hormone-like_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533">
    <property type="entry name" value="NEUROPEPTIDE Y/PANCREATIC HORMONE/PEPTIDE YY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533:SF14">
    <property type="entry name" value="PEPTIDE YY-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00159">
    <property type="entry name" value="Hormone_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00278">
    <property type="entry name" value="PANCHORMONE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00309">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00265">
    <property type="entry name" value="PANCREATIC_HORMONE_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50276">
    <property type="entry name" value="PANCREATIC_HORMONE_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044820" description="Peptide YY-like">
    <location>
      <begin position="1"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="1">
    <location>
      <position position="36"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2019251"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="36" mass="4291" checksum="56A6D8CC086660AA" modified="2005-02-15" version="1">YPPKPENPGEDAPPEELAKYYSALRHYINLITRQRY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>