<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-05-04" modified="2022-05-25" version="59" xmlns="http://uniprot.org/uniprot">
  <accession>O93226</accession>
  <name>DRS5_AGAAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="7">Dermaseptin-A5</fullName>
      <shortName evidence="7">DRS-A5</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Dermaseptin AA-3-6</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="4">Dermaseptin-A4</fullName>
      <shortName evidence="4">DRS-A4</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Agalychnis annae</name>
    <name type="common">Blue-sided leaf frog</name>
    <name type="synonym">Phyllomedusa annae</name>
    <dbReference type="NCBI Taxonomy" id="75990"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Agalychnis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Biochim. Biophys. Acta" volume="1388" first="279" last="283">
      <title>Cloning of cDNAs encoding new peptides of the dermaseptin-family.</title>
      <authorList>
        <person name="Wechselberger C."/>
      </authorList>
      <dbReference type="PubMed" id="9774745"/>
      <dbReference type="DOI" id="10.1016/s0167-4838(98)00202-7"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>AMIDATION AT VAL-77</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="Peptides" volume="29" first="2074" last="2082">
      <title>A consistent nomenclature of antimicrobial peptides isolated from frogs of the subfamily Phyllomedusinae.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="18644413"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.06.017"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Possesses a potent antimicrobial activity against Gram-positive and Gram-negative bacteria. Probably acts by disturbing membrane functions with its amphipathic structure (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="8">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=0967"/>
  </comment>
  <dbReference type="EMBL" id="AJ005188">
    <property type="protein sequence ID" value="CAA06425.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O93226"/>
  <dbReference type="SMR" id="O93226"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000007073" evidence="6">
    <location>
      <begin position="23"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000007074" description="Dermaseptin-A5" evidence="8">
    <location>
      <begin position="46"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000007075" evidence="6">
    <location>
      <begin position="79"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="24"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="8">
    <location>
      <position position="77"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="9774745"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="9774745"/>
    </source>
  </evidence>
  <sequence length="80" mass="8817" checksum="DB13FD831E7E2140" modified="1998-11-01" version="1" precursor="true">MAFLKKSLFLVLFLGLVSLSICEEEKRENEDEEEQEDDEQSEMKRGMWSTIRNVGKSAAKAANLPAKAALGAISEAVGEQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>