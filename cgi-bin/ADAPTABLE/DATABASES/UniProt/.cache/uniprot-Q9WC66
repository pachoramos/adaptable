<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-07-11" modified="2024-11-27" version="101" xmlns="http://uniprot.org/uniprot">
  <accession>Q9WC66</accession>
  <name>TAT_HV1S9</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein Tat</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Transactivating regulatory protein</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">tat</name>
  </gene>
  <organism>
    <name type="scientific">Human immunodeficiency virus type 1 group M subtype J (isolate SE9173)</name>
    <name type="common">HIV-1</name>
    <dbReference type="NCBI Taxonomy" id="388904"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Pararnavirae</taxon>
      <taxon>Artverviricota</taxon>
      <taxon>Revtraviricetes</taxon>
      <taxon>Ortervirales</taxon>
      <taxon>Retroviridae</taxon>
      <taxon>Orthoretrovirinae</taxon>
      <taxon>Lentivirus</taxon>
      <taxon>Human immunodeficiency virus type 1</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1999" name="AIDS Res. Hum. Retroviruses" volume="15" first="293" last="297">
      <title>Virtually full-length sequences of HIV type 1 subtype J reference strains.</title>
      <authorList>
        <person name="Laukkanen T."/>
        <person name="Albert J."/>
        <person name="Liitsola K."/>
        <person name="Green S.D."/>
        <person name="Carr J.K."/>
        <person name="Leitner T."/>
        <person name="McCutchan F.E."/>
        <person name="Salminen M.O."/>
      </authorList>
      <dbReference type="PubMed" id="10052760"/>
      <dbReference type="DOI" id="10.1089/088922299311475"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Microbes Infect." volume="7" first="1364" last="1369">
      <title>Decoding Tat: the biology of HIV Tat posttranslational modifications.</title>
      <authorList>
        <person name="Hetzer C."/>
        <person name="Dormeyer W."/>
        <person name="Schnolzer M."/>
        <person name="Ott M."/>
      </authorList>
      <dbReference type="PubMed" id="16046164"/>
      <dbReference type="DOI" id="10.1016/j.micinf.2005.06.003"/>
    </citation>
    <scope>REVIEW</scope>
    <scope>ALTERNATIVE SPLICING</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2006" name="Front. Biosci." volume="11" first="708" last="717">
      <title>The multiple functions of HIV-1 Tat: proliferation versus apoptosis.</title>
      <authorList>
        <person name="Peruzzi F."/>
      </authorList>
      <dbReference type="PubMed" id="16146763"/>
      <dbReference type="DOI" id="10.2741/1829"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2006" name="Microbes Infect." volume="8" first="1347" last="1357">
      <title>HIV tat and neurotoxicity.</title>
      <authorList>
        <person name="King J.E."/>
        <person name="Eugenin E.A."/>
        <person name="Buckner C.M."/>
        <person name="Berman J.W."/>
      </authorList>
      <dbReference type="PubMed" id="16697675"/>
      <dbReference type="DOI" id="10.1016/j.micinf.2005.11.014"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Transcriptional activator that increases RNA Pol II processivity, thereby increasing the level of full-length viral transcripts. Recognizes a hairpin structure at the 5'-LTR of the nascent viral mRNAs referred to as the transactivation responsive RNA element (TAR) and recruits the cyclin T1-CDK9 complex (P-TEFb complex) that will in turn hyperphosphorylate the RNA polymerase II to allow efficient elongation. The CDK9 component of P-TEFb and other Tat-activated kinases hyperphosphorylate the C-terminus of RNA Pol II that becomes stabilized and much more processive. Other factors such as HTATSF1/Tat-SF1, SUPT5H/SPT5, and HTATIP2 are also important for Tat's function. Besides its effect on RNA Pol II processivity, Tat induces chromatin remodeling of proviral genes by recruiting the histone acetyltransferases (HATs) CREBBP, EP300 and PCAF to the chromatin. This also contributes to the increase in proviral transcription rate, especially when the provirus integrates in transcriptionally silent region of the host genome. To ensure maximal activation of the LTR, Tat mediates nuclear translocation of NF-kappa-B by interacting with host RELA. Through its interaction with host TBP, Tat may also modulate transcription initiation. Tat can reactivate a latently infected cell by penetrating in it and transactivating its LTR promoter. In the cytoplasm, Tat is thought to act as a translational activator of HIV-1 mRNAs.</text>
  </comment>
  <comment type="function">
    <text evidence="1">Extracellular circulating Tat can be endocytosed by surrounding uninfected cells via the binding to several surface receptors such as CD26, CXCR4, heparan sulfate proteoglycans (HSPG) or LDLR. Neurons are rarely infected, but they internalize Tat via their LDLR. Through its interaction with nuclear HATs, Tat is potentially able to control the acetylation-dependent cellular gene expression. Modulates the expression of many cellular genes involved in cell survival, proliferation or in coding for cytokines or cytokine receptors. Tat plays a role in T-cell and neurons apoptosis. Tat induced neurotoxicity and apoptosis probably contribute to neuroAIDS. Circulating Tat also acts as a chemokine-like and/or growth factor-like molecule that binds to specific receptors on the surface of the cells, affecting many cellular pathways. In the vascular system, Tat binds to ITGAV/ITGB3 and ITGA5/ITGB1 integrins dimers at the surface of endothelial cells and competes with bFGF for heparin-binding sites, leading to an excess of soluble bFGF.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with host CCNT1. Associates with the P-TEFb complex composed at least of Tat, P-TEFb (CDK9 and CCNT1), TAR RNA, RNA Pol II. Recruits the HATs CREBBP, TAF1/TFIID, EP300, PCAF and GCN5L2. Interacts with host KAT5/Tip60; this interaction targets the latter to degradation. Interacts with the host deacetylase SIRT1. Interacts with host capping enzyme RNGTT; this interaction stimulates RNGTT. Binds to host KDR, and to the host integrins ITGAV/ITGB3 and ITGA5/ITGB1. Interacts with host KPNB1/importin beta-1 without previous binding to KPNA1/importin alpha-1. Interacts with EIF2AK2. Interacts with host nucleosome assembly protein NAP1L1; this interaction may be required for the transport of Tat within the nucleus, since the two proteins interact at the nuclear rim. Interacts with host C1QBP/SF2P32; this interaction involves lysine-acetylated Tat. Interacts with the host chemokine receptors CCR2, CCR3 and CXCR4. Interacts with host DPP4/CD26; this interaction may trigger an anti-proliferative effect. Interacts with host LDLR. Interacts with the host extracellular matrix metalloproteinase MMP1. Interacts with host PRMT6; this interaction mediates Tat's methylation. Interacts with, and is ubiquitinated by MDM2/Hdm2. Interacts with host PSMC3 and HTATIP2. Interacts with STAB1; this interaction may overcome SATB1-mediated repression of IL2 and IL2RA (interleukin) in T cells by binding to the same domain than HDAC1. Interacts (when acetylated) with human CDK13, thereby increasing HIV-1 mRNA splicing and promoting the production of the doubly spliced HIV-1 protein Nef.Interacts with host TBP; this interaction modulates the activity of transcriptional pre-initiation complex. Interacts with host RELA. Interacts with host PLSCR1; this interaction negatively regulates Tat transactivation activity by altering its subcellular distribution.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
      <location evidence="1">Host nucleolus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <text evidence="1">Probably localizes to both nuclear and nucleolar compartments. Nuclear localization is mediated through the interaction of the nuclear localization signal with importin KPNB1. Secretion occurs through a Golgi-independent pathway. Tat is released from infected cells to the extracellular space where it remains associated to the cell membrane, or is secreted into the cerebrospinal fluid and sera. Extracellular Tat can be endocytosed by surrounding uninfected cells via binding to several receptors depending on the cell type.</text>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>Q9WC66-1</id>
      <name>Long</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>Q9WC66-2</id>
      <name>Short</name>
      <sequence type="described" ref="VSP_022425"/>
    </isoform>
  </comment>
  <comment type="domain">
    <text evidence="1">The cell attachment site mediates the interaction with ITGAV/ITGB3 and ITGA5/ITGB1 integrins, leading to vascular cell migration and invasion. This interaction also provides endothelial cells with the adhesion signal they require to grow in response to mitogens.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The Cys-rich region may bind 2 zinc ions. This region is involved in binding to KAT5.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The transactivation domain mediates the interaction with CCNT1, GCN5L2, and MDM2.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The Arg-rich RNA-binding region binds the TAR RNA. This region also mediates the nuclear localization through direct binding to KPNB1 and is involved in Tat's transfer across cell membranes (protein transduction). The same region is required for the interaction with EP300, PCAF, EIF2AK2 and KDR.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Asymmetrical arginine methylation by host PRMT6 seems to diminish the transactivation capacity of Tat and affects the interaction with host CCNT1.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Acetylation by EP300, CREBBP, GCN5L2/GCN5 and PCAF regulates the transactivation activity of Tat. EP300-mediated acetylation of Lys-50 promotes dissociation of Tat from the TAR RNA through the competitive binding to PCAF's bromodomain. In addition, the non-acetylated Tat's N-terminus can also interact with PCAF. PCAF-mediated acetylation of Lys-28 enhances Tat's binding to CCNT1. Lys-50 is deacetylated by SIRT1.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Polyubiquitination by host MDM2 does not target Tat to degradation, but activates its transactivation function and fosters interaction with CCNT1 and TAR RNA.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Phosphorylated by EIF2AK2 on serine and threonine residues adjacent to the basic region important for TAR RNA binding and function. Phosphorylation of Tat by EIF2AK2 is dependent on the prior activation of EIF2AK2 by dsRNA.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">HIV-1 lineages are divided in three main groups, M (for Major), O (for Outlier), and N (for New, or Non-M, Non-O). The vast majority of strains found worldwide belong to the group M. Group O seems to be endemic to and largely confined to Cameroon and neighboring countries in West Central Africa, where these viruses represent a small minority of HIV-1 strains. The group N is represented by a limited number of isolates from Cameroonian persons. The group M is further subdivided in 9 clades or subtypes (A to D, F to H, J and K).</text>
  </comment>
  <comment type="miscellaneous">
    <molecule>Isoform Short</molecule>
    <text evidence="3">Expressed in the late stage of the infection cycle, when unspliced viral RNAs are exported to the cytoplasm by the viral Rev protein.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the lentiviruses Tat family.</text>
  </comment>
  <dbReference type="EMBL" id="AF082395">
    <property type="protein sequence ID" value="AAD17772.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="SMR" id="Q9WC66"/>
  <dbReference type="Proteomes" id="UP000123434">
    <property type="component" value="Segment"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030430">
    <property type="term" value="C:host cell cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044196">
    <property type="term" value="C:host cell nucleolus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042805">
    <property type="term" value="F:actinin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030332">
    <property type="term" value="F:cyclin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019904">
    <property type="term" value="F:protein domain specific binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004865">
    <property type="term" value="F:protein serine/threonine phosphatase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001070">
    <property type="term" value="F:RNA-binding transcription regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990970">
    <property type="term" value="F:trans-activation response element binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006915">
    <property type="term" value="P:apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006351">
    <property type="term" value="P:DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039525">
    <property type="term" value="P:modulation by virus of host chromatin organization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010801">
    <property type="term" value="P:negative regulation of peptidyl-threonine phosphorylation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032968">
    <property type="term" value="P:positive regulation of transcription elongation by RNA polymerase II"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050434">
    <property type="term" value="P:positive regulation of viral transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039606">
    <property type="term" value="P:symbiont-mediated suppression of host translation initiation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039502">
    <property type="term" value="P:symbiont-mediated suppression of host type I interferon-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.20.10">
    <property type="entry name" value="Tat domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_04079">
    <property type="entry name" value="HIV_TAT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001831">
    <property type="entry name" value="IV_Tat"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036963">
    <property type="entry name" value="Tat_dom_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00539">
    <property type="entry name" value="Tat"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00055">
    <property type="entry name" value="HIVTATDOMAIN"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0010">Activator</keyword>
  <keyword id="KW-0014">AIDS</keyword>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0053">Apoptosis</keyword>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1114">Inhibition of host interferon signaling pathway by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-1017">Isopeptide bond</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0488">Methylation</keyword>
  <keyword id="KW-1122">Modulation of host chromatin by virus</keyword>
  <keyword id="KW-1126">Modulation of host PP1 activity by virus</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0832">Ubl conjugation</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <feature type="chain" id="PRO_0000244856" description="Protein Tat">
    <location>
      <begin position="1"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="region of interest" description="Transactivation" evidence="1">
    <location>
      <begin position="1"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="region of interest" description="Interaction with human CREBBP" evidence="1">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="region of interest" description="Cysteine-rich" evidence="1">
    <location>
      <begin position="22"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="region of interest" description="Core" evidence="1">
    <location>
      <begin position="38"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="48"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="region of interest" description="Interaction with the host capping enzyme RNGTT" evidence="1">
    <location>
      <begin position="49"/>
      <end position="86"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Nuclear localization signal, RNA-binding (TAR), and protein transduction" evidence="1">
    <location>
      <begin position="49"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="2">
    <location>
      <begin position="82"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="22"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="25"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="27"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="30"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="33"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="34"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="37"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="site" description="Essential for Tat translocation through the endosomal membrane" evidence="1">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine; by host PCAF" evidence="1">
    <location>
      <position position="28"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine; by host EP300 and GCN5L2" evidence="1">
    <location>
      <position position="50"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine; by host EP300 and GCN5L2" evidence="1">
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="modified residue" description="Asymmetric dimethylarginine; by host PRMT6" evidence="1">
    <location>
      <position position="52"/>
    </location>
  </feature>
  <feature type="modified residue" description="Asymmetric dimethylarginine; by host PRMT6" evidence="1">
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_022425" description="In isoform Short.">
    <location>
      <begin position="73"/>
      <end position="101"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_04079"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="101" mass="11658" checksum="4607FE0B224E3BBF" modified="1999-11-01" version="1">MEPVDPNREPWNHPGSQPKTACTNCYCKKCCYHCQVCFLQKGLGISYGRKKRRQRRSAPPGSKNHQDLIPEQPLFQTQRKPTGPEESKKEVESKAEPDRFD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>