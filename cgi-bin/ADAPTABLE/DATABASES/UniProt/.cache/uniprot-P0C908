<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-04-14" modified="2024-11-27" version="34" xmlns="http://uniprot.org/uniprot">
  <accession>P0C908</accession>
  <accession>B8XH28</accession>
  <name>KAX3A_BUTIS</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Potassium channel toxin alpha-KTx 3.10</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">BoiTx1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Buthus israelis</name>
    <name type="common">Israeli scorpion</name>
    <name type="synonym">Buthus occitanus israelis</name>
    <dbReference type="NCBI Taxonomy" id="2899555"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Buthus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2007" name="FEBS Lett." volume="581" first="2478" last="2484">
      <title>Isolation of the first toxin from the scorpion Buthus occitanus israelis showing preference for Shaker potassium channels.</title>
      <authorList>
        <person name="Kozminsky-Atias A."/>
        <person name="Somech E."/>
        <person name="Zilberberg N."/>
      </authorList>
      <dbReference type="PubMed" id="17490656"/>
      <dbReference type="DOI" id="10.1016/j.febslet.2007.04.065"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 23-59</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2018" name="Nat. Struct. Mol. Biol." volume="25" first="270" last="278">
      <title>Screening, large-scale production and structure-based classification of cystine-dense peptides.</title>
      <authorList>
        <person name="Correnti C.E."/>
        <person name="Gewe M.M."/>
        <person name="Mehlin C."/>
        <person name="Bandaranayake A.D."/>
        <person name="Johnsen W.A."/>
        <person name="Rupert P.B."/>
        <person name="Brusniak M.Y."/>
        <person name="Clarke M."/>
        <person name="Burke S.E."/>
        <person name="De Van Der Schueren W."/>
        <person name="Pilat K."/>
        <person name="Turnbaugh S.M."/>
        <person name="May D."/>
        <person name="Watson A."/>
        <person name="Chan M.K."/>
        <person name="Bahl C.D."/>
        <person name="Olson J.M."/>
        <person name="Strong R.K."/>
      </authorList>
      <dbReference type="PubMed" id="29483648"/>
      <dbReference type="DOI" id="10.1038/s41594-018-0033-9"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 23-59</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2">Inhibits insect potassium channel. Is at least a 100-fold more potent against the Drosophila Shaker channel than towards its mammalian homologs Kv1.1/KCNA1 and Kv1.3/KCNA3.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="4">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="mass spectrometry" mass="4015.6" method="MALDI" evidence="1"/>
  <comment type="miscellaneous">
    <text evidence="1 2">Mammalian potassium channels (Kv2.1/KCNB1, Kir1.1/KCNJ1, Kv3.4/KCNC4, Kv4.2/KCND2, Kv7.1/KCNQ1, Kv7.2/KCNQ2, Kv7.3/KCNQ3 and Kv11.1/KCNH2) are even less affected by this toxin (PubMed:17490656). Also weakly inhibits Kv11.1/KCNH2/ERG1, Kv1.2/KCNA2 and Nav1.7/SCN9A channels (PubMed:29483648).</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the short scorpion toxin superfamily. Potassium channel inhibitor family. Alpha-KTx 03 subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="FJ360817">
    <property type="protein sequence ID" value="ACJ23137.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="6ATM">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.09 A"/>
    <property type="chains" value="C=23-59"/>
  </dbReference>
  <dbReference type="PDBsum" id="6ATM"/>
  <dbReference type="AlphaFoldDB" id="P0C908"/>
  <dbReference type="SMR" id="P0C908"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008200">
    <property type="term" value="F:ion channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000009">
    <property type="entry name" value="Potassium channel toxin alpha-KTx 4.3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001947">
    <property type="entry name" value="Scorpion_toxinS_K_inh"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00451">
    <property type="entry name" value="Toxin_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00286">
    <property type="entry name" value="CHARYBDTOXIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01138">
    <property type="entry name" value="SCORP_SHORT_TOXIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000368016" description="Potassium channel toxin alpha-KTx 3.10" evidence="1">
    <location>
      <begin position="23"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="6">
    <location>
      <begin position="30"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="6">
    <location>
      <begin position="36"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="6">
    <location>
      <begin position="40"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="24"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="33"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="37"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="45"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="54"/>
      <end position="58"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="17490656"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="29483648"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="17490656"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="17490656"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="6">
    <source>
      <dbReference type="PDB" id="6ATM"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="6ATM"/>
    </source>
  </evidence>
  <sequence length="59" mass="6402" checksum="FEE2327C83D2C862" modified="2009-04-14" version="1" precursor="true">MKVFFAVLIALFVCSMVIGIHGGVPINVKCRGSRDCLDPCKKAGMRFGKCINSKCHCTP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>