<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-10-01" modified="2024-07-24" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>P38583</accession>
  <name>YBM1_CARML</name>
  <protein>
    <recommendedName>
      <fullName>Uncharacterized 9.1 kDa protein in BM1 immunity protein 3'region</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Carnobacterium maltaromaticum</name>
    <name type="common">Carnobacterium piscicola</name>
    <dbReference type="NCBI Taxonomy" id="2751"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Carnobacteriaceae</taxon>
      <taxon>Carnobacterium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="J. Biol. Chem." volume="269" first="12204" last="12211">
      <title>Chemical and genetic characterization of bacteriocins produced by Carnobacterium piscicola LV17B.</title>
      <authorList>
        <person name="Quadri L.E.N."/>
        <person name="Sailer M."/>
        <person name="Roy K.L."/>
        <person name="Vederas J.C."/>
        <person name="Stiles M.E."/>
      </authorList>
      <dbReference type="PubMed" id="8163526"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)32702-3"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>LV17B</strain>
    </source>
  </reference>
  <dbReference type="EMBL" id="L29059">
    <property type="protein sequence ID" value="AAA72433.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="E53589">
    <property type="entry name" value="E53589"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P38583"/>
  <dbReference type="SMR" id="P38583"/>
  <dbReference type="GO" id="GO:0030153">
    <property type="term" value="P:bacteriocin immunity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.1440.140">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR053739">
    <property type="entry name" value="Bact_Immunity_Domain_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015046">
    <property type="entry name" value="LciA_Immunity-like"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08951">
    <property type="entry name" value="EntA_Immun"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="predicted"/>
  <feature type="chain" id="PRO_0000066148" description="Uncharacterized 9.1 kDa protein in BM1 immunity protein 3'region">
    <location>
      <begin position="1"/>
      <end position="81"/>
    </location>
  </feature>
  <sequence length="81" mass="9070" checksum="9FA00FB3441118FB" modified="1994-10-01" version="1">MATITDLLNDLKIDLGNESLQNVLENYLEELEQANAAVPIILGRMNIDISTAIRKDGVTLSEIQSKKLKELISISYIKYGY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>