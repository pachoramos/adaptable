<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2021-09-29" modified="2024-01-24" version="12" xmlns="http://uniprot.org/uniprot">
  <accession>A0A646QV53</accession>
  <name>LECS1_MYTVI</name>
  <protein>
    <recommendedName>
      <fullName evidence="3 4">GM1b/asialo-GM1 oligosaccharide-binding R-type lectin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="7">R-type lectin 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="3 4">SeviL</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="3">SeviL-1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Mytilisepta virgata</name>
    <name type="common">Purplish bifurcate mussel</name>
    <name type="synonym">Tichogonia virgata</name>
    <dbReference type="NCBI Taxonomy" id="2547956"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Bivalvia</taxon>
      <taxon>Autobranchia</taxon>
      <taxon>Pteriomorphia</taxon>
      <taxon>Mytilida</taxon>
      <taxon>Mytiloidea</taxon>
      <taxon>Mytilidae</taxon>
      <taxon>Brachidontinae</taxon>
      <taxon>Mytilisepta</taxon>
    </lineage>
  </organism>
  <reference evidence="7" key="1">
    <citation type="journal article" date="2019" name="FEBS J." volume="287" first="2612" last="2630">
      <title>A GM1b/asialo-GM1 oligosaccharide-binding R-type lectin from purplish bifurcate mussels Mytilisepta virgata and its effect on MAP kinases.</title>
      <authorList>
        <person name="Fujii Y."/>
        <person name="Gerdol M."/>
        <person name="Kawsar S.M.A."/>
        <person name="Hasan I."/>
        <person name="Spazzali F."/>
        <person name="Yoshida T."/>
        <person name="Ogawa Y."/>
        <person name="Rajia S."/>
        <person name="Kamata K."/>
        <person name="Koide Y."/>
        <person name="Sugawara S."/>
        <person name="Hosono M."/>
        <person name="Tame J.R.H."/>
        <person name="Fujita H."/>
        <person name="Pallavicini A."/>
        <person name="Ozeki Y."/>
      </authorList>
      <dbReference type="PubMed" id="31769916"/>
      <dbReference type="DOI" id="10.1111/febs.15154"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 104-115</scope>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>SUBUNIT</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>BIOTECHNOLOGY</scope>
    <scope>3D-STRUCTURE MODELING</scope>
  </reference>
  <reference evidence="8 9" key="2">
    <citation type="journal article" date="2020" name="Sci. Rep." volume="10" first="22102" last="22102">
      <title>The structure of SeviL, a GM1b/asialo-GM1 binding R-type lectin from the mussel Mytilisepta virgata.</title>
      <authorList>
        <person name="Kamata K."/>
        <person name="Mizutani K."/>
        <person name="Takahashi K."/>
        <person name="Marchetti R."/>
        <person name="Silipo A."/>
        <person name="Addy C."/>
        <person name="Park S.Y."/>
        <person name="Fujii Y."/>
        <person name="Fujita H."/>
        <person name="Konuma T."/>
        <person name="Ikegami T."/>
        <person name="Ozeki Y."/>
        <person name="Tame J.R.H."/>
      </authorList>
      <dbReference type="PubMed" id="33328520"/>
      <dbReference type="DOI" id="10.1038/s41598-020-78926-7"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.60 ANGSTROMS) AND IN COMPLEX WITH OLIGOSACCHARIDE OF ASIALO-GM1</scope>
    <scope>FUNCTION</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>SUBUNIT</scope>
    <scope>BIOTECHNOLOGY</scope>
    <scope>MUTAGENESIS OF GLN-12; ASP-39 AND PHE-126</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2">Galbeta1-3GalNAcbeta1-4Galbeta1-4Glc oligosaccharide-binding lectin. Binds strongly to the oligosaccharides of ganglioside GM1b and to a lesser extent its precursor asialo-GM1 (PubMed:31769916, PubMed:33328520). Binds weakly to asialo-GM2 oligosaccharide and to the glycan moiety of globo-series stage-specific embryonal antigen 4 (SSEA-4) hexaose (PubMed:31769916). Binds galactose, N-acetylgalactose and lactose. Does not bind GM1 (PubMed:33328520). Does not bind to Gal-beta1,3-GalNAc (Thomsen-Friedenreich antigen), the oligosaccharide of GM1a ganglioside or SSEA-4 tetraose. Does not bind to N-glycans, O-glycans or glycosaminoglycans of glycoproteins. Does not bind Lewis glycans, derivatives of lactose or N-acetyllactosamine or blood group (ABH-type) oligosaccharides (PubMed:31769916). Does not bind glucose (PubMed:33328520). Has hemagglutination activity towards rabbit erythrocytes (PubMed:31769916, PubMed:33328520). Displays cytotoxic effects against various cultured cell lines including human breast (MCF-7), cervical (HeLa) and colon cancer (Caco2) cell lines, as well as dog kidney (MDCK) cell line that express asialo-GM1 oligosaccharide at their cell surface. Shows dose- and time-dependent activation of MKK3/6, ERK1/2 and p38 MAPK, as well as caspase-3/9 in HeLa cervical cancer cells. No cytotoxic effect on BT474 human breast cancer cell line. May be involved in recognition of glycans found on parasitic or symbiotic microorganisms (PubMed:31769916).</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="1 2">Hemagglutination activity requires divalent cations such as Ca(2+) (PubMed:31769916, PubMed:33328520). Hemagglutination activity is weakly inhibited by monosaccharides such as D-Gal (25 mM), D-GalNAc (25 mM) and D-Fuc (25 mM) and by disaccharides such as melibiose (25 mM) and lactose (25 mM). Hemagglutination activity is inhibited by bovine submaxillary mucin, but not by porcine stomach mucin or fetuin (PubMed:31769916).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 2">Homodimer.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Highest expression in the outer part of the mantle rim. Highly expressed in gills, with a much lower expression in the digestive gland and posterior adductor muscle. Scarcely detectable in foot.</text>
  </comment>
  <comment type="biotechnology">
    <text evidence="5 6">This protein has potential for the detection and control of certain cancer cells and cells of the immune system, that display asialo-GM1 (PubMed:33328520). Has potential clinical applications since it recognizes glycans expressed specifically by the target antigen for Guillain-Barre syndrome (GM1b), natural killer (NK) cells and basophils (asialo-GM1) and glioblastoma multiforme (SSEA-4) in vertebrates (PubMed:31769916).</text>
  </comment>
  <dbReference type="EMBL" id="MK434191">
    <property type="protein sequence ID" value="QBM06340.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="6LF1">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A/B=1-129"/>
  </dbReference>
  <dbReference type="PDB" id="6LF2">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="A/B=1-129"/>
  </dbReference>
  <dbReference type="PDBsum" id="6LF1"/>
  <dbReference type="PDBsum" id="6LF2"/>
  <dbReference type="AlphaFoldDB" id="A0A646QV53"/>
  <dbReference type="SMR" id="A0A646QV53"/>
  <dbReference type="UniLectin" id="A0A646QV53"/>
  <dbReference type="GO" id="GO:0030246">
    <property type="term" value="F:carbohydrate binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.80.10.50">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035992">
    <property type="entry name" value="Ricin_B-like_lectins"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50370">
    <property type="entry name" value="Ricin B-like lectins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0348">Hemagglutinin</keyword>
  <keyword id="KW-0430">Lectin</keyword>
  <feature type="chain" id="PRO_0000453280" description="GM1b/asialo-GM1 oligosaccharide-binding R-type lectin">
    <location>
      <begin position="1"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="binding site" evidence="2 10">
    <location>
      <begin position="21"/>
      <end position="23"/>
    </location>
    <ligand>
      <name>a carbohydrate</name>
      <dbReference type="ChEBI" id="CHEBI:16646"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2 10">
    <location>
      <begin position="26"/>
      <end position="28"/>
    </location>
    <ligand>
      <name>a carbohydrate</name>
      <dbReference type="ChEBI" id="CHEBI:16646"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2 10">
    <location>
      <position position="32"/>
    </location>
    <ligand>
      <name>a carbohydrate</name>
      <dbReference type="ChEBI" id="CHEBI:16646"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2 10">
    <location>
      <begin position="37"/>
      <end position="40"/>
    </location>
    <ligand>
      <name>a carbohydrate</name>
      <dbReference type="ChEBI" id="CHEBI:16646"/>
    </ligand>
  </feature>
  <feature type="mutagenesis site" description="Loss of hemagglutination. Loss of homodimerization, but retains saccharide binding; when associated with K-126." evidence="2">
    <original>Q</original>
    <variation>R</variation>
    <location>
      <position position="12"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of hemagglutination." evidence="2">
    <original>D</original>
    <variation>H</variation>
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of hemagglutination. Loss of homodimerization, but retains saccharide binding; when associated with R-12." evidence="2">
    <original>F</original>
    <variation>K</variation>
    <location>
      <position position="126"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="7"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="turn" evidence="11">
    <location>
      <begin position="14"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="19"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="turn" evidence="11">
    <location>
      <begin position="24"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="28"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="helix" evidence="11">
    <location>
      <begin position="38"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="42"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="53"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="60"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="69"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="helix" evidence="11">
    <location>
      <begin position="77"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="81"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="93"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="turn" evidence="11">
    <location>
      <begin position="98"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="103"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="110"/>
      <end position="116"/>
    </location>
  </feature>
  <feature type="helix" evidence="11">
    <location>
      <begin position="119"/>
      <end position="121"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="123"/>
      <end position="127"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="31769916"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="33328520"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="31769916"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="33328520"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="31769916"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="33328520"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="EMBL" id="QBM06340.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="PDB" id="6LF1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="9">
    <source>
      <dbReference type="PDB" id="6LF2"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="10">
    <source>
      <dbReference type="PDB" id="6LF2"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="11">
    <source>
      <dbReference type="PDB" id="6LF2"/>
    </source>
  </evidence>
  <sequence length="129" mass="15047" checksum="149CC6B3A03C7562" modified="2020-04-22" version="1">MSSVTIGKCYIQNRENGGRAFYNLGRKDLGIFTGKMYDDQIWSFQKSDTPGYYTIGRESKFLQYNGEQVIMSDIEQDTTLWSLEEVPEDKGFYRLLNKVHKAYLDYNGGDLVANKHQTESEKWILFKAY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>