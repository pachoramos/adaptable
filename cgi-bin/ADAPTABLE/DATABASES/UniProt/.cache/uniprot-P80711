<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2022-05-25" version="44" xmlns="http://uniprot.org/uniprot">
  <accession>P80711</accession>
  <name>CLAVB_STYCL</name>
  <protein>
    <recommendedName>
      <fullName>Clavanin-B</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Styela clava</name>
    <name type="common">Sea squirt</name>
    <dbReference type="NCBI Taxonomy" id="7725"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Tunicata</taxon>
      <taxon>Ascidiacea</taxon>
      <taxon>Stolidobranchia</taxon>
      <taxon>Styelidae</taxon>
      <taxon>Styela</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="FEBS Lett." volume="400" first="158" last="162">
      <title>Clavanins, alpha-helical antimicrobial peptides from tunicate hemocytes.</title>
      <authorList>
        <person name="Lee I.H."/>
        <person name="Zhao C."/>
        <person name="Cho Y."/>
        <person name="Harwig S.S.L."/>
        <person name="Cooper E.L."/>
        <person name="Lehrer R.I."/>
      </authorList>
      <dbReference type="PubMed" id="9001389"/>
      <dbReference type="DOI" id="10.1016/s0014-5793(96)01374-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-23</scope>
    <source>
      <tissue>Hemocyte</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Has antimicrobial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="AlphaFoldDB" id="P80711"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008453">
    <property type="entry name" value="Clavanin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05452">
    <property type="entry name" value="Clavanin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044123" description="Clavanin-B">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="9001389"/>
    </source>
  </evidence>
  <sequence length="23" mass="2695" checksum="B30E236441195B15" modified="1998-07-15" version="1">VFQFLGRIIHHVGNFVHGFSHVF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>