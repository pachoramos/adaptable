<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-10-03" modified="2020-02-26" version="28" xmlns="http://uniprot.org/uniprot">
  <accession>P84935</accession>
  <name>BRK3_PITAZ</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">[Val1,Hyp2,Thr6]-bradykinyl-Gln,Ser</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">[Val1,Hyp2,Thr6]-bradykinin-Gln,Ser</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Pithecopus azureus</name>
    <name type="common">Orange-legged monkey tree frog</name>
    <name type="synonym">Phyllomedusa azurea</name>
    <dbReference type="NCBI Taxonomy" id="2034991"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Pithecopus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2006" name="Rapid Commun. Mass Spectrom." volume="20" first="3780" last="3788">
      <title>Bradykinin-related peptides from Phyllomedusa hypochondrialis azurea: Mass spectrometric structural characterisation and cloning of precursor cDNAs.</title>
      <authorList>
        <person name="Thompson A.H."/>
        <person name="Bjourson A.J."/>
        <person name="Shaw C."/>
        <person name="McClean S."/>
      </authorList>
      <dbReference type="PubMed" id="17120273"/>
      <dbReference type="DOI" id="10.1002/rcm.2791"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>HYDROXYLATION AT PRO-2</scope>
    <source>
      <tissue evidence="2">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">May produce in vitro relaxation of rat arterial smooth muscle and constriction of intestinal smooth muscle (By similarity). May target bradykinin receptors (BDKRB).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1247.75" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the bradykinin-related peptide family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042311">
    <property type="term" value="P:vasodilation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009608">
    <property type="entry name" value="Bradykinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06753">
    <property type="entry name" value="Bradykinin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0379">Hydroxylation</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0838">Vasoactive</keyword>
  <keyword id="KW-0840">Vasodilator</keyword>
  <feature type="peptide" id="PRO_0000250426" description="[Val1,Hyp2,Thr6]-bradykinyl-Gln,Ser" evidence="2">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="4-hydroxyproline" evidence="2">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="17120273"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="17120273"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="11" mass="1232" checksum="2BF39687D771A9C8" modified="2006-10-03" version="1">VPPGFTPFRQS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>