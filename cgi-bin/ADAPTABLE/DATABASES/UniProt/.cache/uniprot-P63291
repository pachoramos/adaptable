<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1987-08-13" modified="2024-11-27" version="57" xmlns="http://uniprot.org/uniprot">
  <accession>P63291</accession>
  <accession>P04565</accession>
  <name>VIP_SHEEP</name>
  <protein>
    <recommendedName>
      <fullName>Vasoactive intestinal peptide</fullName>
      <shortName>VIP</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Vasoactive intestinal polypeptide</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">VIP</name>
  </gene>
  <organism>
    <name type="scientific">Ovis aries</name>
    <name type="common">Sheep</name>
    <dbReference type="NCBI Taxonomy" id="9940"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Caprinae</taxon>
      <taxon>Ovis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="Peptides" volume="11" first="703" last="706">
      <title>Isolation and primary structure of VIP from sheep brain.</title>
      <authorList>
        <person name="Gafvelin G."/>
      </authorList>
      <dbReference type="PubMed" id="2235680"/>
      <dbReference type="DOI" id="10.1016/0196-9781(90)90184-7"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1991" name="Regul. Pept." volume="32" first="169" last="179">
      <title>Purification and amino acid sequence of vasoactive intestinal peptide, peptide histidine isoleucinamide and secretin from the ovine small intestine.</title>
      <authorList>
        <person name="Bounjoua Y."/>
        <person name="Vandermeers A."/>
        <person name="Robberecht P."/>
        <person name="Vandermeers-Piret M.C."/>
        <person name="Christophe J."/>
      </authorList>
      <dbReference type="PubMed" id="2034821"/>
      <dbReference type="DOI" id="10.1016/0167-0115(91)90044-h"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT ASN-28</scope>
    <source>
      <tissue>Small intestine</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1992" name="Regul. Pept." volume="38" first="145" last="154">
      <title>Chemical characterization of vasoactive intestinal polypeptide-like immunoreactivity in ovine hypothalamus and intestine.</title>
      <authorList>
        <person name="Miyata A."/>
        <person name="Jiang L."/>
        <person name="Stibbs H.H."/>
        <person name="Arimura A."/>
      </authorList>
      <dbReference type="PubMed" id="1574609"/>
      <dbReference type="DOI" id="10.1016/0167-0115(92)90053-w"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Hypothalamus</tissue>
      <tissue>Intestine</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>VIP causes vasodilation, lowers arterial blood pressure, stimulates myocardial contractility, increases glycogenolysis and relaxes the smooth muscle of trachea, stomach and gall bladder.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the glucagon family.</text>
  </comment>
  <dbReference type="PIR" id="B60072">
    <property type="entry name" value="VRSH"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P63291"/>
  <dbReference type="SMR" id="P63291"/>
  <dbReference type="eggNOG" id="ENOG502QVTA">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000002356">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043005">
    <property type="term" value="C:neuron projection"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051428">
    <property type="term" value="F:peptide hormone receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007189">
    <property type="term" value="P:adenylate cyclase-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048242">
    <property type="term" value="P:epinephrine secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007611">
    <property type="term" value="P:learning or memory"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048255">
    <property type="term" value="P:mRNA stabilization"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="AgBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043066">
    <property type="term" value="P:negative regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043267">
    <property type="term" value="P:negative regulation of potassium ion transport"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048662">
    <property type="term" value="P:negative regulation of smooth muscle cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007200">
    <property type="term" value="P:phospholipase C-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001938">
    <property type="term" value="P:positive regulation of endothelial cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032812">
    <property type="term" value="P:positive regulation of epinephrine secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060406">
    <property type="term" value="P:positive regulation of penile erection"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045732">
    <property type="term" value="P:positive regulation of protein catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070459">
    <property type="term" value="P:prolactin secretion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="AgBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032880">
    <property type="term" value="P:regulation of protein localization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.590">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000532">
    <property type="entry name" value="Glucagon_GIP_secretin_VIP"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046963">
    <property type="entry name" value="VIP/GHRH-like"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11213">
    <property type="entry name" value="GLUCAGON-FAMILY NEUROPEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11213:SF5">
    <property type="entry name" value="VIP PEPTIDES"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00123">
    <property type="entry name" value="Hormone_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00070">
    <property type="entry name" value="GLUCA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00260">
    <property type="entry name" value="GLUCAGON"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043943" description="Vasoactive intestinal peptide">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="modified residue" description="Asparagine amide" evidence="1">
    <location>
      <position position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2034821"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="28" mass="3327" checksum="EF313FB573FF6F3F" modified="1987-08-13" version="1">HSDAVFTDNYTRLRKQMAVKKYLNSILN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>