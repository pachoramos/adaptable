<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-01-09" modified="2019-12-11" version="9" xmlns="http://uniprot.org/uniprot">
  <accession>B0M3C0</accession>
  <name>AKH_STRNA</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Adipokinetic hormone</fullName>
      <shortName evidence="4">AKH</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Striatophasma naukluftense</name>
    <name type="common">Gladiator</name>
    <name type="synonym">Heel-walker</name>
    <dbReference type="NCBI Taxonomy" id="1041429"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Mantophasmatodea</taxon>
      <taxon>Austrophasmatidae</taxon>
      <taxon>Striatophasma</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2012" name="Syst. Biol." volume="61" first="609" last="629">
      <title>Peptidomics-based phylogeny and biogeography of Mantophasmatodea (Hexapoda).</title>
      <authorList>
        <person name="Predel R."/>
        <person name="Neupert S."/>
        <person name="Huetteroth W."/>
        <person name="Kahnt J."/>
        <person name="Waidelich D."/>
        <person name="Roth S."/>
      </authorList>
      <dbReference type="PubMed" id="22508719"/>
      <dbReference type="DOI" id="10.1093/sysbio/sys003"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>AMIDATION AT TRP-8</scope>
    <source>
      <tissue evidence="3">Corpora cardiaca</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">This hormone, released from cells in the corpora cardiaca, causes release of diglycerides from the fat body and stimulation of muscles to use these diglycerides as an energy source during energy-demanding processes.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the AKH/HRTH/RPCH family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007629">
    <property type="term" value="P:flight behavior"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002047">
    <property type="entry name" value="Adipokinetic_hormone_CS"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00256">
    <property type="entry name" value="AKH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0286">Flight</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000420736" description="Adipokinetic hormone" evidence="3">
    <location>
      <begin position="1"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="3">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tryptophan amide" evidence="3">
    <location>
      <position position="8"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22508719"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="22508719"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="22508719"/>
    </source>
  </evidence>
  <sequence length="8" mass="978" checksum="8665A771A9C452D6" modified="2013-01-09" version="1">QVNFTPSW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>