<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-07-28" modified="2024-01-24" version="25" xmlns="http://uniprot.org/uniprot">
  <accession>P0CAZ6</accession>
  <name>CT113_LACTA</name>
  <protein>
    <recommendedName>
      <fullName>M-zodatoxin-Lt8m</fullName>
      <shortName>M-ZDTX-Lt8m</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Cytoinsectotoxin 1-13</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">cit 1-13</name>
  </gene>
  <organism>
    <name type="scientific">Lachesana tarabaevi</name>
    <name type="common">Spider</name>
    <dbReference type="NCBI Taxonomy" id="379576"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Entelegynae incertae sedis</taxon>
      <taxon>Zodariidae</taxon>
      <taxon>Lachesana</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Biochem. J." volume="411" first="687" last="696">
      <title>Cyto-insectotoxins, a novel class of cytolytic and insecticidal peptides from spider venom.</title>
      <authorList>
        <person name="Vassilevski A.A."/>
        <person name="Kozlov S.A."/>
        <person name="Samsonova O.V."/>
        <person name="Egorova N.S."/>
        <person name="Karpunin D.V."/>
        <person name="Pluzhnikov K.A."/>
        <person name="Feofanov A.V."/>
        <person name="Grishin E.V."/>
      </authorList>
      <dbReference type="PubMed" id="18215128"/>
      <dbReference type="DOI" id="10.1042/bj20071123"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Insecticidal, cytolytic and antimicrobial peptide. Forms voltage-dependent, ion-permeable channels in membranes. At high concentration causes cell membrane lysis (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the cationic peptide 06 (cytoinsectotoxin) family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0CAZ6"/>
  <dbReference type="ArachnoServer" id="AS000767">
    <property type="toxin name" value="M-zodatoxin-Lt8m"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018802">
    <property type="entry name" value="Latarcin_precursor"/>
  </dbReference>
  <dbReference type="Pfam" id="PF10279">
    <property type="entry name" value="Latarcin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000380145" evidence="1">
    <location>
      <begin position="21"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000380146" description="M-zodatoxin-Lt8m">
    <location>
      <begin position="61"/>
      <end position="131"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="131" mass="14925" checksum="9BC46072F75C3CF7" modified="2009-07-28" version="1" precursor="true">MKYFVVALALVAAFACIAESKPAESEHELAEVEEENELADLEDAVWLEHLADLSDLEEARGFFGNTWKKIKGKADKIMLKKAVKIMVKKEGISKEEAQAKVDAMSKKQIRLYLLKHYGKKLFKKRPKNCDQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>