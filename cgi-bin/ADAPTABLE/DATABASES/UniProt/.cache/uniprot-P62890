<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1987-08-13" modified="2024-11-27" version="147" xmlns="http://uniprot.org/uniprot">
  <accession>P62890</accession>
  <accession>P04645</accession>
  <name>RL30_RAT</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Large ribosomal subunit protein eL30</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>60S ribosomal protein L30</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Rpl30</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1985" name="Gene" volume="35" first="289" last="296">
      <title>Molecular cloning and nucleotide sequences of cDNAs specific for rat liver ribosomal proteins S17 and L30.</title>
      <authorList>
        <person name="Nakanishi O."/>
        <person name="Oyanagi M."/>
        <person name="Kuwano Y."/>
        <person name="Tanaka T."/>
        <person name="Nakayama T."/>
        <person name="Mitsui H."/>
        <person name="Nabeshima Y."/>
        <person name="Ogata K."/>
      </authorList>
      <dbReference type="PubMed" id="3840111"/>
      <dbReference type="DOI" id="10.1016/0378-1119(85)90007-1"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Pituitary</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Component of the large ribosomal subunit. The ribosome is a large ribonucleoprotein complex responsible for the synthesis of proteins in the cell.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Component of the large ribosomal subunit.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the eukaryotic ribosomal protein eL30 family.</text>
  </comment>
  <dbReference type="EMBL" id="K02932">
    <property type="protein sequence ID" value="AAA42072.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC058471">
    <property type="protein sequence ID" value="AAH58471.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="B24028">
    <property type="entry name" value="R6RT30"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_073190.1">
    <property type="nucleotide sequence ID" value="NM_022699.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_002727755.1">
    <property type="nucleotide sequence ID" value="XM_002727709.4"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_003752376.1">
    <property type="nucleotide sequence ID" value="XM_003752328.4"/>
  </dbReference>
  <dbReference type="PDB" id="7QGG">
    <property type="method" value="EM"/>
    <property type="resolution" value="2.86 A"/>
    <property type="chains" value="d=1-115"/>
  </dbReference>
  <dbReference type="PDBsum" id="7QGG"/>
  <dbReference type="AlphaFoldDB" id="P62890"/>
  <dbReference type="EMDB" id="EMD-13954"/>
  <dbReference type="SMR" id="P62890"/>
  <dbReference type="BioGRID" id="249178">
    <property type="interactions" value="8"/>
  </dbReference>
  <dbReference type="IntAct" id="P62890">
    <property type="interactions" value="4"/>
  </dbReference>
  <dbReference type="STRING" id="10116.ENSRNOP00000054398"/>
  <dbReference type="iPTMnet" id="P62890"/>
  <dbReference type="PhosphoSitePlus" id="P62890"/>
  <dbReference type="jPOST" id="P62890"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000054398"/>
  <dbReference type="Ensembl" id="ENSRNOT00000042688.3">
    <property type="protein sequence ID" value="ENSRNOP00000072453.1"/>
    <property type="gene ID" value="ENSRNOG00000032825.3"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055045329">
    <property type="protein sequence ID" value="ENSRNOP00055037153"/>
    <property type="gene ID" value="ENSRNOG00055026281"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055052255">
    <property type="protein sequence ID" value="ENSRNOP00055043190"/>
    <property type="gene ID" value="ENSRNOG00055030118"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060016673">
    <property type="protein sequence ID" value="ENSRNOP00060013025"/>
    <property type="gene ID" value="ENSRNOG00060009874"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060053937">
    <property type="protein sequence ID" value="ENSRNOP00060044732"/>
    <property type="gene ID" value="ENSRNOG00060031067"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065010113">
    <property type="protein sequence ID" value="ENSRNOP00065007386"/>
    <property type="gene ID" value="ENSRNOG00065006548"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065026281">
    <property type="protein sequence ID" value="ENSRNOP00065020639"/>
    <property type="gene ID" value="ENSRNOG00065015829"/>
  </dbReference>
  <dbReference type="GeneID" id="64640"/>
  <dbReference type="KEGG" id="rno:64640"/>
  <dbReference type="UCSC" id="RGD:621201">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:2318953"/>
  <dbReference type="AGR" id="RGD:621201"/>
  <dbReference type="CTD" id="6156"/>
  <dbReference type="RGD" id="621201">
    <property type="gene designation" value="Rpl30"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSRNOG00000005975"/>
  <dbReference type="eggNOG" id="KOG2988">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000012138"/>
  <dbReference type="HOGENOM" id="CLU_130502_0_1_1"/>
  <dbReference type="InParanoid" id="P62890"/>
  <dbReference type="OMA" id="YFQGGNN"/>
  <dbReference type="OrthoDB" id="374852at2759"/>
  <dbReference type="PhylomeDB" id="P62890"/>
  <dbReference type="TreeFam" id="TF300252"/>
  <dbReference type="Reactome" id="R-RNO-156827">
    <property type="pathway name" value="L13a-mediated translational silencing of Ceruloplasmin expression"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-1799339">
    <property type="pathway name" value="SRP-dependent cotranslational protein targeting to membrane"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-6791226">
    <property type="pathway name" value="Major pathway of rRNA processing in the nucleolus and cytosol"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-72689">
    <property type="pathway name" value="Formation of a pool of free 40S subunits"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-72706">
    <property type="pathway name" value="GTP hydrolysis and joining of the 60S ribosomal subunit"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-975956">
    <property type="pathway name" value="Nonsense Mediated Decay (NMD) independent of the Exon Junction Complex (EJC)"/>
  </dbReference>
  <dbReference type="Reactome" id="R-RNO-975957">
    <property type="pathway name" value="Nonsense Mediated Decay (NMD) enhanced by the Exon Junction Complex (EJC)"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P62890"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 10"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000005975">
    <property type="expression patterns" value="Expressed in thymus and 17 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0022625">
    <property type="term" value="C:cytosolic large ribosomal subunit"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0022626">
    <property type="term" value="C:cytosolic ribosome"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0014069">
    <property type="term" value="C:postsynaptic density"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005840">
    <property type="term" value="C:ribosome"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045202">
    <property type="term" value="C:synapse"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003723">
    <property type="term" value="F:RNA binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035368">
    <property type="term" value="F:selenocysteine insertion sequence binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003735">
    <property type="term" value="F:structural constituent of ribosome"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002181">
    <property type="term" value="P:cytoplasmic translation"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097421">
    <property type="term" value="P:liver regeneration"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:1904571">
    <property type="term" value="P:positive regulation of selenocysteine incorporation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.1330.30:FF:000001">
    <property type="entry name" value="60S ribosomal protein L30"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.1330.30">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00481">
    <property type="entry name" value="Ribosomal_eL30"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000231">
    <property type="entry name" value="Ribosomal_eL30"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039109">
    <property type="entry name" value="Ribosomal_eL30-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029064">
    <property type="entry name" value="Ribosomal_eL30-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022991">
    <property type="entry name" value="Ribosomal_eL30_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004038">
    <property type="entry name" value="Ribosomal_eL8/eL30/eS12/Gad45"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11449:SF1">
    <property type="entry name" value="60S RIBOSOMAL PROTEIN L30"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11449">
    <property type="entry name" value="RIBOSOMAL PROTEIN L30"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01248">
    <property type="entry name" value="Ribosomal_L7Ae"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55315">
    <property type="entry name" value="L30e-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00709">
    <property type="entry name" value="RIBOSOMAL_L30E_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00993">
    <property type="entry name" value="RIBOSOMAL_L30E_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-1017">Isopeptide bond</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0687">Ribonucleoprotein</keyword>
  <keyword id="KW-0689">Ribosomal protein</keyword>
  <keyword id="KW-0832">Ubl conjugation</keyword>
  <feature type="chain" id="PRO_0000146123" description="Large ribosomal subunit protein eL30">
    <location>
      <begin position="1"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="1">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="1">
    <location>
      <position position="16"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine; alternate" evidence="1">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="cross-link" description="Glycyl lysine isopeptide (Lys-Gly) (interchain with G-Cter in SUMO2); alternate" evidence="1">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P62888"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="115" mass="12784" checksum="95186B081E39748C" modified="2007-01-23" version="2">MVAAKKTKKSLESINSRLQLVMKSGKYVLGYKQTLKMIRQGKAKLVILANNCPALRKSEIEYYAMLAKTGVHHYSGNNIELGTACGKYYRVCTLAIIDPGDSDIIRSMPEQTGEK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>