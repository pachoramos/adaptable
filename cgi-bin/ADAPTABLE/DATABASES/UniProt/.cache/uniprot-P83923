<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-06-07" modified="2019-12-11" version="33" xmlns="http://uniprot.org/uniprot">
  <accession>P83923</accession>
  <accession>P82698</accession>
  <name>PVK1_BLACR</name>
  <protein>
    <recommendedName>
      <fullName>Periviscerokinin-1</fullName>
      <shortName>BlaCr-PVK-1</shortName>
      <shortName>PVK-1</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Blaberus craniifer</name>
    <name type="common">Death's head cockroach</name>
    <dbReference type="NCBI Taxonomy" id="6982"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Dictyoptera</taxon>
      <taxon>Blattodea</taxon>
      <taxon>Blaberoidea</taxon>
      <taxon>Blaberidae</taxon>
      <taxon>Blaberinae</taxon>
      <taxon>Blaberus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Eur. J. Biochem." volume="267" first="3869" last="3873">
      <title>Identification of novel periviscerokinins from single neurohaemal release sites in insects. MS/MS fragmentation complemented by Edman degradation.</title>
      <authorList>
        <person name="Predel R."/>
        <person name="Kellner R."/>
        <person name="Baggerman G."/>
        <person name="Steinmetzer T."/>
        <person name="Schoofs L."/>
      </authorList>
      <dbReference type="PubMed" id="10849006"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.2000.01425.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT THR-11</scope>
    <source>
      <tissue>Abdominal perisympathetic organs</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2009" name="BMC Evol. Biol." volume="9" first="50" last="50">
      <title>A proteomic approach for studying insect phylogeny: CAPA peptides of ancient insect taxa (Dictyoptera, Blattoptera) as a test case.</title>
      <authorList>
        <person name="Roth S."/>
        <person name="Fromm B."/>
        <person name="Gaede G."/>
        <person name="Predel R."/>
      </authorList>
      <dbReference type="PubMed" id="19257902"/>
      <dbReference type="DOI" id="10.1186/1471-2148-9-50"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT THR-11</scope>
    <source>
      <tissue>Abdominal perisympathetic organs</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Mediates visceral muscle contractile activity (myotropic activity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="1090.6" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the periviscerokinin family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013231">
    <property type="entry name" value="Periviscerokinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08259">
    <property type="entry name" value="Periviscerokin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044240" description="Periviscerokinin-1">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="Threonine amide" evidence="1 2">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10849006"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19257902"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="11" mass="1091" checksum="2C2D80E2D7605728" modified="2004-05-24" version="1">GSSGLIPFGRT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>