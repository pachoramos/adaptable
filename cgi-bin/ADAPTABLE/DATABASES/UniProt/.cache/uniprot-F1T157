<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-04-12" modified="2022-05-25" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>F1T157</accession>
  <name>NIG2B_ODOIS</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Nigrocin-2ISb</fullName>
    </recommendedName>
  </protein>
  <organism evidence="4">
    <name type="scientific">Odorrana ishikawae</name>
    <name type="common">Ishikawa's frog</name>
    <name type="synonym">Rana ishikawae</name>
    <dbReference type="NCBI Taxonomy" id="310659"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Odorrana</taxon>
    </lineage>
  </organism>
  <reference evidence="7" key="1">
    <citation type="journal article" date="2011" name="Peptides" volume="32" first="670" last="676">
      <title>Identification and characterization of antimicrobial peptides from the skin of the endangered frog Odorrana ishikawae.</title>
      <authorList>
        <person name="Iwakoshi-Ukena E."/>
        <person name="Ukena K."/>
        <person name="Okimoto A."/>
        <person name="Soga M."/>
        <person name="Okada G."/>
        <person name="Sano N."/>
        <person name="Fujii T."/>
        <person name="Sugawara Y."/>
        <person name="Sumida M."/>
      </authorList>
      <dbReference type="PubMed" id="21193000"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2010.12.013"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 46-66</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="4">Skin</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Has antimicrobial activity against Gram-negative bacterium E.coli ATCC 8739 (MIC=50 ug), against Gram positive bacteria S.aureus ATCC 6538 (MIC=3.1 ug), methicillin-resistant S.aureus ATCC 43300 (MIC=12.5 ug), B.subtilis ATCC 6633 (MIC=12.5 ug) and against fungus C.albicans ATCC 90028 (MIC=50 ug).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2006.5" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="5">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AB602059">
    <property type="protein sequence ID" value="BAK08589.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="F1T157"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR032749">
    <property type="entry name" value="Nigrocin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF16047">
    <property type="entry name" value="Antimicrobial22"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000439615" description="Removed in mature form" evidence="6">
    <location>
      <begin position="23"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000439616" description="Nigrocin-2ISb" evidence="3">
    <location>
      <begin position="46"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="60"/>
      <end position="66"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P39084"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="21193000"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="21193000"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="21193000"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="EMBL" id="BAK08589.1"/>
    </source>
  </evidence>
  <sequence length="66" mass="7412" checksum="79A369861DB5D0E6" modified="2011-05-31" version="1" precursor="true">MFTLKKSMLLLFFLGTINLSLCQEERDAEEERRDEDNAKMEEIKRGILGTVFKAGKGIVCGLTGLC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>