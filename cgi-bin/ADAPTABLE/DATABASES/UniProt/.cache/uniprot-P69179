<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-08-01" modified="2024-11-27" version="76" xmlns="http://uniprot.org/uniprot">
  <accession>P69179</accession>
  <accession>P08027</accession>
  <name>STXB_BPH19</name>
  <protein>
    <recommendedName>
      <fullName>Shiga-like toxin 1 subunit B</fullName>
      <shortName>SLT-1 B subunit</shortName>
      <shortName>SLT-1b</shortName>
      <shortName>SLT-Ib</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Verocytotoxin 1 subunit B</fullName>
      <shortName>Verotoxin 1 subunit B</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">stxB</name>
    <name type="synonym">sltB</name>
  </gene>
  <organism>
    <name type="scientific">Enterobacteria phage H19B</name>
    <name type="common">Bacteriophage H19B</name>
    <dbReference type="NCBI Taxonomy" id="69932"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Duplodnaviria</taxon>
      <taxon>Heunggongvirae</taxon>
      <taxon>Uroviricota</taxon>
      <taxon>Caudoviricetes</taxon>
      <taxon>Lambdavirus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Escherichia coli</name>
    <dbReference type="NCBI Taxonomy" id="562"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1987" name="Proc. Natl. Acad. Sci. U.S.A." volume="84" first="4364" last="4368">
      <title>Nucleotide sequence of the Shiga-like toxin genes of Escherichia coli.</title>
      <authorList>
        <person name="Calderwood S.B."/>
        <person name="Auclair F."/>
        <person name="Donohue-Rolfe A."/>
        <person name="Keusch G.T."/>
        <person name="Mekalanos J.J."/>
      </authorList>
      <dbReference type="PubMed" id="3299365"/>
      <dbReference type="DOI" id="10.1073/pnas.84.13.4364"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1987" name="J. Bacteriol." volume="169" first="4313" last="4319">
      <title>Nucleotide sequence and promoter mapping of the Escherichia coli Shiga-like toxin operon of bacteriophage H-19B.</title>
      <authorList>
        <person name="de Grandis S."/>
        <person name="Ginsberg J."/>
        <person name="Toone M."/>
        <person name="Climie S."/>
        <person name="Friesen J."/>
        <person name="Brunton J.L."/>
      </authorList>
      <dbReference type="PubMed" id="3040689"/>
      <dbReference type="DOI" id="10.1128/jb.169.9.4313-4319.1987"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1996" name="Mol. Microbiol." volume="19" first="891" last="899">
      <title>Phenylalanine 30 plays an important role in receptor binding of verotoxin-1.</title>
      <authorList>
        <person name="Clark C."/>
        <person name="Bast D.J."/>
        <person name="Sharp A.M."/>
        <person name="St Hilaire P.M."/>
        <person name="Agha R."/>
        <person name="Stein P.E."/>
        <person name="Toone E.J."/>
        <person name="Read R.J."/>
        <person name="Brunton J.L."/>
      </authorList>
      <dbReference type="PubMed" id="8820657"/>
      <dbReference type="DOI" id="10.1046/j.1365-2958.1996.427962.x"/>
    </citation>
    <scope>MUTAGENESIS OF LYS-33; ASP-37; PHE-50 AND GLU-85</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1999" name="Mol. Microbiol." volume="32" first="953" last="960">
      <title>The identification of three biologically relevant globotriaosyl ceramide receptor binding sites on the Verotoxin 1 B subunit.</title>
      <authorList>
        <person name="Bast D.J."/>
        <person name="Banerjee L."/>
        <person name="Clark C."/>
        <person name="Read R.J."/>
        <person name="Brunton J.L."/>
      </authorList>
      <dbReference type="PubMed" id="10361298"/>
      <dbReference type="DOI" id="10.1046/j.1365-2958.1999.01405.x"/>
    </citation>
    <scope>MUTAGENESIS OF ASP-37; TRP-54 AND GLY-82</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1992" name="Nature" volume="355" first="748" last="750">
      <title>Crystal structure of the cell-binding B oligomer of verotoxin-1 from E. coli.</title>
      <authorList>
        <person name="Stein P.E."/>
        <person name="Boodhoo A."/>
        <person name="Tyrrell G.J."/>
        <person name="Brunton J.L."/>
        <person name="Read R.J."/>
      </authorList>
      <dbReference type="PubMed" id="1741063"/>
      <dbReference type="DOI" id="10.1038/355748a0"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.2 ANGSTROMS) OF 21-89</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1997" name="Nat. Struct. Biol." volume="4" first="190" last="193">
      <title>Solution structure of the carbohydrate-binding B-subunit homopentamer of verotoxin VT-1 from E. coli.</title>
      <authorList>
        <person name="Richardson J.M."/>
        <person name="Evans P.D."/>
        <person name="Homans S.W."/>
        <person name="Donohue-Rolfe A."/>
      </authorList>
      <dbReference type="PubMed" id="9164458"/>
      <dbReference type="DOI" id="10.1038/nsb0397-190"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 21-89</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1998" name="Biochemistry" volume="37" first="1777" last="1788">
      <title>Structure of the shiga-like toxin I B-pentamer complexed with an analogue of its receptor Gb3.</title>
      <authorList>
        <person name="Ling H."/>
        <person name="Boodhoo A."/>
        <person name="Hazes B."/>
        <person name="Cummings M.D."/>
        <person name="Armstrong G.D."/>
        <person name="Brunton J.L."/>
        <person name="Read R.J."/>
      </authorList>
      <dbReference type="PubMed" id="9485303"/>
      <dbReference type="DOI" id="10.1021/bi971806n"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.8 ANGSTROMS) OF 21-89 IN COMPLEX WITH RECEPTOR GB3</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2000" name="Nature" volume="403" first="669" last="672">
      <title>Shiga-like toxins are neutralized by tailored multivalent carbohydrate ligands.</title>
      <authorList>
        <person name="Kitov P.I."/>
        <person name="Sadowska J.M."/>
        <person name="Mulvey G."/>
        <person name="Armstrong G.D."/>
        <person name="Ling H."/>
        <person name="Pannu N.S."/>
        <person name="Read R.J."/>
        <person name="Bundle D.R."/>
      </authorList>
      <dbReference type="PubMed" id="10688205"/>
      <dbReference type="DOI" id="10.1038/35001095"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.23 ANGSTROMS) OF 21-89 IN COMPLEX WITH INHIBITOR</scope>
  </reference>
  <comment type="function">
    <text>The B subunit is responsible for the binding of the holotoxin to specific receptors on the target cell surface, such as globotriaosylceramide (Gb3) in human intestinal microvilli.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2 4">Shiga-like toxin contains a single subunit A and five copies of subunit B.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text>There are three Gb3-binding sites in each subunit B monomer, allowing for a tighter binding to the target cell. Binding sites 1 and 2 have higher binding affinities than site 3.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the stxB family.</text>
  </comment>
  <dbReference type="EMBL" id="M16625">
    <property type="protein sequence ID" value="AAA98100.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M17358">
    <property type="protein sequence ID" value="AAA32230.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="B27052">
    <property type="entry name" value="XVBPH9"/>
  </dbReference>
  <dbReference type="PIR" id="B53887">
    <property type="entry name" value="B53887"/>
  </dbReference>
  <dbReference type="PDB" id="1BOS">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.80 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J/K/L/M/N/O/P/Q/R/S/T=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="1C48">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="A/B/C/D/E=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="1C4Q">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.52 A"/>
    <property type="chains" value="A/B/C/D/E=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="1CQF">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.20 A"/>
    <property type="chains" value="A/B/C/D/E=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="1CZG">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="A/B/C/D/E=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="1CZW">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.50 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="1D1I">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A/B/C/D/E=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="1QNU">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.23 A"/>
    <property type="chains" value="A/B/C/D/E=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="2XSC">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.05 A"/>
    <property type="chains" value="A/B/C/D/E=21-89"/>
  </dbReference>
  <dbReference type="PDB" id="4ULL">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=21-89"/>
  </dbReference>
  <dbReference type="PDBsum" id="1BOS"/>
  <dbReference type="PDBsum" id="1C48"/>
  <dbReference type="PDBsum" id="1C4Q"/>
  <dbReference type="PDBsum" id="1CQF"/>
  <dbReference type="PDBsum" id="1CZG"/>
  <dbReference type="PDBsum" id="1CZW"/>
  <dbReference type="PDBsum" id="1D1I"/>
  <dbReference type="PDBsum" id="1QNU"/>
  <dbReference type="PDBsum" id="2XSC"/>
  <dbReference type="PDBsum" id="4ULL"/>
  <dbReference type="SMR" id="P69179"/>
  <dbReference type="UniLectin" id="P69179"/>
  <dbReference type="ABCD" id="P69179">
    <property type="antibodies" value="2 sequenced antibodies"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P69179"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019836">
    <property type="term" value="P:hemolysis by symbiont of host erythrocytes"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0098676">
    <property type="term" value="P:modulation of host virulence by virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.70:FF:000001">
    <property type="entry name" value="Shiga toxin 1 subunit B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.70">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008992">
    <property type="entry name" value="Enterotoxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003189">
    <property type="entry name" value="SLT_beta"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02258">
    <property type="entry name" value="SLT_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50203">
    <property type="entry name" value="Bacterial enterotoxins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1254">Modulation of host virulence by virus</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1255">Viral exotoxin</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="signal peptide">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000030794" description="Shiga-like toxin 1 subunit B">
    <location>
      <begin position="21"/>
      <end position="89"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="24"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on binding to receptor." evidence="3">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="33"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on binding to receptor." evidence="1 3">
    <original>D</original>
    <variation>A</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decrease in binding affinity for Gb3, and in binding capacity. Reduced cytotoxicity." evidence="1 3">
    <original>D</original>
    <variation>E</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="4-fold decrease in binding affinity for Gb3; 10-fold reduction in binding capacity; great decrease in cell cytotoxicity." evidence="3">
    <original>F</original>
    <variation>A</variation>
    <location>
      <position position="50"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decrease in binding capacity; no effect on binding affinity; moderate effect on cytotoxicity." evidence="1">
    <original>W</original>
    <variation>A</variation>
    <location>
      <position position="54"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decrease in binding affinity for Gb3, and in binding capacity. Reduced cytotoxicity." evidence="1">
    <original>G</original>
    <variation>T</variation>
    <location>
      <position position="82"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No effect on binding to receptor." evidence="3">
    <original>E</original>
    <variation>A</variation>
    <location>
      <position position="85"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="23"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="36"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="40"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="47"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="56"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="69"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="77"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="85"/>
      <end position="89"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10361298"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10688205"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="8820657"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="9485303"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="1C4Q"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="4ULL"/>
    </source>
  </evidence>
  <sequence length="89" mass="9743" checksum="C78F7795CCD7242E" modified="1988-08-01" version="1" precursor="true">MKKTLLIAASLSFFSASALATPDCVTGKVEYTKYNDDDTFTVKVGDKELFTNRWNLQSLLLSAQITGMTVTIKTNACHNGGGFSEVIFR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>