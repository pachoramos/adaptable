<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1992-05-01" modified="2024-11-27" version="119" xmlns="http://uniprot.org/uniprot">
  <accession>P26202</accession>
  <name>P15A_RABIT</name>
  <protein>
    <recommendedName>
      <fullName>15 kDa protein A</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>P15R</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Protein P15A</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Oryctolagus cuniculus</name>
    <name type="common">Rabbit</name>
    <dbReference type="NCBI Taxonomy" id="9986"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Lagomorpha</taxon>
      <taxon>Leporidae</taxon>
      <taxon>Oryctolagus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="J. Biol. Chem." volume="268" first="6058" last="6063">
      <title>Antibacterial 15-kDa protein isoforms (p15s) are members of a novel family of leukocyte proteins.</title>
      <authorList>
        <person name="Levy O."/>
        <person name="Weiss J."/>
        <person name="Zarember K."/>
        <person name="Ooi C.E."/>
        <person name="Elsbach P."/>
      </authorList>
      <dbReference type="PubMed" id="8449963"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)53425-6"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>New Zealand white</strain>
      <tissue>Bone marrow</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="Agents Actions" volume="39" first="C207" last="C210">
      <title>Structural characterization of BPI-modulating 15 kDa proteins from rabbit polymorphonuclear leukocytes: identification of a novel family of leukocyte proteins.</title>
      <authorList>
        <person name="Levy O."/>
        <person name="Weiss J."/>
        <person name="Ooi C.E."/>
        <person name="Elsbach P."/>
      </authorList>
      <dbReference type="PubMed" id="8273570"/>
      <dbReference type="DOI" id="10.1007/bf01972768"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1990" name="J. Biol. Chem." volume="265" first="15956" last="15962">
      <title>Isolation of two isoforms of a novel 15-kDa protein from rabbit polymorphonuclear leukocytes that modulate the antibacterial actions of other leukocyte proteins.</title>
      <authorList>
        <person name="Ooi C.E."/>
        <person name="Weiss J."/>
        <person name="Levy O."/>
        <person name="Elsbach P."/>
      </authorList>
      <dbReference type="PubMed" id="2203792"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)55490-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-40</scope>
    <source>
      <tissue>Neutrophil</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Binds to bacterial lipopolysaccharides (LPS), potentiates strongly the early antibacterial effects of BPI. Inhibits the late lethal action of BPI.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Large granules of neutrophils.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the cathelicidin family.</text>
  </comment>
  <dbReference type="EMBL" id="L07588">
    <property type="protein sequence ID" value="AAA99904.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="S68154">
    <property type="protein sequence ID" value="AAB29403.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A46634">
    <property type="entry name" value="A46634"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001075794.1">
    <property type="nucleotide sequence ID" value="NM_001082325.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P26202"/>
  <dbReference type="SMR" id="P26202"/>
  <dbReference type="STRING" id="9986.ENSOCUP00000015669"/>
  <dbReference type="PaxDb" id="9986-ENSOCUP00000015669"/>
  <dbReference type="Ensembl" id="ENSOCUT00000026191.3">
    <property type="protein sequence ID" value="ENSOCUP00000015669.1"/>
    <property type="gene ID" value="ENSOCUG00000025236.3"/>
  </dbReference>
  <dbReference type="GeneID" id="100009166"/>
  <dbReference type="KEGG" id="ocu:100009166"/>
  <dbReference type="eggNOG" id="ENOG502SU6N">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00730000111701"/>
  <dbReference type="HOGENOM" id="CLU_121724_1_0_1"/>
  <dbReference type="InParanoid" id="P26202"/>
  <dbReference type="OMA" id="SCDRDCG"/>
  <dbReference type="OrthoDB" id="5264571at2759"/>
  <dbReference type="TreeFam" id="TF338457"/>
  <dbReference type="Proteomes" id="UP000001811">
    <property type="component" value="Chromosome 9"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSOCUG00000025236">
    <property type="expression patterns" value="Expressed in blood and 17 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.450.10:FF:000003">
    <property type="entry name" value="Cathelicidin antimicrobial peptide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.450.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001894">
    <property type="entry name" value="Cathelicidin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018216">
    <property type="entry name" value="Cathelicidin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000010">
    <property type="entry name" value="Cystatin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046350">
    <property type="entry name" value="Cystatin_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10206">
    <property type="entry name" value="CATHELICIDIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10206:SF4">
    <property type="entry name" value="NEUTROPHILIC GRANULE PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00666">
    <property type="entry name" value="Cathelicidins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00043">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54403">
    <property type="entry name" value="Cystatin/monellin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00946">
    <property type="entry name" value="CATHELICIDINS_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00947">
    <property type="entry name" value="CATHELICIDINS_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004756" description="15 kDa protein A">
    <location>
      <begin position="21"/>
      <end position="137"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="77"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="99"/>
      <end position="116"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="3" ref="3">
    <location>
      <position position="25"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="3" ref="3">
    <original>Y</original>
    <variation>E</variation>
    <location>
      <position position="39"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="2203792"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="137" mass="15675" checksum="2706BA3D1E2DF28A" modified="1994-02-01" version="2" precursor="true">MAGVWKVLVVLVGLAVVACAIPRRRLRYEEVVAQALQFYNEGQQGQPLFRLLEATPPPSLNSKSRIPLNFRIKETVCIFTLDRQPGNCAFREGGEERICRGAFVRRRWVRALTLRCDRDQRRQPEFPRVTRPAGPTA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>