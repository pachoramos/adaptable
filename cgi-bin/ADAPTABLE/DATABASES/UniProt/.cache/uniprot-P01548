<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2023-06-28" version="35" xmlns="http://uniprot.org/uniprot">
  <accession>P01548</accession>
  <name>ANSA_STRCZ</name>
  <protein>
    <recommendedName>
      <fullName>Antibacterial substance A</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Streptomyces carzinostaticus</name>
    <dbReference type="NCBI Taxonomy" id="1897"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Kitasatosporales</taxon>
      <taxon>Streptomycetaceae</taxon>
      <taxon>Streptomyces</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1969" name="Chem. Pharm. Bull." volume="17" first="2188" last="2191">
      <title>The total amino acid sequence of substance A produced by Streptomyces carzinostaticus.</title>
      <authorList>
        <person name="Sato H."/>
        <person name="Tanimura T."/>
        <person name="Nakajima T."/>
        <person name="Tamura Z."/>
      </authorList>
      <dbReference type="PubMed" id="5353565"/>
      <dbReference type="DOI" id="10.1248/cpb.17.2188"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <strain>F41</strain>
    </source>
  </reference>
  <dbReference type="PIR" id="A01811">
    <property type="entry name" value="BXSMAC"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01548"/>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <feature type="chain" id="PRO_0000064601" description="Antibacterial substance A">
    <location>
      <begin position="1"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="65"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="42"/>
      <end position="50"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="5353565"/>
    </source>
  </evidence>
  <sequence length="87" mass="8477" checksum="C9A114BE1534029B" modified="1986-07-21" version="1">AAGNPSETGGAVATYSTAVGSFLDGTVKVVATGGASRVPGNCGTAAVLECDNPESFDGTRAWGDLSADQGTGEDAPPETASLIFAVN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>