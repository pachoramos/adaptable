<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-11-13" modified="2024-10-02" version="24" xmlns="http://uniprot.org/uniprot">
  <accession>M9P2C1</accession>
  <name>TKN1_THECO</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Tachykinin-like peptide</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Tachykinin-Thel</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Theloderma corticale</name>
    <name type="common">Kwangsi warty tree frog</name>
    <name type="synonym">Theloderma kwangsiense</name>
    <dbReference type="NCBI Taxonomy" id="126966"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Rhacophoridae</taxon>
      <taxon>Rhacophorinae</taxon>
      <taxon>Theloderma</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2013" name="Zool. Sci." volume="30" first="529" last="533">
      <title>Purification and characterization of a tachykinin-like peptide from skin secretions of the tree frog, Theloderma kwangsiensis.</title>
      <authorList>
        <person name="Zhang H."/>
        <person name="Wei L."/>
        <person name="Zou C."/>
        <person name="Bai J.J."/>
        <person name="Song Y."/>
        <person name="Liu H."/>
      </authorList>
      <dbReference type="PubMed" id="23829212"/>
      <dbReference type="DOI" id="10.2108/zsj.30.529"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 52-62</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>AMIDATION AT MET-62</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="7">Skin</tissue>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3 5">Tachykinins are active peptides which excite neurons, evoke behavioral responses, are potent vasodilators and secretagogues, and contract (directly or indirectly) many smooth muscles. In vitro, induces contraction of guinea pig ileum smooth muscle in a dose-dependent manner.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1309.6" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the tachykinin family.</text>
  </comment>
  <dbReference type="EMBL" id="JQ867270">
    <property type="protein sequence ID" value="AFX61593.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="M9P2C1"/>
  <dbReference type="SMR" id="M9P2C1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007217">
    <property type="term" value="P:tachykinin receptor signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013055">
    <property type="entry name" value="Tachy_Neuro_lke_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008216">
    <property type="entry name" value="Tachykinin_fam"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11250">
    <property type="entry name" value="TACHYKININ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11250:SF0">
    <property type="entry name" value="TACHYKININ 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01829">
    <property type="entry name" value="PROTACHYKNIN"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00267">
    <property type="entry name" value="TACHYKININ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000424460" evidence="3">
    <location>
      <begin position="20"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000424461" description="Tachykinin-like peptide" evidence="3">
    <location>
      <begin position="52"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000424462" evidence="3">
    <location>
      <begin position="66"/>
      <end position="91"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="64"/>
      <end position="91"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="3">
    <location>
      <position position="62"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="23829212"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="23829212"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="23829212"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="EMBL" id="AFX61593.1"/>
    </source>
  </evidence>
  <sequence length="91" mass="10658" checksum="1BD5DAB2C18CA5A2" modified="2013-06-26" version="1" precursor="true">MKILVAFAVIMLVSAQVLAAEIGLNDEPEWYSDQIQEDLPVFENFLQRIARKPSPDRFYGLMGKRNNGFGQMSRKRSAERNTIHNYERRRK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>