<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-05-24" modified="2023-02-22" version="16" xmlns="http://uniprot.org/uniprot">
  <accession>P84525</accession>
  <name>ERYGN_PLEER</name>
  <protein>
    <recommendedName>
      <fullName>Eryngin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pleurotus eryngii</name>
    <name type="common">Boletus of the steppes</name>
    <dbReference type="NCBI Taxonomy" id="5323"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Basidiomycota</taxon>
      <taxon>Agaricomycotina</taxon>
      <taxon>Agaricomycetes</taxon>
      <taxon>Agaricomycetidae</taxon>
      <taxon>Agaricales</taxon>
      <taxon>Pleurotineae</taxon>
      <taxon>Pleurotaceae</taxon>
      <taxon>Pleurotus</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2004" name="Peptides" volume="25" first="1" last="5">
      <title>Eryngin, a novel antifungal peptide from fruiting bodies of the edible mushroom Pleurotus eryngii.</title>
      <authorList>
        <person name="Wang H."/>
        <person name="Ng T.B."/>
      </authorList>
      <dbReference type="PubMed" id="15003349"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2003.11.014"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue evidence="1">Fruiting body</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has antifungal activity against F.oxysporum and M.arachidicola.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84525"/>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <feature type="peptide" id="PRO_0000045098" description="Eryngin">
    <location>
      <begin position="1"/>
      <end position="25" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="2">
    <location>
      <position position="25"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="15003349"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="15003349"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="25" mass="2724" checksum="ECF6CBF21E768FA3" modified="2005-05-24" version="1" fragment="single">ATRVVYCNRRSGSVVGGDDTVYYEG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>