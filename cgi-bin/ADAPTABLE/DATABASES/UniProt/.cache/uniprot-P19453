<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1991-02-01" modified="2024-11-27" version="72" xmlns="http://uniprot.org/uniprot">
  <accession>P19453</accession>
  <name>SAA3_MESAU</name>
  <protein>
    <recommendedName>
      <fullName>Serum amyloid A-3 protein</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">SAA3</name>
  </gene>
  <organism>
    <name type="scientific">Mesocricetus auratus</name>
    <name type="common">Golden hamster</name>
    <dbReference type="NCBI Taxonomy" id="10036"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Cricetidae</taxon>
      <taxon>Cricetinae</taxon>
      <taxon>Mesocricetus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="Mol. Cell. Biol." volume="10" first="4412" last="4414">
      <title>Serum amyloid A protein-related mRNA expression in herpes simplex virus type 2-transformed hamster cells.</title>
      <authorList>
        <person name="Gervais C."/>
        <person name="Suh M."/>
      </authorList>
      <dbReference type="PubMed" id="2164641"/>
      <dbReference type="DOI" id="10.1128/mcb.10.8.4412-4414.1990"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Major acute phase reactant. Apolipoprotein of the HDL complex. In vitro exhibits antimicrobial activity against Escherichia coli, Streptococcus uberis and Pseudomonas aeruginosa (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the liver; secreted in plasma.</text>
  </comment>
  <comment type="induction">
    <text>Upon cytokine stimulation.</text>
  </comment>
  <comment type="disease">
    <text>Reactive, secondary amyloidosis is characterized by the extracellular accumulation in various tissues of the SAA protein. These deposits are highly insoluble and resistant to proteolysis; they disrupt tissue structure and compromise function.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the SAA family.</text>
  </comment>
  <dbReference type="EMBL" id="M33431">
    <property type="protein sequence ID" value="AAA37098.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="A30248">
    <property type="entry name" value="A30248"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001297487.1">
    <property type="nucleotide sequence ID" value="NM_001310558.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P19453"/>
  <dbReference type="SMR" id="P19453"/>
  <dbReference type="STRING" id="10036.ENSMAUP00000002152"/>
  <dbReference type="GeneID" id="101833293"/>
  <dbReference type="KEGG" id="maua:101833293"/>
  <dbReference type="OrthoDB" id="2958429at2759"/>
  <dbReference type="Proteomes" id="UP000189706">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034364">
    <property type="term" value="C:high-density lipoprotein particle"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006953">
    <property type="term" value="P:acute-phase response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.132.110:FF:000001">
    <property type="entry name" value="Serum amyloid A protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.132.110">
    <property type="entry name" value="Serum amyloid A protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000096">
    <property type="entry name" value="Serum_amyloid_A"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00277">
    <property type="entry name" value="SAA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF002472">
    <property type="entry name" value="Serum_amyloid_A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00306">
    <property type="entry name" value="SERUMAMYLOID"/>
  </dbReference>
  <dbReference type="SMART" id="SM00197">
    <property type="entry name" value="SAA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00992">
    <property type="entry name" value="SAA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0011">Acute phase</keyword>
  <keyword id="KW-0034">Amyloid</keyword>
  <keyword id="KW-0345">HDL</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000031587" description="Serum amyloid A-3 protein" evidence="2">
    <location>
      <begin position="19"/>
      <end position="122"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="100"/>
      <end position="122"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q8SQ28"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="122" mass="13815" checksum="0DD15D7505518D8A" modified="1991-02-01" version="1" precursor="true">MKPFLAIIFCFLILGVDSQRWFQFMKEAGQGSTDMWRAYSDMREANWKNSDKYFHARGNYDAAKRGPGGAWAAKVISDAREGIQRFTGRGAADSRADQFANKWGRSGKDPNHFRPAGLPSKY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>