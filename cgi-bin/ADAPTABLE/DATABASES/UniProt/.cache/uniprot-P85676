<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-07-07" modified="2019-12-11" version="14" xmlns="http://uniprot.org/uniprot">
  <accession>P85676</accession>
  <name>HTF_LUCVE</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Hypertrehalosaemic factor</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Adipokinetic hormone 1</fullName>
      <shortName evidence="4">LucVe-AKH-1</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="1">Hypertrehalosaemic neuropeptide</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Lucihormetica verrucosa</name>
    <name type="common">Cockroach</name>
    <dbReference type="NCBI Taxonomy" id="521514"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Dictyoptera</taxon>
      <taxon>Blattodea</taxon>
      <taxon>Blaberoidea</taxon>
      <taxon>Blaberidae</taxon>
      <taxon>Blaberinae</taxon>
      <taxon>Lucihormetica</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2009" name="BMC Evol. Biol." volume="9" first="50" last="50">
      <title>A proteomic approach for studying insect phylogeny: CAPA peptides of ancient insect taxa (Dictyoptera, Blattoptera) as a test case.</title>
      <authorList>
        <person name="Roth S."/>
        <person name="Fromm B."/>
        <person name="Gaede G."/>
        <person name="Predel R."/>
      </authorList>
      <dbReference type="PubMed" id="19257902"/>
      <dbReference type="DOI" id="10.1186/1471-2148-9-50"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>AMIDATION AT GLY-9</scope>
    <source>
      <tissue evidence="3">Corpora cardiaca</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="5">Hypertrehalosaemic factors are neuropeptides that elevate the level of trehalose in the hemolymph (trehalose is the major carbohydrate in the hemolymph of insects).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the AKH/HRTH/RPCH family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002047">
    <property type="entry name" value="Adipokinetic_hormone_CS"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00256">
    <property type="entry name" value="AKH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000378656" description="Hypertrehalosaemic factor" evidence="3">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="3">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glycine amide" evidence="3">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P67790"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="19257902"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="19257902"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="9" mass="991" checksum="C236786775B9C452" modified="2009-07-07" version="1">QVNFSPGWG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>