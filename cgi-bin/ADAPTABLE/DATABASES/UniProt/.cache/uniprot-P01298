<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="212" xmlns="http://uniprot.org/uniprot">
  <accession>P01298</accession>
  <name>PAHO_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="9">Pancreatic polypeptide prohormone</fullName>
      <shortName>PH</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="10">Pancreatic polypeptide Y</fullName>
    </alternativeName>
    <innName evidence="6">Obinepitide</innName>
    <component>
      <recommendedName>
        <fullName evidence="7">Pancreatic polypeptide</fullName>
        <shortName evidence="8">HPP</shortName>
        <shortName evidence="7">PP</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="7">Pancreatic icosapeptide</fullName>
        <shortName>PI</shortName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name evidence="10" type="primary">PPY</name>
    <name type="synonym">PNP</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1984" name="EMBO J." volume="3" first="909" last="912">
      <title>A cDNA encoding a small common precursor for human pancreatic polypeptide and pancreatic icosapeptide.</title>
      <authorList>
        <person name="Boel E."/>
        <person name="Schwartz T.W."/>
        <person name="Norris K.E."/>
        <person name="Fiil N.P."/>
      </authorList>
      <dbReference type="PubMed" id="6373251"/>
      <dbReference type="DOI" id="10.1002/j.1460-2075.1984.tb01904.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 1)</scope>
    <scope>PROTEOLYTIC PROCESSING</scope>
    <source>
      <tissue>Pancreas</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1984" name="J. Biol. Chem." volume="259" first="14702" last="14705">
      <title>Structure of a precursor to human pancreatic polypeptide.</title>
      <authorList>
        <person name="Leiter A.B."/>
        <person name="Keutmann H.T."/>
        <person name="Goodman R.H."/>
      </authorList>
      <dbReference type="PubMed" id="6094571"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)42659-7"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 1)</scope>
    <source>
      <tissue>Pancreatic tumor</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1985" name="J. Biol. Chem." volume="260" first="13013" last="13017">
      <title>Exons of the human pancreatic polypeptide gene define functional domains of the precursor.</title>
      <authorList>
        <person name="Leiter A.B."/>
        <person name="Montminy M.R."/>
        <person name="Jamieson E."/>
        <person name="Goodman R.H."/>
      </authorList>
      <dbReference type="PubMed" id="2997153"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(17)38830-0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] (ISOFORM 1)</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1986" name="J. Clin. Invest." volume="77" first="1038" last="1041">
      <title>Genes encoding pancreatic polypeptide and neuropeptide Y are on human chromosomes 17 and 7.</title>
      <authorList>
        <person name="Takeuchi T."/>
        <person name="Gumucio D.L."/>
        <person name="Yamada T."/>
        <person name="Meisler M.H."/>
        <person name="Minth C.D."/>
        <person name="Dixon J.E."/>
        <person name="Eddy R.E."/>
        <person name="Shows T.B."/>
      </authorList>
      <dbReference type="PubMed" id="3753985"/>
      <dbReference type="DOI" id="10.1172/jci112357"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 1)</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2006" name="Nature" volume="440" first="1045" last="1049">
      <title>DNA sequence of human chromosome 17 and analysis of rearrangement in the human lineage.</title>
      <authorList>
        <person name="Zody M.C."/>
        <person name="Garber M."/>
        <person name="Adams D.J."/>
        <person name="Sharpe T."/>
        <person name="Harrow J."/>
        <person name="Lupski J.R."/>
        <person name="Nicholson C."/>
        <person name="Searle S.M."/>
        <person name="Wilming L."/>
        <person name="Young S.K."/>
        <person name="Abouelleil A."/>
        <person name="Allen N.R."/>
        <person name="Bi W."/>
        <person name="Bloom T."/>
        <person name="Borowsky M.L."/>
        <person name="Bugalter B.E."/>
        <person name="Butler J."/>
        <person name="Chang J.L."/>
        <person name="Chen C.-K."/>
        <person name="Cook A."/>
        <person name="Corum B."/>
        <person name="Cuomo C.A."/>
        <person name="de Jong P.J."/>
        <person name="DeCaprio D."/>
        <person name="Dewar K."/>
        <person name="FitzGerald M."/>
        <person name="Gilbert J."/>
        <person name="Gibson R."/>
        <person name="Gnerre S."/>
        <person name="Goldstein S."/>
        <person name="Grafham D.V."/>
        <person name="Grocock R."/>
        <person name="Hafez N."/>
        <person name="Hagopian D.S."/>
        <person name="Hart E."/>
        <person name="Norman C.H."/>
        <person name="Humphray S."/>
        <person name="Jaffe D.B."/>
        <person name="Jones M."/>
        <person name="Kamal M."/>
        <person name="Khodiyar V.K."/>
        <person name="LaButti K."/>
        <person name="Laird G."/>
        <person name="Lehoczky J."/>
        <person name="Liu X."/>
        <person name="Lokyitsang T."/>
        <person name="Loveland J."/>
        <person name="Lui A."/>
        <person name="Macdonald P."/>
        <person name="Major J.E."/>
        <person name="Matthews L."/>
        <person name="Mauceli E."/>
        <person name="McCarroll S.A."/>
        <person name="Mihalev A.H."/>
        <person name="Mudge J."/>
        <person name="Nguyen C."/>
        <person name="Nicol R."/>
        <person name="O'Leary S.B."/>
        <person name="Osoegawa K."/>
        <person name="Schwartz D.C."/>
        <person name="Shaw-Smith C."/>
        <person name="Stankiewicz P."/>
        <person name="Steward C."/>
        <person name="Swarbreck D."/>
        <person name="Venkataraman V."/>
        <person name="Whittaker C.A."/>
        <person name="Yang X."/>
        <person name="Zimmer A.R."/>
        <person name="Bradley A."/>
        <person name="Hubbard T."/>
        <person name="Birren B.W."/>
        <person name="Rogers J."/>
        <person name="Lander E.S."/>
        <person name="Nusbaum C."/>
      </authorList>
      <dbReference type="PubMed" id="16625196"/>
      <dbReference type="DOI" id="10.1038/nature04689"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM 1)</scope>
    <source>
      <tissue>Pancreas</tissue>
      <tissue>Spleen</tissue>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2003" name="J. Mol. Endocrinol." volume="31" first="519" last="528">
      <title>Expression profile of mRNAs from human pancreatic islet tumors.</title>
      <authorList>
        <person name="Jin L."/>
        <person name="Wang H."/>
        <person name="Narita T."/>
        <person name="Kikuno R."/>
        <person name="Ohara O."/>
        <person name="Shihara N."/>
        <person name="Nishigori T."/>
        <person name="Horikawa Y."/>
        <person name="Takeda J."/>
      </authorList>
      <dbReference type="PubMed" id="14664712"/>
      <dbReference type="DOI" id="10.1677/jme.0.0310519"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] OF 48-95 (ISOFORM 2)</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="1984" name="Proc. Natl. Acad. Sci. U.S.A." volume="81" first="708" last="712">
      <title>Human pancreatic icosapeptide: isolation, sequence, and immunocytochemical localization of the COOH-terminal fragment of the pancreatic polypeptide precursor.</title>
      <authorList>
        <person name="Schwartz T.W."/>
        <person name="Hansen H.F."/>
        <person name="Haakanson R."/>
        <person name="Sundler F."/>
        <person name="Tager H.S."/>
      </authorList>
      <dbReference type="PubMed" id="6366786"/>
      <dbReference type="DOI" id="10.1073/pnas.81.3.708"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 69-88</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="1976" name="Gut" volume="17" first="940" last="944">
      <title>Distribution and release of human pancreatic polypeptide.</title>
      <authorList>
        <person name="Adrian T.E."/>
        <person name="Bloom S.R."/>
        <person name="Bryant M.G."/>
        <person name="Polak J.M."/>
        <person name="Heitz P.H."/>
        <person name="Barnes A.J."/>
      </authorList>
      <dbReference type="PubMed" id="828120"/>
      <dbReference type="DOI" id="10.1136/gut.17.12.940"/>
    </citation>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="1979" name="Metabolism" volume="28" first="1179" last="1182">
      <title>Effect of endurance exercise training on plasma pancreatic polypeptide concentration during exercise.</title>
      <authorList>
        <person name="Gingerich R.L."/>
        <person name="Hickson R.C."/>
        <person name="Hagberg J.M."/>
        <person name="Winder W.W."/>
      </authorList>
      <dbReference type="PubMed" id="514078"/>
      <dbReference type="DOI" id="10.1016/0026-0495(79)90129-x"/>
    </citation>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="1995" name="J. Biol. Chem." volume="270" first="26762" last="26765">
      <title>Cloning and functional expression of a human Y4 subtype receptor for pancreatic polypeptide, neuropeptide Y, and peptide YY.</title>
      <authorList>
        <person name="Bard J.A."/>
        <person name="Walker M.W."/>
        <person name="Branchek T.A."/>
        <person name="Weinshank R.L."/>
      </authorList>
      <dbReference type="PubMed" id="7592911"/>
      <dbReference type="DOI" id="10.1074/jbc.270.45.26762"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Placenta</tissue>
    </source>
  </reference>
  <reference key="12">
    <citation type="journal article" date="1995" name="J. Biol. Chem." volume="270" first="29123" last="29128">
      <title>Cloning of a human receptor of the NPY receptor family with high affinity for pancreatic polypeptide and peptide YY.</title>
      <authorList>
        <person name="Lundell I."/>
        <person name="Blomqvist A.G."/>
        <person name="Berglund M.M."/>
        <person name="Schober D.A."/>
        <person name="Johnson D."/>
        <person name="Statnick M.A."/>
        <person name="Gadski R.A."/>
        <person name="Gehlert D.R."/>
        <person name="Larhammar D."/>
      </authorList>
      <dbReference type="PubMed" id="7493937"/>
      <dbReference type="DOI" id="10.1074/jbc.270.49.29123"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2014" name="Drug Discov. Today" volume="19" first="845" last="858">
      <title>Treating obesity: is it all in the gut?</title>
      <authorList>
        <person name="Davenport R.J."/>
        <person name="Wright S."/>
      </authorList>
      <dbReference type="PubMed" id="24291217"/>
      <dbReference type="DOI" id="10.1016/j.drudis.2013.10.025"/>
    </citation>
    <scope>NOMENCLATURE</scope>
    <scope>PHARMACEUTICAL</scope>
  </reference>
  <reference key="14">
    <citation type="journal article" date="2005" name="Biochemistry" volume="44" first="9255" last="9264">
      <title>Strongly altered receptor binding properties in PP and NPY chimeras are accompanied by changes in structure and membrane binding.</title>
      <authorList>
        <person name="Lerch M."/>
        <person name="Kamimori H."/>
        <person name="Folkers G."/>
        <person name="Aguilar M.-I."/>
        <person name="Beck-Sickinger A.G."/>
        <person name="Zerbe O."/>
      </authorList>
      <dbReference type="PubMed" id="15966750"/>
      <dbReference type="DOI" id="10.1021/bi0501232"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 30-54</scope>
  </reference>
  <comment type="function">
    <molecule>Pancreatic polypeptide</molecule>
    <text evidence="3 4">Hormone secreted by pancreatic cells that acts as a regulator of pancreatic and gastrointestinal functions probably by signaling through the G protein-coupled receptor NPY4R2.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-12121422">
      <id>P01298</id>
    </interactant>
    <interactant intactId="EBI-3867333">
      <id>A8MQ03</id>
      <label>CYSRT1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3 4 5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>P01298-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>P01298-2</id>
      <name>2</name>
      <sequence type="described" ref="VSP_057852"/>
    </isoform>
  </comment>
  <comment type="induction">
    <text evidence="2 5">Released in circulation upon food intake (PubMed:828120). Also up-regulated by exercise (PubMed:514078).</text>
  </comment>
  <comment type="pharmaceutical">
    <text evidence="6">Obinepitide is under clinical trial by 7TM Pharma to be used for the treatment of obesity. Obinepitide is derived from pancreatic hormone residues 30 to 65 with a Gln at position 63.</text>
  </comment>
  <comment type="similarity">
    <text evidence="9">Belongs to the NPY family.</text>
  </comment>
  <dbReference type="EMBL" id="X00491">
    <property type="protein sequence ID" value="CAA25161.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M11726">
    <property type="protein sequence ID" value="AAA60156.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M15788">
    <property type="protein sequence ID" value="AAA60161.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC007993">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC032225">
    <property type="protein sequence ID" value="AAH32225.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC040033">
    <property type="protein sequence ID" value="AAH40033.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BP385720">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS11472.1">
    <molecule id="P01298-1"/>
  </dbReference>
  <dbReference type="PIR" id="A92498">
    <property type="entry name" value="PCHU"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_002713.1">
    <molecule id="P01298-1"/>
    <property type="nucleotide sequence ID" value="NM_002722.4"/>
  </dbReference>
  <dbReference type="PDB" id="1TZ4">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=48-52"/>
  </dbReference>
  <dbReference type="PDB" id="1TZ5">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=30-47, A=53-65"/>
  </dbReference>
  <dbReference type="PDB" id="7X9C">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.00 A"/>
    <property type="chains" value="P=30-65"/>
  </dbReference>
  <dbReference type="PDBsum" id="1TZ4"/>
  <dbReference type="PDBsum" id="1TZ5"/>
  <dbReference type="PDBsum" id="7X9C"/>
  <dbReference type="AlphaFoldDB" id="P01298"/>
  <dbReference type="SMR" id="P01298"/>
  <dbReference type="BioGRID" id="111531">
    <property type="interactions" value="13"/>
  </dbReference>
  <dbReference type="IntAct" id="P01298">
    <property type="interactions" value="9"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000466009"/>
  <dbReference type="SwissPalm" id="P01298"/>
  <dbReference type="BioMuta" id="PPY"/>
  <dbReference type="MassIVE" id="P01298"/>
  <dbReference type="PaxDb" id="9606-ENSP00000466009"/>
  <dbReference type="PeptideAtlas" id="P01298"/>
  <dbReference type="ProteomicsDB" id="51372"/>
  <dbReference type="ABCD" id="P01298">
    <property type="antibodies" value="1 sequenced antibody"/>
  </dbReference>
  <dbReference type="Antibodypedia" id="29637">
    <property type="antibodies" value="416 antibodies from 35 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="5539"/>
  <dbReference type="Ensembl" id="ENST00000225992.8">
    <molecule id="P01298-1"/>
    <property type="protein sequence ID" value="ENSP00000225992.3"/>
    <property type="gene ID" value="ENSG00000108849.8"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000591228.4">
    <molecule id="P01298-1"/>
    <property type="protein sequence ID" value="ENSP00000466009.1"/>
    <property type="gene ID" value="ENSG00000108849.8"/>
  </dbReference>
  <dbReference type="GeneID" id="5539"/>
  <dbReference type="KEGG" id="hsa:5539"/>
  <dbReference type="MANE-Select" id="ENST00000225992.8">
    <property type="protein sequence ID" value="ENSP00000225992.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_002722.5"/>
    <property type="RefSeq protein sequence ID" value="NP_002713.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc002iep.4">
    <molecule id="P01298-1"/>
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:9327"/>
  <dbReference type="CTD" id="5539"/>
  <dbReference type="DisGeNET" id="5539"/>
  <dbReference type="GeneCards" id="PPY"/>
  <dbReference type="HGNC" id="HGNC:9327">
    <property type="gene designation" value="PPY"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000108849">
    <property type="expression patterns" value="Tissue enriched (pancreas)"/>
  </dbReference>
  <dbReference type="MIM" id="167780">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P01298"/>
  <dbReference type="OpenTargets" id="ENSG00000108849"/>
  <dbReference type="PharmGKB" id="PA33690"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000108849"/>
  <dbReference type="eggNOG" id="ENOG502TD4B">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00530000064295"/>
  <dbReference type="InParanoid" id="P01298"/>
  <dbReference type="OMA" id="MAATRRC"/>
  <dbReference type="OrthoDB" id="4638016at2759"/>
  <dbReference type="PhylomeDB" id="P01298"/>
  <dbReference type="TreeFam" id="TF332778"/>
  <dbReference type="PathwayCommons" id="P01298"/>
  <dbReference type="Reactome" id="R-HSA-375276">
    <property type="pathway name" value="Peptide ligand-binding receptors"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="SignaLink" id="P01298"/>
  <dbReference type="SIGNOR" id="P01298"/>
  <dbReference type="BioGRID-ORCS" id="5539">
    <property type="hits" value="292 hits in 1147 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="PPY">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P01298"/>
  <dbReference type="GenomeRNAi" id="5539"/>
  <dbReference type="Pharos" id="P01298">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P01298"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 17"/>
  </dbReference>
  <dbReference type="RNAct" id="P01298">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000108849">
    <property type="expression patterns" value="Expressed in type B pancreatic cell and 72 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P01298">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001664">
    <property type="term" value="F:G protein-coupled receptor binding"/>
    <property type="evidence" value="ECO:0000353"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005184">
    <property type="term" value="F:neuropeptide hormone activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031841">
    <property type="term" value="F:neuropeptide Y receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009306">
    <property type="term" value="P:protein secretion"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="CDD" id="cd00126">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.900">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001955">
    <property type="entry name" value="Pancreatic_hormone-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR020392">
    <property type="entry name" value="Pancreatic_hormone-like_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533">
    <property type="entry name" value="NEUROPEPTIDE Y/PANCREATIC HORMONE/PEPTIDE YY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10533:SF2">
    <property type="entry name" value="PANCREATIC PROHORMONE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00159">
    <property type="entry name" value="Hormone_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00278">
    <property type="entry name" value="PANCHORMONE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00309">
    <property type="entry name" value="PAH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00265">
    <property type="entry name" value="PANCREATIC_HORMONE_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50276">
    <property type="entry name" value="PANCREATIC_HORMONE_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0582">Pharmaceutical</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000025365" description="Pancreatic polypeptide">
    <location>
      <begin position="30"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000025366" description="Pancreatic icosapeptide">
    <location>
      <begin position="69"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000025367">
    <location>
      <begin position="89"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="modified residue" description="Tyrosine amide" evidence="1">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_057852" description="In isoform 2." evidence="9">
    <original>R</original>
    <variation>RPRCVPQLGREIPAPGTLGPLHIPGHTLSPAPAPAPSRPALGKTGHLCSTGLDQCALGKMVPTGRYETGGLAPGHSACPCCLFP</variation>
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_050615" description="In dbSNP:rs7215698.">
    <original>E</original>
    <variation>G</variation>
    <location>
      <position position="78"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 4; AAA60161." evidence="9" ref="4">
    <original>V</original>
    <variation>I</variation>
    <location>
      <position position="86"/>
    </location>
  </feature>
  <feature type="helix" evidence="11">
    <location>
      <begin position="43"/>
      <end position="60"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="514078"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7493937"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="7592911"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="828120"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="24291217"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="6373251"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="828120"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="9"/>
  <evidence type="ECO:0000312" key="10">
    <source>
      <dbReference type="HGNC" id="HGNC:9327"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="11">
    <source>
      <dbReference type="PDB" id="7X9C"/>
    </source>
  </evidence>
  <sequence length="95" mass="10445" checksum="44F0265092F9C4A0" modified="1986-07-21" version="1" precursor="true">MAAARLCLSLLLLSTCVALLLQPLLGAQGAPLEPVYPGDNATPEQMAQYAADLRRYINMLTRPRYGKRHKEDTLAFSEWGSPHAAVPRELSPLDL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>