<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-10-05" modified="2023-09-13" version="16" xmlns="http://uniprot.org/uniprot">
  <accession>P86711</accession>
  <name>OCE1_LEPKN</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Ocellatin-K1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Leptodactylus knudseni</name>
    <name type="common">Knudsen's thin-toed frog</name>
    <name type="synonym">Amazonian toad-frog</name>
    <dbReference type="NCBI Taxonomy" id="326593"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Leptodactylidae</taxon>
      <taxon>Leptodactylinae</taxon>
      <taxon>Leptodactylus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2010-07" db="UniProtKB">
      <title>Identification of peptides from Amazonian Leptodactylus knudseni skin secretion by MALDI TOF/TOF.</title>
      <authorList>
        <person name="Cardozo-Filho J.L."/>
        <person name="Soares A.A."/>
        <person name="Bloch C. Jr."/>
        <person name="Silva L.P."/>
        <person name="Stabeli R.G."/>
        <person name="Calderon L.A."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT ILE-25</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Has hemolytic and antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2548.0" error="0.01" method="MALDI" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Ocellatin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P86711"/>
  <dbReference type="BMRB" id="P86711"/>
  <dbReference type="SMR" id="P86711"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019836">
    <property type="term" value="P:hemolysis by symbiont of host erythrocytes"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012518">
    <property type="entry name" value="Antimicrobial15"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08110">
    <property type="entry name" value="Antimicrobial15"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000398811" description="Ocellatin-K1" evidence="2">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="modified residue" description="Isoleucine amide" evidence="2">
    <location>
      <position position="25"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P83951"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="25" mass="2549" checksum="3B32EAC8DA060755" modified="2010-10-05" version="1">GVVDILKGAAKDLAGHLASKVMNKI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>