<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-08-01" modified="2024-11-27" version="131" xmlns="http://uniprot.org/uniprot">
  <accession>P17728</accession>
  <name>SCXA_LEIHE</name>
  <protein>
    <recommendedName>
      <fullName>Alpha-insect toxin LqhaIT</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Lqh-alpha-IT</fullName>
      <shortName>Alpha-IT</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Leiurus hebraeus</name>
    <name type="common">Hebrew deathstalker scorpion</name>
    <name type="synonym">Leiurus quinquestriatus hebraeus</name>
    <dbReference type="NCBI Taxonomy" id="2899558"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Leiurus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="Toxicon" volume="29" first="1270" last="1272">
      <title>Nucleotide sequence and structure analysis of a cDNA encoding an alpha insect toxin from the scorpion Leiurus quinquestriatus hebraeus.</title>
      <authorList>
        <person name="Gurevitz M."/>
        <person name="Urbach D."/>
        <person name="Zlotkin E."/>
        <person name="Zilberberg N."/>
      </authorList>
      <dbReference type="PubMed" id="1801321"/>
      <dbReference type="DOI" id="10.1016/0041-0101(91)90200-b"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1990" name="Biochemistry" volume="29" first="5941" last="5947">
      <title>A scorpion venom neurotoxin paralytic to insects that affects sodium current inactivation: purification, primary structure, and mode of action.</title>
      <authorList>
        <person name="Eitan M."/>
        <person name="Fowler E."/>
        <person name="Herrmann R."/>
        <person name="Duval A."/>
        <person name="Pelhate M."/>
        <person name="Zlotkin E."/>
      </authorList>
      <dbReference type="PubMed" id="2383565"/>
      <dbReference type="DOI" id="10.1021/bi00477a009"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 20-85</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1996" name="Biochemistry" volume="35" first="10215" last="10222">
      <title>Functional expression and genetic alteration of an alpha scorpion neurotoxin.</title>
      <authorList>
        <person name="Zilberberg N."/>
        <person name="Gordon D."/>
        <person name="Pelhate M."/>
        <person name="Adams M.E."/>
        <person name="Norris T.M."/>
        <person name="Zlotkin E."/>
        <person name="Gurevitz M."/>
      </authorList>
      <dbReference type="PubMed" id="8756487"/>
      <dbReference type="DOI" id="10.1021/bi9528309"/>
    </citation>
    <scope>MUTAGENESIS OF TYR-68; ALA-69 AND ASN-73</scope>
    <scope>TOXIC DOSE</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1997" name="J. Biol. Chem." volume="272" first="14810" last="14816">
      <title>Identification of structural elements of a scorpion alpha-neurotoxin important for receptor site recognition.</title>
      <authorList>
        <person name="Zilberberg N."/>
        <person name="Froy O."/>
        <person name="Loret E."/>
        <person name="Cestele S."/>
        <person name="Arad D."/>
        <person name="Gordon D."/>
        <person name="Gurevitz M."/>
      </authorList>
      <dbReference type="PubMed" id="9169449"/>
      <dbReference type="DOI" id="10.1074/jbc.272.23.14810"/>
    </citation>
    <scope>MUTAGENESIS OF LYS-27; 27-LYS--TYR-29; 27-LYS--GLU-34; TYR-29; GLU-34; PHE-36; ARG-37; ASP-38; 37-ARG-ASP-38; 59-GLY-LYS-60; LYS-81 AND ARG-84</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2000" name="Pflugers Arch." volume="439" first="423" last="432">
      <title>Modulation of cloned skeletal muscle sodium channels by the scorpion toxins Lqh II, Lqh III, and Lqh alphaIT.</title>
      <authorList>
        <person name="Chen H."/>
        <person name="Gordon D."/>
        <person name="Heinemann S.H."/>
      </authorList>
      <dbReference type="PubMed" id="10678738"/>
      <dbReference type="DOI" id="10.1007/s004249900181"/>
    </citation>
    <scope>ELECTROPHYSIOLOGICAL CHARACTERIZATION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2006" name="Toxicon" volume="47" first="628" last="639">
      <title>Moving pieces in a taxonomic puzzle: venom 2D-LC/MS and data clustering analyses to infer phylogenetic relationships in some scorpions from the Buthidae family (Scorpiones).</title>
      <authorList>
        <person name="Nascimento D.G."/>
        <person name="Rates B."/>
        <person name="Santos D.M."/>
        <person name="Verano-Braga T."/>
        <person name="Barbosa-Silva A."/>
        <person name="Dutra A.A.A."/>
        <person name="Biondi I."/>
        <person name="Martin-Eauclaire M.-F."/>
        <person name="De Lima M.E."/>
        <person name="Pimenta A.M.C."/>
      </authorList>
      <dbReference type="PubMed" id="16551474"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2006.01.015"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1997" name="Biochemistry" volume="36" first="2414" last="2424">
      <title>Solution structures of a highly insecticidal recombinant scorpion alpha-toxin and a mutant with increased activity.</title>
      <authorList>
        <person name="Tugarinov V."/>
        <person name="Kustanovich I."/>
        <person name="Zilberberg N."/>
        <person name="Gurevitz M."/>
        <person name="Anglister J."/>
      </authorList>
      <dbReference type="PubMed" id="9054546"/>
      <dbReference type="DOI" id="10.1021/bi961497l"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 20-82</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text>Alpha toxins bind voltage-independently at site-3 of sodium channels (Nav) and inhibit the inactivation of the activated channels, thereby blocking neuronal transmission. The dissociation is voltage-dependent. This toxin is active on insects. It is also highly toxic to crustaceans and has a measurable but low toxicity to mice.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="6">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">LD(50) is 250 ug/kg by subcutaneous injection to mice.</text>
  </comment>
  <comment type="miscellaneous">
    <text>The binding sites for this contractive toxin differ from those shared by the excitatory and depressant insect toxins.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Alpha subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="6">The mutagenesis studies described in PubMed:8756487 and PubMed:9169449 are made with the sequence from PubMed:2383565 (see the sequence conflict in position 83-85).</text>
  </comment>
  <dbReference type="PIR" id="A39306">
    <property type="entry name" value="A39306"/>
  </dbReference>
  <dbReference type="PDB" id="1LQH">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=20-82"/>
  </dbReference>
  <dbReference type="PDB" id="1LQI">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=20-82"/>
  </dbReference>
  <dbReference type="PDB" id="2ASC">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.10 A"/>
    <property type="chains" value="A=20-82"/>
  </dbReference>
  <dbReference type="PDB" id="2ATB">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="A/B=20-82"/>
  </dbReference>
  <dbReference type="PDB" id="2YEO">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.08 A"/>
    <property type="chains" value="A=20-82"/>
  </dbReference>
  <dbReference type="PDB" id="8VQC">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.90 A"/>
    <property type="chains" value="H=20-82"/>
  </dbReference>
  <dbReference type="PDBsum" id="1LQH"/>
  <dbReference type="PDBsum" id="1LQI"/>
  <dbReference type="PDBsum" id="2ASC"/>
  <dbReference type="PDBsum" id="2ATB"/>
  <dbReference type="PDBsum" id="2YEO"/>
  <dbReference type="PDBsum" id="8VQC"/>
  <dbReference type="AlphaFoldDB" id="P17728"/>
  <dbReference type="SMR" id="P17728"/>
  <dbReference type="EvolutionaryTrace" id="P17728"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000035296" description="Alpha-insect toxin LqhaIT">
    <location>
      <begin position="20"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="21"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 4">
    <location>
      <begin position="31"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 4">
    <location>
      <begin position="35"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 4">
    <location>
      <begin position="41"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 4">
    <location>
      <begin position="45"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="74% loss of toxicity and 39.4-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="27"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="99.4% loss of toxicity and 1611-fold decrease in affinity to cockroach neuronal receptor. 95% loss of toxicity and 71.4-fold decrease in affinity to cockroach neuronal receptor; when associated with A-34. 99.6% loss of toxicity and 21303-fold decrease in affinity to cockroach neuronal receptor; when associated with D-28 and V-29." evidence="5">
    <original>K</original>
    <variation>D</variation>
    <location>
      <position position="27"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="99.6% loss of toxicity and 21303-fold decrease in affinity to cockroach neuronal receptor; when associated with D-27 and V-29.">
    <original>N</original>
    <variation>D</variation>
    <location>
      <position position="28"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="90% loss of toxicity and 114.3-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>Y</original>
    <variation>S</variation>
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="99.6% loss of toxicity and 21303-fold decrease in affinity to cockroach neuronal receptor; when associated with D-27 and D-28." evidence="5">
    <original>Y</original>
    <variation>V</variation>
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="57% loss of toxicity and 3.7-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>Y</original>
    <variation>W</variation>
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="77% loss of toxicity and 0.6-fold decrease in affinity to cockroach neuronal receptor; 95% loss of toxicity and 71.4-fold decrease in affinity to cockroach neuronal receptor; when associated with D-27." evidence="5">
    <original>E</original>
    <variation>A</variation>
    <location>
      <position position="34"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="86% loss of toxicity and 160-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>F</original>
    <variation>G</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="78% loss of toxicity and 231.4-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="59% loss of toxicity and 0.46-fold decrease in affinity to cockroach neuronal receptor; when associated with N-38." evidence="5">
    <original>R</original>
    <variation>K</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="75% loss of toxicity and 6-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>D</original>
    <variation>H</variation>
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="15% increase in toxicity and 1.2-fold decrease in affinity to cockroach neuronal receptor; 59% loss of toxicity and 0.46-fold decrease in affinity to cockroach neuronal receptor; when associated with K-37." evidence="5">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="36% loss of toxicity and 25.7-fold decrease in affinity to cockroach neuronal receptor; when associated with P-60.">
    <original>G</original>
    <variation>S</variation>
    <location>
      <position position="59"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="36% loss of toxicity and 25.7-fold decrease in affinity to cockroach neuronal receptor; when associated with S-59.">
    <original>K</original>
    <variation>P</variation>
    <location>
      <position position="60"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="20% loss of toxicity to mice and 1.18-fold decrease in affinity to locust synaptosomal membranes; when associated with K-69. 540% loss of toxicity to mice and 1.08-fold decrease in affinity to locust synaptosomal membranes; when associated with K-69 and K-73." evidence="3">
    <original>Y</original>
    <variation>I</variation>
    <location>
      <position position="68"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="20% loss of toxicity to mice and 1.18-fold decrease in affinity to locust synaptosomal membranes; when associated with I-68. 540% loss of toxicity to mice and 1.08-fold decrease in affinity to locust synaptosomal membranes; when associated with I-68 and K-73." evidence="3">
    <original>A</original>
    <variation>K</variation>
    <location>
      <position position="69"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="540% loss of toxicity to mice and 1.08-fold decrease in affinity to locust synaptosomal membranes; when associated with I-68 and K-69." evidence="3">
    <original>N</original>
    <variation>K</variation>
    <location>
      <position position="73"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="81% loss of toxicity and 220-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>K</original>
    <variation>L</variation>
    <location>
      <position position="81"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="47% decrease in toxicity and 54.3-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="84"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="95% decrease in toxicity and 2394-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>R</original>
    <variation>D</variation>
    <location>
      <position position="84"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="220% decrease in toxicity and 0.3-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>R</original>
    <variation>H</variation>
    <location>
      <position position="84"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="6% increase in toxicity and 58.6-fold decrease in affinity to cockroach neuronal receptor." evidence="5">
    <original>R</original>
    <variation>N</variation>
    <location>
      <position position="84"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="6" ref="2">
    <original>HRK</original>
    <variation>R</variation>
    <location>
      <begin position="83"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="20"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="turn" evidence="7">
    <location>
      <begin position="28"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="helix" evidence="7">
    <location>
      <begin position="38"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="51"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="strand" evidence="7">
    <location>
      <begin position="62"/>
      <end position="71"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="2383565"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="8756487"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="9054546"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="9169449"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0007829" key="7">
    <source>
      <dbReference type="PDB" id="2YEO"/>
    </source>
  </evidence>
  <sequence length="85" mass="9571" checksum="3338BED532D53FA7" modified="1992-03-01" version="2" precursor="true">MNHLVMISLALLLLLGVESVRDAYIAKNYNCVYECFRDAYCNELCTKNGASSGYCQWAGKYGNACWCYALPDNVPIRVPGKCHRK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>