<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-04-05" modified="2024-11-27" version="112" xmlns="http://uniprot.org/uniprot">
  <accession>O50461</accession>
  <accession>L0T6B0</accession>
  <name>RELE_MYCTU</name>
  <protein>
    <recommendedName>
      <fullName>Toxin RelE</fullName>
      <ecNumber>3.1.-.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Putative endoribonuclease RelE</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">relE</name>
    <name type="synonym">relE1</name>
    <name type="ordered locus">Rv1246c</name>
  </gene>
  <organism>
    <name type="scientific">Mycobacterium tuberculosis (strain ATCC 25618 / H37Rv)</name>
    <dbReference type="NCBI Taxonomy" id="83332"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Mycobacteriales</taxon>
      <taxon>Mycobacteriaceae</taxon>
      <taxon>Mycobacterium</taxon>
      <taxon>Mycobacterium tuberculosis complex</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Nature" volume="393" first="537" last="544">
      <title>Deciphering the biology of Mycobacterium tuberculosis from the complete genome sequence.</title>
      <authorList>
        <person name="Cole S.T."/>
        <person name="Brosch R."/>
        <person name="Parkhill J."/>
        <person name="Garnier T."/>
        <person name="Churcher C.M."/>
        <person name="Harris D.E."/>
        <person name="Gordon S.V."/>
        <person name="Eiglmeier K."/>
        <person name="Gas S."/>
        <person name="Barry C.E. III"/>
        <person name="Tekaia F."/>
        <person name="Badcock K."/>
        <person name="Basham D."/>
        <person name="Brown D."/>
        <person name="Chillingworth T."/>
        <person name="Connor R."/>
        <person name="Davies R.M."/>
        <person name="Devlin K."/>
        <person name="Feltwell T."/>
        <person name="Gentles S."/>
        <person name="Hamlin N."/>
        <person name="Holroyd S."/>
        <person name="Hornsby T."/>
        <person name="Jagels K."/>
        <person name="Krogh A."/>
        <person name="McLean J."/>
        <person name="Moule S."/>
        <person name="Murphy L.D."/>
        <person name="Oliver S."/>
        <person name="Osborne J."/>
        <person name="Quail M.A."/>
        <person name="Rajandream M.A."/>
        <person name="Rogers J."/>
        <person name="Rutter S."/>
        <person name="Seeger K."/>
        <person name="Skelton S."/>
        <person name="Squares S."/>
        <person name="Squares R."/>
        <person name="Sulston J.E."/>
        <person name="Taylor K."/>
        <person name="Whitehead S."/>
        <person name="Barrell B.G."/>
      </authorList>
      <dbReference type="PubMed" id="9634230"/>
      <dbReference type="DOI" id="10.1038/31159"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 25618 / H37Rv</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Nucleic Acids Res." volume="33" first="966" last="976">
      <title>Toxin-antitoxin loci are highly abundant in free-living but lost from host-associated prokaryotes.</title>
      <authorList>
        <person name="Pandey D.P."/>
        <person name="Gerdes K."/>
      </authorList>
      <dbReference type="PubMed" id="15718296"/>
      <dbReference type="DOI" id="10.1093/nar/gki201"/>
    </citation>
    <scope>POSSIBLE FUNCTION</scope>
    <source>
      <strain>ATCC 25618 / H37Rv</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2009" name="J. Bacteriol." volume="191" first="1618" last="1630">
      <title>Three Mycobacterium tuberculosis Rel toxin-antitoxin modules inhibit mycobacterial growth and are expressed in infected human macrophages.</title>
      <authorList>
        <person name="Korch S.B."/>
        <person name="Contreras H."/>
        <person name="Clark-Curtiss J.E."/>
      </authorList>
      <dbReference type="PubMed" id="19114484"/>
      <dbReference type="DOI" id="10.1128/jb.01318-08"/>
    </citation>
    <scope>FUNCTION AS A TOXIN</scope>
    <scope>FUNCTION AS A TRANSCRIPTIONAL REGULATOR</scope>
    <scope>EXPRESSION IN M.SMEGMATIS</scope>
    <scope>SUBUNIT</scope>
    <scope>INDUCTION</scope>
    <scope>OPERON STRUCTURE</scope>
    <source>
      <strain>ATCC 25618 / H37Rv</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2009" name="PLoS Genet." volume="5" first="E1000767" last="E1000767">
      <title>Comprehensive functional analysis of Mycobacterium tuberculosis toxin-antitoxin systems: implications for pathogenesis, stress responses, and evolution.</title>
      <authorList>
        <person name="Ramage H.R."/>
        <person name="Connolly L.E."/>
        <person name="Cox J.S."/>
      </authorList>
      <dbReference type="PubMed" id="20011113"/>
      <dbReference type="DOI" id="10.1371/journal.pgen.1000767"/>
    </citation>
    <scope>EXPRESSION IN M.SMEGMATIS</scope>
    <scope>FUNCTION AS A TOXIN</scope>
    <source>
      <strain>ATCC 35801 / TMC 107 / Erdman</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2010" name="J. Bacteriol." volume="192" first="1279" last="1291">
      <title>The three RelE homologs of Mycobacterium tuberculosis have individual, drug-specific effects on bacterial antibiotic tolerance.</title>
      <authorList>
        <person name="Singh R."/>
        <person name="Barry C.E. III"/>
        <person name="Boshoff H.I."/>
      </authorList>
      <dbReference type="PubMed" id="20061486"/>
      <dbReference type="DOI" id="10.1128/jb.01285-09"/>
    </citation>
    <scope>FUNCTION IN M.TUBERCULOSIS</scope>
    <scope>INDUCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>ATCC 27294 / TMC 102 / H37Rv</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2010" name="PLoS ONE" volume="5" first="E10672" last="E10672">
      <title>Characterization of the interaction and cross-regulation of three Mycobacterium tuberculosis RelBE modules.</title>
      <authorList>
        <person name="Yang M."/>
        <person name="Gao C."/>
        <person name="Wang Y."/>
        <person name="Zhang H."/>
        <person name="He Z.G."/>
      </authorList>
      <dbReference type="PubMed" id="20498855"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0010672"/>
    </citation>
    <scope>FUNCTION AS A TOXIN</scope>
    <scope>SUBUNIT</scope>
    <scope>INTERACTION WITH RELF</scope>
    <source>
      <strain>ATCC 25618 / H37Rv</strain>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2011" name="Mol. Cell. Proteomics" volume="10" first="M111.011627" last="M111.011627">
      <title>Proteogenomic analysis of Mycobacterium tuberculosis by high resolution mass spectrometry.</title>
      <authorList>
        <person name="Kelkar D.S."/>
        <person name="Kumar D."/>
        <person name="Kumar P."/>
        <person name="Balakrishnan L."/>
        <person name="Muthusamy B."/>
        <person name="Yadav A.K."/>
        <person name="Shrivastava P."/>
        <person name="Marimuthu A."/>
        <person name="Anand S."/>
        <person name="Sundaram H."/>
        <person name="Kingsbury R."/>
        <person name="Harsha H.C."/>
        <person name="Nair B."/>
        <person name="Prasad T.S."/>
        <person name="Chauhan D.S."/>
        <person name="Katoch K."/>
        <person name="Katoch V.M."/>
        <person name="Kumar P."/>
        <person name="Chaerkady R."/>
        <person name="Ramachandran S."/>
        <person name="Dash D."/>
        <person name="Pandey A."/>
      </authorList>
      <dbReference type="PubMed" id="21969609"/>
      <dbReference type="DOI" id="10.1074/mcp.m111.011445"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
    <source>
      <strain>ATCC 25618 / H37Rv</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2 3 4 5">Toxic component of a type II toxin-antitoxin (TA) system. Has RNase activity (By similarity). Overexpression in M.tuberculosis or M.smegmatis inhibits colony formation in a bacteriostatic rather than bacteriocidal fashion. Its toxic effect is neutralized by coexpression with cognate antitoxin RelB (shown only for M.smegmatis).</text>
  </comment>
  <comment type="function">
    <text>In combination with RelB represses its own promoter. Has been seen to bind DNA in complex with cognate antitoxin RelB but not alone.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2 5">Interacts with cognate antitoxin RelB, which neutralizes the toxin. Also interacts with non-cognate antitoxin RelF in vitro, in M.smegmatis coexpression of these 2 genes increases the toxicity of RelE.</text>
  </comment>
  <comment type="induction">
    <text evidence="2 4">Expressed in log phase cells (at protein level). Induced by rifampicin treatment. Expressed in human macrophages 110 hours after infection. Induced in the lungs of mice infected for 4 weeks. A member of the relBE operon.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="4">No visible phenotype in culture or upon infection of mice.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the RelE toxin family.</text>
  </comment>
  <dbReference type="EC" id="3.1.-.-"/>
  <dbReference type="EMBL" id="AL123456">
    <property type="protein sequence ID" value="CCP44002.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="E70953">
    <property type="entry name" value="E70953"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_215762.1">
    <property type="nucleotide sequence ID" value="NC_000962.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003898789.1">
    <property type="nucleotide sequence ID" value="NZ_NVQJ01000049.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="O50461"/>
  <dbReference type="SMR" id="O50461"/>
  <dbReference type="STRING" id="83332.Rv1246c"/>
  <dbReference type="PaxDb" id="83332-Rv1246c"/>
  <dbReference type="GeneID" id="45425216"/>
  <dbReference type="GeneID" id="887099"/>
  <dbReference type="KEGG" id="mtu:Rv1246c"/>
  <dbReference type="KEGG" id="mtv:RVBD_1246c"/>
  <dbReference type="TubercuList" id="Rv1246c"/>
  <dbReference type="eggNOG" id="COG2026">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="InParanoid" id="O50461"/>
  <dbReference type="OrthoDB" id="5326046at2"/>
  <dbReference type="PhylomeDB" id="O50461"/>
  <dbReference type="Proteomes" id="UP000001584">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004519">
    <property type="term" value="F:endonuclease activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MTBBASE"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045926">
    <property type="term" value="P:negative regulation of growth"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MTBBASE"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006401">
    <property type="term" value="P:RNA catabolic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="MTBBASE"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.2310.20:FF:000004">
    <property type="entry name" value="Toxin RelE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.2310.20">
    <property type="entry name" value="RelE-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007712">
    <property type="entry name" value="RelE/ParE_toxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035093">
    <property type="entry name" value="RelE/ParE_toxin_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35601">
    <property type="entry name" value="TOXIN RELE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR35601:SF1">
    <property type="entry name" value="TOXIN RELE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05016">
    <property type="entry name" value="ParE_toxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF143011">
    <property type="entry name" value="RelE-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0678">Repressor</keyword>
  <keyword id="KW-1277">Toxin-antitoxin system</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_0000406199" description="Toxin RelE">
    <location>
      <begin position="1"/>
      <end position="97"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19114484"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="20011113"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="20061486"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="20498855"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="97" mass="11064" checksum="706298CC2CAB3434" modified="1998-06-01" version="1">MSDDHPYHVAITATAARDLQRLPEKIAAACVEFVFGPLLNNPHRLGKPLRNDLEGLHSARRGDYRVVYAIDDGHHRVEIIHIARRSASYRMNPCRPR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>