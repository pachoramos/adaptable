<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-02-10" modified="2024-05-29" version="37" xmlns="http://uniprot.org/uniprot">
  <accession>B6ULW5</accession>
  <accession>P86032</accession>
  <name>BTDB_PAPAN</name>
  <protein>
    <recommendedName>
      <fullName>Theta defensin subunit B</fullName>
      <shortName evidence="6">BTD-b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>BTD-1 subunit 2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>BTD-2</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">BTDB</name>
  </gene>
  <organism>
    <name type="scientific">Papio anubis</name>
    <name type="common">Olive baboon</name>
    <dbReference type="NCBI Taxonomy" id="9555"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Papio</taxon>
    </lineage>
  </organism>
  <reference evidence="7 8" key="1">
    <citation type="journal article" date="2008" name="Infect. Immun." volume="76" first="5883" last="5891">
      <title>Isolation, synthesis, and antimicrobial activities of naturally occurring theta-defensin isoforms from baboon leukocytes.</title>
      <authorList>
        <person name="Garcia A.E."/>
        <person name="Oesapay G."/>
        <person name="Tran P.A."/>
        <person name="Yuan J."/>
        <person name="Selsted M.E."/>
      </authorList>
      <dbReference type="PubMed" id="18852242"/>
      <dbReference type="DOI" id="10.1128/iai.01100-08"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 65-73</scope>
    <scope>IDENTIFICATION OF BTD-1 AND BTD-2</scope>
    <scope>SYNTHESIS OF BTD-1 AND BTD-2</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="4">Bone marrow</tissue>
      <tissue evidence="4">Leukocyte</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Biochemistry" volume="51" first="9718" last="9726">
      <title>Structural characterization of the cyclic cystine ladder motif of theta-defensins.</title>
      <authorList>
        <person name="Conibear A.C."/>
        <person name="Rosengren K.J."/>
        <person name="Harvey P.J."/>
        <person name="Craik D.J."/>
      </authorList>
      <dbReference type="PubMed" id="23148585"/>
      <dbReference type="DOI" id="10.1021/bi301363a"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 65-73</scope>
    <scope>SYNTHESIS OF BTD-2</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>SUBUNIT</scope>
  </reference>
  <comment type="function">
    <text evidence="4">BTD-1 and BTD-2 have antimicrobial activity against the Gram-negative bacterium E.coli ML35, the Gram-positive bacterium S.aureus 502a, and the fungus C.albicans 16820. BTD-2 is more effective against E.coli than BTD-1.</text>
  </comment>
  <comment type="subunit">
    <text evidence="4 5">BTD-1 is a cyclic heterodimer composed of subunits A and B; disulfide-linked. BTD-2 is a cyclic homodimer composed of two subunits B; disulfide-linked.</text>
  </comment>
  <comment type="PTM">
    <text evidence="4">Forms a cyclic peptide with subunit A (BTD-1), or subunit B (BTD-2). An additional intersubunit disulfide bond is formed.</text>
  </comment>
  <comment type="mass spectrometry" mass="2055.69" method="MALDI" evidence="4">
    <text>BTD-1, heterodimer, cyclized.</text>
  </comment>
  <comment type="mass spectrometry" mass="2062.72" method="MALDI" evidence="4">
    <text>BTD-2, homodimer, cyclized.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the alpha-defensin family. Theta subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="FJ030940">
    <property type="protein sequence ID" value="ACJ12914.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001135412.1">
    <property type="nucleotide sequence ID" value="NM_001141940.1"/>
  </dbReference>
  <dbReference type="PDB" id="2LYE">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=65-73"/>
  </dbReference>
  <dbReference type="PDBsum" id="2LYE"/>
  <dbReference type="AlphaFoldDB" id="B6ULW5"/>
  <dbReference type="GeneID" id="100196941"/>
  <dbReference type="KEGG" id="panu:100196941"/>
  <dbReference type="CTD" id="100196941"/>
  <dbReference type="OrthoDB" id="4706736at2759"/>
  <dbReference type="Proteomes" id="UP000028761">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031012">
    <property type="term" value="C:extracellular matrix"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051673">
    <property type="term" value="P:disruption of plasma membrane integrity in another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016327">
    <property type="entry name" value="Alpha-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002366">
    <property type="entry name" value="Alpha-defensin_N"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876">
    <property type="entry name" value="ALPHA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876:SF34">
    <property type="entry name" value="DEMIDEFENSIN-3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00879">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001875">
    <property type="entry name" value="Alpha-defensin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01418">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000364010" evidence="4">
    <location>
      <begin position="23"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000364011" description="Theta defensin subunit B">
    <location>
      <begin position="65"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000364012" evidence="4">
    <location>
      <begin position="74"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="24"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (with C-66 in subunit A); in form BTD-1" evidence="1">
    <location>
      <position position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain (with C-66 in subunit B); in form BTD-2" evidence="5">
    <location>
      <position position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="68"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Arg-Cys) (interchain with C-73 in subunit A); in form BTD-1">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Arg-Cys) (interchain with C-73 in subunit B); in form BTD-2">
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Cys-Arg) (interchain with R-65 in subunit A); in form BTD-1">
    <location>
      <position position="73"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Cys-Arg) (interchain with R-65 in subunit B); in form BTD-2">
    <location>
      <position position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P82271"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="18852242"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="23148585"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="18852242"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="ACJ12914.1"/>
    </source>
  </evidence>
  <sequence length="76" mass="8164" checksum="F0ABB74E7847103A" modified="2008-12-16" version="1" precursor="true">MRTFALLTAMLLLVALQPQAEARQARADEAAAQQQPGADDQGMAHSFTRPENAALPLSESAKGLRCVCRRGVCQLL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>