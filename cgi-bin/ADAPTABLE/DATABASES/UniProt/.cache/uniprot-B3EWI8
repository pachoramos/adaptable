<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-06-13" modified="2022-05-25" version="8" xmlns="http://uniprot.org/uniprot">
  <accession>B3EWI8</accession>
  <name>AMP3_CENMR</name>
  <protein>
    <recommendedName>
      <fullName>Putative antimicrobial protein 3</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">Cm-p2</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Cenchritis muricatus</name>
    <name type="common">Beaded periwinkle</name>
    <dbReference type="NCBI Taxonomy" id="197001"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Caenogastropoda</taxon>
      <taxon>Littorinimorpha</taxon>
      <taxon>Littorinoidea</taxon>
      <taxon>Littorinidae</taxon>
      <taxon>Cenchritis</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2012" name="Biochimie" volume="94" first="968" last="974">
      <title>Functional characterization of a synthetic hydrophilic antifungal peptide derived from the marine snail Cenchritis muricatus.</title>
      <authorList>
        <person name="Lopez-Abarrategui C."/>
        <person name="Alba A."/>
        <person name="Silva O.N."/>
        <person name="Reyes-Acosta O."/>
        <person name="Vasconcelos I.M."/>
        <person name="Oliveira J.T."/>
        <person name="Migliolo L."/>
        <person name="Costa M.P."/>
        <person name="Costa C.R."/>
        <person name="Silva M.R."/>
        <person name="Garay H.E."/>
        <person name="Dias S.C."/>
        <person name="Franco O.L."/>
        <person name="Otero-Gonzalez A.J."/>
      </authorList>
      <dbReference type="PubMed" id="22210491"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2011.12.016"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>POSSIBLE FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="1">May have antimicrobial activity.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="B3EWI8"/>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <feature type="chain" id="PRO_0000417915" description="Putative antimicrobial protein 3">
    <location>
      <begin position="1"/>
      <end position="17" status="greater than"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="2">
    <location>
      <position position="17"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="22210491"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="22210491"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="17" mass="1843" checksum="B700DF71D733CD4C" modified="2012-06-13" version="1" fragment="single">SESILIVHQQQSRSSGS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>