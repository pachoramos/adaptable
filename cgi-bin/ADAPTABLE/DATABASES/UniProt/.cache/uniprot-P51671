<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-10-01" modified="2024-11-27" version="214" xmlns="http://uniprot.org/uniprot">
  <accession>P51671</accession>
  <accession>P50877</accession>
  <accession>Q92490</accession>
  <accession>Q92491</accession>
  <name>CCL11_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Eotaxin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>C-C motif chemokine 11</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Eosinophil chemotactic protein</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Small-inducible cytokine A11</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CCL11</name>
    <name type="synonym">SCYA11</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Nat. Med." volume="2" first="449" last="456">
      <title>Human eotaxin is a specific chemoattractant for eosinophil cells and provides a new mechanism to explain tissue eosinophilia.</title>
      <authorList>
        <person name="Garcia-Zepeda E.A."/>
        <person name="Rothenberg M.E."/>
        <person name="Ownbey T.R."/>
        <person name="Leder P."/>
        <person name="Luster A.D."/>
      </authorList>
      <dbReference type="PubMed" id="8597956"/>
      <dbReference type="DOI" id="10.1038/nm0496-449"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1996" name="J. Clin. Invest." volume="97" first="604" last="612">
      <title>Cloning of the human eosinophil chemoattractant, eotaxin. Expression, receptor binding, and functional properties suggest a mechanism for the selective recruitment of eosinophils.</title>
      <authorList>
        <person name="Ponath P.D."/>
        <person name="Qin S."/>
        <person name="Ringler D.J."/>
        <person name="Clark-Lewis I."/>
        <person name="Wang J."/>
        <person name="Kassam N."/>
        <person name="Smith H."/>
        <person name="Shi X."/>
        <person name="Gonzalo J.A."/>
        <person name="Newman W."/>
        <person name="Gutierrez-Ramos J.-C."/>
        <person name="Mackay C.R."/>
      </authorList>
      <dbReference type="PubMed" id="8609214"/>
      <dbReference type="DOI" id="10.1172/jci118456"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1996" name="J. Biol. Chem." volume="271" first="7725" last="7730">
      <title>Molecular cloning of human eotaxin, an eosinophil-selective CC chemokine, and identification of a specific eosinophil eotaxin receptor, CC chemokine receptor 3.</title>
      <authorList>
        <person name="Kitaura M."/>
        <person name="Nakajima T."/>
        <person name="Imai T."/>
        <person name="Harada S."/>
        <person name="Combadiere C."/>
        <person name="Tiffany H.L."/>
        <person name="Murphy P.M."/>
        <person name="Yoshie O."/>
      </authorList>
      <dbReference type="PubMed" id="8631813"/>
      <dbReference type="DOI" id="10.1074/jbc.271.13.7725"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Small intestine</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1996" name="Biochem. Biophys. Res. Commun." volume="225" first="1045" last="1051">
      <title>Human dermal fibroblasts express eotaxin: molecular cloning, mRNA expression, and identification of eotaxin sequence variants.</title>
      <authorList>
        <person name="Bartels J."/>
        <person name="Schlueter C."/>
        <person name="Richter E."/>
        <person name="Noso N."/>
        <person name="Kulke R."/>
        <person name="Christophers E."/>
        <person name="Schroeder J.-M."/>
      </authorList>
      <dbReference type="PubMed" id="8780731"/>
      <dbReference type="DOI" id="10.1006/bbrc.1996.1292"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 60-65 AND 75-88</scope>
    <scope>VARIANTS</scope>
    <source>
      <tissue>Foreskin</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1997" name="Genomics" volume="41" first="471" last="476">
      <title>Genomic organization, complete sequence, and chromosomal location of the gene for human eotaxin (SCYA11), an eosinophil-specific CC chemokine.</title>
      <authorList>
        <person name="Garcia-Zepeda E.A."/>
        <person name="Rothenberg M.E."/>
        <person name="Weremowicz S."/>
        <person name="Sarafi M.N."/>
        <person name="Morton C.C."/>
        <person name="Luster A.D."/>
      </authorList>
      <dbReference type="PubMed" id="9169149"/>
      <dbReference type="DOI" id="10.1006/geno.1997.4656"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Placenta</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1997" name="Biochem. Biophys. Res. Commun." volume="237" first="537" last="542">
      <title>Genomic organization, sequence, and transcriptional regulation of the human eotaxin gene.</title>
      <authorList>
        <person name="Hein H."/>
        <person name="Schlueter C."/>
        <person name="Kulke R."/>
        <person name="Christophers E."/>
        <person name="Schroeder J.-M."/>
        <person name="Bartels J."/>
      </authorList>
      <dbReference type="PubMed" id="9299399"/>
      <dbReference type="DOI" id="10.1006/bbrc.1997.7169"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Lung</tissue>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Lung</tissue>
    </source>
  </reference>
  <reference key="8">
    <citation type="journal article" date="1998" name="Eur. J. Biochem." volume="253" first="114" last="122">
      <title>Delayed production of biologically active O-glycosylated forms of human eotaxin by tumor-necrosis-factor-alpha-stimulated dermal fibroblasts.</title>
      <authorList>
        <person name="Noso N."/>
        <person name="Bartels J."/>
        <person name="Mallet A.I."/>
        <person name="Mochizuki M."/>
        <person name="Christophers E."/>
        <person name="Schroeder J.-M."/>
      </authorList>
      <dbReference type="PubMed" id="9578468"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.1998.2530114.x"/>
    </citation>
    <scope>GLYCOSYLATION AT THR-94</scope>
    <source>
      <tissue>Blood</tissue>
    </source>
  </reference>
  <reference key="9">
    <citation type="journal article" date="1998" name="J. Biol. Chem." volume="273" first="22471" last="22479">
      <title>Solution structure of eotaxin, a chemokine that selectively recruits eosinophils in allergic inflammation.</title>
      <authorList>
        <person name="Crump M.P."/>
        <person name="Rajarathnam K."/>
        <person name="Kim K.S."/>
        <person name="Clark-Lewis I."/>
        <person name="Sykes B.D."/>
      </authorList>
      <dbReference type="PubMed" id="9712872"/>
      <dbReference type="DOI" id="10.1074/jbc.273.35.22471"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3">In response to the presence of allergens, this protein directly promotes the accumulation of eosinophils, a prominent feature of allergic inflammatory reactions (PubMed:8597956). Binds to CCR3 (PubMed:8631813).</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-727357">
      <id>P51671</id>
    </interactant>
    <interactant intactId="EBI-16744026">
      <id>Q9Y4X3</id>
      <label>CCL27</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-727357">
      <id>P51671</id>
    </interactant>
    <interactant intactId="EBI-7783254">
      <id>Q9NRJ3</id>
      <label>CCL28</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-727357">
      <id>P51671</id>
    </interactant>
    <interactant intactId="EBI-2848366">
      <id>P13501</id>
      <label>CCL5</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-727357">
      <id>P51671</id>
    </interactant>
    <interactant intactId="EBI-6625120">
      <id>P51677</id>
      <label>CCR3</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-727357">
      <id>P51671</id>
    </interactant>
    <interactant intactId="EBI-7815386">
      <id>P02778</id>
      <label>CXCL10</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-727357">
      <id>P51671</id>
    </interactant>
    <interactant intactId="EBI-3913254">
      <id>P48061</id>
      <label>CXCL12</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-727357">
      <id>P51671</id>
    </interactant>
    <interactant intactId="EBI-2871277">
      <id>P27487</id>
      <label>DPP4</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-727357">
      <id>P51671</id>
    </interactant>
    <interactant intactId="EBI-2565740">
      <id>P02776</id>
      <label>PF4</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="7">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text evidence="2">Induced by TNF, IL1A/interleukin-1 alpha and IFNG/IFN-gamma.</text>
  </comment>
  <comment type="PTM">
    <text evidence="4">O-linked glycan consists of a Gal-GalNAc disaccharide which is modified with up to 2 sialic acid residues.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the intercrine beta (chemokine CC) family.</text>
  </comment>
  <comment type="online information" name="Wikipedia">
    <link uri="https://en.wikipedia.org/wiki/CCL11"/>
    <text>CCL11 entry</text>
  </comment>
  <dbReference type="EMBL" id="U46573">
    <property type="protein sequence ID" value="AAA98957.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U34780">
    <property type="protein sequence ID" value="AAC50369.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D49372">
    <property type="protein sequence ID" value="BAA08370.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Z69291">
    <property type="protein sequence ID" value="CAA93258.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Z75668">
    <property type="protein sequence ID" value="CAA99997.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Z75669">
    <property type="protein sequence ID" value="CAA99998.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U46572">
    <property type="protein sequence ID" value="AAC51297.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Z92709">
    <property type="protein sequence ID" value="CAB07027.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC017850">
    <property type="protein sequence ID" value="AAH17850.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS11279.1"/>
  <dbReference type="PIR" id="JC4912">
    <property type="entry name" value="JC4912"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_002977.1">
    <property type="nucleotide sequence ID" value="NM_002986.2"/>
  </dbReference>
  <dbReference type="PDB" id="1EOT">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=24-97"/>
  </dbReference>
  <dbReference type="PDB" id="2EOT">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=24-97"/>
  </dbReference>
  <dbReference type="PDB" id="2MPM">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=24-97"/>
  </dbReference>
  <dbReference type="PDB" id="7SCS">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.51 A"/>
    <property type="chains" value="B=24-97"/>
  </dbReference>
  <dbReference type="PDBsum" id="1EOT"/>
  <dbReference type="PDBsum" id="2EOT"/>
  <dbReference type="PDBsum" id="2MPM"/>
  <dbReference type="PDBsum" id="7SCS"/>
  <dbReference type="AlphaFoldDB" id="P51671"/>
  <dbReference type="BMRB" id="P51671"/>
  <dbReference type="SMR" id="P51671"/>
  <dbReference type="BioGRID" id="112259">
    <property type="interactions" value="28"/>
  </dbReference>
  <dbReference type="DIP" id="DIP-5858N"/>
  <dbReference type="IntAct" id="P51671">
    <property type="interactions" value="28"/>
  </dbReference>
  <dbReference type="MINT" id="P51671"/>
  <dbReference type="STRING" id="9606.ENSP00000302234"/>
  <dbReference type="BindingDB" id="P51671"/>
  <dbReference type="ChEMBL" id="CHEMBL3286077"/>
  <dbReference type="DrugBank" id="DB05429">
    <property type="generic name" value="Bertilimumab"/>
  </dbReference>
  <dbReference type="GlyCosmos" id="P51671">
    <property type="glycosylation" value="1 site, No reported glycans"/>
  </dbReference>
  <dbReference type="GlyGen" id="P51671">
    <property type="glycosylation" value="2 sites, 1 O-linked glycan (1 site)"/>
  </dbReference>
  <dbReference type="iPTMnet" id="P51671"/>
  <dbReference type="PhosphoSitePlus" id="P51671"/>
  <dbReference type="BioMuta" id="CCL11"/>
  <dbReference type="DMDM" id="1706661"/>
  <dbReference type="PaxDb" id="9606-ENSP00000302234"/>
  <dbReference type="PeptideAtlas" id="P51671"/>
  <dbReference type="ABCD" id="P51671">
    <property type="antibodies" value="3 sequenced antibodies"/>
  </dbReference>
  <dbReference type="Antibodypedia" id="15485">
    <property type="antibodies" value="410 antibodies from 42 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="6356"/>
  <dbReference type="Ensembl" id="ENST00000305869.4">
    <property type="protein sequence ID" value="ENSP00000302234.3"/>
    <property type="gene ID" value="ENSG00000172156.4"/>
  </dbReference>
  <dbReference type="GeneID" id="6356"/>
  <dbReference type="KEGG" id="hsa:6356"/>
  <dbReference type="MANE-Select" id="ENST00000305869.4">
    <property type="protein sequence ID" value="ENSP00000302234.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_002986.3"/>
    <property type="RefSeq protein sequence ID" value="NP_002977.1"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:10610"/>
  <dbReference type="CTD" id="6356"/>
  <dbReference type="DisGeNET" id="6356"/>
  <dbReference type="GeneCards" id="CCL11"/>
  <dbReference type="HGNC" id="HGNC:10610">
    <property type="gene designation" value="CCL11"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000172156">
    <property type="expression patterns" value="Tissue enhanced (intestine, lymphoid tissue, stomach, urinary bladder)"/>
  </dbReference>
  <dbReference type="MalaCards" id="CCL11"/>
  <dbReference type="MIM" id="601156">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P51671"/>
  <dbReference type="OpenTargets" id="ENSG00000172156"/>
  <dbReference type="PharmGKB" id="PA35543"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000172156"/>
  <dbReference type="eggNOG" id="ENOG502S8M4">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT01100000263482"/>
  <dbReference type="HOGENOM" id="CLU_141716_1_0_1"/>
  <dbReference type="InParanoid" id="P51671"/>
  <dbReference type="OMA" id="TICCFNV"/>
  <dbReference type="OrthoDB" id="4265193at2759"/>
  <dbReference type="PhylomeDB" id="P51671"/>
  <dbReference type="TreeFam" id="TF334888"/>
  <dbReference type="PathwayCommons" id="P51671"/>
  <dbReference type="Reactome" id="R-HSA-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-6785807">
    <property type="pathway name" value="Interleukin-4 and Interleukin-13 signaling"/>
  </dbReference>
  <dbReference type="SignaLink" id="P51671"/>
  <dbReference type="SIGNOR" id="P51671"/>
  <dbReference type="BioGRID-ORCS" id="6356">
    <property type="hits" value="11 hits in 1137 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="CCL11">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P51671"/>
  <dbReference type="GeneWiki" id="CCL11"/>
  <dbReference type="GenomeRNAi" id="6356"/>
  <dbReference type="Pharos" id="P51671">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P51671"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 17"/>
  </dbReference>
  <dbReference type="RNAct" id="P51671">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000172156">
    <property type="expression patterns" value="Expressed in pylorus and 87 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P51671">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="BHF-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048020">
    <property type="term" value="F:CCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031728">
    <property type="term" value="F:CCR3 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="CAFA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046983">
    <property type="term" value="F:protein dimerization activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="CAFA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048018">
    <property type="term" value="F:receptor ligand activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="CAFA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007015">
    <property type="term" value="P:actin filament organization"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060444">
    <property type="term" value="P:branching involved in mammary gland duct morphogenesis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007155">
    <property type="term" value="P:cell adhesion"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006935">
    <property type="term" value="P:chemotaxis"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002544">
    <property type="term" value="P:chronic inflammatory response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007010">
    <property type="term" value="P:cytoskeleton organization"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048245">
    <property type="term" value="P:eosinophil chemotaxis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070371">
    <property type="term" value="P:ERK1 and ERK2 cascade"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006874">
    <property type="term" value="P:intracellular calcium ion homeostasis"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007611">
    <property type="term" value="P:learning or memory"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="ARUK-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060763">
    <property type="term" value="P:mammary duct terminal end bud growth"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002551">
    <property type="term" value="P:mast cell chemotaxis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050768">
    <property type="term" value="P:negative regulation of neurogenesis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="ARUK-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030838">
    <property type="term" value="P:positive regulation of actin filament polymerization"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="BHF-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045766">
    <property type="term" value="P:positive regulation of angiogenesis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="BHF-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001938">
    <property type="term" value="P:positive regulation of endothelial cell proliferation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="BHF-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043547">
    <property type="term" value="P:positive regulation of GTPase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="BHF-UCL"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006468">
    <property type="term" value="P:protein phosphorylation"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008360">
    <property type="term" value="P:regulation of cell shape"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035962">
    <property type="term" value="P:response to interleukin-13"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070670">
    <property type="term" value="P:response to interleukin-4"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009314">
    <property type="term" value="P:response to radiation"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009615">
    <property type="term" value="P:response to virus"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007165">
    <property type="term" value="P:signal transduction"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="CDD" id="cd00272">
    <property type="entry name" value="Chemokine_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="DisProt" id="DP00641"/>
  <dbReference type="FunFam" id="2.40.50.40:FF:000002">
    <property type="entry name" value="C-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000827">
    <property type="entry name" value="Chemokine_CC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF146">
    <property type="entry name" value="EOTAXIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00472">
    <property type="entry name" value="SMALL_CYTOKINES_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005195" description="Eotaxin">
    <location>
      <begin position="24"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="O-linked (GalNAc...) threonine" evidence="4">
    <location>
      <position position="94"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="32"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="33"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_001634">
    <original>L</original>
    <variation>P</variation>
    <location>
      <position position="7"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_001635" description="In dbSNP:rs1129844.">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_001636">
    <original>R</original>
    <variation>S</variation>
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_001637">
    <original>K</original>
    <variation>R</variation>
    <location>
      <position position="79"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_048705" description="In dbSNP:rs34262946.">
    <original>K</original>
    <variation>T</variation>
    <location>
      <position position="86"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="26"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="30"/>
      <end position="32"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="43"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="46"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="57"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="61"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="67"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="71"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="79"/>
      <end position="90"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="8597956"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="8631813"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="9578468"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="9712872"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="8597956"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="8">
    <source>
      <dbReference type="PDB" id="1EOT"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="9">
    <source>
      <dbReference type="PDB" id="2MPM"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="10">
    <source>
      <dbReference type="PDB" id="7SCS"/>
    </source>
  </evidence>
  <sequence length="97" mass="10732" checksum="B433C30FDA4C71A7" modified="1996-10-01" version="1" precursor="true">MKVSAALLWLLLIAAAFSPQGLAGPASVPTTCCFNLANRKIPLQRLESYRRITSGKCPQKAVIFKTKLAKDICADPKKKWVQDSMKYLDQKSPTPKP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>