<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-07-25" modified="2023-11-08" version="19" xmlns="http://uniprot.org/uniprot">
  <accession>P0C1Q8</accession>
  <name>MAST1_VESVU</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Mastoparan-V1</fullName>
      <shortName evidence="3">MP-V1</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Vespula vulgaris</name>
    <name type="common">Yellow jacket</name>
    <name type="synonym">Wasp</name>
    <dbReference type="NCBI Taxonomy" id="7454"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Vespoidea</taxon>
      <taxon>Vespidae</taxon>
      <taxon>Vespinae</taxon>
      <taxon>Vespula</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Int. Arch. Allergy Immunol." volume="131" first="25" last="32">
      <title>Inflammatory role of two venom components of yellow jackets (Vespula vulgaris): a mast cell degranulating peptide mastoparan and phospholipase A1.</title>
      <authorList>
        <person name="King T.P."/>
        <person name="Jim S.Y."/>
        <person name="Wittkowski K.M."/>
      </authorList>
      <dbReference type="PubMed" id="12759486"/>
      <dbReference type="DOI" id="10.1159/000070431"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>AMIDATION AT ASN-15</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2016" name="Molecules" volume="21" first="512" last="512">
      <title>MP-V1 from the venom of social wasp vespula vulgaris is a de novo type of mastoparan that displays superior antimicrobial activities.</title>
      <authorList>
        <person name="Kim Y."/>
        <person name="Son M."/>
        <person name="Noh E.Y."/>
        <person name="Kim S."/>
        <person name="Kim C."/>
        <person name="Yeo J.H."/>
        <person name="Park C."/>
        <person name="Lee K.W."/>
        <person name="Bang W.Y."/>
      </authorList>
      <dbReference type="PubMed" id="27104500"/>
      <dbReference type="DOI" id="10.3390/molecules21040512"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2">Antimicrobial peptide with high activity on Gram-positive (S.mutans, and S.aureus), and Gram-negative bacteria (S.enterica), as well as on fungi (C.albicans, C.glabrata, and C.neoformans). Shows a potent hemolysis on human erythrocytes (PubMed:27104500). Together with phospholipase A1, stimulates prostaglandin E2 release from murine peritoneal cells and macrophages (PubMed:12759486).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="4">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="6">Assumes an amphipathic alpha-helical conformation in a membrane-like environment.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the MCD family. Mastoparan subfamily.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <feature type="peptide" id="PRO_0000247267" description="Mastoparan-V1" evidence="1">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="site" description="May enhance the stabilization of the helical conformation but leads to a lower membrane selectivity compared to its homologs that do not have a C-terminal Asn" evidence="6">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <feature type="modified residue" description="Asparagine amide" evidence="1">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="12759486"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27104500"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="12759486"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="12759486"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="27104500"/>
    </source>
  </evidence>
  <sequence length="15" mass="1758" checksum="93BBE82237A7FC68" modified="2006-07-25" version="1">INWKKIKSIIKAAMN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>