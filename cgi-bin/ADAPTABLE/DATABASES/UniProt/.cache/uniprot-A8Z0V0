<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-07-22" modified="2023-09-13" version="36" xmlns="http://uniprot.org/uniprot">
  <accession>A8Z0V0</accession>
  <name>PSMA2_STAAT</name>
  <protein>
    <recommendedName>
      <fullName>Phenol-soluble modulin alpha 2 peptide</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">psmA2</name>
    <name type="ordered locus">USA300HOU_0455</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain USA300 / TCH1516)</name>
    <dbReference type="NCBI Taxonomy" id="451516"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2007" name="BMC Microbiol." volume="7" first="99" last="99">
      <title>Subtle genetic changes enhance virulence of methicillin resistant and sensitive Staphylococcus aureus.</title>
      <authorList>
        <person name="Highlander S.K."/>
        <person name="Hulten K.G."/>
        <person name="Qin X."/>
        <person name="Jiang H."/>
        <person name="Yerrapragada S."/>
        <person name="Mason E.O. Jr."/>
        <person name="Shang Y."/>
        <person name="Williams T.M."/>
        <person name="Fortunov R.M."/>
        <person name="Liu Y."/>
        <person name="Igboeli O."/>
        <person name="Petrosino J."/>
        <person name="Tirumalai M."/>
        <person name="Uzman A."/>
        <person name="Fox G.E."/>
        <person name="Cardenas A.M."/>
        <person name="Muzny D.M."/>
        <person name="Hemphill L."/>
        <person name="Ding Y."/>
        <person name="Dugan S."/>
        <person name="Blyth P.R."/>
        <person name="Buhay C.J."/>
        <person name="Dinh H.H."/>
        <person name="Hawes A.C."/>
        <person name="Holder M."/>
        <person name="Kovar C.L."/>
        <person name="Lee S.L."/>
        <person name="Liu W."/>
        <person name="Nazareth L.V."/>
        <person name="Wang Q."/>
        <person name="Zhou J."/>
        <person name="Kaplan S.L."/>
        <person name="Weinstock G.M."/>
      </authorList>
      <dbReference type="PubMed" id="17986343"/>
      <dbReference type="DOI" id="10.1186/1471-2180-7-99"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>USA300 / TCH1516</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Peptide which can recruit, activate and subsequently lyse human neutrophils, thus eliminating the main cellular defense against infection.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the phenol-soluble modulin alpha peptides family.</text>
  </comment>
  <dbReference type="EMBL" id="CP000730">
    <property type="protein sequence ID" value="ABX28481.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A8Z0V0"/>
  <dbReference type="KEGG" id="sax:USA300HOU_0455"/>
  <dbReference type="HOGENOM" id="CLU_222042_0_0_9"/>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031429">
    <property type="entry name" value="PSM_alpha"/>
  </dbReference>
  <dbReference type="NCBIfam" id="NF033425">
    <property type="entry name" value="PSM_alpha_1_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17063">
    <property type="entry name" value="PSMalpha"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="peptide" id="PRO_0000345055" description="Phenol-soluble modulin alpha 2 peptide">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A9JX06"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="21" mass="2278" checksum="E3D5498D1727572D" modified="2008-01-15" version="1">MGIIAGIIKFIKGLIEKFTGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>