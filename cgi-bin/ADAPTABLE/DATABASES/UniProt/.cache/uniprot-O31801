<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-11-24" modified="2024-11-27" version="134" xmlns="http://uniprot.org/uniprot">
  <accession>O31801</accession>
  <name>YNCF_BACSU</name>
  <protein>
    <recommendedName>
      <fullName>Deoxyuridine 5'-triphosphate nucleotidohydrolase YncF</fullName>
      <shortName>dUTPase</shortName>
      <ecNumber evidence="1">3.6.1.23</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>dUTP pyrophosphatase</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">yncF</name>
    <name type="ordered locus">BSU17660</name>
  </gene>
  <organism>
    <name type="scientific">Bacillus subtilis (strain 168)</name>
    <dbReference type="NCBI Taxonomy" id="224308"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Bacillaceae</taxon>
      <taxon>Bacillus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Nature" volume="390" first="249" last="256">
      <title>The complete genome sequence of the Gram-positive bacterium Bacillus subtilis.</title>
      <authorList>
        <person name="Kunst F."/>
        <person name="Ogasawara N."/>
        <person name="Moszer I."/>
        <person name="Albertini A.M."/>
        <person name="Alloni G."/>
        <person name="Azevedo V."/>
        <person name="Bertero M.G."/>
        <person name="Bessieres P."/>
        <person name="Bolotin A."/>
        <person name="Borchert S."/>
        <person name="Borriss R."/>
        <person name="Boursier L."/>
        <person name="Brans A."/>
        <person name="Braun M."/>
        <person name="Brignell S.C."/>
        <person name="Bron S."/>
        <person name="Brouillet S."/>
        <person name="Bruschi C.V."/>
        <person name="Caldwell B."/>
        <person name="Capuano V."/>
        <person name="Carter N.M."/>
        <person name="Choi S.-K."/>
        <person name="Codani J.-J."/>
        <person name="Connerton I.F."/>
        <person name="Cummings N.J."/>
        <person name="Daniel R.A."/>
        <person name="Denizot F."/>
        <person name="Devine K.M."/>
        <person name="Duesterhoeft A."/>
        <person name="Ehrlich S.D."/>
        <person name="Emmerson P.T."/>
        <person name="Entian K.-D."/>
        <person name="Errington J."/>
        <person name="Fabret C."/>
        <person name="Ferrari E."/>
        <person name="Foulger D."/>
        <person name="Fritz C."/>
        <person name="Fujita M."/>
        <person name="Fujita Y."/>
        <person name="Fuma S."/>
        <person name="Galizzi A."/>
        <person name="Galleron N."/>
        <person name="Ghim S.-Y."/>
        <person name="Glaser P."/>
        <person name="Goffeau A."/>
        <person name="Golightly E.J."/>
        <person name="Grandi G."/>
        <person name="Guiseppi G."/>
        <person name="Guy B.J."/>
        <person name="Haga K."/>
        <person name="Haiech J."/>
        <person name="Harwood C.R."/>
        <person name="Henaut A."/>
        <person name="Hilbert H."/>
        <person name="Holsappel S."/>
        <person name="Hosono S."/>
        <person name="Hullo M.-F."/>
        <person name="Itaya M."/>
        <person name="Jones L.-M."/>
        <person name="Joris B."/>
        <person name="Karamata D."/>
        <person name="Kasahara Y."/>
        <person name="Klaerr-Blanchard M."/>
        <person name="Klein C."/>
        <person name="Kobayashi Y."/>
        <person name="Koetter P."/>
        <person name="Koningstein G."/>
        <person name="Krogh S."/>
        <person name="Kumano M."/>
        <person name="Kurita K."/>
        <person name="Lapidus A."/>
        <person name="Lardinois S."/>
        <person name="Lauber J."/>
        <person name="Lazarevic V."/>
        <person name="Lee S.-M."/>
        <person name="Levine A."/>
        <person name="Liu H."/>
        <person name="Masuda S."/>
        <person name="Mauel C."/>
        <person name="Medigue C."/>
        <person name="Medina N."/>
        <person name="Mellado R.P."/>
        <person name="Mizuno M."/>
        <person name="Moestl D."/>
        <person name="Nakai S."/>
        <person name="Noback M."/>
        <person name="Noone D."/>
        <person name="O'Reilly M."/>
        <person name="Ogawa K."/>
        <person name="Ogiwara A."/>
        <person name="Oudega B."/>
        <person name="Park S.-H."/>
        <person name="Parro V."/>
        <person name="Pohl T.M."/>
        <person name="Portetelle D."/>
        <person name="Porwollik S."/>
        <person name="Prescott A.M."/>
        <person name="Presecan E."/>
        <person name="Pujic P."/>
        <person name="Purnelle B."/>
        <person name="Rapoport G."/>
        <person name="Rey M."/>
        <person name="Reynolds S."/>
        <person name="Rieger M."/>
        <person name="Rivolta C."/>
        <person name="Rocha E."/>
        <person name="Roche B."/>
        <person name="Rose M."/>
        <person name="Sadaie Y."/>
        <person name="Sato T."/>
        <person name="Scanlan E."/>
        <person name="Schleich S."/>
        <person name="Schroeter R."/>
        <person name="Scoffone F."/>
        <person name="Sekiguchi J."/>
        <person name="Sekowska A."/>
        <person name="Seror S.J."/>
        <person name="Serror P."/>
        <person name="Shin B.-S."/>
        <person name="Soldo B."/>
        <person name="Sorokin A."/>
        <person name="Tacconi E."/>
        <person name="Takagi T."/>
        <person name="Takahashi H."/>
        <person name="Takemaru K."/>
        <person name="Takeuchi M."/>
        <person name="Tamakoshi A."/>
        <person name="Tanaka T."/>
        <person name="Terpstra P."/>
        <person name="Tognoni A."/>
        <person name="Tosato V."/>
        <person name="Uchiyama S."/>
        <person name="Vandenbol M."/>
        <person name="Vannier F."/>
        <person name="Vassarotti A."/>
        <person name="Viari A."/>
        <person name="Wambutt R."/>
        <person name="Wedler E."/>
        <person name="Wedler H."/>
        <person name="Weitzenegger T."/>
        <person name="Winters P."/>
        <person name="Wipat A."/>
        <person name="Yamamoto H."/>
        <person name="Yamane K."/>
        <person name="Yasumoto K."/>
        <person name="Yata K."/>
        <person name="Yoshida K."/>
        <person name="Yoshikawa H.-F."/>
        <person name="Zumstein E."/>
        <person name="Yoshikawa H."/>
        <person name="Danchin A."/>
      </authorList>
      <dbReference type="PubMed" id="9384377"/>
      <dbReference type="DOI" id="10.1038/36786"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>168</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2007" name="J. Bacteriol." volume="189" first="591" last="602">
      <title>Essential bacterial functions encoded by gene pairs.</title>
      <authorList>
        <person name="Thomaides H.B."/>
        <person name="Davison E.J."/>
        <person name="Burston L."/>
        <person name="Johnson H."/>
        <person name="Brown D.R."/>
        <person name="Hunt A.C."/>
        <person name="Errington J."/>
        <person name="Czaplewski L."/>
      </authorList>
      <dbReference type="PubMed" id="17114254"/>
      <dbReference type="DOI" id="10.1128/jb.01381-06"/>
    </citation>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>168</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2009" name="Acta Crystallogr. F" volume="65" first="339" last="342">
      <title>Crystallization and preliminary X-ray analysis of three dUTPases from Gram-positive bacteria.</title>
      <authorList>
        <person name="Li G.-L."/>
        <person name="Wang J."/>
        <person name="Li L.-F."/>
        <person name="Su X.-D."/>
      </authorList>
      <dbReference type="PubMed" id="19342774"/>
      <dbReference type="DOI" id="10.1107/s1744309109006228"/>
    </citation>
    <scope>PRELIMINARY CRYSTALLIZATION</scope>
  </reference>
  <reference evidence="8 9" key="4">
    <citation type="journal article" date="2010" name="Acta Crystallogr. D" volume="66" first="953" last="961">
      <title>The structure of the genomic Bacillus subtilis dUTPase: novel features in the Phe-lid.</title>
      <authorList>
        <person name="Garcia-Nafria J."/>
        <person name="Burchell L."/>
        <person name="Takezawa M."/>
        <person name="Rzechorzek N.J."/>
        <person name="Fogg M.J."/>
        <person name="Wilson K.S."/>
      </authorList>
      <dbReference type="PubMed" id="20823546"/>
      <dbReference type="DOI" id="10.1107/s0907444910026272"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.84 ANGSTROMS) ALONE AND IN COMPLEX WITH SUBSTRATE ANALOG</scope>
    <scope>PROBABLE ACTIVE SITE</scope>
    <scope>COFACTOR</scope>
    <scope>SUBUNIT</scope>
  </reference>
  <reference evidence="10 11 12 13" key="5">
    <citation type="journal article" date="2013" name="Acta Crystallogr. D" volume="69" first="1367" last="1380">
      <title>Tying down the arm in Bacillus dUTPase: structure and mechanism.</title>
      <authorList>
        <person name="Garcia-Nafria J."/>
        <person name="Timm J."/>
        <person name="Harrison C."/>
        <person name="Turkenburg J.P."/>
        <person name="Wilson K.S."/>
      </authorList>
      <dbReference type="PubMed" id="23897460"/>
      <dbReference type="DOI" id="10.1107/s090744491300735x"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.18 ANGSTROMS) IN COMPLEX WITH 2'-DEOXYURIDINE; DIPHOSPHATE(2-) AND PHOSPHATE(3-)</scope>
    <scope>PROBABLE ACTIVE SITE</scope>
    <scope>SUBUNIT</scope>
    <scope>DOMAIN</scope>
  </reference>
  <comment type="function">
    <text evidence="3 6">Involved in nucleotide metabolism: produces dUMP, the immediate precursor of thymidine nucleotides and decreases the intracellular concentration of dUTP, so that uracil cannot be incorporated into DNA (Probable). The Ser-64 side chain changes its position upon ligand-binding to make contacts with the nucleotide phosphates (PubMed:20823546).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>dUTP + H2O = dUMP + diphosphate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:10248"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:33019"/>
      <dbReference type="ChEBI" id="CHEBI:61555"/>
      <dbReference type="ChEBI" id="CHEBI:246422"/>
      <dbReference type="EC" id="3.6.1.23"/>
    </reaction>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="3 4">
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </cofactor>
    <text evidence="3 4">Binds 1 Mg(2+) per subunit, coordinated entirely by the nucleotide and ordered water molecules.</text>
  </comment>
  <comment type="pathway">
    <text>Pyrimidine metabolism; dUMP biosynthesis; dUMP from dCTP (dUTP route): step 2/2.</text>
  </comment>
  <comment type="subunit">
    <text evidence="3 4">Homotrimer.</text>
  </comment>
  <comment type="domain">
    <text evidence="3 4">The uracil deoxyribose moiety interacts with the protein through Ile-81 and Tyr-85, which discriminate against the ribose form of the nucleotide.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="2">No visible phenotype; double yncF-yosS deletions (both encode dUTPases) are also viable.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the dUTPase family.</text>
  </comment>
  <dbReference type="EC" id="3.6.1.23" evidence="1"/>
  <dbReference type="EMBL" id="AL009126">
    <property type="protein sequence ID" value="CAB13650.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="H69888">
    <property type="entry name" value="H69888"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_389649.1">
    <property type="nucleotide sequence ID" value="NC_000964.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003245820.1">
    <property type="nucleotide sequence ID" value="NZ_OZ025638.1"/>
  </dbReference>
  <dbReference type="PDB" id="2XCD">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.84 A"/>
    <property type="chains" value="A/B/C/D/E/F=1-144"/>
  </dbReference>
  <dbReference type="PDB" id="2XCE">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.85 A"/>
    <property type="chains" value="A/B/C/D/E/F=1-144"/>
  </dbReference>
  <dbReference type="PDB" id="4AOO">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.30 A"/>
    <property type="chains" value="A/B/C/D=1-144"/>
  </dbReference>
  <dbReference type="PDB" id="4AOZ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.05 A"/>
    <property type="chains" value="A/B/C=1-144"/>
  </dbReference>
  <dbReference type="PDB" id="4APZ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.01 A"/>
    <property type="chains" value="1/2/3/4/5/6/7/8/9/A/B/C/D/E/F/G/H/I/J/K/L/M/N/O/P/Q/R/S/T/U/V/W/X/Y/Z/a/b/c/d/e/f/g/h/i/j/k/l/m=1-144"/>
  </dbReference>
  <dbReference type="PDB" id="4B0H">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.18 A"/>
    <property type="chains" value="A/B/C=1-144"/>
  </dbReference>
  <dbReference type="PDBsum" id="2XCD"/>
  <dbReference type="PDBsum" id="2XCE"/>
  <dbReference type="PDBsum" id="4AOO"/>
  <dbReference type="PDBsum" id="4AOZ"/>
  <dbReference type="PDBsum" id="4APZ"/>
  <dbReference type="PDBsum" id="4B0H"/>
  <dbReference type="AlphaFoldDB" id="O31801"/>
  <dbReference type="SMR" id="O31801"/>
  <dbReference type="STRING" id="224308.BSU17660"/>
  <dbReference type="PaxDb" id="224308-BSU17660"/>
  <dbReference type="EnsemblBacteria" id="CAB13650">
    <property type="protein sequence ID" value="CAB13650"/>
    <property type="gene ID" value="BSU_17660"/>
  </dbReference>
  <dbReference type="GeneID" id="939541"/>
  <dbReference type="KEGG" id="bsu:BSU17660"/>
  <dbReference type="PATRIC" id="fig|224308.179.peg.1917"/>
  <dbReference type="eggNOG" id="COG0756">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="InParanoid" id="O31801"/>
  <dbReference type="OrthoDB" id="9809956at2"/>
  <dbReference type="PhylomeDB" id="O31801"/>
  <dbReference type="BioCyc" id="BSUB:BSU17660-MONOMER"/>
  <dbReference type="BRENDA" id="3.6.1.23">
    <property type="organism ID" value="658"/>
  </dbReference>
  <dbReference type="UniPathway" id="UPA00610">
    <property type="reaction ID" value="UER00666"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="O31801"/>
  <dbReference type="Proteomes" id="UP000001570">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004170">
    <property type="term" value="F:dUTP diphosphatase activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000287">
    <property type="term" value="F:magnesium ion binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006226">
    <property type="term" value="P:dUMP biosynthetic process"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046081">
    <property type="term" value="P:dUTP catabolic process"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd07557">
    <property type="entry name" value="trimeric_dUTPase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.70.40.10:FF:000017">
    <property type="entry name" value="dUTPase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.70.40.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008181">
    <property type="entry name" value="dUTPase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029054">
    <property type="entry name" value="dUTPase-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036157">
    <property type="entry name" value="dUTPase-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033704">
    <property type="entry name" value="dUTPase_trimeric"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11241">
    <property type="entry name" value="DEOXYURIDINE 5'-TRIPHOSPHATE NUCLEOTIDOHYDROLASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11241:SF0">
    <property type="entry name" value="DEOXYURIDINE 5'-TRIPHOSPHATE NUCLEOTIDOHYDROLASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00692">
    <property type="entry name" value="dUTPase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF51283">
    <property type="entry name" value="dUTPase-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0460">Magnesium</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0546">Nucleotide metabolism</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000389001" description="Deoxyuridine 5'-triphosphate nucleotidohydrolase YncF">
    <location>
      <begin position="1"/>
      <end position="144"/>
    </location>
  </feature>
  <feature type="active site" description="Proton acceptor" evidence="6 7">
    <location>
      <position position="82"/>
    </location>
  </feature>
  <feature type="binding site" evidence="6 9">
    <location>
      <position position="64"/>
    </location>
    <ligand>
      <name>dUTP</name>
      <dbReference type="ChEBI" id="CHEBI:61555"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 9">
    <location>
      <position position="76"/>
    </location>
    <ligand>
      <name>dUTP</name>
      <dbReference type="ChEBI" id="CHEBI:61555"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 9">
    <location>
      <position position="85"/>
    </location>
    <ligand>
      <name>dUTP</name>
      <dbReference type="ChEBI" id="CHEBI:61555"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="6 9">
    <location>
      <position position="93"/>
    </location>
    <ligand>
      <name>dUTP</name>
      <dbReference type="ChEBI" id="CHEBI:61555"/>
    </ligand>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="3"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="24"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="34"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="41"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="48"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="56"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="helix" evidence="15">
    <location>
      <begin position="66"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="72"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="77"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="93"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="102"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="109"/>
      <end position="117"/>
    </location>
  </feature>
  <feature type="strand" evidence="15">
    <location>
      <begin position="122"/>
      <end position="126"/>
    </location>
  </feature>
  <feature type="turn" evidence="14">
    <location>
      <begin position="138"/>
      <end position="141"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="O34919"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="17114254"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="20823546"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="23897460"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="20823546"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="23897460"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="8">
    <source>
      <dbReference type="PDB" id="2XCD"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="9">
    <source>
      <dbReference type="PDB" id="2XCE"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="10">
    <source>
      <dbReference type="PDB" id="4AOO"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="11">
    <source>
      <dbReference type="PDB" id="4AOZ"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="12">
    <source>
      <dbReference type="PDB" id="4APZ"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="13">
    <source>
      <dbReference type="PDB" id="4B0H"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="14">
    <source>
      <dbReference type="PDB" id="4AOO"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="15">
    <source>
      <dbReference type="PDB" id="4B0H"/>
    </source>
  </evidence>
  <sequence length="144" mass="16410" checksum="2E15AE84170ED23B" modified="1998-01-01" version="1">MTMQIKIKYLDETQTRISKIEQGDWIDLRAAEDVTIKKDEFKLVPLGVAMELPEGYEAHVVPRSSTYKNFGVIQTNSMGVIDESYKGDNDFWFFPAYALRDTEIKKGDRICQFRIMKKMPAVELVEVEHLGNEDRGGLGSTGTK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>