<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-11-14" modified="2024-05-29" version="93" xmlns="http://uniprot.org/uniprot">
  <accession>Q64365</accession>
  <name>DEF1B_CAVPO</name>
  <protein>
    <recommendedName>
      <fullName>Neutrophil cationic peptide 1 type B</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>GNCP-1B</fullName>
    </alternativeName>
    <component>
      <recommendedName>
        <fullName>Neutrophil cationic peptide 1</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Corticostatic peptide GP-CS1</fullName>
        <shortName>CP-1</shortName>
      </alternativeName>
      <alternativeName>
        <fullName>GPNP</fullName>
      </alternativeName>
      <alternativeName>
        <fullName>Neutrophil defensin</fullName>
      </alternativeName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Cavia porcellus</name>
    <name type="common">Guinea pig</name>
    <dbReference type="NCBI Taxonomy" id="10141"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Hystricomorpha</taxon>
      <taxon>Caviidae</taxon>
      <taxon>Cavia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Comp. Biochem. Physiol." volume="106B" first="387" last="390">
      <title>Guinea-pig neutrophil cationic peptides are encoded by at least three kinds of mRNA transcripts.</title>
      <authorList>
        <person name="Nagaoka I."/>
        <person name="Nonoguchi A."/>
        <person name="Yamashita T."/>
      </authorList>
      <dbReference type="PubMed" id="8243060"/>
      <dbReference type="DOI" id="10.1016/0305-0491(93)90317-x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>Hartley</strain>
      <tissue>Bone marrow</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1993" name="DNA Seq." volume="4" first="123" last="128">
      <title>Cloning and characterization of the guinea pig neutrophil cationic peptide-1 and -2 genes.</title>
      <authorList>
        <person name="Nagaoka I."/>
        <person name="Nonoguchi A."/>
        <person name="Yamashita T."/>
      </authorList>
      <dbReference type="PubMed" id="8173076"/>
      <dbReference type="DOI" id="10.3109/10425179309020151"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>Hartley</strain>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1987" name="Infect. Immun." volume="55" first="2281" last="2286">
      <title>Purification, primary structure, and antimicrobial activities of a guinea pig neutrophil defensin.</title>
      <authorList>
        <person name="Selsted M.E."/>
        <person name="Harwig S.S.L."/>
      </authorList>
      <dbReference type="PubMed" id="3623703"/>
      <dbReference type="DOI" id="10.1128/iai.55.9.2281-2286.1987"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 63-93</scope>
    <source>
      <tissue>Peritoneal neutrophil</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1991" name="Biochem. Biophys. Res. Commun." volume="180" first="558" last="565">
      <title>Isolation and characterization of corticostatic peptides from guinea pig bone marrow.</title>
      <authorList>
        <person name="Hu J."/>
        <person name="Bennett H.P.J."/>
        <person name="Lazure C."/>
        <person name="Solomon S."/>
      </authorList>
      <dbReference type="PubMed" id="1659400"/>
      <dbReference type="DOI" id="10.1016/s0006-291x(05)81101-5"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 63-93</scope>
    <source>
      <tissue>Bone marrow</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1989" name="Infect. Immun." volume="57" first="2405" last="2409">
      <title>Purification, primary structure, and biological activity of guinea pig neutrophil cationic peptides.</title>
      <authorList>
        <person name="Yamashita T."/>
        <person name="Saito K."/>
      </authorList>
      <dbReference type="PubMed" id="2473036"/>
      <dbReference type="DOI" id="10.1128/iai.57.8.2405-2409.1989"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 63-93</scope>
    <source>
      <tissue>Neutrophil</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Has antibiotic, anti-fungi and antiviral activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Bone marrow.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the alpha-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="D14118">
    <property type="protein sequence ID" value="BAA03181.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D37972">
    <property type="protein sequence ID" value="BAA07190.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="D14120">
    <property type="protein sequence ID" value="BAA03183.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001166528.1">
    <property type="nucleotide sequence ID" value="NM_001173057.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q64365"/>
  <dbReference type="SMR" id="Q64365"/>
  <dbReference type="GeneID" id="100192401"/>
  <dbReference type="KEGG" id="cpoc:100192401"/>
  <dbReference type="CTD" id="100192401"/>
  <dbReference type="InParanoid" id="Q64365"/>
  <dbReference type="OrthoDB" id="5266470at2759"/>
  <dbReference type="Proteomes" id="UP000005447">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031012">
    <property type="term" value="C:extracellular matrix"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071222">
    <property type="term" value="P:cellular response to lipopolysaccharide"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051607">
    <property type="term" value="P:defense response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051673">
    <property type="term" value="P:disruption of plasma membrane integrity in another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016327">
    <property type="entry name" value="Alpha-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006081">
    <property type="entry name" value="Alpha-defensin_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002366">
    <property type="entry name" value="Alpha-defensin_N"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006080">
    <property type="entry name" value="Beta/alpha-defensin_C"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876">
    <property type="entry name" value="ALPHA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11876:SF28">
    <property type="entry name" value="ALPHA-DEFENSIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00323">
    <property type="entry name" value="Defensin_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00879">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001875">
    <property type="entry name" value="Alpha-defensin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01418">
    <property type="entry name" value="Defensin_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00048">
    <property type="entry name" value="DEFSN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00269">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0051">Antiviral defense</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000006767" evidence="3 4 5">
    <location>
      <begin position="20"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006768" description="Neutrophil cationic peptide 1">
    <location>
      <begin position="63"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="65"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="67"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="72"/>
      <end position="92"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="1659400"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="2473036"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="3623703"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="93" mass="10478" checksum="6D5AAC5A1EBEB8AE" modified="1996-11-01" version="1" precursor="true">MRTVPLFAACLLLTLMAQAEPLPRAADHSDTKMKGDREDHVAVISFWEEESTSLQDAGAGAGRRCICTTRTCRFPYRRLGTCIFQNRVYTFCC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>