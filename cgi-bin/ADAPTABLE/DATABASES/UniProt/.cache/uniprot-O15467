<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1999-07-15" modified="2024-11-27" version="184" xmlns="http://uniprot.org/uniprot">
  <accession>O15467</accession>
  <accession>Q4KKU0</accession>
  <name>CCL16_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>C-C motif chemokine 16</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Chemokine CC-4</fullName>
      <shortName>HCC-4</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Chemokine LEC</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>IL-10-inducible chemokine</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>LCC-1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Liver-expressed chemokine</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Lymphocyte and monocyte chemoattractant</fullName>
      <shortName>LMC</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Monotactin-1</fullName>
      <shortName>MTN-1</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>NCC-4</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Small-inducible cytokine A16</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CCL16</name>
    <name type="synonym">ILINCK</name>
    <name type="synonym">NCC4</name>
    <name type="synonym">SCYA16</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Blood" volume="91" first="4242" last="4247">
      <title>Characterization of a novel CC chemokine, HCC-4, whose expression is increased by interleukin-10.</title>
      <authorList>
        <person name="Hedrick J.A."/>
        <person name="Helms A."/>
        <person name="Vicari A."/>
        <person name="Zlotnik A."/>
      </authorList>
      <dbReference type="PubMed" id="9596672"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF N-TERMINUS</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="Biochim. Biophys. Acta" volume="1396" first="273" last="277">
      <title>Isolation of cDNA encoding a novel human CC chemokine NCC-4/LEC.</title>
      <authorList>
        <person name="Shoudai K."/>
        <person name="Hieshima K."/>
        <person name="Fukuda S."/>
        <person name="Iio M."/>
        <person name="Miura R."/>
        <person name="Imai T."/>
        <person name="Yoshie O."/>
        <person name="Nomiyama H."/>
      </authorList>
      <dbReference type="PubMed" id="9545580"/>
      <dbReference type="DOI" id="10.1016/s0167-4781(97)00235-2"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1999" name="J. Interferon Cytokine Res." volume="19" first="227" last="234">
      <title>Organization of the chemokine gene cluster on human chromosome 17q11.2 containing the genes for CC chemokine MPIF-1, HCC-2, LEC, and RANTES.</title>
      <authorList>
        <person name="Nomiyama H."/>
        <person name="Fukuda S."/>
        <person name="Iio M."/>
        <person name="Tanase S."/>
        <person name="Miura R."/>
        <person name="Yoshie O."/>
      </authorList>
      <dbReference type="PubMed" id="10213461"/>
      <dbReference type="DOI" id="10.1089/107999099314153"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1998" name="Biochem. Biophys. Res. Commun." volume="247" first="217" last="222">
      <title>Isolation and characterization of LMC, a novel lymphocyte and monocyte chemoattractant human CC chemokine, with myelosuppressive activity.</title>
      <authorList>
        <person name="Youn B.-S."/>
        <person name="Zhang S."/>
        <person name="Broxmeyer H.E."/>
        <person name="Antol K."/>
        <person name="Fraser M.J. Jr."/>
        <person name="Hangoc G."/>
        <person name="Kwon B.S."/>
      </authorList>
      <dbReference type="PubMed" id="9642106"/>
      <dbReference type="DOI" id="10.1006/bbrc.1998.8762"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1999" name="DNA Cell Biol." volume="18" first="275" last="283">
      <title>Genomic organization of the genes for human and mouse CC chemokine LEC.</title>
      <authorList>
        <person name="Fukuda S."/>
        <person name="Hanano Y."/>
        <person name="Iio M."/>
        <person name="Miura R."/>
        <person name="Yoshie O."/>
        <person name="Nomiyama H."/>
      </authorList>
      <dbReference type="PubMed" id="10235110"/>
      <dbReference type="DOI" id="10.1089/104454999315330"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2000" name="Cytokine" volume="12" first="101" last="109">
      <title>Cloning, characterization and genomic organization of LCC-1 (scya16), a novel human CC chemokine expressed in liver.</title>
      <authorList>
        <person name="Yang J.-Y."/>
        <person name="Spanaus K.S."/>
        <person name="Widmer U."/>
      </authorList>
      <dbReference type="PubMed" id="10671294"/>
      <dbReference type="DOI" id="10.1006/cyto.1999.0548"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <source>
      <tissue>Liver</tissue>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
  </reference>
  <comment type="function">
    <text>Shows chemotactic activity for lymphocytes and monocytes but not neutrophils. Also shows potent myelosuppressive activity, suppresses proliferation of myeloid progenitor cells. Recombinant SCYA16 shows chemotactic activity for monocytes and THP-1 monocytes, but not for resting lymphocytes and neutrophils. Induces a calcium flux in THP-1 cells that were desensitized by prior expression to RANTES.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-12204739">
      <id>O15467</id>
    </interactant>
    <interactant intactId="EBI-2848366">
      <id>P13501</id>
      <label>CCL5</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-12204739">
      <id>O15467</id>
    </interactant>
    <interactant intactId="EBI-947187">
      <id>Q9UHD9</id>
      <label>UBQLN2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Mainly expressed in liver, also found in spleen and thymus. Highly expressed in LPS- and IFN-gamma-activated monocytes, weakly in some lymphocytes, including natural killer cells, gamma-delta T-cells, and some T-cell clones.</text>
  </comment>
  <comment type="induction">
    <text>By IL10/interleukin-10.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the intercrine beta (chemokine CC) family.</text>
  </comment>
  <comment type="online information" name="Wikipedia">
    <link uri="https://en.wikipedia.org/wiki/CCL16"/>
    <text>CCL16 entry</text>
  </comment>
  <dbReference type="EMBL" id="U91746">
    <property type="protein sequence ID" value="AAC05587.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB007454">
    <property type="protein sequence ID" value="BAA24057.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF088219">
    <property type="protein sequence ID" value="AAC63330.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF055467">
    <property type="protein sequence ID" value="AAC28844.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB018249">
    <property type="protein sequence ID" value="BAA35138.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF039954">
    <property type="protein sequence ID" value="AAC97450.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF039955">
    <property type="protein sequence ID" value="AAC97451.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC099662">
    <property type="protein sequence ID" value="AAH99662.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS11303.1"/>
  <dbReference type="PIR" id="JE0177">
    <property type="entry name" value="JE0177"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_004581.1">
    <property type="nucleotide sequence ID" value="NM_004590.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_005258077.1">
    <property type="nucleotide sequence ID" value="XM_005258020.4"/>
  </dbReference>
  <dbReference type="PDB" id="5LTL">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.45 A"/>
    <property type="chains" value="A/B=24-120"/>
  </dbReference>
  <dbReference type="PDB" id="7SCT">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.84 A"/>
    <property type="chains" value="B=23-120"/>
  </dbReference>
  <dbReference type="PDB" id="8FK9">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.70 A"/>
    <property type="chains" value="C/D=23-120"/>
  </dbReference>
  <dbReference type="PDBsum" id="5LTL"/>
  <dbReference type="PDBsum" id="7SCT"/>
  <dbReference type="PDBsum" id="8FK9"/>
  <dbReference type="AlphaFoldDB" id="O15467"/>
  <dbReference type="SMR" id="O15467"/>
  <dbReference type="BioGRID" id="112263">
    <property type="interactions" value="8"/>
  </dbReference>
  <dbReference type="DIP" id="DIP-5831N"/>
  <dbReference type="IntAct" id="O15467">
    <property type="interactions" value="8"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000478024"/>
  <dbReference type="BioMuta" id="CCL16"/>
  <dbReference type="MassIVE" id="O15467"/>
  <dbReference type="PaxDb" id="9606-ENSP00000478024"/>
  <dbReference type="PeptideAtlas" id="O15467"/>
  <dbReference type="ProteomicsDB" id="48683"/>
  <dbReference type="TopDownProteomics" id="O15467"/>
  <dbReference type="Antibodypedia" id="73562">
    <property type="antibodies" value="499 antibodies from 27 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="6360"/>
  <dbReference type="Ensembl" id="ENST00000611905.2">
    <property type="protein sequence ID" value="ENSP00000478024.1"/>
    <property type="gene ID" value="ENSG00000275152.5"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000621559.2">
    <property type="protein sequence ID" value="ENSP00000482168.1"/>
    <property type="gene ID" value="ENSG00000275095.2"/>
  </dbReference>
  <dbReference type="GeneID" id="6360"/>
  <dbReference type="KEGG" id="hsa:6360"/>
  <dbReference type="MANE-Select" id="ENST00000611905.2">
    <property type="protein sequence ID" value="ENSP00000478024.1"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_004590.4"/>
    <property type="RefSeq protein sequence ID" value="NP_004581.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc002hkl.4">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:10614"/>
  <dbReference type="CTD" id="6360"/>
  <dbReference type="DisGeNET" id="6360"/>
  <dbReference type="GeneCards" id="CCL16"/>
  <dbReference type="HGNC" id="HGNC:10614">
    <property type="gene designation" value="CCL16"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000275152">
    <property type="expression patterns" value="Tissue enriched (liver)"/>
  </dbReference>
  <dbReference type="MIM" id="601394">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_O15467"/>
  <dbReference type="OpenTargets" id="ENSG00000275152"/>
  <dbReference type="PharmGKB" id="PA35547"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000275152"/>
  <dbReference type="eggNOG" id="ENOG502TDPW">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT01120000271901"/>
  <dbReference type="InParanoid" id="O15467"/>
  <dbReference type="OMA" id="DNWVQEY"/>
  <dbReference type="OrthoDB" id="4265193at2759"/>
  <dbReference type="PhylomeDB" id="O15467"/>
  <dbReference type="TreeFam" id="TF334888"/>
  <dbReference type="PathwayCommons" id="O15467"/>
  <dbReference type="Reactome" id="R-HSA-380108">
    <property type="pathway name" value="Chemokine receptors bind chemokines"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-418594">
    <property type="pathway name" value="G alpha (i) signalling events"/>
  </dbReference>
  <dbReference type="SignaLink" id="O15467"/>
  <dbReference type="BioGRID-ORCS" id="6360">
    <property type="hits" value="13 hits in 1102 CRISPR screens"/>
  </dbReference>
  <dbReference type="GenomeRNAi" id="6360"/>
  <dbReference type="Pharos" id="O15467">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:O15467"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 17"/>
  </dbReference>
  <dbReference type="RNAct" id="O15467">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000275152">
    <property type="expression patterns" value="Expressed in liver and 71 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="O15467">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048020">
    <property type="term" value="F:CCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042056">
    <property type="term" value="F:chemoattractant activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007154">
    <property type="term" value="P:cell communication"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007267">
    <property type="term" value="P:cell-cell signaling"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006935">
    <property type="term" value="P:chemotaxis"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048245">
    <property type="term" value="P:eosinophil chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd00272">
    <property type="entry name" value="Chemokine_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000034">
    <property type="entry name" value="C-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000827">
    <property type="entry name" value="Chemokine_CC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF21">
    <property type="entry name" value="C-C MOTIF CHEMOKINE 16"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00472">
    <property type="entry name" value="SMALL_CYTOKINES_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005209" description="C-C motif chemokine 16">
    <location>
      <begin position="24"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="38"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="35"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="48"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="51"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="59"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="64"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="74"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="82"/>
      <end position="89"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="9596672"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="5LTL"/>
    </source>
  </evidence>
  <sequence length="120" mass="13600" checksum="373D73016134D894" modified="1998-01-01" version="1" precursor="true">MKVSEAALSLLVLILIITSASRSQPKVPEWVNTPSTCCLKYYEKVLPRRLVVGYRKALNCHLPAIIFVTKRNREVCTNPNDDWVQEYIKDPNLPLLPTRNLSTVKIITAKNGQPQLLNSQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>