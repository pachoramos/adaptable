<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-05-08" modified="2024-01-24" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>D0NXM3</accession>
  <name>SFI7_PHYIT</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">RxLR effector protein SFI7</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Suppressor of early Flg22-induced immune response 7</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="4" type="primary">SFI7</name>
    <name type="ORF">PITG_18215</name>
  </gene>
  <organism>
    <name type="scientific">Phytophthora infestans (strain T30-4)</name>
    <name type="common">Potato late blight agent</name>
    <dbReference type="NCBI Taxonomy" id="403677"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Sar</taxon>
      <taxon>Stramenopiles</taxon>
      <taxon>Oomycota</taxon>
      <taxon>Peronosporales</taxon>
      <taxon>Peronosporaceae</taxon>
      <taxon>Phytophthora</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="Nature" volume="461" first="393" last="398">
      <title>Genome sequence and analysis of the Irish potato famine pathogen Phytophthora infestans.</title>
      <authorList>
        <consortium name="The Broad Institute Genome Sequencing Platform"/>
        <person name="Haas B.J."/>
        <person name="Kamoun S."/>
        <person name="Zody M.C."/>
        <person name="Jiang R.H."/>
        <person name="Handsaker R.E."/>
        <person name="Cano L.M."/>
        <person name="Grabherr M."/>
        <person name="Kodira C.D."/>
        <person name="Raffaele S."/>
        <person name="Torto-Alalibo T."/>
        <person name="Bozkurt T.O."/>
        <person name="Ah-Fong A.M."/>
        <person name="Alvarado L."/>
        <person name="Anderson V.L."/>
        <person name="Armstrong M.R."/>
        <person name="Avrova A."/>
        <person name="Baxter L."/>
        <person name="Beynon J."/>
        <person name="Boevink P.C."/>
        <person name="Bollmann S.R."/>
        <person name="Bos J.I."/>
        <person name="Bulone V."/>
        <person name="Cai G."/>
        <person name="Cakir C."/>
        <person name="Carrington J.C."/>
        <person name="Chawner M."/>
        <person name="Conti L."/>
        <person name="Costanzo S."/>
        <person name="Ewan R."/>
        <person name="Fahlgren N."/>
        <person name="Fischbach M.A."/>
        <person name="Fugelstad J."/>
        <person name="Gilroy E.M."/>
        <person name="Gnerre S."/>
        <person name="Green P.J."/>
        <person name="Grenville-Briggs L.J."/>
        <person name="Griffith J."/>
        <person name="Grunwald N.J."/>
        <person name="Horn K."/>
        <person name="Horner N.R."/>
        <person name="Hu C.H."/>
        <person name="Huitema E."/>
        <person name="Jeong D.H."/>
        <person name="Jones A.M."/>
        <person name="Jones J.D."/>
        <person name="Jones R.W."/>
        <person name="Karlsson E.K."/>
        <person name="Kunjeti S.G."/>
        <person name="Lamour K."/>
        <person name="Liu Z."/>
        <person name="Ma L."/>
        <person name="Maclean D."/>
        <person name="Chibucos M.C."/>
        <person name="McDonald H."/>
        <person name="McWalters J."/>
        <person name="Meijer H.J."/>
        <person name="Morgan W."/>
        <person name="Morris P.F."/>
        <person name="Munro C.A."/>
        <person name="O'Neill K."/>
        <person name="Ospina-Giraldo M."/>
        <person name="Pinzon A."/>
        <person name="Pritchard L."/>
        <person name="Ramsahoye B."/>
        <person name="Ren Q."/>
        <person name="Restrepo S."/>
        <person name="Roy S."/>
        <person name="Sadanandom A."/>
        <person name="Savidor A."/>
        <person name="Schornack S."/>
        <person name="Schwartz D.C."/>
        <person name="Schumann U.D."/>
        <person name="Schwessinger B."/>
        <person name="Seyer L."/>
        <person name="Sharpe T."/>
        <person name="Silvar C."/>
        <person name="Song J."/>
        <person name="Studholme D.J."/>
        <person name="Sykes S."/>
        <person name="Thines M."/>
        <person name="van de Vondervoort P.J."/>
        <person name="Phuntumart V."/>
        <person name="Wawra S."/>
        <person name="Weide R."/>
        <person name="Win J."/>
        <person name="Young C."/>
        <person name="Zhou S."/>
        <person name="Fry W."/>
        <person name="Meyers B.C."/>
        <person name="van West P."/>
        <person name="Ristaino J."/>
        <person name="Govers F."/>
        <person name="Birch P.R."/>
        <person name="Whisson S.C."/>
        <person name="Judelson H.S."/>
        <person name="Nusbaum C."/>
      </authorList>
      <dbReference type="PubMed" id="19741609"/>
      <dbReference type="DOI" id="10.1038/nature08358"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>T30-4</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2014" name="PLoS Pathog." volume="10" first="E1004057" last="E1004057">
      <title>Functionally redundant RXLR effectors from Phytophthora infestans act at different steps to suppress early flg22-triggered immunity.</title>
      <authorList>
        <person name="Zheng X."/>
        <person name="McLellan H."/>
        <person name="Fraiture M."/>
        <person name="Liu X."/>
        <person name="Boevink P.C."/>
        <person name="Gilroy E.M."/>
        <person name="Chen Y."/>
        <person name="Kandel K."/>
        <person name="Sessa G."/>
        <person name="Birch P.R."/>
        <person name="Brunner F."/>
      </authorList>
      <dbReference type="PubMed" id="24763622"/>
      <dbReference type="DOI" id="10.1371/journal.ppat.1004057"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Effector that suppresses flg22-induced post-translational MAP kinase activation in tomato but not in Arabidopsis. The perception of highly conserved pathogen- or microbe-associated molecular patterns (PAMPs/MAMPs), such as flg22, triggers converging signaling pathways recruiting MAP kinase cascades and inducing transcriptional re-programming, yielding a generic antimicrobial response (PubMed:24763622). Also partially attenuates INF1-triggered cell death (PubMed:24763622).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="3">Host cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="3">Host cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="6">The RxLR-dEER motif acts to carry the protein into the host cell cytoplasm through binding to cell surface phosphatidylinositol-3-phosphate.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the RxLR effector family.</text>
  </comment>
  <dbReference type="EMBL" id="DS028183">
    <property type="protein sequence ID" value="EEY67823.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_002997848.1">
    <property type="nucleotide sequence ID" value="XM_002997802.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="D0NXM3"/>
  <dbReference type="SMR" id="D0NXM3"/>
  <dbReference type="STRING" id="403677.D0NXM3"/>
  <dbReference type="GlyCosmos" id="D0NXM3">
    <property type="glycosylation" value="1 site, No reported glycans"/>
  </dbReference>
  <dbReference type="EnsemblProtists" id="PITG_18215T0">
    <property type="protein sequence ID" value="PITG_18215T0"/>
    <property type="gene ID" value="PITG_18215"/>
  </dbReference>
  <dbReference type="GeneID" id="9463553"/>
  <dbReference type="KEGG" id="pif:PITG_18215"/>
  <dbReference type="VEuPathDB" id="FungiDB:PITG_18215"/>
  <dbReference type="eggNOG" id="ENOG502RGPZ">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_120310_0_0_1"/>
  <dbReference type="InParanoid" id="D0NXM3"/>
  <dbReference type="OMA" id="QNWDALV"/>
  <dbReference type="OrthoDB" id="70462at2759"/>
  <dbReference type="PHI-base" id="PHI:10635"/>
  <dbReference type="PHI-base" id="PHI:4206"/>
  <dbReference type="Proteomes" id="UP000006643">
    <property type="component" value="Partially assembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030430">
    <property type="term" value="C:host cell cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0020002">
    <property type="term" value="C:host cell plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-1032">Host cell membrane</keyword>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1043">Host membrane</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5003013718" description="RxLR effector protein SFI7">
    <location>
      <begin position="23"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="RxLR-dEER" evidence="6">
    <location>
      <begin position="44"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="2">
    <location>
      <position position="32"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00498"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="24763622"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="24763622"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="24763622"/>
    </source>
  </evidence>
  <sequence length="148" mass="16443" checksum="E133290C0E68657E" modified="2009-12-15" version="1" precursor="true">MRAYFVLLVAATAILTYGGATATYSTSKGEMNLTGTVENNRPTRSLRVAPSGGNGEERSWSTIYGISRSKAETVRDWLMPRLNQGMDVQALAREMGITSRQAATQHQNWDALVKYLKMYNYAVRGEKMSKSMAESVLLHNVLTAKNNF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>