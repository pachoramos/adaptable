<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-01-20" modified="2023-05-03" version="26" xmlns="http://uniprot.org/uniprot">
  <accession>P86018</accession>
  <name>ES1R_PELRI</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Esculentin-1R</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pelophylax ridibundus</name>
    <name type="common">Marsh frog</name>
    <name type="synonym">Rana ridibunda</name>
    <dbReference type="NCBI Taxonomy" id="8406"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Pelophylax</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2008" name="Rapid Commun. Mass Spectrom." volume="22" first="3517" last="3525">
      <title>De novo sequencing of peptides secreted by the skin glands of the caucasian green frog Rana ridibunda.</title>
      <authorList>
        <person name="Samgina T.Y."/>
        <person name="Artemenko K.A."/>
        <person name="Gorshkov V.A."/>
        <person name="Ogourtsov S.V."/>
        <person name="Zubarev R.A."/>
        <person name="Lebedev A.T."/>
      </authorList>
      <dbReference type="PubMed" id="18855342"/>
      <dbReference type="DOI" id="10.1002/rcm.3759"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>DISULFIDE BOND</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Shows antibacterial activity against representative Gram-negative and Gram-positive bacterial species, and hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="4771.0" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Esculentin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P86018"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000361073" description="Esculentin-1R" evidence="3">
    <location>
      <begin position="1"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="40"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="3">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18855342"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="18855342"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="18855342"/>
    </source>
  </evidence>
  <sequence length="46" mass="4776" checksum="CDCAD67F71DFE8EF" modified="2009-01-20" version="1">GIFSKLAGKKLKNLLISGLKSVGKEVGMDVVRTGIDIAGCKIKGEC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>