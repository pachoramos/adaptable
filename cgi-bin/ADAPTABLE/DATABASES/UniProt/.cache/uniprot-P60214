<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-01-16" modified="2024-11-27" version="59" xmlns="http://uniprot.org/uniprot">
  <accession>P60214</accession>
  <accession>A0A1E1WVS8</accession>
  <accession>H1ZZH0</accession>
  <name>SCX1_TITOB</name>
  <protein>
    <recommendedName>
      <fullName evidence="8 9">Beta-mammal/insect toxin To1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="8">NaTx12.1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="6 7">Toxin Tc49b</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Tityus obscurus</name>
    <name type="common">Amazonian scorpion</name>
    <name type="synonym">Tityus cambridgei</name>
    <dbReference type="NCBI Taxonomy" id="1221240"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Tityus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2012" name="PLoS ONE" volume="7" first="E30478" last="E30478">
      <title>Identification and phylogenetic analysis of Tityus pachyurus and Tityus obscurus novel putative Na+-channel scorpion toxins.</title>
      <authorList>
        <person name="Guerrero-Vargas J.A."/>
        <person name="Mourao C.B."/>
        <person name="Quintero-Hernandez V."/>
        <person name="Possani L.D."/>
        <person name="Schwartz E.F."/>
      </authorList>
      <dbReference type="PubMed" id="22355312"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0030478"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>AMIDATION AT LYS-84</scope>
    <scope>NOMENCLATURE</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference evidence="12" key="2">
    <citation type="journal article" date="2018" name="PLoS ONE" volume="13" first="e0193739" last="e0193739">
      <title>Proteomic endorsed transcriptomic profiles of venom glands from Tityus obscurus and T. serrulatus scorpions.</title>
      <authorList>
        <person name="de Oliveira U.C."/>
        <person name="Nishiyama M.Y. Jr."/>
        <person name="Dos Santos M.B.V."/>
        <person name="Santos-da-Silva A.P."/>
        <person name="Chalkidis H.M."/>
        <person name="Souza-Imberg A."/>
        <person name="Candido D.M."/>
        <person name="Yamanouye N."/>
        <person name="Dorce V.A.C."/>
        <person name="Junqueira-de-Azevedo I.L.M."/>
      </authorList>
      <dbReference type="PubMed" id="29561852"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0193739"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Telson</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Toxicon" volume="40" first="557" last="562">
      <title>Scorpion toxins from Tityus cambridgei that affect Na(+)-channels.</title>
      <authorList>
        <person name="Batista C.V.F."/>
        <person name="Zamudio F.Z."/>
        <person name="Lucas S."/>
        <person name="Fox J.W."/>
        <person name="Frau A."/>
        <person name="Prestipino G."/>
        <person name="Possani L.D."/>
      </authorList>
      <dbReference type="PubMed" id="11821128"/>
      <dbReference type="DOI" id="10.1016/s0041-0101(01)00252-5"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-84</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>BIOASSAY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2004" name="J. Chromatogr. B" volume="803" first="55" last="66">
      <title>Proteomics of the venom from the Amazonian scorpion Tityus cambridgei and the role of prolines on mass spectrometry analysis of toxins.</title>
      <authorList>
        <person name="Batista C.V.F."/>
        <person name="del Pozo L."/>
        <person name="Zamudio F.Z."/>
        <person name="Contreras S."/>
        <person name="Becerril B."/>
        <person name="Wanke E."/>
        <person name="Possani L.D."/>
      </authorList>
      <dbReference type="PubMed" id="15025998"/>
      <dbReference type="DOI" id="10.1016/j.jchromb.2003.09.002"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-30</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2019" name="Biochim. Biophys. Acta" volume="1861" first="142" last="150">
      <title>Electrophysiological characterization of Tityus obscurus beta toxin 1 (To1) on Na+-channel isoforms.</title>
      <authorList>
        <person name="Tibery D.V."/>
        <person name="Campos L.A."/>
        <person name="Mourao C.B.F."/>
        <person name="Peigneur S."/>
        <person name="Carvalho A.C."/>
        <person name="Tytgat J."/>
        <person name="Schwartz E.F."/>
      </authorList>
      <dbReference type="PubMed" id="30463697"/>
      <dbReference type="DOI" id="10.1016/j.bbamem.2018.08.005"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 5">Beta toxin that show multiple effects. It enhances the open probability at more negative potentials of human Nav1.3/SCN3A and Nav1.6/SCN8A, of the insect channel BgNaV1 and of arachnid VdNaV1 channel (PubMed:30463697). It promotes an important shift in slow inactivation processes as a function of the prepulse voltage in human Nav1.3/SCN3A and Nav1.6/SCN8A and a small shift in Nav1.1/SCN1A, Nav1.2/SCN2A and Nav1.4/SCN4A (PubMed:30463697). Finally, it reduces the peak of sodium currents in Nav1.3/SCN3A (80% inhibition at 70 nM of toxin), Nav1.6/SCN8A (55.3%), Nav1.1/SCN1A (53.3%), Nav1.5/SCN5A (46.7%), Nav1.2/SCN2A (42.7%) and Nav1.4/SCN4A (20%) voltage-gated sodium channels (PubMed:30463697). It has also been shown to affect the sodium current permeability of rat cerebellum granular cells in a partially reversible manner (PubMed:11821128). In vivo, an intraperitoneal injection (20 ug) into mice produces excitability, respiratory problems, convulsions and death, within the first 30 minutes after injection (PubMed:11821128).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2 3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="11">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="10">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="mass spectrometry" mass="7405.0" method="Electrospray" evidence="2"/>
  <comment type="mass spectrometry" mass="7405.6" method="Electrospray" evidence="3"/>
  <comment type="mass spectrometry" mass="7400.26" method="MALDI" evidence="5">
    <text>Monoisotopic mass.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2 5">Negative results: does not modify the function of the Shaker B potassium channels (PubMed:11821128). Does not reduce the peak of sodium currents in Nav1.7/SCN9A, insect BgNav1 and arachnid VdNav1 (PubMed:30463697). It does not promote a shift in slow inactivation processes as a function of the prepulse voltage in human Nav1.5/SCN5A and Nav1.7/SCN9A, insect BgNav1 and arachnid VdNav1 (PubMed:30463697).</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="HE585224">
    <property type="protein sequence ID" value="CCD31418.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="GEMQ01000091">
    <property type="protein sequence ID" value="JAT91098.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P60214"/>
  <dbReference type="SMR" id="P60214"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044489">
    <property type="term" value="P:negative regulation of voltage-gated sodium channel activity in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2 3">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000066811" description="Beta-mammal/insect toxin To1" evidence="2">
    <location>
      <begin position="21"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="22"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="modified residue" description="Lysine amide" evidence="4">
    <location>
      <position position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="36"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="48"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; JAT91098." evidence="10" ref="2">
    <original>K</original>
    <variation>R</variation>
    <location>
      <position position="51"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; JAT91098." evidence="10" ref="2">
    <original>K</original>
    <variation>P</variation>
    <location>
      <position position="74"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; JAT91098." evidence="10" ref="2">
    <original>S</original>
    <variation>R</variation>
    <location>
      <position position="78"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11821128"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15025998"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="22355312"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="30463697"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="11821128"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="15025998"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="22355312"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="30463697"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="11821128"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="12">
    <source>
      <dbReference type="EMBL" id="JAT91098.1"/>
    </source>
  </evidence>
  <sequence length="86" mass="10008" checksum="3386396FD66CB035" modified="2013-05-01" version="2" precursor="true">MTRFVLFISCFFLIDMIVECKKEGYLVGNDGCKYGCITRPHQYCVHECELKKGTDGYCAYWLACYCYNMPDWVKTWSSATNKCKGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>