<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2016-07-06" modified="2024-03-27" version="24" xmlns="http://uniprot.org/uniprot">
  <accession>A0A0H3NGF1</accession>
  <name>YDGT_SALTS</name>
  <protein>
    <recommendedName>
      <fullName>Transcription modulator YdgT</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>H-NS/StpA-binding protein 2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>OriC-binding nucleoid-associated protein</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ydgT</name>
    <name type="synonym">cnu</name>
    <name type="ordered locus">SL1344_1393</name>
  </gene>
  <organism>
    <name type="scientific">Salmonella typhimurium (strain SL1344)</name>
    <dbReference type="NCBI Taxonomy" id="216597"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Salmonella</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2012" name="Proc. Natl. Acad. Sci. U.S.A." volume="109" first="E1277" last="E1286">
      <title>The transcriptional landscape and small RNAs of Salmonella enterica serovar Typhimurium.</title>
      <authorList>
        <person name="Kroger C."/>
        <person name="Dillon S.C."/>
        <person name="Cameron A.D."/>
        <person name="Papenfort K."/>
        <person name="Sivasankaran S.K."/>
        <person name="Hokamp K."/>
        <person name="Chao Y."/>
        <person name="Sittka A."/>
        <person name="Hebrard M."/>
        <person name="Handler K."/>
        <person name="Colgan A."/>
        <person name="Leekitcharoenphon P."/>
        <person name="Langridge G.C."/>
        <person name="Lohan A.J."/>
        <person name="Loftus B."/>
        <person name="Lucchini S."/>
        <person name="Ussery D.W."/>
        <person name="Dorman C.J."/>
        <person name="Thomson N.R."/>
        <person name="Vogel J."/>
        <person name="Hinton J.C."/>
      </authorList>
      <dbReference type="PubMed" id="22538806"/>
      <dbReference type="DOI" id="10.1073/pnas.1201061109"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>SL1344</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2007" name="J. Bacteriol." volume="189" first="3669" last="3673">
      <title>Repression of intracellular virulence factors in Salmonella by the Hha and YdgT nucleoid-associated proteins.</title>
      <authorList>
        <person name="Silphaduang U."/>
        <person name="Mascarenhas M."/>
        <person name="Karmali M."/>
        <person name="Coombes B.K."/>
      </authorList>
      <dbReference type="PubMed" id="17307861"/>
      <dbReference type="DOI" id="10.1128/jb.00002-07"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>SL1344</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2009" name="PLoS Genet." volume="5" first="E1000513" last="E1000513">
      <title>Differential regulation of horizontally acquired and core genome genes by the bacterial modulator H-NS.</title>
      <authorList>
        <person name="Banos R.C."/>
        <person name="Vivero A."/>
        <person name="Aznar S."/>
        <person name="Garcia J."/>
        <person name="Pons M."/>
        <person name="Madrid C."/>
        <person name="Juarez A."/>
      </authorList>
      <dbReference type="PubMed" id="19521501"/>
      <dbReference type="DOI" id="10.1371/journal.pgen.1000513"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>SL1344 / SV5015</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="4">Binds to H-NS and modifies the range of genes it silences; H-NS alone silences 'core' genes while the H-NS-Hha complex (and presumably also H-NS-YdgT) silences genes acquired by horizontal gene transfer (PubMed:19521501). Plays a role silencing virulence factors in the absence of factors that induce pathogenicity (PubMed:17307861).</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="1 2">No effect on expression of pathogenicity island 2 (SPI-2) which is usually repressed in culture; derepression of SPI-2 in double hha-ydgT deletions (PubMed:17307861). Double hha-ydgT deletion mutants lead to deregulation of many genes at least 2-fold, most of which are also deregulated in an hns deletion, suggesting there are a large number of hns-regulated genes whose expression is also modulated by Hha and/or YdgT (PubMed:19521501). Upon infection of C57BL/6 mice a single hha mutant has 10-100-fold reduced virulence compared to wild-type, while a double hha-ydgT deletion is 6 orders of magnitude less virulent (PubMed:17307861).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the Hha/YmoA/Cnu family.</text>
  </comment>
  <dbReference type="EMBL" id="FQ312003">
    <property type="protein sequence ID" value="CBW17489.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A0H3NGF1"/>
  <dbReference type="SMR" id="A0A0H3NGF1"/>
  <dbReference type="KEGG" id="sey:SL1344_1393"/>
  <dbReference type="PATRIC" id="fig|216597.6.peg.1547"/>
  <dbReference type="HOGENOM" id="CLU_190629_0_0_6"/>
  <dbReference type="Proteomes" id="UP000008962">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.1280.40">
    <property type="entry name" value="HHA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007985">
    <property type="entry name" value="Hemolysn_expr_modulating_HHA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036666">
    <property type="entry name" value="HHA_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05321">
    <property type="entry name" value="HHA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF68989">
    <property type="entry name" value="Hemolysin expression modulating protein HHA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="chain" id="PRO_0000436900" description="Transcription modulator YdgT">
    <location>
      <begin position="1"/>
      <end position="76"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="17307861"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19521501"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="19521501"/>
    </source>
  </evidence>
  <sequence length="76" mass="9031" checksum="2BBFFB84A84FA7E1" modified="2015-09-16" version="1">MDQLYMTVQDYLLKFRKISSLESLEKLFDHLNYTLTDDMDIVNMYRAADHRRAELVSGGRLFDVGQVPQSVWRYVQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>