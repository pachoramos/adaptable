<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-04-01" modified="2019-12-11" version="46" xmlns="http://uniprot.org/uniprot">
  <accession>P30249</accession>
  <name>TKL3_LOCMI</name>
  <protein>
    <recommendedName>
      <fullName>Locustatachykinin-3</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Locustatachykinin III</fullName>
      <shortName>TK-III</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Locusta migratoria</name>
    <name type="common">Migratory locust</name>
    <dbReference type="NCBI Taxonomy" id="7004"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Orthoptera</taxon>
      <taxon>Caelifera</taxon>
      <taxon>Acrididea</taxon>
      <taxon>Acridomorpha</taxon>
      <taxon>Acridoidea</taxon>
      <taxon>Acrididae</taxon>
      <taxon>Oedipodinae</taxon>
      <taxon>Locusta</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="Regul. Pept." volume="31" first="199" last="212">
      <title>Locustatachykinin III and IV: two additional insect neuropeptides with homology to peptides of the vertebrate tachykinin family.</title>
      <authorList>
        <person name="Schoofs L."/>
        <person name="Holman G.M."/>
        <person name="Hayes T.K."/>
        <person name="Kochansky J.P."/>
        <person name="Nachman R.J."/>
        <person name="de Loof A."/>
      </authorList>
      <dbReference type="PubMed" id="2132575"/>
      <dbReference type="DOI" id="10.1016/0167-0115(90)90006-i"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT ARG-10</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Myoactive peptide. Stimulates the contraction of the oviduct and foregut.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <dbReference type="PIR" id="A60073">
    <property type="entry name" value="ECLQ3M"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044434" description="Locustatachykinin-3">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="modified residue" description="Arginine amide" evidence="1">
    <location>
      <position position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2132575"/>
    </source>
  </evidence>
  <sequence length="10" mass="1065" checksum="C452CD6B59C87DC6" modified="1993-04-01" version="1">APQAGFYGVR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>