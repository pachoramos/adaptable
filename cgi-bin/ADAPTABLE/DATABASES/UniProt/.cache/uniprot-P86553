<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-07-13" modified="2019-12-11" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>P86553</accession>
  <name>ALLTR_ONCFA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Allatotropin-related peptide</fullName>
      <shortName evidence="3">ATRP</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Oncopeltus fasciatus</name>
    <name type="common">Large milkweed bug</name>
    <dbReference type="NCBI Taxonomy" id="7536"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Paraneoptera</taxon>
      <taxon>Hemiptera</taxon>
      <taxon>Heteroptera</taxon>
      <taxon>Panheteroptera</taxon>
      <taxon>Pentatomomorpha</taxon>
      <taxon>Lygaeoidea</taxon>
      <taxon>Lygaeidae</taxon>
      <taxon>Lygaeinae</taxon>
      <taxon>Oncopeltus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2009" name="Peptides" volume="30" first="483" last="488">
      <title>Neuropeptides in Heteroptera: identification of allatotropin-related peptide and tachykinin-related peptides using MALDI-TOF mass spectrometry.</title>
      <authorList>
        <person name="Neupert S."/>
        <person name="Russell W.K."/>
        <person name="Russell D.H."/>
        <person name="Lopez J.D. Jr."/>
        <person name="Predel R."/>
        <person name="Nachman R.J."/>
      </authorList>
      <dbReference type="PubMed" id="19084564"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.11.009"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT PHE-13</scope>
    <source>
      <tissue evidence="2">Ventral nerve cord</tissue>
    </source>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed in the posterior region of the abdominal ventral nerve cord and in the fourth abdominal nerves (at protein level).</text>
  </comment>
  <comment type="mass spectrometry" mass="1366.8" method="MALDI" evidence="2"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000395623" description="Allatotropin-related peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="2">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P21786"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19084564"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="19084564"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="13" mass="1368" checksum="DE1ACAFC482EB72D" modified="2010-07-13" version="1">GFKNVALSTARGF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>