<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-06-21" modified="2023-05-03" version="41" xmlns="http://uniprot.org/uniprot">
  <accession>Q58T83</accession>
  <name>M3H33_BOMMX</name>
  <protein>
    <recommendedName>
      <fullName>Maximins 3/H3 type 3</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Maximin-3</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Maximin-H3</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Bombina maxima</name>
    <name type="common">Giant fire-bellied toad</name>
    <name type="synonym">Chinese red belly toad</name>
    <dbReference type="NCBI Taxonomy" id="161274"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Bombinatoridae</taxon>
      <taxon>Bombina</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Eur. J. Immunol." volume="35" first="1220" last="1229">
      <title>Variety of antimicrobial peptides in the Bombina maxima toad and evidence of their rapid diversification.</title>
      <authorList>
        <person name="Lee W.-H."/>
        <person name="Li Y."/>
        <person name="Lai R."/>
        <person name="Li S."/>
        <person name="Zhang Y."/>
        <person name="Wang W."/>
      </authorList>
      <dbReference type="PubMed" id="15770703"/>
      <dbReference type="DOI" id="10.1002/eji.200425615"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>AMIDATION AT ILE-143</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2002" name="Peptides" volume="23" first="427" last="435">
      <title>Antimicrobial peptides from skin secretions of Chinese red belly toad Bombina maxima.</title>
      <authorList>
        <person name="Lai R."/>
        <person name="Zheng Y.-T."/>
        <person name="Shen J.-H."/>
        <person name="Liu G.-J."/>
        <person name="Liu H."/>
        <person name="Lee W.-H."/>
        <person name="Tang S.-Z."/>
        <person name="Zhang Y."/>
      </authorList>
      <dbReference type="PubMed" id="11835991"/>
      <dbReference type="DOI" id="10.1016/s0196-9781(01)00641-6"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 45-71 AND 124-143</scope>
    <scope>AMIDATION AT ILE-143</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>FUNCTION OF MAXIMIN-3 AND MAXIMIN-H3</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Maximin-3 shows antibacterial activity against both Gram-positive and Gram-negative bacteria. It shows also antimicrobial activity against the fungus C.albicans, but not against A.flavus nor P.uticale. It has little hemolytic activity. It possess a significant cytotoxicity against tumor cell lines. It possess a significant anti-HIV activity. It shows high spermicidal activity.</text>
  </comment>
  <comment type="function">
    <text evidence="3">Maximin-H3 shows antibacterial activity against both Gram-positive and Gram-negative bacteria. It shows also antimicrobial activity against the fungus C.albicans. Shows strong hemolytic activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2698.0" method="FAB" evidence="3">
    <molecule>Maximin-3</molecule>
  </comment>
  <comment type="mass spectrometry" mass="1944.0" method="FAB" evidence="3">
    <molecule>Maximin-H3</molecule>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the bombinin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY848977">
    <property type="protein sequence ID" value="AAX50198.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY849013">
    <property type="protein sequence ID" value="AAX50234.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY848975">
    <property type="protein sequence ID" value="AAX50196.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q58T83"/>
  <dbReference type="SMR" id="Q58T83"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007962">
    <property type="entry name" value="Bombinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05298">
    <property type="entry name" value="Bombinin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0354">Hemolysis</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003128" evidence="1">
    <location>
      <begin position="19"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003129" description="Maximin-3">
    <location>
      <begin position="45"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003130" evidence="3">
    <location>
      <begin position="74"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003131" description="Maximin-H3">
    <location>
      <begin position="124"/>
      <end position="143"/>
    </location>
  </feature>
  <feature type="modified residue" description="Isoleucine amide" evidence="3 4">
    <location>
      <position position="143"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="11835991"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="15770703"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="144" mass="16050" checksum="CE865C1253E5EFF8" modified="2005-04-26" version="1" precursor="true">MNFKYIVAVSFLIASAYARSVQNDEQSLSQRDVLEEESLREIRGIGGKILSGLKTALKGAAKELASTYLHRKRTAEEHEVMKRLEAVMRDLDSLDYPEEASERETRGFNQDEIANLFTKKEKRILGPVLGLVGNALGGLIKKIG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>