<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2015-10-14" modified="2022-08-03" version="9" xmlns="http://uniprot.org/uniprot">
  <accession>P0CT85</accession>
  <name>FAEA_TALSN</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Feruloyl esterase A</fullName>
      <ecNumber evidence="2">3.1.1.73</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Ferulic acid esterase A</fullName>
      <shortName>FAE</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Talaromyces stipitatus (strain ATCC 10500 / CBS 375.48 / QM 6759 / NRRL 1006)</name>
    <name type="common">Penicillium stipitatum</name>
    <dbReference type="NCBI Taxonomy" id="441959"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Ascomycota</taxon>
      <taxon>Pezizomycotina</taxon>
      <taxon>Eurotiomycetes</taxon>
      <taxon>Eurotiomycetidae</taxon>
      <taxon>Eurotiales</taxon>
      <taxon>Trichocomaceae</taxon>
      <taxon>Talaromyces</taxon>
      <taxon>Talaromyces sect. Talaromyces</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="J. Biotechnol." volume="108" first="227" last="241">
      <title>The feruloyl esterase system of Talaromyces stipitatus: production of three discrete feruloyl esterases, including a novel enzyme, TsFaeC, with a broad substrate specificity.</title>
      <authorList>
        <person name="Garcia-Conesa M.T."/>
        <person name="Crepin V.F."/>
        <person name="Goldson A.J."/>
        <person name="Williamson G."/>
        <person name="Cummings N.J."/>
        <person name="Connerton I.F."/>
        <person name="Faulds C.B."/>
        <person name="Kroon P.A."/>
      </authorList>
      <dbReference type="PubMed" id="15006424"/>
      <dbReference type="DOI" id="10.1016/j.jbiotec.2003.12.003"/>
    </citation>
    <scope>PARTIAL PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <source>
      <strain>ATCC 10500 / CBS 375.48 / QM 6759 / NRRL 1006</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Involved in degradation of plant cell walls. Hydrolyzes the feruloyl-arabinose ester bond in arabinoxylans as well as the feruloyl-galactose and feruloyl-arabinose ester bonds in pectin (By similarity). Active against methyl esters of sinapate (MSA), but not caffeate (MCA) (PubMed:15006424).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>feruloyl-polysaccharide + H2O = ferulate + polysaccharide.</text>
      <dbReference type="EC" id="3.1.1.73"/>
    </reaction>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the AB hydrolase superfamily. FaeA family.</text>
  </comment>
  <dbReference type="EC" id="3.1.1.73" evidence="2"/>
  <dbReference type="AlphaFoldDB" id="P0CT85"/>
  <dbReference type="SMR" id="P0CT85"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030600">
    <property type="term" value="F:feruloyl esterase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045493">
    <property type="term" value="P:xylan catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0119">Carbohydrate metabolism</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0624">Polysaccharide degradation</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0719">Serine esterase</keyword>
  <keyword id="KW-0858">Xylan degradation</keyword>
  <feature type="chain" id="PRO_0000433996" description="Feruloyl esterase A">
    <location>
      <begin position="1"/>
      <end position="49" status="greater than"/>
    </location>
  </feature>
  <feature type="non-consecutive residues">
    <location>
      <begin position="28"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="non-consecutive residues">
    <location>
      <begin position="39"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="49"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="O42807"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="15006424"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="15006424"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="49" mass="5216" checksum="DA38F77428FFEECC" modified="2015-10-14" version="1" fragment="multiple">ASTQGISEDLYNRLVEMATIIQAAYADLAIYNAQTDINGASGNQAFASY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>