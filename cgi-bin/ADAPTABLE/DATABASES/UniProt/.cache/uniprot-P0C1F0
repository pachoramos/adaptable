<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-05-16" modified="2023-02-22" version="45" xmlns="http://uniprot.org/uniprot">
  <accession>P0C1F0</accession>
  <name>NA11_ANTEL</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Delta-actitoxin-Ael1b</fullName>
      <shortName evidence="4">Delta-AITX-Ael1b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Toxin APE 1-1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Anthopleura elegantissima</name>
    <name type="common">Green aggregating anemone</name>
    <name type="synonym">Actinia elegantissima</name>
    <dbReference type="NCBI Taxonomy" id="6110"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Actiniidae</taxon>
      <taxon>Anthopleura</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Toxicon" volume="39" first="693" last="702">
      <title>Isolation and characterisation of five neurotoxic and cardiotoxic polypeptides from the sea anemone Anthopleura elegantissima.</title>
      <authorList>
        <person name="Bruhn T."/>
        <person name="Schaller C."/>
        <person name="Schulze C."/>
        <person name="Sanchez-Rodriguez J."/>
        <person name="Dannmeier C."/>
        <person name="Ravens U."/>
        <person name="Heubach J.F."/>
        <person name="Eckhardt K."/>
        <person name="Schmidtmayer J."/>
        <person name="Schmidt H."/>
        <person name="Aneiros A."/>
        <person name="Wachter E."/>
        <person name="Beress L."/>
      </authorList>
      <dbReference type="PubMed" id="11072049"/>
      <dbReference type="DOI" id="10.1016/s0041-0101(00)00199-9"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>TOXIC DOSE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Produces a positive inotropic effect in mammalian heart muscle. Modifies current passing through the fast sodium channel (Nav) in neuroblastoma cells, leading to delayed and incomplete inactivation. Paralyzes the shore crab (C.maenas) by tetanic contractions after intramuscular injection.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="5">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="toxic dose">
    <text evidence="2">LD(50) is 10 ug/kg by intramuscular injection into crabs.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the sea anemone sodium channel inhibitory toxin family. Type I subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0C1F0"/>
  <dbReference type="SMR" id="P0C1F0"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009966">
    <property type="term" value="P:regulation of signal transduction"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000693">
    <property type="entry name" value="Anenome_toxin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00706">
    <property type="entry name" value="Toxin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF001905">
    <property type="entry name" value="Anenome_toxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0123">Cardiotoxin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000236024" description="Delta-actitoxin-Ael1b" evidence="2">
    <location>
      <begin position="1"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="4"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="6"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="27"/>
      <end position="45"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P10453"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11072049"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="11072049"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="47" mass="4803" checksum="A28BAF5FB05DE53A" modified="2006-05-16" version="1">GIACLCDSDGPSVRGNTLSGTYWLAGCPSGWHNCKSSGQLIGACCKQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>