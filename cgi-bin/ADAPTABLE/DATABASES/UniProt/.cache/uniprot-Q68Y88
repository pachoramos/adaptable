<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-25" modified="2024-11-27" version="100" xmlns="http://uniprot.org/uniprot">
  <accession>Q68Y88</accession>
  <name>CCL13_CANLF</name>
  <protein>
    <recommendedName>
      <fullName>C-C motif chemokine 13</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Monocyte chemoattractant protein 4</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Monocyte chemotactic protein 4</fullName>
      <shortName>MCP-4</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Small-inducible cytokine A13</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CCL13</name>
  </gene>
  <organism>
    <name type="scientific">Canis lupus familiaris</name>
    <name type="common">Dog</name>
    <name type="synonym">Canis familiaris</name>
    <dbReference type="NCBI Taxonomy" id="9615"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Carnivora</taxon>
      <taxon>Caniformia</taxon>
      <taxon>Canidae</taxon>
      <taxon>Canis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2004-02" db="EMBL/GenBank/DDBJ databases">
      <title>Expression analysis of monocyte chemotactic protein-4 (MCP-4/CCL13) gene in canine atopic dermatitis.</title>
      <authorList>
        <person name="Tsukui T."/>
        <person name="Sakaguchi M."/>
        <person name="Maeda S."/>
        <person name="Koyanagi M."/>
        <person name="Masuda K."/>
        <person name="Ohno K."/>
        <person name="Tsujimoto H."/>
        <person name="Iwabuchi S."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Chemotactic factor that attracts monocytes, lymphocytes, basophils and eosinophils, but not neutrophils. Signals through CCR2B and CCR3 receptors (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the intercrine beta (chemokine CC) family.</text>
  </comment>
  <dbReference type="EMBL" id="AB162849">
    <property type="protein sequence ID" value="BAD36827.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001003966.1">
    <property type="nucleotide sequence ID" value="NM_001003966.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q68Y88"/>
  <dbReference type="SMR" id="Q68Y88"/>
  <dbReference type="STRING" id="9615.ENSCAFP00000032928"/>
  <dbReference type="PaxDb" id="9612-ENSCAFP00000032928"/>
  <dbReference type="Ensembl" id="ENSCAFT00000037424.3">
    <property type="protein sequence ID" value="ENSCAFP00000032928.3"/>
    <property type="gene ID" value="ENSCAFG00000024205.3"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSCAFT00030031740.1">
    <property type="protein sequence ID" value="ENSCAFP00030027680.1"/>
    <property type="gene ID" value="ENSCAFG00030017108.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSCAFT00805008958">
    <property type="protein sequence ID" value="ENSCAFP00805007012"/>
    <property type="gene ID" value="ENSCAFG00805004793"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSCAFT00845019830.1">
    <property type="protein sequence ID" value="ENSCAFP00845015525.1"/>
    <property type="gene ID" value="ENSCAFG00845011162.1"/>
  </dbReference>
  <dbReference type="GeneID" id="445451"/>
  <dbReference type="KEGG" id="cfa:445451"/>
  <dbReference type="CTD" id="6357"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSCAFG00845011162"/>
  <dbReference type="eggNOG" id="ENOG502S776">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT01100000263482"/>
  <dbReference type="InParanoid" id="Q68Y88"/>
  <dbReference type="OrthoDB" id="4265193at2759"/>
  <dbReference type="Proteomes" id="UP000002254">
    <property type="component" value="Chromosome 9"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694429">
    <property type="component" value="Chromosome 9"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694542">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000805418">
    <property type="component" value="Chromosome 9"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048020">
    <property type="term" value="F:CCR chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008009">
    <property type="term" value="F:chemokine activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070098">
    <property type="term" value="P:chemokine-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048245">
    <property type="term" value="P:eosinophil chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006954">
    <property type="term" value="P:inflammatory response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030335">
    <property type="term" value="P:positive regulation of cell migration"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd00272">
    <property type="entry name" value="Chemokine_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.40:FF:000002">
    <property type="entry name" value="C-C motif chemokine"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.40">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR039809">
    <property type="entry name" value="Chemokine_b/g/d"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000827">
    <property type="entry name" value="Chemokine_CC_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001811">
    <property type="entry name" value="Chemokine_IL8-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036048">
    <property type="entry name" value="Interleukin_8-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015:SF161">
    <property type="entry name" value="C-C MOTIF CHEMOKINE 7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR12015">
    <property type="entry name" value="SMALL INDUCIBLE CYTOKINE A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00048">
    <property type="entry name" value="IL8"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01721">
    <property type="entry name" value="FRACTALKINE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00199">
    <property type="entry name" value="SCY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54117">
    <property type="entry name" value="Interleukin 8-like chemokines"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00472">
    <property type="entry name" value="SMALL_CYTOKINES_CC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0145">Chemotaxis</keyword>
  <keyword id="KW-0202">Cytokine</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0395">Inflammatory response</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000005200" description="C-C motif chemokine 13">
    <location>
      <begin position="24"/>
      <end position="98"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="2 3">
    <location>
      <position position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="34"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="35"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="Q99616"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="98" mass="10980" checksum="80A81DCD8E76B46F" modified="2004-10-11" version="1" precursor="true">MKVSAALLCLLLTAAILTTQVPAQPDALSALFTCCFTFNNKKIPLQRLESYRITSSHCPRKAVIFSTKLAKAICADPKEKWVQDYVKHLDQRTQTPKT</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>