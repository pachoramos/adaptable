<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-13" modified="2024-10-02" version="92" xmlns="http://uniprot.org/uniprot">
  <accession>P82757</accession>
  <name>DF187_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Putative defensin-like protein 187</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Putative low-molecular-weight cysteine-rich protein 42</fullName>
      <shortName>Protein LCR42</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">LCR42</name>
    <name type="ordered locus">At3g23165</name>
    <name type="ORF">K14B15</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="DNA Res." volume="7" first="131" last="135">
      <title>Structural analysis of Arabidopsis thaliana chromosome 3. I. Sequence features of the regions of 4,504,864 bp covered by sixty P1 and TAC clones.</title>
      <authorList>
        <person name="Sato S."/>
        <person name="Nakamura Y."/>
        <person name="Kaneko T."/>
        <person name="Katoh T."/>
        <person name="Asamizu E."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="10819329"/>
      <dbReference type="DOI" id="10.1093/dnares/7.2.131"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2001" name="Plant Mol. Biol." volume="46" first="17" last="34">
      <title>Two large Arabidopsis thaliana gene families are homologous to the Brassica gene superfamily that encodes pollen coat proteins and the male component of the self-incompatibility response.</title>
      <authorList>
        <person name="Vanoosthuyse V."/>
        <person name="Miege C."/>
        <person name="Dumas C."/>
        <person name="Cock J.M."/>
      </authorList>
      <dbReference type="PubMed" id="11437247"/>
      <dbReference type="DOI" id="10.1023/a:1010664704926"/>
    </citation>
    <scope>IDENTIFICATION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <comment type="caution">
    <text evidence="3">Could be the product of a pseudogene. Lacks 1 of the 4 disulfide bonds, which are conserved features of the family.</text>
  </comment>
  <dbReference type="EMBL" id="AB025608">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002686">
    <property type="protein sequence ID" value="AEE76726.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001030745.1">
    <property type="nucleotide sequence ID" value="NM_001035668.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P82757"/>
  <dbReference type="SMR" id="P82757"/>
  <dbReference type="STRING" id="3702.P82757"/>
  <dbReference type="PaxDb" id="3702-AT3G23165.1"/>
  <dbReference type="EnsemblPlants" id="AT3G23165.1">
    <property type="protein sequence ID" value="AT3G23165.1"/>
    <property type="gene ID" value="AT3G23165"/>
  </dbReference>
  <dbReference type="GeneID" id="3768906"/>
  <dbReference type="Gramene" id="AT3G23165.1">
    <property type="protein sequence ID" value="AT3G23165.1"/>
    <property type="gene ID" value="AT3G23165"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT3G23165"/>
  <dbReference type="Araport" id="AT3G23165"/>
  <dbReference type="TAIR" id="AT3G23165">
    <property type="gene designation" value="LCR42"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_199292_0_0_1"/>
  <dbReference type="InParanoid" id="P82757"/>
  <dbReference type="OMA" id="GEDACKE"/>
  <dbReference type="OrthoDB" id="5545902at2759"/>
  <dbReference type="PhylomeDB" id="P82757"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P82757">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="uncertain"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000017281" description="Putative defensin-like protein 187">
    <location>
      <begin position="20"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="47"/>
      <end position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="77" mass="8668" checksum="3BB9E1667DECE704" modified="2002-10-01" version="1" precursor="true">MKNSSIMFVLIVVFLISSSENQKMVGEAKQCKTGWACFGEDACKEKCMAKYKGVGTVTYFPFPPETIIIYCECLYDC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>