<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-11-01" modified="2019-12-11" version="50" xmlns="http://uniprot.org/uniprot">
  <accession>P04096</accession>
  <name>TY13_PITRO</name>
  <protein>
    <recommendedName>
      <fullName>Tryptophyllin-13</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pithecopus rohdei</name>
    <name type="common">Rohde's leaf frog</name>
    <name type="synonym">Phyllomedusa rohdei</name>
    <dbReference type="NCBI Taxonomy" id="8394"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Pithecopus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1986" name="Int. J. Pept. Protein Res." volume="27" first="175" last="182">
      <title>Primary structure determination of a tryptophan-containing tridecapeptide from Phyllomedusa rohdei.</title>
      <authorList>
        <person name="Montecucchi P.C."/>
        <person name="Gozzini L."/>
        <person name="Erspamer V."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Tryptophillin subfamily.</text>
  </comment>
  <dbReference type="PIR" id="A05174">
    <property type="entry name" value="A05174"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013266">
    <property type="entry name" value="Tryptophillin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08248">
    <property type="entry name" value="Tryp_FSAP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043844" description="Tryptophyllin-13">
    <location>
      <begin position="1"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="13" mass="1646" checksum="33BF33A212227773" modified="1986-11-01" version="1">QEKPYWPPPIYPM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>