<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2022-12-14" version="69" xmlns="http://uniprot.org/uniprot">
  <accession>P79875</accession>
  <name>TPG_RANTE</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Temporin-1Tg</fullName>
      <shortName evidence="1">TG</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="7 9">Temporin-G</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Rana temporaria</name>
    <name type="common">European common frog</name>
    <dbReference type="NCBI Taxonomy" id="8407"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Rana</taxon>
      <taxon>Rana</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Eur. J. Biochem." volume="242" first="788" last="792">
      <title>Temporins, antimicrobial peptides from the European red frog Rana temporaria.</title>
      <authorList>
        <person name="Simmaco M."/>
        <person name="Mignogna G."/>
        <person name="Canofeni S."/>
        <person name="Miele R."/>
        <person name="Mangoni M.L."/>
        <person name="Barra D."/>
      </authorList>
      <dbReference type="PubMed" id="9022710"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1996.0788r.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>AMIDATION AT LEU-59</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2021" name="Anal. Bioanal. Chem." volume="413" first="5333" last="5347">
      <title>Differentiation of Central Slovenian and Moscow populations of Rana temporaria frogs using peptide biomarkers of temporins family.</title>
      <authorList>
        <person name="Samgina T.Y."/>
        <person name="Vasileva I.D."/>
        <person name="Kovalev S.V."/>
        <person name="Trebse P."/>
        <person name="Torkar G."/>
        <person name="Surin A.K."/>
        <person name="Zubarev R.A."/>
        <person name="Lebedev A.T."/>
      </authorList>
      <dbReference type="PubMed" id="34235566"/>
      <dbReference type="DOI" id="10.1007/s00216-021-03506-1"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 47-59</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>AMIDATION AT LEU-59</scope>
    <source>
      <tissue evidence="8">Skin secretion</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2012" name="BMC Biotechnol." volume="12" first="10" last="10">
      <title>A novel PCR-based method for high throughput prokaryotic expression of antimicrobial peptide genes.</title>
      <authorList>
        <person name="Ke T."/>
        <person name="Liang S."/>
        <person name="Huang J."/>
        <person name="Mao H."/>
        <person name="Chen J."/>
        <person name="Dong C."/>
        <person name="Huang J."/>
        <person name="Liu S."/>
        <person name="Kang J."/>
        <person name="Liu D."/>
        <person name="Ma X."/>
      </authorList>
      <dbReference type="PubMed" id="22439858"/>
      <dbReference type="DOI" id="10.1186/1472-6750-12-10"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2018" name="J. Pept. Sci." volume="24" first="1" last="12">
      <title>Assessment of the potential of temporin peptides from the frog Rana temporaria (Ranidae) as anti-diabetic agents.</title>
      <authorList>
        <person name="Musale V."/>
        <person name="Casciaro B."/>
        <person name="Mangoni M.L."/>
        <person name="Abdel-Wahab Y.H.A."/>
        <person name="Flatt P.R."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="29349894"/>
      <dbReference type="DOI" id="10.1002/psc.3065"/>
    </citation>
    <scope>FUNCTION AS INSULINOTROPIC PEPTIDE</scope>
    <scope>BIOASSAY</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4">Amphipathic alpha-helical antimicrobial peptide with activity against Gram-positive bacteria (M.luteus), Gram-negative bacteria (E.coli), and fungi (S.cerevisiae) (PubMed:22439858). Stimulates insulin release from pancreatic beta-cells in a dose-dependent manner, without increasing intracellular calcium (PubMed:29349894). Does not protects BRIN-BD11 beta-cells against cytokine-induced apoptosis (PubMed:29349894). In vivo, intraperitoneal injection together with a glucose load into mice improves glucose tolerance with a concomitant increase in insulin secretion (PubMed:29349894).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5 6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="11 12">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1456.89" method="Electrospray" evidence="5"/>
  <comment type="similarity">
    <text evidence="10">Belongs to the frog skin active peptide (FSAP) family. Temporin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=00099"/>
  </comment>
  <dbReference type="EMBL" id="Y09395">
    <property type="protein sequence ID" value="CAA70564.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P79875"/>
  <dbReference type="TCDB" id="1.C.52.1.5">
    <property type="family name" value="the dermaseptin (dermaseptin) family"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000003473" evidence="12">
    <location>
      <begin position="23"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000003474" description="Temporin-1Tg" evidence="6">
    <location>
      <begin position="47"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="6">
    <location>
      <position position="59"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P56917"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22439858"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="29349894"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="34235566"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="9022710"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="22439858"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="34235566"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="9022710"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="34235566"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="12">
    <source>
      <dbReference type="PubMed" id="9022710"/>
    </source>
  </evidence>
  <sequence length="61" mass="7171" checksum="EDF5A8BC79DFD9F2" modified="1997-05-01" version="1" precursor="true">MFTLKKSLLLLFFLGTINLSLCEEERDADEERRDDLEERDVEVEKRFFPVIGRILNGILGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>