<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-08-16" modified="2024-11-27" version="108" xmlns="http://uniprot.org/uniprot">
  <accession>Q7A189</accession>
  <name>SSPC_STAAW</name>
  <protein>
    <recommendedName>
      <fullName>Staphostatin B</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Staphylococcal cysteine protease B inhibitor</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">sspC</name>
    <name type="ordered locus">MW0930</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2003" name="Protein Sci." volume="12" first="2252" last="2256">
      <title>Staphostatins resemble lipocalins, not cystatins in fold.</title>
      <authorList>
        <person name="Rzychon M."/>
        <person name="Filipek R."/>
        <person name="Sabat A."/>
        <person name="Kosowska K."/>
        <person name="Dubin A."/>
        <person name="Potempa J."/>
        <person name="Bochtler M."/>
      </authorList>
      <dbReference type="PubMed" id="14500882"/>
      <dbReference type="DOI" id="10.1110/ps.03247703"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.4 ANGSTROMS)</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2004-02" db="PDB data bank">
      <title>Crystal structure of a staphylococcal inhibitor/chaperone.</title>
      <authorList>
        <person name="Brown C.K."/>
        <person name="Gu Z.-Y."/>
        <person name="Nickerson N."/>
        <person name="McGavin M.J."/>
        <person name="Ohlendorf D.H."/>
        <person name="Earhart C.A."/>
      </authorList>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.5 ANGSTROMS)</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Specifically inhibits the cysteine protease staphopain B (SspB) by blocking the active site of the enzyme. Probably required to protect cytoplasmic proteins from being degraded by prematurely activated/folded prostaphopain B. Also involved in growth capacity, viability and bacterial morphology (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Forms a stable non-covalent complex with prematurely activated/folded SspB.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text>N-terminal residues are not required for inhibitory activity.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">Inactivated by staphylococcal serine protease (SspA).</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the protease inhibitor I57 (SspC) family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB94795.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000284457.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="PDB" id="1NYC">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.40 A"/>
    <property type="chains" value="A/B=1-109"/>
  </dbReference>
  <dbReference type="PDB" id="1QWX">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.50 A"/>
    <property type="chains" value="A/B=1-109"/>
  </dbReference>
  <dbReference type="PDBsum" id="1NYC"/>
  <dbReference type="PDBsum" id="1QWX"/>
  <dbReference type="AlphaFoldDB" id="Q7A189"/>
  <dbReference type="SMR" id="Q7A189"/>
  <dbReference type="MEROPS" id="I57.001"/>
  <dbReference type="KEGG" id="sam:MW0930"/>
  <dbReference type="HOGENOM" id="CLU_174854_0_0_9"/>
  <dbReference type="EvolutionaryTrace" id="Q7A189"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.310.10">
    <property type="entry name" value="beta-Barrel protease inhibitors"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016085">
    <property type="entry name" value="Protease_inh_b-brl_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR037296">
    <property type="entry name" value="Staphostatin_A/B"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015113">
    <property type="entry name" value="Staphostatin_B"/>
  </dbReference>
  <dbReference type="Pfam" id="PF09023">
    <property type="entry name" value="Staphostatin_B"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50882">
    <property type="entry name" value="beta-Barrel protease inhibitors"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="chain" id="PRO_0000220559" description="Staphostatin B">
    <location>
      <begin position="1"/>
      <end position="109"/>
    </location>
  </feature>
  <feature type="region of interest" description="Binds to staphopain B" evidence="1">
    <location>
      <begin position="97"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="2"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="13"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="18"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="29"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="turn" evidence="3">
    <location>
      <begin position="34"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="38"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="50"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="turn" evidence="3">
    <location>
      <begin position="60"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="64"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="72"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="86"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="103"/>
      <end position="107"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="1NYC"/>
    </source>
  </evidence>
  <sequence length="109" mass="12882" checksum="A4D002333A614362" modified="2004-07-05" version="1">MYQLQFINLVYDTTKLTHLEQTNINLFIGNWSNHQLQKSICIRHGDDTSHNQYHILFIDTAHQRIKFSSIDNEEIIYILDYDDTQHILMQTSSKQGIGTSRPIVYERLV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>