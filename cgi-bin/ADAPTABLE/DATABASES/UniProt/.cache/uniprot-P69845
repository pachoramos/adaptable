<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-05-24" modified="2023-05-03" version="23" xmlns="http://uniprot.org/uniprot">
  <accession>P69845</accession>
  <name>GRAA_GRASX</name>
  <protein>
    <recommendedName>
      <fullName>Grammistin Gs A</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Grammistes sexlineatus</name>
    <name type="common">Goldenstriped soapfish</name>
    <name type="synonym">Perca sexlineata</name>
    <dbReference type="NCBI Taxonomy" id="270576"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Actinopterygii</taxon>
      <taxon>Neopterygii</taxon>
      <taxon>Teleostei</taxon>
      <taxon>Neoteleostei</taxon>
      <taxon>Acanthomorphata</taxon>
      <taxon>Eupercaria</taxon>
      <taxon>Perciformes</taxon>
      <taxon>Serranoidei</taxon>
      <taxon>Serranidae</taxon>
      <taxon>Epinephelinae</taxon>
      <taxon>Grammistini</taxon>
      <taxon>Grammistes</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Toxicon" volume="45" first="595" last="601">
      <title>Further isolation and characterization of grammistins from the skin secretion of the soapfish Grammistes sexlineatus.</title>
      <authorList>
        <person name="Sugiyama N."/>
        <person name="Araki M."/>
        <person name="Ishida M."/>
        <person name="Nagashima Y."/>
        <person name="Shiomi K."/>
      </authorList>
      <dbReference type="PubMed" id="15777955"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2004.12.021"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Thanks to its amphiphilic alpha-helice(s), it may integrate into membrane phospholipids, leading to lysis of the membrane. Has no substantial hemolytic activity. Has antibacterial activity with a broad spectrum against various species of bacteria including both Gram-positive and Gram-negative groups.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Exists as aggregates of 3-4 molecules.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the grammistin family. Group 3 subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P69845"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044528" description="Grammistin Gs A">
    <location>
      <begin position="1"/>
      <end position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000305" key="1"/>
  <sequence length="28" mass="3126" checksum="4F3B3F6EC6751387" modified="2005-05-24" version="1">WWRELLKKLAFTAAGHLGSVLAAKQSGW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>