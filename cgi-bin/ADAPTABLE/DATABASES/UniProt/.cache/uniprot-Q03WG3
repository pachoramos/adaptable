<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-01-15" modified="2024-11-27" version="106" xmlns="http://uniprot.org/uniprot">
  <accession>Q03WG3</accession>
  <name>DTD_LEUMM</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">D-aminoacyl-tRNA deacylase</fullName>
      <shortName evidence="1">DTD</shortName>
      <ecNumber evidence="1">3.1.1.96</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Gly-tRNA(Ala) deacylase</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">dtd</name>
    <name type="ordered locus">LEUM_1366</name>
  </gene>
  <organism>
    <name type="scientific">Leuconostoc mesenteroides subsp. mesenteroides (strain ATCC 8293 / DSM 20343 / BCRC 11652 / CCM 1803 / JCM 6124 / NCDO 523 / NBRC 100496 / NCIMB 8023 / NCTC 12954 / NRRL B-1118 / 37Y)</name>
    <dbReference type="NCBI Taxonomy" id="203120"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Lactobacillales</taxon>
      <taxon>Lactobacillaceae</taxon>
      <taxon>Leuconostoc</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Proc. Natl. Acad. Sci. U.S.A." volume="103" first="15611" last="15616">
      <title>Comparative genomics of the lactic acid bacteria.</title>
      <authorList>
        <person name="Makarova K.S."/>
        <person name="Slesarev A."/>
        <person name="Wolf Y.I."/>
        <person name="Sorokin A."/>
        <person name="Mirkin B."/>
        <person name="Koonin E.V."/>
        <person name="Pavlov A."/>
        <person name="Pavlova N."/>
        <person name="Karamychev V."/>
        <person name="Polouchine N."/>
        <person name="Shakhova V."/>
        <person name="Grigoriev I."/>
        <person name="Lou Y."/>
        <person name="Rohksar D."/>
        <person name="Lucas S."/>
        <person name="Huang K."/>
        <person name="Goodstein D.M."/>
        <person name="Hawkins T."/>
        <person name="Plengvidhya V."/>
        <person name="Welker D."/>
        <person name="Hughes J."/>
        <person name="Goh Y."/>
        <person name="Benson A."/>
        <person name="Baldwin K."/>
        <person name="Lee J.-H."/>
        <person name="Diaz-Muniz I."/>
        <person name="Dosti B."/>
        <person name="Smeianov V."/>
        <person name="Wechter W."/>
        <person name="Barabote R."/>
        <person name="Lorca G."/>
        <person name="Altermann E."/>
        <person name="Barrangou R."/>
        <person name="Ganesan B."/>
        <person name="Xie Y."/>
        <person name="Rawsthorne H."/>
        <person name="Tamir D."/>
        <person name="Parker C."/>
        <person name="Breidt F."/>
        <person name="Broadbent J.R."/>
        <person name="Hutkins R."/>
        <person name="O'Sullivan D."/>
        <person name="Steele J."/>
        <person name="Unlu G."/>
        <person name="Saier M.H. Jr."/>
        <person name="Klaenhammer T."/>
        <person name="Richardson P."/>
        <person name="Kozyavkin S."/>
        <person name="Weimer B.C."/>
        <person name="Mills D.A."/>
      </authorList>
      <dbReference type="PubMed" id="17030793"/>
      <dbReference type="DOI" id="10.1073/pnas.0607117103"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 8293 / DSM 20343 / BCRC 11652 / CCM 1803 / JCM 6124 / NCDO 523 / NBRC 100496 / NCIMB 8023 / NCTC 12954 / NRRL B-1118 / 37Y</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">An aminoacyl-tRNA editing enzyme that deacylates mischarged D-aminoacyl-tRNAs. Also deacylates mischarged glycyl-tRNA(Ala), protecting cells against glycine mischarging by AlaRS. Acts via tRNA-based rather than protein-based catalysis; rejects L-amino acids rather than detecting D-amino acids in the active site. By recycling D-aminoacyl-tRNA to D-amino acids and free tRNA molecules, this enzyme counteracts the toxicity associated with the formation of D-aminoacyl-tRNA entities in vivo and helps enforce protein L-homochirality.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>glycyl-tRNA(Ala) + H2O = tRNA(Ala) + glycine + H(+)</text>
      <dbReference type="Rhea" id="RHEA:53744"/>
      <dbReference type="Rhea" id="RHEA-COMP:9657"/>
      <dbReference type="Rhea" id="RHEA-COMP:13640"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:57305"/>
      <dbReference type="ChEBI" id="CHEBI:78442"/>
      <dbReference type="ChEBI" id="CHEBI:78522"/>
      <dbReference type="EC" id="3.1.1.96"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1">
      <text>a D-aminoacyl-tRNA + H2O = a tRNA + a D-alpha-amino acid + H(+)</text>
      <dbReference type="Rhea" id="RHEA:13953"/>
      <dbReference type="Rhea" id="RHEA-COMP:10123"/>
      <dbReference type="Rhea" id="RHEA-COMP:10124"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:59871"/>
      <dbReference type="ChEBI" id="CHEBI:78442"/>
      <dbReference type="ChEBI" id="CHEBI:79333"/>
      <dbReference type="EC" id="3.1.1.96"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="1">A Gly-cisPro motif from one monomer fits into the active site of the other monomer to allow specific chiral rejection of L-amino acids.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the DTD family.</text>
  </comment>
  <dbReference type="EC" id="3.1.1.96" evidence="1"/>
  <dbReference type="EMBL" id="CP000414">
    <property type="protein sequence ID" value="ABJ62459.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_011680061.1">
    <property type="nucleotide sequence ID" value="NC_008531.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q03WG3"/>
  <dbReference type="SMR" id="Q03WG3"/>
  <dbReference type="EnsemblBacteria" id="ABJ62459">
    <property type="protein sequence ID" value="ABJ62459"/>
    <property type="gene ID" value="LEUM_1366"/>
  </dbReference>
  <dbReference type="GeneID" id="61176155"/>
  <dbReference type="KEGG" id="lme:LEUM_1366"/>
  <dbReference type="eggNOG" id="COG1490">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_076901_1_0_9"/>
  <dbReference type="Proteomes" id="UP000000362">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051500">
    <property type="term" value="F:D-tyrosyl-tRNA(Tyr) deacylase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0106026">
    <property type="term" value="F:Gly-tRNA(Ala) hydrolase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043908">
    <property type="term" value="F:Ser(Gly)-tRNA(Ala) hydrolase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000049">
    <property type="term" value="F:tRNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019478">
    <property type="term" value="P:D-amino acid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="CDD" id="cd00563">
    <property type="entry name" value="Dtyr_deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.50.80.10:FF:000001">
    <property type="entry name" value="D-aminoacyl-tRNA deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.50.80.10">
    <property type="entry name" value="D-tyrosyl-tRNA(Tyr) deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00518">
    <property type="entry name" value="Deacylase_Dtd"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003732">
    <property type="entry name" value="Daa-tRNA_deacyls_DTD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023509">
    <property type="entry name" value="DTD-like_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00256">
    <property type="entry name" value="D-aminoacyl-tRNA deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10472:SF5">
    <property type="entry name" value="D-AMINOACYL-TRNA DEACYLASE 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10472">
    <property type="entry name" value="D-TYROSYL-TRNA TYR DEACYLASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02580">
    <property type="entry name" value="Tyr_Deacylase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF69500">
    <property type="entry name" value="DTD-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-0820">tRNA-binding</keyword>
  <feature type="chain" id="PRO_1000050849" description="D-aminoacyl-tRNA deacylase">
    <location>
      <begin position="1"/>
      <end position="149"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Gly-cisPro motif, important for rejection of L-amino acids" evidence="1">
    <location>
      <begin position="137"/>
      <end position="138"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00518"/>
    </source>
  </evidence>
  <sequence length="149" mass="16307" checksum="9EF763C8122CD355" modified="2006-11-14" version="1">MKVVLQRVSKASVKIDDQVKGAINKGYVLLVGVADGDEQADIDYLVRKIHNLRIFEDESGRMNRSIEDVSGAILSISQFTLFADTKKGNRPSFTKAGAPEFATVMYNQFNATLRESGLQVETGEFGADMQVSLTNDGPVTILFDTKSAQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>