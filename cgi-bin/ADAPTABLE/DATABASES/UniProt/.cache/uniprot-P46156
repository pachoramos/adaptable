<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-10-02" version="104" xmlns="http://uniprot.org/uniprot">
  <accession>P46156</accession>
  <accession>Q09MK1</accession>
  <accession>Q0PWH4</accession>
  <accession>Q6B9W7</accession>
  <accession>Q6GXJ4</accession>
  <name>GLL1_CHICK</name>
  <protein>
    <recommendedName>
      <fullName>Gallinacin-1</fullName>
      <shortName>Gal-1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Beta-defensin 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">GAL1</name>
  </gene>
  <organism>
    <name type="scientific">Gallus gallus</name>
    <name type="common">Chicken</name>
    <dbReference type="NCBI Taxonomy" id="9031"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Archelosauria</taxon>
      <taxon>Archosauria</taxon>
      <taxon>Dinosauria</taxon>
      <taxon>Saurischia</taxon>
      <taxon>Theropoda</taxon>
      <taxon>Coelurosauria</taxon>
      <taxon>Aves</taxon>
      <taxon>Neognathae</taxon>
      <taxon>Galloanserae</taxon>
      <taxon>Galliformes</taxon>
      <taxon>Phasianidae</taxon>
      <taxon>Phasianinae</taxon>
      <taxon>Gallus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Anim. Genet." volume="29" first="283" last="289">
      <title>Characterization of beta-defensin prepropeptide mRNA from chicken and turkey bone marrow.</title>
      <authorList>
        <person name="Brockus C.W."/>
        <person name="Harmon B.G."/>
        <person name="Jackwood M.W."/>
      </authorList>
      <dbReference type="PubMed" id="9745666"/>
      <dbReference type="DOI" id="10.1046/j.1365-2052.1998.00338.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Bone marrow</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="BMC Genomics" volume="5" first="56" last="56">
      <title>A genome-wide screen identifies a single beta-defensin gene cluster in the chicken: implications for the origin and evolution of mammalian defensins.</title>
      <authorList>
        <person name="Xiao Y."/>
        <person name="Hughes A.L."/>
        <person name="Ando J."/>
        <person name="Matsuda Y."/>
        <person name="Cheng J.-F."/>
        <person name="Skinner-Noble D."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="15310403"/>
      <dbReference type="DOI" id="10.1186/1471-2164-5-56"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2004-06" db="EMBL/GenBank/DDBJ databases">
      <title>Chicken defensin gene structure.</title>
      <authorList>
        <person name="Zhang H."/>
        <person name="Bi Y."/>
        <person name="Cao Y."/>
        <person name="Chen X."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="submission" date="2006-07" db="EMBL/GenBank/DDBJ databases">
      <title>Chicken beta-defensin in China chicken breeds.</title>
      <authorList>
        <person name="Chen Y."/>
        <person name="Cao Y."/>
        <person name="Xie Q."/>
        <person name="Bi Y."/>
        <person name="Chen J."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <strain>Guangxi Huang</strain>
      <strain>Huiyang bearded</strain>
      <strain>Qingyuan Ma</strain>
      <strain>Taihe silkies</strain>
      <strain>Xinghua</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="submission" date="2006-07" db="EMBL/GenBank/DDBJ databases">
      <title>The cDNA cloning and sequencing of chicken beta-defensin (Gal-1).</title>
      <authorList>
        <person name="Wu J."/>
        <person name="Song M.X."/>
        <person name="Li Y.F."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Blood</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1994" name="FEBS Lett." volume="342" first="281" last="285">
      <title>Gallinacins: cysteine-rich antimicrobial peptides of chicken leukocytes.</title>
      <authorList>
        <person name="Harwig S.S.L."/>
        <person name="Swiderek K.M."/>
        <person name="Kokryakov V.N."/>
        <person name="Tan L."/>
        <person name="Lee T.D."/>
        <person name="Panyutich E.A."/>
        <person name="Aleshina G.M."/>
        <person name="Shamova O.V."/>
        <person name="Lehrer R.I."/>
      </authorList>
      <dbReference type="PubMed" id="8150085"/>
      <dbReference type="DOI" id="10.1016/0014-5793(94)80517-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 26-65</scope>
    <scope>FUNCTION</scope>
    <scope>CHARACTERIZATION</scope>
    <source>
      <strain>Broiler</strain>
      <tissue>Leukocyte</tissue>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2004" name="Immunogenetics" volume="56" first="170" last="177">
      <title>Bioinformatic discovery and initial characterisation of nine novel antimicrobial peptide genes in the chicken.</title>
      <authorList>
        <person name="Lynn D.J."/>
        <person name="Higgs R."/>
        <person name="Gaines S."/>
        <person name="Tierney J."/>
        <person name="James T."/>
        <person name="Lloyd A.T."/>
        <person name="Fares M.A."/>
        <person name="Mulcahy G."/>
        <person name="O'Farrelly C."/>
      </authorList>
      <dbReference type="PubMed" id="15148642"/>
      <dbReference type="DOI" id="10.1007/s00251-004-0675-0"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2006" name="J. Reprod. Dev." volume="52" first="211" last="218">
      <title>Effects of age, egg-laying activity, and Salmonella-inoculation on the expressions of gallinacin mRNA in the vagina of the hen oviduct.</title>
      <authorList>
        <person name="Yoshimura Y."/>
        <person name="Ohashi H."/>
        <person name="Subedi K."/>
        <person name="Nishibori M."/>
        <person name="Isobe N."/>
      </authorList>
      <dbReference type="PubMed" id="16394622"/>
      <dbReference type="DOI" id="10.1262/jrd.17070"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2007" name="Reproduction" volume="133" first="127" last="133">
      <title>Changes in the expression of gallinacins, antimicrobial peptides, in ovarian follicles during follicular growth and in response to lipopolysaccharide in laying hens (Gallus domesticus).</title>
      <authorList>
        <person name="Subedi K."/>
        <person name="Isobe N."/>
        <person name="Nishibori M."/>
        <person name="Yoshimura Y."/>
      </authorList>
      <dbReference type="PubMed" id="17244739"/>
      <dbReference type="DOI" id="10.1530/rep-06-0083"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>INDUCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="6">Has bactericidal activity. Potent activity against E.coli ML-35, L.monocytogenes EGD and C.albicans.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location>Cytoplasmic granule</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3 4 5">Strong expression in the bone marrow, lung, testis. Moderate expression in the bursa and intestine. Low expression in the cloaca, gall bladder, brain and pancreas. Expressed in the vagina, ovarian stroma and the theca and granulosa layers of the ovarian follicle.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="4 5">Detected in the theca layer of the ovarian follicle in the white follicle (WF) stage, but decreases throughout the F1, F3, F5, and postovulatory follicle stages. Detected in the granulosa layer of the ovarian follicle in the white follicle (WF) stage, F1, F3, F5, and postovulatory follicle stages. In the vagina expression is higher in laying hens than in non-laying hens, and is higher in older laying hens than in young laying hens.</text>
  </comment>
  <comment type="induction">
    <text evidence="4 5">Induced in the theca layer of the F3 stage ovarian follicle by intravenous injection of LPS. Repressed in the granulosa layer of the F3 stage ovarian follicle by intravenous injection of LPS. Expression in cultured vaginal cells is increased by LPS and S.enteritidis. Expression in the kidney and liver is not affected by intravenous injection of LPS.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF033335">
    <property type="protein sequence ID" value="AAC36051.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY621316">
    <property type="protein sequence ID" value="AAT48925.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY672653">
    <property type="protein sequence ID" value="AAT76974.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ677632">
    <property type="protein sequence ID" value="ABG73366.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858338">
    <property type="protein sequence ID" value="ABI48254.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858324">
    <property type="protein sequence ID" value="ABI48240.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858311">
    <property type="protein sequence ID" value="ABI48227.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ858298">
    <property type="protein sequence ID" value="ABI48214.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ862462">
    <property type="protein sequence ID" value="ABI49682.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S43283">
    <property type="entry name" value="S43283"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_990324.1">
    <property type="nucleotide sequence ID" value="NM_204993.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P46156"/>
  <dbReference type="SMR" id="P46156"/>
  <dbReference type="STRING" id="9031.ENSGALP00000035925"/>
  <dbReference type="PaxDb" id="9031-ENSGALP00000035925"/>
  <dbReference type="Ensembl" id="ENSGALT00000036713">
    <property type="protein sequence ID" value="ENSGALP00000035925"/>
    <property type="gene ID" value="ENSGALG00000022815"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00010018716.1">
    <property type="protein sequence ID" value="ENSGALP00010010266.1"/>
    <property type="gene ID" value="ENSGALG00010007863.1"/>
  </dbReference>
  <dbReference type="GeneID" id="395841"/>
  <dbReference type="KEGG" id="gga:395841"/>
  <dbReference type="CTD" id="395841"/>
  <dbReference type="VEuPathDB" id="HostDB:geneid_395841"/>
  <dbReference type="GeneTree" id="ENSGT00730000114755"/>
  <dbReference type="HOGENOM" id="CLU_189296_5_0_1"/>
  <dbReference type="InParanoid" id="P46156"/>
  <dbReference type="OMA" id="RQNGYCG"/>
  <dbReference type="OrthoDB" id="4562366at2759"/>
  <dbReference type="Reactome" id="R-GGA-1461957">
    <property type="pathway name" value="Beta defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-GGA-1461973">
    <property type="pathway name" value="Defensins"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P46156"/>
  <dbReference type="Proteomes" id="UP000000539">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSGALG00000022815">
    <property type="expression patterns" value="Expressed in granulocyte and 7 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030141">
    <property type="term" value="C:secretory granule"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="AgBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042056">
    <property type="term" value="F:chemoattractant activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060326">
    <property type="term" value="P:cell chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="AgBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515:SF0">
    <property type="entry name" value="BETA-DEFENSIN 103"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000007008" evidence="6">
    <location>
      <begin position="20"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000007009" description="Gallinacin-1">
    <location>
      <begin position="26"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="38"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 5; ABI49682." evidence="7" ref="5">
    <original>Y</original>
    <variation>C</variation>
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AAT76974." evidence="7" ref="3">
    <original>L</original>
    <variation>I</variation>
    <location>
      <position position="8"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="15148642"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="16394622"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="17244739"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="8150085"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="65" mass="7209" checksum="17665B795A22521F" modified="1998-12-15" version="2" precursor="true">MRIVYLLLPFILLLAQGAAGSSQALGRKSDCFRKSGFCAFLKCPSLTLISGKCSRFYLCCKRIWG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>