<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-08-01" modified="2022-05-25" version="65" xmlns="http://uniprot.org/uniprot">
  <accession>P08609</accession>
  <name>TKN2_SCYCA</name>
  <protein>
    <recommendedName>
      <fullName>Scyliorhinin-2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Rectin</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Scyliorhinin II</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Scyliorhinus canicula</name>
    <name type="common">Small-spotted catshark</name>
    <name type="synonym">Squalus canicula</name>
    <dbReference type="NCBI Taxonomy" id="7830"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Chondrichthyes</taxon>
      <taxon>Elasmobranchii</taxon>
      <taxon>Galeomorphii</taxon>
      <taxon>Galeoidea</taxon>
      <taxon>Carcharhiniformes</taxon>
      <taxon>Scyliorhinidae</taxon>
      <taxon>Scyliorhinus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1986" name="FEBS Lett." volume="200" first="111" last="116">
      <title>Scyliorhinin I and II: two novel tachykinins from dogfish gut.</title>
      <authorList>
        <person name="Conlon J.M."/>
        <person name="Deacon C.F."/>
        <person name="O'Toole L."/>
        <person name="Thim L."/>
      </authorList>
      <dbReference type="PubMed" id="2422058"/>
      <dbReference type="DOI" id="10.1016/0014-5793(86)80521-x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT MET-18</scope>
    <source>
      <tissue>Intestine</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1995" name="Am. J. Physiol." volume="268" first="R1359" last="R1364">
      <title>Characterization of the endogenous intestinal peptide that stimulates the rectal gland of Scyliorhinus canicula.</title>
      <authorList>
        <person name="Anderson W.G."/>
        <person name="Conlon J.M."/>
        <person name="Hazon N."/>
      </authorList>
      <dbReference type="PubMed" id="7541963"/>
      <dbReference type="DOI" id="10.1152/ajpregu.1995.268.6.r1359"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Small intestine</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Tachykinins are active peptides which excite neurons, evoke behavioral responses, are potent vasodilators and secretagogues, and contract (directly or indirectly) many smooth muscles.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the tachykinin family.</text>
  </comment>
  <dbReference type="PIR" id="B24867">
    <property type="entry name" value="B24867"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P08609"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013055">
    <property type="entry name" value="Tachy_Neuro_lke_CS"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00267">
    <property type="entry name" value="TACHYKININ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044411" description="Scyliorhinin-2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="1">
    <location>
      <position position="18"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="7"/>
      <end position="13"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2422058"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="18" mass="1854" checksum="FCCA3FB01E2F3907" modified="1988-08-01" version="1">SPSNSKCPDGPDCFVGLM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>