<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-10-07" modified="2024-11-27" version="9" xmlns="http://uniprot.org/uniprot">
  <accession>C0HLR5</accession>
  <name>SCX3_CENBA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Beta-toxin Cb3</fullName>
    </recommendedName>
  </protein>
  <organism evidence="3">
    <name type="scientific">Centruroides baergi</name>
    <name type="common">Scorpion</name>
    <name type="synonym">Centruroides nigrovariatus baergi</name>
    <dbReference type="NCBI Taxonomy" id="329350"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Centruroides</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2020" name="Toxicon" volume="184" first="10" last="18">
      <title>Biochemical, electrophysiological and immunological characterization of the venom from Centruroides baergi, a new scorpion species of medical importance in Mexico.</title>
      <authorList>
        <person name="Gomez-Ramirez I.V."/>
        <person name="Riano-Umbarila L."/>
        <person name="Olamendi-Portugal T."/>
        <person name="Restano-Cassulini R."/>
        <person name="Possani L.D."/>
        <person name="Becerril B."/>
      </authorList>
      <dbReference type="PubMed" id="32479835"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2020.05.021"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <tissue evidence="3">Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Beta toxins bind voltage-independently at site-4 of sodium channels (Nav) and reduces peak current and shifts the voltage of activation toward more negative potentials thereby affecting sodium channel activation and promoting spontaneous and repetitive firing (PubMed:32479835). Has an inhibitory effect on voltage-gated sodium channels hNav1.1/SCN1A, hNav1.2/SCN2A, hNav1.4/SCN4A and hNav1.6/SCN8A (PubMed:32479835). Reduces the peak current of hNav1.5/SCN5A but does not shift its voltage of activation (PubMed:32479835). Also affects the inactivation processes of hNav1.1/SCN1A, hNav1.4/SCN4A, hNav1.5/SCN5A and hNav1.6/SCN8A (PubMed:32479835). This toxin is active against mammals and lethal to mice (PubMed:32479835).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="4">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="mass spectrometry" mass="7775.7" error="1.0" method="Electrospray" evidence="2"/>
  <comment type="toxic dose">
    <text evidence="2">LD(50) is 0.5 ug/mouse by intraperitoneal injection into mice.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HLR5"/>
  <dbReference type="SMR" id="C0HLR5"/>
  <dbReference type="ABCD" id="C0HLR5">
    <property type="antibodies" value="2 sequenced antibodies"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044493">
    <property type="term" value="P:envenomation resulting in negative regulation of voltage-gated sodium channel activity in another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000450849" description="Beta-toxin Cb3">
    <location>
      <begin position="1"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="1"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="12"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="16"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="25"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="29"/>
      <end position="48"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="32479835"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="32479835"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="32479835"/>
    </source>
  </evidence>
  <sequence length="66" mass="7783" checksum="A8C7634CCC83E423" modified="2020-10-07" version="1">KEGYIVNYYDGCKYPCVKLGDNDYCLRECRLRYYKSAGGYCYAFACWCTHLYEQAVVWPLPNKTCL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>