<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-10-02" modified="2024-11-27" version="75" xmlns="http://uniprot.org/uniprot">
  <accession>Q0T4F2</accession>
  <name>CNU_SHIF8</name>
  <protein>
    <recommendedName>
      <fullName>OriC-binding nucleoid-associated protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>H-NS/StpA-binding protein 2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Transcription modulator YdgT</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">cnu</name>
    <name type="synonym">ydgT</name>
    <name type="ordered locus">SFV_1642</name>
  </gene>
  <organism>
    <name type="scientific">Shigella flexneri serotype 5b (strain 8401)</name>
    <dbReference type="NCBI Taxonomy" id="373384"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Shigella</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="BMC Genomics" volume="7" first="173" last="173">
      <title>Complete genome sequence of Shigella flexneri 5b and comparison with Shigella flexneri 2a.</title>
      <authorList>
        <person name="Nie H."/>
        <person name="Yang F."/>
        <person name="Zhang X."/>
        <person name="Yang J."/>
        <person name="Chen L."/>
        <person name="Wang J."/>
        <person name="Xiong Z."/>
        <person name="Peng J."/>
        <person name="Sun L."/>
        <person name="Dong J."/>
        <person name="Xue Y."/>
        <person name="Xu X."/>
        <person name="Chen S."/>
        <person name="Yao Z."/>
        <person name="Shen Y."/>
        <person name="Jin Q."/>
      </authorList>
      <dbReference type="PubMed" id="16822325"/>
      <dbReference type="DOI" id="10.1186/1471-2164-7-173"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>8401</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Modifies the set of genes regulated by H-NS; Hha and cnu (YdgT) increase the number of genes bound by H-NS/StpA and may also modulate the oligomerization of the H-NS/StpA-complex on DNA. The complex formed with H-NS binds to the specific 26-bp cnb site in the origin of replication oriC.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Forms complexes with both H-NS and StpA.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the Hha/YmoA/Cnu family.</text>
  </comment>
  <dbReference type="EMBL" id="CP000266">
    <property type="protein sequence ID" value="ABF03813.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000217950.1">
    <property type="nucleotide sequence ID" value="NC_008258.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q0T4F2"/>
  <dbReference type="SMR" id="Q0T4F2"/>
  <dbReference type="GeneID" id="75204470"/>
  <dbReference type="KEGG" id="sfv:SFV_1642"/>
  <dbReference type="HOGENOM" id="CLU_190629_0_0_6"/>
  <dbReference type="Proteomes" id="UP000000659">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.1280.40:FF:000002">
    <property type="entry name" value="OriC-binding nucleoid-associated protein"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.1280.40">
    <property type="entry name" value="HHA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007985">
    <property type="entry name" value="Hemolysn_expr_modulating_HHA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036666">
    <property type="entry name" value="HHA_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05321">
    <property type="entry name" value="HHA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF68989">
    <property type="entry name" value="Hemolysin expression modulating protein HHA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_0000305054" description="OriC-binding nucleoid-associated protein">
    <location>
      <begin position="1"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="site" description="Interacts with H-NS" evidence="1">
    <location>
      <position position="44"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P64467"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="71" mass="8417" checksum="CE9FF2E890E78219" modified="2006-09-05" version="1">MTVQDYLLKFRKISSLESLEKLYDHLNYTLTDDQELINMYRAADHRRAELVSGGRLFDLGQVPKSVWHYVQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>