<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-04-01" modified="2024-11-27" version="151" xmlns="http://uniprot.org/uniprot">
  <accession>P30201</accession>
  <name>LYSC_MACMU</name>
  <protein>
    <recommendedName>
      <fullName>Lysozyme C</fullName>
      <ecNumber>3.2.1.17</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>1,4-beta-N-acetylmuramidase C</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">LYZ</name>
  </gene>
  <organism>
    <name type="scientific">Macaca mulatta</name>
    <name type="common">Rhesus macaque</name>
    <dbReference type="NCBI Taxonomy" id="9544"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Macaca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="J. Mol. Evol." volume="33" first="418" last="425">
      <title>Stomach lysozyme gene of the langur monkey: tests for convergence and positive selection.</title>
      <authorList>
        <person name="Swanson K.W."/>
        <person name="Irwin D.M."/>
        <person name="Wilson A.C."/>
      </authorList>
      <dbReference type="PubMed" id="1960739"/>
      <dbReference type="DOI" id="10.1007/bf02103133"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Mammary gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Lysozymes have primarily a bacteriolytic function; those in tissues and body fluids are associated with the monocyte-macrophage system and enhance the activity of immunoagents.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction>
      <text>Hydrolysis of (1-&gt;4)-beta-linkages between N-acetylmuramic acid and N-acetyl-D-glucosamine residues in a peptidoglycan and between N-acetyl-D-glucosamine residues in chitodextrins.</text>
      <dbReference type="EC" id="3.2.1.17"/>
    </reaction>
  </comment>
  <comment type="subunit">
    <text>Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text>Lysozyme C is capable of both hydrolysis and transglycosylation; it shows also a slight esterase activity. It acts rapidly on both peptide-substituted and unsubstituted peptidoglycan, and slowly on chitin oligosaccharides.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the glycosyl hydrolase 22 family.</text>
  </comment>
  <dbReference type="EC" id="3.2.1.17"/>
  <dbReference type="EMBL" id="X60236">
    <property type="protein sequence ID" value="CAA42796.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001095203.1">
    <property type="nucleotide sequence ID" value="NM_001101733.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P30201"/>
  <dbReference type="SMR" id="P30201"/>
  <dbReference type="STRING" id="9544.ENSMMUP00000047515"/>
  <dbReference type="CAZy" id="GH22">
    <property type="family name" value="Glycoside Hydrolase Family 22"/>
  </dbReference>
  <dbReference type="PaxDb" id="9544-ENSMMUP00000011770"/>
  <dbReference type="Ensembl" id="ENSMMUT00000056068.2">
    <property type="protein sequence ID" value="ENSMMUP00000047515.1"/>
    <property type="gene ID" value="ENSMMUG00000008987.4"/>
  </dbReference>
  <dbReference type="GeneID" id="718361"/>
  <dbReference type="KEGG" id="mcc:718361"/>
  <dbReference type="CTD" id="4069"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSMMUG00000008987"/>
  <dbReference type="VGNC" id="VGNC:74466">
    <property type="gene designation" value="LYZ"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S1S1">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000153832"/>
  <dbReference type="HOGENOM" id="CLU_111620_0_1_1"/>
  <dbReference type="InParanoid" id="P30201"/>
  <dbReference type="OMA" id="NACNIKC"/>
  <dbReference type="OrthoDB" id="5344399at2759"/>
  <dbReference type="TreeFam" id="TF324882"/>
  <dbReference type="Proteomes" id="UP000006718">
    <property type="component" value="Chromosome 11"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMMUG00000008987">
    <property type="expression patterns" value="Expressed in olfactory segment of nasal mucosa and 23 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042802">
    <property type="term" value="F:identical protein binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003796">
    <property type="term" value="F:lysozyme activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd16897">
    <property type="entry name" value="LYZ_C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.530.10:FF:000001">
    <property type="entry name" value="Lysozyme C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.530.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001916">
    <property type="entry name" value="Glyco_hydro_22"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019799">
    <property type="entry name" value="Glyco_hydro_22_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000974">
    <property type="entry name" value="Glyco_hydro_22_lys"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023346">
    <property type="entry name" value="Lysozyme-like_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407">
    <property type="entry name" value="LYSOZYME C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407:SF28">
    <property type="entry name" value="LYSOZYME C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00062">
    <property type="entry name" value="Lys"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00137">
    <property type="entry name" value="LYSOZYME"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00135">
    <property type="entry name" value="LYZLACT"/>
  </dbReference>
  <dbReference type="SMART" id="SM00263">
    <property type="entry name" value="LYZ1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF53955">
    <property type="entry name" value="Lysozyme-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00128">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51348">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0081">Bacteriolytic enzyme</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0326">Glycosidase</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0494">Milk protein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000018470" description="Lysozyme C">
    <location>
      <begin position="19"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="domain" description="C-type lysozyme" evidence="2">
    <location>
      <begin position="19"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="active site" evidence="2">
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="active site" evidence="2">
    <location>
      <position position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="24"/>
      <end position="146"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="48"/>
      <end position="134"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="83"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="95"/>
      <end position="113"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00680"/>
    </source>
  </evidence>
  <sequence length="148" mass="16408" checksum="AF4EE1B3A33F4765" modified="1993-04-01" version="1" precursor="true">MKAVIILGLVLLSVTVQGKIFERCELARTLKRLGLDGYRGISLANWVCLAKWESNYNTQATNYNPGDQSTDYGIFQINSHYWCNNGKTPGAVNACHISCNALLQDNIADAVTCAKRVVSDPQGIRAWVAWRNHCQNRDVSQYVQGCGV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>