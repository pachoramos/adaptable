<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-09-26" modified="2021-02-10" version="54" xmlns="http://uniprot.org/uniprot">
  <accession>P82090</accession>
  <name>CAE32_RANCI</name>
  <protein>
    <recommendedName>
      <fullName>Caerulein-3.2/3.2Y4</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ranoidea citropa</name>
    <name type="common">Australian Blue Mountains tree frog</name>
    <name type="synonym">Litoria citropa</name>
    <dbReference type="NCBI Taxonomy" id="94770"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Rapid Commun. Mass Spectrom." volume="13" first="2498" last="2502">
      <title>Caerulein-like peptides from the skin glands of the Australian blue mountains tree frog Litoria citropa. Part 1. Sequence determination using electrospray mass spectrometry.</title>
      <authorList>
        <person name="Wabnitz P.A."/>
        <person name="Bowie J.H."/>
        <person name="Tyler M.J."/>
      </authorList>
      <dbReference type="PubMed" id="10589099"/>
      <dbReference type="DOI" id="10.1002/(sici)1097-0231(19991230)13:24&lt;2498::aid-rcm817&gt;3.0.co;2-e"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-1</scope>
    <scope>SULFATION AT TYR-4</scope>
    <scope>AMIDATION AT PHE-11</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Hypotensive neuropeptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin dorsal glands.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Isoform 3.2Y4 differs from isoform 3.2 in not being sulfated.</text>
  </comment>
  <comment type="mass spectrometry" mass="1423.0" method="Electrospray" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the gastrin/cholecystokinin family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008217">
    <property type="term" value="P:regulation of blood pressure"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0382">Hypotensive agent</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0765">Sulfation</keyword>
  <feature type="peptide" id="PRO_0000043883" description="Caerulein-3.2/3.2Y4">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Sulfotyrosine" evidence="1">
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10589099"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="11" mass="1363" checksum="10DAB8867861A86B" modified="2000-05-01" version="1">QQDYGTGWFDF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>