<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-11-23" modified="2024-11-27" version="58" xmlns="http://uniprot.org/uniprot">
  <accession>P68410</accession>
  <accession>A0A218QWV5</accession>
  <accession>P23814</accession>
  <name>SCX2_TITSE</name>
  <protein>
    <recommendedName>
      <fullName evidence="10">Alpha-mammal toxin Ts2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>P-Mice-beta* NaTx5.1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Tityustoxin II</fullName>
      <shortName>Toxin II</shortName>
      <shortName>TsTX-II</shortName>
      <shortName>Tst2</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Toxin T1-IV</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="11">TsTX III-8</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>TsTX-III</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Tityus serrulatus</name>
    <name type="common">Brazilian scorpion</name>
    <dbReference type="NCBI Taxonomy" id="6887"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Tityus</taxon>
    </lineage>
  </organism>
  <reference evidence="19 20" key="1">
    <citation type="journal article" date="2018" name="PLoS ONE" volume="13" first="e0193739" last="e0193739">
      <title>Proteomic endorsed transcriptomic profiles of venom glands from Tityus obscurus and T. serrulatus scorpions.</title>
      <authorList>
        <person name="de Oliveira U.C."/>
        <person name="Nishiyama M.Y. Jr."/>
        <person name="Dos Santos M.B.V."/>
        <person name="Santos-da-Silva A.P."/>
        <person name="Chalkidis H.M."/>
        <person name="Souza-Imberg A."/>
        <person name="Candido D.M."/>
        <person name="Yamanouye N."/>
        <person name="Dorce V.A.C."/>
        <person name="Junqueira-de-Azevedo I.L.M."/>
      </authorList>
      <dbReference type="PubMed" id="29561852"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0193739"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Telson</tissue>
    </source>
  </reference>
  <reference evidence="21" key="2">
    <citation type="journal article" date="2021" name="Toxicon" volume="189" first="91" last="104">
      <title>Novel components of Tityus serrulatus venom: a transcriptomic approach.</title>
      <authorList>
        <person name="Kalapothakis Y."/>
        <person name="Miranda K."/>
        <person name="Pereira A.H."/>
        <person name="Witt A.S.A."/>
        <person name="Marani C."/>
        <person name="Martins A.P."/>
        <person name="Leal H.G."/>
        <person name="Campos-Junior E."/>
        <person name="Pimenta A.M.C."/>
        <person name="Borges A."/>
        <person name="Chavez-Olortegui C."/>
        <person name="Kalapothakis E."/>
      </authorList>
      <dbReference type="PubMed" id="33181162"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2020.11.001"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Telson</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1991" name="J. Biol. Chem." volume="266" first="3178" last="3185">
      <title>Discharge effect on pancreatic exocrine secretion produced by toxins purified from Tityus serrulatus scorpion venom.</title>
      <authorList>
        <person name="Possani L.D."/>
        <person name="Martin B.M."/>
        <person name="Fletcher M.D."/>
        <person name="Fletcher P.L. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="1993690"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)49971-1"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-82</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1992" name="Nat. Toxins" volume="1" first="119" last="125">
      <title>The beta-type toxin Ts II from the scorpion Tityus serrulatus: amino acid sequence determination and assessment of biological and antigenic properties.</title>
      <authorList>
        <person name="Mansuelle P."/>
        <person name="Martin-Eauclaire M.-F."/>
        <person name="Chavez-Olortegui C."/>
        <person name="de Lima M.E."/>
        <person name="Rochat H."/>
        <person name="Granier C."/>
      </authorList>
      <dbReference type="PubMed" id="1344906"/>
      <dbReference type="DOI" id="10.1002/nt.2620010211"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-82</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>AMIDATION AT CYS-82</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="FEBS J." volume="279" first="1495" last="1504">
      <title>Investigation of the relationship between the structure and function of Ts2, a neurotoxin from Tityus serrulatus venom.</title>
      <authorList>
        <person name="Cologna C.T."/>
        <person name="Peigneur S."/>
        <person name="Rustiguel J.K."/>
        <person name="Nonato M.C."/>
        <person name="Tytgat J."/>
        <person name="Arantes E.C."/>
      </authorList>
      <dbReference type="PubMed" id="22356164"/>
      <dbReference type="DOI" id="10.1111/j.1742-4658.2012.08545.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-82</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>3D-STRUCTURE MODELING</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2014" name="Toxicon" volume="90" first="326" last="336">
      <title>Influence of post-starvation extraction time and prey-specific diet in Tityus serrulatus scorpion venom composition and hyaluronidase activity.</title>
      <authorList>
        <person name="Pucca M.B."/>
        <person name="Amorim F.G."/>
        <person name="Cerni F.A."/>
        <person name="Bordon K.C.F."/>
        <person name="Cardoso I.A."/>
        <person name="Anjolette F.A."/>
        <person name="Arantes E.C."/>
      </authorList>
      <dbReference type="PubMed" id="25199494"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2014.08.064"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-40</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue evidence="12">Venom</tissue>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1991" name="Toxicon" volume="29" first="663" last="672">
      <title>Further characterization of toxins T1IV (TsTX-III) and T2IV from Tityus serrulatus scorpion venom.</title>
      <authorList>
        <person name="Sampaio S.V."/>
        <person name="Arantes E.C."/>
        <person name="Prado W.A."/>
        <person name="Riccioppo Neto F."/>
        <person name="Giglio J.R."/>
      </authorList>
      <dbReference type="PubMed" id="1926167"/>
      <dbReference type="DOI" id="10.1016/0041-0101(91)90058-y"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-34</scope>
    <scope>FUNCTION</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2011" name="Toxicon" volume="57" first="1101" last="1108">
      <title>Tityus serrulatus venom and toxins Ts1, Ts2 and Ts6 induce macrophage activation and production of immune mediators.</title>
      <authorList>
        <person name="Zoccal K.F."/>
        <person name="Bitencourt C.S."/>
        <person name="Secatto A."/>
        <person name="Sorgi C.A."/>
        <person name="Bordon K.C."/>
        <person name="Sampaio S.V."/>
        <person name="Arantes E.C."/>
        <person name="Faccioli L.H."/>
      </authorList>
      <dbReference type="PubMed" id="21549737"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2011.04.017"/>
    </citation>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2013" name="Toxicon" volume="61" first="1" last="10">
      <title>Ts6 and Ts2 from Tityus serrulatus venom induce inflammation by mechanisms dependent on lipid mediators and cytokine production.</title>
      <authorList>
        <person name="Zoccal K.F."/>
        <person name="Bitencourt C.S."/>
        <person name="Sorgi C.A."/>
        <person name="Bordon K.C."/>
        <person name="Sampaio S.V."/>
        <person name="Arantes E.C."/>
        <person name="Faccioli L.H."/>
      </authorList>
      <dbReference type="PubMed" id="23085190"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.10.002"/>
    </citation>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2009" name="Protein Pept. Lett." volume="16" first="920" last="932">
      <title>Tityus serrulatus scorpion venom and toxins: an overview.</title>
      <authorList>
        <person name="Cologna C.T."/>
        <person name="Marcussi S."/>
        <person name="Giglio J.R."/>
        <person name="Soares A.M."/>
        <person name="Arantes E.C."/>
      </authorList>
      <dbReference type="PubMed" id="19689419"/>
      <dbReference type="DOI" id="10.2174/092986609788923329"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2012" name="PLoS ONE" volume="7" first="E30478" last="E30478">
      <title>Identification and phylogenetic analysis of Tityus pachyurus and Tityus obscurus novel putative Na+-channel scorpion toxins.</title>
      <authorList>
        <person name="Guerrero-Vargas J.A."/>
        <person name="Mourao C.B."/>
        <person name="Quintero-Hernandez V."/>
        <person name="Possani L.D."/>
        <person name="Schwartz E.F."/>
      </authorList>
      <dbReference type="PubMed" id="22355312"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0030478"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4 6 7 8">Alpha toxins bind voltage-independently at site-3 of sodium channels (Nav) and inhibit the inactivation of the activated channels, thereby blocking neuronal transmission. This toxin acts on Nav1.2/SCN2A, Nav1.3/SCN3A, Nav1.5/SCN5A, Nav1.6/SCN8A and Nav1.7/SCN9A voltage-gated sodium channels, with the highest affinity for Nav1.3/SCN3A, followed by Nav1.6/SCN8A and Nav1.7/SCN9A which are affected almost equally. Interestingly, shows a significant shift of the voltage dependence of activation for Nav1.3/SCN3A that is characteristic of beta-toxins (PubMed:21549737). In addition, in presence of LPS, this toxin inhibits the release of NO, IL-6 and TNF-alpha in J774.1 cells (PubMed:21549737). Further, in the absence of LPS, it stimulates the production of the anti-inflammatory cytokine IL-10 (PubMed:21549737, PubMed:23085190). This toxin is active on mammals.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3 4 5 7 9">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="18">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="13">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="mass spectrometry" mass="6998.0" method="Electrospray" evidence="7"/>
  <comment type="toxic dose">
    <text evidence="3 4">LD(50) is 6 ng by intracerebroventricular injection into mice (PubMed:1344906) and 12.9 +- 1.6 ug/kg when intracisternally injected into mice (PubMed:1926167).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="17">Negative results: does not affect Nav1.4/SCN4A, Nav1.8/SCN10A and DmNa1/para.</text>
  </comment>
  <comment type="similarity">
    <text evidence="13">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="13">This toxin is functionally similar to alpha toxins and structurally similar to beta toxins.</text>
  </comment>
  <dbReference type="EMBL" id="GEUW01000066">
    <property type="protein sequence ID" value="JAW06979.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="GEUW01000029">
    <property type="protein sequence ID" value="JAW07016.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="MT450706">
    <property type="protein sequence ID" value="QPD99042.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="B39510">
    <property type="entry name" value="B39510"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P68410"/>
  <dbReference type="SMR" id="P68410"/>
  <dbReference type="ABCD" id="P68410">
    <property type="antibodies" value="1 sequenced antibody"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="1 14 15 16 17 18">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000066824" description="Alpha-mammal toxin Ts2" evidence="3 5 7">
    <location>
      <begin position="21"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="2">
    <location>
      <begin position="21"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="modified residue" description="Cysteine amide" evidence="3">
    <location>
      <position position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="31"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="35"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="43"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="47"/>
      <end position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="1344906"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="1926167"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="1993690"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="21549737"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="22356164"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="23085190"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="25199494"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="10">
    <source>
      <dbReference type="PubMed" id="19689419"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="11">
    <source>
      <dbReference type="PubMed" id="1993690"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="25199494"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13"/>
  <evidence type="ECO:0000305" key="14">
    <source>
      <dbReference type="PubMed" id="1344906"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="15">
    <source>
      <dbReference type="PubMed" id="1926167"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="16">
    <source>
      <dbReference type="PubMed" id="1993690"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="17">
    <source>
      <dbReference type="PubMed" id="22356164"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="18">
    <source>
      <dbReference type="PubMed" id="25199494"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="19">
    <source>
      <dbReference type="EMBL" id="JAW06979.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="20">
    <source>
      <dbReference type="EMBL" id="JAW07016.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="21">
    <source>
      <dbReference type="EMBL" id="QPD99042.1"/>
    </source>
  </evidence>
  <sequence length="84" mass="9349" checksum="C2B66CDAA4D18E21" modified="2022-08-03" version="2" precursor="true">MKGFLLFISILMMIGTIVVGKEGYAMDHEGCKFSCFIRPAGFCDGYCKTHLKASSGYCAWPACYCYGVPDHIKVWDYATNKCGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>