<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-03-28" modified="2024-05-29" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>P0CU59</accession>
  <name>CYAC_AMAPH</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Cycloamanide C</fullName>
      <shortName evidence="5">CyA C</shortName>
      <shortName evidence="4">Cyl C</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Amanita phalloides</name>
    <name type="common">Death cap</name>
    <dbReference type="NCBI Taxonomy" id="67723"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Basidiomycota</taxon>
      <taxon>Agaricomycotina</taxon>
      <taxon>Agaricomycetes</taxon>
      <taxon>Agaricomycetidae</taxon>
      <taxon>Agaricales</taxon>
      <taxon>Pluteineae</taxon>
      <taxon>Amanitaceae</taxon>
      <taxon>Amanita</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Peptides" volume="14" first="1" last="5">
      <title>Immunosuppressive activity in the series of cycloamanide peptides from mushrooms.</title>
      <authorList>
        <person name="Wieczorek Z."/>
        <person name="Siemion I.Z."/>
        <person name="Zimecki M."/>
        <person name="Bolewska-Pedyczak E."/>
        <person name="Wieland T."/>
      </authorList>
      <dbReference type="PubMed" id="8441706"/>
      <dbReference type="DOI" id="10.1016/0196-9781(93)90003-y"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2018" name="ACS Synth. Biol." volume="7" first="145" last="152">
      <title>Versatility of prolyl oligopeptidase B in peptide macrocyclization.</title>
      <authorList>
        <person name="Sgambelluri R.M."/>
        <person name="Smith M.O."/>
        <person name="Walton J.D."/>
      </authorList>
      <dbReference type="PubMed" id="28866879"/>
      <dbReference type="DOI" id="10.1021/acssynbio.7b00264"/>
    </citation>
    <scope>CYCLIZATION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3">Cyclic octapeptide that belongs to the MSDIN-like toxin family responsible for a large number of food poisoning cases and deaths (PubMed:28866879, PubMed:8441706). Cycloaminide B is non-toxic to mammals but shows immunosuppressive activity, probably through the inhibition of the action of interleukin-1 and interleukin-2 (PubMed:8441706).</text>
  </comment>
  <comment type="PTM">
    <text evidence="1 2">Processed by the macrocyclase-peptidase enzyme POPB to yield a cyclic octapeptide (PubMed:28866879). POPB first removes 10 residues from the N-terminus (By similarity). Conformational trapping of the remaining peptide forces the enzyme to release this intermediate rather than proceed to macrocyclization (By similarity). The enzyme rebinds the remaining peptide in a different conformation and catalyzes macrocyclization of the N-terminal 8 residues (PubMed:28866879).</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the MSDIN fungal toxin family.</text>
  </comment>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000443781" description="Cycloamanide C" evidence="7">
    <location>
      <begin position="1"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="cross-link" description="Cyclopeptide (Met-Pro)" evidence="2">
    <location>
      <begin position="1"/>
      <end position="8"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A067SLB9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="28866879"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="8441706"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="28866879"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="8441706"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="8441706"/>
    </source>
  </evidence>
  <sequence length="8" mass="889" checksum="EE6732C729C87726" modified="2018-03-28" version="1">MLGFLVLP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>