<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2015-03-04" modified="2023-02-22" version="15" xmlns="http://uniprot.org/uniprot">
  <accession>N0EAL3</accession>
  <name>NDB28_ANDMU</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Mauriporin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Non-disulfide-bridged peptide 2.8</fullName>
      <shortName evidence="5">NDBP-2.8</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Androctonus mauritanicus</name>
    <name type="common">Fat-tailed scorpion</name>
    <dbReference type="NCBI Taxonomy" id="6859"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Androctonus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2013" name="Int. J. Pept. Res. Ther." volume="4" first="281" last="293">
      <title>Mauriporin, a novel cationic alpha-helical peptide with selective cytotoxic activity against prostate cancer cell lines from the venom of the scorpion Androctonus mauritanicus.</title>
      <authorList>
        <person name="Almaaytah A."/>
        <person name="Tarazi S."/>
        <person name="Mhaidat N."/>
        <person name="Al Balas Q."/>
        <person name="Mukattash T.L."/>
      </authorList>
      <dbReference type="DOI" id="10.1007/s10989-013-9350-3"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>SYNTHESIS OF 23-70</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>CIRCULAR DICHROISM</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2014" name="Int. J. Pept. Res. Ther." volume="20" first="397" last="408">
      <title>Antimicrobial and antibiofilm activity of mauriporin, a multifunctional scorpion venom peptide.</title>
      <authorList>
        <person name="Almaaytah A."/>
        <person name="Tarazi S."/>
        <person name="Alsheyab F."/>
        <person name="Al-Balas Q."/>
        <person name="Mukattash T."/>
      </authorList>
      <dbReference type="DOI" id="10.1007/s10989-014-9405-0"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>3D-STRUCTURE MODELING</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2016" name="Biochim. Biophys. Acta" volume="1858" first="1914" last="1925">
      <title>From a pro-apoptotic peptide to a lytic peptide: one single residue mutation.</title>
      <authorList>
        <person name="Zhou X.R."/>
        <person name="Zhang Q."/>
        <person name="Tian X.B."/>
        <person name="Cao Y.M."/>
        <person name="Liu Z.Q."/>
        <person name="Fan R."/>
        <person name="Ding X.F."/>
        <person name="Zhu Z."/>
        <person name="Chen L."/>
        <person name="Luo S.Z."/>
      </authorList>
      <dbReference type="PubMed" id="27207743"/>
      <dbReference type="DOI" id="10.1016/j.bbamem.2016.05.012"/>
    </citation>
    <scope>FUNCTION OF ANTICANCER PEPTIDE ANALOG ZXR-1 AND ZXR-2</scope>
    <scope>SYNTHESIS OF 23-38</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2014" name="Peptides" volume="51" first="35" last="45">
      <title>Scorpion venom peptides with no disulfide bridges: a review.</title>
      <authorList>
        <person name="Almaaytah A."/>
        <person name="Albalas Q."/>
      </authorList>
      <dbReference type="PubMed" id="24184590"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2013.10.021"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4">Amphipathic peptide that displays potent antimicrobial activities against a range of Gram-positive and Gram-negative planktonic bacteria with MIC values in the range 5 uM to 10 uM (Ref.2). In more details, it is active on Listeria ivanovii (MIC=5 uM), Staphylococcus epidermidis (MIC=10 uM), Salmonella enterica (MIC=5 uM), Pseudomonas aeruginosa (ATCC 27853) (MIC=5 uM), Acinetobacter baumannii (MIC=5 uM), Klebsiella pneumoniae (MIC=5 uM), Escherichia coli (MIC=7.5 uM), Salmonella typhimurium (MIC=7.5 uM), Pseudomonas aeruginosa (ATCC 9027) (MIC=10 uM) (Ref.2). Is also able to prevent P.aeruginosa biofilm formation while showing weak hemolytic activity towards human erythrocytes (Ref.2). Probably induces bacterial cell death through membrane permeabilization (Ref.2). Moreover, shows DNA-binding activities (Ref.2). Also exerts potent selective cytotoxic and antiproliferative activity against three different prostate cancer cell lines (IC(50)=4.4-7.8 uM), compared to non-tumorigenic cell lines (IC(50)=59.7 uM in Vero and 62.5 uM in HUVEC cells) (Ref.1). This peptide possibly exerts its cytotoxic activity through a necrotic mode of cell death (Ref.1).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="8">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="8">Forms a helical membrane channel in the prey.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="5398.1" method="MALDI" evidence="3"/>
  <comment type="miscellaneous">
    <text evidence="3">Only shows diminished hemolytic activity against sheep erythrocytes. Does not induce cell death through apoptosis and consequently is not acting upon an intracellular target.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">ZXR-1 and ZXR-2 are two shortened peptides derived from mauriporin. ZXR-1 sequence consists of the 16 N-terminal residues (FKIGGFIKKLWRSKLA), while ZXR-2 has the additional residue difference K36L (FKIGGFIKKLWRSLLA) (PubMed:27207743). Both peptides display distinct anticancer modes of action (PubMed:27207743). ZXR-1 could translocate into cells, target on the mitochondria and induce cell apoptosis, while ZXR-2 directly targets on the cell membranes and causes membrane lysis (PubMed:27207743). The variance in their acting mechanisms might be due to the different amphipathicity and positive charge distribution (PubMed:27207743).</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the non-disulfide-bridged peptide (NDBP) superfamily. Long chain multifunctional peptide (group 2) family.</text>
  </comment>
  <dbReference type="EMBL" id="HF545613">
    <property type="protein sequence ID" value="CCN80315.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="N0EAL3"/>
  <dbReference type="SMR" id="N0EAL3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000432337" description="Mauriporin">
    <location>
      <begin position="23"/>
      <end position="70"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="27207743"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="24184590"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source ref="1"/>
  </evidence>
  <sequence length="73" mass="8417" checksum="F07132E1C03B6BD0" modified="2013-06-26" version="1" precursor="true">MNKKTLLVIFFITMLIVDEVNSFKIGGFIKKLWRSKLAKKLRAKGRELLKDYANRVINGGPEEEAAVPAERRR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>