<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2021-06-02" modified="2024-10-02" version="18" xmlns="http://uniprot.org/uniprot">
  <accession>A0A3G5BIB4</accession>
  <name>ASC3A_DOLGE</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">U-Asilidin(12)-Dg3a</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Dolopus genitalis</name>
    <name type="common">Giant Australian assassin fly</name>
    <name type="synonym">Asilus genitalis</name>
    <dbReference type="NCBI Taxonomy" id="2488630"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Asiloidea</taxon>
      <taxon>Asilidae</taxon>
      <taxon>Asilinae</taxon>
      <taxon>Dolopus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2018" name="Toxins" volume="10" first="E456" last="E456">
      <title>Buzz kill: function and proteomic composition of venom from the giant assassin fly Dolopus genitalis (Diptera: Asilidae).</title>
      <authorList>
        <person name="Walker A.A."/>
        <person name="Dobson J."/>
        <person name="Jin J."/>
        <person name="Robinson S.D."/>
        <person name="Herzig V."/>
        <person name="Vetter I."/>
        <person name="King G.F."/>
        <person name="Fry B.G."/>
      </authorList>
      <dbReference type="PubMed" id="30400621"/>
      <dbReference type="DOI" id="10.3390/toxins10110456"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Moderately increases Kv11.1/KCNH2/ERG1 currents and shifts the voltage-dependence of the channel activation to hyperpolarised potentials (By similarity). In vivo, induces neurotoxic effects when injected into insects (tested on L.cuprina and A.domesticus) (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="6">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="mass spectrometry" mass="4057.85" method="MALDI" evidence="3">
    <text>Monoisotopic mass.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="3">Is abundant in venom, since it accounts for 45.2% of precursor counts.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the asilidin-12 family.</text>
  </comment>
  <dbReference type="EMBL" id="MK075120">
    <property type="protein sequence ID" value="AYV99523.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A3G5BIB4"/>
  <dbReference type="SMR" id="A0A3G5BIB4"/>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006959">
    <property type="term" value="P:humoral immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="CDD" id="cd21806">
    <property type="entry name" value="DEFL_defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001542">
    <property type="entry name" value="Defensin_invertebrate/fungal"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13645">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR13645:SF0">
    <property type="entry name" value="DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01097">
    <property type="entry name" value="Defensin_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51378">
    <property type="entry name" value="INVERT_DEFENSINS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000452536" evidence="6">
    <location>
      <begin position="20"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5018045393" description="U-Asilidin(12)-Dg3a" evidence="3">
    <location>
      <begin position="34"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="36"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="45"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="49"/>
      <end position="67"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A0A3G5BIB1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="30400621"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="30400621"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="30400621"/>
    </source>
  </evidence>
  <sequence length="69" mass="7932" checksum="A58A07CDE09524C6" modified="2019-02-13" version="1" precursor="true">MRFLNIFLFFAAIIAFATASQVFEEDEIDMEPRITCDLIGNERLCVLHCLAKGFRGGWCDGRKVCNCRR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>