<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-11" modified="2024-01-24" version="98" xmlns="http://uniprot.org/uniprot">
  <accession>P64648</accession>
  <accession>P58038</accession>
  <name>GHOT_ECO57</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Toxin GhoT</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="3" type="primary">ghoT</name>
    <name type="ordered locus">Z5731</name>
    <name type="ordered locus">ECs5110.1</name>
  </gene>
  <organism>
    <name type="scientific">Escherichia coli O157:H7</name>
    <dbReference type="NCBI Taxonomy" id="83334"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Escherichia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Nature" volume="409" first="529" last="533">
      <title>Genome sequence of enterohaemorrhagic Escherichia coli O157:H7.</title>
      <authorList>
        <person name="Perna N.T."/>
        <person name="Plunkett G. III"/>
        <person name="Burland V."/>
        <person name="Mau B."/>
        <person name="Glasner J.D."/>
        <person name="Rose D.J."/>
        <person name="Mayhew G.F."/>
        <person name="Evans P.S."/>
        <person name="Gregor J."/>
        <person name="Kirkpatrick H.A."/>
        <person name="Posfai G."/>
        <person name="Hackett J."/>
        <person name="Klink S."/>
        <person name="Boutin A."/>
        <person name="Shao Y."/>
        <person name="Miller L."/>
        <person name="Grotbeck E.J."/>
        <person name="Davis N.W."/>
        <person name="Lim A."/>
        <person name="Dimalanta E.T."/>
        <person name="Potamousis K."/>
        <person name="Apodaca J."/>
        <person name="Anantharaman T.S."/>
        <person name="Lin J."/>
        <person name="Yen G."/>
        <person name="Schwartz D.C."/>
        <person name="Welch R.A."/>
        <person name="Blattner F.R."/>
      </authorList>
      <dbReference type="PubMed" id="11206551"/>
      <dbReference type="DOI" id="10.1038/35054089"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>O157:H7 / EDL933 / ATCC 700927 / EHEC</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2001" name="DNA Res." volume="8" first="11" last="22">
      <title>Complete genome sequence of enterohemorrhagic Escherichia coli O157:H7 and genomic comparison with a laboratory strain K-12.</title>
      <authorList>
        <person name="Hayashi T."/>
        <person name="Makino K."/>
        <person name="Ohnishi M."/>
        <person name="Kurokawa K."/>
        <person name="Ishii K."/>
        <person name="Yokoyama K."/>
        <person name="Han C.-G."/>
        <person name="Ohtsubo E."/>
        <person name="Nakayama K."/>
        <person name="Murata T."/>
        <person name="Tanaka M."/>
        <person name="Tobe T."/>
        <person name="Iida T."/>
        <person name="Takami H."/>
        <person name="Honda T."/>
        <person name="Sasakawa C."/>
        <person name="Ogasawara N."/>
        <person name="Yasunaga T."/>
        <person name="Kuhara S."/>
        <person name="Shiba T."/>
        <person name="Hattori M."/>
        <person name="Shinagawa H."/>
      </authorList>
      <dbReference type="PubMed" id="11258796"/>
      <dbReference type="DOI" id="10.1093/dnares/8.1.11"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>O157:H7 / Sakai / RIMD 0509952 / EHEC</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Toxic component of a type V toxin-antitoxin (TA) system. Causes membrane damage when induced by MqsR, slowing cell growth and leading to the formation of dormant persister cells; involved with GhoS, its antitoxin, in reducing cell growth during antibacterial stress. Its toxic effects are neutralized by GhoS, which digests ghoT transcripts in a sequence-specific manner.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cell inner membrane</location>
      <topology evidence="3">Multi-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the GhoT/OrtT toxin family.</text>
  </comment>
  <dbReference type="EMBL" id="AE005174">
    <property type="protein sequence ID" value="AAG59328.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BA000007">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="D86108">
    <property type="entry name" value="D86108"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_001173343.1">
    <property type="nucleotide sequence ID" value="NZ_VOAI01000008.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P64648"/>
  <dbReference type="STRING" id="155864.Z5731"/>
  <dbReference type="GeneID" id="75203989"/>
  <dbReference type="KEGG" id="ece:Z5731"/>
  <dbReference type="PATRIC" id="fig|83334.175.peg.5666"/>
  <dbReference type="OMA" id="MSTFECI"/>
  <dbReference type="Proteomes" id="UP000000558">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000002519">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019689">
    <property type="entry name" value="Toxin_GhoT/OrtT"/>
  </dbReference>
  <dbReference type="Pfam" id="PF10753">
    <property type="entry name" value="Toxin_GhoT_OrtT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0997">Cell inner membrane</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-1277">Toxin-antitoxin system</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <feature type="chain" id="PRO_0000169736" description="Toxin GhoT">
    <location>
      <begin position="1"/>
      <end position="57"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="7"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="37"/>
      <end position="57"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P64646"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="57" mass="6555" checksum="A3670A19500F75D6" modified="2004-10-11" version="1">MALFSKILIFYVIGVNISFVIIWFISHEKTHIRLLSAFLVGITWPMSLPVALLFSLF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>