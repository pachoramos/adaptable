<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-01-23" modified="2024-11-27" version="174" xmlns="http://uniprot.org/uniprot">
  <accession>Q9H112</accession>
  <accession>Q0VAF2</accession>
  <accession>Q0VAF3</accession>
  <accession>Q8WXU5</accession>
  <accession>Q8WXU6</accession>
  <accession>Q9H113</accession>
  <name>CST11_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Cystatin-11</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">CST11</name>
    <name type="synonym">CST8L</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Endocrinology" volume="143" first="2787" last="2796">
      <title>Cystatin 11: a new member of the cystatin type 2 family.</title>
      <authorList>
        <person name="Hamil K.G."/>
        <person name="Liu Q."/>
        <person name="Sivashanmugam P."/>
        <person name="Yenugu S."/>
        <person name="Soundararajan R."/>
        <person name="Grossman G."/>
        <person name="Richardson R.T."/>
        <person name="Zhang Y.-L."/>
        <person name="O'Rand M.G."/>
        <person name="Petrusz P."/>
        <person name="French F.S."/>
        <person name="Hall S.H."/>
      </authorList>
      <dbReference type="PubMed" id="12072414"/>
      <dbReference type="DOI" id="10.1210/endo.143.7.8925"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORMS 1 AND 2)</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2001" name="Nature" volume="414" first="865" last="871">
      <title>The DNA sequence and comparative analysis of human chromosome 20.</title>
      <authorList>
        <person name="Deloukas P."/>
        <person name="Matthews L.H."/>
        <person name="Ashurst J.L."/>
        <person name="Burton J."/>
        <person name="Gilbert J.G.R."/>
        <person name="Jones M."/>
        <person name="Stavrides G."/>
        <person name="Almeida J.P."/>
        <person name="Babbage A.K."/>
        <person name="Bagguley C.L."/>
        <person name="Bailey J."/>
        <person name="Barlow K.F."/>
        <person name="Bates K.N."/>
        <person name="Beard L.M."/>
        <person name="Beare D.M."/>
        <person name="Beasley O.P."/>
        <person name="Bird C.P."/>
        <person name="Blakey S.E."/>
        <person name="Bridgeman A.M."/>
        <person name="Brown A.J."/>
        <person name="Buck D."/>
        <person name="Burrill W.D."/>
        <person name="Butler A.P."/>
        <person name="Carder C."/>
        <person name="Carter N.P."/>
        <person name="Chapman J.C."/>
        <person name="Clamp M."/>
        <person name="Clark G."/>
        <person name="Clark L.N."/>
        <person name="Clark S.Y."/>
        <person name="Clee C.M."/>
        <person name="Clegg S."/>
        <person name="Cobley V.E."/>
        <person name="Collier R.E."/>
        <person name="Connor R.E."/>
        <person name="Corby N.R."/>
        <person name="Coulson A."/>
        <person name="Coville G.J."/>
        <person name="Deadman R."/>
        <person name="Dhami P.D."/>
        <person name="Dunn M."/>
        <person name="Ellington A.G."/>
        <person name="Frankland J.A."/>
        <person name="Fraser A."/>
        <person name="French L."/>
        <person name="Garner P."/>
        <person name="Grafham D.V."/>
        <person name="Griffiths C."/>
        <person name="Griffiths M.N.D."/>
        <person name="Gwilliam R."/>
        <person name="Hall R.E."/>
        <person name="Hammond S."/>
        <person name="Harley J.L."/>
        <person name="Heath P.D."/>
        <person name="Ho S."/>
        <person name="Holden J.L."/>
        <person name="Howden P.J."/>
        <person name="Huckle E."/>
        <person name="Hunt A.R."/>
        <person name="Hunt S.E."/>
        <person name="Jekosch K."/>
        <person name="Johnson C.M."/>
        <person name="Johnson D."/>
        <person name="Kay M.P."/>
        <person name="Kimberley A.M."/>
        <person name="King A."/>
        <person name="Knights A."/>
        <person name="Laird G.K."/>
        <person name="Lawlor S."/>
        <person name="Lehvaeslaiho M.H."/>
        <person name="Leversha M.A."/>
        <person name="Lloyd C."/>
        <person name="Lloyd D.M."/>
        <person name="Lovell J.D."/>
        <person name="Marsh V.L."/>
        <person name="Martin S.L."/>
        <person name="McConnachie L.J."/>
        <person name="McLay K."/>
        <person name="McMurray A.A."/>
        <person name="Milne S.A."/>
        <person name="Mistry D."/>
        <person name="Moore M.J.F."/>
        <person name="Mullikin J.C."/>
        <person name="Nickerson T."/>
        <person name="Oliver K."/>
        <person name="Parker A."/>
        <person name="Patel R."/>
        <person name="Pearce T.A.V."/>
        <person name="Peck A.I."/>
        <person name="Phillimore B.J.C.T."/>
        <person name="Prathalingam S.R."/>
        <person name="Plumb R.W."/>
        <person name="Ramsay H."/>
        <person name="Rice C.M."/>
        <person name="Ross M.T."/>
        <person name="Scott C.E."/>
        <person name="Sehra H.K."/>
        <person name="Shownkeen R."/>
        <person name="Sims S."/>
        <person name="Skuce C.D."/>
        <person name="Smith M.L."/>
        <person name="Soderlund C."/>
        <person name="Steward C.A."/>
        <person name="Sulston J.E."/>
        <person name="Swann R.M."/>
        <person name="Sycamore N."/>
        <person name="Taylor R."/>
        <person name="Tee L."/>
        <person name="Thomas D.W."/>
        <person name="Thorpe A."/>
        <person name="Tracey A."/>
        <person name="Tromans A.C."/>
        <person name="Vaudin M."/>
        <person name="Wall M."/>
        <person name="Wallis J.M."/>
        <person name="Whitehead S.L."/>
        <person name="Whittaker P."/>
        <person name="Willey D.L."/>
        <person name="Williams L."/>
        <person name="Williams S.A."/>
        <person name="Wilming L."/>
        <person name="Wray P.W."/>
        <person name="Hubbard T."/>
        <person name="Durbin R.M."/>
        <person name="Bentley D.R."/>
        <person name="Beck S."/>
        <person name="Rogers J."/>
      </authorList>
      <dbReference type="PubMed" id="11780052"/>
      <dbReference type="DOI" id="10.1038/414865a"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2005-09" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Istrail S."/>
        <person name="Sutton G.G."/>
        <person name="Florea L."/>
        <person name="Halpern A.L."/>
        <person name="Mobarry C.M."/>
        <person name="Lippert R."/>
        <person name="Walenz B."/>
        <person name="Shatkay H."/>
        <person name="Dew I."/>
        <person name="Miller J.R."/>
        <person name="Flanigan M.J."/>
        <person name="Edwards N.J."/>
        <person name="Bolanos R."/>
        <person name="Fasulo D."/>
        <person name="Halldorsson B.V."/>
        <person name="Hannenhalli S."/>
        <person name="Turner R."/>
        <person name="Yooseph S."/>
        <person name="Lu F."/>
        <person name="Nusskern D.R."/>
        <person name="Shue B.C."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F."/>
        <person name="Delcher A.L."/>
        <person name="Huson D.H."/>
        <person name="Kravitz S.A."/>
        <person name="Mouchard L."/>
        <person name="Reinert K."/>
        <person name="Remington K.A."/>
        <person name="Clark A.G."/>
        <person name="Waterman M.S."/>
        <person name="Eichler E.E."/>
        <person name="Adams M.D."/>
        <person name="Hunkapiller M.W."/>
        <person name="Myers E.W."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORMS 1 AND 2)</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Has antibacterial activity against the Gram-negative bacteria E.coli. May play a role in sperm maturation and fertilization.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="7">Secreted</location>
    </subcellularLocation>
    <text evidence="3">Probably secreted into the epididymis lumen, where it localizes to the outer surface of sperm. Specifically localizes to the postacrosomal and tail regions of sperm.</text>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>Q9H112-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>Q9H112-2</id>
      <name>2</name>
      <sequence type="described" ref="VSP_001260"/>
    </isoform>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Detected in the epithelium and lumen of the epididymis, and in sperm (at protein level).</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the cystatin family.</text>
  </comment>
  <dbReference type="EMBL" id="AF335480">
    <property type="protein sequence ID" value="AAL71991.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF335481">
    <property type="protein sequence ID" value="AAL71992.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL096677">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH471133">
    <property type="protein sequence ID" value="EAX10146.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC121079">
    <property type="protein sequence ID" value="AAI21080.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC121080">
    <property type="protein sequence ID" value="AAI21081.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS13154.1">
    <molecule id="Q9H112-2"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS13155.1">
    <molecule id="Q9H112-1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_543020.2">
    <molecule id="Q9H112-2"/>
    <property type="nucleotide sequence ID" value="NM_080830.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_570612.1">
    <molecule id="Q9H112-1"/>
    <property type="nucleotide sequence ID" value="NM_130794.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9H112"/>
  <dbReference type="SMR" id="Q9H112"/>
  <dbReference type="BioGRID" id="126748">
    <property type="interactions" value="97"/>
  </dbReference>
  <dbReference type="IntAct" id="Q9H112">
    <property type="interactions" value="71"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000366208"/>
  <dbReference type="MEROPS" id="I25.027"/>
  <dbReference type="GlyCosmos" id="Q9H112">
    <property type="glycosylation" value="1 site, No reported glycans"/>
  </dbReference>
  <dbReference type="GlyGen" id="Q9H112">
    <property type="glycosylation" value="1 site"/>
  </dbReference>
  <dbReference type="iPTMnet" id="Q9H112"/>
  <dbReference type="PhosphoSitePlus" id="Q9H112"/>
  <dbReference type="BioMuta" id="CST11"/>
  <dbReference type="DMDM" id="76803548"/>
  <dbReference type="MassIVE" id="Q9H112"/>
  <dbReference type="PaxDb" id="9606-ENSP00000366208"/>
  <dbReference type="PeptideAtlas" id="Q9H112"/>
  <dbReference type="Antibodypedia" id="54198">
    <property type="antibodies" value="79 antibodies from 16 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="140880"/>
  <dbReference type="Ensembl" id="ENST00000377007.3">
    <molecule id="Q9H112-2"/>
    <property type="protein sequence ID" value="ENSP00000366206.3"/>
    <property type="gene ID" value="ENSG00000125831.10"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000377009.8">
    <molecule id="Q9H112-1"/>
    <property type="protein sequence ID" value="ENSP00000366208.3"/>
    <property type="gene ID" value="ENSG00000125831.10"/>
  </dbReference>
  <dbReference type="GeneID" id="140880"/>
  <dbReference type="KEGG" id="hsa:140880"/>
  <dbReference type="MANE-Select" id="ENST00000377009.8">
    <property type="protein sequence ID" value="ENSP00000366208.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_130794.2"/>
    <property type="RefSeq protein sequence ID" value="NP_570612.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc002wtf.2">
    <molecule id="Q9H112-1"/>
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:15959"/>
  <dbReference type="CTD" id="140880"/>
  <dbReference type="DisGeNET" id="140880"/>
  <dbReference type="GeneCards" id="CST11"/>
  <dbReference type="HGNC" id="HGNC:15959">
    <property type="gene designation" value="CST11"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000125831">
    <property type="expression patterns" value="Tissue enriched (epididymis)"/>
  </dbReference>
  <dbReference type="MIM" id="609731">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_Q9H112"/>
  <dbReference type="OpenTargets" id="ENSG00000125831"/>
  <dbReference type="PharmGKB" id="PA26974"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000125831"/>
  <dbReference type="eggNOG" id="ENOG502RWFM">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00910000144356"/>
  <dbReference type="HOGENOM" id="CLU_118168_2_1_1"/>
  <dbReference type="InParanoid" id="Q9H112"/>
  <dbReference type="OMA" id="NCVPQEG"/>
  <dbReference type="OrthoDB" id="5261405at2759"/>
  <dbReference type="PhylomeDB" id="Q9H112"/>
  <dbReference type="PathwayCommons" id="Q9H112"/>
  <dbReference type="BioGRID-ORCS" id="140880">
    <property type="hits" value="13 hits in 1144 CRISPR screens"/>
  </dbReference>
  <dbReference type="GeneWiki" id="CST11"/>
  <dbReference type="GenomeRNAi" id="140880"/>
  <dbReference type="Pharos" id="Q9H112">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q9H112"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 20"/>
  </dbReference>
  <dbReference type="RNAct" id="Q9H112">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000125831">
    <property type="expression patterns" value="Expressed in corpus epididymis and 55 other cell types or tissues"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q9H112">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0036126">
    <property type="term" value="C:sperm flagellum"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061827">
    <property type="term" value="C:sperm head"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030521">
    <property type="term" value="P:androgen receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00042">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.450.10:FF:000019">
    <property type="entry name" value="Cystatin 11"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.450.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR042930">
    <property type="entry name" value="CST11"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000010">
    <property type="entry name" value="Cystatin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046350">
    <property type="entry name" value="Cystatin_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47886">
    <property type="entry name" value="CYSTATIN-11"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR47886:SF1">
    <property type="entry name" value="CYSTATIN-11"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00031">
    <property type="entry name" value="Cystatin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00043">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54403">
    <property type="entry name" value="Cystatin/monellin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000006658" description="Cystatin-11">
    <location>
      <begin position="27"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="2">
    <location>
      <position position="132"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="94"/>
      <end position="102"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="115"/>
      <end position="135"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_001260" description="In isoform 2." evidence="4 5">
    <location>
      <begin position="77"/>
      <end position="111"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="O76096"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12072414"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="12072414"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="15489334"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="12072414"/>
    </source>
  </evidence>
  <sequence length="138" mass="16506" checksum="E49440ACA3585C64" modified="2005-09-27" version="2" precursor="true">MMAEPWQALQLLLAILLTLMALPYQARKKTFLSVHEVMAVENYAKDSLQWITDQYNKESDDKYHFRIFRVLKVQRQVTDHLEYHLNVEMQWTTCQKPETTNCVPQERELHKQVNCFFSVFAVPWFEQYKILNKSCSSD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>