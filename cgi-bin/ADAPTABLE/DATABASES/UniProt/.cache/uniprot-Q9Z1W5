<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-02-06" modified="2024-11-27" version="146" xmlns="http://uniprot.org/uniprot">
  <accession>Q9Z1W5</accession>
  <accession>Q3TMC9</accession>
  <name>SERP1_MOUSE</name>
  <protein>
    <recommendedName>
      <fullName>Stress-associated endoplasmic reticulum protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Ribosome-attached membrane protein 4</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Serp1</name>
    <name type="synonym">D3Ucla1</name>
    <name type="synonym">Ramp4</name>
  </gene>
  <organism>
    <name type="scientific">Mus musculus</name>
    <name type="common">Mouse</name>
    <dbReference type="NCBI Taxonomy" id="10090"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Mus</taxon>
      <taxon>Mus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Science" volume="309" first="1559" last="1563">
      <title>The transcriptional landscape of the mammalian genome.</title>
      <authorList>
        <person name="Carninci P."/>
        <person name="Kasukawa T."/>
        <person name="Katayama S."/>
        <person name="Gough J."/>
        <person name="Frith M.C."/>
        <person name="Maeda N."/>
        <person name="Oyama R."/>
        <person name="Ravasi T."/>
        <person name="Lenhard B."/>
        <person name="Wells C."/>
        <person name="Kodzius R."/>
        <person name="Shimokawa K."/>
        <person name="Bajic V.B."/>
        <person name="Brenner S.E."/>
        <person name="Batalov S."/>
        <person name="Forrest A.R."/>
        <person name="Zavolan M."/>
        <person name="Davis M.J."/>
        <person name="Wilming L.G."/>
        <person name="Aidinis V."/>
        <person name="Allen J.E."/>
        <person name="Ambesi-Impiombato A."/>
        <person name="Apweiler R."/>
        <person name="Aturaliya R.N."/>
        <person name="Bailey T.L."/>
        <person name="Bansal M."/>
        <person name="Baxter L."/>
        <person name="Beisel K.W."/>
        <person name="Bersano T."/>
        <person name="Bono H."/>
        <person name="Chalk A.M."/>
        <person name="Chiu K.P."/>
        <person name="Choudhary V."/>
        <person name="Christoffels A."/>
        <person name="Clutterbuck D.R."/>
        <person name="Crowe M.L."/>
        <person name="Dalla E."/>
        <person name="Dalrymple B.P."/>
        <person name="de Bono B."/>
        <person name="Della Gatta G."/>
        <person name="di Bernardo D."/>
        <person name="Down T."/>
        <person name="Engstrom P."/>
        <person name="Fagiolini M."/>
        <person name="Faulkner G."/>
        <person name="Fletcher C.F."/>
        <person name="Fukushima T."/>
        <person name="Furuno M."/>
        <person name="Futaki S."/>
        <person name="Gariboldi M."/>
        <person name="Georgii-Hemming P."/>
        <person name="Gingeras T.R."/>
        <person name="Gojobori T."/>
        <person name="Green R.E."/>
        <person name="Gustincich S."/>
        <person name="Harbers M."/>
        <person name="Hayashi Y."/>
        <person name="Hensch T.K."/>
        <person name="Hirokawa N."/>
        <person name="Hill D."/>
        <person name="Huminiecki L."/>
        <person name="Iacono M."/>
        <person name="Ikeo K."/>
        <person name="Iwama A."/>
        <person name="Ishikawa T."/>
        <person name="Jakt M."/>
        <person name="Kanapin A."/>
        <person name="Katoh M."/>
        <person name="Kawasawa Y."/>
        <person name="Kelso J."/>
        <person name="Kitamura H."/>
        <person name="Kitano H."/>
        <person name="Kollias G."/>
        <person name="Krishnan S.P."/>
        <person name="Kruger A."/>
        <person name="Kummerfeld S.K."/>
        <person name="Kurochkin I.V."/>
        <person name="Lareau L.F."/>
        <person name="Lazarevic D."/>
        <person name="Lipovich L."/>
        <person name="Liu J."/>
        <person name="Liuni S."/>
        <person name="McWilliam S."/>
        <person name="Madan Babu M."/>
        <person name="Madera M."/>
        <person name="Marchionni L."/>
        <person name="Matsuda H."/>
        <person name="Matsuzawa S."/>
        <person name="Miki H."/>
        <person name="Mignone F."/>
        <person name="Miyake S."/>
        <person name="Morris K."/>
        <person name="Mottagui-Tabar S."/>
        <person name="Mulder N."/>
        <person name="Nakano N."/>
        <person name="Nakauchi H."/>
        <person name="Ng P."/>
        <person name="Nilsson R."/>
        <person name="Nishiguchi S."/>
        <person name="Nishikawa S."/>
        <person name="Nori F."/>
        <person name="Ohara O."/>
        <person name="Okazaki Y."/>
        <person name="Orlando V."/>
        <person name="Pang K.C."/>
        <person name="Pavan W.J."/>
        <person name="Pavesi G."/>
        <person name="Pesole G."/>
        <person name="Petrovsky N."/>
        <person name="Piazza S."/>
        <person name="Reed J."/>
        <person name="Reid J.F."/>
        <person name="Ring B.Z."/>
        <person name="Ringwald M."/>
        <person name="Rost B."/>
        <person name="Ruan Y."/>
        <person name="Salzberg S.L."/>
        <person name="Sandelin A."/>
        <person name="Schneider C."/>
        <person name="Schoenbach C."/>
        <person name="Sekiguchi K."/>
        <person name="Semple C.A."/>
        <person name="Seno S."/>
        <person name="Sessa L."/>
        <person name="Sheng Y."/>
        <person name="Shibata Y."/>
        <person name="Shimada H."/>
        <person name="Shimada K."/>
        <person name="Silva D."/>
        <person name="Sinclair B."/>
        <person name="Sperling S."/>
        <person name="Stupka E."/>
        <person name="Sugiura K."/>
        <person name="Sultana R."/>
        <person name="Takenaka Y."/>
        <person name="Taki K."/>
        <person name="Tammoja K."/>
        <person name="Tan S.L."/>
        <person name="Tang S."/>
        <person name="Taylor M.S."/>
        <person name="Tegner J."/>
        <person name="Teichmann S.A."/>
        <person name="Ueda H.R."/>
        <person name="van Nimwegen E."/>
        <person name="Verardo R."/>
        <person name="Wei C.L."/>
        <person name="Yagi K."/>
        <person name="Yamanishi H."/>
        <person name="Zabarovsky E."/>
        <person name="Zhu S."/>
        <person name="Zimmer A."/>
        <person name="Hide W."/>
        <person name="Bult C."/>
        <person name="Grimmond S.M."/>
        <person name="Teasdale R.D."/>
        <person name="Liu E.T."/>
        <person name="Brusic V."/>
        <person name="Quackenbush J."/>
        <person name="Wahlestedt C."/>
        <person name="Mattick J.S."/>
        <person name="Hume D.A."/>
        <person name="Kai C."/>
        <person name="Sasaki D."/>
        <person name="Tomaru Y."/>
        <person name="Fukuda S."/>
        <person name="Kanamori-Katayama M."/>
        <person name="Suzuki M."/>
        <person name="Aoki J."/>
        <person name="Arakawa T."/>
        <person name="Iida J."/>
        <person name="Imamura K."/>
        <person name="Itoh M."/>
        <person name="Kato T."/>
        <person name="Kawaji H."/>
        <person name="Kawagashira N."/>
        <person name="Kawashima T."/>
        <person name="Kojima M."/>
        <person name="Kondo S."/>
        <person name="Konno H."/>
        <person name="Nakano K."/>
        <person name="Ninomiya N."/>
        <person name="Nishio T."/>
        <person name="Okada M."/>
        <person name="Plessy C."/>
        <person name="Shibata K."/>
        <person name="Shiraki T."/>
        <person name="Suzuki S."/>
        <person name="Tagami M."/>
        <person name="Waki K."/>
        <person name="Watahiki A."/>
        <person name="Okamura-Oho Y."/>
        <person name="Suzuki H."/>
        <person name="Kawai J."/>
        <person name="Hayashizaki Y."/>
      </authorList>
      <dbReference type="PubMed" id="16141072"/>
      <dbReference type="DOI" id="10.1126/science.1112014"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>BALB/cJ</strain>
      <strain>C57BL/6J</strain>
      <strain>NOD</strain>
      <tissue>Amnion</tissue>
      <tissue>Bone marrow macrophage</tissue>
      <tissue>Cerebellum</tissue>
      <tissue>Dendritic cell</tissue>
      <tissue>Embryonic heart</tissue>
      <tissue>Embryonic liver</tissue>
      <tissue>Kidney</tissue>
      <tissue>Lung</tissue>
      <tissue>Pancreas</tissue>
      <tissue>Pituitary</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>C57BL/6J</strain>
      <strain>FVB/N</strain>
      <tissue>Mammary tumor</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Interacts with target proteins during their translocation into the lumen of the endoplasmic reticulum. Protects unfolded target proteins against degradation during ER stress. May facilitate glycosylation of target proteins after termination of ER stress. May modulate the use of N-glycosylation sites on target proteins.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with SEC61B, SEC61A1 and the SEC61 complex. Interacts with CANX.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Membrane</location>
      <topology evidence="1">Single-pass membrane protein</topology>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Endoplasmic reticulum membrane</location>
      <topology evidence="1">Single-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the RAMP4 family.</text>
  </comment>
  <dbReference type="EMBL" id="AK002653">
    <property type="protein sequence ID" value="BAB22260.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AB041655">
    <property type="protein sequence ID" value="BAB93547.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK032680">
    <property type="protein sequence ID" value="BAC27985.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK050508">
    <property type="protein sequence ID" value="BAC34297.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK075776">
    <property type="protein sequence ID" value="BAC35950.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK144732">
    <property type="protein sequence ID" value="BAE26037.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK148485">
    <property type="protein sequence ID" value="BAE28580.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK149786">
    <property type="protein sequence ID" value="BAE29083.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK150005">
    <property type="protein sequence ID" value="BAE29232.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK150025">
    <property type="protein sequence ID" value="BAE29250.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK150368">
    <property type="protein sequence ID" value="BAE29501.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK150428">
    <property type="protein sequence ID" value="BAE29551.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK150659">
    <property type="protein sequence ID" value="BAE29745.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK150714">
    <property type="protein sequence ID" value="BAE29792.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK150770">
    <property type="protein sequence ID" value="BAE29836.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK150809">
    <property type="protein sequence ID" value="BAE29872.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK151323">
    <property type="protein sequence ID" value="BAE30304.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK151384">
    <property type="protein sequence ID" value="BAE30356.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK151456">
    <property type="protein sequence ID" value="BAE30415.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK151669">
    <property type="protein sequence ID" value="BAE30595.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK152373">
    <property type="protein sequence ID" value="BAE31163.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK152383">
    <property type="protein sequence ID" value="BAE31171.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK152583">
    <property type="protein sequence ID" value="BAE31333.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK152636">
    <property type="protein sequence ID" value="BAE31378.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK152806">
    <property type="protein sequence ID" value="BAE31511.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK152882">
    <property type="protein sequence ID" value="BAE31567.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK152996">
    <property type="protein sequence ID" value="BAE31639.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK153056">
    <property type="protein sequence ID" value="BAE31682.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK153118">
    <property type="protein sequence ID" value="BAE31733.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK153138">
    <property type="protein sequence ID" value="BAE31750.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK153286">
    <property type="protein sequence ID" value="BAE31869.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK153415">
    <property type="protein sequence ID" value="BAE31975.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK159271">
    <property type="protein sequence ID" value="BAE34950.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK161595">
    <property type="protein sequence ID" value="BAE36482.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK166004">
    <property type="protein sequence ID" value="BAE38513.1"/>
    <property type="status" value="ALT_TERM"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK166052">
    <property type="protein sequence ID" value="BAE38545.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK167482">
    <property type="protein sequence ID" value="BAE39563.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK167952">
    <property type="protein sequence ID" value="BAE39953.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK168046">
    <property type="protein sequence ID" value="BAE40028.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK168569">
    <property type="protein sequence ID" value="BAE40439.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK168734">
    <property type="protein sequence ID" value="BAE40575.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK168754">
    <property type="protein sequence ID" value="BAE40592.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK168947">
    <property type="protein sequence ID" value="BAE40753.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK169049">
    <property type="protein sequence ID" value="BAE40837.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK169175">
    <property type="protein sequence ID" value="BAE40954.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AK170734">
    <property type="protein sequence ID" value="BAE41989.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC002114">
    <property type="protein sequence ID" value="AAH02114.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS38436.1"/>
  <dbReference type="RefSeq" id="NP_109610.1">
    <property type="nucleotide sequence ID" value="NM_030685.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9Z1W5"/>
  <dbReference type="SMR" id="Q9Z1W5"/>
  <dbReference type="BioGRID" id="205786">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="10090.ENSMUSP00000029385"/>
  <dbReference type="PhosphoSitePlus" id="Q9Z1W5"/>
  <dbReference type="PaxDb" id="10090-ENSMUSP00000029385"/>
  <dbReference type="Antibodypedia" id="46738">
    <property type="antibodies" value="106 antibodies from 23 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="28146"/>
  <dbReference type="Ensembl" id="ENSMUST00000029385.9">
    <property type="protein sequence ID" value="ENSMUSP00000029385.8"/>
    <property type="gene ID" value="ENSMUSG00000027808.9"/>
  </dbReference>
  <dbReference type="GeneID" id="28146"/>
  <dbReference type="KEGG" id="mmu:28146"/>
  <dbReference type="UCSC" id="uc008phs.2">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="AGR" id="MGI:92638"/>
  <dbReference type="CTD" id="27230"/>
  <dbReference type="MGI" id="MGI:92638">
    <property type="gene designation" value="Serp1"/>
  </dbReference>
  <dbReference type="VEuPathDB" id="HostDB:ENSMUSG00000027808"/>
  <dbReference type="eggNOG" id="KOG3491">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000161729"/>
  <dbReference type="HOGENOM" id="CLU_160944_3_0_1"/>
  <dbReference type="InParanoid" id="Q9Z1W5"/>
  <dbReference type="OMA" id="KNISQCA"/>
  <dbReference type="OrthoDB" id="1326375at2759"/>
  <dbReference type="PhylomeDB" id="Q9Z1W5"/>
  <dbReference type="TreeFam" id="TF313229"/>
  <dbReference type="Reactome" id="R-MMU-9609523">
    <property type="pathway name" value="Insertion of tail-anchored proteins into the endoplasmic reticulum membrane"/>
  </dbReference>
  <dbReference type="BioGRID-ORCS" id="28146">
    <property type="hits" value="1 hit in 78 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="Serp1">
    <property type="organism name" value="mouse"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q9Z1W5"/>
  <dbReference type="Proteomes" id="UP000000589">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="RNAct" id="Q9Z1W5">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSMUSG00000027808">
    <property type="expression patterns" value="Expressed in parotid gland and 267 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005881">
    <property type="term" value="C:cytoplasmic microtubule"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005789">
    <property type="term" value="C:endoplasmic reticulum membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030968">
    <property type="term" value="P:endoplasmic reticulum unfolded protein response"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006006">
    <property type="term" value="P:glucose metabolic process"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030073">
    <property type="term" value="P:insulin secretion"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048644">
    <property type="term" value="P:muscle organ morphogenesis"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060124">
    <property type="term" value="P:positive regulation of growth hormone secretion"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032024">
    <property type="term" value="P:positive regulation of insulin secretion"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046622">
    <property type="term" value="P:positive regulation of organ growth"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045727">
    <property type="term" value="P:positive regulation of translation"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009791">
    <property type="term" value="P:post-embryonic development"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001501">
    <property type="term" value="P:skeletal system development"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR010580">
    <property type="entry name" value="ER_stress-assoc"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15601">
    <property type="entry name" value="STRESS ASSOCIATED ENDOPLASMIC RETICULUM PROTEIN SERP1/RAMP4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15601:SF14">
    <property type="entry name" value="STRESS-ASSOCIATED ENDOPLASMIC RETICULUM PROTEIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06624">
    <property type="entry name" value="RAMP4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0256">Endoplasmic reticulum</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <keyword id="KW-0834">Unfolded protein response</keyword>
  <feature type="chain" id="PRO_0000274795" description="Stress-associated endoplasmic reticulum protein 1">
    <location>
      <begin position="1"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="39"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q9R2C1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="66" mass="7374" checksum="DC15C0143AAE0D91" modified="1999-05-01" version="1">MVAKQRIRMANEKHSKNITQRGNVAKTSRNAPEEKASVGPWLLALFIFVVCGSAIFQIIQSIRMGM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>