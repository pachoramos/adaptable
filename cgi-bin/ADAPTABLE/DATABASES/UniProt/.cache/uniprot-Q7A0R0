<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-10-25" modified="2024-07-24" version="86" xmlns="http://uniprot.org/uniprot">
  <accession>Q7A0R0</accession>
  <name>Y1575_STAAW</name>
  <protein>
    <recommendedName>
      <fullName>UPF0337 protein MW1575</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ordered locus">MW1575</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="similarity">
    <text evidence="2">Belongs to the UPF0337 (CsbD) family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB95440.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000752909.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q7A0R0"/>
  <dbReference type="SMR" id="Q7A0R0"/>
  <dbReference type="KEGG" id="sam:MW1575"/>
  <dbReference type="HOGENOM" id="CLU_135567_0_3_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.1470.10">
    <property type="entry name" value="YjbJ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008462">
    <property type="entry name" value="CsbD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050423">
    <property type="entry name" value="UPF0337_stress_rsp"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036629">
    <property type="entry name" value="YjbJ_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34977">
    <property type="entry name" value="UPF0337 PROTEIN YJBJ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34977:SF1">
    <property type="entry name" value="UPF0337 PROTEIN YJBJ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF05532">
    <property type="entry name" value="CsbD"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF69047">
    <property type="entry name" value="Hypothetical protein YjbJ"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <feature type="chain" id="PRO_0000210037" description="UPF0337 protein MW1575">
    <location>
      <begin position="1"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="18"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="1">
    <location>
      <begin position="21"/>
      <end position="41"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="60" mass="6682" checksum="3385A6A8AC7B3DD1" modified="2004-07-05" version="1">MADESKFDQFKGNVKETVGNVTDNKELEKEGQQDKATGKAKEVVENAKNKITDAIDKLKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>