<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-09-05" modified="2023-02-22" version="34" xmlns="http://uniprot.org/uniprot">
  <accession>P84909</accession>
  <name>PROK1_PELSA</name>
  <protein>
    <recommendedName>
      <fullName>Prokineticin 1-like protein</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pelophylax saharicus</name>
    <name type="common">Sahara frog</name>
    <name type="synonym">Rana saharica</name>
    <dbReference type="NCBI Taxonomy" id="70019"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Pelophylax</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Peptides" volume="26" first="2117" last="2123">
      <title>Isolation and structural characterization of novel Rugosin A-like insulinotropic peptide from the skin secretions of Rana saharica frog.</title>
      <authorList>
        <person name="Marenah L."/>
        <person name="Flatt P.R."/>
        <person name="Orr D.F."/>
        <person name="Shaw C."/>
        <person name="Abdel-Wahab Y.H.A."/>
      </authorList>
      <dbReference type="PubMed" id="16269346"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2005.03.008"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Stimulates insulin secretion by BRIN-BD11 cells in vitro.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2930.8" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the AVIT (prokineticin) family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84909"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032024">
    <property type="term" value="P:positive regulation of insulin secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.10.80.10">
    <property type="entry name" value="Lipase, subunit A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023569">
    <property type="entry name" value="Prokineticin_domain"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06607">
    <property type="entry name" value="Prokineticin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57190">
    <property type="entry name" value="Colipase-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000248508" description="Prokineticin 1-like protein">
    <location>
      <begin position="1"/>
      <end position="24" status="greater than"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="7"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="13"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="18"/>
      <end status="unknown"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="3">
    <location>
      <position position="24"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P25687"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="16269346"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="16269346"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="24" mass="2327" checksum="0C348F8FC6E61785" modified="2006-09-05" version="1" fragment="single">AVITGACERDVQCGGGTCCAVSLI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>