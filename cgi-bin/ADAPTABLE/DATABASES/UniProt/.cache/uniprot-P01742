<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="138" xmlns="http://uniprot.org/uniprot">
  <accession>P01742</accession>
  <accession>A0A0B4J1V5</accession>
  <accession>P01760</accession>
  <accession>P01761</accession>
  <name>HV169_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="6 11">Immunoglobulin heavy variable 1-69</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="13">Ig heavy chain V-I region EU</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="14">Ig heavy chain V-I region SIE</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="14">Ig heavy chain V-I region WOL</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="6 11" type="primary">IGHV1-69</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Nature" volume="421" first="601" last="607">
      <title>The DNA sequence and analysis of human chromosome 14.</title>
      <authorList>
        <person name="Heilig R."/>
        <person name="Eckenberg R."/>
        <person name="Petit J.-L."/>
        <person name="Fonknechten N."/>
        <person name="Da Silva C."/>
        <person name="Cattolico L."/>
        <person name="Levy M."/>
        <person name="Barbe V."/>
        <person name="De Berardinis V."/>
        <person name="Ureta-Vidal A."/>
        <person name="Pelletier E."/>
        <person name="Vico V."/>
        <person name="Anthouard V."/>
        <person name="Rowen L."/>
        <person name="Madan A."/>
        <person name="Qin S."/>
        <person name="Sun H."/>
        <person name="Du H."/>
        <person name="Pepin K."/>
        <person name="Artiguenave F."/>
        <person name="Robert C."/>
        <person name="Cruaud C."/>
        <person name="Bruels T."/>
        <person name="Jaillon O."/>
        <person name="Friedlander L."/>
        <person name="Samson G."/>
        <person name="Brottier P."/>
        <person name="Cure S."/>
        <person name="Segurens B."/>
        <person name="Aniere F."/>
        <person name="Samain S."/>
        <person name="Crespeau H."/>
        <person name="Abbasi N."/>
        <person name="Aiach N."/>
        <person name="Boscus D."/>
        <person name="Dickhoff R."/>
        <person name="Dors M."/>
        <person name="Dubois I."/>
        <person name="Friedman C."/>
        <person name="Gouyvenoux M."/>
        <person name="James R."/>
        <person name="Madan A."/>
        <person name="Mairey-Estrada B."/>
        <person name="Mangenot S."/>
        <person name="Martins N."/>
        <person name="Menard M."/>
        <person name="Oztas S."/>
        <person name="Ratcliffe A."/>
        <person name="Shaffer T."/>
        <person name="Trask B."/>
        <person name="Vacherie B."/>
        <person name="Bellemere C."/>
        <person name="Belser C."/>
        <person name="Besnard-Gonnet M."/>
        <person name="Bartol-Mavel D."/>
        <person name="Boutard M."/>
        <person name="Briez-Silla S."/>
        <person name="Combette S."/>
        <person name="Dufosse-Laurent V."/>
        <person name="Ferron C."/>
        <person name="Lechaplais C."/>
        <person name="Louesse C."/>
        <person name="Muselet D."/>
        <person name="Magdelenat G."/>
        <person name="Pateau E."/>
        <person name="Petit E."/>
        <person name="Sirvain-Trukniewicz P."/>
        <person name="Trybou A."/>
        <person name="Vega-Czarny N."/>
        <person name="Bataille E."/>
        <person name="Bluet E."/>
        <person name="Bordelais I."/>
        <person name="Dubois M."/>
        <person name="Dumont C."/>
        <person name="Guerin T."/>
        <person name="Haffray S."/>
        <person name="Hammadi R."/>
        <person name="Muanga J."/>
        <person name="Pellouin V."/>
        <person name="Robert D."/>
        <person name="Wunderle E."/>
        <person name="Gauguet G."/>
        <person name="Roy A."/>
        <person name="Sainte-Marthe L."/>
        <person name="Verdier J."/>
        <person name="Verdier-Discala C."/>
        <person name="Hillier L.W."/>
        <person name="Fulton L."/>
        <person name="McPherson J."/>
        <person name="Matsuda F."/>
        <person name="Wilson R."/>
        <person name="Scarpelli C."/>
        <person name="Gyapay G."/>
        <person name="Wincker P."/>
        <person name="Saurin W."/>
        <person name="Quetier F."/>
        <person name="Waterston R."/>
        <person name="Hood L."/>
        <person name="Weissenbach J."/>
      </authorList>
      <dbReference type="PubMed" id="12508121"/>
      <dbReference type="DOI" id="10.1038/nature01348"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA] (IMGT ALLELE IGHV1-69*06)</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1970" name="Biochemistry" volume="9" first="3161" last="3170">
      <title>The covalent structure of a human gamma G-immunoglobulin. VII. Amino acid sequence of heavy-chain cyanogen bromide fragments H1-H4.</title>
      <authorList>
        <person name="Cunningham B.A."/>
        <person name="Rutishauser U."/>
        <person name="Gall W.E."/>
        <person name="Gottlieb P.D."/>
        <person name="Waxdal M.J."/>
        <person name="Edelman G.M."/>
      </authorList>
      <dbReference type="PubMed" id="5489771"/>
      <dbReference type="DOI" id="10.1021/bi00818a008"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 20-117</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-20</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1981" name="Biochemistry" volume="20" first="5822" last="5830">
      <title>Amino acid sequence of the variable regions of heavy chains from two idiotypically cross-reactive human IgM anti-gamma-globulins of the Wa group.</title>
      <authorList>
        <person name="Andrews D.W."/>
        <person name="Capra J.D."/>
      </authorList>
      <dbReference type="PubMed" id="7028111"/>
      <dbReference type="DOI" id="10.1021/bi00523a027"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 20-117</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-20</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1970" name="Biochemistry" volume="9" first="3188" last="3196">
      <title>The covalent structure of a human gamma G-immunoglobulin. X. Intrachain disulfide bonds.</title>
      <authorList>
        <person name="Gall W.E."/>
        <person name="Edelman G.M."/>
      </authorList>
      <dbReference type="PubMed" id="4923144"/>
      <dbReference type="DOI" id="10.1021/bi00818a011"/>
    </citation>
    <scope>DISULFIDE BOND</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2001" name="Exp. Clin. Immunogenet." volume="18" first="100" last="116">
      <title>Nomenclature of the human immunoglobulin heavy (IGH) genes.</title>
      <authorList>
        <person name="Lefranc M.P."/>
      </authorList>
      <dbReference type="PubMed" id="11340299"/>
      <dbReference type="DOI" id="10.1159/000049189"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="6">
    <citation type="book" date="2001" name="The Immunoglobulin FactsBook." first="1" last="458" publisher="Academic Press" city="London.">
      <title>The Immunoglobulin FactsBook.</title>
      <editorList>
        <person name="Lefranc M.P."/>
        <person name="Lefranc G."/>
      </editorList>
      <authorList>
        <person name="Lefranc M.P."/>
        <person name="Lefranc G."/>
      </authorList>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2007" name="Annu. Rev. Genet." volume="41" first="107" last="120">
      <title>Immunoglobulin somatic hypermutation.</title>
      <authorList>
        <person name="Teng G."/>
        <person name="Papavasiliou F.N."/>
      </authorList>
      <dbReference type="PubMed" id="17576170"/>
      <dbReference type="DOI" id="10.1146/annurev.genet.41.110306.130340"/>
    </citation>
    <scope>REVIEW ON SOMATIC HYPERMUTATION</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2010" name="J. Allergy Clin. Immunol." volume="125" first="S41" last="S52">
      <title>Structure and function of immunoglobulins.</title>
      <authorList>
        <person name="Schroeder H.W. Jr."/>
        <person name="Cavacini L."/>
      </authorList>
      <dbReference type="PubMed" id="20176268"/>
      <dbReference type="DOI" id="10.1016/j.jaci.2009.09.046"/>
    </citation>
    <scope>REVIEW ON IMMUNOGLOBULINS</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2012" name="Nat. Rev. Immunol." volume="12" first="24" last="34">
      <title>Molecular programming of B cell memory.</title>
      <authorList>
        <person name="McHeyzer-Williams M."/>
        <person name="Okitsu S."/>
        <person name="Wang N."/>
        <person name="McHeyzer-Williams L."/>
      </authorList>
      <dbReference type="PubMed" id="22158414"/>
      <dbReference type="DOI" id="10.1038/nri3128"/>
    </citation>
    <scope>REVIEW ON FUNCTION</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2014" name="Front. Immunol." volume="5" first="22" last="22">
      <title>Immunoglobulin and T Cell Receptor Genes: IMGT((R)) and the Birth and Rise of Immunoinformatics.</title>
      <authorList>
        <person name="Lefranc M.P."/>
      </authorList>
      <dbReference type="PubMed" id="24600447"/>
      <dbReference type="DOI" id="10.3389/fimmu.2014.00022"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="7 8 9 10">V region of the variable domain of immunoglobulin heavy chains that participates in the antigen recognition (PubMed:24600447). Immunoglobulins, also known as antibodies, are membrane-bound or secreted glycoproteins produced by B lymphocytes. In the recognition phase of humoral immunity, the membrane-bound immunoglobulins serve as receptors which, upon binding of a specific antigen, trigger the clonal expansion and differentiation of B lymphocytes into immunoglobulins-secreting plasma cells. Secreted immunoglobulins mediate the effector phase of humoral immunity, which results in the elimination of bound antigens (PubMed:20176268, PubMed:22158414). The antigen binding site is formed by the variable domain of one heavy chain, together with that of its associated light chain. Thus, each immunoglobulin has two antigen binding sites with remarkable affinity for a particular antigen. The variable domains are assembled by a process called V-(D)-J rearrangement and can then be subjected to somatic hypermutations which, after exposure to antigen and selection, allow affinity maturation for a particular antigen (PubMed:17576170, PubMed:20176268).</text>
  </comment>
  <comment type="subunit">
    <text evidence="8">Immunoglobulins are composed of two identical heavy chains and two identical light chains; disulfide-linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8 9">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="8 9">Cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="polymorphism">
    <text evidence="12">There are several alleles. The sequence shown is that of IMGT allele IGHV1-69*06.</text>
  </comment>
  <comment type="caution">
    <text evidence="12">For examples of full-length immunoglobulin heavy chains (of different isotypes) see AC P0DOX2, AC P0DOX3, AC P0DOX4, AC P0DOX5 and AC P0DOX6.</text>
  </comment>
  <dbReference type="EMBL" id="AC245369">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A02043">
    <property type="entry name" value="M1HUWL"/>
  </dbReference>
  <dbReference type="PIR" id="A02044">
    <property type="entry name" value="M1HUSI"/>
  </dbReference>
  <dbReference type="PIR" id="A90563">
    <property type="entry name" value="G1HUEU"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01742"/>
  <dbReference type="EMDB" id="EMD-10733"/>
  <dbReference type="EMDB" id="EMD-34259"/>
  <dbReference type="SMR" id="P01742"/>
  <dbReference type="IntAct" id="P01742">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="IMGT_GENE-DB" id="IGHV1-69"/>
  <dbReference type="iPTMnet" id="P01742"/>
  <dbReference type="PhosphoSitePlus" id="P01742"/>
  <dbReference type="BioMuta" id="IGHV1-69"/>
  <dbReference type="jPOST" id="P01742"/>
  <dbReference type="MassIVE" id="P01742"/>
  <dbReference type="PeptideAtlas" id="P01742"/>
  <dbReference type="Ensembl" id="ENST00000390633.2">
    <property type="protein sequence ID" value="ENSP00000375042.2"/>
    <property type="gene ID" value="ENSG00000211973.2"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000632882.1">
    <property type="protein sequence ID" value="ENSP00000488090.1"/>
    <property type="gene ID" value="ENSG00000282350.1"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:5558"/>
  <dbReference type="GeneCards" id="IGHV1-69"/>
  <dbReference type="HGNC" id="HGNC:5558">
    <property type="gene designation" value="IGHV1-69"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000211973">
    <property type="expression patterns" value="Group enriched (gallbladder, intestine, urinary bladder)"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P01742"/>
  <dbReference type="OpenTargets" id="ENSG00000211973"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000211973"/>
  <dbReference type="GeneTree" id="ENSGT00950000183013"/>
  <dbReference type="InParanoid" id="P01742"/>
  <dbReference type="OMA" id="STYAISW"/>
  <dbReference type="PhylomeDB" id="P01742"/>
  <dbReference type="PathwayCommons" id="P01742"/>
  <dbReference type="Reactome" id="R-HSA-166663">
    <property type="pathway name" value="Initial triggering of complement"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-173623">
    <property type="pathway name" value="Classical antibody-mediated complement activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-198933">
    <property type="pathway name" value="Immunoregulatory interactions between a Lymphoid and a non-Lymphoid cell"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-202733">
    <property type="pathway name" value="Cell surface interactions at the vascular wall"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2029481">
    <property type="pathway name" value="FCGR activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2029482">
    <property type="pathway name" value="Regulation of actin dynamics for phagocytic cup formation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2029485">
    <property type="pathway name" value="Role of phospholipids in phagocytosis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2168880">
    <property type="pathway name" value="Scavenging of heme from plasma"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2454202">
    <property type="pathway name" value="Fc epsilon receptor (FCERI) signaling"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2730905">
    <property type="pathway name" value="Role of LAT2/NTAL/LAB on calcium mobilization"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2871796">
    <property type="pathway name" value="FCERI mediated MAPK activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2871809">
    <property type="pathway name" value="FCERI mediated Ca+2 mobilization"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2871837">
    <property type="pathway name" value="FCERI mediated NF-kB activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-5690714">
    <property type="pathway name" value="CD22 mediated BCR regulation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9664323">
    <property type="pathway name" value="FCGR3A-mediated IL10 synthesis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9664422">
    <property type="pathway name" value="FCGR3A-mediated phagocytosis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9679191">
    <property type="pathway name" value="Potential therapeutics for SARS"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-977606">
    <property type="pathway name" value="Regulation of Complement cascade"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-983695">
    <property type="pathway name" value="Antigen activates B Cell Receptor (BCR) leading to generation of second messengers"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="IGHV1-69">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="Pharos" id="P01742">
    <property type="development level" value="Tdark"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P01742"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 14"/>
  </dbReference>
  <dbReference type="RNAct" id="P01742">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000211973">
    <property type="expression patterns" value="Expressed in vermiform appendix and 82 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019814">
    <property type="term" value="C:immunoglobulin complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003823">
    <property type="term" value="F:antigen binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006955">
    <property type="term" value="P:immune response"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016064">
    <property type="term" value="P:immunoglobulin mediated immune response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.40.10:FF:000556">
    <property type="entry name" value="Immunoglobulin heavy variable 7-81 (non-functional)"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013106">
    <property type="entry name" value="Ig_V-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050199">
    <property type="entry name" value="IgHV"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23266">
    <property type="entry name" value="IMMUNOGLOBULIN HEAVY CHAIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23266:SF262">
    <property type="entry name" value="IMMUNOGLOBULIN HEAVY VARIABLE 1-69-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07686">
    <property type="entry name" value="V-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00406">
    <property type="entry name" value="IGv"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-1280">Immunoglobulin</keyword>
  <keyword id="KW-0393">Immunoglobulin domain</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="4 5">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000059902" description="Immunoglobulin heavy variable 1-69" evidence="4 5">
    <location>
      <begin position="20"/>
      <end position="117"/>
    </location>
  </feature>
  <feature type="domain" description="Ig-like" evidence="2">
    <location>
      <begin position="20"/>
      <end position="117" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-1" evidence="1">
    <location>
      <begin position="20"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-1" evidence="1">
    <location>
      <begin position="45"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-2" evidence="1">
    <location>
      <begin position="53"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-2" evidence="1">
    <location>
      <begin position="70"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-3" evidence="1">
    <location>
      <begin position="78"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-3" evidence="1">
    <location>
      <begin position="116"/>
      <end position="117" status="greater than"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="4 5">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2 3">
    <location>
      <begin position="41"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>V</original>
    <variation>M</variation>
    <location>
      <position position="24"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>K</original>
    <variation>R</variation>
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>S</original>
    <variation>T</variation>
    <location>
      <position position="40"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="43"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>SS</original>
    <variation>VD</variation>
    <location>
      <begin position="49"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="12" ref="2">
    <original>SY</original>
    <variation>RS</variation>
    <location>
      <begin position="50"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>S</original>
    <variation>G</variation>
    <location>
      <position position="50"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>AIS</original>
    <variation>KGL</variation>
    <location>
      <begin position="52"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="52"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="12" ref="2">
    <original>S</original>
    <variation>I</variation>
    <location>
      <position position="54"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>Q</original>
    <variation>K</variation>
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>Q</original>
    <variation>R</variation>
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>M</original>
    <variation>V</variation>
    <location>
      <position position="67"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>GIIPIFGT</original>
    <variation>SP</variation>
    <location>
      <begin position="69"/>
      <end position="76"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>G</original>
    <variation>Q</variation>
    <location>
      <position position="69"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="12" ref="2">
    <original>IPIFGTA</original>
    <variation>VPMFGPP</variation>
    <location>
      <begin position="71"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>IPI</original>
    <variation>PLR</variation>
    <location>
      <begin position="71"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>GTANYAQKFQG</original>
    <variation>NGEVKNPGSVV</variation>
    <location>
      <begin position="75"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>NYAQK</original>
    <variation>KWTDP</variation>
    <location>
      <begin position="78"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>G</original>
    <variation>GVYIKWE</variation>
    <location>
      <position position="85"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>T</original>
    <variation>S</variation>
    <location>
      <position position="88"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>ITADK</original>
    <variation>VSLKP</variation>
    <location>
      <begin position="89"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="12" ref="2">
    <original>KSTS</original>
    <variation>ESTN</variation>
    <location>
      <begin position="93"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>TST</original>
    <variation>FNQ</variation>
    <location>
      <begin position="95"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>Y</original>
    <variation>H</variation>
    <location>
      <position position="99"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>SS</original>
    <variation>VN</variation>
    <location>
      <begin position="103"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>R</original>
    <variation>F</variation>
    <location>
      <position position="106"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>S</original>
    <variation>N</variation>
    <location>
      <position position="107"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 3; AA sequence." evidence="12" ref="3">
    <original>T</original>
    <variation>G</variation>
    <location>
      <position position="110"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="12" ref="2">
    <original>VYYCAR</original>
    <variation>FYFCAG</variation>
    <location>
      <begin position="112"/>
      <end position="117"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="117"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P23083"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00114"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="4923144"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="5489771"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="7028111"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="11340299"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="17576170"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="20176268"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="22158414"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="10">
    <source>
      <dbReference type="PubMed" id="24600447"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="11">
    <source ref="6"/>
  </evidence>
  <evidence type="ECO:0000305" key="12"/>
  <evidence type="ECO:0000305" key="13">
    <source>
      <dbReference type="PubMed" id="5489771"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="14">
    <source>
      <dbReference type="PubMed" id="7028111"/>
    </source>
  </evidence>
  <sequence length="117" mass="12659" checksum="89671FD4910590DD" modified="2016-10-05" version="2" precursor="true">MDWTWRFLFVVAAATGVQSQVQLVQSGAEVKKPGSSVKVSCKASGGTFSSYAISWVRQAPGQGLEWMGGIIPIFGTANYAQKFQGRVTITADKSTSTAYMELSSLRSEDTAVYYCAR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>