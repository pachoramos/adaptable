<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-04-22" modified="2024-07-24" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>A0A059J5P2</accession>
  <name>DEFTR_TRIIM</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Fungal defensin triintsin</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="ORF">H109_04975</name>
  </gene>
  <organism>
    <name type="scientific">Trichophyton interdigitale (strain MR816)</name>
    <dbReference type="NCBI Taxonomy" id="1215338"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Ascomycota</taxon>
      <taxon>Pezizomycotina</taxon>
      <taxon>Eurotiomycetes</taxon>
      <taxon>Eurotiomycetidae</taxon>
      <taxon>Onygenales</taxon>
      <taxon>Arthrodermataceae</taxon>
      <taxon>Trichophyton</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2018" name="Genetics" volume="208" first="1657" last="1669">
      <title>Whole-genome analysis illustrates global clonal population structure of the ubiquitous dermatophyte pathogen Trichophyton rubrum.</title>
      <authorList>
        <person name="Persinoti G.F."/>
        <person name="Martinez D.A."/>
        <person name="Li W."/>
        <person name="Doegen A."/>
        <person name="Billmyre R.B."/>
        <person name="Averette A."/>
        <person name="Goldberg J.M."/>
        <person name="Shea T."/>
        <person name="Young S."/>
        <person name="Zeng Q."/>
        <person name="Oliver B.G."/>
        <person name="Barton R."/>
        <person name="Metin B."/>
        <person name="Hilmioglu-Polat S."/>
        <person name="Ilkit M."/>
        <person name="Graeser Y."/>
        <person name="Martinez-Rossi N.M."/>
        <person name="White T.C."/>
        <person name="Heitman J."/>
        <person name="Cuomo C.A."/>
      </authorList>
      <dbReference type="PubMed" id="29467168"/>
      <dbReference type="DOI" id="10.1534/genetics.117.300573"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MR816</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2018" name="Peptides" volume="107" first="61" last="67">
      <title>Triintsin, a human pathogenic fungus-derived defensin with broad-spectrum antimicrobial activity.</title>
      <authorList>
        <person name="Shen B."/>
        <person name="Song J."/>
        <person name="Zhao Y."/>
        <person name="Zhang Y."/>
        <person name="Liu G."/>
        <person name="Li X."/>
        <person name="Guo X."/>
        <person name="Li W."/>
        <person name="Cao Z."/>
        <person name="Wu Y."/>
      </authorList>
      <dbReference type="PubMed" id="30102941"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2018.08.003"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 48-85</scope>
    <scope>3D-STRUCTURE MODELING</scope>
    <source>
      <strain>H6</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Antimicrobial peptide with broad-spectrum activity against Gram-positive bacteria, Gram-negative bacteria, and fungi. Also inhibits clinical isolates, including methicillin-resistant S.aureus (MRSA) (MIC=32 uM), K.pneumoniae, C.albicans and C.parapsilosis. Displays minimal inhibitory concentration (MIC) values similar to minimal bactericidal concentrations (MBC), suggesting a disruptive mechanism mode of action associated with membrane lysis. In vitro, shows hemolytic activity against human red blood cells.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="PTM">
    <text evidence="4">Disulfide bonds are essential for antimicrobial activity.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the invertebrate defensin family.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=03010"/>
  </comment>
  <dbReference type="EMBL" id="AOKY01000319">
    <property type="protein sequence ID" value="KDB23160.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A059J5P2"/>
  <dbReference type="SMR" id="A0A059J5P2"/>
  <dbReference type="HOGENOM" id="CLU_116797_0_0_1"/>
  <dbReference type="OMA" id="CPLNERE"/>
  <dbReference type="Proteomes" id="UP000024533">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001542">
    <property type="entry name" value="Defensin_invertebrate/fungal"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01097">
    <property type="entry name" value="Defensin_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51378">
    <property type="entry name" value="INVERT_DEFENSINS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000449381" evidence="7">
    <location>
      <begin position="22"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5001579330" description="Fungal defensin triintsin">
    <location>
      <begin position="48"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 7">
    <location>
      <begin position="51"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 7">
    <location>
      <begin position="58"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 7">
    <location>
      <begin position="62"/>
      <end position="82"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="I1T3C7"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="29467168"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="30102941"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="30102941"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="30102941"/>
    </source>
  </evidence>
  <sequence length="85" mass="8882" checksum="06D1665F8D68B19A" modified="2014-07-09" version="1" precursor="true">MQFTKLATVLIVSLMGSAAIAAPSVDNAPAVAAEEVAAAPAENLEKRGFGCPLNERECHSHCQSIGRKFGYCGGTLRLTCICGKE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>