<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-12-20" modified="2024-10-02" version="83" xmlns="http://uniprot.org/uniprot">
  <accession>Q30KK5</accession>
  <accession>A4H234</accession>
  <name>DB123_PANTR</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 123</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 123</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB123</name>
  </gene>
  <organism>
    <name type="scientific">Pan troglodytes</name>
    <name type="common">Chimpanzee</name>
    <dbReference type="NCBI Taxonomy" id="9598"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Pan</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Physiol. Genomics" volume="23" first="5" last="17">
      <title>Cross-species analysis of the mammalian beta-defensin gene family: presence of syntenic gene clusters and preferential expression in the male reproductive tract.</title>
      <authorList>
        <person name="Patil A.A."/>
        <person name="Cai Y."/>
        <person name="Sang Y."/>
        <person name="Blecha F."/>
        <person name="Zhang G."/>
      </authorList>
      <dbReference type="PubMed" id="16033865"/>
      <dbReference type="DOI" id="10.1152/physiolgenomics.00104.2005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="2006-11" db="EMBL/GenBank/DDBJ databases">
      <title>Evolution and sequence variation of human beta-defensin genes.</title>
      <authorList>
        <person name="Hollox E.J."/>
        <person name="Armour J.A.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="DQ012075">
    <property type="protein sequence ID" value="AAY59805.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AM410139">
    <property type="protein sequence ID" value="CAL68954.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001123240.1">
    <property type="nucleotide sequence ID" value="NM_001129768.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q30KK5"/>
  <dbReference type="SMR" id="Q30KK5"/>
  <dbReference type="STRING" id="9598.ENSPTRP00000022892"/>
  <dbReference type="PaxDb" id="9598-ENSPTRP00000022892"/>
  <dbReference type="Ensembl" id="ENSPTRT00000024802.2">
    <property type="protein sequence ID" value="ENSPTRP00000022892.1"/>
    <property type="gene ID" value="ENSPTRG00000013352.2"/>
  </dbReference>
  <dbReference type="GeneID" id="742057"/>
  <dbReference type="KEGG" id="ptr:742057"/>
  <dbReference type="CTD" id="245936"/>
  <dbReference type="VGNC" id="VGNC:6091">
    <property type="gene designation" value="DEFB123"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502TIGY">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000162385"/>
  <dbReference type="HOGENOM" id="CLU_181906_2_0_1"/>
  <dbReference type="InParanoid" id="Q30KK5"/>
  <dbReference type="OMA" id="RCWNLHG"/>
  <dbReference type="OrthoDB" id="4693858at2759"/>
  <dbReference type="TreeFam" id="TF336381"/>
  <dbReference type="Proteomes" id="UP000002277">
    <property type="component" value="Chromosome 20"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSPTRG00000013352">
    <property type="expression patterns" value="Expressed in testis"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050544">
    <property type="entry name" value="Beta-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025933">
    <property type="entry name" value="Beta_defensin_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001:SF3">
    <property type="entry name" value="BETA-DEFENSIN 123"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001">
    <property type="entry name" value="BETA-DEFENSIN 123-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13841">
    <property type="entry name" value="Defensin_beta_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000045351" description="Beta-defensin 123">
    <location>
      <begin position="21"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="25"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="36"/>
      <end position="53"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="67" mass="8105" checksum="94EBFE98C9086D72" modified="2005-12-06" version="1" precursor="true">MKLLLLTLTVLLLLSQLTPGGTQRCWNLYGKCRYRCSKKERVYVYCINNKMCCVKPKYQPKERWWPF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>