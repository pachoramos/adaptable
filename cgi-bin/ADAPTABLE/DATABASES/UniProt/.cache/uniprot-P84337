<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-06-07" modified="2024-05-29" version="8" xmlns="http://uniprot.org/uniprot">
  <accession>P84337</accession>
  <name>HTRSN_PHYTS</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Homotarsinin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Phyllomedusa tarsius</name>
    <name type="common">Brownbelly leaf frog</name>
    <name type="synonym">Phyllomedusa tarsia</name>
    <dbReference type="NCBI Taxonomy" id="306084"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Phyllomedusa</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="submission" date="2004-12" db="UniProtKB">
      <title>Homotarsinin: A novel antimicrobial homodimer peptide from skin secretion of Phyllomedusa tarsius (Amphibia).</title>
      <authorList>
        <person name="Prates M.V."/>
        <person name="Santos N.C.F."/>
        <person name="Leite J.R.S.A."/>
        <person name="Figueredo R.C.R."/>
        <person name="Martins G.R."/>
        <person name="Bloch C. Jr."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT ARG-24</scope>
    <scope>DISULFIDE BOND</scope>
    <source>
      <tissue evidence="3">Skin secretion</tissue>
    </source>
  </reference>
  <reference evidence="4" key="2">
    <citation type="journal article" date="2017" name="Sci. Rep." volume="7" first="40854" last="40854">
      <title>Structure and membrane interactions of the homodimeric antibiotic peptide homotarsinin.</title>
      <authorList>
        <person name="Verly R.M."/>
        <person name="Resende J.M."/>
        <person name="Junior E.F."/>
        <person name="de Magalhaes M.T."/>
        <person name="Guimaraes C.F."/>
        <person name="Munhoz V.H."/>
        <person name="Bemquerer M.P."/>
        <person name="Almeida F.C."/>
        <person name="Santoro M.M."/>
        <person name="Pilo-Veloso D."/>
        <person name="Bechinger B."/>
      </authorList>
      <dbReference type="PubMed" id="28102305"/>
      <dbReference type="DOI" id="10.1038/srep40854"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 1-24</scope>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>MUTAGENESIS OF CYS-23</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2">Antimicrobial peptide (PubMed:28102305, Ref.1). Active against Gram-negative bacteria E.coli ATCC 25922 (MIC=1.5 uM) and P.aeruginosa ATTC 27853 (MIC=23.2 uM) and against Gram-positive bacterium S.aureus ATCC 29313 (MIC=11.6 uM) (PubMed:28102305, Ref.1). Has no hemolytic activity (Ref.1). Associates with and disrupts membranes in vitro (PubMed:28102305).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1 2">Homodimer; disulfide-linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="5503.85" error="0.5" method="MALDI" evidence="2">
    <text>Homodimer.</text>
  </comment>
  <dbReference type="PDB" id="6WUX">
    <property type="method" value="NMR"/>
    <property type="chains" value="A/B=1-24"/>
  </dbReference>
  <dbReference type="PDB" id="7MN3">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-24"/>
  </dbReference>
  <dbReference type="PDBsum" id="6WUX"/>
  <dbReference type="PDBsum" id="7MN3"/>
  <dbReference type="AlphaFoldDB" id="P84337"/>
  <dbReference type="SMR" id="P84337"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000440097" description="Homotarsinin" evidence="2">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="modified residue" description="Arginine amide" evidence="2">
    <location>
      <position position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" description="Interchain" evidence="1 2">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of dimerization." evidence="1">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="helix" evidence="6">
    <location>
      <begin position="4"/>
      <end position="20"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="28102305"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="6WUX"/>
    </source>
  </evidence>
  <sequence length="24" mass="2754" checksum="1AB1B4207AC9E567" modified="2017-06-07" version="1">NLVSDIIGSKKHMEKLISIIKKCR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>