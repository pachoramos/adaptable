<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-07-19" modified="2022-05-25" version="30" xmlns="http://uniprot.org/uniprot">
  <accession>P84011</accession>
  <name>TX90E_PHONI</name>
  <protein>
    <recommendedName>
      <fullName>U11-ctenitoxin-Pn1b</fullName>
      <shortName>U11-CNTX-Pn1b</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Venom protein PNTx22C3</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Phoneutria nigriventer</name>
    <name type="common">Brazilian armed spider</name>
    <name type="synonym">Ctenus nigriventer</name>
    <dbReference type="NCBI Taxonomy" id="6918"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Lycosoidea</taxon>
      <taxon>Ctenidae</taxon>
      <taxon>Phoneutria</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2006" name="Comp. Biochem. Physiol." volume="142" first="173" last="187">
      <title>Comparison of the partial proteomes of the venoms of Brazilian spiders of the genus Phoneutria.</title>
      <authorList>
        <person name="Richardson M."/>
        <person name="Pimenta A.M."/>
        <person name="Bemquerer M.P."/>
        <person name="Santoro M.M."/>
        <person name="Beirao P.S."/>
        <person name="Lima M.E."/>
        <person name="Figueiredo S.G."/>
        <person name="Bloch C. Jr."/>
        <person name="Vasconcelos E.A."/>
        <person name="Campos F.A."/>
        <person name="Gomes P.C."/>
        <person name="Cordeiro M.N."/>
      </authorList>
      <dbReference type="PubMed" id="16278100"/>
      <dbReference type="DOI" id="10.1016/j.cbpc.2005.09.010"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue evidence="1">Venom</tissue>
    </source>
  </reference>
  <reference evidence="3" key="2">
    <citation type="submission" date="2004-06" db="UniProtKB">
      <title>Protein PNTx22C3 from venom of Brazilian armed spider Phoneutria nigriventer has sequence similarities with insecticidal toxins from other spiders.</title>
      <authorList>
        <person name="Richardson M."/>
        <person name="Pimenta A.M.C."/>
        <person name="Bemquerer M.P."/>
        <person name="Santoro M.M."/>
        <person name="Figueiredo S.G."/>
        <person name="Cordeiro M.N."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>VARIANT HIS-42</scope>
    <source>
      <tissue evidence="1">Venom</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Non-toxic to mice.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="3">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="6568.4" method="Electrospray" evidence="1 2"/>
  <comment type="similarity">
    <text evidence="3">Belongs to the neurotoxin 09 (Tx3-6) family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84011"/>
  <dbReference type="SMR" id="P84011"/>
  <dbReference type="ArachnoServer" id="AS000161">
    <property type="toxin name" value="U11-ctenitoxin-Pn1b"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000087635" description="U11-ctenitoxin-Pn1b">
    <location>
      <begin position="1"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="2"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="9"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="15"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="24"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="48"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="sequence variant" evidence="1 2">
    <original>Y</original>
    <variation>H</variation>
    <location>
      <position position="42"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="16278100"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="58" mass="6610" checksum="5A396EB5FBA6A36C" modified="2010-01-19" version="2">ACLARGETCKDDCECCDCDNQCYCPFDWFGGKWHPVGCSCAYTNKYVCDHKKEKCKKA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>