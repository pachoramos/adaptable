<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2023-11-08" version="94" xmlns="http://uniprot.org/uniprot">
  <accession>P55897</accession>
  <name>H2A_BUFGR</name>
  <protein>
    <recommendedName>
      <fullName>Histone H2A</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Buforin-1</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Buforin I</fullName>
      </alternativeName>
    </component>
    <component>
      <recommendedName>
        <fullName>Buforin-2</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Buforin II</fullName>
      </alternativeName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Bufo gargarizans</name>
    <name type="common">Asian toad</name>
    <name type="synonym">Bufo bufo gargarizans</name>
    <dbReference type="NCBI Taxonomy" id="30331"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Bufonidae</taxon>
      <taxon>Bufo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Biochem. Biophys. Res. Commun." volume="218" first="408" last="413">
      <title>A novel antimicrobial peptide from Bufo bufo gargarizans.</title>
      <authorList>
        <person name="Park C.B."/>
        <person name="Kim M.S."/>
        <person name="Kim S.C."/>
      </authorList>
      <dbReference type="PubMed" id="8573171"/>
      <dbReference type="DOI" id="10.1006/bbrc.1996.0071"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Stomach</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1996" name="FEBS Lett." volume="398" first="87" last="90">
      <title>Solution structure of an antimicrobial peptide buforin II.</title>
      <authorList>
        <person name="Yi G.-S."/>
        <person name="Park C.B."/>
        <person name="Kim S.C."/>
        <person name="Cheong C."/>
      </authorList>
      <dbReference type="PubMed" id="8946958"/>
      <dbReference type="DOI" id="10.1016/s0014-5793(96)01193-3"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 16-36</scope>
  </reference>
  <comment type="function">
    <text>Core component of nucleosome. Nucleosomes wrap and compact DNA into chromatin, limiting DNA accessibility to the cellular machineries which require DNA as a template. Histones thereby play a central role in transcription regulation, DNA repair, DNA replication and chromosomal stability. DNA accessibility is regulated via a complex set of post-translational modifications of histones, also called histone code, and nucleosome remodeling.</text>
  </comment>
  <comment type="function">
    <text>Buforins are strong antimicrobial activities in vitro against a broad-spectrum of microorganisms including fungi. Buforin II is more potent than buforin I.</text>
  </comment>
  <comment type="subunit">
    <text>The nucleosome is a histone octamer containing two molecules each of H2A, H2B, H3 and H4 assembled in one H3-H4 heterotetramer and two H2A-H2B heterodimers. The octamer wraps approximately 147 bp of DNA.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location>Chromosome</location>
    </subcellularLocation>
  </comment>
  <comment type="PTM">
    <text evidence="1">Monoubiquitination of C-terminus gives a specific tag for epigenetic transcriptional repression. Following DNA double-strand breaks (DSBs), it is ubiquitinated through 'Lys-63' linkage of ubiquitin moieties (By similarity).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the histone H2A family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P55897"/>
  <dbReference type="SMR" id="P55897"/>
  <dbReference type="GO" id="GO:0000786">
    <property type="term" value="C:nucleosome"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046982">
    <property type="term" value="F:protein heterodimerization activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.20.10">
    <property type="entry name" value="Histone, subunit A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009072">
    <property type="entry name" value="Histone-fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR032458">
    <property type="entry name" value="Histone_H2A_CS"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47113">
    <property type="entry name" value="Histone-fold"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00046">
    <property type="entry name" value="HISTONE_H2A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0158">Chromosome</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0379">Hydroxylation</keyword>
  <keyword id="KW-1017">Isopeptide bond</keyword>
  <keyword id="KW-0544">Nucleosome core</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-0832">Ubl conjugation</keyword>
  <feature type="peptide" id="PRO_0000013167" description="Buforin-1">
    <location>
      <begin position="1"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000013168" description="Buforin-2">
    <location>
      <begin position="16"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="4">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic residues" evidence="4">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-(2-hydroxyisobutyryl)lysine" evidence="3">
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="1">
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-(2-hydroxyisobutyryl)lysine; alternate" evidence="3">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-lactoyllysine; alternate" evidence="2">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-succinyllysine" evidence="3">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-(2-hydroxyisobutyryl)lysine; alternate" evidence="3">
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="cross-link" description="Glycyl lysine isopeptide (Lys-Gly) (interchain with G-Cter in ubiquitin)" evidence="1">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <feature type="cross-link" description="Glycyl lysine isopeptide (Lys-Gly) (interchain with G-Cter in ubiquitin)" evidence="1">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="39"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P0C0S5"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P0C0S8"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="4">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="39" mass="4263" checksum="E9F88A21839219AA" modified="1997-11-01" version="1" fragment="single">AGRGKQGGKVRAKAKTRSSRAGLQFPVGRVHRLLRKGNY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>