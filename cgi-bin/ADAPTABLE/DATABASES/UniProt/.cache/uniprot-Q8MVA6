<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-03-02" modified="2023-06-28" version="29" xmlns="http://uniprot.org/uniprot">
  <accession>Q8MVA6</accession>
  <name>ISAMP_IXOSC</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Antimicrobial peptide ISAMP</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ixodes scapularis</name>
    <name type="common">Black-legged tick</name>
    <name type="synonym">Deer tick</name>
    <dbReference type="NCBI Taxonomy" id="6945"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Acari</taxon>
      <taxon>Parasitiformes</taxon>
      <taxon>Ixodida</taxon>
      <taxon>Ixodoidea</taxon>
      <taxon>Ixodidae</taxon>
      <taxon>Ixodinae</taxon>
      <taxon>Ixodes</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2002" name="J. Exp. Biol." volume="205" first="2843" last="2864">
      <title>Exploring the sialome of the tick Ixodes scapularis.</title>
      <authorList>
        <person name="Valenzuela J.G."/>
        <person name="Francischetti I.M."/>
        <person name="Pham V.M."/>
        <person name="Garfield M.K."/>
        <person name="Mather T.N."/>
        <person name="Ribeiro J.M."/>
      </authorList>
      <dbReference type="PubMed" id="12177149"/>
      <dbReference type="DOI" id="10.1242/jeb.205.18.2843"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <strain evidence="5">Rhode Island</strain>
      <tissue evidence="5">Salivary gland</tissue>
    </source>
  </reference>
  <reference evidence="4" key="2">
    <citation type="journal article" date="2009" name="Biochem. Biophys. Res. Commun." volume="390" first="511" last="515">
      <title>Purification and characterization of a novel salivary antimicrobial peptide from the tick, Ixodes scapularis.</title>
      <authorList>
        <person name="Pichu S."/>
        <person name="Ribeiro J.M."/>
        <person name="Mather T.N."/>
      </authorList>
      <dbReference type="PubMed" id="19852941"/>
      <dbReference type="DOI" id="10.1016/j.bbrc.2009.09.127"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 24-42</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <strain evidence="2">Rhode Island</strain>
      <tissue evidence="2">Saliva</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Has antimicrobial activity against B.cereus (MIC=5.8 ug/ml), B.subtilis (MIC=12.3 ug/ml), S.aureus (MIC=10.4 ug/ml), E.coli Edl 933 (MIC=3.2 ug/ml) and E.coli MG/655 (MIC=4.2 ug/ml). Non-hemolytic.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1 2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed in the fat body, hemocytes and salivary glands of partially-fed female ticks. Not expressed in the midgut.</text>
  </comment>
  <comment type="mass spectrometry" mass="5300.5" method="MALDI" evidence="2"/>
  <dbReference type="EMBL" id="AF483734">
    <property type="protein sequence ID" value="AAM93656.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8MVA6"/>
  <dbReference type="VEuPathDB" id="VectorBase:ISCI008031"/>
  <dbReference type="VEuPathDB" id="VectorBase:ISCP_025164"/>
  <dbReference type="VEuPathDB" id="VectorBase:ISCW008031"/>
  <dbReference type="InParanoid" id="Q8MVA6"/>
  <dbReference type="Proteomes" id="UP000001555">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000391699" description="Antimicrobial peptide ISAMP" evidence="2">
    <location>
      <begin position="24"/>
      <end position="69"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="12177149"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19852941"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="19852941"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000312" key="5">
    <source>
      <dbReference type="EMBL" id="AAM93656.1"/>
    </source>
  </evidence>
  <sequence length="69" mass="7817" checksum="EBB93FDBFA754E24" modified="2002-10-01" version="1" precursor="true">MRAVAIFIVTLLVLECVYFVMSEPDPGQPWQVKAGRPPCYSIPCRKHDECRVGSCSRCNNGLWGDRTCR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>