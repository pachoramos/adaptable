<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-06-01" modified="2021-04-07" version="44" xmlns="http://uniprot.org/uniprot">
  <accession>P35904</accession>
  <name>ACH1_LISFU</name>
  <protein>
    <recommendedName>
      <fullName>Achatin-1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Achatin-I</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Lissachatina fulica</name>
    <name type="common">Giant African land snail</name>
    <name type="synonym">Achatina fulica</name>
    <dbReference type="NCBI Taxonomy" id="2315439"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Spiralia</taxon>
      <taxon>Lophotrochozoa</taxon>
      <taxon>Mollusca</taxon>
      <taxon>Gastropoda</taxon>
      <taxon>Heterobranchia</taxon>
      <taxon>Euthyneura</taxon>
      <taxon>Panpulmonata</taxon>
      <taxon>Eupulmonata</taxon>
      <taxon>Stylommatophora</taxon>
      <taxon>Helicina</taxon>
      <taxon>Achatinoidea</taxon>
      <taxon>Achatinidae</taxon>
      <taxon>Lissachatina</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1989" name="Biochem. Biophys. Res. Commun." volume="160" first="1015" last="1020">
      <title>Achatin-I, an endogenous neuroexcitatory tetrapeptide from Achatina fulica Ferussac containing a D-amino acid residue.</title>
      <authorList>
        <person name="Kamatani Y."/>
        <person name="Minakata H."/>
        <person name="Kenny P.T.M."/>
        <person name="Iwashita T."/>
        <person name="Watanabe K."/>
        <person name="Funase K."/>
        <person name="Sun X.P."/>
        <person name="Yongsiri A."/>
        <person name="Kim K.H."/>
        <person name="Novales-Li P."/>
        <person name="Novales E.T."/>
        <person name="Kanapi C.G."/>
        <person name="Takeuchi H."/>
        <person name="Nomoto K."/>
      </authorList>
      <dbReference type="PubMed" id="2597281"/>
      <dbReference type="DOI" id="10.1016/s0006-291x(89)80103-2"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>D-AMINO ACID AT PHE-2</scope>
    <scope>CHARACTERIZATION</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <strain>Ferussac</strain>
      <tissue>Ganglion</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1991" name="Biochem. Biophys. Res. Commun." volume="177" first="847" last="853">
      <title>Purification of achatin-I from the atria of the African giant snail, Achatina fulica, and its possible function.</title>
      <authorList>
        <person name="Fujimoto K."/>
        <person name="Kubota I."/>
        <person name="Yasuda-Kamatani Y."/>
        <person name="Minakata H."/>
        <person name="Nomoto K."/>
        <person name="Yoshida M."/>
        <person name="Harada A."/>
        <person name="Muneoka Y."/>
        <person name="Kobayashi M."/>
      </authorList>
      <dbReference type="PubMed" id="1675568"/>
      <dbReference type="DOI" id="10.1016/0006-291x(91)91867-c"/>
    </citation>
    <scope>CHARACTERIZATION</scope>
    <source>
      <strain>Ferussac</strain>
      <tissue>Heart atrium</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1992" name="Int. J. Pept. Protein Res." volume="39" first="258" last="264">
      <title>Crystal structure and molecular conformation of achatin-I (H-Gly-D-Phe-Ala-Asp-OH), an endogenous neuropeptide containing a D-amino acid residue.</title>
      <authorList>
        <person name="Ishida T."/>
        <person name="In Y."/>
        <person name="Doi M."/>
        <person name="Inoue M."/>
        <person name="Yasuda-Kamatani Y."/>
        <person name="Minakata H."/>
        <person name="Iwashita T."/>
        <person name="Nomoto K."/>
      </authorList>
      <dbReference type="PubMed" id="1399265"/>
      <dbReference type="DOI" id="10.1111/j.1399-3011.1992.tb00798.x"/>
    </citation>
    <scope>CRYSTALLIZATION</scope>
  </reference>
  <comment type="function">
    <text>Neuroexcitatory peptide; increases the impulse frequency and produces a spike broadening of the identified heart excitatory neuron (PON); also enhances the amplitude and frequency of the heart beat. Has also an effect on several other muscles.</text>
  </comment>
  <dbReference type="PIR" id="A32480">
    <property type="entry name" value="A32480"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0208">D-amino acid</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <feature type="peptide" id="PRO_0000044101" description="Achatin-1">
    <location>
      <begin position="1"/>
      <end position="4"/>
    </location>
  </feature>
  <feature type="modified residue" description="D-phenylalanine" evidence="1">
    <location>
      <position position="2"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2597281"/>
    </source>
  </evidence>
  <sequence length="4" mass="408" checksum="6AADD9C810000000" modified="1994-06-01" version="1">GFAD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>