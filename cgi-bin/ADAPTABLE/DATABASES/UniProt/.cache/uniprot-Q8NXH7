<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2002-10-10" modified="2024-11-27" version="115" xmlns="http://uniprot.org/uniprot">
  <accession>Q8NXH7</accession>
  <name>GCSH_STAAW</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Glycine cleavage system H protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Octanoyl/lipoyl carrier protein</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">gcvH</name>
    <name type="ordered locus">MW0786</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">The glycine cleavage system catalyzes the degradation of glycine. The H protein shuttles the methylamine group of glycine from the P protein to the T protein.</text>
  </comment>
  <comment type="function">
    <text evidence="1">Is also involved in protein lipoylation via its role as an octanoyl/lipoyl carrier protein intermediate.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="1">
      <name>(R)-lipoate</name>
      <dbReference type="ChEBI" id="CHEBI:83088"/>
    </cofactor>
    <text evidence="1">Binds 1 lipoyl cofactor covalently.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">The glycine cleavage system is composed of four proteins: P, T, L and H.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the GcvH family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB94651.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000290489.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q8NXH7"/>
  <dbReference type="SMR" id="Q8NXH7"/>
  <dbReference type="KEGG" id="sam:MW0786"/>
  <dbReference type="HOGENOM" id="CLU_097408_2_0_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005960">
    <property type="term" value="C:glycine cleavage complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019464">
    <property type="term" value="P:glycine decarboxylation via glycine cleavage system"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009249">
    <property type="term" value="P:protein lipoylation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="CDD" id="cd06848">
    <property type="entry name" value="GCS_H"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.100">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00272">
    <property type="entry name" value="GcvH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003016">
    <property type="entry name" value="2-oxoA_DH_lipoyl-BS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000089">
    <property type="entry name" value="Biotin_lipoyl"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002930">
    <property type="entry name" value="GCV_H"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033753">
    <property type="entry name" value="GCV_H/Fam206"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR017453">
    <property type="entry name" value="GCV_H_sub"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011053">
    <property type="entry name" value="Single_hybrid_motif"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00527">
    <property type="entry name" value="gcvH"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11715">
    <property type="entry name" value="GLYCINE CLEAVAGE SYSTEM H PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11715:SF41">
    <property type="entry name" value="GLYCINE CLEAVAGE SYSTEM H PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01597">
    <property type="entry name" value="GCV_H"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF51230">
    <property type="entry name" value="Single hybrid motif"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50968">
    <property type="entry name" value="BIOTINYL_LIPOYL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00189">
    <property type="entry name" value="LIPOYL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0450">Lipoyl</keyword>
  <feature type="chain" id="PRO_0000166251" description="Glycine cleavage system H protein">
    <location>
      <begin position="1"/>
      <end position="126"/>
    </location>
  </feature>
  <feature type="domain" description="Lipoyl-binding" evidence="2">
    <location>
      <begin position="22"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-lipoyllysine" evidence="1">
    <location>
      <position position="63"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_00272"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01066"/>
    </source>
  </evidence>
  <sequence length="126" mass="14081" checksum="CA90394A8529E4CB" modified="2002-10-10" version="1">MAVPNELKYSKEHEWVKVEGNVATIGITEYAQSELGDIVFVELPETDDEINEGDTFGSVESVKTVSELYAPISGKVVEVNEELEDSPEFVNESPYEKAWMVKVEISDESQIEALLTAEKYSEMIGE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>