<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-10-03" modified="2021-09-29" version="17" xmlns="http://uniprot.org/uniprot">
  <accession>P84954</accession>
  <name>HPS1_PITAZ</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Hyposin-H1</fullName>
      <shortName evidence="3">HPS-H1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Hyposin-1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="2">Hyposin-HA1</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Pithecopus azureus</name>
    <name type="common">Orange-legged monkey tree frog</name>
    <name type="synonym">Phyllomedusa azurea</name>
    <dbReference type="NCBI Taxonomy" id="2034991"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Pithecopus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="journal article" date="2007" name="J. Proteome Res." volume="6" first="3604" last="3613">
      <title>Amphibian skin secretomics: application of parallel quadrupole time-of-flight mass spectrometry and peptide precursor cDNA cloning to rapidly characterize the skin secretory peptidome of Phyllomedusa hypochondrialis azurea: discovery of a novel peptide family, the hyposins.</title>
      <authorList>
        <person name="Thompson A.H."/>
        <person name="Bjourson A.J."/>
        <person name="Orr D.F."/>
        <person name="Shaw C."/>
        <person name="McClean S."/>
      </authorList>
      <dbReference type="PubMed" id="17696382"/>
      <dbReference type="DOI" id="10.1021/pr0702666"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LYS-11</scope>
    <source>
      <tissue evidence="1">Skin secretion</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="Peptides" volume="29" first="2074" last="2082">
      <title>A consistent nomenclature of antimicrobial peptides isolated from frogs of the subfamily Phyllomedusinae.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="18644413"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.06.017"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1232.89" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Hyposin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=00902"/>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000250429" description="Hyposin-H1" evidence="1">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="modified residue" description="Lysine amide" evidence="1">
    <location>
      <position position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="17696382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="17696382"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="11" mass="1235" checksum="D36FB75B740042CD" modified="2006-10-03" version="1">LRPAVIRPKGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>