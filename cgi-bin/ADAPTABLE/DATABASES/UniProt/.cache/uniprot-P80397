<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2022-10-12" version="59" xmlns="http://uniprot.org/uniprot">
  <accession>P80397</accession>
  <name>GGN3_GLARU</name>
  <protein>
    <recommendedName>
      <fullName>Gaegurin-3</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">GGN3</name>
  </gene>
  <organism>
    <name type="scientific">Glandirana rugosa</name>
    <name type="common">Japanese wrinkled frog</name>
    <name type="synonym">Rana rugosa</name>
    <dbReference type="NCBI Taxonomy" id="8410"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Glandirana</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Biochem. Biophys. Res. Commun." volume="205" first="948" last="954">
      <title>Antimicrobial peptides from the skin of a Korean frog, Rana rugosa.</title>
      <authorList>
        <person name="Park J.M."/>
        <person name="Jung J.-E."/>
        <person name="Lee B.J."/>
      </authorList>
      <dbReference type="PubMed" id="7999137"/>
      <dbReference type="DOI" id="10.1006/bbrc.1994.2757"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Has a non-hemolytic activity. Has a broad spectrum of activity against both Gram-positive and Gram-negative bacteria, fungi and protozoa.</text>
  </comment>
  <comment type="subunit">
    <text>Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="PIR" id="PC2302">
    <property type="entry name" value="PC2302"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P80397"/>
  <dbReference type="SMR" id="P80397"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044654" description="Gaegurin-3">
    <location>
      <begin position="1"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="27"/>
      <end position="33"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="33" mass="3309" checksum="C1EA65E2489E5475" modified="1995-11-01" version="1">GIMSIVKDVAKTAAKEAAKGALSTLSCKLAKTC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>