<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-02-06" modified="2022-05-25" version="13" xmlns="http://uniprot.org/uniprot">
  <accession>B3EWW0</accession>
  <name>TXS4C_CUPSA</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Short cationic peptide-4c</fullName>
      <shortName evidence="3">SCP-4c</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">Short cationic peptide-4e</fullName>
      <shortName evidence="2">SCP-4e</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="3">Truncated variant of Cupiennin 4 family</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Cupiennius salei</name>
    <name type="common">American wandering spider</name>
    <dbReference type="NCBI Taxonomy" id="6928"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Lycosoidea</taxon>
      <taxon>Ctenidae</taxon>
      <taxon>Cupiennius</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2012" name="FEBS J." volume="279" first="2683" last="2694">
      <title>Multicomponent venom of the spider Cupiennius salei: a bioanalytical investigation applying different strategies.</title>
      <authorList>
        <person name="Trachsel C."/>
        <person name="Siegemund D."/>
        <person name="Kampfer U."/>
        <person name="Kopp L.S."/>
        <person name="Buhr C."/>
        <person name="Grossmann J."/>
        <person name="Luthi C."/>
        <person name="Cunningham M."/>
        <person name="Nentwig W."/>
        <person name="Kuhn-Nentwig L."/>
        <person name="Schurch S."/>
        <person name="Schaller J."/>
      </authorList>
      <dbReference type="PubMed" id="22672445"/>
      <dbReference type="DOI" id="10.1111/j.1742-4658.2012.08650.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT GLU-20</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="unpublished observations" date="2015-06">
      <authorList>
        <person name="Kuhn-Nentwig L."/>
        <person name="Gohel T."/>
      </authorList>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the venom gland.</text>
  </comment>
  <comment type="mass spectrometry" mass="2281.346" method="Electrospray" evidence="1"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the cationic peptide 04 (cupiennin) family. 04 subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="B3EWW0"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035164">
    <property type="entry name" value="Cupiennin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17563">
    <property type="entry name" value="Cu"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="peptide" id="PRO_0000421217" description="Short cationic peptide-4c" evidence="1">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="modified residue" description="Glutamic acid 1-amide" evidence="1">
    <location>
      <position position="20"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="22672445"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="22672445"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="22672445"/>
    </source>
  </evidence>
  <sequence length="20" mass="2284" checksum="65863E9F9E0A8517" modified="2013-02-06" version="1">FLAKKVAKKLVSHVAQKQME</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>