<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-04-01" modified="2024-07-24" version="44" xmlns="http://uniprot.org/uniprot">
  <accession>P30240</accession>
  <name>NS3A_IBVU5</name>
  <protein>
    <recommendedName>
      <fullName>Non-structural protein 3a</fullName>
      <shortName>ns3a</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Accessory protein 3a</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="ORF">3a</name>
  </gene>
  <organism>
    <name type="scientific">Avian infectious bronchitis virus (strain UK/183/66)</name>
    <name type="common">IBV</name>
    <dbReference type="NCBI Taxonomy" id="31629"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Riboviria</taxon>
      <taxon>Orthornavirae</taxon>
      <taxon>Pisuviricota</taxon>
      <taxon>Pisoniviricetes</taxon>
      <taxon>Nidovirales</taxon>
      <taxon>Cornidovirineae</taxon>
      <taxon>Coronaviridae</taxon>
      <taxon>Orthocoronavirinae</taxon>
      <taxon>Gammacoronavirus</taxon>
      <taxon>Igacovirus</taxon>
      <taxon>Avian coronavirus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Gallus gallus</name>
    <name type="common">Chicken</name>
    <dbReference type="NCBI Taxonomy" id="9031"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1991" name="Virology" volume="184" first="531" last="544">
      <title>A polycistronic mRNA specified by the coronavirus infectious bronchitis virus.</title>
      <authorList>
        <person name="Liu D.X."/>
        <person name="Cavanagh D."/>
        <person name="Green P."/>
        <person name="Inglis S.C."/>
      </authorList>
      <dbReference type="PubMed" id="1653486"/>
      <dbReference type="DOI" id="10.1016/0042-6822(91)90423-9"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC RNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Involved in resistance to IFN.</text>
  </comment>
  <dbReference type="EMBL" id="X59820">
    <property type="protein sequence ID" value="CAA42490.1"/>
    <property type="molecule type" value="Genomic_RNA"/>
  </dbReference>
  <dbReference type="PIR" id="D36816">
    <property type="entry name" value="WMIH24"/>
  </dbReference>
  <dbReference type="SMR" id="P30240"/>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005214">
    <property type="entry name" value="IBV_3A"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03617">
    <property type="entry name" value="IBV_3A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000106112" description="Non-structural protein 3a">
    <location>
      <begin position="24"/>
      <end position="57"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P30237"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <sequence length="57" mass="6622" checksum="25C167708473D0CA" modified="1993-04-01" version="1" precursor="true">MIQSPTSFLIVLILLWCKLVISCFRECIVALQQLIQVLLQIINSNLQSRLLLWHSLD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>