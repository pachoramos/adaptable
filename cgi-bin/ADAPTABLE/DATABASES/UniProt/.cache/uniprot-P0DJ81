<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-11-16" modified="2024-11-27" version="39" xmlns="http://uniprot.org/uniprot">
  <accession>P0DJ81</accession>
  <accession>A0A023WAC9</accession>
  <name>VKT24_CYRSC</name>
  <protein>
    <recommendedName>
      <fullName>Kunitz-type U15-theraphotoxin-Hs1e</fullName>
      <shortName>U15-TRTX-Hs1e</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="6">Huwentoxin HW11c24</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="5">Kunitz-type serine protease inhibitor HWTX-XI-IS24</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Cyriopagopus schmidti</name>
    <name type="common">Chinese bird spider</name>
    <name type="synonym">Haplopelma schmidti</name>
    <dbReference type="NCBI Taxonomy" id="29017"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Mygalomorphae</taxon>
      <taxon>Theraphosidae</taxon>
      <taxon>Cyriopagopus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="PLoS ONE" volume="3" first="E3414" last="E3414">
      <title>Discovery of a distinct superfamily of Kunitz-type toxin (KTT) from tarantulas.</title>
      <authorList>
        <person name="Yuan C.-H."/>
        <person name="He Q.-Y."/>
        <person name="Peng K."/>
        <person name="Diao J.-B."/>
        <person name="Jiang L.-P."/>
        <person name="Tang X."/>
        <person name="Liang S.-P."/>
      </authorList>
      <dbReference type="PubMed" id="18923708"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0003414"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference evidence="9" key="2">
    <citation type="journal article" date="2014" name="Peptides" volume="54" first="9" last="18">
      <title>Molecular cloning, bioinformatics analysis and functional characterization of HWTX-XI toxin superfamily from the spider Ornithoctonus huwena.</title>
      <authorList>
        <person name="Jiang L."/>
        <person name="Deng M."/>
        <person name="Duan Z."/>
        <person name="Tang X."/>
        <person name="Liang S."/>
      </authorList>
      <dbReference type="PubMed" id="24418069"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2014.01.001"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
    <scope>RECOMBINANT EXPRESSION</scope>
  </reference>
  <comment type="function">
    <text evidence="4">Toxin with inhibitory activity on serine proteases, but not on potassium channels (PubMed:24418069). The recombinant toxin is active against trypsin (Ki=9.61 nM), kallikrein (Ki=24.8 nM), and chymotrypsin (PubMed:24418069).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="8">Expressed by the venom gland.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="4">Negative results: the recombinant toxin does not show activity on Kv1.1/KCNA1, Kv1.2/KCNA2, Kv1.3/KCNA3, Kv2.1/KCNB1 and Kv4.3/KCND3 channels, as well as on calcium (Cav) and sodium channels (Nav). It also does not show detectable activity on thrombin.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the venom Kunitz-type family. 03 (sub-Kunitz) subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="KF160303">
    <property type="protein sequence ID" value="AHY30314.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0DJ81"/>
  <dbReference type="SMR" id="P0DJ81"/>
  <dbReference type="ArachnoServer" id="AS001841">
    <property type="toxin name" value="U15-theraphotoxin-Hs1e"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004867">
    <property type="term" value="F:serine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044562">
    <property type="term" value="P:envenomation resulting in negative regulation of voltage-gated potassium channel activity in another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd22598">
    <property type="entry name" value="Kunitz_huwentoxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="4.10.410.10:FF:000020">
    <property type="entry name" value="Collagen, type VI, alpha 3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="4.10.410.10">
    <property type="entry name" value="Pancreatic trypsin inhibitor Kunitz domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002223">
    <property type="entry name" value="Kunitz_BPTI"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036880">
    <property type="entry name" value="Kunitz_BPTI_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR051388">
    <property type="entry name" value="Serpin_venom_toxin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46751">
    <property type="entry name" value="EPPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46751:SF1">
    <property type="entry name" value="WAP FOUR-DISULFIDE CORE DOMAIN PROTEIN 6A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00014">
    <property type="entry name" value="Kunitz_BPTI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00759">
    <property type="entry name" value="BASICPTASE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00131">
    <property type="entry name" value="KU"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57362">
    <property type="entry name" value="BPTI-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50279">
    <property type="entry name" value="BPTI_KUNITZ_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0722">Serine protease inhibitor</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-1220">Voltage-gated potassium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000413836" evidence="1">
    <location>
      <begin position="28"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000413837" description="Kunitz-type U15-theraphotoxin-Hs1e">
    <location>
      <begin position="34"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="domain" description="BPTI/Kunitz inhibitor" evidence="3">
    <location>
      <begin position="37"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="site" description="May bind Kv1" evidence="1">
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="site" description="Reactive bond for chymotrypsin" evidence="1">
    <location>
      <begin position="47"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="37"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="60"/>
      <end position="81"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00031"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="24418069"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="18923708"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="24418069"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="18923708"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="9">
    <source>
      <dbReference type="EMBL" id="AHY30314.1"/>
    </source>
  </evidence>
  <sequence length="88" mass="9832" checksum="52E710227DF7CF06" modified="2011-11-16" version="1" precursor="true">MGTARFLSAVLLLSVLLMVTFPALLSAEYHDGRVDICSLPSDSGDRLRFFEMWYFDGTTCTKFVYGGYGGNDNRFPTEKACMKRCAKA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>