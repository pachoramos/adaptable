<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1997-11-01" modified="2024-11-27" version="161" xmlns="http://uniprot.org/uniprot">
  <accession>Q42449</accession>
  <name>PRF1_ARATH</name>
  <protein>
    <recommendedName>
      <fullName evidence="11">Profilin-1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="13">AtPROF1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="11">AthPRF1</fullName>
    </alternativeName>
    <allergenName evidence="13">Ara t 8</allergenName>
  </protein>
  <gene>
    <name evidence="11" type="primary">PRF1</name>
    <name evidence="12" type="synonym">PFN1</name>
    <name evidence="13" type="synonym">PRO1</name>
    <name evidence="14" type="ordered locus">At2g19760</name>
    <name evidence="15" type="ORF">F6F22.21</name>
  </gene>
  <organism>
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1996" name="Plant J." volume="10" first="269" last="279">
      <title>Arabidopsis profilins are functionally similar to yeast profilins: identification of a vascular bundle-specific profilin and a pollen-specific profilin.</title>
      <authorList>
        <person name="Christensen H.E.M."/>
        <person name="Ramachandran S."/>
        <person name="Tan C.T."/>
        <person name="Surana U."/>
        <person name="Dong C.H."/>
        <person name="Chua N.-H."/>
      </authorList>
      <dbReference type="PubMed" id="8771785"/>
      <dbReference type="DOI" id="10.1046/j.1365-313x.1996.10020269.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1996" name="Plant Physiol." volume="111" first="115" last="126">
      <title>The Arabidopsis profilin gene family. Evidence for an ancient split between constitutive and pollen-specific profilin genes.</title>
      <authorList>
        <person name="Huang S."/>
        <person name="McDowell J.M."/>
        <person name="Weise M.J."/>
        <person name="Meagher R.B."/>
      </authorList>
      <dbReference type="PubMed" id="8685262"/>
      <dbReference type="DOI" id="10.1104/pp.111.1.115"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1999" name="Nature" volume="402" first="761" last="768">
      <title>Sequence and analysis of chromosome 2 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Lin X."/>
        <person name="Kaul S."/>
        <person name="Rounsley S.D."/>
        <person name="Shea T.P."/>
        <person name="Benito M.-I."/>
        <person name="Town C.D."/>
        <person name="Fujii C.Y."/>
        <person name="Mason T.M."/>
        <person name="Bowman C.L."/>
        <person name="Barnstead M.E."/>
        <person name="Feldblyum T.V."/>
        <person name="Buell C.R."/>
        <person name="Ketchum K.A."/>
        <person name="Lee J.J."/>
        <person name="Ronning C.M."/>
        <person name="Koo H.L."/>
        <person name="Moffat K.S."/>
        <person name="Cronin L.A."/>
        <person name="Shen M."/>
        <person name="Pai G."/>
        <person name="Van Aken S."/>
        <person name="Umayam L."/>
        <person name="Tallon L.J."/>
        <person name="Gill J.E."/>
        <person name="Adams M.D."/>
        <person name="Carrera A.J."/>
        <person name="Creasy T.H."/>
        <person name="Goodman H.M."/>
        <person name="Somerville C.R."/>
        <person name="Copenhaver G.P."/>
        <person name="Preuss D."/>
        <person name="Nierman W.C."/>
        <person name="White O."/>
        <person name="Eisen J.A."/>
        <person name="Salzberg S.L."/>
        <person name="Fraser C.M."/>
        <person name="Venter J.C."/>
      </authorList>
      <dbReference type="PubMed" id="10617197"/>
      <dbReference type="DOI" id="10.1038/45471"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2003" name="Science" volume="302" first="842" last="846">
      <title>Empirical analysis of transcriptional activity in the Arabidopsis genome.</title>
      <authorList>
        <person name="Yamada K."/>
        <person name="Lim J."/>
        <person name="Dale J.M."/>
        <person name="Chen H."/>
        <person name="Shinn P."/>
        <person name="Palm C.J."/>
        <person name="Southwick A.M."/>
        <person name="Wu H.C."/>
        <person name="Kim C.J."/>
        <person name="Nguyen M."/>
        <person name="Pham P.K."/>
        <person name="Cheuk R.F."/>
        <person name="Karlin-Newmann G."/>
        <person name="Liu S.X."/>
        <person name="Lam B."/>
        <person name="Sakano H."/>
        <person name="Wu T."/>
        <person name="Yu G."/>
        <person name="Miranda M."/>
        <person name="Quach H.L."/>
        <person name="Tripp M."/>
        <person name="Chang C.H."/>
        <person name="Lee J.M."/>
        <person name="Toriumi M.J."/>
        <person name="Chan M.M."/>
        <person name="Tang C.C."/>
        <person name="Onodera C.S."/>
        <person name="Deng J.M."/>
        <person name="Akiyama K."/>
        <person name="Ansari Y."/>
        <person name="Arakawa T."/>
        <person name="Banh J."/>
        <person name="Banno F."/>
        <person name="Bowser L."/>
        <person name="Brooks S.Y."/>
        <person name="Carninci P."/>
        <person name="Chao Q."/>
        <person name="Choy N."/>
        <person name="Enju A."/>
        <person name="Goldsmith A.D."/>
        <person name="Gurjal M."/>
        <person name="Hansen N.F."/>
        <person name="Hayashizaki Y."/>
        <person name="Johnson-Hopson C."/>
        <person name="Hsuan V.W."/>
        <person name="Iida K."/>
        <person name="Karnes M."/>
        <person name="Khan S."/>
        <person name="Koesema E."/>
        <person name="Ishida J."/>
        <person name="Jiang P.X."/>
        <person name="Jones T."/>
        <person name="Kawai J."/>
        <person name="Kamiya A."/>
        <person name="Meyers C."/>
        <person name="Nakajima M."/>
        <person name="Narusaka M."/>
        <person name="Seki M."/>
        <person name="Sakurai T."/>
        <person name="Satou M."/>
        <person name="Tamse R."/>
        <person name="Vaysberg M."/>
        <person name="Wallender E.K."/>
        <person name="Wong C."/>
        <person name="Yamamura Y."/>
        <person name="Yuan S."/>
        <person name="Shinozaki K."/>
        <person name="Davis R.W."/>
        <person name="Theologis A."/>
        <person name="Ecker J.R."/>
      </authorList>
      <dbReference type="PubMed" id="14593172"/>
      <dbReference type="DOI" id="10.1126/science.1088305"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2001" name="Plant Cell" volume="13" first="1179" last="1191">
      <title>Small changes in the regulation of one Arabidopsis profilin isovariant, PRF1, alter seedling development.</title>
      <authorList>
        <person name="McKinney E.C."/>
        <person name="Kandasamy M.K."/>
        <person name="Meagher R.B."/>
      </authorList>
      <dbReference type="PubMed" id="11340190"/>
      <dbReference type="DOI" id="10.1105/tpc.13.5.1179"/>
    </citation>
    <scope>INDUCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2007" name="Plant Cell" volume="19" first="3111" last="3126">
      <title>Class-specific interaction of profilin and ADF isovariants with actin in the regulation of plant development.</title>
      <authorList>
        <person name="Kandasamy M.K."/>
        <person name="Burgos-Rivera B."/>
        <person name="McKinney E.C."/>
        <person name="Ruzicka D.R."/>
        <person name="Meagher R.B."/>
      </authorList>
      <dbReference type="PubMed" id="17933902"/>
      <dbReference type="DOI" id="10.1105/tpc.107.052621"/>
    </citation>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2009" name="J. Integr. Plant Biol." volume="51" first="113" last="121">
      <title>Arabidopsis profilin isoforms, PRF1 and PRF2 show distinctive binding activities and subcellular distributions.</title>
      <authorList>
        <person name="Wang F."/>
        <person name="Jing Y."/>
        <person name="Wang Z."/>
        <person name="Mao T."/>
        <person name="Samaj J."/>
        <person name="Yuan M."/>
        <person name="Ren H."/>
      </authorList>
      <dbReference type="PubMed" id="19200149"/>
      <dbReference type="DOI" id="10.1111/j.1744-7909.2008.00781.x"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2015" name="BMC Plant Biol." volume="15" first="177" last="177">
      <title>Arabidopsis plants deficient in constitutive class profilins reveal independent and quantitative genetic effects.</title>
      <authorList>
        <person name="Muessar K.J."/>
        <person name="Kandasamy M.K."/>
        <person name="McKinney E.C."/>
        <person name="Meagher R.B."/>
      </authorList>
      <dbReference type="PubMed" id="26160044"/>
      <dbReference type="DOI" id="10.1186/s12870-015-0551-0"/>
    </citation>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2016" name="Plant Cell Physiol." volume="57" first="46" last="56">
      <title>Distinct biochemical properties of Arabidopsis thaliana actin isoforms.</title>
      <authorList>
        <person name="Kijima S.T."/>
        <person name="Hirose K."/>
        <person name="Kong S.G."/>
        <person name="Wada M."/>
        <person name="Uyeda T.Q."/>
      </authorList>
      <dbReference type="PubMed" id="26578694"/>
      <dbReference type="DOI" id="10.1093/pcp/pcv176"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2016" name="Plant Physiol." volume="170" first="220" last="233">
      <title>Profilin-dependent nucleation and assembly of actin filaments controls cell elongation in Arabidopsis.</title>
      <authorList>
        <person name="Cao L."/>
        <person name="Henty-Ridilla J.L."/>
        <person name="Blanchoin L."/>
        <person name="Staiger C.J."/>
      </authorList>
      <dbReference type="PubMed" id="26574597"/>
      <dbReference type="DOI" id="10.1104/pp.15.01321"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2018" name="Biochim. Biophys. Acta" volume="1862" first="2545" last="2554">
      <title>Molecular mechanism of Arabidopsis thaliana profilins as antifungal proteins.</title>
      <authorList>
        <person name="Park S.C."/>
        <person name="Kim I.R."/>
        <person name="Kim J.Y."/>
        <person name="Lee Y."/>
        <person name="Kim E.J."/>
        <person name="Jung J.H."/>
        <person name="Jung Y.J."/>
        <person name="Jang M.K."/>
        <person name="Lee J.R."/>
      </authorList>
      <dbReference type="PubMed" id="30056100"/>
      <dbReference type="DOI" id="10.1016/j.bbagen.2018.07.028"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2018" name="Curr. Biol." volume="28" first="1882" last="1895">
      <title>Profilin negatively regulates formin-mediated actin assembly to modulate PAMP-triggered plant immunity.</title>
      <authorList>
        <person name="Sun H."/>
        <person name="Qiao Z."/>
        <person name="Chua K.P."/>
        <person name="Tursic A."/>
        <person name="Liu X."/>
        <person name="Gao Y.G."/>
        <person name="Mu Y."/>
        <person name="Hou X."/>
        <person name="Miao Y."/>
      </authorList>
      <dbReference type="PubMed" id="29861135"/>
      <dbReference type="DOI" id="10.1016/j.cub.2018.04.045"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="14">
    <citation type="journal article" date="1997" name="Structure" volume="5" first="19" last="32">
      <title>The crystal structure of a major allergen from plants.</title>
      <authorList>
        <person name="Thorn K.S."/>
        <person name="Christensen H.E.M."/>
        <person name="Shigeta R. Jr."/>
        <person name="Hudler D. Jr."/>
        <person name="Shalaby L."/>
        <person name="Lindnerg U."/>
        <person name="Chua N.-H."/>
        <person name="Schutt C.E."/>
      </authorList>
      <dbReference type="PubMed" id="9016723"/>
      <dbReference type="DOI" id="10.1016/s0969-2126(97)00163-9"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.60 ANGSTROMS)</scope>
  </reference>
  <comment type="function">
    <text evidence="3 5 6 7 8">Binds to actin monomers and regulates the organization of the actin cytoskeleton (PubMed:26574597). At high concentrations, profilin prevents the polymerization of actin, whereas it enhances it at low concentrations (PubMed:29861135). At low concentrations, associates with the poly-proline motif of formins to enhance actin filament elongation rate (PubMed:29861135). Binds ACT1, ACT7 and ACT11 and inhibits actin polymerization (PubMed:26578694). Coordinates the stochastic dynamic properties of actin filaments by modulating formin-mediated actin nucleation and assembly during axial cell expansion (PubMed:26574597). Binds G-actin and poly-L-proline in vitro (PubMed:19200149). Inhibits cell growth of various pathogenic fungal strains (PubMed:30056100). May play a role as antifungal proteins in the defense system against fungal pathogen attacks (PubMed:30056100).</text>
  </comment>
  <comment type="subunit">
    <text evidence="13">Occurs in many kinds of cells as a complex with monomeric actin in a 1:1 ratio.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="13">Cytoplasm</location>
      <location evidence="13">Cytoskeleton</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2 3">Cytoplasm</location>
    </subcellularLocation>
    <text evidence="3">Probably associated with cytoplasmic actin filaments.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3 8 9 10">Expressed at low levels roots, leaves, stems, flowers and siliques (PubMed:8685262, PubMed:8771785). Expressed in leaf epidermal cells, trichomes and stem epidermal cells (PubMed:19200149). Detected in phloem exudates (at protein level) (PubMed:30056100).</text>
  </comment>
  <comment type="induction">
    <text evidence="1">Down-regulated by light.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="1 4 5">Delayed germination (PubMed:11340190). In young seedlings, excessive numbers of root hairs, abnormal raised cotyledons, elongated hypocotyls, and elongated cells in the hypocotyl (PubMed:11340190, PubMed:26574597). Defects in rosette leaf and inflorescence development (PubMed:26160044).</text>
  </comment>
  <comment type="allergen">
    <text evidence="13">Causes an allergic reaction in human.</text>
  </comment>
  <comment type="similarity">
    <text evidence="13">Belongs to the profilin family.</text>
  </comment>
  <dbReference type="EMBL" id="U43325">
    <property type="protein sequence ID" value="AAB39480.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U43322">
    <property type="protein sequence ID" value="AAB39476.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U43593">
    <property type="protein sequence ID" value="AAG10090.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="U43590">
    <property type="protein sequence ID" value="AAB46750.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AC005169">
    <property type="protein sequence ID" value="AAC62140.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002685">
    <property type="protein sequence ID" value="AEC06923.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY072427">
    <property type="protein sequence ID" value="AAL62419.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BT000264">
    <property type="protein sequence ID" value="AAN15583.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="G84580">
    <property type="entry name" value="G84580"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_179566.1">
    <property type="nucleotide sequence ID" value="NM_127534.3"/>
  </dbReference>
  <dbReference type="PDB" id="1A0K">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.20 A"/>
    <property type="chains" value="A=1-131"/>
  </dbReference>
  <dbReference type="PDB" id="3NUL">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.60 A"/>
    <property type="chains" value="A=2-131"/>
  </dbReference>
  <dbReference type="PDBsum" id="1A0K"/>
  <dbReference type="PDBsum" id="3NUL"/>
  <dbReference type="AlphaFoldDB" id="Q42449"/>
  <dbReference type="SMR" id="Q42449"/>
  <dbReference type="BioGRID" id="1850">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="IntAct" id="Q42449">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="3702.Q42449"/>
  <dbReference type="PaxDb" id="3702-AT2G19760.1"/>
  <dbReference type="ProteomicsDB" id="226504"/>
  <dbReference type="EnsemblPlants" id="AT2G19760.1">
    <property type="protein sequence ID" value="AT2G19760.1"/>
    <property type="gene ID" value="AT2G19760"/>
  </dbReference>
  <dbReference type="GeneID" id="816495"/>
  <dbReference type="Gramene" id="AT2G19760.1">
    <property type="protein sequence ID" value="AT2G19760.1"/>
    <property type="gene ID" value="AT2G19760"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT2G19760"/>
  <dbReference type="Araport" id="AT2G19760"/>
  <dbReference type="TAIR" id="AT2G19760">
    <property type="gene designation" value="PRF1"/>
  </dbReference>
  <dbReference type="eggNOG" id="KOG1755">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_120772_0_1_1"/>
  <dbReference type="InParanoid" id="Q42449"/>
  <dbReference type="OMA" id="MVIQGEQ"/>
  <dbReference type="OrthoDB" id="2965090at2759"/>
  <dbReference type="PhylomeDB" id="Q42449"/>
  <dbReference type="EvolutionaryTrace" id="Q42449"/>
  <dbReference type="PRO" id="PR:Q42449"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="Q42449">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005730">
    <property type="term" value="C:nucleolus"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009524">
    <property type="term" value="C:phragmoplast"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009506">
    <property type="term" value="C:plasmodesma"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005819">
    <property type="term" value="C:spindle"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003779">
    <property type="term" value="F:actin binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008154">
    <property type="term" value="P:actin polymerization or depolymerization"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010229">
    <property type="term" value="P:inflorescence development"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048527">
    <property type="term" value="P:lateral root development"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048366">
    <property type="term" value="P:leaf development"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009826">
    <property type="term" value="P:unidimensional cell growth"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="TAIR"/>
  </dbReference>
  <dbReference type="CDD" id="cd00148">
    <property type="entry name" value="PROF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.450.30:FF:000001">
    <property type="entry name" value="Profilin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.450.30">
    <property type="entry name" value="Dynein light chain 2a, cytoplasmic"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR048278">
    <property type="entry name" value="PFN"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005455">
    <property type="entry name" value="PFN_euk"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036140">
    <property type="entry name" value="PFN_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027310">
    <property type="entry name" value="Profilin_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11604">
    <property type="entry name" value="PROFILIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11604:SF45">
    <property type="entry name" value="PROFILIN-1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00235">
    <property type="entry name" value="Profilin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00392">
    <property type="entry name" value="PROFILIN"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01640">
    <property type="entry name" value="PROFILINPLNT"/>
  </dbReference>
  <dbReference type="SMART" id="SM00392">
    <property type="entry name" value="PROF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55770">
    <property type="entry name" value="Profilin (actin-binding protein)"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00414">
    <property type="entry name" value="PROFILIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0009">Actin-binding</keyword>
  <keyword id="KW-0020">Allergen</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0206">Cytoskeleton</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000199615" description="Profilin-1">
    <location>
      <begin position="1"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="helix" evidence="16">
    <location>
      <begin position="3"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="21"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="32"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="helix" evidence="16">
    <location>
      <begin position="44"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="turn" evidence="16">
    <location>
      <begin position="61"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="65"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="70"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="turn" evidence="16">
    <location>
      <begin position="79"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="82"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="90"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="strand" evidence="16">
    <location>
      <begin position="98"/>
      <end position="106"/>
    </location>
  </feature>
  <feature type="helix" evidence="16">
    <location>
      <begin position="112"/>
      <end position="128"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="11340190"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="17933902"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="19200149"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="26160044"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="26574597"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="26578694"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="29861135"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="30056100"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="8685262"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="8771785"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="11">
    <source>
      <dbReference type="PubMed" id="8685262"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="8771785"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13"/>
  <evidence type="ECO:0000312" key="14">
    <source>
      <dbReference type="Araport" id="AT2G19760"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="15">
    <source>
      <dbReference type="EMBL" id="AAC62140.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="16">
    <source>
      <dbReference type="PDB" id="3NUL"/>
    </source>
  </evidence>
  <sequence length="131" mass="14266" checksum="84212D4681F83F51" modified="1997-11-01" version="1">MSWQSYVDDHLMCDVEGNHLTAAAILGQDGSVWAQSAKFPQLKPQEIDGIKKDFEEPGFLAPTGLFLGGEKYMVIQGEQGAVIRGKKGPGGVTIKKTNQALVFGFYDEPMTGGQCNLVVERLGDYLIESEL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>