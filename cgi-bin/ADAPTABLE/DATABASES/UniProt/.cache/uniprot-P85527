<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-06-10" modified="2024-10-02" version="52" xmlns="http://uniprot.org/uniprot">
  <accession>P85527</accession>
  <name>NEMS_APIME</name>
  <protein>
    <recommendedName>
      <fullName>Myosuppressin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Apis mellifera</name>
    <name type="common">Honeybee</name>
    <dbReference type="NCBI Taxonomy" id="7460"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Apoidea</taxon>
      <taxon>Anthophila</taxon>
      <taxon>Apidae</taxon>
      <taxon>Apis</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="submission" date="2010-11" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="Honey bee genome project"/>
        <person name="Zhang L."/>
        <person name="Deng J."/>
        <person name="Wu Y.-Q."/>
        <person name="Kovar C."/>
        <person name="Aqrawi P."/>
        <person name="Bandaranaike D."/>
        <person name="Blankenburg K."/>
        <person name="Chen D."/>
        <person name="Denson S."/>
        <person name="Dinh H."/>
        <person name="Firestine M."/>
        <person name="Gross S."/>
        <person name="Han Y."/>
        <person name="Hernandez B."/>
        <person name="Holder M."/>
        <person name="Jackson L."/>
        <person name="Javaid M."/>
        <person name="Jing C."/>
        <person name="Jones J."/>
        <person name="Joshi V."/>
        <person name="Kamau G."/>
        <person name="Korchina V."/>
        <person name="Lee S."/>
        <person name="Lorensuhewa L."/>
        <person name="Mata R."/>
        <person name="Mathew T."/>
        <person name="Mims S."/>
        <person name="Ngo R."/>
        <person name="Nguyen L."/>
        <person name="Okwuonu G."/>
        <person name="Ongeri F."/>
        <person name="Osuji N."/>
        <person name="Pham C."/>
        <person name="Puazo M."/>
        <person name="Qu C."/>
        <person name="Quiroz J."/>
        <person name="Raj R."/>
        <person name="Rio Deiros D."/>
        <person name="Santibanez J."/>
        <person name="Scheel M."/>
        <person name="Scherer S."/>
        <person name="Vee V."/>
        <person name="Wang M."/>
        <person name="Xin Y."/>
        <person name="Richards S."/>
        <person name="Reid J.G."/>
        <person name="Newsham I."/>
        <person name="Worley K.C."/>
        <person name="Muzny D.M."/>
        <person name="Gibbs R."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>DH4</strain>
    </source>
  </reference>
  <reference evidence="4" key="2">
    <citation type="journal article" date="2006" name="Science" volume="314" first="647" last="649">
      <title>From the genome to the proteome: uncovering peptides in the Apis brain.</title>
      <authorList>
        <person name="Hummon A.B."/>
        <person name="Richmond T.A."/>
        <person name="Verleyen P."/>
        <person name="Baggerman G."/>
        <person name="Huybrechts J."/>
        <person name="Ewing M.A."/>
        <person name="Vierstraete E."/>
        <person name="Rodriguez-Zas S.L."/>
        <person name="Schoofs L."/>
        <person name="Robinson G.E."/>
        <person name="Sweedler J.V."/>
      </authorList>
      <dbReference type="PubMed" id="17068263"/>
      <dbReference type="DOI" id="10.1126/science.1124128"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 73-82</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>PYROGLUTAMATE FORMATION AT GLN-73</scope>
    <scope>AMIDATION AT PHE-82</scope>
    <source>
      <tissue evidence="3">Brain</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Myoinhibiting neuropeptide.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="1257.5" method="Electrospray" evidence="3"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the myosuppressin family.</text>
  </comment>
  <dbReference type="EMBL" id="AADG06001837">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P85527"/>
  <dbReference type="PaxDb" id="7460-GB44942-PA"/>
  <dbReference type="eggNOG" id="ENOG502SDA3">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_177213_0_0_1"/>
  <dbReference type="InParanoid" id="P85527"/>
  <dbReference type="OMA" id="HIKKVCM"/>
  <dbReference type="PhylomeDB" id="P85527"/>
  <dbReference type="Proteomes" id="UP000005203">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP001105180">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0873">Pyrrolidone carboxylic acid</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000341449" evidence="2 3">
    <location>
      <begin position="19"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000341450" description="Myosuppressin" evidence="3">
    <location>
      <begin position="73"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="modified residue" description="Pyrrolidone carboxylic acid" evidence="3">
    <location>
      <position position="73"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="3">
    <location>
      <position position="82"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P61849"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="17068263"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="86" mass="9788" checksum="A80BC13408161AF3" modified="2008-06-10" version="1" precursor="true">MAIFCNNVLAALPTQCNPGFLDDLPPRIRKVCVALSRIYELGSEMESYIGDKENHITGFHESIPLLDSGVKRQDVDHVFLRFGRRR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>