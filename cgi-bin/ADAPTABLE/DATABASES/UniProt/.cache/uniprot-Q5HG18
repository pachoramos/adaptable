<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-11-28" modified="2024-11-27" version="104" xmlns="http://uniprot.org/uniprot">
  <accession>Q5HG18</accession>
  <accession>Q8RJZ5</accession>
  <name>CSPA_STAAC</name>
  <protein>
    <recommendedName>
      <fullName>Cold shock protein CspA</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">cspA</name>
    <name type="ordered locus">SACOL1437</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain COL)</name>
    <dbReference type="NCBI Taxonomy" id="93062"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Microb. Drug Resist." volume="5" first="163" last="175">
      <title>Antibiotic resistance as a stress response: complete sequencing of a large number of chromosomal loci in Staphylococcus aureus strain COL that impact on the expression of resistance to methicillin.</title>
      <authorList>
        <person name="de Lencastre H."/>
        <person name="Wu S.-W."/>
        <person name="Pinho M.G."/>
        <person name="Ludovice A.M."/>
        <person name="Filipe S."/>
        <person name="Gardete S."/>
        <person name="Sobral R."/>
        <person name="Gill S.R."/>
        <person name="Chung M."/>
        <person name="Tomasz A."/>
      </authorList>
      <dbReference type="PubMed" id="10566865"/>
      <dbReference type="DOI" id="10.1089/mdr.1999.5.163"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="J. Bacteriol." volume="187" first="2426" last="2438">
      <title>Insights on evolution of virulence and resistance from the complete genome analysis of an early methicillin-resistant Staphylococcus aureus strain and a biofilm-producing methicillin-resistant Staphylococcus epidermidis strain.</title>
      <authorList>
        <person name="Gill S.R."/>
        <person name="Fouts D.E."/>
        <person name="Archer G.L."/>
        <person name="Mongodin E.F."/>
        <person name="DeBoy R.T."/>
        <person name="Ravel J."/>
        <person name="Paulsen I.T."/>
        <person name="Kolonay J.F."/>
        <person name="Brinkac L.M."/>
        <person name="Beanan M.J."/>
        <person name="Dodson R.J."/>
        <person name="Daugherty S.C."/>
        <person name="Madupu R."/>
        <person name="Angiuoli S.V."/>
        <person name="Durkin A.S."/>
        <person name="Haft D.H."/>
        <person name="Vamathevan J.J."/>
        <person name="Khouri H."/>
        <person name="Utterback T.R."/>
        <person name="Lee C."/>
        <person name="Dimitrov G."/>
        <person name="Jiang L."/>
        <person name="Qin H."/>
        <person name="Weidman J."/>
        <person name="Tran K."/>
        <person name="Kang K.H."/>
        <person name="Hance I.R."/>
        <person name="Nelson K.E."/>
        <person name="Fraser C.M."/>
      </authorList>
      <dbReference type="PubMed" id="15774886"/>
      <dbReference type="DOI" id="10.1128/jb.187.7.2426-2438.2005"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>COL</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2003" name="Infect. Immun." volume="71" first="4304" last="4312">
      <title>The major cold shock gene, cspA, is involved in the susceptibility of Staphylococcus aureus to an antimicrobial peptide of human cathepsin G.</title>
      <authorList>
        <person name="Katzif S."/>
        <person name="Danavall D."/>
        <person name="Bowers S."/>
        <person name="Balthazar J.T."/>
        <person name="Shafer W.M."/>
      </authorList>
      <dbReference type="PubMed" id="12874306"/>
      <dbReference type="DOI" id="10.1128/iai.71.8.4304-4312.2003"/>
    </citation>
    <scope>FUNCTION IN COLD STRESS RESPONSE</scope>
    <scope>SUSCEPTIBILITY TO ANTIMICROBIAL PEPTIDE</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2005" name="J. Bacteriol." volume="187" first="8181" last="8184">
      <title>CspA regulates pigment production in Staphylococcus aureus through a SigB-dependent mechanism.</title>
      <authorList>
        <person name="Katzif S."/>
        <person name="Lee E.-H."/>
        <person name="Law A.B."/>
        <person name="Tzeng Y.-L."/>
        <person name="Shafer W.M."/>
      </authorList>
      <dbReference type="PubMed" id="16291691"/>
      <dbReference type="DOI" id="10.1128/jb.187.23.8181-8184.2005"/>
    </citation>
    <scope>FUNCTION IN PIGMENT PRODUCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3">Involved in cold stress response and in the susceptibility to an antimicrobial peptide of human cathepsin G (CG117-136). Regulates yellowish-orange pigment production through a still unclear SigB-dependent mechanism.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <dbReference type="EMBL" id="Y18632">
    <property type="protein sequence ID" value="CAC80094.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP000046">
    <property type="protein sequence ID" value="AAW38182.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000809131.1">
    <property type="nucleotide sequence ID" value="NC_002951.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5HG18"/>
  <dbReference type="SMR" id="Q5HG18"/>
  <dbReference type="GeneID" id="66839594"/>
  <dbReference type="KEGG" id="sac:SACOL1437"/>
  <dbReference type="HOGENOM" id="CLU_117621_6_1_9"/>
  <dbReference type="Proteomes" id="UP000000530">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003676">
    <property type="term" value="F:nucleic acid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd04458">
    <property type="entry name" value="CSP_CDS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.40.50.140:FF:000006">
    <property type="entry name" value="Cold shock protein CspC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.20.370.130">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.40.50.140">
    <property type="entry name" value="Nucleic acid-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012156">
    <property type="entry name" value="Cold_shock_CspA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050181">
    <property type="entry name" value="Cold_shock_domain"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011129">
    <property type="entry name" value="CSD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019844">
    <property type="entry name" value="CSD_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002059">
    <property type="entry name" value="CSP_DNA-bd"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012340">
    <property type="entry name" value="NA-bd_OB-fold"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11544">
    <property type="entry name" value="COLD SHOCK DOMAIN CONTAINING PROTEINS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11544:SF142">
    <property type="entry name" value="COLD SHOCK PROTEIN CSPA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00313">
    <property type="entry name" value="CSD"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF002599">
    <property type="entry name" value="Cold_shock_A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00050">
    <property type="entry name" value="COLDSHOCK"/>
  </dbReference>
  <dbReference type="SMART" id="SM00357">
    <property type="entry name" value="CSP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF50249">
    <property type="entry name" value="Nucleic acid-binding proteins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00352">
    <property type="entry name" value="CSD_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51857">
    <property type="entry name" value="CSD_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <feature type="chain" id="PRO_0000262541" description="Cold shock protein CspA">
    <location>
      <begin position="1"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="domain" description="CSD">
    <location>
      <begin position="1"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; CAC80094." evidence="4" ref="1">
    <original>F</original>
    <variation>L</variation>
    <location>
      <position position="27"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="12874306"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="16291691"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="66" mass="7321" checksum="E80AFCA652622943" modified="2005-02-15" version="1">MKQGTVKWFNAEKGFGFIEVEGENDVFVHFSAINQDGYKSLEEGQAVEFEVVEGDRGPQAANVVKL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>