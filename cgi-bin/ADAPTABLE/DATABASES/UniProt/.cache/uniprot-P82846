<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-01-16" modified="2022-08-03" version="47" xmlns="http://uniprot.org/uniprot">
  <accession>P82846</accession>
  <name>ES2P_LITPI</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Esculentin-2P</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Lithobates pipiens</name>
    <name type="common">Northern leopard frog</name>
    <name type="synonym">Rana pipiens</name>
    <dbReference type="NCBI Taxonomy" id="8404"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Lithobates</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="Eur. J. Biochem." volume="267" first="894" last="900">
      <title>Peptides with antimicrobial activity from four different families isolated from the skins of the North American frogs Rana luteiventris, Rana berlandieri and Rana pipiens.</title>
      <authorList>
        <person name="Goraya J."/>
        <person name="Wang Y."/>
        <person name="Li Z."/>
        <person name="O'Flaherty M."/>
        <person name="Knoop F.C."/>
        <person name="Platz J.E."/>
        <person name="Conlon J.M."/>
      </authorList>
      <dbReference type="PubMed" id="10651828"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.2000.01074.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Antibacterial activity against Gram-negative bacterium E.coli.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="3868.4" method="Electrospray" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Esculentin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=00663"/>
  </comment>
  <dbReference type="AlphaFoldDB" id="P82846"/>
  <dbReference type="SMR" id="P82846"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012521">
    <property type="entry name" value="Antimicrobial_frog_2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08023">
    <property type="entry name" value="Antimicrobial_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044651" description="Esculentin-2P" evidence="2">
    <location>
      <begin position="1"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="31"/>
      <end position="37"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10651828"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="10651828"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="10651828"/>
    </source>
  </evidence>
  <sequence length="37" mass="3871" checksum="82FC3962CC1CBF64" modified="2001-03-01" version="1">GFSSIFRGVAKFASKGLGKDLARLGVNLVACKISKQC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>