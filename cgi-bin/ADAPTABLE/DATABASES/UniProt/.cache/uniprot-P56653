<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1999-07-15" modified="2023-09-13" version="80" xmlns="http://uniprot.org/uniprot">
  <accession>P56653</accession>
  <accession>A5JTM7</accession>
  <name>4HBT_PSEUC</name>
  <protein>
    <recommendedName>
      <fullName>4-hydroxybenzoyl-CoA thioesterase</fullName>
      <ecNumber>3.1.2.23</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Pseudomonas sp. (strain CBS-3)</name>
    <dbReference type="NCBI Taxonomy" id="72586"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="Biochemistry" volume="31" first="5594" last="5604">
      <title>Ancestry of the 4-chlorobenzoate dehalogenase: analysis of amino acid sequence identities among families of acyl:adenyl ligases, enoyl-CoA hydratases/isomerases, and acyl-CoA thioesterases.</title>
      <authorList>
        <person name="Babbitt P.C."/>
        <person name="Kenyon G.L."/>
        <person name="Martin B.M."/>
        <person name="Charest H."/>
        <person name="Slyvestre M."/>
        <person name="Scholten J.D."/>
        <person name="Chang K.H."/>
        <person name="Liang P.H."/>
        <person name="Dunaway-Mariano D."/>
      </authorList>
      <dbReference type="PubMed" id="1351742"/>
      <dbReference type="DOI" id="10.1021/bi00139a024"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="2007-04" db="EMBL/GenBank/DDBJ databases">
      <title>The nucleotide sequence upstream and downstream of 4-CBA-CoA ORFs in 9.5 kb Pseudomonas sp. strain CBS 3 chromosomal DNA.</title>
      <authorList>
        <person name="Zhang W."/>
        <person name="Dunaway-Mariano D."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1992" name="Biochemistry" volume="31" first="5605" last="5610">
      <title>Isolation and characterization of the three polypeptide components of 4-chlorobenzoate dehalogenase from Pseudomonas sp. strain CBS-3.</title>
      <authorList>
        <person name="Chang K.H."/>
        <person name="Liang P.H."/>
        <person name="Beck W."/>
        <person name="Scholten J.D."/>
        <person name="Dunaway-Mariano D."/>
      </authorList>
      <dbReference type="PubMed" id="1610806"/>
      <dbReference type="DOI" id="10.1021/bi00139a025"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>PATHWAY</scope>
    <scope>SUBUNIT</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2002" name="Biochemistry" volume="41" first="11152" last="11160">
      <title>Kinetic, Raman, NMR, and site-directed mutagenesis studies of the Pseudomonas sp. strain CBS3 4-hydroxybenzoyl-CoA thioesterase active site.</title>
      <authorList>
        <person name="Zhuang Z."/>
        <person name="Song F."/>
        <person name="Zhang W."/>
        <person name="Taylor K."/>
        <person name="Archambault A."/>
        <person name="Dunaway-Mariano D."/>
        <person name="Dong J."/>
        <person name="Carey P.R."/>
      </authorList>
      <dbReference type="PubMed" id="12220180"/>
      <dbReference type="DOI" id="10.1021/bi0262303"/>
    </citation>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>MUTAGENESIS OF ASP-17; ARG-88; ARG-89; LYS-90; ARG-126 AND ARG-128</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2007" name="Bioorg. Chem." volume="35" first="1" last="10">
      <title>Structure-activity analysis of base and enzyme-catalyzed 4-hydroxybenzoyl coenzyme A hydrolysis.</title>
      <authorList>
        <person name="Song F."/>
        <person name="Zhuang Z."/>
        <person name="Dunaway-Mariano D."/>
      </authorList>
      <dbReference type="PubMed" id="16962159"/>
      <dbReference type="DOI" id="10.1016/j.bioorg.2006.07.002"/>
    </citation>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>MUTAGENESIS OF ASP-32</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1998" name="J. Biol. Chem." volume="273" first="33572" last="33579">
      <title>The three-dimensional structure of 4-hydroxybenzoyl-CoA thioesterase from Pseudomonas sp. strain CBS-3.</title>
      <authorList>
        <person name="Benning M.M."/>
        <person name="Wesenberg G."/>
        <person name="Liu R."/>
        <person name="Taylor K.L."/>
        <person name="Dunaway-Mariano D."/>
        <person name="Holden H.M."/>
      </authorList>
      <dbReference type="PubMed" id="9837940"/>
      <dbReference type="DOI" id="10.1074/jbc.273.50.33572"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (2.0 ANGSTROMS)</scope>
    <scope>SUBUNIT</scope>
    <scope>ACTIVE SITE</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2002" name="J. Biol. Chem." volume="277" first="27468" last="27476">
      <title>X-ray crystallographic analyses of inhibitor and substrate complexes of wild-type and mutant 4-hydroxybenzoyl-CoA thioesterase.</title>
      <authorList>
        <person name="Thoden J.B."/>
        <person name="Holden H.M."/>
        <person name="Zhuang Z."/>
        <person name="Dunaway-Mariano D."/>
      </authorList>
      <dbReference type="PubMed" id="11997398"/>
      <dbReference type="DOI" id="10.1074/jbc.m203904200"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.5 ANGSTROMS) OF WILD-TYPE IN COMPLEX WITH INHIBITOR AND MUTANT ASN-17 IN COMPLEX WITH SUBSTRATE</scope>
    <scope>SUBUNIT</scope>
    <scope>ACTIVE SITE</scope>
  </reference>
  <comment type="function">
    <text evidence="4">Hydrolyzes 4-hydroxybenzoate-CoA, and to a lesser extent benzoyl-CoA and 4-chlorobenzoate-CoA. Not active against aliphatic acyl-CoA thioesters, including palmitoyl-CoA, hexanoyl-CoA and acetyl-CoA.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="1 4">
      <text>4-hydroxybenzoyl-CoA + H2O = 4-hydroxybenzoate + CoA + H(+)</text>
      <dbReference type="Rhea" id="RHEA:11948"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:17879"/>
      <dbReference type="ChEBI" id="CHEBI:57287"/>
      <dbReference type="ChEBI" id="CHEBI:57356"/>
      <dbReference type="EC" id="3.1.2.23"/>
    </reaction>
  </comment>
  <comment type="activity regulation">
    <text evidence="4">Unaffected by EDTA, Mg(2+), Mn(2+), Fe(2+), Ca(2+), Co(2+) and Zn(2+).</text>
  </comment>
  <comment type="biophysicochemical properties">
    <kinetics>
      <KM evidence="3 4 5">6 uM for 4-hydroxybenzoyl-CoA</KM>
      <KM evidence="3 4 5">550 uM for 4-chlorobenzoyl-CoA</KM>
      <KM evidence="3 4 5">200 uM for benzoyl-CoA</KM>
      <KM evidence="3 4 5">270 uM for 4-hydroxybenzoyl-pantetheine</KM>
      <KM evidence="3 4 5">56 uM for 4-methoxybenzoyl-CoA</KM>
      <KM evidence="3 4 5">230 uM for 4-methylbenzoyl-CoA</KM>
      <KM evidence="3 4 5">510 uM for benzoyl-CoA</KM>
      <KM evidence="3 4 5">520 uM for 4-fluorobenzoyl-CoA</KM>
      <KM evidence="3 4 5">300 uM for 4-trifluorobenzoyl-CoA</KM>
    </kinetics>
  </comment>
  <comment type="pathway">
    <text evidence="4">Xenobiotic degradation; 4-chlorobenzoate degradation; 4-hydroxybenzoate from 4-chlorobenzoate: step 3/3.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2 4 6">Homotetramer.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the 4-hydroxybenzoyl-CoA thioesterase family.</text>
  </comment>
  <dbReference type="EC" id="3.1.2.23"/>
  <dbReference type="EMBL" id="EF569604">
    <property type="protein sequence ID" value="ABQ44580.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PDB" id="1BVQ">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.00 A"/>
    <property type="chains" value="A=1-141"/>
  </dbReference>
  <dbReference type="PDB" id="1LO7">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.50 A"/>
    <property type="chains" value="A=1-141"/>
  </dbReference>
  <dbReference type="PDB" id="1LO8">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.80 A"/>
    <property type="chains" value="A=1-141"/>
  </dbReference>
  <dbReference type="PDB" id="1LO9">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="2.80 A"/>
    <property type="chains" value="A=1-141"/>
  </dbReference>
  <dbReference type="PDBsum" id="1BVQ"/>
  <dbReference type="PDBsum" id="1LO7"/>
  <dbReference type="PDBsum" id="1LO8"/>
  <dbReference type="PDBsum" id="1LO9"/>
  <dbReference type="AlphaFoldDB" id="P56653"/>
  <dbReference type="SMR" id="P56653"/>
  <dbReference type="DrugBank" id="DB01652">
    <property type="generic name" value="4-hydroxybenzoyl-CoA"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB04067">
    <property type="generic name" value="4-hydroxybenzyl coenzyme A"/>
  </dbReference>
  <dbReference type="DrugBank" id="DB03613">
    <property type="generic name" value="4-hydroxyphenacyl coenzyme A"/>
  </dbReference>
  <dbReference type="KEGG" id="ag:ABQ44580"/>
  <dbReference type="BioCyc" id="MetaCyc:MONOMER-14754"/>
  <dbReference type="BRENDA" id="3.1.2.23">
    <property type="organism ID" value="5085"/>
  </dbReference>
  <dbReference type="SABIO-RK" id="P56653"/>
  <dbReference type="UniPathway" id="UPA01011">
    <property type="reaction ID" value="UER01022"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P56653"/>
  <dbReference type="GO" id="GO:0018739">
    <property type="term" value="F:4-hydroxybenzoyl-CoA thioesterase activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00586">
    <property type="entry name" value="4HBT"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.129.10">
    <property type="entry name" value="Hotdog Thioesterase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008272">
    <property type="entry name" value="HB-CoA_thioesterase_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029069">
    <property type="entry name" value="HotDog_dom_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13279">
    <property type="entry name" value="4HBT_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54637">
    <property type="entry name" value="Thioesterase/thiol ester dehydrase-isomerase"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01328">
    <property type="entry name" value="4HBCOA_THIOESTERASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <feature type="chain" id="PRO_0000087760" description="4-hydroxybenzoyl-CoA thioesterase">
    <location>
      <begin position="1"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="active site" evidence="1 2 6">
    <location>
      <position position="17"/>
    </location>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="47"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="binding site">
    <location>
      <begin position="59"/>
      <end position="61"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="90"/>
    </location>
    <ligand>
      <name>substrate</name>
    </ligand>
  </feature>
  <feature type="mutagenesis site" description="Reduces catalytic activity. Little effect on substrate binding." evidence="3">
    <original>D</original>
    <variation>E</variation>
    <location>
      <position position="17"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Drastically reduces catalytic activity. No effect on substrate binding." evidence="3">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="17"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Drastically reduces catalytic activity." evidence="3">
    <original>D</original>
    <variation>S</variation>
    <location>
      <position position="17"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Substrate turnover rate is decreased." evidence="5">
    <original>D</original>
    <variation>S</variation>
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No significant effect on catalytic activity or substrate binding." evidence="3">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="88"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No significant effect on catalytic activity or substrate binding." evidence="3">
    <original>R</original>
    <variation>L</variation>
    <location>
      <position position="89"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Decreases substrate binding affinity." evidence="3">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="90"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No significant effect on catalytic activity or substrate binding." evidence="3">
    <original>R</original>
    <variation>L</variation>
    <location>
      <position position="126"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No significant effect on catalytic activity or substrate binding." evidence="3">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="128"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; ABQ44580." evidence="7" ref="2">
    <original>V</original>
    <variation>L</variation>
    <location>
      <position position="107"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="4"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="helix" evidence="8">
    <location>
      <begin position="13"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="20"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="helix" evidence="8">
    <location>
      <begin position="25"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="helix" evidence="8">
    <location>
      <begin position="47"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="56"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="61"/>
      <end position="68"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="77"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="89"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="107"/>
      <end position="123"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="126"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="helix" evidence="8">
    <location>
      <begin position="134"/>
      <end position="140"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10041"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11997398"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12220180"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="1610806"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="16962159"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="9837940"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0007829" key="8">
    <source>
      <dbReference type="PDB" id="1LO7"/>
    </source>
  </evidence>
  <sequence length="141" mass="16105" checksum="410896EA5CC49F22" modified="1999-07-15" version="1">MARSITMQQRIEFGDCDPAGIVWFPNYHRWLDAASRNYFIKCGLPPWRQTVVERGIVGTPIVSCNASFVCTASYDDVLTIETCIKEWRRKSFVQRHSVSRTTPGGDVQLVMRADEIRVFAMNDGERLRAIEVPADYIELCS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>