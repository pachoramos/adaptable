<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-11-08" modified="2024-05-29" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>C0HM83</accession>
  <name>SHMOS_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">Protein SHMOOSE</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="3">Small human mitochondrial ORF over serine tRNA</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <geneLocation type="mitochondrion"/>
  <reference evidence="4" key="1">
    <citation type="journal article" date="1981" name="Nature" volume="290" first="457" last="465">
      <title>Sequence and organization of the human mitochondrial genome.</title>
      <authorList>
        <person name="Anderson S."/>
        <person name="Bankier A.T."/>
        <person name="Barrell B.G."/>
        <person name="de Bruijn M.H.L."/>
        <person name="Coulson A.R."/>
        <person name="Drouin J."/>
        <person name="Eperon I.C."/>
        <person name="Nierlich D.P."/>
        <person name="Roe B.A."/>
        <person name="Sanger F."/>
        <person name="Schreier P.H."/>
        <person name="Smith A.J.H."/>
        <person name="Staden R."/>
        <person name="Young I.G."/>
      </authorList>
      <dbReference type="PubMed" id="7219534"/>
      <dbReference type="DOI" id="10.1038/290457a0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference evidence="4" key="2">
    <citation type="journal article" date="2023" name="Mol. Psychiatry" volume="28" first="1813" last="1826">
      <title>Mitochondrial DNA variation in Alzheimer's disease reveals a unique microprotein called SHMOOSE.</title>
      <authorList>
        <consortium name="Alzheimer's Disease Neuroimaging Initiative"/>
        <person name="Miller B."/>
        <person name="Kim S.J."/>
        <person name="Mehta H.H."/>
        <person name="Cao K."/>
        <person name="Kumagai H."/>
        <person name="Thumaty N."/>
        <person name="Leelaprachakul N."/>
        <person name="Braniff R.G."/>
        <person name="Jiao H."/>
        <person name="Vaughan J."/>
        <person name="Diedrich J."/>
        <person name="Saghatelian A."/>
        <person name="Arpawong T.E."/>
        <person name="Crimmins E.M."/>
        <person name="Ertekin-Taner N."/>
        <person name="Tubi M.A."/>
        <person name="Hare E.T."/>
        <person name="Braskie M.N."/>
        <person name="Decarie-Spain L."/>
        <person name="Kanoski S.E."/>
        <person name="Grodstein F."/>
        <person name="Bennett D.A."/>
        <person name="Zhao L."/>
        <person name="Toga A.W."/>
        <person name="Wan J."/>
        <person name="Yen K."/>
        <person name="Cohen P."/>
      </authorList>
      <dbReference type="PubMed" id="36127429"/>
      <dbReference type="DOI" id="10.1038/s41380-022-01769-3"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 15-25 AND 42-58</scope>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH IMMT</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>VARIANT ASN-47</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
  </reference>
  <reference evidence="4" key="3">
    <citation type="journal article" date="2023" name="Mol. Psychiatry" volume="28" first="1827" last="1827">
      <authorList>
        <consortium name="Alzheimer's Disease Neuroimaging Initiative"/>
        <person name="Miller B."/>
        <person name="Kim S.J."/>
        <person name="Mehta H.H."/>
        <person name="Cao K."/>
        <person name="Kumagai H."/>
        <person name="Thumaty N."/>
        <person name="Leelaprachakul N."/>
        <person name="Braniff R.G."/>
        <person name="Jiao H."/>
        <person name="Vaughan J."/>
        <person name="Diedrich J."/>
        <person name="Saghatelian A."/>
        <person name="Arpawong T.E."/>
        <person name="Crimmins E.M."/>
        <person name="Ertekin-Taner N."/>
        <person name="Tubi M.A."/>
        <person name="Hare E.T."/>
        <person name="Braskie M.N."/>
        <person name="Decarie-Spain L."/>
        <person name="Kanoski S.E."/>
        <person name="Grodstein F."/>
        <person name="Bennett D.A."/>
        <person name="Zhao L."/>
        <person name="Toga A.W."/>
        <person name="Wan J."/>
        <person name="Yen K."/>
        <person name="Cohen P."/>
      </authorList>
      <dbReference type="PubMed" id="36658336"/>
      <dbReference type="DOI" id="10.1038/s41380-023-01956-w"/>
    </citation>
    <scope>ERRATUM OF PUBMED:36127429</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Increases neural cell metabolic activity and mitochondrial oxygen consumption rate.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Interacts with IMMT/mitofilin.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-46442350">
      <id>C0HM83</id>
    </interactant>
    <interactant intactId="EBI-473801">
      <id>Q16891</id>
      <label>IMMT</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>5</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Mitochondrion</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="2">Nucleus</location>
    </subcellularLocation>
    <text evidence="2">Detected in neuronal mitochondria and nuclei.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Detected in cerebrospinal fluid (at protein level).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">Shows elevated expression in Alzheimer disease patient brains and protects against cell death in vitro in neuronal cells stressed with oligomerized amyloid-beta protein 42.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">Intracerebroventricular administration to the rat brain for 24 hours results in significant alterations to the hypothalamus transcriptome and, to a lesser extent, the hippocampus transcriptome.</text>
  </comment>
  <dbReference type="EMBL" id="J01415">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="IntAct" id="C0HM83">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Mitochondrion MT"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0496">Mitochondrion</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000458864" description="Protein SHMOOSE">
    <location>
      <begin position="1"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="27"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_088368" description="Risk factor for Alzheimer disease; protein does not protect against cell death in vitro in neuronal cells stressed with oligomerized amyloid-beta protein 42; dbSNP:rs2853499." evidence="2">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="47"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="36127429"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="36127429"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="58" mass="6637" checksum="961C5C3F9CF34204" modified="2023-11-08" version="1">MPPCLTTWLSQLLKDNSYPLVLGPKNFGATPNKSNNHAHYYNHPNPDFPNSPHPYHPR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>