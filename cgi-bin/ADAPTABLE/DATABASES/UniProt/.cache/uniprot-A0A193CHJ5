<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2023-11-08" modified="2024-11-27" version="20" xmlns="http://uniprot.org/uniprot">
  <accession>A0A193CHJ5</accession>
  <name>PA2BC_CROTA</name>
  <protein>
    <recommendedName>
      <fullName evidence="8">Phospholipase A2 crotoxin basic subunit CBc</fullName>
      <shortName evidence="8">CB1</shortName>
      <shortName evidence="8">CTX subunit CBc</shortName>
      <shortName evidence="8">svPLA2</shortName>
      <ecNumber evidence="7">3.1.1.4</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="8">Phosphatidylcholine 2-acylhydrolase</fullName>
    </alternativeName>
  </protein>
  <organism evidence="11">
    <name type="scientific">Crotalus tzabcan</name>
    <name type="common">Yucatan neotropical rattlesnake</name>
    <name type="synonym">Crotalus simus tzabcan</name>
    <dbReference type="NCBI Taxonomy" id="1043006"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Lepidosauria</taxon>
      <taxon>Squamata</taxon>
      <taxon>Bifurcata</taxon>
      <taxon>Unidentata</taxon>
      <taxon>Episquamata</taxon>
      <taxon>Toxicofera</taxon>
      <taxon>Serpentes</taxon>
      <taxon>Colubroidea</taxon>
      <taxon>Viperidae</taxon>
      <taxon>Crotalinae</taxon>
      <taxon>Crotalus</taxon>
    </lineage>
  </organism>
  <reference evidence="11" key="1">
    <citation type="journal article" date="2016" name="PLoS Negl. Trop. Dis." volume="10" first="E0004587" last="E0004587">
      <title>Full-Length Venom Protein cDNA Sequences from Venom-Derived mRNA: Exploring Compositional Variation and Adaptive Multigene Evolution.</title>
      <authorList>
        <person name="Modahl C.M."/>
        <person name="Mackessy S.P."/>
      </authorList>
      <dbReference type="PubMed" id="27280639"/>
      <dbReference type="DOI" id="10.1371/journal.pntd.0004587"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2022" name="Toxins" volume="14" first="382" last="382">
      <title>Crotoxin B: Heterologous Expression, Protein Folding, Immunogenic Properties, and Irregular Presence in Crotalid Venoms.</title>
      <authorList>
        <person name="Mejia-Sanchez M.A."/>
        <person name="Clement H."/>
        <person name="Corrales-Garcia L.L."/>
        <person name="Olamendi-Portugal T."/>
        <person name="Carbajal A."/>
        <person name="Corzo G."/>
      </authorList>
      <dbReference type="PubMed" id="35737043"/>
      <dbReference type="DOI" id="10.3390/toxins14060382"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] OF 17-138</scope>
    <scope>PROTEIN SEQUENCE OF 57-69; 77-81 AND 115-130</scope>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>ACTIVITY REGULATION</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>BIOTECHNOLOGY</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Heterodimer CA-CB: Crotoxin is a potent presynaptic neurotoxin that possesses phospholipase A2 (PLA2) activity and exerts a lethal action by blocking neuromuscular transmission (By similarity). It consists of a non-covalent association of a basic and weakly toxic PLA2 subunit (CBa2, CBb, CBc, or CBd), with a small acidic, non-enzymatic and non-toxic subunit (CA1, CA2, CA3 or CA4) (By similarity). The complex acts by binding to a specific 48-kDa protein (R48) receptor located on presynaptic membranes, forming a transient ternary complex CA-CB-R48, followed by dissociation of the CA-CB complex and release of the CA subunit (By similarity). At equilibrium, only the CB subunits remain associated with the specific crotoxin receptor (By similarity).</text>
  </comment>
  <comment type="function">
    <text evidence="2 7">Monomer CBc: The basic subunit of crotoxin is a snake venom phospholipase A2 (PLA2) that exhibits weak neurotoxicity (10-fold less than the heterodimer) and very strong anticoagulant effects by binding to factor Xa (F10) and inhibiting the prothrombinase activity (By similarity). In addition, it shows the same effects described for the heterodimer and binds the nucleotide-binding domain (NBD1) of CFTR chloride channels and increases the channel current (By similarity). PLA2 catalyzes the calcium-dependent hydrolysis of the 2-acyl groups in 3-sn-phosphoglycerides (PubMed:35737043).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="7">
      <text>a 1,2-diacyl-sn-glycero-3-phosphocholine + H2O = a 1-acyl-sn-glycero-3-phosphocholine + a fatty acid + H(+)</text>
      <dbReference type="Rhea" id="RHEA:15801"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:28868"/>
      <dbReference type="ChEBI" id="CHEBI:57643"/>
      <dbReference type="ChEBI" id="CHEBI:58168"/>
      <dbReference type="EC" id="3.1.1.4"/>
    </reaction>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="4">
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </cofactor>
    <text evidence="4">Binds 1 Ca(2+) ion per subunit.</text>
  </comment>
  <comment type="activity regulation">
    <text evidence="7">Phosphatidylcholine 2-acylhydrolase activity as well as activity of complete venom of Crotalus tzabcan is inhibited by antibodies against recombinant phospholipase A2 crotoxin basic subunit CBc produced in rabbit.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <kinetics>
      <Vmax evidence="7">9.8 umol/min/mg enzyme (monomer CBc)</Vmax>
    </kinetics>
  </comment>
  <comment type="subunit">
    <text evidence="2">Heterodimer of one of the acidic (CA1, CA2, CA3 or CA4) and one of the basic (CBa1, CBa2, CBb, CBc or CBd) subunits; non-covalently linked (By similarity). The acidic subunit is non-toxic, without enzymatic activity and comprises 3 peptides that are cross-linked by 5 disulfide bridges (By similarity). The basic subunit is toxic, has phospholipase A2 activity and is composed of a single chain (By similarity). Multiple variants of each subunit give different crotoxin complexes that can be subdivided into 2 classes: (1) those of high toxicity, low PLA2 activity (CBb, CBc and CBd linked with high affinity to any CA) and high stability and (2) those of moderate toxicity, high PLA2 activity (CBa2 linked with low affinity to any CA) and low stability (By similarity). Interacts with crotoxin inhibitor from Crotalus serum (CICS); the interaction leads to dissociation of the CA-CB heterodimer and to inhibition of PLA2 activity of the CB subunit (By similarity). Interacts with human NBD1 domain of CFTR (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6 10">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7 9">Expressed by the venom gland.</text>
  </comment>
  <comment type="biotechnology">
    <text evidence="7">Could be a suitable immunogen to raise anti-Crotoxin B and anti-PLA2 antibodies to decrease PLA2 activity of C.tzabcan phospholipases, related to several toxic activities such as myotoxicity or interfering with the platelet function, at least in this C.tzabcan venom.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">The crotoxin heterodimer is inhibited by the crotoxin inhibitor from Crotalus serum (CICS) (By similarity). CICS neutralizes the lethal potency of crotoxin and inhibits its PLA2 activity (By similarity). CICS only binds tightly to the CB subunit and induces the dissociation of the heterodimer (By similarity). Tested on the CA2-CBd heterodimer (By similarity).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="7">Antibodies against recombinant phospholipase A2 crotoxin basic subunit CBc produced in rabbit are able to neutralize whole venoms of C.tzabcan, C.s.salvini, and C.mictlantecuhtli.</text>
  </comment>
  <comment type="similarity">
    <text evidence="8">Belongs to the phospholipase A2 family. Group II subfamily. D49 sub-subfamily.</text>
  </comment>
  <dbReference type="EC" id="3.1.1.4" evidence="7"/>
  <dbReference type="EMBL" id="KU666906">
    <property type="protein sequence ID" value="ANN23915.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0A193CHJ5"/>
  <dbReference type="SMR" id="A0A193CHJ5"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0047498">
    <property type="term" value="F:calcium-dependent phospholipase A2 activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0099106">
    <property type="term" value="F:ion channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005543">
    <property type="term" value="F:phospholipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050482">
    <property type="term" value="P:arachidonic acid secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016042">
    <property type="term" value="P:lipid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042130">
    <property type="term" value="P:negative regulation of T cell proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006644">
    <property type="term" value="P:phospholipid metabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00125">
    <property type="entry name" value="PLA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.90.10:FF:000001">
    <property type="entry name" value="Basic phospholipase A2 homolog"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.90.10">
    <property type="entry name" value="Phospholipase A2 domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001211">
    <property type="entry name" value="PLipase_A2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033112">
    <property type="entry name" value="PLipase_A2_Asp_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016090">
    <property type="entry name" value="PLipase_A2_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036444">
    <property type="entry name" value="PLipase_A2_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033113">
    <property type="entry name" value="PLipase_A2_His_AS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716:SF57">
    <property type="entry name" value="GROUP IID SECRETORY PHOSPHOLIPASE A2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716">
    <property type="entry name" value="PHOSPHOLIPASE A2 FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00068">
    <property type="entry name" value="Phospholip_A2_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00389">
    <property type="entry name" value="PHPHLIPASEA2"/>
  </dbReference>
  <dbReference type="SMART" id="SM00085">
    <property type="entry name" value="PA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48619">
    <property type="entry name" value="Phospholipase A2, PLA2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00119">
    <property type="entry name" value="PA2_ASP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00118">
    <property type="entry name" value="PA2_HIS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1203">Blood coagulation cascade inhibiting toxin</keyword>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1199">Hemostasis impairing toxin</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0442">Lipid degradation</keyword>
  <keyword id="KW-0443">Lipid metabolism</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0638">Presynaptic neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="6">
    <location>
      <begin position="1"/>
      <end position="16"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5008443554" description="Phospholipase A2 crotoxin basic subunit CBc" evidence="6">
    <location>
      <begin position="17"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="active site" evidence="3">
    <location>
      <position position="63"/>
    </location>
  </feature>
  <feature type="active site" evidence="3">
    <location>
      <position position="105"/>
    </location>
  </feature>
  <feature type="binding site" evidence="4">
    <location>
      <position position="43"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="4">
    <location>
      <position position="45"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="4">
    <location>
      <position position="47"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="4">
    <location>
      <position position="64"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="site" description="Responsible for the weak stability and toxicity" evidence="2">
    <location>
      <position position="17"/>
    </location>
  </feature>
  <feature type="site" description="Putative interfacial binding surface (IBS)" evidence="2">
    <location>
      <position position="18"/>
    </location>
  </feature>
  <feature type="site" description="Putative interfacial binding surface (IBS)" evidence="2">
    <location>
      <position position="19"/>
    </location>
  </feature>
  <feature type="site" description="Putative interfacial binding surface (IBS)" evidence="2">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="site" description="Putative interfacial binding surface (IBS)" evidence="2">
    <location>
      <position position="26"/>
    </location>
  </feature>
  <feature type="site" description="Putative interfacial binding surface (IBS)" evidence="2">
    <location>
      <position position="33"/>
    </location>
  </feature>
  <feature type="site" description="Putative interfacial binding surface (IBS)" evidence="2">
    <location>
      <position position="38"/>
    </location>
  </feature>
  <feature type="site" description="Putative interfacial binding surface (IBS)" evidence="2">
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="site" description="Putative interfacial binding surface (IBS)" evidence="2">
    <location>
      <position position="76"/>
    </location>
  </feature>
  <feature type="site" description="Putative interfacial binding surface (IBS)" evidence="2">
    <location>
      <position position="119"/>
    </location>
  </feature>
  <feature type="site" description="Responsible for the higher anticoagulant activity (compared with CBa2)" evidence="2">
    <location>
      <position position="133"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="42"/>
      <end position="131"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="44"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="59"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="65"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="66"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="73"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="5">
    <location>
      <begin position="91"/>
      <end position="102"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="C0HM14"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P62022"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PIRSR" id="PIRSR601211-1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="4">
    <source>
      <dbReference type="PIRSR" id="PIRSR601211-2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="5">
    <source>
      <dbReference type="PIRSR" id="PIRSR601211-3"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="6">
    <source>
      <dbReference type="RuleBase" id="RU361236"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="35737043"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <evidence type="ECO:0000305" key="9">
    <source>
      <dbReference type="PubMed" id="27280639"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10">
    <source>
      <dbReference type="PubMed" id="35737043"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="11">
    <source>
      <dbReference type="EMBL" id="ANN23915.1"/>
    </source>
  </evidence>
  <sequence length="138" mass="15907" checksum="84A118931DFFE2E3" modified="2016-10-05" version="1" precursor="true">MRALWIVAVLLVGVEGHLLQFNKMIKFETRKNAIPFYAFYGCYCGWGGRGRPKDATDRCCFVHDCCYGKLAKCNTKWDIYPYSLKSGYITCGKGTWCEEQICECDRVAAECLRRSLSTYKYGYMFYPDSRCRGPSETC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>