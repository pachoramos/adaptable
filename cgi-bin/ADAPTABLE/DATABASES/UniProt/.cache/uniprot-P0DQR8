<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2021-09-29" modified="2022-12-14" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>P0DQR8</accession>
  <name>CL8L_NEMVE</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Protein Class8-like</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Nematostella vectensis</name>
    <name type="common">Starlet sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="45351"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Edwardsiidae</taxon>
      <taxon>Nematostella</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2020" name="Proc. Natl. Acad. Sci. U.S.A." volume="117" first="27481" last="27492">
      <title>Toxin-like neuropeptides in the sea anemone Nematostella unravel recruitment from the nervous system to venom.</title>
      <authorList>
        <person name="Sachkova M.Y."/>
        <person name="Landau M."/>
        <person name="Surm J.M."/>
        <person name="Macrander J."/>
        <person name="Singer S.A."/>
        <person name="Reitzel A.M."/>
        <person name="Moran Y."/>
      </authorList>
      <dbReference type="PubMed" id="33060291"/>
      <dbReference type="DOI" id="10.1073/pnas.2011120117"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 44-58</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2018" name="Development" volume="145">
      <title>NvERTx: a gene expression database to compare embryogenesis and regeneration in the sea anemone Nematostella vectensis.</title>
      <authorList>
        <person name="Warner J.F."/>
        <person name="Guerlais V."/>
        <person name="Amiel A.R."/>
        <person name="Johnston H."/>
        <person name="Nedoncelle K."/>
        <person name="Roettinger E."/>
      </authorList>
      <dbReference type="PubMed" id="29739837"/>
      <dbReference type="DOI" id="10.1242/dev.162867"/>
    </citation>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <comment type="function">
    <text evidence="6">Probable neuropeptide.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">Expressed in ganglion neurons residing in the mesoglea (observed in both planulae and primary polyps). Not expressed in nematocytes.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="3 4">Is strongly detected in planulae, a little more detected in primary polyps (9d), and still a little more in both adult females and males (at protein level) (PubMed:33060291). Transcripts are expressed early in the life cycle and their expression is maintained through the adult stage (PubMed:29739837).</text>
  </comment>
  <comment type="online information" name="National Center for Biotechnology Information (NCBI)">
    <link uri="https://www.ncbi.nlm.nih.gov/nuccore/HADO01000486.1/"/>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DQR8"/>
  <dbReference type="SMR" id="P0DQR8"/>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000453916" evidence="6">
    <location>
      <begin position="20"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000453917" description="Protein Class8-like" evidence="6">
    <location>
      <begin position="37"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="domain" description="ShKT" evidence="2">
    <location>
      <begin position="38"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="38"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="47"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="56"/>
      <end position="75"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01005"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="29739837"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="33060291"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="33060291"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="33060291"/>
    </source>
  </evidence>
  <sequence length="78" mass="8532" checksum="3138A114C84D1BFD" modified="2021-09-29" version="1" precursor="true">MRTLVVLLIGAVLLCSANAFLDELLAESVNDMTDKRACFDKYKSNICGGVISPAHCVRRSGRMAKFAKENCAHFCGFC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>