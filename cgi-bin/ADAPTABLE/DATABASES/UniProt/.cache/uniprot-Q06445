<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-11-27" version="79" xmlns="http://uniprot.org/uniprot">
  <accession>Q06445</accession>
  <name>CYTI_VIGUN</name>
  <protein>
    <recommendedName>
      <fullName>Cysteine proteinase inhibitor</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cystatin</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Vigna unguiculata</name>
    <name type="common">Cowpea</name>
    <dbReference type="NCBI Taxonomy" id="3917"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>fabids</taxon>
      <taxon>Fabales</taxon>
      <taxon>Fabaceae</taxon>
      <taxon>Papilionoideae</taxon>
      <taxon>50 kb inversion clade</taxon>
      <taxon>NPAAA clade</taxon>
      <taxon>indigoferoid/millettioid clade</taxon>
      <taxon>Phaseoleae</taxon>
      <taxon>Vigna</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Plant Mol. Biol." volume="23" first="215" last="219">
      <title>The resistance of cowpea seeds to bruchid beetles is not related to levels of cysteine proteinase inhibitors.</title>
      <authorList>
        <person name="Fernandes K.V."/>
        <person name="Sabelli P.A."/>
        <person name="Barrat D.H."/>
        <person name="Richardson M."/>
        <person name="Xavier-Filho J."/>
        <person name="Shewry P.R."/>
      </authorList>
      <dbReference type="PubMed" id="8219051"/>
      <dbReference type="DOI" id="10.1007/bf00021433"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="similarity">
    <text evidence="2">Belongs to the cystatin family. Phytocystatin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="Z21954">
    <property type="protein sequence ID" value="CAA79954.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S39506">
    <property type="entry name" value="S39506"/>
  </dbReference>
  <dbReference type="PDB" id="4TX4">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.95 A"/>
    <property type="chains" value="A/B=15-97"/>
  </dbReference>
  <dbReference type="PDBsum" id="4TX4"/>
  <dbReference type="AlphaFoldDB" id="Q06445"/>
  <dbReference type="SMR" id="Q06445"/>
  <dbReference type="MEROPS" id="I25.014"/>
  <dbReference type="EnsemblPlants" id="Vigun06g023000.1.v1.2">
    <property type="protein sequence ID" value="Vigun06g023000.1.v1.2"/>
    <property type="gene ID" value="Vigun06g023000.v1.2"/>
  </dbReference>
  <dbReference type="Gramene" id="Vigun06g023000.1.v1.2">
    <property type="protein sequence ID" value="Vigun06g023000.1.v1.2"/>
    <property type="gene ID" value="Vigun06g023000.v1.2"/>
  </dbReference>
  <dbReference type="OrthoDB" id="992128at2759"/>
  <dbReference type="EvolutionaryTrace" id="Q06445"/>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00042">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.450.10:FF:000011">
    <property type="entry name" value="Cysteine proteinase inhibitor"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.450.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR027214">
    <property type="entry name" value="Cystatin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000010">
    <property type="entry name" value="Cystatin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046350">
    <property type="entry name" value="Cystatin_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018073">
    <property type="entry name" value="Prot_inh_cystat_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11413">
    <property type="entry name" value="CYSTATIN FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11413:SF116">
    <property type="entry name" value="CYSTEINE PROTEINASE INHIBITOR 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF16845">
    <property type="entry name" value="SQAPI"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00043">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54403">
    <property type="entry name" value="Cystatin/monellin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00287">
    <property type="entry name" value="CYSTATIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <feature type="chain" id="PRO_0000207161" description="Cysteine proteinase inhibitor">
    <location>
      <begin position="1"/>
      <end position="97"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Secondary area of contact">
    <location>
      <begin position="49"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="site" description="Reactive site" evidence="1">
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="17"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="39"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="53"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="67"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="helix" evidence="3">
    <location>
      <begin position="79"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="strand" evidence="3">
    <location>
      <begin position="83"/>
      <end position="93"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <evidence type="ECO:0007829" key="3">
    <source>
      <dbReference type="PDB" id="4TX4"/>
    </source>
  </evidence>
  <sequence length="97" mass="10758" checksum="0D08795E734845D5" modified="1995-11-01" version="1">MAALGGNRDVAGNQNSLEIDSLARFAVEEHNKKQNALLEFGRVVSAQQQVVSGTLYTITLEAKDGGQKKVYEAKVWEKPWLNFKELQEFKHVGDAPA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>