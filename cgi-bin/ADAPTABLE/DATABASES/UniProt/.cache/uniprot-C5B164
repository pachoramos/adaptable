<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2024-07-24" modified="2024-11-27" version="98" xmlns="http://uniprot.org/uniprot">
  <accession>C5B164</accession>
  <name>LANM_METEA</name>
  <protein>
    <recommendedName>
      <fullName evidence="4 5">Lanmodulin</fullName>
      <shortName evidence="4 5">LanM</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">Lanthanide-binding protein</fullName>
      <shortName evidence="4">Ln(3+)-binding protein</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="4" type="primary">lanM</name>
    <name evidence="8" type="ordered locus">MexAM1_META1p1786</name>
  </gene>
  <organism>
    <name type="scientific">Methylorubrum extorquens (strain ATCC 14718 / DSM 1338 / JCM 2805 / NCIMB 9133 / AM1)</name>
    <name type="common">Methylobacterium extorquens</name>
    <dbReference type="NCBI Taxonomy" id="272630"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Alphaproteobacteria</taxon>
      <taxon>Hyphomicrobiales</taxon>
      <taxon>Methylobacteriaceae</taxon>
      <taxon>Methylorubrum</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="PLoS ONE" volume="4" first="E5584" last="E5584">
      <title>Methylobacterium genome sequences: a reference blueprint to investigate microbial metabolism of C1 compounds from natural and industrial sources.</title>
      <authorList>
        <person name="Vuilleumier S."/>
        <person name="Chistoserdova L."/>
        <person name="Lee M.-C."/>
        <person name="Bringel F."/>
        <person name="Lajus A."/>
        <person name="Zhou Y."/>
        <person name="Gourion B."/>
        <person name="Barbe V."/>
        <person name="Chang J."/>
        <person name="Cruveiller S."/>
        <person name="Dossat C."/>
        <person name="Gillett W."/>
        <person name="Gruffaz C."/>
        <person name="Haugen E."/>
        <person name="Hourcade E."/>
        <person name="Levy R."/>
        <person name="Mangenot S."/>
        <person name="Muller E."/>
        <person name="Nadalig T."/>
        <person name="Pagni M."/>
        <person name="Penny C."/>
        <person name="Peyraud R."/>
        <person name="Robinson D.G."/>
        <person name="Roche D."/>
        <person name="Rouy Z."/>
        <person name="Saenampechek C."/>
        <person name="Salvignol G."/>
        <person name="Vallenet D."/>
        <person name="Wu Z."/>
        <person name="Marx C.J."/>
        <person name="Vorholt J.A."/>
        <person name="Olson M.V."/>
        <person name="Kaul R."/>
        <person name="Weissenbach J."/>
        <person name="Medigue C."/>
        <person name="Lidstrom M.E."/>
      </authorList>
      <dbReference type="PubMed" id="19440302"/>
      <dbReference type="DOI" id="10.1371/journal.pone.0005584"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 14718 / DSM 1338 / JCM 2805 / NCIMB 9133 / AM1</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2018" name="J. Am. Chem. Soc." volume="140" first="15056" last="15061">
      <title>Lanmodulin: A Highly Selective Lanthanide-Binding Protein from a Lanthanide-Utilizing Bacterium.</title>
      <authorList>
        <person name="Cotruvo J.A. Jr."/>
        <person name="Featherston E.R."/>
        <person name="Mattocks J.A."/>
        <person name="Ho J.V."/>
        <person name="Laremore T.N."/>
      </authorList>
      <dbReference type="PubMed" id="30351021"/>
      <dbReference type="DOI" id="10.1021/jacs.8b09842"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBUNIT</scope>
    <scope>MUTAGENESIS OF PRO-36; PRO-60; PRO-85 AND PRO-109</scope>
    <source>
      <strain>ATCC 14718 / DSM 1338 / JCM 2805 / NCIMB 9133 / AM1</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2020" name="Inorg. Chem." volume="59" first="11855" last="11867">
      <title>Selective and Efficient Biomacromolecular Extraction of Rare-Earth Elements using Lanmodulin.</title>
      <authorList>
        <person name="Deblonde G.J."/>
        <person name="Mattocks J.A."/>
        <person name="Park D.M."/>
        <person name="Reed D.W."/>
        <person name="Cotruvo J.A. Jr."/>
        <person name="Jiao Y."/>
      </authorList>
      <dbReference type="PubMed" id="32686425"/>
      <dbReference type="DOI" id="10.1021/acs.inorgchem.0c01303"/>
    </citation>
    <scope>BIOTECHNOLOGY</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
  </reference>
  <reference evidence="9" key="4">
    <citation type="journal article" date="2019" name="Biochemistry" volume="58" first="120" last="125">
      <title>Structural Basis for Rare Earth Element Recognition by Methylobacterium extorquens Lanmodulin.</title>
      <authorList>
        <person name="Cook E.C."/>
        <person name="Featherston E.R."/>
        <person name="Showalter S.A."/>
        <person name="Cotruvo J.A."/>
      </authorList>
      <dbReference type="PubMed" id="30352145"/>
      <dbReference type="DOI" id="10.1021/acs.biochem.8b01019"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 23-133 IN COMPLEX WITH Y(3+)</scope>
    <source>
      <strain>ATCC 14718 / DSM 1338 / JCM 2805 / NCIMB 9133 / AM1</strain>
    </source>
  </reference>
  <reference evidence="10" key="5">
    <citation type="journal article" date="2023" name="Nature" volume="618" first="87" last="93">
      <title>Enhanced rare-earth separation with a metal-sensitive lanmodulin dimer.</title>
      <authorList>
        <person name="Mattocks J.A."/>
        <person name="Jung J.J."/>
        <person name="Lin C.Y."/>
        <person name="Dong Z."/>
        <person name="Yennawar N.H."/>
        <person name="Featherston E.R."/>
        <person name="Kang-Yun C.S."/>
        <person name="Hamilton T.A."/>
        <person name="Park D.M."/>
        <person name="Boal A.K."/>
        <person name="Cotruvo J.A."/>
      </authorList>
      <dbReference type="PubMed" id="37259003"/>
      <dbReference type="DOI" id="10.1038/s41586-023-05945-5"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.01 ANGSTROMS) OF 29-133 IN COMPLEX WITH ND(3+)</scope>
    <source>
      <strain>ATCC 14718 / DSM 1338 / JCM 2805 / NCIMB 9133 / AM1</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">High-affinity lanthanide (Ln)-binding protein. Shows 100 million-fold selectivity for La(3+) over Ca(2+). Binds 3 equiv of Ln(3+) with picomolar affinity and a fourth with approximately micromolar affinity. May be involved in receiving and then transporting lanthanides (such as La(3+), Nd(3+) and Sm(3+)) to a specific periplasmic destination.</text>
  </comment>
  <comment type="biophysicochemical properties">
    <phDependence>
      <text evidence="2">REE-LanM complexes are remarkably resilient under acidic conditions, retaining REE binding down to pH 2.5.</text>
    </phDependence>
    <temperatureDependence>
      <text evidence="2">REE-LanM complexes are stable over a broad temperature range (up to 95 degrees Celsius).</text>
    </temperatureDependence>
  </comment>
  <comment type="subunit">
    <text evidence="1">Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="7">Periplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="11759.574" method="MALDI" evidence="1"/>
  <comment type="biotechnology">
    <text evidence="2">Has been used for metal extraction from two distinct rare-earth elements (REEs)-containing industrial feedstocks covering a broad range of REE and non-REE concentrations, namely, precombustion coal (lignite) and electronic waste leachates. After only a single all-aqueous step, quantitative and selective recovery of the REEs from all non-REEs initially present (Li, Na, Mg, Ca, Sr, Al, Si, Mn, Fe, Co, Ni, Cu, Zn, and U) was achieved, demonstrating the universal selectivity of LanM for REEs against non-REEs and its potential application even for industrial low-grade sources.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="6">Lanthanides are essential cofactors in some pyrroloquinoline quinone (PQQ)-dependent alcohol dehydrogenase enzymes in the methylotrophic bacterium M.extorquens.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">LanM undergoes a large conformational change from a largely disordered state to a compact, ordered state, selectively in response to picomolar concentrations of all Ln(3+) (Ln = La-Lu) and Y(3+).</text>
  </comment>
  <dbReference type="EMBL" id="CP001510">
    <property type="protein sequence ID" value="ACS39628.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003601797.1">
    <property type="nucleotide sequence ID" value="NC_012808.1"/>
  </dbReference>
  <dbReference type="PDB" id="6MI5">
    <property type="method" value="NMR"/>
    <property type="chains" value="X=23-133"/>
  </dbReference>
  <dbReference type="PDB" id="8FNS">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.01 A"/>
    <property type="chains" value="A=29-133"/>
  </dbReference>
  <dbReference type="PDBsum" id="6MI5"/>
  <dbReference type="PDBsum" id="8FNS"/>
  <dbReference type="AlphaFoldDB" id="C5B164"/>
  <dbReference type="BMRB" id="C5B164"/>
  <dbReference type="SMR" id="C5B164"/>
  <dbReference type="STRING" id="272630.MexAM1_META1p1786"/>
  <dbReference type="GeneID" id="72989511"/>
  <dbReference type="KEGG" id="mea:Mex_1p1786"/>
  <dbReference type="eggNOG" id="COG5126">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_091273_5_0_5"/>
  <dbReference type="OrthoDB" id="8453759at2"/>
  <dbReference type="Proteomes" id="UP000009081">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042597">
    <property type="term" value="C:periplasmic space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.238.10">
    <property type="entry name" value="EF-hand"/>
    <property type="match status" value="2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR052591">
    <property type="entry name" value="Ca-Dep_Signaling_Protein"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR011992">
    <property type="entry name" value="EF-hand-dom_pair"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018247">
    <property type="entry name" value="EF_Hand_1_Ca_BS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002048">
    <property type="entry name" value="EF_hand_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23064:SF68">
    <property type="entry name" value="EF-HAND DOMAIN-CONTAINING PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23064">
    <property type="entry name" value="TROPONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13202">
    <property type="entry name" value="EF-hand_5"/>
    <property type="match status" value="3"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF47473">
    <property type="entry name" value="EF-hand"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00018">
    <property type="entry name" value="EF_HAND_1"/>
    <property type="match status" value="2"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50222">
    <property type="entry name" value="EF_HAND_2"/>
    <property type="match status" value="3"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0574">Periplasm</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0677">Repeat</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5002948339" description="Lanmodulin">
    <location>
      <begin position="22"/>
      <end position="133"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 1" evidence="7">
    <location>
      <begin position="35"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 2" evidence="7">
    <location>
      <begin position="59"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 3" evidence="7">
    <location>
      <begin position="84"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="domain" description="EF-hand 4" evidence="7">
    <location>
      <begin position="108"/>
      <end position="119"/>
    </location>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="35"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="37"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="39"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="41"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="46"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>1</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="59"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="61"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="63"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="65"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="70"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>2</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="84"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>3</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="86"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>3</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="88"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>3</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="90"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>3</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="95"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>3</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="108"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>4</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="110"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>4</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="112"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>4</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="114"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>4</label>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3 10">
    <location>
      <position position="119"/>
    </location>
    <ligand>
      <name>Nd(3+)</name>
      <dbReference type="ChEBI" id="CHEBI:229785"/>
      <label>4</label>
    </ligand>
  </feature>
  <feature type="mutagenesis site" description="The mutant protein shows altered metal-binding properties, it is significantly more conformationally sensitive to Ca(2+); when associated with A-60 and A-85 and A-109." evidence="1">
    <original>P</original>
    <variation>A</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="The mutant protein shows altered metal-binding properties, it is significantly more conformationally sensitive to Ca(2+); when associated with A-36 and A-85 and A-109." evidence="1">
    <original>P</original>
    <variation>A</variation>
    <location>
      <position position="60"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="The mutant protein shows altered metal-binding properties, it is significantly more conformationally sensitive to Ca(2+); when associated with A-36 and A-60 and A-109." evidence="1">
    <original>P</original>
    <variation>A</variation>
    <location>
      <position position="85"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="The mutant protein shows altered metal-binding properties, it is significantly more conformationally sensitive to Ca(2+); when associated with A-36 and A-60 and A-85." evidence="1">
    <original>P</original>
    <variation>A</variation>
    <location>
      <position position="109"/>
    </location>
  </feature>
  <feature type="helix" evidence="11">
    <location>
      <begin position="24"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="helix" evidence="12">
    <location>
      <begin position="31"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="12">
    <location>
      <begin position="39"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="helix" evidence="12">
    <location>
      <begin position="44"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="strand" evidence="12">
    <location>
      <begin position="63"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="turn" evidence="12">
    <location>
      <begin position="68"/>
      <end position="74"/>
    </location>
  </feature>
  <feature type="helix" evidence="12">
    <location>
      <begin position="77"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="strand" evidence="11">
    <location>
      <begin position="85"/>
      <end position="91"/>
    </location>
  </feature>
  <feature type="helix" evidence="12">
    <location>
      <begin position="93"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="strand" evidence="12">
    <location>
      <begin position="112"/>
      <end position="115"/>
    </location>
  </feature>
  <feature type="helix" evidence="12">
    <location>
      <begin position="117"/>
      <end position="121"/>
    </location>
  </feature>
  <feature type="helix" evidence="12">
    <location>
      <begin position="123"/>
      <end position="132"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="30351021"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="32686425"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="37259003"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="30351021"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="30352145"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="30351021"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="ACS39628.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="9">
    <source>
      <dbReference type="PDB" id="6MI5"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="10">
    <source>
      <dbReference type="PDB" id="8FNS"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="11">
    <source>
      <dbReference type="PDB" id="6MI5"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="12">
    <source>
      <dbReference type="PDB" id="8FNS"/>
    </source>
  </evidence>
  <sequence length="133" mass="13846" checksum="4DA0FAC96BB1639D" modified="2009-07-28" version="1" precursor="true">MAFRLSSAVLLAALVAAPAYAAPTTTTKVDIAAFDPDKDGTIDLKEALAAGSAAFDKLDPDKDGTLDAKELKGRVSEADLKKLDPDNDGTLDKKEYLAAVEAQFKAANPDNDGTIDARELASPAGSALVNLIR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>