<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2019-07-03" modified="2024-11-27" version="42" xmlns="http://uniprot.org/uniprot">
  <accession>E7CZZ1</accession>
  <name>SCXNC_MESEU</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Sodium channel neurotoxin MeuNaTxalpha-12</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Mesobuthus eupeus</name>
    <name type="common">Lesser Asian scorpion</name>
    <name type="synonym">Buthus eupeus</name>
    <dbReference type="NCBI Taxonomy" id="34648"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Mesobuthus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2012" name="Mol. Cell. Proteomics" volume="11" first="M111.012054" last="M111.012054">
      <title>Evolutionary diversification of Mesobuthus alpha-scorpion toxins affecting sodium channels.</title>
      <authorList>
        <person name="Zhu S."/>
        <person name="Peigneur S."/>
        <person name="Gao B."/>
        <person name="Lu X."/>
        <person name="Cao C."/>
        <person name="Tytgat J."/>
      </authorList>
      <dbReference type="PubMed" id="21969612"/>
      <dbReference type="DOI" id="10.1074/mcp.m111.012054"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Alpha toxins bind voltage-independently at site-3 of sodium channels (Nav) and inhibit the inactivation of the activated channels, thereby blocking neuronal transmission.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="5">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Alpha subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="HM989914">
    <property type="protein sequence ID" value="ADT82854.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="E7CZZ1"/>
  <dbReference type="SMR" id="E7CZZ1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="5">
    <location>
      <begin position="1" status="less than"/>
      <end position="6"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000447457" description="Sodium channel neurotoxin MeuNaTxalpha-12" evidence="6">
    <location>
      <begin position="7"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000447458" description="Removed by a carboxypeptidase" evidence="5">
    <location>
      <position position="71"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="2">
    <location>
      <begin position="8"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="18"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="22"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="28"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="7">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P86405"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="21969612"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="21969612"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="21969612"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="7">
    <source>
      <dbReference type="EMBL" id="ADT82854.1"/>
    </source>
  </evidence>
  <sequence length="71" mass="7819" checksum="BF53D96822F2E799" modified="2011-03-08" version="1" precursor="true" fragment="single">MTGVESARDAYIAQGNNCVYQCALNSSCNELCTKNGAKSGYCQWFGKHGNACWCIELPDNVPIRIQGKCQR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>