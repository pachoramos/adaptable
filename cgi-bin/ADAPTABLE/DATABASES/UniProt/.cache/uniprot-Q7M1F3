<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-03-24" modified="2023-02-22" version="61" xmlns="http://uniprot.org/uniprot">
  <accession>Q7M1F3</accession>
  <name>DEF1_AESHI</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cysteine-rich antimicrobial protein 1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin AMP1</fullName>
      <shortName>AhAMP1</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Aesculus hippocastanum</name>
    <name type="common">Horse chestnut</name>
    <dbReference type="NCBI Taxonomy" id="43364"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Sapindales</taxon>
      <taxon>Sapindaceae</taxon>
      <taxon>Hippocastanoideae</taxon>
      <taxon>Hippocastaneae</taxon>
      <taxon>Aesculus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="FEBS Lett." volume="368" first="257" last="262">
      <title>Isolation and characterisation of plant defensins from seeds of Asteraceae, Fabaceae, Hippocastanaceae and Saxifragaceae.</title>
      <authorList>
        <person name="Osborn R.W."/>
        <person name="De Samblanx G.W."/>
        <person name="Thevissen K."/>
        <person name="Goderis I."/>
        <person name="Torrekens S."/>
        <person name="Van Leuven F."/>
        <person name="Attenborough S."/>
        <person name="Rees S.B."/>
        <person name="Broekaert W.F."/>
      </authorList>
      <dbReference type="PubMed" id="7628617"/>
      <dbReference type="DOI" id="10.1016/0014-5793(95)00666-w"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <source>
      <tissue>Seed</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1999" name="Proteins" volume="37" first="388" last="403">
      <title>The three-dimensional solution structure of Aesculus hippocastanum antimicrobial protein 1 determined by 1H nuclear magnetic resonance.</title>
      <authorList>
        <person name="Fant F."/>
        <person name="Vranken W.F."/>
        <person name="Borremans F.A.M."/>
      </authorList>
      <dbReference type="PubMed" id="10591099"/>
      <dbReference type="DOI" id="10.1002/(sici)1097-0134(19991115)37:3&lt;388::aid-prot7&gt;3.3.co;2-6"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2000" name="Mol. Plant Microbe Interact." volume="13" first="54" last="61">
      <title>Specific binding sites for an antifungal plant defensin from Dahlia (Dahlia merckii) on fungal cells are required for antifungal activity.</title>
      <authorList>
        <person name="Thevissen K."/>
        <person name="Osborn R.W."/>
        <person name="Acland D.P."/>
        <person name="Broekaert W.F."/>
      </authorList>
      <dbReference type="PubMed" id="10656585"/>
      <dbReference type="DOI" id="10.1094/mpmi.2000.13.1.54"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3">Possesses antimicrobial activity sensitive to inorganic cations. Binds specifically to the fungal plasma membrane. Has no inhibitory effect on insect gut alpha-amylase.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the DEFL family.</text>
  </comment>
  <dbReference type="PIR" id="S66218">
    <property type="entry name" value="S66218"/>
  </dbReference>
  <dbReference type="PDB" id="1BK8">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-50"/>
  </dbReference>
  <dbReference type="PDBsum" id="1BK8"/>
  <dbReference type="AlphaFoldDB" id="Q7M1F3"/>
  <dbReference type="BMRB" id="Q7M1F3"/>
  <dbReference type="SMR" id="Q7M1F3"/>
  <dbReference type="EvolutionaryTrace" id="Q7M1F3"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00304">
    <property type="entry name" value="Gamma-thionin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000366950" description="Defensin-like protein 1">
    <location>
      <begin position="1"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="2"/>
      <end position="50"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="14"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="20"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="24"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="3"/>
      <end position="5"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="8"/>
      <end position="10"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="17"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="26"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="31"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="41"/>
      <end position="48"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10656585"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7628617"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0007829" key="5">
    <source>
      <dbReference type="PDB" id="1BK8"/>
    </source>
  </evidence>
  <sequence length="50" mass="5863" checksum="A87D5BF8752BB560" modified="2003-12-15" version="1">LCNERPSQTWSGNCGNTAHCDKQCQDWEKASHGACHKRENHWKCFCYFNC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>