<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1990-11-01" modified="2022-05-25" version="81" xmlns="http://uniprot.org/uniprot">
  <accession>P18684</accession>
  <name>DIPD_PROTE</name>
  <protein>
    <recommendedName>
      <fullName>Diptericin-D</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Protophormia terraenovae</name>
    <name type="common">Northern blowfly</name>
    <name type="synonym">Lucilia terraenovae</name>
    <dbReference type="NCBI Taxonomy" id="34676"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Oestroidea</taxon>
      <taxon>Calliphoridae</taxon>
      <taxon>Chrysomyinae</taxon>
      <taxon>Protophormia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1989" name="Eur. J. Biochem." volume="182" first="423" last="427">
      <title>Insect immunity. Isolation of cDNA clones corresponding to diptericin, an inducible antibacterial peptide from Phormia terranovae (Diptera). Transcriptional profiles during immunization.</title>
      <authorList>
        <person name="Reichhart J.-M."/>
        <person name="Essrich M."/>
        <person name="Dimarcq J.-L."/>
        <person name="Hoffmann D."/>
        <person name="Hoffmann J.A."/>
        <person name="Lagueux M."/>
      </authorList>
      <dbReference type="PubMed" id="2544427"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1989.tb14848.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>INDUCTION</scope>
    <source>
      <tissue>Larval fat body</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1995" name="Biochemistry" volume="34" first="7394" last="7400">
      <title>Insect immunity. The inducible antibacterial peptide diptericin carries two O-glycans necessary for biological activity.</title>
      <authorList>
        <person name="Bulet P."/>
        <person name="Hegy G."/>
        <person name="Lambert J."/>
        <person name="van Dorsselaer A."/>
        <person name="Hoffmann J.A."/>
        <person name="Hetru C."/>
      </authorList>
      <dbReference type="PubMed" id="7779781"/>
      <dbReference type="DOI" id="10.1021/bi00022a012"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 19-100</scope>
    <scope>GLYCOSYLATION AT THR-28 AND THR-72</scope>
    <scope>AMIDATION AT PHE-100</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1997" name="Anal. Biochem." volume="247" first="366" last="375">
      <title>A matrix-assisted laser desorption ionization time-of-flight mass spectrometry approach to identify the origin of the glycan heterogeneity of diptericin, an O-glycosylated antibacterial peptide from insects.</title>
      <authorList>
        <person name="Uttenweiler-Joseph S."/>
        <person name="Moniatte M."/>
        <person name="Lambert J."/>
        <person name="van Dorsselaer A."/>
        <person name="Bulet P."/>
      </authorList>
      <dbReference type="PubMed" id="9177700"/>
      <dbReference type="DOI" id="10.1006/abio.1997.2083"/>
    </citation>
    <scope>GLYCOSYLATION AT THR-28 AND THR-72</scope>
  </reference>
  <comment type="function">
    <text>Has activity against E.coli.</text>
  </comment>
  <comment type="induction">
    <text evidence="1">By bacterial infection.</text>
  </comment>
  <comment type="miscellaneous">
    <text>There seems to be a family of diptericin in protophormia. Diptericin A is the predominant member.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the attacin/sarcotoxin-2 family.</text>
  </comment>
  <dbReference type="EMBL" id="X15851">
    <property type="protein sequence ID" value="CAB57822.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S04634">
    <property type="entry name" value="S04634"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P18684"/>
  <dbReference type="GlyConnect" id="121">
    <property type="glycosylation" value="3 O-Linked glycans (2 sites)"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000004907" description="Diptericin-D">
    <location>
      <begin position="19"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="2">
    <location>
      <position position="100"/>
    </location>
  </feature>
  <feature type="glycosylation site" id="CAR_000126" description="O-linked (GalNAc...) threonine" evidence="2 3">
    <location>
      <position position="28"/>
    </location>
  </feature>
  <feature type="glycosylation site" id="CAR_000127" description="O-linked (GalNAc...) threonine" evidence="2 3">
    <location>
      <position position="72"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>I</original>
    <variation>L</variation>
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="sequence variant">
    <original>P</original>
    <variation>T</variation>
    <location>
      <position position="35"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="2544427"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="7779781"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="9177700"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="101" mass="10731" checksum="60E4DE34DF0E8C5A" modified="1990-11-01" version="1" precursor="true">MKLFYLLVICALSLAVMADEKPKLILPTPAPPNLPQLVGGGGGNRKDGFGVSVDAHQKVWTSDNGRHSIGVTPGYSQHLGGPYGNSRPDYRIGAGYSYNFG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>