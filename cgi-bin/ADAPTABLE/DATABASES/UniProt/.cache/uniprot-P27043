<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1992-08-01" modified="2024-11-27" version="153" xmlns="http://uniprot.org/uniprot">
  <accession>P27043</accession>
  <name>ANGI_CHICK</name>
  <protein>
    <recommendedName>
      <fullName>Angiogenin</fullName>
      <ecNumber evidence="1">3.1.27.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Ribonuclease A</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ANG</name>
  </gene>
  <organism>
    <name type="scientific">Gallus gallus</name>
    <name type="common">Chicken</name>
    <dbReference type="NCBI Taxonomy" id="9031"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Archelosauria</taxon>
      <taxon>Archosauria</taxon>
      <taxon>Dinosauria</taxon>
      <taxon>Saurischia</taxon>
      <taxon>Theropoda</taxon>
      <taxon>Coelurosauria</taxon>
      <taxon>Aves</taxon>
      <taxon>Neognathae</taxon>
      <taxon>Galloanserae</taxon>
      <taxon>Galliformes</taxon>
      <taxon>Phasianidae</taxon>
      <taxon>Phasianinae</taxon>
      <taxon>Gallus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1992" name="Oncogene" volume="7" first="527" last="534">
      <title>Identification of genes differentially expressed in two types of v-myb-transformed avian myelomonocytic cells.</title>
      <authorList>
        <person name="Nakano T."/>
        <person name="Graf T."/>
      </authorList>
      <dbReference type="PubMed" id="1549365"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA / MRNA]</scope>
    <source>
      <strain>White leghorn</strain>
      <tissue>Bone marrow</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 2">Secreted ribonuclease that can either promote or restrict cell proliferation of target cells, depending on the context. Endocytosed in target cells via its receptor PLXNB2 and translocates to the cytoplasm or nucleus. Under stress conditions, localizes to the cytoplasm and promotes the assembly of stress granules (SGs): specifically cleaves a subset of tRNAs within anticodon loops to produce tRNA-derived stress-induced fragments (tiRNAs), resulting in translation repression and inhibition of cell proliferation (By similarity). tiRNas also prevent formation of apoptosome, thereby promoting cell survival (By similarity). Preferentially cleaves RNAs between a pyrimidine and an adenosine residue, suggesting that it cleaves the anticodon loop of tRNA(Ala) (32-UUAGCAU-38) after positions 33 and 36. Cleaves a subset of tRNAs, including tRNA(Ala), tRNA(Glu), tRNA(Gly), tRNA(Lys), tRNA(Val), tRNA(His), tRNA(Asp) and tRNA(Sec). Under growth conditions and in differentiated cells, translocates to the nucleus and stimulates ribosomal RNA (rRNA) transcription, including that containing the initiation site sequences of 45S rRNA, thereby promoting cell growth and proliferation. Angiogenin induces vascularization of normal and malignant tissues via its ability to promote rRNA transcription (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer. Interacts with RNH1; inhibiting ANG ribonuclease activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Nucleus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Nucleus</location>
      <location evidence="1">Nucleolus</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
      <location evidence="1">Stress granule</location>
    </subcellularLocation>
    <text evidence="1">The secreted protein is rapidly endocytosed by target cells following interaction with PLXNB2 receptor and translocated to the cytoplasm and nucleus. In the nucleus, accumulates in the nucleolus and binds to DNA.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the pancreatic ribonuclease family.</text>
  </comment>
  <dbReference type="EC" id="3.1.27.-" evidence="1"/>
  <dbReference type="EMBL" id="X61192">
    <property type="protein sequence ID" value="CAA43494.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X61193">
    <property type="protein sequence ID" value="CAA43495.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="I50677">
    <property type="entry name" value="I50677"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_990590.1">
    <property type="nucleotide sequence ID" value="NM_205259.2"/>
  </dbReference>
  <dbReference type="PDB" id="4PER">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.92 A"/>
    <property type="chains" value="B=25-137"/>
  </dbReference>
  <dbReference type="PDBsum" id="4PER"/>
  <dbReference type="AlphaFoldDB" id="P27043"/>
  <dbReference type="SMR" id="P27043"/>
  <dbReference type="STRING" id="9031.ENSGALP00000040224"/>
  <dbReference type="PaxDb" id="9031-ENSGALP00000040224"/>
  <dbReference type="Ensembl" id="ENSGALT00000041019">
    <property type="protein sequence ID" value="ENSGALP00000040224"/>
    <property type="gene ID" value="ENSGALG00000003196"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00000087312">
    <property type="protein sequence ID" value="ENSGALP00000062296"/>
    <property type="gene ID" value="ENSGALG00000003196"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00000131269">
    <property type="protein sequence ID" value="ENSGALP00000078211"/>
    <property type="gene ID" value="ENSGALG00000003196"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00000143339">
    <property type="protein sequence ID" value="ENSGALP00000086924"/>
    <property type="gene ID" value="ENSGALG00000003196"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00000153852">
    <property type="protein sequence ID" value="ENSGALP00000089550"/>
    <property type="gene ID" value="ENSGALG00000003196"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00010027349.1">
    <property type="protein sequence ID" value="ENSGALP00010015603.1"/>
    <property type="gene ID" value="ENSGALG00010011433.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00010027351.1">
    <property type="protein sequence ID" value="ENSGALP00010015605.1"/>
    <property type="gene ID" value="ENSGALG00010011433.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00010027352.1">
    <property type="protein sequence ID" value="ENSGALP00010015606.1"/>
    <property type="gene ID" value="ENSGALG00010011433.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00010027353.1">
    <property type="protein sequence ID" value="ENSGALP00010015607.1"/>
    <property type="gene ID" value="ENSGALG00010011433.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00010027355.1">
    <property type="protein sequence ID" value="ENSGALP00010015609.1"/>
    <property type="gene ID" value="ENSGALG00010011433.1"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00015012803">
    <property type="protein sequence ID" value="ENSGALP00015006676"/>
    <property type="gene ID" value="ENSGALG00015005475"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00015012805">
    <property type="protein sequence ID" value="ENSGALP00015006677"/>
    <property type="gene ID" value="ENSGALG00015005475"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00015012808">
    <property type="protein sequence ID" value="ENSGALP00015006678"/>
    <property type="gene ID" value="ENSGALG00015005475"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00015012809">
    <property type="protein sequence ID" value="ENSGALP00015006679"/>
    <property type="gene ID" value="ENSGALG00015005475"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSGALT00015012810">
    <property type="protein sequence ID" value="ENSGALP00015006680"/>
    <property type="gene ID" value="ENSGALG00015005475"/>
  </dbReference>
  <dbReference type="GeneID" id="396194"/>
  <dbReference type="KEGG" id="gga:396194"/>
  <dbReference type="CTD" id="6039"/>
  <dbReference type="VEuPathDB" id="HostDB:geneid_396194"/>
  <dbReference type="eggNOG" id="ENOG502T1FH">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000166697"/>
  <dbReference type="HOGENOM" id="CLU_117006_3_1_1"/>
  <dbReference type="InParanoid" id="P27043"/>
  <dbReference type="OMA" id="MMHRRRM"/>
  <dbReference type="OrthoDB" id="4612546at2759"/>
  <dbReference type="PhylomeDB" id="P27043"/>
  <dbReference type="TreeFam" id="TF333393"/>
  <dbReference type="Reactome" id="R-GGA-418990">
    <property type="pathway name" value="Adherens junctions interactions"/>
  </dbReference>
  <dbReference type="Reactome" id="R-GGA-6798695">
    <property type="pathway name" value="Neutrophil degranulation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-GGA-6803157">
    <property type="pathway name" value="Antimicrobial peptides"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P27043"/>
  <dbReference type="PRO" id="PR:P27043"/>
  <dbReference type="Proteomes" id="UP000000539">
    <property type="component" value="Chromosome 6"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSGALG00000003196">
    <property type="expression patterns" value="Expressed in spleen and 10 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="AgBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030139">
    <property type="term" value="C:endocytic vesicle"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005730">
    <property type="term" value="C:nucleolus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004519">
    <property type="term" value="F:endonuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003676">
    <property type="term" value="F:nucleic acid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042803">
    <property type="term" value="F:protein homodimerization activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004540">
    <property type="term" value="F:RNA nuclease activity"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="AgBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004549">
    <property type="term" value="F:tRNA-specific ribonuclease activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001525">
    <property type="term" value="P:angiogenesis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0071425">
    <property type="term" value="P:hematopoietic stem cell proliferation"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043066">
    <property type="term" value="P:negative regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032055">
    <property type="term" value="P:negative regulation of translation in response to stress"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034063">
    <property type="term" value="P:stress granule assembly"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd06265">
    <property type="entry name" value="RNase_A_canonical"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.130.10:FF:000006">
    <property type="entry name" value="Ribonuclease homolog"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.130.10">
    <property type="entry name" value="Ribonuclease A-like domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001427">
    <property type="entry name" value="RNaseA"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036816">
    <property type="entry name" value="RNaseA-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023411">
    <property type="entry name" value="RNaseA_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023412">
    <property type="entry name" value="RNaseA_domain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437:SF59">
    <property type="entry name" value="ANGIOGENIN-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11437">
    <property type="entry name" value="RIBONUCLEASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00074">
    <property type="entry name" value="RnaseA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00794">
    <property type="entry name" value="RIBONUCLEASE"/>
  </dbReference>
  <dbReference type="SMART" id="SM00092">
    <property type="entry name" value="RNAse_Pc"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54076">
    <property type="entry name" value="RNase A-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00127">
    <property type="entry name" value="RNASE_PANCREATIC"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0037">Angiogenesis</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0217">Developmental protein</keyword>
  <keyword id="KW-0221">Differentiation</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0255">Endonuclease</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-0652">Protein synthesis inhibitor</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0346">Stress response</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000030855" description="Angiogenin">
    <location>
      <begin position="22"/>
      <end position="139"/>
    </location>
  </feature>
  <feature type="active site" description="Proton acceptor" evidence="1">
    <location>
      <position position="34"/>
    </location>
  </feature>
  <feature type="active site" description="Proton donor" evidence="1">
    <location>
      <position position="133"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="102"/>
    </location>
    <ligand>
      <name>tRNA</name>
      <dbReference type="ChEBI" id="CHEBI:17843"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="122"/>
    </location>
    <ligand>
      <name>tRNA</name>
      <dbReference type="ChEBI" id="CHEBI:17843"/>
    </ligand>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="49"/>
      <end position="102"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="64"/>
      <end position="111"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="82"/>
      <end position="126"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="27"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="45"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="66"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="75"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="90"/>
      <end position="92"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="97"/>
      <end position="106"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="108"/>
      <end position="110"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="112"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="123"/>
      <end position="127"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="130"/>
      <end position="134"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P03950"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P21570"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="4PER"/>
    </source>
  </evidence>
  <sequence length="139" mass="15767" checksum="F881E9F10D726F84" modified="1992-08-01" version="1" precursor="true">MAMSSLWWTAILLLALTVSMCYGVPTYQDFLRTHVDFPKTSFPNIAAYCNVMMVRRGINVHGRCKSLNTFVHTDPRNLNTLCINQPNRALRTTQQQLPVTDCKLIRSHPTCSYTGNQFNHRVRVGCWGGLPVHLDGTFP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>