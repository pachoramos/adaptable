<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-01-09" modified="2024-11-27" version="54" xmlns="http://uniprot.org/uniprot">
  <accession>P0DKS7</accession>
  <accession>Q6M898</accession>
  <accession>Q8NTP3</accession>
  <name>ARSC2_CORGL</name>
  <protein>
    <recommendedName>
      <fullName>Arsenate-mycothiol transferase ArsC2</fullName>
      <ecNumber>2.8.4.2</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Mycothiol-dependent arsenate reductase ArsC2</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">arsC2</name>
    <name type="synonym">arsX</name>
    <name type="ordered locus">cg0319</name>
    <name type="ordered locus">Cgl0263</name>
  </gene>
  <organism>
    <name type="scientific">Corynebacterium glutamicum (strain ATCC 13032 / DSM 20300 / JCM 1318 / BCRC 11384 / CCUG 27702 / LMG 3730 / NBRC 12168 / NCIMB 10025 / NRRL B-2784 / 534)</name>
    <dbReference type="NCBI Taxonomy" id="196627"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Mycobacteriales</taxon>
      <taxon>Corynebacteriaceae</taxon>
      <taxon>Corynebacterium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2003" name="Appl. Microbiol. Biotechnol." volume="62" first="99" last="109">
      <title>The Corynebacterium glutamicum genome: features and impacts on biotechnological processes.</title>
      <authorList>
        <person name="Ikeda M."/>
        <person name="Nakagawa S."/>
      </authorList>
      <dbReference type="PubMed" id="12743753"/>
      <dbReference type="DOI" id="10.1007/s00253-003-1328-1"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 13032 / DSM 20300 / JCM 1318 / BCRC 11384 / CCUG 27702 / LMG 3730 / NBRC 12168 / NCIMB 10025 / NRRL B-2784 / 534</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2003" name="J. Biotechnol." volume="104" first="5" last="25">
      <title>The complete Corynebacterium glutamicum ATCC 13032 genome sequence and its impact on the production of L-aspartate-derived amino acids and vitamins.</title>
      <authorList>
        <person name="Kalinowski J."/>
        <person name="Bathe B."/>
        <person name="Bartels D."/>
        <person name="Bischoff N."/>
        <person name="Bott M."/>
        <person name="Burkovski A."/>
        <person name="Dusch N."/>
        <person name="Eggeling L."/>
        <person name="Eikmanns B.J."/>
        <person name="Gaigalat L."/>
        <person name="Goesmann A."/>
        <person name="Hartmann M."/>
        <person name="Huthmacher K."/>
        <person name="Kraemer R."/>
        <person name="Linke B."/>
        <person name="McHardy A.C."/>
        <person name="Meyer F."/>
        <person name="Moeckel B."/>
        <person name="Pfefferle W."/>
        <person name="Puehler A."/>
        <person name="Rey D.A."/>
        <person name="Rueckert C."/>
        <person name="Rupp O."/>
        <person name="Sahm H."/>
        <person name="Wendisch V.F."/>
        <person name="Wiegraebe I."/>
        <person name="Tauch A."/>
      </authorList>
      <dbReference type="PubMed" id="12948626"/>
      <dbReference type="DOI" id="10.1016/s0168-1656(03)00154-8"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 13032 / DSM 20300 / JCM 1318 / BCRC 11384 / CCUG 27702 / LMG 3730 / NBRC 12168 / NCIMB 10025 / NRRL B-2784 / 534</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2005" name="Appl. Environ. Microbiol." volume="71" first="6206" last="6215">
      <title>Analysis of genes involved in arsenic resistance in Corynebacterium glutamicum ATCC 13032.</title>
      <authorList>
        <person name="Ordonez E."/>
        <person name="Letek M."/>
        <person name="Valbuena N."/>
        <person name="Gil J.A."/>
        <person name="Mateos L.M."/>
      </authorList>
      <dbReference type="PubMed" id="16204540"/>
      <dbReference type="DOI" id="10.1128/aem.71.10.6206-6215.2005"/>
    </citation>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2009" name="J. Biol. Chem." volume="284" first="15107" last="15116">
      <title>Arsenate reductase, mycothiol, and mycoredoxin concert thiol/disulfide exchange.</title>
      <authorList>
        <person name="Ordonez E."/>
        <person name="Van Belle K."/>
        <person name="Roos G."/>
        <person name="De Galan S."/>
        <person name="Letek M."/>
        <person name="Gil J.A."/>
        <person name="Wyns L."/>
        <person name="Mateos L.M."/>
        <person name="Messens J."/>
      </authorList>
      <dbReference type="PubMed" id="19286650"/>
      <dbReference type="DOI" id="10.1074/jbc.m900877200"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>CATALYTIC ACTIVITY</scope>
    <scope>BIOPHYSICOCHEMICAL PROPERTIES</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2011" name="Mol. Microbiol." volume="82" first="998" last="1014">
      <title>Corynebacterium glutamicum survives arsenic stress with arsenate reductases coupled to two distinct redox mechanisms.</title>
      <authorList>
        <person name="Villadangos A.F."/>
        <person name="Van Belle K."/>
        <person name="Wahni K."/>
        <person name="Dufe V.T."/>
        <person name="Freitas S."/>
        <person name="Nur H."/>
        <person name="De Galan S."/>
        <person name="Gil J.A."/>
        <person name="Collet J.F."/>
        <person name="Mateos L.M."/>
        <person name="Messens J."/>
      </authorList>
      <dbReference type="PubMed" id="22032722"/>
      <dbReference type="DOI" id="10.1111/j.1365-2958.2011.07882.x"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.72 ANGSTROMS)</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Involved in defense against toxic arsenate. Involved in the mycothiol/myoredoxin redox pathway which uses a mycothioltransferase mechanism; facilitates adduct formation between arsenate and mycothiol.</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="2">
      <text>mycothiol + arsenate = arseno-mycothiol + H2O</text>
      <dbReference type="Rhea" id="RHEA:27349"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:16768"/>
      <dbReference type="ChEBI" id="CHEBI:48597"/>
      <dbReference type="ChEBI" id="CHEBI:59655"/>
      <dbReference type="EC" id="2.8.4.2"/>
    </reaction>
  </comment>
  <comment type="biophysicochemical properties">
    <kinetics>
      <KM evidence="2">142 mM for arsenate in the presence of mycoredoxin 1, mycothiol, mycothione reductase and NADPH</KM>
    </kinetics>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="induction">
    <text evidence="1">By arsenite.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the low molecular weight phosphotyrosine protein phosphatase family.</text>
  </comment>
  <dbReference type="EC" id="2.8.4.2"/>
  <dbReference type="EMBL" id="BA000036">
    <property type="protein sequence ID" value="BAB97656.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BX927148">
    <property type="protein sequence ID" value="CAF18834.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_599516.1">
    <property type="nucleotide sequence ID" value="NC_003450.3"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_003863342.1">
    <property type="nucleotide sequence ID" value="NC_006958.1"/>
  </dbReference>
  <dbReference type="PDB" id="3RH0">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.72 A"/>
    <property type="chains" value="A/B=1-129"/>
  </dbReference>
  <dbReference type="PDBsum" id="3RH0"/>
  <dbReference type="AlphaFoldDB" id="P0DKS7"/>
  <dbReference type="SMR" id="P0DKS7"/>
  <dbReference type="STRING" id="196627.cg0319"/>
  <dbReference type="KEGG" id="cgb:cg0319"/>
  <dbReference type="KEGG" id="cgl:Cgl0263"/>
  <dbReference type="PATRIC" id="fig|196627.13.peg.267"/>
  <dbReference type="eggNOG" id="COG0394">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_071415_3_3_11"/>
  <dbReference type="OrthoDB" id="9799372at2"/>
  <dbReference type="BioCyc" id="CORYNE:G18NG-11093-MONOMER"/>
  <dbReference type="BRENDA" id="2.8.4.2">
    <property type="organism ID" value="960"/>
  </dbReference>
  <dbReference type="SABIO-RK" id="P0DKS7"/>
  <dbReference type="EvolutionaryTrace" id="P0DKS7"/>
  <dbReference type="Proteomes" id="UP000000582">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000001009">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0102100">
    <property type="term" value="F:mycothiol-arsenate ligase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-EC"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046685">
    <property type="term" value="P:response to arsenic-containing substance"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.50.2300">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023485">
    <property type="entry name" value="Ptyr_pPase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036196">
    <property type="entry name" value="Ptyr_pPase_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR43428">
    <property type="entry name" value="ARSENATE REDUCTASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR43428:SF1">
    <property type="entry name" value="ARSENATE REDUCTASE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01451">
    <property type="entry name" value="LMWPc"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00226">
    <property type="entry name" value="LMWPc"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF52788">
    <property type="entry name" value="Phosphotyrosine protein phosphatases I"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0059">Arsenical resistance</keyword>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0808">Transferase</keyword>
  <feature type="chain" id="PRO_0000418725" description="Arsenate-mycothiol transferase ArsC2">
    <location>
      <begin position="1"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="3"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="14"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="30"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="45"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="68"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="75"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="strand" evidence="4">
    <location>
      <begin position="94"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="helix" evidence="4">
    <location>
      <begin position="108"/>
      <end position="127"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="16204540"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19286650"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0007829" key="4">
    <source>
      <dbReference type="PDB" id="3RH0"/>
    </source>
  </evidence>
  <sequence length="129" mass="13763" checksum="9279EF21FF0E52B8" modified="2013-01-09" version="1">MKSVLFVCVGNGGKSQMAAALAQKYASDSVEIHSAGTKPAQGLNQLSVESIAEVGADMSQGIPKAIDPELLRTVDRVVILGDDAQVDMPESAQGALERWSIEEPDAQGMERMRIVRDQIDNRVQALLAG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>