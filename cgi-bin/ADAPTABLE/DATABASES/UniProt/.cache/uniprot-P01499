<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-07-24" version="111" xmlns="http://uniprot.org/uniprot">
  <accession>P01499</accession>
  <name>MCDP_APIME</name>
  <protein>
    <recommendedName>
      <fullName>Mast cell degranulating peptide</fullName>
      <shortName>MCD peptide</shortName>
      <shortName>MCDP</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Peptide 401</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Apis mellifera</name>
    <name type="common">Honeybee</name>
    <dbReference type="NCBI Taxonomy" id="7460"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Hymenoptera</taxon>
      <taxon>Apocrita</taxon>
      <taxon>Aculeata</taxon>
      <taxon>Apoidea</taxon>
      <taxon>Anthophila</taxon>
      <taxon>Apidae</taxon>
      <taxon>Apis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="J. Biol. Chem." volume="270" first="12704" last="12708">
      <title>The precursors of the bee venom constituents apamin and MCD peptide are encoded by two genes in tandem which share the same 3'-exon.</title>
      <authorList>
        <person name="Gmachl M."/>
        <person name="Kreil G."/>
      </authorList>
      <dbReference type="PubMed" id="7759523"/>
      <dbReference type="DOI" id="10.1074/jbc.270.21.12704"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>AMIDATION AT ASN-49</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1969" name="Hoppe-Seyler's Z. Physiol. Chem." volume="350" first="536" last="546">
      <title>Amino acid sequence of MCD-peptide, a specific mast cell-degranulating peptide from bee venom.</title>
      <authorList>
        <person name="Haux P."/>
      </authorList>
      <dbReference type="PubMed" id="5789872"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 28-49</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1978" name="J. Chem. Soc. Perkin Trans. I" volume="12" first="1157" last="1160">
      <title>Identification by mass spectrometry of N(epsilon)-formyl-lysine.</title>
      <authorList>
        <person name="Doonan S."/>
        <person name="Garman A.J."/>
        <person name="Hanson J.M."/>
        <person name="Loudon A.G."/>
        <person name="Vernon C.A."/>
      </authorList>
    </citation>
    <scope>PARTIAL PROTEIN SEQUENCE</scope>
    <scope>FORMYLATION AT LYS-29; LYS-44 AND LYS-48</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1973" name="Nature" volume="245" first="163" last="164">
      <title>An anti-inflammatory peptide from bee venom.</title>
      <authorList>
        <person name="Billingham M.E.J."/>
        <person name="Morley J."/>
        <person name="Hanson J.M."/>
        <person name="Shipolini R.A."/>
        <person name="Vernon C.A."/>
      </authorList>
      <dbReference type="PubMed" id="4582672"/>
      <dbReference type="DOI" id="10.1038/245163a0"/>
    </citation>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1990" name="J. Pharm. Pharmacol." volume="42" first="457" last="461">
      <title>Mast cell degranulating peptide: a multi-functional neurotoxin.</title>
      <authorList>
        <person name="Ziai M.R."/>
        <person name="Russek S."/>
        <person name="Wang H.-C."/>
        <person name="Beer B."/>
        <person name="Blume A.J."/>
      </authorList>
      <dbReference type="PubMed" id="1703229"/>
      <dbReference type="DOI" id="10.1111/j.2042-7158.1990.tb06595.x"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text>Potent anti-inflammatory agent. At low concentrations, mediates the degranulation of mast cells thus evoking an inflammatory response. Also acts as a neurotoxin capable of blocking a class of voltage-gated potassium channels.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <dbReference type="EMBL" id="S78459">
    <property type="protein sequence ID" value="AAB34403.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="B56710">
    <property type="entry name" value="MDHB"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001011611.2">
    <property type="nucleotide sequence ID" value="NM_001011611.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01499"/>
  <dbReference type="STRING" id="7460.P01499"/>
  <dbReference type="PaxDb" id="7460-GB40696-PA"/>
  <dbReference type="GeneID" id="406134"/>
  <dbReference type="KEGG" id="ame:406134"/>
  <dbReference type="CTD" id="406134"/>
  <dbReference type="InParanoid" id="P01499"/>
  <dbReference type="OrthoDB" id="5607909at2759"/>
  <dbReference type="Proteomes" id="UP000005203">
    <property type="component" value="Linkage group LG12"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP001105180">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015459">
    <property type="term" value="F:potassium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043303">
    <property type="term" value="P:mast cell degranulation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR035361">
    <property type="entry name" value="Bee_toxin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17454">
    <property type="entry name" value="Bee_toxin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0291">Formylation</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0467">Mast cell degranulation</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0632">Potassium channel impairing toxin</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000018613" description="Mast cell degranulating peptide" evidence="2">
    <location>
      <begin position="28"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-formyllysine; partial" evidence="4">
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-formyllysine; partial" evidence="4">
    <location>
      <position position="44"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-formyllysine; partial" evidence="4">
    <location>
      <position position="48"/>
    </location>
  </feature>
  <feature type="modified residue" description="Asparagine amide" evidence="3">
    <location>
      <position position="49"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="30"/>
      <end position="42"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="46"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="4582672"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="5789872"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="7759523"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source ref="3"/>
  </evidence>
  <sequence length="50" mass="5781" checksum="FB9A61F8A68B17AA" modified="1996-10-01" version="2" precursor="true">MISMLRCTFFFLSVILITSYFVTPTMSIKCNCKRHVIKPHICRKICGKNG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>