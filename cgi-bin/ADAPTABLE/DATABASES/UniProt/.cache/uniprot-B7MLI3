<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-04-14" modified="2024-11-27" version="85" xmlns="http://uniprot.org/uniprot">
  <accession>B7MLI3</accession>
  <name>NSRR_ECO45</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">HTH-type transcriptional repressor NsrR</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">nsrR</name>
    <name type="ordered locus">ECS88_4764</name>
  </gene>
  <organism>
    <name type="scientific">Escherichia coli O45:K1 (strain S88 / ExPEC)</name>
    <dbReference type="NCBI Taxonomy" id="585035"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Escherichia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="PLoS Genet." volume="5" first="E1000344" last="E1000344">
      <title>Organised genome dynamics in the Escherichia coli species results in highly diverse adaptive paths.</title>
      <authorList>
        <person name="Touchon M."/>
        <person name="Hoede C."/>
        <person name="Tenaillon O."/>
        <person name="Barbe V."/>
        <person name="Baeriswyl S."/>
        <person name="Bidet P."/>
        <person name="Bingen E."/>
        <person name="Bonacorsi S."/>
        <person name="Bouchier C."/>
        <person name="Bouvet O."/>
        <person name="Calteau A."/>
        <person name="Chiapello H."/>
        <person name="Clermont O."/>
        <person name="Cruveiller S."/>
        <person name="Danchin A."/>
        <person name="Diard M."/>
        <person name="Dossat C."/>
        <person name="Karoui M.E."/>
        <person name="Frapy E."/>
        <person name="Garry L."/>
        <person name="Ghigo J.M."/>
        <person name="Gilles A.M."/>
        <person name="Johnson J."/>
        <person name="Le Bouguenec C."/>
        <person name="Lescat M."/>
        <person name="Mangenot S."/>
        <person name="Martinez-Jehanne V."/>
        <person name="Matic I."/>
        <person name="Nassif X."/>
        <person name="Oztas S."/>
        <person name="Petit M.A."/>
        <person name="Pichon C."/>
        <person name="Rouy Z."/>
        <person name="Ruf C.S."/>
        <person name="Schneider D."/>
        <person name="Tourret J."/>
        <person name="Vacherie B."/>
        <person name="Vallenet D."/>
        <person name="Medigue C."/>
        <person name="Rocha E.P.C."/>
        <person name="Denamur E."/>
      </authorList>
      <dbReference type="PubMed" id="19165319"/>
      <dbReference type="DOI" id="10.1371/journal.pgen.1000344"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>S88 / ExPEC</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Nitric oxide-sensitive repressor of genes involved in protecting the cell against nitrosative stress. May require iron for activity.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="1">
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </cofactor>
    <text evidence="1">Binds 1 [2Fe-2S] cluster per subunit.</text>
  </comment>
  <dbReference type="EMBL" id="CU928161">
    <property type="protein sequence ID" value="CAR05913.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_001177639.1">
    <property type="nucleotide sequence ID" value="NC_011742.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B7MLI3"/>
  <dbReference type="SMR" id="B7MLI3"/>
  <dbReference type="GeneID" id="89519165"/>
  <dbReference type="KEGG" id="ecz:ECS88_4764"/>
  <dbReference type="HOGENOM" id="CLU_107144_2_1_6"/>
  <dbReference type="Proteomes" id="UP000000747">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051537">
    <property type="term" value="F:2 iron, 2 sulfur cluster binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003700">
    <property type="term" value="F:DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003690">
    <property type="term" value="F:double-stranded DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005506">
    <property type="term" value="F:iron ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045892">
    <property type="term" value="P:negative regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.10.10:FF:000105">
    <property type="entry name" value="HTH-type transcriptional repressor NsrR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.10.10">
    <property type="entry name" value="Winged helix-like DNA-binding domain superfamily/Winged helix DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01177">
    <property type="entry name" value="HTH_type_NsrR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR030489">
    <property type="entry name" value="TR_Rrf2-type_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000944">
    <property type="entry name" value="Tscrpt_reg_Rrf2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023761">
    <property type="entry name" value="Tscrpt_rep_HTH_NsrR"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036388">
    <property type="entry name" value="WH-like_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036390">
    <property type="entry name" value="WH_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00738">
    <property type="entry name" value="rrf2_super"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33221:SF4">
    <property type="entry name" value="HTH-TYPE TRANSCRIPTIONAL REPRESSOR NSRR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33221">
    <property type="entry name" value="WINGED HELIX-TURN-HELIX TRANSCRIPTIONAL REGULATOR, RRF2 FAMILY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02082">
    <property type="entry name" value="Rrf2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46785">
    <property type="entry name" value="Winged helix' DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01332">
    <property type="entry name" value="HTH_RRF2_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51197">
    <property type="entry name" value="HTH_RRF2_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0001">2Fe-2S</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0408">Iron</keyword>
  <keyword id="KW-0411">Iron-sulfur</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0678">Repressor</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_1000138115" description="HTH-type transcriptional repressor NsrR">
    <location>
      <begin position="1"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="domain" description="HTH rrf2-type" evidence="1">
    <location>
      <begin position="2"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="DNA-binding region" description="H-T-H motif" evidence="1">
    <location>
      <begin position="28"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="91"/>
    </location>
    <ligand>
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="96"/>
    </location>
    <ligand>
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="102"/>
    </location>
    <ligand>
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </ligand>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01177"/>
    </source>
  </evidence>
  <sequence length="141" mass="15593" checksum="6A783840023134D7" modified="2009-03-24" version="1">MQLTSFTDYGLRALIYMASLPEGRMTSISEVTDVYGVSRNHMVKIINQLSRAGYVTAVRGKNGGIRLGKPASAIRIGDVVRELEPLSLVNCSSEFCHITPACRLKQALSKAVQSFLTELDNYTLADLVEENQPLYKLLLVE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>