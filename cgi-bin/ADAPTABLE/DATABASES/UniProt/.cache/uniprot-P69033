<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-02-01" modified="2022-05-25" version="28" xmlns="http://uniprot.org/uniprot">
  <accession>P69033</accession>
  <accession>P56235</accession>
  <accession>P82117</accession>
  <name>CR23_RANSP</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Splendipherin</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ranoidea splendida</name>
    <name type="common">Magnificent tree frog</name>
    <name type="synonym">Litoria splendida</name>
    <dbReference type="NCBI Taxonomy" id="30345"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Ranoidea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1999" name="Nature" volume="401" first="444" last="445">
      <title>Aquatic sex pheromone from a male tree frog.</title>
      <authorList>
        <person name="Wabnitz P.A."/>
        <person name="Bowie J.H."/>
        <person name="Tyler M.J."/>
        <person name="Wallace J.C."/>
        <person name="Smith B.P."/>
      </authorList>
      <dbReference type="PubMed" id="10519546"/>
      <dbReference type="DOI" id="10.1038/46724"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Parotoid gland</tissue>
      <tissue>Rostral gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2000" name="Eur. J. Biochem." volume="267" first="269" last="275">
      <title>Differences in the skin peptides of the male and female Australian tree frog Litoria splendida. The discovery of the aquatic male sex pheromone splendipherin, together with Phe8 caerulein and a new antibiotic peptide caerin 1.10.</title>
      <authorList>
        <person name="Wabnitz P.A."/>
        <person name="Bowie J.H."/>
        <person name="Tyler M.J."/>
        <person name="Wallace J.C."/>
        <person name="Smith B.P."/>
      </authorList>
      <dbReference type="PubMed" id="10601876"/>
      <dbReference type="DOI" id="10.1046/j.1432-1327.2000.01010.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2002" name="Eur. J. Biochem." volume="269" first="100" last="109">
      <title>Amphibian peptides that inhibit neuronal nitric oxide synthase. Isolation of lesuerin from the skin secretion of the Australian stony creek frog Litoria lesueuri.</title>
      <authorList>
        <person name="Doyle J."/>
        <person name="Llewellyn L.E."/>
        <person name="Brinkworth C.S."/>
        <person name="Bowie J.H."/>
        <person name="Wegener K.L."/>
        <person name="Rozek T."/>
        <person name="Wabnitz P.A."/>
        <person name="Wallace J.C."/>
        <person name="Tyler M.J."/>
      </authorList>
      <dbReference type="PubMed" id="11784303"/>
      <dbReference type="DOI" id="10.1046/j.0014-2956.2002.02630.x"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 2 3 4">Acts as a male sex pheromone that attracts females (PubMed:10519546, PubMed:10601876). Has no antimicrobial activity (PubMed:10519546, PubMed:10601876). Strongly inhibits the formation of NO by neuronal nitric oxide synthase (nNOS) at micromolar concentrations (PubMed:11784303). Acts by a non-competitive mechanism, probably by binding to calcium/calmodulin and as a consequence blocking calmodulin attachment to nNOS (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2 3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7 8">Expressed by the skin parotoid and/or rostral glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the frog skin active peptide (FSAP) family. Caerin subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P69033"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005186">
    <property type="term" value="F:pheromone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR032021">
    <property type="entry name" value="Frog_Litoria"/>
  </dbReference>
  <dbReference type="Pfam" id="PF16049">
    <property type="entry name" value="Antimicrobial24"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0588">Pheromone</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000043744" description="Splendipherin" evidence="2 3">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P56249"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10519546"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="10601876"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="11784303"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="10519546"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0000305" key="7">
    <source>
      <dbReference type="PubMed" id="10519546"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="10601876"/>
    </source>
  </evidence>
  <sequence length="25" mass="2366" checksum="DDD82C36B49186B8" modified="2005-02-01" version="1">GLVSSIGKALGGLLADVVKSKGQPA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>