<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-10-05" modified="2023-06-28" version="47" xmlns="http://uniprot.org/uniprot">
  <accession>B1NWR6</accession>
  <accession>A7SCE0</accession>
  <accession>B1NWR8</accession>
  <accession>B1NWR9</accession>
  <accession>B1NWS2</accession>
  <accession>B1NWS7</accession>
  <accession>B1NWS9</accession>
  <accession>B5L633</accession>
  <accession>B5L634</accession>
  <name>NA228_NEMVE</name>
  <protein>
    <recommendedName>
      <fullName evidence="8">N.vectensis toxin 1 3</fullName>
      <shortName evidence="8">Nv1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="9">Delta-edwarditoxin-Nvc1c</fullName>
      <shortName evidence="9">Delta-EWTX-Nvc1c</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="9">Delta-edwarditoxin-Nvc1d</fullName>
      <shortName evidence="9">Delta-EWTX-Nvc1d</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="7">Neurotoxin Nv1-116.28.1</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="16">Neurotoxin Nv1-13</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="17">Neurotoxin Nv1-15</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="10 18">Neurotoxin Nv1-17</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="10 19">Neurotoxin Nv1-18</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="10 20">Neurotoxin Nv1-19</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="12">Neurotoxin Nv1-2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="13">Neurotoxin Nv1-4</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="14">Neurotoxin Nv1-5</fullName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="15">Neurotoxin Nv1-8</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="ORF">v1g113139</name>
  </gene>
  <organism>
    <name type="scientific">Nematostella vectensis</name>
    <name type="common">Starlet sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="45351"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Edwardsiidae</taxon>
      <taxon>Nematostella</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="J. Mol. Biol." volume="380" first="437" last="443">
      <title>Intron retention as a posttranscriptional regulatory mechanism of neurotoxin expression at early life stages of the starlet anemone Nematostella vectensis.</title>
      <authorList>
        <person name="Moran Y."/>
        <person name="Weinberger H."/>
        <person name="Reitzel A.M."/>
        <person name="Sullivan J.C."/>
        <person name="Kahn R."/>
        <person name="Gordon D."/>
        <person name="Finnerty J.R."/>
        <person name="Gurevitz M."/>
      </authorList>
      <dbReference type="PubMed" id="18538344"/>
      <dbReference type="DOI" id="10.1016/j.jmb.2008.05.011"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>ALTERNATIVE SPLICING</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
    <scope>TOXIC DOSE</scope>
    <source>
      <strain>Sippewissett Marsh</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="Mol. Biol. Evol." volume="25" first="737" last="747">
      <title>Concerted evolution of sea anemone neurotoxin genes is revealed through analysis of the Nematostella vectensis genome.</title>
      <authorList>
        <person name="Moran Y."/>
        <person name="Weinberger H."/>
        <person name="Sullivan J.C."/>
        <person name="Reitzel A.M."/>
        <person name="Finnerty J.R."/>
        <person name="Gurevitz M."/>
      </authorList>
      <dbReference type="PubMed" id="18222944"/>
      <dbReference type="DOI" id="10.1093/molbev/msn021"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>Crane Marsh</strain>
      <strain>Neponset River Marsh</strain>
      <strain>Sippewissett Marsh</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2007" name="Science" volume="317" first="86" last="94">
      <title>Sea anemone genome reveals ancestral eumetazoan gene repertoire and genomic organization.</title>
      <authorList>
        <person name="Putnam N.H."/>
        <person name="Srivastava M."/>
        <person name="Hellsten U."/>
        <person name="Dirks B."/>
        <person name="Chapman J."/>
        <person name="Salamov A."/>
        <person name="Terry A."/>
        <person name="Shapiro H."/>
        <person name="Lindquist E."/>
        <person name="Kapitonov V.V."/>
        <person name="Jurka J."/>
        <person name="Genikhovich G."/>
        <person name="Grigoriev I.V."/>
        <person name="Lucas S.M."/>
        <person name="Steele R.E."/>
        <person name="Finnerty J.R."/>
        <person name="Technau U."/>
        <person name="Martindale M.Q."/>
        <person name="Rokhsar D.S."/>
      </authorList>
      <dbReference type="PubMed" id="17615350"/>
      <dbReference type="DOI" id="10.1126/science.1139158"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CH2 X CH6</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2012" name="Proc. R. Soc. B" volume="279" first="1351" last="1358">
      <title>Neurotoxin localization to ectodermal gland cells uncovers an alternative mechanism of venom delivery in sea anemones.</title>
      <authorList>
        <person name="Moran Y."/>
        <person name="Genikhovich G."/>
        <person name="Gordon D."/>
        <person name="Wienkoop S."/>
        <person name="Zenkert C."/>
        <person name="Ozbek S."/>
        <person name="Technau U."/>
        <person name="Gurevitz M."/>
      </authorList>
      <dbReference type="PubMed" id="22048953"/>
      <dbReference type="DOI" id="10.1098/rspb.2011.1731"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="Toxicon" volume="60" first="539" last="550">
      <title>Development of a rational nomenclature for naming peptide and protein toxins from sea anemones.</title>
      <authorList>
        <person name="Oliveira J.S."/>
        <person name="Fuentes-Silva D."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="22683676"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2012.05.020"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2018" name="Elife" volume="7">
      <title>Dynamics of venom composition across a complex life cycle.</title>
      <authorList>
        <person name="Columbus-Shenkar Y.Y."/>
        <person name="Sachkova M.Y."/>
        <person name="Macrander J."/>
        <person name="Fridrich A."/>
        <person name="Modepalli V."/>
        <person name="Reitzel A.M."/>
        <person name="Sunagar K."/>
        <person name="Moran Y."/>
      </authorList>
      <dbReference type="PubMed" id="29424690"/>
      <dbReference type="DOI" id="10.7554/elife.35014"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2019" name="Mol. Biol. Evol." volume="36" first="2001" last="2012">
      <title>The birth and death of toxins with distinct functions: a case study in the sea anemone Nematostella.</title>
      <authorList>
        <person name="Sachkova M.Y."/>
        <person name="Singer S.A."/>
        <person name="Macrander J."/>
        <person name="Reitzel A.M."/>
        <person name="Peigneur S."/>
        <person name="Tytgat J."/>
        <person name="Moran Y."/>
      </authorList>
      <dbReference type="PubMed" id="31134275"/>
      <dbReference type="DOI" id="10.1093/molbev/msz132"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4 5 6">Binds to site 3 of voltage-gated sodium channels and inhibits the inactivation process (PubMed:18538344). Is highly active on DmNav1/TipE (drosophila) and is only extremely weakly active on rat Nav1.4-beta-1/SCN4A-SCN1B, and on human Nav1.5-beta-1/SCN5A-beta-1 (PubMed:18538344). This reveals high specificity for arthropod over mammalian channels (PubMed:18538344). In vivo, when released into the medium, this recombinant toxin induces impaired swimming, paralysis and death of the crustacean A.nauplii within several hours (PubMed:22048953). Also causes paralysis of cherry shrimps immediately after injection at very low doses (PubMed:29424690). Its effect on zebrafish (D.rerio) larvae is also rapid, since it induces tail twitching accompanied by impaired swimming after 20 minutes and complete paralysis within 45 minutes (PubMed:22048953). It has also been observed to cause death of zebrafish larvae within 1 hour (PubMed:31134275).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>B1NWR6-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>B1NWR6-2</id>
      <name>2</name>
      <name>truncated</name>
      <sequence type="described" ref="VSP_039745"/>
    </isoform>
    <text>Intron retention discovered for all transcripts, no experimental confirmation available for this specific sequence.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4 5">Expressed in ectodermal glands and in clumps outside of the extodermal layer (PubMed:22048953). Is not expressed in nematocytes (PubMed:22048953). In adult female tissues, shows similar expression levels in mesenteries (gametes-producing tissue), tentacles, pharynx and physa (PubMed:29424690).</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="3 4 5 6">Is detected in unfertilized eggs (at protein level) (PubMed:29424690, PubMed:31134275). Is also detected in late planulae, primary polyps and adults (both females and males) (at protein level) (PubMed:22048953, PubMed:29424690). Nv1 is transcribed throughout the complete life cycle and is found at multiple developmental stages including unfertilized eggs, blastulae, gastrulae, early planulae, planulae, metamorphosing planulae, primary polyps, juvenile polyps (2 and 4 months old), adult males, and adult females, with highest levels in juvenile polyps and adults (PubMed:18538344, PubMed:29424690). Importantly, Nv1 transcripts are not spliced in the embryo and planula due to intron retention and therefore Nv1 can be considered purely an adult toxin (PubMed:18538344).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">PD(50) is 76 nmol/kg into blowfly larvae.</text>
  </comment>
  <comment type="miscellaneous">
    <text>Nv1 toxin seems to be encoded by 8 different genes. 4 of them code for identical precursors, whereas 4 others code for very similar precursors. In the genome draft, 6 additional loci are also correlated to Nv1 toxin, but they are not predicted to be functional genes. This high similarity may be explained by concerted evolution.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="10">The primary structure of the mature peptide is identical in 9 entries (AC B1NWS4, AC B1NWS1, AC B1NWR6, AC P0CH90, AC P0CH46, AC B1NWS8, AC A7SCE5, AC B1NWR7 and AC P0CH45). Additional information can be found in entry AC B1NWS4.</text>
  </comment>
  <comment type="miscellaneous">
    <molecule>Isoform 2</molecule>
    <text evidence="10">Due to an intron retention observed only in early life stages (embryo and planula).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="3">Negative results: has no activity on the rat brain channel Nav1.2a-beta-1/SCN2A-SCN1B.</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the sea anemone sodium channel inhibitory toxin family. Type II subfamily.</text>
  </comment>
  <comment type="caution">
    <text evidence="10">The 8 variants shown here could also be shown in the 3 identical proteins (AC B1NWS4, AC P0CH46 and AC B1NWR7) encoded by the 3 different genes.</text>
  </comment>
  <comment type="sequence caution" evidence="10">
    <conflict type="erroneous gene model prediction">
      <sequence resource="EMBL-CDS" id="EDO38670" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="EU422969">
    <property type="protein sequence ID" value="ACB71118.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU422970">
    <property type="protein sequence ID" value="ACB71119.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU422971">
    <property type="protein sequence ID" value="ACB71120.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU124453">
    <property type="protein sequence ID" value="ABW97332.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU124455">
    <property type="protein sequence ID" value="ABW97334.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU124456">
    <property type="protein sequence ID" value="ABW97335.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU124459">
    <property type="protein sequence ID" value="ABW97338.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU124464">
    <property type="protein sequence ID" value="ABW97343.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EU124466">
    <property type="protein sequence ID" value="ABW97345.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DS469622">
    <property type="protein sequence ID" value="EDO38670.1"/>
    <property type="status" value="ALT_SEQ"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_001630733.1">
    <property type="nucleotide sequence ID" value="XM_001630683.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_001630735.1">
    <property type="nucleotide sequence ID" value="XM_001630685.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_001630737.1">
    <property type="nucleotide sequence ID" value="XM_001630687.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_001630739.1">
    <property type="nucleotide sequence ID" value="XM_001630689.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B1NWR6"/>
  <dbReference type="SMR" id="B1NWR6"/>
  <dbReference type="HOGENOM" id="CLU_2944416_0_0_1"/>
  <dbReference type="InParanoid" id="B1NWR6"/>
  <dbReference type="PhylomeDB" id="B1NWR6"/>
  <dbReference type="Proteomes" id="UP000001593">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0017080">
    <property type="term" value="F:sodium channel regulator activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.20.20.10">
    <property type="entry name" value="Anthopleurin-A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023355">
    <property type="entry name" value="Myo_ane_neurotoxin_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00706">
    <property type="entry name" value="Toxin_4"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000398311" evidence="10">
    <location>
      <begin position="21"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_5000319660" description="N.vectensis toxin 1 3" evidence="11">
    <location>
      <begin position="39"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="42"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="44"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="65"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_039745" description="In isoform 2." evidence="10">
    <original>RDMMSDDELDFHLSKRGIPCACDSDGPDIRSASLSGIVWMGSCPSGWKKCKSYYSIVADCCNQ</original>
    <variation>K</variation>
    <location>
      <begin position="23"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-15.">
    <original>S</original>
    <variation>L</variation>
    <location>
      <position position="3"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-17 and in Nv1-18.">
    <original>K</original>
    <variation>N</variation>
    <location>
      <position position="5"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-5, Nv8/Nv19 and Nv1-15.">
    <original>V</original>
    <variation>A</variation>
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-15.">
    <original>L</original>
    <variation>V</variation>
    <location>
      <position position="13"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-2, Nv1-13.">
    <original>C</original>
    <variation>S</variation>
    <location>
      <position position="19"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-13.">
    <original>D</original>
    <variation>T</variation>
    <location>
      <position position="29"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-4, Nv8/Nv19.">
    <original>F</original>
    <variation>Y</variation>
    <location>
      <position position="33"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-18.">
    <original>A</original>
    <variation>V</variation>
    <location>
      <position position="43"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-4.">
    <original>M</original>
    <variation>V</variation>
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In Nv1-13.">
    <original>I</original>
    <variation>V</variation>
    <location>
      <position position="78"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P19651"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18538344"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="22048953"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="29424690"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="31134275"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="18222944"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="18538344"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source>
      <dbReference type="PubMed" id="22683676"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="18538344"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="12">
    <source>
      <dbReference type="EMBL" id="ABW97332.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="13">
    <source>
      <dbReference type="EMBL" id="ABW97334.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="14">
    <source>
      <dbReference type="EMBL" id="ABW97335.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="15">
    <source>
      <dbReference type="EMBL" id="ABW97338.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="16">
    <source>
      <dbReference type="EMBL" id="ABW97343.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="17">
    <source>
      <dbReference type="EMBL" id="ABW97345.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="18">
    <source>
      <dbReference type="EMBL" id="ACB71118.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="19">
    <source>
      <dbReference type="EMBL" id="ACB71119.1"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="20">
    <source>
      <dbReference type="EMBL" id="ACB71120.1"/>
    </source>
  </evidence>
  <sequence length="85" mass="9268" checksum="26673B8F76FA1099" modified="2010-10-05" version="2" precursor="true">MASFKIVIVCLALLVAVACARRRDMMSDDELDFHLSKRGIPCACDSDGPDIRSASLSGIVWMGSCPSGWKKCKSYYSIVADCCNQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>