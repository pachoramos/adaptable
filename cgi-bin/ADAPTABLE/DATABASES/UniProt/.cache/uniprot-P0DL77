<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2017-11-22" modified="2023-02-22" version="9" xmlns="http://uniprot.org/uniprot">
  <accession>P0DL77</accession>
  <name>CT1A_CORTR</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">U1-theraphotoxin-Ct1a</fullName>
      <shortName evidence="4">U1-TRTX-Ct1a</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Coremiocnemis tropix</name>
    <name type="common">Australian tarantula spider</name>
    <dbReference type="NCBI Taxonomy" id="1904443"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Mygalomorphae</taxon>
      <taxon>Theraphosidae</taxon>
      <taxon>Coremiocnemis</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2016" name="Toxicon" volume="123" first="62" last="70">
      <title>Isolation of two insecticidal toxins from venom of the Australian theraphosid spider Coremiocnemis tropix.</title>
      <authorList>
        <person name="Ikonomopoulou M.P."/>
        <person name="Smith J.J."/>
        <person name="Herzig V."/>
        <person name="Pineda S.S."/>
        <person name="Dziemborowicz S."/>
        <person name="Er S.Y."/>
        <person name="Durek T."/>
        <person name="Gilchrist J."/>
        <person name="Alewood P.F."/>
        <person name="Nicholson G.M."/>
        <person name="Bosmans F."/>
        <person name="King G.F."/>
      </authorList>
      <dbReference type="PubMed" id="27793656"/>
      <dbReference type="DOI" id="10.1016/j.toxicon.2016.10.013"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 49-87</scope>
    <scope>FUNCTION</scope>
    <scope>BIOASSAY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TOXIC DOSE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SYNTHESIS OF 49-87</scope>
    <source>
      <tissue>Venom</tissue>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1 3">This toxin causes paralysis and death to sheep blowflies (PubMed:27793656). It may inhibit voltage-gated calcium channels (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="PTM">
    <text evidence="6">Contains 3 disulfide bonds. Two different connectivities are observed in similar proteins (C1-C3, C2-C5, C4-C6 or C1-C4, C2-C5, C3-C6).</text>
  </comment>
  <comment type="mass spectrometry" mass="4325.068" method="MALDI" evidence="3">
    <text>Monoisotopic mass.</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">PD(50) is 1335 +- 132 pmol/g in adult sheep blowflies (L.cuprina) (at 24 hours post-injection).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="3">LD(50) is 1688 +- 64 pmol/g in adult sheep blowflies (L.cuprina) (at 24 hours post-injection).</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="3">No effect of the synthetic peptide are observed on voltage-gated sodium channels from the American cockroach Periplanata americana or the German cockroach Blattella germanica.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the neurotoxin 12 (Hwtx-2) family. 03 (juruin) subfamily.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P0DL77"/>
  <dbReference type="SMR" id="P0DL77"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR012625">
    <property type="entry name" value="Toxin_20"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08089">
    <property type="entry name" value="Toxin_20"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57059">
    <property type="entry name" value="omega toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS60022">
    <property type="entry name" value="HWTX_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000442235" evidence="3">
    <location>
      <begin position="24"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000442236" description="U1-theraphotoxin-Ct1a" evidence="3">
    <location>
      <begin position="49"/>
      <end position="87"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P0DL81"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="27793656"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="27793656"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="27793656"/>
    </source>
  </evidence>
  <sequence length="87" mass="9546" checksum="C03FBB9961F133A5" modified="2017-11-22" version="1" precursor="true">MKTFTLIAILTCAVLVIFHAAAAEELEVQDVIQPEDTLTGLATLDEDRLFECSFSCDIKKNGKPCKGSGEKKCSGGWRCKMNFCVKV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>