<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-13" modified="2024-11-27" version="108" xmlns="http://uniprot.org/uniprot">
  <accession>P60699</accession>
  <accession>Q9ZNG0</accession>
  <name>MNHG1_STAAW</name>
  <protein>
    <recommendedName>
      <fullName>Na(+)/H(+) antiporter subunit G1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Mnh complex subunit G1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">mnhG1</name>
    <name type="ordered locus">MW0828</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Mnh complex is a Na(+)/H(+) antiporter involved in Na(+) excretion.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">May form a heterooligomeric complex that consists of seven subunits: mnhA1, mnhB1, mnhC1, mnhD1, mnhE1, mnhF1 and mnhG1.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Cell membrane</location>
      <topology evidence="3">Multi-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the CPA3 antiporters (TC 2.A.63) subunit G family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB94693.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="C89861">
    <property type="entry name" value="C89861"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000590451.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P60699"/>
  <dbReference type="SMR" id="P60699"/>
  <dbReference type="KEGG" id="sam:MW0828"/>
  <dbReference type="HOGENOM" id="CLU_121334_0_3_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0015385">
    <property type="term" value="F:sodium:proton antiporter activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR005133">
    <property type="entry name" value="PhaG_MnhG_YufB"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01300">
    <property type="entry name" value="CPA3_mnhG_phaG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34703">
    <property type="entry name" value="ANTIPORTER SUBUNIT MNHG2-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34703:SF2">
    <property type="entry name" value="NA(+)_H(+) ANTIPORTER SUBUNIT G1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03334">
    <property type="entry name" value="PhaG_MnhG_YufB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0050">Antiport</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0375">Hydrogen ion transport</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0915">Sodium</keyword>
  <keyword id="KW-0739">Sodium transport</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="chain" id="PRO_0000086862" description="Na(+)/H(+) antiporter subunit G1">
    <location>
      <begin position="1"/>
      <end position="118"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="7"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="44"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="2">
    <location>
      <begin position="71"/>
      <end position="90"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="118" mass="12819" checksum="51763F14B3263991" modified="2004-04-13" version="1">MIKIILISLALIFVIIGALISALAAIGLLRLEDVYSRAHAAGKASTLGAMSLLFGTFLYFIATQGFVNMQLIVAIIFVLITGPLSSHMIMKAAYNIKTPYTKKTKVDEISEDLKDTKL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>