<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-11-22" modified="2024-11-27" version="94" xmlns="http://uniprot.org/uniprot">
  <accession>P0AD50</accession>
  <accession>P11285</accession>
  <name>YFIA_ECOL6</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Ribosome-associated factor Y</fullName>
      <shortName>pY</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Ribosome associated inhibitor A</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="4" type="primary">yfiA</name>
    <name type="synonym">raiA</name>
    <name type="ordered locus">c3119</name>
  </gene>
  <organism>
    <name type="scientific">Escherichia coli O6:H1 (strain CFT073 / ATCC 700928 / UPEC)</name>
    <dbReference type="NCBI Taxonomy" id="199310"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Enterobacteriaceae</taxon>
      <taxon>Escherichia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Proc. Natl. Acad. Sci. U.S.A." volume="99" first="17020" last="17024">
      <title>Extensive mosaic structure revealed by the complete genome sequence of uropathogenic Escherichia coli.</title>
      <authorList>
        <person name="Welch R.A."/>
        <person name="Burland V."/>
        <person name="Plunkett G. III"/>
        <person name="Redford P."/>
        <person name="Roesch P."/>
        <person name="Rasko D."/>
        <person name="Buckles E.L."/>
        <person name="Liou S.-R."/>
        <person name="Boutin A."/>
        <person name="Hackett J."/>
        <person name="Stroud D."/>
        <person name="Mayhew G.F."/>
        <person name="Rose D.J."/>
        <person name="Zhou S."/>
        <person name="Schwartz D.C."/>
        <person name="Perna N.T."/>
        <person name="Mobley H.L.T."/>
        <person name="Donnenberg M.S."/>
        <person name="Blattner F.R."/>
      </authorList>
      <dbReference type="PubMed" id="12471157"/>
      <dbReference type="DOI" id="10.1073/pnas.252529799"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>CFT073 / ATCC 700928 / UPEC</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">During stationary phase, prevents 70S dimer formation, probably in order to regulate translation efficiency during transition between the exponential and the stationary phases. In addition, during environmental stress such as cold shock or excessive cell density at stationary phase, stabilizes the 70S ribosome against dissociation, inhibits translation initiation and increase translation accuracy. When normal growth conditions are restored, is quickly released from the ribosome (By similarity).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Associates mainly with 70S ribosomes.</text>
  </comment>
  <comment type="induction">
    <text evidence="2">By environmental stress and during stationary phase.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the HPF/YfiA ribosome-associated protein family. YfiA subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AE014075">
    <property type="protein sequence ID" value="AAN81568.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000178456.1">
    <property type="nucleotide sequence ID" value="NZ_CP051263.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0AD50"/>
  <dbReference type="BMRB" id="P0AD50"/>
  <dbReference type="SMR" id="P0AD50"/>
  <dbReference type="STRING" id="199310.c3119"/>
  <dbReference type="GeneID" id="89517404"/>
  <dbReference type="KEGG" id="ecc:c3119"/>
  <dbReference type="eggNOG" id="COG1544">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_071472_3_0_6"/>
  <dbReference type="BioCyc" id="ECOL199310:C3119-MONOMER"/>
  <dbReference type="Proteomes" id="UP000001410">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0022627">
    <property type="term" value="C:cytosolic small ribosomal subunit"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043024">
    <property type="term" value="F:ribosomal small subunit binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045900">
    <property type="term" value="P:negative regulation of translational elongation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="CDD" id="cd00552">
    <property type="entry name" value="RaiA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.160.100:FF:000002">
    <property type="entry name" value="Ribosome-associated translation inhibitor RaiA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.160.100">
    <property type="entry name" value="Ribosome hibernation promotion factor-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050574">
    <property type="entry name" value="HPF/YfiA_ribosome-assoc"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036567">
    <property type="entry name" value="RHF-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003489">
    <property type="entry name" value="RHF/RaiA"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00741">
    <property type="entry name" value="yfiA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33231">
    <property type="entry name" value="30S RIBOSOMAL PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33231:SF3">
    <property type="entry name" value="RIBOSOME-ASSOCIATED INHIBITOR A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02482">
    <property type="entry name" value="Ribosomal_S30AE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF69754">
    <property type="entry name" value="Ribosome binding protein Y (YfiA homologue)"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0346">Stress response</keyword>
  <keyword id="KW-0810">Translation regulation</keyword>
  <feature type="initiator methionine" description="Removed" evidence="1">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000169263" description="Ribosome-associated factor Y">
    <location>
      <begin position="2"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="91"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="modified residue" description="N6-acetyllysine" evidence="1">
    <location>
      <position position="66"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P0AD49"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="113" mass="12785" checksum="562901F819836A87" modified="2007-01-23" version="2">MTMNITSKQMEITPAIRQHVADRLAKLEKWQTHLINPHIILSKEPQGFVADATINTPNGVLVASGKHEDMYTAINELINKLERQLNKLQHKGEARRAATSVKDANFVEEVEEE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>