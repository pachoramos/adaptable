<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-11-27" version="83" xmlns="http://uniprot.org/uniprot">
  <accession>P45664</accession>
  <name>SCXX_CENNO</name>
  <protein>
    <recommendedName>
      <fullName>Toxin CngtIII</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Centruroides noxius</name>
    <name type="common">Mexican scorpion</name>
    <dbReference type="NCBI Taxonomy" id="6878"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Centruroides</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Gene" volume="128" first="165" last="171">
      <title>Cloning and characterization of cDNAs that code for Na(+)-channel-blocking toxins of the scorpion Centruroides noxius Hoffmann.</title>
      <authorList>
        <person name="Becerril B."/>
        <person name="Vazquez A."/>
        <person name="Garcia C."/>
        <person name="Corona M."/>
        <person name="Bolivar F."/>
        <person name="Possani L.D."/>
      </authorList>
      <dbReference type="PubMed" id="8390386"/>
      <dbReference type="DOI" id="10.1016/0378-1119(93)90559-l"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Beta toxins bind voltage-independently at site-4 of sodium channels (Nav) and shift the voltage of activation toward more negative potentials thereby affecting sodium channel activation and promoting spontaneous and repetitive firing.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="3">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="L05061">
    <property type="protein sequence ID" value="AAA28286.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="JN0670">
    <property type="entry name" value="JN0670"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P45664"/>
  <dbReference type="SMR" id="P45664"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000035284" description="Toxin CngtIII">
    <location>
      <begin position="20"/>
      <end position="87"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="2">
    <location>
      <begin position="20"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="31"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="35"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="44"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="48"/>
      <end position="67"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="87" mass="9587" checksum="F172AA6162C406A3" modified="1995-11-01" version="1" precursor="true">MNSLLMITACLVLFGTVWAKEGYLVNKSTGCKYGCFWLGKNEGCDKECKAKNQGGSYGYCYAFGCWCEGLPESTPTYPLPNKTCSKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>