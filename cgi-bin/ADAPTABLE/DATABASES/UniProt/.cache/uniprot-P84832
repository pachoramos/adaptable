<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-05-02" modified="2021-04-07" version="24" xmlns="http://uniprot.org/uniprot">
  <accession>P84832</accession>
  <name>BRK10_ASCTR</name>
  <protein>
    <recommendedName>
      <fullName>Bradykinin-like peptide AR-10</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Ascaphus truei</name>
    <name type="common">Coastal tailed frog</name>
    <dbReference type="NCBI Taxonomy" id="8439"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Ascaphidae</taxon>
      <taxon>Ascaphus</taxon>
    </lineage>
  </organism>
  <reference evidence="2" key="1">
    <citation type="journal article" date="2005" name="Gen. Comp. Endocrinol." volume="143" first="193" last="199">
      <title>Bradykinin-related peptides and tryptophyllins in the skin secretions of the most primitive extant frog, Ascaphus truei.</title>
      <authorList>
        <person name="Conlon J.M."/>
        <person name="Jouenne T."/>
        <person name="Cosette P."/>
        <person name="Cosquer D."/>
        <person name="Vaudry H."/>
        <person name="Taylor C.K."/>
        <person name="Abel P.W."/>
      </authorList>
      <dbReference type="PubMed" id="15922344"/>
      <dbReference type="DOI" id="10.1016/j.ygcen.2005.04.006"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue evidence="1">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Induces relaxation of mouse trachea that has been precontracted with methacholine. May induce relaxation of arterial smooth muscle. May target bradykinin receptors (BDKRB).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="1">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1039.6" method="MALDI" evidence="1"/>
  <comment type="similarity">
    <text evidence="2">Belongs to the bradykinin-related peptide family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097746">
    <property type="term" value="P:blood vessel diameter maintenance"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042311">
    <property type="term" value="P:vasodilation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0838">Vasoactive</keyword>
  <keyword id="KW-0840">Vasodilator</keyword>
  <feature type="peptide" id="PRO_0000233936" description="Bradykinin-like peptide AR-10" evidence="1">
    <location>
      <begin position="1"/>
      <end position="10"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="15922344"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="10" mass="1040" checksum="DF49D275B7286772" modified="2006-05-02" version="1">APVPGLSPFR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>