<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-12-15" modified="2023-02-22" version="73" xmlns="http://uniprot.org/uniprot">
  <accession>Q40901</accession>
  <name>DEF_PETIN</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Gamma-thionin homolog PPT</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Petunia integrifolia</name>
    <name type="common">Violet-flowered petunia</name>
    <name type="synonym">Salpiglossis integrifolia</name>
    <dbReference type="NCBI Taxonomy" id="4103"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>asterids</taxon>
      <taxon>lamiids</taxon>
      <taxon>Solanales</taxon>
      <taxon>Solanaceae</taxon>
      <taxon>Petunioideae</taxon>
      <taxon>Petunia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="Plant Mol. Biol." volume="26" first="459" last="464">
      <title>Characterization of a predominantly pistil-expressed gene encoding a gamma-thionin-like protein of Petunia inflata.</title>
      <authorList>
        <person name="Karunanandaa B."/>
        <person name="Singh A."/>
        <person name="Kao T.H."/>
      </authorList>
      <dbReference type="PubMed" id="7948892"/>
      <dbReference type="DOI" id="10.1007/bf00039555"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Pistil</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>May be involved in the defense of the pistil against pathogen infection.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Predominantly expressed in the pistil during all stages of flower development.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <comment type="caution">
    <text evidence="4">Was initially thought to be a thionin.</text>
  </comment>
  <dbReference type="EMBL" id="L27173">
    <property type="protein sequence ID" value="AAA64740.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="S52634">
    <property type="entry name" value="S52634"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q40901"/>
  <dbReference type="SMR" id="Q40901"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR008176">
    <property type="entry name" value="Defensin_plant"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33147:SF27">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33147">
    <property type="entry name" value="DEFENSIN-LIKE PROTEIN 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00304">
    <property type="entry name" value="Gamma-thionin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00288">
    <property type="entry name" value="PUROTHIONIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00940">
    <property type="entry name" value="GAMMA_THIONIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000007051" description="Defensin-like protein">
    <location>
      <begin position="32"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="34"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="45"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="51"/>
      <end position="72"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="55"/>
      <end position="74"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="7948892"/>
    </source>
  </evidence>
  <sequence length="78" mass="8799" checksum="263DB3AC9D619265" modified="1996-11-01" version="1" precursor="true">MGRSIRLFATFFLIAMLFLSTEMGPMTSAEARTCESQSHRFHGTCVRESNCASVCQTEGFIGGNCRAFRRRCFCTRNC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>