<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2020-06-17" modified="2022-05-25" version="26" xmlns="http://uniprot.org/uniprot">
  <accession>C5H0C7</accession>
  <name>AMO1_AMOLO</name>
  <protein>
    <recommendedName>
      <fullName evidence="3 6">Amolopin-P1</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Amolops loloensis</name>
    <name type="common">Lolokou Sucker Frog</name>
    <name type="synonym">Staurois loloensis</name>
    <dbReference type="NCBI Taxonomy" id="318551"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Ranoidea</taxon>
      <taxon>Ranidae</taxon>
      <taxon>Amolops</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Biochimie" volume="90" first="863" last="867">
      <title>A novel family of antimicrobial peptides from the skin of Amolops loloensis.</title>
      <authorList>
        <person name="Wang A."/>
        <person name="Wang J."/>
        <person name="Hong J."/>
        <person name="Feng H."/>
        <person name="Yang H."/>
        <person name="Yu X."/>
        <person name="Ma Y."/>
        <person name="Lai R."/>
      </authorList>
      <dbReference type="PubMed" id="18312859"/>
      <dbReference type="DOI" id="10.1016/j.biochi.2008.02.003"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 45-62</scope>
    <scope>FUNCTION</scope>
    <scope>SYNTHESIS OF 45-62</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Skin</tissue>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2">Antimicrobial peptide with activity against Gram-positive bacteria (PubMed:18312859). Has been tested against S.aureus (MIC=37.5 ug/mL), against B.pumilus (MIC=75.0 ug/mL), B.cereus (no activity detected) (PubMed:18312859). Does not show activity against Gram-negative bacteria (E.coli, B.dysenteriae, A.calcoaceticus, P.aeruginosa) and fungi (C.albicans) (PubMed:18312859). Does not show hemolytic activity against rabbit erythrocytes (PubMed:18312859).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1921.1" method="FAB" evidence="2"/>
  <comment type="similarity">
    <text evidence="4">Belongs to the frog skin active peptide (FSAP) family. Amolopin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="EU311540">
    <property type="protein sequence ID" value="ACA09630.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="C5H0C7"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000450012" evidence="5">
    <location>
      <begin position="23"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_5002952490" description="Amolopin-P1" evidence="2">
    <location>
      <begin position="45"/>
      <end position="62"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="18312859"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="18312859"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0000305" key="5">
    <source>
      <dbReference type="PubMed" id="18312859"/>
    </source>
  </evidence>
  <evidence type="ECO:0000312" key="6">
    <source>
      <dbReference type="EMBL" id="ACA09630.1"/>
    </source>
  </evidence>
  <sequence length="62" mass="6958" checksum="5AD718C3AFE3E153" modified="2009-07-28" version="1" precursor="true">MFPMKKSLLLLFFFGPISLSFCDQERGADEEENGGEVTEQEVKRNILSSIVNGINRALSFFG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>