<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-12-19" modified="2024-11-27" version="79" xmlns="http://uniprot.org/uniprot">
  <accession>Q9BKJ1</accession>
  <name>SCN2_MESMA</name>
  <protein>
    <recommendedName>
      <fullName>Anti-neuroexcitation peptide 2</fullName>
      <shortName>BmKANEP2</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Anti-neuroexcitation peptide II</fullName>
      <shortName>ANEPII</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Mesobuthus martensii</name>
    <name type="common">Manchurian scorpion</name>
    <name type="synonym">Buthus martensii</name>
    <dbReference type="NCBI Taxonomy" id="34649"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Mesobuthus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2000-03" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning of anti-neuroexcitation peptide II (ANEP) cDNA from Scorpion Buthus martensii Karsch.</title>
      <authorList>
        <person name="Zhang J.-H."/>
        <person name="Hua Z.C."/>
        <person name="Zhu D.X."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2001" name="Prep. Biochem. Biotechnol." volume="31" first="49" last="57">
      <title>Expression of anti-neuroexcitation peptide (ANEP) of scorpion Buthus martensii Karsch in Escherichia coli.</title>
      <authorList>
        <person name="Zhang J.-H."/>
        <person name="Hua Z.C."/>
        <person name="Xu Z."/>
        <person name="Zheng W.J."/>
        <person name="Zhu D.X."/>
      </authorList>
      <dbReference type="PubMed" id="11321163"/>
      <dbReference type="DOI" id="10.1081/pb-100103371"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2010" name="C. R. Biol." volume="333" first="637" last="641">
      <title>Study of the binding residues between ANEPII and insect sodium channel receptor.</title>
      <authorList>
        <person name="Song Y.B."/>
        <person name="Ma L."/>
        <person name="Yang W.Y."/>
        <person name="Wang J."/>
        <person name="Cheng M.S."/>
        <person name="Wu C.F."/>
        <person name="Zhang J.H."/>
      </authorList>
      <dbReference type="PubMed" id="20816643"/>
      <dbReference type="DOI" id="10.1016/j.crvi.2010.06.005"/>
    </citation>
    <scope>3D-STRUCTURE MODELING OF BMKANEP2-DMNAV1 INTERACTION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 4">Binds to sodium channels (Nav) and inhibits them (By similarity). Recombinant ANEP delays the convulsion seizure of insect models by 18% and shows anti-neuroexcitatory activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="5">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Beta subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AF242736">
    <property type="protein sequence ID" value="AAK28341.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9BKJ1"/>
  <dbReference type="SMR" id="Q9BKJ1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.30.10:FF:000002">
    <property type="entry name" value="Alpha-like toxin BmK-M1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000035264" description="Anti-neuroexcitation peptide 2">
    <location>
      <begin position="22"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="3">
    <location>
      <begin position="22"/>
      <end position="82"/>
    </location>
  </feature>
  <feature type="site" description="Binds the drosophila sodium channel DmNav1" evidence="5">
    <location>
      <position position="55"/>
    </location>
  </feature>
  <feature type="site" description="Binds the drosophila sodium channel DmNav1" evidence="5">
    <location>
      <position position="57"/>
    </location>
  </feature>
  <feature type="site" description="Binds the drosophila sodium channel DmNav1" evidence="5">
    <location>
      <position position="60"/>
    </location>
  </feature>
  <feature type="site" description="Binds the drosophila sodium channel DmNav1" evidence="5">
    <location>
      <position position="61"/>
    </location>
  </feature>
  <feature type="site" description="Binds the drosophila sodium channel DmNav1" evidence="5">
    <location>
      <position position="74"/>
    </location>
  </feature>
  <feature type="site" description="Binds the drosophila sodium channel DmNav1" evidence="5">
    <location>
      <position position="79"/>
    </location>
  </feature>
  <feature type="site" description="Binds the drosophila sodium channel DmNav1" evidence="5">
    <location>
      <position position="82"/>
    </location>
  </feature>
  <feature type="site" description="Binds the drosophila sodium channel DmNav1" evidence="5">
    <location>
      <position position="83"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="31"/>
      <end position="81"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="35"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="42"/>
      <end position="63"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="46"/>
      <end position="65"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="11321163"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="85" mass="9225" checksum="C045C7DACB3513AB" modified="2001-06-01" version="1" precursor="true">MKLSLLLVISASMLIDGLVNADGYIRGSNGCKVSCLWGNDGCNKECRAYGASYGYCWTWGLACWCEGLPDDKTWKSESNTCGGKK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>