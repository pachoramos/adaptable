<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-07-24" modified="2024-07-24" version="90" xmlns="http://uniprot.org/uniprot">
  <accession>Q2M2S2</accession>
  <name>EMRE_BOVIN</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Essential MCU regulator, mitochondrial</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="1">Single-pass membrane protein with aspartate-rich tail 1, mitochondrial</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="1" type="primary">SMDT1</name>
    <name evidence="1" type="synonym">EMRE</name>
  </gene>
  <organism>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bos</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2006-01" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <consortium name="NIH - Mammalian Gene Collection (MGC) project"/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>Hereford</strain>
      <tissue>Heart ventricle</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Essential regulatory subunit of the mitochondrial calcium uniporter complex (uniplex), a complex that mediates calcium uptake into mitochondria. Required to bridge the calcium-sensing proteins MICU1 with the calcium-conducting subunit MCU. Acts by mediating activation of MCU and retention of MICU1 to the MCU pore, in order to ensure tight regulation of the uniplex complex and appropriate responses to intracellular calcium signaling.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Component of the uniplex complex, composed of MCU, EMRE/SMDT1, MICU1 and MICU2 (or MICU3) in a 4:4:1:1 stoichiometry (By similarity). The number of EMRE/SMDT1 molecules is hovewer variable, ranging from 1 to 4 copies per uniplex complex, leading to uniplex complexes with distinct gatekeeping profiles (By similarity). Interacts (via its C-terminal poly-Asp tail) with MCUR1; the interaction is direct. Unprocessed form interacts (via transit peptide) with MAIP1 (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Mitochondrion inner membrane</location>
      <topology evidence="1">Single-pass membrane protein</topology>
    </subcellularLocation>
    <text evidence="1">MAIP1 is required to assist sorting of EMRE/SMDT1 into mitochondrion by protecting EMRE/SMDT1 against protein degradation by YME1L1, thereby ensuring SMDT1/EMRE maturation by the mitochondrial processing peptidase (PMPCA and PMPCB).</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The GXXXX[G/A/S] motif at the C-terminal part of the transmembrane region mediates interaction with MCU and is required to activate the calcium-conducting pore in the uniporter complex.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The poly-Asp region at the C-terminus mediates interaction with the polybasic region of MICU1.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Undergoes proteolytic degradation in neurons: degraded by AFG3L2 and SPG7 before SMDT1/EMRE assembly with the uniporter complex, limiting the availability of SMDT1/EMRE for MCU assembly and promoting efficient assembly of gatekeeper subunits with MCU.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the SMDT1/EMRE family.</text>
  </comment>
  <dbReference type="EMBL" id="BC111682">
    <property type="protein sequence ID" value="AAI11683.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001039402.1">
    <property type="nucleotide sequence ID" value="NM_001045937.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q2M2S2"/>
  <dbReference type="SMR" id="Q2M2S2"/>
  <dbReference type="STRING" id="9913.ENSBTAP00000012511"/>
  <dbReference type="PaxDb" id="9913-ENSBTAP00000012511"/>
  <dbReference type="GeneID" id="506149"/>
  <dbReference type="KEGG" id="bta:506149"/>
  <dbReference type="CTD" id="91689"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSBTAG00000009508"/>
  <dbReference type="eggNOG" id="KOG4542">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_172921_1_0_1"/>
  <dbReference type="InParanoid" id="Q2M2S2"/>
  <dbReference type="OMA" id="FLYIGTQ"/>
  <dbReference type="OrthoDB" id="2877123at2759"/>
  <dbReference type="TreeFam" id="TF314649"/>
  <dbReference type="Reactome" id="R-BTA-8949215">
    <property type="pathway name" value="Mitochondrial calcium ion transport"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-8949664">
    <property type="pathway name" value="Processing of SMDT1"/>
  </dbReference>
  <dbReference type="Reactome" id="R-BTA-9837999">
    <property type="pathway name" value="Mitochondrial protein degradation"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000009136">
    <property type="component" value="Chromosome 5"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSBTAG00000009508">
    <property type="expression patterns" value="Expressed in ileocecal valve and 103 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005743">
    <property type="term" value="C:mitochondrial inner membrane"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1990246">
    <property type="term" value="C:uniplex complex"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0099103">
    <property type="term" value="F:channel activator activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030674">
    <property type="term" value="F:protein-macromolecule adaptor activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0036444">
    <property type="term" value="P:calcium import into the mitochondrion"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051560">
    <property type="term" value="P:mitochondrial calcium ion homeostasis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006851">
    <property type="term" value="P:mitochondrial calcium ion transmembrane transport"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018782">
    <property type="entry name" value="MCU_reg"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33904">
    <property type="entry name" value="ESSENTIAL MCU REGULATOR, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33904:SF1">
    <property type="entry name" value="ESSENTIAL MCU REGULATOR, MITOCHONDRIAL"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF10161">
    <property type="entry name" value="DDDD"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-0109">Calcium transport</keyword>
  <keyword id="KW-0406">Ion transport</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0496">Mitochondrion</keyword>
  <keyword id="KW-0999">Mitochondrion inner membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0809">Transit peptide</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <keyword id="KW-0813">Transport</keyword>
  <feature type="transit peptide" description="Mitochondrion" evidence="2">
    <location>
      <begin position="1"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000296322" description="Essential MCU regulator, mitochondrial">
    <location>
      <begin position="48"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="topological domain" description="Mitochondrial matrix" evidence="3">
    <location>
      <begin position="48"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="1">
    <location>
      <begin position="66"/>
      <end position="85"/>
    </location>
  </feature>
  <feature type="topological domain" description="Mitochondrial intermembrane" evidence="3">
    <location>
      <begin position="86"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="GXXXX[G/A/S]" evidence="1">
    <location>
      <begin position="81"/>
      <end position="85"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q9H4I9"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="107" mass="11450" checksum="D0678EAC1B8178F5" modified="2006-02-21" version="1" precursor="true">MASGAARWLALVRVGSGASRSWLSLRKGGDVSAGRSCSGQSLVPTRSVIVTRSGAILPKPVKMSFGLLRVFSIVIPFLYVGTLISKNFAALLEEHDIFVPEDDDDDD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>