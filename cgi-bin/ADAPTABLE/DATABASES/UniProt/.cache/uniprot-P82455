<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2001-05-04" modified="2022-05-25" version="38" xmlns="http://uniprot.org/uniprot">
  <accession>P82455</accession>
  <name>ORMY_FAXLI</name>
  <protein>
    <recommendedName>
      <fullName>Orcomyotropin</fullName>
      <shortName>OMT</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Faxonius limosus</name>
    <name type="common">Spinycheek crayfish</name>
    <name type="synonym">Orconectes limosus</name>
    <dbReference type="NCBI Taxonomy" id="28379"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Crustacea</taxon>
      <taxon>Multicrustacea</taxon>
      <taxon>Malacostraca</taxon>
      <taxon>Eumalacostraca</taxon>
      <taxon>Eucarida</taxon>
      <taxon>Decapoda</taxon>
      <taxon>Pleocyemata</taxon>
      <taxon>Astacidea</taxon>
      <taxon>Astacoidea</taxon>
      <taxon>Cambaridae</taxon>
      <taxon>Faxonius</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2000" name="J. Exp. Biol." volume="203" first="2807" last="2818">
      <title>Two orcokinins and the novel octapeptide orcomyotropin in the hindgut of the crayfish Orconectes limosus: identified myostimulatory neuropeptides originating together in neurones of the terminal abdominal ganglion.</title>
      <authorList>
        <person name="Dircksen H."/>
        <person name="Burdzik S."/>
        <person name="Sauter A."/>
        <person name="Keller R."/>
      </authorList>
      <dbReference type="PubMed" id="10952880"/>
      <dbReference type="DOI" id="10.1242/jeb.203.18.2807"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT PHE-8</scope>
    <source>
      <tissue>Hindgut</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Myotropic peptide, enhances both the frequency and amplitude of spontaneous hindgut contractions. It is synthesized by abdominal ganglionic neurons.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="mass spectrometry" mass="904.8" method="FAB" evidence="1"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044182" description="Orcomyotropin">
    <location>
      <begin position="1"/>
      <end position="8"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="8"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="10952880"/>
    </source>
  </evidence>
  <sequence length="8" mass="905" checksum="87C861B1A9CDDAA9" modified="2001-05-04" version="1">FDAFTTGF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>