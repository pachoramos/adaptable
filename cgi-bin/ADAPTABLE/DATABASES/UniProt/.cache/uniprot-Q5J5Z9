<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2005-03-15" modified="2024-10-02" version="91" xmlns="http://uniprot.org/uniprot">
  <accession>Q5J5Z9</accession>
  <name>DB122_MACMU</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 122</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 122</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB122</name>
  </gene>
  <organism>
    <name type="scientific">Macaca mulatta</name>
    <name type="common">Rhesus macaque</name>
    <dbReference type="NCBI Taxonomy" id="9544"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Cercopithecidae</taxon>
      <taxon>Cercopithecinae</taxon>
      <taxon>Macaca</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2003-12" db="EMBL/GenBank/DDBJ databases">
      <title>Differential expression of a primate beta defensin gene cluster specific to male reproductive tract.</title>
      <authorList>
        <person name="Yashwanth R."/>
        <person name="Hamil K.G."/>
        <person name="Yenugu S."/>
        <person name="French F.S."/>
        <person name="Hall S.H."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Epididymis</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="3">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AY499409">
    <property type="protein sequence ID" value="AAS89328.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001029368.1">
    <property type="nucleotide sequence ID" value="NM_001034196.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q5J5Z9"/>
  <dbReference type="SMR" id="Q5J5Z9"/>
  <dbReference type="STRING" id="9544.ENSMMUP00000023075"/>
  <dbReference type="PaxDb" id="9544-ENSMMUP00000023075"/>
  <dbReference type="GeneID" id="619506"/>
  <dbReference type="KEGG" id="mcc:619506"/>
  <dbReference type="CTD" id="245935"/>
  <dbReference type="eggNOG" id="ENOG502TD2F">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_181906_2_0_1"/>
  <dbReference type="InParanoid" id="Q5J5Z9"/>
  <dbReference type="OrthoDB" id="4693858at2759"/>
  <dbReference type="Proteomes" id="UP000006718">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050544">
    <property type="entry name" value="Beta-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025933">
    <property type="entry name" value="Beta_defensin_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001:SF5">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001">
    <property type="entry name" value="BETA-DEFENSIN 123-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13841">
    <property type="entry name" value="Defensin_beta_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="19"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000006993" description="Beta-defensin 122">
    <location>
      <begin position="20"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="26"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="33"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="37"/>
      <end position="54"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="69" mass="7953" checksum="6C13A08A2C343768" modified="2005-02-15" version="1" precursor="true">MKPFLVTLAVLLLFFQVTAVGSIEKCWNFRGSCRDECLKNEKVYVFCMSGKLCCLKPKDQPHLPQRTKN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>