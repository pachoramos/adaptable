<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-01-10" modified="2024-03-27" version="50" xmlns="http://uniprot.org/uniprot">
  <accession>P84748</accession>
  <name>PEP16_ARGAU</name>
  <protein>
    <recommendedName>
      <fullName>Astacin-like peptidase p16</fullName>
      <ecNumber>3.4.24.-</ecNumber>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Argiope aurantia</name>
    <name type="common">Black-and-yellow garden spider</name>
    <dbReference type="NCBI Taxonomy" id="156844"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Araneae</taxon>
      <taxon>Araneomorphae</taxon>
      <taxon>Entelegynae</taxon>
      <taxon>Araneoidea</taxon>
      <taxon>Araneidae</taxon>
      <taxon>Argiope</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Comp. Biochem. Physiol." volume="143B" first="257" last="268">
      <title>Astacin family metallopeptidases and serine peptidase inhibitors in spider digestive fluid.</title>
      <authorList>
        <person name="Foradori M.J."/>
        <person name="Tillinghast E.K."/>
        <person name="Smith J.S."/>
        <person name="Townley M.A."/>
        <person name="Mooney R.E."/>
      </authorList>
      <dbReference type="PubMed" id="16458560"/>
      <dbReference type="DOI" id="10.1016/j.cbpb.2005.08.012"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>COFACTOR</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Active against casein. Has a role as a digestive enzyme.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="2">
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </cofactor>
  </comment>
  <dbReference type="EC" id="3.4.24.-"/>
  <dbReference type="AlphaFoldDB" id="P84748"/>
  <dbReference type="SMR" id="P84748"/>
  <dbReference type="BRENDA" id="3.4.24.21">
    <property type="organism ID" value="10238"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004222">
    <property type="term" value="F:metalloendopeptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006508">
    <property type="term" value="P:proteolysis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.390.10">
    <property type="entry name" value="Collagenase (Catalytic Domain)"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR024079">
    <property type="entry name" value="MetalloPept_cat_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001506">
    <property type="entry name" value="Peptidase_M12A"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10127:SF780">
    <property type="entry name" value="ASTACIN-LIKE METALLOENDOPEPTIDASE-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10127">
    <property type="entry name" value="DISCOIDIN, CUB, EGF, LAMININ , AND ZINC METALLOPROTEASE DOMAIN CONTAINING"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01400">
    <property type="entry name" value="Astacin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00480">
    <property type="entry name" value="ASTACIN"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF55486">
    <property type="entry name" value="Metalloproteases ('zincins'), catalytic domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51864">
    <property type="entry name" value="ASTACIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0482">Metalloprotease</keyword>
  <keyword id="KW-0645">Protease</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <feature type="chain" id="PRO_0000078186" description="Astacin-like peptidase p16">
    <location>
      <begin position="1"/>
      <end position="103" status="greater than"/>
    </location>
  </feature>
  <feature type="domain" description="Peptidase M12A" evidence="1">
    <location>
      <begin position="1"/>
      <end position="103" status="greater than"/>
    </location>
  </feature>
  <feature type="non-consecutive residues" evidence="3">
    <location>
      <begin position="37"/>
      <end position="38"/>
    </location>
  </feature>
  <feature type="non-terminal residue" evidence="3">
    <location>
      <position position="103"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01211"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="16458560"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source>
      <dbReference type="PubMed" id="16458560"/>
    </source>
  </evidence>
  <sequence length="103" mass="11832" checksum="10C25716B781405D" modified="2006-01-10" version="1" fragment="multiple">NAIPGQHYRWPGAKVPYVIDSSLQSNTGFIQRAFQNYALGFYHEQNRSDRDDYLIIYVDNVQKGMEFNFAKLAPSQNILYTTFDYGSIMIYGNDAFSRDGSPM</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>