<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-29" modified="2024-11-27" version="109" xmlns="http://uniprot.org/uniprot">
  <accession>Q7A0J4</accession>
  <name>PERR_STAAW</name>
  <protein>
    <recommendedName>
      <fullName>Peroxide-responsive repressor PerR</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">perR</name>
    <name type="ordered locus">MW1801</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain MW2)</name>
    <dbReference type="NCBI Taxonomy" id="196620"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2002" name="Lancet" volume="359" first="1819" last="1827">
      <title>Genome and virulence determinants of high virulence community-acquired MRSA.</title>
      <authorList>
        <person name="Baba T."/>
        <person name="Takeuchi F."/>
        <person name="Kuroda M."/>
        <person name="Yuzawa H."/>
        <person name="Aoki K."/>
        <person name="Oguchi A."/>
        <person name="Nagai Y."/>
        <person name="Iwama N."/>
        <person name="Asano K."/>
        <person name="Naimi T."/>
        <person name="Kuroda H."/>
        <person name="Cui L."/>
        <person name="Yamamoto K."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="12044378"/>
      <dbReference type="DOI" id="10.1016/s0140-6736(02)08713-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MW2</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Manganese-dependent repressor that controls a regulon of oxidative stress resistance and iron-storage proteins. May act as a hydrogen peroxide and organic hydroperoxide sensor (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Cytoplasm</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the Fur family.</text>
  </comment>
  <dbReference type="EMBL" id="BA000033">
    <property type="protein sequence ID" value="BAB95666.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_000110011.1">
    <property type="nucleotide sequence ID" value="NC_003923.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q7A0J4"/>
  <dbReference type="SMR" id="Q7A0J4"/>
  <dbReference type="KEGG" id="sam:MW1801"/>
  <dbReference type="HOGENOM" id="CLU_096072_4_2_9"/>
  <dbReference type="Proteomes" id="UP000000418">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003700">
    <property type="term" value="F:DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000976">
    <property type="term" value="F:transcription cis-regulatory region binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008270">
    <property type="term" value="F:zinc ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045892">
    <property type="term" value="P:negative regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:1900376">
    <property type="term" value="P:regulation of secondary metabolite biosynthetic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="CDD" id="cd07153">
    <property type="entry name" value="Fur_like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.10.10:FF:000147">
    <property type="entry name" value="Fur family transcriptional regulator"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.30.1490.190:FF:000003">
    <property type="entry name" value="Fur family transcriptional regulator"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.1490.190">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.10.10">
    <property type="entry name" value="Winged helix-like DNA-binding domain superfamily/Winged helix DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002481">
    <property type="entry name" value="FUR"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR043135">
    <property type="entry name" value="Fur_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036388">
    <property type="entry name" value="WH-like_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036390">
    <property type="entry name" value="WH_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33202:SF8">
    <property type="entry name" value="PEROXIDE-RESPONSIVE REPRESSOR PERR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33202">
    <property type="entry name" value="ZINC UPTAKE REGULATION PROTEIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01475">
    <property type="entry name" value="FUR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46785">
    <property type="entry name" value="Winged helix' DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0963">Cytoplasm</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0464">Manganese</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0678">Repressor</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <feature type="chain" id="PRO_0000289015" description="Peroxide-responsive repressor PerR">
    <location>
      <begin position="1"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="region of interest" description="DNA-binding" evidence="1">
    <location>
      <begin position="1"/>
      <end position="84"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="102"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="105"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="142"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="145"/>
    </location>
    <ligand>
      <name>Zn(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29105"/>
    </ligand>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="148" mass="17183" checksum="F46AE99B9F367E75" modified="2004-07-05" version="1">MSVEIESIEHELEESIASLRQAGVRITPQRQAILRYLISSHTHPTADEIYQALSPDFPNISVATIYNNLRVFKDIGIVKELTYGDSSSRFDFNTHNHYHIICEQCGKIVDFQYPQLNEIERLAQHMTDFDVTHHRMEIYGVCKECQDK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>