<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2016-11-02" modified="2024-07-24" version="64" xmlns="http://uniprot.org/uniprot">
  <accession>Q0CJ54</accession>
  <name>ATG_ASPTN</name>
  <protein>
    <recommendedName>
      <fullName evidence="7">Cytochrome P450 monooxygenase atG</fullName>
      <ecNumber evidence="4">1.-.-.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName evidence="7">Terreic acid biosynthesis cluster protein G</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="7" type="primary">atG</name>
    <name type="ORF">ATEG_06280</name>
  </gene>
  <organism>
    <name type="scientific">Aspergillus terreus (strain NIH 2624 / FGSC A1156)</name>
    <dbReference type="NCBI Taxonomy" id="341663"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Fungi</taxon>
      <taxon>Dikarya</taxon>
      <taxon>Ascomycota</taxon>
      <taxon>Pezizomycotina</taxon>
      <taxon>Eurotiomycetes</taxon>
      <taxon>Eurotiomycetidae</taxon>
      <taxon>Eurotiales</taxon>
      <taxon>Aspergillaceae</taxon>
      <taxon>Aspergillus</taxon>
      <taxon>Aspergillus subgen. Circumdati</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2005-09" db="EMBL/GenBank/DDBJ databases">
      <title>Annotation of the Aspergillus terreus NIH2624 genome.</title>
      <authorList>
        <person name="Birren B.W."/>
        <person name="Lander E.S."/>
        <person name="Galagan J.E."/>
        <person name="Nusbaum C."/>
        <person name="Devon K."/>
        <person name="Henn M."/>
        <person name="Ma L.-J."/>
        <person name="Jaffe D.B."/>
        <person name="Butler J."/>
        <person name="Alvarez P."/>
        <person name="Gnerre S."/>
        <person name="Grabherr M."/>
        <person name="Kleber M."/>
        <person name="Mauceli E.W."/>
        <person name="Brockman W."/>
        <person name="Rounsley S."/>
        <person name="Young S.K."/>
        <person name="LaButti K."/>
        <person name="Pushparaj V."/>
        <person name="DeCaprio D."/>
        <person name="Crawford M."/>
        <person name="Koehrsen M."/>
        <person name="Engels R."/>
        <person name="Montgomery P."/>
        <person name="Pearson M."/>
        <person name="Howarth C."/>
        <person name="Larson L."/>
        <person name="Luoma S."/>
        <person name="White J."/>
        <person name="Alvarado L."/>
        <person name="Kodira C.D."/>
        <person name="Zeng Q."/>
        <person name="Oleary S."/>
        <person name="Yandava C."/>
        <person name="Denning D.W."/>
        <person name="Nierman W.C."/>
        <person name="Milne T."/>
        <person name="Madden K."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>NIH 2624 / FGSC A1156</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1996" name="Mol. Gen. Genet." volume="253" first="1" last="10">
      <title>Cloning of the polyketide synthase gene atX from Aspergillus terreus and its identification as the 6-methylsalicylic acid synthase gene by heterologous expression.</title>
      <authorList>
        <person name="Fujii I."/>
        <person name="Ono Y."/>
        <person name="Tada H."/>
        <person name="Gomi K."/>
        <person name="Ebizuka Y."/>
        <person name="Sankawa U."/>
      </authorList>
      <dbReference type="PubMed" id="9003280"/>
      <dbReference type="DOI" id="10.1007/s004380050289"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1997" name="Folia Microbiol. (Praha)" volume="42" first="419" last="430">
      <title>Polyketide synthase gene pksM from Aspergillus terreus expressed during growth phase.</title>
      <authorList>
        <person name="Pazoutova S."/>
        <person name="Linka M."/>
        <person name="Storkova S."/>
        <person name="Schwab H."/>
      </authorList>
      <dbReference type="PubMed" id="9438344"/>
      <dbReference type="DOI" id="10.1007/bf02826548"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1999" name="Proc. Natl. Acad. Sci. U.S.A." volume="96" first="2227" last="2232">
      <title>Terreic acid, a quinone epoxide inhibitor of Bruton's tyrosine kinase.</title>
      <authorList>
        <person name="Kawakami Y."/>
        <person name="Hartman S.E."/>
        <person name="Kinoshita E."/>
        <person name="Suzuki H."/>
        <person name="Kitaura J."/>
        <person name="Yao L."/>
        <person name="Inagaki N."/>
        <person name="Franco A."/>
        <person name="Hata D."/>
        <person name="Maeda-Yamamoto M."/>
        <person name="Fukamachi H."/>
        <person name="Nagai H."/>
        <person name="Kawakami T."/>
      </authorList>
      <dbReference type="PubMed" id="10051623"/>
      <dbReference type="DOI" id="10.1073/pnas.96.5.2227"/>
    </citation>
    <scope>BIOTECHNOLOGY</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2014" name="J. Basic Microbiol." volume="54" first="322" last="326">
      <title>Differential antibacterial properties of the MurA inhibitors terreic acid and fosfomycin.</title>
      <authorList>
        <person name="Olesen S.H."/>
        <person name="Ingles D.J."/>
        <person name="Yang Y."/>
        <person name="Schoenbrunn E."/>
      </authorList>
      <dbReference type="PubMed" id="23686727"/>
      <dbReference type="DOI" id="10.1002/jobm.201200617"/>
    </citation>
    <scope>BIOTECHNOLOGY</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2014" name="J. Biotechnol." volume="175" first="53" last="62">
      <title>Culture-based and sequence-based insights into biosynthesis of secondary metabolites by Aspergillus terreus ATCC 20542.</title>
      <authorList>
        <person name="Boruta T."/>
        <person name="Bizukojc M."/>
      </authorList>
      <dbReference type="PubMed" id="24534845"/>
      <dbReference type="DOI" id="10.1016/j.jbiotec.2014.01.038"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2014" name="Org. Lett." volume="16" first="5250" last="5253">
      <title>Molecular genetic characterization of terreic acid pathway in Aspergillus terreus.</title>
      <authorList>
        <person name="Guo C.J."/>
        <person name="Sun W.W."/>
        <person name="Bruno K.S."/>
        <person name="Wang C.C."/>
      </authorList>
      <dbReference type="PubMed" id="25265334"/>
      <dbReference type="DOI" id="10.1021/ol502242a"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
  </reference>
  <comment type="function">
    <text evidence="4 5 6 9">Cytochrome P450 monooxygenase; part of the gene cluster that mediates the biosynthesis of terreic acid, a quinone epoxide inhibitor of Bruton's tyrosine kinase (BTK) (PubMed:24534845, PubMed:25265334). The first step of the pathway is the synthesis of 6-methylsalicylic acid (6-MSA) by the 6-methylsalicylic acid synthase atX (PubMed:25265334, PubMed:9003280, PubMed:9438344). In the biosynthesis of 6-MSA, atX utilizes one acetyl-CoA and three malonyl-CoAs as its substrates and catalyzes a series of programmed reactions including Claisen condensation, dehydration, reduction, and cyclization to yield 6-MSA (PubMed:25265334, PubMed:9003280, PubMed:9438344). The 6-methylsalicylic acid decarboxylase atA then catalyzes the decarboxylative hydroxylation of 6-MSA to 3-methylcatechol (PubMed:25265334). The next step is the conversion of 3-methylcatechol to terremutin via several oxidation steps involving the cytochrome P450 monooxygenase atE and probably also the cytochrome P450 monooxygenase atG (PubMed:25265334). Lastly, atC is required for the oxidation of terremutin to terreic acid (PubMed:25265334). No function could be assigned to atD yet, although it is involved in the biosynthesis of terreic acid (PubMed:25265334).</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="1">
      <name>heme</name>
      <dbReference type="ChEBI" id="CHEBI:30413"/>
    </cofactor>
  </comment>
  <comment type="pathway">
    <text evidence="4">Secondary metabolite biosynthesis.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="4">Abolishes the production of terreic acid (PubMed:25265334).</text>
  </comment>
  <comment type="biotechnology">
    <text evidence="2 3">Terreic acid is a metabolite with antibiotic properties (PubMed:23686727). Terric acid acts also as a selective inhibitor of human Bruton's tyrosine kinase in mast cells and other immune cells (PubMed:10051623).</text>
  </comment>
  <comment type="similarity">
    <text evidence="8">Belongs to the cytochrome P450 family.</text>
  </comment>
  <dbReference type="EC" id="1.-.-.-" evidence="4"/>
  <dbReference type="EMBL" id="CH476602">
    <property type="protein sequence ID" value="EAU32824.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_001215458.1">
    <property type="nucleotide sequence ID" value="XM_001215458.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q0CJ54"/>
  <dbReference type="STRING" id="341663.Q0CJ54"/>
  <dbReference type="EnsemblFungi" id="EAU32824">
    <property type="protein sequence ID" value="EAU32824"/>
    <property type="gene ID" value="ATEG_06280"/>
  </dbReference>
  <dbReference type="GeneID" id="4322429"/>
  <dbReference type="VEuPathDB" id="FungiDB:ATEG_06280"/>
  <dbReference type="eggNOG" id="KOG0159">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_001570_19_1_1"/>
  <dbReference type="OMA" id="LAYCEIN"/>
  <dbReference type="OrthoDB" id="1952162at2759"/>
  <dbReference type="Proteomes" id="UP000007963">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0020037">
    <property type="term" value="F:heme binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005506">
    <property type="term" value="F:iron ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004497">
    <property type="term" value="F:monooxygenase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016705">
    <property type="term" value="F:oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.630.10">
    <property type="entry name" value="Cytochrome P450"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001128">
    <property type="entry name" value="Cyt_P450"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002401">
    <property type="entry name" value="Cyt_P450_E_grp-I"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036396">
    <property type="entry name" value="Cyt_P450_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050121">
    <property type="entry name" value="Cytochrome_P450_monoxygenase"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR24305">
    <property type="entry name" value="CYTOCHROME P450"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR24305:SF242">
    <property type="entry name" value="CYTOCHROME P450 MONOOXYGENASE ATNE-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00067">
    <property type="entry name" value="p450"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00463">
    <property type="entry name" value="EP450I"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48264">
    <property type="entry name" value="Cytochrome P450"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0349">Heme</keyword>
  <keyword id="KW-0408">Iron</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0503">Monooxygenase</keyword>
  <keyword id="KW-0560">Oxidoreductase</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000437642" description="Cytochrome P450 monooxygenase atG">
    <location>
      <begin position="1"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="binding site" description="axial binding residue" evidence="1">
    <location>
      <position position="69"/>
    </location>
    <ligand>
      <name>heme</name>
      <dbReference type="ChEBI" id="CHEBI:30413"/>
    </ligand>
    <ligandPart>
      <name>Fe</name>
      <dbReference type="ChEBI" id="CHEBI:18248"/>
    </ligandPart>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P04798"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10051623"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="23686727"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="25265334"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="9003280"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="9438344"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="25265334"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <evidence type="ECO:0000305" key="9">
    <source>
      <dbReference type="PubMed" id="24534845"/>
    </source>
  </evidence>
  <sequence length="129" mass="14741" checksum="DE65716988A9584B" modified="2006-10-17" version="1">MTALQQFSRDGTFLSEALQTPISMNIWNTHYNKDFFPGPTEFWPERWMGEGTRELEKYLVPFGSGSRMCTGQNLSIAEQVLTIATLFRNYELELYQTTKKNVVMASYCMISLPGSESPGIQVKVRKTVQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>