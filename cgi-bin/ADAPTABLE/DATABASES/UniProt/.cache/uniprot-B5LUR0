<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2009-02-10" modified="2022-05-25" version="16" xmlns="http://uniprot.org/uniprot">
  <accession>B5LUR0</accession>
  <name>FALX8_LITFA</name>
  <protein>
    <recommendedName>
      <fullName evidence="6 8">Preprofallaxidin-8</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName>Fallaxidin-2.1</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Fallaxidin-3.2</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Fallaxidin-5.2</fullName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Litoria fallax</name>
    <name type="common">Eastern dwarf tree frog</name>
    <name type="synonym">Hylomantis fallax</name>
    <dbReference type="NCBI Taxonomy" id="115422"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Pelodryadinae</taxon>
      <taxon>Litoria</taxon>
    </lineage>
  </organism>
  <reference evidence="7 8" key="1">
    <citation type="journal article" date="2008" name="Rapid Commun. Mass Spectrom." volume="22" first="3207" last="3216">
      <title>The fallaxidin peptides from the skin secretion of the eastern dwarf tree frog Litoria fallax. Sequence determination by positive and negative ion electrospray mass spectrometry: antimicrobial activity and cDNA cloning of the fallaxidins.</title>
      <authorList>
        <person name="Jackway R.J."/>
        <person name="Bowie J.H."/>
        <person name="Bilusich D."/>
        <person name="Musgrave I.F."/>
        <person name="Surinya-Johnson K.H."/>
        <person name="Tyler M.J."/>
        <person name="Eichinger P.C.H."/>
      </authorList>
      <dbReference type="PubMed" id="18803332"/>
      <dbReference type="DOI" id="10.1002/rcm.3723"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 47-62; 71-75 AND 84-88</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-62; MET-75 AND MET-88</scope>
    <source>
      <tissue evidence="8">Skin</tissue>
      <tissue evidence="5">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="5">Fallaxidin-2.1 shows no antibacterial activity against Gram-positive or Gram-negative bacteria. Does not inhibit the formation of NO by neuronal nitric oxide synthase. Has no effect on splenocyte proliferation or smooth muscle contraction.</text>
  </comment>
  <comment type="function">
    <text evidence="5">Fallaxidin-3.2 shows antibacterial activity against the Gram-positive bacteria E.faecalis (MIC=100 uM) and L.lactis (MIC=500 uM). No antibacterial activity against the Gram-positive bacteria B.cereus, L.innocua, M.luteus, S.epidermidis, S.uberis and S.aureus, or the Gram-negative bacteria E.cloacae and E.coli.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="5">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="1678.0" method="Electrospray" evidence="5">
    <molecule>Fallaxidin-3.2</molecule>
    <text>The measured mass is that of Fallaxidin-3.2.</text>
  </comment>
  <comment type="mass spectrometry" mass="726.0" method="Electrospray" evidence="5">
    <molecule>Fallaxidin-2.1</molecule>
    <text>The measured mass is that of Fallaxidin-2.1.</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the frog skin active peptide (FSAP) family. Brevinin subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="EU912535">
    <property type="protein sequence ID" value="ACH53453.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="B5LUR0"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR004275">
    <property type="entry name" value="Frog_antimicrobial_propeptide"/>
  </dbReference>
  <dbReference type="Pfam" id="PF03032">
    <property type="entry name" value="FSAP_sig_propep"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3 8">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000361742" evidence="5">
    <location>
      <begin position="23"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000361743" description="Fallaxidin-3.2">
    <location>
      <begin position="47"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000361744" evidence="5">
    <location>
      <begin position="66"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000361745" description="Fallaxidin-2.1">
    <location>
      <begin position="71"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000361746" evidence="5">
    <location>
      <begin position="79"/>
      <end position="83"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000361747" description="Fallaxidin-2.1">
    <location>
      <begin position="84"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000361748" evidence="5">
    <location>
      <begin position="92"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000361749" description="Fallaxidin-5.2" evidence="1">
    <location>
      <begin position="97"/>
      <end position="108"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000361750" evidence="2">
    <location>
      <position position="108"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="4">
    <location>
      <begin position="27"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="5">
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="5">
    <location>
      <position position="75"/>
    </location>
  </feature>
  <feature type="modified residue" description="Methionine amide" evidence="5">
    <location>
      <position position="88"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P82881"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000256" key="4">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="18803332"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="18803332"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000312" key="8">
    <source>
      <dbReference type="EMBL" id="ACH53453.1"/>
    </source>
  </evidence>
  <sequence length="109" mass="12865" checksum="C3C9D06CE90EB1EC" modified="2008-10-14" version="1" precursor="true">MASLKKSLFLVLFLGLLSLSICEEQKRENEEDAEDENHEEESEEKRGLLDFAKHVIGIASKLGKRSEEKRFWPFMGKRSEEKRFWPFMGKRSEEKRFFRVLAKLGKLAK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>