<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1991-05-01" modified="2024-10-02" version="84" xmlns="http://uniprot.org/uniprot">
  <accession>P21252</accession>
  <name>COLI_LOXAF</name>
  <protein>
    <recommendedName>
      <fullName>Pro-opiomelanocortin</fullName>
      <shortName>POMC</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Corticotropin-lipotropin</fullName>
    </alternativeName>
    <component>
      <recommendedName>
        <fullName>Corticotropin</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Adrenocorticotropic hormone</fullName>
        <shortName>ACTH</shortName>
      </alternativeName>
    </component>
    <component>
      <recommendedName>
        <fullName>Melanocyte-stimulating hormone alpha</fullName>
        <shortName>Alpha-MSH</shortName>
      </recommendedName>
      <alternativeName>
        <fullName>Melanotropin alpha</fullName>
      </alternativeName>
    </component>
    <component>
      <recommendedName>
        <fullName>Corticotropin-like intermediary peptide</fullName>
        <shortName>CLIP</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Lipotropin beta</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Beta-LPH</fullName>
      </alternativeName>
    </component>
    <component>
      <recommendedName>
        <fullName>Lipotropin gamma</fullName>
      </recommendedName>
      <alternativeName>
        <fullName>Gamma-LPH</fullName>
      </alternativeName>
    </component>
    <component>
      <recommendedName>
        <fullName>Melanocyte-stimulating hormone beta</fullName>
        <shortName>Beta-MSH</shortName>
      </recommendedName>
      <alternativeName>
        <fullName>Melanotropin beta</fullName>
      </alternativeName>
    </component>
    <component>
      <recommendedName>
        <fullName>Beta-endorphin</fullName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName>Met-enkephalin</fullName>
      </recommendedName>
    </component>
  </protein>
  <gene>
    <name type="primary">POMC</name>
  </gene>
  <organism>
    <name type="scientific">Loxodonta africana</name>
    <name type="common">African elephant</name>
    <dbReference type="NCBI Taxonomy" id="9785"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Afrotheria</taxon>
      <taxon>Proboscidea</taxon>
      <taxon>Elephantidae</taxon>
      <taxon>Loxodonta</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1988" name="Int. J. Pept. Protein Res." volume="32" first="573" last="578">
      <title>Isolation and primary structures of elephant adrenocorticotropin and beta-lipotropin.</title>
      <authorList>
        <person name="Li C.H."/>
        <person name="Oosthuizen M.M.J."/>
        <person name="Chung D."/>
      </authorList>
      <dbReference type="PubMed" id="2854538"/>
      <dbReference type="DOI" id="10.1111/j.1399-3011.1988.tb01389.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
  </reference>
  <comment type="function">
    <molecule>Corticotropin</molecule>
    <text>Stimulates the adrenal glands to release cortisol.</text>
  </comment>
  <comment type="function">
    <molecule>Melanocyte-stimulating hormone alpha</molecule>
    <text>Anorexigenic peptide. Increases the pigmentation of skin by increasing melanin production in melanocytes.</text>
  </comment>
  <comment type="function">
    <molecule>Melanocyte-stimulating hormone beta</molecule>
    <text evidence="4">Increases the pigmentation of skin by increasing melanin production in melanocytes.</text>
  </comment>
  <comment type="function">
    <molecule>Beta-endorphin</molecule>
    <text>Endogenous orexigenic opiate.</text>
  </comment>
  <comment type="function">
    <molecule>Met-enkephalin</molecule>
    <text>Endogenous opiate.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
    <text evidence="4">Melanocyte-stimulating hormone alpha and beta-endorphin are stored in separate granules in hypothalamic POMC neurons, suggesting that secretion may be under the control of different regulatory mechanisms.</text>
  </comment>
  <comment type="tissue specificity">
    <text>ACTH and MSH are produced by the pituitary gland.</text>
  </comment>
  <comment type="PTM">
    <text>Specific enzymatic cleavages at paired basic residues yield the different active peptides.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the POMC family.</text>
  </comment>
  <comment type="caution">
    <text evidence="6">X's at positions 40-41 represent paired basic residues (probably Lys-Arg) assumed, by homology with the bovine sequence, to be present in the precursor molecule.</text>
  </comment>
  <dbReference type="PIR" id="JK0022">
    <property type="entry name" value="JK0022"/>
  </dbReference>
  <dbReference type="BMRB" id="P21252"/>
  <dbReference type="STRING" id="9785.ENSLAFP00000021554"/>
  <dbReference type="InParanoid" id="P21252"/>
  <dbReference type="Proteomes" id="UP000007646">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030141">
    <property type="term" value="C:secretory granule"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001664">
    <property type="term" value="F:G protein-coupled receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000852">
    <property type="term" value="P:regulation of corticosterone secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013531">
    <property type="entry name" value="Mcrtin_ACTH_cent"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013532">
    <property type="entry name" value="Opioid_neuropept"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001941">
    <property type="entry name" value="PMOC"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050878">
    <property type="entry name" value="POMC-derived_peptides"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11416">
    <property type="entry name" value="PRO-OPIOMELANOCORTIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11416:SF7">
    <property type="entry name" value="PRO-OPIOMELANOCORTIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00976">
    <property type="entry name" value="ACTH_domain"/>
    <property type="match status" value="2"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08035">
    <property type="entry name" value="Op_neuropeptide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00383">
    <property type="entry name" value="MELANOCORTIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM01363">
    <property type="entry name" value="ACTH_domain"/>
    <property type="match status" value="2"/>
  </dbReference>
  <dbReference type="SMART" id="SM01365">
    <property type="entry name" value="Op_neuropeptide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0007">Acetylation</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0257">Endorphin</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000024977" description="Corticotropin">
    <location>
      <begin position="1"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000024978" description="Melanocyte-stimulating hormone alpha">
    <location>
      <begin position="1"/>
      <end position="13"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000024979" description="Corticotropin-like intermediary peptide">
    <location>
      <begin position="19"/>
      <end position="39"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000024980" description="Lipotropin beta">
    <location>
      <begin position="42"/>
      <end position="134"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000024981" description="Lipotropin gamma">
    <location>
      <begin position="42"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000024982" description="Melanocyte-stimulating hormone beta">
    <location>
      <begin position="84"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000024983" description="Beta-endorphin">
    <location>
      <begin position="104"/>
      <end position="134"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000024984" description="Met-enkephalin">
    <location>
      <begin position="104"/>
      <end position="108"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="5">
    <location>
      <begin position="34"/>
      <end position="107"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="5">
    <location>
      <begin position="40"/>
      <end position="54"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="5">
    <location>
      <begin position="77"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="modified residue" description="N-acetylserine" evidence="3">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="2">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="1">
    <location>
      <position position="31"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01189"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P01190"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P01191"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="4">
    <source>
      <dbReference type="UniProtKB" id="P01193"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="5">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="134" mass="14935" checksum="F3561D74460562D5" modified="1991-05-01" version="1" precursor="true" fragment="single">SYSMEHFRWGKPVGKKRRPVKVYPNGAEGESAEAFPLEFXXELARERPEPARGPEGPDEGAATQADLDNGLVAEVEATSAEKKDEGPYKMEHFRWGSPAKDKRYGGFMTSEKSQTPLVTLFKNAIIKNAYKKGH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>