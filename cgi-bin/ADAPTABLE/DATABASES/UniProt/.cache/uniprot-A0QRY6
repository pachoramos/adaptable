<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-01-09" modified="2024-11-27" version="100" xmlns="http://uniprot.org/uniprot">
  <accession>A0QRY6</accession>
  <name>VAPC_MYCS2</name>
  <protein>
    <recommendedName>
      <fullName>Ribonuclease VapC</fullName>
      <shortName>RNase VapC</shortName>
      <ecNumber>3.1.-.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Endoribonuclease VapC</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Toxin VapC</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">vapC</name>
    <name type="ordered locus">MSMEG_1284</name>
    <name type="ordered locus">MSMEI_1246</name>
  </gene>
  <organism>
    <name type="scientific">Mycolicibacterium smegmatis (strain ATCC 700084 / mc(2)155)</name>
    <name type="common">Mycobacterium smegmatis</name>
    <dbReference type="NCBI Taxonomy" id="246196"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Actinomycetota</taxon>
      <taxon>Actinomycetes</taxon>
      <taxon>Mycobacteriales</taxon>
      <taxon>Mycobacteriaceae</taxon>
      <taxon>Mycolicibacterium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2006-10" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Fleischmann R.D."/>
        <person name="Dodson R.J."/>
        <person name="Haft D.H."/>
        <person name="Merkel J.S."/>
        <person name="Nelson W.C."/>
        <person name="Fraser C.M."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 700084 / mc(2)155</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2007" name="Genome Biol." volume="8" first="R20.1" last="R20.9">
      <title>Interrupted coding sequences in Mycobacterium smegmatis: authentic mutations or sequencing errors?</title>
      <authorList>
        <person name="Deshayes C."/>
        <person name="Perrodou E."/>
        <person name="Gallien S."/>
        <person name="Euphrasie D."/>
        <person name="Schaeffer C."/>
        <person name="Van-Dorsselaer A."/>
        <person name="Poch O."/>
        <person name="Lecompte O."/>
        <person name="Reyrat J.-M."/>
      </authorList>
      <dbReference type="PubMed" id="17295914"/>
      <dbReference type="DOI" id="10.1186/gb-2007-8-2-r20"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 700084 / mc(2)155</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2009" name="Genome Res." volume="19" first="128" last="135">
      <title>Ortho-proteogenomics: multiple proteomes investigation through orthology and a new MS-based protocol.</title>
      <authorList>
        <person name="Gallien S."/>
        <person name="Perrodou E."/>
        <person name="Carapito C."/>
        <person name="Deshayes C."/>
        <person name="Reyrat J.-M."/>
        <person name="Van Dorsselaer A."/>
        <person name="Poch O."/>
        <person name="Schaeffer C."/>
        <person name="Lecompte O."/>
      </authorList>
      <dbReference type="PubMed" id="18955433"/>
      <dbReference type="DOI" id="10.1101/gr.081901.108"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 700084 / mc(2)155</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2009" name="J. Mol. Biol." volume="390" first="353" last="367">
      <title>The vapBC operon from Mycobacterium smegmatis is an autoregulated toxin-antitoxin module that controls growth via inhibition of translation.</title>
      <authorList>
        <person name="Robson J."/>
        <person name="McKenzie J.L."/>
        <person name="Cursons R."/>
        <person name="Cook G.M."/>
        <person name="Arcus V.L."/>
      </authorList>
      <dbReference type="PubMed" id="19445953"/>
      <dbReference type="DOI" id="10.1016/j.jmb.2009.05.006"/>
    </citation>
    <scope>FUNCTION AS A TOXIN</scope>
    <scope>SUBUNIT</scope>
    <scope>INDUCTION</scope>
    <scope>DNA-BINDING</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>ATCC 700084 / mc(2)155</strain>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="J. Bacteriol." volume="194" first="2189" last="2204">
      <title>A VapBC toxin-antitoxin module is a posttranscriptional regulator of metabolic flux in mycobacteria.</title>
      <authorList>
        <person name="McKenzie J.L."/>
        <person name="Robson J."/>
        <person name="Berney M."/>
        <person name="Smith T.C."/>
        <person name="Ruthe A."/>
        <person name="Gardner P.P."/>
        <person name="Arcus V.L."/>
        <person name="Cook G.M."/>
      </authorList>
      <dbReference type="PubMed" id="22366418"/>
      <dbReference type="DOI" id="10.1128/jb.06790-11"/>
    </citation>
    <scope>FUNCTION AS AN ENDORIBONUCLEASE</scope>
    <scope>SUBSTRATES</scope>
    <scope>SUBUNIT</scope>
    <scope>COFACTOR</scope>
    <scope>RNA-BINDING</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>ATCC 700084 / mc(2)155</strain>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2012" name="J. Biol. Chem." volume="287" first="5340" last="5356">
      <title>Toxin-antitoxin systems of Mycobacterium smegmatis are essential for cell survival.</title>
      <authorList>
        <person name="Frampton R."/>
        <person name="Aggio R.B."/>
        <person name="Villas-Boas S.G."/>
        <person name="Arcus V.L."/>
        <person name="Cook G.M."/>
      </authorList>
      <dbReference type="PubMed" id="22199354"/>
      <dbReference type="DOI" id="10.1074/jbc.m111.286856"/>
    </citation>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>ATCC 700084 / mc(2)155</strain>
    </source>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2014" name="FEMS Microbiol. Lett." volume="352" first="69" last="77">
      <title>Toxin-antitoxin vapBC locus participates in formation of the dormant state in Mycobacterium smegmatis.</title>
      <authorList>
        <person name="Demidenok O.I."/>
        <person name="Kaprelyants A.S."/>
        <person name="Goncharenko A.V."/>
      </authorList>
      <dbReference type="PubMed" id="24417293"/>
      <dbReference type="DOI" id="10.1111/1574-6968.12380"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DISRUPTION PHENOTYPE</scope>
    <source>
      <strain>ATCC 700084 / mc(2)155</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="2 4 5">Toxic component of a type II toxin-antitoxin (TA) system. A sequence-specific endoribonuclease, cleavage occurs after the first AU in the consensus sequence AUA(U/A); RNA secondary structure is probably important in substrate choice. Cuts in 5' and 3' UTRs. The TA system acts as a post-transcriptional regulator of carbon metabolism; in M.smegmatis 3 TA systems (VapB-VapC, MazE-MazF and Phd-Doc) may be involved in monitoring the nutritional supply and physiological state of the cell, linking catabolic with anabolic reactions. When overexpressed inhibits cell growth, via translation; 10-fold in a wild-type strain, 100-fold in a vapB-vapC disruption mutant (PubMed:19445953). Overexpression of VapC leads to differential expression of 205 genes; many down-regulated genes are predicted to be involved in the transport and catabolism of carbohydrates, while genes involved in phosphate transport and scavenging are up-regulated. VapC is bacteriostatic, not bacteriocidal; cells are ovoid, non-replicating and have decreased transcription, becoming dormant (PubMed:24417293). Digests ssRNA but not tRNA, dsRNA, ssDNA or dsDNA. The VapB-VapC complex binds its own promoter DNA, and VapC alone binds RNA.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="4">
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </cofactor>
  </comment>
  <comment type="subunit">
    <text evidence="2 4">Forms a complex with VapB.</text>
  </comment>
  <comment type="induction">
    <text evidence="2">Expression is low but constitutive, and repressed by VapB-VapC. Translation of vapC mRNA requires VapB. Member of the vapB-vapC operon.</text>
  </comment>
  <comment type="disruption phenotype">
    <text evidence="2 3 4 5">The vapB-vapC operon is not essential; cells grow faster than wild-type on rich and minimal glycerol-containing medium. The operon deletion mutants die faster than wild-type under potassium-limiting conditions, which is prevented by overexpression of vapB (PubMed:24417293). The vapB antitoxin gene can be disrupted without causing death, however despite elevated vapC transcription, no VapC protein could be detected, suggesting that RNA processing and translational coupling are important in VapC expression. A triple TA mutant (missing vapB-vapC, mazE-mazF and phd-doc TA systems) survives antibiotic challenge, suggesting the TA systems are not required to generate drug-resistant cells. However the triple mutant is more sensitive to oxidative and heat stress, and does not survive long-term starvation during aerobic growth on complex medium. There is a difference in the level of branched-chain amino acids, which may play a role in monitoring the nutritional supply and physiological state of the cell.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the PINc/VapC protein family.</text>
  </comment>
  <dbReference type="EC" id="3.1.-.-"/>
  <dbReference type="EMBL" id="CP000480">
    <property type="protein sequence ID" value="ABK74630.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP001663">
    <property type="protein sequence ID" value="AFP37722.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_011727564.1">
    <property type="nucleotide sequence ID" value="NZ_SIJM01000042.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="YP_885674.1">
    <property type="nucleotide sequence ID" value="NC_008596.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A0QRY6"/>
  <dbReference type="SMR" id="A0QRY6"/>
  <dbReference type="STRING" id="246196.MSMEG_1284"/>
  <dbReference type="PaxDb" id="246196-MSMEI_1246"/>
  <dbReference type="GeneID" id="66732746"/>
  <dbReference type="KEGG" id="msb:LJ00_06395"/>
  <dbReference type="KEGG" id="msg:MSMEI_1246"/>
  <dbReference type="KEGG" id="msm:MSMEG_1284"/>
  <dbReference type="PATRIC" id="fig|246196.19.peg.1273"/>
  <dbReference type="eggNOG" id="COG3742">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="OrthoDB" id="32625at2"/>
  <dbReference type="Proteomes" id="UP000000757">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000006158">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0000287">
    <property type="term" value="F:magnesium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003723">
    <property type="term" value="F:RNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004540">
    <property type="term" value="F:RNA nuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd09871">
    <property type="entry name" value="PIN_MtVapC28-VapC30-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.40.50.1010">
    <property type="entry name" value="5'-nuclease"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_00265">
    <property type="entry name" value="VapC_Nob1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR029060">
    <property type="entry name" value="PIN-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002716">
    <property type="entry name" value="PIN_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050556">
    <property type="entry name" value="Type_II_TA_system_RNase"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR022907">
    <property type="entry name" value="VapC_family"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33653">
    <property type="entry name" value="RIBONUCLEASE VAPC2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33653:SF1">
    <property type="entry name" value="RIBONUCLEASE VAPC2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF01850">
    <property type="entry name" value="PIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF88723">
    <property type="entry name" value="PIN domain-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0460">Magnesium</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0694">RNA-binding</keyword>
  <keyword id="KW-1277">Toxin-antitoxin system</keyword>
  <feature type="chain" id="PRO_0000420845" description="Ribonuclease VapC">
    <location>
      <begin position="1"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="domain" description="PINc">
    <location>
      <begin position="1"/>
      <end position="124"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="4"/>
    </location>
    <ligand>
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="99"/>
    </location>
    <ligand>
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </ligand>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="19445953"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22199354"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="22366418"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="24417293"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="129" mass="13910" checksum="4AF6CB50CB382317" modified="2007-01-09" version="1">MVIDTSALVAILTDEPDAELLEGAVADDPVRTMSTASYLETAIVIESRFGEPGGRELDLWLHRASVALVAVDADQADAARLAYRRYGKGRHRAGLNYGDCFSYALAKVSGQPLLFKGEAFRLTDVAAVH</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>