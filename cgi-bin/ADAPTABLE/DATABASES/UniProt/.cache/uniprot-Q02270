<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-04-01" modified="2024-07-24" version="102" xmlns="http://uniprot.org/uniprot">
  <accession>Q02270</accession>
  <name>VE6_PCPV1</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein E6</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">E6</name>
  </gene>
  <organism>
    <name type="scientific">Pygmy chimpanzee papillomavirus type 1</name>
    <name type="common">PCPV-1</name>
    <dbReference type="NCBI Taxonomy" id="10576"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Monodnaviria</taxon>
      <taxon>Shotokuvirae</taxon>
      <taxon>Cossaviricota</taxon>
      <taxon>Papovaviricetes</taxon>
      <taxon>Zurhausenvirales</taxon>
      <taxon>Papillomaviridae</taxon>
      <taxon>Firstpapillomavirinae</taxon>
      <taxon>Alphapapillomavirus</taxon>
      <taxon>Alphapapillomavirus 10</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Pan paniscus</name>
    <name type="common">Pygmy chimpanzee</name>
    <name type="synonym">Bonobo</name>
    <dbReference type="NCBI Taxonomy" id="9597"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1992" name="Virology" volume="190" first="587" last="596">
      <title>Human papillomavirus type 13 and pygmy chimpanzee papillomavirus type 1: comparison of the genome organizations.</title>
      <authorList>
        <person name="van Ranst M."/>
        <person name="Fuse A."/>
        <person name="Fiten P."/>
        <person name="Beuken E."/>
        <person name="Pfister H."/>
        <person name="Burk R.D."/>
        <person name="Opdenakker G."/>
      </authorList>
      <dbReference type="PubMed" id="1325697"/>
      <dbReference type="DOI" id="10.1016/0042-6822(92)90896-w"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays a major role in the induction and maintenance of cellular transformation. E6 associates with host UBE3A/E6-AP ubiquitin-protein ligase and modulates its activity. Sequesters tumor suppressor TP53 in the host cytoplasm and modulates its activity by interacting with host EP300 that results in the reduction of TP53 acetylation and activation. In turn, apoptosis induced by DNA damage is inhibited. E6 protects also host keratinocytes from apoptosis by mediating the degradation of host BAK1. May also inhibit host immune response.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Forms homodimers. Interacts with ubiquitin-protein ligase UBE3A/E6-AP; this interaction stimulates UBE3A ubiquitin activity. Interacts with host TP53 and EP300; this interaction inhibits TP53 activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
    </subcellularLocation>
  </comment>
  <comment type="miscellaneous">
    <text evidence="1">Belongs to the low risk human alphapapillomavirus family. The cancer-causing human papillomavirus E6 protein has a unique carboxy terminal PDZ domain containing substrate but low risk E6s do not possess this domain.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the papillomaviridae E6 protein family.</text>
  </comment>
  <dbReference type="EMBL" id="X62844">
    <property type="protein sequence ID" value="CAA44655.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="SMR" id="Q02270"/>
  <dbReference type="Proteomes" id="UP000000469">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030430">
    <property type="term" value="C:host cell cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042025">
    <property type="term" value="C:host cell nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006351">
    <property type="term" value="P:DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006355">
    <property type="term" value="P:regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052150">
    <property type="term" value="P:symbiont-mediated perturbation of host apoptosis"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039648">
    <property type="term" value="P:symbiont-mediated perturbation of host ubiquitin-like protein modification"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039502">
    <property type="term" value="P:symbiont-mediated suppression of host type I interferon-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.240.40">
    <property type="entry name" value="E6 early regulatory protein"/>
    <property type="match status" value="2"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_04006">
    <property type="entry name" value="HPV_E6"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001334">
    <property type="entry name" value="E6"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR038575">
    <property type="entry name" value="E6_sf"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00518">
    <property type="entry name" value="E6"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF161229">
    <property type="entry name" value="E6 C-terminal domain-like"/>
    <property type="match status" value="2"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0010">Activator</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0244">Early protein</keyword>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-1119">Modulation of host cell apoptosis by virus</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <keyword id="KW-0863">Zinc-finger</keyword>
  <feature type="chain" id="PRO_0000133384" description="Protein E6">
    <location>
      <begin position="1"/>
      <end position="150"/>
    </location>
  </feature>
  <feature type="zinc finger region" evidence="1">
    <location>
      <begin position="31"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="zinc finger region" evidence="1">
    <location>
      <begin position="104"/>
      <end position="140"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_04006"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="150" mass="17492" checksum="668EFB1512AC439C" modified="1993-04-01" version="1">MEKANASTSAKTIDQLCKECNLCMHSLQILCVFCRKTLSTAEVYAFQYKDLNIVWQGNFPFAACACCLEIQGKVNQYRHFDFAAYAVTVEEEINKSIFDVRIRCYLCHKPLCDVEKLRHILEKARFIKLNCEWKGRCFHCWTSCMENILP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>