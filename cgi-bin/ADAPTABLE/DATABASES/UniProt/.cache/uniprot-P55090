<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1996-10-01" modified="2024-11-27" version="151" xmlns="http://uniprot.org/uniprot">
  <accession>P55090</accession>
  <name>UCN1_RAT</name>
  <protein>
    <recommendedName>
      <fullName>Urocortin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Corticotensin</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">Ucn</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1995" name="Nature" volume="378" first="287" last="292">
      <title>Urocortin, a mammalian neuropeptide related to fish urotensin I and to corticotropin-releasing factor.</title>
      <authorList>
        <person name="Vaughan J.M."/>
        <person name="Donaldson C.J."/>
        <person name="Bittencourt J."/>
        <person name="Perrin M.H."/>
        <person name="Lewis K.A."/>
        <person name="Sutton S.W."/>
        <person name="Chan R."/>
        <person name="Turnbull A."/>
        <person name="Lovejoy D."/>
        <person name="Rivier C."/>
        <person name="Rivier J.E."/>
        <person name="Sawchenko P."/>
        <person name="Vale W.W."/>
      </authorList>
      <dbReference type="PubMed" id="7477349"/>
      <dbReference type="DOI" id="10.1038/378287a0"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <source>
      <strain>Sprague-Dawley</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="submission" date="1998-09" db="EMBL/GenBank/DDBJ databases">
      <title>Detection of rat urocortin in lymphoid tissues: implications for the functional assessment of urocortin as a novel neuro-immunomodulatory peptide.</title>
      <authorList>
        <person name="Park J.H."/>
        <person name="Lee Y.J."/>
        <person name="Kim K.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE</scope>
    <source>
      <strain>Lewis</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2011" name="Am. J. Physiol." volume="301" first="E72" last="82">
      <title>Urocortin 1 reduces food intake and ghrelin secretion via CRF(2) receptors.</title>
      <authorList>
        <person name="Yakabi K."/>
        <person name="Noguchi M."/>
        <person name="Ohno S."/>
        <person name="Ro S."/>
        <person name="Onouchi T."/>
        <person name="Ochiai M."/>
        <person name="Takabayashi H."/>
        <person name="Takayama K."/>
        <person name="Harada Y."/>
        <person name="Sadakane C."/>
        <person name="Hattori T."/>
      </authorList>
      <dbReference type="PubMed" id="21540451"/>
      <dbReference type="DOI" id="10.1152/ajpendo.00695.2010"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 5 6">Acts in vitro to stimulate the secretion of adrenocorticotropic hormone (ACTH) (PubMed:7477349). Binds with high affinity to CRF receptor types 1, 2-alpha, and 2-beta (By similarity). Plays a role in the establishment of normal hearing thresholds (By similarity). Reduces food intake and regulates ghrelin levels in gastric body and plasma (PubMed:21540451).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with CRHR1 and CRHR2 (via their N-terminal extracellular domain).</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-9030248">
      <id>P55090</id>
    </interactant>
    <interactant intactId="EBI-9030306">
      <id>P35353</id>
      <label>Crhr1</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the sauvagine/corticotropin-releasing factor/urotensin I family.</text>
  </comment>
  <dbReference type="EMBL" id="U33935">
    <property type="protein sequence ID" value="AAA87566.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF093623">
    <property type="protein sequence ID" value="AAF63153.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="S60262">
    <property type="entry name" value="S60262"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_062023.1">
    <property type="nucleotide sequence ID" value="NM_019150.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P55090"/>
  <dbReference type="SMR" id="P55090"/>
  <dbReference type="BioGRID" id="247834">
    <property type="interactions" value="5"/>
  </dbReference>
  <dbReference type="IntAct" id="P55090">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="10116.ENSRNOP00000008037"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000008037"/>
  <dbReference type="Ensembl" id="ENSRNOT00000008037.3">
    <property type="protein sequence ID" value="ENSRNOP00000008037.1"/>
    <property type="gene ID" value="ENSRNOG00000006090.3"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055033939">
    <property type="protein sequence ID" value="ENSRNOP00055027561"/>
    <property type="gene ID" value="ENSRNOG00055019865"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060022619">
    <property type="protein sequence ID" value="ENSRNOP00060017939"/>
    <property type="gene ID" value="ENSRNOG00060013274"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065039111">
    <property type="protein sequence ID" value="ENSRNOP00065031765"/>
    <property type="gene ID" value="ENSRNOG00065022893"/>
  </dbReference>
  <dbReference type="GeneID" id="29151"/>
  <dbReference type="KEGG" id="rno:29151"/>
  <dbReference type="AGR" id="RGD:3929"/>
  <dbReference type="CTD" id="7349"/>
  <dbReference type="RGD" id="3929">
    <property type="gene designation" value="Ucn"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502S63E">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000154473"/>
  <dbReference type="HOGENOM" id="CLU_138901_0_0_1"/>
  <dbReference type="InParanoid" id="P55090"/>
  <dbReference type="OMA" id="QNRIVFD"/>
  <dbReference type="OrthoDB" id="5353151at2759"/>
  <dbReference type="PhylomeDB" id="P55090"/>
  <dbReference type="TreeFam" id="TF332956"/>
  <dbReference type="Reactome" id="R-RNO-373080">
    <property type="pathway name" value="Class B/2 (Secretin family receptors)"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P55090"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 6"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000006090">
    <property type="expression patterns" value="Expressed in skeletal muscle tissue and 7 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030424">
    <property type="term" value="C:axon"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043679">
    <property type="term" value="C:axon terminus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030425">
    <property type="term" value="C:dendrite"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043025">
    <property type="term" value="C:neuronal cell body"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043204">
    <property type="term" value="C:perikaryon"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043196">
    <property type="term" value="C:varicosity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051430">
    <property type="term" value="F:corticotropin-releasing hormone receptor 1 binding"/>
    <property type="evidence" value="ECO:0000353"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051431">
    <property type="term" value="F:corticotropin-releasing hormone receptor 2 binding"/>
    <property type="evidence" value="ECO:0000353"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001664">
    <property type="term" value="F:G protein-coupled receptor binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046811">
    <property type="term" value="F:histone deacetylase inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009060">
    <property type="term" value="P:aerobic respiration"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008306">
    <property type="term" value="P:associative learning"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042756">
    <property type="term" value="P:drinking behavior"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007631">
    <property type="term" value="P:feeding behavior"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007565">
    <property type="term" value="P:female pregnancy"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007611">
    <property type="term" value="P:learning or memory"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043066">
    <property type="term" value="P:negative regulation of apoptotic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032099">
    <property type="term" value="P:negative regulation of appetite"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045776">
    <property type="term" value="P:negative regulation of blood pressure"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045792">
    <property type="term" value="P:negative regulation of cell size"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000252">
    <property type="term" value="P:negative regulation of feeding behavior"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010629">
    <property type="term" value="P:negative regulation of gene expression"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046888">
    <property type="term" value="P:negative regulation of hormone secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043524">
    <property type="term" value="P:negative regulation of neuron apoptotic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031175">
    <property type="term" value="P:neuron projection development"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000987">
    <property type="term" value="P:positive regulation of behavioral fear response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090280">
    <property type="term" value="P:positive regulation of calcium ion import"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043950">
    <property type="term" value="P:positive regulation of cAMP-mediated signaling"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060452">
    <property type="term" value="P:positive regulation of cardiac muscle contraction"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030307">
    <property type="term" value="P:positive regulation of cell growth"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032967">
    <property type="term" value="P:positive regulation of collagen biosynthetic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051461">
    <property type="term" value="P:positive regulation of corticotropin secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045740">
    <property type="term" value="P:positive regulation of DNA replication"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010628">
    <property type="term" value="P:positive regulation of gene expression"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032755">
    <property type="term" value="P:positive regulation of interleukin-6 production"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045944">
    <property type="term" value="P:positive regulation of transcription by RNA polymerase II"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045727">
    <property type="term" value="P:positive regulation of translation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043117">
    <property type="term" value="P:positive regulation of vascular permeability"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051966">
    <property type="term" value="P:regulation of synaptic transmission, glutamatergic"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010996">
    <property type="term" value="P:response to auditory stimulus"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032355">
    <property type="term" value="P:response to estradiol"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051384">
    <property type="term" value="P:response to glucocorticoid"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006979">
    <property type="term" value="P:response to oxidative stress"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048265">
    <property type="term" value="P:response to pain"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007605">
    <property type="term" value="P:sensory perception of sound"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035176">
    <property type="term" value="P:social behavior"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001964">
    <property type="term" value="P:startle response"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042311">
    <property type="term" value="P:vasodilation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="Gene3D" id="6.10.250.1920">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018446">
    <property type="entry name" value="Corticotropin-releasing_fac_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000187">
    <property type="entry name" value="CRF"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003620">
    <property type="entry name" value="Urocortin_CRF"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15035">
    <property type="entry name" value="CORTICOLIBERIN/UROCORTIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15035:SF11">
    <property type="entry name" value="UROCORTIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00473">
    <property type="entry name" value="CRF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR01612">
    <property type="entry name" value="CRFFAMILY"/>
  </dbReference>
  <dbReference type="SMART" id="SM00039">
    <property type="entry name" value="CRF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00511">
    <property type="entry name" value="CRF"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-1009">Hearing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="4">
    <location>
      <begin position="1"/>
      <end position="25"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000006237">
    <location>
      <begin position="26"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006238" description="Urocortin">
    <location>
      <begin position="81"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="1">
    <location>
      <position position="120"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P55089"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P81615"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="4"/>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="21540451"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="7477349"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <sequence length="122" mass="13711" checksum="9F0AF834CBFFCE74" modified="1996-10-01" version="1" precursor="true">MRQRGRATLLVALLLLVQLRPESSQWSPAAAAANVVQDPNLRWNPGVRNQGGGVRALLLLLAERFPRRAGSEPAGERQRRDDPPLSIDLTFHLLRTLLELARTQSQRERAEQNRIIFDSVGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>