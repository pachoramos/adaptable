<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2012-04-18" modified="2024-03-27" version="104" xmlns="http://uniprot.org/uniprot">
  <accession>Q97YC2</accession>
  <name>CAS2A_SACS2</name>
  <protein>
    <recommendedName>
      <fullName>CRISPR-associated endoribonuclease Cas2 1</fullName>
      <ecNumber>3.1.-.-</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>CAS2 endoribonuclease</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">cas21</name>
    <name type="ordered locus">SSO1404</name>
  </gene>
  <organism>
    <name type="scientific">Saccharolobus solfataricus (strain ATCC 35092 / DSM 1617 / JCM 11322 / P2)</name>
    <name type="common">Sulfolobus solfataricus</name>
    <dbReference type="NCBI Taxonomy" id="273057"/>
    <lineage>
      <taxon>Archaea</taxon>
      <taxon>Thermoproteota</taxon>
      <taxon>Thermoprotei</taxon>
      <taxon>Sulfolobales</taxon>
      <taxon>Sulfolobaceae</taxon>
      <taxon>Saccharolobus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2001" name="Proc. Natl. Acad. Sci. U.S.A." volume="98" first="7835" last="7840">
      <title>The complete genome of the crenarchaeon Sulfolobus solfataricus P2.</title>
      <authorList>
        <person name="She Q."/>
        <person name="Singh R.K."/>
        <person name="Confalonieri F."/>
        <person name="Zivanovic Y."/>
        <person name="Allard G."/>
        <person name="Awayez M.J."/>
        <person name="Chan-Weiher C.C.-Y."/>
        <person name="Clausen I.G."/>
        <person name="Curtis B.A."/>
        <person name="De Moors A."/>
        <person name="Erauso G."/>
        <person name="Fletcher C."/>
        <person name="Gordon P.M.K."/>
        <person name="Heikamp-de Jong I."/>
        <person name="Jeffries A.C."/>
        <person name="Kozera C.J."/>
        <person name="Medina N."/>
        <person name="Peng X."/>
        <person name="Thi-Ngoc H.P."/>
        <person name="Redder P."/>
        <person name="Schenk M.E."/>
        <person name="Theriault C."/>
        <person name="Tolstrup N."/>
        <person name="Charlebois R.L."/>
        <person name="Doolittle W.F."/>
        <person name="Duguet M."/>
        <person name="Gaasterland T."/>
        <person name="Garrett R.A."/>
        <person name="Ragan M.A."/>
        <person name="Sensen C.W."/>
        <person name="Van der Oost J."/>
      </authorList>
      <dbReference type="PubMed" id="11427726"/>
      <dbReference type="DOI" id="10.1073/pnas.141222098"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>ATCC 35092 / DSM 1617 / JCM 11322 / P2</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="J. Biol. Chem." volume="283" first="20361" last="20371">
      <title>A novel family of sequence-specific endoribonucleases associated with the clustered regularly interspaced short palindromic repeats.</title>
      <authorList>
        <person name="Beloglazova N."/>
        <person name="Brown G."/>
        <person name="Zimmerman M.D."/>
        <person name="Proudfoot M."/>
        <person name="Makarova K.S."/>
        <person name="Kudritska M."/>
        <person name="Kochinyan S."/>
        <person name="Wang S."/>
        <person name="Chruszcz M."/>
        <person name="Minor W."/>
        <person name="Koonin E.V."/>
        <person name="Edwards A.M."/>
        <person name="Savchenko A."/>
        <person name="Yakunin A.F."/>
      </authorList>
      <dbReference type="PubMed" id="18482976"/>
      <dbReference type="DOI" id="10.1074/jbc.m803225200"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.59 ANGSTROMS)</scope>
    <scope>FUNCTION AS A SSRNA-SPECIFIC ENDORIBONUCLEASE</scope>
    <scope>SUBSTRATE SPECIFICITY</scope>
    <scope>PH-DEPENDENCE</scope>
    <scope>COFACTOR</scope>
    <scope>SUBUNIT</scope>
    <scope>MUTAGENESIS OF TYR-9; ASP-10; ARG-17; ARG-19; ARG-31 AND PHE-37</scope>
    <source>
      <strain>ATCC 35092 / DSM 1617 / JCM 11322 / P2</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2010" name="J. Struct. Funct. Genomics" volume="11" first="167" last="180">
      <title>The Scottish Structural Proteomics Facility: targets, methods and outputs.</title>
      <authorList>
        <person name="Oke M."/>
        <person name="Carter L.G."/>
        <person name="Johnson K.A."/>
        <person name="Liu H."/>
        <person name="McMahon S.A."/>
        <person name="Yan X."/>
        <person name="Kerou M."/>
        <person name="Weikart N.D."/>
        <person name="Kadi N."/>
        <person name="Sheikh M.A."/>
        <person name="Schmelz S."/>
        <person name="Dorward M."/>
        <person name="Zawadzki M."/>
        <person name="Cozens C."/>
        <person name="Falconer H."/>
        <person name="Powers H."/>
        <person name="Overton I.M."/>
        <person name="van Niekerk C.A."/>
        <person name="Peng X."/>
        <person name="Patel P."/>
        <person name="Garrett R.A."/>
        <person name="Prangishvili D."/>
        <person name="Botting C.H."/>
        <person name="Coote P.J."/>
        <person name="Dryden D.T."/>
        <person name="Barton G.J."/>
        <person name="Schwarz-Linek U."/>
        <person name="Challis G.L."/>
        <person name="Taylor G.L."/>
        <person name="White M.F."/>
        <person name="Naismith J.H."/>
      </authorList>
      <dbReference type="PubMed" id="20419351"/>
      <dbReference type="DOI" id="10.1007/s10969-010-9090-y"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.40 ANGSTROMS) OF 2-101</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3">CRISPR (clustered regularly interspaced short palindromic repeat), is an adaptive immune system that provides protection against mobile genetic elements (viruses, transposable elements and conjugative plasmids). CRISPR clusters contain sequences complementary to antecedent mobile elements and target invading nucleic acids. CRISPR clusters are transcribed and processed into CRISPR RNA (crRNA). Involved in the integration of spacer DNA into the CRISPR cassette (By similarity). Functions as a ssRNA-specific endoribonuclease, producing a 5'-phosphomonoester and a 3'-hydroxy. Does not process pre-crRNA in the manner expected if it were the CRISPR-processing endoribonuclease. Prefers U-rich substrates and often cuts between adjacent U residues in regions predicted to be single-stranded. RNAs as short as 10 residues can serve as substrate.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="3">
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
    </cofactor>
  </comment>
  <comment type="biophysicochemical properties">
    <phDependence>
      <text>Optimum pH is 8.5-9.0.</text>
    </phDependence>
  </comment>
  <comment type="subunit">
    <text evidence="1 3">Forms a heterotetramer with a Cas1 homodimer (By similarity). Homodimer.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the CRISPR-associated endoribonuclease Cas2 protein family.</text>
  </comment>
  <dbReference type="EC" id="3.1.-.-"/>
  <dbReference type="EMBL" id="AE006641">
    <property type="protein sequence ID" value="AAK41639.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="H90297">
    <property type="entry name" value="H90297"/>
  </dbReference>
  <dbReference type="PDB" id="2I8E">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.59 A"/>
    <property type="chains" value="A=1-101"/>
  </dbReference>
  <dbReference type="PDB" id="2IVY">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.40 A"/>
    <property type="chains" value="A=2-101"/>
  </dbReference>
  <dbReference type="PDBsum" id="2I8E"/>
  <dbReference type="PDBsum" id="2IVY"/>
  <dbReference type="AlphaFoldDB" id="Q97YC2"/>
  <dbReference type="SMR" id="Q97YC2"/>
  <dbReference type="STRING" id="273057.SSO1404"/>
  <dbReference type="PaxDb" id="273057-SSO1404"/>
  <dbReference type="EnsemblBacteria" id="AAK41639">
    <property type="protein sequence ID" value="AAK41639"/>
    <property type="gene ID" value="SSO1404"/>
  </dbReference>
  <dbReference type="KEGG" id="sso:SSO1404"/>
  <dbReference type="PATRIC" id="fig|273057.12.peg.1420"/>
  <dbReference type="eggNOG" id="arCOG04194">
    <property type="taxonomic scope" value="Archaea"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_161124_2_0_2"/>
  <dbReference type="InParanoid" id="Q97YC2"/>
  <dbReference type="PhylomeDB" id="Q97YC2"/>
  <dbReference type="BRENDA" id="3.1.26.12">
    <property type="organism ID" value="6163"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="Q97YC2"/>
  <dbReference type="Proteomes" id="UP000001974">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004521">
    <property type="term" value="F:RNA endonuclease activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051607">
    <property type="term" value="P:defense response to virus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043571">
    <property type="term" value="P:maintenance of CRISPR repeat elements"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="CDD" id="cd09725">
    <property type="entry name" value="Cas2_I_II_III"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.70.240">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01471">
    <property type="entry name" value="Cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR021127">
    <property type="entry name" value="CRISPR_associated_Cas2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019199">
    <property type="entry name" value="Virulence_VapD/CRISPR_Cas2"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR01573">
    <property type="entry name" value="cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34405">
    <property type="entry name" value="CRISPR-ASSOCIATED ENDORIBONUCLEASE CAS2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR34405:SF1">
    <property type="entry name" value="CRISPR-ASSOCIATED ENDORIBONUCLEASE CAS2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF09827">
    <property type="entry name" value="CRISPR_Cas2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF143430">
    <property type="entry name" value="TTP0101/SSO1404-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0051">Antiviral defense</keyword>
  <keyword id="KW-0255">Endonuclease</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0460">Magnesium</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0540">Nuclease</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000416951" description="CRISPR-associated endoribonuclease Cas2 1">
    <location>
      <begin position="1"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="binding site" evidence="2">
    <location>
      <position position="10"/>
    </location>
    <ligand>
      <name>Mg(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:18420"/>
      <note>catalytic</note>
    </ligand>
  </feature>
  <feature type="mutagenesis site" description="Considerable loss of RNase activity." evidence="3">
    <original>Y</original>
    <variation>A</variation>
    <location>
      <position position="9"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of RNase activity." evidence="3">
    <original>D</original>
    <variation>A</variation>
    <location>
      <position position="10"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Considerable loss of RNase activity." evidence="3">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="17"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Considerable loss of RNase activity." evidence="3">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="19"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of RNase activity." evidence="3">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="31"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of RNase activity." evidence="3">
    <original>F</original>
    <variation>A</variation>
    <location>
      <position position="37"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="3"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="14"/>
      <end position="26"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="30"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="36"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="43"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="strand" evidence="5">
    <location>
      <begin position="68"/>
      <end position="75"/>
    </location>
  </feature>
  <feature type="helix" evidence="5">
    <location>
      <begin position="77"/>
      <end position="81"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18482976"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <evidence type="ECO:0007829" key="5">
    <source>
      <dbReference type="PDB" id="2IVY"/>
    </source>
  </evidence>
  <sequence length="101" mass="11936" checksum="A7C338AD76202E17" modified="2001-10-01" version="1">MAMLYLIFYDITDDNLRNRVAEFLKKKGLDRIQYSVFMGDLNSSRLKDVEAGLKIIGNRKKLQEDERFFILIVPITENQFRERIVIGYSGSEREEKSNVVW</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>