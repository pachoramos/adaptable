<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-11-01" modified="2024-11-27" version="134" xmlns="http://uniprot.org/uniprot">
  <accession>P43434</accession>
  <name>PA21B_CAVPO</name>
  <protein>
    <recommendedName>
      <fullName>Phospholipase A2</fullName>
      <ecNumber evidence="4 5">3.1.1.4</ecNumber>
    </recommendedName>
    <alternativeName>
      <fullName>Group IB phospholipase A2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Phosphatidylcholine 2-acylhydrolase 1B</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">PLA2G1B</name>
  </gene>
  <organism>
    <name type="scientific">Cavia porcellus</name>
    <name type="common">Guinea pig</name>
    <dbReference type="NCBI Taxonomy" id="10141"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Hystricomorpha</taxon>
      <taxon>Caviidae</taxon>
      <taxon>Cavia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="Eur. J. Biochem." volume="215" first="91" last="97">
      <title>Cloning and expression of phospholipase A2 from guinea pig gastric mucosa, its induction by carbachol and secretion in vivo.</title>
      <authorList>
        <person name="Ying Z."/>
        <person name="Tojo H."/>
        <person name="Nonaka Y."/>
        <person name="Okamoto M."/>
      </authorList>
      <dbReference type="PubMed" id="8344290"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1993.tb18010.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="4 5 6">Secretory calcium-dependent phospholipase A2 that primarily targets dietary phospholipids in the intestinal tract. Hydrolyzes the ester bond of the fatty acyl group attached at sn-2 position of phospholipids (phospholipase A2 activity) with preference for phosphatidylethanolamines and phosphatidylglycerols over phosphatidylcholines. May play a role in the biosynthesis of N-acyl ethanolamines that regulate energy metabolism and inflammation in the intestinal tract. Hydrolyzes N-acyl phosphatidylethanolamines to N-acyl lysophosphatidylethanolamines, which are further cleaved by a lysophospholipase D to release N-acyl ethanolamines (By similarity). May act in an autocrine and paracrine manner (By similarity). Has anti-helminth activity in a process regulated by gut microbiota. Upon helminth infection of intestinal epithelia, directly affects phosphatidylethanolamine contents in the membrane of helminth larvae, likely controlling an array of phospholipid-mediated cellular processes such as membrane fusion and cell division while providing for better immune recognition, ultimately reducing larvae integrity and infectivity (By similarity).</text>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="4 5 7 8">
      <text>a 1,2-diacyl-sn-glycero-3-phosphocholine + H2O = a 1-acyl-sn-glycero-3-phosphocholine + a fatty acid + H(+)</text>
      <dbReference type="Rhea" id="RHEA:15801"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:28868"/>
      <dbReference type="ChEBI" id="CHEBI:57643"/>
      <dbReference type="ChEBI" id="CHEBI:58168"/>
      <dbReference type="EC" id="3.1.1.4"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="4">
      <text>1,2-ditetradecanoyl-sn-glycero-3-phosphocholine + H2O = 1-tetradecanoyl-sn-glycero-3-phosphocholine + tetradecanoate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:54456"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:30807"/>
      <dbReference type="ChEBI" id="CHEBI:45240"/>
      <dbReference type="ChEBI" id="CHEBI:64489"/>
    </reaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="5">
      <text>1,2-dihexadecanoyl-sn-glycero-3-phosphocholine + H2O = 1-hexadecanoyl-sn-glycero-3-phosphocholine + hexadecanoate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:41223"/>
      <dbReference type="ChEBI" id="CHEBI:7896"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:72998"/>
      <dbReference type="ChEBI" id="CHEBI:72999"/>
    </reaction>
    <physiologicalReaction direction="left-to-right" evidence="5">
      <dbReference type="Rhea" id="RHEA:41224"/>
    </physiologicalReaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="4">
      <text>1-hexadecanoyl-2-(9Z-octadecenoyl)-sn-glycero-3-phosphocholine + H2O = 1-hexadecanoyl-sn-glycero-3-phosphocholine + (9Z)-octadecenoate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:38779"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:30823"/>
      <dbReference type="ChEBI" id="CHEBI:72998"/>
      <dbReference type="ChEBI" id="CHEBI:73001"/>
    </reaction>
    <physiologicalReaction direction="left-to-right" evidence="4">
      <dbReference type="Rhea" id="RHEA:38780"/>
    </physiologicalReaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="5">
      <text>1-hexadecanoyl-2-(5Z,8Z,11Z,14Z-eicosatetraenoyl)-sn-glycero-3-phosphocholine + H2O = 1-hexadecanoyl-sn-glycero-3-phosphocholine + (5Z,8Z,11Z,14Z)-eicosatetraenoate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:40427"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:32395"/>
      <dbReference type="ChEBI" id="CHEBI:72998"/>
      <dbReference type="ChEBI" id="CHEBI:73003"/>
    </reaction>
    <physiologicalReaction direction="left-to-right" evidence="5">
      <dbReference type="Rhea" id="RHEA:40428"/>
    </physiologicalReaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="4">
      <text>1-hexadecanoyl-2-(9Z-octadecenoyl)-sn-glycero-3-phospho-(1'-sn-glycerol) + H2O = 1-hexadecanoyl-sn-glycero-3-phospho-(1'-sn-glycerol) + (9Z)-octadecenoate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:40919"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:30823"/>
      <dbReference type="ChEBI" id="CHEBI:72841"/>
      <dbReference type="ChEBI" id="CHEBI:75158"/>
    </reaction>
    <physiologicalReaction direction="left-to-right" evidence="4">
      <dbReference type="Rhea" id="RHEA:40920"/>
    </physiologicalReaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="5">
      <text>N-hexadecanoyl-1,2-di-(9Z-octadecenoyl)-sn-glycero-3-phosphoethanolamine + H2O = N-hexadecanoyl-1-(9Z-octadecenoyl)-sn-glycero-3-phosphoethanolamine + (9Z)-octadecenoate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:45424"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:30823"/>
      <dbReference type="ChEBI" id="CHEBI:78097"/>
      <dbReference type="ChEBI" id="CHEBI:85217"/>
    </reaction>
    <physiologicalReaction direction="left-to-right" evidence="5">
      <dbReference type="Rhea" id="RHEA:45425"/>
    </physiologicalReaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="5">
      <text>1-hexadecanoyl-2-(9Z,12Z-octadecadienoyl)-sn-glycero-3-phosphoethanolamine + H2O = 1-hexadecanoyl-sn-glycero-3-phosphoethanolamine + (9Z,12Z)-octadecadienoate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:40815"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:30245"/>
      <dbReference type="ChEBI" id="CHEBI:73004"/>
      <dbReference type="ChEBI" id="CHEBI:73008"/>
    </reaction>
    <physiologicalReaction direction="left-to-right" evidence="5">
      <dbReference type="Rhea" id="RHEA:40816"/>
    </physiologicalReaction>
  </comment>
  <comment type="catalytic activity">
    <reaction evidence="5">
      <text>N,1-dihexadecanoyl-2-(9Z,12Z-octadecadienoyl)-sn-glycero-3-phosphoethanolamine + H2O = N,1-dihexadecanoyl-sn-glycero-3-phosphoethanolamine + (9Z,12Z)-octadecadienoate + H(+)</text>
      <dbReference type="Rhea" id="RHEA:56424"/>
      <dbReference type="ChEBI" id="CHEBI:15377"/>
      <dbReference type="ChEBI" id="CHEBI:15378"/>
      <dbReference type="ChEBI" id="CHEBI:30245"/>
      <dbReference type="ChEBI" id="CHEBI:85334"/>
      <dbReference type="ChEBI" id="CHEBI:85335"/>
    </reaction>
    <physiologicalReaction direction="left-to-right" evidence="5">
      <dbReference type="Rhea" id="RHEA:56425"/>
    </physiologicalReaction>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="3">
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </cofactor>
    <text evidence="3">Binds 1 Ca(2+) ion per subunit.</text>
  </comment>
  <comment type="subunit">
    <text evidence="2">Monomer or homodimer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="4">Secreted</location>
    </subcellularLocation>
    <text evidence="4">Secreted from pancreatic acinar cells in its inactive form.</text>
  </comment>
  <comment type="PTM">
    <text evidence="4">Activated by trypsin cleavage in the duodenum. Can also be activated by thrombin or autocatalytically.</text>
  </comment>
  <comment type="similarity">
    <text evidence="10">Belongs to the phospholipase A2 family.</text>
  </comment>
  <dbReference type="EC" id="3.1.1.4" evidence="4 5"/>
  <dbReference type="EMBL" id="D00740">
    <property type="protein sequence ID" value="BAA00640.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PIR" id="S34049">
    <property type="entry name" value="S34049"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_003477984.1">
    <property type="nucleotide sequence ID" value="XM_003477936.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P43434"/>
  <dbReference type="SMR" id="P43434"/>
  <dbReference type="STRING" id="10141.ENSCPOP00000032707"/>
  <dbReference type="Ensembl" id="ENSCPOT00000047764.1">
    <property type="protein sequence ID" value="ENSCPOP00000032707.1"/>
    <property type="gene ID" value="ENSCPOG00000012358.4"/>
  </dbReference>
  <dbReference type="GeneID" id="100732984"/>
  <dbReference type="KEGG" id="cpoc:100732984"/>
  <dbReference type="CTD" id="5319"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSCPOG00000012358"/>
  <dbReference type="eggNOG" id="KOG4087">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000154885"/>
  <dbReference type="HOGENOM" id="CLU_090683_1_1_1"/>
  <dbReference type="InParanoid" id="P43434"/>
  <dbReference type="OMA" id="ELDTCCQ"/>
  <dbReference type="OrthoDB" id="638584at2759"/>
  <dbReference type="TreeFam" id="TF319283"/>
  <dbReference type="Proteomes" id="UP000005447">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009986">
    <property type="term" value="C:cell surface"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032052">
    <property type="term" value="F:bile acid binding"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0047498">
    <property type="term" value="F:calcium-dependent phospholipase A2 activity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005543">
    <property type="term" value="F:phospholipid binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005102">
    <property type="term" value="F:signaling receptor binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050482">
    <property type="term" value="P:arachidonic acid secretion"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006633">
    <property type="term" value="P:fatty acid biosynthetic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002227">
    <property type="term" value="P:innate immune response in mucosa"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016042">
    <property type="term" value="P:lipid catabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046470">
    <property type="term" value="P:phosphatidylcholine metabolic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046471">
    <property type="term" value="P:phosphatidylglycerol metabolic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048146">
    <property type="term" value="P:positive regulation of fibroblast proliferation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="Ensembl"/>
  </dbReference>
  <dbReference type="GO" id="GO:1904635">
    <property type="term" value="P:positive regulation of podocyte apoptotic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00125">
    <property type="entry name" value="PLA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.20.90.10:FF:000011">
    <property type="entry name" value="Phospholipase A(2)"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.20.90.10">
    <property type="entry name" value="Phospholipase A2 domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001211">
    <property type="entry name" value="PLipase_A2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033112">
    <property type="entry name" value="PLipase_A2_Asp_AS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR016090">
    <property type="entry name" value="PLipase_A2_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036444">
    <property type="entry name" value="PLipase_A2_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR033113">
    <property type="entry name" value="PLipase_A2_His_AS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716:SF94">
    <property type="entry name" value="PHOSPHOLIPASE A2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11716">
    <property type="entry name" value="PHOSPHOLIPASE A2 FAMILY MEMBER"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00068">
    <property type="entry name" value="Phospholip_A2_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00389">
    <property type="entry name" value="PHPHLIPASEA2"/>
  </dbReference>
  <dbReference type="SMART" id="SM00085">
    <property type="entry name" value="PA2c"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48619">
    <property type="entry name" value="Phospholipase A2, PLA2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00119">
    <property type="entry name" value="PA2_ASP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00118">
    <property type="entry name" value="PA2_HIS"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at transcript level"/>
  <keyword id="KW-0068">Autocatalytic cleavage</keyword>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0378">Hydrolase</keyword>
  <keyword id="KW-0443">Lipid metabolism</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-1208">Phospholipid metabolism</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0865">Zymogen</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000022735" description="Activation peptide">
    <location>
      <begin position="16"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000022736" description="Phospholipase A2" evidence="9">
    <location>
      <begin position="23"/>
      <end position="146"/>
    </location>
  </feature>
  <feature type="active site" evidence="3">
    <location>
      <position position="70"/>
    </location>
  </feature>
  <feature type="active site" evidence="3">
    <location>
      <position position="121"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="50"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="52"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="54"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="3">
    <location>
      <position position="71"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="33"/>
      <end position="99"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="49"/>
      <end position="146"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="51"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="66"/>
      <end position="127"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="73"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="83"/>
      <end position="113"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="106"/>
      <end position="118"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P00592"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="3">
    <source>
      <dbReference type="UniProtKB" id="P00593"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="4">
    <source>
      <dbReference type="UniProtKB" id="P04054"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="5">
    <source>
      <dbReference type="UniProtKB" id="P04055"/>
    </source>
  </evidence>
  <evidence type="ECO:0000250" key="6">
    <source>
      <dbReference type="UniProtKB" id="Q9Z0Y2"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="7">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10035"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="8">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU10036"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="8344290"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <sequence length="146" mass="16187" checksum="760DE6CD11E20E07" modified="1995-11-01" version="1" precursor="true">MKLLVLAALLSVSAAAHIITPRALWQFRDMIKCAIPGSRPYSEYNNYGCFCGLGGSGTPVDELDRCCEIHDACYTQAKHLESCKSVIDNPYTNSYSFSCSGTNIICSSKNKECEEFICNCDRAAAICFSKAPYNENNKNINKKERC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>