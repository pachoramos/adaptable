<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1988-04-01" modified="2024-11-27" version="84" xmlns="http://uniprot.org/uniprot">
  <accession>P07396</accession>
  <accession>Q76ZW4</accession>
  <name>PG062_VACCW</name>
  <protein>
    <recommendedName>
      <fullName>Phosphoprotein OPG062</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Phosphoprotein F17</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">OPG062</name>
    <name type="ordered locus">VACWR056</name>
    <name type="ORF">F17R</name>
  </gene>
  <organism>
    <name type="scientific">Vaccinia virus (strain Western Reserve)</name>
    <name type="common">VACV</name>
    <name type="synonym">Vaccinia virus (strain WR)</name>
    <dbReference type="NCBI Taxonomy" id="10254"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Varidnaviria</taxon>
      <taxon>Bamfordvirae</taxon>
      <taxon>Nucleocytoviricota</taxon>
      <taxon>Pokkesviricetes</taxon>
      <taxon>Chitovirales</taxon>
      <taxon>Poxviridae</taxon>
      <taxon>Chordopoxvirinae</taxon>
      <taxon>Orthopoxvirus</taxon>
      <taxon>Vaccinia virus</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Bos taurus</name>
    <name type="common">Bovine</name>
    <dbReference type="NCBI Taxonomy" id="9913"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1985" name="Proc. Natl. Acad. Sci. U.S.A." volume="82" first="2096" last="2100">
      <title>One hundred base pairs of 5' flanking sequence of a vaccinia virus late gene are sufficient to temporally regulate late transcription.</title>
      <authorList>
        <person name="Bertholet C."/>
        <person name="Drillien R."/>
        <person name="Wittek R."/>
      </authorList>
      <dbReference type="PubMed" id="3856886"/>
      <dbReference type="DOI" id="10.1073/pnas.82.7.2096"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="2003-02" db="EMBL/GenBank/DDBJ databases">
      <title>Sequencing of the coding region of Vaccinia-WR to an average 9-fold redundancy and an error rate of 0.16/10kb.</title>
      <authorList>
        <person name="Esposito J.J."/>
        <person name="Frace A.M."/>
        <person name="Sammons S.A."/>
        <person name="Olsen-Rasmussen M."/>
        <person name="Osborne J."/>
        <person name="Wohlhueter R."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1991" name="J. Virol." volume="65" first="6101" last="6110">
      <title>Vaccinia virus morphogenesis is interrupted when expression of the gene encoding an 11-kilodalton phosphorylated protein is prevented by the Escherichia coli lac repressor.</title>
      <authorList>
        <person name="Zhang Y."/>
        <person name="Moss B."/>
      </authorList>
      <dbReference type="PubMed" id="1920628"/>
      <dbReference type="DOI" id="10.1128/jvi.65.11.6101-6110.1991"/>
    </citation>
    <scope>CHARACTERIZATION</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2006" name="Adv. Virus Res." volume="66" first="31" last="124">
      <title>In a nutshell: structure and assembly of the vaccinia virion.</title>
      <authorList>
        <person name="Condit R.C."/>
        <person name="Moussatche N."/>
        <person name="Traktman P."/>
      </authorList>
      <dbReference type="PubMed" id="16877059"/>
      <dbReference type="DOI" id="10.1016/s0065-3527(06)66002-8"/>
    </citation>
    <scope>REVIEW</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2010" name="J. Virol." volume="84" first="6846" last="6860">
      <title>Structure/Function analysis of the vaccinia virus F18 phosphoprotein, an abundant core component required for virion maturation and infectivity.</title>
      <authorList>
        <person name="Wickramasekera N.T."/>
        <person name="Traktman P."/>
      </authorList>
      <dbReference type="PubMed" id="20392848"/>
      <dbReference type="DOI" id="10.1128/jvi.00399-10"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBUNIT</scope>
    <scope>INTERACTION WITH PROTEIN OPG157/A30</scope>
    <scope>PHOSPHORYLATION AT SER-53 AND SER-62</scope>
    <scope>MUTAGENESIS OF SER-53 AND SER-62</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2018" name="Cell">
      <title>Poxviruses Evade Cytosolic Sensing through Disruption of an mTORC1-mTORC2 Regulatory Circuit.</title>
      <authorList>
        <person name="Meade N."/>
        <person name="Furey C."/>
        <person name="Li H."/>
        <person name="Verma R."/>
        <person name="Chai Q."/>
        <person name="Rollins M.G."/>
        <person name="DiGiuseppe S."/>
        <person name="Naghavi M.H."/>
        <person name="Walsh D."/>
      </authorList>
      <dbReference type="PubMed" id="30078703"/>
      <dbReference type="DOI" id="10.1016/j.cell.2018.06.053"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INTERACTION WITH HOST RICTOR AND RPTOR</scope>
    <scope>MUTAGENESIS OF SER-53 AND SER-62</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2015" name="J. Virol." volume="89" first="6874" last="6886">
      <title>Deciphering poxvirus gene expression by RNA sequencing and ribosome profiling.</title>
      <authorList>
        <person name="Yang Z."/>
        <person name="Cao S."/>
        <person name="Martens C.A."/>
        <person name="Porcella S.F."/>
        <person name="Xie Z."/>
        <person name="Ma M."/>
        <person name="Shen B."/>
        <person name="Moss B."/>
      </authorList>
      <dbReference type="PubMed" id="25903347"/>
      <dbReference type="DOI" id="10.1128/jvi.00528-15"/>
    </citation>
    <scope>INDUCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="2 4">Plays an essential role in virion assembly and morphogenesis (PubMed:20392848). Also plays a role in the inhibition of host immune response by dysregulating mTOR. Sequesters host RICTOR and RPTOR, thereby disrupting mTORC1 and mTORC2 crosstalk. In turn, blocks the host antiviral response in part through mTOR-dependent degradation of cGAS, the primary poxvirus sensor (PubMed:30078703).</text>
  </comment>
  <comment type="subunit">
    <text evidence="2 4">Self-associates to form high molecular-weight forms (PubMed:20392848). Interacts with protein OPG157/A30 (PubMed:20392848). Interacts with host RICTOR and RPTOR; these interactions disrupt the mTORC1 and mTORC2 crosstalk (PubMed:30078703).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Virion</location>
    </subcellularLocation>
    <text evidence="5">Major component of the virion comprising about 10% of the virion mass.</text>
  </comment>
  <comment type="induction">
    <text evidence="3">Expressed in the late phase of the viral replicative cycle.</text>
  </comment>
  <comment type="PTM">
    <text evidence="2 4">Phosphorylated on two serines. While these phosphorylations do not play a role in virion assembly; they are essential for the interaction with host RICTOR and RPTOR.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">Originally annotated as the product of the F18R open reading frame (protein F18), it is now referred as protein F17 since there are only 17 open reading frames in the HindIII fragment.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the orthopoxvirus OPG062 family.</text>
  </comment>
  <dbReference type="EMBL" id="M11107">
    <property type="protein sequence ID" value="AAA48278.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY243312">
    <property type="protein sequence ID" value="AAO89335.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A22955">
    <property type="entry name" value="WMVZ11"/>
  </dbReference>
  <dbReference type="iPTMnet" id="P07396"/>
  <dbReference type="KEGG" id="vg:3707513"/>
  <dbReference type="Proteomes" id="UP000000344">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030430">
    <property type="term" value="C:host cell cytoplasm"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044423">
    <property type="term" value="C:virion component"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0140313">
    <property type="term" value="F:molecular sequestering activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039501">
    <property type="term" value="P:suppression by virus of host type I interferon production"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019082">
    <property type="term" value="P:viral protein processing"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006854">
    <property type="entry name" value="Phosphoprotein_F17"/>
  </dbReference>
  <dbReference type="Pfam" id="PF04767">
    <property type="entry name" value="Pox_F17"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF003688">
    <property type="entry name" value="VAC_PP"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-0426">Late protein</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <keyword id="KW-0946">Virion</keyword>
  <feature type="chain" id="PRO_0000099521" description="Phosphoprotein OPG062">
    <location>
      <begin position="1"/>
      <end position="101"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="1">
    <location>
      <begin position="51"/>
      <end position="73"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="2">
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="2">
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="About 50% loss of phosphorylation." evidence="2">
    <original>S</original>
    <variation>A</variation>
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Complete loss of phosphorylation and about 90% loss of binding with host RICTOR and RPTOR; when associated with A-62." evidence="2 4">
    <original>S</original>
    <variation>A</variation>
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="About 50% loss of phosphorylation." evidence="2">
    <original>S</original>
    <variation>A</variation>
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Complete loss of phosphorylation and about 90% loss of binding with host RICTOR and RPTOR; when associated with A-53." evidence="2 4">
    <original>S</original>
    <variation>A</variation>
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AAO89335." evidence="5" ref="2">
    <original>R</original>
    <variation>G</variation>
    <location>
      <position position="73"/>
    </location>
  </feature>
  <evidence type="ECO:0000256" key="1">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="20392848"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="25903347"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="30078703"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="16877059"/>
    </source>
  </evidence>
  <sequence length="101" mass="11435" checksum="D9917F95B27FA3E0" modified="1988-04-01" version="1">MNSHFASAHTPFYINTKEGRYLVLKAVKVCDVRTVECEGSKASCVLKVDKPSSPACERRPSSPSRCERMNNPRKQVPFMRTDMLQNMFAANRDNVASRLLN</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>