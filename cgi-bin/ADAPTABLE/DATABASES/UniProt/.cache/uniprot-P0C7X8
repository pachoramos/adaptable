<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-07-22" modified="2023-09-13" version="27" xmlns="http://uniprot.org/uniprot">
  <accession>P0C7X8</accession>
  <name>PSMA1_STAA1</name>
  <protein>
    <recommendedName>
      <fullName>Phenol-soluble modulin alpha 1 peptide</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">psmA1</name>
    <name type="ordered locus">SAHV_0449.4</name>
  </gene>
  <organism>
    <name type="scientific">Staphylococcus aureus (strain Mu3 / ATCC 700698)</name>
    <dbReference type="NCBI Taxonomy" id="418127"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Bacillota</taxon>
      <taxon>Bacilli</taxon>
      <taxon>Bacillales</taxon>
      <taxon>Staphylococcaceae</taxon>
      <taxon>Staphylococcus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2008" name="Antimicrob. Agents Chemother." volume="52" first="45" last="53">
      <title>Mutated response regulator graR is responsible for phenotypic conversion of Staphylococcus aureus from heterogeneous vancomycin-intermediate resistance to vancomycin-intermediate resistance.</title>
      <authorList>
        <person name="Neoh H.-M."/>
        <person name="Cui L."/>
        <person name="Yuzawa H."/>
        <person name="Takeuchi F."/>
        <person name="Matsuo M."/>
        <person name="Hiramatsu K."/>
      </authorList>
      <dbReference type="PubMed" id="17954695"/>
      <dbReference type="DOI" id="10.1128/aac.00534-07"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Mu3 / ATCC 700698</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Peptide which can recruit, activate and subsequently lyse human neutrophils, thus eliminating the main cellular defense against infection.</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the phenol-soluble modulin alpha peptides family.</text>
  </comment>
  <dbReference type="EMBL" id="AP009324">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P0C7X8"/>
  <dbReference type="SMR" id="P0C7X8"/>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR031429">
    <property type="entry name" value="PSM_alpha"/>
  </dbReference>
  <dbReference type="NCBIfam" id="NF033425">
    <property type="entry name" value="PSM_alpha_1_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF17063">
    <property type="entry name" value="PSMalpha"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0204">Cytolysis</keyword>
  <keyword id="KW-0843">Virulence</keyword>
  <feature type="peptide" id="PRO_0000345029" description="Phenol-soluble modulin alpha 1 peptide">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="A9JX05"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="21" mass="2260" checksum="F291499952C7572D" modified="2008-07-22" version="1">MGIIAGIIKVIKSLIEQFTGK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>