<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1998-07-15" modified="2024-11-27" version="202" xmlns="http://uniprot.org/uniprot">
  <accession>O15263</accession>
  <accession>Q52LC0</accession>
  <name>DFB4A_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="13">Defensin beta 4A</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="11">Beta-defensin 2</fullName>
      <shortName evidence="11">BD-2</shortName>
      <shortName evidence="11">hBD-2</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Defensin, beta 2</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Skin-antimicrobial peptide 1</fullName>
      <shortName>SAP1</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB4A</name>
    <name type="synonym">DEFB102</name>
    <name type="synonym">DEFB2</name>
    <name type="synonym">DEFB4</name>
  </gene>
  <gene>
    <name type="primary">DEFB4B</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1997" name="Nature" volume="387" first="861" last="861">
      <title>A peptide antibiotic from human skin.</title>
      <authorList>
        <person name="Harder J."/>
        <person name="Bartels J.H."/>
        <person name="Christophers E."/>
        <person name="Schroeder J.-M."/>
      </authorList>
      <dbReference type="PubMed" id="9202117"/>
      <dbReference type="DOI" id="10.1038/43088"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION BY TNF AND MICROORGANISMS</scope>
    <source>
      <tissue>Skin</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1998" name="Gene" volume="222" first="237" last="244">
      <title>Structure and mapping of the human beta-defensin HBD-2 gene and its expression at sites of inflammation.</title>
      <authorList>
        <person name="Liu L."/>
        <person name="Wang L."/>
        <person name="Jia H.P."/>
        <person name="Zhao C."/>
        <person name="Heng H.H.Q."/>
        <person name="Schutte B.C."/>
        <person name="McCray P.B. Jr."/>
        <person name="Ganz T."/>
      </authorList>
      <dbReference type="PubMed" id="9831658"/>
      <dbReference type="DOI" id="10.1016/s0378-1119(98)00480-6"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION BY INFLAMMATION</scope>
    <source>
      <tissue>Placenta</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2000" name="Infect. Immun." volume="68" first="113" last="119">
      <title>Transcriptional regulation of beta-defensin gene expression in tracheal epithelial cells.</title>
      <authorList>
        <person name="Diamond G."/>
        <person name="Kaiser V."/>
        <person name="Rhodes J."/>
        <person name="Russell J.P."/>
        <person name="Bevins C.L."/>
      </authorList>
      <dbReference type="PubMed" id="10603376"/>
      <dbReference type="DOI" id="10.1128/iai.68.1.113-119.2000"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2000" name="Am. J. Respir. Cell Mol. Biol." volume="22" first="714" last="721">
      <title>Mucoid Pseudomonas aeruginosa, TNF-alpha, and IL-1beta, but not IL-6, induce human beta-defensin-2 in respiratory epithelia.</title>
      <authorList>
        <person name="Harder J."/>
        <person name="Meyer-Hoffert U."/>
        <person name="Teran L.M."/>
        <person name="Schwichtenberg L."/>
        <person name="Bartels J."/>
        <person name="Maune S."/>
        <person name="Schroeder J.-M."/>
      </authorList>
      <dbReference type="PubMed" id="10837369"/>
      <dbReference type="DOI" id="10.1165/ajrcmb.22.6.4023"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>INDUCTION BY P.AERUGINOSA; LPS; TNF AND IL1B</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1999" name="Science" volume="286" first="525" last="528">
      <title>Beta-defensins: linking innate and adaptive immunity through dendritic and T cell CCR6.</title>
      <authorList>
        <person name="Yang D."/>
        <person name="Chertov O."/>
        <person name="Bykovskaia S.N."/>
        <person name="Chen Q."/>
        <person name="Buffo M.J."/>
        <person name="Shogan J."/>
        <person name="Anderson M."/>
        <person name="Schroeder J.M."/>
        <person name="Wang J.M."/>
        <person name="Howard O.M."/>
        <person name="Oppenheim J.J."/>
      </authorList>
      <dbReference type="PubMed" id="10521347"/>
      <dbReference type="DOI" id="10.1126/science.286.5439.525"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2002" name="J. Pept. Res." volume="59" first="241" last="248">
      <title>Chemical synthesis of beta-defensins and LEAP-1/hepcidin.</title>
      <authorList>
        <person name="Kluever E."/>
        <person name="Schulz A."/>
        <person name="Forssmann W.-G."/>
        <person name="Adermann K."/>
      </authorList>
      <dbReference type="PubMed" id="12010514"/>
      <dbReference type="DOI" id="10.1034/j.1399-3011.2002.00980.x"/>
    </citation>
    <scope>SYNTHESIS OF 24-64</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2010" name="J. Biol. Chem." volume="285" first="7028" last="7034">
      <title>Specific binding and chemotactic activity of mBD4 and its functional orthologue hBD2 to CCR6-expressing cells.</title>
      <authorList>
        <person name="Roehrl J."/>
        <person name="Yang D."/>
        <person name="Oppenheim J.J."/>
        <person name="Hehlgans T."/>
      </authorList>
      <dbReference type="PubMed" id="20068036"/>
      <dbReference type="DOI" id="10.1074/jbc.m109.091090"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>BINDING TO CCR6</scope>
  </reference>
  <reference evidence="15 16" key="9">
    <citation type="journal article" date="2000" name="J. Biol. Chem." volume="275" first="32911" last="32918">
      <title>The structure of human beta-defensin-2 shows evidence of higher order oligomerization.</title>
      <authorList>
        <person name="Hoover D.M."/>
        <person name="Rajashankar K.R."/>
        <person name="Blumenthal R."/>
        <person name="Puri A."/>
        <person name="Oppenheim J.J."/>
        <person name="Chertov O."/>
        <person name="Lubkowski J."/>
      </authorList>
      <dbReference type="PubMed" id="10906336"/>
      <dbReference type="DOI" id="10.1074/jbc.m006098200"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.35 ANGSTROMS) OF 24-64</scope>
    <scope>SUBUNIT</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <reference evidence="17" key="10">
    <citation type="journal article" date="2001" name="Biochemistry" volume="40" first="3810" last="3816">
      <title>The NMR structure of human beta-defensin-2 reveals a novel alpha-helical segment.</title>
      <authorList>
        <person name="Sawai M.V."/>
        <person name="Jia H.P."/>
        <person name="Liu L."/>
        <person name="Aseyev V."/>
        <person name="Wiencek J.M."/>
        <person name="McCray P.B."/>
        <person name="Ganz T."/>
        <person name="Kearney W.R."/>
        <person name="Tack B.F."/>
      </authorList>
      <dbReference type="PubMed" id="11300761"/>
      <dbReference type="DOI" id="10.1021/bi002519d"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 24-64</scope>
    <scope>SUBUNIT</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <reference evidence="14" key="11">
    <citation type="journal article" date="2001" name="Protein Sci." volume="10" first="2470" last="2479">
      <title>Structure determination of human and murine beta-defensins reveals structural conservation in the absence of significant sequence similarity.</title>
      <authorList>
        <person name="Bauer F."/>
        <person name="Schweimer K."/>
        <person name="Kluever E."/>
        <person name="Conejo-Garcia J.-R."/>
        <person name="Forssmann W.-G."/>
        <person name="Roesch P."/>
        <person name="Adermann K."/>
        <person name="Sticht H."/>
      </authorList>
      <dbReference type="PubMed" id="11714914"/>
      <dbReference type="DOI" id="10.1110/ps.24401"/>
    </citation>
    <scope>STRUCTURE BY NMR OF 28-64</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <reference evidence="18" key="12">
    <citation type="journal article" date="2018" name="Sci. Adv." volume="4" first="eaat0979" last="eaat0979">
      <title>Human beta-defensin 2 kills Candida albicans through phosphatidylinositol 4,5-bisphosphate-mediated membrane permeabilization.</title>
      <authorList>
        <person name="Jaervaa M."/>
        <person name="Phan T.K."/>
        <person name="Lay F.T."/>
        <person name="Caria S."/>
        <person name="Kvansakul M."/>
        <person name="Hulett M.D."/>
      </authorList>
      <dbReference type="PubMed" id="30050988"/>
      <dbReference type="DOI" id="10.1126/sciadv.aat0979"/>
    </citation>
    <scope>X-RAY CRYSTALLOGRAPHY (1.85 ANGSTROMS) OF 24-64 IN COMPLEX WITH PIP2</scope>
    <scope>FUNCTION</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>MUTAGENESIS OF LYS-33; ARG-45; ARG-46; TYR-47; LYS-48 AND LYS-59</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 7 8 9">Exhibits antimicrobial activity against Gram-negative bacteria and Gram-positive bacteria, with highest activity against Gram-negative bacteria (PubMed:10837369, PubMed:9202117). Antimicrobial activity against P.aruginosa seems to be salt-sensitive and is reduced with high salt concentrations greater than 25 mM (PubMed:10837369). Also exhibits antimicrobial activity against the yeast C.albicans (PubMed:10837369, PubMed:30050988, PubMed:9202117). Permeabilizes C.albicans cell membranes via targeting plasma membrane lipid phosphatidylinositol 4,5-bisphosphate (PIP2), thereby leading to cell fragmentation and cell death (PubMed:30050988). Acts as a ligand for C-C chemokine receptor CCR6 (PubMed:10521347, PubMed:20068036). Binds to CCR6 and induces chemotactic activity of CCR6-expressing cells, such as immature dendritic cells and memory T cells (PubMed:10521347, PubMed:20068036).</text>
  </comment>
  <comment type="subunit">
    <text evidence="4 5">Monomer (PubMed:11300761). Homodimer (PubMed:10906336).</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-21800352">
      <id>O15263</id>
    </interactant>
    <interactant intactId="EBI-1056377">
      <id>O75844</id>
      <label>ZMPSTE24</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>2</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3 9 10">Expressed in lung epithelial cells (at protein level) (PubMed:10837369). Expressed in foreskin, lung and trachea (PubMed:9202117). Lower expression in kidney, uterus and salivary gland tissue (PubMed:9202117). Expressed in epithelial cells of the respiratory tract, with higher expression in distal parenchyma of the lung, trachea, and tonsils, and lower expression in pharynx and adenoid, and low expression in tongue and larynx (PubMed:10837369, PubMed:9831658).</text>
  </comment>
  <comment type="induction">
    <text evidence="3 9 10">Up-regulated by TNF, IL1B, Gram-negative and Gram-positive bacteria, C.albicans and bacterial lipopolysaccharides (LPS) (PubMed:10837369, PubMed:9202117). Up-regulated by inflammation in skin keratinocytes in epidermal tissue (PubMed:9831658).</text>
  </comment>
  <comment type="similarity">
    <text evidence="12">Belongs to the beta-defensin family. LAP/TAP subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="Z71389">
    <property type="protein sequence ID" value="CAA95992.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF040153">
    <property type="protein sequence ID" value="AAC33549.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF071216">
    <property type="protein sequence ID" value="AAC69554.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AJ000152">
    <property type="protein sequence ID" value="CAB65126.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC069285">
    <property type="protein sequence ID" value="AAH69285.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC093983">
    <property type="protein sequence ID" value="AAH93983.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC093985">
    <property type="protein sequence ID" value="AAH93985.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS5971.1"/>
  <dbReference type="RefSeq" id="NP_001192195.1">
    <property type="nucleotide sequence ID" value="NM_001205266.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_004933.1">
    <property type="nucleotide sequence ID" value="NM_004942.3"/>
  </dbReference>
  <dbReference type="PDB" id="1E4Q">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=28-64"/>
  </dbReference>
  <dbReference type="PDB" id="1FD3">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.35 A"/>
    <property type="chains" value="A/B/C/D=24-64"/>
  </dbReference>
  <dbReference type="PDB" id="1FD4">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.70 A"/>
    <property type="chains" value="A/B/C/D/E/F/G/H/I/J/K/L/M/N/O/P=24-64"/>
  </dbReference>
  <dbReference type="PDB" id="1FQQ">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=24-64"/>
  </dbReference>
  <dbReference type="PDB" id="6CS9">
    <property type="method" value="X-ray"/>
    <property type="resolution" value="1.85 A"/>
    <property type="chains" value="A/B=24-64"/>
  </dbReference>
  <dbReference type="PDBsum" id="1E4Q"/>
  <dbReference type="PDBsum" id="1FD3"/>
  <dbReference type="PDBsum" id="1FD4"/>
  <dbReference type="PDBsum" id="1FQQ"/>
  <dbReference type="PDBsum" id="6CS9"/>
  <dbReference type="AlphaFoldDB" id="O15263"/>
  <dbReference type="SMR" id="O15263"/>
  <dbReference type="BioGRID" id="108037">
    <property type="interactions" value="3"/>
  </dbReference>
  <dbReference type="IntAct" id="O15263">
    <property type="interactions" value="1"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000303532"/>
  <dbReference type="TCDB" id="1.C.85.1.2">
    <property type="family name" value="the pore-forming Beta-defensin (Beta-defensin) family"/>
  </dbReference>
  <dbReference type="BioMuta" id="DEFB4A"/>
  <dbReference type="MassIVE" id="O15263"/>
  <dbReference type="PaxDb" id="9606-ENSP00000303532"/>
  <dbReference type="PeptideAtlas" id="O15263"/>
  <dbReference type="ProteomicsDB" id="48551"/>
  <dbReference type="Antibodypedia" id="22053">
    <property type="antibodies" value="225 antibodies from 17 providers"/>
  </dbReference>
  <dbReference type="Antibodypedia" id="4918">
    <property type="antibodies" value="139 antibodies from 20 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="1673"/>
  <dbReference type="Ensembl" id="ENST00000302247.3">
    <property type="protein sequence ID" value="ENSP00000303532.2"/>
    <property type="gene ID" value="ENSG00000171711.3"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000318157.3">
    <property type="protein sequence ID" value="ENSP00000424598.1"/>
    <property type="gene ID" value="ENSG00000177257.3"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000617136.2">
    <property type="protein sequence ID" value="ENSP00000479138.1"/>
    <property type="gene ID" value="ENSG00000275444.2"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000642856.2">
    <property type="protein sequence ID" value="ENSP00000496499.1"/>
    <property type="gene ID" value="ENSG00000285181.2"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000644124.2">
    <property type="protein sequence ID" value="ENSP00000493760.1"/>
    <property type="gene ID" value="ENSG00000285433.2"/>
  </dbReference>
  <dbReference type="GeneID" id="100289462"/>
  <dbReference type="GeneID" id="1673"/>
  <dbReference type="KEGG" id="hsa:100289462"/>
  <dbReference type="KEGG" id="hsa:1673"/>
  <dbReference type="MANE-Select" id="ENST00000302247.3">
    <property type="protein sequence ID" value="ENSP00000303532.2"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_004942.4"/>
    <property type="RefSeq protein sequence ID" value="NP_004933.1"/>
  </dbReference>
  <dbReference type="MANE-Select" id="ENST00000318157.3">
    <property type="protein sequence ID" value="ENSP00000424598.1"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_001205266.2"/>
    <property type="RefSeq protein sequence ID" value="NP_001192195.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc003wsd.4">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:2767"/>
  <dbReference type="AGR" id="HGNC:30193"/>
  <dbReference type="CTD" id="100289462"/>
  <dbReference type="CTD" id="1673"/>
  <dbReference type="DisGeNET" id="100289462"/>
  <dbReference type="DisGeNET" id="1673"/>
  <dbReference type="GeneCards" id="DEFB4A"/>
  <dbReference type="GeneCards" id="DEFB4B"/>
  <dbReference type="HGNC" id="HGNC:2767">
    <property type="gene designation" value="DEFB4A"/>
  </dbReference>
  <dbReference type="HGNC" id="HGNC:30193">
    <property type="gene designation" value="DEFB4B"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000171711">
    <property type="expression patterns" value="Tissue enriched (esophagus)"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000177257">
    <property type="expression patterns" value="Group enriched (esophagus, lymphoid tissue)"/>
  </dbReference>
  <dbReference type="MIM" id="602215">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_O15263"/>
  <dbReference type="OpenTargets" id="ENSG00000171711"/>
  <dbReference type="OpenTargets" id="ENSG00000177257"/>
  <dbReference type="PharmGKB" id="PA27249"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000171711"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000177257"/>
  <dbReference type="eggNOG" id="ENOG502SYUI">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000160995"/>
  <dbReference type="HOGENOM" id="CLU_189296_4_1_1"/>
  <dbReference type="InParanoid" id="O15263"/>
  <dbReference type="OMA" id="PGTKCCR"/>
  <dbReference type="OrthoDB" id="4840034at2759"/>
  <dbReference type="PhylomeDB" id="O15263"/>
  <dbReference type="PathwayCommons" id="O15263"/>
  <dbReference type="Reactome" id="R-HSA-1461957">
    <property type="pathway name" value="Beta defensins"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-1461973">
    <property type="pathway name" value="Defensins"/>
  </dbReference>
  <dbReference type="SignaLink" id="O15263"/>
  <dbReference type="BioGRID-ORCS" id="100289462">
    <property type="hits" value="15 hits in 1036 CRISPR screens"/>
  </dbReference>
  <dbReference type="BioGRID-ORCS" id="1673">
    <property type="hits" value="62 hits in 1020 CRISPR screens"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="O15263"/>
  <dbReference type="GeneWiki" id="Beta-defensin_2"/>
  <dbReference type="Pharos" id="O15263">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:O15263"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 8"/>
  </dbReference>
  <dbReference type="RNAct" id="O15263">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000171711">
    <property type="expression patterns" value="Expressed in mucosa of stomach and 56 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005796">
    <property type="term" value="C:Golgi lumen"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031731">
    <property type="term" value="F:CCR6 chemokine receptor binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042056">
    <property type="term" value="F:chemoattractant activity"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005546">
    <property type="term" value="F:phosphatidylinositol-4,5-bisphosphate binding"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061760">
    <property type="term" value="P:antifungal innate immune response"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0060326">
    <property type="term" value="P:cell chemotaxis"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006935">
    <property type="term" value="P:chemotaxis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="MGI"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007186">
    <property type="term" value="P:G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006955">
    <property type="term" value="P:immune response"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="ProtInc"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.360.10:FF:000001">
    <property type="entry name" value="Beta-defensin 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR006080">
    <property type="entry name" value="Beta/alpha-defensin_C"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001855">
    <property type="entry name" value="Defensin_beta-typ"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR20515:SF2">
    <property type="entry name" value="BETA-DEFENSIN 4A"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00711">
    <property type="entry name" value="Defensin_beta"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00048">
    <property type="entry name" value="DEFSN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57392">
    <property type="entry name" value="Defensin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="1">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000006968" description="Defensin beta 4A">
    <location>
      <begin position="24"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="region of interest" description="Phosphatidylinositol 4,5-bisphosphate (PIP2) binding" evidence="8">
    <location>
      <begin position="33"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 5 6 8 14 15 16 17 18">
    <location>
      <begin position="31"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 5 6 8 14 15 16 17 18">
    <location>
      <begin position="38"/>
      <end position="53"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="4 5 6 8 14 15 16 17 18">
    <location>
      <begin position="43"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of PIP2 binding and loss of liposomal lysis activity. Decrease in fungal cell permeabilization. Impaired antifungal activity." evidence="8">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="33"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of PIP2 binding and loss of liposomal lysis activity. Decrease in fungal cell permeabilization. Impaired antifungal activity." evidence="8">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="45"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No impact on fungal cell permeabilization. Impaired antifungal activity." evidence="8">
    <original>R</original>
    <variation>A</variation>
    <location>
      <position position="46"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No impact on fungal cell permeabilization. No impact on antifungal activity." evidence="8">
    <original>Y</original>
    <variation>A</variation>
    <location>
      <position position="47"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="Loss of PIP2 binding and reduced liposomal lysis activity. Impaired antifungal activity. Decrease in fungal cell permeabilization." evidence="8">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="48"/>
    </location>
  </feature>
  <feature type="mutagenesis site" description="No impact on PIP binding and liposomal lysis activity. Lack of antifungal activity. Lack of fungal cell permeabilization." evidence="8">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="59"/>
    </location>
  </feature>
  <feature type="strand" evidence="21">
    <location>
      <begin position="25"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="helix" evidence="20">
    <location>
      <begin position="28"/>
      <end position="33"/>
    </location>
  </feature>
  <feature type="strand" evidence="20">
    <location>
      <begin position="37"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="strand" evidence="20">
    <location>
      <begin position="48"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="strand" evidence="19">
    <location>
      <begin position="53"/>
      <end position="55"/>
    </location>
  </feature>
  <feature type="strand" evidence="20">
    <location>
      <begin position="59"/>
      <end position="62"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10521347"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="10837369"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="10906336"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="11300761"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="11714914"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="20068036"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="30050988"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="9202117"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="9831658"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="11">
    <source>
      <dbReference type="PubMed" id="9202117"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="12"/>
  <evidence type="ECO:0000312" key="13">
    <source>
      <dbReference type="HGNC" id="HGNC:2767"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="14">
    <source>
      <dbReference type="PDB" id="1E4Q"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="15">
    <source>
      <dbReference type="PDB" id="1FD3"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="16">
    <source>
      <dbReference type="PDB" id="1FD4"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="17">
    <source>
      <dbReference type="PDB" id="1FQQ"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="18">
    <source>
      <dbReference type="PDB" id="6CS9"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="19">
    <source>
      <dbReference type="PDB" id="1E4Q"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="20">
    <source>
      <dbReference type="PDB" id="1FD3"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="21">
    <source>
      <dbReference type="PDB" id="1FD4"/>
    </source>
  </evidence>
  <sequence length="64" mass="7038" checksum="05D6454CE7ACD10E" modified="1998-01-01" version="1" precursor="true">MRVLYLLFSFLFIFLMPLPGVFGGIGDPVTCLKSGAICHPVFCPRRYKQIGTCGLPGTKCCKKP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>