<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1989-07-01" modified="2024-10-02" version="147" xmlns="http://uniprot.org/uniprot">
  <accession>P11384</accession>
  <name>SECR_RAT</name>
  <protein>
    <recommendedName>
      <fullName evidence="12">Secretin</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="14" type="primary">Sct</name>
  </gene>
  <organism>
    <name type="scientific">Rattus norvegicus</name>
    <name type="common">Rat</name>
    <dbReference type="NCBI Taxonomy" id="10116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Glires</taxon>
      <taxon>Rodentia</taxon>
      <taxon>Myomorpha</taxon>
      <taxon>Muroidea</taxon>
      <taxon>Muridae</taxon>
      <taxon>Murinae</taxon>
      <taxon>Rattus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1990" name="Proc. Natl. Acad. Sci. U.S.A." volume="87" first="2299" last="2303">
      <title>Secretin: structure of the precursor and tissue distribution of the mRNA.</title>
      <authorList>
        <person name="Kopin A.S."/>
        <person name="Wheeler M.B."/>
        <person name="Leiter A.B."/>
      </authorList>
      <dbReference type="PubMed" id="2315322"/>
      <dbReference type="DOI" id="10.1073/pnas.87.6.2299"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1991" name="Proc. Natl. Acad. Sci. U.S.A." volume="88" first="5335" last="5339">
      <title>The secretin gene: evolutionary history, alternative splicing, and developmental regulation.</title>
      <authorList>
        <person name="Kopin A.S."/>
        <person name="Wheeler M.B."/>
        <person name="Nishitani J."/>
        <person name="McBride E.W."/>
        <person name="Chang T.M."/>
        <person name="Chey W.Y."/>
        <person name="Leiter A.B."/>
      </authorList>
      <dbReference type="PubMed" id="1711228"/>
      <dbReference type="DOI" id="10.1073/pnas.88.12.5335"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1991" name="J. Biol. Chem." volume="266" first="12595" last="12598">
      <title>The secretin precursor gene. Structure of the coding region and expression in the brain.</title>
      <authorList>
        <person name="Itoh N."/>
        <person name="Furuya T."/>
        <person name="Ozaki K."/>
        <person name="Kawasaki T."/>
      </authorList>
      <dbReference type="PubMed" id="2061329"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(18)98940-4"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1989" name="Biochem. Biophys. Res. Commun." volume="160" first="862" last="867">
      <title>Isolation and primary structure of rat secretin.</title>
      <authorList>
        <person name="Gossen D."/>
        <person name="Vandermeers A."/>
        <person name="Vandermeers-Piret M.-C."/>
        <person name="Rathe J."/>
        <person name="Cauvin A."/>
        <person name="Robberecht P."/>
        <person name="Christophe J."/>
      </authorList>
      <dbReference type="PubMed" id="2719704"/>
      <dbReference type="DOI" id="10.1016/0006-291x(89)92514-x"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 33-59</scope>
    <scope>AMIDATION AT VAL-59</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2012" name="Nat. Commun." volume="3" first="876" last="876">
      <title>Quantitative maps of protein phosphorylation sites across 14 different rat organs and tissues.</title>
      <authorList>
        <person name="Lundby A."/>
        <person name="Secher A."/>
        <person name="Lage K."/>
        <person name="Nordsborg N.B."/>
        <person name="Dmytriyev A."/>
        <person name="Lundby C."/>
        <person name="Olsen J.V."/>
      </authorList>
      <dbReference type="PubMed" id="22673903"/>
      <dbReference type="DOI" id="10.1038/ncomms1871"/>
    </citation>
    <scope>PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-63</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1994" name="Gastroenterology" volume="107" first="1751" last="1758">
      <title>Dual inhibitory mechanism of secretin action on acid secretion in totally isolated, vascularly perfused rat stomach.</title>
      <authorList>
        <person name="Chung I."/>
        <person name="Li P."/>
        <person name="Lee K."/>
        <person name="Chang T."/>
        <person name="Chey W.Y."/>
      </authorList>
      <dbReference type="PubMed" id="7958688"/>
      <dbReference type="DOI" id="10.1016/0016-5085(94)90817-6"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1995" name="J. Physiol. (Lond.)" volume="488" first="501" last="508">
      <title>The mechanism of inhibitory action of secretin on gastric acid secretion in conscious rats.</title>
      <authorList>
        <person name="Shimizu K."/>
        <person name="Li P."/>
        <person name="Lee K.Y."/>
        <person name="Chang T.M."/>
        <person name="Chey W.Y."/>
      </authorList>
      <dbReference type="PubMed" id="8568688"/>
      <dbReference type="DOI" id="10.1113/jphysiol.1995.sp020984"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="1988" name="Peptides" volume="9" first="583" last="588">
      <title>Effect of the secretin family of peptides on gastric emptying and small intestinal transit in rats.</title>
      <authorList>
        <person name="Murthy S.N."/>
        <person name="Ganiban G."/>
      </authorList>
      <dbReference type="PubMed" id="3047699"/>
      <dbReference type="DOI" id="10.1016/0196-9781(88)90168-4"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="1998" name="Am. J. Physiol." volume="275" first="G22" last="G28">
      <title>Secretin inhibits gastric acid secretion via a vagal afferent pathway in rats.</title>
      <authorList>
        <person name="Li P."/>
        <person name="Chang T.M."/>
        <person name="Chey W.Y."/>
      </authorList>
      <dbReference type="PubMed" id="9655680"/>
      <dbReference type="DOI" id="10.1152/ajpgi.1998.275.1.g22"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="1998" name="J. Biol. Chem." volume="273" first="6756" last="6762">
      <title>A role for receptor kinases in the regulation of class II G protein-coupled receptors. Phosphorylation and desensitization of the secretin receptor.</title>
      <authorList>
        <person name="Shetzline M.A."/>
        <person name="Premont R.T."/>
        <person name="Walker J.K."/>
        <person name="Vigna S.R."/>
        <person name="Caron M.G."/>
      </authorList>
      <dbReference type="PubMed" id="9506976"/>
      <dbReference type="DOI" id="10.1074/jbc.273.12.6756"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2002" name="Mol. Endocrinol." volume="16" first="2490" last="2501">
      <title>Interaction among four residues distributed through the secretin pharmacophore and a focused region of the secretin receptor amino terminus.</title>
      <authorList>
        <person name="Dong M."/>
        <person name="Zang M."/>
        <person name="Pinon D.I."/>
        <person name="Li Z."/>
        <person name="Lybrand T.P."/>
        <person name="Miller L.J."/>
      </authorList>
      <dbReference type="PubMed" id="12403838"/>
      <dbReference type="DOI" id="10.1210/me.2002-0111"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2004" name="Neurosci. Lett." volume="366" first="176" last="181">
      <title>Age-related and regional differences in secretin and secretin receptor mRNA levels in the rat brain.</title>
      <authorList>
        <person name="Tay J."/>
        <person name="Goulet M."/>
        <person name="Rusche J."/>
        <person name="Boismenu R."/>
      </authorList>
      <dbReference type="PubMed" id="15276242"/>
      <dbReference type="DOI" id="10.1016/j.neulet.2004.05.030"/>
    </citation>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2009" name="Proc. Natl. Acad. Sci. U.S.A." volume="106" first="15961" last="15966">
      <title>Secretin as a neurohypophysial factor regulating body water homeostasis.</title>
      <authorList>
        <person name="Chu J.Y."/>
        <person name="Lee L.T."/>
        <person name="Lai C.H."/>
        <person name="Vaudry H."/>
        <person name="Chan Y.S."/>
        <person name="Yung W.H."/>
        <person name="Chow B.K."/>
      </authorList>
      <dbReference type="PubMed" id="19805236"/>
      <dbReference type="DOI" id="10.1073/pnas.0903695106"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3 5 7 8 9 10 11">Hormone involved in different processes, such as regulation of the pH of the duodenal content, food intake and water homeostasis (PubMed:19805236, PubMed:3047699, PubMed:7958688, PubMed:8568688, PubMed:9655680). Exerts its biological effects by binding to secretin receptor (SCTR), a G-protein coupled receptor expressed in the basolateral domain of several cells (PubMed:12403838, PubMed:9506976). Acts as a key gastrointestinal hormone by regulating the pH of the duodenal content (PubMed:3047699, PubMed:7958688, PubMed:8568688, PubMed:9655680). Secreted by S cells of the duodenum in the crypts of Lieberkuehn and regulates the pH of the duodenum by (1) inhibiting the secretion of gastric acid from the parietal cells of the stomach and (2) stimulating the production of bicarbonate (NaHCO(3)) from the ductal cells of the pancreas (PubMed:3047699, PubMed:7958688, PubMed:8568688, PubMed:9655680). Production of bicarbonate is essential to neutralize the pH and ensure no damage is done to the small intestine by the gastric acid (By similarity). In addition to regulating the pH of the duodenal content, plays a central role in diet induced thermogenesis: acts as a non-sympathetic brown fat (BAT) activator mediating prandial thermogenesis, which consequentially induces satiation (By similarity). Mechanistically, secretin released by the gut after a meal binds to secretin receptor (SCTR) in brown adipocytes, activating brown fat thermogenesis by stimulating lipolysis, which is sensed in the brain and promotes satiation (By similarity). Also able to stimulate lipolysis in white adipocytes (By similarity). Also plays an important role in cellular osmoregulation: released into the systemic circulation in response to hyperosmolality and acts at different levels in the hypothalamus, pituitary and kidney to regulate water homeostasis (PubMed:19805236). Also plays a role in the central nervous system, possibly by acting as a neuropeptide hormone: required for hippocampal synaptic function and neural progenitor cells maintenance (By similarity).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5 7">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="4">In the brain, expressed in the central amygdala, hippocampus, area postrema, nucleus of the tractus solitary and cerebellum (PubMed:15276242). Expressed at higher level in the cerebellum (PubMed:15276242).</text>
  </comment>
  <comment type="similarity">
    <text evidence="13">Belongs to the glucagon family.</text>
  </comment>
  <dbReference type="EMBL" id="M31495">
    <property type="protein sequence ID" value="AAA42126.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M64033">
    <property type="protein sequence ID" value="AAA42128.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="M63984">
    <property type="protein sequence ID" value="AAA42127.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A40886">
    <property type="entry name" value="A40959"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_073161.1">
    <property type="nucleotide sequence ID" value="NM_022670.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_006230580.1">
    <property type="nucleotide sequence ID" value="XM_006230518.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P11384"/>
  <dbReference type="SMR" id="P11384"/>
  <dbReference type="STRING" id="10116.ENSRNOP00000024094"/>
  <dbReference type="iPTMnet" id="P11384"/>
  <dbReference type="PhosphoSitePlus" id="P11384"/>
  <dbReference type="PaxDb" id="10116-ENSRNOP00000024094"/>
  <dbReference type="Ensembl" id="ENSRNOT00000024094.6">
    <property type="protein sequence ID" value="ENSRNOP00000024094.2"/>
    <property type="gene ID" value="ENSRNOG00000017873.8"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055004527">
    <property type="protein sequence ID" value="ENSRNOP00055003351"/>
    <property type="gene ID" value="ENSRNOG00055002900"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00055050835">
    <property type="protein sequence ID" value="ENSRNOP00055041882"/>
    <property type="gene ID" value="ENSRNOG00055029333"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00060057332">
    <property type="protein sequence ID" value="ENSRNOP00060047389"/>
    <property type="gene ID" value="ENSRNOG00060033051"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENSRNOT00065032470">
    <property type="protein sequence ID" value="ENSRNOP00065025919"/>
    <property type="gene ID" value="ENSRNOG00065019290"/>
  </dbReference>
  <dbReference type="GeneID" id="24769"/>
  <dbReference type="KEGG" id="rno:24769"/>
  <dbReference type="UCSC" id="RGD:3643">
    <property type="organism name" value="rat"/>
  </dbReference>
  <dbReference type="AGR" id="RGD:3643"/>
  <dbReference type="CTD" id="6343"/>
  <dbReference type="RGD" id="3643">
    <property type="gene designation" value="Sct"/>
  </dbReference>
  <dbReference type="eggNOG" id="ENOG502R8F0">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000002624"/>
  <dbReference type="HOGENOM" id="CLU_1991937_0_0_1"/>
  <dbReference type="InParanoid" id="P11384"/>
  <dbReference type="OMA" id="HAPFPWL"/>
  <dbReference type="OrthoDB" id="4605098at2759"/>
  <dbReference type="PhylomeDB" id="P11384"/>
  <dbReference type="TreeFam" id="TF338215"/>
  <dbReference type="Reactome" id="R-RNO-420092">
    <property type="pathway name" value="Glucagon-type ligand receptors"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P11384"/>
  <dbReference type="Proteomes" id="UP000002494">
    <property type="component" value="Chromosome 1"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSRNOG00000017873">
    <property type="expression patterns" value="Expressed in duodenum and 13 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046659">
    <property type="term" value="F:digestive hormone activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001664">
    <property type="term" value="F:G protein-coupled receptor binding"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005102">
    <property type="term" value="F:signaling receptor binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007189">
    <property type="term" value="P:adenylate cyclase-activating G protein-coupled receptor signaling pathway"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007420">
    <property type="term" value="P:brain development"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0021542">
    <property type="term" value="P:dentate gyrus development"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002024">
    <property type="term" value="P:diet induced thermogenesis"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048566">
    <property type="term" value="P:embryonic digestive tract development"/>
    <property type="evidence" value="ECO:0000270"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0021766">
    <property type="term" value="P:hippocampus development"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0009992">
    <property type="term" value="P:intracellular water homeostasis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:1903640">
    <property type="term" value="P:negative regulation of gastrin-induced gastric acid secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043524">
    <property type="term" value="P:negative regulation of neuron apoptotic process"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051402">
    <property type="term" value="P:neuron apoptotic process"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0097150">
    <property type="term" value="P:neuronal stem cell population maintenance"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0043950">
    <property type="term" value="P:positive regulation of cAMP-mediated signaling"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050996">
    <property type="term" value="P:positive regulation of lipid catabolic process"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090187">
    <property type="term" value="P:positive regulation of pancreatic juice secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090274">
    <property type="term" value="P:positive regulation of somatostatin secretion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032098">
    <property type="term" value="P:regulation of appetite"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0048167">
    <property type="term" value="P:regulation of synaptic plasticity"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031667">
    <property type="term" value="P:response to nutrient levels"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008542">
    <property type="term" value="P:visual learning"/>
    <property type="evidence" value="ECO:0000266"/>
    <property type="project" value="RGD"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000532">
    <property type="entry name" value="Glucagon_GIP_secretin_VIP"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR015675">
    <property type="entry name" value="Prosecretin"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR17378">
    <property type="entry name" value="SECRETIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR17378:SF1">
    <property type="entry name" value="SECRETIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00123">
    <property type="entry name" value="Hormone_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00070">
    <property type="entry name" value="GLUCA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00260">
    <property type="entry name" value="GLUCAGON"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="21"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000011432" evidence="6">
    <location>
      <begin position="22"/>
      <end position="31"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000011433" description="Secretin" evidence="6">
    <location>
      <begin position="33"/>
      <end position="59"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000011434" evidence="6">
    <location>
      <begin position="63"/>
      <end position="134"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="6">
    <location>
      <position position="59"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="15">
    <location>
      <position position="63"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q08535"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="12403838"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="15276242"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="19805236"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="2719704"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="3047699"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="7958688"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="9">
    <source>
      <dbReference type="PubMed" id="8568688"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="10">
    <source>
      <dbReference type="PubMed" id="9506976"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="11">
    <source>
      <dbReference type="PubMed" id="9655680"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="12">
    <source>
      <dbReference type="PubMed" id="2315322"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="13"/>
  <evidence type="ECO:0000312" key="14">
    <source>
      <dbReference type="RGD" id="3643"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="15">
    <source>
      <dbReference type="PubMed" id="22673903"/>
    </source>
  </evidence>
  <sequence length="134" mass="15072" checksum="D9FA1A4C1F7C86E6" modified="1990-04-01" version="2" precursor="true">MEPLLPTPPLLLLLLLLLSSSFVLPAPPRTPRHSDGTFTSELSRLQDSARLQRLLQGLVGKRSEEDTENIPENSVARPKPLEDQLCLLWSNTQALQDWLLPRLSLDGSLSLWLPPGPRPAVDHSEWTETTRQPR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>