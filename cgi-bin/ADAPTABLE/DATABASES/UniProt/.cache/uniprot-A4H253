<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2007-05-29" modified="2024-10-02" version="41" xmlns="http://uniprot.org/uniprot">
  <accession>A4H253</accession>
  <name>DB128_PONPY</name>
  <protein>
    <recommendedName>
      <fullName>Beta-defensin 128</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Defensin, beta 128</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">DEFB128</name>
  </gene>
  <organism>
    <name type="scientific">Pongo pygmaeus</name>
    <name type="common">Bornean orangutan</name>
    <dbReference type="NCBI Taxonomy" id="9600"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Pongo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="2006-11" db="EMBL/GenBank/DDBJ databases">
      <title>Evolution and sequence variation of human beta-defensin genes.</title>
      <authorList>
        <person name="Hollox E.J."/>
        <person name="Armour J.A.L."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Has antibacterial activity.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the beta-defensin family.</text>
  </comment>
  <dbReference type="EMBL" id="AM410158">
    <property type="protein sequence ID" value="CAL68968.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="A4H253"/>
  <dbReference type="SMR" id="A4H253"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050544">
    <property type="entry name" value="Beta-defensin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR025933">
    <property type="entry name" value="Beta_defensin_dom"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001:SF5">
    <property type="entry name" value="BETA-DEFENSIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR15001">
    <property type="entry name" value="BETA-DEFENSIN 123-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF13841">
    <property type="entry name" value="Defensin_beta_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0211">Defensin</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000289853" description="Beta-defensin 128">
    <location>
      <begin position="19"/>
      <end position="93"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="24"/>
      <end position="52"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="32"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="36"/>
      <end position="53"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="93" mass="10728" checksum="61863925D8732577" modified="2007-05-01" version="1" precursor="true">MKLFLVLIILLFEVLTDGARLKKCFNNVTGYCRKKCKVGERYEIGCLSGKLCCINYEEEKEHVSFKKPHQHSGEKLSVQQDYIILPTVTIFTV</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>