<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="216" xmlns="http://uniprot.org/uniprot">
  <accession>P01036</accession>
  <accession>Q9UBI5</accession>
  <accession>Q9UCS9</accession>
  <name>CYTS_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Cystatin-S</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Cystatin-4</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Cystatin-SA-III</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Salivary acidic protein 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">CST4</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="Biochem. J." volume="278" first="627" last="635">
      <title>Human salivary cystatin S. Cloning, sequence analysis, hybridization in situ and immunocytochemistry.</title>
      <authorList>
        <person name="Bobek L.A."/>
        <person name="Aguirre A."/>
        <person name="Levine M.J."/>
      </authorList>
      <dbReference type="PubMed" id="1898352"/>
      <dbReference type="DOI" id="10.1042/bj2780627"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <source>
      <tissue>Submandibular gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1992" name="Agents Actions" volume="38" first="340" last="348">
      <title>Characterization of two members (CST4 and CST5) of the cystatin gene family and molecular evolution of cystatin genes.</title>
      <authorList>
        <person name="Saitoh E."/>
        <person name="Isemura S."/>
        <person name="Sanada K."/>
        <person name="Ohnishi K."/>
      </authorList>
      <dbReference type="PubMed" id="1334620"/>
      <dbReference type="DOI" id="10.1007/978-3-0348-7321-5_43"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2000-11" db="EMBL/GenBank/DDBJ databases">
      <title>Acquisition of complex patterns of differential expression in epithelial cell populations during the evolution of type 2 cystatin genes.</title>
      <authorList>
        <person name="Dickinson D.P."/>
        <person name="Hewett-Emmett D."/>
        <person name="Thiesse M."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2001" name="Nature" volume="414" first="865" last="871">
      <title>The DNA sequence and comparative analysis of human chromosome 20.</title>
      <authorList>
        <person name="Deloukas P."/>
        <person name="Matthews L.H."/>
        <person name="Ashurst J.L."/>
        <person name="Burton J."/>
        <person name="Gilbert J.G.R."/>
        <person name="Jones M."/>
        <person name="Stavrides G."/>
        <person name="Almeida J.P."/>
        <person name="Babbage A.K."/>
        <person name="Bagguley C.L."/>
        <person name="Bailey J."/>
        <person name="Barlow K.F."/>
        <person name="Bates K.N."/>
        <person name="Beard L.M."/>
        <person name="Beare D.M."/>
        <person name="Beasley O.P."/>
        <person name="Bird C.P."/>
        <person name="Blakey S.E."/>
        <person name="Bridgeman A.M."/>
        <person name="Brown A.J."/>
        <person name="Buck D."/>
        <person name="Burrill W.D."/>
        <person name="Butler A.P."/>
        <person name="Carder C."/>
        <person name="Carter N.P."/>
        <person name="Chapman J.C."/>
        <person name="Clamp M."/>
        <person name="Clark G."/>
        <person name="Clark L.N."/>
        <person name="Clark S.Y."/>
        <person name="Clee C.M."/>
        <person name="Clegg S."/>
        <person name="Cobley V.E."/>
        <person name="Collier R.E."/>
        <person name="Connor R.E."/>
        <person name="Corby N.R."/>
        <person name="Coulson A."/>
        <person name="Coville G.J."/>
        <person name="Deadman R."/>
        <person name="Dhami P.D."/>
        <person name="Dunn M."/>
        <person name="Ellington A.G."/>
        <person name="Frankland J.A."/>
        <person name="Fraser A."/>
        <person name="French L."/>
        <person name="Garner P."/>
        <person name="Grafham D.V."/>
        <person name="Griffiths C."/>
        <person name="Griffiths M.N.D."/>
        <person name="Gwilliam R."/>
        <person name="Hall R.E."/>
        <person name="Hammond S."/>
        <person name="Harley J.L."/>
        <person name="Heath P.D."/>
        <person name="Ho S."/>
        <person name="Holden J.L."/>
        <person name="Howden P.J."/>
        <person name="Huckle E."/>
        <person name="Hunt A.R."/>
        <person name="Hunt S.E."/>
        <person name="Jekosch K."/>
        <person name="Johnson C.M."/>
        <person name="Johnson D."/>
        <person name="Kay M.P."/>
        <person name="Kimberley A.M."/>
        <person name="King A."/>
        <person name="Knights A."/>
        <person name="Laird G.K."/>
        <person name="Lawlor S."/>
        <person name="Lehvaeslaiho M.H."/>
        <person name="Leversha M.A."/>
        <person name="Lloyd C."/>
        <person name="Lloyd D.M."/>
        <person name="Lovell J.D."/>
        <person name="Marsh V.L."/>
        <person name="Martin S.L."/>
        <person name="McConnachie L.J."/>
        <person name="McLay K."/>
        <person name="McMurray A.A."/>
        <person name="Milne S.A."/>
        <person name="Mistry D."/>
        <person name="Moore M.J.F."/>
        <person name="Mullikin J.C."/>
        <person name="Nickerson T."/>
        <person name="Oliver K."/>
        <person name="Parker A."/>
        <person name="Patel R."/>
        <person name="Pearce T.A.V."/>
        <person name="Peck A.I."/>
        <person name="Phillimore B.J.C.T."/>
        <person name="Prathalingam S.R."/>
        <person name="Plumb R.W."/>
        <person name="Ramsay H."/>
        <person name="Rice C.M."/>
        <person name="Ross M.T."/>
        <person name="Scott C.E."/>
        <person name="Sehra H.K."/>
        <person name="Shownkeen R."/>
        <person name="Sims S."/>
        <person name="Skuce C.D."/>
        <person name="Smith M.L."/>
        <person name="Soderlund C."/>
        <person name="Steward C.A."/>
        <person name="Sulston J.E."/>
        <person name="Swann R.M."/>
        <person name="Sycamore N."/>
        <person name="Taylor R."/>
        <person name="Tee L."/>
        <person name="Thomas D.W."/>
        <person name="Thorpe A."/>
        <person name="Tracey A."/>
        <person name="Tromans A.C."/>
        <person name="Vaudin M."/>
        <person name="Wall M."/>
        <person name="Wallis J.M."/>
        <person name="Whitehead S.L."/>
        <person name="Whittaker P."/>
        <person name="Willey D.L."/>
        <person name="Williams L."/>
        <person name="Williams S.A."/>
        <person name="Wilming L."/>
        <person name="Wray P.W."/>
        <person name="Hubbard T."/>
        <person name="Durbin R.M."/>
        <person name="Bentley D.R."/>
        <person name="Beck S."/>
        <person name="Rogers J."/>
      </authorList>
      <dbReference type="PubMed" id="11780052"/>
      <dbReference type="DOI" id="10.1038/414865a"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Thyroid</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1987" name="Biochem. Biophys. Res. Commun." volume="145" first="1248" last="1253">
      <title>Identification of a long form of cystatin from human saliva by rapid microbore HPLC mapping.</title>
      <authorList>
        <person name="Hawke D.H."/>
        <person name="Yuan P.M."/>
        <person name="Wilson K.J."/>
        <person name="Hunkapiller M.W."/>
      </authorList>
      <dbReference type="PubMed" id="3496880"/>
      <dbReference type="DOI" id="10.1016/0006-291x(87)91571-3"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-51</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="1991" name="Biochem. J." volume="280" first="341" last="352">
      <title>Large-scale purification and characterization of the major phosphoproteins and mucins of human submandibular-sublingual saliva.</title>
      <authorList>
        <person name="Ramasubbu N."/>
        <person name="Reddy M.S."/>
        <person name="Bergey E.J."/>
        <person name="Haraszthy G.G."/>
        <person name="Soni S.-D."/>
        <person name="Levine M.J."/>
      </authorList>
      <dbReference type="PubMed" id="1747107"/>
      <dbReference type="DOI" id="10.1042/bj2800341"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-55</scope>
    <scope>PHOSPHORYLATION AT SER-23</scope>
    <source>
      <tissue>Saliva</tissue>
    </source>
  </reference>
  <reference key="8">
    <citation type="journal article" date="1991" name="J. Biochem." volume="110" first="648" last="654">
      <title>Identification of full-sized forms of salivary (S-type) cystatins (cystatin SN, cystatin SA, cystatin S, and two phosphorylated forms of cystatin S) in human whole saliva and determination of phosphorylation sites of cystatin S.</title>
      <authorList>
        <person name="Isemura S."/>
        <person name="Saitoh E."/>
        <person name="Sanada K."/>
        <person name="Minakata K."/>
      </authorList>
      <dbReference type="PubMed" id="1778989"/>
      <dbReference type="DOI" id="10.1093/oxfordjournals.jbchem.a123634"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-36</scope>
    <scope>PHOSPHORYLATION AT SER-21 AND SER-23</scope>
    <source>
      <tissue>Saliva</tissue>
    </source>
  </reference>
  <reference key="9">
    <citation type="journal article" date="1991" name="Arch. Oral Biol." volume="36" first="631" last="636">
      <title>The effects of human salivary cystatins and statherin on hydroxyapatite crystallization.</title>
      <authorList>
        <person name="Johnsson M."/>
        <person name="Richardson C.F."/>
        <person name="Bergey E.J."/>
        <person name="Levine M.J."/>
        <person name="Nancollas G.H."/>
      </authorList>
      <dbReference type="PubMed" id="1741693"/>
      <dbReference type="DOI" id="10.1016/0003-9969(91)90014-l"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 21-36</scope>
    <scope>PHOSPHORYLATION AT SER-23</scope>
    <source>
      <tissue>Saliva</tissue>
    </source>
  </reference>
  <reference key="10">
    <citation type="journal article" date="1984" name="J. Biochem." volume="96" first="489" last="498">
      <title>Isolation and amino acid sequence of SAP-1, an acidic protein of human whole saliva, and sequence homology with human gamma-trace.</title>
      <authorList>
        <person name="Isemura S."/>
        <person name="Saitoh E."/>
        <person name="Sanada K."/>
      </authorList>
      <dbReference type="PubMed" id="6501254"/>
      <dbReference type="DOI" id="10.1093/oxfordjournals.jbchem.a134861"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 29-141</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2015" name="J. Proteome Res." volume="14" first="2649" last="2658">
      <title>Human basal tear peptidome characterization by CID, HCD, and ETD followed by in silico and in vitro analyses for antimicrobial peptide identification.</title>
      <authorList>
        <person name="Azkargorta M."/>
        <person name="Soria J."/>
        <person name="Ojeda C."/>
        <person name="Guzman F."/>
        <person name="Acera A."/>
        <person name="Iloro I."/>
        <person name="Suarez T."/>
        <person name="Elortza F."/>
      </authorList>
      <dbReference type="PubMed" id="25946035"/>
      <dbReference type="DOI" id="10.1021/acs.jproteome.5b00179"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 85-141</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <source>
      <tissue>Tear</tissue>
    </source>
  </reference>
  <reference key="12">
    <citation type="journal article" date="1984" name="J. Biochem." volume="96" first="1311" last="1314">
      <title>Cystatin S: a cysteine proteinase inhibitor of human saliva.</title>
      <authorList>
        <person name="Isemura S."/>
        <person name="Saitoh E."/>
        <person name="Ito S."/>
        <person name="Isemura M."/>
        <person name="Sanada K."/>
      </authorList>
      <dbReference type="PubMed" id="6394600"/>
      <dbReference type="DOI" id="10.1093/oxfordjournals.jbchem.a134952"/>
    </citation>
    <scope>INHIBITOR SPECIFICITY</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="1991" name="Arch. Biochem. Biophys." volume="288" first="664" last="670">
      <title>Salivary cystatin SA-III, a potential precursor of the acquired enamel pellicle, is phosphorylated at both its amino- and carboxyl-terminal regions.</title>
      <authorList>
        <person name="Lamkin M.S."/>
        <person name="Jensen J.L."/>
        <person name="Setayesh M.R."/>
        <person name="Troxler R.F."/>
        <person name="Oppenheim F.G."/>
      </authorList>
      <dbReference type="PubMed" id="1898055"/>
      <dbReference type="DOI" id="10.1016/0003-9861(91)90249-i"/>
    </citation>
    <scope>PHOSPHORYLATION</scope>
  </reference>
  <reference key="14">
    <citation type="journal article" date="2010" name="J. Am. Soc. Mass Spectrom." volume="21" first="908" last="917">
      <title>Confident assignment of intact mass tags to human salivary cystatins using top-down Fourier-transform ion cyclotron resonance mass spectrometry.</title>
      <authorList>
        <person name="Ryan C.M."/>
        <person name="Souda P."/>
        <person name="Halgand F."/>
        <person name="Wong D.T."/>
        <person name="Loo J.A."/>
        <person name="Faull K.F."/>
        <person name="Whitelegge J.P."/>
      </authorList>
      <dbReference type="PubMed" id="20189825"/>
      <dbReference type="DOI" id="10.1016/j.jasms.2010.01.025"/>
    </citation>
    <scope>PHOSPHORYLATION AT SER-21 AND SER-23</scope>
    <scope>DISULFIDE BONDS</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>MASS SPECTROMETRY</scope>
    <source>
      <tissue>Saliva</tissue>
    </source>
  </reference>
  <reference key="15">
    <citation type="journal article" date="2006" name="Science" volume="314" first="268" last="274">
      <title>The consensus coding sequences of human breast and colorectal cancers.</title>
      <authorList>
        <person name="Sjoeblom T."/>
        <person name="Jones S."/>
        <person name="Wood L.D."/>
        <person name="Parsons D.W."/>
        <person name="Lin J."/>
        <person name="Barber T.D."/>
        <person name="Mandelker D."/>
        <person name="Leary R.J."/>
        <person name="Ptak J."/>
        <person name="Silliman N."/>
        <person name="Szabo S."/>
        <person name="Buckhaults P."/>
        <person name="Farrell C."/>
        <person name="Meeh P."/>
        <person name="Markowitz S.D."/>
        <person name="Willis J."/>
        <person name="Dawson D."/>
        <person name="Willson J.K.V."/>
        <person name="Gazdar A.F."/>
        <person name="Hartigan J."/>
        <person name="Wu L."/>
        <person name="Liu C."/>
        <person name="Parmigiani G."/>
        <person name="Park B.H."/>
        <person name="Bachman K.E."/>
        <person name="Papadopoulos N."/>
        <person name="Vogelstein B."/>
        <person name="Kinzler K.W."/>
        <person name="Velculescu V.E."/>
      </authorList>
      <dbReference type="PubMed" id="16959974"/>
      <dbReference type="DOI" id="10.1126/science.1133427"/>
    </citation>
    <scope>VARIANT [LARGE SCALE ANALYSIS] ASN-77</scope>
  </reference>
  <comment type="function">
    <text>This protein strongly inhibits papain and ficin, partially inhibits stem bromelain and bovine cathepsin C, but does not inhibit porcine cathepsin B or clostripain. Papain is inhibited non-competitively.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-1049999">
      <id>P01036</id>
    </interactant>
    <interactant intactId="EBI-947187">
      <id>Q9UHD9</id>
      <label>UBQLN2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="7">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="7">Expressed in submandibular and sublingual saliva but not in parotid saliva (at protein level). Expressed in saliva, tears, urine and seminal fluid.</text>
  </comment>
  <comment type="PTM">
    <text evidence="3 4 5 6 7">Phosphorylated at both its N- and C-terminal regions.</text>
  </comment>
  <comment type="mass spectrometry" mass="14175.856" error="0.0564" method="Electrospray" evidence="7"/>
  <comment type="mass spectrometry" mass="14255.856" error="0.0899" method="Electrospray" evidence="7">
    <text>Monophosphorylated at Ser-23, also called form S1.</text>
  </comment>
  <comment type="mass spectrometry" mass="14335.811" error="0.0775" method="Electrospray" evidence="7">
    <text>Diphosphorylated at Ser-21 and Ser-23, also called form S2.</text>
  </comment>
  <comment type="similarity">
    <text evidence="9">Belongs to the cystatin family.</text>
  </comment>
  <dbReference type="EMBL" id="X54667">
    <property type="protein sequence ID" value="CAA38478.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="S51222">
    <property type="protein sequence ID" value="AAB24493.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="S51214">
    <property type="protein sequence ID" value="AAB24493.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="S51219">
    <property type="protein sequence ID" value="AAB24493.1"/>
    <property type="status" value="JOINED"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AF319565">
    <property type="protein sequence ID" value="AAK11571.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL359433">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC065714">
    <property type="protein sequence ID" value="AAH65714.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC074952">
    <property type="protein sequence ID" value="AAH74952.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC074953">
    <property type="protein sequence ID" value="AAH74953.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS13159.1"/>
  <dbReference type="PIR" id="S17667">
    <property type="entry name" value="UDHUP1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001890.1">
    <property type="nucleotide sequence ID" value="NM_001899.2"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01036"/>
  <dbReference type="SMR" id="P01036"/>
  <dbReference type="BioGRID" id="107854">
    <property type="interactions" value="110"/>
  </dbReference>
  <dbReference type="IntAct" id="P01036">
    <property type="interactions" value="83"/>
  </dbReference>
  <dbReference type="STRING" id="9606.ENSP00000217423"/>
  <dbReference type="MEROPS" id="I25.008"/>
  <dbReference type="iPTMnet" id="P01036"/>
  <dbReference type="PhosphoSitePlus" id="P01036"/>
  <dbReference type="SwissPalm" id="P01036"/>
  <dbReference type="BioMuta" id="CST4"/>
  <dbReference type="DMDM" id="399336"/>
  <dbReference type="jPOST" id="P01036"/>
  <dbReference type="MassIVE" id="P01036"/>
  <dbReference type="PaxDb" id="9606-ENSP00000217423"/>
  <dbReference type="PeptideAtlas" id="P01036"/>
  <dbReference type="PRIDE" id="P01036"/>
  <dbReference type="ProteomicsDB" id="51312"/>
  <dbReference type="TopDownProteomics" id="P01036"/>
  <dbReference type="Antibodypedia" id="9816">
    <property type="antibodies" value="337 antibodies from 26 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="1472"/>
  <dbReference type="Ensembl" id="ENST00000217423.4">
    <property type="protein sequence ID" value="ENSP00000217423.3"/>
    <property type="gene ID" value="ENSG00000101441.5"/>
  </dbReference>
  <dbReference type="GeneID" id="1472"/>
  <dbReference type="KEGG" id="hsa:1472"/>
  <dbReference type="MANE-Select" id="ENST00000217423.4">
    <property type="protein sequence ID" value="ENSP00000217423.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_001899.3"/>
    <property type="RefSeq protein sequence ID" value="NP_001890.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc002wto.2">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:2476"/>
  <dbReference type="CTD" id="1472"/>
  <dbReference type="DisGeNET" id="1472"/>
  <dbReference type="GeneCards" id="CST4"/>
  <dbReference type="HGNC" id="HGNC:2476">
    <property type="gene designation" value="CST4"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000101441">
    <property type="expression patterns" value="Tissue enriched (salivary)"/>
  </dbReference>
  <dbReference type="MIM" id="123857">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P01036"/>
  <dbReference type="OpenTargets" id="ENSG00000101441"/>
  <dbReference type="PharmGKB" id="PA26977"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000101441"/>
  <dbReference type="eggNOG" id="ENOG502SC50">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00940000163410"/>
  <dbReference type="HOGENOM" id="CLU_118168_0_1_1"/>
  <dbReference type="InParanoid" id="P01036"/>
  <dbReference type="OMA" id="PWEDKMS"/>
  <dbReference type="OrthoDB" id="3086783at2759"/>
  <dbReference type="PhylomeDB" id="P01036"/>
  <dbReference type="PathwayCommons" id="P01036"/>
  <dbReference type="SignaLink" id="P01036"/>
  <dbReference type="BioGRID-ORCS" id="1472">
    <property type="hits" value="20 hits in 1112 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="CST4">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="GeneWiki" id="CST4"/>
  <dbReference type="GenomeRNAi" id="1472"/>
  <dbReference type="Pharos" id="P01036">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P01036"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 20"/>
  </dbReference>
  <dbReference type="RNAct" id="P01036">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000101441">
    <property type="expression patterns" value="Expressed in parotid gland and 57 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005737">
    <property type="term" value="C:cytoplasm"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070062">
    <property type="term" value="C:extracellular exosome"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031982">
    <property type="term" value="C:vesicle"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004869">
    <property type="term" value="F:cysteine-type endopeptidase inhibitor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001580">
    <property type="term" value="P:detection of chemical stimulus involved in sensory perception of bitter taste"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0010466">
    <property type="term" value="P:negative regulation of peptidase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045861">
    <property type="term" value="P:negative regulation of proteolysis"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="CDD" id="cd00042">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.450.10:FF:000004">
    <property type="entry name" value="Cystatin C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.450.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000010">
    <property type="entry name" value="Cystatin_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046350">
    <property type="entry name" value="Cystatin_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018073">
    <property type="entry name" value="Prot_inh_cystat_CS"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46186">
    <property type="entry name" value="CYSTATIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR46186:SF3">
    <property type="entry name" value="CYSTATIN-S-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00031">
    <property type="entry name" value="Cystatin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00043">
    <property type="entry name" value="CY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54403">
    <property type="entry name" value="Cystatin/monellin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00287">
    <property type="entry name" value="CYSTATIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0597">Phosphoprotein</keyword>
  <keyword id="KW-0646">Protease inhibitor</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0789">Thiol protease inhibitor</keyword>
  <feature type="signal peptide" evidence="3 4 5 8">
    <location>
      <begin position="1"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000006650" description="Cystatin-S">
    <location>
      <begin position="21"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Secondary area of contact">
    <location>
      <begin position="76"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="site" description="Reactive site">
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="5 7">
    <location>
      <position position="21"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phosphoserine" evidence="3 4 5 7">
    <location>
      <position position="23"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="94"/>
      <end position="104"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="118"/>
      <end position="138"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_048852" description="In dbSNP:rs3210291.">
    <original>D</original>
    <variation>N</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_036549" description="In a breast cancer sample; somatic mutation." evidence="2">
    <original>T</original>
    <variation>N</variation>
    <location>
      <position position="77"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 10; AA sequence." evidence="9" ref="10">
    <original>N</original>
    <variation>D</variation>
    <location>
      <position position="135"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="16959974"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="1741693"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="1747107"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="1778989"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="1898055"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source>
      <dbReference type="PubMed" id="20189825"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="8">
    <source>
      <dbReference type="PubMed" id="3496880"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="9"/>
  <sequence length="141" mass="16214" checksum="65B1FEB8F074DEA6" modified="1993-07-01" version="3" precursor="true">MARPLCTLLLLMATLAGALASSSKEENRIIPGGIYDADLNDEWVQRALHFAISEYNKATEDEYYRRPLQVLRAREQTFGGVNYFFDVEVGRTICTKSQPNLDTCAFHEQPELQKKQLCSFEIYEVPWEDRMSLVNSRCQEA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>