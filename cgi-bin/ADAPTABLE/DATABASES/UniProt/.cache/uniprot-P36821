<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-06-01" modified="2024-10-02" version="99" xmlns="http://uniprot.org/uniprot">
  <accession>P36821</accession>
  <name>VE7_HPV17</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">Protein E7</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">E7</name>
  </gene>
  <organism>
    <name type="scientific">Human papillomavirus 17</name>
    <dbReference type="NCBI Taxonomy" id="10607"/>
    <lineage>
      <taxon>Viruses</taxon>
      <taxon>Monodnaviria</taxon>
      <taxon>Shotokuvirae</taxon>
      <taxon>Cossaviricota</taxon>
      <taxon>Papovaviricetes</taxon>
      <taxon>Zurhausenvirales</taxon>
      <taxon>Papillomaviridae</taxon>
      <taxon>Firstpapillomavirinae</taxon>
      <taxon>Betapapillomavirus</taxon>
      <taxon>Betapapillomavirus 2</taxon>
    </lineage>
  </organism>
  <organismHost>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
  </organismHost>
  <reference key="1">
    <citation type="journal article" date="1994" name="Curr. Top. Microbiol. Immunol." volume="186" first="13" last="31">
      <title>Primer-directed sequencing of human papillomavirus types.</title>
      <authorList>
        <person name="Delius H."/>
        <person name="Hofmann B."/>
      </authorList>
      <dbReference type="PubMed" id="8205838"/>
      <dbReference type="DOI" id="10.1007/978-3-642-78487-3_2"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2002" name="Rev. Med. Virol." volume="12" first="81" last="92">
      <title>Interactions of SV40 large T antigen and other viral proteins with retinoblastoma tumour suppressor.</title>
      <authorList>
        <person name="Lee C."/>
        <person name="Cho Y."/>
      </authorList>
      <dbReference type="PubMed" id="11921304"/>
      <dbReference type="DOI" id="10.1002/rmv.340"/>
    </citation>
    <scope>REVIEW</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays a role in viral genome replication by driving entry of quiescent cells into the cell cycle. Stimulation of progression from G1 to S phase allows the virus to efficiently use the cellular DNA replicating machinery to achieve viral genome replication. E7 protein has both transforming and trans-activating activities. Induces the disassembly of the E2F1 transcription factor from RB1, with subsequent transcriptional activation of E2F1-regulated S-phase genes. Interferes with host histone deacetylation mediated by HDAC1 and HDAC2, leading to transcription activation. Also plays a role in the inhibition of both antiviral and antiproliferative functions of host interferon alpha. Interaction with host TMEM173/STING impairs the ability of TMEM173/STING to sense cytosolic DNA and promote the production of type I interferon (IFN-alpha and IFN-beta).</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Homodimer. Homooligomer. Interacts with host RB1; this interaction induces dissociation of RB1-E2F1 complex thereby disrupting RB1 activity. Interacts with host EP300; this interaction represses EP300 transcriptional activity. Interacts with protein E2; this interaction inhibits E7 oncogenic activity. Interacts with host TMEM173/STING; this interaction impairs the ability of TMEM173/STING to sense cytosolic DNA and promote the production of type I interferon (IFN-alpha and IFN-beta).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Host cytoplasm</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Host nucleus</location>
    </subcellularLocation>
    <text evidence="1">Predominantly found in the host nucleus.</text>
  </comment>
  <comment type="domain">
    <text evidence="1">The E7 terminal domain is an intrinsically disordered domain, whose flexibility and conformational transitions confer target adaptability to the oncoprotein. It allows adaptation to a variety of protein targets and exposes the PEST degradation sequence that regulates its turnover in the cell.</text>
  </comment>
  <comment type="PTM">
    <text evidence="1">Highly phosphorylated.</text>
  </comment>
  <comment type="similarity">
    <text evidence="1">Belongs to the papillomaviridae E7 protein family.</text>
  </comment>
  <dbReference type="EMBL" id="X74469">
    <property type="protein sequence ID" value="CAA52513.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="S36480">
    <property type="entry name" value="S36480"/>
  </dbReference>
  <dbReference type="SMR" id="P36821"/>
  <dbReference type="Proteomes" id="UP000006932">
    <property type="component" value="Genome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030430">
    <property type="term" value="C:host cell cytoplasm"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042025">
    <property type="term" value="C:host cell nucleus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003677">
    <property type="term" value="F:DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003700">
    <property type="term" value="F:DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0046872">
    <property type="term" value="F:metal ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019904">
    <property type="term" value="F:protein domain specific binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006351">
    <property type="term" value="P:DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039645">
    <property type="term" value="P:symbiont-mediated perturbation of host cell cycle G1/S transition checkpoint"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0052170">
    <property type="term" value="P:symbiont-mediated suppression of host innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0039502">
    <property type="term" value="P:symbiont-mediated suppression of host type I interferon-mediated signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019049">
    <property type="term" value="P:virus-mediated perturbation of host defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.160.330">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_04004">
    <property type="entry name" value="PPV_E7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000148">
    <property type="entry name" value="Papilloma_E7"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00527">
    <property type="entry name" value="E7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PIRSF" id="PIRSF003407">
    <property type="entry name" value="Papvi_E7"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF161234">
    <property type="entry name" value="E7 C-terminal domain-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0010">Activator</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0244">Early protein</keyword>
  <keyword id="KW-1078">G1/S host cell cycle checkpoint dysregulation by virus</keyword>
  <keyword id="KW-1035">Host cytoplasm</keyword>
  <keyword id="KW-1048">Host nucleus</keyword>
  <keyword id="KW-0945">Host-virus interaction</keyword>
  <keyword id="KW-1090">Inhibition of host innate immune response by virus</keyword>
  <keyword id="KW-1114">Inhibition of host interferon signaling pathway by virus</keyword>
  <keyword id="KW-0922">Interferon antiviral system evasion</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-1121">Modulation of host cell cycle by virus</keyword>
  <keyword id="KW-0553">Oncogene</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <keyword id="KW-0899">Viral immunoevasion</keyword>
  <keyword id="KW-0862">Zinc</keyword>
  <keyword id="KW-0863">Zinc-finger</keyword>
  <feature type="chain" id="PRO_0000133415" description="Protein E7">
    <location>
      <begin position="1"/>
      <end position="95"/>
    </location>
  </feature>
  <feature type="zinc finger region" evidence="1">
    <location>
      <begin position="52"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="region of interest" description="E7 terminal domain" evidence="1">
    <location>
      <begin position="1"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="LXCXE motif; interaction with host RB1 and TMEM173/STING" evidence="1">
    <location>
      <begin position="24"/>
      <end position="28"/>
    </location>
  </feature>
  <feature type="short sequence motif" description="Nuclear export signal" evidence="1">
    <location>
      <begin position="70"/>
      <end position="78"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_04004"/>
    </source>
  </evidence>
  <sequence length="95" mass="10860" checksum="86AB02A77BFD80D9" modified="1994-06-01" version="1">MIGKEATIPDIVLELQQLVQPTDLHCYEELSEEETETEEEPRRIPYKIVAPCCFCGSKLRLIVLATHAGIRSQEELLLGEVQLVCPNCREKLRHD</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>