<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-04-13" modified="2024-10-02" version="89" xmlns="http://uniprot.org/uniprot">
  <accession>P82763</accession>
  <accession>A0MJW7</accession>
  <accession>A7REH1</accession>
  <name>DEF51_ARATH</name>
  <protein>
    <recommendedName>
      <fullName>Defensin-like protein 51</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Low-molecular-weight cysteine-rich protein 48</fullName>
      <shortName>Protein LCR48</shortName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">LCR48</name>
    <name type="ordered locus">At3g48231</name>
    <name type="ORF">T24C20</name>
  </gene>
  <organism evidence="3">
    <name type="scientific">Arabidopsis thaliana</name>
    <name type="common">Mouse-ear cress</name>
    <dbReference type="NCBI Taxonomy" id="3702"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Viridiplantae</taxon>
      <taxon>Streptophyta</taxon>
      <taxon>Embryophyta</taxon>
      <taxon>Tracheophyta</taxon>
      <taxon>Spermatophyta</taxon>
      <taxon>Magnoliopsida</taxon>
      <taxon>eudicotyledons</taxon>
      <taxon>Gunneridae</taxon>
      <taxon>Pentapetalae</taxon>
      <taxon>rosids</taxon>
      <taxon>malvids</taxon>
      <taxon>Brassicales</taxon>
      <taxon>Brassicaceae</taxon>
      <taxon>Camelineae</taxon>
      <taxon>Arabidopsis</taxon>
    </lineage>
  </organism>
  <reference evidence="3" key="1">
    <citation type="journal article" date="2000" name="Nature" volume="408" first="820" last="822">
      <title>Sequence and analysis of chromosome 3 of the plant Arabidopsis thaliana.</title>
      <authorList>
        <person name="Salanoubat M."/>
        <person name="Lemcke K."/>
        <person name="Rieger M."/>
        <person name="Ansorge W."/>
        <person name="Unseld M."/>
        <person name="Fartmann B."/>
        <person name="Valle G."/>
        <person name="Bloecker H."/>
        <person name="Perez-Alonso M."/>
        <person name="Obermaier B."/>
        <person name="Delseny M."/>
        <person name="Boutry M."/>
        <person name="Grivell L.A."/>
        <person name="Mache R."/>
        <person name="Puigdomenech P."/>
        <person name="De Simone V."/>
        <person name="Choisne N."/>
        <person name="Artiguenave F."/>
        <person name="Robert C."/>
        <person name="Brottier P."/>
        <person name="Wincker P."/>
        <person name="Cattolico L."/>
        <person name="Weissenbach J."/>
        <person name="Saurin W."/>
        <person name="Quetier F."/>
        <person name="Schaefer M."/>
        <person name="Mueller-Auer S."/>
        <person name="Gabel C."/>
        <person name="Fuchs M."/>
        <person name="Benes V."/>
        <person name="Wurmbach E."/>
        <person name="Drzonek H."/>
        <person name="Erfle H."/>
        <person name="Jordan N."/>
        <person name="Bangert S."/>
        <person name="Wiedelmann R."/>
        <person name="Kranz H."/>
        <person name="Voss H."/>
        <person name="Holland R."/>
        <person name="Brandt P."/>
        <person name="Nyakatura G."/>
        <person name="Vezzi A."/>
        <person name="D'Angelo M."/>
        <person name="Pallavicini A."/>
        <person name="Toppo S."/>
        <person name="Simionati B."/>
        <person name="Conrad A."/>
        <person name="Hornischer K."/>
        <person name="Kauer G."/>
        <person name="Loehnert T.-H."/>
        <person name="Nordsiek G."/>
        <person name="Reichelt J."/>
        <person name="Scharfe M."/>
        <person name="Schoen O."/>
        <person name="Bargues M."/>
        <person name="Terol J."/>
        <person name="Climent J."/>
        <person name="Navarro P."/>
        <person name="Collado C."/>
        <person name="Perez-Perez A."/>
        <person name="Ottenwaelder B."/>
        <person name="Duchemin D."/>
        <person name="Cooke R."/>
        <person name="Laudie M."/>
        <person name="Berger-Llauro C."/>
        <person name="Purnelle B."/>
        <person name="Masuy D."/>
        <person name="de Haan M."/>
        <person name="Maarse A.C."/>
        <person name="Alcaraz J.-P."/>
        <person name="Cottet A."/>
        <person name="Casacuberta E."/>
        <person name="Monfort A."/>
        <person name="Argiriou A."/>
        <person name="Flores M."/>
        <person name="Liguori R."/>
        <person name="Vitale D."/>
        <person name="Mannhaupt G."/>
        <person name="Haase D."/>
        <person name="Schoof H."/>
        <person name="Rudd S."/>
        <person name="Zaccaria P."/>
        <person name="Mewes H.-W."/>
        <person name="Mayer K.F.X."/>
        <person name="Kaul S."/>
        <person name="Town C.D."/>
        <person name="Koo H.L."/>
        <person name="Tallon L.J."/>
        <person name="Jenkins J."/>
        <person name="Rooney T."/>
        <person name="Rizzo M."/>
        <person name="Walts A."/>
        <person name="Utterback T."/>
        <person name="Fujii C.Y."/>
        <person name="Shea T.P."/>
        <person name="Creasy T.H."/>
        <person name="Haas B."/>
        <person name="Maiti R."/>
        <person name="Wu D."/>
        <person name="Peterson J."/>
        <person name="Van Aken S."/>
        <person name="Pai G."/>
        <person name="Militscher J."/>
        <person name="Sellers P."/>
        <person name="Gill J.E."/>
        <person name="Feldblyum T.V."/>
        <person name="Preuss D."/>
        <person name="Lin X."/>
        <person name="Nierman W.C."/>
        <person name="Salzberg S.L."/>
        <person name="White O."/>
        <person name="Venter J.C."/>
        <person name="Fraser C.M."/>
        <person name="Kaneko T."/>
        <person name="Nakamura Y."/>
        <person name="Sato S."/>
        <person name="Kato T."/>
        <person name="Asamizu E."/>
        <person name="Sasamoto S."/>
        <person name="Kimura T."/>
        <person name="Idesawa K."/>
        <person name="Kawashima K."/>
        <person name="Kishida Y."/>
        <person name="Kiyokawa C."/>
        <person name="Kohara M."/>
        <person name="Matsumoto M."/>
        <person name="Matsuno A."/>
        <person name="Muraki A."/>
        <person name="Nakayama S."/>
        <person name="Nakazaki N."/>
        <person name="Shinpo S."/>
        <person name="Takeuchi C."/>
        <person name="Wada T."/>
        <person name="Watanabe A."/>
        <person name="Yamada M."/>
        <person name="Yasuda M."/>
        <person name="Tabata S."/>
      </authorList>
      <dbReference type="PubMed" id="11130713"/>
      <dbReference type="DOI" id="10.1038/35048706"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2017" name="Plant J." volume="89" first="789" last="804">
      <title>Araport11: a complete reannotation of the Arabidopsis thaliana reference genome.</title>
      <authorList>
        <person name="Cheng C.Y."/>
        <person name="Krishnakumar V."/>
        <person name="Chan A.P."/>
        <person name="Thibaud-Nissen F."/>
        <person name="Schobel S."/>
        <person name="Town C.D."/>
      </authorList>
      <dbReference type="PubMed" id="27862469"/>
      <dbReference type="DOI" id="10.1111/tpj.13415"/>
    </citation>
    <scope>GENOME REANNOTATION</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2006" name="Plant Biotechnol. J." volume="4" first="317" last="324">
      <title>Simultaneous high-throughput recombinational cloning of open reading frames in closed and open configurations.</title>
      <authorList>
        <person name="Underwood B.A."/>
        <person name="Vanderhaeghen R."/>
        <person name="Whitford R."/>
        <person name="Town C.D."/>
        <person name="Hilson P."/>
      </authorList>
      <dbReference type="PubMed" id="17147637"/>
      <dbReference type="DOI" id="10.1111/j.1467-7652.2006.00183.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2007" name="Plant J." volume="51" first="262" last="280">
      <title>Small cysteine-rich peptides resembling antimicrobial peptides have been under-predicted in plants.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Moskal W.A. Jr."/>
        <person name="Wu H.C."/>
        <person name="Underwood B.A."/>
        <person name="Graham M.A."/>
        <person name="Town C.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="17565583"/>
      <dbReference type="DOI" id="10.1111/j.1365-313x.2007.03136.x"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <strain>cv. Columbia</strain>
    </source>
  </reference>
  <reference evidence="3" key="5">
    <citation type="journal article" date="2001" name="Plant Mol. Biol." volume="46" first="17" last="34">
      <title>Two large Arabidopsis thaliana gene families are homologous to the Brassica gene superfamily that encodes pollen coat proteins and the male component of the self-incompatibility response.</title>
      <authorList>
        <person name="Vanoosthuyse V."/>
        <person name="Miege C."/>
        <person name="Dumas C."/>
        <person name="Cock J.M."/>
      </authorList>
      <dbReference type="PubMed" id="11437247"/>
      <dbReference type="DOI" id="10.1023/a:1010664704926"/>
    </citation>
    <scope>IDENTIFICATION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2005" name="Plant Physiol." volume="138" first="600" last="610">
      <title>Genome organization of more than 300 defensin-like genes in Arabidopsis.</title>
      <authorList>
        <person name="Silverstein K.A.T."/>
        <person name="Graham M.A."/>
        <person name="Paape T.D."/>
        <person name="VandenBosch K.A."/>
      </authorList>
      <dbReference type="PubMed" id="15955924"/>
      <dbReference type="DOI" id="10.1104/pp.105.060079"/>
    </citation>
    <scope>GENE FAMILY</scope>
  </reference>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the DEFL family.</text>
  </comment>
  <comment type="sequence caution" evidence="3">
    <conflict type="erroneous termination">
      <sequence resource="EMBL-CDS" id="ABK27962" version="1"/>
    </conflict>
    <text>Extended C-terminus.</text>
  </comment>
  <dbReference type="EMBL" id="AL096856">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CP002686">
    <property type="protein sequence ID" value="AEE78388.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ912220">
    <property type="protein sequence ID" value="ABI34023.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ912266">
    <property type="protein sequence ID" value="ABK27962.1"/>
    <property type="status" value="ALT_SEQ"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF182814">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001030830.1">
    <property type="nucleotide sequence ID" value="NM_001035753.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P82763"/>
  <dbReference type="SMR" id="P82763"/>
  <dbReference type="STRING" id="3702.P82763"/>
  <dbReference type="PaxDb" id="3702-AT3G48231.1"/>
  <dbReference type="ProteomicsDB" id="224045"/>
  <dbReference type="EnsemblPlants" id="AT3G48231.1">
    <property type="protein sequence ID" value="AT3G48231.1"/>
    <property type="gene ID" value="AT3G48231"/>
  </dbReference>
  <dbReference type="GeneID" id="3769681"/>
  <dbReference type="Gramene" id="AT3G48231.1">
    <property type="protein sequence ID" value="AT3G48231.1"/>
    <property type="gene ID" value="AT3G48231"/>
  </dbReference>
  <dbReference type="KEGG" id="ath:AT3G48231"/>
  <dbReference type="Araport" id="AT3G48231"/>
  <dbReference type="TAIR" id="AT3G48231">
    <property type="gene designation" value="LCR48"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_165205_1_0_1"/>
  <dbReference type="InParanoid" id="P82763"/>
  <dbReference type="OMA" id="APEHCCC"/>
  <dbReference type="OrthoDB" id="679687at2759"/>
  <dbReference type="PhylomeDB" id="P82763"/>
  <dbReference type="PRO" id="PR:P82763"/>
  <dbReference type="Proteomes" id="UP000006548">
    <property type="component" value="Chromosome 3"/>
  </dbReference>
  <dbReference type="ExpressionAtlas" id="P82763">
    <property type="expression patterns" value="baseline and differential"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0611">Plant defense</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="27"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000017287" description="Defensin-like protein 51">
    <location>
      <begin position="28"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="39"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="52"/>
      <end position="77"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="56"/>
      <end position="78"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000305" key="3"/>
  <sequence length="80" mass="8718" checksum="03256D34B899091B" modified="2002-10-01" version="1" precursor="true">MGFTKILVTFFLVGLLVISSSPQNAIASEIKAKINGLECFNTCTSYYDDHKCNVDCLSSGYPAGECYTVSPSQPKKCCCY</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>