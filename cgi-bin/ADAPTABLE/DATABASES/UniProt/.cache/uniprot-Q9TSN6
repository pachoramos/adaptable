<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2003-01-27" modified="2024-11-27" version="86" xmlns="http://uniprot.org/uniprot">
  <accession>Q9TSN6</accession>
  <accession>A3FMK7</accession>
  <accession>Q0PNI0</accession>
  <accession>Q645J4</accession>
  <accession>Q645J5</accession>
  <accession>Q645J6</accession>
  <accession>Q645J7</accession>
  <accession>Q645J8</accession>
  <name>LALBA_BUBBU</name>
  <protein>
    <recommendedName>
      <fullName>Alpha-lactalbumin</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Lactose synthase B protein</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">LALBA</name>
  </gene>
  <organism>
    <name type="scientific">Bubalus bubalis</name>
    <name type="common">Domestic water buffalo</name>
    <dbReference type="NCBI Taxonomy" id="89462"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Ruminantia</taxon>
      <taxon>Pecora</taxon>
      <taxon>Bovidae</taxon>
      <taxon>Bovinae</taxon>
      <taxon>Bubalus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="submission" date="1999-10" db="EMBL/GenBank/DDBJ databases">
      <title>Research on the sequences of milk protein genes in ruminants.</title>
      <authorList>
        <person name="Fan B."/>
        <person name="Li N."/>
        <person name="Wu C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
  </reference>
  <reference key="2">
    <citation type="submission" date="2006-06" db="EMBL/GenBank/DDBJ databases">
      <title>Cloning and characterization of buffalo alpha-lactalbumin gene.</title>
      <authorList>
        <person name="Bhattacharya T.K."/>
        <person name="Kumar P."/>
        <person name="Sharma A."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>VARIANTS SER-12 AND ARG-32</scope>
    <source>
      <tissue>Mammary gland</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2005" name="DNA Seq." volume="16" first="173" last="179">
      <title>Genetic polymorphism of alpha-lactalbumin gene in riverine buffalo.</title>
      <authorList>
        <person name="Dayal S."/>
        <person name="Bhattacharya T.K."/>
        <person name="Vohra V."/>
        <person name="Kumar P."/>
        <person name="Sharma A."/>
      </authorList>
      <dbReference type="PubMed" id="16147872"/>
      <dbReference type="DOI" id="10.1080/10425170500088205"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] OF 1-97 (ALLELES A; B; C; D AND E)</scope>
    <scope>VARIANTS SER-12; ARG-32; ALA-35; CYS-39; ASP-68; MET-85; LYS-85; LYS-93 AND GLY-96</scope>
    <source>
      <strain>Bhadawari</strain>
      <strain>Mehsana</strain>
      <strain>Murrah</strain>
      <strain>Surti</strain>
      <tissue>Blood</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="submission" date="2007-01" db="EMBL/GenBank/DDBJ databases">
      <title>A new variant of alpha-lactalbumin gene in water buffalo.</title>
      <authorList>
        <person name="Nautiyal B."/>
        <person name="Saxena P."/>
        <person name="Singh K.B."/>
        <person name="Sharma D."/>
        <person name="Mohapatra J.K."/>
        <person name="Rasool T.J."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA] OF 1-44 (ALLELE E)</scope>
    <scope>VARIANT SER-43</scope>
    <source>
      <tissue>Blood</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="submission" date="2010-10" db="UniProtKB">
      <title>Antibacterial activity analysis of buffalo milk whey protein.</title>
      <authorList>
        <person name="Meignanalakshmi S."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE OF 19-142</scope>
    <scope>MASS SPECTROMETRY</scope>
  </reference>
  <comment type="function">
    <text>Regulatory subunit of lactose synthase, changes the substrate specificity of galactosyltransferase in the mammary gland making glucose a good acceptor substrate for this enzyme. This enables LS to synthesize lactose, the major carbohydrate component of milk. In other tissues, galactosyltransferase transfers galactose onto the N-acetylglucosamine of the oligosaccharide chains in glycoproteins.</text>
  </comment>
  <comment type="subunit">
    <text>Lactose synthase (LS) is a heterodimer of a catalytic component, beta1,4-galactosyltransferase (beta4Gal-T1) and a regulatory component, alpha-lactalbumin (LA).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Mammary gland specific. Secreted in milk.</text>
  </comment>
  <comment type="mass spectrometry" mass="14227.0" method="MALDI" evidence="7"/>
  <comment type="miscellaneous">
    <text>In vitro digestion with trypsin produces a peptide comprising residues Ile-78 to Lys-98 which has antibacterial activity. It is active against Gram-negative bacterium E.coli (MIC=150 ug/ml) and against Gram-positive bacterium S.aureus (MIC=250 ug/ml).</text>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the glycosyl hydrolase 22 family.</text>
  </comment>
  <dbReference type="EMBL" id="AF194373">
    <property type="protein sequence ID" value="AAF06794.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="DQ785796">
    <property type="protein sequence ID" value="ABG78269.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AH014129">
    <property type="protein sequence ID" value="AAU13909.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AH014130">
    <property type="protein sequence ID" value="AAU13910.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AH014131">
    <property type="protein sequence ID" value="AAU13911.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AH014132">
    <property type="protein sequence ID" value="AAU13912.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AY726617">
    <property type="protein sequence ID" value="AAU13913.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="EF408824">
    <property type="protein sequence ID" value="ABN54437.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="S74175">
    <property type="entry name" value="S74175"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001277865.1">
    <property type="nucleotide sequence ID" value="NM_001290936.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9TSN6"/>
  <dbReference type="BMRB" id="Q9TSN6"/>
  <dbReference type="SMR" id="Q9TSN6"/>
  <dbReference type="CAZy" id="GH22">
    <property type="family name" value="Glycoside Hydrolase Family 22"/>
  </dbReference>
  <dbReference type="GlyCosmos" id="Q9TSN6">
    <property type="glycosylation" value="2 sites, No reported glycans"/>
  </dbReference>
  <dbReference type="GeneID" id="102410146"/>
  <dbReference type="KEGG" id="bbub:102410146"/>
  <dbReference type="CTD" id="3906"/>
  <dbReference type="OrthoDB" id="5344399at2759"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005509">
    <property type="term" value="F:calcium ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0004461">
    <property type="term" value="F:lactose synthase activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003796">
    <property type="term" value="F:lysozyme activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005989">
    <property type="term" value="P:lactose biosynthetic process"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="CDD" id="cd16898">
    <property type="entry name" value="LYZ_LA"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.530.10:FF:000014">
    <property type="entry name" value="Alpha-lactalbumin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.530.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001916">
    <property type="entry name" value="Glyco_hydro_22"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR019799">
    <property type="entry name" value="Glyco_hydro_22_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000545">
    <property type="entry name" value="Lactalbumin"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023346">
    <property type="entry name" value="Lysozyme-like_dom_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407:SF32">
    <property type="entry name" value="ALPHA-LACTALBUMIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR11407">
    <property type="entry name" value="LYSOZYME C"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00062">
    <property type="entry name" value="Lys"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00136">
    <property type="entry name" value="LACTALBUMIN"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00135">
    <property type="entry name" value="LYZLACT"/>
  </dbReference>
  <dbReference type="SMART" id="SM00263">
    <property type="entry name" value="LYZ1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF53955">
    <property type="entry name" value="Lysozyme-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00128">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51348">
    <property type="entry name" value="GLYCOSYL_HYDROL_F22_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0106">Calcium</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0325">Glycoprotein</keyword>
  <keyword id="KW-0422">Lactose biosynthesis</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0494">Milk protein</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="7">
    <location>
      <begin position="1"/>
      <end position="18"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000018440" description="Alpha-lactalbumin">
    <location>
      <begin position="19"/>
      <end position="142"/>
    </location>
  </feature>
  <feature type="domain" description="C-type lysozyme" evidence="3">
    <location>
      <begin position="20"/>
      <end position="142"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="98"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="101"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="103"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="106"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="107"/>
    </location>
    <ligand>
      <name>Ca(2+)</name>
      <dbReference type="ChEBI" id="CHEBI:29108"/>
    </ligand>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="2">
    <location>
      <position position="64"/>
    </location>
  </feature>
  <feature type="glycosylation site" description="N-linked (GlcNAc...) asparagine" evidence="2">
    <location>
      <position position="93"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="25"/>
      <end position="139"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="47"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="80"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="3">
    <location>
      <begin position="92"/>
      <end position="110"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele D." evidence="4 5">
    <original>I</original>
    <variation>S</variation>
    <location>
      <position position="12"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele B and allele D." evidence="4 5">
    <original>K</original>
    <variation>R</variation>
    <location>
      <position position="32"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele B." evidence="4">
    <original>K</original>
    <variation>A</variation>
    <location>
      <position position="35"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele C." evidence="4">
    <original>G</original>
    <variation>C</variation>
    <location>
      <position position="39"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele E." evidence="6">
    <original>P</original>
    <variation>S</variation>
    <location>
      <position position="43"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele C." evidence="4">
    <original>E</original>
    <variation>D</variation>
    <location>
      <position position="68"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele E." evidence="4">
    <original>N</original>
    <variation>K</variation>
    <location>
      <position position="85"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele C." evidence="4">
    <original>N</original>
    <variation>M</variation>
    <location>
      <position position="85"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele C." evidence="4">
    <original>N</original>
    <variation>K</variation>
    <location>
      <position position="93"/>
    </location>
  </feature>
  <feature type="sequence variant" description="In allele C, allele D and allele E." evidence="4">
    <original>C</original>
    <variation>G</variation>
    <location>
      <position position="96"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 5; AA sequence." evidence="8" ref="5">
    <original>A</original>
    <variation>M</variation>
    <location>
      <position position="19"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 5; AA sequence." evidence="8" ref="5">
    <original>D</original>
    <variation>K</variation>
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 1; AAF06794 and 5; AA sequence." evidence="8" ref="1 5">
    <original>A</original>
    <variation>T</variation>
    <location>
      <position position="49"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 5; AA sequence." evidence="8" ref="5">
    <original>I</original>
    <variation>T</variation>
    <location>
      <position position="60"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 5; AA sequence." evidence="8" ref="5">
    <original>D</original>
    <variation>T</variation>
    <location>
      <position position="65"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 5; AA sequence." evidence="8" ref="5">
    <original>Y</original>
    <variation>V</variation>
    <location>
      <position position="69"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 5; AA sequence." evidence="8" ref="5">
    <original>M</original>
    <variation>V</variation>
    <location>
      <position position="109"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P00711"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000255" key="3">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00680"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="16147872"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source ref="2"/>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source ref="4"/>
  </evidence>
  <evidence type="ECO:0000269" key="7">
    <source ref="5"/>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <sequence length="142" mass="16275" checksum="6EF3254A09779E2D" modified="2011-01-11" version="2" precursor="true">MMSFVSLLLVGILFHATQAEQLTKCEVFRELKDLKDYGGVSLPEWVCTAFHTSGYDTQAIVQNNDSTEYGLFQINNKIWCKDDQNPHSSNICNISCDKFLDDDLTDDIMCVKKILDKVGINYWLAHKALCSEKLDQWLCEKL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>