<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2015-03-04" modified="2024-03-27" version="43" xmlns="http://uniprot.org/uniprot">
  <accession>E0SDG7</accession>
  <name>CDII_DICD3</name>
  <protein>
    <recommendedName>
      <fullName evidence="2">Immunity protein CdiI</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name type="primary">cdiI</name>
    <name type="ordered locus">Dda3937_02097</name>
  </gene>
  <organism>
    <name type="scientific">Dickeya dadantii (strain 3937)</name>
    <name type="common">Erwinia chrysanthemi (strain 3937)</name>
    <dbReference type="NCBI Taxonomy" id="198628"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Pectobacteriaceae</taxon>
      <taxon>Dickeya</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2011" name="J. Bacteriol." volume="193" first="2076" last="2077">
      <title>Genome sequence of the plant-pathogenic bacterium Dickeya dadantii 3937.</title>
      <authorList>
        <person name="Glasner J.D."/>
        <person name="Yang C.H."/>
        <person name="Reverchon S."/>
        <person name="Hugouvieux-Cotte-Pattat N."/>
        <person name="Condemine G."/>
        <person name="Bohin J.P."/>
        <person name="Van Gijsegem F."/>
        <person name="Yang S."/>
        <person name="Franza T."/>
        <person name="Expert D."/>
        <person name="Plunkett G. III"/>
        <person name="San Francisco M.J."/>
        <person name="Charkowski A.O."/>
        <person name="Py B."/>
        <person name="Bell K."/>
        <person name="Rauscher L."/>
        <person name="Rodriguez-Palenzuela P."/>
        <person name="Toussaint A."/>
        <person name="Holeva M.C."/>
        <person name="He S.Y."/>
        <person name="Douet V."/>
        <person name="Boccara M."/>
        <person name="Blanco C."/>
        <person name="Toth I."/>
        <person name="Anderson B.D."/>
        <person name="Biehl B.S."/>
        <person name="Mau B."/>
        <person name="Flynn S.M."/>
        <person name="Barras F."/>
        <person name="Lindeberg M."/>
        <person name="Birch P.R."/>
        <person name="Tsuyumu S."/>
        <person name="Shi X."/>
        <person name="Hibbing M."/>
        <person name="Yap M.N."/>
        <person name="Carpentier M."/>
        <person name="Dassa E."/>
        <person name="Umehara M."/>
        <person name="Kim J.F."/>
        <person name="Rusch M."/>
        <person name="Soni P."/>
        <person name="Mayhew G.F."/>
        <person name="Fouts D.E."/>
        <person name="Gill S.R."/>
        <person name="Blattner F.R."/>
        <person name="Keen N.T."/>
        <person name="Perna N.T."/>
      </authorList>
      <dbReference type="PubMed" id="21217001"/>
      <dbReference type="DOI" id="10.1128/jb.01513-10"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>3937</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2010" name="Nature" volume="468" first="439" last="442">
      <title>A widespread family of polymorphic contact-dependent toxin delivery systems in bacteria.</title>
      <authorList>
        <person name="Aoki S.K."/>
        <person name="Diner E.J."/>
        <person name="de Roodenbeke C.T."/>
        <person name="Burgess B.R."/>
        <person name="Poole S.J."/>
        <person name="Braaten B.A."/>
        <person name="Jones A.M."/>
        <person name="Webb J.S."/>
        <person name="Hayes C.S."/>
        <person name="Cotter P.A."/>
        <person name="Low D.A."/>
      </authorList>
      <dbReference type="PubMed" id="21085179"/>
      <dbReference type="DOI" id="10.1038/nature09490"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>STRAIN SPECIFICITY</scope>
    <scope>INTERACTION WITH CDIA-CT</scope>
    <scope>SUBUNIT</scope>
    <source>
      <strain>3937</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Immunity protein component of a toxin-immunity protein module, which functions as a cellular contact-dependent growth inhibition (CDI) system. CDI modules allow bacteria to communicate with and inhibit the growth of closely related neighboring bacteria in a contact-dependent fashion. Protects cells against the DNase activity of CdiA, its cognate toxin protein, but not against non-cognate CdiA from E.coli strain 536 / UPEC.</text>
  </comment>
  <comment type="subunit">
    <text evidence="1">Interacts with cognate CdiA-CT but not non-cognate CdiA-CT from E.coli strain 536 / UPEC.</text>
  </comment>
  <comment type="miscellaneous">
    <text evidence="2">There are 2 cdiBAI loci in this strain, this is locus 2.</text>
  </comment>
  <dbReference type="EMBL" id="CP002038">
    <property type="protein sequence ID" value="ADM98659.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_013318105.1">
    <property type="nucleotide sequence ID" value="NC_014500.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="E0SDG7"/>
  <dbReference type="STRING" id="198628.Dda3937_02097"/>
  <dbReference type="GeneID" id="9733894"/>
  <dbReference type="KEGG" id="ddd:Dda3937_02097"/>
  <dbReference type="eggNOG" id="ENOG5032WZ3">
    <property type="taxonomic scope" value="Bacteria"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_144800_0_0_6"/>
  <dbReference type="OrthoDB" id="6630808at2"/>
  <dbReference type="Proteomes" id="UP000006859">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1185">Reference proteome</keyword>
  <feature type="chain" id="PRO_0000432077" description="Immunity protein CdiI">
    <location>
      <begin position="1"/>
      <end position="143"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="21085179"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="21085179"/>
    </source>
  </evidence>
  <sequence length="143" mass="16195" checksum="65C4CA52D78D28FD" modified="2010-11-02" version="1">MLAWNNLVEMLGCSKASNEFIYLPQKLNELPVFEEGVLGDRSYYSFFNSGVLFLLEDDLVNQISLYIQADEGFSAYTGELPLPVNSRESEIIQVLGTPSGSGGGKMDMLLGYVNRWIKYKTESHTLHIQFDQNDQLCRVTLMQ</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>