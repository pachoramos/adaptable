<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-12-12" modified="2024-11-27" version="98" xmlns="http://uniprot.org/uniprot">
  <accession>Q1CEG1</accession>
  <accession>C4GY05</accession>
  <name>NSRR_YERPN</name>
  <protein>
    <recommendedName>
      <fullName evidence="1">HTH-type transcriptional repressor NsrR</fullName>
    </recommendedName>
  </protein>
  <gene>
    <name evidence="1" type="primary">nsrR</name>
    <name type="ordered locus">YPN_3292</name>
    <name type="ORF">YP516_3741</name>
  </gene>
  <organism>
    <name type="scientific">Yersinia pestis bv. Antiqua (strain Nepal516)</name>
    <dbReference type="NCBI Taxonomy" id="377628"/>
    <lineage>
      <taxon>Bacteria</taxon>
      <taxon>Pseudomonadota</taxon>
      <taxon>Gammaproteobacteria</taxon>
      <taxon>Enterobacterales</taxon>
      <taxon>Yersiniaceae</taxon>
      <taxon>Yersinia</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="J. Bacteriol." volume="188" first="4453" last="4463">
      <title>Complete genome sequence of Yersinia pestis strains Antiqua and Nepal516: evidence of gene reduction in an emerging pathogen.</title>
      <authorList>
        <person name="Chain P.S.G."/>
        <person name="Hu P."/>
        <person name="Malfatti S.A."/>
        <person name="Radnedge L."/>
        <person name="Larimer F."/>
        <person name="Vergez L.M."/>
        <person name="Worsham P."/>
        <person name="Chu M.C."/>
        <person name="Andersen G.L."/>
      </authorList>
      <dbReference type="PubMed" id="16740952"/>
      <dbReference type="DOI" id="10.1128/jb.00124-06"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Nepal516</strain>
    </source>
  </reference>
  <reference key="2">
    <citation type="submission" date="2009-04" db="EMBL/GenBank/DDBJ databases">
      <title>Yersinia pestis Nepal516A whole genome shotgun sequencing project.</title>
      <authorList>
        <person name="Plunkett G. III"/>
        <person name="Anderson B.D."/>
        <person name="Baumler D.J."/>
        <person name="Burland V."/>
        <person name="Cabot E.L."/>
        <person name="Glasner J.D."/>
        <person name="Mau B."/>
        <person name="Neeno-Eckwall E."/>
        <person name="Perna N.T."/>
        <person name="Munk A.C."/>
        <person name="Tapia R."/>
        <person name="Green L.D."/>
        <person name="Rogers Y.C."/>
        <person name="Detter J.C."/>
        <person name="Bruce D.C."/>
        <person name="Brettin T.S."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>Nepal516</strain>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Nitric oxide-sensitive repressor of genes involved in protecting the cell against nitrosative stress. May require iron for activity.</text>
  </comment>
  <comment type="cofactor">
    <cofactor evidence="1">
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </cofactor>
    <text evidence="1">Binds 1 [2Fe-2S] cluster per subunit.</text>
  </comment>
  <comment type="sequence caution" evidence="2">
    <conflict type="erroneous initiation">
      <sequence resource="EMBL-CDS" id="ABG19619" version="1"/>
    </conflict>
  </comment>
  <dbReference type="EMBL" id="CP000305">
    <property type="protein sequence ID" value="ABG19619.1"/>
    <property type="status" value="ALT_INIT"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="ACNQ01000017">
    <property type="protein sequence ID" value="EEO75805.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="WP_002217229.1">
    <property type="nucleotide sequence ID" value="NZ_ACNQ01000017.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q1CEG1"/>
  <dbReference type="SMR" id="Q1CEG1"/>
  <dbReference type="GeneID" id="66843151"/>
  <dbReference type="KEGG" id="ypn:YPN_3292"/>
  <dbReference type="HOGENOM" id="CLU_107144_2_1_6"/>
  <dbReference type="Proteomes" id="UP000008936">
    <property type="component" value="Chromosome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005829">
    <property type="term" value="C:cytosol"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="TreeGrafter"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051537">
    <property type="term" value="F:2 iron, 2 sulfur cluster binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003700">
    <property type="term" value="F:DNA-binding transcription factor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003690">
    <property type="term" value="F:double-stranded DNA binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005506">
    <property type="term" value="F:iron ion binding"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-UniRule"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045892">
    <property type="term" value="P:negative regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="FunFam" id="1.10.10.10:FF:000105">
    <property type="entry name" value="HTH-type transcriptional repressor NsrR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="1.10.10.10">
    <property type="entry name" value="Winged helix-like DNA-binding domain superfamily/Winged helix DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="HAMAP" id="MF_01177">
    <property type="entry name" value="HTH_type_NsrR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR030489">
    <property type="entry name" value="TR_Rrf2-type_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000944">
    <property type="entry name" value="Tscrpt_reg_Rrf2"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR023761">
    <property type="entry name" value="Tscrpt_rep_HTH_NsrR"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036388">
    <property type="entry name" value="WH-like_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036390">
    <property type="entry name" value="WH_DNA-bd_sf"/>
  </dbReference>
  <dbReference type="NCBIfam" id="TIGR00738">
    <property type="entry name" value="rrf2_super"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33221:SF4">
    <property type="entry name" value="HTH-TYPE TRANSCRIPTIONAL REPRESSOR NSRR"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR33221">
    <property type="entry name" value="WINGED HELIX-TURN-HELIX TRANSCRIPTIONAL REGULATOR, RRF2 FAMILY"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF02082">
    <property type="entry name" value="Rrf2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF46785">
    <property type="entry name" value="Winged helix' DNA-binding domain"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS01332">
    <property type="entry name" value="HTH_RRF2_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51197">
    <property type="entry name" value="HTH_RRF2_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0001">2Fe-2S</keyword>
  <keyword id="KW-0238">DNA-binding</keyword>
  <keyword id="KW-0408">Iron</keyword>
  <keyword id="KW-0411">Iron-sulfur</keyword>
  <keyword id="KW-0479">Metal-binding</keyword>
  <keyword id="KW-0678">Repressor</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_0000268956" description="HTH-type transcriptional repressor NsrR">
    <location>
      <begin position="1"/>
      <end position="141"/>
    </location>
  </feature>
  <feature type="domain" description="HTH rrf2-type" evidence="1">
    <location>
      <begin position="2"/>
      <end position="129"/>
    </location>
  </feature>
  <feature type="DNA-binding region" description="H-T-H motif" evidence="1">
    <location>
      <begin position="28"/>
      <end position="51"/>
    </location>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="91"/>
    </location>
    <ligand>
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="96"/>
    </location>
    <ligand>
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </ligand>
  </feature>
  <feature type="binding site" evidence="1">
    <location>
      <position position="102"/>
    </location>
    <ligand>
      <name>[2Fe-2S] cluster</name>
      <dbReference type="ChEBI" id="CHEBI:190135"/>
    </ligand>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="HAMAP-Rule" id="MF_01177"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="141" mass="15645" checksum="F8F86890F25EE43C" modified="2006-12-12" version="2">MQLTSFTDYGLRALIYMASLPDGQMTSISQVTEVYGVSRNHMVKIINQLSRVGLVTAVRGKNGGIRLGKPADQILIGDVVRQMEPLTLVNCSSDFCHITPACRLKQVLNQAVQSFLKELDNYTLADMVKDNSPLYKLLLVE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>