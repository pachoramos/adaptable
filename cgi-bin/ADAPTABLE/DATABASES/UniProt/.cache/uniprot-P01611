<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2024-11-27" version="139" xmlns="http://uniprot.org/uniprot">
  <accession>P01611</accession>
  <accession>A0A0B4J2E7</accession>
  <name>KVD12_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName evidence="4 9">Immunoglobulin kappa variable 1D-12</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="11">Ig kappa chain V-I region Wes</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name evidence="4 9" type="primary">IGKV1D-12</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Nature" volume="434" first="724" last="731">
      <title>Generation and annotation of the DNA sequences of human chromosomes 2 and 4.</title>
      <authorList>
        <person name="Hillier L.W."/>
        <person name="Graves T.A."/>
        <person name="Fulton R.S."/>
        <person name="Fulton L.A."/>
        <person name="Pepin K.H."/>
        <person name="Minx P."/>
        <person name="Wagner-McPherson C."/>
        <person name="Layman D."/>
        <person name="Wylie K."/>
        <person name="Sekhon M."/>
        <person name="Becker M.C."/>
        <person name="Fewell G.A."/>
        <person name="Delehaunty K.D."/>
        <person name="Miner T.L."/>
        <person name="Nash W.E."/>
        <person name="Kremitzki C."/>
        <person name="Oddy L."/>
        <person name="Du H."/>
        <person name="Sun H."/>
        <person name="Bradshaw-Cordum H."/>
        <person name="Ali J."/>
        <person name="Carter J."/>
        <person name="Cordes M."/>
        <person name="Harris A."/>
        <person name="Isak A."/>
        <person name="van Brunt A."/>
        <person name="Nguyen C."/>
        <person name="Du F."/>
        <person name="Courtney L."/>
        <person name="Kalicki J."/>
        <person name="Ozersky P."/>
        <person name="Abbott S."/>
        <person name="Armstrong J."/>
        <person name="Belter E.A."/>
        <person name="Caruso L."/>
        <person name="Cedroni M."/>
        <person name="Cotton M."/>
        <person name="Davidson T."/>
        <person name="Desai A."/>
        <person name="Elliott G."/>
        <person name="Erb T."/>
        <person name="Fronick C."/>
        <person name="Gaige T."/>
        <person name="Haakenson W."/>
        <person name="Haglund K."/>
        <person name="Holmes A."/>
        <person name="Harkins R."/>
        <person name="Kim K."/>
        <person name="Kruchowski S.S."/>
        <person name="Strong C.M."/>
        <person name="Grewal N."/>
        <person name="Goyea E."/>
        <person name="Hou S."/>
        <person name="Levy A."/>
        <person name="Martinka S."/>
        <person name="Mead K."/>
        <person name="McLellan M.D."/>
        <person name="Meyer R."/>
        <person name="Randall-Maher J."/>
        <person name="Tomlinson C."/>
        <person name="Dauphin-Kohlberg S."/>
        <person name="Kozlowicz-Reilly A."/>
        <person name="Shah N."/>
        <person name="Swearengen-Shahid S."/>
        <person name="Snider J."/>
        <person name="Strong J.T."/>
        <person name="Thompson J."/>
        <person name="Yoakum M."/>
        <person name="Leonard S."/>
        <person name="Pearman C."/>
        <person name="Trani L."/>
        <person name="Radionenko M."/>
        <person name="Waligorski J.E."/>
        <person name="Wang C."/>
        <person name="Rock S.M."/>
        <person name="Tin-Wollam A.-M."/>
        <person name="Maupin R."/>
        <person name="Latreille P."/>
        <person name="Wendl M.C."/>
        <person name="Yang S.-P."/>
        <person name="Pohl C."/>
        <person name="Wallis J.W."/>
        <person name="Spieth J."/>
        <person name="Bieri T.A."/>
        <person name="Berkowicz N."/>
        <person name="Nelson J.O."/>
        <person name="Osborne J."/>
        <person name="Ding L."/>
        <person name="Meyer R."/>
        <person name="Sabo A."/>
        <person name="Shotland Y."/>
        <person name="Sinha P."/>
        <person name="Wohldmann P.E."/>
        <person name="Cook L.L."/>
        <person name="Hickenbotham M.T."/>
        <person name="Eldred J."/>
        <person name="Williams D."/>
        <person name="Jones T.A."/>
        <person name="She X."/>
        <person name="Ciccarelli F.D."/>
        <person name="Izaurralde E."/>
        <person name="Taylor J."/>
        <person name="Schmutz J."/>
        <person name="Myers R.M."/>
        <person name="Cox D.R."/>
        <person name="Huang X."/>
        <person name="McPherson J.D."/>
        <person name="Mardis E.R."/>
        <person name="Clifton S.W."/>
        <person name="Warren W.C."/>
        <person name="Chinwalla A.T."/>
        <person name="Eddy S.R."/>
        <person name="Marra M.A."/>
        <person name="Ovcharenko I."/>
        <person name="Furey T.S."/>
        <person name="Miller W."/>
        <person name="Eichler E.E."/>
        <person name="Bork P."/>
        <person name="Suyama M."/>
        <person name="Torrents D."/>
        <person name="Waterston R.H."/>
        <person name="Wilson R.K."/>
      </authorList>
      <dbReference type="PubMed" id="15815621"/>
      <dbReference type="DOI" id="10.1038/nature03466"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA] (IMGT ALLELE IGKV1D-12*02)</scope>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1980" name="Hoppe-Seyler's Z. Physiol. Chem." volume="361" first="1591" last="1598">
      <title>Preparative separation of the tryptic hydrolysate of a protein by high-pressure liquid chromatography. The primary structure of a monoclonal L-chain of k-type, subgroup I (Bence-Jones protein Wes).</title>
      <authorList>
        <person name="Kratzin H."/>
        <person name="Yang C."/>
        <person name="Krusche J.U."/>
        <person name="Hilschmann N."/>
      </authorList>
      <dbReference type="PubMed" id="6778806"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 23-117</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2001" name="Exp. Clin. Immunogenet." volume="18" first="161" last="174">
      <title>Nomenclature of the human immunoglobulin kappa (IGK) genes.</title>
      <authorList>
        <person name="Lefranc M.P."/>
      </authorList>
      <dbReference type="PubMed" id="11549845"/>
      <dbReference type="DOI" id="10.1159/000049195"/>
    </citation>
    <scope>NOMEMCLATURE</scope>
  </reference>
  <reference key="4">
    <citation type="book" date="2001" name="The Immunoglobulin FactsBook." first="1" last="458" publisher="Academic Press" city="London.">
      <title>The Immunoglobulin FactsBook.</title>
      <editorList>
        <person name="Lefranc M.P."/>
        <person name="Lefranc G."/>
      </editorList>
      <authorList>
        <person name="Lefranc M.P."/>
        <person name="Lefranc G."/>
      </authorList>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2007" name="Annu. Rev. Genet." volume="41" first="107" last="120">
      <title>Immunoglobulin somatic hypermutation.</title>
      <authorList>
        <person name="Teng G."/>
        <person name="Papavasiliou F.N."/>
      </authorList>
      <dbReference type="PubMed" id="17576170"/>
      <dbReference type="DOI" id="10.1146/annurev.genet.41.110306.130340"/>
    </citation>
    <scope>REVIEW ON SOMATIC HYPERMUTATION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2010" name="J. Allergy Clin. Immunol." volume="125" first="S41" last="S52">
      <title>Structure and function of immunoglobulins.</title>
      <authorList>
        <person name="Schroeder H.W. Jr."/>
        <person name="Cavacini L."/>
      </authorList>
      <dbReference type="PubMed" id="20176268"/>
      <dbReference type="DOI" id="10.1016/j.jaci.2009.09.046"/>
    </citation>
    <scope>REVIEW ON IMMUNOGLOBULINS</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2012" name="Nat. Rev. Immunol." volume="12" first="24" last="34">
      <title>Molecular programming of B cell memory.</title>
      <authorList>
        <person name="McHeyzer-Williams M."/>
        <person name="Okitsu S."/>
        <person name="Wang N."/>
        <person name="McHeyzer-Williams L."/>
      </authorList>
      <dbReference type="PubMed" id="22158414"/>
      <dbReference type="DOI" id="10.1038/nri3128"/>
    </citation>
    <scope>REVIEW ON FUNCTION</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2014" name="Front. Immunol." volume="5" first="22" last="22">
      <title>Immunoglobulin and T Cell Receptor Genes: IMGT((R)) and the Birth and Rise of Immunoinformatics.</title>
      <authorList>
        <person name="Lefranc M.P."/>
      </authorList>
      <dbReference type="PubMed" id="24600447"/>
      <dbReference type="DOI" id="10.3389/fimmu.2014.00022"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="5 6 7 8">V region of the variable domain of immunoglobulin light chains that participates in the antigen recognition (PubMed:24600447). Immunoglobulins, also known as antibodies, are membrane-bound or secreted glycoproteins produced by B lymphocytes. In the recognition phase of humoral immunity, the membrane-bound immunoglobulins serve as receptors which, upon binding of a specific antigen, trigger the clonal expansion and differentiation of B lymphocytes into immunoglobulins-secreting plasma cells. Secreted immunoglobulins mediate the effector phase of humoral immunity, which results in the elimination of bound antigens (PubMed:20176268, PubMed:22158414). The antigen binding site is formed by the variable domain of one heavy chain, together with that of its associated light chain. Thus, each immunoglobulin has two antigen binding sites with remarkable affinity for a particular antigen. The variable domains are assembled by a process called V-(D)-J rearrangement and can then be subjected to somatic hypermutations which, after exposure to antigen and selection, allow affinity maturation for a particular antigen (PubMed:17576170, PubMed:20176268).</text>
  </comment>
  <comment type="subunit">
    <text evidence="6">Immunoglobulins are composed of two identical heavy chains and two identical light chains; disulfide-linked.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6 7">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="6 7">Cell membrane</location>
    </subcellularLocation>
  </comment>
  <comment type="polymorphism">
    <text>There are several alleles. The sequence shown is that of IMGT allele IGKV1D-12*02.</text>
  </comment>
  <comment type="caution">
    <text evidence="10">For an example of a full-length immunoglobulin kappa light chain see AC P0DOX7.</text>
  </comment>
  <dbReference type="EMBL" id="AC243981">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="A01877">
    <property type="entry name" value="K1HUWS"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01611"/>
  <dbReference type="EMDB" id="EMD-20947"/>
  <dbReference type="EMDB" id="EMD-21648"/>
  <dbReference type="EMDB" id="EMD-24402"/>
  <dbReference type="EMDB" id="EMD-24403"/>
  <dbReference type="EMDB" id="EMD-25794"/>
  <dbReference type="EMDB" id="EMD-25797"/>
  <dbReference type="EMDB" id="EMD-27765"/>
  <dbReference type="EMDB" id="EMD-33133"/>
  <dbReference type="EMDB" id="EMD-33150"/>
  <dbReference type="EMDB" id="EMD-34226"/>
  <dbReference type="EMDB" id="EMD-34410"/>
  <dbReference type="EMDB" id="EMD-34411"/>
  <dbReference type="EMDB" id="EMD-34654"/>
  <dbReference type="SMR" id="P01611"/>
  <dbReference type="IntAct" id="P01611">
    <property type="interactions" value="3"/>
  </dbReference>
  <dbReference type="IMGT_GENE-DB" id="IGKV1D-12"/>
  <dbReference type="BioMuta" id="IGKV1D-12"/>
  <dbReference type="DMDM" id="125775"/>
  <dbReference type="jPOST" id="P01611"/>
  <dbReference type="MassIVE" id="P01611"/>
  <dbReference type="PeptideAtlas" id="P01611"/>
  <dbReference type="Ensembl" id="ENST00000377032.5">
    <property type="protein sequence ID" value="ENSP00000482464.1"/>
    <property type="gene ID" value="ENSG00000278857.1"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:5746"/>
  <dbReference type="GeneCards" id="IGKV1D-12"/>
  <dbReference type="HGNC" id="HGNC:5746">
    <property type="gene designation" value="IGKV1D-12"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000278857">
    <property type="expression patterns" value="Tissue enhanced (intestine, lung, lymphoid tissue)"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P01611"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000278857"/>
  <dbReference type="GeneTree" id="ENSGT00940000153048"/>
  <dbReference type="InParanoid" id="P01611"/>
  <dbReference type="OMA" id="TSCDILM"/>
  <dbReference type="PathwayCommons" id="P01611"/>
  <dbReference type="Reactome" id="R-HSA-166663">
    <property type="pathway name" value="Initial triggering of complement"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-173623">
    <property type="pathway name" value="Classical antibody-mediated complement activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-198933">
    <property type="pathway name" value="Immunoregulatory interactions between a Lymphoid and a non-Lymphoid cell"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-202733">
    <property type="pathway name" value="Cell surface interactions at the vascular wall"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2029481">
    <property type="pathway name" value="FCGR activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2029482">
    <property type="pathway name" value="Regulation of actin dynamics for phagocytic cup formation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2029485">
    <property type="pathway name" value="Role of phospholipids in phagocytosis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2168880">
    <property type="pathway name" value="Scavenging of heme from plasma"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2454202">
    <property type="pathway name" value="Fc epsilon receptor (FCERI) signaling"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2730905">
    <property type="pathway name" value="Role of LAT2/NTAL/LAB on calcium mobilization"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2871796">
    <property type="pathway name" value="FCERI mediated MAPK activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2871809">
    <property type="pathway name" value="FCERI mediated Ca+2 mobilization"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-2871837">
    <property type="pathway name" value="FCERI mediated NF-kB activation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-5690714">
    <property type="pathway name" value="CD22 mediated BCR regulation"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9664323">
    <property type="pathway name" value="FCGR3A-mediated IL10 synthesis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9664422">
    <property type="pathway name" value="FCGR3A-mediated phagocytosis"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-9679191">
    <property type="pathway name" value="Potential therapeutics for SARS"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-977606">
    <property type="pathway name" value="Regulation of Complement cascade"/>
  </dbReference>
  <dbReference type="Reactome" id="R-HSA-983695">
    <property type="pathway name" value="Antigen activates B Cell Receptor (BCR) leading to generation of second messengers"/>
  </dbReference>
  <dbReference type="SignaLink" id="P01611"/>
  <dbReference type="Pharos" id="P01611">
    <property type="development level" value="Tdark"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P01611"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="RNAct" id="P01611">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000278857">
    <property type="expression patterns" value="Expressed in mucosa of transverse colon and 80 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0072562">
    <property type="term" value="C:blood microparticle"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0070062">
    <property type="term" value="C:extracellular exosome"/>
    <property type="evidence" value="ECO:0007005"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019814">
    <property type="term" value="C:immunoglobulin complex"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005886">
    <property type="term" value="C:plasma membrane"/>
    <property type="evidence" value="ECO:0000304"/>
    <property type="project" value="Reactome"/>
  </dbReference>
  <dbReference type="GO" id="GO:0003823">
    <property type="term" value="F:antigen binding"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0002250">
    <property type="term" value="P:adaptive immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006955">
    <property type="term" value="P:immune response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="CDD" id="cd04980">
    <property type="entry name" value="IgV_L_kappa"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="FunFam" id="2.60.40.10:FF:000212">
    <property type="entry name" value="Immunoglobulin kappa chain variable 12-38"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.60.40.10">
    <property type="entry name" value="Immunoglobulins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR007110">
    <property type="entry name" value="Ig-like_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036179">
    <property type="entry name" value="Ig-like_dom_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013783">
    <property type="entry name" value="Ig-like_fold"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003599">
    <property type="entry name" value="Ig_sub"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013106">
    <property type="entry name" value="Ig_V-set"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR050150">
    <property type="entry name" value="IgV_Light_Chain"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23267:SF480">
    <property type="entry name" value="IMMUNOGLOBULIN KAPPA VARIABLE 1-12-RELATED"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR23267">
    <property type="entry name" value="IMMUNOGLOBULIN LIGHT CHAIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF07686">
    <property type="entry name" value="V-set"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00409">
    <property type="entry name" value="IG"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM00406">
    <property type="entry name" value="IGv"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF48726">
    <property type="entry name" value="Immunoglobulin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS50835">
    <property type="entry name" value="IG_LIKE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-1064">Adaptive immunity</keyword>
  <keyword id="KW-1003">Cell membrane</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-1280">Immunoglobulin</keyword>
  <keyword id="KW-0393">Immunoglobulin domain</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="22"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000059753" description="Immunoglobulin kappa variable 1D-12" evidence="3">
    <location>
      <begin position="23"/>
      <end position="117"/>
    </location>
  </feature>
  <feature type="domain" description="Ig-like" evidence="2">
    <location>
      <begin position="24"/>
      <end position="117" status="greater than"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-1" evidence="1">
    <location>
      <begin position="23"/>
      <end position="45"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-1" evidence="1">
    <location>
      <begin position="46"/>
      <end position="56"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-2" evidence="1">
    <location>
      <begin position="57"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-2" evidence="1">
    <location>
      <begin position="72"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="region of interest" description="Framework-3" evidence="1">
    <location>
      <begin position="79"/>
      <end position="110"/>
    </location>
  </feature>
  <feature type="region of interest" description="Complementarity-determining-3" evidence="1">
    <location>
      <begin position="111"/>
      <end position="117" status="greater than"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="2">
    <location>
      <begin position="45"/>
      <end position="110"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>G</original>
    <variation>D</variation>
    <location>
      <position position="50"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>S</original>
    <variation>H</variation>
    <location>
      <position position="53"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>P</original>
    <variation>S</variation>
    <location>
      <position position="62"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>A</original>
    <variation>S</variation>
    <location>
      <position position="72"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>QS</original>
    <variation>EN</variation>
    <location>
      <begin position="77"/>
      <end position="78"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>D</original>
    <variation>E</variation>
    <location>
      <position position="92"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>Y</original>
    <variation>F</variation>
    <location>
      <position position="109"/>
    </location>
  </feature>
  <feature type="sequence conflict" description="In Ref. 2; AA sequence." evidence="10" ref="2">
    <original>NSF</original>
    <variation>HSV</variation>
    <location>
      <begin position="114"/>
      <end position="116"/>
    </location>
  </feature>
  <feature type="non-terminal residue">
    <location>
      <position position="117"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P01602"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU00114"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="6778806"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="11549845"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="17576170"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="20176268"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="22158414"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="8">
    <source>
      <dbReference type="PubMed" id="24600447"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="9">
    <source ref="4"/>
  </evidence>
  <evidence type="ECO:0000305" key="10"/>
  <evidence type="ECO:0000305" key="11">
    <source>
      <dbReference type="PubMed" id="6778806"/>
    </source>
  </evidence>
  <sequence length="117" mass="12620" checksum="229C6F0487AF667E" modified="2016-11-02" version="2" precursor="true">MDMMVPAQLLGLLLLWFPGSRCDIQMTQSPSSVSASVGDRVTITCRASQGISSWLAWYQQKPGKAPKLLIYAASSLQSGVPSRFSGSGSGTDFTLTISSLQPEDFATYYCQQANSFP</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>