<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2011-02-08" modified="2020-02-26" version="16" xmlns="http://uniprot.org/uniprot">
  <accession>P86814</accession>
  <name>BRK4_PHYSG</name>
  <protein>
    <recommendedName>
      <fullName evidence="3">[Ala1,Thr6]-bradykinyl-Ser,Lys</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Physalaemus signifer</name>
    <name type="common">Girard's dwarf frog</name>
    <dbReference type="NCBI Taxonomy" id="364768"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Leptodactylidae</taxon>
      <taxon>Leiuperinae</taxon>
      <taxon>Physalaemus</taxon>
    </lineage>
  </organism>
  <reference evidence="4" key="1">
    <citation type="submission" date="2010-09" db="UniProtKB">
      <title>Bradykinin-related peptides in skin secretion of Physalaemus signifer (Girard, 1853) (Anura, Leiuperidae).</title>
      <authorList>
        <person name="Rates B."/>
        <person name="Ireno I.C."/>
        <person name="Canelas M.A."/>
        <person name="de Lima M.E."/>
        <person name="Pimenta A.M.C."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <source>
      <tissue evidence="2">Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Produces in vitro relaxation of rat arterial smooth muscle and constriction of intestinal smooth muscle (By similarity). May target bradykinin receptors (BDKRB).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2">Expressed by the skin glands.</text>
  </comment>
  <comment type="similarity">
    <text evidence="4">Belongs to the bradykinin-related peptide family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042311">
    <property type="term" value="P:vasodilation"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR009608">
    <property type="entry name" value="Bradykinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF06753">
    <property type="entry name" value="Bradykinin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1213">G-protein coupled receptor impairing toxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0838">Vasoactive</keyword>
  <keyword id="KW-0840">Vasodilator</keyword>
  <feature type="peptide" id="PRO_0000404689" description="[Ala1,Thr6]-bradykinyl-Ser,Lys" evidence="2">
    <location>
      <begin position="1"/>
      <end position="11"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000303" key="3">
    <source ref="1"/>
  </evidence>
  <evidence type="ECO:0000305" key="4"/>
  <sequence length="11" mass="1204" checksum="3D70E40CD771A9C8" modified="2011-02-08" version="1">APPGFTPFRSK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>