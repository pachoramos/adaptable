<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2008-09-23" modified="2024-01-24" version="71" xmlns="http://uniprot.org/uniprot">
  <accession>Q295C6</accession>
  <name>DSK_DROPS</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Drosulfakinins</fullName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="4">Drosulfakinin-0</fullName>
        <shortName evidence="4">DSK-0</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="4">Drosulfakinin-1</fullName>
      </recommendedName>
      <alternativeName>
        <fullName evidence="4">Drosulfakinin I</fullName>
        <shortName evidence="4">DSK-I</shortName>
      </alternativeName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="4">Drosulfakinin-2</fullName>
      </recommendedName>
      <alternativeName>
        <fullName evidence="4">Drosulfakinin II</fullName>
        <shortName evidence="4">DSK-II</shortName>
      </alternativeName>
    </component>
  </protein>
  <gene>
    <name evidence="2" type="primary">Dsk</name>
    <name type="ORF">GA14787</name>
  </gene>
  <organism>
    <name type="scientific">Drosophila pseudoobscura pseudoobscura</name>
    <name type="common">Fruit fly</name>
    <dbReference type="NCBI Taxonomy" id="46245"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Diptera</taxon>
      <taxon>Brachycera</taxon>
      <taxon>Muscomorpha</taxon>
      <taxon>Ephydroidea</taxon>
      <taxon>Drosophilidae</taxon>
      <taxon>Drosophila</taxon>
      <taxon>Sophophora</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2005" name="Genome Res." volume="15" first="1" last="18">
      <title>Comparative genome sequencing of Drosophila pseudoobscura: chromosomal, gene, and cis-element evolution.</title>
      <authorList>
        <person name="Richards S."/>
        <person name="Liu Y."/>
        <person name="Bettencourt B.R."/>
        <person name="Hradecky P."/>
        <person name="Letovsky S."/>
        <person name="Nielsen R."/>
        <person name="Thornton K."/>
        <person name="Hubisz M.J."/>
        <person name="Chen R."/>
        <person name="Meisel R.P."/>
        <person name="Couronne O."/>
        <person name="Hua S."/>
        <person name="Smith M.A."/>
        <person name="Zhang P."/>
        <person name="Liu J."/>
        <person name="Bussemaker H.J."/>
        <person name="van Batenburg M.F."/>
        <person name="Howells S.L."/>
        <person name="Scherer S.E."/>
        <person name="Sodergren E."/>
        <person name="Matthews B.B."/>
        <person name="Crosby M.A."/>
        <person name="Schroeder A.J."/>
        <person name="Ortiz-Barrientos D."/>
        <person name="Rives C.M."/>
        <person name="Metzker M.L."/>
        <person name="Muzny D.M."/>
        <person name="Scott G."/>
        <person name="Steffen D."/>
        <person name="Wheeler D.A."/>
        <person name="Worley K.C."/>
        <person name="Havlak P."/>
        <person name="Durbin K.J."/>
        <person name="Egan A."/>
        <person name="Gill R."/>
        <person name="Hume J."/>
        <person name="Morgan M.B."/>
        <person name="Miner G."/>
        <person name="Hamilton C."/>
        <person name="Huang Y."/>
        <person name="Waldron L."/>
        <person name="Verduzco D."/>
        <person name="Clerc-Blankenburg K.P."/>
        <person name="Dubchak I."/>
        <person name="Noor M.A.F."/>
        <person name="Anderson W."/>
        <person name="White K.P."/>
        <person name="Clark A.G."/>
        <person name="Schaeffer S.W."/>
        <person name="Gelbart W.M."/>
        <person name="Weinstock G.M."/>
        <person name="Gibbs R.A."/>
      </authorList>
      <dbReference type="PubMed" id="15632085"/>
      <dbReference type="DOI" id="10.1101/gr.3059305"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
    <source>
      <strain>MV2-25 / Tucson 14011-0121.94</strain>
    </source>
  </reference>
  <reference evidence="5" key="2">
    <citation type="journal article" date="2007" name="J. Insect Physiol." volume="53" first="1125" last="1133">
      <title>The drosulfakinin 0 (DSK 0) peptide encoded in the conserved Dsk gene affects adult Drosophila melanogaster crop contractions.</title>
      <authorList>
        <person name="Palmer G.C."/>
        <person name="Tran T."/>
        <person name="Duttlinger A."/>
        <person name="Nichols R."/>
      </authorList>
      <dbReference type="PubMed" id="17632121"/>
      <dbReference type="DOI" id="10.1016/j.jinsphys.2007.06.001"/>
    </citation>
    <scope>IDENTIFICATION</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Drosulfakinin-0 (DSK 0) plays diverse biological roles including regulating gut muscle contraction in adults but not in larvae.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="3">Belongs to the gastrin/cholecystokinin family.</text>
  </comment>
  <dbReference type="EMBL" id="CM000070">
    <property type="protein sequence ID" value="EAL28786.2"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_001359636.2">
    <property type="nucleotide sequence ID" value="XM_001359599.3"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q295C6"/>
  <dbReference type="SMR" id="Q295C6"/>
  <dbReference type="STRING" id="46245.Q295C6"/>
  <dbReference type="EnsemblMetazoa" id="FBtr0282883">
    <property type="protein sequence ID" value="FBpp0281321"/>
    <property type="gene ID" value="FBgn0074814"/>
  </dbReference>
  <dbReference type="GeneID" id="4802776"/>
  <dbReference type="KEGG" id="dpo:4802776"/>
  <dbReference type="eggNOG" id="ENOG502SESC">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="HOGENOM" id="CLU_1847224_0_0_1"/>
  <dbReference type="InParanoid" id="Q295C6"/>
  <dbReference type="OMA" id="FGDRRNQ"/>
  <dbReference type="Proteomes" id="UP000001819">
    <property type="component" value="Chromosome 2"/>
  </dbReference>
  <dbReference type="Bgee" id="FBgn0074814">
    <property type="expression patterns" value="Expressed in insect adult head"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005179">
    <property type="term" value="F:hormone activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006939">
    <property type="term" value="P:smooth muscle contraction"/>
    <property type="evidence" value="ECO:0000250"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013152">
    <property type="entry name" value="Gastrin/cholecystokinin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR013259">
    <property type="entry name" value="Sulfakinin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF08257">
    <property type="entry name" value="Sulfakinin"/>
    <property type="match status" value="2"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00259">
    <property type="entry name" value="GASTRIN"/>
    <property type="match status" value="2"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0372">Hormone</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0765">Sulfation</keyword>
  <feature type="signal peptide" evidence="3">
    <location>
      <begin position="1"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000351170" evidence="3">
    <location>
      <begin position="36"/>
      <end position="71"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000351171" description="Drosulfakinin-0" evidence="3">
    <location>
      <begin position="74"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000351172" evidence="3">
    <location>
      <begin position="81"/>
      <end position="109"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000351173" description="Drosulfakinin-1" evidence="3">
    <location>
      <begin position="112"/>
      <end position="120"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000351174" description="Drosulfakinin-2" evidence="1">
    <location>
      <begin position="124"/>
      <end position="137"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="3">
    <location>
      <position position="80"/>
    </location>
  </feature>
  <feature type="modified residue" description="Sulfotyrosine" evidence="3">
    <location>
      <position position="115"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="3">
    <location>
      <position position="120"/>
    </location>
  </feature>
  <feature type="modified residue" description="Sulfotyrosine" evidence="1">
    <location>
      <position position="132"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="1">
    <location>
      <position position="137"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000250" key="2">
    <source>
      <dbReference type="UniProtKB" id="P09040"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="3"/>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="17632121"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <sequence length="139" mass="15743" checksum="CB7E9A4EBE268A51" modified="2009-01-20" version="2" precursor="true">MGHRGMGCAHFATMAMPLWALTFYLLVVLPVPSQTASVEVGKEERRLQDLDPKMGSEAGNTDGLSLARFGSRRHQRSTGFGHRVPIISRPVIPIELDLLMDNEDDRTMSKRFDDYGHMRFGKRGGDDQFDDYGHMRFGR</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>