<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2015-03-04" modified="2022-10-12" version="11" xmlns="http://uniprot.org/uniprot">
  <accession>R4JNJ5</accession>
  <name>NDB4J_PANIM</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Pantinin-1</fullName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="5">Non-disulfide-bridged peptide 4.20</fullName>
      <shortName evidence="5">NDBP-4.20</shortName>
    </alternativeName>
    <alternativeName>
      <fullName evidence="4">Non-disulfide-bridged peptide 5.21</fullName>
      <shortName evidence="4">NDBP-5.21</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Pandinus imperator</name>
    <name type="common">Emperor scorpion</name>
    <dbReference type="NCBI Taxonomy" id="55084"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Iurida</taxon>
      <taxon>Scorpionoidea</taxon>
      <taxon>Scorpionidae</taxon>
      <taxon>Pandininae</taxon>
      <taxon>Pandinus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2013" name="Peptides" volume="45" first="28" last="34">
      <title>Three new antimicrobial peptides from the scorpion Pandinus imperator.</title>
      <authorList>
        <person name="Zeng X.C."/>
        <person name="Zhou L."/>
        <person name="Shi W."/>
        <person name="Luo X."/>
        <person name="Zhang L."/>
        <person name="Nie Y."/>
        <person name="Wang J."/>
        <person name="Wu S."/>
        <person name="Cao B."/>
        <person name="Cao H."/>
      </authorList>
      <dbReference type="PubMed" id="23624072"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2013.03.026"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>SYNTHESIS OF 24-37</scope>
    <scope>FUNCTION</scope>
    <scope>NOMENCLATURE</scope>
    <source>
      <tissue>Venom gland</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2014" name="Peptides" volume="51" first="35" last="45">
      <title>Scorpion venom peptides with no disulfide bridges: a review.</title>
      <authorList>
        <person name="Almaaytah A."/>
        <person name="Albalas Q."/>
      </authorList>
      <dbReference type="PubMed" id="24184590"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2013.10.021"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="3">Amphipathic peptide that possesses relatively strong activities against Gram-positive bacteria and a fungus, but has very weak antimicrobial activities against Gram-negative bacteria. Also exhibits very low hemolytic activities against human erythrocytes (64 uM induce 21% of hemolysis). Minimal inhibitory concentration (MIC) are the following: 8 uM against S.aureus, 32 uM against B.magaterium, 32 uM against M.luteus, 28 uM against vancomycin-resistant Enterococci, 14 uM against methicillin-resistant S.aureus, 62 uM against E.coli, &gt;87 uM against P.putida, &gt;87 uM against K.oxytoca, 76 uM against E.cloacae, 72 uM against S.enterica and 16 uM against the fungus C.tropicalis.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="1">Target cell membrane</location>
    </subcellularLocation>
    <text evidence="1">Forms an alpha-helical membrane channel in the prey.</text>
  </comment>
  <comment type="tissue specificity">
    <text evidence="6">Expressed by the venom gland.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the non-disulfide-bridged peptide (NDBP) superfamily. Short antimicrobial peptide (group 4) family.</text>
  </comment>
  <dbReference type="EMBL" id="KC538864">
    <property type="protein sequence ID" value="AGK88380.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="R4JNJ5"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0016020">
    <property type="term" value="C:membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0044218">
    <property type="term" value="C:other organism cell membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="inferred from homology"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-1052">Target cell membrane</keyword>
  <keyword id="KW-1053">Target membrane</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000432375" description="Pantinin-1">
    <location>
      <begin position="24"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000432376" evidence="1">
    <location>
      <begin position="41"/>
      <end position="69"/>
    </location>
  </feature>
  <feature type="modified residue" description="Valine amide" evidence="1">
    <location>
      <position position="37"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="23624072"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="23624072"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="24184590"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="69" mass="7891" checksum="F937E27F86055CE2" modified="2013-07-24" version="1" precursor="true">MKTQFVILMITVILMQMLVQTEGGILGKLWEGFKSIVGKRGLNDRDQLDDLFDSDLSDADIKLLKEMFK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>