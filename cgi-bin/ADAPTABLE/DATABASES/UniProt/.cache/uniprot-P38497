<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1994-10-01" modified="2019-12-11" version="31" xmlns="http://uniprot.org/uniprot">
  <accession>P38497</accession>
  <name>LMA2_LOCMI</name>
  <protein>
    <recommendedName>
      <fullName>Lom-AG-myotropin-2</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Accessory gland myotropin II</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Lom-AG-myotropin II</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Locusta migratoria</name>
    <name type="common">Migratory locust</name>
    <dbReference type="NCBI Taxonomy" id="7004"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Orthoptera</taxon>
      <taxon>Caelifera</taxon>
      <taxon>Acrididea</taxon>
      <taxon>Acridomorpha</taxon>
      <taxon>Acridoidea</taxon>
      <taxon>Acrididae</taxon>
      <taxon>Oedipodinae</taxon>
      <taxon>Locusta</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1991" name="Insect Biochem." volume="21" first="243" last="248">
      <title>Isolation, identification and synthesis of Lom-AG-myotropin II, a novel peptide in the male accessory reproductive glands of Locusta migratoria.</title>
      <authorList>
        <person name="Paemen L."/>
        <person name="Schoofs L."/>
        <person name="Proost P."/>
        <person name="Decock B."/>
        <person name="de Loof A."/>
      </authorList>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>SYNTHESIS</scope>
    <source>
      <tissue>Male accessory gland</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Myotropic peptide.</text>
  </comment>
  <comment type="tissue specificity">
    <text>Male accessory glands.</text>
  </comment>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <feature type="peptide" id="PRO_0000044156" description="Lom-AG-myotropin-2">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <sequence length="15" mass="1592" checksum="8C80FFF4B41941CF" modified="1994-10-01" version="1">AHRFAAEDFGALDTA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>