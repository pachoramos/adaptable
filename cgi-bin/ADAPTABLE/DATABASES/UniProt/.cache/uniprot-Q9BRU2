<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-05-30" modified="2024-11-27" version="156" xmlns="http://uniprot.org/uniprot">
  <accession>Q9BRU2</accession>
  <accession>B3KSV2</accession>
  <accession>Q96AT4</accession>
  <name>TCAL7_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Transcription elongation factor A protein-like 7</fullName>
      <shortName>TCEA-like protein 7</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Transcription elongation factor S-II protein-like 7</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">TCEAL7</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2004" name="Nat. Genet." volume="36" first="40" last="45">
      <title>Complete sequencing and characterization of 21,243 full-length human cDNAs.</title>
      <authorList>
        <person name="Ota T."/>
        <person name="Suzuki Y."/>
        <person name="Nishikawa T."/>
        <person name="Otsuki T."/>
        <person name="Sugiyama T."/>
        <person name="Irie R."/>
        <person name="Wakamatsu A."/>
        <person name="Hayashi K."/>
        <person name="Sato H."/>
        <person name="Nagai K."/>
        <person name="Kimura K."/>
        <person name="Makita H."/>
        <person name="Sekine M."/>
        <person name="Obayashi M."/>
        <person name="Nishi T."/>
        <person name="Shibahara T."/>
        <person name="Tanaka T."/>
        <person name="Ishii S."/>
        <person name="Yamamoto J."/>
        <person name="Saito K."/>
        <person name="Kawai Y."/>
        <person name="Isono Y."/>
        <person name="Nakamura Y."/>
        <person name="Nagahari K."/>
        <person name="Murakami K."/>
        <person name="Yasuda T."/>
        <person name="Iwayanagi T."/>
        <person name="Wagatsuma M."/>
        <person name="Shiratori A."/>
        <person name="Sudo H."/>
        <person name="Hosoiri T."/>
        <person name="Kaku Y."/>
        <person name="Kodaira H."/>
        <person name="Kondo H."/>
        <person name="Sugawara M."/>
        <person name="Takahashi M."/>
        <person name="Kanda K."/>
        <person name="Yokoi T."/>
        <person name="Furuya T."/>
        <person name="Kikkawa E."/>
        <person name="Omura Y."/>
        <person name="Abe K."/>
        <person name="Kamihara K."/>
        <person name="Katsuta N."/>
        <person name="Sato K."/>
        <person name="Tanikawa M."/>
        <person name="Yamazaki M."/>
        <person name="Ninomiya K."/>
        <person name="Ishibashi T."/>
        <person name="Yamashita H."/>
        <person name="Murakawa K."/>
        <person name="Fujimori K."/>
        <person name="Tanai H."/>
        <person name="Kimata M."/>
        <person name="Watanabe M."/>
        <person name="Hiraoka S."/>
        <person name="Chiba Y."/>
        <person name="Ishida S."/>
        <person name="Ono Y."/>
        <person name="Takiguchi S."/>
        <person name="Watanabe S."/>
        <person name="Yosida M."/>
        <person name="Hotuta T."/>
        <person name="Kusano J."/>
        <person name="Kanehori K."/>
        <person name="Takahashi-Fujii A."/>
        <person name="Hara H."/>
        <person name="Tanase T.-O."/>
        <person name="Nomura Y."/>
        <person name="Togiya S."/>
        <person name="Komai F."/>
        <person name="Hara R."/>
        <person name="Takeuchi K."/>
        <person name="Arita M."/>
        <person name="Imose N."/>
        <person name="Musashino K."/>
        <person name="Yuuki H."/>
        <person name="Oshima A."/>
        <person name="Sasaki N."/>
        <person name="Aotsuka S."/>
        <person name="Yoshikawa Y."/>
        <person name="Matsunawa H."/>
        <person name="Ichihara T."/>
        <person name="Shiohata N."/>
        <person name="Sano S."/>
        <person name="Moriya S."/>
        <person name="Momiyama H."/>
        <person name="Satoh N."/>
        <person name="Takami S."/>
        <person name="Terashima Y."/>
        <person name="Suzuki O."/>
        <person name="Nakagawa S."/>
        <person name="Senoh A."/>
        <person name="Mizoguchi H."/>
        <person name="Goto Y."/>
        <person name="Shimizu F."/>
        <person name="Wakebe H."/>
        <person name="Hishigaki H."/>
        <person name="Watanabe T."/>
        <person name="Sugiyama A."/>
        <person name="Takemoto M."/>
        <person name="Kawakami B."/>
        <person name="Yamazaki M."/>
        <person name="Watanabe K."/>
        <person name="Kumagai A."/>
        <person name="Itakura S."/>
        <person name="Fukuzumi Y."/>
        <person name="Fujimori Y."/>
        <person name="Komiyama M."/>
        <person name="Tashiro H."/>
        <person name="Tanigami A."/>
        <person name="Fujiwara T."/>
        <person name="Ono T."/>
        <person name="Yamada K."/>
        <person name="Fujii Y."/>
        <person name="Ozaki K."/>
        <person name="Hirao M."/>
        <person name="Ohmori Y."/>
        <person name="Kawabata A."/>
        <person name="Hikiji T."/>
        <person name="Kobatake N."/>
        <person name="Inagaki H."/>
        <person name="Ikema Y."/>
        <person name="Okamoto S."/>
        <person name="Okitani R."/>
        <person name="Kawakami T."/>
        <person name="Noguchi S."/>
        <person name="Itoh T."/>
        <person name="Shigeta K."/>
        <person name="Senba T."/>
        <person name="Matsumura K."/>
        <person name="Nakajima Y."/>
        <person name="Mizuno T."/>
        <person name="Morinaga M."/>
        <person name="Sasaki M."/>
        <person name="Togashi T."/>
        <person name="Oyama M."/>
        <person name="Hata H."/>
        <person name="Watanabe M."/>
        <person name="Komatsu T."/>
        <person name="Mizushima-Sugano J."/>
        <person name="Satoh T."/>
        <person name="Shirai Y."/>
        <person name="Takahashi Y."/>
        <person name="Nakagawa K."/>
        <person name="Okumura K."/>
        <person name="Nagase T."/>
        <person name="Nomura N."/>
        <person name="Kikuchi H."/>
        <person name="Masuho Y."/>
        <person name="Yamashita R."/>
        <person name="Nakai K."/>
        <person name="Yada T."/>
        <person name="Nakamura Y."/>
        <person name="Ohara O."/>
        <person name="Isogai T."/>
        <person name="Sugano S."/>
      </authorList>
      <dbReference type="PubMed" id="14702039"/>
      <dbReference type="DOI" id="10.1038/ng1285"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Cerebellum</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2005" name="Nature" volume="434" first="325" last="337">
      <title>The DNA sequence of the human X chromosome.</title>
      <authorList>
        <person name="Ross M.T."/>
        <person name="Grafham D.V."/>
        <person name="Coffey A.J."/>
        <person name="Scherer S."/>
        <person name="McLay K."/>
        <person name="Muzny D."/>
        <person name="Platzer M."/>
        <person name="Howell G.R."/>
        <person name="Burrows C."/>
        <person name="Bird C.P."/>
        <person name="Frankish A."/>
        <person name="Lovell F.L."/>
        <person name="Howe K.L."/>
        <person name="Ashurst J.L."/>
        <person name="Fulton R.S."/>
        <person name="Sudbrak R."/>
        <person name="Wen G."/>
        <person name="Jones M.C."/>
        <person name="Hurles M.E."/>
        <person name="Andrews T.D."/>
        <person name="Scott C.E."/>
        <person name="Searle S."/>
        <person name="Ramser J."/>
        <person name="Whittaker A."/>
        <person name="Deadman R."/>
        <person name="Carter N.P."/>
        <person name="Hunt S.E."/>
        <person name="Chen R."/>
        <person name="Cree A."/>
        <person name="Gunaratne P."/>
        <person name="Havlak P."/>
        <person name="Hodgson A."/>
        <person name="Metzker M.L."/>
        <person name="Richards S."/>
        <person name="Scott G."/>
        <person name="Steffen D."/>
        <person name="Sodergren E."/>
        <person name="Wheeler D.A."/>
        <person name="Worley K.C."/>
        <person name="Ainscough R."/>
        <person name="Ambrose K.D."/>
        <person name="Ansari-Lari M.A."/>
        <person name="Aradhya S."/>
        <person name="Ashwell R.I."/>
        <person name="Babbage A.K."/>
        <person name="Bagguley C.L."/>
        <person name="Ballabio A."/>
        <person name="Banerjee R."/>
        <person name="Barker G.E."/>
        <person name="Barlow K.F."/>
        <person name="Barrett I.P."/>
        <person name="Bates K.N."/>
        <person name="Beare D.M."/>
        <person name="Beasley H."/>
        <person name="Beasley O."/>
        <person name="Beck A."/>
        <person name="Bethel G."/>
        <person name="Blechschmidt K."/>
        <person name="Brady N."/>
        <person name="Bray-Allen S."/>
        <person name="Bridgeman A.M."/>
        <person name="Brown A.J."/>
        <person name="Brown M.J."/>
        <person name="Bonnin D."/>
        <person name="Bruford E.A."/>
        <person name="Buhay C."/>
        <person name="Burch P."/>
        <person name="Burford D."/>
        <person name="Burgess J."/>
        <person name="Burrill W."/>
        <person name="Burton J."/>
        <person name="Bye J.M."/>
        <person name="Carder C."/>
        <person name="Carrel L."/>
        <person name="Chako J."/>
        <person name="Chapman J.C."/>
        <person name="Chavez D."/>
        <person name="Chen E."/>
        <person name="Chen G."/>
        <person name="Chen Y."/>
        <person name="Chen Z."/>
        <person name="Chinault C."/>
        <person name="Ciccodicola A."/>
        <person name="Clark S.Y."/>
        <person name="Clarke G."/>
        <person name="Clee C.M."/>
        <person name="Clegg S."/>
        <person name="Clerc-Blankenburg K."/>
        <person name="Clifford K."/>
        <person name="Cobley V."/>
        <person name="Cole C.G."/>
        <person name="Conquer J.S."/>
        <person name="Corby N."/>
        <person name="Connor R.E."/>
        <person name="David R."/>
        <person name="Davies J."/>
        <person name="Davis C."/>
        <person name="Davis J."/>
        <person name="Delgado O."/>
        <person name="Deshazo D."/>
        <person name="Dhami P."/>
        <person name="Ding Y."/>
        <person name="Dinh H."/>
        <person name="Dodsworth S."/>
        <person name="Draper H."/>
        <person name="Dugan-Rocha S."/>
        <person name="Dunham A."/>
        <person name="Dunn M."/>
        <person name="Durbin K.J."/>
        <person name="Dutta I."/>
        <person name="Eades T."/>
        <person name="Ellwood M."/>
        <person name="Emery-Cohen A."/>
        <person name="Errington H."/>
        <person name="Evans K.L."/>
        <person name="Faulkner L."/>
        <person name="Francis F."/>
        <person name="Frankland J."/>
        <person name="Fraser A.E."/>
        <person name="Galgoczy P."/>
        <person name="Gilbert J."/>
        <person name="Gill R."/>
        <person name="Gloeckner G."/>
        <person name="Gregory S.G."/>
        <person name="Gribble S."/>
        <person name="Griffiths C."/>
        <person name="Grocock R."/>
        <person name="Gu Y."/>
        <person name="Gwilliam R."/>
        <person name="Hamilton C."/>
        <person name="Hart E.A."/>
        <person name="Hawes A."/>
        <person name="Heath P.D."/>
        <person name="Heitmann K."/>
        <person name="Hennig S."/>
        <person name="Hernandez J."/>
        <person name="Hinzmann B."/>
        <person name="Ho S."/>
        <person name="Hoffs M."/>
        <person name="Howden P.J."/>
        <person name="Huckle E.J."/>
        <person name="Hume J."/>
        <person name="Hunt P.J."/>
        <person name="Hunt A.R."/>
        <person name="Isherwood J."/>
        <person name="Jacob L."/>
        <person name="Johnson D."/>
        <person name="Jones S."/>
        <person name="de Jong P.J."/>
        <person name="Joseph S.S."/>
        <person name="Keenan S."/>
        <person name="Kelly S."/>
        <person name="Kershaw J.K."/>
        <person name="Khan Z."/>
        <person name="Kioschis P."/>
        <person name="Klages S."/>
        <person name="Knights A.J."/>
        <person name="Kosiura A."/>
        <person name="Kovar-Smith C."/>
        <person name="Laird G.K."/>
        <person name="Langford C."/>
        <person name="Lawlor S."/>
        <person name="Leversha M."/>
        <person name="Lewis L."/>
        <person name="Liu W."/>
        <person name="Lloyd C."/>
        <person name="Lloyd D.M."/>
        <person name="Loulseged H."/>
        <person name="Loveland J.E."/>
        <person name="Lovell J.D."/>
        <person name="Lozado R."/>
        <person name="Lu J."/>
        <person name="Lyne R."/>
        <person name="Ma J."/>
        <person name="Maheshwari M."/>
        <person name="Matthews L.H."/>
        <person name="McDowall J."/>
        <person name="McLaren S."/>
        <person name="McMurray A."/>
        <person name="Meidl P."/>
        <person name="Meitinger T."/>
        <person name="Milne S."/>
        <person name="Miner G."/>
        <person name="Mistry S.L."/>
        <person name="Morgan M."/>
        <person name="Morris S."/>
        <person name="Mueller I."/>
        <person name="Mullikin J.C."/>
        <person name="Nguyen N."/>
        <person name="Nordsiek G."/>
        <person name="Nyakatura G."/>
        <person name="O'dell C.N."/>
        <person name="Okwuonu G."/>
        <person name="Palmer S."/>
        <person name="Pandian R."/>
        <person name="Parker D."/>
        <person name="Parrish J."/>
        <person name="Pasternak S."/>
        <person name="Patel D."/>
        <person name="Pearce A.V."/>
        <person name="Pearson D.M."/>
        <person name="Pelan S.E."/>
        <person name="Perez L."/>
        <person name="Porter K.M."/>
        <person name="Ramsey Y."/>
        <person name="Reichwald K."/>
        <person name="Rhodes S."/>
        <person name="Ridler K.A."/>
        <person name="Schlessinger D."/>
        <person name="Schueler M.G."/>
        <person name="Sehra H.K."/>
        <person name="Shaw-Smith C."/>
        <person name="Shen H."/>
        <person name="Sheridan E.M."/>
        <person name="Shownkeen R."/>
        <person name="Skuce C.D."/>
        <person name="Smith M.L."/>
        <person name="Sotheran E.C."/>
        <person name="Steingruber H.E."/>
        <person name="Steward C.A."/>
        <person name="Storey R."/>
        <person name="Swann R.M."/>
        <person name="Swarbreck D."/>
        <person name="Tabor P.E."/>
        <person name="Taudien S."/>
        <person name="Taylor T."/>
        <person name="Teague B."/>
        <person name="Thomas K."/>
        <person name="Thorpe A."/>
        <person name="Timms K."/>
        <person name="Tracey A."/>
        <person name="Trevanion S."/>
        <person name="Tromans A.C."/>
        <person name="d'Urso M."/>
        <person name="Verduzco D."/>
        <person name="Villasana D."/>
        <person name="Waldron L."/>
        <person name="Wall M."/>
        <person name="Wang Q."/>
        <person name="Warren J."/>
        <person name="Warry G.L."/>
        <person name="Wei X."/>
        <person name="West A."/>
        <person name="Whitehead S.L."/>
        <person name="Whiteley M.N."/>
        <person name="Wilkinson J.E."/>
        <person name="Willey D.L."/>
        <person name="Williams G."/>
        <person name="Williams L."/>
        <person name="Williamson A."/>
        <person name="Williamson H."/>
        <person name="Wilming L."/>
        <person name="Woodmansey R.L."/>
        <person name="Wray P.W."/>
        <person name="Yen J."/>
        <person name="Zhang J."/>
        <person name="Zhou J."/>
        <person name="Zoghbi H."/>
        <person name="Zorilla S."/>
        <person name="Buck D."/>
        <person name="Reinhardt R."/>
        <person name="Poustka A."/>
        <person name="Rosenthal A."/>
        <person name="Lehrach H."/>
        <person name="Meindl A."/>
        <person name="Minx P.J."/>
        <person name="Hillier L.W."/>
        <person name="Willard H.F."/>
        <person name="Wilson R.K."/>
        <person name="Waterston R.H."/>
        <person name="Rice C.M."/>
        <person name="Vaudin M."/>
        <person name="Coulson A."/>
        <person name="Nelson D.L."/>
        <person name="Weinstock G."/>
        <person name="Sulston J.E."/>
        <person name="Durbin R.M."/>
        <person name="Hubbard T."/>
        <person name="Gibbs R.A."/>
        <person name="Beck S."/>
        <person name="Rogers J."/>
        <person name="Bentley D.R."/>
      </authorList>
      <dbReference type="PubMed" id="15772651"/>
      <dbReference type="DOI" id="10.1038/nature03440"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="3">
    <citation type="submission" date="2005-09" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Istrail S."/>
        <person name="Sutton G.G."/>
        <person name="Florea L."/>
        <person name="Halpern A.L."/>
        <person name="Mobarry C.M."/>
        <person name="Lippert R."/>
        <person name="Walenz B."/>
        <person name="Shatkay H."/>
        <person name="Dew I."/>
        <person name="Miller J.R."/>
        <person name="Flanigan M.J."/>
        <person name="Edwards N.J."/>
        <person name="Bolanos R."/>
        <person name="Fasulo D."/>
        <person name="Halldorsson B.V."/>
        <person name="Hannenhalli S."/>
        <person name="Turner R."/>
        <person name="Yooseph S."/>
        <person name="Lu F."/>
        <person name="Nusskern D.R."/>
        <person name="Shue B.C."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F."/>
        <person name="Delcher A.L."/>
        <person name="Huson D.H."/>
        <person name="Kravitz S.A."/>
        <person name="Mouchard L."/>
        <person name="Reinert K."/>
        <person name="Remington K.A."/>
        <person name="Clark A.G."/>
        <person name="Waterman M.S."/>
        <person name="Eichler E.E."/>
        <person name="Adams M.D."/>
        <person name="Hunkapiller M.W."/>
        <person name="Myers E.W."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2008" name="Oncogene" volume="27" first="7223" last="7234">
      <title>A role for candidate tumor-suppressor gene TCEAL7 in the regulation of c-Myc activity, cyclin D1 levels and cellular transformation.</title>
      <authorList>
        <person name="Chien J."/>
        <person name="Narita K."/>
        <person name="Rattan R."/>
        <person name="Giri S."/>
        <person name="Shridhar R."/>
        <person name="Staub J."/>
        <person name="Beleford D."/>
        <person name="Lai J."/>
        <person name="Roberts L.R."/>
        <person name="Molina J."/>
        <person name="Kaufmann S.H."/>
        <person name="Prendergast G.C."/>
        <person name="Shridhar V."/>
      </authorList>
      <dbReference type="PubMed" id="18806825"/>
      <dbReference type="DOI" id="10.1038/onc.2008.360"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2010" name="Neoplasia" volume="12" first="405" last="414">
      <title>TCEAL7 inhibition of c-Myc activity in alternative lengthening of telomeres regulates hTERT expression.</title>
      <authorList>
        <person name="Lafferty-Whyte K."/>
        <person name="Bilsland A."/>
        <person name="Hoare S.F."/>
        <person name="Burns S."/>
        <person name="Zaffaroni N."/>
        <person name="Cairney C.J."/>
        <person name="Keith W.N."/>
      </authorList>
      <dbReference type="PubMed" id="20454512"/>
      <dbReference type="DOI" id="10.1593/neo.10180"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2010" name="Oncogene" volume="29" first="1362" last="1373">
      <title>TCEAL7, a putative tumor suppressor gene, negatively regulates NF-kappaB pathway.</title>
      <authorList>
        <person name="Rattan R."/>
        <person name="Narita K."/>
        <person name="Chien J."/>
        <person name="Maguire J.L."/>
        <person name="Shridhar R."/>
        <person name="Giri S."/>
        <person name="Shridhar V."/>
      </authorList>
      <dbReference type="PubMed" id="19966855"/>
      <dbReference type="DOI" id="10.1038/onc.2009.431"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <comment type="function">
    <text evidence="3 4 5">Plays a role in the negative regulation of NF-kappa-B signaling at the basal level by modulating transcriptional activity of NF-kappa-B on its target gene promoters. Associates with cyclin D1 promoter containing Myc E-box sequence and transcriptionally represses cyclin D1 expression. Regulates telomerase reverse transcriptase expression and telomerase activity in both ALT (alternative lengthening of telomeres)and telomerase-positive cell lines.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Nucleus</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Highly expressed in normal and fetal brain tissues, and weakly expressed in uterus and ovary. Down-regulated in epithelial ovarian, cervical, prostate, breast, brain and lung cancer cell lines and in brain and ovarian tumors.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the TFS-II family. TFA subfamily.</text>
  </comment>
  <dbReference type="EMBL" id="AK094400">
    <property type="protein sequence ID" value="BAG52864.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="Z92846">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH471190">
    <property type="protein sequence ID" value="EAW54714.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC005988">
    <property type="protein sequence ID" value="AAH05988.2"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC016786">
    <property type="protein sequence ID" value="AAH16786.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS14506.1"/>
  <dbReference type="RefSeq" id="NP_001335187.1">
    <property type="nucleotide sequence ID" value="NM_001348258.1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_689491.1">
    <property type="nucleotide sequence ID" value="NM_152278.4"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="Q9BRU2"/>
  <dbReference type="SMR" id="Q9BRU2"/>
  <dbReference type="BioGRID" id="121209">
    <property type="interactions" value="108"/>
  </dbReference>
  <dbReference type="IntAct" id="Q9BRU2">
    <property type="interactions" value="22"/>
  </dbReference>
  <dbReference type="MINT" id="Q9BRU2"/>
  <dbReference type="STRING" id="9606.ENSP00000329794"/>
  <dbReference type="BioMuta" id="TCEAL7"/>
  <dbReference type="DMDM" id="74761221"/>
  <dbReference type="MassIVE" id="Q9BRU2"/>
  <dbReference type="PaxDb" id="9606-ENSP00000329794"/>
  <dbReference type="PeptideAtlas" id="Q9BRU2"/>
  <dbReference type="ProteomicsDB" id="78837"/>
  <dbReference type="Pumba" id="Q9BRU2"/>
  <dbReference type="Antibodypedia" id="44282">
    <property type="antibodies" value="73 antibodies from 20 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="56849"/>
  <dbReference type="Ensembl" id="ENST00000332431.5">
    <property type="protein sequence ID" value="ENSP00000329794.4"/>
    <property type="gene ID" value="ENSG00000182916.8"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000372666.1">
    <property type="protein sequence ID" value="ENSP00000361751.1"/>
    <property type="gene ID" value="ENSG00000182916.8"/>
  </dbReference>
  <dbReference type="GeneID" id="56849"/>
  <dbReference type="KEGG" id="hsa:56849"/>
  <dbReference type="MANE-Select" id="ENST00000332431.5">
    <property type="protein sequence ID" value="ENSP00000329794.4"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_152278.5"/>
    <property type="RefSeq protein sequence ID" value="NP_689491.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc004ekc.3">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:28336"/>
  <dbReference type="CTD" id="56849"/>
  <dbReference type="DisGeNET" id="56849"/>
  <dbReference type="GeneCards" id="TCEAL7"/>
  <dbReference type="HGNC" id="HGNC:28336">
    <property type="gene designation" value="TCEAL7"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000182916">
    <property type="expression patterns" value="Tissue enhanced (brain)"/>
  </dbReference>
  <dbReference type="MIM" id="300771">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_Q9BRU2"/>
  <dbReference type="OpenTargets" id="ENSG00000182916"/>
  <dbReference type="PharmGKB" id="PA134936264"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000182916"/>
  <dbReference type="eggNOG" id="ENOG502TCAG">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00950000183164"/>
  <dbReference type="HOGENOM" id="CLU_181913_0_0_1"/>
  <dbReference type="InParanoid" id="Q9BRU2"/>
  <dbReference type="OMA" id="DYRHFKG"/>
  <dbReference type="OrthoDB" id="4827922at2759"/>
  <dbReference type="PhylomeDB" id="Q9BRU2"/>
  <dbReference type="PathwayCommons" id="Q9BRU2"/>
  <dbReference type="SignaLink" id="Q9BRU2"/>
  <dbReference type="BioGRID-ORCS" id="56849">
    <property type="hits" value="112 hits in 758 CRISPR screens"/>
  </dbReference>
  <dbReference type="GenomeRNAi" id="56849"/>
  <dbReference type="Pharos" id="Q9BRU2">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:Q9BRU2"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome X"/>
  </dbReference>
  <dbReference type="RNAct" id="Q9BRU2">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000182916">
    <property type="expression patterns" value="Expressed in Brodmann (1909) area 46 and 155 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005654">
    <property type="term" value="C:nucleoplasm"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="HPA"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005634">
    <property type="term" value="C:nucleus"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045892">
    <property type="term" value="P:negative regulation of DNA-templated transcription"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0032088">
    <property type="term" value="P:negative regulation of NF-kappaB transcription factor activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR021156">
    <property type="entry name" value="TF_A-like/BEX"/>
  </dbReference>
  <dbReference type="Pfam" id="PF04538">
    <property type="entry name" value="BEX"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0175">Coiled coil</keyword>
  <keyword id="KW-0539">Nucleus</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0678">Repressor</keyword>
  <keyword id="KW-0804">Transcription</keyword>
  <keyword id="KW-0805">Transcription regulation</keyword>
  <feature type="chain" id="PRO_0000239214" description="Transcription elongation factor A protein-like 7">
    <location>
      <begin position="1"/>
      <end position="100"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="1"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="coiled-coil region" evidence="1">
    <location>
      <begin position="60"/>
      <end position="88"/>
    </location>
  </feature>
  <feature type="compositionally biased region" description="Basic and acidic residues" evidence="2">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="18806825"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="19966855"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="20454512"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="100" mass="12324" checksum="84AF23BD169606D0" modified="2004-03-01" version="2">MQKPCKENEGKPKCSVPKREEKRPYGEFERQQTEGNFRQRLLQSLEEFKEDIDYRHFKDEEMTREGDEMERCLEEIRGLRKKFRALHSNHRHSRDRPYPI</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>