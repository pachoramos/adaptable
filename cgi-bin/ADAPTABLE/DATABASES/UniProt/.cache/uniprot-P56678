<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1999-07-15" modified="2024-10-02" version="119" xmlns="http://uniprot.org/uniprot">
  <accession>P56678</accession>
  <name>SCL3_LEIHE</name>
  <protein>
    <recommendedName>
      <fullName>Alpha-like toxin Lqh3</fullName>
    </recommendedName>
    <alternativeName>
      <fullName>Lqh III</fullName>
      <shortName>LqhIII</shortName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Leiurus hebraeus</name>
    <name type="common">Hebrew deathstalker scorpion</name>
    <name type="synonym">Leiurus quinquestriatus hebraeus</name>
    <dbReference type="NCBI Taxonomy" id="2899558"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Chelicerata</taxon>
      <taxon>Arachnida</taxon>
      <taxon>Scorpiones</taxon>
      <taxon>Buthida</taxon>
      <taxon>Buthoidea</taxon>
      <taxon>Buthidae</taxon>
      <taxon>Leiurus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1998" name="Toxicon" volume="36" first="1141" last="1154">
      <title>New toxins acting on sodium channels from the scorpion Leiurus quinquestriatus hebraeus suggest a clue to mammalian vs insect selectivity.</title>
      <authorList>
        <person name="Sautiere P."/>
        <person name="Cestele S."/>
        <person name="Kopeyan C."/>
        <person name="Martinage A."/>
        <person name="Drobecq H."/>
        <person name="Doljansky Y."/>
        <person name="Gordon D."/>
      </authorList>
      <dbReference type="PubMed" id="9690781"/>
      <dbReference type="DOI" id="10.1016/s0041-0101(98)00080-4"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>AMIDATION AT SER-67</scope>
    <scope>TOXIC DOSE</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Venom</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2000" name="Pflugers Arch." volume="439" first="423" last="432">
      <title>Modulation of cloned skeletal muscle sodium channels by the scorpion toxins Lqh II, Lqh III, and Lqh alphaIT.</title>
      <authorList>
        <person name="Chen H."/>
        <person name="Gordon D."/>
        <person name="Heinemann S.H."/>
      </authorList>
      <dbReference type="PubMed" id="10678738"/>
      <dbReference type="DOI" id="10.1007/s004249900181"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1999" name="J. Neurosci." volume="19" first="8730" last="8739">
      <title>A scorpion alpha-like toxin that is active on insects and mammals reveals an unexpected specificity and distribution of sodium channel subtypes in rat brain neurons.</title>
      <authorList>
        <person name="Gilles N."/>
        <person name="Blanchet C."/>
        <person name="Shichor I."/>
        <person name="Zaninetti M."/>
        <person name="Lotan I."/>
        <person name="Bertrand D."/>
        <person name="Gordon D."/>
      </authorList>
      <dbReference type="PubMed" id="10516292"/>
      <dbReference type="DOI" id="10.1523/jneurosci.19-20-08730.1999"/>
    </citation>
    <scope>FUNCTION IN RAT BRAIN NEURONS</scope>
  </reference>
  <reference key="4">
    <citation type="journal article" date="2001" name="J. Gen. Physiol." volume="117" first="505" last="518">
      <title>Interaction of scorpion alpha-toxins with cardiac sodium channels: binding properties and enhancement of slow inactivation.</title>
      <authorList>
        <person name="Chen H."/>
        <person name="Heinemann S.H."/>
      </authorList>
      <dbReference type="PubMed" id="11382802"/>
      <dbReference type="DOI" id="10.1085/jgp.117.6.505"/>
    </citation>
    <scope>FUNCTION ON HUMAN CARDIAC SODIUM CHANNELS</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2007" name="FEBS J." volume="274" first="1918" last="1931">
      <title>The unique pharmacology of the scorpion alpha-like toxin Lqh3 is associated with its flexible C-tail.</title>
      <authorList>
        <person name="Karbat I."/>
        <person name="Kahn R."/>
        <person name="Cohen L."/>
        <person name="Ilan N."/>
        <person name="Gilles N."/>
        <person name="Corzo G."/>
        <person name="Froy O."/>
        <person name="Gur M."/>
        <person name="Albrecht G."/>
        <person name="Heinemann S.H."/>
        <person name="Gordon D."/>
        <person name="Gurevitz M."/>
      </authorList>
      <dbReference type="PubMed" id="17355257"/>
      <dbReference type="DOI" id="10.1111/j.1742-4658.2007.05737.x"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1999" name="J. Mol. Biol." volume="285" first="1749" last="1763">
      <title>NMR structures and activity of a novel alpha-like toxin from the scorpion Leiurus quinquestriatus hebraeus.</title>
      <authorList>
        <person name="Krimm I."/>
        <person name="Gilles N."/>
        <person name="Sautiere P."/>
        <person name="Stankiewicz M."/>
        <person name="Pelhate M."/>
        <person name="Gordon D."/>
        <person name="Lancelin J.-M."/>
      </authorList>
      <dbReference type="PubMed" id="9917409"/>
      <dbReference type="DOI" id="10.1006/jmbi.1998.2418"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2000" name="J. Neurochem." volume="75" first="1735" last="1745">
      <title>Structural implications on the interaction of scorpion alpha-like toxins with the sodium channel receptor site inferred from toxin iodination and pH-dependent binding.</title>
      <authorList>
        <person name="Gilles N."/>
        <person name="Krimm I."/>
        <person name="Bouet F."/>
        <person name="Froy O."/>
        <person name="Gurevitz M."/>
        <person name="Lancelin J.-M."/>
        <person name="Gordon D."/>
      </authorList>
      <dbReference type="PubMed" id="10987857"/>
      <dbReference type="DOI" id="10.1046/j.1471-4159.2000.0751735.x"/>
    </citation>
    <scope>STRUCTURE BY NMR</scope>
    <scope>DISULFIDE BONDS</scope>
  </reference>
  <comment type="function">
    <text evidence="2 3 4 5 6">Alpha toxins bind voltage-independently at site-3 of sodium channels (Nav) and inhibit the inactivation of the activated channels, thereby blocking neuronal transmission. The dissociation is voltage-dependent. This alpha-like toxin is highly toxic to insects and competes with LqhaIT on binding to insect sodium channels. Differs from classical anti-mammalian alpha-toxins as it inhibits sodium channel inactivation in cell bodies of hippocampus brain neurons, on which the anti-mammalian Lqh2 is inactive, and is unable to affect Nav1.2 in the rat brain, on which Lqh2 is highly active. Moreover, its pharmacological properties are unique in that its binding affinity for insect channels drops &gt;30-fold at pH 8.5 versus pH 6.5, and its rate of association with receptor site-3 on both insect and mammalian sodium channels is 4-15-fold slower compared with LqhaIT and Lqh2.</text>
  </comment>
  <comment type="subunit">
    <text>Monomer.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="8">Expressed by the venom gland.</text>
  </comment>
  <comment type="domain">
    <text evidence="7">Has the structural arrangement of an alpha-helix connected to antiparallel beta-sheets by disulfide bonds (CS-alpha/beta).</text>
  </comment>
  <comment type="toxic dose">
    <text evidence="6">LD(50) is 50 mg/kg by intracerebroventricular injection into mice.</text>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the long (4 C-C) scorpion toxin superfamily. Sodium channel inhibitor family. Alpha subfamily.</text>
  </comment>
  <dbReference type="PDB" id="1BMR">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-67"/>
  </dbReference>
  <dbReference type="PDB" id="1FH3">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=1-67"/>
  </dbReference>
  <dbReference type="PDB" id="7K18">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.30 A"/>
    <property type="chains" value="B=1-67"/>
  </dbReference>
  <dbReference type="PDB" id="7XSU">
    <property type="method" value="EM"/>
    <property type="resolution" value="3.40 A"/>
    <property type="chains" value="B=1-67"/>
  </dbReference>
  <dbReference type="PDBsum" id="1BMR"/>
  <dbReference type="PDBsum" id="1FH3"/>
  <dbReference type="PDBsum" id="7K18"/>
  <dbReference type="PDBsum" id="7XSU"/>
  <dbReference type="AlphaFoldDB" id="P56678"/>
  <dbReference type="BMRB" id="P56678"/>
  <dbReference type="EMDB" id="EMD-22621"/>
  <dbReference type="EMDB" id="EMD-33435"/>
  <dbReference type="SMR" id="P56678"/>
  <dbReference type="TCDB" id="8.B.1.1.8">
    <property type="family name" value="the long (4c-c) scorpion toxin (l-st) superfamily"/>
  </dbReference>
  <dbReference type="EvolutionaryTrace" id="P56678"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019871">
    <property type="term" value="F:sodium channel inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="CDD" id="cd00107">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.30.30.10">
    <property type="entry name" value="Knottin, scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR044062">
    <property type="entry name" value="LCN-type_CS_alpha_beta_dom"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR003614">
    <property type="entry name" value="Scorpion_toxin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR036574">
    <property type="entry name" value="Scorpion_toxin-like_sf"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018218">
    <property type="entry name" value="Scorpion_toxinL"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR002061">
    <property type="entry name" value="Scorpion_toxinL/defensin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00537">
    <property type="entry name" value="Toxin_3"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PRINTS" id="PR00285">
    <property type="entry name" value="SCORPNTOXIN"/>
  </dbReference>
  <dbReference type="SMART" id="SM00505">
    <property type="entry name" value="Knot1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57095">
    <property type="entry name" value="Scorpion toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS51863">
    <property type="entry name" value="LCN_CSAB"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0872">Ion channel impairing toxin</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <keyword id="KW-0738">Voltage-gated sodium channel impairing toxin</keyword>
  <feature type="chain" id="PRO_0000066784" description="Alpha-like toxin Lqh3">
    <location>
      <begin position="1"/>
      <end position="67"/>
    </location>
  </feature>
  <feature type="domain" description="LCN-type CS-alpha/beta" evidence="1">
    <location>
      <begin position="2"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="modified residue" description="Serine amide" evidence="6">
    <location>
      <position position="67"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="12"/>
      <end position="65"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="16"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="23"/>
      <end position="47"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="27"/>
      <end position="49"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="2"/>
      <end position="5"/>
    </location>
  </feature>
  <feature type="turn" evidence="10">
    <location>
      <begin position="9"/>
      <end position="11"/>
    </location>
  </feature>
  <feature type="strand" evidence="9">
    <location>
      <begin position="18"/>
      <end position="20"/>
    </location>
  </feature>
  <feature type="helix" evidence="10">
    <location>
      <begin position="24"/>
      <end position="30"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="36"/>
      <end position="40"/>
    </location>
  </feature>
  <feature type="turn" evidence="10">
    <location>
      <begin position="41"/>
      <end position="43"/>
    </location>
  </feature>
  <feature type="strand" evidence="10">
    <location>
      <begin position="44"/>
      <end position="52"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1">
    <source>
      <dbReference type="PROSITE-ProRule" id="PRU01210"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="10516292"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="10678738"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="11382802"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="17355257"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="9690781"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0000305" key="8">
    <source>
      <dbReference type="PubMed" id="9690781"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="9">
    <source>
      <dbReference type="PDB" id="1BMR"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="10">
    <source>
      <dbReference type="PDB" id="7K18"/>
    </source>
  </evidence>
  <sequence length="67" mass="7057" checksum="19FE8EF96154328F" modified="1999-07-15" version="1">VRDGYIAQPENCVYHCFPGSSGCDTLCKEKGGTSGHCGFKVGHGLACWCNALPDNVGIIVEGEKCHS</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>