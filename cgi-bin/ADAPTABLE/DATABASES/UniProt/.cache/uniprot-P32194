<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1993-10-01" modified="2024-11-27" version="118" xmlns="http://uniprot.org/uniprot">
  <accession>P32194</accession>
  <name>PG1_PIG</name>
  <protein>
    <recommendedName>
      <fullName>Protegrin-1</fullName>
      <shortName>PG-1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Neutrophil peptide 1</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">NPG1</name>
  </gene>
  <organism>
    <name type="scientific">Sus scrofa</name>
    <name type="common">Pig</name>
    <dbReference type="NCBI Taxonomy" id="9823"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Laurasiatheria</taxon>
      <taxon>Artiodactyla</taxon>
      <taxon>Suina</taxon>
      <taxon>Suidae</taxon>
      <taxon>Sus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1994" name="FEBS Lett." volume="346" first="285" last="288">
      <title>Identification of a new member of the protegrin family by cDNA cloning.</title>
      <authorList>
        <person name="Zhao C."/>
        <person name="Liu L."/>
        <person name="Lehrer R.I."/>
      </authorList>
      <dbReference type="PubMed" id="8013647"/>
      <dbReference type="DOI" id="10.1016/0014-5793(94)00493-5"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>AMIDATION AT ARG-148</scope>
    <source>
      <tissue>Bone marrow</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="1995" name="FEBS Lett." volume="368" first="197" last="202">
      <title>The structure of porcine protegrin genes.</title>
      <authorList>
        <person name="Zhao C."/>
        <person name="Ganz T."/>
        <person name="Lehrer R.I."/>
      </authorList>
      <dbReference type="PubMed" id="7628604"/>
      <dbReference type="DOI" id="10.1016/0014-5793(95)00633-k"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [GENOMIC DNA]</scope>
    <source>
      <strain>Red Duroc</strain>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="1993" name="FEBS Lett." volume="327" first="231" last="236">
      <title>Protegrins: leukocyte antimicrobial peptides that combine features of corticostatic defensins and tachyplesins.</title>
      <authorList>
        <person name="Kokryakov V.N."/>
        <person name="Harwig S.S.L."/>
        <person name="Panyutich E.A."/>
        <person name="Shevchenko A.A."/>
        <person name="Aleshina G.M."/>
        <person name="Shamova O.V."/>
        <person name="Korneva H.A."/>
        <person name="Lehrer R.I."/>
      </authorList>
      <dbReference type="PubMed" id="8335113"/>
      <dbReference type="DOI" id="10.1016/0014-5793(93)80175-t"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 131-148</scope>
    <source>
      <tissue>Leukocyte</tissue>
    </source>
  </reference>
  <reference key="4">
    <citation type="journal article" date="1993" name="FEBS Lett." volume="330" first="339" last="342">
      <title>Primary structure of three cationic peptides from porcine neutrophils. Sequence determination by the combined usage of electrospray ionization mass spectrometry and Edman degradation.</title>
      <authorList>
        <person name="Mirgorodskaya O.A."/>
        <person name="Shevchenko A.A."/>
        <person name="Abdalla K.O.M.A."/>
        <person name="Chernushevich I.V."/>
        <person name="Egorov T.A."/>
        <person name="Musoliamov A.X."/>
        <person name="Kokryakov V.N."/>
        <person name="Shamova O.V."/>
      </authorList>
      <dbReference type="PubMed" id="8375505"/>
      <dbReference type="DOI" id="10.1016/0014-5793(93)80900-f"/>
    </citation>
    <scope>PROTEIN SEQUENCE OF 131-148</scope>
    <source>
      <tissue>Neutrophil</tissue>
    </source>
  </reference>
  <reference key="5">
    <citation type="journal article" date="1996" name="Eur. J. Biochem." volume="237" first="575" last="583">
      <title>Synthesis and solution structure of the antimicrobial peptide protegrin-1.</title>
      <authorList>
        <person name="Aumelase A."/>
        <person name="Mangoni M."/>
        <person name="Roumestand C."/>
        <person name="Chiche L."/>
        <person name="Despaux E."/>
        <person name="Grassy G."/>
        <person name="Calas B."/>
        <person name="Chavanieu A."/>
      </authorList>
      <dbReference type="PubMed" id="8647100"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1996.0575p.x"/>
    </citation>
    <scope>STRUCTURE BY NMR OF PROTEGRIN 1</scope>
  </reference>
  <reference key="6">
    <citation type="journal article" date="1996" name="Chem. Biol." volume="3" first="543" last="550">
      <title>Solution structure of protegrin-1, a broad-spectrum antimicrobial peptide from porcine leukocytes.</title>
      <authorList>
        <person name="Fahrner R.L."/>
        <person name="Dieckmann T."/>
        <person name="Harwig S.S."/>
        <person name="Lehrer R.I."/>
        <person name="Eisenberg D."/>
        <person name="Feigon J."/>
      </authorList>
      <dbReference type="PubMed" id="8807886"/>
      <dbReference type="DOI" id="10.1016/s1074-5521(96)90145-3"/>
    </citation>
    <scope>STRUCTURE BY NMR OF PROTEGRIN 1</scope>
  </reference>
  <comment type="function">
    <text>Microbicidal activity. Active against E.coli, Listeria monocytogenes and C.albicans, in vitro.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="7">Belongs to the cathelicidin family.</text>
  </comment>
  <dbReference type="EMBL" id="X79868">
    <property type="protein sequence ID" value="CAA56251.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="X84094">
    <property type="protein sequence ID" value="CAA58890.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="PIR" id="S66284">
    <property type="entry name" value="S57607"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_001116621.1">
    <property type="nucleotide sequence ID" value="NM_001123149.1"/>
  </dbReference>
  <dbReference type="PDB" id="1PG1">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=131-148"/>
  </dbReference>
  <dbReference type="PDB" id="1ZY6">
    <property type="method" value="NMR"/>
    <property type="chains" value="A/B=131-148"/>
  </dbReference>
  <dbReference type="PDBsum" id="1PG1"/>
  <dbReference type="PDBsum" id="1ZY6"/>
  <dbReference type="AlphaFoldDB" id="P32194"/>
  <dbReference type="BMRB" id="P32194"/>
  <dbReference type="SMR" id="P32194"/>
  <dbReference type="DIP" id="DIP-61300N"/>
  <dbReference type="TCDB" id="1.C.33.1.9">
    <property type="family name" value="the cathelicidin (cathelicidin) family"/>
  </dbReference>
  <dbReference type="PeptideAtlas" id="P32194"/>
  <dbReference type="Ensembl" id="ENSSSCT00070010715.1">
    <property type="protein sequence ID" value="ENSSSCP00070008821.1"/>
    <property type="gene ID" value="ENSSSCG00070005556.1"/>
  </dbReference>
  <dbReference type="GeneID" id="100144483"/>
  <dbReference type="KEGG" id="ssc:100144483"/>
  <dbReference type="CTD" id="100144483"/>
  <dbReference type="eggNOG" id="ENOG502SAES">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="InParanoid" id="P32194"/>
  <dbReference type="OrthoDB" id="4210525at2759"/>
  <dbReference type="EvolutionaryTrace" id="P32194"/>
  <dbReference type="Proteomes" id="UP000008227">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000314985">
    <property type="component" value="Unassembled WGS sequence"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694570">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694571">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694720">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694722">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694723">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694724">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694725">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694726">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694727">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="Proteomes" id="UP000694728">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005615">
    <property type="term" value="C:extracellular space"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0001530">
    <property type="term" value="F:lipopolysaccharide binding"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="FunFam" id="3.10.450.10:FF:000003">
    <property type="entry name" value="Cathelicidin antimicrobial peptide"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Gene3D" id="3.10.450.10">
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR001894">
    <property type="entry name" value="Cathelicidin-like"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018216">
    <property type="entry name" value="Cathelicidin_CS"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR046350">
    <property type="entry name" value="Cystatin_sf"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10206">
    <property type="entry name" value="CATHELICIDIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR10206:SF2">
    <property type="entry name" value="CATHELICIDIN ANTIMICROBIAL PEPTIDE"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00666">
    <property type="entry name" value="Cathelicidins"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF54403">
    <property type="entry name" value="Cystatin/monellin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00946">
    <property type="entry name" value="CATHELICIDINS_1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00947">
    <property type="entry name" value="CATHELICIDINS_2"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <feature type="signal peptide" evidence="2">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000004744" evidence="5 6">
    <location>
      <begin position="30"/>
      <end position="130"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000004745" description="Protegrin-1">
    <location>
      <begin position="131"/>
      <end position="148"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="3">
    <location>
      <begin position="61"/>
      <end position="80"/>
    </location>
  </feature>
  <feature type="modified residue" description="Arginine amide" evidence="4">
    <location>
      <position position="148"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="85"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="107"/>
      <end position="124"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="136"/>
      <end position="145"/>
    </location>
  </feature>
  <feature type="disulfide bond">
    <location>
      <begin position="138"/>
      <end position="143"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="133"/>
      <end position="139"/>
    </location>
  </feature>
  <feature type="strand" evidence="8">
    <location>
      <begin position="142"/>
      <end position="147"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000256" key="3">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="8013647"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="8335113"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="6">
    <source>
      <dbReference type="PubMed" id="8375505"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="7"/>
  <evidence type="ECO:0007829" key="8">
    <source>
      <dbReference type="PDB" id="1PG1"/>
    </source>
  </evidence>
  <sequence length="149" mass="16677" checksum="6EFBA98429CD6EC4" modified="1996-10-01" version="2" precursor="true">METQRASLCLGRWSLWLLLLALVVPSASAQALSYREAVLRAVDRLNEQSSEANLYRLLELDQPPKADEDPGTPKPVSFTVKETVCPRPTRQPPELCDFKENGRVKQCVGTVTLDQIKDPLDITCNEVQGVRGGRLCYCRRRFCVCVGRG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>