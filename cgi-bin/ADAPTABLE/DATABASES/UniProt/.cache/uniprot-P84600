<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2006-09-05" modified="2024-01-24" version="26" xmlns="http://uniprot.org/uniprot">
  <accession>P84600</accession>
  <name>DRS7_PITHY</name>
  <protein>
    <recommendedName>
      <fullName evidence="5">Dermaseptin-H7</fullName>
      <shortName evidence="5">DRS-H7</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="4">DShypo 05</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Pithecopus hypochondrialis</name>
    <name type="common">Orange-legged leaf frog</name>
    <name type="synonym">Phyllomedusa hypochondrialis</name>
    <dbReference type="NCBI Taxonomy" id="317381"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Neobatrachia</taxon>
      <taxon>Hyloidea</taxon>
      <taxon>Hylidae</taxon>
      <taxon>Phyllomedusinae</taxon>
      <taxon>Pithecopus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2006" name="Biochem. Biophys. Res. Commun." volume="347" first="739" last="746">
      <title>Novel dermaseptins from Phyllomedusa hypochondrialis (Amphibia).</title>
      <authorList>
        <person name="Brand G.D."/>
        <person name="Leite J.R.S.A."/>
        <person name="de Sa Mandel S.M."/>
        <person name="Mesquita D.A."/>
        <person name="Silva L.P."/>
        <person name="Prates M.V."/>
        <person name="Barbosa E.A."/>
        <person name="Vinecky F."/>
        <person name="Martins G.R."/>
        <person name="Galasso J.H."/>
        <person name="Kuckelhaus S.A.S."/>
        <person name="Sampaio R.N.R."/>
        <person name="Furtado J.R. Jr."/>
        <person name="Andrade A.C."/>
        <person name="Bloch C. Jr."/>
      </authorList>
      <dbReference type="PubMed" id="16844081"/>
      <dbReference type="DOI" id="10.1016/j.bbrc.2006.06.168"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT LEU-29</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2008" name="Peptides" volume="29" first="2074" last="2082">
      <title>A consistent nomenclature of antimicrobial peptides isolated from frogs of the subfamily Phyllomedusinae.</title>
      <authorList>
        <person name="Amiche M."/>
        <person name="Ladram A."/>
        <person name="Nicolas P."/>
      </authorList>
      <dbReference type="PubMed" id="18644413"/>
      <dbReference type="DOI" id="10.1016/j.peptides.2008.06.017"/>
    </citation>
    <scope>NOMENCLATURE</scope>
  </reference>
  <comment type="function">
    <text evidence="1 3">Has antibacterial activity against the Gram-negative bacterium E.coli and the Gram-positive bacterium S.aureus (PubMed:16844081). Has antiprotozoal activity against L.amazonensis (PubMed:16844081). Has antifungal activity (By similarity). Has no hemolytic activity (PubMed:16844081).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="3">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">Expressed by the skin glands.</text>
  </comment>
  <comment type="mass spectrometry" mass="2793.76" error="0.1" method="MALDI" evidence="3"/>
  <comment type="miscellaneous">
    <text evidence="6">The primary structure of this peptide is identical to that of Dermaseptin 01 from Pithecopus oreades (AC P83637).</text>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the frog skin active peptide (FSAP) family. Dermaseptin subfamily.</text>
  </comment>
  <comment type="online information" name="The antimicrobial peptide database">
    <link uri="https://wangapd3.com/database/query_output.php?ID=1389"/>
  </comment>
  <dbReference type="AlphaFoldDB" id="P84600"/>
  <dbReference type="SMR" id="P84600"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050832">
    <property type="term" value="P:defense response to fungus"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0295">Fungicide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000248497" description="Dermaseptin-H7">
    <location>
      <begin position="1"/>
      <end position="29"/>
    </location>
  </feature>
  <feature type="modified residue" description="Leucine amide" evidence="3">
    <location>
      <position position="29"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P83637"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="16844081"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="16844081"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="18644413"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <sequence length="29" mass="2795" checksum="B2B596776E2074C6" modified="2006-09-05" version="1">GLWSTIKQKGKEAAIAAAKAAGQAALGAL</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>