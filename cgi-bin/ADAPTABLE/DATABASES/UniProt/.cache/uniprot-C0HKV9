<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2018-05-23" modified="2022-05-25" version="6" xmlns="http://uniprot.org/uniprot">
  <accession>C0HKV9</accession>
  <accession>C0HKW0</accession>
  <accession>C0HKW1</accession>
  <name>SNPF_AGRIP</name>
  <protein>
    <recommendedName>
      <fullName evidence="6">Short neuropeptide F</fullName>
      <shortName evidence="6">sNPF</shortName>
    </recommendedName>
    <component>
      <recommendedName>
        <fullName evidence="6">sNPF peptide 1</fullName>
        <shortName evidence="4">sNPF-1</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="6">sNPF peptide 2</fullName>
        <shortName evidence="4">sNPF-2</shortName>
      </recommendedName>
    </component>
    <component>
      <recommendedName>
        <fullName evidence="6">sNPF peptide 3</fullName>
        <shortName evidence="4">sNPF-3</shortName>
      </recommendedName>
    </component>
  </protein>
  <organism>
    <name type="scientific">Agrotis ipsilon</name>
    <name type="common">Black cutworm moth</name>
    <dbReference type="NCBI Taxonomy" id="56364"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Noctuoidea</taxon>
      <taxon>Noctuidae</taxon>
      <taxon>Noctuinae</taxon>
      <taxon>Noctuini</taxon>
      <taxon>Agrotis</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2018" name="J. Proteome Res." volume="17" first="1397" last="1414">
      <title>Mating-induced differential peptidomics of neuropeptides and protein hormones in Agrotis ipsilon moths.</title>
      <authorList>
        <person name="Diesner M."/>
        <person name="Gallot A."/>
        <person name="Binz H."/>
        <person name="Gaertner C."/>
        <person name="Vitecek S."/>
        <person name="Kahnt J."/>
        <person name="Schachtner J."/>
        <person name="Jacquin-Joly E."/>
        <person name="Gadenne C."/>
      </authorList>
      <dbReference type="PubMed" id="29466015"/>
      <dbReference type="DOI" id="10.1021/acs.jproteome.7b00779"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 26-36; 64-70 AND 98-105</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT PHE-36; PHE-70 AND PHE-105</scope>
  </reference>
  <comment type="function">
    <text evidence="1">Plays a role in controlling food intake and regulating body size.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="5">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3">sNPF peptide 1: Expressed in corpora cardiaca (CC), corpora allata (CA), antennal lobe (AL) and gnathal ganglion (GNG) (at protein level). Expression in AL detected in all animals, in GNG in most animals, expression in CC and CA in some animals (at protein level). sNPF peptide 2: Expressed in corpora cardiaca (CC), corpora allata (CA), antennal lobe (AL) and gnathal ganglion (GNG) (at protein level). Expression in AL detected in all animals, in GNG, CC and CA in most animals (at protein level). sNPF peptide 3: Expressed in corpora cardiaca (CC), corpora allata (CA), antennal lobe (AL) and gnathal ganglion (GNG) (at protein level). Expression detected in all animals (at protein level).</text>
  </comment>
  <comment type="mass spectrometry" mass="1359.81" method="MALDI" evidence="3">
    <molecule>sNPF peptide 1</molecule>
    <text>Short neuropeptide F 1.</text>
  </comment>
  <comment type="mass spectrometry" mass="887.54" method="MALDI" evidence="3">
    <molecule>sNPF peptide 2</molecule>
    <text>Short neuropeptide F 2.</text>
  </comment>
  <comment type="mass spectrometry" mass="958.58" method="MALDI" evidence="3">
    <molecule>sNPF peptide 3</molecule>
    <text>Short neuropeptide F 3.</text>
  </comment>
  <comment type="similarity">
    <text evidence="5">Belongs to the NPY family.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="C0HKV9"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0165">Cleavage on pair of basic residues</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="propeptide" id="PRO_0000444276" evidence="5">
    <location>
      <begin position="1"/>
      <end position="23"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000444277" description="sNPF peptide 1" evidence="3">
    <location>
      <begin position="26"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444278" evidence="5">
    <location>
      <begin position="40"/>
      <end position="62"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000444279" description="sNPF peptide 2" evidence="3">
    <location>
      <begin position="64"/>
      <end position="70"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444280" evidence="5">
    <location>
      <begin position="74"/>
      <end position="96"/>
    </location>
  </feature>
  <feature type="peptide" id="PRO_0000444281" description="sNPF peptide 3" evidence="3">
    <location>
      <begin position="98"/>
      <end position="105"/>
    </location>
  </feature>
  <feature type="propeptide" id="PRO_0000444282" evidence="5">
    <location>
      <begin position="109"/>
      <end position="139"/>
    </location>
  </feature>
  <feature type="region of interest" description="Disordered" evidence="2">
    <location>
      <begin position="1"/>
      <end position="48"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="3">
    <location>
      <position position="36"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="3">
    <location>
      <position position="70"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="3">
    <location>
      <position position="105"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="Q9VIQ0"/>
    </source>
  </evidence>
  <evidence type="ECO:0000256" key="2">
    <source>
      <dbReference type="SAM" id="MobiDB-lite"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="29466015"/>
    </source>
  </evidence>
  <sequence length="139" mass="16066" checksum="CD9D26D836254B34" modified="2018-05-23" version="1">MGRARRTVRAPAQHDALGGHALARKSVRSPSRRLRFGRRSDPDMPPQAPLDEMNELLSLREVRTPVRLRFGRRSEERAVPHIFPQEFLTQEQDRAVRAPSIRLRFGRRSDNNMFLLPYESALPQEVKANGSVEDDRQQE</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>