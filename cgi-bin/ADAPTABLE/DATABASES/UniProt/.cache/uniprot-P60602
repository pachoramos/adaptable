<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2004-03-15" modified="2024-11-27" version="153" xmlns="http://uniprot.org/uniprot">
  <accession>P60602</accession>
  <accession>A7M872</accession>
  <accession>E1P5R9</accession>
  <accession>E9KL28</accession>
  <accession>Q3MHD5</accession>
  <accession>Q5QP16</accession>
  <accession>Q9CQ98</accession>
  <accession>Q9H1N2</accession>
  <name>ROMO1_HUMAN</name>
  <protein>
    <recommendedName>
      <fullName>Reactive oxygen species modulator 1</fullName>
      <shortName>ROS modulator 1</shortName>
    </recommendedName>
    <alternativeName>
      <fullName>Epididymis tissue protein Li 175</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Glyrichin</fullName>
    </alternativeName>
    <alternativeName>
      <fullName>Mitochondrial targeting GxxxG motif protein</fullName>
      <shortName>MTGM</shortName>
    </alternativeName>
    <alternativeName>
      <fullName>Protein MGR2 homolog</fullName>
    </alternativeName>
  </protein>
  <gene>
    <name type="primary">ROMO1</name>
    <name type="synonym">C20orf52</name>
  </gene>
  <organism>
    <name type="scientific">Homo sapiens</name>
    <name type="common">Human</name>
    <dbReference type="NCBI Taxonomy" id="9606"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Mammalia</taxon>
      <taxon>Eutheria</taxon>
      <taxon>Euarchontoglires</taxon>
      <taxon>Primates</taxon>
      <taxon>Haplorrhini</taxon>
      <taxon>Catarrhini</taxon>
      <taxon>Hominidae</taxon>
      <taxon>Homo</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2009" name="J. Cell Sci." volume="122" first="2252" last="2262">
      <title>The novel conserved mitochondrial inner-membrane protein MTGM regulates mitochondrial morphology and cell proliferation.</title>
      <authorList>
        <person name="Zhao J."/>
        <person name="Liu T."/>
        <person name="Jin S.B."/>
        <person name="Tomilin N."/>
        <person name="Castro J."/>
        <person name="Shupliakov O."/>
        <person name="Lendahl U."/>
        <person name="Nister M."/>
      </authorList>
      <dbReference type="PubMed" id="19535734"/>
      <dbReference type="DOI" id="10.1242/jcs.038513"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 1)</scope>
    <scope>FUNCTION</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <source>
      <tissue>Brain</tissue>
    </source>
  </reference>
  <reference key="2">
    <citation type="journal article" date="2010" name="Mol. Cell. Proteomics" volume="9" first="2517" last="2528">
      <title>Systematic mapping and functional analysis of a family of human epididymal secretory sperm-located proteins.</title>
      <authorList>
        <person name="Li J."/>
        <person name="Liu F."/>
        <person name="Wang H."/>
        <person name="Liu X."/>
        <person name="Liu J."/>
        <person name="Li N."/>
        <person name="Wan F."/>
        <person name="Wang W."/>
        <person name="Zhang C."/>
        <person name="Jin S."/>
        <person name="Liu J."/>
        <person name="Zhu P."/>
        <person name="Liu Y."/>
      </authorList>
      <dbReference type="PubMed" id="20736409"/>
      <dbReference type="DOI" id="10.1074/mcp.m110.001719"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA]</scope>
    <source>
      <tissue>Epididymis</tissue>
    </source>
  </reference>
  <reference key="3">
    <citation type="journal article" date="2001" name="Nature" volume="414" first="865" last="871">
      <title>The DNA sequence and comparative analysis of human chromosome 20.</title>
      <authorList>
        <person name="Deloukas P."/>
        <person name="Matthews L.H."/>
        <person name="Ashurst J.L."/>
        <person name="Burton J."/>
        <person name="Gilbert J.G.R."/>
        <person name="Jones M."/>
        <person name="Stavrides G."/>
        <person name="Almeida J.P."/>
        <person name="Babbage A.K."/>
        <person name="Bagguley C.L."/>
        <person name="Bailey J."/>
        <person name="Barlow K.F."/>
        <person name="Bates K.N."/>
        <person name="Beard L.M."/>
        <person name="Beare D.M."/>
        <person name="Beasley O.P."/>
        <person name="Bird C.P."/>
        <person name="Blakey S.E."/>
        <person name="Bridgeman A.M."/>
        <person name="Brown A.J."/>
        <person name="Buck D."/>
        <person name="Burrill W.D."/>
        <person name="Butler A.P."/>
        <person name="Carder C."/>
        <person name="Carter N.P."/>
        <person name="Chapman J.C."/>
        <person name="Clamp M."/>
        <person name="Clark G."/>
        <person name="Clark L.N."/>
        <person name="Clark S.Y."/>
        <person name="Clee C.M."/>
        <person name="Clegg S."/>
        <person name="Cobley V.E."/>
        <person name="Collier R.E."/>
        <person name="Connor R.E."/>
        <person name="Corby N.R."/>
        <person name="Coulson A."/>
        <person name="Coville G.J."/>
        <person name="Deadman R."/>
        <person name="Dhami P.D."/>
        <person name="Dunn M."/>
        <person name="Ellington A.G."/>
        <person name="Frankland J.A."/>
        <person name="Fraser A."/>
        <person name="French L."/>
        <person name="Garner P."/>
        <person name="Grafham D.V."/>
        <person name="Griffiths C."/>
        <person name="Griffiths M.N.D."/>
        <person name="Gwilliam R."/>
        <person name="Hall R.E."/>
        <person name="Hammond S."/>
        <person name="Harley J.L."/>
        <person name="Heath P.D."/>
        <person name="Ho S."/>
        <person name="Holden J.L."/>
        <person name="Howden P.J."/>
        <person name="Huckle E."/>
        <person name="Hunt A.R."/>
        <person name="Hunt S.E."/>
        <person name="Jekosch K."/>
        <person name="Johnson C.M."/>
        <person name="Johnson D."/>
        <person name="Kay M.P."/>
        <person name="Kimberley A.M."/>
        <person name="King A."/>
        <person name="Knights A."/>
        <person name="Laird G.K."/>
        <person name="Lawlor S."/>
        <person name="Lehvaeslaiho M.H."/>
        <person name="Leversha M.A."/>
        <person name="Lloyd C."/>
        <person name="Lloyd D.M."/>
        <person name="Lovell J.D."/>
        <person name="Marsh V.L."/>
        <person name="Martin S.L."/>
        <person name="McConnachie L.J."/>
        <person name="McLay K."/>
        <person name="McMurray A.A."/>
        <person name="Milne S.A."/>
        <person name="Mistry D."/>
        <person name="Moore M.J.F."/>
        <person name="Mullikin J.C."/>
        <person name="Nickerson T."/>
        <person name="Oliver K."/>
        <person name="Parker A."/>
        <person name="Patel R."/>
        <person name="Pearce T.A.V."/>
        <person name="Peck A.I."/>
        <person name="Phillimore B.J.C.T."/>
        <person name="Prathalingam S.R."/>
        <person name="Plumb R.W."/>
        <person name="Ramsay H."/>
        <person name="Rice C.M."/>
        <person name="Ross M.T."/>
        <person name="Scott C.E."/>
        <person name="Sehra H.K."/>
        <person name="Shownkeen R."/>
        <person name="Sims S."/>
        <person name="Skuce C.D."/>
        <person name="Smith M.L."/>
        <person name="Soderlund C."/>
        <person name="Steward C.A."/>
        <person name="Sulston J.E."/>
        <person name="Swann R.M."/>
        <person name="Sycamore N."/>
        <person name="Taylor R."/>
        <person name="Tee L."/>
        <person name="Thomas D.W."/>
        <person name="Thorpe A."/>
        <person name="Tracey A."/>
        <person name="Tromans A.C."/>
        <person name="Vaudin M."/>
        <person name="Wall M."/>
        <person name="Wallis J.M."/>
        <person name="Whitehead S.L."/>
        <person name="Whittaker P."/>
        <person name="Willey D.L."/>
        <person name="Williams L."/>
        <person name="Williams S.A."/>
        <person name="Wilming L."/>
        <person name="Wray P.W."/>
        <person name="Hubbard T."/>
        <person name="Durbin R.M."/>
        <person name="Bentley D.R."/>
        <person name="Beck S."/>
        <person name="Rogers J."/>
      </authorList>
      <dbReference type="PubMed" id="11780052"/>
      <dbReference type="DOI" id="10.1038/414865a"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="4">
    <citation type="submission" date="2005-09" db="EMBL/GenBank/DDBJ databases">
      <authorList>
        <person name="Mural R.J."/>
        <person name="Istrail S."/>
        <person name="Sutton G.G."/>
        <person name="Florea L."/>
        <person name="Halpern A.L."/>
        <person name="Mobarry C.M."/>
        <person name="Lippert R."/>
        <person name="Walenz B."/>
        <person name="Shatkay H."/>
        <person name="Dew I."/>
        <person name="Miller J.R."/>
        <person name="Flanigan M.J."/>
        <person name="Edwards N.J."/>
        <person name="Bolanos R."/>
        <person name="Fasulo D."/>
        <person name="Halldorsson B.V."/>
        <person name="Hannenhalli S."/>
        <person name="Turner R."/>
        <person name="Yooseph S."/>
        <person name="Lu F."/>
        <person name="Nusskern D.R."/>
        <person name="Shue B.C."/>
        <person name="Zheng X.H."/>
        <person name="Zhong F."/>
        <person name="Delcher A.L."/>
        <person name="Huson D.H."/>
        <person name="Kravitz S.A."/>
        <person name="Mouchard L."/>
        <person name="Reinert K."/>
        <person name="Remington K.A."/>
        <person name="Clark A.G."/>
        <person name="Waterman M.S."/>
        <person name="Eichler E.E."/>
        <person name="Adams M.D."/>
        <person name="Hunkapiller M.W."/>
        <person name="Myers E.W."/>
        <person name="Venter J.C."/>
      </authorList>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA]</scope>
  </reference>
  <reference key="5">
    <citation type="journal article" date="2004" name="Genome Res." volume="14" first="2121" last="2127">
      <title>The status, quality, and expansion of the NIH full-length cDNA project: the Mammalian Gene Collection (MGC).</title>
      <authorList>
        <consortium name="The MGC Project Team"/>
      </authorList>
      <dbReference type="PubMed" id="15489334"/>
      <dbReference type="DOI" id="10.1101/gr.2596504"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM 1)</scope>
    <source>
      <tissue>Bone marrow</tissue>
    </source>
  </reference>
  <reference key="6">
    <citation type="journal article" date="2006" name="Biochem. Biophys. Res. Commun." volume="347" first="649" last="655">
      <title>A novel protein, Romo1, induces ROS production in the mitochondria.</title>
      <authorList>
        <person name="Chung Y.M."/>
        <person name="Kim J.S."/>
        <person name="Yoo Y.D."/>
      </authorList>
      <dbReference type="PubMed" id="16842742"/>
      <dbReference type="DOI" id="10.1016/j.bbrc.2006.06.140"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>SUBCELLULAR LOCATION</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <reference key="7">
    <citation type="journal article" date="2007" name="Biochem. Biophys. Res. Commun." volume="359" first="304" last="310">
      <title>Drug resistance to 5-FU linked to reactive oxygen species modulator 1.</title>
      <authorList>
        <person name="Hwang I.T."/>
        <person name="Chung Y.M."/>
        <person name="Kim J.J."/>
        <person name="Chung J.S."/>
        <person name="Kim B.S."/>
        <person name="Kim H.J."/>
        <person name="Kim J.S."/>
        <person name="Yoo Y.D."/>
      </authorList>
      <dbReference type="PubMed" id="17537404"/>
      <dbReference type="DOI" id="10.1016/j.bbrc.2007.05.088"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>INDUCTION</scope>
  </reference>
  <reference key="8">
    <citation type="journal article" date="2008" name="Biochem. Biophys. Res. Commun." volume="369" first="672" last="678">
      <title>A critical role for Romo1-derived ROS in cell proliferation.</title>
      <authorList>
        <person name="Na A.R."/>
        <person name="Chung Y.M."/>
        <person name="Lee S.B."/>
        <person name="Park S.H."/>
        <person name="Lee M.-S."/>
        <person name="Yoo Y.D."/>
      </authorList>
      <dbReference type="PubMed" id="18313394"/>
      <dbReference type="DOI" id="10.1016/j.bbrc.2008.02.098"/>
    </citation>
    <scope>FUNCTION</scope>
  </reference>
  <reference key="9">
    <citation type="journal article" date="2008" name="J. Biol. Chem." volume="283" first="33763" last="33771">
      <title>Replicative senescence induced by Romo1-derived reactive oxygen species.</title>
      <authorList>
        <person name="Chung Y.M."/>
        <person name="Lee S.B."/>
        <person name="Kim H.J."/>
        <person name="Park S.H."/>
        <person name="Kim J.J."/>
        <person name="Chung J.S."/>
        <person name="Yoo Y.D."/>
      </authorList>
      <dbReference type="PubMed" id="18836179"/>
      <dbReference type="DOI" id="10.1074/jbc.m805334200"/>
    </citation>
    <scope>FUNCTION</scope>
    <scope>DEVELOPMENTAL STAGE</scope>
  </reference>
  <reference key="10">
    <citation type="journal article" date="2011" name="BMC Syst. Biol." volume="5" first="17" last="17">
      <title>Initial characterization of the human central proteome.</title>
      <authorList>
        <person name="Burkard T.R."/>
        <person name="Planyavsky M."/>
        <person name="Kaupe I."/>
        <person name="Breitwieser F.P."/>
        <person name="Buerckstuemmer T."/>
        <person name="Bennett K.L."/>
        <person name="Superti-Furga G."/>
        <person name="Colinge J."/>
      </authorList>
      <dbReference type="PubMed" id="21269460"/>
      <dbReference type="DOI" id="10.1186/1752-0509-5-17"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <reference key="11">
    <citation type="journal article" date="2012" name="J. Pept. Sci." volume="18" first="97" last="104">
      <title>Antibacterial potential of hGlyrichin encoded by a human gene.</title>
      <authorList>
        <person name="Sha J."/>
        <person name="Zhao G."/>
        <person name="Chen X."/>
        <person name="Guan W."/>
        <person name="He Y."/>
        <person name="Wang Z."/>
      </authorList>
      <dbReference type="PubMed" id="22083756"/>
      <dbReference type="DOI" id="10.1002/psc.1421"/>
    </citation>
    <scope>ANTIBACTERIAL FUNCTION</scope>
  </reference>
  <reference key="12">
    <citation type="journal article" date="2012" name="Proc. Natl. Acad. Sci. U.S.A." volume="109" first="12449" last="12454">
      <title>N-terminal acetylome analyses and functional insights of the N-terminal acetyltransferase NatB.</title>
      <authorList>
        <person name="Van Damme P."/>
        <person name="Lasa M."/>
        <person name="Polevoda B."/>
        <person name="Gazquez C."/>
        <person name="Elosegui-Artola A."/>
        <person name="Kim D.S."/>
        <person name="De Juan-Pardo E."/>
        <person name="Demeyer K."/>
        <person name="Hole K."/>
        <person name="Larrea E."/>
        <person name="Timmerman E."/>
        <person name="Prieto J."/>
        <person name="Arnesen T."/>
        <person name="Sherman F."/>
        <person name="Gevaert K."/>
        <person name="Aldabe R."/>
      </authorList>
      <dbReference type="PubMed" id="22814378"/>
      <dbReference type="DOI" id="10.1073/pnas.1210303109"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <reference key="13">
    <citation type="journal article" date="2015" name="Proteomics" volume="15" first="2519" last="2524">
      <title>N-terminome analysis of the human mitochondrial proteome.</title>
      <authorList>
        <person name="Vaca Jacome A.S."/>
        <person name="Rabilloud T."/>
        <person name="Schaeffer-Reiss C."/>
        <person name="Rompais M."/>
        <person name="Ayoub D."/>
        <person name="Lane L."/>
        <person name="Bairoch A."/>
        <person name="Van Dorsselaer A."/>
        <person name="Carapito C."/>
      </authorList>
      <dbReference type="PubMed" id="25944712"/>
      <dbReference type="DOI" id="10.1002/pmic.201400617"/>
    </citation>
    <scope>CLEAVAGE OF INITIATOR METHIONINE [LARGE SCALE ANALYSIS]</scope>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS]</scope>
  </reference>
  <comment type="function">
    <text>Induces production of reactive oxygen species (ROS) which are necessary for cell proliferation. May play a role in inducing oxidative DNA damage and replicative senescence. May play a role in the coordination of mitochondrial morphology and cell proliferation.</text>
  </comment>
  <comment type="function">
    <text>Has antibacterial activity against a variety of bacteria including S.aureus, P.aeruginosa and M.tuberculosis. Acts by inducing bacterial membrane breakage.</text>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-11909831">
      <id>P60602</id>
    </interactant>
    <interactant intactId="EBI-7062247">
      <id>Q9UHD4</id>
      <label>CIDEB</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="interaction">
    <interactant intactId="EBI-11909831">
      <id>P60602</id>
    </interactant>
    <interactant intactId="EBI-17589229">
      <id>Q6NTF9-3</id>
      <label>RHBDD2</label>
    </interactant>
    <organismsDiffer>false</organismsDiffer>
    <experiments>3</experiments>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="2 5">Mitochondrion inner membrane</location>
      <topology evidence="2 5">Single-pass membrane protein</topology>
    </subcellularLocation>
  </comment>
  <comment type="alternative products">
    <event type="alternative splicing"/>
    <isoform>
      <id>P60602-1</id>
      <name>1</name>
      <sequence type="displayed"/>
    </isoform>
    <isoform>
      <id>P60602-2</id>
      <name>2</name>
      <sequence type="described" ref="VSP_036486"/>
    </isoform>
  </comment>
  <comment type="tissue specificity">
    <text evidence="2 5">Up-regulated in a number of cancer cell lines when compared to a normal lung fibroblast cell line. Highly expressed in brain tumors.</text>
  </comment>
  <comment type="developmental stage">
    <text evidence="4">Expression increases in senescent cells.</text>
  </comment>
  <comment type="induction">
    <text evidence="3">By the anticancer drug fluorouracil (5FU).</text>
  </comment>
  <comment type="miscellaneous">
    <text>Enforced expression in IMR-90 cells leads to increased levels of ROS and induces premature cell senescence and nuclear DNA damage.</text>
  </comment>
  <comment type="similarity">
    <text evidence="6">Belongs to the MGR2 family.</text>
  </comment>
  <dbReference type="EMBL" id="AM397244">
    <property type="protein sequence ID" value="CAL37002.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AM397245">
    <property type="protein sequence ID" value="CAL37003.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AM397246">
    <property type="protein sequence ID" value="CAL37004.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="GU727625">
    <property type="protein sequence ID" value="ADU87627.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="AL357374">
    <property type="status" value="NOT_ANNOTATED_CDS"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH471077">
    <property type="protein sequence ID" value="EAW76168.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="CH471077">
    <property type="protein sequence ID" value="EAW76169.1"/>
    <property type="molecule type" value="Genomic_DNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC008488">
    <property type="protein sequence ID" value="AAH08488.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="EMBL" id="BC105281">
    <property type="protein sequence ID" value="AAI05282.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="CCDS" id="CCDS13264.1">
    <molecule id="P60602-1"/>
  </dbReference>
  <dbReference type="RefSeq" id="NP_542786.1">
    <molecule id="P60602-1"/>
    <property type="nucleotide sequence ID" value="NM_080748.2"/>
  </dbReference>
  <dbReference type="RefSeq" id="XP_016883167.1">
    <molecule id="P60602-1"/>
    <property type="nucleotide sequence ID" value="XM_017027678.1"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P60602"/>
  <dbReference type="SMR" id="P60602"/>
  <dbReference type="BioGRID" id="126718">
    <property type="interactions" value="31"/>
  </dbReference>
  <dbReference type="ComplexPortal" id="CPX-6129">
    <property type="entry name" value="TIM23 mitochondrial inner membrane pre-sequence translocase complex, TIM17A variant"/>
  </dbReference>
  <dbReference type="ComplexPortal" id="CPX-6130">
    <property type="entry name" value="TIM23 mitochondrial inner membrane pre-sequence translocase complex, TIM17B variant"/>
  </dbReference>
  <dbReference type="IntAct" id="P60602">
    <property type="interactions" value="6"/>
  </dbReference>
  <dbReference type="MINT" id="P60602"/>
  <dbReference type="STRING" id="9606.ENSP00000363191"/>
  <dbReference type="TCDB" id="1.A.111.1.1">
    <property type="family name" value="the reactive oxygen species modulator 1 (romo1) family"/>
  </dbReference>
  <dbReference type="GlyGen" id="P60602">
    <property type="glycosylation" value="1 site, 1 O-linked glycan (1 site)"/>
  </dbReference>
  <dbReference type="iPTMnet" id="P60602"/>
  <dbReference type="PhosphoSitePlus" id="P60602"/>
  <dbReference type="SwissPalm" id="P60602"/>
  <dbReference type="BioMuta" id="ROMO1"/>
  <dbReference type="DMDM" id="45593159"/>
  <dbReference type="jPOST" id="P60602"/>
  <dbReference type="MassIVE" id="P60602"/>
  <dbReference type="PaxDb" id="9606-ENSP00000363191"/>
  <dbReference type="PeptideAtlas" id="P60602"/>
  <dbReference type="ProteomicsDB" id="57216">
    <molecule id="P60602-1"/>
  </dbReference>
  <dbReference type="ProteomicsDB" id="57217">
    <molecule id="P60602-2"/>
  </dbReference>
  <dbReference type="Pumba" id="P60602"/>
  <dbReference type="TopDownProteomics" id="P60602-1">
    <molecule id="P60602-1"/>
  </dbReference>
  <dbReference type="Antibodypedia" id="2412">
    <property type="antibodies" value="151 antibodies from 17 providers"/>
  </dbReference>
  <dbReference type="DNASU" id="140823"/>
  <dbReference type="Ensembl" id="ENST00000336695.4">
    <molecule id="P60602-1"/>
    <property type="protein sequence ID" value="ENSP00000338293.4"/>
    <property type="gene ID" value="ENSG00000125995.16"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000374072.5">
    <molecule id="P60602-2"/>
    <property type="protein sequence ID" value="ENSP00000363185.1"/>
    <property type="gene ID" value="ENSG00000125995.16"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000374077.8">
    <molecule id="P60602-1"/>
    <property type="protein sequence ID" value="ENSP00000363190.3"/>
    <property type="gene ID" value="ENSG00000125995.16"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000374078.5">
    <molecule id="P60602-1"/>
    <property type="protein sequence ID" value="ENSP00000363191.1"/>
    <property type="gene ID" value="ENSG00000125995.16"/>
  </dbReference>
  <dbReference type="Ensembl" id="ENST00000397416.1">
    <molecule id="P60602-1"/>
    <property type="protein sequence ID" value="ENSP00000380561.1"/>
    <property type="gene ID" value="ENSG00000125995.16"/>
  </dbReference>
  <dbReference type="GeneID" id="140823"/>
  <dbReference type="KEGG" id="hsa:140823"/>
  <dbReference type="MANE-Select" id="ENST00000374077.8">
    <property type="protein sequence ID" value="ENSP00000363190.3"/>
    <property type="RefSeq nucleotide sequence ID" value="NM_080748.3"/>
    <property type="RefSeq protein sequence ID" value="NP_542786.1"/>
  </dbReference>
  <dbReference type="UCSC" id="uc002xdy.4">
    <molecule id="P60602-1"/>
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="AGR" id="HGNC:16185"/>
  <dbReference type="CTD" id="140823"/>
  <dbReference type="DisGeNET" id="140823"/>
  <dbReference type="GeneCards" id="ROMO1"/>
  <dbReference type="HGNC" id="HGNC:16185">
    <property type="gene designation" value="ROMO1"/>
  </dbReference>
  <dbReference type="HPA" id="ENSG00000125995">
    <property type="expression patterns" value="Low tissue specificity"/>
  </dbReference>
  <dbReference type="MIM" id="618894">
    <property type="type" value="gene"/>
  </dbReference>
  <dbReference type="neXtProt" id="NX_P60602"/>
  <dbReference type="OpenTargets" id="ENSG00000125995"/>
  <dbReference type="PharmGKB" id="PA164725540"/>
  <dbReference type="VEuPathDB" id="HostDB:ENSG00000125995"/>
  <dbReference type="eggNOG" id="KOG4096">
    <property type="taxonomic scope" value="Eukaryota"/>
  </dbReference>
  <dbReference type="GeneTree" id="ENSGT00390000005315"/>
  <dbReference type="HOGENOM" id="CLU_142435_2_0_1"/>
  <dbReference type="InParanoid" id="P60602"/>
  <dbReference type="OMA" id="LKMGGMM"/>
  <dbReference type="OrthoDB" id="2898048at2759"/>
  <dbReference type="PhylomeDB" id="P60602"/>
  <dbReference type="TreeFam" id="TF300273"/>
  <dbReference type="PathwayCommons" id="P60602"/>
  <dbReference type="SignaLink" id="P60602"/>
  <dbReference type="SIGNOR" id="P60602"/>
  <dbReference type="BioGRID-ORCS" id="140823">
    <property type="hits" value="513 hits in 1167 CRISPR screens"/>
  </dbReference>
  <dbReference type="ChiTaRS" id="ROMO1">
    <property type="organism name" value="human"/>
  </dbReference>
  <dbReference type="GenomeRNAi" id="140823"/>
  <dbReference type="Pharos" id="P60602">
    <property type="development level" value="Tbio"/>
  </dbReference>
  <dbReference type="PRO" id="PR:P60602"/>
  <dbReference type="Proteomes" id="UP000005640">
    <property type="component" value="Chromosome 20"/>
  </dbReference>
  <dbReference type="RNAct" id="P60602">
    <property type="molecule type" value="protein"/>
  </dbReference>
  <dbReference type="Bgee" id="ENSG00000125995">
    <property type="expression patterns" value="Expressed in apex of heart and 183 other cell types or tissues"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005743">
    <property type="term" value="C:mitochondrial inner membrane"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005739">
    <property type="term" value="C:mitochondrion"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005744">
    <property type="term" value="C:TIM23 mitochondrial import inner membrane translocase complex"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008324">
    <property type="term" value="F:monoatomic cation transmembrane transporter activity"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0061844">
    <property type="term" value="P:antimicrobial humoral immune response mediated by antimicrobial peptide"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0034614">
    <property type="term" value="P:cellular response to reactive oxygen species"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0051838">
    <property type="term" value="P:cytolysis by host of symbiont cells"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042742">
    <property type="term" value="P:defense response to bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050829">
    <property type="term" value="P:defense response to Gram-negative bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006886">
    <property type="term" value="P:intracellular protein transport"/>
    <property type="evidence" value="ECO:0000303"/>
    <property type="project" value="ComplexPortal"/>
  </dbReference>
  <dbReference type="GO" id="GO:0031640">
    <property type="term" value="P:killing of cells of another organism"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0098655">
    <property type="term" value="P:monoatomic cation transmembrane transport"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0008284">
    <property type="term" value="P:positive regulation of cell population proliferation"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:2000379">
    <property type="term" value="P:positive regulation of reactive oxygen species metabolic process"/>
    <property type="evidence" value="ECO:0000314"/>
    <property type="project" value="UniProtKB"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030150">
    <property type="term" value="P:protein import into mitochondrial matrix"/>
    <property type="evidence" value="ECO:0000318"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045039">
    <property type="term" value="P:protein insertion into mitochondrial inner membrane"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="FlyBase"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090399">
    <property type="term" value="P:replicative senescence"/>
    <property type="evidence" value="ECO:0000315"/>
    <property type="project" value="GO_Central"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR018450">
    <property type="entry name" value="Romo1/Mgr2"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR28525">
    <property type="entry name" value="REACTIVE OXYGEN SPECIES MODULATOR 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PANTHER" id="PTHR28525:SF1">
    <property type="entry name" value="REACTIVE OXYGEN SPECIES MODULATOR 1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="Pfam" id="PF10247">
    <property type="entry name" value="Romo1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="SMART" id="SM01378">
    <property type="entry name" value="Romo1"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0025">Alternative splicing</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0472">Membrane</keyword>
  <keyword id="KW-0496">Mitochondrion</keyword>
  <keyword id="KW-0999">Mitochondrion inner membrane</keyword>
  <keyword id="KW-1267">Proteomics identification</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0812">Transmembrane</keyword>
  <keyword id="KW-1133">Transmembrane helix</keyword>
  <feature type="initiator methionine" description="Removed" evidence="7">
    <location>
      <position position="1"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000079433" description="Reactive oxygen species modulator 1">
    <location>
      <begin position="2"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="transmembrane region" description="Helical" evidence="1">
    <location>
      <begin position="22"/>
      <end position="44"/>
    </location>
  </feature>
  <feature type="region of interest" description="Sufficient for antibacterial activity">
    <location>
      <begin position="42"/>
      <end position="60"/>
    </location>
  </feature>
  <feature type="splice variant" id="VSP_036486" description="In isoform 2." evidence="6">
    <original>RIGMRGRELMGGIGKTMMQSGGTFGTFMAIGMGIRC</original>
    <variation>SSILVSSSGSECGVES</variation>
    <location>
      <begin position="44"/>
      <end position="79"/>
    </location>
  </feature>
  <feature type="sequence variant" id="VAR_014127" description="In dbSNP:rs1044521.">
    <original>A</original>
    <variation>P</variation>
    <location>
      <position position="28"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="16842742"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="17537404"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="18836179"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="5">
    <source>
      <dbReference type="PubMed" id="19535734"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="6"/>
  <evidence type="ECO:0007744" key="7">
    <source>
      <dbReference type="PubMed" id="25944712"/>
    </source>
  </evidence>
  <sequence length="79" mass="8183" checksum="AD959C88C89EF80A" modified="2004-03-15" version="1">MPVAVGPYGQSQPSCFDRVKMGFVMGCAVGMAAGALFGTFSCLRIGMRGRELMGGIGKTMMQSGGTFGTFMAIGMGIRC</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>