<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2024-01-24" modified="2024-10-02" version="4" xmlns="http://uniprot.org/uniprot">
  <accession>P0DRC2</accession>
  <name>AITX3_METSE</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Alpha-actitoxin-Ms11a-3</fullName>
      <shortName evidence="4">Alpha-AITX-Ms11a-3</shortName>
    </recommendedName>
    <alternativeName>
      <fullName evidence="2">Alpha-anmTX-Ms11a-3</fullName>
    </alternativeName>
  </protein>
  <organism>
    <name type="scientific">Metridium senile</name>
    <name type="common">Brown sea anemone</name>
    <name type="synonym">Frilled sea anemone</name>
    <dbReference type="NCBI Taxonomy" id="6116"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Cnidaria</taxon>
      <taxon>Anthozoa</taxon>
      <taxon>Hexacorallia</taxon>
      <taxon>Actiniaria</taxon>
      <taxon>Nynantheae</taxon>
      <taxon>Metridiidae</taxon>
      <taxon>Metridium</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="2022" name="Toxins" volume="15">
      <title>Peptides from the sea anemone Metridium senile with modified inhibitor cystine knot (ICK) fold inhibit nicotinic acetylcholine receptors.</title>
      <authorList>
        <person name="Kasheverov I.E."/>
        <person name="Logashina Y.A."/>
        <person name="Kornilov F.D."/>
        <person name="Lushpa V.A."/>
        <person name="Maleeva E.E."/>
        <person name="Korolkova Y.V."/>
        <person name="Yu J."/>
        <person name="Zhu X."/>
        <person name="Zhangsun D."/>
        <person name="Luo S."/>
        <person name="Stensvaag K."/>
        <person name="Kudryavtsev D.S."/>
        <person name="Mineev K.S."/>
        <person name="Andreev Y.A."/>
      </authorList>
      <dbReference type="PubMed" id="36668848"/>
      <dbReference type="DOI" id="10.3390/toxins15010028"/>
    </citation>
    <scope>NUCLEOTIDE SEQUENCE [MRNA]</scope>
    <scope>PROTEIN SEQUENCE OF 25-54</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>RECOMBINANT EXPRESSION</scope>
    <scope>PROBABLE AMIDATION AT LYS-66</scope>
    <scope>STRUCTURE BY NMR OF 25-66</scope>
    <source>
      <tissue>Tentacle</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">Alpha-toxins act on postsynaptic membranes, they bind to the nicotinic acetylcholine receptors (nAChR) and thus inhibit them. This toxin shows inhibition against mouse alpha-1-beta-1-delta-epsilon (CHRNA1-CHRNB1-CHRND-CHRNE) (IC(50)=1215 nM), rat alpha-3-beta-4/CHRNA3-CHRNB4 (IC(50)=5.173 uM), rat alpha-7/CHRNA7 (IC(50)=4.786 uM), human alpha-7/CHRNA7 (IC(50)=8.869 uM), and rat alpha-9-alpha-10/CHRNA9-CHRNA10 (IC(50)=202 nM). Also competes with alpha-bungarotoxin for binding to orthosteric sites on muscle-type T.carlifornicus (IC(50)=256 nM) and human alpha-7/CHRNA7 nAChRs (IC(50)=19.81 uM).</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="1">Secreted</location>
    </subcellularLocation>
    <subcellularLocation>
      <location evidence="3">Nematocyst</location>
    </subcellularLocation>
  </comment>
  <comment type="domain">
    <text evidence="3">The presence of a 'disulfide through disulfide knot' structurally defines this protein as a knottin.</text>
  </comment>
  <comment type="mass spectrometry" mass="4918.4" method="MALDI" evidence="1"/>
  <comment type="miscellaneous">
    <text evidence="1">Negative results: does not show activity on rat alpha-2-beta-2/CHRNA2-CHRNB2, rat alpha-2-beta-4/CHRNA2-CHRNB4, rat alpha-3-beta-2/CHRNA3-CHRNB2, rat alpha-4-beta-2/CHRNA4-CHRNB2, rat alpha-4-beta-4/CHRNA4-CHRNB4 and rat alpha-6/alpha-3-beta-4 (CHRNA6/CHRNA3-CHRNB4). Does not show activity on rat TRPV1, human TRPV3, rat TRPA1 channels, the human ghrelin receptor, human neurotensin receptor 1, rat ASIC1a and ASIC3 channels.</text>
  </comment>
  <dbReference type="EMBL" id="ON605615">
    <property type="protein sequence ID" value="WCB99797.1"/>
    <property type="molecule type" value="mRNA"/>
  </dbReference>
  <dbReference type="PDB" id="6XYI">
    <property type="method" value="NMR"/>
    <property type="chains" value="A=25-66"/>
  </dbReference>
  <dbReference type="PDBsum" id="6XYI"/>
  <dbReference type="SMR" id="P0DRC2"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0035792">
    <property type="term" value="C:host cell postsynaptic membrane"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0042151">
    <property type="term" value="C:nematocyst"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0030550">
    <property type="term" value="F:acetylcholine receptor inhibitor activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="GO" id="GO:0090729">
    <property type="term" value="F:toxin activity"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0002">3D-structure</keyword>
  <keyword id="KW-0008">Acetylcholine receptor inhibiting toxin</keyword>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-0960">Knottin</keyword>
  <keyword id="KW-0166">Nematocyst</keyword>
  <keyword id="KW-0528">Neurotoxin</keyword>
  <keyword id="KW-0629">Postsynaptic neurotoxin</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <keyword id="KW-0732">Signal</keyword>
  <keyword id="KW-0800">Toxin</keyword>
  <feature type="signal peptide" evidence="4">
    <location>
      <begin position="1"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="chain" id="PRO_0000459529" description="Alpha-actitoxin-Ms11a-3">
    <location>
      <begin position="25"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="modified residue" description="Lysine amide" evidence="4">
    <location>
      <position position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 5">
    <location>
      <begin position="26"/>
      <end position="41"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 5">
    <location>
      <begin position="33"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1 5">
    <location>
      <begin position="40"/>
      <end position="61"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="32"/>
      <end position="34"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="44"/>
      <end position="46"/>
    </location>
  </feature>
  <feature type="strand" evidence="6">
    <location>
      <begin position="59"/>
      <end position="63"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="36668848"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="2">
    <source>
      <dbReference type="PubMed" id="36668848"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="3"/>
  <evidence type="ECO:0000305" key="4">
    <source>
      <dbReference type="PubMed" id="36668848"/>
    </source>
  </evidence>
  <evidence type="ECO:0007744" key="5">
    <source>
      <dbReference type="PDB" id="6XYI"/>
    </source>
  </evidence>
  <evidence type="ECO:0007829" key="6">
    <source>
      <dbReference type="PDB" id="6XYI"/>
    </source>
  </evidence>
  <sequence length="67" mass="7581" checksum="E5E1AC97D8C8F7FC" modified="2024-01-24" version="1" precursor="true">MASKIFFVLAVFLVMSAVLPESFAGCKKLNSYCTRQHRECCHGLVCRRPDYGIGRGILWKCTRARKG</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>