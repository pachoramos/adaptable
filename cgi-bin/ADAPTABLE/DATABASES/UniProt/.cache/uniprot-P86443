<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2010-03-23" modified="2019-12-11" version="12" xmlns="http://uniprot.org/uniprot">
  <accession>P86443</accession>
  <name>NPF_SCHGR</name>
  <protein>
    <recommendedName>
      <fullName evidence="5 6 7">Neuropeptide F</fullName>
      <shortName evidence="5">NPF</shortName>
      <shortName evidence="7">Scg-NPF</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Schistocerca gregaria</name>
    <name type="common">Desert locust</name>
    <name type="synonym">Gryllus gregarius</name>
    <dbReference type="NCBI Taxonomy" id="7010"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Orthoptera</taxon>
      <taxon>Caelifera</taxon>
      <taxon>Acrididea</taxon>
      <taxon>Acridomorpha</taxon>
      <taxon>Acridoidea</taxon>
      <taxon>Acrididae</taxon>
      <taxon>Cyrtacanthacridinae</taxon>
      <taxon>Schistocerca</taxon>
    </lineage>
  </organism>
  <reference evidence="8" key="1">
    <citation type="journal article" date="2001" name="Peptides" volume="22" first="219" last="227">
      <title>Newly discovered functions for some myotropic neuropeptides in locusts.</title>
      <authorList>
        <person name="Schoofs L."/>
        <person name="Clynen E."/>
        <person name="Cerstiaens A."/>
        <person name="Baggerman G."/>
        <person name="Wei Z."/>
        <person name="Vercammen T."/>
        <person name="Nachman R."/>
        <person name="De Loof A."/>
        <person name="Tanaka S."/>
      </authorList>
      <dbReference type="PubMed" id="11179815"/>
      <dbReference type="DOI" id="10.1016/s0196-9781(00)00385-5"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>FUNCTION</scope>
  </reference>
  <reference evidence="8" key="2">
    <citation type="journal article" date="2009" name="Ann. N. Y. Acad. Sci." volume="1163" first="60" last="74">
      <title>Identification of new members of the (short) neuropeptide F family in locusts and Caenorhabditis elegans.</title>
      <authorList>
        <person name="Clynen E."/>
        <person name="Husson S.J."/>
        <person name="Schoofs L."/>
      </authorList>
      <dbReference type="PubMed" id="19456328"/>
      <dbReference type="DOI" id="10.1111/j.1749-6632.2008.03624.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>TISSUE SPECIFICITY</scope>
    <scope>MASS SPECTROMETRY</scope>
    <scope>AMIDATION AT PHE-9</scope>
  </reference>
  <reference evidence="8" key="3">
    <citation type="journal article" date="2009" name="Insect Biochem. Mol. Biol." volume="39" first="491" last="507">
      <title>Peptidomic survey of the locust neuroendocrine system.</title>
      <authorList>
        <person name="Clynen E."/>
        <person name="Schoofs L."/>
      </authorList>
      <dbReference type="PubMed" id="19524670"/>
      <dbReference type="DOI" id="10.1016/j.ibmb.2009.06.001"/>
    </citation>
    <scope>IDENTIFICATION BY MASS SPECTROMETRY</scope>
    <scope>TISSUE SPECIFICITY</scope>
  </reference>
  <comment type="function">
    <text evidence="2">Accelerates ovarian maturation in females.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="8">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text evidence="3 4">Widely expressed in the nervous system. Expressed in corpora cardiaca, hypocerebral ganglion, frontal ganglion, protocerebrum, antennal lobe, tritocerebrum, thoracic ganglia, esophageal nerves, recurrent nerve and frontal connectives. Not detected in corpora allata, circumesophageal connectives, subesophageal ganglion, abdominal ganglion and abdominal perisympathetic organs.</text>
  </comment>
  <comment type="mass spectrometry" mass="1121.6" method="MALDI" evidence="3"/>
  <comment type="similarity">
    <text evidence="1">Belongs to the NPY family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000392446" description="Neuropeptide F" evidence="2 3">
    <location>
      <begin position="1"/>
      <end position="9"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="3">
    <location>
      <position position="9"/>
    </location>
  </feature>
  <evidence type="ECO:0000255" key="1"/>
  <evidence type="ECO:0000269" key="2">
    <source>
      <dbReference type="PubMed" id="11179815"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="19456328"/>
    </source>
  </evidence>
  <evidence type="ECO:0000269" key="4">
    <source>
      <dbReference type="PubMed" id="19524670"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="5">
    <source>
      <dbReference type="PubMed" id="11179815"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="6">
    <source>
      <dbReference type="PubMed" id="19456328"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="7">
    <source>
      <dbReference type="PubMed" id="19524670"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="8"/>
  <sequence length="9" mass="1123" checksum="88A917740DC2D6C5" modified="2010-03-23" version="1">YSQVARPRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>