<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1986-07-21" modified="2022-08-03" version="66" xmlns="http://uniprot.org/uniprot">
  <accession>P01511</accession>
  <name>CECD_ANTPE</name>
  <protein>
    <recommendedName>
      <fullName>Cecropin-D</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Antheraea pernyi</name>
    <name type="common">Chinese oak silk moth</name>
    <name type="synonym">Bombyx pernyi</name>
    <dbReference type="NCBI Taxonomy" id="7119"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Endopterygota</taxon>
      <taxon>Lepidoptera</taxon>
      <taxon>Glossata</taxon>
      <taxon>Ditrysia</taxon>
      <taxon>Bombycoidea</taxon>
      <taxon>Saturniidae</taxon>
      <taxon>Saturniinae</taxon>
      <taxon>Saturniini</taxon>
      <taxon>Antheraea</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1982" name="Eur. J. Biochem." volume="127" first="219" last="224">
      <title>Insect immunity: isolation and structure of cecropins B and D from pupae of the Chinese oak silk moth, Antheraea pernyi.</title>
      <authorList>
        <person name="Qu X.-M."/>
        <person name="Steiner H."/>
        <person name="Engstroem A."/>
        <person name="Bennich H."/>
        <person name="Boman H.G."/>
      </authorList>
      <dbReference type="PubMed" id="6754375"/>
      <dbReference type="DOI" id="10.1111/j.1432-1033.1982.tb06858.x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT LYS-36</scope>
  </reference>
  <comment type="function">
    <text>Cecropins have lytic and antibacterial activity against several Gram-positive and Gram-negative bacteria.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the cecropin family.</text>
  </comment>
  <dbReference type="PIR" id="A01773">
    <property type="entry name" value="CKAODP"/>
  </dbReference>
  <dbReference type="AlphaFoldDB" id="P01511"/>
  <dbReference type="SMR" id="P01511"/>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0019731">
    <property type="term" value="P:antibacterial humoral response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="InterPro"/>
  </dbReference>
  <dbReference type="GO" id="GO:0050830">
    <property type="term" value="P:defense response to Gram-positive bacterium"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProt"/>
  </dbReference>
  <dbReference type="GO" id="GO:0045087">
    <property type="term" value="P:innate immune response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR000875">
    <property type="entry name" value="Cecropin"/>
  </dbReference>
  <dbReference type="Pfam" id="PF00272">
    <property type="entry name" value="Cecropin"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="PROSITE" id="PS00268">
    <property type="entry name" value="CECROPIN"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0044">Antibiotic</keyword>
  <keyword id="KW-0929">Antimicrobial</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0391">Immunity</keyword>
  <keyword id="KW-0399">Innate immunity</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000044678" description="Cecropin-D">
    <location>
      <begin position="1"/>
      <end position="36"/>
    </location>
  </feature>
  <feature type="modified residue" description="Lysine amide" evidence="1">
    <location>
      <position position="36"/>
    </location>
  </feature>
  <evidence type="ECO:0000269" key="1">
    <source>
      <dbReference type="PubMed" id="6754375"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="2"/>
  <sequence length="36" mass="3807" checksum="FDB270F2AC4873B9" modified="1986-07-21" version="1">WNPFKELERAGQRVRDAIISAGPAVATVAQATALAK</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>