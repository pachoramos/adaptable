<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="1995-02-01" modified="2024-11-27" version="52" xmlns="http://uniprot.org/uniprot">
  <accession>P38952</accession>
  <name>XEN3_XENLA</name>
  <protein>
    <recommendedName>
      <fullName>Xenoxin-3</fullName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Xenopus laevis</name>
    <name type="common">African clawed frog</name>
    <dbReference type="NCBI Taxonomy" id="8355"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Chordata</taxon>
      <taxon>Craniata</taxon>
      <taxon>Vertebrata</taxon>
      <taxon>Euteleostomi</taxon>
      <taxon>Amphibia</taxon>
      <taxon>Batrachia</taxon>
      <taxon>Anura</taxon>
      <taxon>Pipoidea</taxon>
      <taxon>Pipidae</taxon>
      <taxon>Xenopodinae</taxon>
      <taxon>Xenopus</taxon>
      <taxon>Xenopus</taxon>
    </lineage>
  </organism>
  <reference key="1">
    <citation type="journal article" date="1993" name="J. Biol. Chem." volume="268" first="16458" last="16464">
      <title>Xenoxins, a family of peptides from dorsal gland secretion of Xenopus laevis related to snake venom cytotoxins and neurotoxins.</title>
      <authorList>
        <person name="Kolbe H.V.J."/>
        <person name="Huber A."/>
        <person name="Cordier P."/>
        <person name="Rasmussen U.B."/>
        <person name="Bouchon B."/>
        <person name="Jaquinod M."/>
        <person name="Vlasak R."/>
        <person name="Delot E.C."/>
        <person name="Kreil G."/>
      </authorList>
      <dbReference type="PubMed" id="8393864"/>
      <dbReference type="DOI" id="10.1016/s0021-9258(19)85442-x"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <source>
      <tissue>Skin secretion</tissue>
    </source>
  </reference>
  <comment type="function">
    <text>Lacks alpha-neurotoxic activity, has apparently no antibacterial activity, nor anti-coagulant potency.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location>Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="tissue specificity">
    <text>Expressed by the skin dorsal glands.</text>
  </comment>
  <dbReference type="AlphaFoldDB" id="P38952"/>
  <dbReference type="SMR" id="P38952"/>
  <dbReference type="Proteomes" id="UP000186698">
    <property type="component" value="Unplaced"/>
  </dbReference>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0006952">
    <property type="term" value="P:defense response"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <dbReference type="Gene3D" id="2.10.60.10">
    <property type="entry name" value="CD59"/>
    <property type="match status" value="1"/>
  </dbReference>
  <dbReference type="InterPro" id="IPR045860">
    <property type="entry name" value="Snake_toxin-like_sf"/>
  </dbReference>
  <dbReference type="SUPFAM" id="SSF57302">
    <property type="entry name" value="Snake toxin-like"/>
    <property type="match status" value="1"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0878">Amphibian defense peptide</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-1015">Disulfide bond</keyword>
  <keyword id="KW-1185">Reference proteome</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="chain" id="PRO_0000190097" description="Xenoxin-3">
    <location>
      <begin position="1"/>
      <end position="66"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="3"/>
      <end position="24"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="17"/>
      <end position="37"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="43"/>
      <end position="58"/>
    </location>
  </feature>
  <feature type="disulfide bond" evidence="1">
    <location>
      <begin position="59"/>
      <end position="64"/>
    </location>
  </feature>
  <feature type="unsure residue">
    <location>
      <begin position="23"/>
      <end position="35"/>
    </location>
  </feature>
  <feature type="unsure residue">
    <location>
      <begin position="42"/>
      <end position="47"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1"/>
  <sequence length="66" mass="7259" checksum="36A2C26D981D88F5" modified="1995-02-01" version="1">LKCVNLQANGVKMTQECAKEDTKCLTLRSLKKTLKFCASDRICKTMKIASLPGEQITCCEGNMCNA</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>