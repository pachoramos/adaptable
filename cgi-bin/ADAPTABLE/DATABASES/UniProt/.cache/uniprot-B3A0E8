<?xml version="1.0" encoding="UTF-8"  standalone="no" ?>
<uniprot xmlns="http://uniprot.org/uniprot" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://uniprot.org/uniprot http://www.uniprot.org/docs/uniprot.xsd">
<entry dataset="Swiss-Prot" created="2013-03-06" modified="2019-12-11" version="7" xmlns="http://uniprot.org/uniprot">
  <accession>B3A0E8</accession>
  <name>FAR12_AUSGA</name>
  <protein>
    <recommendedName>
      <fullName evidence="4">Extended FMRFamide-12</fullName>
      <shortName evidence="4">FMRFa-12</shortName>
    </recommendedName>
  </protein>
  <organism>
    <name type="scientific">Austrophasma gansbaaiense</name>
    <name type="common">Gladiator</name>
    <name type="synonym">Heel-walker</name>
    <dbReference type="NCBI Taxonomy" id="253136"/>
    <lineage>
      <taxon>Eukaryota</taxon>
      <taxon>Metazoa</taxon>
      <taxon>Ecdysozoa</taxon>
      <taxon>Arthropoda</taxon>
      <taxon>Hexapoda</taxon>
      <taxon>Insecta</taxon>
      <taxon>Pterygota</taxon>
      <taxon>Neoptera</taxon>
      <taxon>Polyneoptera</taxon>
      <taxon>Mantophasmatodea</taxon>
      <taxon>Austrophasmatidae</taxon>
      <taxon>Austrophasma</taxon>
    </lineage>
  </organism>
  <reference evidence="5" key="1">
    <citation type="journal article" date="2012" name="Syst. Biol." volume="61" first="609" last="629">
      <title>Peptidomics-based phylogeny and biogeography of Mantophasmatodea (Hexapoda).</title>
      <authorList>
        <person name="Predel R."/>
        <person name="Neupert S."/>
        <person name="Huetteroth W."/>
        <person name="Kahnt J."/>
        <person name="Waidelich D."/>
        <person name="Roth S."/>
      </authorList>
      <dbReference type="PubMed" id="22508719"/>
      <dbReference type="DOI" id="10.1093/sysbio/sys003"/>
    </citation>
    <scope>PROTEIN SEQUENCE</scope>
    <scope>AMIDATION AT PHE-15</scope>
    <source>
      <tissue evidence="3">Thoracic perisympathetic organs</tissue>
    </source>
  </reference>
  <comment type="function">
    <text evidence="1">FMRFamides and FMRFamide-like peptides are neuropeptides.</text>
  </comment>
  <comment type="subcellular location">
    <subcellularLocation>
      <location evidence="6">Secreted</location>
    </subcellularLocation>
  </comment>
  <comment type="similarity">
    <text evidence="2">Belongs to the FARP (FMRF amide related peptide) family.</text>
  </comment>
  <dbReference type="GO" id="GO:0005576">
    <property type="term" value="C:extracellular region"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-SubCell"/>
  </dbReference>
  <dbReference type="GO" id="GO:0007218">
    <property type="term" value="P:neuropeptide signaling pathway"/>
    <property type="evidence" value="ECO:0007669"/>
    <property type="project" value="UniProtKB-KW"/>
  </dbReference>
  <proteinExistence type="evidence at protein level"/>
  <keyword id="KW-0027">Amidation</keyword>
  <keyword id="KW-0903">Direct protein sequencing</keyword>
  <keyword id="KW-0527">Neuropeptide</keyword>
  <keyword id="KW-0964">Secreted</keyword>
  <feature type="peptide" id="PRO_0000421554" description="Extended FMRFamide-12" evidence="3">
    <location>
      <begin position="1"/>
      <end position="15"/>
    </location>
  </feature>
  <feature type="modified residue" description="Phenylalanine amide" evidence="3">
    <location>
      <position position="15"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="3">
    <location>
      <position position="4"/>
    </location>
  </feature>
  <feature type="unsure residue" description="L or I" evidence="3">
    <location>
      <position position="13"/>
    </location>
  </feature>
  <evidence type="ECO:0000250" key="1">
    <source>
      <dbReference type="UniProtKB" id="P34405"/>
    </source>
  </evidence>
  <evidence type="ECO:0000255" key="2"/>
  <evidence type="ECO:0000269" key="3">
    <source>
      <dbReference type="PubMed" id="22508719"/>
    </source>
  </evidence>
  <evidence type="ECO:0000303" key="4">
    <source>
      <dbReference type="PubMed" id="22508719"/>
    </source>
  </evidence>
  <evidence type="ECO:0000305" key="5"/>
  <evidence type="ECO:0000305" key="6">
    <source>
      <dbReference type="PubMed" id="22508719"/>
    </source>
  </evidence>
  <sequence length="15" mass="1790" checksum="E4910723E80AF29E" modified="2013-03-06" version="1">SPALDDEHNDNFLRF</sequence>
</entry>
<copyright>
Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms Distributed under the Creative Commons Attribution (CC BY 4.0) License
</copyright>
</uniprot>