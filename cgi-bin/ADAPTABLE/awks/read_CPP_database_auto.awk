function read_CPP_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value,s) {
limit=3000
                a=1000;
                limit_=a+limit
                print "" > "tmp_files/modified_aa_cpp"
                path="DATABASES/CPP/CPPfiles/CPP"
                root_online="http://crdd.osdd.net/raghava/cppsite/display.php?details="
                length_numb=3
                add_tag="no"
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                while(a<limit_) {
		file=path""tag""a
                        if (system("[ -f " file " ] ") ==0 ) {
			input_file=file
#seq
				field=read_database_line(input_file,"PEPTIDE_SEQUENCE",0,"black'>","</b></td>",1)
				gsub("_","",field)
				gsub("Cyclo","",field)
				gsub("Biotin","",field)
				seq_a[a]=field
                                seq_a[a]=correct_sequence(seq_a[a])				
				read_modified_aa(modif,translate_modif)
				field=read_database_line(input_file,"'>CHEMICAL MODIFICATION",0,"'black'>","</b",1)
                                gsub("*","#",field)
                                gsub("*","#",seq_a[a])

                                # Fix awful unclosed parenthesis
                                field=clean_string(field)

                                # Make modifications separated by 'and' be separated by ; as all the others
                                gsub(" and ",";",field)

				# Make modification separated by ',' be separated by '=' without altering other uses of , (like 3,3-..., 4,2...) as here they split legend and acronym in this way
				string=""
				split(field,cc,","); w=0; while(w<length(cc)) {w=w+1
					string=string""cc[w]
					if (w<length(cc)) {
						if(substr(cc[w],length(cc[w]),1)!~/^[0-9]+$/ && substr(cc[w],length(cc[w]),1)!="S" && substr(cc[w],length(cc[w]),1)!="O" && substr(cc[w],length(cc[w]),1)!="P" ) {string=string"="} else {string=string","}
					}
				}
                                field=string

                                # We don't want to leave parenthesis in the final sequence
                                gsub("\\(","",seq_a[a])
                                gsub("\\)","",seq_a[a])
                                gsub("\\[","",seq_a[a])
                                gsub("\\]","",seq_a[a])
                                
				# This for many modifications splitted by ;
                                split(field,aa,";"); z=0;while(z<length(aa)) {z=z+1;

	                                gsub("_","",aa[z])
	                                split(aa[z],bb,":")

	                                # Generate a temporal file to allow us to update the modified aa assignments
	                                # Here we replace the patterns in the sequence as taken from Type of Modification field, that indicates the letter and the meaning
	                                if(aa[z]~/=/) {split(aa[z],bb,"=")}
	                                if(aa[z]~/:/) {split(aa[z],bb,":")}

                                        # Ignore case
                                        bb[2]=tolower(bb[2])

	                                if(translate_modif[bb[2]]=="" && bb[2]!="") {
	                                	print "translate_modif[\""bb[2]"\"]=\"X\";" >> "tmp_files/modified_aa_cpp"
		                               	# Show X for now until we update read_modified_aa
						translate_modif[bb[2]]="X"
					}
                                        # Do the substitution: we replace bb[1] (from sequence) with the translate_modif of the LEGEND (bb[2]) because sometimes there are cases like
                                        # O=Ornithine
                                        # X=Ornithine
                                        # for different IDs
					gsub(bb[1],translate_modif[bb[2]],seq_a[a])
				}
				seq_a[a]=analyze_sequence(seq_a[a])
				print "Reading "file" for sequence "seq_a[a]""
#ID
                                field=read_database_line(input_file,"CPPsite_ID",0,"black'>","</b></td>",1)
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"CPP"field";" }
				analyze_sequence(seq_a[a])
#name
                                field=read_database_line(input_file,"PEPTIDE_NAME",0,"black'>","</b></td>",1)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"SOURCE",0,"black'>","</b></td>",1)
                                field=clean_string(field)
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
#cyclic                 
                        field=read_database_line(input_file,"LINEAR/CYCLIC",0,"black'>","</b></td>",1)
                        if(field~/yclic/) {cyclic_[seq_a[a]]="cyclic"}
                        if(PTM_[seq_a[a]]!~field && field~/yclic/ && PTM_[seq_a[a]]!~/cyclization/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"cyclization;"}
#N_terminus
                                field=read_database_line(input_file,"N-TERMINAL_MODIFICATION",0,"black'>","</b></td>",1)
                                field=clean_string(field)
                                if(field!~"Free" && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                                gsub("Free","",field)
                                if(N_terminus_[seq_a[a]]!~field) {if(field!="") {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
#C_terminus
                                field=read_database_line(input_file,"C-TERMINAL MODIFICATION",0,"black'>","</b></td>",1)
                                field=clean_string(field)
                                if(field!~"Free" && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                                gsub("Free","",field)
                                if(C_terminus_[seq_a[a]]!~field) {if(field!="") {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
#stereo
                                field=read_database_line(input_file,"CHIRALITY",0,"black'>","</b></td>",1)
                                gsub("Modified","",field)
                                if (stereo_[seq_a[a]]!~field){if(field!="") {stereo_[seq_a[a]]=stereo_[seq_a[a]]""field";"}}
#Function
                        field="cell_penetrating"
			if(cell_penetrating_[seq_a[a]]!~field) {cell_penetrating_[seq_a[a]]=cell_penetrating_[seq_a[a]]""field";"}
#all organisms
# Nothing to get them
#DSSP
                                field=read_database_line(input_file,"Secondary_Structure",0,"black'>","</b></td>",2)
                                if(field=="NA") {field=""}
				if(DSSP_[seq_a[a]]!~field) {if(field!="") {DSSP_[seq_a[a]]=field}}
#Pubmed			
                        field=read_database_line(input_file,"PMID",0,"blank>","</a></b>",1)
                        if(field=="0") {field=""}
                        if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}
                        # http://crdd.osdd.net/raghava/cppsite/
			experimental_[seq_a[a]]="experimental"
	}
	a=a+1
}
cpmax=a
return cpmax
}
