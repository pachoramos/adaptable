function read_Camp_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value,mm,m) {
	limit=30000
	a=1;

	limit_=a+limit
		file="DATABASES/Camp/Campfiles/CAMPSQ"a
		while(a<limit_) {
			if (system("[ -f " file " ] ") ==0 ) {
#seq
				input_file=file

					field=read_database_line(input_file,"strong>Sequence:<",1,"fasta\">","</td>",1)
					gsub("<br>","",field)
					sub(/\r/,"",field)
					split(field,pp,",")
					field=pp[1]
					gsub("-NH_2","",field)
					gsub("-NH 2","",field)
					seq_a[a]=field
					print "Reading "file" for sequence "seq_a[a]""
					seq_a[a]=analyze_sequence(seq_a[a])
#PTM
# FIXME: for now write down the comments explaining the modifications as PTMs.
# For example: https://www.camp.bicnirrh.res.in/seqDisp.php?id=CAMPSQ17927
			field=read_database_line(input_file,"Comment :",1,"\"left\">","</td>")
                        read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                        antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                        antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                        cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                        tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)

			# Only keep Comments with ")=", as this field is really often poluted
			if (field~/)=/) {
				if(PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
			}
			if (field~/NH2/) {
				if(PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]"amidation;"}}
			}
#ID
					field=read_database_line(input_file,"Contact_Us",9,">","</td>",1)
					gsub("</td","",field)
	                                if(ID_[seq_a[a]]!~field) {if(field!="") {ID_[seq_a[a]]=ID_[seq_a[a]]""field";" }}
#Family
					field=read_database_line(input_file,"AMP_Family",1,">","</td>",1)
					gsub("</td","",field)
                                        if(Family_[seq_a[a]]!~field) {if(field!="") {Family_[seq_a[a]]=Family_[seq_a[a]]""field";" }}
#PMID,functions
                        field=1; z=0; while(field!="") {z=z+1
	                        field=read_database_line(input_file,"PubMed",1,"blank>","</a></td>",z)
        	                if (field!~/Reference:/) {gsub("_","",field)}
                	        gsub("Reference:","",field)
                	        if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}
                        }

			field=read_database_line(input_file,"Activity :",1,"\"left\">","</td>")
			read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
					antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
					antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
					cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
					tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)

			field=read_database_line(input_file,"Gram Nature :",1,"\"left\">","</td>")
			gsub("Gram_","Gram",field)
			read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
					antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
					antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
					cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
					tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)

			field=read_database_line(input_file,"Target",1,"\"justify\">","</div>")
			read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
					antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
					antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
					cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
					tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
			field=":"field
			gsub("&plusmn;","_+-",field)
			gsub("subsp.","subsp",field)
			gsub("MIC_against","",field)
			# Replace MICs inside [] by ()
			gsub("\\[","_(",field)
			gsub("\\]",")",field)
			gsub(";",",",field)
			# More cleaning
			gsub("_=_>","_=_",field)
			if(field!~/\(/) {
				gsub("microM;","microM);",field)
				gsub("_MIC","(_MIC",field)
			}
			string=read_activity_description(field,":",seq_a[a],all_organisms_)
#Hemolytic Activity
			field=read_database_line(input_file,"Hemolytic Activity :",1,"\"justify\">","</div>")
                        read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                        antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                        antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                        cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                        tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
                        field=":"field
                        # Also append to Target organisms until we work on merging Hemolytic activites there
                        string=read_activity_description(field,":",seq_a[a],all_organisms_)
#                        if(hemolytic_activity_[seq_a[a]]!="" && hemolytic_activity_[seq_a[a]]!="_") {field=field"("hemolytic_activity_test_[seq_a[a]]"="hemolytic_activity_[seq_a[a]]")"}
#                        string_to_compare=escape_pattern(field)
#                        if(RBC_source_[seq_a[a]]!~string_to_compare) {if(field!="") {RBC_source_[seq_a[a]]=RBC_source_[seq_a[a]]""field";"}}

#name
			field=read_database_line(input_file,"Title :",1,"\"left\">","<")
			gsub("Uncharacterized_protein","",field)
			gsub("PREDICTED:","",field)
			gsub("LOW_QUALITY_PROTEIN:","",field)
			gsub("Undefined","",field)
			field=clean_string(field)
			if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";"}}
#source
			field=read_database_line(input_file,"Source :",1,"\"left\">","<")
			field=clean_string(field)
			if(field~/ynthetic/) {
				synthetic_[seq_a[a]]="synthetic"
				gsub("Synthetic_construct","",field)
			}
			if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";"}}
#taxonomy
			field=read_database_line(input_file,"<b>Taxonomy :",1,"\"left\">","<")
			field=clean_string(field)
			field=tolower(field)
			if(taxonomy_[seq_a[a]]!~field) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}}
#experimental
                        field=read_database_line(input_file,"Validated",1,">","</td>",1)
                        field=tolower(field)
                        if(field~/experimentally_validated/ && experimental_[seq_a[a]]=="") {experimental_[seq_a[a]]="experimental"}
#Experimental structure/PDB
                        field=read_database_line(input_file,"http://www.rcsb.org/pdb/explore/",0,"structureId=","target")
                        field=toupper(field)
                        field=clean_string(field)
                        if(field!="") { if(experim_structure_[seq_a[a]]!~field) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}}
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}
		}
		close(input_file)
		a=a+1
			file="DATABASES/Camp/Campfiles/CAMPSQ"a
		amax=a
	}
	return amax
}
