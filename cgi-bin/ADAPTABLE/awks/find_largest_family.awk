function find_largest_family(family,original_family,fam_type,fam_father,original_fam_father,fam_size,fam_aligned,fam_properties,fam_names,alignments,alignments_transl,OUTPUT_INPUT,seq_vector,seq_vector_transl,level_agreement,class,properties,seq_names,min_seq_length,align_method,\
		d,dd,dmax,f,limit,a,len_seq,len_ref,head,tail,incr,e,le,max_le,q)
{
	dmax=length_nD(seq_vector,1)
		delete used_sequence
		delete seq_fam
		delete fam
		delete type_seq
		delete shift_seq
		delete min_shift
		delete max_leng
		delete prop_seq
		delete seq_names_fam
		delete seq_fam_transl
		if(class==1) {system("date")}
	t0=systime()
print ""
print "Forming family of sequences "class" (choosing from "dmax" sequences):"
		d=0;while(d<dmax) {d=d+1
			if (d==2 && class==1) {
				t1=systime() ; seconds=(t1-t0)*dmax
					print "Time estimated is at least "seconds" seconds ("seconds/60" minutes ; "seconds/3600" hours)."
			}
if(d>1) {if(d==2) {printf "1,"} ;printf d",";q=q+1; if(q==20) {q=0; print ""}}
			delete max_score_
				split(seq_vector[d],reference,"");shift[d]=0 ; len_ref=length(reference)
				split(seq_vector[d],p_seq,"")
				align_sequences(seq_type,max_score_,"***",d,reference,p_seq,len_ref,align_method,blosum)
				max_possible_score[d]=max_score_[d]; type_seq[d]=seq_type[1]
				min_shift[d]=100
				max_leng[d]=0
				if(len_ref>=min_seq_length) {
					dd=0;while(dd<dmax) {dd=dd+1
						delete max_score
							len_seq=length(seq_vector[dd]); if(len_seq>len_ref) {align_range[d]=len_seq} else {align_range[d]=len_ref}
						split(seq_vector[dd],p_seq,"")
							if(dd!=d) {
						shift[dd]=align_sequences(seq_type,max_score,"***",d,reference,p_seq,align_range[d],align_method,blosum);
								if(level_agreement=="auto") {if(len_ref<14) {limit=max_possible_score[d]/5*3} ; if(len_ref>=14 && len_ref<30) {limit=max_possible_score[d]/5*2} ; if(len_ref>=30) {limit=max_possible_score[d]/5}}
								else {
								limit=max_possible_score[d]/5*level_agreement
								}
									if(max_score[d]>limit && (((len_ref-((shift[dd]**2)**0.5))>=min_seq_length && shift[dd]>=0) || ((len_seq-((shift[dd]**2)**0.5))>=min_seq_length && shift[dd]<0) || level_agreement=="all") && used_sequence[dd]!="yes") {
										if (len_seq>max_leng[d]) {max_leng[d]=len_seq}
										if (shift[dd]<min_shift[d]){min_shift[d]=shift[dd]}
										fam[d]=fam[d]+1;seq_fam[d,fam[d]]=seq_vector[dd];seq_fam_transl[d,fam[d]]=seq_vector_transl[dd];
										type_seq[d]=seq_type[1];prop_seq[d,fam[d]]=properties[dd]
											seq_names_fam[d,fam[d]]=seq_names[dd]
											used_sequence[dd]="yes"
											shift_seq[d,fam[d]]=shift[dd]
									}
							}
							else {fam[d]=fam[d]+1;seq_fam[d,fam[d]]=seq_vector[dd];seq_fam_transl[d,fam[d]]=seq_vector_transl[dd];
							seq_aligned[d,fam[d]]=seq_vector[dd];prop_seq[d,fam[d]]=properties[dd];seq_names_fam[d,fam[d]]=seq_names[dd]
								used_sequence[dd]="yes"}
					}
				}
		}
	delete conv_index
		sort_nD(fam,conv_index,"@>","no","v","","","","","",1,dmax)
		fam_size[class]=fam[conv_index[1]]
print ""
print "Family of sequences "class" has "fam_size[class]" elements"
print ""
		fam_type[class]=type_seq[conv_index[1]]
		fam_father[class]=seq_vector[conv_index[1]]
		original_fam_father[class]=seq_vector_transl[conv_index[1]]
#		print ""
#		print "Family "class" has "fam_size[class]" elements. The best alignment is found with "fam_father[class]" (type "fam_type[class]"):"
		max_le=0
		f=0;while(f<fam_size[class]) {f=f+1
			if(min_shift[q]>0) {min_shift[q]=0}
			a=0;head=""; while(a<-min_shift[conv_index[1]]+shift_seq[conv_index[1],f]){a=a+1 ; head=head""sprintf("-"); }
			le=length(seq_fam[conv_index[1],f])+length(head) ; if (le>max_le) {max_le=le}
		}
	f=0;while(f<fam_size[class]) {f=f+1
		family[class,f]=seq_fam[conv_index[1],f]
		original_family[class,f]=seq_fam_transl[conv_index[1],f]
			fam_properties[class,f]=prop_seq[conv_index[1],f]
			fam_names[class,f]=seq_names_fam[conv_index[1],f]
			if(min_shift[conv_index[1]]>0) {min_shift[conv_index[1]]=0}
			a=0;head=""; while(a<-min_shift[conv_index[1]]+shift_seq[conv_index[1],f]){a=a+1 ; head=head""sprintf("-"); }
#		a=0;tail=""; while(a<max_leng[conv_index[1]]-min_shift[conv_index[1]]-length(seq_fam[conv_index[1],f])+(max_leng[conv_index[1]]-length(seq_fam[conv_index[1],f]))){a=a+1 ; tail=tail""sprintf("-"); }
		tail="";e=0; while(e<max_le-length(seq_fam[conv_index[1],f])-length(head)) {e=e+1;tail=tail""sprintf("-")}
		fam_aligned[class,f]=head""seq_fam[conv_index[1],f]""tail
			alignments[class,f]=head""seq_fam[conv_index[1],f]""tail
			alignments_transl[class,f]=head""seq_fam_transl[conv_index[1],f]""tail
#print fam_aligned[class,f],min_shift[conv_index[1]],max_leng[conv_index[1]],head,tail,seq_fam[conv_index[1],f],shift_seq[conv_index[1],f]
	}
#		read_alignment(alignments)
}

