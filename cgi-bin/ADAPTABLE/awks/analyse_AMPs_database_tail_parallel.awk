@include "awks/library.awk"
# Space needed since gawk-4.2
BEGIN{
	parameters(prp_name,prp_option)
	read_aa_prop_and_colors(aa_name,conv_aa,class,class_def,class_length,type,polarity,color_type)

		delete seq
		delete original_seq
		delete prop
		delete seq_names

		comparison_option=ENVIRON["comparison_option"]

		max_len=0
		input_file="OUTPUTS/"calculation_label"/selection"
		while ( getline < input_file >0 ) {
			if($2=="seq") {seq[$1]=$3}
			if($2=="oseq") {original_seq[$1]=$3;len=length($3);population[int(len)]=population[int(len)]+1 ; if(len>max_len) {max_len=len}}
			if($2=="prop_separator") {split($0,aaaa,"prop_separator ") ; prop[$1]=aaaa[2] }
			if($2=="names") {seq_names[$1]=$3}
			dmax=$1
		}
		close(input_file)

		# Get updated number of families after dropping duplicated ones in sort_families
		input_file="OUTPUTS/"calculation_label"/.fam_num"
                while ( getline < input_file >0 ) {
                        fam_num=$1
                }
                close(input_file)
                families_to_analyse=fam_num
                cmax=fam_num
                

                # Get the family to run tail on
                family_index=ENVIRON["family_index"]

		# [if f indicate the family index abd k is an index running on the components of each family]
		# family[f,k]: list of sequences that might be translated in different spaces (aa types or DSSP)
		# original_family[f,k] : list of pure sequences
		# alignments[f,k]: list of pure sequences aligned by introduction of - symbols
		# alignments_transl[f,k]: list of sequences (that might be translated in different spaces) aligned by introduction of - symbols
		# fam_type[f,k]: list of sequences translated in aa types FIXME: looks to not be used here, neither is generated to be used
		# fathers[f,ff]: list of fathers (that might be translated in different spaces) that can originate the same family, listed by score
		# original_fathers[f,ff]: list of fathers that can originate the same family, listed by score FIXME: Looks to not be really used in tail
		# fam_size[f]=number of elements in the family 
		# fam_properties[f,k]=list of properties of each sequence
		# fam_names[f,k]= list of names of each sequence
		# score_fathers[f,ff]=scores clqssifying the ability of each father to generate the family
		# code_fathers[f,ff]=code summarising the components of the family # FIXME: is not used for anything here
		# type_fathers[f,ff]=list of fathers translated in aa types

#		input_file="OUTPUTS/"calculation_label"/"calculation_label"_FASTA_orig_OUTPUT_f"family_index

#		input_file="tmp_files/"calculation_label"_sort_families_output_f"family_index
#		while ( getline <input_file>0 ) {
			#($1=""; print $0)
#			name=$1
			# We skip the first column once we have used it to set 'name', we cannot simply set value=$0 because we need to drop spaces coming from old separators
			# we use substr for that with 1 number higher than the number of characters to drop
#			$1=""
#			value=substr($0,2)

#			$name"="$value
#			print name,value
#		}
#		close(input_file)

                input_file="tmp_files/"calculation_label"_FASTA_fathers_OUTPUT_f"family_index
                f=0
                while ( getline < input_file >0 ) {f=f+1
                        score_fathers[family_index,f]=$1
                        fathers[family_index,f]=$2
			original_index_d[family_index,f]=$3
                }
                close(input_file)
                f=0
                input_file="tmp_files/"calculation_label"_FASTA_orig_fathers_OUTPUT_f"family_index
                while ( getline < input_file >0 ) {f=f+1
                        original_fathers[family_index,f]=$0
                }
                close(input_file)
                
                f=0
                input_file="tmp_files/"calculation_label"_FASTA_type_fathers_OUTPUT_f"family_index
                while ( getline < input_file >0 ) {f=f+1
                        type_fathers[family_index,f]=$0
                }
                close(input_file)

#                f=0
#                input_file="OUTPUTS/"calculation_label"/"calculation_label"_FASTA_OUTPUT_f"family_index
#                while ( getline < input_file >0 ) {f=f+1
#                        if ($0 ~ />/) {fam_properties[family_index,f]=$0}
#                        if ($0 !~ />/) {family[family_index,f]=$0}
#                }
#                close(input_file)
                
                f=0
                input_file="tmp_files/"calculation_label"_FASTA_prop_seq_OUTPUT_f"family_index
                while ( getline < input_file >0 ) {f=f+1
			fam_properties[family_index,f]=$0
                }
                close(input_file)

                f=0
                input_file="tmp_files/"calculation_label"_FASTA_seq_fam_OUTPUT_f"family_index
                while ( getline < input_file >0 ) {f=f+1
                        family[family_index,f]=$0
                }
                close(input_file)

                f=0
                input_file="tmp_files/"calculation_label"_FASTA_orig_OUTPUT_f"family_index
                while ( getline < input_file >0 ) {f=f+1
                	original_family[family_index,f]=$0
                }
                close(input_file)

                # FIXME: fam_type looks to not be generated by sort_families
#                f=0
#                input_file="OUTPUTS/"calculation_label"/"calculation_label"_FASTA_fam_OUTPUT_f"family_index
#                while ( getline < input_file >0 ) {f=f+1
#                	if($1!~/>/) {fam_size[family_index,1]=2}
#                }
#                close(input_file)
		input_file="tmp_files/"calculation_label"_FASTA_fam_OUTPUT_f"family_index
		while ( getline < input_file >0 ) {
			fam_size[family_index]=$0
		}
		close(input_file)
	
                f=0
                input_file="tmp_files/"calculation_label"_FASTA_fam_names_OUTPUT_f"family_index
                while ( getline < input_file >0 ) {f=f+1
                	fam_names[family_index,f]=$0
                }
                close(input_file)

                f=0
                input_file="tmp_files/"calculation_label"_FASTA_align_OUTPUT_f"family_index
                while ( getline < input_file >0 ) {f=f+1
                	alignments[family_index,f]=$1
			alignment_score_to_father[family_index,f]=$2
                }
                close(input_file)
                
                f=0
                input_file="tmp_files/"calculation_label"_FASTA_orig_align_OUTPUT_f"family_index
                while ( getline < input_file >0 ) {f=f+1
                	alignments_transl[family_index,f]=$1
			alignment__transl_score_to_father[family_index,f]=$2
                }
                close(input_file)

		if(generate_overlapping_families=="yes") {if(families_to_analyse=="") {fam_auto="yes";cmax=dmax;families_to_analyse=cmax}}
		else {
			fam_auto=""
				if(families_to_analyse=="" || fam_auto=="yes") {families_to_analyse=c ; fam_auto="yes"} else {if(c==families_to_analyse) {families_to_analyse=c}}
			cmax=dmax
		}

		families_to_analyse=family_index
		fam_count=family_index
		cmax=family_index

# 3) ANALYSIS. All families are generated; Now each one is analised 

		if(families_to_analyse==cmax && fam_auto=="yes") {fam_count0=0 ; last_fam=cmax}
		else {split(families_to_analyse,cl,"-"); fam_count0=cl[1]-1 ; last_fam=cl[2]}
	if(calculate_only_global_properties=="y") {fam_count0=0;last_fam=1}
	title("blue","_",80)
			if(calculate_only_global_properties!="y") {
			last_fam=fam_num
					print ""
					print "Calculating for Family "families_to_analyse"(out of "last_fam")..." 	
					print ""
					title("blue","_",80)
# arrays in two indexes (index of the family and index of the sequence) are transfered to arrays in one index, because we are analysing each family ata a time. At each familys these new arrays have to be deleted

        if (n_regions=="") {
                # In the case user has not set a desired n_regions, we set it to the length of the longuest selected peptide
                while ("grep ' seq ' OUTPUTS/"calculation_label"/selection | cut -d ' ' -f3 | wc -L" | getline n_regions){
                        print ""
# It seems people could be confused with this information
#                        print "Selecting n_regions="n_regions" automatically"
#                        print ""
                }}
                else{
#                        print "Using n_regions="n_regions" as selected by the user"
                        print ""
        }

					delete seq
					delete prop_f
					delete prp_glob
					delete short_prp
					delete prp_val
					delete align
					delete align_transl
					delete string
					delete string_summary
					delete prp_matrix
					delete DSSP_
					delete prp_matrix_unordered
					delete prp_val_unordered
					delete align_sorted
					delete align_transl_sorted
					delete short_prp_sorted
# 3.1) Data tranformation
# one-index arrays are generated; properties are converted into numerical code (1 and 0) for the generation of a compact table of properties
# depending on the parameter analyse_only_region_of_the_fam_father, the aligned sequences are cut to the length of the family father for analysis or not

					delete conv_index2
                                        sort_nD(score_fathers,conv_index2,">","no",families_to_analyse,"v","","","","","","",1,100)
                                        
                                        input_file="tmp_files/fam"original_index_d[families_to_analyse,conv_index2[1]]"_"calculation_label
                                        f=0
                                        while ( getline < input_file >0 ) {f=f+1
                                                alignment_score_to_father[family_index,f]=$9
                                                #align_score_to_father[f]=alignment_score_to_father[families_to_analyse,f]
						# Give a really small (to not alter percentages) "push" to the real father to get the proper order when all the scores are the same for a pattern (like EEEAA)
						if($10=="father") {align_score_to_father[f]=alignment_score_to_father[families_to_analyse,f]+0.000000001} else align_score_to_father[f]=alignment_score_to_father[families_to_analyse,f]
                                        }
                                        close(input_file)

                                        delete conv_index3
                                        sort_nD(align_score_to_father,conv_index3,">","no","v","","","","","",1,fam_size[families_to_analyse])

					index_fath=""
                                        f=0; while(f<fam_size[families_to_analyse]) {f=f+1 ; tmp_string=alignments[families_to_analyse,f] ; gsub("-","",tmp_string);
						if(tmp_string==fathers[families_to_analyse,conv_index2[1]]) {index_fath=f}}
					f=0; while(f<fam_size[families_to_analyse]) {f=f+1
						if(analyse_only_region_of_the_fam_father=="y") {
							# the sequences will be cut according to the length of the father. The line of the father might be preceeded by a series of "---"
							# in order to determine their number we split the line of the father with the sequence of the father
							split(alignments[families_to_analyse,index_fath],fff,fathers[families_to_analyse,conv_index2[1]])
							if(fff[1]~/-/) {st_scalar=length(fff[1])+1} else {st_scalar=1}
							seq[f]=substr(alignments[families_to_analyse,f],st_scalar,length(fathers[families_to_analyse,conv_index2[1]]))
						}
						else {seq[f]=alignments[families_to_analyse,f]
						}
						pmax=split(seq[f],fff,"")
						upper=pmax
						if(fam_count==fam_count0+1) {if (n_regions>pmax) {n_regions=pmax}}
							else {n_regions=pmax}
						prop_f[f]=fam_properties[families_to_analyse,f]

						align[f]=alignments[families_to_analyse,f]
						align_transl[f]=alignments_transl[families_to_analyse,f]
						split(prop_f[f],prp," ")
						short_prp[f]=fam_names[families_to_analyse,f]

						n_prop=66
						p=0;while(p<n_prop) {p=p+1
							prp_matrix_unordered[p,f]=prp[p]
								if(p==64) {prp_matrix_unordered[p,f]=peptide_solubility(family[families_to_analyse,f])}
							if((prp[p]=="_" || prp[p]=="") && p!=64) {prp_val_unordered[p,f]=0} else {prp_val_unordered[p,f]=1}
							prp_glob[p]=prp_glob[p]+prp_val_unordered[p,f]
						}
					}

					# Generate reordered vars accoding to their scores to father
                                        f=0; while(f<fam_size[families_to_analyse]) {f=f+1
                                                align_sorted[f]=align[conv_index3[f]]
                                                align_transl_sorted[f]=align_transl[conv_index3[f]]
                                                if(f==1) {short_prp_sorted[1]="         parent         \t"}#short_prp[conv_index3[1]]}
                                                else {
                                                	if(align_score_to_father[conv_index3[1]]!=0) {
                                                		score_similarity=sprintf("%3.1f",align_score_to_father[conv_index3[f]]/align_score_to_father[conv_index3[1]]*100)
                                                		short_prp_sorted[f]="Similarity to parent="score_similarity"% \t "
							} #short_prp[conv_index3[f]]}
						}

                                                p=0;while(p<n_prop) {p=p+1
                                                        prp_matrix[p,f]=prp_matrix_unordered[p,conv_index3[f]]
							prp_val[p,f]=prp_val_unordered[p,conv_index3[f]]
                                                }

                                        }

# for each property that cannot be translated into numbers, a summary of the members of the family is given; trying to avoid repetition of the same sentence 
					p=0;while(p<n_prop) {p=p+1
						f=0; while(f<fam_size[families_to_analyse]) {f=f+1
							if(p==1) {IDs[f]=prp_matrix[p,f]}
							if(p==2) {original_seq[f]=prp_matrix[p,f]}
							if(p==52) {DSSP_[f]=prp_matrix[p,f]}
							if(prp_matrix[p,f]!="_") {string3=clean_string(prp_matrix[p,f]);
								len_string3=split(string3,string4,";"); w=0; while(w<len_string3) {w=w+1 ; string5=clean_string(string4[w]) 
									if(string_summary[p]!~string5) { string_summary[p]=string5";"string_summary[p]}
								}
								string[p,f]=prp_matrix[p,f]";"string[p,f];
							}
						}
					}

					if(fam_size[families_to_analyse]==1) {print "Family "families_to_analyse" has "fam_size[families_to_analyse]" element. The best alignment is found with: "fathers[families_to_analyse,conv_index2[1]]} else {print "Family "families_to_analyse" has "fam_size[families_to_analyse]" elements. The best alignment is found with: "fathers[families_to_analyse,conv_index2[1]]}
					# We prefer now to not show this list and print it to a file instead
					#k=1;while(fathers[families_to_analyse,conv_index2[k]]!="") {print "father_score="score_fathers[families_to_analyse,conv_index2[k]],"       \t"fathers[families_to_analyse,conv_index2[k]],""; k=k+1} #index" original_index_d[families_to_analyse,conv_index2[k]]      \ttype("type_fathers[families_to_analyse,conv_index2[k]]"
					k=1;while(fathers[families_to_analyse,conv_index2[k]]!="") {print fathers[families_to_analyse,conv_index2[k]] >> "tmp_files/.all_fathers_"family_index; k=k+1}
					print ""
					print "Note: When 'simple' or 'DSSP' align methods are used, the original sequence is listed after modified one."
					read_alignment(align_sorted,short_prp_sorted,DSSP_,align_transl_sorted,work_in_simplified_aa_space)

					print ""
					print ""
					print "This family has the following properties:"
					printf "( 1 in "; color("red ,","red","","bold") ; printf "for user-selected properties (boolean or not)," 
					color(" orange ","orange","","bold");printf "for unselected boolean properties,"
					color(" (sub-μM)","blue_cyan0","","bold");color("-(μM)","blue_cyan1","","bold"); color("-(sub-mM)","blue_cyan2","","bold");color("-(mM)","blue_cyan3","","bold");color("-(very weak)","blue_cyan4","","bold");printf " for activity values, "
					color("green ","green","","bold");print "for unselected non-boolean information)"
					print ""
					n_prop=66
					p=0;max_string=0;while(p<n_prop) {p=p+1 ; str[p]=split(prp_name[p],ss,""); if (str[p]>max_string) {max_string=str[p]}}
#p=0;while(p<n_prop) {p=p+1 ; printf p"="prp_name[p]" "}
				n=0; while(n<max_string) {n=n+1
					printf("      ")
						p=0;while(p<n_prop) {p=p+1 ;if(n>(max_string-str[p])) {color(" "substr(prp_name[p],n-(max_string-str[p]),1)" ","blue_cyan0","","bold");} else {printf "   ";} ;if(p==n_prop) {print ""} }#5 "} } # We don't need this hack anymore for extract
				}
				print ""
					printf("      ");p=0;while(p<n_prop) {p=p+1 ; printf ("%2s ",p)}
				print ""
					f=0; while(f<fam_size[families_to_analyse]) {f=f+1 ;print "" ;printf("%4d. ",f) 
						p=0;while(p<n_prop) {p=p+1 ;
							if(prp_val[p,f]==1) {
								if(prp_option[p]=="i") {
									if(prp_matrix[p,f]==prp_name[p]) {color(" "prp_val[p,f]" ","orange","","bold")} 
									else {
										if(p==32 || p==38 || p==58 || p==65) {prp_tmp1=prp_matrix[p,f]+0
											if(prp_tmp1<1 && prp_tmp1>0) {color(" "prp_val[p,f]" ","blue_cyan0","","bold")}
											if(prp_tmp1<100 && prp_tmp1>=1) {color(" "prp_val[p,f]" ","blue_cyan1","","bold")}
											if(prp_tmp1<1000 && prp_tmp1>=100) {color(" "prp_val[p,f]" ","blue_cyan2","","bold")}	
											if(prp_tmp1<10000 && prp_tmp1>=1000) {color(" "prp_val[p,f]" ","blue_cyan3","","bold")}	
											if(prp_tmp1>100000) {color(" "prp_val[p,f]" ","blue_cyan4","","bold")}
											if(prp_tmp1==0) {color(" "prp_val[p,f]" ","dark_gray","","")}

										}
										else {
											if(p==64) {
												if(prp_matrix[p,f]=="soluble") {color(" S ","black","green_yellow2","bold")}
												if(prp_matrix[p,f]=="poorly_soluble") {color(" S ","black","yellow","bold")}
												if(prp_matrix[p,f]=="almost_insoluble") {color(" S ","black","orange","bold")}
												if(prp_matrix[p,f]=="insoluble") {color(" S ","black","red","bold")}
											}
											else {
												if(p==8 || p==9 || p==10) {if ((prp_matrix[p,f]~/Free/ && (p==8 || p==9 )) \
														|| ((prp_matrix[p,f]=="(Free);;PTM;PTM" || prp_matrix[p,f]=="(Free);;PTM;PTM;PTM") && p==10)) {
													if(prp_matrix[p,f]=="(Free);" || p==10) {color(" "0" ","dark_gray","","")} else {color(" "0" ","gray","","")}}
												else {color(" "prp_val[p,f]" ","green","","bold")} 
												}
												else {color(" "prp_val[p,f]" ","green","","bold")}
											}
										}
									}
								} 
								else{color(" "prp_val[p,f]" ","red","","bold")}
							} 
							else {color(" "prp_val[p,f]" ","dark_gray","","");}
						}; 
#	printf fam_names[families_to_analyse,f]
					}
#printf short_prp[f]}
				print ""
					print""	
					if(comparison_option=="sons") {printf "" > "OUTPUTS/"calculation_label"/.seq_ordered_"families_to_analyse}
					p=0;while(p<n_prop) {p=p+1
						delete string_f
							f=0; while(f<fam_size[families_to_analyse]) {f=f+1 ;
								string_f[f]=string[p,f]
							}
						if(fam_size[families_to_analyse]>=1) {
							color(" "prp_name[p]": (data available for the "prp_glob[p]/fam_size[families_to_analyse]*100" %% of members)","blue_cyan1","","bold"); print ""
							if(prp_glob[p]/fam_size[families_to_analyse]*100!=0) {
							if(prp_name[p]!="ID" && prp_name[p]!="sequence" && prp_name[p]!="DSSP" && prp_name[p]!="pdb" && prp_name[p]!="experim_structure") {
								printf("      "); color("summary:","blue_cyan3","","bold"); print "" ;
								# We need lower (1 instead of 3) length limit for certain fields
								if(prp_name[p]=="stereo" || prp_name[p]~/activity/){
									printf "      ";sort_text_by_occurence(string_f,"",5,1,"average_on_lines",170,6,"pale_violet","",""); print "";
									printf "      "; sort_text_by_occurence(string_f,"-_",2,1,"average_on_lines",170,6,"blue_cyan4","",""); print "";
								}
								else {
									printf "      ";sort_text_by_occurence(string_f,"",5,3,"average_on_lines",170,6,"pale_violet","",""); print "";
									printf "      "; sort_text_by_occurence(string_f,"-_",2,3,"average_on_lines",170,6,"blue_cyan4","",""); print "";
								}
							}
							f=0; while(f<fam_size[families_to_analyse]) {f=f+1 ; 
								#if(string[p,f]!="") {
								printf("%4d. ",f); print_newline(string[p,f],170,6,"light_gray","","");print ""
								if(comparison_option=="sons" && prp_name[p]=="sequence") {
									print string[p,f] >> "OUTPUTS/"calculation_label"/.seq_ordered_"families_to_analyse
									}
								#}
							}}; print "___"
						}
					}
					if(comparison_option=="sons") {close("OUTPUTS/"calculation_label"/.seq_ordered_"families_to_analyse)}

					dmax=fam_size[families_to_analyse]
					best_align_ref=father_index[families_to_analyse]

					delete count_aa_type_number_per_position
					delete count_average_aa_type_number_per_region
					delete count_average_correl_per_region
					delete count_correl
					delete count_correl_tot
					delete count_correl_pos
					delete count_correl_pos_tot
					delete count_aa_presence_per_peptide
					delete count_average_aa_presence_per_peptide
					delete count_average_aa_type_number_per_peptide
					delete perc
					delete total_percent
					delete count_aa
					delete max_score
					delete count_aa_type_number_per_peptide
					split(seq[best_align_ref],reference,"");shift[d]=0
					len_ref=length(reference)
}
if(calculate_only_global_properties=="y") {pmax=n_regions}	

d=0;while(d<dmax) {d=d+1
	split(seq[d],p_seq,"")
		a=0; while(a<20) {a=a+1; 
			p=0; while(p<pmax) {p=p+1;  
				if(p_seq[p]==aa_name[a] && p_seq[p]!="-") {count_aa[a,d,p]=count_aa[a,d,p]+1  ; 
					aa=0; while(aa<20) {aa=aa+1; 
						pp=p; while(pp<pmax) {pp=pp+1; 
							if(p_seq[pp]==aa_name[aa] && p_seq[pp]!="-") {
								count_correl[a,aa,pp-p]=count_correl[a,aa,pp-p]+1; count_correl_pos[p,a,aa,pp-p]=count_correl_pos[p,a,aa,pp-p]+1;
								count_correl_tot[a,pp-p]=count_correl_tot[a,pp-p]+1; count_correl_pos_tot[p,a,pp-p]=count_correl_pos_tot[p,a,pp-p]+1;
								count_correl[aa,a,p-pp]=count_correl[a,aa,pp-p];count_correl_pos[pp,aa,a,p-pp]=count_correl_pos[p,a,aa,pp-p]
								count_correl_tot[aa,p-pp]=count_correl_tot[aa,p-pp]+1;;count_correl_pos_tot[pp,aa,p-pp]=count_correl_pos_tot[pp,aa,p-pp]+1	
							}
						}
					}
				}
			}
		}
	print ">" >> "OUTPUTS/"calculation_label"/"calculation_label"_FASTA_OUTPUT_cut_f"families_to_analyse
	split(prop_f[d],fff," ")
	print seq[d] >> "OUTPUTS/"calculation_label"/"calculation_label"_FASTA_OUTPUT_cut_f"families_to_analyse
}
close("OUTPUTS/"calculation_label"/"calculation_label"_FASTA_OUTPUT_cut_f"families_to_analyse)	

#		title("blue","_",80)
#		print "AMINOACID STATISTICS"
#		title("blue","_",80)
delete count_aa_type_number_per_position
a=0 ; while(a<20) {a=a+1
	d=0 ; while(d<dmax) {d=d+1
		p=0 ; while(p<pmax) {p=p+1
				count_aa_type_number_per_peptide[a,d]=count_aa_type_number_per_peptide[a,d]+count_aa[a,d,p]
				count_aa_type_number_per_position[a,p]=count_aa_type_number_per_position[a,p]+count_aa[a,d,p]
				if(count_aa_type_number_per_peptide[a,d]==1) {count_aa_presence_per_peptide[a]=count_aa_presence_per_peptide[a]+count_aa[a,d,p]}
				#count_aa_presence_per_peptide[a]=count_aa_presence_per_peptide[a]+count_aa[a,d,p]
		}
		count_average_aa_type_number_per_peptide[a]=count_average_aa_type_number_per_peptide[a]+count_aa_type_number_per_peptide[a,d]
	}
	if (dmax==0) { print "No peptide found with specified properties" ; a=20} else {count_average_aa_presence_per_peptide[a]=count_aa_presence_per_peptide[a]/dmax*100
		count_average_aa_type_number_per_peptide[a]=count_average_aa_type_number_per_peptide[a]/dmax
#				print "Average number per peptide="count_average_aa_per_peptide[a]
#				print "Average presence per peptide="count_average_aa_presence_per_peptide[a]
#				print ""
	}
}

class_number=4
if(plot_graphs=="y") {print "" > "tmp_files/tmp0_"calculation_label"_"fam_count}
if(plot_graphs=="y") {print "CLASS DEFINITIONS" >> "tmp_files/tmp0_"calculation_label"_"fam_count}
c=0 ; while(c<class_number) {c=c+1
	cc=0 ; while(cc<class_length[c]) {cc=cc+1
		if(plot_graphs=="y") {printf class[c,cc]": "class_def[c,cc]" ; " >> "tmp_files/tmp0_"calculation_label"_"fam_count}
	}
	if(plot_graphs=="y") {print "" >> "tmp_files/tmp0_"calculation_label"_"fam_count}
}

if(plot_graphs=="y") {print "Peptide_length Number_of_peptides Peptide_Length_Population" > "tmp_files/tmp_1_1_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Residue_Type Essentiality(%) Average_presence_of_the_aminoacid_in_the_family" > "tmp_files/tmp_1_2_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Class_type Essentiality(%) Average_presence_of_the_aminoacid_class_in_the_family" > "tmp_files/tmp_1_3_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Class_type Essentiality(%) Average_presence_of_the_aminoacid_in_the_family" > "tmp_files/tmp_1_4_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Class_type Essentiality(%) Average_presence_of_the_aminoacid_in_the_family" > "tmp_files/tmp_1_5_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Class_type Essentiality(%) Average_presence_of_the_aminoacid_in_the_family" > "tmp_files/tmp_1_6_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Residue_Type Number_of_aminoacid_type Average_number_of_aminoacid_types_per_peptide" > "tmp_files/tmp_1_7_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Class_type Number_of_aminoacid_type Average_number_of_aminoacid_class_per_peptide" > "tmp_files/tmp_1_8_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Class_type Number_of_aminoacid_type Average_number_of_aminoacid_class_per_peptide" > "tmp_files/tmp_1_9_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Class_type Number_of_aminoacid_type Average_number_of_aminoacid_class_per_peptide" > "tmp_files/tmp_1_10_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Class_type Number_of_aminoacid_type Average_number_of_aminoacid_class_per_peptide" > "tmp_files/tmp_1_11_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "Residue_Type Occurence(%) Occurence_of_aminoacid_type_per_region/position 3d" > "tmp_files/tmp_2_1_"fam_count"_"calculation_label}
ind=1
if(verbose=="y") {
	title("blue","-",80)
	print "LENGTH POPULATION"
	title("blue","-",80)
}
l=0 ; while(l<max_len) {l=l+1
	if(population[l]=="") {population[l]=0}
	if(verbose=="y") {
		printf l":"
			s=0 ; while(s<population[l]) {s=s+1 ; printf color("*","red")}; print ""
	}
	if(plot_graphs=="y") {print l,population[l],l >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
}
if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
ind=ind+1
if(verbose=="y") {
	title("blue","-",80)
	print "AVERAGE PRESENCE OF AA TYPE PER PEPTIDE"
	title("blue","-",80)
}
a=0 ; while(a<20) {a=a+1
	if(verbose=="y") {
		printf aa_name[a]":"
			s=0 ; while(s<int(count_average_aa_presence_per_peptide[a])) {s=s+1 ; printf color("*","red")}; print ""
	}
	if(plot_graphs=="y") {print a,count_average_aa_presence_per_peptide[a],aa_name[a] >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
}
if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
if(verbose=="y") { 
		title("blue","_",80)
		print "AVERAGE PRESENCE OF AA CLASS PER PEPTIDE"
		title("blue","_",80)             
}
delete count_class
c=0 ; while(c<class_number) {c=c+1	
	ind=ind+1
		cc=0 ; while(cc<class_length[c]) {cc=cc+1
			if(verbose=="y") {printf class[c,cc]":"} ; split(class_def[c,cc],comp,"")
				ccc=0; while(ccc<length(class_def[c,cc])) {ccc=ccc+1 ;
					count_class[c,cc]=count_class[c,cc]+count_average_aa_presence_per_peptide[conv_aa[comp[ccc]]]/length(class_def[c,cc])
				}
			if(verbose=="y") {
				s=0 ; while(s<int(count_class[c,cc])) {s=s+1 ; 
					if(c==1) {printf color("*","red")} ; 
					if(c==2) {printf color("*","green")} ; 
					if(c==3) {printf color("*","blue")} ; 
					if(c==4) {printf color("*","magenta")} ;}; 
				print ""
			}
			if(plot_graphs=="y") {print cc,count_class[c,cc],class[c,cc] >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
		}
	if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
}
if(verbose=="y") {
	title("blue","-",80)
	print "AVERAGE NUMBER OF AA TYPE PER PEPTIDE"
	title("blue","-",80)
}
ind=ind+1
a=0 ; while(a<20) {a=a+1
	if(verbose=="y") {
		printf aa_name[a]":"
			s=0 ; while(s<int(count_average_aa_type_number_per_peptide[a])) {s=s+1 ; printf color("*","red")}; print ""
	}
	if(plot_graphs=="y") {print a,count_average_aa_type_number_per_peptide[a],aa_name[a] >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
}
if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
if(verbose=="y") {
		title("blue","_",80)
		print "AVERAGE NUMBER OF AA CLASS PER PEPTIDE"
		title("blue","_",80)       
}     

delete count_class 
c=0 ; while(c<class_number) {c=c+1
	ind=ind+1
		cc=0 ; while(cc<class_length[c]) {cc=cc+1
			if(verbose=="y") {printf class[c,cc]":" ;}
			split(class_def[c,cc],comp,"")
				ccc=0; while(ccc<length(class_def[c,cc])) {ccc=ccc+1 ;
					count_class[c,cc]=count_class[c,cc]+count_average_aa_type_number_per_peptide[conv_aa[comp[ccc]]]
				}
			if(verbose=="y") {
				s=0 ; while(s<int(count_class[c,cc])) {s=s+1 ;
					if(c==1) {printf color("*","red")} ;
					if(c==2) {printf color("*","green")} ;
					if(c==3) {printf color("*","blue")} ;
					if(c==4) {printf color("*","magenta")} ;};
				print ""
			}
			if(plot_graphs=="y") {print cc,count_class[c,cc],class[c,cc] >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
		}
	if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
}
if(verbose=="y") {
		title("blue","_",80)
		print "AVERAGE PERCENTAGE OF AA TYPE PER REGION"
		title("blue","-",80)
}

r=0 ; while(r<n_regions) {r=r+1
	limit1=int((r-1)*upper/n_regions+0.5)+1 ; limit2=int(r*upper/n_regions)
		if(verbose=="y") {print "region"r"("limit1"-"limit2"):"}
	a=0 ; while(a<20) {a=a+1
		if(verbose=="y") {
			printf aa_name[a]":"
		}
		p=limit1-1 ; while(p<limit2) {p=p+1
			count_average_aa_type_number_per_region[a,r]=count_average_aa_type_number_per_region[a,r]+count_aa_type_number_per_position[a,p]/dmax
		}
		if(count_average_aa_type_number_per_region[a,r]=="") {count_average_aa_type_number_per_region[a,r]=0}
		if(plot_graphs=="y") {print r,a,count_average_aa_type_number_per_region[a,r],"r:"limit1"-"limit2,aa_name[a] > "tmp_files/tmp_2_1_"fam_count"_"calculation_label}
		if(verbose=="y") {
			s=0 ; while(s<int(count_average_aa_type_number_per_region[a,r]*10)) {s=s+1 ; printf color(aa_name[a],"red")}; print ""
		}
	}
}

if(plot_graphs=="y") {close("tmp_files/tmp_2_1_"fam_count"_"calculation_label)}
delete count_average_aa_type_number_per_region
a=0 ; while(a<20) {a=a+1 
	if(plot_graphs=="y") {print "Region/Position Occurence_aminoacid(%) "aa_name[a] > "tmp_files/tmp_3_"a"_"fam_count"_"calculation_label}
	aa=0 ; while(aa<20) {aa=aa+1
		if(plot_graphs=="y") {print "Distance_in_aminoacid_position/regions Occurence(%) "aa_name[a] > "tmp_files/tmp_4_"a"_"aa"_"fam_count"_"calculation_label}
	}
	r=0 ; while(r<n_regions) {r=r+1
		limit1=int((r-1)*upper/n_regions+0.5)+1 ; limit2=int(r*upper/n_regions+0.5)
			p=limit1-1 ; while(p<limit2) {p=p+1
				count_average_aa_type_number_per_region[a,r]=count_average_aa_type_number_per_region[a,r]+count_aa_type_number_per_position[a,p]/dmax
					aa=0 ; while(aa<20) {aa=aa+1
						if(count_correl_tot[a,r]!="" && count_correl_tot[a,r]!=0) {
						count_average_correl_per_region[a,aa,r]=count_average_correl_per_region[a,aa,r]+count_correl[a,aa,r]/count_correl_tot[a,r]
						}
					#	count_average_correl_per_region[a,aa,r]=count_average_correl_per_region[a,aa,r]+count_correl[a,aa,r]/dmax
					}
			}
		if(plot_graphs=="y") {print r,count_average_aa_type_number_per_region[a,r]*100,limit1":"limit2 >> "tmp_files/tmp_3_"a"_"fam_count"_"calculation_label}
		aa=0 ; while(aa<20) {aa=aa+1
			if(plot_graphs=="y") {print r,count_average_correl_per_region[a,aa,r]*100,limit1":"limit2 >> "tmp_files/tmp_4_"a"_"aa"_"fam_count"_"calculation_label}
		}
	}
	if(plot_graphs=="y") {	close("tmp_files/tmp_3_"a"_"fam_count"_"calculation_label)}
	aa=0 ; while(aa<20) {aa=aa+1
		if(plot_graphs=="y") {	close("tmp_files/tmp_4_"a"_"aa"_"fam_count"_"calculation_label)}
	}
}
title("blue","-",80)
if(plot_graphs=="y") {print "Family "fam_count" "calculation_label >> "tmp_files/tmp0_"calculation_label"_"fam_count }
if ((fam_size[families_to_analyse]>1 && design_family_representative_peptide=="y") || calculate_only_global_properties=="y") {
	print "SUMMARY PER REGION (in order of probability)"
		title("blue","-",80)
		r=0 ; while(r<n_regions) {r=r+1
			delete conv_index
				sort_nD(count_average_aa_type_number_per_region,conv_index,"@>","no","v",r,"","","","",1,20)
				printf ("%3d: ",r)
				ar=0 ; while(ar<20) {ar=ar+1
					perc[ar,r]=count_average_aa_type_number_per_region[conv_index[ar],r]*100
						if (perc[ar,r]>10) { printf ("%1s (%3.1f%) ",aa_name[conv_index[ar]],perc[ar,r]) }
				}
			if(plot_graphs=="y") {
				aa=0 ; while(aa<20) {aa=aa+1
					print "distance_in_aminoacid_positions Occurence(%) position_"r"("aa_name[conv_index[1]]")" > "tmp_files2/tmp_5_"r"_"aa"_"fam_count"_"calculation_label
						pp=0; while(pp<n_regions) {pp=pp+1

							if(count_correl_pos_tot[r,conv_index[1],pp]!="" && count_correl_pos_tot[r,conv_index[1],pp]!=0) {
							print pp,count_correl_pos[r,conv_index[1],aa,pp]*(count_correl_pos_tot[r,conv_index[1],pp])**(-1)*100,pp >> "tmp_files2/tmp_5_"r"_"aa"_"fam_count"_"calculation_label
							}
						}
					close("tmp_files2/tmp_5_"r"_"aa"_"fam_count"_"calculation_label)
				}
			}
			print ""
		}
	max_percent[fam_count]=0
	if(plot_graphs=="y") {printf "" > "tmp_files/tmp00_"fam_count"_"calculation_label}
	title("blue","-",80)
		# FIXME: This is really slow when the peptide is really long
		print "SUMMARY PER POSITION WITHIN THE PEPTIDE (in order of propability and coloured by polarity)"
		title("blue","-",80)
		r=0 ; while(r<n_regions) {r=r+1
			if(plot_graphs=="y") {print "Sequence_of_the_scaffold_n."r" Occurence(%) Global_probability_for_scaffold_n."r > "tmp_files2/tmp_6_"r"_"fam_count"_"calculation_label}
			printf ("%3d: ",r) ; if(plot_graphs=="y") {printf ("%3d: ",r) >> "tmp_files/tmp00_"fam_count"_"calculation_label}

			delete conv_index
				sort_nD(count_average_aa_type_number_per_region,conv_index,"@>","no","v",r,"","","","",1,20)
				if(plot_graphs=="y") {printf aa_name[conv_index[1]] >> "tmp_files/tmp0_"calculation_label"_"fam_count}

			pp=0; while(pp<n_regions) {pp=pp+1
				delete conv_index_2
				delete conv_index_3
					if (pp==r) {
#printf ("%1s",aa_name[conv_index[1]]) ;
						best_seq[fam_count,r]=best_seq[fam_count,r]""aa_name[conv_index[1]] 
						if(count_average_aa_type_number_per_region[conv_index[1],r]==0) {printf "-"} else {color(aa_name[conv_index[1]],color_type[polarity[aa_name[conv_index[1]]]],"","bold");}
						if(plot_graphs=="y") {printf aa_name[conv_index[1]] >> "tmp_files/tmp00_"fam_count"_"calculation_label} ; percent=100
						if(plot_graphs=="y") {print pp,percent,aa_name[conv_index[1]] > "tmp_files2/tmp_6_"r"_"fam_count"_"calculation_label}
					}
					else {
						if(pp>r || pp<r) {sort_nD(count_correl_pos,conv_index_2,"@>","no",r,conv_index[1],"v",pp-r,"","","","",1,n_regions,1,20,1,20);
					#		if(count_correl_pos_tot[r,conv_index[1],pp-r]!="" && count_correl_pos_tot[r,conv_index[1],pp-r]!=0)  {
					#		percent=count_correl_pos[r,conv_index[1],conv_index_2[1],pp-r]/count_correl_pos_tot[r,conv_index[1],pp-r]*100
					#		}
							percent=count_correl_pos[r,conv_index[1],conv_index_2[1],pp-r]/dmax*100
							}
						if(percent >0) {if(plot_graphs=="y") {print pp,percent,aa_name[conv_index_2[1]] > "tmp_files2/tmp_6_"r"_"fam_count"_"calculation_label ;} 
							total_percent[fam_count,r]=total_percent[fam_count,r]+percent
								best_seq[fam_count,r]=best_seq[fam_count,r]""aa_name[conv_index_2[1]]
						}
						if(percent >10) {
#printf ("%1s",aa_name[conv_index_2[1]]); 
							color(aa_name[conv_index_2[1]],color_type[polarity[aa_name[conv_index_2[1]]]],"","bold")
								if(plot_graphs=="y") {printf ("%1s",aa_name[conv_index_2[1]]) >> "tmp_files/tmp00_"fam_count"_"calculation_label}
								}
						else {printf "-"; if(plot_graphs=="y") {printf "-" >> "tmp_files/tmp00_"fam_count"_"calculation_label}}
					}
#	if(pp<r) {sort_nD(count_correl_pos,conv_index_3,"@>","no",r,conv_index[1],"v",r-pp,"","","","",1,n_regions,1,20,1,20);printf ("%1s",aa_name[conv_index_3[1]])}
			}
			print ""
				if(plot_graphs=="y") {print "" >> "tmp_files/tmp00_"fam_count"_"calculation_label}
			if(plot_graphs=="y") {close("tmp_files2/tmp_6_"r"_"fam_count"_"calculation_label)}
			if(total_percent[fam_count,r] > max_percent[fam_count]) {max_percent[fam_count]=total_percent[fam_count,r]; best_index[fam_count]=r }
		}
	label_scalar=""
	dssp=""
		d=0;while(d<dmax) {d=d+1 
			if (best_seq[fam_count,best_index[fam_count]]==original_seq[d]) {label_scalar=IDs[d]; dssp=DSSP_[d]}
		}
	if (label_scalar=="") {label_scalar="New sequence"}
	print ""
		print "Best representing sequence is sequence: "best_seq[fam_count,best_index[fam_count]],"("label_scalar")"
		print "" 
		print "Secondary structure prediction"
	        color("Alpha-helix ; ","red","","bold") ;
	        color("Beta-strand; ","blue","","bold") ;
	        color("Turn ; ","green","","bold") ;
	        print ""
	        print ""
		pr=ChouFasman(best_seq[fam_count,best_index[fam_count]],"tail","","")
		print ""

		if(dssp!="_" && dssp!="" && dssp!="X"){
			print "DSSP prediction"
		        color("Alpha-helix ; ","red_yellow1","","bold") ;
		        color("Alpha-310 ; ","red_yellow2","","bold") ;
	       		color("Alpha-pi ; ","red_yellow3","","bold") ;
		        color("Beta-bridge; ","blue_cyan1","","bold") ;
		        color("Beta-strand; ","blue_cyan2","","bold") ;
		        color("Turn ; ","green_yellow1","","bold") ;
		        color("Bend ; ","green_yellow2","","bold") ;
		        print ""
		        print ""
			pr=ChouFasman(best_seq[fam_count,best_index[fam_count]],"notail","",dssp)
			print ""
			title("blue","-",80)
		}
} 
# When family has only one member, the best representing is the only one: fathers[families_to_analyse,conv_index2[1]]
else {
	if (label_scalar=="") {label_scalar="New sequence"}
	print "Best representing sequence is sequence: "fathers[families_to_analyse,conv_index2[1]],"("label_scalar")"
	print ""
	print "Secondary structure prediction"
        color("Alpha-helix ; ","red","","bold") ;
        color("Beta-strand; ","blue","","bold") ;
        color("Turn ; ","green","","bold") ;
        print ""
	pr=ChouFasman(fathers[families_to_analyse,conv_index2[1]],"tail","","")
	print ""

	if(DSSP_[1]!="_" && DSSP_[1]!="" && DSSP_[1]!="X"){
		print "DSSP prediction"
	        color("Alpha-helix ; ","red_yellow1","","bold") ;
	        color("Alpha-310 ; ","red_yellow2","","bold") ;
	        color("Alpha-pi ; ","red_yellow3","","bold") ;
	        color("Beta-bridge; ","blue_cyan1","","bold") ;
	        color("Beta-strand; ","blue_cyan2","","bold") ;
	        color("Turn ; ","green_yellow1","","bold") ;
	        color("Bend ; ","green_yellow2","","bold") ;
	        print ""
		pr=ChouFasman(fathers[families_to_analyse,conv_index2[1]],"notail","",DSSP_[1])
		print ""
		title("blue","-",80)
	}
}

if(plot_graphs=="y") {close("tmp_files/tmp0_"calculation_label"_"fam_count)}
if(plot_graphs=="y") {close("tmp_files/tmp00_"fam_count"_"calculation_label)}
print "End of analysis for family "fam_count""

if(plot_graphs=="y") {
		system("mkdir -p OUTPUTS/"calculation_label"/Graphics")
		printf calculation_label  > "tmp_files/tmp0_"calculation_label""
}
}
