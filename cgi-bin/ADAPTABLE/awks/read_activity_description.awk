function read_activity_description(all_sentences,keyword,sequence,all_organisms_,negative_keyword,negative_keyword2) {
	phrase=""; n=0
	if(negative_keyword=="") {negative_keyword="dontincludeme"}
	if(negative_keyword2=="") {negative_keyword2="dontincludeme"}
        # Some scripts parsing web pages could forget to replace " " by "_", ensure they are replaced
        gsub(" ","_",all_sentences)

                        gsub("<li>","",all_sentences)
                        gsub("</li>","",all_sentences)
                        gsub("<b>","",all_sentences)
                        gsub("</b>","",all_sentences)

	# the sentences are separated by full stops but we need to avoid separation by full stops in numbers or cases like E.coli
	# for this the last word must be larger than 2 characters. In cases where we have =0.2 this would not be sufficient. We need to add a space.
#        all_sentences="Insecticidal_and_antimicrobial_peptide._Has_insecticidal_activity_against_larvae_of_flesh_fly_S.carnaria._Has_antibacterial_activity_against_Gram-positive_bacterium_B.subtilis_B-501_(MIC=1.25_µM)_and_Gram-negative_bacterium_E.coli_DH5alpha_(MIC=2.5_µM)."
	gsub("=","_=_",all_sentences)
	gsub("<","_<_",all_sentences)
	gsub(">","_>_",all_sentences)
	all_sentences=all_sentences"."
	zmax=split(all_sentences,zz,".") ; 
	z=0; while(z<zmax) {
		z=z+1
		bbmax=split(zz[z],bb,"_")
		jjmax=split(zz[z+1],jj,"_")
		if(length(bb[bbmax])==1 || length(bb[bbmax])==2 || substr(bb[bbmax],length(bb[bbmax])-1,1)=="," || (substr(bb[bbmax],length(bb[bbmax])-1,1)~/[0-9]/ && substr(jj[1],1,1)~/[0-9]/)) {phrase=phrase""zz[z]"."} else {n=n+1; sentence[n]=phrase""zz[z]"."; phrase=""}
	}
	nmax=n;
string=""
	b=0; while(b<nmax) {b=b+1
		if(sentence[b]~keyword) {
#		print "sentence",sentence[b]
			# we remove all part of the sentence preceeding the keyword
			temp=""
			amax=split(sentence[b],aa,keyword)
			a=1; while(a<amax) {a=a+1
				temp=temp""aa[a]
			}
			sentence[b]=temp
			name=""
			activity=""
			gsub("Â ","_",sentence[b])
			gsub("<_/i_>","",sentence[b])
			gsub("__","_",sentence[b])
			gsub("__","_",sentence[b])
			gsub("_and_",",",sentence[b])
			gsub(keyword,"",sentence[b])
			gsub("<BR>","",sentence[b]);gsub("</i>","",sentence[b]);gsub("<i>","",sentence[b]);
			gsub("</b>","",sentence[b]);gsub("<b>","",sentence[b]);

			# Before dropping more keywords from final all_organisms we take advantage of them to try to switch more properties
			read_database_classify_field(sentence[b],",",sequence,cyclic_,synthetic_,antimicrobial_,antibacterial_,\
				antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
				antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
				cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
				tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
			
			# Now we drop shit and continue
		        gsub("neagtive","negative",field)
		        gsub("positve","positive",field)
		        gsub("poaitive","positive",field)

			gsub("the_bacteria_tested_under_the_conditions","",sentence[b])
			gsub("hemolytic_activity","",sentence[b])
			gsub("tested bacteria","",sentence[b])
			gsub("antiparasitic_activity_promastigote_forms_of_","",sentence[b])	
			gsub("_There_is_moderate_antimicrobial_activity_",",",sentence[b])
			gsub("a_broad_spectrum_of_Gram-positive","",sentence[b])
			gsub("a_range_of_Gram-positive","",sentence[b])
			gsub("other_bacteria_like_","",sentence[b])
			gsub("closely_related_bacteria","",sentence[b])
			gsub("Gram-positive strain ","",sentence[b])
                        gsub("Gram-positive_bacterium_","",sentence[b])
                        gsub("Gram-negative_bacterium_","",sentence[b])
			gsub("Gram-positive_bacteria","",sentence[b])
			gsub("Gram-negative_bacteria","",sentence[b])
			gsub("Gram-positive","",sentence[b])
			gsub("Gram-negative","",sentence[b])
			gsub("the_fungus_","",sentence[b])
			gsub("fungus_","",sentence[b])
			gsub("the_fungi_","",sentence[b])
			gsub("Fungi","",sentence[b])
			gsub("fungi","",sentence[b])
                        gsub("the_yeast_","",sentence[b])
                        gsub("the_yeasts_","",sentence[b])
                        gsub("yeast_","",sentence[b])
                        gsub("yeasts_","",sentence[b])
                        gsub("_but_exhibits_low_hemolytic_activity_at_concentrations_up_to_200_ugml","",sentence[b])
                        gsub("moderate_antibacterial_activity_","",sentence[b])
                        gsub("moderately_stimulates_degranulation_from_rat_peritoneal_mast_cells","",sentence[b])
                        gsub("moderate_antibacterial_activity_","",sentence[b])
                        gsub("antifungal_activity_","",sentence[b])
                        gsub("antibacterial_activity_","",sentence[b])
                        gsub("filamentous_fungi_","",sentence[b])
                        gsub("phytopathogenic_bacteria_","",sentence[b])
                        gsub("bacterial_horse_pathogens_Gram-positive_","",sentence[b])
                        gsub("bacteria_including_","",sentence[b])
                        gsub("bacteria_","",sentence[b])
			gsub("protozoa","",sentence[b])
			gsub("l._Not_active_fungi","",sentence[b])
			gsub("all_tested_","",sentence[b])
			gsub("penicillin-resistant_","",sentence[b])
			gsub("clinical_isolates_","",sentence[b])
			gsub("\\+_Ca2\\+_=_Medium_supplemented_with_high_concentration_of_CaCl2","",sentence[b])
			gsub("lysodeikticus","lysodeikticus_",sentence[b])
			gsub("including_parasite_","",sentence[b])
			gsub("Gram\\+_","",sentence[b])
			gsub("Gram-_","",sentence[b])
			gsub("Gram\\+","",sentence[b])
			gsub("_or_",",",sentence[b])
			gsub("_and_",",",sentence[b])
			gsub("Ca2\\+_=_Medium_with_low_content_of_CaCl2","",sentence[b])
			gsub("\\+_Ca2\\+_","",sentence[b])
			gsub("\\+_Ca2\\+","",sentence[b])
			gsub("oral_S._mutans","S._mutans",sentence[b])
			gsub("multidrug-resistant_","",sentence[b])			                        
			gsub("including_","",sentence[b])
			gsub("highly_","",sentence[b])
			gsub("several_pathogens","",sentence[b])
			gsub("FUNGI_such_as_","",sentence[b])
			gsub("FUNGI_","",sentence[b])
			gsub("such_as_","",sentence[b])
			gsub("escecially_","",sentence[b])
			gsub("no_effect_on_the_growth_of_the_tested_Bacillus_megaterium","",sentence[b])
			gsub("V._dahliae_G._zeae_A._nicotianae_F._moniliforme","V._dahliae,G._zeae,A._nicotianae,F._moniliforme",sentence[b])
			gsub("G-_than_G\\+_bacteria","",sentence[b])
			gsub("the_bacterium_","",sentence[b])
			gsub("the_Gram\\+_","",sentence[b])
			gsub("these_bacteria","",sentence[b])
			gsub("multiple_strains","",sentence[b])
			gsub("<_a_href_=_\"https:www","",sentence[b])					
			gsub("Active_against_","",sentence[b])
			gsub("-negative_bacteria","",sentence[b])
			gsub("both_Gram-positive","",sentence[b])
			gsub("EC50_=_concentration_that_is_50%_effective","",sentence[b])
			gsub("collistin-resistant_","",sentence[b])
			gsub("Refer_2473036_","",sentence[b])
			gsub("Refer_3623703_","",sentence[b])
			gsub("G\\+_bacteira_than_G-._<_td_>","",sentence[b])
			gsub("weakly_against_the_bacterium","",sentence[b])
			gsub("negative","",sentence[b])
			gsub("._In_20%_SAB_in_SPB_","",sentence[b])
			gsub("._In_5%_TSB_in_SPB_","",sentence[b])
			gsub("._In_20%_TSB_in_PIL_","",sentence[b])
			gsub("._In_20%_SAB_in_PIL_","",sentence[b])
			gsub("._In_50%_MH_in_SPB_medium_","",sentence[b])
			gsub("as_determined_with_","",sentence[b])
			gsub("against_a_","",sentence[b])
			gsub("<_i_>_","",sentence[b])
			gsub("tested_only_agzainst_","",sentence[b])
			gsub("parasite_","",sentence[b])
			gsub("_~_",",",sentence[b])
			gsub("_>_",",",sentence[b])
			gsub("but_weakly_against_","",sentence[b])
			gsub("but_not_commensal_bacteria","",sentence[b])
			gsub("Active_against_","",sentence[b])
			gsub("-negative_bacteria","",sentence[b])
			gsub("an_species","",sentence[b])
			gsub("antiparasitic_activity_","",sentence[b])
			gsub("but_mammalian_sodium_channels","",sentence[b])
			gsub("some_","",sentence[b])
			gsub("Inhibitory_activity_tested_with_supernatant_adjusted_to_pH_7","",sentence[b])
			gsub("a_broad_range_of_","",sentence[b])
			gsub("other_lactobacilli","Lactobacillus",sentence[b])
			gsub("_=_","=",sentence[b])						
			gsub("Gram-positive_","",sentence[b])
			gsub("Gram-poaitive_","",sentence[b])
			gsub("of_Acinetobacter","Acinetobacter",sentence[b])			
			gsub("of_Acetinobacter","Acinetobacter",sentence[b])			
			gsub("all_Carnobacterium","Carnobacterium",sentence[b])
			gsub("Symbols_\\+","",sentence[b])
			gsub("sensitive_to_mundticin_KS","",sentence[b])
			gsub("Many_strains_of_","",sentence[b])
			gsub("Synthetic_low_ionic_strength_growth_medium","",sentence[b])
			gsub("Medium_with_10_mM_phosphate_buffer","",sentence[b])
			gsub("species_of_","",sentence[b])
			gsub("_species","",sentence[b])			
			gsub("Extended-spectrum_ÃŸ-lactamases_","",sentence[b])
			gsub("Gram-negative_bacterium_","",sentence[b])
			gsub("Gram-neagtive_bacterium_","",sentence[b])
			gsub("Because_of_the_low_amount_of_the_purified_peptide","",sentence[b])
			gsub("this_minimal_concentration_activity_is_only_tested_on_","",sentence[b])
			gsub("various_isolates_of_","",sentence[b])
			gsub("0._1_molL_sodium_chloride","",sentence[b])
			gsub("And_","",sentence[b])
			gsub("MIC=minimal_inhibitory_concentration","",sentence[b])
			gsub("MBC=minimal_bactericidal_concentration","",sentence[b])
			gsub("80_µM_in_L._B","",sentence[b])
			gsub("Synthetic_","",sentence[b])
			gsub("Washed_with_a_phosphate_buffered_saline","",sentence[b])
			gsub("Fungus_","",sentence[b])
			gsub("At_high_physiological_salt_concentrations_","",sentence[b])
			gsub("mouse-avirulent_","",sentence[b])
			gsub("Dimerized_peptide_was_two-_to_fourfold_more_potent_than_monomeric_peptide","",sentence[b])
			gsub("MIC_is_defied_as_the_lowest_peptide_concentration_that_gives_no_visible_growth_after_overnight_incubation_in_100%_Muller-Hinton_broth","",sentence[b])
			gsub("vLD50","",sentence[b])
			gsub("virtual_lethal_doses","",sentence[b])
			gsub("Multidrug-resistant_","",sentence[b])
			gsub("Diameter_of_clear_zone_from_disc_diffusion_assay","",sentence[b])
			gsub("mutant_S._enterica","Salmonella_enterica",sentence[b])			
			gsub("Yeast_","",sentence[b])
			gsub("._against_","",sentence[b])

			gsub(/^_/,"",sentence[b])
			gsub(";",",",sentence[b]);
			delete cc
			ccmax=split(sentence[b],cc,",")
			ccc=0;while(ccc<ccmax) {ccc=ccc+1
			if(cc[ccc]!~negative_keyword && cc[ccc]!~negative_keyword2) { 
				string2=""
				activity=""
				string_to_compare=""
				delete bb
				length_string=split(cc[ccc],bb,"")
				if(cc[ccc]!~/\)/) {cc[ccc]=cc[ccc]"_()"}
				if(cc[ccc]~/\(/) {split(cc[ccc],aa,"("); name=aa[1]; split(aa[2],bb,")"); gsub("_","",bb[1]); activity=bb[1]}
				   cc[ccc]=clean_string(cc[ccc])
				   name=clean_string(name)

				   string2=name"("activity")"
				   string2=analyze_organism(string2,sequence)
				   # We need to build a string to compare escaping the parenthesis, while we still add the original 'non-escaped' version to all_organisms_[sequence]
				   string_to_compare=escape_pattern(string2)

				   # We compare and do our magic
                                   if(all_organisms_[sequence]!~string_to_compare) {if(string2!="") {all_organisms_[sequence]=all_organisms_[sequence]""string2";"}}
				   string=string""string2";"
				}
			   }
		   }

	   }
	   return string
   }
