# Don't enable IGNORECASE here, it's used for D/L/Mix classification
function analyze_sequence(sequence) {

	# Take care of really difficult sequences
	sequence=correct_sequence(sequence)

        # Modified aa
        read_modified_aa(modif)

	z=0; while(z<length(modif)) {z=z+1
	gsub(modif[z],translate_modif[modif[z]],sequence)
	gsub("<td>","",sequence)
	gsub("<tr>","",sequence)
	gsub("<tbody>","",sequence)
	
	if(PTM_[sequence]!~modif[z] && sequence~modif[z]) {PTM_[sequence]=PTM_[sequence]"contains_"modif[z]";"}
	}


                                gsub("Ala","A",sequence)
                                gsub("Cys","C",sequence)
                                gsub("Asp","D",sequence)
                                gsub("Glu","E",sequence)
                                gsub("Phe","F",sequence)
                                gsub("Gly","G",sequence)
                                gsub("His","H",sequence)
                                gsub("Ile","I",sequence)
                                gsub("Lys","K",sequence)
                                gsub("Leu","L",sequence)
                                gsub("Met","M",sequence)
                                gsub("Asn","N",sequence)
                                gsub("Pro","P",sequence)
                                gsub("Gln","Q",sequence)
                                gsub("Arg","R",sequence)
                                gsub("Ser","S",sequence)
                                gsub("Thr","T",sequence)
                                gsub("Val","V",sequence)
                                gsub("Trp","W",sequence)
                                gsub("Tyr","Y",sequence)

                                gsub("tryptophanyl","W",sequence)
                                gsub("isoleucinyl","I",sequence)
				gsub("isoleucyl","I",sequence)
				gsub("leucyl","L",sequence)
				gsub("leucinyl","L",sequence)
				gsub("phenylalanyl","P",sequence)
				gsub("histidyl","H",sequence)
				gsub("glycyl","G",sequence)
				gsub("valyl","V",sequence)
				gsub("alanyl","A",sequence)
				gsub("serinyl","S",sequence)
				gsub("threonyl","T",sequence)
	                        gsub("cysteinyl","C",sequence)
                                gsub("glutaminyl","Q",sequence)
                                gsub("glutamyl","E",sequence)
                                gsub("aspartyl","D",sequence)
                                gsub("asparaginyl","N",sequence)
                                gsub("arginyl","R",sequence)
                                gsub("prolyl","P",sequence)
                                gsub("lysyl","K",sequence)
                                gsub("tyrosyl","Y",sequence)
                                gsub("methionyl","M",sequence)

                                gsub("L-A","A",sequence)
                                gsub("L-C","C",sequence)
                                gsub("L-D","D",sequence)
                                gsub("L-E","E",sequence)
                                gsub("L-F","F",sequence)
                                gsub("L-G","G",sequence)
                                gsub("L-H","H",sequence)
                                gsub("L-I","I",sequence)
                                gsub("L-K","K",sequence)
                                gsub("L-L","L",sequence)
                                gsub("L-M","M",sequence)
                                gsub("L-N","N",sequence)
                                gsub("L-P","P",sequence)
                                gsub("L-Q","Q",sequence)
                                gsub("L-R","R",sequence)
                                gsub("L-S","S",sequence)
                                gsub("L-T","T",sequence)
                                gsub("L-V","V",sequence)
                                gsub("L-W","W",sequence)
                                gsub("L-Y","Y",sequence)
                                gsub("L-X","X",sequence)

                                gsub("D-A","a",sequence)
                                gsub("D-C","c",sequence)
                                gsub("D-D","d",sequence)
                                gsub("D-E","e",sequence)
                                gsub("D-F","f",sequence)
                                gsub("D-G","g",sequence)
                                gsub("D-H","h",sequence)
                                gsub("D-I","i",sequence)
                                gsub("D-K","k",sequence)
                                gsub("D-L","l",sequence)
                                gsub("D-M","m",sequence)
                                gsub("D-N","n",sequence)
                                gsub("D-P","p",sequence)
                                gsub("D-Q","q",sequence)
                                gsub("D-R","r",sequence)
                                gsub("D-S","s",sequence)
                                gsub("D-T","t",sequence)
                                gsub("D-U","u",sequence)
                                gsub("D-V","v",sequence)
                                gsub("D-W","w",sequence)
                                gsub("D-Y","y",sequence)
                                gsub("D-X","x",sequence)


				gsub("-","",sequence)
				if(sequence~/[a-z]/ && sequence~/[A-Z]/) {field="Mix"}
                                else {if(sequence~/[a-z]/) {field="D"}
                                        else {if(sequence~/[A-Z]/) {field="L"}}
                                        }
                               # field=read_database_line(input_file,"L\/D\/Mix",0,"black\'>","<")
                                if (stereo_[sequence]!~field){stereo_[sequence]=stereo_[sequence]""field";"}

return sequence
	}
