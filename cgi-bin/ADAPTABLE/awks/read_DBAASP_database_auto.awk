function read_DBAASP_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {
limit=30000
a=1
print "" > "tmp_files/modified_aa_dbaasp"
limit_=a+limit
                while(a<limit_) {
file="DATABASES/DBAASP/DBAASPfiles/DBAASP_"a
                        if (system("[ -f " file " ] ") ==0 ) {

			input_file=file
#seq
				field=read_database_line(input_file,"\"sequence\"",0,"\"","\",",1)
				gsub("_","",field)
				if(field~/quenceLength/) {field=""}
				seq_a[a]=field

                        read_modified_aa(modif,translate_modif)
                        field1=1; 
			b=0; while(field1!="") {b=b+1
				field1=read_database_line(input_file,"\"position\"",0,":",",",b)
                        	field2=read_database_line(input_file,"modificationType",0,":\"","\"",b)
                        	if(field2!="") {
        	                	# Find the legend from https://dbaasp.org/help#collapseSix
					field2_legend_full=""
	                        	command="grep '^"field2" ' DATABASES/DBAASP/DBAASPfiles/help|sed -e 's/"field2"//'"
	                        	command | getline field2_legend_full
	                        	close(command)
					field2_legend_full=clean_string(field2_legend_full)
					# Make modification separated by ',' be separated by ';' without altering other uses of , (like 3,3-..., 4,2...)
					string=""
					split(field2_legend_full,cc,","); w=0; while(w<length(cc)) {w=w+1
					string=string""cc[w]
					if (w<length(cc)) {
						if(substr(cc[w],length(cc[w]),1)!~/^[0-9]+$/ && substr(cc[w],length(cc[w]),1)!="N" && substr(cc[w],length(cc[w]),1)!="S" && substr(cc[w],length(cc[w]),1)!="O" && substr(cc[w],length(cc[w]),1)!="P" ) {string=string";"} else {string=string","}
					}
					}
					field2_legend_full=string
					# Only took first value if there are many separated by ';'
					split(field2_legend_full,bb,";")
					field2_legend=bb[1]
				}

                        	# Ignore case
                        	field2_legend=tolower(field2_legend)

                        	if(translate_modif[field2_legend]=="" && field2_legend!="") {
                        		print "translate_modif[\""field2_legend"\"]=\"X\";" >> "tmp_files/modified_aa_dbaasp"
                        		# Show X for now until we update read_modified_aa
                         		translate_modif[field2_legend]="X"
                         	}                     
                        	# Do the substitution: we replace field1 position (from sequence) with the translate_modif of the LEGEND (field2)
                        	seq_a[a]=modify_sequence_in_position(seq_a[a],field1,translate_modif[field2_legend])
                        }

                        print "Reading "file" for sequence "seq_a[a]""
			seq_a[a]=analyze_sequence(seq_a[a])
#ID
                                field=read_database_line(input_file,"\"id\"",0,":",",",1)
                                field=clean_string(field)
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"DBAASP"field";" }
#name
                                field=read_database_line(input_file,"name",0,":\"","\",",1)
                                field=clean_string(field)
                                gsub("*","",field)
                                gsub("\\\\t","",field)
                                gsub("\"","",field)
                                gsub("\\\\","",field)
                                gsub("PREDICTED:","",field)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"\"source\"",0,":\"","\",",1)
                                field=clean_string(field)
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
#synthetic,ribosomal,non-ribosomal
                                field=read_database_line(input_file,"synthesisType",0,":\"","\"}",1)
                                field=clean_string(field)
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
				if(field=="Ribosomal") {ribosomal_[seq_a[a]]="ribosomal"}
				if(field=="Nonribosomal") {ribosomal_[seq_a[a]]="nonribosomal"}
#gene
                                field=read_database_line(input_file,"geneInSequence",0,":\"","\"",1)
				gsub("#","",field)
				field=clean_string(field)
                                if(gene_[seq_a[a]]!~field) {if(field!="") {gene_[seq_a[a]]=gene_[seq_a[a]]""field";" }}
#stereo
				field=""
				if(seq_a[a]~/[a-z]/ && seq_a[a]~/[A-Z]/) {field="Mix"} 
				else {if(seq_a[a]~/[a-z]/) {field="D"}
					else {if(seq_a[a]~/[A-Z]/) {field="L"}}
					}
                                if (stereo_[seq_a[a]]!~field){stereo_[seq_a[a]]=stereo_[seq_a[a]]""field";"}
#N_terminus
                                field=read_database_line(input_file,"nTerminus",0,":\"","\",",1)
				gsub("#","",field)
				if(field=="ACT") {field="acetylation"}
				if(field=="AMD") {field="amidation"}
				if(field~/complexity/) {field=""}
				if(field~/description/) {field=""}
				field=clean_string(field)
				if(field!~"Free" && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
				gsub("Free","",field)
				if(field~/complexity/) {field=""}
                                if(N_terminus_[seq_a[a]]!~field) {if(field!="") {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
#C_terminus
                                field=read_database_line(input_file,"cTerminus",0,":\"","\",",1)
				gsub("#","",field)
				if(field=="ACT") {field="acetylation"}
				if(field=="AMD") {field="amidation"}
				if(field~/complexity/) {field=""}
				if(field~/description/) {field=""}
				field=clean_string(field)
				if(field!~"Free" && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
				gsub("Free","",field)
				if(field~/complexity/) {field=""}
                                if(C_terminus_[seq_a[a]]!~field) {if(field!="") {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
#PTM
				z=1; field=1; while(field!="") {z=z+1
					field=""
					field=read_database_line(input_file,"\"sequence\"",0,"\"","\",",z)
					field=clean_string(field)
					if(PTM_[seq_a[a]]!~field) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"bound_to_"field";"}
				}
#cyclic                 
                        field=read_database_line(input_file,"modificationType",0,":{\"","}",1)
                        field=clean_string(field)
                        if(field~/description/) {field=""}
                        if(field!~/None/ && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                        if(field~/cetyl/ && field~/N-term/) {if(N_terminus_[seq_a[a]]!~field) {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
                        if(field~/midation/ && field~/C-term/) {if(C_terminus_[seq_a[a]]!~field) {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
                        if(field~/isulfide/) {disulfide_[seq_a[a]]="disulfide";if(PTM_[seq_a[a]]!~field) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"disulfide;"}}
                        if(field~/lantibio/) {if(PTM_[seq_a[a]]!~/lantibio/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lantibiotic;"}}
                        if(field~/lycos/) {if(PTM_[seq_a[a]]!~/lycos/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"Glycosylation;"}}
#target
                        field=read_database_line(input_file,"targetObjects",0,"{\"name\":\"","\"}",1)
                        field=clean_string(field)
                        if(field~/description/) {field=""}
                        if(field~/value/) {field=""}
			if(target_[seq_a[a]]!~field) {if(field!="") {target_[seq_a[a]]=target_[seq_a[a]]""field";"}}
#Function
                        field=read_database_line(input_file,"targetGroups",0,":\\[","\\],",1)
			gsub("\"","",field); gsub("_","",field);amax=split(field,aa,",")
			aaa=0; while(aaa<amax) {aaa=aaa+1
                                gsub("Gram-","gram-neg",aa[aaa])
                                gsub("Gram\\+","gram-pos",aa[aaa])
                                read_database_classify_field(aa[aaa],",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
				}
#Experimental structure/PDB
                        field=read_database_line(input_file,"pdb",0,":\"","\"",1)
                        field=clean_string(field)
                        field=toupper(field)
                        if(field~/BR/) {field=""}
			if(field~/_/) {field=""}
                        if(field!="") {
                        	if(experim_structure_[seq_a[a]]!~field) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}
                        	if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}
			}
#Taxonomy
				field=read_database_line(input_file,"kingdom",0,":\"","\"}",1)
				field=clean_string(field)
				field=tolower(field); 
                                if(taxonomy_[seq_a[a]]!~field) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}}
				field=read_database_line(input_file,"subkingdom",0,":\"","\",",1)
        	                field=clean_string(field)
        	                field=tolower(field)
                                if(taxonomy_[seq_a[a]]!~field) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}}
#all organisms
			field1=1; 
			b=0; while(field1!="") {b=b+1
                           field1=read_database_line(input_file,"targetSpecies",0,":\"","\",",b)
			   field2=read_database_line(input_file,"activityMeasureGroup",0,":\"","\",",b)
			   field3=read_database_line(input_file,"concentration",0,":\"","\",",b)
			   field4=read_database_line(input_file,"unit",0,":\"","\",",b)
			   if(field3!="NA") {string=field1"("field2"="field3""field4")"} else {string=field1}
			   if(field2==""||field2=="_"||field2=="-"||field2~/KIlling/||field2~/Killing/) {string=field1}
			   gsub("\"},\"activityMeasureGroup\":{\"name\"","",string)
			   gsub("\"},\"activityMeasureGroup\":null,\"activityMeasureValue\";","",string)
			   gsub(",\"activityMeasureGroup\":null,\"activityMeasureValue\"","",string)
			   # Drop cruft
			   gsub("\"}","",string)
			   string=analyze_organism(string,seq_a[a])
			   string_to_compare=escape_pattern(string)
			   if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
			}
#Pubmed			
			field=1; b=0; while(field!="") {b=b+1
                        	field=read_database_line(input_file,"pubmedId",0,":\"","\",",b)
                        	gsub("_","",field)
                        	gsub("\"","",field)
                        	if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}
			}

	# https://dbaasp.org
	experimental_[seq_a[a]]="experimental"
	}
       close(input_file)
	a=a+1
file="DATABASES/DBAASP/DBAASPfiles/DBAASP_"a
amax=a
}
return amax
}
