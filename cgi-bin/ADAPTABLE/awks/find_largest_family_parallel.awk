function find_largest_family_parallel(seq_vector,seq_vector_transl,level_agreement,family_index,properties,seq_names,min_seq_length,align_method,comparison_option,\
		d,dd,dmax,f,limit,a,len_seq,len_ref,head,tail,incr,e,le,max_le,q,ddmax,d0)
{
	dmax=length_nD(seq_vector,1)
	d0=0 ; ddmax=dmax
	# the user peptide is compared with the fathers of all families (family_index is all values but dmax, because the user peptide is added in the end of the selection list; in addition the selction file must contain only the fathers)
	#if (comparison_option=="fathers") {d0=0}
	# the user peptide is compared with the all sequences as he was the father (family_index=dmax because the user peptide is added in the end of the selection list)	
	#if (comparison_option=="sons") {ddmax=dmax}

		read_blosum("tmp_files/align_matrix",blosum)

		delete used_sequence
		delete seq_fam
		delete fam
		delete type_seq
		delete shift_seq
		delete min_shift
		delete max_leng
		delete prop_seq
		delete seq_names_fam
		delete seq_fam_transl
		delete reference
		delete p_seq
		printf("%1d, ",family_index)

		d=family_index-1;while(d<family_index) {d=d+1
				printf "" > "tmp_files/fam"d"_"calculation_label
				printf "" > "tmp_files/fam_prop_"d"_"calculation_label
				printf "" > "tmp_files/score"d"_"calculation_label
				printf "" > "tmp_files/sorted_fam"d"_"calculation_label
				printf "" > "tmp_files/sorted_fam_prop_"d"_"calculation_label
				delete max_score_
				split(seq_vector[d],reference,"");shift[d]=0 ; len_ref=length(reference)
				split(seq_vector[d],p_seq,"")
				align_sequences(seq_type,max_score_,"***",d,reference,p_seq,len_ref,align_method,blosum)
				max_possible_score[d]=max_score_[d]; type_seq[d]=seq_type[1]
				min_shift[d]=100
				max_leng[d]=0
				if(len_ref>=min_seq_length) {
					delete max_score
					dd=d0;while(dd<ddmax) {dd=dd+1
						if ((comparison_option=="fathers" && (dd==family_index || dd==dmax)) || comparison_option=="" || comparison_option=="sons" ) {
							len_seq=length(seq_vector[dd]); if(len_seq>len_ref) {align_range[d]=len_seq} else {align_range[d]=len_ref}
						split(seq_vector[dd],p_seq,"")

							if(dd!=d) {
								shift[dd]=align_sequences(seq_type,max_score,"***",dd,reference,p_seq,align_range[d],align_method,blosum);
								#if(level_agreement=="auto") {if(len_ref<14) {limit=max_possible_score[d]/5*3} ; if(len_ref>=14 && len_ref<30) {limit=max_possible_score[d]/5*2} ; if(len_ref>=30) {limit=max_possible_score[d]/5}}
								#else {
								#	limit=max_possible_score[d]/5*level_agreement
								#}
								limit=max_possible_score[d]*level_agreement/100
								if(max_score[dd]>limit && (((len_ref-((shift[dd]**2)**0.5))>=min_seq_length && shift[dd]>=0) || ((len_seq-((shift[dd]**2)**0.5))>=min_seq_length && shift[dd]<0) || level_agreement=="all") && used_sequence[dd]!="yes") {
									if (len_seq>max_leng[d]) {max_leng[d]=len_seq}
									if (shift[dd]<min_shift[d]){min_shift[d]=shift[dd]}
									fam[d]=fam[d]+1;seq_fam[d,fam[d]]=seq_vector[dd];seq_fam_transl[d,fam[d]]=seq_vector_transl[dd];
									type_seq[d]=seq_type[1];prop_seq[d,fam[d]]=properties[dd]
										seq_names_fam[d,fam[d]]=seq_names[dd]
										shift_seq[d,fam[d]]=shift[dd]
										if(shift_seq[d,fam[d]]=="") {shift_seq[d,fam[d]]=0};
										if(seq_aligned[d,fam[d]]=="") {seq_aligned[d,fam[d]]="-"}
									print fam[d],seq_fam[d,fam[d]],seq_fam_transl[d,fam[d]],seq_names_fam[d,fam[d]],shift_seq[d,fam[d]],type_seq[d],min_shift[d],seq_aligned[d,fam[d]],max_score[dd] >> "tmp_files/fam"d"_"calculation_label 
										print prop_seq[d,fam[d]] >> "tmp_files/fam_prop_"d"_"calculation_label
										score_father[d]=score_father[d]+max_score[dd] 
										used_sequence[dd]="yes"
										code[d]=code[d]"_"dd
								}
							}
							else {fam[d]=fam[d]+1;seq_fam[d,fam[d]]=seq_vector[dd];seq_fam_transl[d,fam[d]]=seq_vector_transl[dd];
								seq_aligned[d,fam[d]]=seq_vector[dd];prop_seq[d,fam[d]]=properties[dd];seq_names_fam[d,fam[d]]=seq_names[dd]
									if(shift_seq[d,fam[d]]=="") {shift_seq[d,fam[d]]=0};
									if(seq_aligned[d,fam[d]]=="") {seq_aligned[d,fam[d]]="-"}
									print fam[d],seq_fam[d,fam[d]],seq_fam_transl[d,fam[d]],seq_names_fam[d,fam[d]],shift_seq[d,fam[d]],type_seq[d],min_shift[d],seq_aligned[d,fam[d]],max_possible_score[d],"father" >> "tmp_files/fam"d"_"calculation_label
									print prop_seq[d,fam[d]] >> "tmp_files/fam_prop_"d"_"calculation_label 
									used_sequence[dd]="yes"
									code[d]=code[d]"_"dd
									max_score[dd]=0
							}
					}
					}
				        delete conv_index3
                		sort_nD(max_score,conv_index3,"@>","no","v","","","","","",1,fam[d])
				b=0; while(b<fam[d]) {b=b+1
					bb=conv_index3[b]
					seq_fam[d,b]=seq_vector[bb];seq_fam_transl[d,b]=seq_vector_transl[bb]
					type_seq[d]=seq_type[1];prop_seq[d,b]=properties[bb]
					seq_names_fam[d,b]=seq_names[bb]
					shift_seq[d,b]=shift[bb]
					if(shift_seq[d,b]=="") {shift_seq[d,b]=0}
					print b,seq_fam[d,bb],seq_fam_transl[d,bb],seq_names_fam[d,bb],shift_seq[d,bb],type_seq[d],min_shift[d],seq_aligned[d,bb],max_score[bb] >> "tmp_files/sorted_fam"d"_"calculation_label
					print prop_seq[d,bb] >> "tmp_files/sorted_fam_prop_"d"_"calculation_label
				}
				}
				if(score_father[d]=="") {score_father[d]=0}
				# Handle peptides shorter than 6
				if(code[d]=="") {code[d]="_"}
			print d,seq_vector[d],score_father[d],code[d],type_seq[d] >> "tmp_files/score"d"_"calculation_label
			close("tmp_files/fam"d"_"calculation_label)
			close("tmp_files/fam_prop_"d"_"calculation_label)
			close("tmp_files/score"d"_"calculation_label)
			close("tmp_files/sorted_fam"d"_"calculation_label)
			close("tmp_files/sorted_fam_prop_"d"_"calculation_label)
		}
# We don't really need to generate *code* files because fam_prop also show the equal families
#		d=0;while(d<dmax) {d=d+1
#		print code[d] >> "tmp_files/fam_code"d"_"calculation_label
#		}
}
