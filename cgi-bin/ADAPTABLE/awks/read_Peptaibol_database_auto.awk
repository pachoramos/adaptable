function read_Peptaibol_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {
limit=5000
                        a=0
                        path="DATABASES/Peptaibol/Peptaibolfiles/Peptaibol"
                        file_names="http://peptaibol.cryst.bbk.ac.uk/peptaibol_database_justnames.htm"
                        split(path,aa,"/")
                        system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                        file_list=aa[1]"/"aa[2]"/list"
                        name=1
                        ll=1; while(name!="") {ll=ll+1
                                name=read_database_line(file_list,"singlerecords",0,">","<",ll)
                                        file=path""name
                        gsub("'","",file)
                        if (system("[ -f " file " ] ") ==0 ) {
			a=a+1
			input_file=file
#seq
				field=read_database_line(input_file,"Sequence_:",1,"dontcut","dontcut",1)
				seq_a[a]=field
                                # Take care of really difficult sequences
                                seq_a[a]=correct_sequence(seq_a[a])

                                read_modified_aa(modif,translate_modif)

        conv_OLC_case["Ala"]="A";conv_OLC_case["Val"]="V";conv_OLC_case["Leu"]="L";conv_OLC_case["Ile"]="I";conv_OLC_case["Pro"]="P";
        conv_OLC_case["Phe"]="F";conv_OLC_case["Trp"]="W";conv_OLC_case["Met"]="M";conv_OLC_case["Cys"]="C";conv_OLC_case["Gly"]="G";
        conv_OLC_case["Asn"]="N";conv_OLC_case["Gln"]="Q";conv_OLC_case["Ser"]="S";conv_OLC_case["Thr"]="T";conv_OLC_case["Tyr"]="Y";
        conv_OLC_case["Lys"]="K";conv_OLC_case["Arg"]="R";conv_OLC_case["His"]="H";conv_OLC_case["Asp"]="D";conv_OLC_case["Glu"]="E";
	conv_TLC_["Aib"]=translate_modif["aminoisobutyricacid"];conv_TLC_["EtN"]=translate_modif["ethylnorvaline"];conv_TLC_["EtNor"]=translate_modif["ethylnorvaline"];
	conv_TLC_["Iva"]=translate_modif["isovaline"];conv_TLC_["Hyp"]=translate_modif["hydroxyproline"];
	conv_TLC_["AHMO"]=translate_modif["(2S,4S)-2-amino-6-hydroxy-4-methyl-8-oxodecanoicacid"]

# We need this definitions
#	translate_modif["aminoisobutyricacid"]="X"
#	translate_modif["ethylnorvaline"]="X"
#	translate_modif["isovaline"]="X"
#	translate_modif["hydroxyproline"]="X"
#	translate_modif["(2S,4S)-2-amino-6-hydroxy-4-methyl-8-oxodecanoicacid"]="X"
	
        conv_T_["Ac"]="acetylation"
        conv_T_["Oc"]="octyl"
        conv_T_["Dec"]="decanyl"
        conv_T_["MDA"]="(2R)-methyldecanoicacid"
        conv_T_["Z"]="benzyloxycarbonyl"
        conv_T_["(Z)-4-decenoyl-"]="branched-decanyl"
        conv_T_["OH"]="alcohol"
        conv_T_["OCH3"]="methylation"



sequ=""
seq_a[a]=clean_string(seq_a[a])
nres=split(seq_a[a],aa,"_")
        nter=""
        cter=""
r=0; while(r<nres) {r=r+1
if (r==1 &&    conv_OLC_case[aa[r]]=="" && conv_TLC_[aa[r]]=="") {if(conv_T_[aa[r]]!="") {nter=conv_T_[aa[r]]} else {nter=aa[r]}}
if (r==nres && conv_OLC_case[aa[r]]=="" && conv_TLC_[aa[r]]=="") {if(conv_T_[aa[r]]!="") {cter=conv_T_[aa[r]]} else {cter=aa[r]}} 
	if(aa[r]~/x/) {sequ=sequ"X"} 
	else {
	if(conv_OLC_case[aa[r]]!="") {sequ=sequ""conv_OLC_case[aa[r]]} 
	else {sequ=sequ""conv_TLC_[aa[r]]}
	}
}
seq_a[a]=sequ
if(C_terminus_[seq_a[a]]!~cter) {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""cter";"}
if(N_terminus_[seq_a[a]]!~nter) {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""nter";"}


analyze_sequence(seq_a[a])
print "Reading "file" for sequence "seq_a[a]""

#ID
                                field=name
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"Peptaibol"field";" }
				analyze_sequence(seq_a[a])
#name
                                field=read_database_line(input_file,"UPPER_NAVIGATION_BAR_END",2,"<h1>","</h1>",1)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"Source Mould",1,"dontcut","dontcut",1)
                                field=clean_string(field)
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
#Pubmed			
			
                        b=0;field=1; while(field!="") {b=b+1
			field=read_database_line(input_file,"Reference_:",b,"<LI>",":",1)
                        if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}
		}

		}
	}
pbmax=a
return pbmax
}
