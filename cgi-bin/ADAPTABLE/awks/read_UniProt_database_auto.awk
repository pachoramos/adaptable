function read_UniProt_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,v,qq,si,d) {
	
		queries[1]="antibacterial"
		queries[2]="antifungal"
		queries[3]="antiyeast"
		queries[4]="antiviral"
		queries[5]="antiprotozoal"
		queries[6]="antiparasitic"
		queries[7]="antiplasmodial"
		queries[8]="antitrypanosomic"
		queries[9]="antileishmania"
		queries[10]="insecticidal"
		queries[11]="anticancer"
		queries[12]="antitumor"
		queries[13]="immunomodulant"
		queries[14]="antihypertensive"
		queries[15]="antioxidant"
		queries[16]="antiproliferative"
		queries[17]="hormon"
		queries[18]="antimicrobial"
		queries[19]="antibiofilm"
		queries[20]="lantibiotic"
		queries[21]="bacteriocin"
		queries[22]="cecropin"
		queries[23]="cathelicidin"
		queries[24]="defensin"
		queries[25]="arenicin"
		queries[26]="bombinin"
		queries[27]="clavanin"
		queries[28]="efrapeptin"
		queries[29]="esculentin"
		queries[30]="histatin"
		queries[31]="magainin"
		queries[32]="antimalarial"
		queries[33]="conopeptide"
		queries[34]="\\\"host+defense+peptide\\\""
		queries[35]="\\\"cytolytic+peptide\\\""
		queries[36]="\\\"lytic+peptide\\\""
		queries[37]="\\\"cell+penetrating+peptide\\\""
		queries[38]="\\\"cell+penetrating\\\""
		queries[39]="\\\"mast+cell+degranulating\\\""
		queries[40]="cyanobactin"
		queries[41]="\\\"ionophore+peptide\\\""
		queries[42]="cyclopeptide"
		queries[43]="\\\"antibiotic+peptide\\\""
		queries[44]="fungicide"
		queries[45]="\\\"fungicide+peptide\\\""
		queries[46]="microbicidal"
		queries[47]="bactericide"
		queries[48]="bactericidal"
		queries[49]="fungicidal"
		queries[50]="anticandidal"
		queries[51]="\\\"anti+hiv\\\""
		queries[52]="\\\"viral+inhibitor\\\""
		queries[53]="\\\"anti+hcv\\\""
		queries[54]="\\\"microbe+inhibitor\\\""
		queries[55]="\\\"microbial+inhibitor\\\""
		queries[56]="against"
		queries[57]="anticarcinogenic"
		queries[58]="antileukemic"
		queries[59]="tumoricidal"
		queries[60]="\\\"antiinflammatory+peptide\\\""
		queries[61]="\\\"cysteine+protease+inhibitor\\\""
		queries[62]="\\\"cysteine+proteinase+inhibitor\\\""
		queries[63]="degranulating"
		queries[64]="parasiticidal"
		queries[65]="\\\"scorpion+venom+toxin\\\""
		queries[66]="ic50"
		queries[67]="mbc"
		queries[68]="ec50"
		queries[69]="lc50"
		queries[70]="hd50"
		queries[71]="hemolysin"
		queries[72]="hc50"
		queries[73]="\\\"defense+response+to+bacteria\\\""
		queries[74]="insecticide"
		queries[75]="neuropeptide"
		queries[76]="thiopeptide"
		queries[77]="thiazolyl"
		queries[78]="VRE"
		queries[79]="MRSA"
		queries[80]="MSSA"
		queries[81]="VRSA"
		queries[82]="trypanocidal"
		queries[83]="leishmanicidal"
		queries[84]="\\\"chemotactic+peptide\\\""
		queries[85]="\\\"hormone+peptide\\\""
		queries[86]="\\\"defense+response+to+bacterium\\\""
		queries[87]="larvicide"
		queries[88]="nematicide"
		queries[89]="bacteriocidal"
		queries[90]="bacteriostatic"
		queries[91]="\\\"immunosuppressive+peptide\\\""
		queries[92]="\\\"angiogenesis+inhibitor\\\""
		queries[93]="\\\"antineoplastic+peptide\\\""
		queries[94]="anthelmintic"
		queries[95]="antihelminthic"
		queries[96]="microbicide"
		queries[97]="virucidal"
		queries[98]="\\\"Minimum+inhibitory+concentration\\\""
		queries[99]="\\\"minimum+bactericidal+concentration\\\""
		queries[100]="antimycotic"
		queries[101]="fungistatic"
		queries[102]="\\\"bioactive+peptide\\\""
		queries[103]="\\\"tumor+suppressor\\\""
		queries[104]="\\\"defense+peptide\\\""
		queries[105]="bacteriolytic"
		queries[106]="hemolysis"
		queries[107]="cyclotide"
		queries[108]="uterotonic"
		queries[109]="\\\"containing+peptide\\\""
		queries[110]="surfactin"
		queries[111]="iturin"
		queries[112]="mycosubtilin"
		queries[113]="eliciting+immune+peptide"
		queries[114]="elicitor+immune+peptide"
		# Inspired in APD database
		queries[115]="anti-TB"
		queries[116]="anti-HIV"
		queries[117]="anti-diabetic"
		queries[118]="candidacidal"
		queries[119]="anti-diabetes"
		queries[120]="antitumour"
		queries[121]="\\\"tumour+suppressor\\\""
		queries[122]="antibody"
d=0
		qq=0; while(qq<length(queries)) {qq=qq+1
# For testing purposes
#		qq=4; while(qq<5) {qq=qq+1
			query=queries[qq]
				system("cd DATABASES/UniProt ; python3 Uniprot-importer.py --verbose --max-length 150 "query" ; cp uniprot_importer.log uniprot_importer_"query".log ; cd ../..")
				input_file="DATABASES/UniProt/DATABASE_"query
				delete Uniprot_ids 
				s=0
				read_ADAPTABLE_database(input_file,d,ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
                synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
                antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
                tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
                drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
                pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
				while ( getline < input_file >0 ) {
					if(substr($0,1,1)==">") {
						d=d+1
						sequence=$2
							s=s+1
							sequence_parse_description[s]=sequence
							split($1,aa,"uniprot")
							Uniprot_ids[s]=aa[2]
							# https://www.uniprot.org/help/biocuration -> Not explicetly testing for their activity, hence, not experimental for us
							#experimental_[sequence]="experimental"
							if(queries[qq]~/antimicrobial/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/bioactive+peptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/microbicide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/lantibiotic/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/bacteriocin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/cecropin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/cathelicidin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/defensin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/arenicin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/bombinin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/clavanin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/efrapeptin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/esculentin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/histatin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/magainin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/conopeptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/microbe+inhibitor/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/microbial+inhibitor/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/defense+peptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/cytolytic+peptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/lytic+peptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/cell+penetrating+peptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/cell+penetrating/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/mast+cell+degranulating/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/cyanobactin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/ionophore+peptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/containing+peptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/surfactin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/mycosubtilin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/iturin/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/iturin/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/cyclopeptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/microbicidal/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/antibiotic+peptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/antibiotic+peptide/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/defense+response+to+bacter/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/defense+response+to+bacter/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/bactericide/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/bactericide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/mbc/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/mbc/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/minimum+bactericidal+concentration/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/minimum+bactericidal+concentration/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/bacteriolytic/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/bacteriolytic/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/anti-TB/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/anti-TB/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/bactericidal/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/bacteriocidal/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/bactericidal/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/bacteriocidal/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/bacteriostatic/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
							if(queries[qq]~/antibacterial/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/lantibiotic/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/bacteriocin/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/bacteriostatic/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/cecropin/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/cathelicidin/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/arenicin/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/histatin/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/arenicin/ && antigram_neg_[sequence]=="") {antigram_neg_[sequence]="antigram_neg"}
							if(queries[qq]~/thiopeptide/ && antigram_pos_[sequence]=="") {antigram_pos_[sequence]="antigram_pos"}
							if(queries[qq]~/thiazolyl/ && antigram_pos_[sequence]=="") {antigram_pos_[sequence]="antigram_pos"}
							if(queries[qq]~/VRE/ && antigram_pos_[sequence]=="") {antigram_pos_[sequence]="antigram_pos"}
							if(queries[qq]~/MRSA/ && antigram_pos_[sequence]=="") {antigram_pos_[sequence]="antigram_pos"}
							if(queries[qq]~/MSSA/ && antigram_pos_[sequence]=="") {antigram_pos_[sequence]="antigram_pos"}
							if(queries[qq]~/VRSA/ && antigram_pos_[sequence]=="") {antigram_pos_[sequence]="antigram_pos"}
                                                        if(queries[qq]~/thiopeptide/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
                                                        if(queries[qq]~/thiazolyl/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
                                                        if(queries[qq]~/VRE/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
                                                        if(queries[qq]~/MRSA/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
                                                        if(queries[qq]~/MSSA/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
                                                        if(queries[qq]~/VRSA/ && antimicrobial_[sequence]=="") {antimicrobial_[sequence]="antimicrobial"}
                                                        if(queries[qq]~/VRE/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
                                                        if(queries[qq]~/MRSA/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
                                                        if(queries[qq]~/MSSA/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
                                                        if(queries[qq]~/VRSA/ && antibacterial_[sequence]=="") {antibacterial_[sequence]="antibacterial"}
							if(queries[qq]~/antifungal/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/fungicide/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/fungicidal/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/candida/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/fungicide+peptide/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/efrapeptin/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/histatin/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/antimycotic/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/fungistatic/ && antifungal_[sequence]=="") {antifungal_[sequence]="antifungal"}
							if(queries[qq]~/antiyeast/ && antiyeast_[sequence]=="") {antiyeast_[sequence]="antiyeast"}
							if(queries[qq]~/antiviral/ && antiviral_[sequence]=="") {antiviral_[sequence]="antiviral"}
							if(queries[qq]~/anti+hiv/ && antiviral_[sequence]=="") {antiviral_[sequence]="antiviral"}
							if(queries[qq]~/viral+inhibitor/ && antiviral_[sequence]=="") {antiviral_[sequence]="antiviral"}
							if(queries[qq]~/anti+hcv/ && antiviral_[sequence]=="") {antiviral_[sequence]="antiviral"}
							if(queries[qq]~/anti-HIV/ && antiviral_[sequence]=="") {antiviral_[sequence]="antiviral"}
							if(queries[qq]~/virucidal/ && antiviral_[sequence]=="") {antiviral_[sequence]="antiviral"}
							if(queries[qq]~/antiprotozoal/ && antiprotozoal_[sequence]=="") {antiprotozoal_[sequence]="antiprotozoal"}
							if(queries[qq]~/antimalarial/ && antiprotozoal_[sequence]=="") {antiprotozoal_[sequence]="antiprotozoal"}
							if(queries[qq]~/nematicide/ && antiprotozoal_[sequence]=="") {antiprotozoal_[sequence]="antiprotozoal"}
							if(queries[qq]~/antiparasitic/ && antiparasitic_[sequence]=="") {antiparasitic_[sequence]="antiparasitic"}
							if(queries[qq]~/antimalarial/ && antiparasitic_[sequence]=="") {antiparasitic_[sequence]="antiparasitic"}
							if(queries[qq]~/parasiticidal/ && antiparasitic_[sequence]=="") {antiparasitic_[sequence]="antiparasitic"}
							if(queries[qq]~/nematicide/ && antiparasitic_[sequence]=="") {antiparasitic_[sequence]="antiparasitic"}
							if(queries[qq]~/anthelmintic/ && antiparasitic_[sequence]=="") {antiparasitic_[sequence]="antiparasitic"}
							if(queries[qq]~/antihelminthic/ && antiparasitic_[sequence]=="") {antiparasitic_[sequence]="antiparasitic"}
							if(queries[qq]~/antiplasmodial/ && antiplasmodial_[sequence]=="") {antiplasmodial_[sequence]="antiplasmodial"}
							if(queries[qq]~/antimalarial/ && antiplasmodial_[sequence]=="") {antiplasmodial_[sequence]="antiplasmodial"}
							if(queries[qq]~/antitrypanosomic/ && antitrypanosomic_[sequence]=="") {antitrypanosomic_[sequence]="antitrypanosomic"}
							if(queries[qq]~/trypanocidal/ && antitrypanosomic_[sequence]=="") {antitrypanosomic_[sequence]="antitrypanosomic"}
							if(queries[qq]~/antileishmania/ && antileishmania_[sequence]=="") {antileishmania_[sequence]="antileishmania"}
							if(queries[qq]~/leishmanicidal/ && antileishmania_[sequence]=="") {antileishmania_[sequence]="antileishmania"}
							if(queries[qq]~/insecticidal/ && insecticidal_[sequence]=="") {insecticidal_[sequence]="insecticidal"}
							if(queries[qq]~/efrapeptin/ && insecticidal_[sequence]=="") {insecticidal_[sequence]="insecticidal"}
							if(queries[qq]~/scorpion+venom+toxin/ && insecticidal_[sequence]=="") {insecticidal_[sequence]="insecticidal"}
							if(queries[qq]~/insecticide/ && insecticidal_[sequence]=="") {insecticidal_[sequence]="insecticidal"}
							if(queries[qq]~/larvicide/ && insecticidal_[sequence]=="") {insecticidal_[sequence]="insecticidal"}
							if(queries[qq]~/anticancer/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/anticarcinogenic/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/antileukemic/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/antitumor/ && antitumor_[sequence]=="") {antitumor_[sequence]="antitumor"}
							if(queries[qq]~/antitumor/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/antitumour/ && antitumor_[sequence]=="") {antitumor_[sequence]="antitumor"}
							if(queries[qq]~/antitumour/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/tumoricidal/ && antitumor_[sequence]=="") {antitumor_[sequence]="antitumor"}
							if(queries[qq]~/tumoricidal/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/antiangiogenic/ && antiangiogenic_[sequence]=="") {antiangiogenic_[sequence]="antiangiogenic"}
							if(queries[qq]~/antiangiogenic/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/hemolytic/ && hemolytic_[sequence]=="") {hemolytic_[sequence]="hemolytic"}
							if(queries[qq]~/hemolysis/ && hemolytic_[sequence]=="") {hemolytic_[sequence]="hemolytic"}
							if(queries[qq]~/hd50/ && hemolytic_[sequence]=="") {hemolytic_[sequence]="hemolytic"}
							if(queries[qq]~/hc50/ && hemolytic_[sequence]=="") {hemolytic_[sequence]="hemolytic"}
							if(queries[qq]~/hemolysin/ && hemolytic_[sequence]=="") {hemolytic_[sequence]="hemolytic"}
							if(queries[qq]~/immunomodulant/ && immunomodulant_[sequence]=="") {immunomodulant_[sequence]="immunomodulant"}
							if(queries[qq]~/antiinflammatory+peptide/ && immunomodulant_[sequence]=="") {immunomodulant_[sequence]="immunomodulant"}
							if(queries[qq]~/cysteine+protease+inhibitor/ && immunomodulant_[sequence]=="") {immunomodulant_[sequence]="immunomodulant"}
							if(queries[qq]~/cysteine+proteinase+inhibitor/ && immunomodulant_[sequence]=="") {immunomodulant_[sequence]="immunomodulant"}
							if(queries[qq]~/mast+cell+degranulating/ && immunomodulant_[sequence]=="") {immunomodulant_[sequence]="immunomodulant"}
							if(queries[qq]~/degranulating/ && immunomodulant_[sequence]=="") {immunomodulant_[sequence]="immunomodulant"}
							if(queries[qq]~/immunosuppressive/ && immunomodulant_[sequence]=="") {immunomodulant_[sequence]="immunomodulant"}
							if(queries[qq]~/antihypertensive/ && antihypertensive_[sequence]=="") {antihypertensive_[sequence]="antihypertensive"}
							if(queries[qq]~/antioxidant/ && antioxidant_[sequence]=="") {antioxidant_[sequence]="antioxidant"}
							if(queries[qq]~/antiproliferative/ && antiproliferative_[sequence]=="") {antiproliferative_[sequence]="antiproliferative"}
							if(queries[qq]~/hormone/ && hormone_[sequence]=="") {hormone_[sequence]="hormone"}
							if(queries[qq]~/antibiofilm/ && biofilm_[sequence]=="") {biofilm_[sequence]="biofilm"}
							if(queries[qq]~/antiangiogenic/ && antiangiogenic_[sequence]=="") {antiangiogenic_[sequence]="antiangiogenic"}
							if(queries[qq]~/neuropeptide/ && cell_cell_[sequence]=="") {cell_cell_[sequence]="cell_cell"}
							if(queries[qq]~/cell+penetrating+peptide/ && cell_penetrating_[sequence]=="") {cell_penetrating_[sequence]="cell_penetrating"}
							if(queries[qq]~/cell+penetrating+peptide/ && drug_delivery_[sequence]=="") {drug_delivery_[sequence]="drug_delivery"}
							if(queries[qq]~/chemotactic/ && cell_cell_[sequence]="") {cell_cell_[sequence]="cell_cell"}
							if(queries[qq]~/chemotactic/ && cell_cell_[sequence]="") {cell_cell_[sequence]="immunomodulant"}
							if(queries[qq]~/hormone/ && hormone_[sequence]="") {hormone_[sequence]="hormone"}
							if(queries[qq]~/anti-diabetic/ && hormone_[sequence]="") {hormone_[sequence]="hormone"}
							if(queries[qq]~/uterotonic/ && hormone_[sequence]="") {hormone_[sequence]="hormone"}
							if(queries[qq]~/diabetes/ && hormone_[sequence]="") {hormone_[sequence]="hormone"}
							if(queries[qq]~/neurotoxin/ && cytotoxic_[sequence]="") {cytotoxic_[sequence]="cytotoxic"}
							if(queries[qq]~/neurotoxin/ && toxic_[sequence]="") {toxic_[sequence]="toxic"}
							if(queries[qq]~/angiogenesis/ && antiangiogenic_[sequence]="") {antiangiogenic_[sequence]="antiangiogenic"}
							if(queries[qq]~/angiogenesis/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/antineoplastic/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/tumor+suppressor/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/tumor+suppressor/ && antitumor_[sequence]=="") {antitumor_[sequence]="antitumor"}
							if(queries[qq]~/tumour+suppressor/ && anticancer_[sequence]=="") {anticancer_[sequence]="anticancer"}
							if(queries[qq]~/tumour+suppressor/ && antitumor_[sequence]=="") {antitumor_[sequence]="antitumor"}
							if(queries[qq]~/eliciting+immune+peptide/ && immunomodulant_[sequence]=="") {immunomodulant_[sequence]="immunomodulant"}
							if(queries[qq]~/elicitor+immune+peptide/ && immunomodulant_[sequence]=="") {immunomodulant_[sequence]="immunomodulant"}
					}
				}
			close(input_file)
			input_file="DATABASES/UniProt/uniprot_importer_"query".log"
			print "Parsing descriptions from "input_file
			s=0; while(s<length(Uniprot_ids)) {s=s+1
				while ( getline < input_file >0 ) {
						# Explore "key" lines
						if($0~Uniprot_ids[s] && $0~/key/ && $0~/Entry/) {start=1}
						if($0~/\<dbReference/ && $0~/PubMed/&& start==1) {
							field = $0
							pmid = ""
							# Extract the id values from the field variable
							while (match(field, /id="([0-9]+)"/, arr)) {
								idpub = arr[1]
								# Append the idpub to pmid if it's not already present
								if (index(";" pmid ";", ";" idpub ";") == 0) {
									pmid = pmid (pmid ? ";" : "") idpub
								}
								# Remove the matched part from the field to process the next match
								field = substr(field, RSTART + RLENGTH)
							}
							if (pmid) {if(PMID_[sequence_parse_description[s]]!~pmid) {PMID_[sequence_parse_description[s]]=PMID_[sequence_parse_description[s]]""pmid";"}}
						}
						if($0~/<\/reference>/) {start=0}
						
						# Explore type="function" lines
						if($0~Uniprot_ids[s] && $0~/function/ && $0~/Entry/) {start=1}
						if($0~/\<text/ && start==1) {
							description[s]=$0
							description[s]=description[s]"."
							gsub("the marine like ","",description[s])
							gsub("the marine ","",description[s])
							gsub("bacteriun","bacterium",description[s])
							gsub("larvae of flesh fly ","",description[s])
							gsub("several ","",description[s])
							gsub("multidrug-resistant ","",description[s])
							gsub("methicillin-resistant ","",description[s])
							gsub("representative ","",description[s])
							gsub("l species","",description[s])
							gsub(" strains","",description[s])
							gsub("various species of bacteria including both ","",description[s])
							gsub(" groups","",description[s])
							gsub("both ","",description[s])
							gsub("plant pathogenic ","",description[s])
							gsub("the enveloped ","",description[s])
							gsub("epimastigote form of ","",description[s])
							gsub("trypomastigote form of ","",description[s])
							gsub(". Probably acts by disturbing membrane functions with its amphipathic structure","",description[s])
							gsub("acts by inducing bacterial membrane breakage","",description[s])
							gsub("a variety of ","",description[s])
							gsub("the human pathogenic ","",description[s])
							gsub("some ","",description[s])
							gsub("respectively","",description[s])
							gsub("this protein can neutralize the effects of the venom","",description[s])
							gsub(" Strongly inhibits development of ",",",description[s])
							gsub("a potent inhibitory effect on ookinete","",description[s])
							# Ensure subsequent gsub are not influenced by the following removal
							gsub("the ","",description[s])
							gsub("but not P. aeruginosa or E. coli","",description[s])
							gsub("but not E. coli or P. carotovorum subsp carotovorum","",description[s])
							gsub(" or ",",",description[s])
							gsub("many of ","",description[s])
							gsub("but is less active","",description[s])
							gsub("less ","",description[s])
							gsub("does not display antimicrobial activity S. aureus","",description[s])
							gsub(" but does not possess antibacterial activity","",description[s])
							gsub("but does not show antibacterial activity other","",description[s])
							gsub(" Acipensin 6 has antibacterial activity ","",description[s])
							gsub(" but not S. aureus ATCC 12600","",description[s])							
							gsub("very weak activity ","",description[s])
							gsub("probably by facilitating their incorporation into bacteria","",description[s])
							gsub("weak activity ","",description[s])
							gsub(". Is also active ",",",description[s])
							gsub("non-typeable ","",description[s])
							gsub("l pathogens","",description[s])
							gsub("may act via membrane-permeabilization of these cells","",description[s])
							gsub("promastigote forms of ","",description[s])
							gsub("but not C. glabrata","",description[s])
							gsub("but not B. bassiana","",description[s])
							gsub("but not E. coli K12D31","",description[s])
							gsub("but lacks antibacterial activity S. aureus","",description[s])
							gsub("but lacks C. gloeosporioides KACC 40003","",description[s])
							gsub("but lacks A. niger","",description[s])
							gsub("but lacks ","",description[s])
							gsub("antibiotic resistant bacteria methicilin-resistant ","",description[s])
							gsub("vancomycin-resistant ","",description[s])
							gsub(" Does not inhibit trypsin","",description[s])
							gsub("which are resistant beta-lactam antibiotics","",description[s])
							gsub("highly infectious wild-type strain ","",description[s])
							gsub("intra-erythrocyte stage of ","",description[s])
							gsub("possibly by attacking bacterial cell membrane","",description[s])
							gsub("microbes by interacting directly with their membranes","",description[s])
							gsub("but none other","",description[s])
							gsub("of Clostridium","Clostridium",description[s])
							gsub("but not C. neoformans 201211","",description[s])
							gsub("but not C. albicans ATCC 12231","",description[s])
							gsub("but not L. curvatus CWBI-B28","",description[s])
							gsub("but not P. aeruginosa","",description[s])
							gsub("but not B. cinerea","",description[s])
							gsub("but not F. oxysporum","",description[s])
							gsub("but not S. typhimurium","",description[s])
							gsub("but not E. coli","",description[s])
							gsub("pathogenic of ","",description[s])
							gsub("against E. coli","E. coli",description[s])
							gsub("against S. aureus","S. aureus",description[s])
							gsub("species of ","",description[s])
							gsub("such as ","",description[s])														
							gsub("a wide spectrum of including ","",description[s])
							gsub("aerobic","",description[s])
							gsub("anaerobic","",description[s])
							gsub("but does not show antibacterial activity other","",description[s])
							gsub("but does not possess antibacterial activity","",description[s])
							gsub("does not display antimicrobial activity S. aureus","",description[s])
							gsub("promastigote form of ","",description[s])
							gsub("Gram-negative than","",description[s])
							gsub("enterobacteria including species of Klebsiella","Enterobacteriaceae,Klebsiella",description[s])
							gsub("of E. coli","E. coli",description[s])
							gsub("especially strain G","",description[s])
							gsub("species of l genera ","",description[s])
							gsub("of fungi ","",description[s])							
							gsub("including pathogenic ","",description[s])
							gsub("including ","",description[s])
							gsub(" than E. coli","",description[s])
							gsub("a number of ","",description[s])
							gsub("glycopeptide-intermediate ","",description[s])
							gsub("against C. utilis","C. utilis",description[s])
							gsub("probably by facilitating their incorporation into bacteria","",description[s])
							gsub("a range of ","",description[s])
							gsub("like B. subtilis","B. subtilis",description[s])
							gsub("as well as induction of platelet aggregation","",description[s])
							gsub(" as well as like ",",",description[s])
							gsub("as well as ",",",description[s])
							gsub(" but has weak","",description[s])
							gsub("a lower activity ","",description[s])
							gsub("lower activity ","",description[s])
							gsub("many Gram-negative enterobacterial","Enterobacteriaceae",description[s])
							gsub(" Has no S. cerevisiae","",description[s])
							gsub("of L. monocytogenes","L. monocytogenes",description[s])
							gsub("is virtually inactive ","",description[s])
							gsub("is inactive ","",description[s])
							gsub("is also active ","",description[s])
							gsub("but is inactive mammalian sodium channels","",description[s])
							gsub("inactive ","",description[s])
							gsub("many epidemiologically unrelated of ","",description[s])
							gsub(" only at high concentrations","",description[s])
							gsub("stronger activity ","",description[s])
							gsub("strong activity ","",description[s])
							gsub("when in combination with XT1","",description[s])
							gsub("but not P. aeruginosa","",description[s])
							gsub("weaker activity ","",description[s])
							gsub("slight activity ","",description[s])
							gsub("slight ","",description[s])
							gsub("to a lesser extent","",description[s])
							gsub("against E. aerogenes","E. aerogenes",description[s])
							gsub(" along with two other Vibrio species","Vibrio",description[s])
							gsub("funga ","fungus ",description[s])
							gsub("L-Met","",description[s])
							gsub("L-Leu","",description[s])
							gsub("L-Nle","",description[s])
							gsub("L-Trp","",description[s])
							gsub("L-Phe","",description[s])
							gsub("thus producing hydrogen peroxide that may contribute to diverse toxic effects of this enzyme","",description[s])
							gsub("a wide range of lactic acid bacteria","Lactic_acid_bacteria,gram-pos",description[s])
							gsub("multi-resistant clinical isolate strain ","",description[s])
							gsub(" even at high concentrations","",description[s])
							gsub("probably by forming pores in cell membrane","",description[s])
							gsub("clinical antibiotics-resistant bacterial","",description[s])
							gsub("a fungus","fungus",description[s])
							gsub(" PV. < text >","",description[s])
							gsub("has no effect L. garvieae","",description[s])
							gsub("clinical isolates of Gram-negative multidrug resistant of ","",description[s])
							gsub("against clinical isolates of C. albicans","C. albicans",description[s])
							gsub("it displays low cytolytic","",description[s])
							gsub("but no toxicity is observed in mice","",description[s])
							gsub("oxidative stress during pollen development","",description[s])
							gsub("polyuracil","",description[s])
							gsub("Additionally","",description[s])
							gsub("< text >","",description[s])
							gsub("marine like","",description[s])
							gsub("but not","",description[s])
							gsub("&gt;",">",description[s])
							# Now we can drop this values from all_families
							field=description[s]
							string=read_activity_description(field,"_against",sequence_parse_description[s],all_organisms_,"not_act","no_act")

							# Extract PubMed IDs
							# Initialize an associative array to store unique PubMed IDs
							delete pmid_array
							# Use a regular expression to find all PubMed IDs in the "field" variable
							while (match(field, /PubMed:[0-9]+/)) {
								# Extract the matched PubMed ID
								idpub = substr(field, RSTART, RLENGTH)
							        # Remove the "PubMed:" prefix using the sub function
							        sub(/^PubMed:/, "", idpub)
							        # Store the ID in the associative array (duplicates are automatically handled)
							        pmid_array[idpub]
							        # Remove the matched part from the string to find the next match
						        	field = substr(field, RSTART + RLENGTH)
							}
							# Join the unique PubMed IDs into a semicolon-separated string
							pmid = ""
							for (idpub in pmid_array) {
								if (pmid == "") {
							            pmid = idpub
							        } else {
							            pmid = pmid ";" idpub
							        }
							}
							if (pmid) {if(PMID_[sequence_parse_description[s]]!~pmid) {PMID_[sequence_parse_description[s]]=PMID_[sequence_parse_description[s]]""pmid";"}}
						}
						if($0~/<\/comment>/) {start=0}
					}
				close(input_file)
				}
		}
dmax=d
return dmax
}
