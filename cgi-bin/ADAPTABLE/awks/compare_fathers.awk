function compare_fathers(seq_vector,level_agreement,min_seq_length,align_method,\
		d,dd,dmax,f,u,k,limit,a,len_seq,len_ref,head,tail,incr,e,le,max_le,q)
{
	dmax=length_nD(seq_vector,1)
		delete used_sequence
		delete seq_fam
		delete fam
		delete type_seq
		delete shift_seq
		delete min_shift
		delete max_leng
		delete prop_seq
		delete seq_names_fam
		delete seq_fam_transl
		delete kill_family_
		delete counter_sp
		delete superfamily 
		delete max_sp

		# We want to force higher similarity when comparing families instead of it changing with the similarity chosen for making each family
		level_agreement=70
		d=0;while(d<dmax) {d=d+1
			if(kill_family_[d]=="") {
			superfamily[d,0]=d
			delete max_score_
				split(seq_vector[d],reference,"");shift[d]=0 ; len_ref=length(reference)
				split(seq_vector[d],p_seq,"")
				align_sequences(seq_type,max_score_,"***",d,reference,p_seq,len_ref,align_method,blosum)
				max_possible_score[d]=max_score_[d]; type_seq[d]=seq_type[1]
				min_shift[d]=100
				max_leng[d]=0
				if(len_ref>=min_seq_length) {
					dd=d;while(dd<dmax) {dd=dd+1
						delete max_score
							len_seq=length(seq_vector[dd]); if(len_seq>len_ref) {align_range[d]=len_seq} else {align_range[d]=len_ref}
						split(seq_vector[dd],p_seq,"")
						shift[dd]=align_sequences(seq_type,max_score,"***",d,reference,p_seq,align_range[d],align_method,blosum);
							#if(level_agreement=="auto") {if(len_ref<14) {limit=max_possible_score[d]/5*3} ; if(len_ref>=14 && len_ref<30) {limit=max_possible_score[d]/5*2} ; if(len_ref>=30) {limit=max_possible_score[d]/5}}
							#else {
							#limit=max_possible_score[d]/5*level_agreement}
							limit=max_possible_score[d]*level_agreement/100
							if(max_score[d]>limit && (((len_ref-((shift[dd]**2)**0.5))>=min_seq_length && shift[dd]>=0) || ((len_seq-((shift[dd]**2)**0.5))>=min_seq_length && shift[dd]<0) || level_agreement=="all") && used_sequence[dd]!="yes") {
									kill_family_[dd]=1
									counter_sp[d]=counter_sp[d]+1
									max_sp[d]=counter_sp[d]
									superfamily[d,counter_sp[d]]=dd
								}
					}
				}
		}
		}
	k=0
	d=0;while(d<dmax) {d=d+1
		if(kill_family_[d]=="") {
			cmd="sed -n 's/Family of sequences "d" has //p' OUTPUTS/"calculation_label"/.family_elements"
			cmd | getline elements
			close(cmd)
			close("OUTPUTS/"calculation_label"/.family_elements")
			# It seems it's enough with "family group" information
			k=k+1
			printf "Group similar to family "d" "seq_vector[d]": " 
			u=-1;while(u<max_sp[d]) {u=u+1; if(u<max_sp[d])  {printf superfamily[d,u]","} else {printf superfamily[d,u]"."}}; print ""
			printf "Group similar to family "d" "seq_vector[d]": " >> "OUTPUTS/"calculation_label"/.different-fathers-all"
			u=-1;while(u<max_sp[d]) {u=u+1; if(u<max_sp[d])  {printf superfamily[d,u]"," >> "OUTPUTS/"calculation_label"/.different-fathers-all" } else {printf superfamily[d,u]"." >> "OUTPUTS/"calculation_label"/.different-fathers-all" }}; print "" >> "OUTPUTS/"calculation_label"/.different-fathers-all"
			}
		}
		system("grep ',' OUTPUTS/"calculation_label"/.different-fathers-all > OUTPUTS/"calculation_label"/.different-fathers")
		system("rm -f OUTPUTS/"calculation_label"/.different-fathers-all")
		#print k > "OUTPUTS/"calculation_label"/.group_fam_num"
		cmd="wc -l <OUTPUTS/"calculation_label"/.different-fathers"
		cmd | getline group_fam_num
		close(cmd)
		close("OUTPUTS/"calculation_label"/.different-fathers")
		print group_fam_num > "OUTPUTS/"calculation_label"/.group_fam_num"
}
