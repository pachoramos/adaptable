BEGIN {}
{
	if ($1~/:/) {
		tag=""
			if($1~/anti/) {tag="ACTIVITY"} 
		if($1~/hemo/ || $1~/toxic/) {tag="TOXICITY"}
		if($1~/PTM/ || $1~/terminus/ || $1~/cycl/ || $1~/stereo/) {tag="CHEMICAL_CHARACTERISTICS"}
		if($1~/source/ || $1~/name/ || $1~/family/ || $1~/taxonomy/) {tag="SOURCE"}
		if(tag=="") {tag="OTHERS"}
		line=$0
		getline; 
		if ($1~/summary/){counter[tag]=counter[tag]+1 ; head[tag,counter[tag]]=line 
			getline;
			property[tag,counter[tag]]=property[tag,counter[tag]]""$0 
		}
	}
}
END{
name_tag[1]="ACTIVITY"
name_tag[2]="TOXICITY"
name_tag[3]="SOURCE"
name_tag[4]="CHEMICAL_CHARACTERISTICS"
name_tag[5]="OTHERS"
max_tag=5
j=0; while(j<max_tag) {j=j+1
	tag=name_tag[j]
		print ""
		print tag":"
		print ""
		i=0; while(i<counter[tag]) {i=i+1
			print head[tag,i]
			print property[tag,i]
				print ""
				print "-----------------";
			print ""
		}
print ""
print "################################################################################"
print ""
}
}
