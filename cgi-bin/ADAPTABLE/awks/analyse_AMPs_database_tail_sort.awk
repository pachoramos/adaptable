@include "awks/library.awk"
# Space needed since gawk-4.2
BEGIN{
	parameters(prp_name,prp_option)
	read_aa_prop_and_colors(aa_name,conv_aa,class,class_def,class_length,type,polarity,color_type)

		delete seq
		delete original_seq
		delete prop
		delete seq_names

		comparison_option=ENVIRON["comparison_option"]
		
		max_len=0
		input_file="OUTPUTS/"calculation_label"/selection"
		while ( getline < input_file >0 ) {
			if($2=="seq") {seq[$1]=$3}
			if($2=="oseq") {original_seq[$1]=$3;len=length($3);population[int(len)]=population[int(len)]+1 ; if(len>max_len) {max_len=len}}
			if($2=="prop_separator") {split($0,aaaa,"prop_separator ") ; prop[$1]=aaaa[2] }
			if($2=="names") {seq_names[$1]=$3}
			dmax=$1
		}
		close(input_file)

		# [if f indicate the family index abd k is an index running on the components of each family]
		# family[f,k]: list of sequences that might be translated in different spaces (aa types or DSSP)
		# original_family[f,k] : list of pure sequences
		# alignments[f,k]: list of pure sequences aligned by introduction of - symbols
		# alignments_transl[f,k]: list of sequences (that might be translated in different spaces) aligned by introduction of - symbols
		# fam_type[f,k]: list of sequences translated in aa types
		# fathers[f,ff]: list of fathers (that might be translated in different spaces) that can originate the same family, listed by score
		# original_fathers[f,ff]: list of fathers that can originate the same family, listed by score
		# fam_size[f]=number of elements in the family 
		# fam_properties[f,k]=list of properties of each sequence
		# fam_names[f,k]= list of names of each sequence
		# score_fathers[f,ff]=scores clqssifying the ability of each father to generate the family
		# code_fathers[f,ff]=code summarising the components of the family 
		# type_fathers[f,ff]=list of fathers translated in aa types
		print ""
		printf "Sorting families..."
                sort_families(family,original_family,fam_type,fathers,original_fathers,fam_size,fam_aligned,fam_properties,fam_names,alignments,alignments_transl,score_fathers,code_fathers,type_fathers,"OUTPUT_INPUT",\
                                dmax,seq,original_seq,c)

		print "DONE!"
}