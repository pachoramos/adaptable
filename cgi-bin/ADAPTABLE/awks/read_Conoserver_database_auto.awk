function read_conoserver_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
                synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
                antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
                tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
                drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
                pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
                a,b,c,aa,aaa,p,pp,ppp,f,ff,fff,v) {
        limit=10000

a=1;
limit_=a+limit
print "" > "tmp_files/modified_aa_conoserver"
path="DATABASES/Conoserver/Conoserverfiles/Conoserver"
length_numb=4
add_tag="no"
split(path,aa,"/")
tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
file_online=root_online""tag""a
while (a<limit_) {
	tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
	file=path""tag""a
	if (system("[ -f " file " ] ") ==0 ) {
		#seq
		input_file=file
		seq=""
		field=read_database_line(input_file,"name='sequence'",0,"value='","'>",1)
		gsub("<div_class='seq_signal'>","",field)
		gsub("</div>","",field)
		gsub("</td>","",field)
		gsub("<br>","",field)
                gsub("<div_class='seq_mature'>","",field)

		n_e=split(field,aa,"(")
		nn=0; while(nn<n_e) {nn=nn+1
			split(aa[nn],bb,")");if((bb[2]!="" || substr(aa[nn],length(aa[nn])-1,1)==")") && aa[nn]!="nh2)") {aa[nn]="X"bb[2]}
					if(aa[nn]!="nh2)") {seq=seq""aa[nn]} 
					}
					gsub("<b>","",seq);gsub("</b>","",seq);
					gsub("_","",seq)
					seq_a[a]=seq
					seq_a[a]=correct_sequence(seq_a[a])
					read_modified_aa(modif,translate_modif)
					nter=""
					cter=""
					position=0; b=0;while(position!="") {
					position=read_database_line(input_file,"Modified_residues",3+b,"<td>","</td>",1)
					symbol=read_database_line(input_file,"Modified_residues",3+b,"<td>","</td>",2)
					species=read_database_line(input_file,"Modified_residues",3+b,"<td>","</td>",3)
					species=clean_string(species); 
					species=tolower(species)
					if(species!="" && translate_modif[species]=="") {
						print "translate_modif[\""species"\"]=\"X\";" >> "tmp_files/modified_aa_conoserver"
						translate_modif[species]="X"
					}
					if(translate_modif[species]=="") {translate_modif[species]="X"}
					if (species~/C-term/) {gsub("C-term","",species); cter=clean_string(species);}
					if (species~/N-term/) {gsub("N-term","",species); nter=clean_string(species);}
					seq_a[a]=modify_sequence_in_position(seq_a[a],position,translate_modif[species])
					b=b+1
					}
					
					print "Reading "file" for sequence "seq_a[a]""
					seq_a[a]=analyze_sequence(seq_a[a])
					if(C_terminus_[seq_a[a]]!~cter) {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""cter";"}
					if(N_terminus_[seq_a[a]]!~nter) {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""nter";"}

					#ID
					# Now is '10' lines after Pharmacological families line
					field=read_database_line(input_file,">Pharmacological families<",10,"(",")",1)
					if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"Conoserver"field";"}

					#source
					field=read_database_line(input_file,"<b>Organism</b>",2,"<i>","</i>",1)
					field=clean_string(field)
					if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
					if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";"}}
					#name
					field=read_database_line(input_file,"<b>Name</b>",2,"'>","</a>",1)
					field=clean_string(field)
					gsub("Patent","patent",field)
					split(field,zzzz,"from_patent_")
					patent=zzzz[2]
					if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";"}}

					#Family
					field=read_database_line(input_file,"Conopeptide_class",2,"dontcut","</td>",1)
					field=clean_string(field)
					if(Family_[seq_a[a]]!~field) {if(field!="") {Family_[seq_a[a]]=Family_[seq_a[a]]""field";" }}
					#gene
					field=read_database_line(input_file,"<b>Gene superfamily</b>",2,"'>","</a>",1)
					field=clean_string(field)
					if(gene_[seq_a[a]]!~field) {if(field!="") {gene_[seq_a[a]]=gene_[seq_a[a]]""field";" }}
					#activity
					field1=read_database_line(input_file,"Inhibition",0,"black'>","</td",1)
					gsub("&plusmn;","+-",field1)
					field2=read_database_line(input_file,"Unit</FONT",3,"dontcut","</a",1)
					gsub("&mu;","u",field2)
					activity_viral_[seq_a[a]]=field1""field2
					start=0
					while ( getline < input_file >0 ) {
						if($0~/<h2>/) { start=1
							if(start==1) {
								llmax=split($0,lines,"</td></tr")
								ll=0;while(ll<llmax) {ll=ll+1
									if(lines[ll]~/<h2>/) {
										split(lines[ll],methods,"colspan=\"2\">"); split(methods[2],bb,"</th><th>"); method=bb[1]; 
										ffmax=split(lines[ll],title_,"</th><th>")
                                                                                ff=0; while(ff<ffmax) {ff=ff+1
                                                                                if(title_[ff]~/colspan/) {pos_value=ff+1}
                                                                        }
										}
										split(lines[ll],fields,"</td><td")
									gsub("<i>","",fields[2])
                                                                        gsub("</i>","",fields[2])
                                                                        gsub(">","",fields[2])
									if(fields[2]!="") {organism=fields[2]}
									split(fields[pos_value],bb,"right")
									value=bb[2]
									gsub("&gt;",">",value)
									gsub(">","",value)
									gsub("<","",value)
									gsub("\"","",value)
									if(value!="") {
									if(organism!="Unknown") {string=organism"("method"="value")"} else {string=""}
									gsub(" ","_",string)
									string=analyze_organism(string,seq_a[a])
									string_to_compare=""
									string_to_compare=escape_pattern(string)
									if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
								}
							}
						}
					}
				}
				close(input_file)

				# target
				field=read_database_line(input_file,"Target",0,"TARGET\">","</a",1)
				field=clean_string(field)
				if(target_[seq_a[a]]!~field) {if(field!="") {target_[seq_a[a]]=target_[seq_a[a]]""field";"}}
				#PMID
				field=1; b=0; while(field!="") {b=b+1
					field=read_database_line(input_file,"<b>References</b>",3,"list_uids=","&query",b)
					if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}
					if(PMID_[seq_a[a]]!~patent) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""patent";"}}
				}
			
					# http://www.conoserver.org/?page=about_conoserver
					# "curated"..., but curated doesn't implies experimental as sometimes it's simply that they confirm it's a precursor or a predicted sequence (depends on the upstream DB)
					# http://www.conoserver.org/?page=help
#					experimental_[seq_a[a]]="experimental"
			
			}
			close(input_file)
			a=a+1
			file= "DATABASES/Conoserver/Conoserverfiles/Conoserver"a
			omax=a
		}
		return omax
	}
