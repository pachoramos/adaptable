function read_microbes(full_microbe_name,kingdom_microbe,gram,biofilm,genus_microbe,\
i)
{
# full_microbe_name[],kingdom_microbe[],gram[] and biofilm[] return their value when [] contains the abbreviated name 
# (e.g. full_microbe_name["M.luteus"]=Micrococcus_luteus ;  gram["M.luteus"]=gram_pos (or "gram_neg"); biofilm[["M.luteus"]=biofilm (or "_") ; kingdom["M.luteus"]=Bacteria (or "Viruses" or "Archaea" or "Eukaryota" or "Viroids"))
# All vectors would also read "M._luteus", Micrococcus_spp., or "Micrococcus_luteus"   
 
input_file="MD2/datasets/all/microbe-directory.csv"
		i=0
		while ( getline < input_file >0 ) {
			i=i+1
			split($0,fields,",")
			name=fields[8]
			gsub(" ","_",name)
			gsub("_sp_","_",name)
			split(name,names,"_")
			genus=names[1]
			species=names[2]
			initial=substr(genus,1,1)
			if(species=="") {species="sp"}

			abbreviated_name=initial"."species
			full_microbe_name[abbreviated_name]=genus"_"species
			full_microbe_name[full_microbe_name[abbreviated_name]]=genus"_"species
			kingdom_microbe[abbreviated_name]=fields[2]
			kingdom_microbe[full_microbe_name[abbreviated_name]]=fields[2]
			genus_microbe[abbreviated_name]=fields[7]

                        name_spp=genus"_spp."
                        full_microbe_name[name_spp]=genus"_spp."
                        kingdom_microbe[name_spp]=fields[2]
			genus_microbe[name_spp]=fields[7]


			abbreviated_name_space=initial"._"species
			full_microbe_name[abbreviated_name_space]=genus"_"species
			kingdom_microbe[abbreviated_name_space]=fields[2]
			kingdom_microbe[full_microbe_name[abbreviated_name_space]]=fields[2]
			genus_microbe[abbreviated_name_space]=fields[7]
			genus_microbe[full_microbe_name[abbreviated_name_space]]=fields[7]

			if(fields[9]==1) {gram[abbreviated_name]="gram-pos"} 
			if(fields[9]==0) {gram[abbreviated_name]="gram-neg"}
			gram[abbreviated_name_space]=gram[abbreviated_name];gram[full_microbe_name[abbreviated_name]]=gram[abbreviated_name];
			gram[full_microbe_name[abbreviated_name_space]]=gram[abbreviated_name]; gram[name_spp]=gram[abbreviated_name]
			if(fields[19]==1) {biofilm[abbreviated_name]="biofilm"} else {biofilm[abbreviated_name]="_"}
			biofilm[abbreviated_name_space]=biofilm[abbreviated_name];biofilm[full_microbe_name[abbreviated_name]]=biofilm[abbreviated_name];
			biofilm[full_microbe_name[abbreviated_name_space]]=biofilm[abbreviated_name]; biofilm[name_spp]=biofilm[abbreviated_name]

		#	print name,gram[abbreviated_name],biofilm[abbreviated_name],abbreviated_name,full_microbe_name[abbreviated_name],kingdom_microbe[abbreviated_name]
		}
}
