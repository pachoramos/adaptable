@include "awks/library.awk"
{
        conv_aa["ALA"]=1;conv_aa["ARG"]=2;conv_aa["ASN"]=3;conv_aa["ASP"]=4;conv_aa["CYS"]=5;conv_aa["GLN"]=6;conv_aa["GLU"]=7;conv_aa["GLY"]=8;conv_aa["HIS"]=9;conv_aa["ILE"]=10;
        conv_aa["LEU"]=11;conv_aa["LYS"]=12;conv_aa["MET"]=13;conv_aa["PHE"]=14;conv_aa["PRO"]=15;conv_aa["SER"]=16;conv_aa["THR"]=17;conv_aa["TRP"]=18;conv_aa["TYR"]=19;conv_aa["VAL"]=20;

        conv_rev[1]="A"; conv_rev[2]="R";conv_rev[3]="N";conv_rev[4]="D";conv_rev[5]="C";conv_rev[6]="Q";conv_rev[7]="E";conv_rev[8]="G";conv_rev[9]="H";conv_rev[10]="I";
        conv_rev[11]="L";conv_rev[12]="K";conv_rev[13]="M";conv_rev[14]="F";conv_rev[15]="P";conv_rev[16]="S";conv_rev[17]="T";conv_rev[18]="W";conv_rev[19]="Y";conv_rev[20]="V";

        conv_OLC["ALA"]="A";conv_OLC["VAL"]="V";conv_OLC["LEU"]="L";conv_OLC["ILE"]="I";conv_OLC["PRO"]="P";
        conv_OLC["PHE"]="F";conv_OLC["TRP"]="W";conv_OLC["MET"]="M";conv_OLC["CYS"]="C";conv_OLC["GLY"]="G";
        conv_OLC["ASN"]="N";conv_OLC["GLN"]="Q";conv_OLC["SER"]="S";conv_OLC["THR"]="T";conv_OLC["TYR"]="Y";
        conv_OLC["LYS"]="K";conv_OLC["ARG"]="R";conv_OLC["HIS"]="H";conv_OLC["ASP"]="D";conv_OLC["GLU"]="E";
        conv_OLC["ala"]="A";conv_OLC["val"]="V";conv_OLC["leu"]="L";conv_OLC["ile"]="I";conv_OLC["pro"]="P";
        conv_OLC["phe"]="F";conv_OLC["trp"]="W";conv_OLC["met"]="M";conv_OLC["cys"]="C";conv_OLC["gly"]="G";
        conv_OLC["asn"]="N";conv_OLC["gln"]="Q";conv_OLC["ser"]="S";conv_OLC["thr"]="T";conv_OLC["tyr"]="Y";
        conv_OLC["lys"]="K";conv_OLC["arg"]="R";conv_OLC["his"]="H";conv_OLC["asp"]="D";conv_OLC["glu"]="E";

        conv_OLC_["valine"]="V";
        conv_OLC_["leucine"]="L";
        conv_OLC_["isoleucine"]="I";
        conv_OLC_["proline"]="P";
        conv_OLC_["phenylalanine"]="F";
        conv_OLC_["tryptophan"]="W";
        conv_OLC_["methionine"]="M";
        conv_OLC_["cysteine"]="C";
        conv_OLC_["glycine"]="G";
        conv_OLC_["asparagine"]="N";
        conv_OLC_["glutamine"]="Q";
        conv_OLC_["serine"]="S";
        conv_OLC_["threonine"]="T";
        conv_OLC_["tyrosine"]="Y";
        conv_OLC_["lysine"]="K";
        conv_OLC_["arginine"]="R";
        conv_OLC_["histidine"]="H";
        conv_OLC_["aspartate"]="D";
        conv_OLC_["asparticacid"]="D";
        conv_OLC_["aspartic-acid"]="D";
        conv_OLC_["glutamate"]="E";
        conv_OLC_["glutamicacid"]="E";
        conv_OLC_["glutamic-acid"]="E";
        conv_OLC_["ornithine"]="O";

	conv_TLC["O"]="ORN"
        conv_TLC["A"]="ALA";conv_TLC["V"]="VAL";conv_TLC["L"]="LEU";conv_TLC["I"]="ILE";conv_TLC["P"]="PRO";
        conv_TLC["F"]="PHE";conv_TLC["W"]="TRP";conv_TLC["M"]="MET";conv_TLC["C"]="CYS";conv_TLC["G"]="GLY";
        conv_TLC["N"]="ASN";conv_TLC["Q"]="GLN";conv_TLC["S"]="SER";conv_TLC["T"]="THR";conv_TLC["Y"]="TYR";
        conv_TLC["K"]="LYS";conv_TLC["R"]="ARG";conv_TLC["H"]="HIS";conv_TLC["D"]="ASP";conv_TLC["E"]="GLU";

        conv["ala"]="ALA";conv["val"]="VAL";conv["leu"]="LEU";conv["ile"]="ILE";conv["pro"]="PRO";conv["phe"]="PHE";conv["trp"]="TRP";conv["met"]="MET";conv["cys"]="CYS";conv["gly"]="GLY";
        conv["asn"]="ASN";conv["gln"]="GLN";conv["ser"]="SER";conv["thr"]="THR";conv["tyr"]="TYR";conv["lys"]="LYS";conv["arg"]="ARG";conv["his"]="HIS";conv["asp"]="ASP";conv["glu"]="GLU";

        conv_["ALA"]="ala";conv_["VAL"]="val";conv_["LEU"]="leu";conv_["ILE"]="ile";conv_["PRO"]="pro";conv_["PHE"]="phe";conv_["TRP"]="trp";conv_["MET"]="met";conv_["CYS"]="cys";conv_["GLY"]="gly";
        conv_["ASN"]="asn";conv_["GLN"]="gln";conv_["SER"]="ser";conv_["THR"]="thr";conv_["TYR"]="tyr";conv_["LYS"]="lys";conv_["ARG"]="arg";conv_["HIS"]="his";conv_["ASP"]="asp";conv_["GLU"]="glu";

        fam=""

        if(conv_TLC[$2]=="") {fam=conv_OLC_[$2]","conv_TLC[conv_OLC_[$2]]} else {fam=$2","conv_TLC[$2]}

        if(fam==",") {fam="  -  "}

        if(found[$5]=="") {
            i=i+1
            found[$5]=1
            family[i]=fam
            list[i]=sprintf("<tr><td class=\"char-td\">%1s</td><td class=\"char-td\"><a class=\"char-link\" target=\"blank\" href=\"https://pubchem.ncbi.nlm.nih.gov/compound/%1s\">%1s</a></td><td class=\"char-td\"><a class=\"char-link\" onclick=\"iAC('%1s');\" href=\"javascript:;\">%1s</a></td></tr>",fam,$7,$5,$3,$3)
            symbol[i]=$3
            name[i]=$5
            found[$5]=1
        }
}
END {
      imax=i
      delete conv_index
      sort_nD(family,conv_index,"<","no","v","","","","","",1,imax)

      # Open "character-list" file for writing
      output_file1 = "../../character-selector"

      # Append the HTML and JavaScript lines to output_file1
      print "<style scoped>" > output_file1
      print ".char-link {text-decoration: none; color: blue;}" >> output_file1
      print ".char-table {" >> output_file1
      print "  border-collapse: collapse;" >> output_file1
      print "  width: 100%;" >> output_file1
      print "}" >> output_file1
      print "" >> output_file1
      print ".char-td, .char-th {" >> output_file1
      print "  border: 1px solid #dddddd;" >> output_file1
      print "  padding: 8px" >> output_file1
      print "}" >> output_file1
      print "</style>" >> output_file1
      print "" >> output_file1
      print "<script>" >> output_file1
      print "   //from http://jsfiddle.net/Znarkus/Z99mK/" >> output_file1
      print "   function iAC(myValue) {" >> output_file1
      print "      var myField = document.getElementById('input-text');" >> output_file1
      print "      console.log(myField);" >> output_file1
      print "      if (document.selection) {" >> output_file1
      print "         myField.focus();" >> output_file1
      print "         sel = document.selection.createRange();" >> output_file1
      print "         sel.text = myValue;" >> output_file1
      print "      }" >> output_file1
      print "      else if (myField.selectionStart || myField.selectionStart == '0') {" >> output_file1
      print "         var startPos = myField.selectionStart;" >> output_file1
      print "         var endPos = myField.selectionEnd;" >> output_file1
      print "         myField.value = myField.value.substring(0, startPos)" >> output_file1
      print "            + myValue" >> output_file1
      print "            + myField.value.substring(endPos, myField.value.length);" >> output_file1
      print "         myField.selectionStart = startPos + myValue.length;" >> output_file1
      print "         myField.selectionEnd = startPos + myValue.length;" >> output_file1
      print "      } else {" >> output_file1
      print "         myField.value += myValue;" >> output_file1
      print "      }" >> output_file1
      print "   }" >> output_file1
      print "</script>" >> output_file1
      print "" >> output_file1
      print "<table class=\"char-table\">" >> output_file1
      print "<tr><th class=\"char-th\">Parent aminoacid</th><th class=\"char-th\">Modified Aminoacid</th><th class=\"char-th\">Symbol</th></tr>" >> output_file1
      #i=0; while(i<imax) {i=i+1;print list[conv_index[i]] >> output_file1 }
      # We prefer to list the "-" ones after the big amino acid families
      i=0; while(i<imax) {i=i+1;if(family[conv_index[i]]!~/-/) {print list[conv_index[i]] >> output_file1 }}
      i=0; while(i<imax) {i=i+1;if(family[conv_index[i]]~/-/) {print list[conv_index[i]] >> output_file1 }}
      print "</table>" >> output_file1
      close(output_file1)
      
      # output_file2
      output_file2 = "../../character-selector-list"
      #i=0; while(i<imax) {i=i+1;print family[conv_index[i]],name[conv_index[i]],symbol[conv_index[i]] > output_file2 ; print family[conv_index[i]]}
      # We prefer to list the "-" ones after the big amino acid families
      i=0; while(i<imax) {i=i+1; if(family[conv_index[i]]!~/-/) {print family[conv_index[i]],name[conv_index[i]],symbol[conv_index[i]] > output_file2 }}
      i=0; while(i<imax) {i=i+1; if(family[conv_index[i]]~/-/) {print family[conv_index[i]],name[conv_index[i]],symbol[conv_index[i]] > output_file2 }}
      close(output_file2)
} 
