function read_defensins_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,v) {
	limit=2000
	                a=1;
        limit_=a+limit
                path="DATABASES/Defensins/Defensinsfiles/Defe"
                length_numb=3
                add_tag="no"
                split(path,aa,"/")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file=path""tag""a
			if (system("[ -f " file " ] ") ==0 ) {
#seq
				input_file=file
					field=read_database_line(input_file,"Protein Sequence",0,"</td><td>","</td></tr><tr",1)
					gsub("_","",field)
					field=clean_string(field)
					gsub("<td><tr><tbody>","",field)
					seq_a[a]=field
					print "Reading "file" for sequence "seq_a[a]""
					seq_a[a]=analyze_sequence(seq_a[a])
#name
					field=read_database_line(input_file,"Protein",2,"\">","</td>",1)
					field=clean_string(field)
					if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";"}}
#ID
					field=a
					if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"Defe"field";" }								
#family
                                field=read_database_line(input_file,"Family",2,"\">","</td>",1)
                                field=clean_string(field)
                                if(Family_[seq_a[a]]!~field) {if(field!="") {Family_[seq_a[a]]=Family_[seq_a[a]]""field";"}}
#source
                                field=read_database_line(input_file,"Organism",2,"\">","</td>",1)
                                field=clean_string(field)
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";"}}
#gene
                                field=read_database_line(input_file,"Chromosome_Location",0,"</td><td>","</td></tr>",1)
                                field=clean_string(field)
                                if(gene_[seq_a[a]]!~field) {if(field!="") {gene_[seq_a[a]]=gene_[seq_a[a]]""field";"}}
#activity
				name=1; b=0; while(name!="") {b=b+1
				field=read_database_line(input_file,"Microbe_Type",6,"</td><td_width=\"160\">","</td></tr>",b)
				split(field,aa,"</td><td>")
				name=aa[1]
				if(aa[2]!="") {
				if(aa[2]!~/microM/) {
					split(aa[2],bb,"/mL"); 
					split(bb[1],cc,"<br>")
					activity=cc[1]"/mL"
				} 
				else 
				{split(aa[2],bb,"microM"); 
					split(bb[1],cc,"<br>")
					activity=cc[1]"uM"
				}
				field4=name"("activity")"
				}
				else {field4=name}
				field4=analyze_organism(field4,seq_a[a])
				string_to_compare=escape_pattern(field4)
				if(all_organisms_[seq_a[a]]!~string_to_compare) {if(field4!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""field4";"}}
				}
				
# target
				field=read_database_line(input_file,"Target",0,"TARGET\">","</a",1)
				field=clean_string(field)
				if(target_[seq_a[a]]!~field) {if(field!="") {target_[seq_a[a]]=target_[seq_a[a]]""field";"}}
#Experimental structure/PDB
                       field=1; b=0; while(field!="") {b=b+1
                                field=read_database_line(input_file,">PDB:",0,"_","</a>",b)
                                field=clean_string(field)
                                field=toupper(field)
                        if(field!="") {
                        	#experimental_[seq_a[a]]="experimental"
                        	if(experim_structure_[seq_a[a]]!~field) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}}}

			 field=1; b=0; while(field!="") {b=b+1
                                field=read_database_line(input_file,">PDB:",0,"_","</a>",b)
                                field=clean_string(field)
                                field=toupper(field)
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}}
#PMID
	                field=1; b=0; while(field!="") {b=b+1
				field=read_database_line(input_file,">PMID",0,":_","</a",b)
				gsub("_","",field)
				if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}
			}
		}

		# "Manually curated"
		# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1669742/
#		experimental_[seq_a[a]]="experimental"

			close(input_file)
			a=a+1
			emax=a
		}
return emax
}
