function color(s,col,bg,style) {
# see http://misc.flogisoft.com/bash/tip_colors_and_formatting

    fgcolours[""] = 0;

    fgcolours["red"] = 31;
    fgcolours["green"] = 32;
    fgcolours["yellow"] = 33;
    fgcolours["blue"] = 34;
    fgcolours["magenta"] ="38;5;13" ;
    fgcolours["cyan"] = 36;
    fgcolours["white"] = 37;
    fgcolours["light_gray"] ="38;5;244"
    fgcolours["gray"] = 90;
    fgcolours["dark_gray"] ="38;5;235"
    fgcolours["black"] ="38;5;0"
    fgcolours["dark_blue"] ="38;5;20"
    fgcolours["orange"] ="38;5;202"
    fgcolours["dark_green"] ="38;5;22"
    fgcolours["pale_violet"] ="38;5;63"
    fgcolours["gold"] ="38;5;214"
    fgcolours["brown"] ="38;5;52"
    fgcolours["red_yellow1"] ="38;5;9"	
    fgcolours["red_yellow2"] ="38;5;202"
    fgcolours["red_yellow3"] ="38;5;214"
    fgcolours["red_yellow4"] ="38;5;221"
    fgcolours["blue_cyan0"] ="38;5;17"
    fgcolours["blue_cyan1"] ="38;5;20"
    fgcolours["blue_cyan2"] ="38;5;27"
    fgcolours["blue_cyan3"] ="38;5;39"
    fgcolours["blue_cyan4"] ="38;5;45"
    fgcolours["green_yellow1"] ="38;5;22"
    fgcolours["green_yellow2"] ="38;5;28"
    fgcolours["green_yellow3"] ="38;5;34"
    fgcolours["green_yellow4"] ="38;5;46"
    fgcolours["green_yellow5"] ="38;5;214"
    




    bgcolours[""] = 1;
    bgcolours["red"] = 41;
    bgcolours["green"] = 42;
    bgcolours["yellow"] = 43;
    bgcolours["blue"] = 44;
    bgcolours["magenta"] ="48;5;13";
    bgcolours["cyan"] = 46;
    bgcolours["white"] = 47;
    bgcolours["ligth_gray"] ="48;5;244"
    bgcolours["gray"] = 100;
    bgcolours["dark_gray"] ="48;5;235"
    bgcolours["black"] ="48;5;0"
    bgcolours["dark_blue"] ="48;5;20"
    bgcolours["orange"] ="48;5;202"
    bgcolours["dark_green"] ="48;5;22"
    bgcolours["pale_violet"] ="48;5;63"
    bgcolours["gold"] ="48;5;214"
    bgcolours["brown"] ="48;5;52"
    bgcolours["red_yellow1"] ="48;5;9" 
    bgcolours["red_yellow2"] ="48;5;202"
    bgcolours["red_yellow3"] ="48;5;214"
    bgcolours["red_yellow4"] ="48;5;221"
    bgcolours["red_yellow5"] ="48;5;224"
    bgcolours["blue_cyan1"] ="48;5;20"
    bgcolours["blue_cyan2"] ="48;5;27"
    bgcolours["blue_cyan3"] ="48;5;39"
    bgcolours["blue_cyan4"] ="48;5;45"
    bgcolours["green_yellow1"] ="48;5;22"
    bgcolours["green_yellow2"] ="48;5;28"
    bgcolours["green_yellow3"] ="48;5;34"
    bgcolours["green_yellow4"] ="48;5;46"
    bgcolours["green_yellow5"] ="48;5;214"

    attributes[""] = 0;
    attributes["normal"] = 0;
    attributes["bold"] = 1;
    attributes["underscore"] = 4;
    attributes["blink"] = 5;
    attributes["reverseVideo"] = 7;
    attributes["concealed"] = 8;

printf "\033["attributes[style]";"bgcolours[bg]";"fgcolours[col]"m"s"\033[0m"

#if (col=="red") {
#        printf "\033[1;31m"s"\033[0m"
#}
#if (col=="green") {
#        printf "\033[1;32m"s"\033[0m"
#}
#if (col=="blue"){
#        printf "\033[1;34m"s"\033[0m"
#}
#if (col=="yellow") {
#        printf "\033[1;33m"s"\033[0m"
#}
#if (col=="violet"){
#        printf "\033[1;35m"s"\033[0m"
#}
}
