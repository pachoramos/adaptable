function ChouFasman(sequ,\
		ave,pp,p,a,b,n,nna,nnb,ave_heli,s,ss,ll,pred,fullpred,aa,bb,predic_a,predic_b,predic_t,nucleation_helix,length_helix,length_helix_left,nucleation_beta,length_beta,length_beta_left,calc_turn) {
	heli["A"]=142 ; beta["A"]=83 ; turn["A"]=66 ; fi["A"]=0.06 ; fip1["A"]=0.076 ; fip2["A"]=0.035 ; fip3["A"]=0.058 ;
	heli["R"]=98 ; beta["R"]=93 ; turn["R"]=95 ; fi["R"]=0.070 ; fip1["R"]=0.106 ; fip2["R"]=0.099 ; fip3["R"]=0.085 ;
	heli["D"]=101 ; beta["D"]=54 ; turn["D"]=146 ; fi["D"]=0.147 ; fip1["D"]=0.110 ; fip2["D"]=0.179 ; fip3["D"]=0.081 ;
	heli["N"]=67 ; beta["N"]=89 ; turn["N"]=156 ; fi["N"]=0.161 ; fip1["N"]=0.083 ; fip2["N"]=0.191 ; fip3["N"]=0.091 ;
	heli["C"]=70 ; beta["C"]=119 ; turn["C"]=119 ; fi["C"]=0.149 ; fip1["C"]=0.050 ; fip2["C"]=0.117 ; fip3["C"]=0.128 ;
	heli["E"]=151 ; beta["E"]=037 ; turn["E"]=74 ; fi["E"]=0.056 ; fip1["E"]=0.060 ; fip2["E"]=0.077 ; fip3["E"]=0.064 ;
	heli["Q"]=111 ; beta["Q"]=110 ; turn["Q"]=98 ; fi["Q"]=0.074 ; fip1["Q"]=0.098 ; fip2["Q"]=0.037 ; fip3["Q"]=0.098 ;
	heli["G"]=57 ; beta["G"]=75 ; turn["G"]=156 ; fi["G"]=0.102 ; fip1["G"]=0.085 ; fip2["G"]=0.190 ; fip3["G"]=0.152 ;
	heli["H"]=100 ; beta["H"]=87 ; turn["H"]=95 ; fi["H"]=0.140 ; fip1["H"]=0.047 ; fip2["H"]=0.093 ; fip3["H"]=0.054 ;
	heli["I"]=108 ; beta["I"]=160 ; turn["I"]=47 ; fi["I"]=0.043 ; fip1["I"]=0.034 ; fip2["I"]=0.013 ; fip3["I"]=0.056 ;
	heli["L"]=121 ; beta["L"]=130 ; turn["L"]=59 ; fi["L"]=0.061 ; fip1["L"]=0.025 ; fip2["L"]=0.036 ; fip3["L"]=0.070 ;
	heli["K"]=114 ; beta["K"]=74 ; turn["K"]=101 ; fi["K"]=0.055 ; fip1["K"]=0.115 ; fip2["K"]=0.072 ; fip3["K"]=0.095 ;
	heli["M"]=145 ; beta["M"]=105 ; turn["M"]=60 ; fi["M"]=0.068 ; fip1["M"]=0.082 ; fip2["M"]=0.014 ; fip3["M"]=0.055 ;
	heli["F"]=113 ; beta["F"]=138 ; turn["F"]=60 ; fi["F"]=0.059 ; fip1["F"]=0.041 ; fip2["F"]=0.065 ; fip3["F"]=0.065 ;
	heli["P"]=57 ; beta["P"]=55 ; turn["P"]=152 ; fi["P"]=0.102 ; fip1["P"]=0.301 ; fip2["P"]=0.034 ; fip3["P"]=0.068 ;
	heli["S"]=77 ; beta["S"]=75 ; turn["S"]=143 ; fi["S"]=0.120 ; fip1["S"]=0.139 ; fip2["S"]=0.125 ; fip3["S"]=0.106 ;
	heli["T"]=83 ; beta["T"]=119 ; turn["T"]=96 ; fi["T"]=0.086 ; fip1["T"]=0.108 ; fip2["T"]=0.065 ; fip3["T"]=0.079 ;
	heli["W"]=108 ; beta["W"]=137 ; turn["W"]=96 ; fi["W"]=0.077 ; fip1["W"]=0.013 ; fip2["W"]=0.064 ; fip3["W"]=0.167 ;
	heli["Y"]=69 ; beta["Y"]=147 ; turn["Y"]=114 ; fi["Y"]=0.082 ; fip1["Y"]=0.065 ; fip2["Y"]=0.114 ; fip3["Y"]=0.125 ;
	heli["V"]=106 ; beta["V"]=170 ; turn["V"]=50 ; fi["V"]=0.062 ; fip1["V"]=0.048 ; fip2["V"]=0.028 ; fip3["V"]=0.053 ;
consecutive="no"
	pp=split(sequ,pept,"")


		delete aa
		delete bb
		delete predic_a
		delete predic_b
		delete predic_t
		delete nucleation_helix
		delete length_helix
		delete length_helix_left
		delete nucleation_beta
		delete length_beta
		delete length_beta_left
		delete calc_turn
# find nucleation of alpha helices and extend them at both ends (out of nna segments, the segment n start in position p with total length length_helix[n] given by a nucleous of 4 + ss residues at the right and ll residues at the left)
		n=0
		aa[1]=0
		p=0 ; while(p<pp) {p=p+1
			a=0;ave=0;ss=0
				s=-1 ; while(s<6) {s=s+1
		ave=ave+heli[pept[s+p]] ; if(p==17) {print p,s,pept[p+s],ave,100*(s+1)}
		if((consecutives!="yes" && ave>100*(s+1) && s>2 && (s+p)<=pp) || (consecutives=="yes" && heli[pept[s+p]]>100 && s==aa[s] && (s+p)<=pp)) {a=a+1 ;aa[s+1]=s+1 ;
			if ((a>3 && consecutives=="yes") || consecutives!="yes") {
				n=n+1; nucleation_helix[n]=p ; print "a",p,pept[p],pept[p+1],pept[p+2],pept[p+3]
					ss=0 ; while(((heli[pept[p+s+ss+1]]+heli[pept[p+s+ss]]+heli[pept[p+s+ss-1]]+heli[pept[p+s+ss-2]])/4)>100 && (p+s+ss)<=pp) {ss=ss+1
					}
				ll=0 ; while(((heli[pept[p-ll-1]]+heli[pept[p-ll]]+heli[pept[p-ll+1]]+heli[pept[p-ll+2]])/4)>100 && (p-ll-1)>0) {ll=ll+1
				}
				length_helix[n]=4+ll+ss; s=6
					length_helix_left[n]=ll
			}
		}
}
p=p+4+ss-1; 
}
nna=n
# find nucleation of beta strands and extend them at both ends (out of nnb segments, the segment n start in position p with total length length_helix[n] given by a nucleous of 4 + ss residues at the right and ll residues at the left)
n=0
bb[1]=0
p=0 ; while(p<pp) {p=p+1
	b=0;ave=0;ss=0
		s=-1 ; while(s<5) {s=s+1
			ave=ave+beta[pept[s+p]] ; 
			if((consecutives=="yes" && ave>100*(s+1) && s>1 && (s+p)<=pp) || (consecutives=="yes" && beta[pept[s+p]]>100 && s==bb[s] && (s+p)<=pp)){b=b+1 ;bb[s+1]=s+1 ; 
				if ((b>2 && consecutives=="yes") || consecutives!="yes") {
					n=n+1; nucleation_beta[n]=p ; print "b",p,pept[p],pept[p+1],pept[p+2],pept[p+3]
						ss=0 ; while(((beta[pept[p+s+ss+1]]+beta[pept[p+s+ss]]+beta[pept[p+s+ss-1]]+beta[pept[p+s+ss-2]])/4)>100 && (p+s+ss)<=pp) {ss=ss+1
						}
						ll=0 ; while(((beta[pept[p-ll-1]]+beta[pept[p-ll]]+beta[pept[p-ll+1]]+beta[pept[p-ll+2]])/4)>100 && (p-ll-1)>0) {ll=ll+1
						}
					length_beta[n]=3+ss+ll ; s=5
					length_beta_left[n]=ll
				}
			}
		}
	p=p+3+ss-1; 
}
nnb=n
# find turns in position p
p=0 ; while(p<pp) {p=p+1
	ave_heli=0
		ave_beta=0
		ave_turn=0
		calc_turn[p]=fi[pept[p]]*fip1[pept[p+1]]*fip2[pept[p+2]]*fip3[pept[p+3]]
		s=-1 ; while(s<4) {s=s+1
				ave_heli=ave_heli+heli[pept[p+s]]
				ave_beta=ave_beta+beta[pept[p+s]]
				ave_turn=ave_turn+turn[pept[p+s]]
		}
	ave_heli=ave_heli/4
		ave_beta=ave_beta/4
		ave_turn=ave_turn/4
		if (calc_turn[p]>0.000075 && ave_turn>100 && ave_turn>ave_beta && ave_turn>ave_heli) {
			predic_t[p]="T"
		}
}
# assign secondary structures alpha if conditions apply (length of segment, comparison of probability with beta)

n=0; while(n<nna) {n=n+1
	ave_heli=0
		ave_beta=0
		s=nucleation_helix[n]-length_helix_left[n]-1; while (s<nucleation_helix[n]+length_helix[n]) {s=s+1
			ave_heli=ave_heli+heli[pept[s]] 
				ave_beta=ave_beta+beta[pept[s]]
		}
	ave_heli=ave_heli/length_helix[n]
		ave_beta=ave_beta/length_helix[n]
		if(length_helix[n]>5 && ave_heli>ave_beta && ave_heli>103) {
			s=nucleation_helix[n]-length_helix_left[n]-1; while (s<nucleation_helix[n]+length_helix[n]) {s=s+1
				predic_a[s]="H"
			}
		}
}
# assign secondary structures beta if conditions apply (length of segment, comparison of probability with alpha)
n=0; while(n<nnb) {n=n+1
	ave_heli=0
		ave_beta=0
		s=nucleation_beta[n]-length_beta_left[n]-1; while (s<nucleation_beta[n]+length_beta[n]) {s=s+1
			ave_heli=ave_heli+heli[pept[s]] 
				ave_beta=ave_beta+beta[pept[s]]
		}
	ave_heli=ave_heli/length_beta[n]
		ave_beta=ave_beta/length_beta[n]
	#	if(ave_beta>ave_heli || ave_beta>100) {
	#		s=nucleation_beta[n]-length_beta_left[n]-1; while (s<nucleation_beta[n]+length_beta[n]) {s=s+1
	#			predic_b_easy[s]="B"
	#		}
	#	}
	if(ave_beta>105 && ave_beta>ave_heli) {
		s=nucleation_beta[n]-length_beta_left[n]-1; while (s<nucleation_beta[n]+length_beta[n]) {s=s+1
			predic_b[s]="B"
		}
	}
}
# write the prediction; for segments with multiple assignment compare probabilities
pred=""
p=0 ; while(p<pp) {p=p+1
	ave_heli=0
		ave_beta=0
		if (predic_t[p]=="" && predic_a[p]=="" && predic_b[p]=="") {pred=pred"-"}
		else {
			if (predic_t[p]!="") {pred=pred"T"}
			else {
				if(predic_a[p]!="" && predic_b[p]=="") {pred=pred"H"}
				if(predic_b[p]!="" && predic_a[p]=="") {pred=pred"B"}
				if (predic_a[p]!="" && predic_b[p]!="") {
					s=0; while(predic_a[p+s]!="" && predic_b[p+s]!="")
					{
						ave_heli=ave_heli+heli[pept[p+s]]
							ave_beta=ave_beta+beta[pept[p+s]]
					s=s+1
					}
					ss=s
						ave_heli=ave_heli/ss
						ave_beta=ave_beta/ss
						if(ave_heli>=ave_beta) {s=0; while(s<ss) {s=s+1; pred=pred"H"}}
						else {s=0; while(s<ss) {s=s+1; pred=pred"B"}}
					p=p+ss-1
				}
			}
		}
}
#	print sequ
ll=split(pred,tmp11,"")
print "helix"
l=0;while(l<ll) {l=l+1
	if(predic_a[l]=="H") {color("H","red","","bold")}
	else  {color("-","black","","bold")}
}
print ""
print "strand"
l=0;while(l<ll) {l=l+1
	if(predic_b[l]=="B") {color("B","blue","","bold")}
	else  {color("-","black","","bold")}
}
print ""
#print "strand_biased"
#l=0;while(l<ll) {l=l+1
#	if(predic_b_easy[l]=="B") {color("B","cyan","","bold")}
#	else  {color("-","black","","bold")}
#}
#print ""
print "turn"
l=0;while(l<ll) {l=l+1
	if(predic_t[l]=="T") {color("T","green","","bold")}
	else  {color("-","black","","bold")}
}
print ""
print "Consensus"
l=0;while(l<ll) {l=l+1
	if(tmp11[l]=="H") {color(""tmp11[l]"","red","","bold")}
	if(tmp11[l]=="B") {color(""tmp11[l]"","blue","","bold")}
	if(tmp11[l]=="T") {color(""tmp11[l]"","green","","bold")}
	if(tmp11[l]=="-") {color(""tmp11[l]"","black","","bold")}
}
print ""

fullpred=pred
return fullpred
}

