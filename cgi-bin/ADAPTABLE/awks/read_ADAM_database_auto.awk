function read_ADAM_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {
limit=10000
               a=1;
        limit_=a+limit
                path="DATABASES/ADAM/ADAMfiles/ADAM"
                root_online="https://bioinformatics.cs.ntou.edu.tw/ADAM/adam_info.php?f=ADAM_"
                length_numb=4
                add_tag="yes"
                while(a<limit_) {
			tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
		file=path""tag""a
                        if (system("[ -f " file " ] ") ==0 ) {

			input_file=file
#seq
				field=read_database_line(input_file,"<b>Sequence</b>",0,"</td><td>","</td></tr>",1)
				gsub("&nbsp","",field)
				gsub("<br>","",field)
				gsub("_","",field)
				seq_a[a]=field
print "Reading "file" for sequence "seq_a[a]""
analyze_sequence(seq_a[a])

#ID
                                field=read_database_line(input_file,"ADAM_ID",0,"</td><td>","</td></tr>",1)
                                if(ID_[seq_a[a]]!~field) {if(field!="") {ID_[seq_a[a]]=ID_[seq_a[a]]""field";" }}
				analyze_sequence(seq_a[a])
#name
                                field=read_database_line(input_file,"Description",0,"</td><td>","</td></td>",1)
                                if(field=="-") {field=""}
                                gsub("_AltName:_Full=","",field)
                                gsub("_AltName:_","",field)
                                gsub("_Flags:_Precursor","",field)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"<b>Related_Species</b>",0,"</td><td>","</td></tr>",1)
                                if(field=="-") {field=""}
                                field=clean_string(field)
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
#cyclic                 
                        field=read_database_line(input_file,"Type Modification",0,"black'>","<")
                        if(field!~/None/ && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                        if(field~/cetyl/ && field~/N-term/) {if(N_terminus_[seq_a[a]]!~field) {if(field!="") {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}}
                        if(field~/midation/ && field~/C-term/) {if(C_terminus_[seq_a[a]]!~field) {if(field!="") {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}}
                        if(field~/isulfide/) {disulfide_[seq_a[a]]="disulfide";if(PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]"disulfide;"}}}
                        if(field~/lantibio/) {if(PTM_[seq_a[a]]!~/lantibio/) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lantibiotic;"}}}
                        if(field~/lycos/) {if(PTM_[seq_a[a]]!~/lycos/) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]"Glycosylation;"}}}

#Function
			field=1; b=0; while(field!="") {b=b+1
                        field=read_database_line(input_file,"Anti_Microbial_Activity",0,"<center><b>","</b></center>",b)
		gsub("&",",",field); amax=split(field,aa,",")
		aaa=0; while(aaa<amax) {aaa=aaa+1
		# /pattern$/ to only replace Gram- at the end of the string
		if(aa[aaa]~/Gram-/) {gsub(/Gram-$/,"gram-neg",aa[aaa])}
		if(aa[aaa]~/Gram\+/) {gsub("Gram+","gram-pos",aa[aaa])}
                                read_database_classify_field(aa[aaa],",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
				}}
#Taxonomy
                                field="animalia"
                                if(taxonomy_[seq_a[a]]!~field) {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}

				field=read_database_line(input_file,"PHYLUM:",0,"b>_","_<br>",1)
				field=clean_string(field)
        			field=tolower(field)
                                if(taxonomy_[seq_a[a]]!~field) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}}

#all organisms

#Pubmed			
# FIXME: It seems this DB has not references
			field=read_database_line(input_file,"REFERENCE:",1,"pubmed/","\"_target",1)
			# No clean_string or it could kill titles
                        if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}
#experimental
			field=read_database_line(input_file,"Experimental_Evidence",0,"</td><td>","</td></td>",1)
			if(field~/YES/) {experimental_[seq_a[a]]="experimental"}
			if(field~/NO/) {experimental_[seq_a[a]]=experimental_[seq_a[a]]""}
#Experimental structure/PDB
                        field=read_database_line(input_file,"Corresponding PDB",0,"structureId=","\">")
                        field=clean_string(field)
                        field=toupper(field)
                        if(field!="") { if(experim_structure_[seq_a[a]]!~field) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}}
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}
#                        if(field!="") { experimental_[seq_a[a]]="experimental" }
	}
	a=a+1
admax=a
}
return admax
}
