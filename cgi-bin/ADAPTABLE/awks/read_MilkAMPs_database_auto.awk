function read_MilkAMPs_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value,u) {

limit=3000
                a=1;
		u=1;
        limit_=a+limit
                path="DATABASES/MilkAMPs/MilkAMPsfiles/MilK"
                length_numb=4
                add_tag="yes"
                label[1]="ALA";label[2]="CAA";label[3]="CAB";label[4]="BLA";label[5]="CAK";label[6]="LFH";label[7]="LAP";label[8]="WAP";label[9]="GWP";tmax=9
                split(path,aa,"/")
                t=0; while(t<tmax) {t=t+1; a=1
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                while (a<limit_) {
			tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file=path""label[t]""tag""a
                        if (system("[ -f " file " ] ") ==0 ) {

			input_file=file
#seq
				field=read_database_line(input_file,"{'sequence'",0,":'","',",1)
				seq_a[u]=field
				seq_a[u]=analyze_sequence(seq_a[u])
				print "Reading "file" for sequence "seq_a[u]""
#ID
                                field=read_database_line(input_file,"Cited_Entries",0,">","</a",1)
                                if(ID_[seq_a[u]]!~field) {ID_[seq_a[u]]=ID_[seq_a[u]]"MilkAMP"field";" }
#name
                                field=read_database_line(input_file,"MilkAMP database_",0,":_","_details",1)
                                field=clean_string(field)
                                if(name_[seq_a[u]]!~field) {if(field!="") {name_[seq_a[u]]=name_[seq_a[u]]""field";" }}
#source
                                field=read_database_line(input_file,"Producer_Organism",6,"<span>","</span>",1)
				smax=split(field,aa,">")
				string=""
				s=0; while(s<smax) {s=s+1
				if(aa[s]!~/html/ && aa[s]!~/width/) {string=string""aa[s]}
				}
				gsub("</a","",string); gsub("<p","",string);gsub("<i/","",string)
				if(string~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[u]]!~field) {if(field!="") {source_[seq_a[u]]=source_[seq_a[u]]""field";" }}
#stereo
				if(seq_a[u]~/[a-z]/ && seq_a[u]~/[A-Z]/) {field="Mix"}
                                else {if(seq_a[u]~/[a-z]/) {field="D"}
                                        else {if(seq_a[u]~/[A-Z]/) {field="L"}}
                                        }
                                if (stereo_[seq_a[u]]!~field){stereo_[seq_a[u]]=stereo_[seq_a[u]]""field";"}
#Function
                        field=read_database_line(input_file,"<b>Activity</b>",1,"<span>","</span>",1)
			gsub("&",",",field); gsub("_","",field);aamax=split(field,aa,",")
			aaa=0; while(aaa<aamax) {aaa=aaa+1
	              read_database_classify_field(aa[aaa],",",seq_a[u],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
				}
#all organisms
			field1=read_database_line(input_file,"{Gram-positive",0,":","dontcut",1)
			field2=read_database_line(input_file,"{Gram-negative",0,":","dontcut",1)
			field3=read_database_line(input_file,"{Yeasts",0,":","dontcut",1)
			field4=read_database_line(input_file,"{Filamentous fungi",0,":","dontcut",1)
			field5=read_database_line(input_file,"{Parasite",0,":","dontcut",1)
			field=field1","field2","field3","field4","field5
			gsub("subsp.","subsp",field)
			string=read_activity_description(field,"}",seq_a[u],all_organisms_)
#Pubmed			
			field=1; b=0; while(field!="") {b=b+1
                        field=read_database_line(input_file,">Citation:",0,"_/>_","</a></h3>",b)
			gsub("<i>","",field)
			gsub("</i>","",field)
			gsub("<91>","",field)
			gsub("<92>","",field)

                        if(PMID_[seq_a[u]]!~field) {PMID_[seq_a[u]]=PMID_[seq_a[u]]""field";"}
	
		}

#Experimental structure/PDB
                        field=read_database_line(input_file,"http://www.rcsb.org/pdb/explore/explore.do",0,"structureId=","target=_blank")
                        field=toupper(field)
                        field=clean_string(field)
                        if(field!="") { if(experim_structure_[seq_a[a]]!~field) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}}
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}

                        # Not all activities are verified: http://milkampdb.org/entrieslist.php?x_Activity=E
#			experimental_[seq_a[a]]="experimental"

	}
       a=a+1; u=u+1
}
amax=u
}
amax=tmax*limit
return amax
}
