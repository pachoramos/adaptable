function align_sequences(seq_type,max_score,OUTPUT_INPUT,ss,seq_ref,seq_,range,method,score_matrix,\
		i,j,p,length_father,length_son) {

	delete score
		type["G"]="G"
		type["A"]="h"
		type["V"]="h"
		type["I"]="h"
		type["L"]="h"
		type["P"]="P"
		type["M"]="h"
		type["W"]="a"
		type["F"]="a"
		type["C"]="C"
		type["Y"]="a"
		type["S"]="l"
		type["T"]="l"
		type["N"]="l"
		type["Q"]="l"
		type["D"]="n"
		type["E"]="n"
		type["R"]="p"
		type["K"]="p"
		type["H"]="a"
		type["Ⓜ"]="Ⓜ"
		type_DSSP["H"]="h"
		type_DSSP["B"]="b"
                type_DSSP["E"]="e"
                type_DSSP["G"]="g"
                type_DSSP["S"]="s"
                type_DSSP["T"]="t"
                type_DSSP["I"]="i"

		
		seq_type[1]=""
		max_p=length(seq_ref)+length(seq_)-1
		length_father=length(seq_ref)
		length_son=length(seq_)
		p=0 ; while(p<max_p) {p=p+1
			j=0; while(j<length_father) {j=j+1
				i=length_son-p+j
				if(method=="simple") {
						if(type[seq_[i]]==type[seq_ref[j]] || seq_[i]==seq_ref[j]) {score[p]=score[p]+1} 
					}
				if(method=="DSSP") {
                                                if(type_DSSP[seq_[i]]==type_DSSP[seq_ref[j]]) {score[p]=score[p]+1} 
                                        }
				if(method=="blosum") { 
					if(score_matrix[seq_[i],seq_ref[j]]=="" && seq_[i]==seq_ref[j]) {score_matrix[seq_[i],seq_ref[j]]=1} 
					score[p]=score[p]+score_matrix[seq_[i],seq_ref[j]]
				}
				if(p==1) {seq_type[1]=seq_type[1]""sprintf(type[seq_ref[j]])}
			}
		}
		if(seq_type[1]=="") {seq_type[1]="-"}
		delete conv_index
                sort_nD(score,conv_index,"@>","no","v","","","","","",1,max_p)
		max_score[ss]=score[conv_index[1]]
		return conv_index[1]-length_son
}
