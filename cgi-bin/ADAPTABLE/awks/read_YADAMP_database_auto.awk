function read_YADAMP_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value,pmax) {
limit=3000
               a=1;
        limit_=a+limit
                path="DATABASES/YADAMP/YADAMPfiles/YADAMP"
                root_online="http://yadamp.unisa.it/showItem.aspx?yadampid="
                length_numb=3
                add_tag="no"
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                while(a<limit_) {
		file=path""tag""a
                        if (system("[ -f " file " ] ") ==0 ) {

			input_file=file
#seq
				field=read_database_line(input_file,"SEQUENCE",1,"<b>","<br/>",1)
				seq_a[a]=field
print "Reading "file" for sequence "seq_a[a]""
analyze_sequence(seq_a[a])

#ID
                                field=read_database_line(input_file,"ID",2,"_______________","&nbsp",1)
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"YADAMP"field";" }
				analyze_sequence(seq_a[a])
#name
                                field=read_database_line(input_file,"NAME",1,">","&nbsp",1)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"SPECIES",1,">","&nbsp",1)
				if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                else {
                                field=clean_string(field)
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
                                }
#cyclic                 
                        field=read_database_line(input_file,"Type Modification",0,"black'>","<")
                        field=clean_string(field)
                        if(field!~/None/ && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                        if(field~/cetyl/ && field~/N-term/) {if(N_terminus_[seq_a[a]]!~field) {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
                        if(field~/midation/ && field~/C-term/) {if(C_terminus_[seq_a[a]]!~field) {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
                        if(field~/isulfide/) {disulfide_[seq_a[a]]="disulfide";if(PTM_[seq_a[a]]!~field) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"disulfide;"}}
                        if(field~/lantibio/) {if(PTM_[seq_a[a]]!~/lantibio/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lantibiotic;"}}
                        if(field~/lycos/) {if(PTM_[seq_a[a]]!~/lycos/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"Glycosylation;"}}
#Function
                field=read_database_line(input_file,"ACTIVITY",0,"b>","<br",1)
                field=clean_string(field)
		gsub("&",",",field); amax=split(field,aa,",")
		aaa=0; while(aaa<amax) {aaa=aaa+1
		# $ for not matching gram-positive
		if(aa[aaa]~/Gram-/) {gsub(/Gram-$/,"gram-neg",aa[aaa])}
		if(aa[aaa]~/Gram\+/) {gsub("Gram+","gram-pos",aa[aaa])}
                                read_database_classify_field(aa[aaa],",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
				}
#Taxonomy
			string=""
                        field1=read_database_line(input_file,"PHYLUM",1,">","&nbsp",1)
			field2=read_database_line(input_file,"CLASS",1,">","&nbsp",1)
			field3=read_database_line(input_file,"ORDER",1,">","&nbsp",1)
			field4=read_database_line(input_file,"FAMILY",1,">","&nbsp",1)
			field5=read_database_line(input_file,"GENUS",1,">","&nbsp",1)
			field6=read_database_line(input_file,"SPECIES",1,">","&nbsp",1)
			if(field1~/ynthetic/) {field1=""}
			if(field2~/ynthetic/) {field2=""}
			if(field3~/ynthetic/) {field3=""}
			if(field4~/ynthetic/) {field4=""}
			if(field5~/ynthetic/) {field5=""}
			if(field6~/ynthetic/) {field6=""}
			if(field1!="" || field2!="" || field3!="" || field4!="" || field5!="" || field6!="") {
				string=field1";"field2";"field3";"field4";"field5";"field6
			}
			string=clean_string(string)
			string=tolower(string)
                        if(taxonomy_[seq_a[a]]!~string) {if(string!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""string";"}}
#all organisms
			string="";field=read_database_line(input_file,"MIC_E._coli",1,"_sibodyMIC\">","&nbsp;",1)
			if (field!="") {activity=get_number_units("MIC="field"μM",seq_a[a]); string="E.coli"activity""}
			string=analyze_organism(string,seq_a[a])
			string_to_compare=escape_pattern(string)
                        if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
                        string="";field=read_database_line(input_file,"MIC_P._aeruginosa",1,"_sibodyMIC\">","&nbsp;",1)
                        if (field!="") {activity=get_number_units("MIC="field"μM",seq_a[a],"MIC"); string="P.aeruginosa"activity""}
                        string=analyze_organism(string,seq_a[a])
                        string_to_compare=escape_pattern(string)
                        if(all_organisms_[seq_a[a]]!~string) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
                        string="";field=read_database_line(input_file,"MIC_S._aureus",1,"_sibodyMIC\">","&nbsp;",1)
                        if (field!="") {activity=get_number_units("MIC="field"μM",seq_a[a],"MIC"); string="S.aureus"activity""}
                        string=analyze_organism(string,seq_a[a])
                        string_to_compare=escape_pattern(string)
                        if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
                        string="";field=read_database_line(input_file,"MIC_S._typhimurium",1,"_sibodyMIC\">","&nbsp;",1)
                        if (field!="") {activity=get_number_units("MIC="field"μM",seq_a[a],"MIC"); string="S.typhimurium"activity""}
                        string=analyze_organism(string,seq_a[a])
                        string_to_compare=escape_pattern(string)
                        if(all_organisms_[seq_a[a]]!~string) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
                        string="";field=read_database_line(input_file,"MIC_M._luteus",1,"_sibodyMIC\">","&nbsp;",1)
                        if (field!="") {activity=get_number_units("MIC="field"μM",seq_a[a],"MIC"); string="M.luteus"activity""}
                        string=analyze_organism(string,seq_a[a])
                        string_to_compare=escape_pattern(string)
                        if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
                        string="";field=read_database_line(input_file,"MIC_B._subtilis",1,"_sibodyMIC\">","&nbsp;",1)
                        if (field!="") {activity=get_number_units("MIC="field"μM",seq_a[a],"MIC"); string="B.subtilis"activity""}
                        string=analyze_organism(string,seq_a[a])
                        string_to_compare=escape_pattern(string)
                        if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
                        string="";field=read_database_line(input_file,"MIC_C._albicans",1,"_sibodyMIC\">","&nbsp;",1)
                        if (field!="") {activity=get_number_units("MIC="field"μM",seq_a[a],"MIC"); string="C.albicans"activity""}
                        string=analyze_organism(string,seq_a[a])
                        string_to_compare=escape_pattern(string)
                        if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
			string="";field=read_database_line(input_file,"gram+",1,"_sibodyMIC\">","&nbsp;",1)
                        if (field!="") {antigram_pos_[seq_a[a]]="antigram_pos"
                                gsub("__","_",field)
				gsub(/^_/,"",field)
				pmax=split(field,aa,";"); p=0; while(p<pmax) {
				p=p+1; split(aa[p],bb,"="); 
				if(bb[2]!="") {activity=get_number_units("MIC="bb[2]"μM",seq_a[a],"MIC"); string=bb[1]""activity""}
				else {string=bb[1]}
				        gsub("__","_",string)
				        gsub(/^_/,"",string)
					string=analyze_organism(string,seq_a[a])
					string_to_compare=escape_pattern(string)
				if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
					}
			}
                        string="";field=read_database_line(input_file,"gram-",1,"_sibodyMIC\">","&nbsp;",1)
                        if (field!="") {antigram_neg_[seq_a[a]]="antigram_neg"
                                gsub("__","_",field)
				gsub(/^_/,"",field)
				pmax=split(field,aa,";"); p=0; while(p<pmax) {
                                p=p+1; split(aa[p],bb,"="); 
                                if(bb[2]!="") {activity=get_number_units("MIC="bb[2]"μM",seq_a[a],"MIC"); string=bb[1]""activity""}
                                else {string=bb[1]}
                                        gsub("__","_",string)
                                        gsub(/^_/,"",string)
					string=analyze_organism(string,seq_a[a])
					string_to_compare=escape_pattern(string)
				if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
                                        }
                        }

#Experimental structure/PDB
                        field=read_database_line(input_file,"http://www.rcsb.org/pdb/explore/explore.do",0,"structureId=","target")
                        field=toupper(field)
                        field=clean_string(field)
                        if(field!="") { if(experim_structure_[seq_a[a]]!~field) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}}
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}

# https://www.sciencedirect.com/science/article/pii/S0924857912000106
#https://ars.els-cdn.com/content/image/1-s2.0-S0924857912000106-mmc1.doc
                        experimental_[seq_a[a]]="experimental"

#Pubmed			
                        field=read_database_line(input_file,"TITLE_PAPER",1,"href=\"","\">",1)
                        gsub("/?tool=pubmed","",field) # Drop false PMID positives
                        if (field~/pubmed/) {
                                gsub("?","/",field)
                                g=split(field,gg,"/")
                                field=gg[g]
                                gsub("term=","",field)
                                g=split(field,gg,"=")
                                field=gg[g]
                                gsub("search&","",field)
                                gsub("%3A","",field)
                                gsub("%2C","",field)
                                # Drop shit
                                if (field~/%20/) {
                                        # Behave the same as we do for broken links
                                        field=read_database_line(input_file,"TITLE_PAPER",1,"href=\"","</a>")
                                        g=split(field,gg,">")
                                        field=gg[g]
                                }
                                gsub("%5Buid%5D","",field)
                        } else {
                        # It seems that most of the other links are broken, hence, get the title of the paper and rely on ADAPTABLE web paged to get proper links from Google
                        field=read_database_line(input_file,"TITLE_PAPER",1,"href=\"","</a>")
                        g=split(field,gg,">")
                        field=gg[g]
                        }
                        if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}


	}
       close(input_file)
	a=a+1
file="DATABASES/DBAASP/DBAASPfiles/DBAASP_"a
yamax=a
}
return yamax
}
