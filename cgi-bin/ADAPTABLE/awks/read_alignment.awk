function read_alignment(alignment,name_seqs,DSSP_,alignment_transl,work_in_simplified_aa_space,\
		r,c,columns,rows,frq)
{
read_aa_prop_and_colors(aa_name,conv_aa,class,class_def,class_length,type,polarity,color_type)
		delete count
		delete column
		rows=length_nD(alignment,1)
		r=0; while(r<rows) {r=r+1
			if(work_in_simplified_aa_space!="n") {alignment[r]=alignment[r]";"alignment_transl[r]}
			columns=split(alignment[r],column,"")
				c=0; while(c<columns) {c=c+1
					count[c,column[c]]=count[c,column[c]]+1
				}
		}
	print ""
	print "Colouring by frequency"
	color("100 % ; ","black",color_type["freq_100"],"bold") ;
	color("99-80 % ; ","black",color_type["freq_80"],"bold") ;
	color("79-50 % ; ","black",color_type["freq_50"],"bold") ;
	color("49-30 % ; ","black",color_type["freq_30"],"bold") ;
	color("29-10 % ; ","black",color_type["freq_10"],"bold") ;
	print ""
		print ""
		r=0; while(r<rows) {r=r+1
			printf("%4d. ",r)
			columns=split(alignment[r],column,"")
				c=0; while(c<columns) {c=c+1
					frq=count[c,column[c]]/rows*100
						if(frq>=10 && column[c]!="-") {
							if(frq==100) {color(column[c],"black",color_type["freq_100"],"bold")}
							if(frq<100 && frq>=80) {color(column[c],"black",color_type["freq_80"],"bold")}
							if(frq<80 && frq>=50) {color(column[c],"black",color_type["freq_50"],"bold")}
							if(frq<50 && frq>=30) {color(column[c],"black",color_type["freq_30"],"bold")}
							if(frq<30 && frq>=10) {color(column[c],"black",color_type["freq_10"],"bold")}
						} 
						else {color(column[c],"black","","")}
				}
			split(name_seqs[r],tmp,";") ; print " ; "tmp[1]#"\t fff" # Extract script doesn't rely on this fff/rrr... for a long time
		}
	print ""
	print "Colouring by residue type"
	color("Hydrophobic ; ",color_type["h"],"","bold") ;
	color("Aromatic ; ",color_type["a"],"","bold") ;
	color("Polar ; ",color_type["l"],"","bold") ;
	color("Positive ; ",color_type["p"],"","bold") ;
	color("Negative ; ",color_type["n"],"","bold") ;
	color("Glycine ; ",color_type["G"],"","bold") ;
	color("Proline ; ",color_type["P"],"","bold") ;
	color("Cysteine.",color_type["C"],"","bold") ;
	print ""
		print ""
		r=0; while(r<rows) {r=r+1
			printf("%4d. ",r)
			columns=split(alignment[r],column,"")
				c=0; while(c<columns) {c=c+1
					color(column[c],color_type[type[column[c]]],"","bold")
				}
		#	print " ; "name_seqs[r]" rrr"
		split(name_seqs[r],tmp,";") ; print " ; "tmp[1]#"\t rrr"
		}
	print ""
	print "Colouring by polarity"
	color("Non polar ; ",color_type["non_polar"],"","bold") ;
	color("Essentially non polar ; ",color_type["essentially_non_polar"],"","bold") ;
	color("Polar ; ",color_type["polar"],"","bold") ;
	color("Charged ; ",color_type["charged"],"","bold") ;
	color("Cysteine.",color_type["C"],"","bold") ;
        helix_[1]="/"; helix_[2]="¨"; helix_[3]="\\"; helix_[4]="/"; helix_[5]="¨"; helix_[6]="\\"; helix_[7]="_"
	print ""
		print ""
		columns=split(alignment[1],column,"")
		printf("      ");c=0; while(c<columns) {c=c+1; h=h+1; if(h>7) {h=1}; color(helix_[h],"red","","bold")}
		print ""
		r=0; while(r<rows) {r=r+1
			printf("%4d. ",r)
			columns=split(alignment[r],column,"")
				c=0; while(c<columns) {c=c+1
					color(column[c],color_type[polarity[column[c]]],"","bold")
				}
			# print " ; "name_seqs[r]" ppp"
			split(name_seqs[r],tmp,";") ; print " ; "tmp[1]#"\t ppp"
		}
        print ""
        print "Secondary structure prediction"
        color("Alpha-helix ; ","red","","bold") ;
        color("Beta-strand; ","blue","","bold") ;
        color("Turn ; ","green","","bold") ;
        print ""

        print ""
        r=0; while(r<rows) {r=r+1
		printf("%4d. ",r)
		split(name_seqs[r],tmp,";")
                if(work_in_simplified_aa_space!="n") {
                        pr=ChouFasman(alignment_transl[r],"short",tmp[1],"")#"\t sss"
                } else {
                        pr=ChouFasman(alignment[r],"short",tmp[1],"")#"\t sss"
                }
        }
	print ""

	print "DSSP structure"
	color("Alpha-helix ; ","red_yellow1","","bold") ;
        color("Alpha-310 ; ","red_yellow2","","bold") ;
        color("Alpha-pi ; ","red_yellow3","","bold") ;
        color("Beta-bridge; ","blue_cyan1","","bold") ;
	color("Beta-strand; ","blue_cyan2","","bold") ;
        color("Turn ; ","green_yellow1","","bold") ;
        color("Bend ; ","green_yellow2","","bold") ;
        print ""

	print ""
        r=0; while(r<rows) {r=r+1
		printf("%4d. ",r)
		split(name_seqs[r],tmp,";")
		if(DSSP_[r]=="_" || DSSP_[r]=="") {DSSP_[r]="X"}
                if(work_in_simplified_aa_space!="n") {
                        pr=ChouFasman(alignment_transl[r],"short",tmp[1],DSSP_[r])#"\t sss"
                } else {
                        pr=ChouFasman(alignment[r],"short",tmp[1],DSSP_[r])#"\t sss"
                }
	}

	print "__________"
}
