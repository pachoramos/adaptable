@include "awks/library.awk"
BEGIN{
	jjj=0
		assigned_codes="DATABASES/charDB_assigned"
		datafile="DATABASES/Sidechain_database_PubChem_global"
		printf("") > datafile
		macro="awks/modified_aa_macro_global"
		printf("") > macro
		filechar="DATABASES/charDB"
		while ( getline < filechar >0 ) {
			split($1,char,"")
		}
	close(filechar)
}
{
	name=$1
		analise_compound_name(name,"",weight,simplification,reconstructed_name)
#print name,weight[name],simplification[name],reconstructed_name[name]
		gsub("acid","-acid",reconstructed_name[name])
		# For "unknown" and "any" we leave it with X
		if(name!="" && name!="unknown" && name!="any" && name!="0") {
		        # better_name allows to directly set a name that will be found in pubchem for specific cases
		        better_name=""
		        # First try to rely on autocomplete PubChem API to find the right synonym
		        url = "https://pubchem.ncbi.nlm.nih.gov/rest/autocomplete/Compound/" name
		        command = "curl -s \"" url "\""
		        while ((command | getline response) > 0) {
		                if (response ~ /"compound"/) {
		                        if ((command | getline response) > 0) {
		                                match(response, /"([^"]+)"/, matches)
		                                better_name = matches[1]
		                                break
                                        }
                                }
                        }
                        close(command)
                        print "Better name found for "name": "better_name
                        
		        if(name=="methyl-glutamicacid"){better_name="n-methyl-l-glutamicacid"} 
        	        if(name=="chloro-lysine"){better_name="chlorolysine"}
        	        if(name=="1-amino-isobutyricacid"){better_name="2-amino-isobutyricacid"}
        	        if(name=="diphenylalanine"){better_name="SAUDSWFPPKSVMK-LBPRGKRZSA-N"}
        	        if(name=="gamma_carboxylic_glutamic_acid"){better_name="gamma_carboxy_glutamic_acid"}
        	        if(name=="methylgroup"){better_name="methane"}
        	        if(name=="ch3"){better_name="methane"}
        	        if(name=="ch2ch3"){better_name="ethyl_radical"}
        	        if(name=="chch32"){better_name="isopropyl_radical"}
        	        if(name=="ch2chch32"){better_name="isobutyl_radical"}
        	        if(name=="ch2c6h5"){better_name="benzyl_radical"}
        	        if(name=="c16h28o5n2"){better_name="C16H28O5N2"}
        	        if(name=="c16h28o5n3"){better_name="C16H28O5N3"}
        	        if(name=="och3"){better_name="methoxy_radical"}
        	        if(name=="ch2-nh-coch3"){better_name="acetamidomethyl"}
        	        # FIXME: ,r* are bogus, shouldn't be generated
        	        if(name=="ch3chch3ch2ch3,r2"){better_name="2-methylbutane"}
        	        if(name=="ch3chch3ch2ch3,r4"){better_name="2-methylbutane"}
        	        if(name=="c6h6ch2ch3,r2"){better_name="ethylbenzene"}
        	        if(name=="dalpha-aminobutyrate"){better_name="D-2-Aminobutyrate"}
        	        if(name=="methyl-glu"){better_name="N-Methyl glutamic acid"}
        	        if(name=="beta-homoarginine"){better_name="AC1NDZPH"}
        	        if(name=="lysinenepsilon-trimethylated"){better_name="Epsilon-Trimethyllysine"}
        	        if(name=="r-2-7-octenylalanine"){better_name="ACN-044458"}
        	        if(name=="methoxy-glu"){better_name="methoxy_glutamic_acid"}
        	        if(name=="d-beta-naphthylalanine"){better_name="beta-Naphthyl-D-ala"}
        	        if(name=="tetrahydroisoquinolinecarboxylicacid"){better_name="D-Tetrahydroisoquinoline-3-carboxylic acid"}
        	        if(name=="d-piperazicacid"){better_name="AC1LVURJ"}
        	        if(name=="2,2,6,6-tetramethylpiperidine-1-oxyl-4-amino-4-carboxylic"){better_name="TOAC"}
        	        if(name=="1r"){better_name="13304-EP2308812A2"}
        	        if(name=="acetylateddiaminobutyrate"){better_name="n-gamma-acetyldiaminobutyrate"}
        	        if(name=="9-anthracenyl-alanine"){better_name="2-(9-Anthracenyl)-L-Alanine"}
        	        if(name=="benzoyl"){better_name="benzoyl_radical"}
        	        if(name=="methyl"){better_name="methyl_radical"}
        	        if(name=="myristoyl"){better_name="Myristic_acid"}
        	        if(name=="succinyl"){better_name="Succinic_acid"}
        	        if(name=="carbobenzoxy"){better_name="carbobenzoxy_group"}
        	        if(name=="acetyl"){better_name="acetyl_radical"}
        	        if(name=="acteyl"){better_name="acetyl_radical"}
        	        if(name=="4-nitro"){better_name="nitro_radical"}
        	        if(name=="nitro"){better_name="nitro_radical"}
        	        if(name=="4-amino"){better_name="amino_radical"}
        	        if(name=="amino"){better_name="amino_radical"}
        	        if(name=="4-azido"){better_name="azido_radical"}
        	        if(name=="azido"){better_name="azido_radical"}
        	        if(name=="trifloromethyl"){better_name="trifluoromethyl_radical"}
        	        if(name=="trifluoromethyl"){better_name="trifluoromethyl_radical"}
        	        if(name=="4-fluorobenzoyl"){better_name="4-fluorobenzoyl_radical"}
        	        if(name=="4-fluorobenzyl"){better_name="4-fluorobenzyl_radical"}
                        if(name=="2-fluorobenzoyl"){better_name="2-fluorobenzoyl_radical"}
                        if(name=="2-fluorobenzyl"){better_name="2-fluorobenzyl_radical"}
        	        if(name=="nitrile"){better_name="nitrile_anion"}
        	        if(name=="tert-butyloxycarbonyl"){better_name="tert-butyloxycarbonyl_group"}
        	        if(name=="carboxybenzyl"){better_name="carboxybenzyl_radical"}
        	        if(name=="acetylaminoalanine"){better_name="alpha-acetyl-beta-aminoalanine"}
        	        if(name=="dicyclohexylsubstitutedhistidine"){better_name="2-Cyclohexyl-L-Histidine"}
        	        if(name=="disulphide"){better_name="Sulfur_Dimer"}
        	        if(name=="fluoromethylketone"){better_name="1,3-Difluoroacetone"}
        	        if(name=="lysinewitholeoylatedsidegroup"){better_name="N6-Oleoyl_Lysine"}
		        if(better_name!=""){
		                find_compound_in_PubChem(name,20,80,better_name,"","","","",official_name,official_alternative_names,weight,simplification,id)
		                if(official_name[name]=="") {find_compound_in_PubChem(name,20,80,better_name,"complexity","","","",official_name,official_alternative_names,weight,simplification,id)}
                        } else {
        		        find_compound_in_PubChem(name,150,80,"","","","","",official_name,official_alternative_names,weight,simplification,id)
        			if(official_name[name]=="") {find_compound_in_PubChem(name,20,80,reconstructed_name[name],"","","","",official_name,official_alternative_names,weight,simplification,id)}
        			if(official_name[name]=="") {reconstructed_name[name]="d- "substr(name,2,length(name)-1); find_compound_in_PubChem(name,20,90,reconstructed_name[name],"","","","",official_name,official_alternative_names,weight,simplification,id)}
        			if(official_name[name]=="") {find_compound_in_PubChem(name,50,80,"","complexity","","","",official_name,official_alternative_names,weight,simplification,id)}
        			if(official_name[name]=="") {find_compound_in_PubChem(name,20,80,reconstructed_name[name],"complexity","","","",official_name,official_alternative_names,weight,simplification,id)}
                                if(official_name[name]=="") {reconstructed_name[name]="d- "substr(name,2,length(name)-1); find_compound_in_PubChem(name,20,90,reconstructed_name[name],"complexity","","","",official_name,official_alternative_names,weight,simplification,id)}
                                # Last try dropping numbers
                                if(official_name[name]=="") {name_without_numbers=name; gsub(/[0-9]-/,"",name_without_numbers); find_compound_in_PubChem(name,20,90,name_without_numbers,"","","","",official_name,official_alternative_names,weight,simplification,id)}
                                if(official_name[name]=="") {name_without_numbers=name; gsub(/[0-9]-/,"",name_without_numbers); find_compound_in_PubChem(name,20,90,name_without_numbers,"complexity","","","",official_name,official_alternative_names,weight,simplification,id)}
                                # Try without hyphens, at first stricter (removing hyphens), later relaxing to use spaces instead of hyphens
                                if(official_name[name]=="") {name_without_numbers=name; gsub(/[0-9]-/,"",name_without_numbers); find_compound_in_PubChem(name,20,90,name_without_numbers,"","yes","","",official_name,official_alternative_names,weight,simplification,id)}
                                if(official_name[name]=="") {name_without_numbers=name; gsub(/[0-9]-/,"",name_without_numbers); find_compound_in_PubChem(name,20,90,name_without_numbers,"complexity","yes","","",official_name,official_alternative_names,weight,simplification,id)}
                                if(official_name[name]=="") {name_without_numbers=name; gsub(/[0-9]-/,"",name_without_numbers); find_compound_in_PubChem(name,20,90,name_without_numbers,"","","yes","",official_name,official_alternative_names,weight,simplification,id)}
                                if(official_name[name]=="") {name_without_numbers=name; gsub(/[0-9]-/,"",name_without_numbers); find_compound_in_PubChem(name,20,90,name_without_numbers,"complexity","","yes","",official_name,official_alternative_names,weight,simplification,id)}
                        }
				if(official_name[name]!="") {
				if(unichar[official_name[name]]=="") {jjj=jjj+1; unichar[official_name[name]]=char[jjj]}
						print official_name[name],official_alternative_names[name]
						print "MW:"weight[name],"Simplified name:"simplification[name]
						print""
						gsub(" ","_",official_alternative_names[name])
						if (official_name[name]=="") {official_name[name]="_"}
					if (official_alternative_names[name]=="") {official_alternative_names[name]="_"}
					if (weight[name]=="") {weight[name]="_"}
					if (simplification[name]=="") {simplification[name]="_"}
					if(code=="") {code="_"}

					if(official_name[name]=="Alanine") {unichar[official_name[name]]="A"}					
                                        if(official_name[name]=="alanine") {unichar[official_name[name]]="A"}                                   
					if(official_name[name]=="L-Alanine") {unichar[official_name[name]]="A"}
                                        if(official_name[name]=="L-alanine") {unichar[official_name[name]]="A"}
                                        if(official_name[name]=="D-Alanine") {unichar[official_name[name]]="a"}
                                        if(official_name[name]=="D-alanine") {unichar[official_name[name]]="a"}
					if(official_name[name]=="Alanine,_D-") {unichar[official_name[name]]="a"}

					if(official_name[name]=="Valine") {unichar[official_name[name]]="V"}
                                        if(official_name[name]=="valine") {unichar[official_name[name]]="V"}
                                        if(official_name[name]=="L-Valine") {unichar[official_name[name]]="V"}
					if(official_name[name]=="L-valine") {unichar[official_name[name]]="V"}
                                        if(official_name[name]=="D-Valine") {unichar[official_name[name]]="v"}
					if(official_name[name]=="D-valine") {unichar[official_name[name]]="v"}
                                        if(official_name[name]=="Valine,_D-") {unichar[official_name[name]]="v"}

                                        if(official_name[name]=="Leucine") {unichar[official_name[name]]="L"}
					if(official_name[name]=="leucine") {unichar[official_name[name]]="L"}
                                        if(official_name[name]=="L-Leucine") {unichar[official_name[name]]="L"}
					if(official_name[name]=="L-leucine") {unichar[official_name[name]]="L"}
                                        if(official_name[name]=="D-Leucine") {unichar[official_name[name]]="l"}
					if(official_name[name]=="D-leucine") {unichar[official_name[name]]="l"}
                                        if(official_name[name]=="Leucine,_D-") {unichar[official_name[name]]="l"}

                                        if(official_name[name]=="Isoleucine") {unichar[official_name[name]]="I"}
                                        if(official_name[name]=="isoleucine") {unichar[official_name[name]]="I"}
                                        if(official_name[name]=="L-Isoleucine") {unichar[official_name[name]]="I"}
                                        if(official_name[name]=="L-isoleucine") {unichar[official_name[name]]="I"}
                                        if(official_name[name]=="D-Isoleucine") {unichar[official_name[name]]="i"}
                                        if(official_name[name]=="D-isoleucine") {unichar[official_name[name]]="i"}
                                        if(official_name[name]=="Isoleucine,_D-") {unichar[official_name[name]]="i"}

                                        if(official_name[name]=="Proline") {unichar[official_name[name]]="P"}
                                        if(official_name[name]=="proline") {unichar[official_name[name]]="P"}
                                        if(official_name[name]=="L-Proline") {unichar[official_name[name]]="P"}
                                        if(official_name[name]=="L-proline") {unichar[official_name[name]]="P"}
                                        if(official_name[name]=="D-Proline") {unichar[official_name[name]]="p"}
                                        if(official_name[name]=="D-proline") {unichar[official_name[name]]="p"}
                                        if(official_name[name]=="Proline,_D-") {unichar[official_name[name]]="p"}


                                        if(official_name[name]=="Phenylalanine") {unichar[official_name[name]]="F"}
                                        if(official_name[name]=="phenylalanine") {unichar[official_name[name]]="F"}
                                        if(official_name[name]=="L-Phenylalanine") {unichar[official_name[name]]="F"}
                                        if(official_name[name]=="L-phenylalanine") {unichar[official_name[name]]="F"}
                                        if(official_name[name]=="D-Phenylalanine") {unichar[official_name[name]]="f"}
                                        if(official_name[name]=="D-phenylalanine") {unichar[official_name[name]]="f"}
                                        if(official_name[name]=="Phenylalanine,_D-") {unichar[official_name[name]]="f"}

                                        if(official_name[name]=="Tryptophan") {unichar[official_name[name]]="W"}
                                        if(official_name[name]=="tryptophan") {unichar[official_name[name]]="W"}
                                        if(official_name[name]=="L-Tryptophan") {unichar[official_name[name]]="W"}
                                        if(official_name[name]=="L-tryptophan") {unichar[official_name[name]]="W"}
                                        if(official_name[name]=="D-Tryptophan") {unichar[official_name[name]]="w"}
                                        if(official_name[name]=="D-tryptophan") {unichar[official_name[name]]="w"}
                                        if(official_name[name]=="Tryptophan,_D-") {unichar[official_name[name]]="w"}

                                        if(official_name[name]=="Methionine") {unichar[official_name[name]]="M"}
                                        if(official_name[name]=="methionine") {unichar[official_name[name]]="M"}
                                        if(official_name[name]=="L-Methionine") {unichar[official_name[name]]="M"}
                                        if(official_name[name]=="L-methionine") {unichar[official_name[name]]="M"}
                                        if(official_name[name]=="D-Methionine") {unichar[official_name[name]]="m"}
                                        if(official_name[name]=="D-methionine") {unichar[official_name[name]]="m"}
                                        if(official_name[name]=="Methionine,_D-") {unichar[official_name[name]]="m"}

                                        if(official_name[name]=="Cysteine") {unichar[official_name[name]]="C"}
                                        if(official_name[name]=="cysteine") {unichar[official_name[name]]="C"}
                                        if(official_name[name]=="L-Cysteine") {unichar[official_name[name]]="C"}
                                        if(official_name[name]=="L-cysteine") {unichar[official_name[name]]="C"}
                                        if(official_name[name]=="D-Cysteine") {unichar[official_name[name]]="c"}
                                        if(official_name[name]=="D-cysteine") {unichar[official_name[name]]="c"}
                                        if(official_name[name]=="Cysteine,_D-") {unichar[official_name[name]]="c"}

                                        if(official_name[name]=="Glycine") {unichar[official_name[name]]="G"}
                                        if(official_name[name]=="glycine") {unichar[official_name[name]]="G"}
                                        if(official_name[name]=="L-Glycine") {unichar[official_name[name]]="G"}
                                        if(official_name[name]=="L-glycine") {unichar[official_name[name]]="G"}
                                        if(official_name[name]=="D-Glycine") {unichar[official_name[name]]="g"}
                                        if(official_name[name]=="D-glycine") {unichar[official_name[name]]="g"}
                                        if(official_name[name]=="Glycine,_D-") {unichar[official_name[name]]="g"}

                                        if(official_name[name]=="Asparagine") {unichar[official_name[name]]="N"}
                                        if(official_name[name]=="asparagine") {unichar[official_name[name]]="N"}
                                        if(official_name[name]=="L-Asparagine") {unichar[official_name[name]]="N"}
                                        if(official_name[name]=="L-asparagine") {unichar[official_name[name]]="N"}
                                        if(official_name[name]=="D-Asparagine") {unichar[official_name[name]]="n"}
                                        if(official_name[name]=="D-asparagine") {unichar[official_name[name]]="n"}
                                        if(official_name[name]=="Asparagine,_D-") {unichar[official_name[name]]="n"}

                                        if(official_name[name]=="Glutamine") {unichar[official_name[name]]="Q"}
                                        if(official_name[name]=="glutamine") {unichar[official_name[name]]="Q"}
                                        if(official_name[name]=="L-Glutamine") {unichar[official_name[name]]="Q"}
                                        if(official_name[name]=="L-glutamine") {unichar[official_name[name]]="Q"}
                                        if(official_name[name]=="D-Glutamine") {unichar[official_name[name]]="q"}
                                        if(official_name[name]=="D-glutamine") {unichar[official_name[name]]="q"}
                                        if(official_name[name]=="Glutamine,_D-") {unichar[official_name[name]]="q"}

                                        if(official_name[name]=="Serine") {unichar[official_name[name]]="S"}
                                        if(official_name[name]=="serine") {unichar[official_name[name]]="S"}
                                        if(official_name[name]=="L-Serine") {unichar[official_name[name]]="S"}
                                        if(official_name[name]=="L-serine") {unichar[official_name[name]]="S"}
                                        if(official_name[name]=="D-Serine") {unichar[official_name[name]]="s"}
                                        if(official_name[name]=="D-serine") {unichar[official_name[name]]="s"}
                                        if(official_name[name]=="Serine,_D-") {unichar[official_name[name]]="s"}

                                        if(official_name[name]=="Threonine") {unichar[official_name[name]]="T"}
                                        if(official_name[name]=="threonine") {unichar[official_name[name]]="T"}
                                        if(official_name[name]=="L-Threonine") {unichar[official_name[name]]="T"}
                                        if(official_name[name]=="L-threonine") {unichar[official_name[name]]="T"}
                                        if(official_name[name]=="D-Threonine") {unichar[official_name[name]]="t"}
                                        if(official_name[name]=="D-threonine") {unichar[official_name[name]]="t"}
                                        if(official_name[name]=="Threonine,_D-") {unichar[official_name[name]]="t"}

                                        if(official_name[name]=="Tyrosine") {unichar[official_name[name]]="Y"}
                                        if(official_name[name]=="tyrosine") {unichar[official_name[name]]="Y"}
                                        if(official_name[name]=="L-Tyrosine") {unichar[official_name[name]]="Y"}
                                        if(official_name[name]=="L-tyrosine") {unichar[official_name[name]]="Y"}
                                        if(official_name[name]=="D-Tyrosine") {unichar[official_name[name]]="y"}
                                        if(official_name[name]=="D-tyrosine") {unichar[official_name[name]]="y"}
                                        if(official_name[name]=="Tyrosine,_D-") {unichar[official_name[name]]="y"}

                                        if(official_name[name]=="Lysine") {unichar[official_name[name]]="K"}
                                        if(official_name[name]=="lysine") {unichar[official_name[name]]="K"}
                                        if(official_name[name]=="L-Lysine") {unichar[official_name[name]]="K"}
                                        if(official_name[name]=="L-lysine") {unichar[official_name[name]]="K"}
                                        if(official_name[name]=="D-Lysine") {unichar[official_name[name]]="k"}
                                        if(official_name[name]=="D-lysine") {unichar[official_name[name]]="k"}
                                        if(official_name[name]=="Lysine,_D-") {unichar[official_name[name]]="k"}

                                        if(official_name[name]=="Arginine") {unichar[official_name[name]]="R"}
                                        if(official_name[name]=="arginine") {unichar[official_name[name]]="R"}
                                        if(official_name[name]=="L-Arginine") {unichar[official_name[name]]="R"}
                                        if(official_name[name]=="L-arginine") {unichar[official_name[name]]="R"}
                                        if(official_name[name]=="D-Arginine") {unichar[official_name[name]]="r"}
                                        if(official_name[name]=="D-arginine") {unichar[official_name[name]]="r"}
                                        if(official_name[name]=="Arginine,_D-") {unichar[official_name[name]]="r"}

                                        if(official_name[name]=="Histidine") {unichar[official_name[name]]="H"}
                                        if(official_name[name]=="histidine") {unichar[official_name[name]]="H"}
                                        if(official_name[name]=="L-Histidine") {unichar[official_name[name]]="H"}
                                        if(official_name[name]=="L-histidine") {unichar[official_name[name]]="H"}
                                        if(official_name[name]=="D-Histidine") {unichar[official_name[name]]="h"}
                                        if(official_name[name]=="D-histidine") {unichar[official_name[name]]="h"}
                                        if(official_name[name]=="Histidine,_D-") {unichar[official_name[name]]="h"}

                                        if(official_name[name]=="Aspartate") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="aspartate") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="L-Aspartate") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="L-aspartate") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="D-Aspartate") {unichar[official_name[name]]="d"}
                                        if(official_name[name]=="D-aspartate") {unichar[official_name[name]]="d"}
                                        if(official_name[name]=="Aspartate,_D-") {unichar[official_name[name]]="d"}

                                        if(official_name[name]=="Asparticacid") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="asparticacid") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="L-Asparticacid") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="L-asparticacid") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="D-Asparticacid") {unichar[official_name[name]]="d"}
                                        if(official_name[name]=="D-asparticacid") {unichar[official_name[name]]="d"}
                                        if(official_name[name]=="Asparticacid,_D-") {unichar[official_name[name]]="d"}
                                        if(official_name[name]=="Aspartic-acid") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="aspartic-acid") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="Aspartic_acid") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="L-Aspartic-acid") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="L-aspartic-acid") {unichar[official_name[name]]="D"}
                                        if(official_name[name]=="D-Aspartic-acid") {unichar[official_name[name]]="d"}
                                        if(official_name[name]=="D-aspartic-acid") {unichar[official_name[name]]="d"}
                                        if(official_name[name]=="Aspartic-acid,_D-") {unichar[official_name[name]]="d"}
                                        if(official_name[name]=="Aspartic_acid,_D-") {unichar[official_name[name]]="d"}
                                        
                                        if(official_name[name]=="Glutamate") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="glutamate") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="L-Glutamate") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="L-glutamate") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="D-Glutamate") {unichar[official_name[name]]="e"}
                                        if(official_name[name]=="D-glutamate") {unichar[official_name[name]]="e"}
                                        if(official_name[name]=="Glutamate,_D-") {unichar[official_name[name]]="e"}

                                        if(official_name[name]=="Glutamicacid") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="glutamicacid") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="L-Glutamicacid") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="L-glutamicacid") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="D-Glutamicacid") {unichar[official_name[name]]="e"}
                                        if(official_name[name]=="D-glutamicacid") {unichar[official_name[name]]="e"}
                                        if(official_name[name]=="Glutamicacid,_D-") {unichar[official_name[name]]="e"}
                                        if(official_name[name]=="Glutamic-acid") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="glutamic-acid") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="L-Glutamic-acid") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="L-glutamic-acid") {unichar[official_name[name]]="E"}
                                        if(official_name[name]=="D-Glutamic-acid") {unichar[official_name[name]]="e"}
                                        if(official_name[name]=="D-glutamic-acid") {unichar[official_name[name]]="e"}
                                        if(official_name[name]=="Glutamic-acid,_D-") {unichar[official_name[name]]="e"}
                                        if(official_name[name]=="Glutamic_acid,_D-") {unichar[official_name[name]]="e"}

                                        if(official_name[name]=="Ornithine") {unichar[official_name[name]]="O"}
                                        if(official_name[name]=="ornithine") {unichar[official_name[name]]="O"}
                                        if(official_name[name]=="L-Ornithine") {unichar[official_name[name]]="O"}
                                        if(official_name[name]=="L-ornithine") {unichar[official_name[name]]="O"}
                                        if(official_name[name]=="D-Ornithine") {unichar[official_name[name]]="o"}
                                        if(official_name[name]=="D-ornithine") {unichar[official_name[name]]="o"}
                                        if(official_name[name]=="Ornithine,_D-") {unichar[official_name[name]]="o"}

                                        if(official_name[name]=="Selenocysteine") {unichar[official_name[name]]="U"}
                                        if(official_name[name]=="selenocysteine") {unichar[official_name[name]]="U"}
                                        if(official_name[name]=="L-Selenocysteine") {unichar[official_name[name]]="U"}
                                        if(official_name[name]=="L-selenocysteine") {unichar[official_name[name]]="U"}
                                        if(official_name[name]=="D-Selenocysteine") {unichar[official_name[name]]="u"}
                                        if(official_name[name]=="D-selenocysteine") {unichar[official_name[name]]="u"}
                                        if(official_name[name]=="Selenocysteine,_D-") {unichar[official_name[name]]="u"}

					onecode=unichar[official_name[name]]
					printf(onecode) >> assigned_codes
						printf("%-5s %1s %10s %4.2f %-40s %-20s %10s \n",code,simplification[name],onecode,weight[name],official_name[name],official_alternative_names[name],id[name]) >> datafile 
						fflush()
						print "translate_modif[\""name"\"]=\""onecode"\";" >> macro
						print "official_name[\""name"\"]=\""official_name[name]"\";" >> macro
						print "weight[\""name"\"]="weight[name]";" >> macro
						print "weight[\""onecode"\"]="weight[name]";" >> macro
						print "simplification[\""name"\"]=\""simplification[name]"\";" >> macro
						print "nature[\""name"\"]=\"non-natural\";" >> macro
						print "id[\""name"\"]="id[name]";" >> macro
						print "" >> macro
						fflush()
				}
				else {print "ERROR: "name" not found in PubChem"; print ""}
		}
}
