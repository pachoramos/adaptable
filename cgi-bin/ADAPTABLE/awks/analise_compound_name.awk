function analise_compound_name(name_,INPUT_OUTPUT,weight_,simplification,reconstructed_name)
{
i=0
        i=i+1;chemical_groups[i]="val"; partial_weight_[i]=117.1
        i=i+1;chemical_groups[i]="isoleuc"; partial_weight_[i]=131.2
        i=i+1;chemical_groups[i]="leuc"; partial_weight_[i]=131.2
        i=i+1;chemical_groups[i]="prol"; partial_weight_[i]=115.1
        i=i+1;chemical_groups[i]="phenylalan"; partial_weight_[i]=165.2
        i=i+1;chemical_groups[i]="alan"; partial_weight_[i]=89.1
        i=i+1;chemical_groups[i]="tryptophan"; partial_weight_[i]=204.2
        i=i+1;chemical_groups[i]="methion"; partial_weight_[i]=149.2
        i=i+1;chemical_groups[i]="cyste"; partial_weight_[i]=121.2
        i=i+1;chemical_groups[i]="glyc"; partial_weight_[i]=75.1
        i=i+1;chemical_groups[i]="asparag"; partial_weight_[i]=132.1
        i=i+1;chemical_groups[i]="glutam"; partial_weight_[i]=146.2
        i=i+1;chemical_groups[i]="ser";  partial_weight_[i]=105.1
        i=i+1;chemical_groups[i]="threon"; partial_weight_[i]=119.1
        i=i+1;chemical_groups[i]="tyros"; partial_weight_[i]=181.2
        i=i+1;chemical_groups[i]="lys";  partial_weight_[i]=146.2
        i=i+1;chemical_groups[i]="argin"; partial_weight_[i]=174.2
        i=i+1;chemical_groups[i]="histid"; partial_weight_[i]=155.2
        i=i+1;chemical_groups[i]="aspart"; partial_weight_[i]=133.1
        i=i+1;chemical_groups[i]="aspartate"; partial_weight_[i]=133.1
        i=i+1;chemical_groups[i]="glutam";partial_weight_[i]=147.1
        i=i+1;chemical_groups[i]="glutamate";partial_weight_[i]=147.1
        i=i+1;chemical_groups[i]="ornith"; partial_weight_[i]=132.16

    #    i=i+1;chemical_groups[i]="oic "; partial_weight_[i]=44
        i=i+1;chemical_groups[i]="acid"; partial_weight_[i]=45
        i=i+1;chemical_groups[i]="meth"; partial_weight_[i]=15
        i=i+1;chemical_groups[i]="eth"; partial_weight_[i]=12*2+5
        i=i+1;chemical_groups[i]="fluor"; partial_weight_[i]=19
        i=i+1;chemical_groups[i]="chloro"; partial_weight_[i]=35
        i=i+1;chemical_groups[i]="bromo"; partial_weight_[i]=35
        i=i+1;chemical_groups[i]="iodo"; partial_weight_[i]=253.8/2
        i=i+1;chemical_groups[i]="amin"; partial_weight_[i]=16
        i=i+1;chemical_groups[i]="carbox"; partial_weight_[i]=45
        i=i+1;chemical_groups[i]="hydroxy"; partial_weight_[i]=17
        i=i+1;chemical_groups[i]="propion"; partial_weight_[i]=74.08
        i=i+1;chemical_groups[i]="propan"; partial_weight_[i]=43
        i=i+1;chemical_groups[i]="propyl"; partial_weight_[i]=43
        i=i+1;chemical_groups[i]="butyr"; partial_weight_[i]=87
        i=i+1;chemical_groups[i]="butyl"; partial_weight_[i]=57
        i=i+1;chemical_groups[i]="butan"; partial_weight_[i]=57
        i=i+1;chemical_groups[i]="benz"; partial_weight_[i]=78
        i=i+1;chemical_groups[i]="phen"; partial_weight_[i]=78
        i=i+1;chemical_groups[i]="naphth"; partial_weight_[i]=128
        i=i+1;chemical_groups[i]="laur"; partial_weight_[i]=200
        i=i+1;chemical_groups[i]="glyco"; partial_weight_[i]=180
        i=i+1;chemical_groups[i]="fructo"; partial_weight_[i]=180
        i=i+1;chemical_groups[i]="manno"; partial_weight_[i]=180
        i=i+1;chemical_groups[i]="sulph"; partial_weight_[i]=96
        i=i+1;chemical_groups[i]="sulf"; partial_weight_[i]=96
        i=i+1;chemical_groups[i]="phosph"; partial_weight_[i]=94.97
        i=i+1;chemical_groups[i]="acet"; partial_weight_[i]=15+12+16
        i=i+1;chemical_groups[i]="amid"; partial_weight_[i]=16
        i=i+1;chemical_groups[i]="nitro"; partial_weight_[i]=16*2+14
        i=i+1;chemical_groups[i]="guanid"; partial_weight_[i]=59.07
        i=i+1;chemical_groups[i]="azid"; partial_weight_[i]=14*3
        i=i+1;chemical_groups[i]="cyan"; partial_weight_[i]=14+12
        i=i+1;chemical_groups[i]="citrull"; partial_weight_[i]=175.18
        i=i+1;chemical_groups[i]="carbam"; partial_weight_[i]=12+16+14+2
        i=i+1;chemical_groups[i]="succin"; partial_weight_[i]=118
        i=i+1;chemical_groups[i]="pent"; partial_weight_[i]=(12+2)*5+1
        i=i+1;chemical_groups[i]="hex"; partial_weight_[i]=(12+2)*6+1
        i=i+1;chemical_groups[i]="ept"; partial_weight_[i]=(12+2)*7+1
        i=i+1;chemical_groups[i]="oct"; partial_weight_[i]=(12+2)*8+1
        i=i+1;chemical_groups[i]="nona"; partial_weight_[i]=(12+2)*9+1
        i=i+1;chemical_groups[i]="undeca"; partial_weight_[i]=(12+2)*11+1
        i=i+1;chemical_groups[i]="dodeca"; partial_weight_[i]=(12+2)*12+1
        i=i+1;chemical_groups[i]="deca"; partial_weight_[i]=(12+2)*10+1
        i=i+1;chemical_groups[i]="myristo"; partial_weight_[i]=(12+2)*14+44-12
        i=i+1;chemical_groups[i]="oleo"; partial_weight_[i]=282
        i=i+1;chemical_groups[i]="oleic"; partial_weight_[i]=282
        i=i+1;chemical_groups[i]="palm"; partial_weight_[i]=256
        i=i+1;chemical_groups[i]="stear"; partial_weight_[i]=284
        i=i+1;chemical_groups[i]="anthracen"; partial_weight_[i]=178.23
        i=i+1;chemical_groups[i]="sarcos"; partial_weight_[i]=89.09
        i=i+1;chemical_groups[i]="glutathion"; partial_weight_[i]=307.32
        i=i+1;chemical_groups[i]="pyruv"; partial_weight_[i]=88.06
        i=i+1;chemical_groups[i]="adamant"; partial_weight_[i]=136.24
        i=i+1;chemical_groups[i]="pyrrolid"; partial_weight_[i]=71.12
        i=i+1;chemical_groups[i]="kynuren"; partial_weight_[i]=208.22
        i=i+1;chemical_groups[i]="diazep"; partial_weight_[i]=94.11
        i=i+1;chemical_groups[i]="piperid"; partial_weight_[i]=85.15


        j=0
        j=j+1;suffix[j]="tetra-" ; factor[j]=4
        j=j+1;suffix[j]="tetra" ; factor[j]=4
        j=j+1;suffix[j]="tri-" ; factor[j]=3
        j=j+1;suffix[j]="tri" ; factor[j]=3
        j=j+1;suffix[j]="di-" ; factor[j]=2
        j=j+1;suffix[j]="di" ; factor[j]=2
        j=j+1;suffix[j]="mono-" ; factor[j]=1
        j=j+1;suffix[j]="mono" ; factor[j]=1
        j=j+1;suffix[j]="" ; factor[j]=1
        j=j+1;suffix[j]="iso" ; factor[j]=1
        j=0
        j=j+1;postfix[j]="yl" ; factor_[j]=1
        j=j+1;postfix[j]="ine" ; factor_[j]=1
        j=j+1;postfix[j]="" ; factor_[j]=1

reconstructed_name[name_]=""

l=0
                        name_tmp=tolower(name_)
			weight_[name_]=0
                n=0; while(n<length(chemical_groups)) {n=n+1;
                                nn=0;  while(nn<length(suffix)) { nn=nn+1;
                                        mm=0;  while(mm<length(postfix)) {mm=mm+1;
                                                if(chemical_groups[n]!="") {
                                                composed_name=suffix[nn]""chemical_groups[n]""postfix[mm]
                                                b=split(name_tmp,bb,composed_name);
						a=name_
                                                if(b>1) {weight_[a]=weight_[a]+(b-1)*partial_weight_[n]*factor[nn]*factor_[mm]
						reconstructed_name[a]=reconstructed_name[a]""composed_name"-"
                                                gsub(composed_name,"",name_tmp)
                                                if (n<23) {simplification[a]=chemical_groups[n]"ine";
                                                if(chemical_groups[n]~/tryptophan/) {simplification[a]="tryptophan"}
                                                if(chemical_groups[n]~/glutama/) {simplification[a]="glutamate"}
                                                if(chemical_groups[n]~/aspart/) {simplification[a]="aspartate"}
                                                }
                                                }
                                        }
                                        }
                                }
                        }
	reconstructed_name[a]=reconstructed_name[a]""name_tmp
}
