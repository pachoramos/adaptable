function maxmin_nD(ARRAY,maxmin,indexvalue,x,y,z,f,g,h,firstx,lastx,firsty,lasty,firstz,lastz,firstf,lastf,firstg,lastg,firsth,lasth,\
ARRAY_avg,kk,max,end) {

delete  ARRAY_tmp
# Usage: call the function by specfying with "v" the column that has to be varied to find the mamimum e.g. maxmin_nD(a,"max","value",1,"v",7,4,3) find the max in elements a[1,i,7,4,3] 
#  use maxmin="@max" or "@min" for absolute values, if indexvalue=index, teh function returns the index of the vector for which the max or min is found.
# maxmin can be used to find the average id iyts value is "avg" or to clean a vector if its value is "clean"
        if(lastx=="") {ELEMENTS=length_nD(ARRAY,1)}
        if((x=="v" || x=="") && lastx=="") {ELEMENTS=length_nD(ARRAY,1)}
        if(y=="v" && lasty=="") {ELEMENTS=length_nD(ARRAY,2)}
        if(z=="v" && lastz=="") {ELEMENTS=length_nD(ARRAY,3)}
        if(f=="v" && lastf=="") {ELEMENTS=length_nD(ARRAY,4)}
        if(g=="v" && lastg=="") {ELEMENTS=length_nD(ARRAY,5)}
        if(h=="v" && lasth=="") {ELEMENTS=length_nD(ARRAY,6)}
        if(firstx=="") {firstx=0}
        if(firsty=="") {firsty=0}
        if(firstz=="") {firstz=0}
        if(firstf=="") {firstf=0}
        if(firstg=="") {firstg=0}
        if(firsth=="") {firsth=0}
        if(x=="") {x="v"}
        if(x=="v") {i0=firstx; if(lastx!="") {ELEMENTS=lastx-firstx+1}}
        if(y=="v") {i0=firsty ; if(lasty!="") {ELEMENTS=lasty-firsty+1}}
        if(z=="v") {i0=firstz; if(lastz!="") {ELEMENTS=lastz-firstz+1}}
        if(f=="v") {i0=firstf; if(lastf!="") {ELEMENTS=lastf-firstf+1}}
        if(g=="v") {i0=firstg; if(lastg!="") {ELEMENTS=lastg-firstg+1}}
        if(h=="v") {i0=firsth; if(lasth!="") {ELEMENTS=lasth-firsth+1}}

	if (maxmin=="") {maxmin="max"}
	if(indexvalue=="") {indexvalue="value"}
	last_index=i0+ELEMENTS-1	
	if(y=="") {i=i0-1;while(i<last_index) {i=i+1; ARRAY_tmp[i]=ARRAY[i]}}
	if(y!="" && z=="") {i=i0-1;while(i<last_index) {i=i+1; if(x=="v") {ARRAY_tmp[i]=ARRAY[i,y]} else {ARRAY_tmp[i]=ARRAY[x,i]}}}
	if(z!="" && f=="") {i=i0-1;while(i<last_index) {i=i+1; if (x=="v") {ARRAY_tmp[i]=ARRAY[i,y,z]} else {if (y=="v") {ARRAY_tmp[i]=ARRAY[x,i,z]} else {ARRAY_tmp[i]=ARRAY[x,y,i]}}}}
	if(f!="") {i=i0-1;while(i<last_index) {i=i+1;if (x=="v") {ARRAY_tmp[i]=ARRAY[i,y,z,f]} else {if (y=="v") {ARRAY_tmp[i]=ARRAY[x,i,z,f]} else {if(z=="v") {ARRAY_tmp[i]=ARRAY[x,y,i,f]} else {ARRAY_tmp[i]=ARRAY[x,y,z,i]}}}}}
        if(g!="") {i=i0-1;while(i<last_index) {i=i+1;if (x=="v") {ARRAY_tmp[i]=ARRAY[i,y,z,f,g]} else {if (y=="v") {ARRAY_tmp[i]=ARRAY[x,i,z,f,g]} else {if(z=="v") {ARRAY_tmp[i]=ARRAY[x,y,i,f,g]} else {if(f=="v") {ARRAY_tmp[i]=ARRAY[x,y,z,i,g]} else {ARRAY_tmp[i]=ARRAY[x,y,z,f,i]}}}}}}
        if(h!="") {i=i0-1;while(i<last_index) {i=i+1;if (x=="v") {ARRAY_tmp[i]=ARRAY[i,y,z,f,g,h]} else {if (y=="v") {ARRAY_tmp[i]=ARRAY[x,i,z,f,g,h]} else {if(z=="v") {ARRAY_tmp[i]=ARRAY[x,y,i,f,g,h]} else {if(f=="v") {ARRAY_tmp[i]=ARRAY[x,y,z,i,g,h]} else {if(g=="v") {ARRAY_tmp[i]=ARRAY[x,y,z,f,g,i]} else {ARRAY_tmp[i]=ARRAY[x,y,z,f,i,h]}}}}}}}
	
if (maxmin~/@/) {i=i0-1;while(i<last_index) {i=i+1 ; if(ARRAY_tmp[i]<0) {ARRAY_tmp[i]=-ARRAY_tmp[i]}}}
	i=i0
#		while(i<last_index) {i=i+1

			k=i0-1;if(maxmin~/min/) {min=1e10000000000000000}; if(maxmin~/max/) {max=-1e10000000000000000};
			;while(k<last_index) {k=k+1

				if(ARRAY_tmp[k]!="") {
					if(maxmin~/min/)  {        if(ARRAY_tmp[k]<min) {min=ARRAY_tmp[k];index_min=k}}
					if(maxmin~/max/)  {        if(ARRAY_tmp[k]>max) {max=ARRAY_tmp[k];index_max=k}}
					if(maxmin~/avg/)  {        ARRAY_avg=ARRAY_avg+ARRAY_tmp[k]; kk=kk+1}

				}
			}
#		}
delete ARRAY_tmp
	if(maxmin~/avg/) {if(indexvalue=="value") {return ARRAY_avg/kk} else {return index_min}} 
	if(maxmin~/min/) {if(indexvalue=="value") {return min;} else {return index_min}}
	if(maxmin~/max/) {if(indexvalue=="value") {return max} else {return index_max}}
	max=0; min=0

}

