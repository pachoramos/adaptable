BEGIN {}
{
	output_files=ENVIRON["output_files"]
	if($3=="frequency") {
		getline
			while($1!="Colouring") {
				getline
					split($1,a,".") 
					sequences[a[1]]=$2
			}
	}
        if($1=="ID:") {
                getline
                        while($1!="sequence:") {
                                        split($1,a,".") ; lin=substr($2,2,length($2)) ; bmax=split(lin,link,";")
					b=0;while(b<bmax) {b=b+1
					if(link[b]~/satpdb/) {split(link[b],c,"satpdb"); name="satpdb"c[2]; root="http://crdd.osdd.net/raghava/satpdb/display_seq.php?details="}
					if(link[b]~/hemo/) {split(link[b],c,"hemo"); name=c[2]; root="http://crdd.osdd.net/raghava/hemolytik/display.php?details="}
					if(link[b]~/DRAMP/) {split(link[b],c,"DRAMP"); name="DRAMP"c[2]; root="http://dramp.cpu-bioinfor.org/browse/All_Information.php?id="}
					if(link[b]~/canPPD/) {split(link[b],c,"canPPD"); name=""c[2]; root="http://crdd.osdd.net/raghava/cancerppd/display_sub.php?details="}
					if(link[b]~/PHYT/) {split(link[b],c,"PHYT"); name="PHYT"c[2]; root="http://phytamp.hammamilab.org/"}
					if(link[b]~/CAMPSQ/) {split(link[b],c,"CAMPSQ"); name="CAMPSQ"c[2]; root="http://www.camp.bicnirrh.res.in/seqDisp.php?id="}
					if(link[b]~/HIP/) {split(link[b],c,"HIP"); name="HIP"c[2]; root="http://crdd.osdd.net/servers/hipdb/record.php?details="}
                                        links[a[1]]=links[a[1]]" "root""name
                                        namelink[a[1]]=name
					}
				getline
                        }
        }

	if ($1~/:/) {
		tag=""
			if($1~/anti/) {tag="ACTIVITY"} 
		if($1~/hemo/ || $1~/toxic/) {tag="TOXICITY"}
		if($1~/PTM/ || $1~/terminus/ || $1~/cycl/ || $1~/stereo/) {tag="CHEMICAL_CHARACTERISTICS"}
		if($1~/source/ || $1~/name/ || $1~/family/ || $1~/taxonomy/) {tag="SOURCE"}
		if(tag=="") {tag="OTHERS"}
		variable=$1
		line=$0
		split(line,a,":");simpleline=a[1]
			getline; 
		# Some like solubility and others need to get the sub-summary, not the main summary
		if ($1~/summary/){getline; if(variable=="solubility:"||variable=="antigram_pos:"||variable=="antigram_neg:"||variable=="drug_delivery:"||variable=="cell_penetrating:"||variable=="cell_line:"||variable~/activity/) {while($0!=""){getline}; while($0=="") {getline}}; counter[tag]=counter[tag]+1 ; head[tag,counter[tag]]=line ; namevar[tag,counter[tag]]=simpleline ;
			while($0!="") { 
			property[tag,counter[tag]]=property[tag,counter[tag]]""$0
			getline;
			}
		}
		gsub("  ","",property[tag,counter[tag]])
	}
}
END{
	name_tag[1]="ACTIVITY"
		name_tag[2]="TOXICITY"
		name_tag[3]="SOURCE"
		name_tag[4]="CHEMICAL_CHARACTERISTICS"
		name_tag[5]="OTHERS"
		max_tag=5
		print "" > output_files".bash"
		j=0; while(j<max_tag) {j=j+1
			tag=name_tag[j]
#				print ""
#				print tag":"
#				print ""
				i=0; while(i<counter[tag]) {i=i+1
					print namevar[tag,i]"=\""head[tag,i]"\"" >> output_files".bash"
					print namevar[tag,i]"_value=\""property[tag,i]"\"" >> output_files".bash"
#					print property[tag,i]
#					print ""
#					print "-----------------";
#					print ""
				}
#			print ""
#				print "################################################################################"
#				print ""
		}
#print ""
#print "FAMILY MEMBERS"
#print ""
print "" > output_files".seqs"
print "" > output_files".IDs"
# FIXME: we don't use PMIDs from here for now
#print "" > output_files".PMIDs"
#fmax=length(sequences)
#	f=0; while(f<fmax) {f=f+1
#if(sequences[f]!="") {	print f,sequences[f],links[f]}
# FIXME: for some reason this fails in random sequences causing 'satpdb' prefix to be sometimes dropped
#if(sequences[f]!="") { print f,sequences[f],namelink[f] >> output_files".seqs"}
system("sed -n '/Colouring by frequency/,/Colouring by residue type/{/Colouring by residue type/!p}' "output_files" |tail -n +4|head -n -1|awk '{print $2}'| ansi2txt >> "output_files".seqs")
# We need to handle sequences with a lot of IDs that end up using more than one line and moving everthing down, hence, we put the lines together and later we split them again as we want
system("awk '/ ID:/,/___/' "output_files"|ansi2txt |sed -e '/ID:/d' -e '/___/d' |tr -d '[:space:]'|sed -e 's/;;/\\n/g'|cut -d '>' -f2 >> "output_files".IDs")
#system("awk '/PMID:/,/___/' "output_files"|awk '{print $2}'|ansi2txt|sed -e 's/>//' -e '/PMID/d' >> "output_files".PMIDs")
#	}
}
