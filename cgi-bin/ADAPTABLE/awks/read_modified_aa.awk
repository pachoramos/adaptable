function read_modified_aa(modif,translate_modif) {
translate_modif["10-aminodecanoicacid"]="Ա";
official_name["10-aminodecanoicacid"]="10-Aminodecanoic_acid";
weight["10-aminodecanoicacid"]=187.280;
weight["Ա"]=187.280;
simplification["10-aminodecanoicacid"]="_";
nature["10-aminodecanoicacid"]="non-natural";
id["10-aminodecanoicacid"]=83150;

translate_modif["1,2,3,4-tetrahydroisoquinoline-3-l-carboxylicacid"]="Բ";
official_name["1,2,3,4-tetrahydroisoquinoline-3-l-carboxylicacid"]="Quinolinepropionic_acid,_1,2,3,4-tetrahydro-";
weight["1,2,3,4-tetrahydroisoquinoline-3-l-carboxylicacid"]=205.250;
weight["Բ"]=205.250;
simplification["1,2,3,4-tetrahydroisoquinoline-3-l-carboxylicacid"]="_";
nature["1,2,3,4-tetrahydroisoquinoline-3-l-carboxylicacid"]="non-natural";
id["1,2,3,4-tetrahydroisoquinoline-3-l-carboxylicacid"]=98707;

translate_modif["1-amino-cyclobutanecarboxylicacid"]="Գ";
official_name["1-amino-cyclobutanecarboxylicacid"]="1-Aminocyclobutanecarboxylic_acid";
weight["1-amino-cyclobutanecarboxylicacid"]=115.130;
weight["Գ"]=115.130;
simplification["1-amino-cyclobutanecarboxylicacid"]="_";
nature["1-amino-cyclobutanecarboxylicacid"]="non-natural";
id["1-amino-cyclobutanecarboxylicacid"]=89643;

translate_modif["1-aminocyclohexanecarboxylicacid"]="Դ";
official_name["1-aminocyclohexanecarboxylicacid"]="3-Aminocyclohexanecarboxylic_acid";
weight["1-aminocyclohexanecarboxylicacid"]=143.180;
weight["Դ"]=143.180;
simplification["1-aminocyclohexanecarboxylicacid"]="_";
nature["1-aminocyclohexanecarboxylicacid"]="non-natural";
id["1-aminocyclohexanecarboxylicacid"]=544887;

translate_modif["1-aminocyclopentane-1-carboxylicacidsac5c"]="Ե";
official_name["1-aminocyclopentane-1-carboxylicacidsac5c"]="Cycloleucine";
weight["1-aminocyclopentane-1-carboxylicacidsac5c"]=129.160;
weight["Ե"]=129.160;
simplification["1-aminocyclopentane-1-carboxylicacidsac5c"]="_";
nature["1-aminocyclopentane-1-carboxylicacidsac5c"]="non-natural";
id["1-aminocyclopentane-1-carboxylicacidsac5c"]=2901;

translate_modif["1-amino-cyclopentanecarboxylicacid"]="Գ";
official_name["1-amino-cyclopentanecarboxylicacid"]="1-Aminocyclobutanecarboxylic_acid";
weight["1-amino-cyclopentanecarboxylicacid"]=115.130;
weight["Գ"]=115.130;
simplification["1-amino-cyclopentanecarboxylicacid"]="_";
nature["1-amino-cyclopentanecarboxylicacid"]="non-natural";
id["1-amino-cyclopentanecarboxylicacid"]=89643;

translate_modif["1-amino-cyclopropanecarboxylicacid"]="Զ";
official_name["1-amino-cyclopropanecarboxylicacid"]="1-Aminocyclopropanecarboxylic_acid";
weight["1-amino-cyclopropanecarboxylicacid"]=101.100;
weight["Զ"]=101.100;
simplification["1-amino-cyclopropanecarboxylicacid"]="_";
nature["1-amino-cyclopropanecarboxylicacid"]="non-natural";
id["1-amino-cyclopropanecarboxylicacid"]=535;

translate_modif["1-amino-isobutyricacid"]="Է";
official_name["1-amino-isobutyricacid"]="2-Aminoisobutyric_acid";
weight["1-amino-isobutyricacid"]=103.120;
weight["Է"]=103.120;
simplification["1-amino-isobutyricacid"]="_";
nature["1-amino-isobutyricacid"]="non-natural";
id["1-amino-isobutyricacid"]=6119;

translate_modif["1-naphthylalanine"]="Ը";
official_name["1-naphthylalanine"]="1-Naphthyl-L-alanine";
weight["1-naphthylalanine"]=215.250;
weight["Ը"]=215.250;
simplification["1-naphthylalanine"]="A";
nature["1-naphthylalanine"]="non-natural";
id["1-naphthylalanine"]=2724883;

translate_modif["1-naphtyl-alanine"]="Ը";
official_name["1-naphtyl-alanine"]="1-Naphthyl-L-alanine";
weight["1-naphtyl-alanine"]=215.250;
weight["Ը"]=215.250;
simplification["1-naphtyl-alanine"]="A";
nature["1-naphtyl-alanine"]="non-natural";
id["1-naphtyl-alanine"]=2724883;

translate_modif["2,2,6,6-tetramethylpiperidine-1-oxyl-4-amino-4-carboxylic"]="Թ";
official_name["2,2,6,6-tetramethylpiperidine-1-oxyl-4-amino-4-carboxylic"]="2,2,6,6-Tetramethylpiperidine-N-oxide-4-amino-4-carboxylic_acid";
weight["2,2,6,6-tetramethylpiperidine-1-oxyl-4-amino-4-carboxylic"]=215.270;
weight["Թ"]=215.270;
simplification["2,2,6,6-tetramethylpiperidine-1-oxyl-4-amino-4-carboxylic"]="_";
nature["2,2,6,6-tetramethylpiperidine-1-oxyl-4-amino-4-carboxylic"]="non-natural";
id["2,2,6,6-tetramethylpiperidine-1-oxyl-4-amino-4-carboxylic"]=167486;

translate_modif["2_2-dimethylthiazolidine"]="Ժ";
official_name["2_2-dimethylthiazolidine"]="2,2-Dimethylthiazolidine";
weight["2_2-dimethylthiazolidine"]=117.220;
weight["Ժ"]=117.220;
simplification["2_2-dimethylthiazolidine"]="_";
nature["2_2-dimethylthiazolidine"]="non-natural";
id["2_2-dimethylthiazolidine"]=88015;

translate_modif["22-dimethylthiazolidine"]="Ժ";
official_name["22-dimethylthiazolidine"]="2,2-Dimethylthiazolidine";
weight["22-dimethylthiazolidine"]=117.220;
weight["Ժ"]=117.220;
simplification["22-dimethylthiazolidine"]="_";
nature["22-dimethylthiazolidine"]="non-natural";
id["22-dimethylthiazolidine"]=88015;

translate_modif["2,3-dehydro-2-aminobutyricacid"]="Ի";
official_name["2,3-dehydro-2-aminobutyricacid"]="(2Z)-2-aminobut-2-enoic_acid";
weight["2,3-dehydro-2-aminobutyricacid"]=101.100;
weight["Ի"]=101.100;
simplification["2,3-dehydro-2-aminobutyricacid"]="_";
nature["2,3-dehydro-2-aminobutyricacid"]="non-natural";
id["2,3-dehydro-2-aminobutyricacid"]=6449989;

translate_modif["2,3-diaminopropionicacid"]="Խ";
official_name["2,3-diaminopropionicacid"]="2,3-Diaminopropionic_acid";
weight["2,3-diaminopropionicacid"]=104.110;
weight["Խ"]=104.110;
simplification["2,3-diaminopropionicacid"]="_";
nature["2,3-diaminopropionicacid"]="non-natural";
id["2,3-diaminopropionicacid"]=364;

translate_modif["2-amino-3-ethylpentanoicacid"]="Ծ";
official_name["2-amino-3-ethylpentanoicacid"]="2-Bromo-4-methylpentanoic_acid";
weight["2-amino-3-ethylpentanoicacid"]=195.050;
weight["Ծ"]=195.050;
simplification["2-amino-3-ethylpentanoicacid"]="_";
nature["2-amino-3-ethylpentanoicacid"]="non-natural";
id["2-amino-3-ethylpentanoicacid"]=142681;

translate_modif["2-amino-4-guanidinobutyricacid"]="Կ";
official_name["2-amino-4-guanidinobutyricacid"]="4-Guanidinobutyric_acid";
weight["2-amino-4-guanidinobutyricacid"]=145.160;
weight["Կ"]=145.160;
simplification["2-amino-4-guanidinobutyricacid"]="_";
nature["2-amino-4-guanidinobutyricacid"]="non-natural";
id["2-amino-4-guanidinobutyricacid"]=500;

translate_modif["2-amino-4-methyl-8-oxodecenoicacid"]="Հ";
official_name["2-amino-4-methyl-8-oxodecenoicacid"]="2,2-Dimethyl-beta-alanine";
weight["2-amino-4-methyl-8-oxodecenoicacid"]=117.150;
weight["Հ"]=117.150;
simplification["2-amino-4-methyl-8-oxodecenoicacid"]="_";
nature["2-amino-4-methyl-8-oxodecenoicacid"]="non-natural";
id["2-amino-4-methyl-8-oxodecenoicacid"]=95739;

translate_modif["2-amino-6-hydroxy-4-methyl-8-oxodecenoicacid"]="Ձ";
official_name["2-amino-6-hydroxy-4-methyl-8-oxodecenoicacid"]="2-Fluoro-5-hydroxymethylphenylboronic_acid";
weight["2-amino-6-hydroxy-4-methyl-8-oxodecenoicacid"]=169.950;
weight["Ձ"]=169.950;
simplification["2-amino-6-hydroxy-4-methyl-8-oxodecenoicacid"]="_";
nature["2-amino-6-hydroxy-4-methyl-8-oxodecenoicacid"]="non-natural";
id["2-amino-6-hydroxy-4-methyl-8-oxodecenoicacid"]=44717544;

translate_modif["2-amino-9,10-epoxi-8-oxodecanoicacid"]="Ղ";
official_name["2-amino-9,10-epoxi-8-oxodecanoicacid"]="Sebacamic_acid";
weight["2-amino-9,10-epoxi-8-oxodecanoicacid"]=201.260;
weight["Ղ"]=201.260;
simplification["2-amino-9,10-epoxi-8-oxodecanoicacid"]="_";
nature["2-amino-9,10-epoxi-8-oxodecanoicacid"]="non-natural";
id["2-amino-9,10-epoxi-8-oxodecanoicacid"]=14150069;

translate_modif["2-aminobenzoyl"]="Ճ";
official_name["2-aminobenzoyl"]="anthraniloyl-CoA";
weight["2-aminobenzoyl"]=886.700;
weight["Ճ"]=886.700;
simplification["2-aminobenzoyl"]="_";
nature["2-aminobenzoyl"]="non-natural";
id["2-aminobenzoyl"]=9543032;

translate_modif["2-aminobutyricacidglut"]="Մ";
official_name["2-aminobutyricacidglut"]="Gamma-Aminobutyric_Acid";
weight["2-aminobutyricacidglut"]=103.120;
weight["Մ"]=103.120;
simplification["2-aminobutyricacidglut"]="_";
nature["2-aminobutyricacidglut"]="non-natural";
id["2-aminobutyricacidglut"]=119;

translate_modif["2-aminobutyricacidsuc"]="Մ";
official_name["2-aminobutyricacidsuc"]="Gamma-Aminobutyric_Acid";
weight["2-aminobutyricacidsuc"]=103.120;
weight["Մ"]=103.120;
simplification["2-aminobutyricacidsuc"]="_";
nature["2-aminobutyricacidsuc"]="non-natural";
id["2-aminobutyricacidsuc"]=119;

translate_modif["2-amino-dl-dodecanoicacid"]="Յ";
official_name["2-amino-dl-dodecanoicacid"]="12-Aminododecanoic_acid";
weight["2-amino-dl-dodecanoicacid"]=215.330;
weight["Յ"]=215.330;
simplification["2-amino-dl-dodecanoicacid"]="_";
nature["2-amino-dl-dodecanoicacid"]="non-natural";
id["2-amino-dl-dodecanoicacid"]= 69661;

translate_modif["2-amino-dl-dodecanoic_acid"]="Յ";
official_name["2-amino-dl-dodecanoic_acid"]="12-Aminododecanoic_acid";
weight["2-amino-dl-dodecanoic_acid"]=215.330;
weight["Յ"]=215.330;
simplification["2-amino-dl-dodecanoic_acid"]="_";
nature["2-amino-dl-dodecanoic_acid"]="non-natural";
id["2-amino-dl-dodecanoic_acid"]=69661;

translate_modif["2-aminoindane-2-carboxylicacid"]="Ն";
official_name["2-aminoindane-2-carboxylicacid"]="Undecanedioic_Acid";
weight["2-aminoindane-2-carboxylicacid"]=216.270;
weight["Ն"]=216.270;
simplification["2-aminoindane-2-carboxylicacid"]="_";
nature["2-aminoindane-2-carboxylicacid"]="non-natural";
id["2-aminoindane-2-carboxylicacid"]=15816;

translate_modif["2-aminoisobutyricacid"]="Է";
official_name["2-aminoisobutyricacid"]="2-Aminoisobutyric_acid";
weight["2-aminoisobutyricacid"]=103.120;
weight["Է"]=103.120;
simplification["2-aminoisobutyricacid"]="_";
nature["2-aminoisobutyricacid"]="non-natural";
id["2-aminoisobutyricacid"]=6119;

translate_modif["2-mercaptoethanesulfonate-cysteine"]="Շ";
official_name["2-mercaptoethanesulfonate-cysteine"]="Mesna";
weight["2-mercaptoethanesulfonate-cysteine"]=164.180;
weight["Շ"]=164.180;
simplification["2-mercaptoethanesulfonate-cysteine"]="C";
nature["2-mercaptoethanesulfonate-cysteine"]="non-natural";
id["2-mercaptoethanesulfonate-cysteine"]=23662354;

translate_modif["2-mercaptoethyltrimethylammonium_thiocholine_cysteine"]="Ո";
official_name["2-mercaptoethyltrimethylammonium_thiocholine_cysteine"]="Mercaptomethylmethyldiethoxysilane";
weight["2-mercaptoethyltrimethylammonium_thiocholine_cysteine"]=180.340;
weight["Ո"]=180.340;
simplification["2-mercaptoethyltrimethylammonium_thiocholine_cysteine"]="C";
nature["2-mercaptoethyltrimethylammonium_thiocholine_cysteine"]="non-natural";
id["2-mercaptoethyltrimethylammonium_thiocholine_cysteine"]=10130226;

translate_modif["2-methylalaninei.e.a-amino-isobutyricacid"]="Չ";
official_name["2-methylalaninei.e.a-amino-isobutyricacid"]="2-Amino-5-methylpyridine-3-carbonitrile";
weight["2-methylalaninei.e.a-amino-isobutyricacid"]=133.150;
weight["Չ"]=133.150;
simplification["2-methylalaninei.e.a-amino-isobutyricacid"]="A";
nature["2-methylalaninei.e.a-amino-isobutyricacid"]="non-natural";
id["2-methylalaninei.e.a-amino-isobutyricacid"]=13070842;

translate_modif["2-naphthylalanine"]="Պ";
official_name["2-naphthylalanine"]="3-(2-Naphthyl)-L-alanine";
weight["2-naphthylalanine"]=215.250;
weight["Պ"]=215.250;
simplification["2-naphthylalanine"]="A";
nature["2-naphthylalanine"]="non-natural";
id["2-naphthylalanine"]=185915;

translate_modif["2-napthylalanine"]="Ը";
official_name["2-napthylalanine"]="1-Naphthyl-L-alanine";
weight["2-napthylalanine"]=215.250;
weight["Ը"]=215.250;
simplification["2-napthylalanine"]="A";
nature["2-napthylalanine"]="non-natural";
id["2-napthylalanine"]=2724883;

translate_modif["2-phenyltryptophan"]="Ջ";
official_name["2-phenyltryptophan"]="N-Acetyl-DL-tryptophan";
weight["2-phenyltryptophan"]=246.260;
weight["Ջ"]=246.260;
simplification["2-phenyltryptophan"]="W";
nature["2-phenyltryptophan"]="non-natural";
id["2-phenyltryptophan"]=2002;

translate_modif["(2S,4S)-2-amino-6-hydroxy-4-methyl-8-oxodecanoicacid"]="Ռ";
official_name["(2S,4S)-2-amino-6-hydroxy-4-methyl-8-oxodecanoicacid"]="3-Aminomethyl-3-hydroxymethyloxetane";
weight["(2S,4S)-2-amino-6-hydroxy-4-methyl-8-oxodecanoicacid"]=117.150;
weight["Ռ"]=117.150;
simplification["(2S,4S)-2-amino-6-hydroxy-4-methyl-8-oxodecanoicacid"]="_";
nature["(2S,4S)-2-amino-6-hydroxy-4-methyl-8-oxodecanoicacid"]="non-natural";
id["(2S,4S)-2-amino-6-hydroxy-4-methyl-8-oxodecanoicacid"]=18706850;

translate_modif["2-thienylalanine"]="Վ";
official_name["2-thienylalanine"]="3-(2-Thienyl)alanine";
weight["2-thienylalanine"]=171.220;
weight["Վ"]=171.220;
simplification["2-thienylalanine"]="A";
nature["2-thienylalanine"]="non-natural";
id["2-thienylalanine"]=91444;

translate_modif["3-1-naphthylalanine"]="Ը";
official_name["3-1-naphthylalanine"]="1-Naphthyl-L-alanine";
weight["3-1-naphthylalanine"]=215.250;
weight["Ը"]=215.250;
simplification["3-1-naphthylalanine"]="A";
nature["3-1-naphthylalanine"]="non-natural";
id["3-1-naphthylalanine"]=2724883;

translate_modif["3,3-diphenylalanine"]="Ր";
official_name["3,3-diphenylalanine"]="Fmoc-D-3,3-Diphenylalanine";
weight["3,3-diphenylalanine"]=463.500;
weight["Ր"]=463.500;
simplification["3,3-diphenylalanine"]="F";
nature["3,3-diphenylalanine"]="non-natural";
id["3,3-diphenylalanine"]=7020786;

translate_modif["3,4-dihydroxyphenylalanine"]="Ց";
official_name["3,4-dihydroxyphenylalanine"]="Levodopa";
weight["3,4-dihydroxyphenylalanine"]=197.190;
weight["Ց"]=197.190;
simplification["3,4-dihydroxyphenylalanine"]="F";
nature["3,4-dihydroxyphenylalanine"]="non-natural";
id["3,4-dihydroxyphenylalanine"]=6047;

translate_modif["3-4-hydroxyphenylpropionicacid"]="Ւ";
official_name["3-4-hydroxyphenylpropionicacid"]="Dihydrocaffeic_acid";
weight["3-4-hydroxyphenylpropionicacid"]=182.170;
weight["Ւ"]=182.170;
simplification["3-4-hydroxyphenylpropionicacid"]="_";
nature["3-4-hydroxyphenylpropionicacid"]="non-natural";
id["3-4-hydroxyphenylpropionicacid"]=348154;

translate_modif["3,5-diiodotyrosine"]="Փ";
official_name["3,5-diiodotyrosine"]="3,5-Diiodo-L-tyrosine";
weight["3,5-diiodotyrosine"]=432.980;
weight["Փ"]=432.980;
simplification["3,5-diiodotyrosine"]="Y";
nature["3,5-diiodotyrosine"]="non-natural";
id["3,5-diiodotyrosine"]=9305;

translate_modif["3-aminodecanoyl"]="Ա";
official_name["3-aminodecanoyl"]="10-Aminodecanoic_acid";
weight["3-aminodecanoyl"]=187.280;
weight["Ա"]=187.280;
simplification["3-aminodecanoyl"]="_";
nature["3-aminodecanoyl"]="non-natural";
id["3-aminodecanoyl"]=83150;

translate_modif["3-biphenyl-4-yl-l-alanine"]="Ք";
official_name["3-biphenyl-4-yl-l-alanine"]="N-Boc-N-methyl-3-(biphenyl-4-yl)-D-alanine";
weight["3-biphenyl-4-yl-l-alanine"]=355.400;
weight["Ք"]=355.400;
simplification["3-biphenyl-4-yl-l-alanine"]="A";
nature["3-biphenyl-4-yl-l-alanine"]="non-natural";
id["3-biphenyl-4-yl-l-alanine"]=20694694;

translate_modif["3-bromobenzoyl"]="Ֆ";
official_name["3-bromobenzoyl"]="3-Bromobenzoyl_chloride";
weight["3-bromobenzoyl"]=219.460;
weight["Ֆ"]=219.460;
simplification["3-bromobenzoyl"]="_";
nature["3-bromobenzoyl"]="non-natural";
id["3-bromobenzoyl"]=74377;

translate_modif["3-bromobenzyl"]="Ѐ";
official_name["3-bromobenzyl"]="3-Bromobenzylamine";
weight["3-bromobenzyl"]=186.050;
weight["Ѐ"]=186.050;
simplification["3-bromobenzyl"]="_";
nature["3-bromobenzyl"]="non-natural";
id["3-bromobenzyl"]=457587;

translate_modif["3-chlorobenzyl"]="Ё";
official_name["3-chlorobenzyl"]="3-Chlorobenzyl_alcohol";
weight["3-chlorobenzyl"]=142.580;
weight["Ё"]=142.580;
simplification["3-chlorobenzyl"]="_";
nature["3-chlorobenzyl"]="non-natural";
id["3-chlorobenzyl"]=70117;

translate_modif["3-hydroxy-4-methylpentadecanoicacid"]="Ђ";
official_name["3-hydroxy-4-methylpentadecanoicacid"]="15-Hydroxypentadecanoic_acid";
weight["3-hydroxy-4-methylpentadecanoicacid"]=258.399;
weight["Ђ"]=258.399;
simplification["3-hydroxy-4-methylpentadecanoicacid"]="_";
nature["3-hydroxy-4-methylpentadecanoicacid"]="non-natural";
id["3-hydroxy-4-methylpentadecanoicacid"]=78360;

translate_modif["3-hydroxyvaline"]="Ѓ";
official_name["3-hydroxyvaline"]="(R)-2-Amino-3-Hydroxy-3-Methylbutanoic_Acid";
weight["3-hydroxyvaline"]=133.150;
weight["Ѓ"]=133.150;
simplification["3-hydroxyvaline"]="V";
nature["3-hydroxyvaline"]="non-natural";
id["3-hydroxyvaline"]=2724755;

translate_modif["3-hydroxy-valine"]="Є";
official_name["3-hydroxy-valine"]="Hydroxylamine";
weight["3-hydroxy-valine"]=33.030;
weight["Є"]=33.030;
simplification["3-hydroxy-valine"]="V";
nature["3-hydroxy-valine"]="non-natural";
id["3-hydroxy-valine"]=787;

translate_modif["3-iodotyrosine"]="Ї";
official_name["3-iodotyrosine"]="3-Iodo-L-tyrosine";
weight["3-iodotyrosine"]=307.080;
weight["Ї"]=307.080;
simplification["3-iodotyrosine"]="Y";
nature["3-iodotyrosine"]="non-natural";
id["3-iodotyrosine"]=439744;

translate_modif["3-iodo-tyrosine"]="Ї";
official_name["3-iodo-tyrosine"]="3-Iodo-L-tyrosine";
weight["3-iodo-tyrosine"]=307.080;
weight["Ї"]=307.080;
simplification["3-iodo-tyrosine"]="Y";
nature["3-iodo-tyrosine"]="non-natural";
id["3-iodo-tyrosine"]=439744;

translate_modif["3-nitrotyrosine"]="Љ";
official_name["3-nitrotyrosine"]="3-nitro-L-tyrosine";
weight["3-nitrotyrosine"]=226.190;
weight["Љ"]=226.190;
simplification["3-nitrotyrosine"]="Y";
nature["3-nitrotyrosine"]="non-natural";
id["3-nitrotyrosine"]=65124;

translate_modif["3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]="Њ";
official_name["3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]="6-Bromo-2-(trifluoromethyl)quinoline";
weight["3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]=276.050;
weight["Њ"]=276.050;
simplification["3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]="G";
nature["3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]="non-natural";
id["3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]=15325795;

translate_modif["4-amino"]="Ћ";
official_name["4-amino"]="Amidogen";
weight["4-amino"]=16.023;
weight["Ћ"]=16.023;
simplification["4-amino"]="_";
nature["4-amino"]="non-natural";
id["4-amino"]=0.000;

translate_modif["4-aminopiperidine-4-carboxylicacid"]="Ќ";
official_name["4-aminopiperidine-4-carboxylicacid"]="1-Acetylpiperidine-4-carboxylic_acid";
weight["4-aminopiperidine-4-carboxylicacid"]=171.190;
weight["Ќ"]=171.190;
simplification["4-aminopiperidine-4-carboxylicacid"]="_";
nature["4-aminopiperidine-4-carboxylicacid"]="non-natural";
id["4-aminopiperidine-4-carboxylicacid"]=117255;

translate_modif["4-azido"]="Ѝ";
official_name["4-azido"]="Azido";
weight["4-azido"]=42.021;
weight["Ѝ"]=42.021;
simplification["4-azido"]="_";
nature["4-azido"]="non-natural";
id["4-azido"]=6857664;

translate_modif["4-azidoF-3,5-127I"]="Ў";
official_name["4-azidoF-3,5-127I"]="2-(4-(2-(2-((4-Azidophenyl)methylcarbonylamino)ethylaminocarbonyl)ethyl)phenyl)ethylamino-5'-N-ethylcarboxamidoadenosine";
weight["4-azidoF-3,5-127I"]=700.700;
weight["Ў"]=700.700;
simplification["4-azidoF-3,5-127I"]="_";
nature["4-azidoF-3,5-127I"]="non-natural";
id["4-azidoF-3,5-127I"]=3083135;

translate_modif["4-benzoyl-phenylalanine"]="Џ";
official_name["4-benzoyl-phenylalanine"]="p-Benzoyl-L-phenylalanine";
weight["4-benzoyl-phenylalanine"]=269.290;
weight["Џ"]=269.290;
simplification["4-benzoyl-phenylalanine"]="F";
nature["4-benzoyl-phenylalanine"]="non-natural";
id["4-benzoyl-phenylalanine"]=7020128;

translate_modif["4-chlorophenylalanine"]="Б";
official_name["4-chlorophenylalanine"]="Fenclonine";
weight["4-chlorophenylalanine"]=199.630;
weight["Б"]=199.630;
simplification["4-chlorophenylalanine"]="F";
nature["4-chlorophenylalanine"]="non-natural";
id["4-chlorophenylalanine"]=4652;

translate_modif["4-chloro-phenylalanine"]="Г";
official_name["4-chloro-phenylalanine"]="L-Homophenylalanine";
weight["4-chloro-phenylalanine"]=179.220;
weight["Г"]=179.220;
simplification["4-chloro-phenylalanine"]="F";
nature["4-chloro-phenylalanine"]="non-natural";
id["4-chloro-phenylalanine"]=2724505;

translate_modif["4-fluorobenzyl"]="Д";
official_name["4-fluorobenzyl"]="4-Fluorotoluene";
weight["4-fluorobenzyl"]=110.130;
weight["Д"]=110.130;
simplification["4-fluorobenzyl"]="_";
nature["4-fluorobenzyl"]="non-natural";
id["4-fluorobenzyl"]=9603;

translate_modif["4-fluorophenylalanine"]="Ж";
official_name["4-fluorophenylalanine"]="4-Fluorophenylalanine";
weight["4-fluorophenylalanine"]=183.180;
weight["Ж"]=183.180;
simplification["4-fluorophenylalanine"]="F";
nature["4-fluorophenylalanine"]="non-natural";
id["4-fluorophenylalanine"]=4654;

translate_modif["4-hexyloxyproline"]="З";
official_name["4-hexyloxyproline"]="4-Hexyloxyaniline";
weight["4-hexyloxyproline"]=193.280;
weight["З"]=193.280;
simplification["4-hexyloxyproline"]="P";
nature["4-hexyloxyproline"]="non-natural";
id["4-hexyloxyproline"]=38360;

translate_modif["4-hydroxyproline"]="И";
official_name["4-hydroxyproline"]="Hydroxyproline";
weight["4-hydroxyproline"]=131.130;
weight["И"]=131.130;
simplification["4-hydroxyproline"]="P";
nature["4-hydroxyproline"]="non-natural";
id["4-hydroxyproline"]=5810;

translate_modif["4-methoxy-phenylalanine"]="Й";
official_name["4-methoxy-phenylalanine"]="N-Benzoyl-4-hydroxy-3-methoxy-phenylalanine";
weight["4-methoxy-phenylalanine"]=315.320;
weight["Й"]=315.320;
simplification["4-methoxy-phenylalanine"]="F";
nature["4-methoxy-phenylalanine"]="non-natural";
id["4-methoxy-phenylalanine"]=4055155;

translate_modif["4-nitrophenylalanine"]="Л";
official_name["4-nitrophenylalanine"]="4-Nitro-DL-phenylalanine";
weight["4-nitrophenylalanine"]=210.190;
weight["Л"]=210.190;
simplification["4-nitrophenylalanine"]="F";
nature["4-nitrophenylalanine"]="non-natural";
id["4-nitrophenylalanine"]=65089;

translate_modif["4-r-1-naphtylmehyl-proline"]="П";
official_name["4-r-1-naphtylmehyl-proline"]="N-Methyl-N-naphthylmethylamine";
weight["4-r-1-naphtylmehyl-proline"]=171.240;
weight["П"]=171.240;
simplification["4-r-1-naphtylmehyl-proline"]="P";
nature["4-r-1-naphtylmehyl-proline"]="non-natural";
id["4-r-1-naphtylmehyl-proline"]=84474;

translate_modif["4-r-fluoro-proline"]="Я";
official_name["4-r-fluoro-proline"]="5,6-Difluoroindoline";
weight["4-r-fluoro-proline"]=155.140;
weight["Я"]=155.140;
simplification["4-r-fluoro-proline"]="P";
nature["4-r-fluoro-proline"]="non-natural";
id["4-r-fluoro-proline"]=16792594;

translate_modif["4-r-guanidino-proline"]="Ю";
official_name["4-r-guanidino-proline"]="Guanidinospermidine";
weight["4-r-guanidino-proline"]=187.290;
weight["Ю"]=187.290;
simplification["4-r-guanidino-proline"]="P";
nature["4-r-guanidino-proline"]="non-natural";
id["4-r-guanidino-proline"]=3082086;

translate_modif["4-s-fluoro-proline"]="Я";
official_name["4-s-fluoro-proline"]="5,6-Difluoroindoline";
weight["4-s-fluoro-proline"]=155.140;
weight["Я"]=155.140;
simplification["4-s-fluoro-proline"]="P";
nature["4-s-fluoro-proline"]="non-natural";
id["4-s-fluoro-proline"]=16792594;

translate_modif["5-amino-3-oxo-pentanoicacid"]="Э";
official_name["5-amino-3-oxo-pentanoicacid"]="3-Aminopentanoic_acid";
weight["5-amino-3-oxo-pentanoicacid"]=117.150;
weight["Э"]=117.150;
simplification["5-amino-3-oxo-pentanoicacid"]="_";
nature["5-amino-3-oxo-pentanoicacid"]="non-natural";
id["5-amino-3-oxo-pentanoicacid"]=14731978;

translate_modif["5-amino-3-oxo-pentanoic_acid"]="Э";
official_name["5-amino-3-oxo-pentanoic_acid"]="3-Aminopentanoic_acid";
weight["5-amino-3-oxo-pentanoic_acid"]=117.150;
weight["Э"]=117.150;
simplification["5-amino-3-oxo-pentanoic_acid"]="_";
nature["5-amino-3-oxo-pentanoic_acid"]="non-natural";
id["5-amino-3-oxo-pentanoic_acid"]=14731978;

translate_modif["5-aminovalericacid"]="Ь";
official_name["5-aminovalericacid"]="5-Aminovaleric_acid";
weight["5-aminovalericacid"]=117.150;
weight["Ь"]=117.150;
simplification["5-aminovalericacid"]="V";
nature["5-aminovalericacid"]="non-natural";
id["5-aminovalericacid"]=138;

translate_modif["5-hydroxylysine"]="Ы";
official_name["5-hydroxylysine"]="5-Hydroxylysine";
weight["5-hydroxylysine"]=162.190;
weight["Ы"]=162.190;
simplification["5-hydroxylysine"]="K";
nature["5-hydroxylysine"]="non-natural";
id["5-hydroxylysine"]=3032849;

translate_modif["5-hydroxy-lysine"]="Ъ";
official_name["5-hydroxy-lysine"]="4-Hydroxy-lysine";
weight["5-hydroxy-lysine"]=162.190;
weight["Ъ"]=162.190;
simplification["5-hydroxy-lysine"]="K";
nature["5-hydroxy-lysine"]="non-natural";
id["5-hydroxy-lysine"]=14192130;

translate_modif["5-hydroxytryptophan"]="Щ";
official_name["5-hydroxytryptophan"]="5-Hydroxytryptophan";
weight["5-hydroxytryptophan"]=220.220;
weight["Щ"]=220.220;
simplification["5-hydroxytryptophan"]="W";
nature["5-hydroxytryptophan"]="non-natural";
id["5-hydroxytryptophan"]=144;

translate_modif["5-methylisoxazole-3-carboxyl"]="Ш";
official_name["5-methylisoxazole-3-carboxyl"]="3-Carboxy-5-methylisoxazole";
weight["5-methylisoxazole-3-carboxyl"]=127.100;
weight["Ш"]=127.100;
simplification["5-methylisoxazole-3-carboxyl"]="_";
nature["5-methylisoxazole-3-carboxyl"]="non-natural";
id["5-methylisoxazole-3-carboxyl"]=76947;

translate_modif["6-aminohexanoicaci"]="Ч";
official_name["6-aminohexanoicaci"]="Aminocaproic_acid";
weight["6-aminohexanoicaci"]=131.170;
weight["Ч"]=131.170;
simplification["6-aminohexanoicaci"]="_";
nature["6-aminohexanoicaci"]="non-natural";
id["6-aminohexanoicaci"]=564;

translate_modif["6-aminohexanoicacid"]="Ч";
official_name["6-aminohexanoicacid"]="Aminocaproic_acid";
weight["6-aminohexanoicacid"]=131.170;
weight["Ч"]=131.170;
simplification["6-aminohexanoicacid"]="_";
nature["6-aminohexanoicacid"]="non-natural";
id["6-aminohexanoicacid"]=564;

translate_modif["6-aminohexanoicacidahx"]="Ч";
official_name["6-aminohexanoicacidahx"]="Aminocaproic_acid";
weight["6-aminohexanoicacidahx"]=131.170;
weight["Ч"]=131.170;
simplification["6-aminohexanoicacidahx"]="_";
nature["6-aminohexanoicacidahx"]="non-natural";
id["6-aminohexanoicacidahx"]=564;

translate_modif["6-aminohexanoicacidahxandb"]="Ч";
official_name["6-aminohexanoicacidahxandb"]="Aminocaproic_acid";
weight["6-aminohexanoicacidahxandb"]=131.170;
weight["Ч"]=131.170;
simplification["6-aminohexanoicacidahxandb"]="_";
nature["6-aminohexanoicacidahxandb"]="non-natural";
id["6-aminohexanoicacidahxandb"]=564;

translate_modif["6-bromotryptophan"]="Ц";
official_name["6-bromotryptophan"]="6-Bromo-L-tryptophan";
weight["6-bromotryptophan"]=283.120;
weight["Ц"]=283.120;
simplification["6-bromotryptophan"]="W";
nature["6-bromotryptophan"]="non-natural";
id["6-bromotryptophan"]=9856996;

translate_modif["6-bromo-tryptophan"]="Ц";
official_name["6-bromo-tryptophan"]="6-Bromo-L-tryptophan";
weight["6-bromo-tryptophan"]=283.120;
weight["Ц"]=283.120;
simplification["6-bromo-tryptophan"]="W";
nature["6-bromo-tryptophan"]="non-natural";
id["6-bromo-tryptophan"]=9856996;

translate_modif["6-fluoro-tryptophan"]="Х";
official_name["6-fluoro-tryptophan"]="5-Fluorotryptophan";
weight["6-fluoro-tryptophan"]=222.220;
weight["Х"]=222.220;
simplification["6-fluoro-tryptophan"]="W";
nature["6-fluoro-tryptophan"]="non-natural";
id["6-fluoro-tryptophan"]=9577;

translate_modif["6-l-bromotryptophan"]="Ц";
official_name["6-l-bromotryptophan"]="6-Bromo-L-tryptophan";
weight["6-l-bromotryptophan"]=283.120;
weight["Ц"]=283.120;
simplification["6-l-bromotryptophan"]="W";
nature["6-l-bromotryptophan"]="non-natural";
id["6-l-bromotryptophan"]=9856996;

translate_modif["8-amino-3,6-dioxaoctanoicacid"]="Ф";
official_name["8-amino-3,6-dioxaoctanoicacid"]="3,6-Dioxaoctandioic_acid";
weight["8-amino-3,6-dioxaoctanoicacid"]=178.140;
weight["Ф"]=178.140;
simplification["8-amino-3,6-dioxaoctanoicacid"]="_";
nature["8-amino-3,6-dioxaoctanoicacid"]="non-natural";
id["8-amino-3,6-dioxaoctanoicacid"]=90039;

translate_modif["8-amino-octanoicacid"]="У";
official_name["8-amino-octanoicacid"]="8-Aminooctanoic_acid";
weight["8-amino-octanoicacid"]=159.230;
weight["У"]=159.230;
simplification["8-amino-octanoicacid"]="_";
nature["8-amino-octanoicacid"]="non-natural";
id["8-amino-octanoicacid"]=66085;

translate_modif["acetyl"]="Ѣ";
official_name["acetyl"]="Acetyl_radical";
weight["acetyl"]=43.040;
weight["Ѣ"]=43.040;
simplification["acetyl"]="_";
nature["acetyl"]="non-natural";
id["acetyl"]=137849;

translate_modif["acetylaminoalanine"]="Ґ";
official_name["acetylaminoalanine"]="alpha-Acetyl-beta-aminoalanine";
weight["acetylaminoalanine"]=146.140;
weight["Ґ"]=146.140;
simplification["acetylaminoalanine"]="A";
nature["acetylaminoalanine"]="non-natural";
id["acetylaminoalanine"]=129850561;

translate_modif["acetylateddiaminobutyrate"]="Ғ";
official_name["acetylateddiaminobutyrate"]="(2S)-4-(acetylamino)-2-aminobutanoic_acid";
weight["acetylateddiaminobutyrate"]=160.170;
weight["Ғ"]=160.170;
simplification["acetylateddiaminobutyrate"]="_";
nature["acetylateddiaminobutyrate"]="non-natural";
id["acetylateddiaminobutyrate"]=441021;

translate_modif["acetyl-d-ornithine"]="Ҕ";
official_name["acetyl-d-ornithine"]="N(5)-Acetyl-L-ornithine";
weight["acetyl-d-ornithine"]=174.200;
weight["Ҕ"]=174.200;
simplification["acetyl-d-ornithine"]="_";
nature["acetyl-d-ornithine"]="non-natural";
id["acetyl-d-ornithine"]=193343;

translate_modif["acteyl"]="Ѣ";
official_name["acteyl"]="Acetyl_radical";
weight["acteyl"]=43.040;
weight["Ѣ"]=43.040;
simplification["acteyl"]="_";
nature["acteyl"]="non-natural";
id["acteyl"]=137849;

translate_modif["adamantylglycine"]="Җ";
official_name["adamantylglycine"]="(2S)-2-(Adamantan-1-yl)-2-((tert-butoxycarbonyl)amino)acetic_acid";
weight["adamantylglycine"]=309.400;
weight["Җ"]=309.400;
simplification["adamantylglycine"]="G";
nature["adamantylglycine"]="non-natural";
id["adamantylglycine"]=11738405;

translate_modif["alaninepeptoid"]="Ҙ";
official_name["alaninepeptoid"]="Beta-alanine_betaine";
weight["alaninepeptoid"]=131.170;
weight["Ҙ"]=131.170;
simplification["alaninepeptoid"]="A";
nature["alaninepeptoid"]="non-natural";
id["alaninepeptoid"]=441440;

translate_modif["allo-isoleucine"]="Қ";
official_name["allo-isoleucine"]="L-Alloisoleucine";
weight["allo-isoleucine"]=131.170;
weight["Қ"]=131.170;
simplification["allo-isoleucine"]="I";
nature["allo-isoleucine"]="non-natural";
id["allo-isoleucine"]=99288;

translate_modif["allylglycine"]="Ң";
official_name["allylglycine"]="Allylglycine";
weight["allylglycine"]=115.130;
weight["Ң"]=115.130;
simplification["allylglycine"]="G";
nature["allylglycine"]="non-natural";
id["allylglycine"]=14044;

translate_modif["alpha"]="Ҥ";
official_name["alpha"]="Mangostin";
weight["alpha"]=410.500;
weight["Ҥ"]=410.500;
simplification["alpha"]="_";
nature["alpha"]="non-natural";
id["alpha"]=5281650;

translate_modif["alpha-aminobutyrate"]="Ҫ";
official_name["alpha-aminobutyrate"]="gamma-Glutamyl-alpha-aminobutyrate";
weight["alpha-aminobutyrate"]=232.230;
weight["Ҫ"]=232.230;
simplification["alpha-aminobutyrate"]="_";
nature["alpha-aminobutyrate"]="non-natural";
id["alpha-aminobutyrate"]=7014903;

translate_modif["alpha-aminobutyrateinlantibiotics"]="Ҫ";
official_name["alpha-aminobutyrateinlantibiotics"]="gamma-Glutamyl-alpha-aminobutyrate";
weight["alpha-aminobutyrateinlantibiotics"]=232.230;
weight["Ҫ"]=232.230;
simplification["alpha-aminobutyrateinlantibiotics"]="_";
nature["alpha-aminobutyrateinlantibiotics"]="non-natural";
id["alpha-aminobutyrateinlantibiotics"]=7014903;

translate_modif["alpha-aminobutyricacid"]="Ҭ";
official_name["alpha-aminobutyricacid"]="2,3-Dichloroisobutyric_acid";
weight["alpha-aminobutyricacid"]=156.990;
weight["Ҭ"]=156.990;
simplification["alpha-aminobutyricacid"]="_";
nature["alpha-aminobutyricacid"]="non-natural";
id["alpha-aminobutyricacid"]=25246;

translate_modif["alpha-aminobutyric_acid"]="Ұ";
official_name["alpha-aminobutyric_acid"]="4-Glycylaminobutyric_acid";
weight["alpha-aminobutyric_acid"]=160.170;
weight["Ұ"]=160.170;
simplification["alpha-aminobutyric_acid"]="_";
nature["alpha-aminobutyric_acid"]="non-natural";
id["alpha-aminobutyric_acid"]=99281;

translate_modif["alpha-aminoisobutyric"]="Է";
official_name["alpha-aminoisobutyric"]="2-Aminoisobutyric_acid";
weight["alpha-aminoisobutyric"]=103.120;
weight["Է"]=103.120;
simplification["alpha-aminoisobutyric"]="_";
nature["alpha-aminoisobutyric"]="non-natural";
id["alpha-aminoisobutyric"]=6119;

translate_modif["alphaketoamide"]="Ҳ";
official_name["alphaketoamide"]="Sulfacetamide";
weight["alphaketoamide"]=214.240;
weight["Ҳ"]=214.240;
simplification["alphaketoamide"]="_";
nature["alphaketoamide"]="non-natural";
id["alphaketoamide"]=5320;

translate_modif["amide"]="Ӂ";
official_name["amide"]="Amidephrine";
weight["amide"]=244.310;
weight["Ӂ"]=244.310;
simplification["amide"]="_";
nature["amide"]="non-natural";
id["amide"]=15010;

translate_modif["amideape"]="Ӄ";
official_name["amideape"]="3-Aminoisoquinoline";
weight["amideape"]=144.170;
weight["Ӄ"]=144.170;
simplification["amideape"]="_";
nature["amideape"]="non-natural";
id["amideape"]=311869;

translate_modif["amidenal1"]="Ӈ";
official_name["amidenal1"]="Aprindine_hydrochloride";
weight["amidenal1"]=358.900;
weight["Ӈ"]=358.900;
simplification["amidenal1"]="_";
nature["amidenal1"]="non-natural";
id["amidenal1"]=71413;

translate_modif["amidenal2"]="Ӈ";
official_name["amidenal2"]="Aprindine_hydrochloride";
weight["amidenal2"]=358.900;
weight["Ӈ"]=358.900;
simplification["amidenal2"]="_";
nature["amidenal2"]="non-natural";
id["amidenal2"]=71413;

translate_modif["amino"]="Ћ";
official_name["amino"]="Amidogen";
weight["amino"]=16.023;
weight["Ћ"]=16.023;
simplification["amino"]="_";
nature["amino"]="non-natural";
id["amino"]=0.000;

translate_modif["aminocyclohexanecarboxylicacid"]="Դ";
official_name["aminocyclohexanecarboxylicacid"]="3-Aminocyclohexanecarboxylic_acid";
weight["aminocyclohexanecarboxylicacid"]=143.180;
weight["Դ"]=143.180;
simplification["aminocyclohexanecarboxylicacid"]="_";
nature["aminocyclohexanecarboxylicacid"]="non-natural";
id["aminocyclohexanecarboxylicacid"]=544887;

translate_modif["aminohexylresidue"]="Ӌ";
official_name["aminohexylresidue"]="Hexanamide,_6-amino-";
weight["aminohexylresidue"]=130.190;
weight["Ӌ"]=130.190;
simplification["aminohexylresidue"]="_";
nature["aminohexylresidue"]="non-natural";
id["aminohexylresidue"]=67798;

translate_modif["aminoisobutyricacid"]="Է";
official_name["aminoisobutyricacid"]="2-Aminoisobutyric_acid";
weight["aminoisobutyricacid"]=103.120;
weight["Է"]=103.120;
simplification["aminoisobutyricacid"]="_";
nature["aminoisobutyricacid"]="non-natural";
id["aminoisobutyricacid"]=6119;

translate_modif["aminoisobutyricacidaibor?-methylalanineor2-"]="Է";
official_name["aminoisobutyricacidaibor?-methylalanineor2-"]="2-Aminoisobutyric_acid";
weight["aminoisobutyricacidaibor?-methylalanineor2-"]=103.120;
weight["Է"]=103.120;
simplification["aminoisobutyricacidaibor?-methylalanineor2-"]="A";
nature["aminoisobutyricacidaibor?-methylalanineor2-"]="non-natural";
id["aminoisobutyricacidaibor?-methylalanineor2-"]=6119;

translate_modif["aminopropionicacid"]="Ӑ";
official_name["aminopropionicacid"]="beta-Alanine";
weight["aminopropionicacid"]=89.090;
weight["Ӑ"]=89.090;
simplification["aminopropionicacid"]="_";
nature["aminopropionicacid"]="non-natural";
id["aminopropionicacid"]=239;

translate_modif["aminopyrrolidinecarboxylicacid"]="Ӓ";
official_name["aminopyrrolidinecarboxylicacid"]="5-Methylpyrimidine-4-carboxylic_acid";
weight["aminopyrrolidinecarboxylicacid"]=138.120;
weight["Ӓ"]=138.120;
simplification["aminopyrrolidinecarboxylicacid"]="_";
nature["aminopyrrolidinecarboxylicacid"]="non-natural";
id["aminopyrrolidinecarboxylicacid"]=53415084;

translate_modif["argininewithbutyrylatedsidegroup"]="Ӕ";
official_name["argininewithbutyrylatedsidegroup"]="N-butyryl_arginine";
weight["argininewithbutyrylatedsidegroup"]=244.290;
weight["Ӕ"]=244.290;
simplification["argininewithbutyrylatedsidegroup"]="R";
nature["argininewithbutyrylatedsidegroup"]="non-natural";
id["argininewithbutyrylatedsidegroup"]=18653905;

translate_modif["asymmetricdimethylarginine"]="Ӗ";
official_name["asymmetricdimethylarginine"]="N,N-Dimethylarginine";
weight["asymmetricdimethylarginine"]=202.250;
weight["Ӗ"]=202.250;
simplification["asymmetricdimethylarginine"]="R";
nature["asymmetricdimethylarginine"]="non-natural";
id["asymmetricdimethylarginine"]=123831;

translate_modif["asymmetric_dimethylarginine"]="Ӗ";
official_name["asymmetric_dimethylarginine"]="N,N-Dimethylarginine";
weight["asymmetric_dimethylarginine"]=202.250;
weight["Ӗ"]=202.250;
simplification["asymmetric_dimethylarginine"]="R";
nature["asymmetric_dimethylarginine"]="non-natural";
id["asymmetric_dimethylarginine"]=123831;

translate_modif["azetidine"]="Ә";
official_name["azetidine"]="Azetidine_hydrochloride";
weight["azetidine"]=93.550;
weight["Ә"]=93.550;
simplification["azetidine"]="_";
nature["azetidine"]="non-natural";
id["azetidine"]=12308726;

translate_modif["azidolysine"]="Ӛ";
official_name["azidolysine"]="(S)-2-((((9H-Fluoren-9-yl)methoxy)carbonyl)amino)-6-azidohexanoic_acid";
weight["azidolysine"]=394.400;
weight["Ӛ"]=394.400;
simplification["azidolysine"]="K";
nature["azidolysine"]="non-natural";
id["azidolysine"]=25146063;

translate_modif["benzoylphenylalanine"]="Ӝ";
official_name["benzoylphenylalanine"]="N-Benzoyl-l-phenylalanine";
weight["benzoylphenylalanine"]=269.290;
weight["Ӝ"]=269.290;
simplification["benzoylphenylalanine"]="F";
nature["benzoylphenylalanine"]="non-natural";
id["benzoylphenylalanine"]=97370;

translate_modif["benzylatedtyrosine"]="Ӟ";
official_name["benzylatedtyrosine"]="N6-Benzyladenosine";
weight["benzylatedtyrosine"]=357.400;
weight["Ӟ"]=357.400;
simplification["benzylatedtyrosine"]="Y";
nature["benzylatedtyrosine"]="non-natural";
id["benzylatedtyrosine"]=92208;

translate_modif["benzyloxycarbonyl"]="Ӡ";
official_name["benzyloxycarbonyl"]="Benzyloxycarbonylserine";
weight["benzyloxycarbonyl"]=239.220;
weight["Ӡ"]=239.220;
simplification["benzyloxycarbonyl"]="_";
nature["benzyloxycarbonyl"]="non-natural";
id["benzyloxycarbonyl"]=100310;

translate_modif["benzyloxycarbonylgroupboundtothelysineepsilonaminogroup"]="Ӣ";
official_name["benzyloxycarbonylgroupboundtothelysineepsilonaminogroup"]="N-Benzyloxycarbonylserylleucinamide";
weight["benzyloxycarbonylgroupboundtothelysineepsilonaminogroup"]=351.400;
weight["Ӣ"]=351.400;
simplification["benzyloxycarbonylgroupboundtothelysineepsilonaminogroup"]="K";
nature["benzyloxycarbonylgroupboundtothelysineepsilonaminogroup"]="non-natural";
id["benzyloxycarbonylgroupboundtothelysineepsilonaminogroup"]=152211;

translate_modif["beta"]="Ӥ";
official_name["beta"]="beta-Lactose";
weight["beta"]=342.300;
weight["Ӥ"]=342.300;
simplification["beta"]="_";
nature["beta"]="non-natural";
id["beta"]=6134;

translate_modif["beta-1-azulenyl-l-alanine"]="Ӧ";
official_name["beta-1-azulenyl-l-alanine"]="beta-(1-Azulenyl)-l-alanine";
weight["beta-1-azulenyl-l-alanine"]=215.250;
weight["Ӧ"]=215.250;
simplification["beta-1-azulenyl-l-alanine"]="A";
nature["beta-1-azulenyl-l-alanine"]="non-natural";
id["beta-1-azulenyl-l-alanine"]=54147442;

translate_modif["beta-alanine"]="Ӑ";
official_name["beta-alanine"]="beta-Alanine";
weight["beta-alanine"]=89.090;
weight["Ӑ"]=89.090;
simplification["beta-alanine"]="A";
nature["beta-alanine"]="non-natural";
id["beta-alanine"]=239;

translate_modif["beta-alanineandx"]="Ӑ";
official_name["beta-alanineandx"]="beta-Alanine";
weight["beta-alanineandx"]=89.090;
weight["Ӑ"]=89.090;
simplification["beta-alanineandx"]="A";
nature["beta-alanineandx"]="non-natural";
id["beta-alanineandx"]=239;

translate_modif["betaasparticacid"]="Ө";
official_name["betaasparticacid"]="L-Aspartic_acid-15N";
weight["betaasparticacid"]=134.100;
weight["Ө"]=134.100;
simplification["betaasparticacid"]="D";
nature["betaasparticacid"]="non-natural";
id["betaasparticacid"]=16212100;

translate_modif["beta-cyclohexylalanine"]="Ӫ";
official_name["beta-cyclohexylalanine"]="(2S)-4-Cyclohexyl-2-((((9H-fluoren-9-yl)methoxy)carbonyl)amino)butanoic_acid";
weight["beta-cyclohexylalanine"]=407.500;
weight["Ӫ"]=407.500;
simplification["beta-cyclohexylalanine"]="A";
nature["beta-cyclohexylalanine"]="non-natural";
id["beta-cyclohexylalanine"]=17040133;

translate_modif["beta-homo-tryptophan"]="Ӭ";
official_name["beta-homo-tryptophan"]="Fmoc-L-beta-homotryptophan";
weight["beta-homo-tryptophan"]=440.500;
weight["Ӭ"]=440.500;
simplification["beta-homo-tryptophan"]="W";
nature["beta-homo-tryptophan"]="non-natural";
id["beta-homo-tryptophan"]=2761552;

translate_modif["beta-leucine"]="Ӯ";
official_name["beta-leucine"]="(r)-3-Amino-4-methylpentanoic_acid";
weight["beta-leucine"]=131.170;
weight["Ӯ"]=131.170;
simplification["beta-leucine"]="L";
nature["beta-leucine"]="non-natural";
id["beta-leucine"]=2761558;

translate_modif["beta-naphthylalanine"]="Ӱ";
official_name["beta-naphthylalanine"]="beta-Naphthylalanine";
weight["beta-naphthylalanine"]=251.710;
weight["Ӱ"]=251.710;
simplification["beta-naphthylalanine"]="A";
nature["beta-naphthylalanine"]="non-natural";
id["beta-naphthylalanine"]=6365580;

translate_modif["beta-napthylalanine"]="Ը";
official_name["beta-napthylalanine"]="1-Naphthyl-L-alanine";
weight["beta-napthylalanine"]=215.250;
weight["Ը"]=215.250;
simplification["beta-napthylalanine"]="A";
nature["beta-napthylalanine"]="non-natural";
id["beta-napthylalanine"]= 2724883;

translate_modif["biotin-aminohexanoyl"]="Ӳ";
official_name["biotin-aminohexanoyl"]="6-N-Biotinylaminohexanol";
weight["biotin-aminohexanoyl"]=343.500;
weight["Ӳ"]=343.500;
simplification["biotin-aminohexanoyl"]="_";
nature["biotin-aminohexanoyl"]="non-natural";
id["biotin-aminohexanoyl"]=46783689;

translate_modif["biphenylalanine"]="Ӵ";
official_name["biphenylalanine"]="(S)-3-([1,1'-Biphenyl]-4-yl)-2-aminopropanoic_acid";
weight["biphenylalanine"]=241.280;
weight["Ӵ"]=241.280;
simplification["biphenylalanine"]="F";
nature["biphenylalanine"]="non-natural";
id["biphenylalanine"]=2761815;

translate_modif["bromotryptophan"]="Ц";
official_name["bromotryptophan"]="6-Bromo-L-tryptophan";
weight["bromotryptophan"]=283.120;
weight["Ц"]=283.120;
simplification["bromotryptophan"]="W";
nature["bromotryptophan"]="non-natural";
id["bromotryptophan"]=9856996;

translate_modif["c6h6ch2ch3,r2"]="Ӷ";
official_name["c6h6ch2ch3,r2"]="Ethylbenzene";
weight["c6h6ch2ch3,r2"]=106.160;
weight["Ӷ"]=106.160;
simplification["c6h6ch2ch3,r2"]="_";
nature["c6h6ch2ch3,r2"]="non-natural";
id["c6h6ch2ch3,r2"]=7500;

translate_modif["carbobenzoxy"]="Ӹ";
official_name["carbobenzoxy"]="Benzyl_formate";
weight["carbobenzoxy"]=136.150;
weight["Ӹ"]=136.150;
simplification["carbobenzoxy"]="_";
nature["carbobenzoxy"]="non-natural";
id["carbobenzoxy"]=7708;

translate_modif["carbonpentaflouride"]="¢";
official_name["carbonpentaflouride"]="Carbon_Tetrachloride";
weight["carbonpentaflouride"]=153.800;
weight["¢"]=153.800;
simplification["carbonpentaflouride"]="_";
nature["carbonpentaflouride"]="non-natural";
id["carbonpentaflouride"]=5943;

translate_modif["carboxyglutamicacid"]="£";
official_name["carboxyglutamicacid"]="gamma-Carboxyglutamic_acid";
weight["carboxyglutamicacid"]=191.140;
weight["£"]=191.140;
simplification["carboxyglutamicacid"]="Q";
nature["carboxyglutamicacid"]="non-natural";
id["carboxyglutamicacid"]=104625;

translate_modif["ch2c6h5"]="¤";
official_name["ch2c6h5"]="Benzyl";
weight["ch2c6h5"]=91.130;
weight["¤"]=91.130;
simplification["ch2c6h5"]="_";
nature["ch2c6h5"]="non-natural";
id["ch2c6h5"]=123147;

translate_modif["ch2ch3"]="¥";
official_name["ch2ch3"]="Ethyl";
weight["ch2ch3"]=29.060;
weight["¥"]=29.060;
simplification["ch2ch3"]="_";
nature["ch2ch3"]="non-natural";
id["ch2ch3"]=123138;

translate_modif["ch2chch32"]="§";
official_name["ch2chch32"]="Isobutyl_radical";
weight["ch2chch32"]=57.110;
weight["§"]=57.110;
simplification["ch2chch32"]="_";
nature["ch2chch32"]="non-natural";
id["ch2chch32"]=138305;

translate_modif["ch3"]="¶";
official_name["ch3"]="Methane";
weight["ch3"]=16.043;
weight["¶"]=16.043;
simplification["ch3"]="_";
nature["ch3"]="non-natural";
id["ch3"]=297;

translate_modif["ch3chch3ch2ch3,r2"]="¼";
official_name["ch3chch3ch2ch3,r2"]="Isopentane";
weight["ch3chch3ch2ch3,r2"]=72.150;
weight["¼"]=72.150;
simplification["ch3chch3ch2ch3,r2"]="_";
nature["ch3chch3ch2ch3,r2"]="non-natural";
id["ch3chch3ch2ch3,r2"]=6556;

translate_modif["ch3chch3ch2ch3,r4"]="¼";
official_name["ch3chch3ch2ch3,r4"]="Isopentane";
weight["ch3chch3ch2ch3,r4"]=72.150;
weight["¼"]=72.150;
simplification["ch3chch3ch2ch3,r4"]="_";
nature["ch3chch3ch2ch3,r4"]="non-natural";
id["ch3chch3ch2ch3,r4"]=6556;

translate_modif["chloro-lysine"]="½";
official_name["chloro-lysine"]="Chlorolysine";
weight["chloro-lysine"]=180.630;
weight["½"]=180.630;
simplification["chloro-lysine"]="K";
nature["chloro-lysine"]="non-natural";
id["chloro-lysine"]=14459450;

translate_modif["chloro-tryptophan"]="¾";
official_name["chloro-tryptophan"]="7-Chlorotryptophan";
weight["chloro-tryptophan"]=238.670;
weight["¾"]=238.670;
simplification["chloro-tryptophan"]="W";
nature["chloro-tryptophan"]="non-natural";
id["chloro-tryptophan"]=643956;

translate_modif["cholesterol"]="˥";
official_name["cholesterol"]="Cholesterol";
weight["cholesterol"]=386.700;
weight["˥"]=386.700;
simplification["cholesterol"]="_";
nature["cholesterol"]="non-natural";
id["cholesterol"]=5997;

translate_modif["cholesteryl"]="˦";
official_name["cholesteryl"]="Cholesterylstearate";
weight["cholesteryl"]=653.100;
weight["˦"]=653.100;
simplification["cholesteryl"]="_";
nature["cholesteryl"]="non-natural";
id["cholesteryl"]=25009317;

translate_modif["citrulline"]="˧";
official_name["citrulline"]="Citrulline";
weight["citrulline"]=175.190;
weight["˧"]=175.190;
simplification["citrulline"]="_";
nature["citrulline"]="non-natural";
id["citrulline"]=9750;

translate_modif["cyclicx"]="˨";
official_name["cyclicx"]="Cyclen";
weight["cyclicx"]=172.270;
weight["˨"]=172.270;
simplification["cyclicx"]="_";
nature["cyclicx"]="non-natural";
id["cyclicx"]=64963;

translate_modif["cyclohexaglycine"]="˩";
official_name["cyclohexaglycine"]="D-Cyclohexylglycine";
weight["cyclohexaglycine"]=157.210;
weight["˩"]=157.210;
simplification["cyclohexaglycine"]="G";
nature["cyclohexaglycine"]="non-natural";
id["cyclohexaglycine"]=736849;

translate_modif["cyclohexyl-1-carboxylicacid"]="؟";
official_name["cyclohexyl-1-carboxylicacid"]="Cyclohexanecarboxylic_acid";
weight["cyclohexyl-1-carboxylicacid"]=128.169;
weight["؟"]=128.169;
simplification["cyclohexyl-1-carboxylicacid"]="_";
nature["cyclohexyl-1-carboxylicacid"]="non-natural";
id["cyclohexyl-1-carboxylicacid"]=7413;

translate_modif["cyclohexylalanine"]="฿";
official_name["cyclohexylalanine"]="(S)-2-amino-3-cyclohexylpropanoic_acid";
weight["cyclohexylalanine"]=171.240;
weight["฿"]=171.240;
simplification["cyclohexylalanine"]="A";
nature["cyclohexylalanine"]="non-natural";
id["cyclohexylalanine"]=712421;

translate_modif["cyclohexyl_alanine"]="†";
official_name["cyclohexyl_alanine"]="4-Cyclohexylaniline";
weight["cyclohexyl_alanine"]=175.270;
weight["†"]=175.270;
simplification["cyclohexyl_alanine"]="A";
nature["cyclohexyl_alanine"]="non-natural";
id["cyclohexyl_alanine"]=80764;

translate_modif["cyclohexylglycine"]="‡";
official_name["cyclohexylglycine"]="2-Amino-2-cyclohexylacetic_acid";
weight["cyclohexylglycine"]=157.210;
weight["‡"]=157.210;
simplification["cyclohexylglycine"]="G";
nature["cyclohexylglycine"]="non-natural";
id["cyclohexylglycine"]=224391;

translate_modif["cycloleucineor1-aminocyclopentane-1-carboxylicacidanddisulfidebondbetweencysandpen"]="‣";
official_name["cycloleucineor1-aminocyclopentane-1-carboxylicacidanddisulfidebondbetweencysandpen"]="3-Cyclopentylcyclohexan-1-amine";
weight["cycloleucineor1-aminocyclopentane-1-carboxylicacidanddisulfidebondbetweencysandpen"]=167.290;
weight["‣"]=167.290;
simplification["cycloleucineor1-aminocyclopentane-1-carboxylicacidanddisulfidebondbetweencysandpen"]="L";
nature["cycloleucineor1-aminocyclopentane-1-carboxylicacidanddisulfidebondbetweencysandpen"]="non-natural";
id["cycloleucineor1-aminocyclopentane-1-carboxylicacidanddisulfidebondbetweencysandpen"]=64506018;

translate_modif["cysteinylateddiaminobutyrate"]="Ғ";
official_name["cysteinylateddiaminobutyrate"]="(2S)-4-(acetylamino)-2-aminobutanoic_acid";
weight["cysteinylateddiaminobutyrate"]=160.170;
weight["Ғ"]=160.170;
simplification["cysteinylateddiaminobutyrate"]="C";
nature["cysteinylateddiaminobutyrate"]="non-natural";
id["cysteinylateddiaminobutyrate"]=441021;

translate_modif["d-1-naphthylalanine"]="‰";
official_name["d-1-naphthylalanine"]="3-(1-Naphthyl)-D-alanine";
weight["d-1-naphthylalanine"]=215.250;
weight["‰"]=215.250;
simplification["d-1-naphthylalanine"]="A";
nature["d-1-naphthylalanine"]="non-natural";
id["d-1-naphthylalanine"]=6950505;

translate_modif["d-2-naphthylalanine"]="‱";
official_name["d-2-naphthylalanine"]="Boc-3-(2-naphthyl)-D-alanine";
weight["d-2-naphthylalanine"]=315.400;
weight["‱"]=315.400;
simplification["d-2-naphthylalanine"]="A";
nature["d-2-naphthylalanine"]="non-natural";
id["d-2-naphthylalanine"]=2734481;

translate_modif["d-3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]="Њ";
official_name["d-3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]="6-Bromo-2-(trifluoromethyl)quinoline";
weight["d-3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]=276.050;
weight["Њ"]=276.050;
simplification["d-3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]="G";
nature["d-3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]="non-natural";
id["d-3-trifluoromethyl-bicyclopent-1,1,1-1-ylglycine"]=15325795;

translate_modif["dalanine"]="‽";
official_name["dalanine"]="N-(Carboxymethyl)-D-alanine";
weight["dalanine"]=147.130;
weight["‽"]=147.130;
simplification["dalanine"]="A";
nature["dalanine"]="non-natural";
id["dalanine"]=5462254;

translate_modif["d-alanine"]="a";
official_name["d-alanine"]="D-Alanine";
weight["d-alanine"]=89.090;
weight["a"]=89.090;
simplification["d-alanine"]="A";
nature["d-alanine"]="non-natural";
id["d-alanine"]=71080;

translate_modif["dallo-isoleucine"]="⁆";
official_name["dallo-isoleucine"]="D-Alloisoleucine";
weight["dallo-isoleucine"]=131.170;
weight["⁆"]=131.170;
simplification["dallo-isoleucine"]="I";
nature["dallo-isoleucine"]="non-natural";
id["dallo-isoleucine"]=94206;

translate_modif["d-allo-threonine"]="⁋";
official_name["d-allo-threonine"]="D-Allothreonine";
weight["d-allo-threonine"]=119.120;
weight["⁋"]=119.120;
simplification["d-allo-threonine"]="T";
nature["d-allo-threonine"]="non-natural";
id["d-allo-threonine"]=90624;

translate_modif["dalpha-aminobutyrate"]="₠";
official_name["dalpha-aminobutyrate"]="D-2-Aminobutyric_acid";
weight["dalpha-aminobutyrate"]=103.120;
weight["₠"]=103.120;
simplification["dalpha-aminobutyrate"]="_";
nature["dalpha-aminobutyrate"]="non-natural";
id["dalpha-aminobutyrate"]=439691;

translate_modif["d-aminoacid"]="₡";
official_name["d-aminoacid"]="5-Aminouracil";
weight["d-aminoacid"]=127.100;
weight["₡"]=127.100;
simplification["d-aminoacid"]="_";
nature["d-aminoacid"]="non-natural";
id["d-aminoacid"]=13611;

translate_modif["daminoacids"]="₢";
official_name["daminoacids"]="Daminozide";
weight["daminoacids"]=160.170;
weight["₢"]=160.170;
simplification["daminoacids"]="_";
nature["daminoacids"]="non-natural";
id["daminoacids"]=15331;

translate_modif["d-aminoacids"]="₣";
official_name["d-aminoacids"]="Aminoacyl_tRNA_synthetase-IN-1";
weight["d-aminoacids"]=459.500;
weight["₣"]=459.500;
simplification["d-aminoacids"]="_";
nature["d-aminoacids"]="non-natural";
id["d-aminoacids"]=10095725;

translate_modif["darginine"]="R";
official_name["darginine"]="Arginine";
weight["darginine"]=174.200;
weight["R"]=174.200;
simplification["darginine"]="R";
nature["darginine"]="non-natural";
id["darginine"]=6322;

translate_modif["d-arginine"]="r";
official_name["d-arginine"]="D-Arginine";
weight["d-arginine"]=174.200;
weight["r"]=174.200;
simplification["d-arginine"]="R";
nature["d-arginine"]="non-natural";
id["d-arginine"]=71070;

translate_modif["dasparagine"]="N";
official_name["dasparagine"]="Asparagine";
weight["dasparagine"]=132.120;
weight["N"]=132.120;
simplification["dasparagine"]="N";
nature["dasparagine"]="non-natural";
id["dasparagine"]=6267;

translate_modif["d-asparagine"]="n";
official_name["d-asparagine"]="D-Asparagine";
weight["d-asparagine"]=132.120;
weight["n"]=132.120;
simplification["d-asparagine"]="N";
nature["d-asparagine"]="non-natural";
id["d-asparagine"]=439600;

translate_modif["dasparticacid"]="Ө";
official_name["dasparticacid"]="L-Aspartic_acid-15N";
weight["dasparticacid"]=134.100;
weight["Ө"]=134.100;
simplification["dasparticacid"]="D";
nature["dasparticacid"]="non-natural";
id["dasparticacid"]=16212100;

translate_modif["d-beta-naphthylalanine"]="₨";
official_name["d-beta-naphthylalanine"]="3-(2-Naphthyl)-D-alanine";
weight["d-beta-naphthylalanine"]=215.250;
weight["₨"]=215.250;
simplification["d-beta-naphthylalanine"]="A";
nature["d-beta-naphthylalanine"]="non-natural";
id["d-beta-naphthylalanine"]=2733661;

translate_modif["d-biphenylalanine"]="₩";
official_name["d-biphenylalanine"]="(R)-3-([1,1'-Biphenyl]-4-yl)-2-aminopropanoic_acid";
weight["d-biphenylalanine"]=241.280;
weight["₩"]=241.280;
simplification["d-biphenylalanine"]="F";
nature["d-biphenylalanine"]="non-natural";
id["d-biphenylalanine"]=7006711;

translate_modif["dcysteine"]="C";
official_name["dcysteine"]="Cysteine";
weight["dcysteine"]=121.160;
weight["C"]=121.160;
simplification["dcysteine"]="C";
nature["dcysteine"]="non-natural";
id["dcysteine"]=5862;

translate_modif["d-cysteine"]="c";
official_name["d-cysteine"]="D-cysteine";
weight["d-cysteine"]=121.160;
weight["c"]=121.160;
simplification["d-cysteine"]="C";
nature["d-cysteine"]="non-natural";
id["d-cysteine"]=92851;

translate_modif["d-diaminobutyrate"]="€";
official_name["d-diaminobutyrate"]="L-2,4-diaminobutyric_acid";
weight["d-diaminobutyrate"]=118.130;
weight["€"]=118.130;
simplification["d-diaminobutyrate"]="_";
nature["d-diaminobutyrate"]="non-natural";
id["d-diaminobutyrate"]=134490;

translate_modif["d-diaminopropionicacid"]="Խ";
official_name["d-diaminopropionicacid"]="2,3-Diaminopropionic_acid";
weight["d-diaminopropionicacid"]=104.110;
weight["Խ"]=104.110;
simplification["d-diaminopropionicacid"]="_";
nature["d-diaminopropionicacid"]="non-natural";
id["d-diaminopropionicacid"]=364;

translate_modif["d-diphenylalanine"]="₭";
official_name["d-diphenylalanine"]="beta-Phenyl-D-phenylalanine";
weight["d-diphenylalanine"]=241.280;
weight["₭"]=241.280;
simplification["d-diphenylalanine"]="F";
nature["d-diphenylalanine"]="non-natural";
id["d-diphenylalanine"]=1520861;

translate_modif["decanoyl"]="₮";
official_name["decanoyl"]="Decanoyl_chloride";
weight["decanoyl"]=190.710;
weight["₮"]=190.710;
simplification["decanoyl"]="_";
nature["decanoyl"]="non-natural";
id["decanoyl"]=66982;

translate_modif["dehydro-leucine"]="₯";
official_name["dehydro-leucine"]="(2S)-2-amino-4-methyl-4-pentenoic_acid";
weight["dehydro-leucine"]=129.160;
weight["₯"]=129.160;
simplification["dehydro-leucine"]="L";
nature["dehydro-leucine"]="non-natural";
id["dehydro-leucine"]=7000158;

translate_modif["dehydroserine"]="₰";
official_name["dehydroserine"]="Dehydroserine";
weight["dehydroserine"]=103.080;
weight["₰"]=103.080;
simplification["dehydroserine"]="S";
nature["dehydroserine"]="non-natural";
id["dehydroserine"]=3037689;

translate_modif["dehydrothreonine"]="₱";
official_name["dehydrothreonine"]="Dehydrocrebanine";
weight["dehydrothreonine"]=337.400;
weight["₱"]=337.400;
simplification["dehydrothreonine"]="T";
nature["dehydrothreonine"]="non-natural";
id["dehydrothreonine"]=149600;

translate_modif["dehydro-valine"]="₲";
official_name["dehydro-valine"]="Fmoc-2,3-dehydroval-OH";
weight["dehydro-valine"]=337.400;
weight["₲"]=337.400;
simplification["dehydro-valine"]="V";
nature["dehydro-valine"]="non-natural";
id["dehydro-valine"]=51340546;

translate_modif["dextro"]="₳";
official_name["dextro"]="Dextroamphetamine";
weight["dextro"]=135.210;
weight["₳"]=135.210;
simplification["dextro"]="_";
nature["dextro"]="non-natural";
id["dextro"]=5826;

translate_modif["d-fluorenylglycine"]="₴";
official_name["d-fluorenylglycine"]="3,4-Difluorophenylglycine";
weight["d-fluorenylglycine"]=187.140;
weight["₴"]=187.140;
simplification["d-fluorenylglycine"]="G";
nature["d-fluorenylglycine"]="non-natural";
id["d-fluorenylglycine"]=2737052;

translate_modif["d-gamma-carboxyglutamicacid"]="₵";
official_name["d-gamma-carboxyglutamicacid"]="4-Hydroxy-L-glutamic_acid";
weight["d-gamma-carboxyglutamicacid"]=163.130;
weight["₵"]=163.130;
simplification["d-gamma-carboxyglutamicacid"]="Q";
nature["d-gamma-carboxyglutamicacid"]="non-natural";
id["d-gamma-carboxyglutamicacid"]=439902;

translate_modif["d-gamma-carboxyglutamicacidcha"]="£";
official_name["d-gamma-carboxyglutamicacidcha"]="gamma-Carboxyglutamic_acid";
weight["d-gamma-carboxyglutamicacidcha"]=191.140;
weight["£"]=191.140;
simplification["d-gamma-carboxyglutamicacidcha"]="Q";
nature["d-gamma-carboxyglutamicacidcha"]="non-natural";
id["d-gamma-carboxyglutamicacidcha"]=104625;

translate_modif["dglutamine"]="Q";
official_name["dglutamine"]="Glutamine";
weight["dglutamine"]=146.140;
weight["Q"]=146.140;
simplification["dglutamine"]="Q";
nature["dglutamine"]="non-natural";
id["dglutamine"]=5961;

translate_modif["dhistidine"]="H";
official_name["dhistidine"]="Histidine";
weight["dhistidine"]=155.150;
weight["H"]=155.150;
simplification["dhistidine"]="H";
nature["dhistidine"]="non-natural";
id["dhistidine"]=6274;

translate_modif["d-histidine"]="h";
official_name["d-histidine"]="D-Histidine";
weight["d-histidine"]=155.150;
weight["h"]=155.150;
simplification["d-histidine"]="H";
nature["d-histidine"]="non-natural";
id["d-histidine"]=71083;

translate_modif["d-homophenylalanine"]="₽";
official_name["d-homophenylalanine"]="Homophenylalanine,_D-";
weight["d-homophenylalanine"]=179.220;
weight["₽"]=179.220;
simplification["d-homophenylalanine"]="F";
nature["d-homophenylalanine"]="non-natural";
id["d-homophenylalanine"]=6950302;

translate_modif["d-hydroxyproline"]="И";
official_name["d-hydroxyproline"]="Hydroxyproline";
weight["d-hydroxyproline"]=131.130;
weight["И"]=131.130;
simplification["d-hydroxyproline"]="P";
nature["d-hydroxyproline"]="non-natural";
id["d-hydroxyproline"]=5810;

translate_modif["diaminobutyrate"]="€";
official_name["diaminobutyrate"]="L-2,4-diaminobutyric_acid";
weight["diaminobutyrate"]=118.130;
weight["€"]=118.130;
simplification["diaminobutyrate"]="_";
nature["diaminobutyrate"]="non-natural";
id["diaminobutyrate"]=134490;

translate_modif["diaminopropane"]="ℂ";
official_name["diaminopropane"]="1,3-Diaminopropane";
weight["diaminopropane"]=74.130;
weight["ℂ"]=74.130;
simplification["diaminopropane"]="_";
nature["diaminopropane"]="non-natural";
id["diaminopropane"]=428;

translate_modif["diaminopropionicacid"]="Խ";
official_name["diaminopropionicacid"]="2,3-Diaminopropionic_acid";
weight["diaminopropionicacid"]=104.110;
weight["Խ"]=104.110;
simplification["diaminopropionicacid"]="_";
nature["diaminopropionicacid"]="non-natural";
id["diaminopropionicacid"]=364;

translate_modif["didehydrophenylalanine"]="℅";
official_name["didehydrophenylalanine"]="Phenyldehydroalanine";
weight["didehydrophenylalanine"]=163.170;
weight["℅"]=163.170;
simplification["didehydrophenylalanine"]="F";
nature["didehydrophenylalanine"]="non-natural";
id["didehydrophenylalanine"]=5702627;

translate_modif["diethylglycine"]="ℍ";
official_name["diethylglycine"]="N,N-Diethylglycine";
weight["diethylglycine"]=131.170;
weight["ℍ"]=131.170;
simplification["diethylglycine"]="G";
nature["diethylglycine"]="non-natural";
id["diethylglycine"]=74151;

translate_modif["dihydroxyarginine"]="ℎ";
official_name["dihydroxyarginine"]="2,8-Dihydroxyadenine";
weight["dihydroxyarginine"]=167.130;
weight["ℎ"]=167.130;
simplification["dihydroxyarginine"]="R";
nature["dihydroxyarginine"]="non-natural";
id["dihydroxyarginine"]=92268;

translate_modif["dihydroxylysine"]="ℏ";
official_name["dihydroxylysine"]="Xanthine";
weight["dihydroxylysine"]=152.110;
weight["ℏ"]=152.110;
simplification["dihydroxylysine"]="K";
nature["dihydroxylysine"]="non-natural";
id["dihydroxylysine"]=1188;

translate_modif["diiodotyrosine"]="Փ";
official_name["diiodotyrosine"]="3,5-Diiodo-L-tyrosine";
weight["diiodotyrosine"]=432.980;
weight["Փ"]=432.980;
simplification["diiodotyrosine"]="Y";
nature["diiodotyrosine"]="non-natural";
id["diiodotyrosine"]=9305;

translate_modif["dimethylarginineanti-symmetric"]="Ӗ";
official_name["dimethylarginineanti-symmetric"]="N,N-Dimethylarginine";
weight["dimethylarginineanti-symmetric"]=202.250;
weight["Ӗ"]=202.250;
simplification["dimethylarginineanti-symmetric"]="R";
nature["dimethylarginineanti-symmetric"]="non-natural";
id["dimethylarginineanti-symmetric"]=123831;

translate_modif["dimethyl-d-ornithine"]="ℕ";
official_name["dimethyl-d-ornithine"]="Paraxanthine";
weight["dimethyl-d-ornithine"]=180.160;
weight["ℕ"]=180.160;
simplification["dimethyl-d-ornithine"]="_";
nature["dimethyl-d-ornithine"]="non-natural";
id["dimethyl-d-ornithine"]=4687;

translate_modif["diphenylalanine"]="№";
official_name["diphenylalanine"]="(2S)-2-(N-phenylanilino)propanoic_acid";
weight["diphenylalanine"]=241.280;
weight["№"]=241.280;
simplification["diphenylalanine"]="F";
nature["diphenylalanine"]="non-natural";
id["diphenylalanine"]=54295913;

translate_modif["d-isoglutamaticacid"]="℗";
official_name["d-isoglutamaticacid"]="3-Aminopentanedioic_acid";
weight["d-isoglutamaticacid"]=147.130;
weight["℗"]=147.130;
simplification["d-isoglutamaticacid"]="Q";
nature["d-isoglutamaticacid"]="non-natural";
id["d-isoglutamaticacid"]=73064;

translate_modif["disoleucine"]="ℙ";
official_name["disoleucine"]="l-Isoleucine";
weight["disoleucine"]=131.170;
weight["ℙ"]=131.170;
simplification["disoleucine"]="I";
nature["disoleucine"]="non-natural";
id["disoleucine"]=6306;

translate_modif["disulphide"]="ℚ";
official_name["disulphide"]="Disulfur";
weight["disulphide"]=64.129;
weight["ℚ"]=64.129;
simplification["disulphide"]="_";
nature["disulphide"]="non-natural";
id["disulphide"]=5460602;

translate_modif["dleucine"]="L";
official_name["dleucine"]="Leucine";
weight["dleucine"]=131.170;
weight["L"]=131.170;
simplification["dleucine"]="L";
nature["dleucine"]="non-natural";
id["dleucine"]=6106;

translate_modif["d-leucine"]="l";
official_name["d-leucine"]="D-Leucine";
weight["d-leucine"]=131.170;
weight["l"]=131.170;
simplification["d-leucine"]="L";
nature["d-leucine"]="non-natural";
id["d-leucine"]=439524;

translate_modif["dlysine"]="℮";
official_name["dlysine"]="Lysine,_DL-";
weight["dlysine"]=146.190;
weight["℮"]=146.190;
simplification["dlysine"]="K";
nature["dlysine"]="non-natural";
id["dlysine"]=866;

translate_modif["d-lysine"]="k";
official_name["d-lysine"]="D-Lysine";
weight["d-lysine"]=146.190;
weight["k"]=146.190;
simplification["d-lysine"]="K";
nature["d-lysine"]="non-natural";
id["d-lysine"]=57449;

translate_modif["d-methionine"]="m";
official_name["d-methionine"]="D-Methionine";
weight["d-methionine"]=149.210;
weight["m"]=149.210;
simplification["d-methionine"]="M";
nature["d-methionine"]="non-natural";
id["d-methionine"]=84815;

translate_modif["d-methyltryptophan"]="∂";
official_name["d-methyltryptophan"]="4-Methyltryptophan";
weight["d-methyltryptophan"]=218.250;
weight["∂"]=218.250;
simplification["d-methyltryptophan"]="W";
nature["d-methyltryptophan"]="non-natural";
id["d-methyltryptophan"]=150886;

translate_modif["d-n-methylalanine"]="Է";
official_name["d-n-methylalanine"]="2-Aminoisobutyric_acid";
weight["d-n-methylalanine"]=103.120;
weight["Է"]=103.120;
simplification["d-n-methylalanine"]="A";
nature["d-n-methylalanine"]="non-natural";
id["d-n-methylalanine"]=6119;

translate_modif["d-n-methylleucine"]="∃";
official_name["d-n-methylleucine"]="N-Methylleucine,_D-";
weight["d-n-methylleucine"]=145.200;
weight["∃"]=145.200;
simplification["d-n-methylleucine"]="L";
nature["d-n-methylleucine"]="non-natural";
id["d-n-methylleucine"]=6951123;

translate_modif["d-n-methyltyrosine"]="∄";
official_name["d-n-methyltyrosine"]="O-Methyltyrosine";
weight["d-n-methyltyrosine"]=195.210;
weight["∄"]=195.210;
simplification["d-n-methyltyrosine"]="Y";
nature["d-n-methyltyrosine"]="non-natural";
id["d-n-methyltyrosine"]=97118;

translate_modif["dnorleucine"]="∅";
official_name["dnorleucine"]="L-Norleucine";
weight["dnorleucine"]=131.170;
weight["∅"]=131.170;
simplification["dnorleucine"]="L";
nature["dnorleucine"]="non-natural";
id["dnorleucine"]=21236;

translate_modif["d-norvaline"]="∆";
official_name["d-norvaline"]="D-Norvaline";
weight["d-norvaline"]=117.150;
weight["∆"]=117.150;
simplification["d-norvaline"]="V";
nature["d-norvaline"]="non-natural";
id["d-norvaline"]=439575;

translate_modif["dornithine"]="O";
official_name["dornithine"]="L-ornithine";
weight["dornithine"]=132.160;
weight["O"]=132.160;
simplification["dornithine"]="_";
nature["dornithine"]="non-natural";
id["dornithine"]=6262;

translate_modif["d-ornithine"]="o";
official_name["d-ornithine"]="D-Ornithine";
weight["d-ornithine"]=132.160;
weight["o"]=132.160;
simplification["d-ornithine"]="_";
nature["d-ornithine"]="non-natural";
id["d-ornithine"]=71082;

translate_modif["dphenylalanine"]="∉";
official_name["dphenylalanine"]="(2S)-2-amino-2-phenylpropanoic_acid";
weight["dphenylalanine"]=165.190;
weight["∉"]=165.190;
simplification["dphenylalanine"]="F";
nature["dphenylalanine"]="non-natural";
id["dphenylalanine"]=853461;

translate_modif["d-phenylalanine"]="∉";
official_name["d-phenylalanine"]="(2S)-2-amino-2-phenylpropanoic_acid";
weight["d-phenylalanine"]=165.190;
weight["∉"]=165.190;
simplification["d-phenylalanine"]="F";
nature["d-phenylalanine"]="non-natural";
id["d-phenylalanine"]=853461;

translate_modif["dproline"]="∊";
official_name["dproline"]="N-Acetyl-L-proline";
weight["dproline"]=157.170;
weight["∊"]=157.170;
simplification["dproline"]="P";
nature["dproline"]="non-natural";
id["dproline"]=66141;

translate_modif["d-prolinecha"]="P";
official_name["d-prolinecha"]="Proline";
weight["d-prolinecha"]=115.130;
weight["P"]=115.130;
simplification["d-prolinecha"]="P";
nature["d-prolinecha"]="non-natural";
id["d-prolinecha"]=145742;

translate_modif["dpyroglutamicacid"]="∌";
official_name["dpyroglutamicacid"]="L-Pyroglutamic_acid";
weight["dpyroglutamicacid"]=129.110;
weight["∌"]=129.110;
simplification["dpyroglutamicacid"]="Q";
nature["dpyroglutamicacid"]="non-natural";
id["dpyroglutamicacid"]=7405;

translate_modif["dserine"]="S";
official_name["dserine"]="Serine";
weight["dserine"]=105.090;
weight["S"]=105.090;
simplification["dserine"]="S";
nature["dserine"]="non-natural";
id["dserine"]=5951;

translate_modif["d-tetrahydroisoquinoline-3-carboxylicacid"]="∏";
official_name["d-tetrahydroisoquinoline-3-carboxylicacid"]="1,2,3,4-Tetrahydroisoquinoline";
weight["d-tetrahydroisoquinoline-3-carboxylicacid"]=133.190;
weight["∏"]=133.190;
simplification["d-tetrahydroisoquinoline-3-carboxylicacid"]="_";
nature["d-tetrahydroisoquinoline-3-carboxylicacid"]="non-natural";
id["d-tetrahydroisoquinoline-3-carboxylicacid"]=7046;

translate_modif["dthreonine"]="T";
official_name["dthreonine"]="Threonine";
weight["dthreonine"]=119.120;
weight["T"]=119.120;
simplification["dthreonine"]="T";
nature["dthreonine"]="non-natural";
id["dthreonine"]=6288;

translate_modif["dtryptophan"]="W";
official_name["dtryptophan"]="Tryptophan";
weight["dtryptophan"]=204.220;
weight["W"]=204.220;
simplification["dtryptophan"]="W";
nature["dtryptophan"]="non-natural";
id["dtryptophan"]=6305;

translate_modif["d-tryptophan"]="w";
official_name["d-tryptophan"]="D-Tryptophan";
weight["d-tryptophan"]=204.220;
weight["w"]=204.220;
simplification["d-tryptophan"]="W";
nature["d-tryptophan"]="non-natural";
id["d-tryptophan"]=9060;

translate_modif["dtyrosine"]="Y";
official_name["dtyrosine"]="Tyrosine";
weight["dtyrosine"]=181.190;
weight["Y"]=181.190;
simplification["dtyrosine"]="Y";
nature["dtyrosine"]="non-natural";
id["dtyrosine"]=6057;

translate_modif["d-tyrosine"]="y";
official_name["d-tyrosine"]="D-tyrosine";
weight["d-tyrosine"]=181.190;
weight["y"]=181.190;
simplification["d-tyrosine"]="Y";
nature["d-tyrosine"]="non-natural";
id["d-tyrosine"]=71098;

translate_modif["dvaline"]="∜";
official_name["dvaline"]="N-Acetyl-L-valine";
weight["dvaline"]=159.180;
weight["∜"]=159.180;
simplification["dvaline"]="V";
nature["dvaline"]="non-natural";
id["dvaline"]=66789;

translate_modif["enduracididine"]="∝";
official_name["enduracididine"]="Enduracididine";
weight["enduracididine"]=172.190;
weight["∝"]=172.190;
simplification["enduracididine"]="_";
nature["enduracididine"]="non-natural";
id["enduracididine"]=15284838;

translate_modif["epsilon-aminocaproicacid"]="K";
official_name["epsilon-aminocaproicacid"]="Lysine";
weight["epsilon-aminocaproicacid"]=146.190;
weight["K"]=146.190;
simplification["epsilon-aminocaproicacid"]="_";
nature["epsilon-aminocaproicacid"]="non-natural";
id["epsilon-aminocaproicacid"]=5962;

translate_modif["epsilon-aminocaproicacidresidue"]="℮";
official_name["epsilon-aminocaproicacidresidue"]="Lysine,_DL-";
weight["epsilon-aminocaproicacidresidue"]=146.190;
weight["℮"]=146.190;
simplification["epsilon-aminocaproicacidresidue"]="_";
nature["epsilon-aminocaproicacidresidue"]="non-natural";
id["epsilon-aminocaproicacidresidue"]=866;

translate_modif["ethyl-cysteine"]="∟";
official_name["ethyl-cysteine"]="Ethyl-cysteine";
weight["ethyl-cysteine"]=149.210;
weight["∟"]=149.210;
simplification["ethyl-cysteine"]="C";
nature["ethyl-cysteine"]="non-natural";
id["ethyl-cysteine"]=30993;

translate_modif["ethylestet"]="∠";
official_name["ethylestet"]="Ethylene_glycol_diacetate";
weight["ethylestet"]=146.140;
weight["∠"]=146.140;
simplification["ethylestet"]="_";
nature["ethylestet"]="non-natural";
id["ethylestet"]=8121;

translate_modif["ethylnorvaline"]="∤";
official_name["ethylnorvaline"]="2-Amino-3-ethylpentanoic_acid";
weight["ethylnorvaline"]=145.200;
weight["∤"]=145.200;
simplification["ethylnorvaline"]="V";
nature["ethylnorvaline"]="non-natural";
id["ethylnorvaline"]=518928;

translate_modif["fluorenylglycine"]="∧";
official_name["fluorenylglycine"]="N-o-Fluorophenylglycine";
weight["fluorenylglycine"]=169.150;
weight["∧"]=169.150;
simplification["fluorenylglycine"]="G";
nature["fluorenylglycine"]="non-natural";
id["fluorenylglycine"]=5171030;

translate_modif["fluoromethylketone"]="∨";
official_name["fluoromethylketone"]="1,3-Difluoroacetone";
weight["fluoromethylketone"]=94.060;
weight["∨"]=94.060;
simplification["fluoromethylketone"]="_";
nature["fluoromethylketone"]="non-natural";
id["fluoromethylketone"]=228397;

translate_modif["fumaricacid"]="∩";
official_name["fumaricacid"]="2-Formamidobutanoic_acid";
weight["fumaricacid"]=131.130;
weight["∩"]=131.130;
simplification["fumaricacid"]="_";
nature["fumaricacid"]="non-natural";
id["fumaricacid"]=559455;

translate_modif["gamma-aminobutyricacid"]="Ұ";
official_name["gamma-aminobutyricacid"]="4-Glycylaminobutyric_acid";
weight["gamma-aminobutyricacid"]=160.170;
weight["Ұ"]=160.170;
simplification["gamma-aminobutyricacid"]="_";
nature["gamma-aminobutyricacid"]="non-natural";
id["gamma-aminobutyricacid"]=99281;

translate_modif["gamma-carboxyglutamicacid"]="₵";
official_name["gamma-carboxyglutamicacid"]="4-Hydroxy-L-glutamic_acid";
weight["gamma-carboxyglutamicacid"]=163.130;
weight["₵"]=163.130;
simplification["gamma-carboxyglutamicacid"]="Q";
nature["gamma-carboxyglutamicacid"]="non-natural";
id["gamma-carboxyglutamicacid"]=439902;

translate_modif["gammacarboxylicglutamicacid"]="£";
official_name["gammacarboxylicglutamicacid"]="gamma-Carboxyglutamic_acid";
weight["gammacarboxylicglutamicacid"]=191.140;
weight["£"]=191.140;
simplification["gammacarboxylicglutamicacid"]="Q";
nature["gammacarboxylicglutamicacid"]="non-natural";
id["gammacarboxylicglutamicacid"]=104625;

translate_modif["gamma_carboxylic_glutamic_acid"]="£";
official_name["gamma_carboxylic_glutamic_acid"]="gamma-Carboxyglutamic_acid";
weight["gamma_carboxylic_glutamic_acid"]=191.140;
weight["£"]=191.140;
simplification["gamma_carboxylic_glutamic_acid"]="Q";
nature["gamma_carboxylic_glutamic_acid"]="non-natural";
id["gamma_carboxylic_glutamic_acid"]=104625;

translate_modif["gamma-hydroxy-d-valine"]="∪";
official_name["gamma-hydroxy-d-valine"]="3-Hydroxynorvaline";
weight["gamma-hydroxy-d-valine"]=133.150;
weight["∪"]=133.150;
simplification["gamma-hydroxy-d-valine"]="V";
nature["gamma-hydroxy-d-valine"]="non-natural";
id["gamma-hydroxy-d-valine"]=65097;

translate_modif["glutathionylated_cysteine"]="∫";
official_name["glutathionylated_cysteine"]="Glutathionylspermidine";
weight["glutathionylated_cysteine"]=434.600;
weight["∫"]=434.600;
simplification["glutathionylated_cysteine"]="C";
nature["glutathionylated_cysteine"]="non-natural";
id["glutathionylated_cysteine"]=440772;

translate_modif["g-sagamma-samino-d-proline"]="≎";
official_name["g-sagamma-samino-d-proline"]="N-Amino-D-proline";
weight["g-sagamma-samino-d-proline"]=130.150;
weight["≎"]=130.150;
simplification["g-sagamma-samino-d-proline"]="P";
nature["g-sagamma-samino-d-proline"]="non-natural";
id["g-sagamma-samino-d-proline"]=2737005;

translate_modif["guanidinopropionicacid"]="≍";
official_name["guanidinopropionicacid"]="3-Guanidinopropionic_acid";
weight["guanidinopropionicacid"]=131.130;
weight["≍"]=131.130;
simplification["guanidinopropionicacid"]="_";
nature["guanidinopropionicacid"]="non-natural";
id["guanidinopropionicacid"]=67701;

translate_modif["h"]="≌";
official_name["h"]="Hydrogen";
weight["h"]=2.016;
weight["≌"]=2.016;
simplification["h"]="_";
nature["h"]="non-natural";
id["h"]=0.000;

translate_modif["hexapeptidepeg4linker"]="≋";
official_name["hexapeptidepeg4linker"]="Hexapeptide-9";
weight["hexapeptidepeg4linker"]=582.600;
weight["≋"]=582.600;
simplification["hexapeptidepeg4linker"]="_";
nature["hexapeptidepeg4linker"]="non-natural";
id["hexapeptidepeg4linker"]=16129319;

translate_modif["homoarginine"]="≊";
official_name["homoarginine"]="Homoarginine";
weight["homoarginine"]=188.230;
weight["≊"]=188.230;
simplification["homoarginine"]="R";
nature["homoarginine"]="non-natural";
id["homoarginine"]=9085;

translate_modif["homocysteine"]="∻";
official_name["homocysteine"]="L-Homocysteine";
weight["homocysteine"]=135.190;
weight["∻"]=135.190;
simplification["homocysteine"]="C";
nature["homocysteine"]="non-natural";
id["homocysteine"]=91552;

translate_modif["homocysteineinwhichthesidechainisonemethylenegrouplongerthanincysteine"]="∻";
official_name["homocysteineinwhichthesidechainisonemethylenegrouplongerthanincysteine"]="L-Homocysteine";
weight["homocysteineinwhichthesidechainisonemethylenegrouplongerthanincysteine"]=135.190;
weight["∻"]=135.190;
simplification["homocysteineinwhichthesidechainisonemethylenegrouplongerthanincysteine"]="C";
nature["homocysteineinwhichthesidechainisonemethylenegrouplongerthanincysteine"]="non-natural";
id["homocysteineinwhichthesidechainisonemethylenegrouplongerthanincysteine"]=91552;

translate_modif["homoleucine"]="∺";
official_name["homoleucine"]="Fmoc-D-homoleucine";
weight["homoleucine"]=367.400;
weight["∺"]=367.400;
simplification["homoleucine"]="L";
nature["homoleucine"]="non-natural";
id["homoleucine"]=2756131;

translate_modif["homophenylalanine"]="Г";
official_name["homophenylalanine"]="L-Homophenylalanine";
weight["homophenylalanine"]=179.220;
weight["Г"]=179.220;
simplification["homophenylalanine"]="F";
nature["homophenylalanine"]="non-natural";
id["homophenylalanine"]=2724505;

translate_modif["homophenylalaninehyp"]="Г";
official_name["homophenylalaninehyp"]="L-Homophenylalanine";
weight["homophenylalaninehyp"]=179.220;
weight["Г"]=179.220;
simplification["homophenylalaninehyp"]="F";
nature["homophenylalaninehyp"]="non-natural";
id["homophenylalaninehyp"]=2724505;

translate_modif["homoserine"]="∹";
official_name["homoserine"]="L-homoserine";
weight["homoserine"]=119.120;
weight["∹"]=119.120;
simplification["homoserine"]="S";
nature["homoserine"]="non-natural";
id["homoserine"]=12647;

translate_modif["homo-tryptophan"]="Ӭ";
official_name["homo-tryptophan"]="Fmoc-L-beta-homotryptophan";
weight["homo-tryptophan"]=440.500;
weight["Ӭ"]=440.500;
simplification["homo-tryptophan"]="W";
nature["homo-tryptophan"]="non-natural";
id["homo-tryptophan"]=2761552;

translate_modif["hydoxyasparaginyl"]="∷";
official_name["hydoxyasparaginyl"]="3-Hydroxyasparagine";
weight["hydoxyasparaginyl"]=148.120;
weight["∷"]=148.120;
simplification["hydoxyasparaginyl"]="N";
nature["hydoxyasparaginyl"]="non-natural";
id["hydoxyasparaginyl"]=152191;

translate_modif["hydrogen"]="≌";
official_name["hydrogen"]="Hydrogen";
weight["hydrogen"]=2.016;
weight["≌"]=2.016;
simplification["hydrogen"]="_";
nature["hydrogen"]="non-natural";
id["hydrogen"]=0.000;

translate_modif["hydroxyasparticacid"]="≏";
official_name["hydroxyasparticacid"]="3-Hydroxyaspartic_acid";
weight["hydroxyasparticacid"]=149.100;
weight["≏"]=149.100;
simplification["hydroxyasparticacid"]="D";
nature["hydroxyasparticacid"]="non-natural";
id["hydroxyasparticacid"]=5425;

translate_modif["hydroxyl"]="Є";
official_name["hydroxyl"]="Hydroxylamine";
weight["hydroxyl"]=33.030;
weight["Є"]=33.030;
simplification["hydroxyl"]="_";
nature["hydroxyl"]="non-natural";
id["hydroxyl"]=787;

translate_modif["hydroxy-l-asparagine"]="≐";
official_name["hydroxy-l-asparagine"]="L-Aspartic_acid_beta-hydroxamate";
weight["hydroxy-l-asparagine"]=148.120;
weight["≐"]=148.120;
simplification["hydroxy-l-asparagine"]="N";
nature["hydroxy-l-asparagine"]="non-natural";
id["hydroxy-l-asparagine"]=97663;

translate_modif["hydroxyleucyl"]="≑";
official_name["hydroxyleucyl"]="L-Apiose";
weight["hydroxyleucyl"]=150.130;
weight["≑"]=150.130;
simplification["hydroxyleucyl"]="L";
nature["hydroxyleucyl"]="non-natural";
id["hydroxyleucyl"]=12306753;

translate_modif["hydroxylysine"]="Ы";
official_name["hydroxylysine"]="5-Hydroxylysine";
weight["hydroxylysine"]=162.190;
weight["Ы"]=162.190;
simplification["hydroxylysine"]="K";
nature["hydroxylysine"]="non-natural";
id["hydroxylysine"]=3032849;

translate_modif["hydroxymethylserine"]="≒";
official_name["hydroxymethylserine"]="alpha-(Hydroxymethyl)serine";
weight["hydroxymethylserine"]=135.120;
weight["≒"]=135.120;
simplification["hydroxymethylserine"]="S";
nature["hydroxymethylserine"]="non-natural";
id["hydroxymethylserine"]=439893;

translate_modif["hydroxyproline"]="И";
official_name["hydroxyproline"]="Hydroxyproline";
weight["hydroxyproline"]=131.130;
weight["И"]=131.130;
simplification["hydroxyproline"]="P";
nature["hydroxyproline"]="non-natural";
id["hydroxyproline"]=5810;

translate_modif["iodine-127"]="≓";
official_name["iodine-127"]="Iodine";
weight["iodine-127"]=253.808;
weight["≓"]=253.808;
simplification["iodine-127"]="_";
nature["iodine-127"]="non-natural";
id["iodine-127"]=807;

translate_modif["isopropyl-d-ornithine"]="≔";
official_name["isopropyl-d-ornithine"]="4-Isopropylpyridine";
weight["isopropyl-d-ornithine"]=121.180;
weight["≔"]=121.180;
simplification["isopropyl-d-ornithine"]="_";
nature["isopropyl-d-ornithine"]="non-natural";
id["isopropyl-d-ornithine"]=69674;

translate_modif["isovaline"]="≖";
official_name["isovaline"]="Isovaline";
weight["isovaline"]=117.150;
weight["≖"]=117.150;
simplification["isovaline"]="V";
nature["isovaline"]="non-natural";
id["isovaline"]=94744;

translate_modif["kynurenine"]="≗";
official_name["kynurenine"]="Kynurenine";
weight["kynurenine"]=208.210;
weight["≗"]=208.210;
simplification["kynurenine"]="_";
nature["kynurenine"]="non-natural";
id["kynurenine"]=846;

translate_modif["l-2-furylalanine"]="≘";
official_name["l-2-furylalanine"]="(S)-2-Amino-3-(furan-2-yl)propanoic_acid";
weight["l-2-furylalanine"]=155.150;
weight["≘"]=155.150;
simplification["l-2-furylalanine"]="A";
nature["l-2-furylalanine"]="non-natural";
id["l-2-furylalanine"]=2761503;

translate_modif["l-2-naphthylalanine"]="≙";
official_name["l-2-naphthylalanine"]="N-acetyl-2-naphthylalanine";
weight["l-2-naphthylalanine"]=257.279;
weight["≙"]=257.279;
simplification["l-2-naphthylalanine"]="A";
nature["l-2-naphthylalanine"]="non-natural";
id["l-2-naphthylalanine"]=54233443;

translate_modif["l-2-thienylalanine"]="≚";
official_name["l-2-thienylalanine"]="3-(2-Thienyl)-L-alanine";
weight["l-2-thienylalanine"]=171.220;
weight["≚"]=171.220;
simplification["l-2-thienylalanine"]="A";
nature["l-2-thienylalanine"]="non-natural";
id["l-2-thienylalanine"]=146719;

translate_modif["l-3-benzothienylalanine"]="≛";
official_name["l-3-benzothienylalanine"]="3-(3-Benzo(b)thienyl)alanine";
weight["l-3-benzothienylalanine"]=221.280;
weight["≛"]=221.280;
simplification["l-3-benzothienylalanine"]="A";
nature["l-3-benzothienylalanine"]="non-natural";
id["l-3-benzothienylalanine"]=150953;

translate_modif["l-3-pyridylalanine"]="≜";
official_name["l-3-pyridylalanine"]="3-(3-Pyridyl)-L-alanine";
weight["l-3-pyridylalanine"]=166.180;
weight["≜"]=166.180;
simplification["l-3-pyridylalanine"]="A";
nature["l-3-pyridylalanine"]="non-natural";
id["l-3-pyridylalanine"]=152953;

translate_modif["lacticacid"]="≝";
official_name["lacticacid"]="Lactivicin";
weight["lacticacid"]=272.210;
weight["≝"]=272.210;
simplification["lacticacid"]="_";
nature["lacticacid"]="non-natural";
id["lacticacid"]=9881937;

translate_modif["leucineboronicacid"]="≞";
official_name["leucineboronicacid"]="Phenylboronic_acid";
weight["leucineboronicacid"]=121.930;
weight["≞"]=121.930;
simplification["leucineboronicacid"]="L";
nature["leucineboronicacid"]="non-natural";
id["leucineboronicacid"]=66827;

translate_modif["leucinol"]="≟";
official_name["leucinol"]="L-Leucinol";
weight["leucinol"]=117.190;
weight["≟"]=117.190;
simplification["leucinol"]="L";
nature["leucinol"]="non-natural";
id["leucinol"]=111307;

translate_modif["l-homolysine"]="≸";
official_name["l-homolysine"]="Vasopressin,_9-homo-lys-";
weight["l-homolysine"]=1070.200;
weight["≸"]=1070.200;
simplification["l-homolysine"]="K";
nature["l-homolysine"]="non-natural";
id["l-homolysine"]=122361358;

translate_modif["l-homophenylalanine"]="Г";
official_name["l-homophenylalanine"]="L-Homophenylalanine";
weight["l-homophenylalanine"]=179.220;
weight["Г"]=179.220;
simplification["l-homophenylalanine"]="F";
nature["l-homophenylalanine"]="non-natural";
id["l-homophenylalanine"]=2724505;

translate_modif["l-homoserine"]="∹";
official_name["l-homoserine"]="L-homoserine";
weight["l-homoserine"]=119.120;
weight["∹"]=119.120;
simplification["l-homoserine"]="S";
nature["l-homoserine"]="non-natural";
id["l-homoserine"]=12647;

translate_modif["lysinenepsilon-trimethylated"]="≹";
official_name["lysinenepsilon-trimethylated"]="N-Trimethyllysine";
weight["lysinenepsilon-trimethylated"]=189.280;
weight["≹"]=189.280;
simplification["lysinenepsilon-trimethylated"]="K";
nature["lysinenepsilon-trimethylated"]="non-natural";
id["lysinenepsilon-trimethylated"]=440121;

translate_modif["lysinewithacetylatedsidegroup"]="≼";
official_name["lysinewithacetylatedsidegroup"]="N-epsilon-Acetyl-L-lysine";
weight["lysinewithacetylatedsidegroup"]=188.220;
weight["≼"]=188.220;
simplification["lysinewithacetylatedsidegroup"]="K";
nature["lysinewithacetylatedsidegroup"]="non-natural";
id["lysinewithacetylatedsidegroup"]=92832;

translate_modif["lysinewithbutyrylatedsidegroup"]="≽";
official_name["lysinewithbutyrylatedsidegroup"]="H-Lys(butyryl)-OH";
weight["lysinewithbutyrylatedsidegroup"]=216.280;
weight["≽"]=216.280;
simplification["lysinewithbutyrylatedsidegroup"]="K";
nature["lysinewithbutyrylatedsidegroup"]="non-natural";
id["lysinewithbutyrylatedsidegroup"]=49866729;

translate_modif["lysinewithdecanoylatedsidegroup"]="≾";
official_name["lysinewithdecanoylatedsidegroup"]="N2-(1-Oxodecyl)-L-lysine";
weight["lysinewithdecanoylatedsidegroup"]=300.440;
weight["≾"]=300.440;
simplification["lysinewithdecanoylatedsidegroup"]="K";
nature["lysinewithdecanoylatedsidegroup"]="non-natural";
id["lysinewithdecanoylatedsidegroup"]=18634292;

translate_modif["lysinewithhexanoylatedsidegroup"]="≿";
official_name["lysinewithhexanoylatedsidegroup"]="Hexanoyllysine";
weight["lysinewithhexanoylatedsidegroup"]=244.330;
weight["≿"]=244.330;
simplification["lysinewithhexanoylatedsidegroup"]="K";
nature["lysinewithhexanoylatedsidegroup"]="non-natural";
id["lysinewithhexanoylatedsidegroup"]=76128638;

translate_modif["lysinewithlaurylatedsidegroup"]="⊄";
official_name["lysinewithlaurylatedsidegroup"]="Lauryl_lysine";
weight["lysinewithlaurylatedsidegroup"]=314.500;
weight["⊄"]=314.500;
simplification["lysinewithlaurylatedsidegroup"]="K";
nature["lysinewithlaurylatedsidegroup"]="non-natural";
id["lysinewithlaurylatedsidegroup"]=22846330;

translate_modif["lysinewithmyristoylatedsidegroup"]="⊅";
official_name["lysinewithmyristoylatedsidegroup"]="Myristoyl_lysine";
weight["lysinewithmyristoylatedsidegroup"]=356.500;
weight["⊅"]=356.500;
simplification["lysinewithmyristoylatedsidegroup"]="K";
nature["lysinewithmyristoylatedsidegroup"]="non-natural";
id["lysinewithmyristoylatedsidegroup"]=18606615;

translate_modif["lysinewithoctanoylatedsidegroup"]="⊆";
official_name["lysinewithoctanoylatedsidegroup"]="N2-Capryloyl_lysine";
weight["lysinewithoctanoylatedsidegroup"]=272.380;
weight["⊆"]=272.380;
simplification["lysinewithoctanoylatedsidegroup"]="K";
nature["lysinewithoctanoylatedsidegroup"]="non-natural";
id["lysinewithoctanoylatedsidegroup"]=9835217;

translate_modif["lysinewitholeoylatedsidegroup"]="⊇";
official_name["lysinewitholeoylatedsidegroup"]="N6-Oleoyl-L-lysine";
weight["lysinewitholeoylatedsidegroup"]=410.600;
weight["⊇"]=410.600;
simplification["lysinewitholeoylatedsidegroup"]="K";
nature["lysinewitholeoylatedsidegroup"]="non-natural";
id["lysinewitholeoylatedsidegroup"]=6436547;

translate_modif["lysinewithpalmitoylatedsidegroup"]="⊈";
official_name["lysinewithpalmitoylatedsidegroup"]="n-Palmitoyl_lysine";
weight["lysinewithpalmitoylatedsidegroup"]=384.600;
weight["⊈"]=384.600;
simplification["lysinewithpalmitoylatedsidegroup"]="K";
nature["lysinewithpalmitoylatedsidegroup"]="non-natural";
id["lysinewithpalmitoylatedsidegroup"]=9821462;

translate_modif["methionineboronicacid"]="⊉";
official_name["methionineboronicacid"]="5-Bromo-4-methoxynicotinic_acid";
weight["methionineboronicacid"]=232.030;
weight["⊉"]=232.030;
simplification["methionineboronicacid"]="M";
nature["methionineboronicacid"]="non-natural";
id["methionineboronicacid"]=90203056;

translate_modif["methioninesulfone"]="⊊";
official_name["methioninesulfone"]="Busulfan";
weight["methioninesulfone"]=246.300;
weight["⊊"]=246.300;
simplification["methioninesulfone"]="M";
nature["methioninesulfone"]="non-natural";
id["methioninesulfone"]=2478;

translate_modif["methioninesulfoxide"]="⊋";
official_name["methioninesulfoxide"]="Methionine_sulfoxide";
weight["methioninesulfoxide"]=165.210;
weight["⊋"]=165.210;
simplification["methioninesulfoxide"]="M";
nature["methioninesulfoxide"]="non-natural";
id["methioninesulfoxide"]=847;

translate_modif["methyl"]="⊍";
official_name["methyl"]="Methyl_radical";
weight["methyl"]=15.035;
weight["⊍"]=15.035;
simplification["methyl"]="_";
nature["methyl"]="non-natural";
id["methyl"]=0.000;

translate_modif["methyl-glu"]="⊎";
official_name["methyl-glu"]="N-Methyl-L-glutamic_acid";
weight["methyl-glu"]=161.160;
weight["⊎"]=161.160;
simplification["methyl-glu"]="_";
nature["methyl-glu"]="non-natural";
id["methyl-glu"]=439377;

translate_modif["methyl-glutamicacid"]="⊎";
official_name["methyl-glutamicacid"]="N-Methyl-L-glutamic_acid";
weight["methyl-glutamicacid"]=161.160;
weight["⊎"]=161.160;
simplification["methyl-glutamicacid"]="Q";
nature["methyl-glutamicacid"]="non-natural";
id["methyl-glutamicacid"]=439377;

translate_modif["methylgroup"]="¶";
official_name["methylgroup"]="Methane";
weight["methylgroup"]=16.043;
weight["¶"]=16.043;
simplification["methylgroup"]="_";
nature["methylgroup"]="non-natural";
id["methylgroup"]=297;

translate_modif["methylphenylalanine"]="⊏";
official_name["methylphenylalanine"]="N-Methyl-L-phenylalanine";
weight["methylphenylalanine"]=179.220;
weight["⊏"]=179.220;
simplification["methylphenylalanine"]="F";
nature["methylphenylalanine"]="non-natural";
id["methylphenylalanine"]=6951135;

translate_modif["methyltryptophan"]="∂";
official_name["methyltryptophan"]="4-Methyltryptophan";
weight["methyltryptophan"]=218.250;
weight["∂"]=218.250;
simplification["methyltryptophan"]="W";
nature["methyltryptophan"]="non-natural";
id["methyltryptophan"]=150886;

translate_modif["morpholine-d-ornithine"]="⊐";
official_name["morpholine-d-ornithine"]="4-Morpholinoaniline";
weight["morpholine-d-ornithine"]=178.230;
weight["⊐"]=178.230;
simplification["morpholine-d-ornithine"]="_";
nature["morpholine-d-ornithine"]="non-natural";
id["morpholine-d-ornithine"]=75655;

translate_modif["myristoyl"]="⊒";
official_name["myristoyl"]="Myristic_Acid";
weight["myristoyl"]=228.370;
weight["⊒"]=228.370;
simplification["myristoyl"]="_";
nature["myristoyl"]="non-natural";
id["myristoyl"]=11005;

translate_modif["n2,n2-dimethyl-1,2-propanediamine"]="⊑";
official_name["n2,n2-dimethyl-1,2-propanediamine"]="1,2-Propanediamine,_N2,N2-dimethyl-";
weight["n2,n2-dimethyl-1,2-propanediamine"]=102.180;
weight["⊑"]=102.180;
simplification["n2,n2-dimethyl-1,2-propanediamine"]="_";
nature["n2,n2-dimethyl-1,2-propanediamine"]="non-natural";
id["n2,n2-dimethyl-1,2-propanediamine"]=89217;

translate_modif["n-3-guanidinoethylglycine"]="⊓";
official_name["n-3-guanidinoethylglycine"]="Guanidinoacetylglycine";
weight["n-3-guanidinoethylglycine"]=174.160;
weight["⊓"]=174.160;
simplification["n-3-guanidinoethylglycine"]="G";
nature["n-3-guanidinoethylglycine"]="non-natural";
id["n-3-guanidinoethylglycine"]=192768;

translate_modif["n-3-guanidinopropylglycine"]="⊔";
official_name["n-3-guanidinopropylglycine"]="Guanidinophenylglycine-2-naphthylamide";
weight["n-3-guanidinopropylglycine"]=333.400;
weight["⊔"]=333.400;
simplification["n-3-guanidinopropylglycine"]="G";
nature["n-3-guanidinopropylglycine"]="non-natural";
id["n-3-guanidinopropylglycine"]=195136;

translate_modif["n-4-aminobutyl-glycine"]="⊵";
official_name["n-4-aminobutyl-glycine"]="4-Aminobutylalanine";
weight["n-4-aminobutyl-glycine"]=255.110;
weight["⊵"]=255.110;
simplification["n-4-aminobutyl-glycine"]="G";
nature["n-4-aminobutyl-glycine"]="non-natural";
id["n-4-aminobutyl-glycine"]=5492389;

translate_modif["napthylalanine"]="Ը";
official_name["napthylalanine"]="1-Naphthyl-L-alanine";
weight["napthylalanine"]=215.250;
weight["Ը"]=215.250;
simplification["napthylalanine"]="A";
nature["napthylalanine"]="non-natural";
id["napthylalanine"]=2724883;

translate_modif["n-formyl-kynurenine"]="⊴";
official_name["n-formyl-kynurenine"]="N'-formylkynurenine";
weight["n-formyl-kynurenine"]=236.220;
weight["⊴"]=236.220;
simplification["n-formyl-kynurenine"]="_";
nature["n-formyl-kynurenine"]="non-natural";
id["n-formyl-kynurenine"]=910;

translate_modif["n-isopropylglycine"]="⊬";
official_name["n-isopropylglycine"]="2-(Isopropylamino)acetic_acid";
weight["n-isopropylglycine"]=117.150;
weight["⊬"]=117.150;
simplification["n-isopropylglycine"]="G";
nature["n-isopropylglycine"]="non-natural";
id["n-isopropylglycine"]=414244;

translate_modif["nitrile"]="⊥";
official_name["nitrile"]="Cyanide_ion";
weight["nitrile"]=26.017;
weight["⊥"]=26.017;
simplification["nitrile"]="_";
nature["nitrile"]="non-natural";
id["nitrile"]=5975;

translate_modif["nitroarginine"]="⊤";
official_name["nitroarginine"]="Nitroarginine";
weight["nitroarginine"]=219.200;
weight["⊤"]=219.200;
simplification["nitroarginine"]="R";
nature["nitroarginine"]="non-natural";
id["nitroarginine"]=440005;

translate_modif["n-methyl-4-methoxytryptophan"]="⊣";
official_name["n-methyl-4-methoxytryptophan"]="5-Methoxytryptophan";
weight["n-methyl-4-methoxytryptophan"]=234.250;
weight["⊣"]=234.250;
simplification["n-methyl-4-methoxytryptophan"]="W";
nature["n-methyl-4-methoxytryptophan"]="non-natural";
id["n-methyl-4-methoxytryptophan"]=151018;

translate_modif["n-methylalanine"]="⊢";
official_name["n-methylalanine"]="N-Methyl-L-alanine";
weight["n-methylalanine"]=103.120;
weight["⊢"]=103.120;
simplification["n-methylalanine"]="A";
nature["n-methylalanine"]="non-natural";
id["n-methylalanine"]=5288725;

translate_modif["n-methyl-allo-isoleucine"]="⊡";
official_name["n-methyl-allo-isoleucine"]="N-Methylisoleucine";
weight["n-methyl-allo-isoleucine"]=145.200;
weight["⊡"]=145.200;
simplification["n-methyl-allo-isoleucine"]="I";
nature["n-methyl-allo-isoleucine"]="non-natural";
id["n-methyl-allo-isoleucine"]=560437;

translate_modif["n-methylarginine"]="⊠";
official_name["n-methylarginine"]="Tilarginine";
weight["n-methylarginine"]=188.230;
weight["⊠"]=188.230;
simplification["n-methylarginine"]="R";
nature["n-methylarginine"]="non-natural";
id["n-methylarginine"]=132862;

translate_modif["n-methyl-asparagine"]="⊟";
official_name["n-methyl-asparagine"]="beta-Methyl-asparagine";
weight["n-methyl-asparagine"]=146.140;
weight["⊟"]=146.140;
simplification["n-methyl-asparagine"]="N";
nature["n-methyl-asparagine"]="non-natural";
id["n-methyl-asparagine"]=3016562;

translate_modif["n-methylationoftheamide"]="⊞";
official_name["n-methylationoftheamide"]="2-(Methylamino)benzamide";
weight["n-methylationoftheamide"]=150.180;
weight["⊞"]=150.180;
simplification["n-methylationoftheamide"]="_";
nature["n-methylationoftheamide"]="non-natural";
id["n-methylationoftheamide"]=101222;

translate_modif["n-methylcysteine"]="⊝";
official_name["n-methylcysteine"]="N-Methylcysteine";
weight["n-methylcysteine"]=135.190;
weight["⊝"]=135.190;
simplification["n-methylcysteine"]="C";
nature["n-methylcysteine"]="non-natural";
id["n-methylcysteine"]=452303;

translate_modif["n-methyl_glycine"]="⊸";
official_name["n-methyl_glycine"]="N,N-Dimethylglycine";
weight["n-methyl_glycine"]=103.120;
weight["⊸"]=103.120;
simplification["n-methyl_glycine"]="G";
nature["n-methyl_glycine"]="non-natural";
id["n-methyl_glycine"]=673;

translate_modif["n-methylisoleucine"]="⊡";
official_name["n-methylisoleucine"]="N-Methylisoleucine";
weight["n-methylisoleucine"]=145.200;
weight["⊡"]=145.200;
simplification["n-methylisoleucine"]="I";
nature["n-methylisoleucine"]="non-natural";
id["n-methylisoleucine"]=560437;

translate_modif["n-methylleucine"]="⋂";
official_name["n-methylleucine"]="N-Methylleucine";
weight["n-methylleucine"]=145.200;
weight["⋂"]=145.200;
simplification["n-methylleucine"]="L";
nature["n-methylleucine"]="non-natural";
id["n-methylleucine"]=567743;

translate_modif["n-methyl-lysine"]="⋃";
official_name["n-methyl-lysine"]="N(6)-Methyllysine";
weight["n-methyl-lysine"]=160.210;
weight["⋃"]=160.210;
simplification["n-methyl-lysine"]="K";
nature["n-methyl-lysine"]="non-natural";
id["n-methyl-lysine"]=164795;

translate_modif["n-methylserine"]="⋄";
official_name["n-methylserine"]="N-Methyl-L-serine";
weight["n-methylserine"]=119.120;
weight["⋄"]=119.120;
simplification["n-methylserine"]="S";
nature["n-methylserine"]="non-natural";
id["n-methylserine"]=7009640;

translate_modif["n-methyltyrosine"]="⋎";
official_name["n-methyltyrosine"]="N-Methyltyrosine";
weight["n-methyltyrosine"]=195.210;
weight["⋎"]=195.210;
simplification["n-methyltyrosine"]="Y";
nature["n-methyltyrosine"]="non-natural";
id["n-methyltyrosine"]=519723;

translate_modif["n-methylvaline"]="⋏";
official_name["n-methylvaline"]="N-Methyl-L-valine";
weight["n-methylvaline"]=131.170;
weight["⋏"]=131.170;
simplification["n-methylvaline"]="V";
nature["n-methylvaline"]="non-natural";
id["n-methylvaline"]=444080;

translate_modif["n-methyl_valine"]="⋐";
official_name["n-methyl_valine"]="2-Chloro-4-methylaniline";
weight["n-methyl_valine"]=141.600;
weight["⋐"]=141.600;
simplification["n-methyl_valine"]="V";
nature["n-methyl_valine"]="non-natural";
id["n-methyl_valine"]=12007;

translate_modif["n,n-dimethylglutaminefluoroketone"]="⋑";
official_name["n,n-dimethylglutaminefluoroketone"]="Dimethyl_glutamic_acid";
weight["n,n-dimethylglutaminefluoroketone"]=175.180;
weight["⋑"]=175.180;
simplification["n,n-dimethylglutaminefluoroketone"]="Q";
nature["n,n-dimethylglutaminefluoroketone"]="non-natural";
id["n,n-dimethylglutaminefluoroketone"]=12021025;

translate_modif["n,n-dimethylisoleucine"]="⋚";
official_name["n,n-dimethylisoleucine"]="n,n-Dimethylisoleucine";
weight["n,n-dimethylisoleucine"]=159.230;
weight["⋚"]=159.230;
simplification["n,n-dimethylisoleucine"]="I";
nature["n,n-dimethylisoleucine"]="non-natural";
id["n,n-dimethylisoleucine"]=11768849;

translate_modif["n,n-dimethylvaline"]="⋭";
official_name["n,n-dimethylvaline"]="2-(Dimethylamino)-3-methylbutanoic_acid";
weight["n,n-dimethylvaline"]=145.200;
weight["⋭"]=145.200;
simplification["n,n-dimethylvaline"]="V";
nature["n,n-dimethylvaline"]="non-natural";
id["n,n-dimethylvaline"]=14584527;

translate_modif["norleucinal"]="⋬";
official_name["norleucinal"]="Acetylleucyl-leucyl-norleucinal";
weight["norleucinal"]=383.500;
weight["⋬"]=383.500;
simplification["norleucinal"]="L";
nature["norleucinal"]="non-natural";
id["norleucinal"]=443118;

translate_modif["norleucine"]="∅";
official_name["norleucine"]="L-Norleucine";
weight["norleucine"]=131.170;
weight["∅"]=131.170;
simplification["norleucine"]="L";
nature["norleucine"]="non-natural";
id["norleucine"]=21236;

translate_modif["norleucinec6h13no2"]="∅";
official_name["norleucinec6h13no2"]="L-Norleucine";
weight["norleucinec6h13no2"]=131.170;
weight["∅"]=131.170;
simplification["norleucinec6h13no2"]="L";
nature["norleucinec6h13no2"]="non-natural";
id["norleucinec6h13no2"]=21236;

translate_modif["norleucinehyp"]="∅";
official_name["norleucinehyp"]="L-Norleucine";
weight["norleucinehyp"]=131.170;
weight["∅"]=131.170;
simplification["norleucinehyp"]="L";
nature["norleucinehyp"]="non-natural";
id["norleucinehyp"]=21236;

translate_modif["norvaline"]="⋫";
official_name["norvaline"]="Norvaline";
weight["norvaline"]=117.150;
weight["⋫"]=117.150;
simplification["norvaline"]="V";
nature["norvaline"]="non-natural";
id["norvaline"]=65098;

translate_modif["och3"]="⋪";
official_name["och3"]="Methoxy";
weight["och3"]=31.034;
weight["⋪"]=31.034;
simplification["och3"]="_";
nature["och3"]="non-natural";
id["och3"]=123146;

translate_modif["o-methyl-tyrosine"]="⋩";
official_name["o-methyl-tyrosine"]="alpha-Methyl-tyrosine";
weight["o-methyl-tyrosine"]=195.210;
weight["⋩"]=195.210;
simplification["o-methyl-tyrosine"]="Y";
nature["o-methyl-tyrosine"]="non-natural";
id["o-methyl-tyrosine"]=6918925;

translate_modif["orinithine"]="O";
official_name["orinithine"]="L-ornithine";
weight["orinithine"]=132.160;
weight["O"]=132.160;
simplification["orinithine"]="_";
nature["orinithine"]="non-natural";
id["orinithine"]=6262;

translate_modif["ornithine"]="O";
official_name["ornithine"]="L-ornithine";
weight["ornithine"]=132.160;
weight["O"]=132.160;
simplification["ornithine"]="_";
nature["ornithine"]="non-natural";
id["ornithine"]=6262;

translate_modif["oxomethionine"]="⋨";
official_name["oxomethionine"]="Fmoc-L-beta-homomethionine";
weight["oxomethionine"]=385.500;
weight["⋨"]=385.500;
simplification["oxomethionine"]="M";
nature["oxomethionine"]="non-natural";
id["oxomethionine"]=7010010;

translate_modif["paclitaxel"]="⋧";
official_name["paclitaxel"]="Paclitaxel";
weight["paclitaxel"]=853.900;
weight["⋧"]=853.900;
simplification["paclitaxel"]="_";
nature["paclitaxel"]="non-natural";
id["paclitaxel"]=36314;

translate_modif["palmitatepal"]="⋦";
official_name["palmitatepal"]="Methyl_palmitate";
weight["palmitatepal"]=270.500;
weight["⋦"]=270.500;
simplification["palmitatepal"]="_";
nature["palmitatepal"]="non-natural";
id["palmitatepal"]=8181;

translate_modif["pcmarg"]="⋥";
official_name["pcmarg"]="9-Phenylcarboxylate-10-methylacridinium";
weight["pcmarg"]=314.400;
weight["⋥"]=314.400;
simplification["pcmarg"]="_";
nature["pcmarg"]="non-natural";
id["pcmarg"]=195393;

translate_modif["peg4linker"]="⋤";
official_name["peg4linker"]="2-(4-(((((9H-Fluoren-9-yl)methoxy)carbonyl)amino)(2,4-dimethoxyphenyl)methyl)phenoxy)acetic_acid";
weight["peg4linker"]=539.600;
weight["⋤"]=539.600;
simplification["peg4linker"]="_";
nature["peg4linker"]="non-natural";
id["peg4linker"]=2761459;

translate_modif["pemetrexed"]="⋣";
official_name["pemetrexed"]="Pemetrexed";
weight["pemetrexed"]=427.400;
weight["⋣"]=427.400;
simplification["pemetrexed"]="_";
nature["pemetrexed"]="non-natural";
id["pemetrexed"]=135410875;

translate_modif["penicillamine"]="⋢";
official_name["penicillamine"]="Penicillamine";
weight["penicillamine"]=149.210;
weight["⋢"]=149.210;
simplification["penicillamine"]="_";
nature["penicillamine"]="non-natural";
id["penicillamine"]=5852;

translate_modif["pentafluorophenyl"]="⋡";
official_name["pentafluorophenyl"]="(Pentafluorophenyl)hydrazine";
weight["pentafluorophenyl"]=198.090;
weight["⋡"]=198.090;
simplification["pentafluorophenyl"]="_";
nature["pentafluorophenyl"]="non-natural";
id["pentafluorophenyl"]=13236;

translate_modif["phenylalaninol"]="⋠";
official_name["phenylalaninol"]="L-Phenylalaninol";
weight["phenylalaninol"]=151.210;
weight["⋠"]=151.210;
simplification["phenylalaninol"]="F";
nature["phenylalaninol"]="non-natural";
id["phenylalaninol"]=447213;

translate_modif["phenylglycine"]="⋟";
official_name["phenylglycine"]="D-4-Hydroxyphenylglycine";
weight["phenylglycine"]=167.160;
weight["⋟"]=167.160;
simplification["phenylglycine"]="G";
nature["phenylglycine"]="non-natural";
id["phenylglycine"]=89853;

translate_modif["phosphoserine"]="⋞";
official_name["phosphoserine"]="Phosphoserine";
weight["phosphoserine"]=185.070;
weight["⋞"]=185.070;
simplification["phosphoserine"]="S";
nature["phosphoserine"]="non-natural";
id["phosphoserine"]=68841;

translate_modif["phosphotyrosine"]="⋝";
official_name["phosphotyrosine"]="O-Phospho-L-tyrosine";
weight["phosphotyrosine"]=261.170;
weight["⋝"]=261.170;
simplification["phosphotyrosine"]="Y";
nature["phosphotyrosine"]="non-natural";
id["phosphotyrosine"]=30819;

translate_modif["pipecolicacid"]="⋜";
official_name["pipecolicacid"]="l-Pipecolic_acid";
weight["pipecolicacid"]=129.160;
weight["⋜"]=129.160;
simplification["pipecolicacid"]="_";
nature["pipecolicacid"]="non-natural";
id["pipecolicacid"]=439227;

translate_modif["piperazicacid"]="⋛";
official_name["piperazicacid"]="2,5-Piperazinedione";
weight["piperazicacid"]=114.100;
weight["⋛"]=114.100;
simplification["piperazicacid"]="_";
nature["piperazicacid"]="non-natural";
id["piperazicacid"]=7817;

translate_modif["propargylglycine"]="⌀";
official_name["propargylglycine"]="Acetic_acid,_2-(prop-2-ynylamino)-";
weight["propargylglycine"]=113.110;
weight["⌀"]=113.110;
simplification["propargylglycine"]="G";
nature["propargylglycine"]="non-natural";
id["propargylglycine"]=185909;

translate_modif["pyridylethylatedcysteine"]="⌁";
official_name["pyridylethylatedcysteine"]="2-(2-Aminoethyl)pyridine";
weight["pyridylethylatedcysteine"]=122.170;
weight["⌁"]=122.170;
simplification["pyridylethylatedcysteine"]="C";
nature["pyridylethylatedcysteine"]="non-natural";
id["pyridylethylatedcysteine"]=75919;

translate_modif["pyroglutamicacid"]="∌";
official_name["pyroglutamicacid"]="L-Pyroglutamic_acid";
weight["pyroglutamicacid"]=129.110;
weight["∌"]=129.110;
simplification["pyroglutamicacid"]="Q";
nature["pyroglutamicacid"]="non-natural";
id["pyroglutamicacid"]=7405;

translate_modif["pyroglutamic_acid"]="∌";
official_name["pyroglutamic_acid"]="L-Pyroglutamic_acid";
weight["pyroglutamic_acid"]=129.110;
weight["∌"]=129.110;
simplification["pyroglutamic_acid"]="Q";
nature["pyroglutamic_acid"]="non-natural";
id["pyroglutamic_acid"]=7405;

translate_modif["pyroglutamine"]="∌";
official_name["pyroglutamine"]="L-Pyroglutamic_acid";
weight["pyroglutamine"]=129.110;
weight["∌"]=129.110;
simplification["pyroglutamine"]="Q";
nature["pyroglutamine"]="non-natural";
id["pyroglutamine"]=7405;

translate_modif["s-2-4-pentenylalanine"]="⌂";
official_name["s-2-4-pentenylalanine"]="N-(3-Methylbut-2-EN-1-YL)-9H-purin-6-amine";
weight["s-2-4-pentenylalanine"]=203.240;
weight["⌂"]=203.240;
simplification["s-2-4-pentenylalanine"]="A";
nature["s-2-4-pentenylalanine"]="non-natural";
id["s-2-4-pentenylalanine"]=92180;

translate_modif["s-2-aminoundecanoicacid"]="⌅";
official_name["s-2-aminoundecanoicacid"]="11-Aminoundecanoic_acid";
weight["s-2-aminoundecanoicacid"]=201.310;
weight["⌅"]=201.310;
simplification["s-2-aminoundecanoicacid"]="_";
nature["s-2-aminoundecanoicacid"]="non-natural";
id["s-2-aminoundecanoicacid"]=17083;

translate_modif["s-acetamidomethylcysteine"]="⌆";
official_name["s-acetamidomethylcysteine"]="S-(Acetamidomethyl)-L-cysteine";
weight["s-acetamidomethylcysteine"]=192.240;
weight["⌆"]=192.240;
simplification["s-acetamidomethylcysteine"]="C";
nature["s-acetamidomethylcysteine"]="non-natural";
id["s-acetamidomethylcysteine"]=1590100;

translate_modif["sarcosine"]="⌑";
official_name["sarcosine"]="Sarcosine";
weight["sarcosine"]=89.090;
weight["⌑"]=89.090;
simplification["sarcosine"]="_";
nature["sarcosine"]="non-natural";
id["sarcosine"]=1088;

translate_modif["s-cysteinylated_cysteine"]="⌓";
official_name["s-cysteinylated_cysteine"]="Cysteinylcysteine";
weight["s-cysteinylated_cysteine"]=224.300;
weight["⌓"]=224.300;
simplification["s-cysteinylated_cysteine"]="C";
nature["s-cysteinylated_cysteine"]="non-natural";
id["s-cysteinylated_cysteine"]=152238;

translate_modif["selenocysteine"]="U";
official_name["selenocysteine"]="Selenocysteine";
weight["selenocysteine"]=167.060;
weight["U"]=167.060;
simplification["selenocysteine"]="C";
nature["selenocysteine"]="non-natural";
id["selenocysteine"]=6326983;

translate_modif["selenocystine_half"]="⌕";
official_name["selenocystine_half"]="L-Selenocystine";
weight["selenocystine_half"]=334.110;
weight["⌕"]=334.110;
simplification["selenocystine_half"]="_";
nature["selenocystine_half"]="non-natural";
id["selenocystine_half"]=207306;

translate_modif["s-methylthio_cysteine"]="⌘";
official_name["s-methylthio_cysteine"]="2-(Methylthio)pyridine";
weight["s-methylthio_cysteine"]=125.190;
weight["⌘"]=125.190;
simplification["s-methylthio_cysteine"]="C";
nature["s-methylthio_cysteine"]="non-natural";
id["s-methylthio_cysteine"]=29076;

translate_modif["statin"]="⌙";
official_name["statin"]="Statine";
weight["statin"]=175.230;
weight["⌙"]=175.230;
simplification["statin"]="_";
nature["statin"]="non-natural";
id["statin"]=123915;

translate_modif["succiniccacid=qn"]="⍃";
official_name["succiniccacid=qn"]="Succinimide";
weight["succiniccacid=qn"]=99.090;
weight["⍃"]=99.090;
simplification["succiniccacid=qn"]="_";
nature["succiniccacid=qn"]="non-natural";
id["succiniccacid=qn"]=11439;

translate_modif["succinyl"]="⍂";
official_name["succinyl"]="Succinic_Acid";
weight["succinyl"]=118.090;
weight["⍂"]=118.090;
simplification["succinyl"]="_";
nature["succinyl"]="non-natural";
id["succinyl"]=1110;

translate_modif["sulfotyrosine"]="⍁";
official_name["sulfotyrosine"]="tyrosine_O-sulfate";
weight["sulfotyrosine"]=261.250;
weight["⍁"]=261.250;
simplification["sulfotyrosine"]="Y";
nature["sulfotyrosine"]="non-natural";
id["sulfotyrosine"]=514186;

translate_modif["symmetricdimethylarginine"]="Ӗ";
official_name["symmetricdimethylarginine"]="N,N-Dimethylarginine";
weight["symmetricdimethylarginine"]=202.250;
weight["Ӗ"]=202.250;
simplification["symmetricdimethylarginine"]="R";
nature["symmetricdimethylarginine"]="non-natural";
id["symmetricdimethylarginine"]=123831;

translate_modif["t-butylcarbamate"]="⍀";
official_name["t-butylcarbamate"]="(3-Triethoxysilylpropyl)-T-butylcarbamate";
weight["t-butylcarbamate"]=321.480;
weight["⍀"]=321.480;
simplification["t-butylcarbamate"]="_";
nature["t-butylcarbamate"]="non-natural";
id["t-butylcarbamate"]=22283777;

translate_modif["tert-butylalanine"]="⌿";
official_name["tert-butylalanine"]="4-Methyl-D-leucine";
weight["tert-butylalanine"]=145.200;
weight["⌿"]=145.200;
simplification["tert-butylalanine"]="A";
nature["tert-butylalanine"]="non-natural";
id["tert-butylalanine"]=6950508;

translate_modif["tert-butylglycine"]="⌾";
official_name["tert-butylglycine"]="DL-tert-Leucine";
weight["tert-butylglycine"]=131.170;
weight["⌾"]=131.170;
simplification["tert-butylglycine"]="G";
nature["tert-butylglycine"]="non-natural";
id["tert-butylglycine"]=306131;

translate_modif["tert-butyl_glycine"]="⌽";
official_name["tert-butyl_glycine"]="1,2,3,5-Tetrahydroxybenzene";
weight["tert-butyl_glycine"]=142.110;
weight["⌽"]=142.110;
simplification["tert-butyl_glycine"]="G";
nature["tert-butyl_glycine"]="non-natural";
id["tert-butyl_glycine"]=12;

translate_modif["tert-butyloxycarbonyl"]="⌼";
official_name["tert-butyloxycarbonyl"]="Tert-butyl_formate";
weight["tert-butyloxycarbonyl"]=102.130;
weight["⌼"]=102.130;
simplification["tert-butyloxycarbonyl"]="_";
nature["tert-butyloxycarbonyl"]="non-natural";
id["tert-butyloxycarbonyl"]=61207;

translate_modif["tert-leucine"]="⌾";
official_name["tert-leucine"]="DL-tert-Leucine";
weight["tert-leucine"]=131.170;
weight["⌾"]=131.170;
simplification["tert-leucine"]="L";
nature["tert-leucine"]="non-natural";
id["tert-leucine"]=306131;

translate_modif["thiazoline"]="⌻";
official_name["thiazoline"]="Thiazoline";
weight["thiazoline"]=87.150;
weight["⌻"]=87.150;
simplification["thiazoline"]="_";
nature["thiazoline"]="non-natural";
id["thiazoline"]=50.900;

translate_modif["trifloromethyl"]="⌺";
official_name["trifloromethyl"]="Trifluoromethyl_radical";
weight["trifloromethyl"]=69.006;
weight["⌺"]=69.006;
simplification["trifloromethyl"]="_";
nature["trifloromethyl"]="non-natural";
id["trifloromethyl"]=137518;

translate_modif["trifluoromethyl-bicyclopent-1.1.1-1-ylglycine"]="Њ";
official_name["trifluoromethyl-bicyclopent-1.1.1-1-ylglycine"]="6-Bromo-2-(trifluoromethyl)quinoline";
weight["trifluoromethyl-bicyclopent-1.1.1-1-ylglycine"]=276.050;
weight["Њ"]=276.050;
simplification["trifluoromethyl-bicyclopent-1.1.1-1-ylglycine"]="G";
nature["trifluoromethyl-bicyclopent-1.1.1-1-ylglycine"]="non-natural";
id["trifluoromethyl-bicyclopent-1.1.1-1-ylglycine"]=15325795;

translate_modif["trimethyl-d-ornithine"]="⌹";
official_name["trimethyl-d-ornithine"]="Caffeine";
weight["trimethyl-d-ornithine"]=194.190;
weight["⌹"]=194.190;
simplification["trimethyl-d-ornithine"]="_";
nature["trimethyl-d-ornithine"]="non-natural";
id["trimethyl-d-ornithine"]=2519;

translate_modif["tryptophanol"]="⌸";
official_name["tryptophanol"]="Tryptophanol";
weight["tryptophanol"]=190.240;
weight["⌸"]=190.240;
simplification["tryptophanol"]="W";
nature["tryptophanol"]="non-natural";
id["tryptophanol"]=6951149;

translate_modif["z-2,3-diaminoacrylicacid"]="⌷";
official_name["z-2,3-diaminoacrylicacid"]="3,3-Diaminoacrylic_acid";
weight["z-2,3-diaminoacrylicacid"]=102.090;
weight["⌷"]=102.090;
simplification["z-2,3-diaminoacrylicacid"]="_";
nature["z-2,3-diaminoacrylicacid"]="non-natural";
id["z-2,3-diaminoacrylicacid"]=20463843;

translate_modif["α-aminoisobutyricacid"]="Է";
official_name["α-aminoisobutyricacid"]="2-Aminoisobutyric_acid";
weight["α-aminoisobutyricacid"]=103.120;
weight["Է"]=103.120;
simplification["α-aminoisobutyricacid"]="_";
nature["α-aminoisobutyricacid"]="non-natural";
id["α-aminoisobutyricacid"]=6119;

translate_modif["α-aminoisobutyricacidaib"]="Է";
official_name["α-aminoisobutyricacidaib"]="2-Aminoisobutyric_acid";
weight["α-aminoisobutyricacidaib"]=103.120;
weight["Է"]=103.120;
simplification["α-aminoisobutyricacidaib"]="_";
nature["α-aminoisobutyricacidaib"]="non-natural";
id["α-aminoisobutyricacidaib"]=6119;

translate_modif["α-amino-n-butyricacid"]="Մ";
official_name["α-amino-n-butyricacid"]="Gamma-Aminobutyric_Acid";
weight["α-amino-n-butyricacid"]=103.120;
weight["Մ"]=103.120;
simplification["α-amino-n-butyricacid"]="_";
nature["α-amino-n-butyricacid"]="non-natural";
id["α-amino-n-butyricacid"]=119;

translate_modif["α-galnac"]="⌶";
official_name["α-galnac"]="N-Acetyl-D-Galactosamine";
weight["α-galnac"]=221.210;
weight["⌶"]=221.210;
simplification["α-galnac"]="_";
nature["α-galnac"]="non-natural";
id["α-galnac"]=35717;

translate_modif["α-hydroxyisovalericacid"]="⌫";
official_name["α-hydroxyisovalericacid"]="beta-Hydroxyisovaleric_acid";
weight["α-hydroxyisovalericacid"]=118.130;
weight["⌫"]=118.130;
simplification["α-hydroxyisovalericacid"]="V";
nature["α-hydroxyisovalericacid"]="non-natural";
id["α-hydroxyisovalericacid"]=69362;

translate_modif["α-methylalanine"]="Է";
official_name["α-methylalanine"]="2-Aminoisobutyric_acid";
weight["α-methylalanine"]=103.120;
weight["Է"]=103.120;
simplification["α-methylalanine"]="A";
nature["α-methylalanine"]="non-natural";
id["α-methylalanine"]=6119;

translate_modif["β-asparticacid"]="Ө";
official_name["β-asparticacid"]="L-Aspartic_acid-15N";
weight["β-asparticacid"]=134.100;
weight["Ө"]=134.100;
simplification["β-asparticacid"]="D";
nature["β-asparticacid"]="non-natural";
id["β-asparticacid"]=16212100;

translate_modif["2-1-oxo-propylpyrrolidine"]="⌨";
official_name["2-1-oxo-propylpyrrolidine"]="3-Cyclopropylpyrrolidine";
weight["2-1-oxo-propylpyrrolidine"]=111.180;
weight["⌨"]=111.180;
simplification["2-1-oxo-propylpyrrolidine"]="_";
nature["2-1-oxo-propylpyrrolidine"]="non-natural";
id["2-1-oxo-propylpyrrolidine"]=53715280;

translate_modif["2,6-difluoro-phenylalanine"]="⌧";
official_name["2,6-difluoro-phenylalanine"]="2-amino-3-(3,4-difluorophenyl)propanoic_Acid";
weight["2,6-difluoro-phenylalanine"]=201.170;
weight["⌧"]=201.170;
simplification["2,6-difluoro-phenylalanine"]="F";
nature["2,6-difluoro-phenylalanine"]="non-natural";
id["2,6-difluoro-phenylalanine"]=2737044;

translate_modif["2-aminodecanoicacid"]="Ա";
official_name["2-aminodecanoicacid"]="10-Aminodecanoic_acid";
weight["2-aminodecanoicacid"]=187.280;
weight["Ա"]=187.280;
simplification["2-aminodecanoicacid"]="_";
nature["2-aminodecanoicacid"]="non-natural";
id["2-aminodecanoicacid"]=83150;

translate_modif["2-aminooctanedioicacid"]="⌦";
official_name["2-aminooctanedioicacid"]="2-Aminooctanedioic_acid";
weight["2-aminooctanedioicacid"]=189.210;
weight["⌦"]=189.210;
simplification["2-aminooctanedioicacid"]="_";
nature["2-aminooctanedioicacid"]="non-natural";
id["2-aminooctanedioicacid"]=77937;

translate_modif["2-fluoro-phenylalanine"]="Ж";
official_name["2-fluoro-phenylalanine"]="4-Fluorophenylalanine";
weight["2-fluoro-phenylalanine"]=183.180;
weight["Ж"]=183.180;
simplification["2-fluoro-phenylalanine"]="F";
nature["2-fluoro-phenylalanine"]="non-natural";
id["2-fluoro-phenylalanine"]=4654;

translate_modif["2-fluoro-tyrosine"]="⌥";
official_name["2-fluoro-tyrosine"]="N-Fmoc-3-fluoro-L-tyrosine";
weight["2-fluoro-tyrosine"]=421.400;
weight["⌥"]=421.400;
simplification["2-fluoro-tyrosine"]="Y";
nature["2-fluoro-tyrosine"]="non-natural";
id["2-fluoro-tyrosine"]=89668487;

translate_modif["4,4-difluoroproline"]="⍄";
official_name["4,4-difluoroproline"]="4,4-Difluoroproline";
weight["4,4-difluoroproline"]=151.110;
weight["⍄"]=151.110;
simplification["4,4-difluoroproline"]="P";
nature["4,4-difluoroproline"]="non-natural";
id["4,4-difluoroproline"]=15216102;

translate_modif["4-bromino-phenylalanine"]="⍅";
official_name["4-bromino-phenylalanine"]="2-Bromophenylalanine";
weight["4-bromino-phenylalanine"]=244.080;
weight["⍅"]=244.080;
simplification["4-bromino-phenylalanine"]="F";
nature["4-bromino-phenylalanine"]="non-natural";
id["4-bromino-phenylalanine"]=193338;

translate_modif["4-chloro-phenylacetyl"]="⍆";
official_name["4-chloro-phenylacetyl"]="4-Chlorophenylacetyl_chloride";
weight["4-chloro-phenylacetyl"]=189.040;
weight["⍆"]=189.040;
simplification["4-chloro-phenylacetyl"]="_";
nature["4-chloro-phenylacetyl"]="non-natural";
id["4-chloro-phenylacetyl"]=90692;

translate_modif["4-cyclohexyl-proline"]="⍇";
official_name["4-cyclohexyl-proline"]="N-(3-Aminopropyl)cyclohexylamine";
weight["4-cyclohexyl-proline"]=156.270;
weight["⍇"]=156.270;
simplification["4-cyclohexyl-proline"]="P";
nature["4-cyclohexyl-proline"]="non-natural";
id["4-cyclohexyl-proline"]=18718;

translate_modif["4-fluoro-phenylalanine"]="Ж";
official_name["4-fluoro-phenylalanine"]="4-Fluorophenylalanine";
weight["4-fluoro-phenylalanine"]=183.180;
weight["Ж"]=183.180;
simplification["4-fluoro-phenylalanine"]="F";
nature["4-fluoro-phenylalanine"]="non-natural";
id["4-fluoro-phenylalanine"]=4654;

translate_modif["4-fluoroproline"]="⍈";
official_name["4-fluoroproline"]="1-Bromo-3-fluoropropane";
weight["4-fluoroproline"]=140.980;
weight["⍈"]=140.980;
simplification["4-fluoroproline"]="P";
nature["4-fluoroproline"]="non-natural";
id["4-fluoroproline"]=67699;

translate_modif["4-hydroxybenzyl_proline"]="⍉";
official_name["4-hydroxybenzyl_proline"]="4-Hydroxybenzylamine";
weight["4-hydroxybenzyl_proline"]=123.150;
weight["⍉"]=123.150;
simplification["4-hydroxybenzyl_proline"]="P";
nature["4-hydroxybenzyl_proline"]="non-natural";
id["4-hydroxybenzyl_proline"]=97472;

translate_modif["4-imidazoleacrylicacid"]="⍊";
official_name["4-imidazoleacrylicacid"]="Urocanic_acid";
weight["4-imidazoleacrylicacid"]=138.120;
weight["⍊"]=138.120;
simplification["4-imidazoleacrylicacid"]="_";
nature["4-imidazoleacrylicacid"]="non-natural";
id["4-imidazoleacrylicacid"]=736715;

translate_modif["4-iodino-phenylalanine"]="⍋";
official_name["4-iodino-phenylalanine"]="4-Iodo-L-phenylalanine";
weight["4-iodino-phenylalanine"]=291.090;
weight["⍋"]=291.090;
simplification["4-iodino-phenylalanine"]="F";
nature["4-iodino-phenylalanine"]="non-natural";
id["4-iodino-phenylalanine"]=134497;

translate_modif["4-mercaptoproline_reduced"]="⍌";
official_name["4-mercaptoproline_reduced"]="Benzyl_3-mercaptopyrrolidine-1-carboxylate";
weight["4-mercaptoproline_reduced"]=237.320;
weight["⍌"]=237.320;
simplification["4-mercaptoproline_reduced"]="P";
nature["4-mercaptoproline_reduced"]="non-natural";
id["4-mercaptoproline_reduced"]=13324656;

translate_modif["4-methyloxide-benzyl_proline"]="⍍";
official_name["4-methyloxide-benzyl_proline"]="N-Benzyloxycarbonyl-L-proline";
weight["4-methyloxide-benzyl_proline"]=249.260;
weight["⍍"]=249.260;
simplification["4-methyloxide-benzyl_proline"]="P";
nature["4-methyloxide-benzyl_proline"]="non-natural";
id["4-methyloxide-benzyl_proline"]=101987;

translate_modif["4-methylthio-proline"]="⌘";
official_name["4-methylthio-proline"]="2-(Methylthio)pyridine";
weight["4-methylthio-proline"]=125.190;
weight["⌘"]=125.190;
simplification["4-methylthio-proline"]="P";
nature["4-methylthio-proline"]="non-natural";
id["4-methylthio-proline"]=29076;

translate_modif["4-nitrous-benzyl_proline"]="⍎";
official_name["4-nitrous-benzyl_proline"]="Benzylphenylnitrosamine";
weight["4-nitrous-benzyl_proline"]=212.250;
weight["⍎"]=212.250;
simplification["4-nitrous-benzyl_proline"]="P";
nature["4-nitrous-benzyl_proline"]="non-natural";
id["4-nitrous-benzyl_proline"]=11935;

translate_modif["4-phenoxy-proline"]="⍏";
official_name["4-phenoxy-proline"]="3-Phenoxy-1,2-propanediol";
weight["4-phenoxy-proline"]=168.190;
weight["⍏"]=168.190;
simplification["4-phenoxy-proline"]="P";
nature["4-phenoxy-proline"]="non-natural";
id["4-phenoxy-proline"]=10857;

translate_modif["5,5,5,5"]="⍐";
official_name["5,5,5,5"]="Allopregnanolone";
weight["5,5,5,5"]=318.500;
weight["⍐"]=318.500;
simplification["5,5,5,5"]="_";
nature["5,5,5,5"]="non-natural";
id["5,5,5,5"]=92786;

translate_modif["5-hydroxyanthranilicacid"]="⍑";
official_name["5-hydroxyanthranilicacid"]="3-Hydroxyanthranilic_Acid";
weight["5-hydroxyanthranilicacid"]=153.140;
weight["⍑"]=153.140;
simplification["5-hydroxyanthranilicacid"]="_";
nature["5-hydroxyanthranilicacid"]="non-natural";
id["5-hydroxyanthranilicacid"]=86;

translate_modif["7-dimethylallyltryptophan"]="⍒";
official_name["7-dimethylallyltryptophan"]="N-Methyl-4-dimethylallyltryptophan";
weight["7-dimethylallyltryptophan"]=286.370;
weight["⍒"]=286.370;
simplification["7-dimethylallyltryptophan"]="W";
nature["7-dimethylallyltryptophan"]="non-natural";
id["7-dimethylallyltryptophan"]=68294649;

translate_modif["allo-threonine"]="⍓";
official_name["allo-threonine"]="L-Allothreonine";
weight["allo-threonine"]=119.120;
weight["⍓"]=119.120;
simplification["allo-threonine"]="T";
nature["allo-threonine"]="non-natural";
id["allo-threonine"]=99289;

translate_modif["bishomopropargylglycine"]="⍔";
official_name["bishomopropargylglycine"]="Fmoc-D-bishomopropargylglycine";
weight["bishomopropargylglycine"]=363.400;
weight["⍔"]=363.400;
simplification["bishomopropargylglycine"]="G";
nature["bishomopropargylglycine"]="non-natural";
id["bishomopropargylglycine"]=87539004;

translate_modif["c4h9no2s2"]="⍕";
official_name["c4h9no2s2"]="Tert-Butylamine";
weight["c4h9no2s2"]=73.140;
weight["⍕"]=73.140;
simplification["c4h9no2s2"]="_";
nature["c4h9no2s2"]="non-natural";
id["c4h9no2s2"]=6385;

translate_modif["capreomycidine"]="⍖";
official_name["capreomycidine"]="Capreomycin";
weight["capreomycidine"]=1321.400;
weight["⍖"]=1321.400;
simplification["capreomycidine"]="_";
nature["capreomycidine"]="non-natural";
id["capreomycidine"]=3000502;

translate_modif["carbobenzyloxy-l-lysine"]="⍗";
official_name["carbobenzyloxy-l-lysine"]="N-Benzyloxycarbonylglycine";
weight["carbobenzyloxy-l-lysine"]=209.200;
weight["⍗"]=209.200;
simplification["carbobenzyloxy-l-lysine"]="K";
nature["carbobenzyloxy-l-lysine"]="non-natural";
id["carbobenzyloxy-l-lysine"]=14349;

translate_modif["carboxyl-phenylalanine"]="⍙";
official_name["carboxyl-phenylalanine"]="L-4-Carbamoylphenylalanine";
weight["carboxyl-phenylalanine"]=208.210;
weight["⍙"]=208.210;
simplification["carboxyl-phenylalanine"]="F";
nature["carboxyl-phenylalanine"]="non-natural";
id["carboxyl-phenylalanine"]=2762272;

translate_modif["d-4-fluoro-phenylalanine"]="Ж";
official_name["d-4-fluoro-phenylalanine"]="4-Fluorophenylalanine";
weight["d-4-fluoro-phenylalanine"]=183.180;
weight["Ж"]=183.180;
simplification["d-4-fluoro-phenylalanine"]="F";
nature["d-4-fluoro-phenylalanine"]="non-natural";
id["d-4-fluoro-phenylalanine"]=4654;

translate_modif["d-aspartic_acid"]="⍚";
official_name["d-aspartic_acid"]="Aspartic_Acid";
weight["d-aspartic_acid"]=133.100;
weight["⍚"]=133.100;
simplification["d-aspartic_acid"]="D";
nature["d-aspartic_acid"]="non-natural";
id["d-aspartic_acid"]=5960;

translate_modif["diaminobutyratewithbutyrylatedsidegroup"]="€";
official_name["diaminobutyratewithbutyrylatedsidegroup"]="L-2,4-diaminobutyric_acid";
weight["diaminobutyratewithbutyrylatedsidegroup"]=118.130;
weight["€"]=118.130;
simplification["diaminobutyratewithbutyrylatedsidegroup"]="_";
nature["diaminobutyratewithbutyrylatedsidegroup"]="non-natural";
id["diaminobutyratewithbutyrylatedsidegroup"]=134490;

translate_modif["diaminobutyratewithdecanoylatedsidegroup"]="€";
official_name["diaminobutyratewithdecanoylatedsidegroup"]="L-2,4-diaminobutyric_acid";
weight["diaminobutyratewithdecanoylatedsidegroup"]=118.130;
weight["€"]=118.130;
simplification["diaminobutyratewithdecanoylatedsidegroup"]="_";
nature["diaminobutyratewithdecanoylatedsidegroup"]="non-natural";
id["diaminobutyratewithdecanoylatedsidegroup"]=134490;

translate_modif["diaminobutyratewithhexanoylatedsidegroup"]="€";
official_name["diaminobutyratewithhexanoylatedsidegroup"]="L-2,4-diaminobutyric_acid";
weight["diaminobutyratewithhexanoylatedsidegroup"]=118.130;
weight["€"]=118.130;
simplification["diaminobutyratewithhexanoylatedsidegroup"]="_";
nature["diaminobutyratewithhexanoylatedsidegroup"]="non-natural";
id["diaminobutyratewithhexanoylatedsidegroup"]=134490;

translate_modif["diaminobutyratewithlaurylatedsidegroup"]="€";
official_name["diaminobutyratewithlaurylatedsidegroup"]="L-2,4-diaminobutyric_acid";
weight["diaminobutyratewithlaurylatedsidegroup"]=118.130;
weight["€"]=118.130;
simplification["diaminobutyratewithlaurylatedsidegroup"]="_";
nature["diaminobutyratewithlaurylatedsidegroup"]="non-natural";
id["diaminobutyratewithlaurylatedsidegroup"]=134490;

translate_modif["diaminobutyratewithmyristoylatedsidegroup"]="€";
official_name["diaminobutyratewithmyristoylatedsidegroup"]="L-2,4-diaminobutyric_acid";
weight["diaminobutyratewithmyristoylatedsidegroup"]=118.130;
weight["€"]=118.130;
simplification["diaminobutyratewithmyristoylatedsidegroup"]="_";
nature["diaminobutyratewithmyristoylatedsidegroup"]="non-natural";
id["diaminobutyratewithmyristoylatedsidegroup"]=134490;

translate_modif["diaminobutyratewithpalmitoylatedsidegroup"]="€";
official_name["diaminobutyratewithpalmitoylatedsidegroup"]="L-2,4-diaminobutyric_acid";
weight["diaminobutyratewithpalmitoylatedsidegroup"]=118.130;
weight["€"]=118.130;
simplification["diaminobutyratewithpalmitoylatedsidegroup"]="_";
nature["diaminobutyratewithpalmitoylatedsidegroup"]="non-natural";
id["diaminobutyratewithpalmitoylatedsidegroup"]=134490;

translate_modif["diaminopropionicacidwithdecanoylatedsidegroup"]="Խ";
official_name["diaminopropionicacidwithdecanoylatedsidegroup"]="2,3-Diaminopropionic_acid";
weight["diaminopropionicacidwithdecanoylatedsidegroup"]=104.110;
weight["Խ"]=104.110;
simplification["diaminopropionicacidwithdecanoylatedsidegroup"]="_";
nature["diaminopropionicacidwithdecanoylatedsidegroup"]="non-natural";
id["diaminopropionicacidwithdecanoylatedsidegroup"]=364;

translate_modif["diaminopropionicacidwithmyristoylatedsidegroup"]="Խ";
official_name["diaminopropionicacidwithmyristoylatedsidegroup"]="2,3-Diaminopropionic_acid";
weight["diaminopropionicacidwithmyristoylatedsidegroup"]=104.110;
weight["Խ"]=104.110;
simplification["diaminopropionicacidwithmyristoylatedsidegroup"]="_";
nature["diaminopropionicacidwithmyristoylatedsidegroup"]="non-natural";
id["diaminopropionicacidwithmyristoylatedsidegroup"]=364;

translate_modif["diaminopropionicacidwithpalmitoylatedsidegroup"]="Խ";
official_name["diaminopropionicacidwithpalmitoylatedsidegroup"]="2,3-Diaminopropionic_acid";
weight["diaminopropionicacidwithpalmitoylatedsidegroup"]=104.110;
weight["Խ"]=104.110;
simplification["diaminopropionicacidwithpalmitoylatedsidegroup"]="_";
nature["diaminopropionicacidwithpalmitoylatedsidegroup"]="non-natural";
id["diaminopropionicacidwithpalmitoylatedsidegroup"]=364;

translate_modif["dipropylglycine"]="⍛";
official_name["dipropylglycine"]="Dipropylglycine";
weight["dipropylglycine"]=159.230;
weight["⍛"]=159.230;
simplification["dipropylglycine"]="G";
nature["dipropylglycine"]="non-natural";
id["dipropylglycine"]=97817;

translate_modif["d-octyl-glycine"]="⍜";
official_name["d-octyl-glycine"]="N-Octylglycine";
weight["d-octyl-glycine"]=187.280;
weight["⍜"]=187.280;
simplification["d-octyl-glycine"]="G";
nature["d-octyl-glycine"]="non-natural";
id["d-octyl-glycine"]=350339;

translate_modif["d-proline"]="⍝";
official_name["d-proline"]="Alpha-Tocopherol";
weight["d-proline"]=430.700;
weight["⍝"]=430.700;
simplification["d-proline"]="P";
nature["d-proline"]="non-natural";
id["d-proline"]=14985;

translate_modif["d-serine"]="s";
official_name["d-serine"]="D-serine";
weight["d-serine"]=105.090;
weight["s"]=105.090;
simplification["d-serine"]="S";
nature["d-serine"]="non-natural";
id["d-serine"]=71077;

translate_modif["epsilonlysine"]="⍟";
official_name["epsilonlysine"]="Psicoselysine";
weight["epsilonlysine"]=308.330;
weight["⍟"]=308.330;
simplification["epsilonlysine"]="K";
nature["epsilonlysine"]="non-natural";
id["epsilonlysine"]=49859645;

translate_modif["iodoacetamide-cysteine"]="⍠";
official_name["iodoacetamide-cysteine"]="Iodoacetamidoeosin";
weight["iodoacetamide-cysteine"]=830.800;
weight["⍠"]=830.800;
simplification["iodoacetamide-cysteine"]="C";
nature["iodoacetamide-cysteine"]="non-natural";
id["iodoacetamide-cysteine"]=194316;

translate_modif["isostatin"]="⍡";
official_name["isostatin"]="Isostatin";
weight["isostatin"]=175.230;
weight["⍡"]=175.230;
simplification["isostatin"]="_";
nature["isostatin"]="non-natural";
id["isostatin"]=3087448;

translate_modif["mercaptoethylglycine_oxidised"]="⍢";
official_name["mercaptoethylglycine_oxidised"]="2-Mercaptoethylguanidine";
weight["mercaptoethylglycine_oxidised"]=119.190;
weight["⍢"]=119.190;
simplification["mercaptoethylglycine_oxidised"]="G";
nature["mercaptoethylglycine_oxidised"]="non-natural";
id["mercaptoethylglycine_oxidised"]=4070;

translate_modif["methanimine"]="⍣";
official_name["methanimine"]="Methanimine";
weight["methanimine"]=29.041;
weight["⍣"]=29.041;
simplification["methanimine"]="_";
nature["methanimine"]="non-natural";
id["methanimine"]=2.000;

translate_modif["methoxypolyethyleneglycolmaleimidecysteine"]="⍤";
official_name["methoxypolyethyleneglycolmaleimidecysteine"]="Methoxytriethyleneoxypropyltrimethoxysilane";
weight["methoxypolyethyleneglycolmaleimidecysteine"]=326.460;
weight["⍤"]=326.460;
simplification["methoxypolyethyleneglycolmaleimidecysteine"]="G";
nature["methoxypolyethyleneglycolmaleimidecysteine"]="non-natural";
id["methoxypolyethyleneglycolmaleimidecysteine"]=14746502;

translate_modif["mimosine"]="⍥";
official_name["mimosine"]="Mimosine";
weight["mimosine"]=198.180;
weight["⍥"]=198.180;
simplification["mimosine"]="_";
nature["mimosine"]="non-natural";
id["mimosine"]=440473;

translate_modif["n-1-naphthylmethylglycine"]="⍦";
official_name["n-1-naphthylmethylglycine"]="1-Naphthalenemethylamine";
weight["n-1-naphthylmethylglycine"]=157.210;
weight["⍦"]=157.210;
simplification["n-1-naphthylmethylglycine"]="G";
nature["n-1-naphthylmethylglycine"]="non-natural";
id["n-1-naphthylmethylglycine"]=8355;

translate_modif["n-2,2-diphenylethyl-glycine"]="⎉";
official_name["n-2,2-diphenylethyl-glycine"]="N-Diphenylacetylglycine";
weight["n-2,2-diphenylethyl-glycine"]=269.290;
weight["⎉"]=269.290;
simplification["n-2,2-diphenylethyl-glycine"]="G";
nature["n-2,2-diphenylethyl-glycine"]="non-natural";
id["n-2,2-diphenylethyl-glycine"]=101383;

translate_modif["n-2-aminoethyl-glycine"]="⎈";
official_name["n-2-aminoethyl-glycine"]="N-(2-Aminoethyl)glycine";
weight["n-2-aminoethyl-glycine"]=118.130;
weight["⎈"]=118.130;
simplification["n-2-aminoethyl-glycine"]="G";
nature["n-2-aminoethyl-glycine"]="non-natural";
id["n-2-aminoethyl-glycine"]=428913;

translate_modif["n-2-indolethyl-glycine"]="⎃";
official_name["n-2-indolethyl-glycine"]="D,L-3-Indolylglycine";
weight["n-2-indolethyl-glycine"]=190.200;
weight["⎃"]=190.200;
simplification["n-2-indolethyl-glycine"]="G";
nature["n-2-indolethyl-glycine"]="non-natural";
id["n-2-indolethyl-glycine"]=438674;

translate_modif["n-4-methylbenzylglycine"]="⎂";
official_name["n-4-methylbenzylglycine"]="4-Methylbenzylamine";
weight["n-4-methylbenzylglycine"]=121.180;
weight["⎂"]=121.180;
simplification["n-4-methylbenzylglycine"]="G";
nature["n-4-methylbenzylglycine"]="non-natural";
id["n-4-methylbenzylglycine"]=66035;

translate_modif["n-5-indene-glycine"]="⎁";
official_name["n-5-indene-glycine"]="Fmoc-L-2-indanylglycine";
weight["n-5-indene-glycine"]=413.500;
weight["⎁"]=413.500;
simplification["n-5-indene-glycine"]="G";
nature["n-5-indene-glycine"]="non-natural";
id["n-5-indene-glycine"]=44784803;

translate_modif["n-acetylthreonine"]="⎀";
official_name["n-acetylthreonine"]="N-Acetylthreonine";
weight["n-acetylthreonine"]=161.160;
weight["⎀"]=161.160;
simplification["n-acetylthreonine"]="T";
nature["n-acetylthreonine"]="non-natural";
id["n-acetylthreonine"]=152204;

translate_modif["naphthyl-2-carboxyl_proline"]="⍽";
official_name["naphthyl-2-carboxyl_proline"]="N-Methyl-1-naphthalenecarboxamide";
weight["naphthyl-2-carboxyl_proline"]=185.220;
weight["⍽"]=185.220;
simplification["naphthyl-2-carboxyl_proline"]="P";
nature["naphthyl-2-carboxyl_proline"]="non-natural";
id["naphthyl-2-carboxyl_proline"]=18839;

translate_modif["n-butylglycine"]="⍺";
official_name["n-butylglycine"]="Di-n-butylglycine";
weight["n-butylglycine"]=187.280;
weight["⍺"]=187.280;
simplification["n-butylglycine"]="G";
nature["n-butylglycine"]="non-natural";
id["n-butylglycine"]=239818;

translate_modif["n-ethylglycine"]="⍹";
official_name["n-ethylglycine"]="N-Ethylglycine";
weight["n-ethylglycine"]=103.120;
weight["⍹"]=103.120;
simplification["n-ethylglycine"]="G";
nature["n-ethylglycine"]="non-natural";
id["n-ethylglycine"]=316542;

translate_modif["n-ethyl-valine"]="⍸";
official_name["n-ethyl-valine"]="3-Ethylaniline";
weight["n-ethyl-valine"]=121.180;
weight["⍸"]=121.180;
simplification["n-ethyl-valine"]="V";
nature["n-ethyl-valine"]="non-natural";
id["n-ethyl-valine"]=11475;

translate_modif["n-hydroxyethylglycine"]="⋟";
official_name["n-hydroxyethylglycine"]="D-4-Hydroxyphenylglycine";
weight["n-hydroxyethylglycine"]=167.160;
weight["⋟"]=167.160;
simplification["n-hydroxyethylglycine"]="G";
nature["n-hydroxyethylglycine"]="non-natural";
id["n-hydroxyethylglycine"]=89853;

translate_modif["n-isobutyl-glycine"]="⍷";
official_name["n-isobutyl-glycine"]="N-Isobutylglycine";
weight["n-isobutyl-glycine"]=131.170;
weight["⍷"]=131.170;
simplification["n-isobutyl-glycine"]="G";
nature["n-isobutyl-glycine"]="non-natural";
id["n-isobutyl-glycine"]=10261121;

translate_modif["nitrotyrosine"]="Љ";
official_name["nitrotyrosine"]="3-nitro-L-tyrosine";
weight["nitrotyrosine"]=226.190;
weight["Љ"]=226.190;
simplification["nitrotyrosine"]="Y";
nature["nitrotyrosine"]="non-natural";
id["nitrotyrosine"]=65124;

translate_modif["n-methoxytryptophan"]="⊣";
official_name["n-methoxytryptophan"]="5-Methoxytryptophan";
weight["n-methoxytryptophan"]=234.250;
weight["⊣"]=234.250;
simplification["n-methoxytryptophan"]="W";
nature["n-methoxytryptophan"]="non-natural";
id["n-methoxytryptophan"]=151018;

translate_modif["n-methylglycine"]="⌑";
official_name["n-methylglycine"]="Sarcosine";
weight["n-methylglycine"]=89.090;
weight["⌑"]=89.090;
simplification["n-methylglycine"]="G";
nature["n-methylglycine"]="non-natural";
id["n-methylglycine"]=1088;

translate_modif["n-methyl_half_cystine"]="⍶";
official_name["n-methyl_half_cystine"]="S-Methylthiocysteine";
weight["n-methyl_half_cystine"]=167.300;
weight["⍶"]=167.300;
simplification["n-methyl_half_cystine"]="_";
nature["n-methyl_half_cystine"]="non-natural";
id["n-methyl_half_cystine"]=3080775;

translate_modif["norarginine"]="⍵";
official_name["norarginine"]="L-Norarginine";
weight["norarginine"]=160.170;
weight["⍵"]=160.170;
simplification["norarginine"]="R";
nature["norarginine"]="non-natural";
id["norarginine"]=11389551;

translate_modif["n-s-phenylethylglycine"]="⍴";
official_name["n-s-phenylethylglycine"]="Phenylacetylglycine";
weight["n-s-phenylethylglycine"]=193.200;
weight["⍴"]=193.200;
simplification["n-s-phenylethylglycine"]="G";
nature["n-s-phenylethylglycine"]="non-natural";
id["n-s-phenylethylglycine"]=68144;

translate_modif["n-term_3-bromobenzoyl"]="Ֆ";
official_name["n-term_3-bromobenzoyl"]="3-Bromobenzoyl_chloride";
weight["n-term_3-bromobenzoyl"]=219.460;
weight["Ֆ"]=219.460;
simplification["n-term_3-bromobenzoyl"]="_";
nature["n-term_3-bromobenzoyl"]="non-natural";
id["n-term_3-bromobenzoyl"]=74377;

translate_modif["n-term_3-chlorobenzoyl"]="⍳";
official_name["n-term_3-chlorobenzoyl"]="2,4,6-Trichlorobenzoyl_chloride";
weight["n-term_3-chlorobenzoyl"]=243.900;
weight["⍳"]=243.900;
simplification["n-term_3-chlorobenzoyl"]="_";
nature["n-term_3-chlorobenzoyl"]="non-natural";
id["n-term_3-chlorobenzoyl"]=2733703;

translate_modif["n-term_3-chlorobenzyl"]="⍲";
official_name["n-term_3-chlorobenzyl"]="2,4,6-Trichlorobenzyl_alcohol";
weight["n-term_3-chlorobenzyl"]=211.500;
weight["⍲"]=211.500;
simplification["n-term_3-chlorobenzyl"]="_";
nature["n-term_3-chlorobenzyl"]="non-natural";
id["n-term_3-chlorobenzyl"]=4582574;

translate_modif["n-term_4-chlorobenzyl"]="⍲";
official_name["n-term_4-chlorobenzyl"]="2,4,6-Trichlorobenzyl_alcohol";
weight["n-term_4-chlorobenzyl"]=211.500;
weight["⍲"]=211.500;
simplification["n-term_4-chlorobenzyl"]="_";
nature["n-term_4-chlorobenzyl"]="non-natural";
id["n-term_4-chlorobenzyl"]=4582574;

translate_modif["n-term_4-fluorobenzoyl"]="⍱";
official_name["n-term_4-fluorobenzoyl"]="2,4,5-Trifluorobenzoyl_chloride";
weight["n-term_4-fluorobenzoyl"]=194.540;
weight["⍱"]=194.540;
simplification["n-term_4-fluorobenzoyl"]="_";
nature["n-term_4-fluorobenzoyl"]="non-natural";
id["n-term_4-fluorobenzoyl"]=145164;

translate_modif["n-term_4-fluorobenzyl"]="⍰";
official_name["n-term_4-fluorobenzyl"]="2,4,5-Trifluorobenzyl_Alcohol";
weight["n-term_4-fluorobenzyl"]=162.110;
weight["⍰"]=162.110;
simplification["n-term_4-fluorobenzyl"]="_";
nature["n-term_4-fluorobenzyl"]="non-natural";
id["n-term_4-fluorobenzyl"]=2777035;

translate_modif["n-term_benzoyl"]="⍯";
official_name["n-term_benzoyl"]="4-Nitrobenzoyl_chloride";
weight["n-term_benzoyl"]=185.560;
weight["⍯"]=185.560;
simplification["n-term_benzoyl"]="_";
nature["n-term_benzoyl"]="non-natural";
id["n-term_benzoyl"]=8502;

translate_modif["n-tridecylglycine"]="⍮";
official_name["n-tridecylglycine"]="N-Tritylglycine";
weight["n-tridecylglycine"]=317.400;
weight["⍮"]=317.400;
simplification["n-tridecylglycine"]="G";
nature["n-tridecylglycine"]="non-natural";
id["n-tridecylglycine"]=248898;

translate_modif["obu2-oxobutanoicacid"]="⍭";
official_name["obu2-oxobutanoicacid"]="3-Fluorobutyric_acid";
weight["obu2-oxobutanoicacid"]=106.100;
weight["⍭"]=106.100;
simplification["obu2-oxobutanoicacid"]="_";
nature["obu2-oxobutanoicacid"]="non-natural";
id["obu2-oxobutanoicacid"]=15578254;

translate_modif["octyl-glycine"]="⍜";
official_name["octyl-glycine"]="N-Octylglycine";
weight["octyl-glycine"]=187.280;
weight["⍜"]=187.280;
simplification["octyl-glycine"]="G";
nature["octyl-glycine"]="non-natural";
id["octyl-glycine"]=350339;

translate_modif["penicillamine_oxidised"]="⍬";
official_name["penicillamine_oxidised"]="DL-Penicillamine";
weight["penicillamine_oxidised"]=149.210;
weight["⍬"]=149.210;
simplification["penicillamine_oxidised"]="_";
nature["penicillamine_oxidised"]="non-natural";
id["penicillamine_oxidised"]=4727;

translate_modif["r-2-4-pentenylalanine"]="⌂";
official_name["r-2-4-pentenylalanine"]="N-(3-Methylbut-2-EN-1-YL)-9H-purin-6-amine";
weight["r-2-4-pentenylalanine"]=203.240;
weight["⌂"]=203.240;
simplification["r-2-4-pentenylalanine"]="A";
nature["r-2-4-pentenylalanine"]="non-natural";
id["r-2-4-pentenylalanine"]=92180;

translate_modif["s-benzyl-l-cysteine"]="⍫";
official_name["s-benzyl-l-cysteine"]="S-Benzyl-L-cysteine";
weight["s-benzyl-l-cysteine"]=211.280;
weight["⍫"]=211.280;
simplification["s-benzyl-l-cysteine"]="C";
nature["s-benzyl-l-cysteine"]="non-natural";
id["s-benzyl-l-cysteine"]=193613;

translate_modif["s-carboxymethyl-cysteine"]="⍪";
official_name["s-carboxymethyl-cysteine"]="S-Carboxymethylcysteine";
weight["s-carboxymethyl-cysteine"]=179.200;
weight["⍪"]=179.200;
simplification["s-carboxymethyl-cysteine"]="C";
nature["s-carboxymethyl-cysteine"]="non-natural";
id["s-carboxymethyl-cysteine"]=1080;

translate_modif["s-n-fmoc-2-4-pentenylalanine"]="⍩";
official_name["s-n-fmoc-2-4-pentenylalanine"]="(R)-3-((((9H-Fluoren-9-yl)methoxy)carbonyl)amino)-4-phenylbutanoic_acid";
weight["s-n-fmoc-2-4-pentenylalanine"]=401.500;
weight["⍩"]=401.500;
simplification["s-n-fmoc-2-4-pentenylalanine"]="A";
nature["s-n-fmoc-2-4-pentenylalanine"]="non-natural";
id["s-n-fmoc-2-4-pentenylalanine"]=7014919;

translate_modif["tetrahydroisoquinoline-1-carboxylic_acid"]="Բ";
official_name["tetrahydroisoquinoline-1-carboxylic_acid"]="Quinolinepropionic_acid,_1,2,3,4-tetrahydro-";
weight["tetrahydroisoquinoline-1-carboxylic_acid"]=205.250;
weight["Բ"]=205.250;
simplification["tetrahydroisoquinoline-1-carboxylic_acid"]="_";
nature["tetrahydroisoquinoline-1-carboxylic_acid"]="non-natural";
id["tetrahydroisoquinoline-1-carboxylic_acid"]=98707;

translate_modif["trifluoromethane-proline"]="Њ";
official_name["trifluoromethane-proline"]="6-Bromo-2-(trifluoromethyl)quinoline";
weight["trifluoromethane-proline"]=276.050;
weight["Њ"]=276.050;
simplification["trifluoromethane-proline"]="P";
nature["trifluoromethane-proline"]="non-natural";
id["trifluoromethane-proline"]=15325795;

translate_modif["trifluorophenylalanine"]="⍨";
official_name["trifluorophenylalanine"]="2,4,5-Trifluoro-L-phenylalanine";
weight["trifluorophenylalanine"]=219.160;
weight["⍨"]=219.160;
simplification["trifluorophenylalanine"]="F";
nature["trifluorophenylalanine"]="non-natural";
id["trifluorophenylalanine"]=2762263;

translate_modif["trimethylglycine"]="⍧";
official_name["trimethylglycine"]="Betaine";
weight["trimethylglycine"]=117.150;
weight["⍧"]=117.150;
simplification["trimethylglycine"]="G";
nature["trimethylglycine"]="non-natural";
id["trimethylglycine"]=247;

translate_modif["trimethyl-lysine"]="⎊";
official_name["trimethyl-lysine"]="Trimethylamine";
weight["trimethyl-lysine"]=59.110;
weight["⎊"]=59.110;
simplification["trimethyl-lysine"]="K";
nature["trimethyl-lysine"]="non-natural";
id["trimethyl-lysine"]=1146;

translate_modif["xylene_cystine_half"]="C";
official_name["xylene_cystine_half"]="Cysteine";
weight["xylene_cystine_half"]=121.160;
weight["C"]=121.160;
simplification["xylene_cystine_half"]="_";
nature["xylene_cystine_half"]="non-natural";
id["xylene_cystine_half"]=5862;

}
