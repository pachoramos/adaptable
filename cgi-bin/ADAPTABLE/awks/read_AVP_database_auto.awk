function read_AVP_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,v) {
	limit=2000
	                a=1;
        limit_=a+limit
                path="DATABASES/AVP/AVPfiles/AVP"
                root_online="http://crdd.osdd.net/servers/avpdb/record.php?details=AVP"
                length_numb=4
                add_tag="yes"
                split(path,aa,"/")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file=path""tag""a
			if (system("[ -f " file " ] ") ==0 ) {
#seq
				input_file=file
					field=read_database_line(input_file,">Sequence<",0,"COLOR='black'>","</b",1)
					gsub("_","",field)
					seq_a[a]=field
					seq_a[a]=correct_sequence(seq_a[a])
					print "Reading "file" for sequence "seq_a[a]""
					seq_a[a]=analyze_sequence(seq_a[a])
#ID
					field=read_database_line(input_file,">AVPid<",0,"COLOR='black'>","</b",1)
					if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]""field";"}
#name
				field=read_database_line(input_file,">Nomenclature<",0,"COLOR='black'>","</b",1)
				field=clean_string(field)
				if(field=="-") {field=""}
				if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";"}}

#source
                                field=read_database_line(input_file,">Source</b>",0,"'>","</b>",1)
                                field=clean_string(field)
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}

#PTM
                        field=read_database_line(input_file,"Type Modification",0,"black'>","<")
                        field=clean_string(field)
                        if(field!~/None/ && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                        if(field~/cetyl/ && field~/N-term/) {if(N_terminus_[seq_a[a]]!~field) {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
                        if(field~/midation/ && field~/C-term/) {if(C_terminus_[seq_a[a]]!~field) {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
                        if(field~/isulfide/) {disulfide_[seq_a[a]]="disulfide";if(PTM_[seq_a[a]]!~field) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"disulfide;"}}
                        if(field~/lantibio/) {if(PTM_[seq_a[a]]!~/lantibio/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lantibiotic;"}}
                        if(field~/lycos/) {if(PTM_[seq_a[a]]!~/lycos/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"Glycosylation;"}}
#Function
			field=read_database_line(input_file,"<head>",1,"verified_","_peptides",1)
			if(Function_[seq_a[a]]!~field) {Function_[seq_a[a]]=Function_[seq_a[a]]""field";"}

				field="antiviral"
                                read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
# activity_viral_test_	
				field1=read_database_line(input_file,">Against virus</b>",0,"black'>","</b>",1)
					gsub("&nbsp;","_",field1)
					gsub("\\(","_",field1)
					gsub("\\)","",field1)
					field2=read_database_line(input_file,">Inhibition",0,"/","&nbsp;",1)
					gsub("&mu;M","µM",field2)
					gsub("IC<SUB>50","IC50",field2)
					field3=read_database_line(input_file,">Inhibition",0,"COLOR='black'>","</td></tr><tr><td",1)
					gsub("&plusmn;","_+-",field3); 
					field4=read_database_line(input_file,">Unit<",3,"dontcut","</a></b></td>",1)
					gsub("&mu;M","µM",field4)
					field5=""
					if(field4~/\(/) {split(field4,aa,"("); split(aa[2],bb,")"); units=bb[1]; gsub(" ","",aa[1]);if(aa[1]!="" && aa[1]!="%") {field5=aa[1]}} else {if(field4!="%") {units=field4} else {units="%"}}
					if(field5=="") {field5="IC50"}
					if(activity_viral_test_[seq_a[a]]!~field5) {activity_viral_test_[seq_a[a]]=activity_viral_test_[seq_a[a]]""field5";"}
					

				string=field1"("field5"="field3""units")"
				string=analyze_organism(string,seq_a[a])
				string_to_compare=""
				string_to_compare=escape_pattern(string)
					if(string!="(=)") {
						if(activity_viral_[seq_a[a]]!~string_to_compare) {if(string!="") {activity_viral_[seq_a[a]]=activity_viral_[seq_a[a]]""string";"}}
						# Also in all_organisms as some DBs are not able to do this splitting
						if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
					}


#Taxonomy
					field=read_database_line(input_file,">Virus Family</b>",0,"'>","</b>",1)
					field=clean_string(field)
					field=tolower(field)
					if(taxonomy_[seq_a[a]]!~field) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}}
#Pubmed			
				field=1; b=0; while(field!="") {b=b+1
					field=read_database_line(input_file,"Accession",0,"pubmed/","\"_rel",b)
						if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}
				}
#Experimental structure/PDB
                        field=read_database_line(input_file,"http://www.rcsb.org/pdb/",0,"pdbId=","\\\"")
                        field=toupper(field)
                        field=clean_string(field)
                        if(field!="") { if(experim_structure_[seq_a[a]]!~field) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}}
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}

			# It is a "manually curated, open source archive having experimentally..."
			experimental_[seq_a[a]]="experimental"
			}
			a=a+1
				amax=a
		}
		return amax
}
