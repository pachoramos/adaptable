function read_ParaPep_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {
limit=2000

                a=1000;
        limit_=a+limit
        print "" > "tmp_files/modified_aa_parapep"
                path="DATABASES/ParaPep/ParaPepfiles/ParaPep"
                root_online="http://crdd.osdd.net/raghava/parapep/display_sub.php?details="
                length_numb=4
                add_tag="no"
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                while(a<limit_) {
		file=path""tag""a
                        if (system("[ -f " file " ] ") ==0 ) {

			input_file=file
#seq
				field=read_database_line(input_file,"SEQUENCE",0,"black'>","</b></td></tr><tr><td",1)
				field_orig=field
				seq_a[a]=field
				seq_a[a]=correct_sequence(seq_a[a])
				read_modified_aa(modif,translate_modif)
				field=read_database_line(input_file,"'>Chemical Modification",0,"'black'>","</b",1)
                                gsub("*","#",field)
                                gsub("*","#",seq_a[a])
                                # Clean Not_ statements in seq
                                gsub("Not_Available","",seq_a[a])
                                
                                # Fix awful unclosed parenthesis
                                field=clean_string(field)

                                # Make modifications separated by 'and' be separated by ; as all the others
                                gsub(" and ",";",field)

				# Make modification separated by ',' be separated by ';' without altering other uses of , (like 3,3-..., 4,2...)
				string=""
				split(field,cc,","); w=0; while(w<length(cc)) {w=w+1
					string=string""cc[w]
					if (w<length(cc)) {
						if(substr(cc[w],length(cc[w]),1)!~/^[0-9]+$/ && substr(cc[w],length(cc[w]),1)!="N" && substr(cc[w],length(cc[w]),1)!="S" && substr(cc[w],length(cc[w]),1)!="O" && substr(cc[w],length(cc[w]),1)!="P" ) {string=string";"} else {string=string","}
					}
				}
                                field=string

                                # We don't want to leave parenthesis in the final sequence
                                gsub("\\(","",seq_a[a])
                                gsub("\\)","",seq_a[a])
                                gsub("\\[","",seq_a[a])
                                gsub("\\]","",seq_a[a])
                                gsub("\\{","",seq_a[a])
                                gsub("\\}","",seq_a[a])

				# This for many modifications splitted by ;
                                split(field,aa,";"); z=0;while(z<length(aa)) {z=z+1;

	                                gsub("_","",aa[z])
	                                split(aa[z],bb,":")

	                                # Generate a temporal file to allow us to update the modified aa assignments
	                                # Here we replace the patterns in the sequence as taken from Type of Modification field, that indicates the letter and the meaning
	                                if(aa[z]~/=/) {split(aa[z],bb,"=")}
	                                if(aa[z]~/:/) {split(aa[z],bb,":")}

                                        # Ignore case
                                        bb[2]=tolower(bb[2])

	                                if(translate_modif[bb[2]]=="" && bb[2]!="") {
	                                	print "translate_modif[\""bb[2]"\"]=\"X\";" >> "tmp_files/modified_aa_parapep"
		                               	# Show X for now until we update read_modified_aa
						translate_modif[bb[2]]="X"
					}
                                        # Do the substitution: we replace bb[1] (from sequence) with the translate_modif of the LEGEND (bb[2]) because sometimes there are cases like
                                        # O=Ornithine
                                        # X=Ornithine
                                        # for different IDs
					gsub(bb[1],translate_modif[bb[2]],seq_a[a])
				}
				seq_a[a]=analyze_sequence(seq_a[a])
				print "Reading "file" for sequence "seq_a[a]""

#ID
                                field=read_database_line(input_file,"ParaPep_ID",0,"COLOR='black'>","</b></td></tr><tr><td",1)
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"ParaPep"field";" }
#name
                                field=read_database_line(input_file,"NAME",0,"COLOR='black'>","</b></td></tr><tr><td",1)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#stereo
                                field=read_database_line(input_file,"Stereochemistry",0,"COLOR='black'>","</b></td></tr><tr><td",1)
                                if (stereo_[seq_a[a]]!~field){if(field!="") {stereo_[seq_a[a]]=stereo_[seq_a[a]]""field";"}}
#N_terminus
                                field=read_database_line(input_file,"N-ter_Modification",0,"COLOR='black'>","</b></td></tr><tr><td",1)
                                field=clean_string(field)
                                gsub("Free","",field)
                                if(N_terminus_[seq_a[a]]!~field) {if(field!="") {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
#C_terminus
                                field=read_database_line(input_file,"C-ter_Modification",0,"COLOR='black'>","</b></td></tr><tr><td",1)
                                field=clean_string(field)
                                gsub("Free","",field)
                                if(C_terminus_[seq_a[a]]!~field) {if(field!="") {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
#PTM
				field=read_database_line(input_file,"Chemical_Modification",0,"COLOR='black'>","</b></td></tr><tr><td",1)
				field=clean_string(field)
				if (PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}

#cyclic                 
                        field=read_database_line(input_file,"Linear/Cyclic",0,"COLOR='black'>","</b></td></tr><tr><td",1)

                        field=clean_string(field)
                        if(field~/yclic/) {cyclic_[seq_a[a]]="cyclic"}
                        field=clean_string(field)                        
                        if(PTM_[seq_a[a]]!~field && field~/yclic/ && PTM_[seq_a[a]]!~/cyclization/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"cyclization;"}

                        field=read_database_line(input_file,"Type Modification",0,"black'>","<")
                        field=clean_string(field)
                        if(field!~/None/ && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                        if(field~/cetyl/ && field~/N-term/) {if(N_terminus_[seq_a[a]]!~field) {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
                        if(field~/midation/ && field~/C-term/) {if(C_terminus_[seq_a[a]]!~field) {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
                        if(field~/isulfide/) {disulfide_[seq_a[a]]="disulfide";if(PTM_[seq_a[a]]!~field) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"disulfide;"}}
                        if(field~/lantibio/) {if(PTM_[seq_a[a]]!~/lantibio/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lantibiotic;"}}
                        if(field~/lycos/) {if(PTM_[seq_a[a]]!~/lycos/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"Glycosylation;"}}

#Function

                        field=read_database_line(input_file,"Nature",0,"COLOR='black'>","</b>",2)
                        gsub(" ionophores","_ionophores",field)
		gsub("_and_",",",field); amax=split(field,aa,",")
		aaa=0; while(aaa<amax) {aaa=aaa+1
                                read_database_classify_field(aa[aaa],",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
				}

#all organisms
                        field1=read_database_line(input_file,"Activity",0,"black'>","</b>")
                        gsub("<sub>50</sub>","50",field1)
                        gsub("<sub>100</sub>","100",field1)
                        gsub("Minimum_amoebicidal_concentration\\(MAC\\)","MAC",field1)
                        gsub("Not_reported","",field1)
                        gsub("_for","",field1)

                        field2=read_database_line(input_file,"Parasite_Type",0,"COLOR='black'>","</b></td></tr><tr><td",2)
                        gsub("\\(","_",field2)
                        gsub("\\)","",field2)
                        gsub("","",field2)
                        field=field2"("field1")"
			field=analyze_organism(field,seq_a[a])
			string_to_compare=escape_pattern(field)
			if(all_organisms_[seq_a[a]]!~string_to_compare) {if(field!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""field";"}}
#pdb
			field=1; b=0; while(field!="") {b=b+1
			 field=read_database_line(input_file,">PDB exact</td>",1,"pdbId=","'>",b)
			 field=clean_string(field)
			 field=toupper(field)
                                if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}
			}

			field=1; b=0; while(field!="") {b=b+1
                         field=read_database_line(input_file,">PDB exact</td>",2,"pdbId=","'>",b)
                         field=clean_string(field)
                         field=toupper(field)
                                if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}
                        }

#Pubmed			
				field=1; b=0; while(field!="") {b=b+1
                        field=read_database_line(input_file,"PMID",0,"COLOR='black'>","</b></td></tr><tr><td",b)
                        gsub("NA","",field)
                        if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}
			}

			# All in this database are experimentally validated
			# http://crdd.osdd.net/raghava/parapep/
			experimental_[seq_a[a]]="experimental"

	}
       close(input_file)
	a=a+1
file="DATABASES/DBAASP/DBAASPfiles/DBAASP_"a
amax=a
}
return amax
}
