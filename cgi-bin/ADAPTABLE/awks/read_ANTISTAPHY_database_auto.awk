function read_ANTISTAPHY_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {
			path="DATABASES/ANTISTAPHY/ANTISTAPHYfiles/ANTISTAPHY"
                        split(path,aa,"/")
                        file_list=aa[1]"/"aa[2]"/list"
                        name=1
                        ll=1; while(name!="") {ll=ll+1
                                name=read_database_line(file_list,"trouver",0,"(",")",ll)
                                        gsub("_","+",name); gsub("'","",name)
                                        file=path""name
                        if (system("[ -f " file " ] ") ==0 ) {

			input_file=file
#seq
				field=read_database_line(input_file,"Sequence</th",5,"<TD>","</TD>",3)
				seq_a[a]=field
print "Reading "file" for sequence "seq_a[a]""
seq_a[a]=analyze_sequence(seq_a[a])
#ID
                                field=read_database_line(input_file,">ID</th",7,"<TD>","</TD>",1)
                                if(ID_[seq_a[a]]!~field) {if(field!="") {ID_[seq_a[a]]=ID_[seq_a[a]]"ANTISTAPHY"field";" }}
#name
                                field=read_database_line(input_file,">Peptide_Name</th",6,"<TD>","</TD>",2)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,">Producer_organism</th",4,"<TD>","</TD>",4)
				field=clean_string(field)
				if(field=="synthesized") {synthetic_[seq_a[a]]="synthetic"}
				else {
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}}
#stereo
                                field=read_database_line(input_file,"Stereochemistry",0,"COLOR='black'>","</b></td></tr><tr><td",1)
                                if (stereo_[seq_a[a]]!~field){if(field!="") {stereo_[seq_a[a]]=stereo_[seq_a[a]]""field";"}}
#N_terminus
                                field=read_database_line(input_file,"N-ter_Modification",0,"COLOR='black'>","</b></td></tr><tr><td",1)
                                field=clean_string(field)
                                if(N_terminus_[seq_a[a]]!~field) {if(field!="") {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
#C_terminus
                                field=read_database_line(input_file,"C-ter_Modification",0,"COLOR='black'>","</b></td></tr><tr><td",1)
                                field=clean_string(field)
                                if(C_terminus_[seq_a[a]]!~field) {if(field!="") {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
#cyclic and PTM
                        field=read_database_line(input_file,"Linear/Cyclic",0,"COLOR='black'>","</b></td></tr><tr><td",1)
                        field=clean_string(field)
                        if(field~/yclic/) {cyclic_[seq_a[a]]="cyclic";if(PTM_[seq_a[a]]!~/yclic/ && PTM_[seq_a[a]]!~/cyclization/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"cyclization;"}}

                        field=read_database_line(input_file,"Type Modification",0,"black'>","<")
                        field=clean_string(field)
                        if(field!~/None/ && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                        if(field~/cetyl/ && field~/N-term/) {if(N_terminus_[seq_a[a]]!~field) {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
                        if(field~/midation/ && field~/C-term/) {if(C_terminus_[seq_a[a]]!~field) {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
                        if(field~/isulfide/) {disulfide_[seq_a[a]]="disulfide";if(PTM_[seq_a[a]]!~field) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"disulfide;"}}
                        if(field~/lantibio/) {if(PTM_[seq_a[a]]!~/lantibio/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lantibiotic;"}}
                        if(field~/lycos/) {if(PTM_[seq_a[a]]!~/lycos/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"Glycosylation;"}}
#Function
                field=read_database_line(input_file,"Nature",0,"COLOR='black'>","</b>",2)
		gsub("_and_",",",field); amax=split(field,aa,",")
		aaa=0; while(aaa<amax) {aaa=aaa+1
                                read_database_classify_field(aa[aaa],",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
				}
#all organisms
                        field1=read_database_line(input_file,"Target_organism",3,"<TD>","</TD>",5)
                        if(field1=="") {field1=read_database_line(input_file,"Target_organism",4,"<TD>","</TD>",1)}
                        field1=clean_string(field1)
                        gsub("__","",field1)
        		field2="MIC"
			field3=read_database_line(input_file,"Target_organism",3,"<TD>","</TD>",6)
			if(field3=="") {field3=read_database_line(input_file,"Target_organism",4,"<TD>","</TD>",2); 
			        if(substr(field3,1,1)!~/^[0-9]+$/) {
					   field3=read_database_line(input_file,"Target_organism",4,"<TD>","</TD>",1)
				   }
			}
			field4="µM"

			string=field1"("field2"="field3""field4")"
          		string=analyze_organism(string,seq_a[a])
          		string_to_compare=""
                        # We need to build a string to compare escaping the parenthesis, while we still add the original 'non-escaped' version to all_organisms_[sequence]
                        string_to_compare=escape_pattern(string)

			if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}

			string=full_microbe_name["S.aureus"]
			string=analyze_organism(string,seq_a[a])
			if(all_organisms_[seq_a[a]]!~string) {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}

#PMID
                                field=1; b=0; while(field!="") {b=b+1
                                        field=read_database_line(input_file,">Abstract</th>",1,"href=\"","\">View_abst",b)
                         if (field~/pubmed/) {
                          g=split(field,gg,"/")
                          field=gg[g]
                          gsub("?","",field)
                          gsub("pubmedterm=","",field)
                          gsub("dopt=Citation","",field)
                          gsub("\\[uid\\]","",field)
                          gsub("term=","",field)
                          if (field~/pubmedcmd/) {
                           g=split(field,gg,"=")
                           field=gg[g]
                           gsub("search&","",field)
                          }
                        } 
                        if (field~/%20/) {field=""} # Drop strange values
                        gsub("tool=pubmed","",field)
                                                
                        if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}
                        }

                        # https://link.springer.com/article/10.1007%2Fs00203-016-1293-6
                        # The EOs, AMPs and their relative information were extracted from the scientific literature using a manual text mining from available bibliographic databases. Sequences of active AMPs were mainly extracted from public databases 
			experimental_[seq_a[a]]="experimental"
	}
       close(input_file)
	a=a+1
file="DATABASES/DBAASP/DBAASPfiles/DBAASP_"a
amax=a
}
return amax
}
