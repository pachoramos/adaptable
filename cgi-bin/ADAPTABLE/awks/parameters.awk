function parameters(prp_name,prp_option)
{
        delete prp_name
        delete prp_option

        calculation_label=ENVIRON["__calculation_label"]
        ID=ENVIRON["__ID"]
        sequence=ENVIRON["__sequence"]
        name=ENVIRON["__name"]
        source=ENVIRON["__source"]
        taxonomy=ENVIRON["__taxonomy"]
        Family=ENVIRON["__Family"]
        gene=ENVIRON["__gene"]
        stereo=ENVIRON["__stereo"]                                                                              #L,D,mix
        N_terminus=ENVIRON["__N_terminus"]
        C_terminus=ENVIRON["__C_terminus"]
        PTM=ENVIRON["__PTM"]
        cyclic=ENVIRON["__cyclic"]                                                                              
        target=ENVIRON["__target"]
        all_organisms=ENVIRON["__all_organisms"] ; activity=ENVIRON["__activity"] ; activity_test=ENVIRON["__activity_test"]
#       ____________________________________________________________________________________________________________
        antimicrobial=ENVIRON["__antimicrobial"] 
#       ------------------------------------------------------------------------------------------------------------
                antibacterial=ENVIRON["__antibacterial"] ; antigram_pos=ENVIRON["__antigram_pos"] ; antigram_neg=ENVIRON["__antigram_neg"]
                antifungal=ENVIRON["__antifungal"]   ;  antiyeast=ENVIRON["__antiyeast"]
                antiviral=ENVIRON["__antiviral"]   ;   activity_viral=ENVIRON["__activity_viral"] ; activity_viral_test=ENVIRON["__activity_viral_test"]
#       _____________________________________________________________________________________________________________
        antiprotozoal=ENVIRON["__antiprotozoal"]
#       -------------------------------------------------------------------------------------------------------------
                antiparasitic=ENVIRON["__antiparasitic"] ; antiplasmodial=ENVIRON["__antiplasmodial"] ; antitrypanosomic=ENVIRON["__antitrypanosomic"] ; antileishmania=ENVIRON["__antileishmania"]
#       _____________________________________________________________________________________________________________
        insecticidal=ENVIRON["__insecticidal"]
#       _____________________________________________________________________________________________________________
        anticancer=ENVIRON["__anticancer"]; 
#       -------------------------------------------------------------------------------------------------------------
                antitumor=ENVIRON["__antitumor"] ; cell_line=ENVIRON["__cell_line"] ; tissue=ENVIRON["__tissue"] ; cancer_type=ENVIRON["__cancer_type"] ; anticancer_activity=ENVIRON["__anticancer_activity"] ; anticancer_activity_test=ENVIRON["__anticancer_activity_test"] 
                antiangiogenic=ENVIRON["__antiangiogenic"]
#       _____________________________________________________________________________________________________________
        toxic=ENVIRON["__toxic"] ; 
#       -------------------------------------------------------------------------------------------------------------
                cytotoxic=ENVIRON["__cytotoxic"] ; 
                hemolytic=ENVIRON["__hemolytic"] ; hemolytic_activity=ENVIRON["__hemolytic_activity"] ; hemolytic_activity_test=ENVIRON["__hemolytic_activity_test"] ; RBC_source=ENVIRON["__RBC_source"]
#       _____________________________________________________________________________________________________________
        cell_cell=ENVIRON["__cell_cell"]
#       -------------------------------------------------------------------------------------------------------------
                hormone=ENVIRON["__hormone"]
                quorum_sensing=ENVIRON["__quorum_sensing"]
                immunomodulant=ENVIRON["__immunomodulant"]
                antihypertensive=ENVIRON["__antihypertensive"]
#       _____________________________________________________________________________________________________________
        drug_delivery=ENVIRON["__drug_delivery"]
#       -------------------------------------------------------------------------------------------------------------
                cell_penetrating=ENVIRON["__cell_penetrating"]
                tumor_homing=ENVIRON["__tumor_homing"]
                blood_brain=ENVIRON["__blood_brain"]
#       _____________________________________________________________________________________________________________
        antioxidant=ENVIRON["__antioxidant"]
        antiproliferative=ENVIRON["__antiproliferative"]

        DSSP=ENVIRON["__DSSP"]
        pdb=ENVIRON["__pdb"]
#experim_structure="pdb" toselect experimental structures
        experim_structure=ENVIRON["__experim_structure"]
        PMID=ENVIRON["__PMID"]
        synthetic=ENVIRON["__synthetic"]
	ribosomal=ENVIRON["__ribosomal"]
	experimental=ENVIRON["__experimental"]
	biofilm=ENVIRON["__antibiofilm"]
# solubility = "soluble","poorly_soluble","almost_insoluble","insoluble","i"
        solubility=ENVIRON["__solubility"]
        sequence_length_range=ENVIRON["__sequence_length_range"]
# n_region is usually the maximal length of the sequence_length_range
#        n_regions=50
        n_regions=ENVIRON["__n_regions"]
# align_method="simple" or align_method="blosum"
        align_method=ENVIRON["__align_method"]  
# Families can be generated so that each family contains different peptides or each member can give rise to his family (the same element is found in different families)
# The second method is much more fast and correspond to generate_overlapping_families="yes"
generate_overlapping_families="yes"
# Choose the classe for which parameters are calculated. 
# The number of classes is defined by the level_agreement=from -5 to 5 (if "all" only one class is created; negative values appliable only to blosum)
# if "auto" levels are created based on sequence length 
        level_agreement=ENVIRON["__level_agreement"]
# families_to_analyse="1-3" repeat the analysis for classes from 1 to 3; "1-1" only class 1 ; an empty value calculate for all classes 
        families_to_analyse=""
# Minimum length of sequence to generate a family, think for the cases of all_families choosing fathers of only two aminoacids and creating huge output with useless families.
        min_seq_length=2
# work_in_simplified_aa_space="y" align the sequences based on the presence of aromatic (indicated by F), Glycines(G),cysteines(C),positive(K),negative(D),hydroohobic(A),proline(P)
# work_in_simplified_aa_space="DSSP" align the sequence based on their structure: β-bridge (B), extended strand(E), α-helix(H),310 helix(G),π-helix(I),bend(S),hydrogen bonded turn(T)  
        work_in_simplified_aa_space=ENVIRON["__work_in_simplified_aa_space"]
        verbose="n"
        plot_graphs=ENVIRON["__plot_graphs"]
        simplify_aminoacids=ENVIRON["__simplify_aminoacids"]
        design_family_representative_peptide="y"
        analyse_only_region_of_the_fam_father="y"
        virus_name=ENVIRON["__virus_name"]
        calculate_only_global_properties="n"

               prp_name[1]="ID"; prp_name[2]="sequence"; prp_name[3]="name"; prp_name[4]="source"; prp_name[5]="Family"
                        prp_name[6]="gene"; prp_name[7]="stereo"; prp_name[8]="N_terminus"; prp_name[9]="C_terminus"; prp_name[10]="PTM"
                        prp_name[11]="cyclic"; prp_name[12]="target"; prp_name[13]="synthetic"; prp_name[14]="antimicrobial"; prp_name[15]="antibacterial"
                        prp_name[16]="antigram_pos"; prp_name[17]="antigram_neg"; prp_name[18]="antifungal"; prp_name[19]="antiyeast"; prp_name[20]="antiviral"
                        prp_name[21]="antiprotozoal"; prp_name[22]="antiparasitic"; prp_name[23]="antiplasmodial"; prp_name[24]="antitrypanosomic"; prp_name[25]="antileishmania"
                        prp_name[26]="insecticidal"; prp_name[27]="anticancer"; prp_name[28]="antitumor"; prp_name[29]="cell_line"; prp_name[30]="tissue"
                        prp_name[31]="cancer_type"; prp_name[32]="anticancer_activity"; prp_name[33]="anticancer_activity_test"; prp_name[34]="antiangiogenic"; prp_name[35]="toxic"
                        prp_name[36]="cytotoxic"; prp_name[37]="hemolytic"; prp_name[38]="hemolytic_activity"; prp_name[39]="hemolytic_activity_test"; prp_name[40]="RBC_source"
                        prp_name[41]="cell_cell"; prp_name[42]="hormone"; prp_name[43]="quorum_sensing"; prp_name[44]="immunomodulant"; prp_name[45]="antihypertensive"
                        prp_name[46]="drug_delivery"; prp_name[47]="cell_penetrating"; prp_name[48]="tumor_homing"; prp_name[49]="blood_brain"; prp_name[50]="antioxidant"
                        prp_name[51]="antiproliferative"; prp_name[52]="DSSP"; prp_name[53]="pdb"; prp_name[54]="experim_structure"; prp_name[55]="PMID"
                        prp_name[56]="taxonomy"; prp_name[57]="all_organisms"; prp_name[58]="activity_viral" ; prp_name[59]="activity_viral_test" ; 
			prp_name[60]="ribosomal";prp_name[61]="experimental";prp_name[62]="biofilm";
			prp_name[63]="virus_name";
			prp_name[64]="solubility" ; prp_name[65]="activity";prp_name[66]="activity_test";
                        prp_name[67]="sequence_length_range"; prp_name[68]="n_regions"; prp_name[69]="align_method"
                        prp_name[70]="level_agreement"; prp_name[71]="families_to_analyse"; prp_name[72]="min_seq_length";prp_name[73]="work_in_simplified_aa_space"; prp_name[74]="verbose"; 
                prp_name[75]="plot_graphs"; prp_name[76]="design_family_representative_peptide"; prp_name[77]="analyse_only_region_of_the_fam_father"; prp_name[78]="calculation_label";
                prp_name[79]="simplify_aminoacids";
                prp_option[1]=ID; prp_option[2]=sequence; prp_option[3]=name; prp_option[4]=source; prp_option[5]=Family
                        prp_option[6]=gene; prp_option[7]=stereo; prp_option[8]=N_terminus; prp_option[9]=C_terminus; prp_option[10]=PTM
                        prp_option[11]=cyclic; prp_option[12]=target; prp_option[13]=synthetic; prp_option[14]=antimicrobial; prp_option[15]=antibacterial
                        prp_option[16]=antigram_pos; prp_option[17]=antigram_neg; prp_option[18]=antifungal; prp_option[19]=antiyeast; prp_option[20]=antiviral
                        prp_option[21]=antiprotozoal; prp_option[22]=antiparasitic; prp_option[23]=antiplasmodial; prp_option[24]=antitrypanosomic; prp_option[25]=antileishmania
                        prp_option[26]=insecticidal; prp_option[27]=anticancer; prp_option[28]=antitumor; prp_option[29]=cell_line; prp_option[30]=tissue
                        prp_option[31]=cancer_type; prp_option[32]=anticancer_activity; prp_option[33]=anticancer_activity_test; prp_option[34]=antiangiogenic; prp_option[35]=toxic
                        prp_option[36]=cytotoxic; prp_option[37]=hemolytic; prp_option[38]=hemolytic_activity; prp_option[39]=hemolytic_activity_test; prp_option[40]=RBC_source
                        prp_option[41]=cell_cell; prp_option[42]=hormone; prp_option[43]=quorum_sensing; prp_option[44]=immunomodulant; prp_option[45]=antihypertensive
                        prp_option[46]=drug_delivery; prp_option[47]=cell_penetrating; prp_option[48]=tumor_homing; prp_option[49]=blood_brain; prp_option[50]=antioxidant
                        prp_option[51]=antiproliferative; prp_option[52]=DSSP; prp_option[53]=pdb; prp_option[54]=experim_structure; prp_option[55]=PMID
                        prp_option[56]=taxonomy; prp_option[57]=all_organisms; prp_option[58]=activity_viral ; prp_option[59]=activity_viral_test ;
			prp_option[60]=ribosomal;prp_option[61]=experimental;prp_option[62]=biofilm;
			prp_option[63]=virus_name;
                        prp_option[64]=solubility ; prp_option[65]=activity;prp_option[66]=activity_test;
                        prp_option[67]=sequence_length_range; prp_option[68]=n_regions; prp_option[69]=align_method
                        prp_option[70]=level_agreement; prp_option[71]=families_to_analyse; prp_option[72]=min_seq_length;prp_option[73]=work_in_simplified_aa_space; prp_option[74]=verbose;
                	prp_option[75]=plot_graphs; prp_option[76]=design_family_representative_peptide; prp_option[77]=analyse_only_region_of_the_fam_father; prp_option[78]=calculation_label;
                	prp_option[79]=simplify_aminoacids;

                p=0;while(p<length(prp_name)) {p=p+1
                        if (prp_option[p]=="" && p<=63) {prp_option[p]="i"}
                }

}
