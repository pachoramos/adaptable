function translate_sequence_in_simplified_aa_space(translated_seq,OUTPUT_INPUT,sequence_to_translate,\
l,ll) {
                type_aa["G"]="G"
                type_aa["A"]="A"
                type_aa["V"]="A"
                type_aa["I"]="A"
                type_aa["L"]="A"
                type_aa["P"]="P"
                type_aa["M"]="A"
                type_aa["W"]="F"
                type_aa["F"]="F"
                type_aa["C"]="C"
                type_aa["Y"]="F"
                type_aa["S"]="S"
                type_aa["T"]="S"
                type_aa["N"]="S"
                type_aa["Q"]="S"
                type_aa["D"]="D"
                type_aa["E"]="D"
                type_aa["R"]="K"
                type_aa["K"]="K"
                type_aa["H"]="F"

ll=length_nD(sequence_to_translate,1)
l=0 ; while(l<ll) {l=l+1
if(type_aa[sequence_to_translate[l]]=="") {type_aa[sequence_to_translate[l]]="Ⓜ"}
translated_seq[l]=type_aa[sequence_to_translate[l]]
}
}
