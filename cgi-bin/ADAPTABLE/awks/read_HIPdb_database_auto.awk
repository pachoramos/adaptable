function read_HIPdb_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,v) {
	limit=2000
		a=1;
	limit_=a+limit
		file= "DATABASES/HIPdb/HIPdbfiles/HIP"a
		while(a<limit_) {
			if (system("[ -f " file " ] ") ==0 ) {
#seq
				input_file=file
					field=read_database_line(input_file,">Sequence<",0,"COLOR='black'>","</b",1)
					gsub("_","",field)
					seq_a[a]=field
					print "Reading "file" for sequence "seq_a[a]""
					seq_a[a]=analyze_sequence(seq_a[a])
#ID
					field=read_database_line(input_file,"HIPid",0,"black'>","</b",1)
					if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]""field";"}
#name
				field=read_database_line(input_file,"Nomenclature",0,"black'>","</b",1)
				if(field=="-") {field=""}
				field=clean_string(field)
				if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[v]""field";"}}
#source
                                field=read_database_line(input_file,">Source</b>",0,"'>","</b>",1)
				if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                else {
                                field=clean_string(field)
                                if(field=="NA") {field=""}
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
                                }
#activity
				field1=read_database_line(input_file,"Inhibition",0,"\\/","&nbsp",1)
				gsub("&plusmn;","+-",field1)
				gsub ("<SUB>","",field1)
				field2=read_database_line(input_file,"Unit</FONT",3,"dontcut","</a",1)
				gsub("&mu;","u",field2)
				field=field1""field2
				if(field2!="NA") {field=field1""field2} else {field=""}
	       	                field=get_number_units(field,seq_a[a])
       		                field=analyze_organism(field,seq_a[a])
       		                field=clean_string(field)
       		                split(field,aa,"=")
				activity_viral_test_[seq_a[a]]=aa[1]
				antiviral_[seq_a[a]]="antiviral"
			        if(aa[1]!="") {	
			        	string="HIV("activity_viral_test_[seq_a[a]]"="aa[2]")"
			        	string_to_compare=escape_pattern(string)
					if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
					if(activity_viral_[seq_a[a]]!~string_to_compare) {if(string!="") {activity_viral_[seq_a[a]]=activity_viral_[seq_a[a]]""string";"}}
				}
				else {
					if(activity_viral_[seq_a[a]]!~/HIV/) {activity_viral_[seq_a[a]]="HIV;"}
					if(all_organisms_[seq_a[a]]!~/HIV/) {all_organisms_[seq_a[a]]="HIV;"}
				}
#target
				field=read_database_line(input_file,"Target",0,"TARGET\">","</a",1)
				field=clean_string(field)
				if(target_[seq_a[a]]!~field) {if(field!="") {target_[seq_a[a]]=target_[seq_a[a]]""field";"}}
#PMID
				field=read_database_line(input_file,"Pubmed ID</b></td><td><FONT  COLOR='black'><",0,">","</a",1)
					if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}
			
					# http://crdd.osdd.net/servers/hipdb/
					if(experimental_[seq_a[a]]=="") {experimental_[seq_a[a]]="experimental"}
				}
			close(input_file)
			a=a+1
			file= "DATABASES/HIPdb/HIPdbfiles/HIP"a
			amax=a
		}
return amax
}
