function read_DADP_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {

		 delete used
                        path="DATABASES/DADP/DADPfiles/DADP"
                        split(path,aa,"/")
                        file_list=aa[1]"/"aa[2]"/list"
                        name=1
                        ll=1; while(name!="") {ll=ll+1
                                name=read_database_line(file_list,"SP",0,"dontcut",",",ll)
                                        gsub("'","",name)
                                        file=path""name
                                        

                        gsub("\\];","",file)
                        if (system("[ -f " file " ] ") ==0 && used[name]=="") {
                        used[name]=1
                        input_file=file
#seq
				field=read_database_line(input_file,"<td>Sequence</td>",6,"</td><td>","</td><td",2)
				seq_a[a]=field
				print "Reading "file" for sequence "seq_a[a]""
				seq_a[a]=analyze_sequence(seq_a[a])
#ID
				field=name
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"DADP"field";" }
#name
                                field=read_database_line(input_file,"<td>Peptide_names</td>",1,"<td>","</td>",1)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"<td>Species</td>",1,"<i>","</i>",1)
                                field=clean_string(field)
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}

#activities
                field=read_database_line(input_file,"<td>Antimicrobial & other activities</td>",1,"<td>","</td>",1)
		gsub("_and_",",",field); amax=split(field,aa,",")
		aaa=0; while(aaa<amax) {aaa=aaa+1
                                read_database_classify_field(aa[aaa],",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
				}
#Taxonomy
                           field1=read_database_line(input_file,"<td>Suborder</td>",1,"<td>","</td>",1)
                           field2=read_database_line(input_file,"<td>Family</td>",1,"<td>","</td>",1)
                           field3=read_database_line(input_file,"<td>Genus</td>",1,"<td>","</td>",1)
			   field4=read_database_line(input_file,"<td>Species</td>",1,"<td><i>","</i></td>",1)

			string=field1","field2","field3","field4
			gsub("</i>","",string)
			gsub("<i>","",string)

			string=clean_string(string)
			string=tolower(string)
			if(taxonomy_[seq_a[a]]!~string) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""string";"}}
                                                                    
#all organisms		
			   field1=read_database_line(input_file,"<td>Sequence</td>",1,"<td>","&nbsp",2)
                           field2=read_database_line(input_file,"<td>Sequence</td>",6,"align=right_width=\"10\">","</td>",1)
                           field3=read_database_line(input_file,"<td>Sequence</td>",1,"(",")",1)
                        string1="RBC("field1"="field2""field3")"
                        gsub("&mu;M","µM",string1)
			gsub("&nbsp;","_",string1)
			gsub("=>",">",string1)
                           field4=read_database_line(input_file,"<td>MIC",0,"<i>","</i>",1)
			   field5=read_database_line(input_file,"<td>HC50",1,"<td>","_<i>",1)
			   field6=read_database_line(input_file,"<td>Sequence</td>",6,"align=right_width=\"10\">","</td>",2)
			   field7=read_database_line(input_file,"<td>MIC_<i>",0,"(",")",1)
			string2=field4"("field5"="field6""field7")"
			gsub("&mu;M","µM",string2)
			gsub("&nbsp;","_",string2)
			gsub("=>",">",string2)

			   field8=read_database_line(input_file,"<td>MIC",0,"<i>","</i>",2)
                           field9=read_database_line(input_file,"<td>HC50",1,"<td>","_<i>",1)
                           field10=read_database_line(input_file,"<td>Sequence</td>",6,"align=right_width=\"10\">","</td>",3)
                           field11=read_database_line(input_file,"<td>MIC_<i>",0,"(",")",1)
                        string3=field8"("field9"="field10""field11")"
                        gsub("&mu;M","µM",string3)
			gsub("&nbsp;","_",string3)
			gsub("=>",">",string3)

			string=string1","string2","string3"."
			string=":"string
			string=read_activity_description(string,":",seq_a[a],all_organisms_)
#Pubmed                 

                        field1=read_database_line(input_file,"MIC/HC50",1,"<td>","</td>",1)
                        if(PMID_[seq_a[a]]!~field1) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field1";"}}

         		 field1=read_database_line(input_file,"for__other_activities",1,"<td>","</td>",1)
                        if(PMID_[seq_a[a]]!~field1) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field1";"}}

                # http://split4.pmfst.hr/dadp/
                # "Manually curated", but they don't speak explicetly about experimental tests
#		experimental_[seq_a[a]]="experimental"
		}
       close(input_file)
	a=a+1
file="DATABASES/DBAASP/DBAASPfiles/DBAASP_"a
amax=a
}
return amax
}
