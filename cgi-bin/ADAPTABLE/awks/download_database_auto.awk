@include "awks/library.awk"
# the awk reads a sequence file and dowload the relative html pages from different databases.
BEGIN{
	limit=50000
		a=1;
	file="https://dbaasp.org/peptide-card?id="a
		while (system("[ -f " file " ] ") !=0 && a<limit) {
		        path="DATABASES/DBAASP/DBAASPfiles/"
		        split(path,aa,"/")
		        system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
			file_tmp="DATABASES/DBAASP/DBAASPfiles/DBAASP_"a
				if(system("[ -f " file_tmp " ] ") !=0 ) {
					print "******************************************************************************************",a,file,"DBAASP"
#       system("curl  -v -L -d \"query=lookup&lookup_type=target_group&format=json\" https://dbaasp.org/api/v"a" -o DATABASES/DBAASP/DBAASPfiles/DBAASP_"a"") 
						system("curl  -L -d \"query=peptide_card&peptide_id="a"&format=json\" https://dbaasp.org/api/v1 -o DATABASES/DBAASP/DBAASPfiles/DBAASP_"a"")
						system("curl  -L -d \"query=search&target_group_id="a"&synthesis_type=36&format=xml\" https://dbaasp.org/api/v1 -o DATABASES/DBAASP/DBAASPfiles/DBAASP2_"a"")
						system("curl  -L -d \"query=ranking_search&target_species_id="a"&activity_measure_id=72&operation=<=&activity=0.5&format=json\" https://dbaasp.org/api/v1 -o DATABASES/DBAASP/DBAASPfiles/DBAASP3_"a"")
						system("cat DATABASES/DBAASP/DBAASPfiles/DBAASP2_"a" >> DATABASES/DBAASP/DBAASPfiles/DBAASP_"a"")
						system("cat DATABASES/DBAASP/DBAASPfiles/DBAASP3_"a" >> DATABASES/DBAASP/DBAASPfiles/DBAASP_"a"")
						system("rm DATABASES/DBAASP/DBAASPfiles/DBAASP2_"a" DATABASES/DBAASP/DBAASPfiles/DBAASP3_"a" ")
				}
			a=a+1
				file="https://dbaasp.org/peptide-card?id="a
		}
	close(input_file)
                a=1;
        limit_=a+limit
                path="DATABASES/ADAM/ADAMfiles/ADAM"
                root_online="http://bioinformatics.cs.ntou.edu.tw/ADAM/adam_info.php?f=ADAM_"
                length_numb=4
                add_tag="yes"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" \""file_online"\"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
                close(input_file)
                a=1;
        limit_=a+limit
                path="DATABASES/YADAMP/YADAMPfiles/YADAMP"
                root_online="http://yadamp.unisa.it/showItem.aspx?yadampid="
                length_numb=3
                add_tag="no"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" \""file_online"\"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
                close(input_file)
               a=1000;
        limit_=a+limit
                path="DATABASES/CPP/CPPfiles/CPP"
                root_online="http://crdd.osdd.net/raghava/cppsite/display.php?details="
                length_numb=3
                add_tag="no"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" \""file_online"\"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
                close(input_file)
		a=10000;
	limit_=a+limit
	        path="DATABASES/satp/satpfiles/"
	        split(path,aa,"/")
	        system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
		file="http://crdd.osdd.net/raghava/satpdb/display_seq.php?details=satpdb"a
		while (system("[ -f " file " ] ") !=0 && a<limit_) { 
			file_tmp="DATABASES/satp/satpfiles/satpdb"a
				if(system("[ -f " file_tmp " ] ") !=0 ) {
					print "******************************************************************************************",a,file,"SAT"
						system("curl  -o DATABASES/satp/satpfiles/satpdb"a" http://crdd.osdd.net/raghava/satpdb/display_seq.php?details=satpdb"a"")
				}
			a=a+1
				file="http://crdd.osdd.net/raghava/satpdb/display_seq.php?details=satpdb"a
		}
	close(input_file)
		a=1000;
	limit_=a+limit
	        path="DATABASES/cancerPPD/cancerPPDfiles/"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
		file="http://crdd.osdd.net/raghava/cancerppd/display_sub.php?details="a
		while (system("[ -f " file " ] ") !=0 && a<limit_) { 
			file_tmp="DATABASES/cancerPPD/cancerPPDfiles/cancerPPD"a
				if(system("[ -f " file_tmp " ] ") !=0 ) {
					print "******************************************************************************************",a,file,"CANC"
						system("curl  -o DATABASES/cancerPPD/cancerPPDfiles/cancerPPD"a" http://crdd.osdd.net/raghava/cancerppd/display_sub.php?details="a"")
				}
			a=a+1
				file="http://crdd.osdd.net/raghava/cancerppd/display_sub.php?details="a
		}
	close(input_file)
		a=1000;
	limit_=a+limit
                path="DATABASES/Hemolytik/Hemolytikfiles/"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
		file="http://crdd.osdd.net/raghava/hemolytik/display.php?details="a
		while (system("[ -f " file " ] ") !=0 && a<limit_) { 
			file_tmp="DATABASES/Hemolytik/Hemolytikfiles/Hemo"a
				if(system("[ -f " file_tmp " ] ") !=0 ) {
					print "******************************************************************************************",a,file,"HEMO"
						system("curl  -o DATABASES/Hemolytik/Hemolytikfiles/Hemo"a" http://crdd.osdd.net/raghava/hemolytik/display.php?details="a"")
				}
			a=a+1
				file="http://crdd.osdd.net/raghava/hemolytik/display.php?details="a
		}
	close(input_file)
		a=1;
	limit_=a+limit
                path="DATABASES/Camp/Campfiles/"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
		file="http://www.camp.bicnirrh.res.in/seqDisp.php?id=CAMPSQ"a
		while (system("[ -f " file " ] ") !=0 && a<limit_) { 
			file_tmp="DATABASES/Camp/Campfiles/CAMPSQ"a
				if(system("[ -f " file_tmp " ] ") !=0 ) {
					print "******************************************************************************************",a,file,"CAMP"
						system("curl  -o DATABASES/Camp/Campfiles/CAMPSQ"a"  http://www.camp.bicnirrh.res.in/seqDisp.php?id=CAMPSQ"a"")
				}
			a=a+1
				file="http://www.camp.bicnirrh.res.in/seqDisp.php?id=CAMPSQ"a
		}
	close(input_file)
		a=1;
	limit_=a+limit
                path="DATABASES/HIPdb/HIPdbfiles/"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
		file="http://crdd.osdd.net/servers/hipdb/record.php?details=HIP"a
		while (system("[ -f " file " ] ") !=0 && a<limit_) { 
			file_tmp="DATABASES/HIPdb/HIPdbfiles/HIP"a
				if(system("[ -f " file_tmp " ] ") !=0 ) {
					print "******************************************************************************************",a,file,"HIP"
						system("curl  -o DATABASES/HIPdb/HIPdbfiles/HIP"a"  http://crdd.osdd.net/servers/hipdb/record.php?details=HIP"a"")
				}
			a=a+1
				file="http://crdd.osdd.net/servers/hipdb/record.php?details=HIP"a
		}
	close(input_file)
		a=1;
	limit_=a+limit
                path="DATABASES/DRAMP/DRAMPfiles/"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
		file="http://dramp.cpu-bioinfor.org/browse/All_Information.php?id=DRAMP0000"a
		while (system("[ -f " file " ] ") !=0 && a<limit_) { 
				tag="" ; imax=5-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}
			file_tmp="DATABASES/DRAMP/DRAMPfiles/DRAMP"tag""a
				if(system("[ -f " file_tmp " ] ") !=0 ) {
				print "******************************************************************************************",a,file,"DRAMP"
					system("curl  -o DATABASES/DRAMP/DRAMPfiles/DRAMP"tag""a" http://dramp.cpu-bioinfor.org/browse/All_Information.php?id=DRAMP"tag""a"")
				}
			a=a+1
				file="http://dramp.cpu-bioinfor.org/browse/All_Information.php?id=DRAMP"tag""a
		}
	close(input_file)
		a=1;
	limit_=a+limit
                path="DATABASES/PhytAMP/PhytAMPfiles/"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
		file="http://phytamp.hammamilab.org/PHYT0000"a
		while (system("[ -f " file " ] ") !=0 && a<limit_) { 
				tag="" ; imax=5-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}
			file_tmp="DATABASES/PhytAMP/PhytAMPfiles/PHYT"tag""a
				if(system("[ -f " file_tmp " ] ") !=0 ) {
				print "******************************************************************************************",a,file,"PHYT"
					system("curl  -o DATABASES/PhytAMP/PhytAMPfiles/PHYT"tag""a" http://phytamp.hammamilab.org/PHYT"tag""a"")
					system("iconv -f windows-1252 -t utf-8 "file_tmp" > tmp")
                                                        system("mv tmp "file_tmp"")
				}
			a=a+1
				file="http://phytamp.hammamilab.org/PHYT"tag""a
		}
	close(input_file)
                a=1;
        limit_=a+limit
		path="DATABASES/BACTIBASE/BACTIBASEfiles/BAC"
		root_online="http://bactibase.hammamilab.org/BAC"
		length_numb=3
		add_tag="yes"
		split(path,aa,"/")
		system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
			file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" "file_online"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
        close(input_file)
                a=1;
        limit_=a+limit
                path="DATABASES/MilkAMPs/MilkAMPsfiles/MilK"
                root_online="http://milkampdb.org/"
                length_numb=4
                add_tag="yes"
		label[1]="ALA";label[2]="CAA";label[3]="CAB";label[4]="BLA";label[5]="CAK";label[6]="LFH";label[7]="LAP";label[8]="WAP";label[9]="GWP";tmax=9
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
		t=0; while(t<tmax) {t=t+1; a=1
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""label[t]""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""label[t]""tag""a
                        file_online=root_online""label[t]""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" "file_online"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
        close(input_file)
		}
                a=1;
        limit_=a+limit
                path="DATABASES/APD/APDfiles/APD"
                root_online="http://aps.unmc.edu/AP/database/query_output.php?ID="
                length_numb=3
                add_tag="yes"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
		tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
		file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" "file_online"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
        close(input_file)

                a=1;
        limit_=a+limit
                path="DATABASES/Defensins/Defensinsfiles/Defe"
                root_online="http://defensins.bii.a-star.edu.sg/pops/pop_proteinDetails.php?id="
                length_numb=3
                add_tag="no"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" "file_online"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
        close(input_file)
                a=1;
        limit_=a+limit
                path="DATABASES/BAAMPS/BAAMPSfiles/BAAMPS"
                root_online="http://www.baamps.it/browse?task=peptide.display&ID="
                length_numb=3
                add_tag="no"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" \""file_online"\"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
        close(input_file)
                a=1;
        limit_=a+limit
                path="DATABASES/InverPep/InverPepfiles/InverPep"
                root_online="http://ciencias.medellin.unal.edu.co/gruposdeinvestigacion/prospeccionydisenobiomoleculas/InverPep/public/Peptido/see/"
                length_numb=3
                add_tag="no"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                #        system("lynx -dump -list_inline "file_online" > "file_tmp"")
				system("curl  -o "file_tmp" "file_online"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
        close(input_file)
                
		a=1;
        limit_=a+limit
                path="DATABASES/AVP/AVPfiles/AVP"
                root_online="http://crdd.osdd.net/servers/avpdb/record.php?details=AVP"
                length_numb=4
                add_tag="yes"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" "file_online"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
        close(input_file)
                a=1000;
        limit_=a+limit
                path="DATABASES/ParaPep/ParaPepfiles/ParaPep"
                root_online="http://crdd.osdd.net/raghava/parapep/display_sub.php?details="
                length_numb=4
                add_tag="no"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" "file_online"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
        close(input_file)
                a=1;
        limit_=a+limit
                path="DATABASES/Conoserver/Conoserverfiles/Conoserver"
                root_online="http://www.conoserver.org/index.php?page=card&table=protein&id="
                length_numb=4
                add_tag="no"
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""tag""a
                        file_online=root_online""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                        system("curl  -o "file_tmp" \""file_online"\"")
                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
		close(input_file)
			a=1
			path="DATABASES/ANTISTAPHY/ANTISTAPHYfiles/ANTISTAPHY"
			file_names="http://www.antistaphybase.com/gen_data_saureus.php"
			split(path,aa,"/")
			system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
			file_list=aa[1]"/"aa[2]"/list"
			system("curl  -o "file_list" "file_names"")
			name=1
			ll=1; while(name!="") {ll=ll+1
				name=read_database_line(file_list,"trouver",0,"(",")",ll)
					gsub("_","+",name); gsub("'","",name)
					file_online="http://www.antistaphybase.com/get_saureus_name.php?pn="name
					file_tmp=path""name
					if(system("[ -f " file_tmp " ] ") !=0 ) {
						print "******************************************************************************************",a,file_tmp,file_online,aa[3]
							system("curl  -o "file_tmp" "file_online"")
					}
				a=a+1
			}
                        a=1
                        path="DATABASES/Peptaibol/Peptaibolfiles/Peptaibol"
                        file_names="http://peptaibol.cryst.bbk.ac.uk/peptaibol_database_justnames.htm"
                        split(path,aa,"/")
                        system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                        file_list=aa[1]"/"aa[2]"/list"
                        system("curl  -o "file_list" "file_names"")
                        name=1
                        ll=1; while(name!="") {ll=ll+1
                                name=read_database_line(file_list,"singlerecords",0,">","<",ll)
                                        file_online="http://peptaibol.cryst.bbk.ac.uk/singlerecords/"name".html"
                                        file_tmp=path""name
                                        if(system("[ -f " file_tmp " ] ") !=0 ) {
                                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                                        system("curl  -o "file_tmp" "file_online"")
                                        }
                                a=a+1
                        }

                        a=1
                        path="DATABASES/DADP/DADPfiles/DADP"
                        file_names="http://split4.pmfst.hr/dadp/?a=list"
                        split(path,aa,"/")
                        system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                        file_list=aa[1]"/"aa[2]"/list"
                        system("curl  -o "file_list" "file_names"")
                        name=1
                        ll=1; while(name!="") {ll=ll+1
                                name=read_database_line(file_list,"SP",0,"dontcut",",",ll)
                                        gsub("'","",name)
                                        file_online="http://split4.pmfst.hr/dadp/?a=kartica&id=SP"name
                                        file_tmp=path""name
                                        if(system("[ -f " file_tmp " ] ") !=0 ) {
                                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                                        system("curl  -o "file_tmp" \""file_online"\"")
                                        }
                                a=a+1
                        }
                        a=1
                        path="DATABASES/LAMP/LAMPfiles/LAMP"
                        file_names="http://biotechlab.fudan.edu.cn/database/lamp/db/lamp.fasta"
                        split(path,aa,"/")
                        system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                        file_list=aa[1]"/"aa[2]"/list"
                        system("curl  -o "file_list" "file_names"")
			 while ( getline < file_list >0 ) {
				 if(substr($0,1,1)==">") {split($0,aa,">"); split(aa[2],bb,"|"); name=bb[1]
                                        file_online="http://biotechlab.fudan.edu.cn/database/lamp/detail.php?id="name
                                        file_tmp=path""name
                                        if(system("[ -f " file_tmp " ] ") !=0 ) {
                                                print "******************************************************************************************",a,file_tmp,file_online,aa[3]
                                                        system("curl  -o "file_tmp" \""file_online"\"")
                                        }
				}
                                a=a+1
                        }
}
