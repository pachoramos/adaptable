@include "awks/library.awk"
# the awk reads a sequence file and dowload the relative html pages from different databases.
BEGIN{
        limit=11000
        a=1;
        limit_=a+limit
        path="DATABASES/ADAM/ADAMfiles/ADAM"
        # Try to use one server (there are multiple) that is more reliable
        root_online="https://bioinformatics.cs.ntou.edu.tw/ADAM/adam_info.php?f=ADAM_"
        #root_online="http://140.121.197.185/ADAM/adam_info.php?f=ADAM_"
        length_numb=4
        add_tag="yes"
        split(path,aa,"/")
        system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
        tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
        file_online=root_online""tag""a
        while (a<limit_) {
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_tmp=path""tag""a
                file_online=root_online""tag""a
                if(system("[ -f " file_tmp " ] ") !=0 ) {
                        print "Downloading...",a,file_tmp,file_online,aa[3]
                        # -k to ignore wrong certificate
                        system("curl -k -o "file_tmp" \""file_online"\"")
                }
                a=a+1
                file_online=root_online""tag""a
        }
        close(input_file)
}

