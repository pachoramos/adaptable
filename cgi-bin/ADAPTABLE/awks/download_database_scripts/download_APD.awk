@include "awks/library.awk"
# the awk reads a sequence file and dowload the relative html pages from different databases.
# Example: curl -k -d "ID=00029" -X POST https://aps.unmc.edu/database/peptide
BEGIN{
        limit=10000
        a=1;
        limit_=a+limit
        path="DATABASES/APD/APDfiles/APD"
        length_numb=5
        add_tag="yes"
        
        split(path,aa,"/")
        system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
        tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
        while (a<limit_) {
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_tmp=path""tag""a
                if(system("[ -f " file_tmp " ] ") !=0 ) {
                        print "Downloading...",a,file_tmp,aa[3]
                        system("curl -k -d 'ID="tag""a"' -X POST https://aps.unmc.edu/database/peptide -o "file_tmp"")
                }
        a=a+1
        }
        close(input_file)
}

