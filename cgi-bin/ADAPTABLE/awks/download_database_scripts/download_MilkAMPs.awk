@include "awks/library.awk"
# the awk reads a sequence file and dowload the relative html pages from different databases.
BEGIN{
        limit=5000
                a=1;
        limit_=a+limit
                path="DATABASES/MilkAMPs/MilkAMPsfiles/MilK"
                root_online="http://milkampdb.org/"
                length_numb=4
                add_tag="yes"
                label[1]="ALA";label[2]="CAA";label[3]="CAB";label[4]="BLA";label[5]="CAK";label[6]="LFH";label[7]="LAP";label[8]="WAP";label[9]="GWP";tmax=9
                split(path,aa,"/")
                system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
                t=0; while(t<tmax) {t=t+1; a=1
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                file_online=root_online""label[t]""tag""a
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file_tmp=path""label[t]""tag""a
                        file_online=root_online""label[t]""tag""a
                                if(system("[ -f " file_tmp " ] ") !=0 ) {
                                print "Downloading...",a,file_tmp,file_online,aa[3]
                                system("curl  -o "file_tmp" "file_online"")
                                system("iconv -c -f windows-1252 -t utf-8 "file_tmp" > tmp")
                                system("mv -f tmp "file_tmp"")

                                }
                        a=a+1
                                file_online=root_online""tag""a
                }
        close(input_file)
                }
}

