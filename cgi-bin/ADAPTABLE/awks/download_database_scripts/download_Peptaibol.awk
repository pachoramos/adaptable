@include "awks/library.awk"
# the awk reads a sequence file and dowload the relative html pages from different databases.
BEGIN{
        limit=50000
        a=1
        path="DATABASES/Peptaibol/Peptaibolfiles/Peptaibol"
        file_names="https://peptaibol.cryst.bbk.ac.uk/peptaibol_database_justnames.htm"
        split(path,aa,"/")
        system(" if [ ! -d "aa[1]"/"aa[2]"/"aa[3]" ] ; then mkdir -p "aa[1]"/"aa[2]"/"aa[3]" ; fi ")
        file_list=aa[1]"/"aa[2]"/list"
        print "****************************",file_names
        system("curl -k -o "file_list" "file_names"")
        name=1
        ll=1; while(name!="") {ll=ll+1
                name=read_database_line(file_list,"singlerecords",0,">","<",ll)
                gsub("'","",name)
                file_online="https://peptaibol.cryst.bbk.ac.uk/singlerecords/"name".html"
                file_tmp=path""name
                if(system("[ -f " file_tmp " ] ") !=0 ) {
                        print "*******************************",a,file_tmp,file_online,aa[3]
                        system("curl -k -o "file_tmp" "file_online"")
                }
                a=a+1
        }
}
