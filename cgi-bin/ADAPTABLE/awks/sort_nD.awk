function is_numeric_array(ARRAY_tmp, last_index) {
        for (i = 1; i <= last_index; i++) {
                if (ARRAY_tmp[i] != "" && ARRAY_tmp[i] + 0 == 0 && ARRAY_tmp[i] !~ /^0+$/) {
                        return 0  # Non-numeric value found
                }
        }
        return 1 # All values are numeric
}

function sort_nD(ARRAY,conv_index,sense,sort_matrix,x,y,z,f,g,h,firstx,lastx,firsty,lasty,firstz,lastz,firstf,lastf,firstg,lastg,firsth,lasth,\
end) {

delete  ARRAY_tmp
delete  ARRAY_tmp_
delete  conv_index
#delete  ARRAY
# Usage: call the function by specifying with "v" the column that has to be varied to find the mamimum e.g. sort_array_3D(a,">",1,"v",7,4,3) sort in ascending order elements a[1,i,7,4,3] 
# sense is ">" for ascending and "<" for discending order; use sense="@>" or "@<" for absolute values
        if(lastx=="") {ELEMENTS=length_nD(ARRAY,1)}
        if((x=="v" || x=="") && lastx=="") {ELEMENTS=length_nD(ARRAY,1)}
        if(y=="v" && lasty=="") {ELEMENTS=length_nD(ARRAY,2)}
        if(z=="v" && lastz=="") {ELEMENTS=length_nD(ARRAY,3)}
        if(f=="v" && lastf=="") {ELEMENTS=length_nD(ARRAY,4)}
        if(g=="v" && lastg=="") {ELEMENTS=length_nD(ARRAY,5)}
        if(h=="v" && lasth=="") {ELEMENTS=length_nD(ARRAY,6)}
        if(firstx=="") {firstx=1}
        if(firsty=="") {firsty=1}
        if(firstz=="") {firstz=1}
        if(firstf=="") {firstf=1}
        if(firstg=="") {firstg=1}
        if(firsth=="") {firsth=1}
        if(x=="") {x="v"}
        if(x=="v") {i0=firstx; if(lastx!="") {ELEMENTS=lastx-firstx+1}}
        if(y=="v") {i0=firsty ; if(lasty!="") {ELEMENTS=lasty-firsty+1}}
        if(z=="v") {i0=firstz; if(lastz!="") {ELEMENTS=lastz-firstz+1}}
        if(f=="v") {i0=firstf; if(lastf!="") {ELEMENTS=lastf-firstf+1}}
        if(g=="v") {i0=firstg; if(lastg!="") {ELEMENTS=lastg-firstg+1}}
        if(h=="v") {i0=firsth; if(lasth!="") {ELEMENTS=lasth-firsth+1}}
        if(indexvalue=="") {indexvalue="value"}
        last_index=i0+ELEMENTS-1
        if(y=="") {i=i0-1;while(i<last_index) {i=i+1; ARRAY_tmp[i]=ARRAY[i]}}
        if(y!="" && z=="") {i=i0-1;while(i<last_index) {i=i+1; if(x=="v") {ARRAY_tmp[i]=ARRAY[i,y]} else {ARRAY_tmp[i]=ARRAY[x,i]}}}
        if(z!="" && f=="") {i=i0-1;while(i<last_index) {i=i+1; if (x=="v") {ARRAY_tmp[i]=ARRAY[i,y,z]} else {if (y=="v") {ARRAY_tmp[i]=ARRAY[x,i,z]} else {ARRAY_tmp[i]=ARRAY[x,y,i]}}}}
        if(f!="") {i=i0-1;while(i<last_index) {i=i+1;if (x=="v") {ARRAY_tmp[i]=ARRAY[i,y,z,f]} else {if (y=="v") {ARRAY_tmp[i]=ARRAY[x,i,z,f]} else {if(z=="v") {ARRAY_tmp[i]=ARRAY[x,y,i,f]} else {ARRAY_tmp[i]=ARRAY[x,y,z,i]}}}}}
        if(g!="") {i=i0-1;while(i<last_index) {i=i+1;if (x=="v") {ARRAY_tmp[i]=ARRAY[i,y,z,f,g]} else {if (y=="v") {ARRAY_tmp[i]=ARRAY[x,i,z,f,g]} else {if(z=="v") {ARRAY_tmp[i]=ARRAY[x,y,i,f,g]} else {if(f=="v") {ARRAY_tmp[i]=ARRAY[x,y,z,i,g]} else {ARRAY_tmp[i]=ARRAY[x,y,z,f,i]}}}}}}
        if(h!="") {i=i0-1;while(i<last_index) {i=i+1;if (x=="v") {ARRAY_tmp[i]=ARRAY[i,y,z,f,g,h]} else {if (y=="v") {ARRAY_tmp[i]=ARRAY[x,i,z,f,g,h]} else {if(z=="v") {ARRAY_tmp[i]=ARRAY[x,y,i,f,g,h]} else {if(f=="v") {ARRAY_tmp[i]=ARRAY[x,y,z,i,g,h]} else {if(g=="v") {ARRAY_tmp[i]=ARRAY[x,y,z,f,g,i]} else {ARRAY_tmp[i]=ARRAY[x,y,z,f,i,h]}}}}}}}

	if (sense=="") {sense="<"}
 	if (sense~/@/) {i=i0-1; while(i<last_index) {i=i+1 ; if(ARRAY_tmp[i]<0) {ARRAY_tmp[i]=-ARRAY_tmp[i]}}}

 	# Determine if the array is numeric or text
 	if (is_numeric_array(ARRAY_tmp, last_index)) {
 	        numeric = 1
        } else {
                numeric = 0
        }

	i=i0-1
	while(i<last_index) {i=i+1
		k=i0-1;
		# FIXME: potential bug here but, for now, it seems that we don't hit it. As most ADAPTABLE scripts
		# only use ">" sense, we are not affected but, if in the future strange behavior is observed for
		# numerical values and "<" sense, please refer to:
		# https://unix.stackexchange.com/questions/584421/inf-in-awk-not-working-the-way-inf-does
		# https://chat.stackexchange.com/rooms/107742/discussion-on-answer-by-stephen-kitt-inf-in-awk-not-working-the-way-inf-doe
		# https://www.gnu.org/software/gawk/manual/html_node/POSIX-Floating-Point-Problems.html#POSIX-Floating-Point-Problems
		#
		# This ends up setting mm = +inf and mm = -inf for numerical
		#
		# Initialize mm based on sorting sense and data type
		if(sense~/</) {
		        mm = (numeric) ? 1e100000000 : "zzzzzzzzzz"  # Very high value for descending order (numerical : lexical)
                }
                if(sense~/>/) {
                        mm = (numeric) ? -1e100000000 : ""	# Very low value for ascending order
                }
                
		# print mm
		ii=0 ;
		while(k<last_index) {k=k+1
			if(sense~/</)  {if ((numeric && ARRAY_tmp[k] < mm) || (!numeric && ARRAY_tmp[k] < mm && ARRAY_tmp[k] != "")) {mm=ARRAY_tmp[k];ii=k}}
			if(sense~/>/)  {if ((numeric && ARRAY_tmp[k] > mm) || (!numeric && ARRAY_tmp[k] > mm && ARRAY_tmp[k] != "")) {mm=ARRAY_tmp[k];ii=k}}
		}
		
		# Store the sorted value and update the index mapping
		ARRAY_tmp_[i]=ARRAY_tmp[ii]; conv_index[i]=ii; 
		
		# Replace the sorted value in ARRAY_tmp with an appropriate value
		if(sense~/</) {ARRAY_tmp[ii] = (numeric) ? 1e100000000 : "zzzzzzzzzz"}  # Very high value for descending order
		if(sense~/>/) {ARRAY_tmp[ii] = (numeric) ? -1e100000000 : ""}           # Very low value for ascending order
	}

if(sort_matrix=="sort_matrix") {
	if(y=="") {i=i0-1;while(i<last_index) {i=i+1; ARRAY[i]=ARRAY_tmp_[i]}}
	if(y!="" && z=="") {i=i0-1;while(i<last_index) {i=i+1; if(x=="v") {ARRAY[i,y]=ARRAY_tmp_[i]} else {ARRAY[x,i]=ARRAY_tmp_[i]}}} 
	if(z!="" && f=="") {i=i0-1;while(i<last_index) {i=i+1;  if (x=="v") {ARRAY[i,y,z]=ARRAY_tmp_[i]} else {if (y=="v") {ARRAY[x,i,z]=ARRAY_tmp_[i]} else {ARRAY[x,y,i]=ARRAY_tmp_[i]}}}} 
	if(f!="") {i=i0-1;while(i<last_index) {i=i+1; ;if (x=="v") {ARRAY[i,y,z,f]=ARRAY_tmp_[i]} else {if (y=="v") {ARRAY[x,i,z,f]=ARRAY_tmp_[i]} else {if(z=="v") {ARRAY[x,y,i,f]=ARRAY_tmp_[i]} else {ARRAY[x,y,z,i]=ARRAY_tmp_[i]}}}}}
        if(g!="") {i=i0-1;while(i<last_index) {i=i+1;  if (x=="v") {ARRAY[i,y,z,f,g]=ARRAY_tmp_[i]} else {if (y=="v") {ARRAY[x,i,z,f,g]=ARRAY_tmp_[i]} else {if(z=="v") {ARRAY[x,y,i,f,g]=ARRAY_tmp_[i]} else {if(f=="v") {ARRAY[x,y,z,i,g]=ARRAY_tmp_[i]} else {ARRAY[x,y,z,f,i]=ARRAY_tmp_[i]}}}}}}
        if(h!="") {i=i0-1;while(i<last_index) {i=i+1;  if (x=="v") {ARRAY[i,y,z,f,g,h]=ARRAY_tmp_[i]} else {if (y=="v") {ARRAY[x,i,z,f,g,h]=ARRAY_tmp_[i]} else {if(z=="v") {ARRAY[x,y,i,f,g,h]=ARRAY_tmp_[i]} else {if(f=="v") {ARRAY[x,y,z,i,g,h]=ARRAY_tmp_[i]} else {if(g=="v") { ARRAY[x,y,z,f,g,i]=ARRAY_tmp_[i]} else {ARRAY[x,y,z,f,i,h]=ARRAY_tmp_[i]}}}}}}}
}
delete ARRAY_tmp
delete ARRAY_tmp_
}
