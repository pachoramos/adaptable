function clean_string(field,n_,n,n_open,n_closed,head,tail,a,field2,l)
{
	split(field,a,"]");field2="";n=0;while(n<length(a)) {n=n+1; field2=field2""a[n]}; field=field2
	split(field,a,"[");field2="";n=0;while(n<length(a)) {n=n+1; field2=field2""a[n]}; field=field2
	gsub("/","",field)
#	gsub("\\","",field)
	gsub("<s>","",field)
	gsub("</s>","",field)
	gsub("Unknown","",field)
	# DRAMP typos
	gsub("Unknow","",field)
	gsub("unknown","",field)
	gsub("unknow","",field)
	gsub("Unidentified","",field)
	gsub("Not_Available","",field)
	gsub("None","",field)
	gsub("unclassified","",field)
	gsub("Unidentified","",field)
	gsub("syntheitc","synthetic",field)
	gsub("neagtive","negative",field)
	gsub("positve","positive",field)
	gsub("poaitive","positive",field)
	gsub("<a>","",field)
        gsub("<i>","",field)
        gsub("</i>","",field)
        gsub("<B>","",field)
        gsub("</B>","",field)
        gsub("<I>","",field)
        gsub("</I>","",field)
        gsub("Unknow","",field)
	gsub("None","",field)
        gsub("<b>","",field)
        gsub("</b>","",field)
        gsub("<sub>","",field)
        gsub("</sub>","",field)
	gsub("<li>","",field)
        gsub("</li>","",field)
        gsub("Contains:","",field)
	gsub(" ","",field)
	gsub("	","",field)
	gsub("__","_",field)
	gsub("No_MICs_found_on_DRAMP_database","",field)
	gsub("Problely","Probably",field)
	gsub("onjuagtion","onjugation",field)
	gsub("onjugated","onjugation",field)
	gsub("Labelled_with_","Conjugation_with_",field)
	gsub("fluoresceine","fluorescein",field)
	gsub("Â","",field)
	gsub("Î±","",field)
	# Unify with DBs that are forcing lowercase for this concrete words
	gsub("Stearylation","stearylation",field)
	gsub("Sulfhydrylation","sulfhydrylation",field)
	gsub("Lauroylation","lauroylation",field)
	gsub("Biotinylation","biotinylation",field)
	gsub("Acetylation","acetylation",field)
	gsub("Amidation","amidation",field)
	gsub("Disulfide","disulfide",field)
	gsub("yclisation","cyclization",field)
	gsub("Cyclization","cyclization",field)
	gsub("_This_is_a_cyclic_peptide.","cyclization",field)
	gsub("Lantibiotic","lantibiotic",field)
	gsub("Glycosylation","glycosylation",field)
	gsub("Cycliclization","cyclization",field)
	gsub("Cyclization","cyclization",field)
	gsub("termianal","terminal",field)
	gsub("Lipid_Bilayer\",","Lipid_Bilayer",field)
	gsub("Membrane_Protein\",","Membrane_Protein",field)
	gsub("Cytoplasmic_Protein\",","Cytoplasmic_Protein",field)
	gsub("cellline","cell_line",field)
	gsub("membrance","membrane",field)
	# From CancerPPD3175
	gsub("= =","=",field)
	gsub(/^_/,"",field)
	gsub(/^ /,"",field)
	gsub(",_",",",field)
	gsub("_=_","=",field)
	# From LAMP entries
	gsub("<span>","",field)
	gsub("<span_class=state_t>","",field)
	# From CAMPS
	# This character will break less command, but it works with cat or more
	gsub("","-",field)
	# Remove starting or ending spaces (or ending full stops)
	no_starting_spaces=0
	while(no_starting_spaces!=1) {
		if(substr(field,1,1)!="_") {no_starting_spaces=1} else {field=substr(field,2,length(field))}
			}
	no_ending_spaces=0
        while(no_ending_spaces!=1) {
        	# FIXME: Don't scape two times, \\. doesn't work as expected
                if(substr(field,length(field),1)!="_" && substr(field,length(field),1)!=".") {no_ending_spaces=1} else {field=substr(field,1,length(field)-1)}
                        }

	n_=split(field,a,"");
	n=0;n_open=0;n_closed=0;head="";tail=""
		while(n<n_) {n=n+1; if (a[n]=="(") {n_open=n_open+1} ; if (a[n]==")") {n_closed=n_closed+1}} ;
	if(n_open>n_closed) {n=0;while(n<(n_open-n_closed)) {n=n+1;tail=tail")"}};
	if(n_closed>n_open) {n=0;while(n<(n_closed-n_open)) {n=n+1;head=head"("}};
	if(n_open!=0 || n_closed!=0) {field="("head""field""tail")" ; if(field=="()") {field=""}}

	# Try to close non closed parenthesis and other shit
	n=0;n_open2=0;n_closed2=0;head="";tail=""
	while(n<n_) {n=n+1; if (a[n]=="[") {n_open2=n_open2+1} ; if (a[n]=="]") {n_closed2=n_closed2+1}} ;
	if(n_open2>n_closed2) {n=0;while(n<(n_open2-n_closed2)) {n=n+1;tail=tail")"}};
        if(n_closed2>n_open2) {n=0;while(n<(n_closed2-n_open2)) {n=n+1;head=head"("}};
        if(n_open2!=0 || n_closed2!=0) {field="("head""field""tail")" ; if(field=="()") {field=""}}

	string_no_p=""
	all_letters=split(field,ab,""); l=0; while(l<all_letters) {l=l+1; if(ab[l]!=")" && ab[l]!="(") {string_no_p=string_no_p""ab[l]}}
		field=string_no_p
	return field
}
