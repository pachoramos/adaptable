function correct_sequence(sequence) {

if (sequence=="KK_β2,2WKK") {sequence="KK"""translate_modif["2,2-di(naphthalen-2-ylmethyl)-3-amino-propionicacid"]"""WKK"
	print "2,2-di(naphthalen-2-ylmethyl)-3-amino-propionic acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="WI-D-pipecolinyl-L-2-amino-8-oxodecanoyl") {sequence="WID"""translate_modif["pipecolinyl"]"""L"
        print "pipecolinyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="KW_β2,2KK") {sequence="KW"""translate_modif["(2,2-di(4-[trifluoromethyl]benzyl)-3-amino-propionic acid)"]"""KK"
        print "2,2-di(4-[trifluoromethyl]benzyl)-3-amino-propionic acid" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="KKWβ2,2WKK") {sequence="KKW"""translate_modif["(2,2-di(4-[trifluoromethyl]benzyl)-3-amino-propionic acid)"]"""WKK"
	print "2,2-di(4-[trifluoromethyl]benzyl)-3-amino-propionic acid" >> "tmp_files/correct_sequence_aa"
	}

if (sequence=="nitro-FYAGAVVNDL") {sequence="FYAGAVVNDL"
        }

if (sequence=="GWLDVAKKIGKAAFNVAKNFL-spacer-LFNKAVNFAAKGIKKAVDLWG") {sequence="GWLDVAKKIGKAAFNVAKNFLLFNKAVNFAAKGIKKAVDLWG"
        }


if (sequence=="SLLGRMKG(beta-A)C") {sequence="SLLGRMKG"""translate_modif["beta-alanine"]"""C"
        print "beta-alanine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="NH2-EDGGSFMC(acm)LWC(acm)GEVHG-CONH2") {sequence="EDGGSFMCXLWCXGEVHGCO"
        }

if (sequence=="cinnamoyl-KRR") {sequence=""translate_modif["cinnamoyl"]"""KRR"
        print "cinnamoyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="MHRSLLGRMKGAG(beta-A)GC") {sequence="MHRSLLGRMKGAGA"translate_modif["beta-alanine"]"GC"
        }

if (sequence=="A-AnthrylAla-YKKFKKKLLKSLKRLG") {sequence="A"translate_modif["Anthryl"]"AAYKKFKKKLLKSLKRLG"
        print "Anthryl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="Structure_Given") {sequence=""
	}

if (sequence=="Not_Available") {sequence=""
        }

if (sequence=="Boc-Asp-CH2F") {sequence=translate_modif["Boc"]"D"translate_modif["methylphenylalanine"]
        print "Boc" >> "tmp_files/correct_sequence_aa"
        print "methylphenylalanine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="(TyrOMe)2-HPhe") {sequence=translate_modif["methyltyrosine"]"HF"
        print "methyltyrosine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="trans-epoxysuccinyl-L-leucylamido-(4-guanadino)butane") {sequence=translate_modif["transepoxysuccinyl"]"L"translate_modif["leucylamido-(4-guanadino)butane"]
        print "transepoxysuccinyl" >> "tmp_files/correct_sequence_aa"
        print "leucylamido-(4-guanadino)butane" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="(1R,7S,8S,11R)-7-Hydroxy-11-(hydroxymethyl)-15-methyl-12,13-dithia-9,15-diazatetracyclo[9.2.2.01,9.03,8]pentadeca-3,5-diene-10,14-dione") {sequence=""
        }

if (sequence=="N-Acetyl-S-{[(2R,3S,4R)-3-hydroxy-2-(1-hydroxy-2-methylpropyl)-4-methyl-5-oxo-2-pyrrolidinyl]carbonyl}-L-cysteine") {sequence=""
        }

if (sequence=="[(2S)-1-[[(2S)-1-[[(E,2S)-5-[(2S)-3-methoxy-2-methyl-5-oxo-2H-pyrrol-1-yl]-5-oxopent-3-en-2-yl]amino]-4-methyl-1-oxopentan-2-yl]amino]-4-methyl-1-oxopentan-2-yl]-(2S,3S)-2-(dimethylamino)-3-methylpentanoate") {sequence=""
        }

if (sequence=="N-Acetyl-N-methyl-L-isoleucyl-L-isoleucyl-N-{(2S)-4-methyl-1-[(2R)-2-methyl-2-oxiranyl]-1-oxo-2-pentanyl}-L-threoninamide") {sequence=translate_modif["NacetylNmethylisoleucine"]"L"translate_modif["N2S4methyl12R2methyl2oxiranyl1oxo2pentanylLthreoninamide"]
        print "N2S4methyl12R2methyl2oxiranyl1oxo2pentanylLthreoninamide" >> "tmp_files/correct_sequence_aa"
        print "NacetylNmethylisoleucine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="N-(2,4-Dimethyl-9-decynoyl)-N-methyl-L-phenylalanyl-L-alanyl-N-methyl-L-alanyl-Nalpha,O-dimethyl-L-tyrosinamid") {sequence=translate_modif["N2,4Dimethyl9decynoylNmethylphenylalanine"]""A""translate_modif["methylalanine"]""translate_modif["Nalpha,OdimethylLtyrosinamid"]
        print "N2,4Dimethyl9decynoylNmethylphenylalanine" >> "tmp_files/correct_sequence_aa"
        print "methylalanine" >> "tmp_files/correct_sequence_aa"
	print "Nalpha,OdimethylLtyrosinamid" >> "tmp_files/correct_sequence_aa"

        }

if (sequence=="N-Methyl-N-[(2S)-2-methyl-7-octynoyl]-L-phenylalanyl-L-alanyl-N-methyl-L-alanyl-Nα,O-dimethyl-L-tyrosinamide") {sequence=translate_modif["NmethylN[(2S)-2-methyl-7-octynoyl]phenylalanine"]""A""translate_modif["methylalanine"]""translate_modif["Nalpha,OdimethylLtyrosinamid"]
        print "NmethylN[(2S)-2-methyl-7-octynoyl]phenylalanine" >> "tmp_files/correct_sequence_aa"
        print "Nα,OdimethylLtyrosinamide" >> "tmp_files/correct_sequence_aa"

        }

if (sequence=="N-methyl-N-[(2S)-2-methyloct-7-ynoyl]-L-valyl-N-methyl-L-valyl-N-methyl-L-valyl-Nα-methyl-L-phenylalaninamide") {sequence=translate_modif["NmethylN[(2S)-2-methyl-7-octynoyl]valine"]""translate_modif["methylvaline"]""translate_modif["methylvaline"]""translate_modif["NαmethylLphenylalaninamide"]
        print "NmethylN[(2S)-2-methyl-7-octynoyl]valine" >> "tmp_files/correct_sequence_aa"
        print "methylvaline" >> "tmp_files/correct_sequence_aa"
        print "NαmethylLphenylalaninamide" >> "tmp_files/correct_sequence_aa"

        }

if (sequence=="N-Methyl-N-[(2S)-2-methyl-7-octynoyl]-L-valyl-N-methyl-L-valyl-N-methyl-L-valyl-N2-methyl-L-valinamide") {sequence=translate_modif["NmethylN[(2S)-2-methyl-7-octynoyl]valine"]""translate_modif["methylvaline"]""translate_modif["methylvaline"]""translate_modif["N2methylLvalinamide"]
        print "N2methylLvalinamide" >> "tmp_files/correct_sequence_aa"

        }

if (sequence=="K-Hey-FQWQRNMRKVRGPPVS-Hey-IKR") {sequence="K"translate_modif["homocysteine"]"FQWQRNMRKVRGPPVS"translate_modif["homocysteine"]"IKR"
        print "homocysteine" >> "tmp_files/correct_sequence_aa"
	}

if (sequence=="Cyclo_(FΦRRRRQ)") {sequence="FXRRRRQ"
        }

if (sequence=="Cyclo(FΦRRRRQ)") {sequence="FXRRRRQ"
        }

if (sequence=="NLys-Nspe-Nspe-NLys-Nspe-Nspe-NLys-Nspe-Nspe") {sequence=translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]
        print "Nlysine" >> "tmp_files/correct_sequence_aa"
	print "Nphenylethylglycine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="NLys-Nspe-Nspe-NLys-Nspe-Nspe-NLys-Nspe-Nspe-NLys-Nspe-Nspe") {sequence=translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]
        print "Nphenylethylglycine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="NLys-NLys-Nspe-NLys-NLys-Nspe-NLys-NLys-Nspe-NLys-NLys-Nspe") {sequence=translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]
       print "Nlysine" >> "tmp_files/correct_sequence_aa"
        print "Nphenylethylglycine" >> "tmp_files/correct_sequence_aa"
       	}

if (sequence=="NLys-NLys-NLys-NLys-NLys-NLys-NLys-NLys-Nspe-Nspe-Nspe-Nspe") {sequence=translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]
   print "Nlysine" >> "tmp_files/correct_sequence_aa"
        print "Nphenylethylglycine" >> "tmp_files/correct_sequence_aa"
   	}

if (sequence=="NLys-NLys-NLys-NLys-NLys-NLys-NLys-NLys-NLeu-NLeu-NLeu-NLeu-Nleu") {sequence=translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nlysine"]""translate_modif["Nleucine"]""translate_modif["Nleucine"]""translate_modif["Nleucine"]""translate_modif["Nleucine"]""translate_modif["Nleucine"]
        print "Nleucine" >> "tmp_files/correct_sequence_aa"
	print "Nlysine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="Nbtg-Nspe-Nspe-Nbtg-Nspe-Nspe-Nbtg-Nspe-Nspe-Nbtg-Nspe-Nspe") {sequence=translate_modif["Nbutylguanidineglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nphenylethylglycine"]
        print "Nbutylguanidineglycine" >> "tmp_files/correct_sequence_aa"
	print "Nphenylethylglycine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="Nbtg-Nbtg-Nspe-Nbtg-Nbtg-Nspe-Nbtg-Nbtg-Nspe-Nbtg-Nbtg-Nspe") {sequence=translate_modif["Nbutylguanidineglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nphenylethylglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nbutylguanidineglycine"]""translate_modif["Nphenylethylglycine"]
	print "Nbutylguanidineglycine" >> "tmp_files/correct_sequence_aa"
        print "Nphenylethylglycine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="Myristoyl-B-RRRRRRRRRRR") {sequence=translate_modif["myristoyl"]"ЛRRRRRRRRRRR"
        print "myristoyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="Myristoyl-B-rrrrrrrrrrr") {sequence=translate_modif["myristoyl"]"Лrrrrrrrrrrr"
        }

if (sequence=="δ_-_(Stearyl-AGYLLG)_OINLKALAALAKKIL") {sequence=translate_modif["stearyl"]"AGYLLG"translate_modif["ornithine"]"INLKALAALAKKIL"
        print "stearyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="α,_ε_(Stearyl-AGYLLG)2KINLKALAALAKKIL") {sequence=translate_modif["stearyl"]"AGYLLGKINLKALAALAKKIL"
	print "stearyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="ε_-_(Steary-_AGYLLG)KINLKALAALAKKIL") {sequence=translate_modif["stearyl"]"AGYLLGKINLKALAALAKKIL"
        }

if (sequence=="AGYLLGK(Stearyl)INLKALAALAKKIL") {sequence="AGYLLGK"translate_modif["stearyl"]"INLKALAALAKKIL"
        }

if (sequence=="Biotin(O)-GGGG-RRWWRRWRR") {sequence=translate_modif["biotin"]"GGGGRRWWRRWRR"
        print "biotin" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="Biotin(O)-GGGG-RRFFRRFRR") {sequence=translate_modif["biotin"]"GGGGRRFFRRFRR"
        }

if (sequence=="Biotin(O)-GGGG-RRFFRRWRR") {sequence=translate_modif["biotin"]"GGGGRRFFRRWRR"
        }

if (sequence=="Biotin(O)-GGGG-RRWFRRFRR") {sequence=translate_modif["biotin"]"GGGGRRWFRRFRR"
        }

if (sequence=="Biotin(O)-GGGG-RRFWRRFRR") {sequence=translate_modif["biotin"]"GGGGRRFWRRFRR"
        }

if (sequence=="Biotin(O)-GGGG-RRFWRRWRR") {sequence=translate_modif["biotin"]"GGGGRRFWRRWRR"
        }

if (sequence=="Biotin(O)-GGGG-RRWWRRFRR") {sequence=translate_modif["biotin"]"GGGGRRWWRRFRR"
        }

if (sequence=="Biotin(O)-GGGG-RRWFRRWRR") {sequence=translate_modif["biotin"]"GGGGRRWFRRWRR"
        }

if (sequence=="Bz-Nle-Lys-Arg-(p-guanidinyl)-Phe-H") {sequence="XՔKR"translate_modif["guanidinyl"]"FH"
        print "guanidinyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="SLLGRMKG(beta-A)C") {sequence="SLLGRMKG"translate_modif["betaalanine"]
        print "betaalanine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="RSLLGRMKGA(beta-A)C") {sequence="RSLLGRMKGA"translate_modif["betaalanine"]"C"
        }

if (sequence=="MHRSLLGRMKGAG(beta-A)GC") {sequence="MHRSLLGRMKGAG"translate_modif["betaalanine"]"GC"
        }

if (sequence=="SucMEEC-OH") {sequence=translate_modif["succinyl"]"MEEC‱"
        print "succinyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="SucDVVAbuC-OH") {sequence=translate_modif["succinyl"]"DVVXC‱"
        }

if (sequence=="a-(RQIKIWFPNRRMKWKK)(d)-amide") {sequence="˩RQIKIWFPNRRMKWKK"translate_modif["amide"]
	print "amide" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="b-KLAKLALKALKAALKLA-amide") {sequence="XKLAKLALKALKAALKLA"translate_modif["amide"]
        print "amide" >> "tmp_files/correct_sequence_aa"
        }


if (sequence=="b-RQIKIWFPNRRMKWKK-amide") {sequence="XRQIKIWFPNRRMKWKK"translate_modif["amide"]
        }

if (sequence=="b-RQIKIFFPNRRMKFKK-amide") {sequence="XRQIKIFFPNRRMKFKK"translate_modif["amide"]
        }

if (sequence=="b-KLAKLALKAL(d)K(d)AALKLA-amide") {sequence="XKLAKLALKALKAALKLA"translate_modif["amide"]
        }

if (sequence=="b-(RQIKIWFPNRRMKWKK)(d)-amide") {sequence="XRQIKIWFPNRRMKWKK"translate_modif["amide"]
        }

if (sequence=="NH2-GRKKRRQRRRC-amide") {sequence="؟GRKKRRQRRRC"translate_modif["amide"]
        }

if (sequence=="YbetaAGAVVNDL") {sequence="Y"translate_modif["betaalanine"]"GAVVNDL"
        }

if (sequence=="2-aminobenzoyl_-KRR") {sequence=translate_modif["aminobenzoyl"]"KRR"
        print "aminobenzoyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="4-phenyl-phenacetyl-KRR") {sequence=translate_modif["phenylphenacetyl"]"KRR"
        print "phenylphenacetyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="Bz-N-Me-Nle-Lys-Arg-Arg-H") {sequence="XN"translate_modif["methylnorleucine"]"KRRH"
        print "methylnorleucine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="cinnamoyl-KRR") {sequence=translate_modif["cinnamoyl"]"KRR"
        }

if (sequence=="phenacetyl-KRR") {sequence=translate_modif["phenacetyl"]"KRR"
        print "phenacetyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="phenoxyacetyl-KRR") {sequence=translate_modif["phenoxyacetyl"]"KRR"
        print "phenoxyacetyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="phenylpropanoyl-KRR") {sequence=translate_modif["phenylpropanoyl"]"KRR"
        print "phenylpropanoyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="nitro-FYAGAVVNDL") {sequence=translate_modif["nitro"]"FYAGAVVNDL"
        print "nitro" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="azido-FYAGAVVNDL") {sequence=translate_modif["azido"]"FYAGAVVNDL"
        print "azido" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="A-AnthrylAla-YKKFKKKLLKSLKRLG") {sequence="A"translate_modif["anthrylalanine"]"YKKFKKKLLKSLKRLG"
        print "anthrylalanine" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="2-naphthoyl-KRR") {sequence=translate_modif["naphthoyl"]"KRR"
        print "naphthoyl" >> "tmp_files/correct_sequence_aa"
        }

if (sequence=="Aib-GGL-Aib-GIL") {sequence=translate_modif["aminoisobutyricacid"]"GGL"translate_modif["aminoisobutyricacid"]"GIL"
        print "aminoisobutyricacid" >> "tmp_files/correct_sequence_aa"
        }









if (sequence=="GW*LR**K**AAK**SVGK**FY*Y*K**HK*Y*Y*IK*AAWQIGKHAL-NH2") {sequence="GXLXXAAXSVGXFXXXHXXXIXAAWQIGKHAL"}

if (sequence=="azido-F-3',5'-127I-YAGAVVNDL") {sequence=translate_modif["4'-azidoF-3',5'-127I"]"YAGAVVNDL"
	print "4'-azidoF-3',5'-127I" >> "tmp_files/correct_sequence_aa"}

	if (sequence=="Pyr-H-(3,3-Me)P") {sequence=""translate_modif["pyroglutamic-acid"]"""H"""translate_modif["(3,3-Me)proline"]
	print "pyroglutamic-acid" >> "tmp_files/correct_sequence_aa"
	print "(3,3-Me)proline" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Pyr-(Me)HP") {sequence=""translate_modif["pyroglutamic-acid"]""translate_modif["methylhistidine"]"""P"
	print "pyroglutamic-acid" >> "tmp_files/correct_sequence_aa"
        print "methylhistidine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="(3-sulfanylpropanoic acid)SSLF") {sequence=""translate_modif["(3-sulfanylpropanoic_acid)"]"""SSLF"
	print "(3-sulfanylpropanoic_acid)" >> "tmp_files/correct_sequence_aa"}

if (sequence=="fCYw-Orn-T-Pen-T") {sequence="fCYw"""translate_modif["Ornithine"]"""T"""translate_modif["Penicillamine"]"""T"
	print "Ornithine" >> "tmp_files/correct_sequence_aa"
        print "Penicillamine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Y-cyclo(aGF-(Nω-methylguanidino)A-)-NH2") {sequence="YaGF"""translate_modif["Nω-methylguanidinoalanine"]""
	print "Nω-methylguanidinoalanine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="(Obu)AGPAIRAAVKQAQK(Dhb)LKA(Dhb)RLF(Abu)VAAKGKNGAL") {sequence=""translate_modif["2-oxobutyryl"]"""AGPAIRAAVKQAQK"""translate_modif["dehydrobutyrine"]"""LKA"""translate_modif["dehydrobutyrine"]"""RLF"""translate_modif["amino-butyric-acid"]"""VAAKGKNGAL"
	print "2-oxobutyryl" >> "tmp_files/correct_sequence_aa"
        print "dehydrobutyrine" >> "tmp_files/correct_sequence_aa"
	print "amino-butyric-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="YtGFLS-(β-D-Glc)") {sequence="YtGFL"""translate_modif["serine-beta-D-glucose"]""
	print "serine-beta-D-glucose" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Pyr-H-(3-Me)P") {sequence=""translate_modif["pyroglutamic-acid"]"""H"""translate_modif["methylhistidine"]""
	print "pyroglutamic-acid" >> "tmp_files/correct_sequence_aa"
        print "methylhistidine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Nle-Lys-Arg-Arg-Thiazole") {sequence="XKRR"}

if (sequence=="Nal2-ED-Nal2-VG-Nal2-I") {sequence=""translate_modif["3-naphthalen-2-yl-L-alanine"]"""ED"""translate_modif["3-naphthalen-2-yl-L-alanine"]"""VG"""translate_modif["3-naphthalen-2-yl-L-alanine"]"""I"
	print "3-naphthalen-2-yl-L-alanine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="CH3-YGGFLR-CH3R-l-NHC2H5") {sequence="YGGFLRRl"}

if (sequence=="GTYDHDVYRDEALNNRFQIKGVELKSG-GSGSG-C(CH2CONH2)") {sequence="GTYDHDVYRDEALNNRFQIKGVELKSGGSGSGG"}

if (sequence=="Pyr-HP") {sequence=""translate_modif["pyroglutamic-acid"]"""HP"
	print "pyroglutamic-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Y-(1S,2R)Achc-FF") {sequence="Y"translate_modif["alicyclic-beta-amino-acid"]"FF"

if (sequence=="YSTC(alpha-aminobutyric acid)FIM") {sequence="YSTC"""translate_modif["alpha-aminobutyric-acid"]"""FIM"
	print "alpha-aminobutyric-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="FAAGRR-alphaketo-SL-CONH2") {sequence="FAAGRRXSL"}

if (sequence=="SucMEEC-OH") {sequence=""translate_modif["succinyl"]"""MEEC"
	print "succinyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Nle-Lys-Arg-Arg-CF3") {sequence=translate_modif["norleucine"]"KRR"}
	print "norleucine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="(C14:0)-Dwrrrrrrrrg") {sequence=""translate_modif["myristic-acid"]"""Dwrrrrrrrrg"
	print "myristic-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="(C10:0)-Dwrrrrrrrg") {sequence=""translate_modif["decanoic-acid"]"""Dwrrrrrrrg"
	print "decanoic-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="VV-Nme-VPP-2-hydroxyisovaleryl-2-") {sequence=""}

if (sequence=="Myristoyl-Cys-Ala-Val-Ala-Tyr-(3 methyl)His-OMe") {sequence=translate_modif["myristic-acid"]"CAVAY"translate_modif["methylhistidine"]
	print "myristic-acid" >> "tmp_files/correct_sequence_aa"
        print "methylhistidine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="A-Hleu-QN-MeIle-Hasn-TPLT-Ade-V-Hleu") {sequence="A"translate_modif["hydroxyleucine"]"QN"translate_modif["methylisoleucine"]""translate_modif["hydoxyasparaginyl"]"TPLT"translate_modif["3-aminodecanoyl"]"V"translate_modif["hydroxyleucine"]
	print "hydroxyleucine" >> "tmp_files/correct_sequence_aa"
	print "3-aminodecanoyl" >> "tmp_files/correct_sequence_aa"
	print "hydoxyasparaginyl" >> "tmp_files/correct_sequence_aa"
        print "methylisoleucine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="VV-Nme-VPP-2-hydroxyisovaleryl-2-oxo-4-methoxy-5-benzyl-3-pyrroline") {sequence="VVN"translate_modif["methylvaline"]"PP"translate_modif["2hydroxyisovaleryl2oxo4methoxy5benzyl3pyrroline"]
	print "methylvaline" >> "tmp_files/correct_sequence_aa"
        print "2hydroxyisovaleryl2oxo4methoxy5benzyl3pyrroline" >> "tmp_files/correct_sequence_aa"}

if (sequence=="2-[[2-(dimethylamino)-3-methylbutanoyl]amino]-N-[3-methoxy-1-[2-[1-methoxy-2-methyl-3-oxo-3-[[2-phenyl-1-(1,3-thiazol-2-yl)ethyl]amino]propyl]pyrrolidin-1-yl]-5-methyl-1-oxoheptan-4-yl]-N,3-dimethylbutanamide") {sequence=translate_modif["2-[[2-(dimethylamino)-3-methylbutanoyl]amino]-N-[3-methoxy-1-[2-[1-methoxy-2-methyl-3-oxo-3-[[2-phenyl-1-(1,3-thiazol-2-yl)ethyl]amino]propyl]pyrrolidin-1-yl]-5-methyl-1-oxoheptan-4-yl]-N,3-dimethylbutanamide"]
	print "2-[[2-(dimethylamino)-3-methylbutanoyl]amino]-N-[3-methoxy-1-[2-[1-methoxy-2-methyl-3-oxo-3-[[2-phenyl-1-(1,3-thiazol-2-yl)ethyl]amino]propyl]pyrrolidin-1-yl]-5-methyl-1-oxoheptan-4-yl]-N,3-dimethylbutanamide" >> "tmp_files/correct_sequence_aa"}

if (sequence=="wm-p-chloro-f-Gla") {sequence="wmpf"translate_modif["carboxyglutamic-acid"]
	print "carboxyglutamic-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="cyclo[(2R)-2-hydroxy-3-methylbutanoyl-N-methyl-L-phenylalanyl-(2R)-2-hydroxy-3-methylbutanoyl-N-methyl-L-phenylalanyl-(2R)-2-hydroxy-3-methylbutanoyl-N-methyl-L-phenylalanyl]") {sequence=translate_modif["hydroxymethylbutanoyl"]""translate_modif["methylphenylalanyl"]""translate_modif["hydroxymethylbutanoyl"]""translate_modif["methylphenylalanyl"]""translate_modif["hydroxymethylbutanoyl"]""translate_modif["methylphenylalanyl"]
	print "hydroxymethylbutanoyl" >> "tmp_files/correct_sequence_aa"
        print "methylphenylalanyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Nle-Lys-Arg-Arg-Benzoxazole") {sequence=translate_modif["norleucine"]"KRR"translate_modif["benzoxazole"]
	print "norleucine" >> "tmp_files/correct_sequence_aa"
        print "benzoxazole" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Nle-Lys-Arg-Arg-Thiazole") {sequence=translate_modif["norleucine"]"KRR"translate_modif["thiazole"]
	print "norleucine" >> "tmp_files/correct_sequence_aa"
        print "thiazole" >> "tmp_files/correct_sequence_aa"}

if (sequence=="4-Aminobenzoyl-K-R-R-H") {sequence=translate_modif["4aminobenzoyl"]"KRRH"
	print "4aminobenzoyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="4-Aminobenzoyl-K-K-R-H") {sequence=translate_modif["4aminobenzoyl"]"KKRH"
	print "4aminobenzoyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="4-Aminophenylacetyl-K-R-R-H") {sequence=translate_modif["4aminophenylacetyl"]"KRRH"
	print "4aminophenylacetyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="4-Phenylphenylacetyl-K-R-R-H") {sequence=translate_modif["4phenylphenylacetyl"]"KRRH"
	print "4phenylphenylacetyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="4-Phenylphenylacetyl-K-K-R-H") {sequence="translate_modif["4phenylphenylacetyl"}""KKRH"
	print "4phenylphenylacetyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="W-Ape-WVGWI") {sequence="W"translate_modif["5-aminopentanoic-acid"]"WVGWI"
	print "5-aminopentanoic-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="W-Ape-W-Ape-WIle") {sequence="W"translate_modif["5-aminopentanoic-acid"]"W"translate_modif["5-aminopentanoic-acid"]"WI"
	print "5-aminopentanoic-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="KW-Ape-W-Ape-WI") {sequence="KW"translate_modif["5-aminopentanoic-acid"]"W"translate_modif["5-aminopentanoic-acid"]"WI"
	print "5-aminopentanoic-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Nal2-Ape-Nal2-Ape-Nal2-I") {sequence=translate_modif["naphthalenylalanine"]""translate_modif["5-aminopentanoic-acid"]""translate_modif["naphthalenylalanine"]""translate_modif["5-aminopentanoic-acid"]""translate_modif["naphthalenylalanine"]"I"
	print "5-aminopentanoic-acid" >> "tmp_files/correct_sequence_aa"
        print "naphthalenylalanine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="WEDW-Ape-WI") {sequence="WEDW"translate_modif["5-aminopentanoic-acid"]"WI"
	print "5-aminopentanoic-acid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="DEDifIChaC(Me)ANleSY") {sequence="DE"translate_modif["diphenylalanine"]"I"translate_modif["cyclohexylalanin"]"C"translate_modif["methylalanine"]""translate_modif["norleucine"]"SY"
	print "diphenylalanine" >> "tmp_files/correct_sequence_aa"
        print "cyclohexylalanin" >> "tmp_files/correct_sequence_aa"
	print "methylalanine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="wLwSeNsK") {sequence="wLwSeNsK"

if (sequence=="wLMewSMeeNSK") {sequence="w"translate_modif["methylleucine"]"w"translate_modif["methylserine"]"eNSK"
	print "methylleucine" >> "tmp_files/correct_sequence_aa"
        print "methylserine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="(NH2-EDGGSFMC(acm)LWC(acm)GEVHG)2-β-Ala-Lys-CONH") {sequence="EDGGSFM"translate_modif["protectedcysteine"]"LW"translate_modif["protectedcysteine"]"GEVHG"translate_modif["betaalanine"]"K"
	print "protectedcysteine" >> "tmp_files/correct_sequence_aa"
        print "betaalanine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="2-(3-benzylureido)-KRR") {sequence=translate_modif["benzylureido"]"KRR"
	print "benzylureido" >> "tmp_files/correct_sequence_aa"}

if (sequence=="2-(7-methoxy-naphthyl)-propanoyl-KRR") {sequence=translate_modif["methoxynaphthylpropanoyl"]"KRR"
	print "methoxynaphthylpropanoyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="3-methoxy-phenacetyl-KRR") {sequence=translate_modif["methoxyphenacetyl"]"KRR"
	print "methoxyphenacetyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="amino-FYAGAVVNDL") {sequence=translate_modif["amino"]"FYAGAVVNDL"
	print "amino" >> "tmp_files/correct_sequence_aa"}

if (sequence=="azido-F-3',5'-127I-YAGAVVNDL") {sequence=translate_modif["azido"]"FYAGAVVNDL"
	print "azido" >> "tmp_files/correct_sequence_aa"}

if (sequence=="VV-Nme-VPP-2-hydroxyisovaleryl-2-") {sequence=""}

if (sequence=="DPITRQW(Farnesyl)GD") {sequence="DPITRQW"translate_modif["farnesyl"]"GD"
	print "farnesyl" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Y-(1S,2R)Acpc-FF") {sequence="Y"translate_modif["aminocyclopentanecarboxylicacid"]"FF"
	print "aminocyclopentanecarboxylicacid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="YcGFc-(β-D-Xyl)SG") {sequence="YcGFc"translate_modif["xylose"]"SG"
	print "xylose" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Thr-Ser-Ala-Val-Leu-NHCH(CH2CH2CON(CH3)2)-CHO") {sequence="TSAVL"}

if (sequence=="	Y-Pen-GF-Pen") {sequence="Y"translate_modif["Penicillamine"]"GF"translate_modif["Penicillamine"]
	print "Penicillamine" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Pyr-LYENKPRRPwIL") {sequence=translate_modif["pyroglutamicacid"]"LYENKPRRPwIL"
	print "pyroglutamicacid" >> "tmp_files/correct_sequence_aa"}

if (sequence=="Ser-Ala-Val-Leu-NHCH(CH2CH2CON(CH3)2)-CHO") {sequence="SAVL"}



return sequence



