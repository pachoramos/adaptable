function read_satpdb_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,s,pdb_tmp) {
limit=20000
mm=0
                a=10000;
                limit_=a+limit
                print "" > "tmp_files/modified_aa_satpdb"
		file="DATABASES/satp/satpfiles/satpdb"a
		while(a<limit_) {
                if (system("[ -f " file " ] ") ==0 ) {
#seq
                                input_file=file
				field=read_database_line(input_file,"'>Sequence",0,"'black'>","</b",1)
                                gsub("\\[","",field)
                                gsub("\\]","",field)
                                gsub("'","",field)
                                gsub("*","#",field)
				seq_a[a]=field
				# Some seqs have a bogus _ at the end
				gsub("_","",seq_a[a])
			        # Take care of really difficult sequences
			        seq_a[a]=correct_sequence(seq_a[a])

				read_modified_aa(modif,translate_modif)
				field=read_database_line(input_file,"'>Type_of_Modification",0,"'black'>","</b",1)
                                gsub("*","#",field)
                                gsub("*","#",seq_a[a])

                                # Fix awful unclosed parenthesis
                                field=clean_string(field)

                                # Make modifications separated by 'and' be separated by ; as all the others
                                gsub(" and ",";",field)

				# Make modification separated by ',' be separated by ';' without altering other uses of , (like 3,3-..., 4,2...)
				string=""
				split(field,cc,","); w=0; while(w<length(cc)) {w=w+1
					string=string""cc[w]
					if (w<length(cc)) {
						if(substr(cc[w],length(cc[w]),1)!~/^[0-9]+$/ && substr(cc[w],length(cc[w]),1)!="N" && substr(cc[w],length(cc[w]),1)!="S" && substr(cc[w],length(cc[w]),1)!="O" && substr(cc[w],length(cc[w]),1)!="P" ) {string=string";"} else {string=string","}
					}
				}
                                field=string

                                # We don't want to leave parenthesis in the final sequence
                                gsub("\\(","",seq_a[a])
                                gsub("\\)","",seq_a[a])
				gsub(":","",seq_a[a])

				# This for many modifications splitted by ;
                                split(field,aa,";")
                                # in cases where the modifications are called with similar names, the substitution should be made 
                                # following the order of the lengh. For example T#=hydroxyquinine; T##=quinoline and sequence T##T#GGAA
                                # T## must be substituted first otherwise we would obtain X#XGGAA instead of XXGGAA
                                delete conv_index
                                z=0;while(z<length(aa)) {z=z+1;split(aa[z],bb,"=");
                                       ert[z]=length(bb[1])     
                                }
                                sort_nD(ert,conv_index,">","no","v","","","","","",1,length(aa))
                                z=0;while(z<length(aa)) {z=z+1;aa_ordered[z]=aa[conv_index[z]]}

                                        z=0;while(z<length(aa)) {z=z+1;

                                        gsub("_","",aa_ordered[z])
                                        split(aa_ordered[z],bb,":")

                                        # Generate a temporal file to allow us to update the modified aa assignments
                                        # Here we replace the patterns in the sequence as taken from Type of Modification field, that indicates the letter and the meaning
                                        if(aa_ordered[z]~/=/) {split(aa_ordered[z],bb,"=")}
                                        if(aa_ordered[z]~/:/) {split(aa_ordered[z],bb,":")}

                                        # Ignore case
                                        bb[2]=tolower(bb[2])

	                                if(translate_modif[bb[2]]=="" && bb[2]!="") {
	                                	print "translate_modif[\""bb[2]"\"]=\"X\";" >> "tmp_files/modified_aa_satpdb"
		                               	# Show X for now until we update read_modified_aa
						translate_modif[bb[2]]="X"
					}
                                        # Do the substitution: we replace bb[1] (from sequence) with the translate_modif of the LEGEND (bb[2]) because sometimes there are cases like
                                        # O=Ornithine
                                        # X=Ornithine
                                        # for different IDs
					gsub(bb[1],translate_modif[bb[2]],seq_a[a])
				}
				seq_a[a]=analyze_sequence(seq_a[a])
				print "Reading "file" for sequence "seq_a[a]""

#ID
				field=read_database_line(input_file,"Peptide ID",0,"'black'>","<")
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]""field";"; pdb_[seq_a[a]]=pdb_[seq_a[a]]""field".pdb;";pdb_tmp=field}
#N_terminus
				field=read_database_line(input_file,"N-terminal modification",0,"'black'>","<")
				gsub("Free","",field)
				field=clean_string(field)
				if(N_terminus_[seq_a[a]]!~field) {if(field!="") {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}} ;
				if(field!~/Free/ && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
#C_terminus
			field=read_database_line(input_file,"C-terminal modification",0,"'black'>","<")
			gsub("Free","",field)
			field=clean_string(field)
			if(C_terminus_[seq_a[a]]!~field) {if(field!="") {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}} ;
#PTM
			field=read_database_line(input_file,"Type_of_Modification",0,"'black'>","<",1)
			field=clean_string(field)
			if(PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}

#cyclic
			field=read_database_line(input_file,"Peptide_Type",0,"'black'>","<")
			if(field~/yclic/) {cyclic_[seq_a[a]]="cyclic"}
			field=clean_string(field)
			if(PTM_[seq_a[a]]!~field && field~/yclic/ && PTM_[seq_a[a]]!~/cyclization/ ) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"cyclization;"}
#Function
			field=read_database_line(input_file,"Major Functions",0,"(",")")
				read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
						antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
						antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
						cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
						tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
#Sub-function
				field=read_database_line(input_file,"Sub-functions",0,"'black'>","<")
				read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
						antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
						antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
						cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
						tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
#DSSP
			field=read_database_line(input_file,"DSSP states",0,"'black'>","<")
			if(field=="NA") {field=""}
			DSSP_[seq_a[a]]=field
#Experimental structure/PDB
# They can be experimental even if they are "satpdb..."
                        field=read_database_line(input_file,"Experimentally determined",0,"structure","PDB")
                        field=clean_string(field)
			if(field!="") {
				if(experim_structure_[seq_a[a]]!~pdb_tmp) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""pdb_tmp".pdb;"}
				#experimental_[seq_a[a]]="experimental"
			}
                }
		a=a+1
                file="DATABASES/satp/satpfiles/satpdb"a
	}
       close(input_file)
       smax=a
return smax
}
