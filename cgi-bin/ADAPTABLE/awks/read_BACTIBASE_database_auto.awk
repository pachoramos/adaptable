function read_BACTIBASE_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {

limit=1000
                a=1;
        limit_=a+limit
                path="DATABASES/BACTIBASE/BACTIBASEfiles/BAC"
                root_online="http://bactibase.hammamilab.org/BAC"
                length_numb=3
                add_tag="yes"
                split(path,aa,"/")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file=path""tag""a
                        if (system("[ -f " file " ] ") ==0 ) {

			input_file=file
#seq
				field=read_database_line(input_file,"name=\"UserSequence\"",0,"value=\"","\">",1)
				seq_a[a]=field
				print "Reading "file" for sequence "seq_a[a]""
				seq_a[a]=analyze_sequence(seq_a[a])
#ID
                                field=read_database_line(input_file,"id=\"sequence",0,"_","\">",1)
                                if(ID_[seq_a[a]]!~field) {if(field!="") {ID_[seq_a[a]]=ID_[seq_a[a]]"BACTIBASE_"field";" }}
#name
                                field=read_database_line(input_file,"data-original-title",0,"\">","</span>",2)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"nomenclature",0,"genus=","\"",1)
				smax=split(field,aa,">")
				string=""
				s=0; while(s<smax) {s=s+1
				if(aa[s]!~/html/ && aa[s]!~/width/) {string=string""aa[s]}
				}
				gsub("</a","",string); gsub("<p","",string);gsub("<i/","",string)
				field=clean_string(field)
				if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
				
#Family
                                field=read_database_line(input_file,"Classification",2,"Class/","\"",1)
                                field=clean_string(field)
                                if(Family_[seq_a[a]]!~field) {if(field!="") {Family_[seq_a[a]]=Family_[seq_a[a]]""field";" }}
#gene
                                field=read_database_line(input_file,"Gene:_",0,"<span>","</span>",1)
				gsub("<s>","",field)
				gsub("</s>","",field)
				field=clean_string(field)
                                if(gene_[seq_a[a]]!~field) {if(field!="") {gene_[seq_a[a]]=gene_[seq_a[a]]""field";" }}

#all_organisms
        		 variable=""
			 field=1; b=0; while (field!="")  {b=b+1
	        		string=read_database_line(input_file,"View_all_bacteriocins_active",0,"href=\"/Target/","\"_title=",b)
		        	if(string~/+/) {
			        	gsub("+","_",string)
			        	gsub("active","",string)
			        	gsub("active+on","",string)
			        	field1=string
                                }
                                else {field1=""}

        			field2=read_database_line(input_file,"nomenclature at DSMZ</a>' target='_blank'>",0,"</i>","<i>",b)
	        		gsub("Î¼","μ",field2)
		        	gsub("&#956;","μ",field2)
	
		        	split(field2,aa,"\\(")
		        	split (aa[2],bb,"\\)")
		        	field2=bb[1]
		        	field=field1"("field2")"
		        	variable=variable""sprintf("%1s",field)";"
		        	if(field=="()"){field=""}
		        	field=analyze_organism(field,seq_a[a])
			}
			gsub("\\(\\)","",variable)
			gsub(";\\(","(",variable)
			ccmax=split(variable,cc,";")
			c=0; while(c<ccmax) {c=c+1
				field=cc[c]
				gsub("NOTE:_not","",field)
				gsub(" ","_",field)
				field=analyze_organism(field,seq_a[a])
				gsub("Gram-positive_bacteria","",field)
				field_to_compare=""
				field_to_compare=escape_pattern(field)
	                       	if(all_organisms_[seq_a[a]]!~field_to_compare) {if(field!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""field";"}}
			}

#PDB
                       field=1; b=0; while(field!="") {b=b+1
			       	field=read_database_line(input_file,"PDB_entry",0,"<b>","</b>",b)
			       	field=clean_string(field)
			       	field=toupper(field)
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}}

                       field=1; b=0; while(field!="") {b=b+1
                                field=read_database_line(input_file,"PDB_entry",0,"<b>","</b>",b)
                                field=clean_string(field)
                                field=toupper(field)
                        if(field!="") { if(experim_structure_[seq_a[a]]!~field) {
                              				experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"
		                        		#experimental_[seq_a[a]]="experimental"
		                        		}
				}
			}

#Taxonomy
			field=1; b=0; while(field!="") {b=b+1
                        field=read_database_line(input_file,"_infos\"",0,"><i>","</i></a>",b)
                        field=clean_string(field)
                        field=tolower(field)
                        if(taxonomy_[seq_a[a]]!~field) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}}
                        }

#Pubmed			
			field=1; b=0; while(field!="") {b=b+1
                        field=read_database_line(input_file,"Pubmed_citation",0,"<u>","</u>",b)
			if(field!="") {if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}}
		
		}
       close(input_file)
	a=a+1
file="DATABASES/DBAASP/DBAASPfiles/DBAASP_"a
amax=a
}
return amax
}
