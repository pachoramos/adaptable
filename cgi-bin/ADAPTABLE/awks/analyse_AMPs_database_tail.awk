@include "awks/library.awk"
BEGIN{
#read_parameters(prp_name,prp_option)
#read_parameters_()
	parameters(prp_name,prp_option)

		read_aa_prop_and_colors(aa_name,conv_aa,class,class_def,class_length,type,polarity,color_type)

		delete seq
		delete original_seq
		delete prop
		delete seq_names

		max_len=0
		input_file="OUTPUTS/"calculation_label"/selection"
		while ( getline < input_file >0 ) {
			if($2=="seq") {seq[$1]=$3}
			if($2=="oseq") {original_seq[$1]=$3;len=length($3);population[int(len)]=population[int(len)]+1 ; if(len>max_len) {max_len=len}}
			if($2=="prop_separator") {split($0,aaaa,"prop_separator ") ; prop[$1]=aaaa[2] }
			if($2=="names") {seq_names[$1]=$3}
			dmax=$1
		}
	close(input_file)

		sort_families(family,original_family,fam_type,fathers,original_fathers,fam_size,fam_aligned,fam_properties,fam_names,alignments,alignments_transl,score_fathers,code_fathers,type_fathers,"OUTPUT_INPUT",\
				dmax,seq,original_seq,c)

		if(generate_overlapping_families=="yes") {if(families_to_analyse=="") {fam_auto="yes";cmax=dmax;families_to_analyse=cmax}}
		else {
			fam_auto=""
				if(families_to_analyse=="" || fam_auto=="yes") {families_to_analyse=c ; fam_auto="yes"} else {if(c==families_to_analyse) {families_to_analyse=c}}
			cmax=dmax
		}

# Get updated number of families after dropping duplicated ones in sort_families
	input_file="OUTPUTS/"calculation_label"/.fam_num"
		while ( getline < input_file >0 ) {
			fam_num=$1
		}
	close(input_file)
		families_to_analyse=fam_num
		cmax=fam_num

# 3) ANALYSIS. All families are generated; Now each one is analised 

		if(families_to_analyse==cmax && fam_auto=="yes") {fam_count0=0 ; last_fam=cmax}
		else {split(families_to_analyse,cl,"-"); fam_count0=cl[1]-1 ; last_fam=cl[2]}
	if(calculate_only_global_properties=="y") {fam_count0=0;last_fam=1}
	title("blue","#",100)
		fam_count=fam_count0 ; while(fam_count<last_fam && fam_size[fam_count+1]>=1) {fam_count=fam_count+1
			if(calculate_only_global_properties!="y") {
				families_to_analyse=fam_count
					title("blue","#",100)

					print ""
					print "Calculating for Family "families_to_analyse"(out of "last_fam")..." 	
					print ""
					title("blue","#",100)
# arrays in two indexes (index of the family and index of the sequence) are transfered to arrays in one index, because we are analysing each family ata a time. At each familys these new arrays have to be deleted

					delete seq
					delete prop_f
					delete prp_glob
					delete short_prp
					delete prp_val
					delete align
					delete align_transl
					delete string
					delete string_summary
					delete prp_matrix
					delete DSSP_
					delete index_fath

# 3.1) Data tranformation
# one-index arrays are generated; properties are converted into numerical code (1 and 0) for the generation of a compact table of properties
# depending on the parameter analyse_only_region_of_the_fam_father, the aligned sequences are cut to the length of the family father for analysis or not

					delete conv_index2
                                        sort_nD(score_fathers,conv_index2,">","no",families_to_analyse,"v","","","","","","",1,100)
                                        f=0; while(f<fam_size[families_to_analyse]) {f=f+1 ; tmp_string=fam_aligned[families_to_analyse,f] ; gsub("-","",tmp_string); 
					if(tmp_string==fathers[families_to_analyse,conv_index2[1]]) {index_fath[f]=f}}
					f=0; while(f<fam_size[families_to_analyse]) {f=f+1
						if(analyse_only_region_of_the_fam_father=="y") {
							# the sequences will be cut according to the length of the father. The line of the father might be preceeded by a series of "---"
							# in order to determine their number we split the line of the father with the sequence of the father
							split(fam_aligned[families_to_analyse,index_fath[f]],fff,fathers[families_to_analyse,conv_index2[1]])
								if(fff[1]~/-/) {st=length(fff[1])+1} else {st=1}
							seq[f]=substr(fam_aligned[families_to_analyse,f],st,length(fathers[families_to_analyse,conv_index2[1]]))
						}
						else {seq[f]=fam_aligned[families_to_analyse,f]
						}
						pmax=split(seq[f],fff,"")
							upper=pmax
							if(fam_count==fam_count0+1) {if (n_regions>pmax) {n_regions=pmax}}
							else {n_regions=pmax}
						prop_f[f]=fam_properties[families_to_analyse,f]

							align[f]=alignments[families_to_analyse,f]
							align_transl[f]=alignments_transl[families_to_analyse,f]
							split(prop_f[f],prp," ")
							n_prop=62
							short_prp[f]=fam_names[families_to_analyse,f]
							p=0;while(p<n_prop) {p=p+1
								prp_matrix[p,f]=prp[p]
									if(p==60) {prp_matrix[p,f]=peptide_solubility(family[families_to_analyse,f])}
								if((prp[p]=="_" || prp[p]=="") && p!=60) {prp_val[p,f]=0} else {prp_val[p,f]=1}
								prp_glob[p]=prp_glob[p]+prp_val[p,f]
							}
					}
# for each property that cannot be translated into numbers, a summary of the members of the family is given; trying to avoid repetition of the same sentence 
				p=0;while(p<n_prop) {p=p+1
					f=0; while(f<fam_size[families_to_analyse]) {f=f+1
						if(p==52) {DSSP_[f]=prp_matrix[p,f]}
						if(prp_matrix[p,f]!="_") {string3=clean_string(prp_matrix[p,f]);
							len_string3=split(string3,string4,";"); w=0; while(w<len_string3) {w=w+1 ; string5=clean_string(string4[w]) 
								if(string_summary[p]!~string5) { string_summary[p]=string5";"string_summary[p]}
							}
							string[p,f]=prp_matrix[p,f]";"string[p,f];
						}
					}
				}


					print "Family "families_to_analyse" has "fam_size[families_to_analyse]" elements. The best alignment is found with: "
					k=1;while(fathers[families_to_analyse,conv_index2[k]]!="") {print "score="score_fathers[families_to_analyse,conv_index2[k]],"\t"fathers[families_to_analyse,conv_index2[k]]," type("type_fathers[families_to_analyse,conv_index2[k]]")"; k=k+1}
				read_alignment(align,short_prp,DSSP_,align_transl,work_in_simplified_aa_space)
					print ""

					print ""
					print "This family has the following properties:"
					printf "( 1 in "; color("red ,","red","","bold") ; printf "for user-selected properties (boolean or not)," 
					color(" orange ","orange","","bold");printf "for unselected boolean properties,"
					color(" (sub-μM)","blue_cyan0","","bold");color("-(μM)","blue_cyan1","","bold"); color("-(sub-mM)","blue_cyan2","","bold");color("-(mM)","blue_cyan3","","bold");color("-(very weak)","blue_cyan4","","bold");printf " for activity values, "
					color("green ","green","","bold");print "for unselected non-boolean information)"
					print ""
					n_prop=62
					p=0;max_string=0;while(p<n_prop) {p=p+1 ; str[p]=split(prp_name[p],ss,""); if (str[p]>max_string) {max_string=str[p]}}
#p=0;while(p<n_prop) {p=p+1 ; printf p"="prp_name[p]" "}
				n=0; while(n<max_string) {n=n+1
					printf("      ")
						p=0;while(p<n_prop) {p=p+1 ;if(n>(max_string-str[p])) {color(" "substr(prp_name[p],n-(max_string-str[p]),1)" ","blue_cyan0","","bold");} else {printf "   ";} ;if(p==n_prop) {print "      1 "} }
				}
#			print ""
#
				print ""
					printf("      ");p=0;while(p<n_prop) {p=p+1 ; printf ("%2s ",p)}
				print ""
					f=0; while(f<fam_size[families_to_analyse]) {f=f+1 ;print "" ;printf("%4d. ",f) 
						p=0;while(p<n_prop) {p=p+1 ;
							if(prp_val[p,f]==1) {
								if(prp_option[p]=="i") {
									if(prp_matrix[p,f]==prp_name[p]) {color(" "prp_val[p,f]" ","orange","","bold")} 
									else {
										if(p==32 || p==38 || p==58 || p==61) {prp_tmp1=prp_matrix[p,f]+0
											if(prp_tmp1<1 && prp_tmp1>0) {color(" "prp_val[p,f]" ","blue_cyan0","","bold")}
											if(prp_tmp1<100 && prp_tmp1>=1) {color(" "prp_val[p,f]" ","blue_cyan1","","bold")}
											if(prp_tmp1<1000 && prp_tmp1>=100) {color(" "prp_val[p,f]" ","blue_cyan2","","bold")}	
											if(prp_tmp1<10000 && prp_tmp1>=1000) {color(" "prp_val[p,f]" ","blue_cyan3","","bold")}	
											if(prp_tmp1>100000) {color(" "prp_val[p,f]" ","blue_cyan4","","bold")}
											if(prp_tmp1==0) {color(" "prp_val[p,f]" ","white","","")}

										}
										else {
											if(p==60) {
												if(prp_matrix[p,f]=="soluble") {color(" S ","black","green_yellow2","bold")}
												if(prp_matrix[p,f]=="poorly_soluble") {color(" S ","black","yellow","bold")}
												if(prp_matrix[p,f]=="almost_insoluble") {color(" S ","black","orange","bold")}
												if(prp_matrix[p,f]=="insoluble") {color(" S ","black","red","bold")}
											}
											else {
												if(p==8 || p==9 || p==10) {if ((prp_matrix[p,f]~/Free/ && (p==8 || p==9 )) \
														|| ((prp_matrix[p,f]=="(Free);;PTM;PTM" || prp_matrix[p,f]=="(Free);;PTM;PTM;PTM") && p==10)) {
													if(prp_matrix[p,f]=="(Free);" || p==10) {color(" "0" ","white","","")} else {color(" "0" ","gray","","")}}
												else {color(" "prp_val[p,f]" ","green","","bold")} 
												}
												else {color(" "prp_val[p,f]" ","green","","bold")}
											}
										}
									}
								} 
								else{color(" "prp_val[p,f]" ","red","","bold")}
							} 
							else {color(" "prp_val[p,f]" ","white","","");}
						}; 
#	printf fam_names[families_to_analyse,f]
					}
#printf short_prp[f]}
				print ""
					print""	
					p=0;while(p<n_prop) {p=p+1
						delete string_f
							f=0; while(f<fam_size[families_to_analyse]) {f=f+1 ;
								string_f[f]=string[p,f]
							}
						if(fam_size[families_to_analyse]>=1) {
							color(" "prp_name[p]": (data available for the "prp_glob[p]/fam_size[families_to_analyse]*100" %% of members)","blue_cyan1","","bold"); print ""
							if(prp_glob[p]/fam_size[families_to_analyse]*100!=0) {
							if(prp_name[p]!="ID" && prp_name[p]!="sequence" && prp_name[p]!="DSSP" && prp_name[p]!="pdb" && prp_name[p]!="experim_structure") {
								printf("      "); color("summary:","blue_cyan3","","bold"); print "" ;
								#if(prp_name[p]=="name" || prp_name[p]=="source" || prp_name[p]=="RBC_source" || prp_name[p]=="cell_line" ||  prp_name[p]=="cancer_type" ||\
								#prp_name[p]=="gene" || prp_name[p]=="Family" || prp_name[p]=="taxonomy" || prp_name[p]=="all_organisms") {
								printf "      ";sort_text_by_occurence(string_f,"",5,3,"average_on_lines",170,6,"pale_violet","",""); print "";
								printf "      "; sort_text_by_occurence(string_f,"-_",2,3,"average_on_lines",170,6,"cyan","",""); print "";
								#}
								#else {
								#printf("      "); print_newline(string_summary[p],170,6,"pale_violet","",""); print ""; 
								#}
							}
							f=0; while(f<fam_size[families_to_analyse]) {f=f+1 ; 
								#if(string[p,f]!="") {
								printf("%4d. ",f); print_newline(string[p,f],170,6,"light_gray","","");print ""
								#}
							}}; print "___"
						}
					}

				dmax=fam_size[families_to_analyse]
					best_align_ref=father_index[families_to_analyse]
					delete count_aa_type_number_per_position
					delete count_average_aa_type_number_per_region
					delete count_average_correl_per_region
					delete count_correl
					delete count_correl_pos
					delete count_aa_presence_per_peptide
					delete count_average_aa_presence_per_peptide
					delete count_average_aa_type_number_per_peptide
					delete perc
					delete total_percent
					delete count_aa
					delete max_score
					delete count_aa_type_number_per_peptide
					split(seq[best_align_ref],reference,"");shift[d]=0
					len_ref=length(reference)
					print "best alignment with sequence number "best_align_ref": "
					print ""seq[best_align_ref]" ("len_ref" aa; score="max_score_of_all" with "align_method" method)"
#	Fasman=seq[best_align_ref]; if (len_ref<40) {g=0 ; while(g<(40-len_ref)) {g=g+1;Fasman=Fasman"A"}} 
#	if(plot_graphs=="y") {print ">" > "tmp.fasta"
#	if(plot_graphs=="y") {print Fasman >> "tmp.fasta"
#	close("tmp.fasta")
# system("python alphabeta.py < tmp | grep -")
	pr=ChouFasman(seq[best_align_ref],"",short_prp[best_align_ref],"")
		pr=ChouFasman(seq[best_align_ref],"",short_prp[best_align_ref],DSSP_[best_align_ref])
#split(pr,predi,"_")
#print predi[1]
#print predi[2]
		print "(Chou-Fasman predictions)"
		print "" > "OUTPUTS/"calculation_label"/"calculation_label"_FASTA_OUTPUT_cut_f"families_to_analyse
}
if(calculate_only_global_properties=="y") {pmax=n_regions}	
d=0;while(d<dmax) {d=d+1
#	len_seq=length(seq[d]);len_ref=length(reference) ; if(len_seq>len_ref) {align_range[d]=len_seq-len_ref+int(len_ref/2)} else {align_range[d]=len_ref-len_seq+int(len_ref/2)} 
#
#	split(seq[d],p_seq,"")
#		if(d!=d) {shift[d]=align_sequences(seq_type,max_score"***",d,reference,p_seq,align_range[d],align_method,blosum); type_seq[d]=seq_type[1]} 
#	if(shift[d]<0) {len=length(seq[d])+shift[d]; cutseq[d]=substr(seq[d],1-shift[d],len)}
#	add=""
#		if(shift[d]>0) {a=0;while(a<shift[d]){a=a+1 ; add=add""sprintf("-"); } ;cutseq[d]=add""seq[d];len=length(seq[d])+shift[d]}
#	if(shift[d]==0 || d==best_align_ref) {cutseq[d]=seq[d];len=length(seq[d]);type_seq[d]="_"}
#	len_diff=len_ref-len ; if(len_diff>0) {k=0; while(k<len_diff) {k=k+1;cutseq[d]=cutseq[d]"-"}} ; if(len_diff<0) {cutseq[d]=substr(cutseq[d],1,len_ref)}
#	split(cutseq[d],p_seq,"")
	split(seq[d],p_seq,"")
		a=0; while(a<20) {a=a+1; 
			p=0; while(p<pmax) {p=p+1;  
				if(p_seq[p]==aa_name[a] && p_seq[p]!="-") {count_aa[a,d,p]=count_aa[a,d,p]+1  ; 
					aa=0; while(aa<20) {aa=aa+1; 
						pp=p; while(pp<pmax) {pp=pp+1; 
							if(p_seq[pp]==aa_name[aa] && p_seq[pp]!="-") {count_correl[a,aa,pp-p]=count_correl[a,aa,pp-p]+1; count_correl_pos[p,a,aa,pp-p]=count_correl_pos[p,a,aa,pp-p]+1;
								count_correl[aa,a,p-pp]=count_correl[a,aa,pp-p];count_correl_pos[pp,aa,a,p-pp]=count_correl_pos[p,a,aa,pp-p]}
						}
					}
				}
			}
		}
	print ">" >> "OUTPUTS/"calculation_label"/"calculation_label"_FASTA_OUTPUT_cut_f"families_to_analyse
#print ">"prop_f[d]" "type_seq[d]" "shift[d] >> "OUTPUTS/"calculation_label"_FASTA_OUTPUT_cut_f"families_to_analyse
#	print cutseq[d] >> "FASTA_OUTPUT"
		split(prop_f[d],fff," ")
		print seq[d] >> "OUTPUTS/"calculation_label"/"calculation_label"_FASTA_OUTPUT_cut_f"families_to_analyse
}
close("OUTPUTS/"calculation_label"/"calculation_label"_FASTA_OUTPUT_cut_f"families_to_analyse)	
#		title("blue","#",100)
#		print "AMINOACID STATISTICS"
#		title("blue","#",100)
delete count_aa_type_number_per_position
a=0 ; while(a<20) {a=a+1
#	print aa_name[a]":"
	d=0 ; while(d<dmax) {d=d+1
		p=0 ; while(p<pmax) {p=p+1
			count_aa_type_number_per_peptide[a,d]=count_aa_type_number_per_peptide[a,d]+count_aa[a,d,p]
				count_aa_type_number_per_position[a,p]=count_aa_type_number_per_position[a,p]+count_aa[a,d,p]
				if(count_aa_type_number_per_peptide[a,d]==1) {count_aa_presence_per_peptide[a]=count_aa_presence_per_peptide[a]+1}
		}
		count_average_aa_type_number_per_peptide[a]=count_average_aa_type_number_per_peptide[a]+count_aa_type_number_per_peptide[a,d]
	}
	if (dmax==0) { print "No peptide found with specified properties" ; a=20} else {count_average_aa_presence_per_peptide[a]=count_aa_presence_per_peptide[a]/dmax
		count_average_aa_type_number_per_peptide[a]=count_average_aa_type_number_per_peptide[a]/dmax
#				print "Average number per peptide="count_average_aa_per_peptide[a]
#				print "Average presence per peptide="count_average_aa_presence_per_peptide[a]
#				print ""
	}
}
class_number=4
if(plot_graphs=="y") {print "" > "tmp_files/tmp0_"calculation_label"_"fam_count}
if(plot_graphs=="y") {print "CLASS DEFINITIONS" >> "tmp_files/tmp0_"calculation_label"_"fam_count}
c=0 ; while(c<class_number) {c=c+1
	cc=0 ; while(cc<class_length[c]) {cc=cc+1
		if(plot_graphs=="y") {printf class[c,cc]": "class_def[c,cc]" ; " >> "tmp_files/tmp0_"calculation_label"_"fam_count}
	}
	if(plot_graphs=="y") {print "" >> "tmp_files/tmp0_"calculation_label"_"fam_count}
}

if(plot_graphs=="y") {print "LENGTH N LENGTH_POPULATION" > "tmp_files/tmp_1_1_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "RESIDUE_TYPE % AVERAGE_PRESENCE_OF_AA_TYPE_PER_PEPTIDE" > "tmp_files/tmp_1_2_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "CLASS_TYPE % AVERAGE_PRESENCE_OF_AA_CLASS_PER_PEPTIDE" > "tmp_files/tmp_1_3_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "CLASS_TYPE % AVERAGE_PRESENCE_OF_AA_CLASS_PER_PEPTIDE" > "tmp_files/tmp_1_4_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "CLASS_TYPE % AVERAGE_PRESENCE_OF_AA_CLASS_PER_PEPTIDE" > "tmp_files/tmp_1_5_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "CLASS_TYPE % AVERAGE_PRESENCE_OF_AA_CLASS_PER_PEPTIDE" > "tmp_files/tmp_1_6_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "RESIDUE_TYPE N AVERAGE_NUMBER_OF_AA_TYPE_PER_PEPTIDE" > "tmp_files/tmp_1_7_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "CLASS_TYPE N AVERAGE_NUMBER_OF_AA_CLASS_PER_PEPTIDE" > "tmp_files/tmp_1_8_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "CLASS_TYPE N AVERAGE_NUMBER_OF_AA_CLASS_PER_PEPTIDE" > "tmp_files/tmp_1_9_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "CLASS_TYPE N AVERAGE_NUMBER_OF_AA_CLASS_PER_PEPTIDE" > "tmp_files/tmp_1_10_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "CLASS_TYPE N AVERAGE_NUMBER_OF_AA_CLASS_PER_PEPTIDE" > "tmp_files/tmp_1_11_"fam_count"_"calculation_label}
if(plot_graphs=="y") {print "RESIDUE_TYPE % AVERAGE_PERCENTAGE_OF_AA_TYPE_PER_REGION 3d" > "tmp_files/tmp_2_1_"fam_count"_"calculation_label}
ind=1
if(verbose=="y") {
	title("blue","-",100)
		print "LENGTH POPULATION"
		title("blue","-",100)
}
l=0 ; while(l<max_len) {l=l+1
	if(population[l]=="") {population[l]=0}
	if(verbose=="y") {
		printf l":"
			s=0 ; while(s<population[l]) {s=s+1 ; printf color("*","red")}; print ""
	}
	if(plot_graphs=="y") {print l,population[l],l >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
}
if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
ind=ind+1
if(verbose=="y") {
	title("blue","-",100)
		print "AVERAGE PRESENCE OF AA TYPE PER PEPTIDE"
		title("blue","-",100)
}
a=0 ; while(a<20) {a=a+1
	if(verbose=="y") {
		printf aa_name[a]":"
			s=0 ; while(s<int(count_average_aa_presence_per_peptide[a])) {s=s+1 ; printf color("*","red")}; print ""
	}
	if(plot_graphs=="y") {print a,count_average_aa_presence_per_peptide[a],aa_name[a] >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
}
if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
if(verbose=="y") { 
	title("blue","#",100)
		print "AVERAGE PRESENCE OF AA CLASS PER PEPTIDE"
		title("blue","#",100)             
}
delete count_class
c=0 ; while(c<class_number) {c=c+1	
	ind=ind+1
		cc=0 ; while(cc<class_length[c]) {cc=cc+1
			if(verbose=="y") {printf class[c,cc]":"} ; split(class_def[c,cc],comp,"")
				ccc=0; while(ccc<length(class_def[c,cc])) {ccc=ccc+1 ;
					count_class[c,cc]=count_class[c,cc]+count_average_aa_presence_per_peptide[conv_aa[comp[ccc]]]
				}
			if(verbose=="y") {
				s=0 ; while(s<int(count_class[c,cc])) {s=s+1 ; 
					if(c==1) {printf color("*","red")} ; 
					if(c==2) {printf color("*","green")} ; 
					if(c==3) {printf color("*","blue")} ; 
					if(c==4) {printf color("*","magenta")} ;}; 
				print ""
			}
			if(plot_graphs=="y") {print cc,count_class[c,cc],class[c,cc] >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
		}
	if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
}
if(verbose=="y") {
	title("blue","-",100)
		print "AVERAGE NUMBER OF AA TYPE PER PEPTIDE"
		title("blue","-",100)
}
ind=ind+1
a=0 ; while(a<20) {a=a+1
	if(verbose=="y") {
		printf aa_name[a]":"
			s=0 ; while(s<int(count_average_aa_type_number_per_peptide[a])) {s=s+1 ; printf color("*","red")}; print ""
	}
	if(plot_graphs=="y") {print a,count_average_aa_type_number_per_peptide[a],aa_name[a] >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
}
if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
if(verbose=="y") {
	title("blue","#",100)
		print "AVERAGE NUMBER OF AA CLASS PER PEPTIDE"
		title("blue","#",100)       
}     

delete count_class 
c=0 ; while(c<class_number) {c=c+1
	ind=ind+1
		cc=0 ; while(cc<class_length[c]) {cc=cc+1
			if(verbose=="y") {printf class[c,cc]":" ;}
			split(class_def[c,cc],comp,"")
				ccc=0; while(ccc<length(class_def[c,cc])) {ccc=ccc+1 ;
					count_class[c,cc]=count_class[c,cc]+count_average_aa_type_number_per_peptide[conv_aa[comp[ccc]]]
				}
			if(verbose=="y") {
				s=0 ; while(s<int(count_class[c,cc])) {s=s+1 ;
					if(c==1) {printf color("*","red")} ;
					if(c==2) {printf color("*","green")} ;
					if(c==3) {printf color("*","blue")} ;
					if(c==4) {printf color("*","magenta")} ;};
				print ""
			}
			if(plot_graphs=="y") {print cc,count_class[c,cc],class[c,cc] >> "tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label}
		}
	if(plot_graphs=="y") {close("tmp_files/tmp_1_"ind"_"fam_count"_"calculation_label)}
}
if(verbose=="y") {
	title("blue","#",100)
		print "AVERAGE PERCENTAGE OF AA TYPE PER REGION"
		title("blue","-",100)
}

r=0 ; while(r<n_regions) {r=r+1
	limit1=int((r-1)*upper/n_regions) ; limit2=int(r*upper/n_regions)
		if(verbose=="y") {print "region"r"("limit1"-"limit2"):"}
	a=0 ; while(a<20) {a=a+1
		if(verbose=="y") {
			printf aa_name[a]":"
		}
		p=limit1 ; while(p<limit2) {p=p+1
			count_average_aa_type_number_per_region[a,r]=count_average_aa_type_number_per_region[a,r]+count_aa_type_number_per_position[a,p]/dmax
		}
		if(count_average_aa_type_number_per_region[a,r]=="") {count_average_aa_type_number_per_region[a,r]=0}
		if(plot_graphs=="y") {print r,a,count_average_aa_type_number_per_region[a,r],"r:"limit1"-"limit2,aa_name[a] > "tmp_files/tmp_2_1_"fam_count"_"calculation_label}
		if(verbose=="y") {
			s=0 ; while(s<int(count_average_aa_type_number_per_region[a,r]*10)) {s=s+1 ; printf color(aa_name[a],"red")}; print ""
		}
	}
}

if(plot_graphs=="y") {close("tmp_files/tmp_2_1_"fam_count"_"calculation_label)}
delete count_average_aa_type_number_per_region
a=0 ; while(a<20) {a=a+1 
	if(plot_graphs=="y") {print "REGIONS % "aa_name[a] > "tmp_files/tmp_3_"a"_"fam_count"_"calculation_label}
	aa=0 ; while(aa<20) {aa=aa+1
		if(plot_graphs=="y") {print "DISTANCE % "aa_name[a] > "tmp_files/tmp_4_"a"_"aa"_"fam_count"_"calculation_label}
	}
	r=0 ; while(r<n_regions) {r=r+1
		limit1=int((r-1)*upper/n_regions) ; limit2=int(r*upper/n_regions)
			p=limit1 ; while(p<limit2) {p=p+1
				count_average_aa_type_number_per_region[a,r]=count_average_aa_type_number_per_region[a,r]+count_aa_type_number_per_position[a,p]/dmax
					aa=0 ; while(aa<20) {aa=aa+1
						count_average_correl_per_region[a,aa,r]=count_average_correl_per_region[a,aa,r]+count_correl[a,aa,r]/dmax
					}
			}
		if(plot_graphs=="y") {print r,count_average_aa_type_number_per_region[a,r]*100,limit1":"limit2 >> "tmp_files/tmp_3_"a"_"fam_count"_"calculation_label}
		aa=0 ; while(aa<20) {aa=aa+1
			if(plot_graphs=="y") {print r,count_average_correl_per_region[a,aa,r]*100,limit1":"limit2 >> "tmp_files/tmp_4_"a"_"aa"_"fam_count"_"calculation_label}
		}
	}
	if(plot_graphs=="y") {	close("tmp_files/tmp_3_"a"_"fam_count"_"calculation_label)}
	aa=0 ; while(aa<20) {aa=aa+1
		if(plot_graphs=="y") {	close("tmp_files/tmp_4_"a"_"aa"_"fam_count"_"calculation_label)}
	}
}
title("blue","-",100)
if(plot_graphs=="y") {print "Family "fam_count" "calculation_label >> "tmp_files/tmp0_"calculation_label"_"fam_count }
if ((fam_size[families_to_analyse]>1 && design_family_representative_peptide=="y") || calculate_only_global_properties=="y") {
	print "SUMMARY PER REGION (in order of probability)"
		title("blue","-",100)
		r=0 ; while(r<n_regions) {r=r+1
			delete conv_index
				sort_nD(count_average_aa_type_number_per_region,conv_index,"@>","no","v",r,"","","","",1,20)
				printf ("%3d: ",r)
				ar=0 ; while(ar<20) {ar=ar+1
					perc[ar,r]=count_average_aa_type_number_per_region[conv_index[ar],r]*100
						if (perc[ar,r]>10) { printf ("%1s (%3.1f%) ",aa_name[conv_index[ar]],perc[ar,r]) }
				}
			if(plot_graphs=="y") {
				aa=0 ; while(aa<20) {aa=aa+1
					print "distance_in_aa % position_"r"("aa_name[conv_index[1]]")" > "tmp_files2/tmp_5_"r"_"aa"_"fam_count"_"calculation_label
						pp=0; while(pp<n_regions) {pp=pp+1
							print pp,count_correl_pos[r,conv_index[1],aa,pp]/dmax*100,pp >> "tmp_files2/tmp_5_"r"_"aa"_"fam_count"_"calculation_label
						}
					close("tmp_files2/tmp_5_"r"_"aa"_"fam_count"_"calculation_label)
				}
			}
			print ""
		}
	max_percent[fam_count]=0
		if(plot_graphs=="y") {printf "" > "tmp_files/tmp00_"calculation_label}
	title("blue","-",100)
		print "SUMMARY PER POSITION WITHIN THE PEPTIDE (in order of propability and coloured by polarity)"
		title("blue","-",100)
		r=0 ; while(r<n_regions) {r=r+1
			if(plot_graphs=="y") {print "POSSIBLE_SEQUENCE % PROBABILITY_WITHIN_THE_PEPTIDE_FROM_POS"r > "tmp_files2/tmp_6_"r"_"fam_count"_"calculation_label}
			printf ("%3d: ",r) ; if(plot_graphs=="y") {printf ("%3d: ",r) >> "tmp_files/tmp00_"calculation_label}
			delete conv_index
				sort_nD(count_average_aa_type_number_per_region,conv_index,"@>","no","v",r,"","","","",1,20)
				if(plot_graphs=="y") {printf aa_name[conv_index[1]] >> "tmp_files/tmp0_"calculation_label"_"fam_count}
			pp=0; while(pp<n_regions) {pp=pp+1
				delete conv_index_2
					delete conv_index_3
					if (pp==r) {
#printf ("%1s",aa_name[conv_index[1]]) ;
						best_seq[fam_count,r]=best_seq[fam_count,r]""aa_name[conv_index[1]] 
							if(count_average_aa_type_number_per_region[conv_index[1],r]==0) {printf "-"} else {color(aa_name[conv_index[1]],color_type[polarity[aa_name[conv_index[1]]]],"","bold");}
						if(plot_graphs=="y") {printf aa_name[conv_index[1]] >> "tmp_files/tmp00_"calculation_label} ; percent=100
							if(plot_graphs=="y") {print pp,percent,aa_name[conv_index[1]] > "tmp_files2/tmp_6_"r"_"fam_count"_"calculation_label}
					}
					else {
						if(pp>r || pp<r) {sort_nD(count_correl_pos,conv_index_2,"@>","no",r,conv_index[1],"v",pp-r,"","","","",1,n_regions,1,20,1,20);
							percent=count_correl_pos[r,conv_index[1],conv_index_2[1],pp-r]/dmax*100}
						if(percent >0) {if(plot_graphs=="y") {print pp,percent,aa_name[conv_index_2[1]] > "tmp_files2/tmp_6_"r"_"fam_count"_"calculation_label ;} 
							total_percent[fam_count,r]=total_percent[fam_count,r]+percent
								best_seq[fam_count,r]=best_seq[fam_count,r]""aa_name[conv_index_2[1]]
						}
						if(percent >10) {
#printf ("%1s",aa_name[conv_index_2[1]]); 
							color(aa_name[conv_index_2[1]],color_type[polarity[aa_name[conv_index_2[1]]]],"","bold")
								if(plot_graphs=="y") {printf ("%1s",aa_name[conv_index_2[1]]) >> "tmp_files/tmp00_"calculation_label}}
						else {printf "-"; if(plot_graphs=="y") {printf "-" >> "tmp_files/tmp00_"calculation_label}}
					}
#	if(pp<r) {sort_nD(count_correl_pos,conv_index_3,"@>","no",r,conv_index[1],"v",r-pp,"","","","",1,n_regions,1,20,1,20);printf ("%1s",aa_name[conv_index_3[1]])}
			}
			print ""
				if(plot_graphs=="y") {print "" >> "tmp_files/tmp00_"calculation_label}
			if(plot_graphs=="y") {close("tmp_files2/tmp_6_"r"_"fam_count"_"calculation_label)}
			if(total_percent[fam_count,r] > max_percent[fam_count]) {max_percent[fam_count]=total_percent[fam_count,r]; best_index[fam_count]=r }
		}
	label=""
		d=0;while(d<dmax) {d=d+1 
			if (best_seq[fam_count,best_index[fam_count]]==seq[d]) {label=short_prp[d]}
		}
	if (label=="") {label="new"}
	print ""
		print "Best representing sequence is sequence "best_index[fam_count]":"best_seq[fam_count,best_index[fam_count]]
		print "" 
		pr=ChouFasman(best_seq[fam_count,best_index[fam_count]],"",label,"")
		pr=ChouFasman(best_seq[fam_count,best_index[fam_count]],"",label,DSSP_[fam_count,best_index[fam_count]])
		print ""
} 
if(plot_graphs=="y") {close("tmp_files/tmp0_"calculation_label"_"fam_count)}
if(plot_graphs=="y") {close("tmp_files/tmp00_"calculation_label)}
print "End of analysis for family "fam_count""

if(plot_graphs=="y") {
		system("mkdir -p OUTPUTS/"calculation_label"/plot_graphs")
		printf calculation_label  > "tmp_files/tmp0"
		print "Generating files graphs1 to 6 and FASTA_OUTPUT files for family "fam_count
		system("fam_count="fam_count" python python/plot_graph.py "calculation_label"")
}
#       system("find tmp_files/tmp* -delete");system("find tmp_files2/tmp* -delete");
}
}
