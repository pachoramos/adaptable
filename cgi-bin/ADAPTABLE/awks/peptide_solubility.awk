function peptide_solubility(sequence,\
l,lmax,count_polar,count_hydrophobic,count_charged,perc_charged,perc_hydrophobic) {
lmax=split(sequence,res,"")
count_polar=0
count_hydrophobic=0
count_charged=0
l=0; while(l<lmax) {l=l+1
if(res[l]=="G" || res[l]=="A" || res[l]=="S" || res[l]=="T" || res[l]=="C" || res[l]=="N" || res[l]=="Q" || res[l]=="P") {count_polar=count_polar+1}
if(res[l]=="F" || res[l]=="I" || res[l]=="L" || res[l]=="M" || res[l]=="V" || res[l]=="W" || res[l]=="Y") {count_hydrophobic=count_hydrophobic+1}
if(res[l]=="K" || res[l]=="R" || res[l]=="H" || res[l]=="D" || res[l]=="E") {count_charged=count_charged+1}
}
perc_charged=count_charged/lmax
perc_hydrophobic=count_hydrophobic/lmax
if(lmax<5) {if(perc_hydrophobic<1) {return "soluble"} else {return "poorly_soluble"}}
else {
	if(perc_hydrophobic<0.5) {
		if(perc_charged>0.25) {return "soluble"} else {return "poorly_soluble"}
	}
	else {if(perc_hydrophobic<0.75) {return "almost_insoluble"} else {return "insoluble"}
	}
}
}
