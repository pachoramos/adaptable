function analyze_organism(nom_organism,sequence,\
		field2,field3) {
	delete ee
		gsub("aumanii","aumannii",nom_organism)
		gsub("auerus","aureus",nom_organism)
		gsub("ecalis","aecalis",nom_organism)
		gsub("Terrigena","terrigena",nom_organism)
		gsub("pneumonia\\(","pneumoniae(",nom_organism)
		gsub("pmeumoniae","pneumoniae",nom_organism)
		gsub("cinera","cinerea",nom_organism)
		gsub("faaecalis","faecalis",nom_organism)
		gsub("faecilis","faecalis",nom_organism)
		gsub("innocus","innocua",nom_organism)
		gsub("delbruekii","delbrueckii",nom_organism)
		gsub("hemolyticus","haemolyticus",nom_organism)
		gsub("stiptis","stipitis",nom_organism)
		gsub("influenza\\(","influenzae(",nom_organism)
		gsub("asbriae","asburiae",nom_organism)
		gsub("harizianum","harzianum",nom_organism)
		gsub("phei","phlei",nom_organism)
		gsub("mesenteriodes","mesenteroides",nom_organism)
		gsub("harveyis","harveyi",nom_organism)
		gsub("multoci\\(","multocida(",nom_organism)
		gsub("plantarun","plantarum",nom_organism)
		gsub("guiller-mondii","guillermondii",nom_organism)
		gsub("dysenteria\\(","dysenteriae(",nom_organism)
		gsub("solacearum","solanacearum",nom_organism)
		gsub("sorokinina","sorokiniana",nom_organism)
		gsub("Salmonellar","Salmonella",nom_organism)
		gsub("F._oxysporum","Fusarium_oxysporum",nom_organism)
		gsub("Streptococcal","Streptococcus",nom_organism)
		gsub("michiganense","michiganensis",nom_organism)
		gsub("herpes","Herpes",nom_organism)		
		gsub("Rhataybacter","Rathayibacter",nom_organism)
		gsub("parahamelytics","parahaemolyticus",nom_organism)
		gsub("magaterium","megaterium",nom_organism)
		gsub("MRSA","Staphylococcus_aureus_MRSA",nom_organism)
		gsub("VRE","Enterococcus_VRE",nom_organism)
		gsub("VRSA","Staphylococcus_aureus_VRSA",nom_organism)
		gsub("Methicillin-resistant_S._aureus","Staphylococcus_aureus_MRSA",nom_organism)
		gsub("methicillin-resistant_S._aureus","Staphylococcus_aureus_MRSA",nom_organism)
		gsub("Methicillin-resistant_coagulase-_S._aureus","Staphylococcus_aureus_MR-CoNS",nom_organism)
		gsub("methicillin-resistant_coagulase-_S._aureus","Staphylococcus_aureus_MR-CoNS",nom_organism)
		gsub("Methicillin-resistant_coagulase-_Staphylococcus_aureus","Staphylococcus_aureus_MR-CoNS",nom_organism)
		gsub("methicillin-resistant_coagulase-_Staphylococcus_aureus","Staphylococcus_aureus_MR-CoNS",nom_organism)
                gsub("Methicillin-resistant_Staphylococcus_aureus","Staphylococcus_aureus_MRSA",nom_organism)
                gsub("methicillin-resistant_Staphylococcus_aureus","Staphylococcus_aureus_MRSA",nom_organism)
                gsub("methicillin-sensitive_S._aureus","Staphylococcus_aureus_MSSA",nom_organism)
                gsub("Methicillin-sensitive_S._aureus","Staphylococcus_aureus_MSSA",nom_organism)
                gsub("Methicillin-sensitive_Staphylococcus_aureus ","Staphylococcus_aureus_MSSA",nom_organism)
                gsub("methicillin-sensitive_Staphylococcus_aureus ","Staphylococcus_aureus_MSSA",nom_organism)
                gsub("Vancomycin-resistant_S._aureus","Staphylococcus_aureus_VRSA",nom_organism)
                gsub("vancomycin-resistant_S._aureus","Staphylococcus_aureus_VRSA",nom_organism)
                gsub("streptococci","Streptococcus",nom_organism)
                gsub("Viridans_Streptococcus","Viridans_streptococci",nom_organism)
		gsub("Vibriopenaeicida","Vibrio_penaeicida",nom_organism)
		gsub("jaaecalis","faecalis",nom_organism)
		gsub("fermenti","fermentum",nom_organism)
		gsub("typhymurium","typhimurium",nom_organism)
		gsub("S._typhimurium","Salmonella_typhimurium",nom_organism)
		gsub("S._Typhimurium","Salmonella_typhimurium",nom_organism)
		gsub("X._japonicus","Xenorhabdus_japonicus",nom_organism)
		gsub("B._thuringiensis","Bacillus_thuringiensis",nom_organism)
		gsub("subtilus","subtilis",nom_organism)
		gsub("falciparam","falciparum",nom_organism)
		gsub("a_n_that_causes_Chagas'_disease","Trypanosoma_cruzi",nom_organism)
                gsub("S._newport","Salmonella_newport",nom_organism)
                gsub("R._norvegicus","Rat",nom_organism)
                gsub("H._sapiens","Human",nom_organism)
                gsub("immobilisc","immobilis",nom_organism)
                gsub("ruckeric","ruckeri",nom_organism)
                gsub("Y._uckeri","Yersinia_ruckeri",nom_organism)
                gsub("M._musculus","Mus_musculus",nom_organism)
		gsub("S._aureu_mprF","Staphylococcus_aureus_mprF",nom_organism)
		gsub("megateriurn","megaterium",nom_organism)
		gsub("S._pyogenes3_hem","Streptococcus_pyogenes",nom_organism)
		gsub("ablicans","albicans",nom_organism)
		gsub("Gonococci","Neisseria_gonorrhoeae",nom_organism)
		gsub("hepaptitis","hepatitis",nom_organism)
		gsub("Strep._","S._",nom_organism)		
		gsub("Acetinobacter","Acinetobacter",nom_organism)
		gsub("Enterococci","Enterococcus",nom_organism)
		gsub(/^clostridium/,"Clostridium",nom_organism)
		gsub("_Enteritidis","_enteritidis",nom_organism)
		gsub("_Choleraesuis","_choleraesuis",nom_organism)
		gsub("_Gallinarum","_gallinarum",nom_organism)
		gsub("Mycophaerella","Mycosphaerella",nom_organism)
		gsub("dameselae","damselae",nom_organism)
		gsub("marsescens","marcescens",nom_organism)
		gsub("E._coli","Escherichia_coli",nom_organism)
		gsub("epidermis","epidermidis",nom_organism)
				
		if(nom_organism=="UA_140") {nom_organism="S._mutans"}
		if(nom_organism=="Z61_supersusceptible") {nom_organism="Pseudomonas_aeruginosa_Z61"}
		if(nom_organism=="UB1005_parent_of_DC2") {nom_organism="Escherichia_coli_UB1005_parent_of_DC2"}
		
		gsub("?","",nom_organism)
		split(nom_organism,ee,"(");
		gsub("=)","",ee[2])
		gsub(">=","=",ee[2])
		gsub("=>","=",ee[2])
		gsub(">","=",ee[2])
		gsub("<=","=",ee[2])
		gsub("=<","=",ee[2])
		gsub("<","=",ee[2])
		if(ee[2]!="=)" && ee[2]!=")" && ee[2]!="") {
			experimental_[sequence]="experimental"
			field="("ee[2]
			activity=get_number_units(field,sequence)
		}
		gsub("\\.","._",ee[1])
		gsub("__","_",ee[1])
		split(ee[1],ff,"_") ; if(ff[2]!="") {field3=ff[1]"_"ff[2]} else {field3=ff[1]}
	if(ee[2]~/\)/){nom_organism_clean=ee[1]""activity} else {nom_organism_clean=ee[1]}
	field3=clean_string(field3)
		if(full_microbe_name[field3]!="") {
			field2=gram[field3]
			read_database_classify_field(field2,",",sequence,cyclic_,synthetic_,antimicrobial_,antibacterial_,\
						antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
						antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
						cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
						tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_)
			field2=kingdom_microbe[field3]
			read_database_classify_field(field2,",",sequence,cyclic_,synthetic_,antimicrobial_,antibacterial_,\
						antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
						antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
						cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
						tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_)

			field2=genus_microbe[field3]
			read_database_classify_field(field2,",",sequence,cyclic_,synthetic_,antimicrobial_,antibacterial_,\
						antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
						antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
						cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
						tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_)

			if(e[2]~/\)/) {field=full_microbe_name[field3]"("e[2]}
			else {field=full_microbe_name[field3]""e[2]}

			if(biofilm[field3]=="biofilm") {
				if(biofilm_[sequence]!~/biofilm/) {biofilm_[sequence]="biofilm"biofilm_[sequence]";"}
				if(biofilm_[sequence]!~full_microbe_name[field3]) {if(ff[3]!="") {biofilm_[sequence]=biofilm_[sequence]""full_microbe_name[field3]"_"ff[3]";"} else {biofilm_[sequence]=biofilm_[sequence]""full_microbe_name[field3]";"}}
			}

		} else {
                        read_database_classify_field(field3,",",sequence,cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
		}
	if(full_microbe_name[field3]!="") {gsub(field3,full_microbe_name[field3],nom_organism_clean)}
	return nom_organism_clean
}
