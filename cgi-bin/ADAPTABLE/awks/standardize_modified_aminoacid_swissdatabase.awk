@include "awks/library.awk"
BEGIN{
# execute as awk -f standardize_modified_aminoacid_swissdatabase.awk DATABASES/swiss_sidechain_database
# a file with unstandardized names must be provided "DATABASES/modified_aa_names"
input_file="DATABASES/modified_aa_names"
 
	show_only_the_best="yes"
	conv_OLC_["alanine"]="A";
	conv_OLC_["valine"]="V";
	conv_OLC_["leucine"]="L";
	conv_OLC_["isoleucine"]="I";
	conv_OLC_["proline"]="P";
	conv_OLC_["phenylalanine"]="F";
	conv_OLC_["tryptophan"]="W";
	conv_OLC_["methionine"]="M";
	conv_OLC_["cysteine"]="C";
	conv_OLC_["glycine"]="G";
	conv_OLC_["asparagine"]="N";
	conv_OLC_["glutamine"]="Q";
	conv_OLC_["serine"]="S";
	conv_OLC_["threonine"]="T";
	conv_OLC_["tyrosine"]="Y";
	conv_OLC_["lysine"]="K";
	conv_OLC_["arginine"]="R";
	conv_OLC_["histidine"]="H";
	conv_OLC_["aspartate"]="D";
	conv_OLC_["asparticacid"]="D";
	conv_OLC_["aspartic-acid"]="D";
	conv_OLC_["glutamate"]="E";
	conv_OLC_["glutamicacid"]="E";
	conv_OLC_["glutamic-acid"]="E";
	conv_OLC_["ornithine"]="O";
	weight["A"]=89.1
	weight["R"]=174.2
	weight["N"]=132.1
	weight["D"]=133.1
	weight["C"]=121.2
	weight["E"]=147.1
	weight["Q"]=146.2
	weight["G"]=75.1
	weight["H"]=155.2
	weight["I"]=131.2
	weight["L"]=131.2
	weight["K"]=146.2
	weight["M"]=149.2
	weight["F"]=165.2
	weight["P"]=115.1
	weight["S"]=105.1
	weight["T"]=119.1
	weight["W"]=204.2
	weight["Y"]=181.2
	weight["V"]=117.1
	weight["O"]=178.23
i=0
        i=i+1;chemical_groups[i]="val"; weight[i]=117.1 
        i=i+1;chemical_groups[i]="isoleuc"; weight[i]=131.2
        i=i+1;chemical_groups[i]="leuc"; weight[i]=131.2
        i=i+1;chemical_groups[i]="prol"; weight[i]=115.1
        i=i+1;chemical_groups[i]="phenylalan"; weight[i]=165.2
        i=i+1;chemical_groups[i]="alan"; weight[i]=89.1
        i=i+1;chemical_groups[i]="tryptophan"; weight[i]=204.2
        i=i+1;chemical_groups[i]="methion"; weight[i]=149.2
        i=i+1;chemical_groups[i]="cyste"; weight[i]=121.2
        i=i+1;chemical_groups[i]="glyc"; weight[i]=75.1
        i=i+1;chemical_groups[i]="asparag"; weight[i]=132.1
        i=i+1;chemical_groups[i]="glutam"; weight[i]=146.2
        i=i+1;chemical_groups[i]="ser";  weight[i]=105.1
        i=i+1;chemical_groups[i]="threon"; weight[i]=119.1
        i=i+1;chemical_groups[i]="tyros"; weight[i]=181.2
        i=i+1;chemical_groups[i]="lys";  weight[i]=146.2
        i=i+1;chemical_groups[i]="argin"; weight[i]=174.2
        i=i+1;chemical_groups[i]="histid"; weight[i]=155.2
        i=i+1;chemical_groups[i]="aspart"; weight[i]=133.1
        i=i+1;chemical_groups[i]="aspartate"; weight[i]=133.1
        i=i+1;chemical_groups[i]="glutam";weight[i]=147.1
        i=i+1;chemical_groups[i]="glutamate";weight[i]=147.1
        i=i+1;chemical_groups[i]="ornith"; weight[i]=132.16

    #    i=i+1;chemical_groups[i]="oic "; weight[i]=44
    	i=i+1;chemical_groups[i]="acid"; weight[i]=45
        i=i+1;chemical_groups[i]="meth"; weight[i]=15
        i=i+1;chemical_groups[i]="eth"; weight[i]=12*2+5
        i=i+1;chemical_groups[i]="fluor"; weight[i]=19
        i=i+1;chemical_groups[i]="chloro"; weight[i]=35
	i=i+1;chemical_groups[i]="bromo"; weight[i]=35
	i=i+1;chemical_groups[i]="iodo"; weight[i]=253.8/2
        i=i+1;chemical_groups[i]="amin"; weight[i]=16
        i=i+1;chemical_groups[i]="carbox"; weight[i]=45
        i=i+1;chemical_groups[i]="hydroxy"; weight[i]=17
        i=i+1;chemical_groups[i]="propion"; weight[i]=74.08
	i=i+1;chemical_groups[i]="propan"; weight[i]=43
	i=i+1;chemical_groups[i]="propyl"; weight[i]=43
        i=i+1;chemical_groups[i]="butyr"; weight[i]=87
        i=i+1;chemical_groups[i]="butyl"; weight[i]=57
	i=i+1;chemical_groups[i]="butan"; weight[i]=57
        i=i+1;chemical_groups[i]="benz"; weight[i]=78
	i=i+1;chemical_groups[i]="phen"; weight[i]=78
        i=i+1;chemical_groups[i]="naphth"; weight[i]=128
        i=i+1;chemical_groups[i]="laur"; weight[i]=200
        i=i+1;chemical_groups[i]="glyco"; weight[i]=180
        i=i+1;chemical_groups[i]="fructo"; weight[i]=180
        i=i+1;chemical_groups[i]="manno"; weight[i]=180
        i=i+1;chemical_groups[i]="sulph"; weight[i]=96
        i=i+1;chemical_groups[i]="sulf"; weight[i]=96
	i=i+1;chemical_groups[i]="phosph"; weight[i]=94.97
	i=i+1;chemical_groups[i]="acet"; weight[i]=15+12+16
	i=i+1;chemical_groups[i]="amid"; weight[i]=16
	i=i+1;chemical_groups[i]="nitro"; weight[i]=16*2+14
	i=i+1;chemical_groups[i]="guanid"; weight[i]=59.07
	i=i+1;chemical_groups[i]="azid"; weight[i]=14*3
	i=i+1;chemical_groups[i]="cyan"; weight[i]=14+12
	i=i+1;chemical_groups[i]="citrull"; weight[i]=175.18
	i=i+1;chemical_groups[i]="carbam"; weight[i]=12+16+14+2
        i=i+1;chemical_groups[i]="succin"; weight[i]=118
        i=i+1;chemical_groups[i]="pent"; weight[i]=(12+2)*5+1
        i=i+1;chemical_groups[i]="hex"; weight[i]=(12+2)*6+1
        i=i+1;chemical_groups[i]="ept"; weight[i]=(12+2)*7+1
        i=i+1;chemical_groups[i]="oct"; weight[i]=(12+2)*8+1
        i=i+1;chemical_groups[i]="nona"; weight[i]=(12+2)*9+1
        i=i+1;chemical_groups[i]="undeca"; weight[i]=(12+2)*11+1
        i=i+1;chemical_groups[i]="dodeca"; weight[i]=(12+2)*12+1
        i=i+1;chemical_groups[i]="deca"; weight[i]=(12+2)*10+1
        i=i+1;chemical_groups[i]="myristo"; weight[i]=(12+2)*14+44-12
        i=i+1;chemical_groups[i]="oleo"; weight[i]=282
        i=i+1;chemical_groups[i]="oleic"; weight[i]=282
        i=i+1;chemical_groups[i]="palm"; weight[i]=256
        i=i+1;chemical_groups[i]="stear"; weight[i]=284
	i=i+1;chemical_groups[i]="anthracen"; weight[i]=178.23
        i=i+1;chemical_groups[i]="sarcos"; weight[i]=89.09
	i=i+1;chemical_groups[i]="glutathion"; weight[i]=307.32
        i=i+1;chemical_groups[i]="pyruv"; weight[i]=88.06
	i=i+1;chemical_groups[i]="adamant"; weight[i]=136.24
	i=i+1;chemical_groups[i]="pyrrolid"; weight[i]=71.12
        i=i+1;chemical_groups[i]="kynuren"; weight[i]=208.22
        i=i+1;chemical_groups[i]="diazep"; weight[i]=94.11
        i=i+1;chemical_groups[i]="piperid"; weight[i]=85.15


	j=0

	j=j+1;suffix[j]="tetra-" ; factor[j]=4
	j=j+1;suffix[j]="tetra" ; factor[j]=4 
	j=j+1;suffix[j]="tri-" ; factor[j]=3
	j=j+1;suffix[j]="tri" ; factor[j]=3
	j=j+1;suffix[j]="di-" ; factor[j]=2
	j=j+1;suffix[j]="di" ; factor[j]=2
	j=j+1;suffix[j]="mono-" ; factor[j]=1
	j=j+1;suffix[j]="mono" ; factor[j]=1
	j=j+1;suffix[j]="" ; factor[j]=1
	j=j+1;suffix[j]="iso" ; factor[j]=1
	j=0
	j=j+1;postfix[j]="yl" ; factor_[j]=1
	j=j+1;postfix[j]="ine" ; factor_[j]=1
	j=j+1;postfix[j]="" ; factor_[j]=1





# The names of modified amax aminoacids found in different databases are read and put in the arrays orig_name and names (the "clean" and tolower version of orig_names)
	a=0
	while ( getline < input_file >0 ) {
		a=a+1
			orig_name[a]=$1
			name[a]=tolower($1)
			n=0; while(n<length(chemical_groups)) { n=n+1;
				nn=0;  while(nn<length(suffix)) { nn=nn+1;
					mm=0;  while(mm<length(postfix)) {mm=mm+1;
						if(chemical_groups[n]!="") {
						composed_name=suffix[nn]""chemical_groups[n]""postfix[mm]
						b=split(name[a],bb,composed_name); 
						if(b>1) {MW[a]=MW[a]+(b-1)*weight[n]*factor[nn]*factor_[mm]
						gsub(composed_name,"",name[a])
						if (n<23) {simplification[a]=chemical_groups[n]"ine"; 
						if(chemical_groups[n]~/tryptophan/) {simplification[a]="tryptophan"}
						if(chemical_groups[n]~/glutama/) {simplification[a]="glutamate"}
						if(chemical_groups[n]~/aspart/) {simplification[a]="aspartate"}
						} 
						#print orig_name[a],name[a],MW[a],b,composed_name
						}
					}
					}
				}
			}
			print name[a]
		name[a]=tolower($1)
	#	print orig_name[a],name[a],MW[a]
	}
	amax=a
		m=0
}
{
# The names of modified mmax aminoacids found in swiss official database are read and split by "-" into fragments. If oen fragment is contained in names, it increases the percentage of similarity.
# Also, the difference is length between each element of name array and the candidate name of the swiss database is computed. In this phase, the nature (natural or non-natural), 
# the family (eg homocysteine coles from the family cysteine) and teh molecular weight of teh candidates are stored. The conversion matrix potentially converts the our names into the ones of teh swiss database.
	m=m+1
		$2=tolower(clean_string($2))
		nmax=split($2,aa,"-")
		n=0; while(n<nmax) { n=n+1
			a=0; while(a<amax) { a=a+1
				if(name[a]~aa[n] && length(aa[n])>0) {
					probability[a,m]=probability[a,m]+length(aa[n]); 
					percentage[a,m]=percentage[a,m]+length(aa[n])/length(name[a])*100; 
					length_diff[a,m]=((length(name[a])-length($2))**2)**0.5
						weight[a,m]=$5
						family[a,m]=$(NF-1)
						nature[a,m]=$NF
						conversion[a,m]=$2; 
#print $2,aa[n],name[a],probability[a,m],m
				}
			}
		}
	mmax=m
}
END{
# all candidates of the swiss database are evaluated to be represenattive of our starting names and sorted according to thrir percental similarity. 
# Furthermore, if the percentage of similarity is more than 80% and the difference in word lengths is smaller than 5 letters; the candidates are retained. If the option show_only_the_best is set to "yes"
# the program shows only the best candidate, otherwide it shows the candidates in order of decreasing similarity.
	a=0; while(a<amax) { a=a+1
		tag=""
#		delete conv_index
#			sort_nD(probability,conv_index,">","no",a,"v","","","","","","",1,mmax)
			delete conv_index
			sort_nD(percentage,conv_index,">","no",a,"v","","","","","","",1,mmax)
			k=1; while(conv_index[k]!="") {
				if((percentage[a,conv_index[k]]>80 && length_diff[a,conv_index[k]]<5) || conv_OLC_[name[a]]!="" || (conv_OLC_[substr(name[a],2,length(name[a]))]!="" && substr(name[a],1,1)=="d")) {	
#print name[a],conversion[a,conv_index[k]],percentage[a,conv_index[k]],weight[a,conv_index[k]],family[a,conv_index[k]],nature[a,conv_index[k]]

# D and L isomers have high similarity and length. We must therefore recognise them:
					try="d"family[a,conv_index[k]]; try2="d-"family[a,conv_index[k]]
						if((conversion[a,conv_index[k]]~try || conversion[a,conv_index[k]]~try2) && conv_OLC_[family[a,conv_index[k]]]!="") {
							gsub(try,"D-"family[a,conv_index[k]],conversion[a,conv_index[k]])
								gsub(try2,"D-"family[a,conv_index[k]],conversion[a,conv_index[k]])
						}
					onecode="X"
# Since the swiss database does not contains all the 20 D- and L-aminoacids that are present in our name list, we artificially create them
						if(conv_OLC_[name[a]]!="") {onecode=conv_OLC_[name[a]];tag="L-";conversion[a,conv_index[k]]=name[a];weight[a,conv_index[k]]=weight[onecode];family[a,conv_index[k]]=name[a];nature[a,conv_index[k]]="natural"}
					if(conv_OLC_[substr(name[a],2,length(name[a]))]!="" && substr(name[a],1,1)=="d") {onecode=tolower(conv_OLC_[substr(name[a],2,length(name[a]))]);tag="D-";conversion[a,conv_index[k]]=substr(name[a],2,length(name[a]));
						weight[a,conv_index[k]]=weight[toupper(onecode)];family[a,conv_index[k]]=substr(name[a],2,length(name[a])); nature[a,conv_index[k]]="D-aminoacid"}

					print "translate_modif[\""orig_name[a]"\"]=\""onecode"\";"
						print "official_name[\""orig_name[a]"\"]=\""tag""conversion[a,conv_index[k]]"\";"
						print "weight[\""orig_name[a]"\"]="weight[a,conv_index[k]]";"
						print "simplification[\""orig_name[a]"\"]=\""conv_OLC_[family[a,conv_index[k]]]"\";"
						if(substr(orig_name[a],1,2)=="d-" || substr(conversion[a,conv_index[k]],1,2)=="d-" ) {
						print "nature[\""orig_name[a]"\"]=\"D-aminoacid\";"} 
						else {print "nature[\""orig_name[a]"\"]=\""nature[a,conv_index[k]]"\";"}
						print ""
						used[name[a]]=1
				}
				k=k+1
					if(show_only_the_best=="yes") {conv_index[k]=""}
			}
		if(used[name[a]]=="") {
					print "#WARNING "name[a]" was not found, trying to get missing information"; 
					print "translate_modif[\""orig_name[a]"\"]=\""onecode"\";"
					print "official_name[\""orig_name[a]"\"]=\""orig_name[a]"\";"
					print "weight[\""orig_name[a]"\"]="MW[a]";"
					print "simplification[\""orig_name[a]"\"]=\""conv_OLC_[simplification[a]]"\";"
					if(substr(orig_name[a],1,2)=="d-") {print "nature[\""orig_name[a]"\"]=\"D-aminoacid\";"} else {print "nature[\""orig_name[a]"\"]=\"non-natural\";"}
					print ""
					}
	}
}
