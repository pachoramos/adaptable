function find_compound_in_PubChem(name_,number_of_tries,minimum_percentage_of_similarity,name_separated_by_hyphens,method,strip_hyphens,hyphen2space,INPUT_OUTPUT,official_name,official_alternative_names,weight,simplification,id,\
		i,n) {
# It looks for chemical compounds in PubChem 
	conv_OLC_["alanine"]="A";
	conv_OLC_["valine"]="V";
	conv_OLC_["leucine"]="L";
	conv_OLC_["isoleucine"]="I";
	conv_OLC_["proline"]="P";
	conv_OLC_["phenylalanine"]="F";
	conv_OLC_["tryptophan"]="W";
	conv_OLC_["methionine"]="M";
	conv_OLC_["cysteine"]="C";
	conv_OLC_["glycine"]="G";
	conv_OLC_["asparagine"]="N";
	conv_OLC_["glutamine"]="Q";
	conv_OLC_["serine"]="S";
	conv_OLC_["threonine"]="T";
	conv_OLC_["tyrosine"]="Y";
	conv_OLC_["lysine"]="K";
	conv_OLC_["arginine"]="R";
	conv_OLC_["histidine"]="H";
	conv_OLC_["aspartate"]="D";
	conv_OLC_["asparticacid"]="D";
	conv_OLC_["aspartic-acid"]="D";
	conv_OLC_["glutamate"]="E";
	conv_OLC_["glutamicacid"]="E";
	conv_OLC_["glutamic-acid"]="E";
	conv_OLC_["ornithine"]="O";

	if(number_of_tries=="") {number_of_tries=25}
	if(minimum_percentage_of_similarity=="") {minimum_percentage_of_similarity=75}
	if(name_separated_by_hyphens!="") {name_tmp=tolower(name_separated_by_hyphens)} else {name_tmp=tolower(name_)}
	name_tmp2=name_tmp
	name_tmp=clean_string(name_tmp)
	if(name_tmp!="enduracididine")	{gsub("acid"," acid",name_tmp)}

	# Remove shit
	gsub("palmitatepal","palmitate",name_tmp)
	gsub("orinithine","ornithine",name_tmp)
	gsub("oxo-","oxo",name_tmp)
	gsub("-proline"," proline",name_tmp)
	gsub("mehyl","methyl",name_tmp)
	gsub("ated","",name_tmp)
	gsub("residue","",name_tmp)
	gsub("isosteric","",name_tmp)
	gsub("analog"," ",name_tmp)
	gsub("inwhichthesidechainisonemethylenegrouplongerthanincysteine","",name_tmp)
	gsub("anddisulfidebondbetweencysandpen","",name_tmp)
	gsub("groupboundtothe","",name_tmp)
	gsub("aci$"," acid",name_tmp)
	gsub("aaa2,2","",name_tmp)
	gsub("aaa1,1","",name_tmp)
	gsub("group"," group",name_tmp)
        gsub("with"," ",name_tmp)
        gsub("sidechain"," ",name_tmp)
        gsub("side group"," ",name_tmp) 
	gsub("adamantyl","adamantyl-",name_tmp)
	gsub("betaasparticacid","betaaspartic acid",name_tmp)
	gsub("sulfone"," sulfone ",name_tmp)			
	gsub("indole"," indole ",name_tmp)
	gsub("anti-symmetric"," asymmetric",name_tmp)
	gsub("symmetric","symmetric ",name_tmp)
	gsub("azido","azido ",name_tmp)
	gsub("homo-","homo",name_tmp)
	gsub("functionalized","",name_tmp)
	gsub("containingamino acid","",name_tmp)
	gsub("oftheamidenitrogen","",name_tmp)
	gsub("oftheamide","",name_tmp)
	gsub("homophenylalaninehyp","homophenylalanine",name_tmp)
	gsub("norleucinehyp","norleucine",name_tmp)
	gsub("2-aminobutyric acidglut","2-aminobutyric acid",name_tmp)
	gsub("2-aminobutyric acidsuc","2-aminobutyric acid",name_tmp)
	gsub("d-gamma-carboxyglutamic acidcha","d-gamma-carboxyglutamic acid",name_tmp)
	gsub("d-prolinecha","d-proline",name_tmp)
	gsub("hydroxyphenyl","hydroxyphenyl ",name_tmp)		
	gsub("proprionic","propionic",name_tmp)
	gsub("chloromethyl","chloromethyl ",name_tmp)	
	gsub("acteyl","acetyl",name_tmp)
	gsub("floro","fluoro",name_tmp)	
	gsub("aminolauryl","lauryl",name_tmp)
	gsub("betaaspartic","beta aspartic",name_tmp)
	gsub("hydoxy","hydroxy",name_tmp)
	gsub("napthylalanine","naphthylalanine",name_tmp)		
	# FIXME: From CPP
	gsub("andx","",name_tmp)
	gsub("aelp,","",name_tmp)
	gsub("aedp,","",name_tmp)
	gsub("aplp,","",name_tmp)
	gsub("apdp,","",name_tmp)
	gsub("acidahx","acid",name_tmp)					
	gsub("ahxandb","",name_tmp)
	gsub("acidandb","acid",name_tmp)
	gsub("sac5c","",name_tmp)
	gsub("aib","",name_tmp)
	gsub("=qn","",name_tmp)
	gsub("succinicc","succinic",name_tmp)
	# FIXME: SatPDB, like ID 10811
	gsub("amideaoc","amide",name_tmp)
	gsub("amideape","amide",name_tmp)
	gsub("amidebala","amide",name_tmp)
	gsub("amidebip","amide",name_tmp)
	gsub("amidebta","amide",name_tmp)
	gsub("amidecha","amide",name_tmp)
	gsub("amidenal1","amide",name_tmp)
	gsub("amidenal2","amide",name_tmp)
	gsub("amidepal","amide",name_tmp)
	gsub("prolinecha","proline",name_tmp)	
	gsub("ethylestet","ethylester",name_tmp)
	
	# From DBASSP
	# https://dbaasp.org/help
	# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4662063/
	gsub("lalanineinlantibiotics","lanthionine",name_tmp)
	gsub("alpha-aminobutyrateinlantibiotics","3-methyllanthionine",name_tmp)
	gsub("didehydroalanine","2,3-didehydroalanine",name_tmp)
	gsub("dalanineinlantibiotics","d-lanthionine",name_tmp)
#	gsub("lysinewithlaurylside group","lauryl lysine",name_tmp)
	gsub("c6h13no2","",name_tmp)
	gsub("tyrosinesulfation","tyrosine sulfate",name_tmp)
	gsub("22-dimethylthiazolidine","2,2-dimethylthiazolidine",name_tmp)
	# Take care with the dropping of L- later
	gsub("2-nitroimidazol-1-yl","(2-nitroimidazol-1-yl)",name_tmp)
		
        # R=D and S=L aminoacids
        gsub("r-","d-",name_tmp)
        gsub("s-","",name_tmp)
        # Dropping L works better
        gsub("-l-"," ",name_tmp)

        gsub("d-diphenyl","diphenyl-d-",name_tmp)
        gsub("d-fluorenyl","fluorenyl-d-",name_tmp)
        gsub("d-beta-naphthyl","beta-naphthyl-d-",name_tmp)
        gsub("d-n-methyl","n-methyl-d-",name_tmp)
        gsub("d-methyl","methyl-d-",name_tmp)
        gsub("d-benzyl","benzyl-d-",name_tmp)
        gsub("d-benzoyl","benzoyl-d-",name_tmp)
        gsub("d-betainamidyl","betainamidyl-d-",name_tmp)
        gsub("d-fluoro","fluoro-d-",name_tmp)
        gsub("d-guanidino","guanidino-d-",name_tmp)
        gsub("d-phenyl","phenyl-d-",name_tmp)
        gsub("d-naphtylmethyl","naphtylmethyl-d-",name_tmp)

	# Drop strange characters that cannot be used in pubchem
	gsub("β","beta",name_tmp)
	gsub("α","alpha",name_tmp)
	gsub("γ","gamma",name_tmp)
	
	gsub("_"," ",name_tmp)
	if(strip_hyphens!="") {gsub("-","",name_tmp)}
	if(hyphen2space!="") {gsub("-"," ",name_tmp)}

	# Useful for debugging
	#print name_tmp
	
		query=""
		delete ddd
		ddmax=split(name_tmp,ddd," ")
		dd=0; while(dd<ddmax) {dd=dd+1
			if(length(ddd[dd])==1) {ddd[dd]=ddd[dd]"-"}
			query=query"{%22*%22:%22"ddd[dd]"%22},"
		}
#	query=query"{%22*%22:%22amino%22},{%22*%22:%22acid%22}"
	n=split(query,aa,"")
		query=substr(query,1,n-1)
		n=0
		found=""
		entry=0
		i=0
		while(found!="yes" && i<number_of_tries) {i=i+1
		if(method!="complexity") {query_method="relevancescore,desc"} else {query_method="complexity,asc"}
			command="lynx -dump -accept_all_cookies 'https://pubchem.ncbi.nlm.nih.gov/sdq/sdqagent.cgi?infmt=csv&outfmt=csv&query={%22download%22:%22*%22,%22collection%22:%22compound%22,%22where%22:{%22ands%22:["query"]},%22order%22:[%22"query_method"%22],%22start%22:"i",%22limit%22:1}'|tail -n1"
				command | getline result_query
#print command
				close(command)
				delete ff
				if(result_query~/Warning: no hits found satisfying your input query criteria/) {i=number_of_tries}
				split(result_query,ff,"\"")
				gsub(",","@",ff[2])
				gsub("\"","",ff[2])
				gsub(",","@",ff[4])
				gsub("\"","",ff[4])
				result_query=ff[1]""ff[2]""ff[3]""ff[4]""ff[5]
				
			#	gsub("\",",";",result_query)
			#	gsub(",\"",";",result_query)
			#	split(result_query,bb,";")
			#	if(bb[5]=="") {split(bb[1],ff,","); gsub(ff[1],"",bb[1]); bb[1]=ff[1]";"bb[1]} else {gsub(",",";",bb[1])}
			#	gsub(",",";",bb[3])
			#	gsub(",",";",bb[5])
			#	string=bb[1]";"bb[2]";"bb[3]";"bb[4]";"bb[5]
			#	split(string,aa,";")
			        split(result_query,aa,",")
	
			        # When sorted by complexity a column is added in the start, hence, you will need to increase numbers by one for all aa[X]
				if(method!="complexity") {
					cid=aa[1]
					standard_n=aa[2] # !!
					# FIXME: This ID is broken in Pubchem and they forgot to add the most common name to the list of names downloaded in the CSV file
					if(cid=="135410875"){standard_n="Pemetrexed"}
					if(cid=="8988"){standard_n="D-Proline"}
					alternative_names=standard_n"|"aa[3] # !!
				}
				else {
					cid=aa[2]
					standard_n=aa[3] # !!
					if(cid=="135410875"){standard_n="Pemetrexed"}
					if(cid=="8988"){standard_n="D-Proline"}
	                                alternative_names=standard_n"|"aa[4] # !!
				}
				# FIXME: we don't pass parenthesis, skip them for calculating similarity then
				gsub("\\(","",alternative_names)
				gsub("\\)","",alternative_names)
				# We also change R- to D-
				gsub("R-","D-",alternative_names)
				
				delete probability
				delete percentage
				delete length_diff
				smax=split(alternative_names,synonims,"|")
				s=0; while(s<smax) {s=s+1
					standard_name=tolower(synonims[s])
					gsub("@",",",standard_name)
						if(name_tmp==tolower(standard_name)) {percentage[s]=100;length_diff[s]=0;probability[s]=100;}
						else {
							dd=0; while(dd<ddmax) {dd=dd+1
								name_fragment=ddd[dd]
									if(standard_name~name_fragment && length(name_fragment)>0) {
#	probability[s]=probability[s]+length(name_fragment);
										percentage[s]=percentage[s]+length(name_fragment)/length(standard_name)*100;
#	length_diff[s]=((length(name_tmp)-length(standard_name))**2)**0.5
									}
							}
						}
				}
			s=0; maxi=0; while(s<smax) {s=s+1; if(percentage[s]>maxi) {maxi=percentage[s]}}; 
#print maxi
			pass_stereo=""
		#	if(substr(name_tmp2,1,1)!="d" && alternative_names~/D-/) {pass_stereo="no"}
			if(substr(name_tmp2,1,2)=="d-" && (substr(alternative_names,1,2)=="l-" || substr(alternative_names,1,2)=="L-")) {pass_stereo="no"}
			#print substr(name_tmp2,1,2),alternative_names,pass_stereo,name_tmp2
			if(maxi>minimum_percentage_of_similarity && pass_stereo!="no") {
				n=n+1
					good_max[n]=maxi
					gsub("@",",",standard_n); gsub("@",",",alternative_names)
					st[n]=standard_n
					st_a[n]=alternative_names
				if(method!="complexity") {wg[n]=aa[4] } else {wg[n]=aa[5] }
				if(method!="complexity") {pubchem_id[n]=aa[1] } else {pubchem_id[n]=aa[2] }
					entry=1
#print name_" found in PubChem ("maxi"%)"
#official_name[name_]=standard_n
#official_alternative_names[name_]=alternative_names
#weight[name_]=aa[5]
					analise_compound_name(name_,"",mw,simplification)
					smp[n]=conv_OLC_[simplification[name_]]
#simplification[name_]=conv_OLC_[simplification[name_]]
#	found="yes"
#		i=0
			}
#	else {if(i==number_of_tries) {print "WARNING: "name" was not found in PubChem ("maxi"%)"}}
		}
	if(entry==1) {
		delete conv_index
			nmax=n
			sort_nD(good_max,conv_index,">","no","v","","","","","",1,nmax)
			print name_" found in PubChem ("good_max[conv_index[1]]"%)"
			gsub(" ","_",st[conv_index[1]])

			# Pubchem ID points to "Ornithine" as official_name even for D-Ornithine
			if(st[conv_index[1]] && st_a[conv_index[1]]~/Ornithine, D-/) {st[conv_index[1]]="D-Ornithine"}
			
			official_name[name_]=st[conv_index[1]]
			if(official_name[name_]=="") {split(st_a[conv_index[1]],vv,"|"); official_name[name_]=vv[1]}
			gsub(" ","_",st_a[conv_index[1]])
			official_alternative_names[name_]=st_a[conv_index[1]]
			weight[name_]=wg[conv_index[1]]
			simplification[name_]=smp[conv_index[1]]
			id[name_]=pubchem_id[conv_index[1]]
	}
	else {print "WARNING: "name" was not found in PubChem"}
}

