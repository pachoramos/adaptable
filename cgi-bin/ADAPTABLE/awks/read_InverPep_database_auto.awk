function read_InverPep_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {
limit=1000
               a=1;
        limit_=a+limit
                path="DATABASES/InverPep/InverPepfiles/InverPep"
                root_online="http://ciencias.medellin.unal.edu.co/gruposdeinvestigacion/prospeccionydisenobiomoleculas/InverPep/public/Peptido/see/"
                length_numb=3
                add_tag="no"
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                while(a<limit_) {
		file=path""tag""a
                        if (system("[ -f " file " ] ") ==0 ) {

			input_file=file
#seq
				field=read_database_line(input_file,"SEQUENCE:",0,"b>_","_<br",1)
				gsub("_","",field)
				seq_a[a]=field
                                print "Reading "file" for sequence "seq_a[a]""
                                seq_a[a]=analyze_sequence(seq_a[a])
#ID
                                field=read_database_line(input_file,"CODE:",0,"b>_","_<br",1)
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"InverPep"field";" }
				analyze_sequence(seq_a[a])
#name
                                field=read_database_line(input_file,"NAME:",0,"b>_","_<br",1)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"SPECIE:",0,"<i>","</i>",1)
                                field=clean_string(field)
                                if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
#cyclic                 
                        field=read_database_line(input_file,"Type Modification",0,"black'>","<")
                        field=clean_string(field)
                        if(field!~/None/ && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                        if(field~/cetyl/ && field~/N-term/) {if(N_terminus_[seq_a[a]]!~field) {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]""field";"}}
                        if(field~/midation/ && field~/C-term/) {if(C_terminus_[seq_a[a]]!~field) {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}
                        if(field~/isulfide/) {disulfide_[seq_a[a]]="disulfide";if(PTM_[seq_a[a]]!~field) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"disulfide;"}}
                        if(field~/lantibio/) {if(PTM_[seq_a[a]]!~/lantibio/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lantibiotic;"}}
                        if(field~/lycos/) {if(PTM_[seq_a[a]]!~/lycos/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"Glycosylation;"}}
#Function
                field=read_database_line(input_file,"ACTIVITY",0,"b>","<br",1)
		gsub("&",",",field); amax=split(field,aa,",")
		aaa=0; while(aaa<amax) {aaa=aaa+1
		if(aa[aaa]~/Gram-/) {gsub(/Gram-$/,"gram-neg",aa[aaa])}
		if(aa[aaa]~/Gram\+/) {gsub("Gram+","gram-pos",aa[aaa])}
                                read_database_classify_field(aa[aaa],",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
				}
#Taxonomy
				        field="animalia"
                                        if(taxonomy_[seq_a[a]]!~field) {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}

				field=read_database_line(input_file,"PHYLUM:",0,"b>_","_<br>",1)
				field=clean_string(field)
				field=tolower(field)
                                if(taxonomy_[seq_a[a]]!~field) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}}
#Pubmed			
                        field=read_database_line(input_file,"REFERENCE:",1,"pubmed/","\"_target",1)
                        gsub("?term=","",field)
                        if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}
	
                        field=read_database_line(input_file,"SOURCE:",0,"</b>","<br>",1)
                        field=tolower(field)
                        if(field~/xperimentally/ && field!~/no/) {experimental_[seq_a[a]]="experimental"}

	}
       close(input_file)
	a=a+1
imax=a
}
return imax
}
