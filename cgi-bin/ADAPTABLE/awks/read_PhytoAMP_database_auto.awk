function read_PhytoAMP_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {
limit=2000
                a=1;
                limit_=a+limit
while (a<limit_) {
		tag="" ; imax=5-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}
		file="DATABASES/PhytAMP/PhytAMPfiles/PHYT"tag""a
                if (system("[ -f " file " ] ") ==0 ) { 
                                input_file=file
                        field=read_database_line(input_file,"x_UserSequence\"_value=",0,"\"","\">",1)
                                seq_a[a]=field
                                print "Reading "file" for sequence "seq_a[a]""
#seq
				seq_a[a]=analyze_sequence(seq_a[a])
#PTM
					field=read_database_line(input_file,"Post-translational modification",0,"<br>","<br>")
						if(field~/isulfide/) {if(PTM_[seq_a[a]]!~/isulfide/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"disulfide;"}}
					if(field~/yclic/) {cyclic_[seq_a[a]]="cyclic"}
					if(PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
#Activity
			field=read_database_line(input_file,"Activity",1,"<span>","</span>")
				read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
						antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
						antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
						cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
						tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
#all_organisms
                        field=read_database_line(input_file,"organism",1,"<span>","</span>")
			                                gsub("<s>","",field)
                                gsub("</s>","",field)
                                gsub("<B>","",field)
                                gsub("</B>","",field)
                                gsub("</I>","",field)
                                gsub("<I>","",field)
                                gsub("<BR>","",field)
				string=read_activity_description(field,":",seq_a[a],all_organisms_)
#id			
				field=read_database_line(input_file,"<span>id<",1,"<span>","</span></td>")
				if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]""field";"} 
#name
				field=read_database_line(input_file,"<span>Name<",1,"<span><b>","</b></span></td>")
				field=clean_string(field)
				if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";"}}
#gene
				field=read_database_line(input_file,"<span>Gene<",1,"<span>","</")
			       	gsub("<s>","",field); if(field=="Unidentified") {field=""}	
			       	field=clean_string(field)
				if(gene_[seq_a[a]]!~field) {if(field!="") {gene_[seq_a[a]]=gene_[seq_a[a]]""field";"}}
#Family
				field=read_database_line(input_file,"<span>Family<",1,"<span>","</span></td>")
				field=clean_string(field)
				if(Family_[seq_a[a]]!~field) {if(field!="") {Family_[seq_a[a]]=Family_[seq_a[a]]""field";"}}
				field=read_database_line(input_file,"<span>Plant<",1,"<i>","</span>")
				field=clean_string(field)
				gsub("</i>","",field)  ; gsub("</a>","",field)
#source
				if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";"}}
#taxonomy
				taxonomy_[seq_a[a]]="plant;"
				field=1; z=0; while(field!="") {z=z+1
					field=read_database_line(input_file,"<span>Taxonomy<",1,"title=","</a>",z)
					field=clean_string(field)
					split(field,bb,">")
				field2=bb[2]
				field2=clean_string(field2)
				field2=tolower(field2)
                                if(taxonomy_[seq_a[a]]!~field2) {if(field2!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field2";"}}
			}
#PDB and co.
				field=read_database_line(input_file,"View PDB entry",0,">","<")
				field=clean_string(field)
				field=toupper(field)
				if(pdb_[seq_a[a]]!~field) {if(field!="") {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}
                                field=read_database_line(input_file,"View PDB entry",0,">","<")
                                field=clean_string(field)
                                field=toupper(field)
                                if(experim_structure_[seq_a[a]]!~field) {if(field!="") {
                                	# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2686510/ -> not clear that they are experimentally validated
                                	#experimental_[seq_a[a]]="experimental"
                                	experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}
                                	}

#Pubmed
                        field=1; z=0; while(field!="") {z=z+1
	                        field=read_database_line(input_file,"PubMed=",0,"<b>","</b>",z)
	                        # Sometimes they don't close properly the tags and DOI gets into the same field
	                        split(field,bb,"DOI")
	                        field=bb[1]
        	                if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}
                        }

                }
		close(input_file)
		a=a+1
                tag="" ; imax=5-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}
		file="DATABASES/PhytAMP/PhytAMPfiles/PHYT"tag""a
		amax=a
		}
return amax
}
