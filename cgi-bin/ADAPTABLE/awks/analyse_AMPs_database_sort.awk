@include "awks/library.awk"
BEGIN{
parameters(prp_name,prp_option)
        
read_aa_prop_and_colors(aa_name,conv_aa,class,class_def,class_length,type,polarity,color_type)

delete seq
delete original_seq
delete prop
delete seq_names

		max_len=0
		input_file="OUTPUTS/"calculation_label"/selection"
                while ( getline < input_file >0 ) {
                        if($2=="seq") {seq[$1]=$3}
                        if($2=="oseq") {original_seq[$1]=$3;len=length($3);population[int(len)]=population[int(len)]+1 ; if(len>max_len) {max_len=len}}
                        if($2=="prop_separator") {split($0,aaaa,"prop_separator") ; prop[$1]=aaaa[2]}
                        if($2=="names") {seq_names[$1]=$3}
                        dmax=$1
                }
        close(input_file)

#                sort_families(family,original_family,fam_type,fathers,original_fathers,fam_size,fam_aligned,fam_properties,fam_names,alignments,alignments_transl,score_fathers,code_fathers,type_fathers,"OUTPUT_INPUT",\
#                                dmax,seq,original_seq,c)



if(generate_overlapping_families=="yes") {if(families_to_analyse=="") {fam_auto="yes";cmax=dmax;families_to_analyse=cmax}}
else {
                                fam_auto=""
                                        if(families_to_analyse=="" || fam_auto=="yes") {families_to_analyse=c ; fam_auto="yes"} else {if(c==families_to_analyse) {families_to_analyse=c}}
cmax=dmax
}} 
