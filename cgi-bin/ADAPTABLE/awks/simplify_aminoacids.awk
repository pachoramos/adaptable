BEGIN{
# Convert non-standard aa to standard if requested
datafile="DATABASES/Sidechain_database_PubChem_global"
        while ( getline < datafile >0 ) {
        standard[$3]=$2
}
close(datafile)
}
{
sssmax=split($1,sss,""); sequ=""; s=0; while(s<sssmax) {s=s+1; if(standard[sss[s]]!="" && standard[sss[s]]!="_") {sss[s]=standard[sss[s]]}; sequ=sequ""sss[s]}; print sequ
}
