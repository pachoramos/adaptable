function read_ADAPTABLE_database(input_file,start_index,ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,a) {
a=start_index
	while ( getline < input_file >0 ) {
		if(substr($0,1,1)==">") {
			a=a+1
			sequence=$2
			split($1,aa,">")
			gsub("\\[","-",aa[2])
			p=0; while(p<65) {
			p=p+1
			sub("\\[","-",$(p))
			sub("\\]","-",$(p))
			}
			# Some fields must be lowercase to catch more duplicates
			taxonomy_low=tolower($56)
			if($1!="_" && ID_[sequence]!~aa[2])  {ID_[sequence]=ID_[sequence]""aa[2]";"}
			if($2!="_" && seq_a[a]!~$2)  {seq_a[a]=$2}
			if($3!="_" && name_[sequence]!~$3)  {name_[sequence]=name_[sequence]""$3";"}
			if($4!="_" && source_[sequence]!~$4)  {source_[sequence]=source_[sequence]""$4";"}
			if($5!="_" && Family_[sequence]!~$5)  {Family_[sequence]=Family_[sequence]""$5";"}
			if($6!="_" && gene_[sequence]!~$6)  {gene_[sequence]=gene_[sequence]""$6";"}
			if($7!="_" && stereo_[sequence]!~$7)  {stereo_[sequence]=stereo_[sequence]""$7";"}
			if($8!="_" && N_terminus_[sequence]!~$8)  {N_terminus_[sequence]=N_terminus_[sequence]""$8";"}
			if($9!="_" && C_terminus_[sequence]!~$9)  {C_terminus_[sequence]=C_terminus_[sequence]""$9";"}
			if($10!="_" && PTM_[sequence]!~$10)  {PTM_[sequence]=PTM_[sequence]""$10";"}
			if($11!="_" && cyclic_[sequence]!~$11)  {cyclic_[sequence]=$11}
			if($12!="_" && target_[sequence]!~$12)  {target_[sequence]=target_[sequence]""$12";"}
			if($13!="_" && synthetic_[sequence]!~$13)  {synthetic_[sequence]=$13}
			if($14!="_" && antimicrobial_[sequence]!~$14)  {antimicrobial_[sequence]=$14}
			if($15!="_" && antibacterial_[sequence]!~$15)  {antibacterial_[sequence]=$15}
			if($16!="_" && antigram_pos_[sequence]!~$16)  {antigram_pos_[sequence]=$16}
			if($17!="_" && antigram_neg_[sequence]!~$17)  {antigram_neg_[sequence]=$17}
			if($18!="_" && antifungal_[sequence]!~$18)  {antifungal_[sequence]=$18}
			if($19!="_" && antiyeast_[sequence]!~$19)  {antiyeast_[sequence]=$19}
			if($20!="_" && antiviral_[sequence]!~$20)  {antiviral_[sequence]=$20}
			if($21!="_" && antiprotozoal_[sequence]!~$21)  {antiprotozoal_[sequence]=$21}
			if($22!="_" && antiparasitic_[sequence]!~$22)  {antiparasitic_[sequence]=$22}
			if($23!="_" && antiplasmodial_[sequence]!~$23)  {antiplasmodial_[sequence]=$23}
			if($24!="_" && antitrypanosomic_[sequence]!~$24)  {antitrypanosomic_[sequence]=$24}
			if($25!="_" && antileishmania_[sequence]!~$25)  {antileishmania_[sequence]=$25}
			if($26!="_" && insecticidal_[sequence]!~$26)  {insecticidal_[sequence]=$26}
			if($27!="_" && anticancer_[sequence]!~$27)  {anticancer_[sequence]=$27}
			if($28!="_" && antitumor_[sequence]!~$28)  {antitumor_[sequence]=$28}
			if($29!="_" && cell_line_[sequence]!~$29)  {cell_line_[sequence]=cell_line_[sequence]""$29";"}
			if($30!="_" && tissue_[sequence]!~$30)  {tissue_[sequence]=tissue_[sequence]""$30";"}
			if($31!="_" && cancer_type_[sequence]!~$31)  {cancer_type_[sequence]=cancer_type_[sequence]""$31";"}
			if($32!="_" && anticancer_activity_[sequence]!~$32)  {anticancer_activity_[sequence]=anticancer_activity_[sequence]""$32";"}
			if($33!="_" && anticancer_activity_test_[sequence]!~$33)  {anticancer_activity_test_[sequence]=anticancer_activity_test_[sequence]""$33";"}
			if($34!="_" && antiangiogenic_[sequence]!~$34)  {antiangiogenic_[sequence]=$34}
			if($35!="_" && toxic_[sequence]!~$35)  {toxic_[sequence]=$35}
			if($36!="_" && cytotoxic_[sequence]!~$36)  {cytotoxic_[sequence]=$36}
			if($37!="_" && hemolytic_[sequence]!~$37)  {hemolytic_[sequence]=$37}
			if($38!="_" && hemolytic_activity_[sequence]!~$38)  {hemolytic_activity_[sequence]=hemolytic_activity_[sequence]""$38";"}
			if($39!="_" && hemolytic_activity_test_[sequence]!~$39)  {hemolytic_activity_test_[sequence]=hemolytic_activity_test_[sequence]""$39";"}
			if($40!="_" && RBC_source_[sequence]!~$40)  {RBC_source_[sequence]=RBC_source_[sequence]""RBC_source_[sequence]""$40";"}
			if($41!="_" && cell_cell_[sequence]!~$41)  {cell_cell_[sequence]=$41}
			if($42!="_" && hormone_[sequence]!~$42)  {hormone_[sequence]=$42}
			if($43!="_" && quorum_sensing_[sequence]!~$43)  {quorum_sensing_[sequence]=$43}
			if($44!="_" && immunomodulant_[sequence]!~$44)  {immunomodulant_[sequence]=$44}
			if($45!="_" && antihypertensive_[sequence]!~$45)  {antihypertensive_[sequence]=$45}
			if($46!="_" && drug_delivery_[sequence]!~$46)  {drug_delivery_[sequence]=$46}
			if($47!="_" && cell_penetrating_[sequence]!~$47)  {cell_penetrating_[sequence]=$47}
			if($48!="_" && tumor_homing_[sequence]!~$48)  {tumor_homing_[sequence]=$48}
			if($49!="_" && blood_brain_[sequence]!~$49)  {blood_brain_[sequence]=$49}
			if($50!="_" && antioxidant_[sequence]!~$50)  {antioxidant_[sequence]=$50}
			if($51!="_" && antiproliferative_[sequence]!~$51)  {antiproliferative_[sequence]=$51}
			if($52!="_" && DSSP_[sequence]!~$52)  {DSSP_[sequence]=$52}
			if($53!="_" && pdb_[sequence]!~$53)  {pdb_[sequence]=pdb_[sequence]""$53";"}
			if($54!="_" && experim_structure_[sequence]!~$54)  {experim_structure_[sequence]=experim_structure_[sequence]""$54";"}
			if($55!="_" && PMID_[sequence]!~$55)  {PMID_[sequence]=PMID_[sequence]""$55";"}
			if($56!="_" && taxonomy_[sequence]!~taxonomy_low)  {taxonomy_[sequence]=taxonomy_[sequence]""taxonomy_low";"}
			if($57!="_" && all_organisms_[sequence]!~$57)  {all_organisms_[sequence]=all_organisms_[sequence]""$57";"}
			if($58!="_" && activity_viral_[sequence]!~$58)  {activity_viral_[sequence]=activity_viral_[sequence]""$58";"}
			if($59!="_" && activity_viral_test_[sequence]!~$59)  {activity_viral_test_[sequence]=activity_viral_test_[sequence]""$59";"}
			if($60!="_" && ribosomal_[sequence]!~$60)  {ribosomal_[sequence]=$60}
			if($61!="_" && experimental_[sequence]!~$61)  {experimental_[sequence]=$61}
			if($62!="_" && biofilm_[sequence]!~$62)  {biofilm_[sequence]=biofilm_[sequence]""$62";"}
		}
	}
	close(input_file)
return a
}

