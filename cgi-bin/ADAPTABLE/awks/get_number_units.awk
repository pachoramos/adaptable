function get_number_units(field,sequence,param,st,f,ff,nnn,end,start,number,units,MW,factor,factor1,factor2,factor3,value,field2,nn,b,a)
{
	factor1=1
	factor2=1
	factor3=1
	param="_"
	gsub("&plusmn;","±",field)
	gsub("micrograms","µg",field)
	gsub("µM","μM",field)
	# For UniProt
	gsub("um","μM",field)
	gsub("nm","nM",field)
	gsub("ug)","μg/ml)",field)
	gsub("&lt;","<",field)

	gsub("_=_","=",field)

	gsub("u","μ",field)
	gsub("µ","μ",field)
	gsub("Kg","kg",field)
	gsub("mL","ml",field) ; gsub("Âµ","u",field)
	gsub("microg","μg",field)
	split(field,a,"mM") ; if (field!=a[1]) {factor2=1000; units="mM"; field2=a[1]}
        split(field,a,"μM") ; if (field!=a[1]) {factor2=1; units="μM"; field2=a[1]}
	split(field,a,"microM") ; if (field!=a[1]) {factor2=1; units="μM"; field2=a[1]}
	split(field,a,"μMo/l") ; if (field!=a[1]) {factor2=1; units="μM"; field2=a[1]}
	split(field,a,"µMo/ml") ; if (field!=a[1]) {factor2=1000; units="mM"; field2=a[1]}
	split(field,a,"nM") ; if (field!=a[1]) {factor2=0.001; units="μM" ;; field2=a[1]}
        split(field,a,"g/l") ; if (field!=a[1]) {factor2=1000000; units="g/l";; field2=a[1]; MW=protein_MW(sequence); if(MW!=0 && MW!="") {factor3=1/MW}}
        split(field,a,"mg/l") ; if (field!=a[1]) {factor2=1000; units="mg/L";; field2=a[1]; MW=protein_MW(sequence); if(MW!=0 && MW!="") {factor3=1/MW}}
	split(field,a,"μg/l") ; if (field!=a[1]) {factor2=1; units="mg/L";; field2=a[1]; MW=protein_MW(sequence); if(MW!=0 && MW!="") {factor3=1/MW}}
	split(field,a,"ng/l") ; if (field!=a[1]) {factor2=0.001; units="ng/l";; field2=a[1]; MW=protein_MW(sequence); if(MW!=0 && MW!="") {factor3=1/MW}}
	split(field,a,"μg/ml") ; if (field!=a[1]) {factor2=1000; units="µg/ml";; field2=a[1]; MW=protein_MW(sequence); if(MW!=0 && MW!="") {factor3=1/MW}}
	split(field,a,"ng/ml") ; if (field!=a[1]) {factor2=1; units="ng/ml";; field2=a[1]; MW=protein_MW(sequence); if(MW!=0 && MW!="") {factor3=1/MW}}
	split(field,a,"mg/ml") ; if (field!=a[1]) {factor2=1000000; units="mg/ml";; field2=a[1]; MW=protein_MW(sequence); if(MW!=0 && MW!="") {factor3=1/MW}}
	split(field,a,"mg/kg") ; if (field!=a[1]) {factor2=1000; units="mg/L";; field2=a[1]; MW=protein_MW(sequence); if(MW!=0 && MW!="") {factor3=1/MW}}
	if(field2=="") {units="μM" ;; field2=field}
	if(a[2]=="") {gsub(")","",field2)}
	if ((field~/%/ || field~/fractions/ || field~/Inhibition/ || field~/Cytotoxicity/) && field~/μM/) {factor2=1; units="μM"; nnn=split(field,a,"μM");field2=a[nnn-1];param="MHC" ; }
        if ((field~/%/ || field~/fractions/ || field~/Inhibition/ || field~/Cytotoxicity/) && field~/µM/) {factor2=1; units="μM"; nnn=split(field,a,"µM");field2=a[nnn-1];param="MHC"; }
        if ((field~/%/ || field~/fractions/ || field~/Inhibition/ || field~/Cytotoxicity/) && field~/μg\/ml/) {factor2=1; units="μg/ml"; nnn=split(field,a,"μg/ml");field2=a[nnn-1];param="MHC"}
        if ((field~/%/ || field~/fractions/ || field~/Inhibition/ || field~/Cytotoxicity/) && field~/µg\/ml/) {factor2=1; units="µg/ml"; nnn=split(field,a,"µg/ml");field2=a[nnn-1];param="MHC"}
        if ((field~/%/ || field~/fractions/ || field~/Inhibition/ || field~/Cytotoxicity/) && field~/mg\/l/) {factor2=1000; units="mg/l"; nnn=split(field,a,"mg/l");field2=a[nnn-1];param="MHC"}
        if ((field~/%/ || field~/fractions/ || field~/Inhibition/ || field~/Cytotoxicity/) && field~/mg\/L/) {factor2=1000; units="mg/L"; nnn=split(field,a,"mg/L");field2=a[nnn-1];param="MHC"}
        if(field~/MIC/) {param="MIC"}
        if(field~/mic/) {param="MIC"}
        if(field~/MEC/) {param="MIC"} # MEC/MIC are the same
	if(field~/MIC50/) {param="MIC50"}
	if(field~/mic50/) {param="MIC50"}
	if(field~/MIC90/) {param="MIC90"}
	if(field~/mic90/) {param="MIC90"}
	if(field~/MAC/) {param="MAC"} # minimum amoebicidal concentration
	if(field~/MBC/) {param="MBC"}
	if(field~/mbc/) {param="MBC"}
	if(field~/MBC50/) {param="MBC50"}
	if(field~/mbc50/) {param="MBC50"}
	if(field~/MBC90/) {param="MBC90"}
	if(field~/mbc90/) {param="MBC90"}
        if(field~/GI50/) {param="GI50"} 
        if(field~/gi50/) {param="GI50"}
	if(field~/IC50/ && field!~/MIC50/) {param="IC50"} #min conc to inhibit 50% of enzyme
	if(field~/ic50/ && field!~/mic50/) {param="IC50"} 
	if(field~/ic(50)/ && field!~/mic(50)/) {param="IC50"}
        if(field~/IC100/ && field!~/MIC100/) {param="IC100"} #min conc to inhibit 50% of enzyme
        if(field~/ic100/ && field!~/mic100/) {param="IC100"} 
        if(field~/ic(100)/ && field!~/mic(100)/) {param="IC100"}
        if(field~/HC10/) {param="HC10"} # hazardous conc
        if(field~/hc10/) {param="HC10"}
        if(field~/EC50/) {param="EC50"} # minimum dose for a druf to work
        if(field~/ec50/) {param="EC50"}
        if(field~/ED100/) {param="EC100"} # EC=ED
        if(field~/ed100/) {param="EC100"}
        if(field~/LC50/) {param="LC50"} # lethal dose in the air
        if(field~/lc50/) {param="LC50"}
        if(field~/lc(50)/) {param="LC50"}
        if(field~/HD50/) {param="HD50"}
        if(field~/hd50/) {param="HD50"}
	if(field~/HC50/) {param="HC50"}
	if(field~/hc50/) {param="HC50"} 
        if(field~/LD50/) {param="LD50"} # Lethal dose to kill 50 of animals
        if(field~/ld50/) {param="LD50"}
        if(field~/MHC/) {param="MHC"}  # hemolitic activity; min conc to lysate 10% of cells
        if(field~/mhc/) {param="MHC"}
	if(field~/non-hemolytic/ || field~/Non hemolytic/) {param="MHC"}
        gsub(".)","",field2)
	gsub(" ","",field2);gsub("_","",field2);split(field2,vv,"="); if(vv[2]!="") {field2=vv[2]} ; split(field2,f,""); st="";ff=length(field2)+1; end=0;start=0;
	while(ff>1) {ff=ff-1 ; 
		if(f[ff]~/[0-9]/ || f[ff]=="." || f[ff]=="," || f[ff]=="±") {
			if(st=="") {end=ff; st="end"}
		}
		else {start=ff+1; ff=0}
	} ;
	number=substr(field2,start,end-start+1) ;
#	gsub(",",".",number)
	if(field~/non-hemolytic/ || field~/Non hemolytic/) {param="MHC"; number=0}
	split(number,nn,"±"); number_tmp=nn[1]; number=number_tmp;
	value=number*factor1*factor2*factor3
# the value is in uM
	if (param!~/=/) {param=param"="}
	if(value==0 || value=="" || param=="=" || param=="_=" || param=="") {return ""}
	else {return "("param""value"_μM)"}
}
