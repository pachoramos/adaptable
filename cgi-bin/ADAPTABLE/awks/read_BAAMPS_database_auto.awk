function read_BAAMPS_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,v) {
limit=1000
a=1;
        limit_=a+limit
                path="DATABASES/BAAMPS/BAAMPSfiles/BAAMPS"
                length_numb=3
                add_tag="no"
                split(path,aa,"/")
                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                while (a<limit_) {
                                tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
                        file=path""tag""a
			if (system("[ -f " file " ] ") ==0 ) {
#seq
				input_file=file
					field=read_database_line(input_file,">Sequence:<",0,"<td>","</td>",1)
					seq_a[a]=field
					seq_a[a]=analyze_sequence(seq_a[a])
					print "Reading "file" for sequence "seq_a[a]""
#ID
					field="BAAMPS"tag""a
					if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]""field";"}
#name
				field=read_database_line(input_file,">Name:<",0,"<td>","</td>",1)
				field=clean_string(field)
				if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";"}}

#source
                                field=read_database_line(input_file,">AMP_source:<",0,"<td>","</td>",1)
				if(field=="de_novo") {synthetic_[seq_a[a]]="synthetic"}
				if(field=="de_novo" || field=="modified" || field=="other") {field=""}
				field=clean_string(field)
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";"}}
                                field=tolower(field)
				if(taxonomy_[seq_a[a]]!~field) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}}

#C_terminus
                                field=read_database_line(input_file,"<b>Tags:</b>",2,"c-term-","\"_class",1)
                                field=clean_string(field)
                                if(field!~"Free" && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
                                gsub("Free","",field)
                                if(C_terminus_[seq_a[a]]!~field) {if(field!="") {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]""field";"}}

#activity
				field1=read_database_line(input_file,"Inhibition",0,"black'>","</td",1)
				gsub("&plusmn;","+-",field1)
					field2=read_database_line(input_file,"Unit</FONT",3,"dontcut","</a",1)
					gsub("&mu;","u",field2)
					field3=field1""field2
					if(activity_viral_[seq_a[a]]!~field3) {if(field3!="") {activity_viral_[seq_a[a]]=activity_viral_[seq_a[a]]""field3";"}}

#all_organisms			
					field=1; b=0; while(field!="") {b=b+1
                                        field=read_database_line(input_file,"<b>Data</b>",0,"<i>","</i>",b)
					split(field,ee,"(");
					      split(ee[1],ff,"_") ; if(ff[2]!="") {field3=ff[1]"_"ff[2]} else {field3=ff[1]}
					      if(full_microbe_name[field3]!="") {
						      field2=gram[field3]
		             read_database_classify_field(field2,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
						field2=kingdom_microbe[field3]
						read_database_classify_field(field2,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)

						      if(e[2]~/\)/) {field=full_microbe_name[field3]"("e[2]} 
						      else {field=full_microbe_name[field3]""e[2]}

							      }
					string=analyze_organism(field,seq_a[a])
					field=clean_string(field)
					field_to_compare=""
					field_to_compare=escape_pattern(field)
							      if(all_organisms_[seq_a[a]]!~field_to_compare) {if(field!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""field";"}}
                                        
					}

# target
					field="biofilm"
					if(target_[seq_a[a]]!~field) {target_[seq_a[a]]=target_[seq_a[a]]""field";"}
#PMID
					 field=1; b=0; while(field!="") {b=b+1
					field=read_database_line(input_file,"peptideReferences",0,"href=\"","\"_target",b)
					if(PMID_[seq_a[a]]!~field) {if(field!="") {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}}
				}
					# Welcome to BaAMPs, the first database dedicated to antimicrobial peptides (AMPs) specifically tested against microbial biofilms...
					# In BaAMPs, information related to AMP sequence, chemical modifications, associated with target organisms, experimental methods, peptide concentration
					# http://www.baamps.it/
					experimental_[seq_a[a]]="experimental"
			
			}
			close(input_file)
			a=a+1
			fmax=a
		}
return fmax
}
