function read_DRAMP_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
z,a,b,aa,aaa,p,pp,ppp,f,ff,fff,d) {
limit=5000
                a=1;
                limit_=a+limit
		while(a<limit_) {
		tag="" ; imax=5-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}
		file="DATABASES/DRAMP/DRAMPfiles/DRAMP"tag""a
		if (system("[ -f " file " ] ") ==0 ) {
#seq
                                input_file=file
                        field=read_database_line(input_file,"\"general_sequence",1,"<li>","</li>",1)
                                seq_a[a]=field
                                # Don't empty completely their information will end up attached to random sequences when merging all DBs
                                gsub("Unknow","_",field)
                                print "Reading "file" for sequence "seq_a[a]""
				seq_a[a]=analyze_sequence(seq_a[a])

 
                tag="" ; imax=5-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}
#ID
                                field=read_database_line(input_file,"DRAMP ID</h4>",1,"<li>","</li>",1)
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]""field";" }
#name
                                field=read_database_line(input_file,"Peptide Name</h4>",1,"<li>","</li>",1)
                                field=clean_string(field)
                                if(field=="NA"){field=""}
                                if(field=="Not_found"){field=""}
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"Source</h4>",1,"<li>","</li>",1)
                                field=clean_string(field)
                                if(field=="NA"){field=""}
                                if(field=="Not_found"){field=""}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
				if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
#Family
                                field=read_database_line(input_file,"Family</h4>",1,"<li>","</li>",1)
                                field=clean_string(field)
                                if(field=="NA"){field=""}
                                if(field=="Not_found"){field=""}
                                if(Family_[seq_a[a]]!~field) {if(field!="") {Family_[seq_a[a]]=Family_[seq_a[a]]""field";" }}
#gene
                                field=read_database_line(input_file,"Gene</h4>",1,"<li>","</li>",1)
                                field=clean_string(field)
				gsub("<i>","",field)
				gsub("</i>","",field)
				if(field=="NA"){field=""}
				if(field=="Not_found"){field=""}
                                if(gene_[seq_a[a]]!~field) {if(field!="") {gene_[seq_a[a]]=gene_[seq_a[a]]""field";" }}
#cyclic
                        field=read_database_line(input_file,"PTM</h4>",0,"<li>","</li>",1)
                        field=clean_string(field)
                        if(field=="NA"){field=""}
                        if(field=="Not_found"){field=""}
			if(field!~/None/ && PTM_[seq_a[a]]!~field) {if(field!="") {PTM_[seq_a[a]]=PTM_[seq_a[a]]""field";"}}
			if(field~/cetyl/ && field~/N-term/) {if(N_terminus_[seq_a[a]]!~field) {N_terminus_[seq_a[a]]=N_terminus_[seq_a[a]]"acetylation;"}}
                        if(field~/midation/ && field~/C-term/) {if(C_terminus_[seq_a[a]]!~field) {C_terminus_[seq_a[a]]=C_terminus_[seq_a[a]]"amidation;"}}
                        if(field~/isulfide/) {disulfide_[seq_a[a]]="disulfide";if(PTM_[seq_a[a]]!~field) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"disulfide;"}}
                        if(field~/yclic/) {cyclic_[seq_a[a]]="cyclic";if(PTM_[seq_a[a]]!~/ycliz/ && PTM_[seq_a[a]]!~/cyclization/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"cyclization;"}}
                        if(field~/lantibio/) {if(PTM_[seq_a[a]]!~/lantibio/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lantibiotic;"}}
                        if(field~/lycos/) {if(PTM_[seq_a[a]]!~/lycos/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"glycosylation;"}}
#target
			field=read_database_line(input_file,"Binding_Target</h4>",1,"<li>","</li>",1)
			field=clean_string(field)
			if(field=="NA"){field=""}
			if(field=="Not_found"){field=""}
                        if(target_[seq_a[a]]!~field) {if(field!="") {target_[seq_a[a]]=target_[seq_a[a]]""field";"}}
#Function
                        field=read_database_line(input_file,"Biological Activity</h4>",1,"<li>","</li>",1)
                                read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
#Sub-function
                                field=read_database_line(input_file,"Target Organism</h4>",3,"unstyled'><li>","</ul>",1)
                                read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                      	                         antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
#Experimental structure/PDB
                        field=read_database_line(input_file,"PDB_ID</h4",4,">","</a>",1)
			gsub("</li","",field)
			gsub("</a","",field)
			field=clean_string(field)
			field=toupper(field)
			if(field=="NA"){field=""}
			if(field=="Not_found"){field=""}
			if(field=="None"){field=""}
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}

                        field=read_database_line(input_file,"PDB_ID</h4",4,">","</a>",1)
                        gsub("</li","",field)
                        gsub("</a","",field)
                        field=clean_string(field)
                        field=toupper(field)
                        if(field=="NA"){field=""}
                        if(field!="") {
                                #experimental_[seq_a[a]]="experimental"
                                if(experim_structure_[seq_a[a]]!~field) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}}

#all organisms
                        field=read_database_line(input_file,"Target Organism</h4>",3,"unstyled'><li>","</ul>",1)
                        gsub("ï¬n","fi",field)
			gsub("<li>","",field)
                        gsub("</li>","",field)
                        gsub("<b>","",field)
                        gsub("</b>","",field)
			string=read_activity_description(field,":",seq_a[a],all_organisms_)
			string=clean_string(string)
			string=analyze_organism(string,seq_a[a])

			# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6692298/
			# The AMPs have the following characteristics: (1) less than 100 amino acids in length; (2) a clear mature sequence; (3) determined their activity
			experimental_[seq_a[a]]="experimental"
#Pubmed
                        field=1; z=0; while(field!="") {z=z+1
                        field=read_database_line(input_file,"Pubmed ID</h4",1,"\">","<",z)
			gsub("PubMed_ID_is_not_available","",field)
			gsub("Pubmed_ID_is_not_available","",field)
			gsub("PubMed_ID_is_not_availbale","",field)
			gsub("Unknow","",field)
			gsub("#",";",field)
			gsub("_","",field)
                        if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}
                        }
			 
                }
	close(input_file)
		a=a+1
                tag="" ; imax=5-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}
		file="DATABASES/DRAMP/DRAMPfiles/DRAMP"tag""a
		amax=a
		}
return amax
}
