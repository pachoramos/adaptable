BEGIN {s=0}
{if ($1~/>/) {head=$1;prope=$2} 
	else {
		if(a[$1]=="") {
			s=s+1;title[s]=head; seq[s]=$0; 
			if (prope=="antimicrobial")  {microbs[s]="antimicrobial"}
			if (prope=="antibacterial") {bacteria[s]="antibacterial"}
			if (prope=="antifungal") {fungi[s]="antifungal"}
			if (prope=="anticancer") {cancer[s]="anticancer"}
			if (prope=="antiviral")  {virus[s]="antiviral"}
			if (prope=="structure") {structure[s]="struct"}
			if (prope=="antihypertension") {hypertension[s]="antihyper"}
			if (prope=="cell_cell_communication") {cell_cell[s]="cell_cell"}
			if (prope=="drug_delivery") {drug[s]="drug"}
			if (prope=="toxic") {toxic[s]="toxic"}
			if (prope=="antiparassital") {antiparass[s]="antiparass"}

			a[$1]=1; number[$1]=s
		} 
		else {
			if (prope=="antimicrobial")  {microbs[number[$1]]="antimicrobial"}
			if (prope=="antibacterial") {bacteria[number[$1]]="antibacterial"}
			if (prope=="antifungal") {fungi[number[$1]]="antifungal"}
			if (prope=="anticancer") {cancer[number[$1]]="anticancer"}
			if (prope=="antiviral")  {virus[number[$1]]="antiviral"}
			if (prope=="structure") {structure[number[$1]]="struct"}
			if (prope=="antihypertension") {hypertension[number[$1]]="antihyper"} 
			if (prope=="cell_cell_communication") {cell_cell[number[$1]]="cell_cell"}
			if (prope=="drug_delivery") {drug[number[$1]]="drug"}
			if (prope=="toxic") {toxic[number[$1]]="toxic"}
			if (prope=="antiparassital") {antiparass[number[$1]]="antiparass"}
		}
	}
}
END{
	smax=s
		s=0;while(s<smax){s=s+1
			print title[s],microbs[s],bacteria[s],fungi[s],cancer[s],virus[s],structure[s],hypertension[s],cell_cell[s],drug[s],toxic[s],antiparass[s]
				print seq[s]
		}
}

