@include "awks/library.awk"
# In BEGIN choose for each parameter y (yes), i (indifferent) or n (not). 
# Parmameters which imply a name or a number (e.g. tissue="Breast" or IC50_cancer=3) can only have their value or "i" (indifferent). 
# Choose also the sequence length range (e.g "13-28").
# Launch as awk -f read_dramp_database.awk database, where database is the list of sequences in fasta format where all the properties are in the description line
# Example: all_organisms="coccus" ; antiyeast="n" will select all the peptides active against cocci without antiyeast activity 
# In order to avoid certain type you can add n_ in front. Example all_organisms="n_coccus" will select only peptides active against all organisms except cocci.
BEGIN{
#read_parameters_()
	parameters(prp_name,prp_option)
		read_aa_prop_and_colors(aa_name,conv_aa,class,class_def,class_length,type,polarity,color_type)
		family_index=ENVIRON["family_index"]
		comparison_option=ENVIRON["comparison_option"]
#        min_seq_length=6

		input_file="OUTPUTS/"calculation_label"/.selection-fathers-sorted"	
		while ( getline < input_file >0 ) {
			if($2=="seq") {seq[$1]=$3}
			if($2=="oseq") {original_seq[$1]=$3}
			if($2=="prop_separator") {split($0,aaaa,"prop_separator ") ; prop[$1]=aaaa[2]}
			if($2=="names") {seq_names[$1]=$3}
			dmax=$1
		}
	close(input_file)

		read_blosum("tmp_files/align_matrix",blosum)

# 2) GENERATION OF FAMILIES
# arrays of sequences (in their original form of translated into structure (DSSP) or simplified aa) are passed into temporary arrays which will contains the list for each family found

		if(calculate_only_global_properties!="y") {
			delete seq_tmp
				delete seq_tmp_original
				delete prop_tmp
				delete seq_names_tmp

				if (work_in_simplified_aa_space=="DSSP") {align_method="DSSP"}
			d=0; while(d<dmax) {d=d+1
				seq_tmp[d]=seq[d]
					seq_tmp_original[d]=original_seq[d]
					prop_tmp[d]=prop[d]
					seq_names_tmp[d]=seq_names[d]
			}

			delete exclude_seq
				delete population_fam
				system("mkdir -p OUTPUTS/"calculation_label"")
				if(families_to_analyse=="") {fam_auto="yes";cmax=dmax;families_to_analyse=cmax}
			compare_fathers(seq_tmp,level_agreement,min_seq_length,align_method,comparison_option)

		}
}
