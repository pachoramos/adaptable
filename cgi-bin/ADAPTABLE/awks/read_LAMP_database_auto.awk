function read_LAMP_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value) {

                        a=1
                        limit=100000
                        path="DATABASES/LAMP/LAMPfiles/LAMP"
                        file_names="http://biotechlab.fudan.edu.cn/database/lamp/db/lamp.fasta"
                        split(path,aa,"/")
                        file_list=aa[1]"/"aa[2]"/list"
                         while ( getline < file_list >0 && dd<limit)  {
				 dd=dd+1
                                 if(substr($0,1,1)==">") {split($0,aa,">"); split(aa[2],bb,"|")

                                 # Start examining detailed files
                                        name=bb[1]
                                        file_online="http://biotechlab.fudan.edu.cn/database/lamp/detail.php?id="name
                                        file=path""name
				if (system("[ -f " file " ] ") ==0 ) {
        			input_file=file
#seq
				field=read_database_line(input_file,"Sequence<br>&nbsp;&nbsp;&nbsp;&nbsp",0,";","<br/>",1)
				if(field==""){field=read_database_line(input_file,"Sequence<br><span class=state_f>&nbsp;&nbsp;&nbsp;&nbsp",0,";","</span></li><li>",1)}
				seq_a[a]=field
print "Reading "file" for sequence "seq_a[a]""
seq_a[a]=analyze_sequence(seq_a[a])

# Get more information from main list file
                                gsub(" ","_",bb[2]);bb[2]=clean_string(bb[2]);if(name_[seq_a[a]]!~bb[2]) {if(bb[2]!="") {name_[seq_a[a]]=name_[seq_a[a]]""bb[2]";" }}
                                gsub(" ","_",bb[3]);gsub("Synthetic","",bb[3]);bb[3]=clean_string(bb[3]);if(source_[seq_a[a]]!~bb[3]) {if(bb[3]!="") {source_[seq_a[a]]=source_[seq_a[a]]""bb[3]";" }}
                                if(bb[4]~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(bb[5]~/xperimental/) {experimental_[seq_a[a]]="experimental"}
                                if(bb[6]~/ntimicrobial/) {antimicrobial_[seq_a[a]]="antimicrobial"}
                                if(bb[6]~/ntibacterial/) {antibacterial_[seq_a[a]]="antibacterial"}
                                if(bb[6]~/ntifungal/) {antifungal_[seq_a[a]]="antifungal"}
                                if(bb[6]~/ntiviral/) {antiviral_[seq_a[a]]="antiviral"}
                                if(bb[6]~/nticancer/) {anticancer_[seq_a[a]]="anticancer"}
                                if(bb[6]~/ntiparasitic/) {antiparasitic_[seq_a[a]]="antiparasitic"}
                                if(bb[6]~/nsecticidal/) {insecticidal_[seq_a[a]]="insecticidal"}
                                
#ID
                                field=read_database_line(input_file,"lamp_id:<a",0,">","<",1)
                                if(field==""){field=read_database_line(input_file,"lamp_id:",0,">","<",1)}
                                if(ID_[seq_a[a]]!~field) {ID_[seq_a[a]]=ID_[seq_a[a]]"LAMP"field";" }
#name
                                # Don't start from ':' because it will cut fields that contain ':' in the middle
                                field=read_database_line(input_file,"FullN",0,"ame","<br/>",1)
                                if(field~/span_class=state_t/){field=read_database_line(input_file,"FullN",0,"ame","</span>",1)}
                                gsub("<span_class=state_t>","",field)
                                gsub(":","",field)
                                gsub("_others","",field)
                                gsub(";_;",";",field)
                                gsub(";_)","",field)
                                gsub("PREDICTED:","",field)
				field=clean_string(field)
				gsub("Patent","patent",field)
				split(field,nnnn,"from_patent_")
				patent=nnnn[2]
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"Source",0,":","<br/>",1)
				if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
				else {
				field=clean_string(field)
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
				}
#Function
                        field=read_database_line(input_file,"Activity",0,":","<br/>",1)
                                read_database_classify_field(field,",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
#activity_test
			field1=1;
                        b=0; while(field1!="") {b=b+1
                           field1=read_database_line(input_file,"Flora",0,":","&nbsp",b)
                           field2=read_database_line(input_file,"Flora",0,"&nbsp;&nbsp;",":",b)
                           field3=read_database_line(input_file,"Flora",0,"(",")",b)

                        field1=clean_string(field1)
                        field2=clean_string(field2)
                        field3=clean_string(field3)
			string2=field2"="field3
			string=field1"("string2")"
			string=analyze_organism(string,seq_a[a])
			string_to_compare=escape_pattern(string)
				if(string!="(=)") {
                                        if(all_organisms_[seq_a[a]]!~string_to_compare) {if(string!="") {all_organisms_[seq_a[a]]=all_organisms_[seq_a[a]]""string";"}}
                                }
                        }
#Pubmed			
		field=1; b=0; while(field!="") {b=b+1
                        field=read_database_line(input_file,"PubMed",0,":","]",b)
                        field=clean_string(field)
                        # Clean bogus entries
                        if (field~/&nbsp/) {field=""}
                        if (field~/_/) {
                                split(field,gg,"_")
                                field=gg[1]
                                gsub("\\.","",field)
                                gsub(",","",field)
                        }
                       if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}
                       if(PMID_[seq_a[a]]!~patent) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""patent";"}

                       # MEDLINE IDs are not the same as PubMed, I cannot find where to use them and in most cases the simply point to the same as PMID collected in previous line
#		       field=read_database_line(input_file,"MEDLINE",0,":","]",b)
#                       if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}
                }
#Experimental structure/PDB
                        field=read_database_line(input_file,"http://www.pdb.org/pdb/explore",0,"structureId=",">")
                        field=toupper(field)
                        field=clean_string(field)
                        if(field!="") { if(experim_structure_[seq_a[a]]!~field) {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}}
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}
#                        if(field!="") { experimental_[seq_a[a]]="experimental" }
	}
       close(input_file)
	a=a+1
}
wmax=a
}
close(file_list)
return wmax
}
