function read_APD_database_auto(ID_,seq_a,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
		synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
		antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
		tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
		hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
		drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
		pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_,OUTPUT_INPUT,filedir,sequences_file,
		a,b,aa,aaa,p,pp,ppp,f,ff,fff,value,n,z,zmax) {
limit=10000
                a=1;
        limit_=a+limit
        print "" > "tmp_files/modified_aa_apd"
                path="DATABASES/APD/APDfiles/APD"
                length_numb=5
                while (a<limit_) {
#                        if(a<100) {
                          add_tag="yes"
                          tag="" ; if(add_tag=="yes") {imax=length_numb-length(a); i=0; while(i<imax) {  i=i+1; tag=tag"0"}}
#                        }
#                        else {tag=""}

			file=path""tag""a
                        if (system("[ -f " file " ] ") ==0 ) {
                        input_file=file
#seq
                                field=read_database_line(input_file,"peptide_sequence",0,">","</p",1)
                                gsub("_","",field)
                                seq_a[a]=field

                                seq_a[a]=correct_sequence(seq_a[a]) 
                                if(seq_a[a]~/Structures/) {seq_a[a]=""}
                                
                                read_modified_aa(modif,translate_modif)
                                read_modified_aa_per_id()

                                seq_id=tag""a
                                if(modify[seq_id]!="") {
                                   fmax=split(modify[seq_id],ee,";") # We get each change per position
                                   f=0; while(f<fmax) {f=f+1
                                     split(ee[f],gg,"_")
                                     if(translate_modif[gg[2]]=="" && gg[2]!="") {print "translate_modif[\""gg[2]"\"]=\"X\";" >> "tmp_files/modified_aa_apd"; translate_modif[gg[2]]="X"}
                                     seq_a[a]=modify_sequence_in_position(seq_a[a],gg[1],translate_modif[gg[2]])
                                   }
                                }
                                   
				print "Reading "file" for sequence "seq_a[a]""
				seq_a[a]=analyze_sequence(seq_a[a])
#ID
                                field=read_database_line(input_file,"APD_ID",3,"AP","</td>",1)
                                field=clean_string(field)
                                if(ID_[seq_a[a]]!~field) {if(field!="") {ID_[seq_a[a]]=ID_[seq_a[a]]"APD_AP"field";" }}
#name
                                field=read_database_line(input_file,"Name/Class",3,"dontcut","</td>",1)
                                gsub("<td>","",field)
                                sub(/\r/,"",field)
                                field=clean_string(field)
                                if(name_[seq_a[a]]!~field) {if(field!="") {name_[seq_a[a]]=name_[seq_a[a]]""field";" }}
#source
                                field=read_database_line(input_file,"Source",3,">","</td>",1)
				zmax=split(field,bb,">")
				z=0
				field_reduced=""
				while(z<zmax) {z=z+1
				 if(bb[z]!~/a_href=/){field_reduced=field_reduced""bb[z]}
				}
				gsub("</a","",field_reduced)
				gsub("<i/","",field_reduced)
				gsub("<i","",field_reduced)
				gsub("</i","",field_reduced)
				gsub("<td>","",field_reduced)
				gsub("td>","",field_reduced)
				sub(/\r/,"",field_reduced)
				field=field_reduced
				field=clean_string(field)
				gsub("<td","",field)
				gsub("?","",field)
				gsub("<.i","",field)
				field=clean_string(field)
				if(field~/ynthetic/) {synthetic_[seq_a[a]]="synthetic"}
                                if(source_[seq_a[a]]!~field) {if(field!="") {source_[seq_a[a]]=source_[seq_a[a]]""field";" }}
				
#Function
                        field=read_database_line(input_file,"Activity",3,"dontcut","</td>",1)
			gsub("&",",",field); gsub("_","",field);amax=split(field,aa,",")
			aaa=0; while(aaa<amax) {aaa=aaa+1
                        read_database_classify_field(aa[aaa],",",seq_a[a],cyclic_,synthetic_,antimicrobial_,antibacterial_,\
                                                antigram_pos_,antigram_neg_,antifungal_,anitiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
                                                antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
                                                cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
                                                tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_)
				}
#Experimental structure/PDB
                        field=read_database_line(input_file,"PDB ID",0,":","<a")
                        field=toupper(field)
                        field=clean_string(field)
                        if(field!="") { if(experim_structure_[seq_a[a]]!~field) {if(field!="") {experim_structure_[seq_a[a]]=experim_structure_[seq_a[a]]""field";"}}}
                        if(field!="") { if(pdb_[seq_a[a]]!~field) {if(field!="") {pdb_[seq_a[a]]=pdb_[seq_a[a]]""field";"}}}
#                        if(field!="") { experimental_[seq_a[a]]="experimental"}
#Taxonomy
			field=read_database_line(input_file,"Name/Class",3,"dontcut","</td>",1)
			field=clean_string(field)
			       if(field~/XXA/) {if(PTM_[seq_a[a]]!~/amidation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"amidation;"}}
			       gsub("XXA","",field)
                               if(field~/XXC/) {if(PTM_[seq_a[a]]!~/backbone cyclization/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"backbone_cyclization;"}}
                               gsub("XXC","",field)
                               if(field~/XXE/) {if(PTM_[seq_a[a]]!~/acetylation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"acetylation;"}}
                               gsub("XXE","",field)
                               if(field~/XXB/) {if(PTM_[seq_a[a]]!~/chromophore ion binding moieties/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"chromophore_ion_binding_moieties;"}}
                               gsub("XXB","",field)
                               if(field~/XXD/) {if(PTM_[seq_a[a]]!~/D-amino acids/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"D-aminoacids;"}}
                               gsub("XXD","",field)
                               if(field~/XXF/) {if(PTM_[seq_a[a]]!~/carboxylic acid containing unit/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"carboxylic_acid_containing_unit;"}}
                               gsub("XXF","",field)
                               if(field~/XXG/) {if(PTM_[seq_a[a]]!~/glycosylation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"glycosylation;"}}
                               gsub("XXG","",field)
                               if(field~/XXH/) {if(PTM_[seq_a[a]]!~/halogenation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"halogenation;"}}
                               gsub("XXH","",field)
                               if(field~/XXJ/) {if(PTM_[seq_a[a]]!~/sidechain backbone cyclization/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"sidechain_backbone_cyclization;"}}
                               gsub("XXJ","",field)
                               if(field~/XXK/) {if(PTM_[seq_a[a]]!~/hydroxylation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"hydroxylation;"}}
                               gsub("XXK","",field)
                               if(field~/XXL/) {if(PTM_[seq_a[a]]!~/lipidation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lipidation;"}}
                               gsub("XXL","",field)
                               if(field~/XXM/) {if(PTM_[seq_a[a]]!~/methylation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"methylation;"}}
                               gsub("XXM","",field)
                               if(field~/XXN/) {if(PTM_[seq_a[a]]!~/nitrolation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"nitrolation;"}}
                               gsub("XXN","",field)
                               if(field~/XXO/) {if(PTM_[seq_a[a]]!~/oxidation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"oxidation;"}}
                               gsub("XXO","",field)
                               if(field~/XXP/) {if(PTM_[seq_a[a]]!~/phosphorylation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"phosphorylation;"}}
                               gsub("XXP","",field)
                               if(field~/XXQ/) {if(PTM_[seq_a[a]]!~/N-terminal cyclic glutamate/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"N-terminal_cyclic_glutamate;"}}
                               gsub("XXQ","",field)
                               if(field~/XXR/) {if(PTM_[seq_a[a]]!~/reduction/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"reduction;"}}
                               gsub("XXR","",field)
                               if(field~/XXS/) {if(PTM_[seq_a[a]]!~/sulfation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"sulfation;"}}
                               gsub("XXS","",field)
                               if(field~/XXT/) {if(PTM_[seq_a[a]]!~/thioether bridge/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"thioether_bridge;"}}
                               gsub("XXT","",field)
                               if(field~/XXU/) {if(PTM_[seq_a[a]]!~/rana box via a single S-S bond/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"rana_box_via_a_single_S-S_bond;"}}
                               gsub("XXU","",field)
                               if(field~/XXV/) {if(PTM_[seq_a[a]]!~/lantibiotic C-C bridge/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"lantibiotic_C-C_bridge;"}}
                               gsub("XXV","",field)
                               if(field~/XXW/) {if(PTM_[seq_a[a]]!~/dehydration/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"dehydration;"}}
                               gsub("XXW","",field)
                               if(field~/XXX/) {if(PTM_[seq_a[a]]!~/ADP-ribosylation/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"ADP-ribosylation;"}}
                               gsub("XXX","",field)
                               if(field~/XXY/) {if(PTM_[seq_a[a]]!~/citrullination/) {PTM_[seq_a[a]]=PTM_[seq_a[a]]"citrullination;"}}
                               gsub("XXY","",field)

			       if(field~/BBBh2o/) {if(target_[seq_a[a]]!~/in solution/) {target_[seq_a[a]]=target_[seq_a[a]]"in_solution;"}}
                               gsub("BBBh2o","",field)
                               if(field~/BBII/) {if(target_[seq_a[a]]!~/in solution/) {target_[seq_a[a]]=target_[seq_a[a]]"in_solution;"}}
                               gsub("BBII","",field)
                               if(field~/BBS/) {if(target_[seq_a[a]]!~/fungal surface/) {target_[seq_a[a]]=target_[seq_a[a]]"fungal_surface;"}}
                               gsub("BBS","",field)
                               if(field~/BBW/) {if(target_[seq_a[a]]!~/gram\+ bacterial cell wall/) {target_[seq_a[a]]=target_[seq_a[a]]"gram_pos_bacterial_cell_wall;"}}
                               gsub("BBW","",field)
                               if(field~/BBL/) {if(target_[seq_a[a]]!~/gram\- bacterial cell wall/) {target_[seq_a[a]]=target_[seq_a[a]]"gram_neg_bacterial_cell_wall;"}}
                               gsub("BBL","",field)
                               if(field~/BBr/) {if(target_[seq_a[a]]!~/cell surface/) {target_[seq_a[a]]=target_[seq_a[a]]"cell_surface;"}}
                               gsub("BBr","",field)
                               if(field~/BBMm/) {if(target_[seq_a[a]]!~/inner membrane/) {target_[seq_a[a]]=target_[seq_a[a]]"inner_membrane;"}}
                               gsub("BBMm","",field)
                               if(field~/BBBm/) {if(target_[seq_a[a]]!~/membrane/) {target_[seq_a[a]]=target_[seq_a[a]]"membrane;"}}
                               gsub("BBBm","",field)
                               if(field~/BBN/) {if(target_[seq_a[a]]!~/within the cells/) {target_[seq_a[a]]=target_[seq_a[a]]"within_the_cells;"}}
                               gsub("BBN","",field)
                               if(field~/BBribo/) {if(target_[seq_a[a]]!~/within bacteria/) {target_[seq_a[a]]=target_[seq_a[a]]"within_bacteria;"}}
                               gsub("BBribo","",field)
				gsub(",,","",field)
				gsub(" ","_",field)
				gsub("<td>","",field)
				sub(/\r/,"",field)
				field=clean_string(field)
				field=tolower(field)
                                if(taxonomy_[seq_a[a]]!~field) {if(field!="") {taxonomy_[seq_a[a]]=taxonomy_[seq_a[a]]""field";"}}

#all_organisms
field1=read_database_line(input_file,"Activity:",3,"dontcut","</td>",1)
field2=read_database_line(input_file,"Additional info:",3,"dontcut","</td>",1)
# Sometimes, like for APD00001 and APD00001, we need to look again inside Additional
# info section due to subsection paragraph splitting
field3=read_database_line(input_file,"<b>Activity</b>:",0,"dontcut","</td>",1)
field=field1""field2""field3
			gsub("Active","active",field)
			gsub("<td>","",field)
			gsub("< td >","",field)
			gsub("<_td_>","",field)
			sub(/\r/,"",field)
                        sub(/\x02/,"",field) #^B			
                        string=read_activity_description(field,"active_against",seq_a[a],all_organisms_)
                        gsub("<_a_href=\"http:www","",string)
                        gsub("<_a_href=\"https:www","",string)
#Pubmed			
			field=1; b=0; while(field!="") {b=b+1
                        field=read_database_line(input_file,"Reference:",3,"http","\"",b)
                        
                        if (field~/pubmed/) {
			 g=split(field,gg,"/")
			 field=gg[g]
			} else {
			 gsub("://","http://",field)
			 gsub("shttp://","https://",field)
                        }
			
			gsub("td>","",field)
			sub(/\r/,"",field)
                        if(PMID_[seq_a[a]]!~field) {PMID_[seq_a[a]]=PMID_[seq_a[a]]""field";"}
			}

			# http://aps.unmc.edu/AP/about.php
			# Included if:
			# ...
			# (2). antimicrobial activities are demonstrated (MIC <100 uM or 100 ug/ml);
			# (3). the amino acid sequences of the mature peptides have been elucidated, at least partially; and
			if(experimental_[seq_a[a]]=="") {experimental_[seq_a[a]]="experimental"}
		}

a=a+1
	}
       close(input_file)
amax=a

return amax
}
