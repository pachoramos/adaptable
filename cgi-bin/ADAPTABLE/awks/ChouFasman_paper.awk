function ChouFasman_paper(sequ,short,name_seq,\
		charged,missing,pro,symbol,symbol_a,symbol_b,symbol_t,b,B,nb,ave,pp,p,a,A,na,n,nna,nnb,ave_heli,s,ss,ll,pred,fullpred,aa,bb,predic_a,predic_b,predic_t,nucleation_helix,length_helix,length_helix_left,nucleation_beta,length_beta,length_beta_left,calc_turn) {
	heli["A"]=142 ; beta["A"]=83 ; turn["A"]=66 ; fi["A"]=0.06 ; fip1["A"]=0.076 ; fip2["A"]=0.035 ; fip3["A"]=0.058 ;
	heli["R"]=98 ; beta["R"]=93 ; turn["R"]=95 ; fi["R"]=0.070 ; fip1["R"]=0.106 ; fip2["R"]=0.099 ; fip3["R"]=0.085 ;
	heli["D"]=101 ; beta["D"]=54 ; turn["D"]=146 ; fi["D"]=0.147 ; fip1["D"]=0.110 ; fip2["D"]=0.179 ; fip3["D"]=0.081 ;
	heli["N"]=67 ; beta["N"]=89 ; turn["N"]=156 ; fi["N"]=0.161 ; fip1["N"]=0.083 ; fip2["N"]=0.191 ; fip3["N"]=0.091 ;
	heli["C"]=70 ; beta["C"]=119 ; turn["C"]=119 ; fi["C"]=0.149 ; fip1["C"]=0.050 ; fip2["C"]=0.117 ; fip3["C"]=0.128 ;
	heli["E"]=151 ; beta["E"]=037 ; turn["E"]=74 ; fi["E"]=0.056 ; fip1["E"]=0.060 ; fip2["E"]=0.077 ; fip3["E"]=0.064 ;
	heli["Q"]=111 ; beta["Q"]=110 ; turn["Q"]=98 ; fi["Q"]=0.074 ; fip1["Q"]=0.098 ; fip2["Q"]=0.037 ; fip3["Q"]=0.098 ;
	heli["G"]=57 ; beta["G"]=75 ; turn["G"]=156 ; fi["G"]=0.102 ; fip1["G"]=0.085 ; fip2["G"]=0.190 ; fip3["G"]=0.152 ;
	heli["H"]=100 ; beta["H"]=87 ; turn["H"]=95 ; fi["H"]=0.140 ; fip1["H"]=0.047 ; fip2["H"]=0.093 ; fip3["H"]=0.054 ;
	heli["I"]=108 ; beta["I"]=160 ; turn["I"]=47 ; fi["I"]=0.043 ; fip1["I"]=0.034 ; fip2["I"]=0.013 ; fip3["I"]=0.056 ;
	heli["L"]=121 ; beta["L"]=130 ; turn["L"]=59 ; fi["L"]=0.061 ; fip1["L"]=0.025 ; fip2["L"]=0.036 ; fip3["L"]=0.070 ;
	heli["K"]=114 ; beta["K"]=74 ; turn["K"]=101 ; fi["K"]=0.055 ; fip1["K"]=0.115 ; fip2["K"]=0.072 ; fip3["K"]=0.095 ;
	heli["M"]=145 ; beta["M"]=105 ; turn["M"]=60 ; fi["M"]=0.068 ; fip1["M"]=0.082 ; fip2["M"]=0.014 ; fip3["M"]=0.055 ;
	heli["F"]=113 ; beta["F"]=138 ; turn["F"]=60 ; fi["F"]=0.059 ; fip1["F"]=0.041 ; fip2["F"]=0.065 ; fip3["F"]=0.065 ;
	heli["P"]=57 ; beta["P"]=55 ; turn["P"]=152 ; fi["P"]=0.102 ; fip1["P"]=0.301 ; fip2["P"]=0.034 ; fip3["P"]=0.068 ;
	heli["S"]=77 ; beta["S"]=75 ; turn["S"]=143 ; fi["S"]=0.120 ; fip1["S"]=0.139 ; fip2["S"]=0.125 ; fip3["S"]=0.106 ;
	heli["T"]=83 ; beta["T"]=119 ; turn["T"]=96 ; fi["T"]=0.086 ; fip1["T"]=0.108 ; fip2["T"]=0.065 ; fip3["T"]=0.079 ;
	heli["W"]=108 ; beta["W"]=137 ; turn["W"]=96 ; fi["W"]=0.077 ; fip1["W"]=0.013 ; fip2["W"]=0.064 ; fip3["W"]=0.167 ;
	heli["Y"]=69 ; beta["Y"]=147 ; turn["Y"]=114 ; fi["Y"]=0.082 ; fip1["Y"]=0.065 ; fip2["Y"]=0.114 ; fip3["Y"]=0.125 ;
	heli["V"]=106 ; beta["V"]=170 ; turn["V"]=50 ; fi["V"]=0.062 ; fip1["V"]=0.048 ; fip2["V"]=0.028 ; fip3["V"]=0.053 ;
#	heli["-"]=0 ; beta["-"]=0 ; turn["-"]=0 ; fi["-"]=0 ; fip1["-"]=0 ; fip2["-"]=0 ; fip3["-"]=0
	pp=split(sequ,pept,"")

		delete symbol
		delete symbol_a
                delete symbol_b
                delete symbol_t
		delete aa
		delete bb
		delete predic_a
		delete predic_b
		delete predic_t
		delete nucleation_helix
		delete length_helix
		delete length_helix_left
		delete nucleation_beta
		delete length_beta
		delete length_beta_left
		delete calc_turn
# find nucleation of alpha helices and extend them at both ends (out of nna segments, the segment n start in position p with total length length_helix[n] given by a nucleous of 4 + ss residues at the right and ll residues at the left)
		n=0
		p=0 ; while(p<pp) {p=p+1
			A=0;a=0;na=0;ave=0;ss=0; pro=""; missing=""
				s=-1 ; while(s<3) {s=s+1 ; 
				if((s+p)<=pp) {ave=ave+heli[pept[s+p]]} ; 
				if(heli[pept[s+p]]>100) {A=A+1};
				if(heli[pept[s+p]]>99 && heli[pept[s+p]]<102) {a=a+1} ; 
				if(heli[pept[s+p]]<70) {na=na+1}
				if(pept[s+p]=="P"){pro="yes"}
				if(pept[s+p]=="-"){missing="yes"}}; ave=ave/4;  
		if(ave>100 && ((A>2 && a>1) || A>3) && na<3 && pro=="" && missing=="") {
					ss=0 ; while(ss<2 && ((heli[pept[p+s+ss+1]]+heli[pept[p+s+ss]]+heli[pept[p+s+ss-1]]+heli[pept[p+s+ss-2]])/4)>100 && pept[p+s+ss+1]!="P" && pept[p+s+ss+1]!="-" && (p+s+ss+1)<=pp) {ss=ss+1
					}
				if (ss==2) {n=n+1; nucleation_helix[n]=p ; length_helix[n]=4+ss; 
                                        ss=0 ; while(((heli[pept[p+s+ss+1]]+heli[pept[p+s+ss]]+heli[pept[p+s+ss-1]]+heli[pept[p+s+ss-2]])/4)>100 && pept[p+s+ss+1]!="P" && pept[p+s+ss+1]!="-" && (p+s+ss+1)<=pp) {ss=ss+1
                                        }
				length_helix[n]=length_helix[n]+ss;length_helix_left[n]=0
				}
		#	p=p+5+ss;
		}
}
nna=n
# find nucleation of beta strands and extend them at both ends (out of nnb segments, the segment n start in position p with total length length_helix[n] given by a nucleous of 4 + ss residues at the right and ll residues at the left)
n=0
p=0 ; while(p<pp) {p=p+1
	B=0;b=0;nb=0;ave=0;ss=0;charged=0;missing=""
		s=-1 ; while(s<3) {s=s+1; if((s+p)<=pp) {ave=ave+beta[pept[s+p]]}; 
						if(beta[pept[s+p]]>100) {B=B+1}; 
						if(beta[pept[s+p]]<70) {nb=nb+1};
				#		if(pept[s+p]=="R"|| pept[s+p]=="K"|| pept[s+p]=="E"){charged=charged+1}
						if(pept[s+p]=="-"){missing="yes"}}; ave=ave/4 ;
			if(ave>100 && B>2 && nb<3 && missing=="" && charged==0) { 
						ss=0 ; while(ss<2 && ((beta[pept[p+s+ss+1]]+beta[pept[p+s+ss]]+beta[pept[p+s+ss-1]]+beta[pept[p+s+ss-2]])/4)>100 && pept[p+s+ss+1]!="-" &&\
						(p+s+ss+1)<=pp) {ss=ss+1
						}
					if (ss==2) {n=n+1; nucleation_beta[n]=p ; length_beta[n]=4+ss;
                                                ss=0 ; while(((beta[pept[p+s+ss+1]]+beta[pept[p+s+ss]]+beta[pept[p+s+ss-1]]+beta[pept[p+s+ss-2]])/4)>100 && pept[p+s+ss+1]!="-" &&\
                                                (p+s+ss+1)<=pp) {ss=ss+1
                                                }
						length_beta[n]=length_beta[n]+ss ; length_beta_left[n]=0
					}
		#		p=p+4+ss
				}
}
nnb=n
# find turns in position p
p=0 ; while(p<pp) {p=p+1
	ave_heli=0
		ave_beta=0
		ave_turn=0
		calc_turn[p]=fi[pept[p]]*fip1[pept[p+1]]*fip2[pept[p+2]]*fip3[pept[p+3]]
		s=-1 ; while(s<4) {s=s+1
				ave_heli=ave_heli+heli[pept[p+s]]
				ave_beta=ave_beta+beta[pept[p+s]]
				ave_turn=ave_turn+turn[pept[p+s]]
		}
	ave_heli=ave_heli/4
		ave_beta=ave_beta/4
		ave_turn=ave_turn/4
		if (calc_turn[p]>0.000075 && ave_turn>100 && ave_turn>ave_beta && ave_turn>ave_heli) {
			predic_t[p]="T"
			symbol_t[p]="◠"
		}
}
# assign secondary structures alpha if conditions apply (length of segment, comparison of probability with beta)

n=0; while(n<nna) {n=n+1
	ave_heli=0
		ave_beta=0
		s=nucleation_helix[n]-length_helix_left[n]-1; while (s<nucleation_helix[n]+length_helix[n]-1-length_helix_left[n]) {s=s+1
			ave_heli=ave_heli+heli[pept[s]] 
				ave_beta=ave_beta+beta[pept[s]]
		}
	ave_heli=ave_heli/length_helix[n]
		ave_beta=ave_beta/length_helix[n]
		if(length_helix[n]>5 && ave_heli>103) {
			s=nucleation_helix[n]-length_helix_left[n]-1; while (s<nucleation_helix[n]+length_helix[n]-1-length_helix_left[n]) {s=s+1
				predic_a[s]="H"; 
	if(s==nucleation_helix[n]-length_helix_left[n]) {symbol_a[s]="◖"}
	else {symbol_a[s]="■"}
	if(s==nucleation_helix[n]+length_helix[n]-1-length_helix_left[n]) {symbol_a[s]="◗"}	
			}
		}
}
# assign secondary structures beta if conditions apply (length of segment, comparison of probability with alpha)
n=0; while(n<nnb) {n=n+1
	ave_heli=0
		ave_beta=0
		s=nucleation_beta[n]-length_beta_left[n]-1; while (s<nucleation_beta[n]+length_beta[n]-1-length_beta_left[n]) {s=s+1
			ave_heli=ave_heli+heli[pept[s]] 
				ave_beta=ave_beta+beta[pept[s]]
		}
	ave_heli=ave_heli/length_beta[n]
		ave_beta=ave_beta/length_beta[n]
	#	if(ave_beta>ave_heli || ave_beta>100) {
	#		s=nucleation_beta[n]-length_beta_left[n]-1; while (s<nucleation_beta[n]+length_beta[n]) {s=s+1
	#			predic_b_easy[s]="B"
	#		}
	#	}
	if(ave_beta>103) {
		s=nucleation_beta[n]-length_beta_left[n]-1; while (s<nucleation_beta[n]+length_beta[n]-1-length_beta_left[n]) {s=s+1
			predic_b[s]="B" ; if (s==nucleation_beta[n]+length_beta[n]-1-length_beta_left[n]) {symbol_b[s]="▶"} else {symbol_b[s]="▬" }
		}
	}
}
# write the prediction; for segments with multiple assignment compare probabilities
pred=""
p=0 ; while(p<pp) {p=p+1
	ave_heli=0
		ave_beta=0
		if (predic_t[p]=="" && predic_a[p]=="" && predic_b[p]=="") {pred=pred"-"; symbol[p]="-"}
		else {
			if (predic_t[p]!="") {pred=pred"T"; symbol[p]=symbol_t[p]}
			else {
				if(predic_a[p]!="" && predic_b[p]=="") {pred=pred"H"; symbol[p]=symbol_a[p]}
				if(predic_b[p]!="" && predic_a[p]=="") {pred=pred"B";symbol[p]=symbol_b[p]}
				if (predic_a[p]!="" && predic_b[p]!="") {
					s=0; while(predic_a[p+s]!="" && predic_b[p+s]!="")
					{
						ave_heli=ave_heli+heli[pept[p+s]]
							ave_beta=ave_beta+beta[pept[p+s]]
					s=s+1
					}
					ss=s
						ave_heli=ave_heli/ss
						ave_beta=ave_beta/ss
						if(ave_heli>=ave_beta) {s=0; while(s<ss) {if(predic_t[p+s]==""){pred=pred"H";symbol[p+s]=symbol_a[p+s]} else {pred=pred"T";symbol[p+s]=symbol_t[p+s]}; 
						if(s==0) {symbol[p+s-1]="▶"} ; s=s+1}}
						else {s=0; while(s<ss) {if(predic_t[p+s]==""){pred=pred"B";symbol[p+s]=symbol_b[p+s]} else {pred=pred"T";symbol[p+s]=symbol_t[p+s]};s=s+1 }}
				p=p+ss-1
				}
			}
		}
}
#	print sequ
ll=split(pred,tmp11,"")
if(short=="") {
print "helix"
l=0;while(l<ll) {l=l+1
	if(predic_a[l]=="H") {color("H","red","","bold")}
	else  {color("-","black","","bold")}
}
print ""
print "strand"
l=0;while(l<ll) {l=l+1
	if(predic_b[l]=="B") {color("B","blue","","bold")}
	else  {color("-","black","","bold")}
}
print ""
#print "strand_biased"
#l=0;while(l<ll) {l=l+1
#	if(predic_b_easy[l]=="B") {color("B","cyan","","bold")}
#	else  {color("-","black","","bold")}
#}
#print ""
print "turn"
l=0;while(l<ll) {l=l+1
	if(predic_t[l]=="T") {color("T","green","","bold")}
	else  {color("-","black","","bold")}
}
print ""
#print "Consensus"
#l=0;while(l<ll) {l=l+1
#	if(tmp11[l]=="H") {color(""tmp11[l]"","red","","bold")}
#	if(tmp11[l]=="B") {color(""tmp11[l]"","blue","","bold")}
#	if(tmp11[l]=="T") {color(""tmp11[l]"","green","","bold")}
#	if(tmp11[l]=="-") {color(""tmp11[l]"","black","","bold")}
#}
#print ""
print "Consensus"
}
l=0;while(l<ll) {l=l+1
        if(tmp11[l]=="H") {color(""symbol[l]"","red","red","bold")}
        if(tmp11[l]=="B") {color(""symbol[l]"","blue","","bold")}
        if(tmp11[l]=="T") {color(""symbol[l]"","green","","bold")}
        if(tmp11[l]=="-") {color(""tmp11[l]"","black","","bold")}
}
print "; "name_seq""
if(short=="") {
print "________________________________________________________________________________________"
}
fullpred=pred
return fullpred
}

