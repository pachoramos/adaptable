@include "awks/library.awk"
# In BEGIN choose for each parameter y (yes), i (indifferent) or n (not). 
# Parmameters which imply a name or a number (e.g. tissue="Breast" or IC50_cancer=3) can only have their value or "i" (indifferent). 
# Choose also the sequence length range (e.g "13-28").
# Launch as awk -f read_dramp_database.awk database, where database is the list of sequences in fasta format where all the properties are in the description line
# Example: all_organisms="coccus" ; antiyeast="n" will select all the peptides active against cocci without antiyeast activity 
# In order to avoid certain type you can add n_ in front. Example all_organisms="n_coccus" will select only peptides active against all organisms except cocci.
BEGIN{
# Try to not care about lower/uppercase
# https://www.gnu.org/software/gawk/manual/html_node/Case_002dsensitivity.html
	IGNORECASE = 1
	parameters(prp_name,prp_option)
	# Create all dirs we will need
	system("mkdir -p tmp_files/")
	system("mkdir -p tmp_files2/")
	system("mkdir -p OUTPUTS/"calculation_label"")

#####################################################################################################################################################
	printf "" > "OUTPUTS/"calculation_label"/selection"
	if(plot_graphs=="y") {printf "" > "tmp_files/tmp0_"calculation_label}

	# mofify parameters if given by command line as: awk -f awks/analyse_AMPs_database.awk DATABASE -v "calculation_label="prova2"" -v "all_organisms="aureus""
	p=0;while(p<length(prp_name)) {p=p+1
		if (prp_option[p]=="" && p<=63) {prp_option[p]="i"}
		n=0;while(n<90) {n=n+1
			if(ARGV[n]~prp_name[p]) {split(ARGV[n],fff,"=");prp_option[p]=fff[2]}
		}
	}

	print_parameters(prp_name,prp_option,sequence_length_range,n_regions,align_method,level_agreement,families_to_analyse,work_in_simplified_aa_space)
	read_blosum("tmp_files/align_matrix",blosum)
	max_len=0
	d=0

	read_aa_prop_and_colors(aa_name,conv_aa,class,class_def,class_length,type,polarity,color_type)          
	print ""
	print "Selected sequences:"
#######################################################################################################################################################
# Convert non-standard aa to standard if requested
datafile="DATABASES/Sidechain_database_PubChem_global"
        while ( getline < datafile >0 ) {
        standard[$3]=$2
}
close(datafile)
}
{
# 1) SELECTION OF SEQUENCES. Sequences are selected according to criteria
# Replace - by any accepted residue
if(sequence~/-/ || sequence~/{/) {
	pattern=sequence

	# Replace '-' by any accepted char
	gsub("-","[ABCDEFGHIJKLMNÑOPQRSTUVWXYZԱԲԳԴԵԶԷԸԹԺԻԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՎՐՑՒՓՔՖЀЁЂЃЄЇЉЊЋЌЍЎЏБГДЖЗИЙЛПЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҰҲӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸ¢£¤¥§¶¼½¾˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅■□▢▣▤▥▦▧▨▩▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◊◈◇◆◄◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✔✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΠΦΨΩΪΫϴἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊῌῘῙῚΊῨῩῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]",pattern)

	# Allow people to replace for their desired residues
        # https://www.unix.com/shell-programming-and-scripting/152934-two-delimiters-awk.html
        n=split(sequence,user_pattern,"{");     # split the input line based on open
        for( i = 2; i <= n; i++ )               # for each open; skip things before first hence i=2
        {
                gsub( "}.*", "", user_pattern[i] );     # delete everything after close
                user_pattern_nocommas[i]=user_pattern[i]
                gsub( ",", "", user_pattern_nocommas[i] );      # delete commas for getting it ready to be used as a pattern later
                # Special cases
                if(user_pattern_nocommas[i]=="hydrophobic") {user_pattern_nocommas[i]="AVILM"}
                if(user_pattern_nocommas[i]=="negative") {user_pattern_nocommas[i]="DE"}
                if(user_pattern_nocommas[i]=="positive") {user_pattern_nocommas[i]="KR"}
                if(user_pattern_nocommas[i]=="aromatic") {user_pattern_nocommas[i]="WYHF"}
                if(user_pattern_nocommas[i]=="polar") {user_pattern_nocommas[i]="STNQ"}
                # Do final substitutions
                gsub("{"user_pattern[i]"}","["user_pattern_nocommas[i]"]",pattern)
        }
} else
# Set something when a 'positional' pattern of this style (AA-AA-KKVK) is not provided as, otherwise, awk considers $2~pattern as OK with an empty pattern
{pattern="--no-pattern-supplied--"}
print pattern

if($1~">") {
		split(sequence_length_range,tmp,"-") ; upper=tmp[2]+0 ; lower=tmp[1]+0
			if(\
					(($1~ID) || ID=="i" || ($1!~substr(ID,3,length(ID)) && substr(ID,1,2)=="n_")) && \
					(($2~sequence) || sequence=="i" || ($2!~substr(sequence,3,length(sequence)) && substr(sequence,1,2)=="n_") || $2~pattern) && \
					(($3~name) || name=="i" || ($3!~substr(name,3,length(name)) && substr(name,1,2)=="n_")) && \
					(($4~source) || source=="i" || ($4!~substr(source,3,length(source)) && substr(source,1,2)=="n_")) && \
					(($5~Family) || Family=="i" || ($5!~substr(Family,3,length(Family)) && substr(Family,1,2)=="n_")) && \
					(($6~gene) || gene=="i" || ($6!~substr(gene,3,length(gene)) && substr(gene,1,2)=="n_")) && \
					(($7~stereo) || stereo=="i" || ($7!~substr(stereo,3,length(stereo)) && substr(stereo,1,2)=="n_")) && \
					(($8~N_terminus) || N_terminus=="i" || ($8!~substr(N_terminus,3,length(N_terminus)) && substr(N_terminus,1,2)=="n_")) && \
					(($9~C_terminus) || C_terminus=="i" || ($9!~substr(C_terminus,3,length(C_terminus)) && substr(C_terminus,1,2)=="n_")) && \
					(($10~PTM) || PTM=="i" || ($10!~substr(PTM,3,length(PTM)) && substr(PTM,1,2)=="n_")) && \
					((cyclic=="y" && $11!="_")|| cyclic=="i" || (cyclic=="n" && $11=="_")) && \
					(($12~target) || target=="i" || ($12!~substr(target,3,length(target)) && substr(target,1,2)=="n_")) && \
					((synthetic=="y" && $13!="_")|| synthetic=="i" || (synthetic=="n" && $13=="_")) && \
					((antimicrobial=="y" && $14!="_")|| antimicrobial=="i" || (antimicrobial=="n" && $14=="_")) && \
					((antibacterial=="y" && $15!="_")|| antibacterial=="i" || (antibacterial=="n" && $15=="_")) && \
					((antigram_pos=="y" && $16!="_")|| antigram_pos=="i" || (antigram_pos=="n" && $16=="_")) && \
					((antigram_neg=="y" && $17!="_")|| antigram_neg=="i" || (antigram_neg=="n" && $17=="_")) && \
					((antifungal=="y" && $18!="_")|| antifungal=="i" || (antifungal=="n" && $18=="_")) && \
					((antiyeast=="y" && $19!="_")|| antiyeast=="i" || (antiyeast=="n" && $19=="_")) && \
					((antiviral=="y" && $20!="_")|| antiviral=="i" || (antiviral=="n" && $20=="_")) && \
					((antiprotozoal=="y" && $21!="_")|| antiprotozoal=="i" || (antiprotozoal=="n" && $21=="_")) && \
					((antiparasitic=="y" && $22!="_")|| antiparasitic=="i" || (antiparasitic=="n" && $22=="_")) && \
					((antiplasmodial=="y" && $23!="_")|| antiplasmodial=="i" || (antiplasmodial=="n" && $23=="_")) && \
					((antitrypanosomic=="y" && $24!="_")|| antitrypanosomic=="i" || (antitrypanosomic=="n" && $24=="_")) && \
					((antileishmania=="y" && $25!="_")|| antileishmania=="i" || (antileishmania=="n" && $25=="_")) && \
					((insecticidal=="y" && $26!="_")|| insecticidal=="i" || (insecticidal=="n" && $26=="_")) && \
					((anticancer=="y" && $27!="_")|| anticancer=="i" || (anticancer=="n" && $27=="_")) && \
					((antitumor=="y" && $28!="_")|| antitumor=="i" || (antitumor=="n" && $28=="_")) && \
					((antiangiogenic=="y" && $34!="_")|| antiangiogenic=="i" || (antiangiogenic=="n" && $34=="_")) && \
					((toxic=="y" && $35!="_")|| toxic=="i" || (toxic=="n" && $35=="_")) && \
					((cytotoxic=="y" && $36!="_")|| cytotoxic=="i" || (cytotoxic=="n" && $36=="_")) && \
					((hemolytic=="y" && $37!="_")|| hemolytic=="i" || (hemolytic=="n" && $37=="_")) && \
					((cell_cell=="y" && $41!="_")|| cell_cell=="i" || (cell_cell=="n" && $41=="_")) && \
					((hormone=="y" && $42!="_")|| hormone=="i" || (hormone=="n" && $42=="_")) && \
					((quorum_sensing=="y" && $43!="_")|| quorum_sensing=="i" || (quorum_sensing=="n" && $43=="_")) && \
					((immunomodulant=="y" && $44!="_")|| immunomodulant=="i" || (immunomodulant=="n" && $44=="_")) && \
					((antihypertensive=="y" && $45!="_")|| antihypertensive=="i" || (antihypertensive=="n" && $45=="_")) && \
					((drug_delivery=="y" && $46!="_")|| drug_delivery=="i" || (drug_delivery=="n" && $46=="_")) && \
					((cell_penetrating=="y" && $47!="_")|| cell_penetrating=="i" || (cell_penetrating=="n" && $47=="_")) && \
					((tumor_homing=="y" && $48!="_")|| tumor_homing=="i" || (tumor_homing=="n" && $48=="_")) && \
					((blood_brain=="y" && $49!="_")|| blood_brain=="i" || (blood_brain=="n" && $49=="_")) && \
					((antioxidant=="y" && $50!="_")|| antioxidant=="i" || (antioxidant=="n" && $50=="_")) && \
					((antiproliferative=="y" && $51!="_")|| antiproliferative=="i" || (antiproliferative=="n" && $51=="_")) && \
					(($52~DSSP) || DSSP=="i" || ($52!~substr(DSSP,3,length(DSSP)) && substr(DSSP,1,2)=="n_")) && \
					((work_in_simplified_aa_space=="DSSP" && $52!="_") || work_in_simplified_aa_space!="DSSP") && \
					(($53~pdb) || pdb=="i" || ($53!~substr(pdb,3,length(pdb)) && substr(pdb,1,2)=="n_")) && \
					(($54~experim_structure) || experim_structure=="i" || ($54!~substr(experim_structure,3,length(experim_structure)) && substr(experim_structure,1,2)=="n_")) && \
					(($55~PMID) || PMID=="i" || ($55!~substr(PMID,3,length(PMID)) && substr(PMID,1,2)=="n_")) && \
					(($56~taxonomy) || taxonomy=="i" || ($56!~substr(taxonomy,3,length(taxonomy)) && substr(taxonomy,1,2)=="n_")) && \
					((ribosomal=="y" && $60!="_")|| ribosomal=="i" || (ribosomal=="n" && $60=="_")) && \
					((experimental=="y" && $61!="_")|| experimental=="i" || (experimental=="n" && $61=="_")) && \
					((biofilm=="y" && $62!="_")|| biofilm=="i" || (biofilm=="n" && $62=="_")))
{
				        activity_pass=""; delete allo; delete allo2; delete allo3; delete ppp; delete act; delete org
			                oo=split(all_organisms,org,",")
			                aa=split(activity,act,",")
			                tmp_activity_pass_tot=0
			                o=0;while(o<oo) {o=o+1;
			                	tmp_activity_pass=0; 
			                        if(act[o]!~">" && act[o]!~"<") {larger="no";} else {if(act[o]~">") {larger="yes"; gsub(">","",act[o])} ; if(act[o]~"<") {larger="no";gsub("<","",act[o])}} ; if(act[o]=="" || act[o]=="X") {act[o]="i"}                                        
			                        if(($57!~org[o] && org[o]!="" && org[o]!="i") || ($57~substr(org[o],3,length(org[o])) && substr(org[o],1,2)=="n_") || (org[o]=="X" && $57=="_") || (act[o]=="X" && $57=="_") ) {activity_pass=0; if ($1!~substr(org[o],3,length(org[o])) && substr(org[o],1,2)=="n_"){tmp_activity_pass=1}}
			                        else {llll=split($57,allo,";");$65=""; $66=""
				                        	lll=0; while(lll<llll) {lll=lll+1;
			                                        split(allo[lll],ppp,"(");split(ppp[2],allo2,"=");
                        			                split(allo2[2],allo3,"_")

                        			                activity_value=allo3[1]
		                                                activity_type=allo2[1]
		                                                
		                                                temp=substr(allo2[2],1,length(allo2[2])-1)
		                                                if(temp!="") {$65=$65""substr(allo2[2],1,length(allo2[2])-1)";"}
                			                        if(allo2[1]!="") {$66=$66""allo2[1]";"}
								if( \
		                                                        (allo[lll]~org[o] || org[o]=="" || org[o]=="i") && \
		                                                        (((activity_value+0)<=(act[o]+0) && activity_value>0 && larger=="no") || ((activity_value+0)>=(act[o]+0) && activity_value>0 && larger=="yes") || act[o]=="i" || act[o]=="" ) && \
                		                                        ((activity_type~activity_test) || activity_test=="i" || (activity_type!~substr(activity_test,3,length(activity_test)) && substr($66,1,2)=="n_")) || \
                		                                        (org[o]=="X" && allo[lll]!="" && (act[o]=="X" || act[o]=="" || act[o]=="i" || ((activity_value+0)<=(act[o]+0) && activity_value>0 && larger=="no") || ((activity_value+0)>=(act[o]+0) && activity_value>0 && larger=="yes"))) || \
                                                		        (act[o]=="X" && activity_value!="" && (org[o]=="X" || org[o]=="" || org[o]=="i" || allo[lll]~org[o])) \
                                          			) {
                                                			tmp_activity_pass=1
#$65=substr(allo2[2],1,length(allo2[2])-1)
#$66=allo2[1] 
                                        			}
                                			}
                        			}
		                        tmp_activity_pass_tot=tmp_activity_pass_tot+tmp_activity_pass
                			}
                				if(tmp_activity_pass_tot==oo) {activity_pass=1}

						if($65=="") {$65="_"}; if($66=="") {$66="_"}
	                                        hemolytic_pass=""; delete allo; delete allo2; delete allo3; delete ppp
                                                if(($40!~RBC_source && RBC_source!="" && RBC_source!="i") || ($40~substr(RBC_source,3,length(RBC_source)) && substr(RBC_source,1,2)=="n_")) {hemolytic_pass=0}
                                                else {split($40,allo,";");$38=""; $39=""
                                                                lll=0; while(lll<length(allo)) {lll=lll+1;
                                                                split(allo[lll],ppp,"(");split(ppp[2],allo2,"=");
                                                                split(allo2[2],allo3,"_")
                                                                activity_value=allo3[1]
                                                                activity_type=allo2[1]
								temp=substr(allo2[2],1,length(allo2[2])-1)
								if(temp!="") {$38=$38""substr(allo2[2],1,length(allo2[2])-1)";"}
								if(allo2[1]!="") {$39=$39""allo2[1]";"}
                                                                if((allo[lll]~RBC_source || RBC_source=="" || RBC_source=="i") && \
                                                                (((activity_value+0)<=hemolytic_activity && activity_value>0) || hemolytic_activity=="i" || hemolytic_activity=="" ) && \
                                                                 ((activity_type~hemolytic_activity_test) || hemolytic_activity_test=="i" || (activity_type!~substr(hemolytic_activity_test,3,length(hemolytic_activity_test)) && substr($39,1,2)=="n_"))) {
                                                                        hemolytic_pass=1
                                                                       # $38=substr(allo2[2],1,length(allo2[2])-1)
                                                                        #$39=allo2[1]
                                                                        }
                                                                        }
                                                }
                                                if($38=="") {$38="_"}; if($39=="") {$39="_"}
# NB. $58 must be updated with the $ of the virus name
                                               viral_pass=""; delete allo; delete allo2; delete allo3; delete ppp
                                                if(($58!~virus_name && virus_name!="" && virus_name!="i") || ($58~substr(virus_name,3,length(virus_name)) && substr(virus_name,1,2)=="n_")) {viral_pass=0}
                                                else {split($58,allo,";");$63=$58; $58=""; $59=""
                                                                lll=0; while(lll<length(allo)) {lll=lll+1;
                                                                	split(allo[lll],ppp,"(");
									#$63=$63""ppp[1]";";
									split(ppp[2],allo2,"=");
	                                                                split(allo2[2],allo3,"_")
        	                                                        activity_value=allo3[1]
        	                                                        activity_type=allo2[1]
									temp=substr(allo2[2],1,length(allo2[2])-1)
									if(temp!="") {$58=$58""substr(allo2[2],1,length(allo2[2])-1)";"}
									if(allo2[1]!="") {$59=$59""allo2[1]";"}
	                                                                if((allo[lll]~virus_name || virus_name=="" || virus_name=="i") && \
        	                                                        (((activity_value+0)<=activity_viral && activity_value>0) || activity_viral=="i" || activity_viral=="" ) && \
                	                                                ((activity_type~activity_viral_test) || activity_viral_test=="i" || (activity_type!~substr(activity_viral_test,3,length(activity_viral_test)) && substr($59,1,2)=="n_"))) {
                        		                                                viral_pass=1
                                        		                               # $58=substr(allo2[2],1,length(allo2[2])-1)
                                                        		               # $59=allo2[1]
                                                                        }
								}
                                                }
                                                if($58=="") {$58="_"}; if($59=="") {$59="_"}; if($63=="") {$63="_"};

                                               anticancer_pass=""; delete allo; delete allo2; delete allo3; delete ppp
                                                if(($29!~cell_line && cell_line!="" && cell_line!="i") || ($29~substr(cell_line,3,length(cell_line)) && substr(cell_line,1,2)=="n_") || \
						($30!~tissue && tissue!="" && tissue!="i") || ($30~substr(tissue,3,length(tissue)) && substr(tissue,1,2)=="n_") || \
						($31!~cancer_type && cancer_type!="" && cancer_type!="i") || ($31~substr(cancer_type,3,length(cancer_type)) && substr(cancer_type,1,2)=="n_")) {anticancer_pass=0}
                                                else {split($31,allo,";");$32=""; $33=""
                                                                lll=0; while(lll<length(allo)) {lll=lll+1;
                                                                split(allo[lll],ppp,"(");split(ppp[2],allo2,"=");
                                                                split(allo2[2],allo3,"_")
                                                                activity_value=allo3[1]
                                                                activity_type=allo2[1]
								temp=substr(allo2[2],1,length(allo2[2])-1)
								if(temp!="") {$32=$32""substr(allo2[2],1,length(allo2[2])-1)";"}
								if(allo2[1]!="") {$33=$33""allo2[1]";"}
                                                                if((allo[lll]~cancer_type || cancer_type=="" || cancer_type=="i") && ($29~cell_line || cell_line=="" || cell_line=="i") && ($30~tissue || tissue=="" || tissue=="i") && \
                                                                (((activity_value+0)<=anticancer_activity && activity_value>0) || anticancer_activity=="i" || anticancer_activity=="" ) && \
                                                                 ((activity_type~anticancer_activity_test) || anticancer_activity_test=="i" || (activity_type!~substr(anticancer_activity_test,3,length(anticancer_activity_test)) && substr($33,1,2)=="n_"))) {
                                                                        anticancer_pass=1
                                                                       # $32=substr(allo2[2],1,length(allo2[2])-1)
                                                                       # $33=allo2[1]
                                                                        }
                                                                        }
                                                }
                                                if($32=="") {$32="_"}; if($33=="") {$33="_"}

						# Adapt to the list of valid aminoacid codes!!
						if (activity_pass==1 && hemolytic_pass==1 && viral_pass==1 && anticancer_pass==1 && $2~/^[ACDEFGHIKLMNPQRSTVWYOUXԱԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕՖЀЁЂЃЄЅІЇЈЉЊЋЌЍЎЏАБВГДЕЖЗИЙКЛМНОПРСТЯЮЭЬЫЪЩШЧЦХФУѢҐҒҔҖҘҚҢҤҪҬҮҰҲҺӀӁӃӇӋӐӒӔӖӘӚӜӞӠӢӤӦӨӪӬӮӰӲӴӶӸԚԜ$0¢£¤¥§¶¼½¾˞˥˦˧˨˩؟฿†‡‣‰‱‽⁅⁆⁋₠₡₢₣₤₥₦₧₨₩₪₫€₭₮₯₰₱₲₳₴₵₸₹₺₽ℂ℅ℍℎℏℕ№℗ℙℚℝℤ℮ⅈ∀∁∂∃∄∅∆∇∈∉∊∋∌∍∏∐∑∓√∛∜∝∞∟∠∤∧∨∩∪∫≎≍≌≋≊∻∺∹∷≏≐≑≒≓≔≖≗≘≙≚≛≜≝≞≟≸≹≼≽≾≿⊄⊅⊆⊇⊈⊉⊊⊋⊍⊎⊏⊐⊒⊑⊓⊔⊵⊴⊬⊥⊤⊣⊢⊡⊠⊟⊞⊝⊜⊛⊚⊙⊘⊗⊖⊕⊸⋂⋃⋄⋎⋏⋐⋑⋚⋭⋬⋫⋪⋩⋨⋧⋦⋥⋤⋣⋢⋡⋠⋟⋞⋝⋜⋛⌀⌁⌂⌅⌆⌑⌓⌔⌕⌘⌙⍃⍂⍁⍀⌿⌾⌽⌼⌻⌺⌹⌸⌷⌶⌫⌨⌧⌦⌥⍄⍅⍆⍇⍈⍉⍊⍋⍌⍍⍎⍏⍐⍑⍒⍓⍔⍕⍖⍗⍙⍚⍛⍜⍝⍞⍟⍠⍡⍢⍣⍤⍥⍦⎉⎈⎃⎂⎁⎀⍽⍺⍹⍸⍷⍶⍵⍴⍳⍲⍱⍰⍯⍮⍭⍬⍫⍪⍩⍨⍧⎊⎋⎕⏏⏎┅░▒▓▗▘▙▚▛▜▝▞▟■□▢▣▤▥▦▧▨▩▪▫▬▭▮▯▰▱▲△▴▵▶▷▸▹◛◚◙◘◗◖◕◔◓◒◑◐●◎◍◌○◊◉◈◇◆◅◄◃◂◀◁▼▽►◢◣◤◥◧◨◩◪◫◬◭◮◰◱◲◯◳◴◵◶◷◸◹◺◻◼◽◾◿☢☡☠☟☞☝☜☛☚☙☘☗☖☕☔☓☒☑☐☏☎☍☌☋☊☉☈☇☆★☄☃☂☁☀☣☤☥☦☧☨☩☪☫☬☭☮☯☸☹☺☻☼☽☾☿♀♁♂♃♄♅♨♧♦♥♤♣♢♡♠♟♞♝♜♛♚♙♘♗♖♕♔♓♒♑♐♏♎♍♌♋♊♉♈♇♆♩♪♫♬♭♮♯♰♱♲♳♴♵♶♷♸♹♺♻♼♽♾♿⚀⚁⚂⚃⚄⚅⚆⚇⚈⚉⚐⚑⚒⚓⚔⚕⚖⚗⚘⚙⚚⚛⚜⚠⚡⚰⚱✁✂✃✄✆✇✈✉✌✍✎✏✐✑✒✓✔✕✖✗✘✙✚✛✜✝✞✟✠✡✢✣✤✥✦✧✩✪✬✫✭✮✯✰✱✲✳✵✴✶✷✸✹✺✻✼✽✾✿❀❁❂❃❄❅❆❇❈❉❊❋❍❏❐❑❒❖❡❢❣❤❥❦❧⟆⟅⟂➾➽➼➻➺➹➸➷➶➵➴➳➲➱➯➮➭➬➫➪➩➨➧➦➥➤⟜⟠⧻⧺⩫⬒⬓⬔⬕⬖⬗⬘⬙⸘⸟აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰჱჲჳჴჵჶჷჸჹჺͶΆΈΉΊΌΎΏΐΞΣΤΥΠΦΨΩΪΫϴϺἈἉἊἋἌἍἎἏἘἙἚἛἜἝἨἩἪἫἬἭἮἯἸἹἺἻἼἽἾἿὈὉὊὋὌὍὙὛὝὟὨὩὪὫὬὭὮὯᾈᾉᾊᾋᾌᾍᾎᾏᾘᾙᾚᾛᾜᾝᾞᾟᾨᾩᾪᾫᾬᾭᾮᾯᾸᾹᾺΆᾼῈΈῊΉῌῘῙῚΊῨῩῪΎῬῸΌῺΏῼກຂຄງຈຊຍດຕຖທນບປຜຝພຟມຢຣລວສຫອຮຯະາຳÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞĖĔĒĐĎČĊĈĆĄĂĀĘĚĜĞĠĢĤĦĨĪĬĮİĲĴĶĹŜŚŘŖŔŒŐŎŌŊŇŅŃŁĿĽĻŞŠŢŤŦŨŪŬŮŰŲŴŶŸŹŻŽƠƟƝƜƘƗƖƔƓƑƐƏƎƋƊƉƇƆƄƂƁƤƦƧƩƪƬƮƯƱƲƳƵƷƸƻƼƾƿǂǍǏǑǓǕǗǙǛǞǠǢǦǨȌȊȈȆȄȂȀǾǼǺǸǶǴǮǬǪȎȐȒȔȖȘȚȜȞȠȤȦȨȪȬȮɌɅɄɃɁȾȽȻȺȲȰḀḂḄḆḈḊḌḎḐḒḔḖḘḚḜḞḠṄṂṀḾḼḺḸḶḴḲḰḮḬḪḨḦḤḢṆṈṊṌṎṐṒṔṖṘṚṜṞṠṢṤṦẊẈẆẄẂẀṾṼṺṸṶṴṲṰṮṬṪṨẌẎẐẒẔẢẠẤẦẨẪẬỐỎỌỊỈỆỄỂỀẾẼẺẸẶẴẲẰẮỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲÅKỸỶỴⱤⱭⱮⱯⱰⱾⱿꜢꜤꜦꞐꞪꞍ]*$/)
						{
                                                        # Simplify modified aa before doing the workspace simplification
							if(simplify_aminoacids=="y" || work_in_simplified_aa_space=="y"){sssmax=split($2,sss,""); sequ=""; s=0; while(s<sssmax) {s=s+1; if(standard[sss[s]]!="" && standard[sss[s]]!="_") {sss[s]=standard[sss[s]]}; sequ=sequ""sss[s]}; $2=sequ}

							seq_=$2 ; if(seq_!="") {$64=peptide_solubility(seq_)}; if($64=="") {$64="_"};
							original_seq_=seq_
								if(work_in_simplified_aa_space=="y") {
									tt=split(seq_,seq_transl,"") ; translate_sequence_in_simplified_aa_space(translated_seq,"***",seq_transl) ;
									seq_translation=seq_; seq_=""; t=0; while(t<tt) {t=t+1; seq_=seq_""translated_seq[t]}}
							if(work_in_simplified_aa_space=="DSSP") {seq_=$52}
							len=length(seq_);
							population[int(len)]=population[int(len)]+1 ; if(len>max_len) {max_len=len}
							if(len>=lower && len<=upper && (solubility==peptide_solubility(original_seq_) || solubility=="i")) {
								d=d+1
									seq[d]=seq_ ;
								original_seq[d]=original_seq_
									prop[d]=$0
#p=0; while(p<length(prp_name)) {p=p+1
#prop[d]""sprintf("%1s ",$p)
#}
									print d":"seq[d]
									seq_names[d]=$1
									print d,"seq",seq[d] >> "OUTPUTS/"calculation_label"/selection"
									print d,"oseq",original_seq[d] >> "OUTPUTS/"calculation_label"/selection"
									print d,"prop_separator",prop[d] >> "OUTPUTS/"calculation_label"/selection"
									print d, "names",seq_names[d] >> "OUTPUTS/"calculation_label"/selection"
									print d > "OUTPUTS/"calculation_label"/.fam_num"
							}
						}
					}
	}
}
END{
	dmax=d
		print dmax" sequences found!"
		if(dmax=="0") {print "\033[1m *** You need to choose different criteria! \033[0m"}
		if(dmax=="0") {print "" > "OUTPUTS/"calculation_label"/.empty"}
		title("blue","_",80)
		if(calculate_only_global_properties!="y") {
			print "FAMILIES"
				title("blue","_",80)
		}
	print ""
		print "Forming families of sequences (choosing from "dmax" sequences):"
}
