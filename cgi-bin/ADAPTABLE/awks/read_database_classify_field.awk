function read_database_classify_field(field,separator,sequence,cyclic_,synthetic_,antimicrobial_,antibacterial_,\
antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,antiparasitic_,antiplasmodial_,\
antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,drug_delivery_,cell_penetrating_,\
tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,taxonomy_,antibiofilm_,\
f,fields) {
                # /pattern$/ to only replace Gram- at the end of the string
                if(field~/Gram-/) {gsub(/Gram-$/,"gram-neg",field)}
                if(field~/Gram\+/) {gsub("Gram+","gram-pos",field)}

					split(field,fields,separator)
                                        f=0; while(f<length(fields)) {f=f+1
								gsub("_","",fields[f])
                                                                field=tolower(fields[f])
									if(field~/synthetic/){
                                                                                synthetic_[sequence]="synthetic"
                                                                        }
									if(field~/cyclic/){
                                                                                cyclic_[sequence]="cyclic"
                                                                        }
									if(field~/plant/){
                                                                                plant_[sequence]="plant"
                                                                        }
                                                                        # prenyltransferases_inhibitors are for many (anticancer, virus...), the same for Histidine-rich amphipathic cationic peptides, cyanobactins, Natural Neutral ionophores, cyclopeptide alkaloids...
                                                                        if(field~/microb/ || field~/host_defense/ || field~/ell_penetrating/ || field~/prenyltransferase/ || field~/histidine-rich/ || field~/histidinerich/ || field~/defensin/ || field~/cyanobactin/ || field~/ionophor/ || field~/cyclopeptid/ || field~/mast_cell_degranulating/ ){
                                                                                antimicrobial_[sequence]="antimicrobial"
                                                                        }
                                                                        if(field~/bacter/ || field~/mollicut/ || field~/antibiotic/ || field~/mycoplasm/ || field~/MBC/ || field~/anti-TB/ ){
                                                                                antibacterial_[sequence]="antibacterial"
										antimicrobial_[sequence]="antimicrobial"
                                                                        }
                                                                        # Antiaspartic proteases are against fungi (Candida usually)
                                                                        if(field~/fung/ || field~/ntiaspartic_protease/ || field~/Candida/ || field~/candida/ || field~/Aspergillus/ || field~/antimycotic/ ){
                                                                                antifungal_[sequence]="antifungal"
										antimicrobial_[sequence]="antimicrobial"
                                                                        }
                                                                        if(field~/viral/ || field~/viru/ || field~/viric/ || field~/hiv/ || field~/herpes/ || field~/HCV/ || field~/vaccinia/ || field~/HIV/ ){
                                                                                antiviral_[sequence]="antiviral"
										antimicrobial_[sequence]="antimicrobial"
                                                                        }
                                                                        # Proteasome inhibitors are against cancer, also protein farnesyltransferase inhibitors
                                                                        if(field~/cancer/ || field~/antineoplastic/ || field~/leukem/ || field~/ovarian_cells/ || field~/sarcoma/ || field~/carcino/ || field~/blastoma/ || field~/roteasome_inhibitor/ || field~/farnesyltransferase/ || field~/leukosis/ || field~/lymphoma/ || field~/BGC-823/ || field~/MM96L/ || field~/RPMI-8226/ || field~/U-937/ || field~/CCRF-/ || field~/NCI-H69/ || field~/MDA-MB/ || field~/Jurkat/ || field~/K-562/ || field~/MCF-7/ || field~/Bcap-37/ || field~/GI50/ ){
                                                                                anticancer_[sequence]="anticancer"
                                                                        }
                                                                        if(field~/hypertens/){
                                                                                antihypertensive_[sequence]="antihypertensive"
										cell_cell_[sequence]="cell_cell"
                                                                        }
                                                                        # https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3594739/ for cysteine protease/proteinase inhibitors role, cyclolinopeptides...
									if(field~/mmun/ || field~/macrophag/ || field~/ifng/ || field~/nflamma/ || field~/mononuclear/ || field~/mast_cell/ || field~/ysteine_prote/ || field~/host_defense/ || field~/cyclolinopeptide/ || field~/degranulat/ || field~/elicit/ ){
                                                                                immunomodulant_[sequence]="immunomodulant"
                                                                                cell_cell_[sequence]="cell_cell"
                                                                        }
                                                                        if(field~/parasit/ || field~/eimeria/ || field~/helmintic/ || field~/helminthic/ || field~/cestode/ || field~/trematode/ ){
                                                                                antiparasitic_[sequence]="antiparasitic"
                                                                        }
                                                                        if(field~/protozo/ || field~/nemat/ || field~/toxoplasma/){
                                                                                antiparasitic_[sequence]="antiparasitic"
										antiprotozoal_[sequence]="antiprotozoal"
                                                                        }
                                                                        # Scorpion venom toxins are insecticidal
                                                                        if(field~/insect/ || field~/cricket/ || field~/worm/ || field~/edes_aegypti/ || field~/scorpion/ || field~/flesh_fly/ || field~/spider/ || field~/cockroach/ || field~/flies/ || field~/fly/ || field~/lepidopter/ || field~/efrapeptin/ ){
                                                                                insecticidal_[sequence]="insecticidal"
                                                                        }
                                                                        if(field~/toxic/){
                                                                                toxic_[sequence]="toxic"
                                                                        }
                                                                        if(field~/cell-cell/ || field~/chemotactic/ || field~/neuropeptide/){
                                                                                cell_cell_[sequence]="cell_cell"
                                                                        }
                                                                        if(field~/drug_delivery/){
                                                                                drug_delivery_[sequence]="drug_delivery"
                                                                        }
                                                                        # Thiopeptides (thiazolyl peptides) are against gram+, VRE=Vancomycin-resistant Enterococcus, MRSA=Methicillin-resistant Staphylococcus aureus...
                                                                        if(field~/gram-pos/ || field~/thiopeptide/ || field~/thiazolyl/ || field~/VRE/ || field~/MRSA/ || field~/MSSA/ || field~/VRSA/){
                                                                                antigram_pos_[sequence]="antigram_pos"
										antibacterial_[sequence]="antibacterial"
										antimicrobial_[sequence]="antimicrobial"
                                                                        }
                                                                        if(field~/gram-neg/){
                                                                                antigram_neg_[sequence]="antigram_neg"
										antibacterial_[sequence]="antibacterial"
										antimicrobial_[sequence]="antimicrobial"
                                                                        }
                                                                        if(field~/tumor/ || field~/tumour/){
                                                                                antitumor_[sequence]="antitumor"
										anticancer_[sequence]="anticancer"
                                                                        }
                                                                        if(field~/angiogen/){
                                                                                antiangiogenic_[sequence]="antiangiogenic"
										anticancer_[sequence]="anticancer"
                                                                        }
                                                                        if(field~/yeast/ || field~/Candida/){
                                                                                antiyeast_[sequence]="antiyeast"
										antimicrobial_[sequence]="antimicrobial"
                                                                        }
                                                                        # slime mold is amoeba in modern classification
                                                                        if(field~/protozoa/ || field~/protist/ || field~/slime_mold/ || field~/amoeb/){
                                                                                antiprotozoal_[sequence]="antiprotozoal"
                                                                                antiparasitic_[sequence]="antiparasitic"
                                                                        }
                                                                        # Alkynoic_lipopeptide is against malaria
                                                                        if(field~/plasmod/ || field~/malar/ || field~/alkynoic/){
                                                                                antiplasmodial_[sequence]="antiplasmodial"
										antiprotozoal_[sequence]="antiprotozoal"
										antiparasitic_[sequence]="antiparasitic"
                                                                        }
                                                                        if(field~/trypano/){
                                                                                antitrypanosomic_[sequence]="antitrypanosomic"
										antiprotozoal_[sequence]="antiprotozoal"
										antiparasitic_[sequence]="antiparasitic"
                                                                        }
                                                                        if(field~/leishman/){
                                                                                antileishmania_[sequence]="antileishmania"
										antiprotozoal_[sequence]="antiprotozoal"
										antiparasitic_[sequence]="antiparasitic"
                                                                        }
                                                                        if(field~/penetrating/){
                                                                                cell_penetrating_[sequence]="cell_penetrating"
										drug_delivery_[sequence]="drug_delivery"
                                                                        }
                                                                        if(field~/tumor_homing/){
                                                                                tumor_homing_[sequence]="tumor_homing"
										drug_delivery_[sequence]="drug_delivery"
                                                                        }
                                                                        if(field~/blood_brain/){
                                                                                blood_brain_[sequence]="blood_brain"
										drug_delivery_[sequence]="drug_delivery"
                                                                        }
                                                                        if(field~/hormone/ || field~/diabet/){
                                                                                hormone_[sequence]="hormone"
										cell_cell_[sequence]="cell_cell"
                                                                        }
                                                                        if(field~/quorum/){
                                                                                quorum_sensing_[sequence]="quorum_sensing"
										cell_cell_[sequence]="cell_cell"
                                                                        }
                                                                        if(field~/hemolyt/ || field~/erythrocy/ || field~/_blood/ || field~/RBC/ || field~/hd50/){
                                                                                hemolytic_[sequence]="hemolytic"
										toxic_[sequence]="toxic"
                                                                        }
                                                                        if(field~/cytotoxi/ || field~/neurotoxin/){
                                                                                cytotoxic_[sequence]="cytotoxic"
										toxic_[sequence]="toxic"
                                                                        }
                                                                        if(field~/antioxid/){
                                                                                antioxidant_[sequence]="antioxidant"
                                                                        }
                                                                        if(field~/antiprolif/){
                                                                                antiproliferative_[sequence]="antiproliferative"
                                                                        }
                                                                        if(field~/biofilm/){
                                                                                antibiofilm_[sequence]="antibiofilm"
                                                                        }

       							 }        
}
