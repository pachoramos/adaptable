@include "awks/library.awk"
BEGIN{

# Uncompress pdb2dssp DB one time to be more efficient at searching time
system("unxz DATABASES/PDB_DSSP/ss.txt.xz")

satpdb="n"
cancerPPD="n"
Hemolytik="n"
DRAMP="n"
HIPdb="n"
PhytoAMP="n"
Camp="n"
DBAASP="n"
APD="n"
InverPep="n"
ParaPep="n"
ANTISTAPHY="n"
DADP="n"
MilkAMPs="n"
BACTIBASE="n"
BAAMPS="n"
defensins="n"
conoserver="n"
AVP="n"
MAVP="n"
LAMP="n"
ADAM="n"
YADAMP="n"
Peptaibol="n"
CPP="n"
UniProt="n"

read_microbes(full_microbe_name,kingdom_microbe,gram,biofilm,genus_microbe)

if(satpdb=="y" || ARGV[1]=="satpdb" || ARGV[1]=="all") {
build_smax=read_satpdb_database_auto(ID_,seq_s,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_) 
}

if(cancerPPD=="y" || ARGV[1]=="cancerPPD" || ARGV[1]=="all") {
build_cmax=read_cancerPPD_database_auto(ID_,seq_c,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(Hemolytik=="y" || ARGV[1]=="Hemolytik" || ARGV[1]=="all") {
build_hmax=read_Hemolytik_database_auto(ID_,seq_h,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(DRAMP=="y" || ARGV[1]=="DRAMP" || ARGV[1]=="all") {
build_dmax=read_DRAMP_database_auto(ID_,seq_d,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(HIPdb=="y" || ARGV[1]=="HIPdb" || ARGV[1]=="all") {
build_vmax=read_HIPdb_database_auto(ID_,seq_v,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(PhytoAMP=="y" || ARGV[1]=="PhytoAMP" || ARGV[1]=="all") {
build_ppmax=read_PhytoAMP_database_auto(ID_,seq_pp,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(Camp=="y" || ARGV[1]=="Camp" || ARGV[1]=="all") {
build_mmax=read_Camp_database_auto(ID_,seq_m,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(DBAASP=="y" || ARGV[1]=="DBAASP" || ARGV[1]=="all") {
build_damax=read_DBAASP_database_auto(ID_,seq_da,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(APD=="y" || ARGV[1]=="APD" || ARGV[1]=="all") {
build_pmax=read_APD_database_auto(ID_,seq_p,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(InverPep=="y" || ARGV[1]=="InverPep" || ARGV[1]=="all") {
build_imax=read_InverPep_database_auto(ID_,seq_i,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(ParaPep=="y" || ARGV[1]=="ParaPep" || ARGV[1]=="all") {
build_rmax=read_ParaPep_database_auto(ID_,seq_r,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(ANTISTAPHY=="y" || ARGV[1]=="ANTISTAPHY" || ARGV[1]=="all") {
build_nmax=read_ANTISTAPHY_database_auto(ID_,seq_n,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(DADP=="y" || ARGV[1]=="DADP" || ARGV[1]=="all") {
build_zmax=read_DADP_database_auto(ID_,seq_z,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(MilkAMPs=="y" || ARGV[1]=="MilkAMPs" || ARGV[1]=="all") {
build_ymax=read_MilkAMPs_database_auto(ID_,seq_y,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(BACTIBASE=="y" || ARGV[1]=="BACTIBASE" || ARGV[1]=="all") {
build_tmax=read_BACTIBASE_database_auto(ID_,seq_t,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(BAAMPS=="y" || ARGV[1]=="BAAMPS" || ARGV[1]=="all") {
build_fmax=read_BAAMPS_database_auto(ID_,seq_f,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(defensins=="y" || ARGV[1]=="defensins" || ARGV[1]=="all") {
build_emax=read_defensins_database_auto(ID_,seq_e,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(conoserver=="y" || ARGV[1]=="conoserver" || ARGV[1]=="all") {
build_omax=read_conoserver_database_auto(ID_,seq_o,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(AVP=="y" || ARGV[1]=="AVP" || ARGV[1]=="all") {
build_xmax=read_AVP_database_auto(ID_,seq_x,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(LAMP=="y" || ARGV[1]=="LAMP" || ARGV[1]=="all") {
build_wmax=read_LAMP_database_auto(ID_,seq_w,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(ADAM=="y" || ARGV[1]=="ADAM" || ARGV[1]=="all") {
build_admax=read_ADAM_database_auto(ID_,seq_ad,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(YADAMP=="y" || ARGV[1]=="YADAMP" || ARGV[1]=="all") {
build_yamax=read_YADAMP_database_auto(ID_,seq_ya,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(Peptaibol=="y" || ARGV[1]=="Peptaibol" || ARGV[1]=="all") {
build_pbmax=read_Peptaibol_database_auto(ID_,seq_pb,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(CPP=="y" || ARGV[1]=="CPP" || ARGV[1]=="all") {
build_cpmax=read_CPP_database_auto(ID_,seq_cp,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(UniProt=="y" || ARGV[1]=="UniProt" || ARGV[1]=="all") {
build_upmax=read_UniProt_database_auto(ID_,seq_up,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

if(MAVP=="y" || ARGV[1]=="MAVP" || ARGV[1]=="all") {
build_mxmax=read_MAVP_database_auto(ID_,seq_mx,name_,source_,Family_,gene_,stereo_,N_terminus_,C_terminus_,PTM_,cyclic_,target_,\
synthetic_,antimicrobial_,antibacterial_,antigram_pos_,antigram_neg_,antifungal_,antiyeast_,antiviral_,antiprotozoal_,\
antiparasitic_,antiplasmodial_,antitrypanosomic_,antileishmania_,insecticidal_,anticancer_,antitumor_,cell_line_,\
tissue_,cancer_type_,anticancer_activity_,anticancer_activity_test_,antiangiogenic_,toxic_,cytotoxic_,hemolytic_,\
hemolytic_activity_,hemolytic_activity_test_,RBC_source_,cell_cell_,hormone_,quorum_sensing_,immunomodulant_,antihypertensive_,\
drug_delivery_,cell_penetrating_,tumor_homing_,blood_brain_,antioxidant_,antiproliferative_,DSSP_,\
pdb_,experim_structure_,PMID_,taxonomy_,all_organisms_,activity_viral_,activity_viral_test_,ribosomal_,experimental_,biofilm_)
}

max[1]=build_smax
max[2]=build_cmax
max[3]=build_hmax
max[4]=build_dmax
max[5]=build_vmax
max[6]=build_ppmax
max[7]=build_mmax
max[8]=build_damax
max[9]=build_pmax
max[10]=build_imax
max[11]=build_rmax
max[12]=build_nmax
max[13]=build_zmax
max[14]=build_ymax
max[15]=build_tmax
max[16]=build_fmax
max[17]=build_emax
max[18]=build_omax
max[19]=build_xmax
max[20]=build_wmax
max[21]=build_admax
max[22]=build_yamax
max[23]=build_pbmax
max[24]=build_cpmax
max[25]=build_upmax
max[26]=build_mxmax

tempo="tmp_files/build_database_auto-temp"

print "" > tempo
ll=0
mmm=0; while(mmm<length(max)) {mmm=mmm+1
l=0; while (l<max[mmm]) {l=l+1
		if((used[seq_s[l]]=="" && mmm==1) || (used[seq_c[l]]=="" && mmm==2) || (used[seq_h[l]]=="" && mmm==3) || (used[seq_d[l]]=="" && mmm==4)	|| \
		(used[seq_v[l]]=="" && mmm==5) || (used[seq_pp[l]]=="" && mmm==6) || (used[seq_m[l]]=="" && mmm==7) || (used[seq_da[l]]=="" && mmm==8) || \
		(used[seq_p[l]]=="" && mmm==9) || (used[seq_i[l]]=="" && mmm==10) || (used[seq_r[l]]=="" && mmm==11) || (used[seq_n[l]]=="" && mmm==12) || \
		 (used[seq_z[l]]=="" && mmm==13) || (used[seq_y[l]]=="" && mmm==14) || (used[seq_t[l]]=="" && mmm==15) || \
		 (used[seq_f[l]]=="" && mmm==16) || (used[seq_e[l]]=="" && mmm==17) || (used[seq_o[l]]=="" && mmm==18) || \
		 (used[seq_x[l]]=="" && mmm==19) || (used[seq_w[l]]=="" && mmm==20)  || (used[seq_ad[l]]=="" && mmm==21)  || (used[seq_ya[l]]=="" && mmm==22) || \
		 (used[seq_pb[l]]=="" && mmm==23) || (used[seq_cp[l]]=="" && mmm==24)  || (used[seq_up[l]]=="" && mmm==25) || (used[seq_mx[l]]=="" && mmm==26)) {
			ll=ll+1
			if (mmm==1) {seq_l[ll]=seq_s[l]};if (mmm==2) {seq_l[ll]=seq_c[l]};if (mmm==3) {seq_l[ll]=seq_h[l]};if (mmm==4) {seq_l[ll]=seq_d[l]};
			if (mmm==5) {seq_l[ll]=seq_v[l]};if (mmm==6) {seq_l[ll]=seq_pp[l]} ; if (mmm==7) {seq_l[ll]=seq_m[l]};if (mmm==8) {seq_l[ll]=seq_da[l]}
			if (mmm==9) {seq_l[ll]=seq_p[l]};if (mmm==10) {seq_l[ll]=seq_i[l]};if (mmm==11) {seq_l[ll]=seq_r[l]};
			if (mmm==12) {seq_l[ll]=seq_n[l]};if (mmm==13) {seq_l[ll]=seq_z[l]};if (mmm==14) {seq_l[ll]=seq_y[l]};
			if (mmm==15) {seq_l[ll]=seq_t[l]};if (mmm==16) {seq_l[ll]=seq_f[l]};if (mmm==17) {seq_l[ll]=seq_e[l]};
			if (mmm==18) {seq_l[ll]=seq_o[l]};if (mmm==19) {seq_l[ll]=seq_x[l]}; if (mmm==20) {seq_l[ll]=seq_w[l]};
			if (mmm==21) {seq_l[ll]=seq_ad[l]};if (mmm==22) {seq_l[ll]=seq_ya[l]}; if (mmm==23) {seq_l[ll]=seq_pb[l]};
			if (mmm==24) {seq_l[ll]=seq_cp[l]};if (mmm==25) {seq_l[ll]=seq_up[l]}; if (mmm==26) {seq_l[ll]=seq_mx[l]};
if(ID_[seq_l[ll]]=="") {ID_[seq_l[ll]]="_"};if(sequence_[seq_l[ll]]=="") {sequence_[seq_l[ll]]="_"}
if(name_[seq_l[ll]]=="") {name_[seq_l[ll]]="_"};if(source_[seq_l[ll]]=="") {source_[seq_l[ll]]="_"}
if(Family_[seq_l[ll]]=="") {Family_[seq_l[ll]]="_"};if(gene_[seq_l[ll]]=="") {gene_[seq_l[ll]]="_"}
if(stereo_[seq_l[ll]]=="") {stereo_[seq_l[ll]]="_"};if(N_terminus_[seq_l[ll]]=="") {N_terminus_[seq_l[ll]]="_"}
if(C_terminus_[seq_l[ll]]=="") {C_terminus_[seq_l[ll]]="_"};if(PTM_[seq_l[ll]]=="") {PTM_[seq_l[ll]]="_"}
if(cyclic_[seq_l[ll]]=="") {cyclic_[seq_l[ll]]="_"};if(target_[seq_l[ll]]=="") {target_[seq_l[ll]]="_"}
if(synthetic_[seq_l[ll]]=="") {synthetic_[seq_l[ll]]="_"};if(antimicrobial_[seq_l[ll]]=="") {antimicrobial_[seq_l[ll]]="_"}
if(antibacterial_[seq_l[ll]]=="") {antibacterial_[seq_l[ll]]="_"};if(antigram_pos_[seq_l[ll]]=="") {antigram_pos_[seq_l[ll]]="_"}
if(antigram_neg_[seq_l[ll]]=="") {antigram_neg_[seq_l[ll]]="_"};if(antifungal_[seq_l[ll]]=="") {antifungal_[seq_l[ll]]="_"}
if(antiyeast_[seq_l[ll]]=="") {antiyeast_[seq_l[ll]]="_"};if(antiviral_[seq_l[ll]]=="") {antiviral_[seq_l[ll]]="_"}
if(antiprotozoal_[seq_l[ll]]=="") {antiprotozoal_[seq_l[ll]]="_"};if(antiparasitic_[seq_l[ll]]=="") {antiparasitic_[seq_l[ll]]="_"}
if(antiplasmodial_[seq_l[ll]]=="") {antiplasmodial_[seq_l[ll]]="_"};if(antitrypanosomic_[seq_l[ll]]=="") {antitrypanosomic_[seq_l[ll]]="_"}
if(antileishmania_[seq_l[ll]]=="") {antileishmania_[seq_l[ll]]="_"};if(insecticidal_[seq_l[ll]]=="") {insecticidal_[seq_l[ll]]="_"}
if(anticancer_[seq_l[ll]]=="") {anticancer_[seq_l[ll]]="_"};if(antitumor_[seq_l[ll]]=="") {antitumor_[seq_l[ll]]="_"}
if(cell_line_[seq_l[ll]]=="") {cell_line_[seq_l[ll]]="_"};if(tissue_[seq_l[ll]]=="") {tissue_[seq_l[ll]]="_"}
if(cancer_type_[seq_l[ll]]=="") {cancer_type_[seq_l[ll]]="_"};if(anticancer_activity_[seq_l[ll]]=="") {anticancer_activity_[seq_l[ll]]="_"}
if(anticancer_activity_test_[seq_l[ll]]=="") {anticancer_activity_test_[seq_l[ll]]="_"};if(antiangiogenic_[seq_l[ll]]=="") {antiangiogenic_[seq_l[ll]]="_"}
if(toxic_[seq_l[ll]]=="") {toxic_[seq_l[ll]]="_"};if(cytotoxic_[seq_l[ll]]=="") {cytotoxic_[seq_l[ll]]="_"}
if(hemolytic_[seq_l[ll]]=="") {hemolytic_[seq_l[ll]]="_"};if(hemolytic_activity_[seq_l[ll]]=="") {hemolytic_activity_[seq_l[ll]]="_"}
if(hemolytic_activity_test_[seq_l[ll]]=="") {hemolytic_activity_test_[seq_l[ll]]="_"};if(RBC_source_[seq_l[ll]]=="") {RBC_source_[seq_l[ll]]="_"}
if(cell_cell_[seq_l[ll]]=="") {cell_cell_[seq_l[ll]]="_"};if(hormone_[seq_l[ll]]=="") {hormone_[seq_l[ll]]="_"}
if(quorum_sensing_[seq_l[ll]]=="") {quorum_sensing_[seq_l[ll]]="_"};if(immunomodulant_[seq_l[ll]]=="") {immunomodulant_[seq_l[ll]]="_"}
if(antihypertensive_[seq_l[ll]]=="") {antihypertensive_[seq_l[ll]]="_"};if(drug_delivery_[seq_l[ll]]=="") {drug_delivery_[seq_l[ll]]="_"}
if(cell_penetrating_[seq_l[ll]]=="") {cell_penetrating_[seq_l[ll]]="_"};if(tumor_homing_[seq_l[ll]]=="") {tumor_homing_[seq_l[ll]]="_"}
if(blood_brain_[seq_l[ll]]=="") {blood_brain_[seq_l[ll]]="_"};if(antioxidant_[seq_l[ll]]=="") {antioxidant_[seq_l[ll]]="_"}
if(antiproliferative_[seq_l[ll]]=="") {antiproliferative_[seq_l[ll]]="_"};if(DSSP_[seq_l[ll]]=="") {DSSP_[seq_l[ll]]="_"}
if(pdb_[seq_l[ll]]=="") {pdb_[seq_l[ll]]="_"};if(experim_structure_[seq_l[ll]]=="") {experim_structure_[seq_l[ll]]="_"}
if(PMID_[seq_l[ll]]=="") {PMID_[seq_l[ll]]="_"};if(taxonomy_[seq_l[ll]]=="") {taxonomy_[seq_l[ll]]="_"}
if(all_organisms_[seq_l[ll]]=="") {all_organisms_[seq_l[ll]]="_"}
if(activity_viral_[seq_l[ll]]=="") {activity_viral_[seq_l[ll]]="_"}
if(activity_viral_test_[seq_l[ll]]=="") {activity_viral_test_[seq_l[ll]]="_"}
if(ribosomal_[seq_l[ll]]=="") {ribosomal_[seq_l[ll]]="_"}
if(experimental_[seq_l[ll]]=="") {experimental_[seq_l[ll]]="_"}
if(biofilm_[seq_l[ll]]=="") {biofilm_[seq_l[ll]]="_"}
if(antibacterial_[seq_l[ll]]=="antibacterial") {antimicrobial_[seq_l[ll]]="antimicrobial"}
if(antigram_pos_[seq_l[ll]]=="antigram_pos" || antigram_neg_[seq_l[ll]]=="antigram_neg") {antibacterial_[seq_l[ll]]="antibacterial";antimicrobial_[seq_l[ll]]="antimicrobial"}
if(antiyeast_[seq_l[ll]]=="antiyeast") {antimicrobial_[seq_l[ll]]="antimicrobial";antifungal_[seq_l[ll]]="antifungal"}
if(antifungal_[seq_l[ll]]=="antifungal") {antimicrobial_[seq_l[ll]]="antimicrobial"}
if(antiviral_[seq_l[ll]]=="antiviral") {antimicrobial_[seq_l[ll]]="antimicrobial"}
if(antitrypanosomic_[seq_l[ll]]=="antitrypanosomic" || antiplasmodial_[seq_l[ll]]=="antiplasmodial" || antileishmania_[seq_l[ll]]=="antileishmania") {antiparasitic_[seq_l[ll]]="antiparasitic"}
if(antiparasitic_[seq_l[ll]]=="antiparasitic") {antiprotozoal_[seq_l[ll]]="antiprotozoal"}
if(antiprotozoal_[seq_l[ll]]=="antiprotozoal") {antimicrobial_[seq_l[ll]]="antimicrobial"}
if(antitumor_[seq_l[ll]]=="antitumor" || antiangiogenic_[seq_l[ll]]=="antiangiogenic" || tumor_homing_[seq_l[ll]]=="tumor_homing") {anticancer_[seq_l[ll]]="anticancer"}
if(cytotoxic_[seq_l[ll]]=="cytotoxic" || hemolytic_[seq_l[ll]]=="hemolytic") {toxic_[seq_l[ll]]="toxic"}
if(hormone_[seq_l[ll]]=="hormone" || quorum_sensing_[seq_l[ll]]=="quorum_sensing" || immunomodulant_[seq_l[ll]]=="immunomodulant" || antihypertensive_[seq_l[ll]]=="antihypertensive") {cell_cell_[seq_l[ll]]="cell_cell"}
if(cell_penetrating_[seq_l[ll]]=="cell_penetrating" || tumor_homing_[seq_l[ll]]=="tumor_homing" || blood_brain_[seq_l[ll]]=="blood_brain") {drug_delivery_[seq_l[ll]]="drug_delivery"}
#seque=substr(seq_l[ll],1,length(seq_l))
# Assign missing PDBs and DSSPs, we prefer DSSPs from PDB as they are regularly updated
#if(seq_l[ll]!="" && DSSP_[seq_l[ll]]=="_") {
if(seq_l[ll]!="") {
	fullbar=""
	cmd="DATABASES/PDB_DSSP/seq2dssp.sh "seq_l[ll]""
	cmd | getline fullbar
	close(cmd)
	if(fullbar!="") {
		split(fullbar,pdbdssp,":")
		if(pdb_[seq_l[ll]]!~pdbdssp[1]) {
			if(pdb_[seq_l[ll]]!="_") {
				pdb_[seq_l[ll]]=pdb_[seq_l[ll]]""pdbdssp[1]";"
			} else {
				pdb_[seq_l[ll]]=pdbdssp[1]";"
			}
		}
                if(experim_structure_[seq_l[ll]]!~pdbdssp[1]) {
                        if(experim_structure_[seq_l[ll]]!="_") {
				experim_structure_[seq_l[ll]]=experim_structure_[seq_l[ll]]""pdbdssp[1]";"
                        } else {
                                experim_structure_[seq_l[ll]]=pdbdssp[1]";"
                        }
                }
		DSSP_[seq_l[ll]]=pdbdssp[2]
		# If they are in PDB they are experimental
		# FIXME: depends on our definition...
		#experimental_[seq_l[ll]]="experimental"
	}
}
# Assign missing PDBs and DSSPs using predictions from PSSpred
if(seq_l[ll]!="" && DSSP_[seq_l[ll]]=="_") {
        fullbar=""
        cmd="DATABASES/PSSpred_DB/seq2dssp.sh "seq_l[ll]""
        cmd | getline fullbar
        close(cmd)
        if(fullbar!="") {
                DSSP_[seq_l[ll]]=fullbar
        }
}
print  ">"ID_[seq_l[ll]],seq_l[ll],name_[seq_l[ll]],source_[seq_l[ll]],Family_[seq_l[ll]],gene_[seq_l[ll]],stereo_[seq_l[ll]],N_terminus_[seq_l[ll]],C_terminus_[seq_l[ll]],PTM_[seq_l[ll]],cyclic_[seq_l[ll]],target_[seq_l[ll]],\
synthetic_[seq_l[ll]],antimicrobial_[seq_l[ll]],antibacterial_[seq_l[ll]],antigram_pos_[seq_l[ll]],antigram_neg_[seq_l[ll]],antifungal_[seq_l[ll]],antiyeast_[seq_l[ll]],antiviral_[seq_l[ll]],antiprotozoal_[seq_l[ll]],\
antiparasitic_[seq_l[ll]],antiplasmodial_[seq_l[ll]],antitrypanosomic_[seq_l[ll]],antileishmania_[seq_l[ll]],insecticidal_[seq_l[ll]],anticancer_[seq_l[ll]],antitumor_[seq_l[ll]],cell_line_[seq_l[ll]],\
tissue_[seq_l[ll]],cancer_type_[seq_l[ll]],anticancer_activity_[seq_l[ll]],anticancer_activity_test_[seq_l[ll]],antiangiogenic_[seq_l[ll]],toxic_[seq_l[ll]],cytotoxic_[seq_l[ll]],hemolytic_[seq_l[ll]],\
hemolytic_activity_[seq_l[ll]],hemolytic_activity_test_[seq_l[ll]],RBC_source_[seq_l[ll]],cell_cell_[seq_l[ll]],hormone_[seq_l[ll]],quorum_sensing_[seq_l[ll]],immunomodulant_[seq_l[ll]],antihypertensive_[seq_l[ll]],\
drug_delivery_[seq_l[ll]],cell_penetrating_[seq_l[ll]],tumor_homing_[seq_l[ll]],blood_brain_[seq_l[ll]],antioxidant_[seq_l[ll]],antiproliferative_[seq_l[ll]],DSSP_[seq_l[ll]],\
pdb_[seq_l[ll]],experim_structure_[seq_l[ll]],PMID_[seq_l[ll]],taxonomy_[seq_l[ll]],all_organisms_[seq_l[ll]],activity_viral_[seq_l[ll]],activity_viral_test_[seq_l[ll]],ribosomal_[seq_l[ll]],experimental_[seq_l[ll]],biofilm_[seq_l[ll]]
print "" >> tempo
print seq_l[ll],"**********************************************************************************************" >> tempo
printf("%50s %50s \n","seq_l=",seq_l[ll]) >> tempo
printf("%50s %50s \n","ID_=",ID_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","seq_l=",seq_l[ll]) >> tempo
printf("%50s %50s \n","name_=",name_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","source_=",source_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","Family_=",Family_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","gene_=",gene_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","stereo_=",stereo_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","N_terminus_=",N_terminus_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","C_terminus_=",C_terminus_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","PTM_=",PTM_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","cyclic_=",cyclic_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","target_=",target_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","synthetic_=",synthetic_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antimicrobial_=",antimicrobial_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antibacterial_=",antibacterial_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antigram_pos_=",antigram_pos_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antigram_neg_=",antigram_neg_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antifungal_=",antifungal_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antiyeast_=",antiyeast_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antiviral_=",antiviral_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antiprotozoal_=",antiprotozoal_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antiparasitic_=",antiparasitic_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antiplasmodial_=",antiplasmodial_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antitrypanosomic_=",antitrypanosomic_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antileishmania_=",antileishmania_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","insecticidal_=",insecticidal_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","anticancer_=",anticancer_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antitumor_=",antitumor_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","cell_line_=",cell_line_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","tissue_=",tissue_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","cancer_type_=",cancer_type_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","anticancer_activity_=",anticancer_activity_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","anticancer_activity_test_=",anticancer_activity_test_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antiangiogenic_=",antiangiogenic_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","toxic_=",toxic_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","cytotoxic_=",cytotoxic_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","hemolytic_=",hemolytic_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","hemolytic_activity_=",hemolytic_activity_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","hemolytic_activity_test_=",hemolytic_activity_test_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","RBC_source_=",RBC_source_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","cell_cell_=",cell_cell_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","hormone_=",hormone_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","quorum_sensing_=",quorum_sensing_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","immunomodulant_=",immunomodulant_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antihypertensive_=",antihypertensive_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","drug_delivery_=",drug_delivery_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","cell_penetrating_=",cell_penetrating_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","tumor_homing_=",tumor_homing_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","blood_brain_=",blood_brain_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antioxidant_=",antioxidant_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","antiproliferative_=",antiproliferative_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","DSSP_=",DSSP_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","pdb_=",pdb_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","experim_structure_=",experim_structure_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","PMID_=",PMID_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","taxonomy_=",taxonomy_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","all_organisms_=",all_organisms_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","activity_viral_=",activity_viral_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","activity_viral_test_=",activity_viral_test_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","ribosomal_=",ribosomal_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","experimental_=",experimental_[seq_l[ll]]) >> tempo
printf("%50s %50s \n","biofilm_=",biofilm_[seq_l[ll]]) >> tempo
				used[seq_l[ll]]="used"
		}
	}
}
system("rm -f "tempo"")
system("xz -T0 -9 DATABASES/PDB_DSSP/ss.txt")
}
