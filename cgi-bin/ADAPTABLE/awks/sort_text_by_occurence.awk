# given an array of strings it prints the most repeated words in order of occurence, after eliminating spaces and special character
# save_symbol can retain some symbols as part of the word. Multiple symbols can be saved e.g. sort_text_by_occurence(strings,"-)" saves - and )
# percental_tolerance limit the output to words that occur more than this limit. length_tolerance limit the output to words longer than this limit
function sort_text_by_occurence(strings,save_symbol,percental_tolerance,length_tolerance,average_on_lines,chars,chars_init,col,bg,style,\
		texts,s,ss,string,t,tt,name_w,label_w,w,wmax,new_text,line,factor1,factor2,empty) {
delete count_text
delete name_w
delete label_w

ss=length(strings)
delete count_per_line
delete count_per_line_
	s=0; while(s<ss) {s=s+1
		string=strings[s]
		if(string=="") {empty=empty+1}
		string=tolower(string)
			if(save_symbol!~/\(/) {gsub("\\("," ",string)}
			if(save_symbol!~/\)/) {gsub("\\)"," ",string)}
			if(save_symbol!~/,/) {gsub(","," ",string)}
			if(save_symbol!~/;/) {gsub(";"," ",string)}
			if(save_symbol!~/-/) {gsub("-"," ",string)}
			if(save_symbol!~/_/) {gsub("_"," ",string)}
			if(save_symbol!~/\{/) {gsub("{"," ",string)}
			if(save_symbol!~/\}/) {gsub("}"," ",string)}
			if(save_symbol!~/./) {gsub("\\."," ",string)}
			if(save_symbol!~/!/) {gsub("!"," ",string)}
			if(save_symbol!~/?/) {gsub("?"," ",string)}
			if(save_symbol!~/:/) {gsub(":"," ",string)}
			if(save_symbol!~/\//) {gsub("/"," ",string)}
#			if(save_symbol!~/\\/) {gsub("\\"," ",string)}
			if(save_symbol!~/'/) {gsub("'"," ",string)}
			if(save_symbol!~/&/) {gsub("&"," ",string)}
			if(save_symbol!~/=/) {gsub("="," ",string)}
			if(save_symbol!~/+/) {gsub("+"," ",string)}
			if(save_symbol!~/%/) {gsub("%"," ",string)}
                        if(save_symbol!~/\]/) {gsub("\\]"," ",string)}
#			if(save_symbol!~/\[/) {gsub("\\["," ",string)}

			gsub("  "," ",string)
			gsub("  "," ",string)
			tt=split(string,texts," ")
			t=0; while(t<tt) {t=t+1
			# Exclude patterns that we don't want
			if(texts[t]!~/^the$|^and$|^peptide$|^belongs$|^not$|^also$|^one$|^two$|^three$|^four$|^five$|^six$|^seven$|^eighth$|^nine$|^pubmed$|^http$|^www$|^ncbi$|^gov$|^entrez$|^cancer$|^tumor|^activity$|^against|^available$|^cellline$|^>my$|^family$|^subfamily$|^μm$|^contains$|^analogs$|^peptides$|^its$|^their$|^from$/) {
				if(new_text[texts[t]]=="") {w=w+1;name_w[w]=texts[t]; label_w[texts[t]]=w; new_text[texts[t]]=1}
				count_text[label_w[texts[t]]]=count_text[label_w[texts[t]]]+1
				if(count_per_line[texts[t],s]=="") {count_per_line_[label_w[texts[t]]]=count_per_line_[label_w[texts[t]]]+1;count_per_line[texts[t],s]=1}
			}
			}
	}
	wmax=w
		delete conv_index
		sort_nD(count_text,conv_index,"@>","no","v","","","","","",1,vmax)
		w=0; while(w<wmax) {w=w+1
		if(average_on_lines=="average_on_lines") {factor1=count_per_line_[conv_index[w]]*100/ss;factor2=count_per_line_[conv_index[w]]*100/(ss-empty)} 
		else {factor1=count_text[conv_index[w]]*100/wmax;}
		if((factor1>=percental_tolerance || factor2>=percental_tolerance) && length(name_w[conv_index[w]])>=length_tolerance) {
		line=line""sprintf("%1s (%4.1f-%4.1f %); ", name_w[conv_index[w]],factor1,factor2)
			}
		}
		print_newline(line,chars,chars_init,col,bg,style); print "" 
}

