#!/bin/bash
# Main difference with build-selection-fathers.sh is that this skips the duplicated fathers, hence, here there is only one father per family
export calculation_label="$(head -n1 OUTPUTS/.out_parallel)"
cd OUTPUTS/"${calculation_label}"
for i in $(grep "The best alignment is found with" ${calculation_label}-output|awk '{print$12}'); do grep -a -E " seq ${i}$" -A3 selection; done| cut -d" " -f2- |awk -v x=4 '{print (NR%x?c+1:++c),$0}' > .selection-fathers-sorted
sed -e '1,/Sorting families.../d' -e '/DONE!/,$d' "${calculation_label}"-output > .family_elements
cd ../../
