#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from jinja2 import Environment, FileSystemLoader
import sys
import os
from collections import OrderedDict
from typing import List
import htmlmin
from itertools import groupby
# For debugging
#from pprint import pprint

error_msg = ""

class Entry(object):
    def __init__(self, family: 'Family', id: int):
        self.family = family
        self.id = id

        self.aligned_sequence = ""
        self.sequence = ""

        self.is_father = False
        self.similarity = 0.

        self.frequencies = []
        self.restypes = []
        self.polarities = []

        self.ss_predicted = []
        self.ss_dssp = []

    def __str__(self):
        if self.is_father:
            similarity = "Family parent"
        else:
            similarity = "{:.1f}% similarity".format(self.similarity)

        return "{}: aligned sequence='{}' ({})".format(self.sequence, self.aligned_sequence, similarity)

    def __getitem__(self, item):
        return self.family.properties[item][self.id]

    def __len__(self):
        return len(self.sequence)

    def update_sequence(self, aligned_sequence):
        self.aligned_sequence = aligned_sequence
        self.sequence = aligned_sequence.strip("-")

    def frequency_items(self):
        return zip(self.aligned_sequence, self.frequencies)

    def restype_items(self):
        return zip(self.aligned_sequence, self.restypes)

    def polarity_items(self):
        return zip(self.aligned_sequence, self.polarities)

    def get_property_values(self):
        values = [prop[self.id] for prop in self.family.properties]
        return values


class Property(object):
    def __init__(self, raw_name: str, name: str = None, description: str = None, values: List[List] = list()):
        self.raw_name = raw_name
        if name is None:
            self.name = raw_name.replace("_", " ")
        else:
            self.name = name

        self.description = description

        self.values = values

    def format_values_for_member(self, member_id: int, output_format: str= "html", header: bool=False):
        value = self.values[member_id]

        output = []

        if len(value) == 0:
            output.append("No data")

        else:
            raw_name = self.raw_name.lower()
            if raw_name in ["sequence", "id", "dssp"]:
                output.extend(value)
            else:
                if raw_name in ["biofilm"]:
                    biofilmval= list(val.replace('biofilm', '') for val in value)
                    biofilmval2 = filter(None, biofilmval)
                    output.extend(list(val.capitalize().replace("_", " ") for val in biofilmval2))
                if raw_name in ["all_organisms"] or raw_name in ["cancer_type"] or raw_name in ["RBC_source"] or raw_name in ["virus_name"] or raw_name in ["cancer_type"] or raw_name in ["anticancer_activity_test"] or raw_name in ["activity_viral_test"] or raw_name in ["hemolytic_activity_test"] or raw_name in ["activity_test"]:
                    # We want to respect capitalization for units and organisms as they are already fixed
                    output.extend(list(val.replace("_", " ") for val in value))
                else:
                    output.extend(list(val.capitalize().replace("_", " ") for val in value))
                    # All (MIC=xxx) are skipped to make pie charts more readable and useful
                    # FIXME: We cannot do it here or we lose the "Values" text, do it later only for "Statistics"
#                    valreduced = list(re.sub('\(.*','',val) for val in value)
#                    output.extend(list(val.capitalize().replace("_", " ") for val in valreduced))

        return output

    def get_statistics(self, is_id):
#        pprint(vars(self))
        raw_name = self.raw_name.lower()

        flat_values = []

        values = list(self.format_values_for_member(i) for i in range(len(self.values)))

        for value in values:
            flat_values.extend(value)

        stats = {}
        for val in flat_values:
            # We drop activities here for getting meaningful pie charts
            if raw_name in ["all_organisms"] or raw_name in ["cancer_type"] or raw_name in ["RBC_source"] or raw_name in ["virus_name"] or raw_name in ["cancer_type"]:
                 val = re.sub('\(.*','',val)
            if is_id:
                val = re.sub('satpdb.*','satpdb',val)
                val = re.sub('DRAMP.*','DRAMP',val)
                val = re.sub('PHYT.*','PHYT',val)
                val = re.sub('CAMPSQ.*','CAMPSQ',val)
                val = re.sub('HIP.*','HIP',val)
                val = re.sub('hemo.*','hemo',val)
                val = re.sub('canPPD.*','canPPD',val)
                val = re.sub('DBAASP.*','DBAASP',val)
                val = re.sub('APD_.*','APD_AP',val)
                val = re.sub('InverPep.*','InverPep',val)
                val = re.sub('ParaPep.*','ParaPep',val)
                val = re.sub('ANTISTAPHY.*','ANTISTAPHY',val)
                val = re.sub('DADP_.*','DADP_',val)
                val = re.sub('MilkAMP.*','MilkAMP',val)
                val = re.sub('BACTIBASE_.*','BACTIBASE_',val)
                val = re.sub('BAAMPS.*','BAAMPS',val)
                val = re.sub('Defe.*','Defe',val)
                val = re.sub('Conoserver.*','Conoserver',val)
                val = re.sub('AVP.*','AVP',val)
                val = re.sub('MAVP.*','MAVP',val)
                val = re.sub('LAMP.*','LAMP',val)
                val = re.sub('ADAM.*','ADAM',val)
                val = re.sub('YADAMP.*','YADAMP',val)
                val = re.sub('Peptaibol.*','Peptaibol',val)
                val = re.sub('CPP.*','CPP',val)
                val = re.sub('uniprot.*','uniprot',val)
            try:
                stats[val] += 1
            except KeyError:
                stats[val] = 1

        return list(stats.keys()), list(stats.values())

    def __getitem__(self, item):
        return self.values[item]

    def __setitem__(self, key, value):
        self.values[key] = value

    def number_of_actual_values(self):
        n = 0
        for value in self.values:
            if len(value) > 0:
                n += 1
        return n

    def is_value_empty(self, item: int):
        return len(self.values[item]) == 0

    def is_empty(self):
        return self.number_of_actual_values() == 0


class Family(object):
    def __init__(self, family_id: int, nfamilies: int, size: int, best_alignement: str):
        self.family_id = family_id
        self.nfamilies = nfamilies

        self.members = []  # type: List[Entry]
        for i in range(size):
            self.members.append(Entry(self, i))

        self.best_alignment = best_alignement

        self.frequency_colorscale = []
        self.restype_colorscale = []
        self.polarity_colorscale = []

        self.summary_per_position = []

        self.best_representing_sequence = []
        self.best_representing_ids = []
        self.ss_prediction = []
        self.dssp_prediction = []

        self.properties = OrderedDict()  # type: OrderedDict[str, Property]

    @property
    def size(self):
        return len(self.members)

    @property
    def has_empty_properties(self):
        for prop in self.properties.values():
            if prop.is_empty():
                return True
        return False

    def __getitem__(self, item):
        return self.members[item]

    def __str__(self):
        return "Family #{} (out of {}) with {} members".format(self.family_id, self.nfamilies, self.size)

    def get_aminoacid_probability(self):
        aminoacids = []

        for i, father_aa in enumerate(self.members[0].aligned_sequence):
            if father_aa != "-":
                aminoacids.append(OrderedDict())

                aminoacids[-1][father_aa] = 1

                for member in self.members[1:]:
                    aa = member.aligned_sequence[i]

                    try:
                        aminoacids[-1][aa] += 1
                    except KeyError:
                        aminoacids[-1][aa] = 1

                for key, val in aminoacids[-1].items():
                    aminoacids[-1][key] = val / self.size

        return aminoacids


def is_property_value_header(line: str, family: Family) -> bool:
    if family is None:
        return False

    if len(family.properties) == 0:
        return False

    potential_property_name = get_property_header(line)

    return potential_property_name in family.properties.keys()


def get_property_header(line: str):
    return line.split(":")[0].replace("\x1b[1;1;38;5;20m", "").strip()


def extract_ss_elem_from_line(line: str, aligned_sequence: str) -> List:
    elems = []

    line = line.strip()

    if line[0].isdigit():
        line = line[line.find(".")+2:line.rfind(";")]
    else:
        line = line[:line.rfind(";")]

    for chunk in line.split('\x1b'):
        val = chunk.split(";")[-1]

        if val.startswith("[0m"):
            val = val[3:]

        if len(val) != 0:
            pos = val.find("m")

            if pos > 0:
                color = val[:pos].strip()
                if color in ["34", "27"]:
                    elem_class = "sss"
                elif color == "20":
                    elem_class = "ssb"
                elif color in ["31", "9"]:
                    elem_class = "ssa"
                elif color == "202":
                    elem_class = "ss3"
                elif color == "214":
                    elem_class = "ssp"
                elif color in ["32", "22"]:
                    elem_class = "sst"
                elif color == "28":
                    elem_class = "sse"
                else:
                    elem_class = None
            else:
                color  = "unspecified"
                elem_class = None

            for character in val[pos+1:]:
                if character == "-":
                    if aligned_sequence[len(elems)] == "-":
                        elem_icon = "v"
                        elem_class = "ssv"
                    else:
                        elem_icon = "c"
                        elem_class = "ssc"
                elif character in ["◖", "■", "◗"]:
                    elem_icon = "h"
                elif character in ["▬", "▶"]:
                    elem_icon = "b"
                elif character in ["◠"]:
                    elem_icon = "t"
                else:
                    elem_icon = None

                if elem_class is None:
                    raise ValueError("ERROR: Unknown secondary combination: color={}({}) "
                                     "and character={}({}) -> Debugging info= elem_class={},"
                                     "elem_icon={}\n"
                                     "line:{}".format(color,
                                                           color.encode("utf-8"),
                                                           character,
                                                           character.encode("utf-8"),
                                                           elem_class,
                                                           elem_icon,line),
                                     )

                elems.append((elem_class, elem_icon))

    curated_elems = []

    for key, group in groupby(elems):
        nelem = sum(1 for _ in group)

        elem_class, elem_icon = key

        for counter in range(nelem):
            if elem_icon in ["c", "v"]:
                curated_elems.append((elem_class, elem_icon))
            else:
                if nelem == 1:
                    curated_elems.append((elem_class, "{}o".format(elem_icon)))
                else:
                    if counter == 0:
                        curated_elems.append((elem_class, "{}s".format(elem_icon)))
                    elif counter == nelem - 1:
                        curated_elems.append((elem_class, "{}e".format(elem_icon)))
                    else:
                        curated_elems.append((elem_class, "{}m".format(elem_icon)))

    return curated_elems


def extract_member_data_from_section(fp, family):
    nmembers = family.size

    current_member_id = 0

    member_data = []

    current_seek_pos = fp.tell()

    while current_member_id < nmembers:
        seek_pos = fp.tell()

        line = fp.readline().rstrip()
        tag = "{}.".format(current_member_id + 1)
        pos = line.find(tag)

        if pos > 0:  # member data begin
            member_data.append(line[pos+len(tag)+1:].strip())
            current_member_id += 1
        elif len(line) == 0:  # empty line, just ignored
            continue
        elif line[0] == " ":  # data continuation
            line = line.strip()
            try:
                member_data[-1] += line.strip()
            except IndexError:
                # if we are here, something is badly shaped in the file and we are probably not inside member data
                # or there is no data
                # we put everything back as it was prior the function call and reutnr an empty list
                fp.seek(current_seek_pos)
                return []
        else:  # no more member data available
            # We put the reader pointer bakc at the beginning of the line so it can be read/processed again
            fp.seek(seek_pos)
            break

    return member_data


def read_property_values(fp, family):
    nmembers = family.size

    member_properties = []
    raw_values = []

    current_member_id = 0

    while current_member_id < nmembers:
        seek_pos = fp.tell()

        line = fp.readline().rstrip()
        tag = "{}.".format(current_member_id + 1)
        pos = line.find(tag)

        if pos > 0:
            raw_values.append(line[pos + len(tag) + 1:].strip())
            current_member_id += 1
        elif len(line) == 0:  # empty line, just ignored
            continue
        elif line == "___":
            break
        else:
            if len(raw_values) == 0:  # Still not inside values list
                continue
            else:
                raw_values[-1] += line.strip()

    for raw_value in raw_values:
        raw_value = ansi_escape.sub("", raw_value).strip(";")
        member_properties.append(list(value.strip("_>") for value in raw_value.split(";") if value != ""))

    return member_properties


def create_dblink(value: str, additional_classes:list=[]) -> str:
    if value.startswith("satpdb"):
        db_url = "http://crdd.osdd.net/raghava/satpdb/display_seq.php?details={}".format(value)
    elif value.startswith("DRAMP"):
        db_url = "http://dramp.cpu-bioinfor.org/browse/All_Information.php?id={}".format(value)
    elif value.startswith("PHYT"):
        db_url = "http://phytamp.hammamilab.org/{}".format(value)
    elif value.startswith("CAMPSQ"):
        db_url = "http://www.camp.bicnirrh.res.in/seqDisp.php?id={}".format(value)
    elif value.startswith("HIP"):
        db_url = "http://crdd.osdd.net/servers/hipdb/record.php?details={}".format(value)
    elif value.startswith("hemo"):
        db_url = "http://crdd.osdd.net/raghava/hemolytik/display.php?details={}' target=".format(value[4:])
    elif value.startswith("canPPD"):
        db_url = "http://crdd.osdd.net/raghava/cancerppd/display_sub.php?details={}".format(value[6:])
    elif value.startswith("DBAASP"):
        db_url = "https://dbaasp.org/peptide-card?id={}".format(value[6:])
    elif value.startswith("APD_"):
        db_url = "http://aps.unmc.edu/AP/database/query_output.php?ID={}".format(value[len("APD_AP"):])
    elif value.startswith("InverPep"):
        db_url = "http://ciencias.medellin.unal.edu.co/gruposdeinvestigacion/prospeccionydisenobiomoleculas/InverPep/" \
                 "public/Peptido/see/{}'".format(value[8:])
    elif value.startswith("ParaPep"):
        db_url = "http://crdd.osdd.net/raghava/parapep/display_sub.php?details={}".format(value[7:])
    elif value.startswith("ANTISTAPHY"):
        return value
    elif value.startswith("DADP_"):
        db_url = "http://split4.pmfst.hr/dadp/?a=kartica&id=SP_{}".format(value[len("DADP_"):])
    elif value.startswith("MilkAMP"):
        db_url = "http://milkampdb.org/{}".format(value[len("MilkAMP"):])
    elif value.startswith("BACTIBASE_"):
        db_url = "http://bactibase.hammamilab.org/{}".format(value[len("BACTIBASE_"):])
    elif value.startswith("BAAMPS"):
        db_url = "http://www.baamps.it/peptidelist?task=peptide.display&ID={}".format(value[len("BAAMPS"):])
    elif value.startswith("Defe"):
        db_url = "http://defensins.bii.a-star.edu.sg/pops/pop_proteinDetails.php?id={}".format(value[len("Defe"):])
    elif value.startswith("Conoserver"):
        db_url = "http://www.conoserver.org/index.php?page=card&table=protein&id={}".format(value[len("ConoserverP0"):])
    elif value.startswith("AVP"):
        db_url = "http://crdd.osdd.net/servers/avpdb/record.php?details={}".format(value)
    elif value.startswith("MAVP"):
        db_url = "http://crdd.osdd.net/servers/avpdb/record2.php?details={}".format(value)
    elif value.startswith("LAMP"):
        db_url = "http://biotechlab.fudan.edu.cn/database/lamp/detail.php?id={}".format(value[len("LAMP"):])
    elif value.startswith("ADAM"):
        db_url = "http://bioinformatics.cs.ntou.edu.tw/ADAM/adam_info.php?f={}".format(value)
    elif value.startswith("YADAMP"):
        db_url = "http://yadamp.unisa.it/showItem.aspx?yadampid={}".format(value[len("YADAMP"):])
    elif value.startswith("Peptaibol"):
        db_url = "http://peptaibol.cryst.bbk.ac.uk/singlerecords/{}.html".format(value[len("Peptaibol"):])
    elif value.startswith("CPP"):
        db_url = "http://crdd.osdd.net/raghava/cppsite/display.php?details={}".format(value[len("CPP"):])
    elif value.startswith("uniprot"):
        db_url = "https://www.uniprot.org/uniprot/{}".format(value[len("uniprot"):])
    else:
        return value.replace("_", " ")

    if len(additional_classes) > 0:
        classes = " class='{}'".format(" ".join(additional_classes))
    else:
        classes = ""

    db_link = "<a href='{}' target='_blank' title='Go to entry page'{}>{}</a>".format(db_url,
                                                                                      classes,
                                                                                      value)

    return db_link


ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
restype_color = re.compile(r'[0-9]*m[a-zA-Z]+')


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Usage: {} OUTPUT_FILE".format(sys.argv[0]))
        sys.exit(-1)
    else:
        fin = sys.argv[1]
        fout = "{}.html".format(fin)

    env = Environment(
        loader=FileSystemLoader('templates'),
        autoescape=False
    )

    env.filters["dblink"] = create_dblink

    template = env.get_template('family_template.html')

    with open(fin) as fp:
        print("Converting '{}' to fully-fledged HTML:".format(fp.name))
        raw_content = ""

        family = None

        family_id = -1
        nfamilies = -1
        family_size = -1

        current_section = None
        current_property = None
        section_counter = 0

        property_names = []
        property_names_curated = False

        summary_read_next_line = False

        cur_seek_pos = -1
        old_seek_pos = 0
        while old_seek_pos != cur_seek_pos:
            old_seek_pos = cur_seek_pos
            cur_seek_pos = fp.tell()
            line = fp.readline()

            if line.startswith("Calculating for Family"):
                line = line.split()

                family_id = int(line[3][:line[3].find("(")])
                nfamilies = int(line[-1][:line[-1].find(")")])

                print("- this file contains family {} out of {}".format(family_id, nfamilies))

            elif "The best alignment is" in line:
                line = line.strip().split()

                assert int(line[1]) == family_id

                family_size = int(line[3])
                best_alignement = line[-1]

                # Initialize family
                family = Family(family_id, nfamilies, family_size, best_alignement)

                print("- Family size: {}; best alignement: {}".format(family_size, best_alignement))

                # FIXME later
                #if family_size > 1000:
                #    print("ERROR: number of members ({}) is greater than limit (1000): Aborting!".format(family_size))
                #    sys.exit(1)

            elif line.startswith("Colouring by frequency"):
                current_section = "frequency"

                assert family.size > 0

            elif line.startswith("Colouring by residue type"):
                current_section = "restype"

                assert family.size > 0

            elif line.startswith("Secondary structure prediction"):
                assert family.size > 0

                if len(family[0].ss_predicted) == 0:
                    current_section = "ss_prediction"

            elif line.startswith("DSSP structure"):
                assert family.size > 0

                if len(family[0].ss_dssp) == 0:
                    current_section = "ss_dssp"

            elif line.startswith("Colouring by polarity"):
                current_section = "polarity"

                assert family.size > 0

            elif line.startswith("This family has the following properties:"):
                assert family.size > 0

                current_section = "property_table"

            elif is_property_value_header(line, family):
                current_section = "property_value"
                current_property = get_property_header(line)

            elif line.startswith("SUMMARY PER POSITION WITHIN THE PEPTIDE"):
                assert family_size > 0
                current_section = "summary_per_position"
            else:
                if current_section == "frequency":

                    if section_counter == 0:
                        for val in line.split("\x1b[1;48;5;"):
                            val = val.split(";")
                            if len(val) > 1:
                                ansi_bgcolor = val[0]
                                percent_range = val[3][2:].strip(" %")

                                family.frequency_colorscale.append([ansi_bgcolor, percent_range])
                        section_counter += 1
                    else:
                        fp.seek(cur_seek_pos)
                        member_data = extract_member_data_from_section(fp, family)

                        for i, member_line in enumerate(member_data):
                            elems = []
                            for val in member_line.split("\x1b[")[:-1]:
                                val = val.split(";")
                                if len(val) > 1:
                                    try:
                                        letter = val[-1][-1]
                                    except IndexError:  # raised if the last last is empty so it means the last character was actually ";"
                                        letter = ";"
                                    if len(val) == 7:
                                        ansi_bgcolor = val[3]
                                    else:
                                        ansi_bgcolor = None
                                    elems.append((ansi_bgcolor, letter))

                            sequence = "".join(val[1] for val in elems)
                            frequencies = [val[0] for val in elems]

                            member = family[i]
                            member.update_sequence(sequence)
                            member.frequencies = frequencies

                            val = member_line.split(" ; ")[1].strip()
                            if val == "parent":
                                member.is_father = True
                            else:
                                try:
                                    similarity = float(val.split("=")[1].strip("%"))
                                except ValueError:
                                    print("DEBUG: '{}'".format("\n\n".join(member_data)))
                                    print(len(member_data))
                                else:
                                    member.similarity = similarity

                        current_section = None
                        section_counter = 0
                        print("- Frequency output processed ({} values)".format(len(member_data)))

                elif current_section == "restype":
                    if section_counter == 0:
                        for val in line.split("\x1b["):
                            val = val.split(";")
                            for item in val:
                                if restype_color.search(item):  # line is like 123mAminoacid
                                    m_pos = item.find("m")

                                    color = item[:m_pos]
                                    info = item[m_pos + 1:].strip(".")

                                    family.restype_colorscale.append([color, info])
                        section_counter += 1

                    else:
                        fp.seek(cur_seek_pos)
                        member_data = extract_member_data_from_section(fp, family)

                        for i, member_line in enumerate(member_data):
                            elems = []
                            for val in member_line.split("\x1b[")[:-1]:
                                val = val.split(";")
                                if len(val) > 1:
                                    letter_item = val[-1]

                                    m_pos = letter_item.find("m")
                                    ansi_color = letter_item[:m_pos]

                                    elems.append(ansi_color)

                            member = family[i]
                            assert len(elems) == len(member.aligned_sequence)

                            member.restypes = elems

                        current_section = None
                        section_counter = 0

                        print("- Residue type output processed ({} values)".format(len(member_data)))

                elif current_section == "polarity":
                    if section_counter == 0:
                        for val in line.split("\x1b["):
                            val = val.split(";")
                            for item in val:
                                if restype_color.search(item):  # line is like 123mAminoacid
                                    m_pos = item.find("m")

                                    color = item[:m_pos]
                                    info = item[m_pos + 1:].strip(".")

                                    family.polarity_colorscale.append([color, info])

                        section_counter += 1

                    else:
                        # We need to skip the first line after the legend
                        if section_counter < 3:
                            section_counter += 1
                            continue

                        fp.seek(cur_seek_pos)
                        member_data = extract_member_data_from_section(fp, family)

                        for i, member_line in enumerate(member_data):
                            elems = []
                            for val in member_line.split("\x1b[")[:-1]:
                                val = val.split(";")
                                if len(val) > 1:
                                    letter_item = val[-1]

                                    m_pos = letter_item.find("m")
                                    ansi_color = letter_item[:m_pos]

                                    elems.append(ansi_color)

                            member = family[i]
                            assert len(elems) == len(member.aligned_sequence)

                            member.polarities = elems

                        current_section = None
                        section_counter = 0

                        print("- Polarity type output processed ({} values)".format(len(member_data)))

                elif current_section == "ss_prediction":

                    if section_counter > 1:
                        fp.seek(cur_seek_pos)
                        member_data = extract_member_data_from_section(fp, family)

                        for i, member_line in enumerate(member_data):
                            member = family[i]
                            ss_elems = extract_ss_elem_from_line(member_line, member.aligned_sequence)
                            member.ss_predicted = ss_elems
                        current_section = None
                        section_counter = 0

                        print("- Secondary structure prediction processed ({} values)".format(len(member_data)))
                    section_counter += 1

                elif current_section == "ss_dssp":

                    if section_counter > 1:
                        fp.seek(cur_seek_pos)
                        member_data = extract_member_data_from_section(fp, family)

                        for i, member_line in enumerate(member_data):
                            member = family[i]
                            ss_elems = extract_ss_elem_from_line(member_line, member.aligned_sequence)
                            member.ss_dssp = ss_elems
                        current_section = None
                        section_counter = 0

                        print("- DSSP prediction processed ({} values)".format(len(member_data)))
                    section_counter += 1

                elif current_section == "property_table":
                    if line.startswith("      ") and not property_names_curated:  # We are inside the table header
                        # First, remove ansi formatting
                        line = line.replace("\x1b[1;1;38;5;17m", "")
                        line = line.replace("\x1b[0m", "")
                        line = line.strip("\n")[7:]

                        name_counter = 0
                        for i in range(0, len(line), 3):  # Letters are 3 characters apart
                            character = line[i]
                            try:
                                property_names[name_counter] += character
                            except IndexError:
                                property_names.append(character)

                            name_counter += 1

                    elif len(property_names) > 0 and line.strip() == "":
                        for i in range(len(property_names)):
                            property_names[i] = property_names[i].strip(" 0123456789")
                        property_names_curated = True

                    if line.split(".")[0].strip() == "{}".format(family_size):
                        section_counter = 0
                        current_section = None

                        for raw_name in property_names:
                            values = list(list() for _ in family.members)

                            prop = Property(raw_name, values=values)

                            family.properties[raw_name] = prop

                        print("- {} property names extracted from table".format(len(family.properties)))

                elif current_section == "property_value":
                    fp.seek(cur_seek_pos)
                    member_properties = read_property_values(fp, family)

                    for i, prop_list in enumerate(member_properties):
                        family.properties[current_property][i] = prop_list

                    print("- Values for '{}' property processed "
                          "({} values found)".format(current_property,
                                                     len(member_properties)))

                    section_counter = 0
                    current_property = None
                    current_section = None

                elif current_section == "summary_per_position":
                    if section_counter > 5 and ansi_escape.sub("", line.strip()) == "------------------------------" \
                                                                                    "-------------------------------" \
                                                                                    "-------------------":
                        section_counter = 0
                        current_section = None
                    elif line.split(":")[0].strip().isdigit():
                        elems = []
                        for chunk in line[line.find(":")+1:].strip().split("\x1b"):
                            val = chunk.split(";")[-1]

                            if val.startswith("[0m"):
                                val = val[3:]

                            if len(val) != 0:
                                pos = val.find("m")

                                if pos > 0:
                                    color = val[:pos].strip()
                                else:
                                    color = "0"

                                for character in val[pos + 1:]:
                                    elems.append((color, character))

                        family.summary_per_position.append(elems)
                    elif line.startswith("Best representing sequence"):
                        line = line[line.find(":")+1:]

                        pos = line.find("(")
                        sequence = line[:pos].strip()
                        ids = list(val.strip(">") for val in line[pos+1:].strip("\n)(").split(";") if val != "")

                        family.best_representing_sequence = sequence
                        family.best_representing_ids = ids
                    elif summary_read_next_line and len(family.ss_prediction) == 0:
                        family.ss_prediction = extract_ss_elem_from_line(line, family.best_alignment)
                        summary_read_next_line = False
                    elif summary_read_next_line and len(family.dssp_prediction) == 0:

                        if "Alpha-helix" not in line and line.strip() != "":
                            family.dssp_prediction = extract_ss_elem_from_line(line, family.best_alignment)

                    else:
                        if line.startswith("Consensus"):
                            summary_read_next_line = True
                        elif line.startswith("DSSP prediction"):
                            summary_read_next_line = True
                        else:
                            summary_read_next_line = False
                    section_counter += 1
                #else:
                    #print(line)


    with open(fout, "w") as fp:
        print("INFO: Rendering the HTML code...")
        sys.stdout.flush()
        html_code = template.render(family=family, error_msg=error_msg)

        try:
            print("INFO: Minifying the HTML output...")
            sys.stdout.flush()
            html_code = htmlmin.minify(html_code, remove_comments=True, remove_empty_space=True)
        except:
            print("INFO: HTML output could not be minified")
            pass

        fp.write(html_code)

    print("{} succesfully save to {}".format(fin, fout))
